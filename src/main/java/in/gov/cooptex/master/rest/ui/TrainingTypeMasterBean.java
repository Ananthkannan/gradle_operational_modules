package in.gov.cooptex.master.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TrainingDTO;

import in.gov.cooptex.core.model.TenderCategory;
import in.gov.cooptex.core.model.TrainingTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("trainingTypeMasterBean")
public class TrainingTypeMasterBean {

	private final String CREATE_PAGE = "/pages/masters/trainingtype/createTrainingType.xhtml?faces-redirect=true";

	private final String LIST_PAGE_URL = "/pages/masters/trainingtype/listTrainingType.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/trainingtype/viewTrainingType.xhtml?faces-redirect=true;";
	@Getter
	@Setter
	TrainingTypeMaster trainingTypeMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	TrainingDTO trainingDTO;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<TrainingDTO> trainingTypeMasterLazyList;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	public String gotoPage(String URL) {

		log.info("Goto Page [" + URL + "]");

		if (URL.equalsIgnoreCase("LIST")) {
			return gotoListPage();
		} else if (URL.equalsIgnoreCase("ADD")) {
			action = "ADD";
			return create();
		} else if (URL.equalsIgnoreCase("EDIT")) {
			action = "EDIT";
			return editTrainingTypeMaster();
		} else if (URL.equalsIgnoreCase("VIEW")) {
			action = "VIEW";
			return gotoViewPage();
		} else {
			return null;
		}

	}

	private String gotoViewPage() {

		if (trainingDTO == null || trainingDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}

		trainingTypeMaster = getTrainingTypeMasterById(trainingDTO.getId());
		return VIEW_PAGE_URL;
	}

	public TrainingTypeMaster getTrainingTypeMasterById(Long id) {
		String URL = appPreference.getPortalServerURL() + "/trainingtypemaster/get/" + id;
		TrainingTypeMaster trainingTypeMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				trainingTypeMaster = mapper.readValue(jsonValue, TrainingTypeMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return trainingTypeMaster;
	}

	public String showList() {
		log.info("<---------- Loading TrainingTypeMasterBean showList list page---------->");
		trainingTypeMaster = new TrainingTypeMaster();
		return LIST_PAGE_URL;
	}

	public String create() {

		log.info("create method is executing..");
		String URL = appPreference.getPortalServerURL();
		log.info("create method is executing.." + URL + action);
		if (action.equalsIgnoreCase("ADD")) {
			log.info("inside add method");
			URL = URL + "/trainingtypemaster/create";
		}

		if (action.equalsIgnoreCase("EDIT")) {
			log.info("inside edit method");
			URL = URL + "/trainingtypemaster/update";
		}

		try {
			BaseDTO baseDTO = httpService.post(URL, trainingTypeMaster);
			log.info("--saved" + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Saved successfully");
					if (action.equalsIgnoreCase("ADD")) {
						log.info("saved");
						errorMap.notify(ErrorDescription.TRAININGTYPE_SAVED_SUCCESSFULLY.getCode());
					} else
						errorMap.notify(ErrorDescription.TRAININGTYPE_UPDATED_SUCCESSFULLY.getCode());

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return gotoListPage();
	}

	public String addTrainingType() {
		log.info("<---- Start TrainingTypeMasterBean.addTrainingType ---->");
		try {
			action = "ADD";
			
			trainingTypeMaster = new TrainingTypeMaster();
			trainingTypeMaster.setStatus(true);

		} catch (Exception e) {
			log.error("Error occured while redirectin", e);
		}
		return CREATE_PAGE;
	}

	public void processDelete() {
		if (trainingDTO == null || trainingDTO.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		log.info( trainingDTO.getId());
		String URL = appPreference.getPortalServerURL() + "/trainingtypemaster/delete/" + trainingDTO.getId();

		try {

			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				disableAddButton = false;
				errorMap.notify(ErrorDescription.TRAININGTYPE_DELETE_SUCCESSFULLY.getCode());
				return LIST_PAGE_URL;
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public String gotoListPage() {
		trainingDTO = new TrainingDTO();
		disableAddButton = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	public void loadLazyList() {

		trainingTypeMasterLazyList = new LazyDataModel<TrainingDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<TrainingDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<TrainingDTO> data = new ArrayList<TrainingDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<TrainingDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) throws ParseException {

				BaseDTO baseDTO = new BaseDTO();

				log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
						+ "sortField:[" + sortField + "]");

				for (Map.Entry<String, Object> entry : filters.entrySet()) {
					log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
				}

				TrainingDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

				String URL = appPreference.getPortalServerURL() + "/trainingtypemaster/list";

				baseDTO = httpService.post(URL, request);

				return baseDTO;
			}

			private TrainingDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
				TrainingDTO request = new TrainingDTO();

				PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
				request.setPaginationDTO(paginationDTO);

				for (Map.Entry<String, Object> entry : filters.entrySet()) {
					String value = entry.getValue().toString();

					if (entry.getKey().equals("code")) {
						log.info("code : " + value);
						request.setCode(value);
					}

					if (entry.getKey().equals("name")) {
						log.info("name : " + value);
						request.setName(value);
					}

					if (entry.getKey().equals("l_name")) {
						log.info("l_name : " + value);
						request.setL_name(value);
					}

					if (entry.getKey().equals("remarks")) {
						log.info("remarks : " + value);
						request.setRemarks(value);
					}

					if (entry.getKey().equals("status")) {
						log.info("...........status.............. : " + value);
						request.setStatus(Boolean.valueOf(value));
					}

				}

				return request;
			}

			@Override
			public Object getRowKey(TrainingDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TrainingDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<TrainingDTO> responseList = (List<TrainingDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (TrainingDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		trainingDTO = ((TrainingDTO) event.getObject());
		disableAddButton = true;
	}

	public String editTrainingTypeMaster() {
		try {
			if (trainingDTO == null || trainingDTO.getId() == null) {
				log.info("----select TrainingTypeMaster-------");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<===== trainingDTO :: " + trainingDTO.getId());

		trainingTypeMaster = getTrainingTypeMasterById(trainingDTO.getId());

		return CREATE_PAGE;

	}

}
