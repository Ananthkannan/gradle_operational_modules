package in.gov.cooptex.dashboard.ui.bean;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DashBoardChartDTO;
import in.gov.cooptex.core.dto.DashboardDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("dashboardBean")
@Scope("session")
@Log4j2
public class DashboardBean extends CommonBean {

	private static final String PORTAL_SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	private List<DashBoardChartDTO> salesValueComparison = new ArrayList<DashBoardChartDTO>();

	@Getter
	@Setter
	private Map<String, String> janMap = new LinkedHashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> febMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> marMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> aprMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> mayMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> junMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> julMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> augMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> sepMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> octMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> novMap = new HashMap<String, String>();

	@Getter
	@Setter
	private Map<String, String> decMap = new HashMap<String, String>();

	@Getter
	@Setter
	private String janval, febval, marval, aprval, mayval, junval, julval, augval, sepval, octval, novval, decval,
			yearval;

	@Getter
	@Setter
	private String janProgressval, febProgressval, marProgressval, aprProgressval, mayProgressval, junProgressval,
			julProgressval, augProgressval, sepProgressval, octProgressval, novProgressval, decProgressval,
			yearProgressval;

	@Getter
	private List<DashBoardChartDTO> dailyActivities = new ArrayList<DashBoardChartDTO>();

	@Getter
	private List<DashBoardChartDTO> topMovingProductsList = new ArrayList<DashBoardChartDTO>();

	@Getter
	@Setter
	List<EntityMaster> regionList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	EntityMaster selectedRegion = new EntityMaster();

	@Getter
	@Setter
	EntityMaster selectedShowroom;

	@Getter
	@Setter
	List<EntityMaster> showroomList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	DashboardDTO headerDTO = new DashboardDTO();
	
	@Getter
	@Setter
	DashboardDTO showroomInfoDTO = new DashboardDTO();

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	String salesValues;
	
	@Getter
	@Setter
	private Map<String, Integer> currentMonthYearMap = new HashMap<String, Integer>();
	
	@Getter
	@Setter
	String entityCode;
	
	@Getter
	@Setter
	Date searchDate;

	public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	@PostConstruct
	public void init() {
		log.info("<<=====  INIT CALLED  ======>>");
		getEntityTypeCodeByUserId();
		salesValueSummary();
		loadHeaderValues();
		salesProgressSummary();
		dailyActivityReport();
		topMovingProducts();
		loadRegionwiseShowroomData();
	}

	public void loadHeaderValues() {
		log.info("<<========================  HEADER VALUES =======## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = PORTAL_SERVER_URL + "/dashboard/home/headerValues";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				log.info("<===================" + baseDTO.getStatusCode());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				headerDTO = mapper.readValue(jsonResponse, DashboardDTO.class);
				log.info("<<===============  HEADER VALUES ::: " + headerDTO);
			}

		} catch (Exception e) {
			log.error("<<===============   ERROR in fetching HEADER VALUES ::: ", e);
		}
		log.info("<<========================  HEADER VALUES =======## ENDS");
	}

	public void salesValueSummary() {
		log.info("<<=======  SALES VALUE SUMMARY ======== ## STARTS");
		salesValueComparison.clear();
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = PORTAL_SERVER_URL + "/dashboard/home/salesvalue/"+entityCode;
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				log.info("<===================" + baseDTO.getStatusCode());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				salesValueComparison = mapper.readValue(jsonResponse, new TypeReference<List<DashBoardChartDTO>>() {
				}); 
				if (salesValueComparison != null) {
					for (DashBoardChartDTO dto : salesValueComparison) {
						switch (dto.getMonth()) {
						case "Jan":
							janMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Feb":
							febMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Mar":
							marMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Apr":
							aprMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "May":
							mayMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Jun":
							junMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Jul":
							julMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Aug":
							augMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Sep":
							sepMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Oct":
							octMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Nov":
							novMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						case "Dec":
							decMap.put(dto.getYear().toString(), dto.getPrice().toString());
							break;
						}
					}

					yearval = yearValueGenerator();
					janval = salesValueGenerator(janMap, "Jan");
					febval = salesValueGenerator(febMap, "Feb");
					marval = salesValueGenerator(marMap, "Mar");
					aprval = salesValueGenerator(aprMap, "Apr");
					mayval = salesValueGenerator(mayMap, "May");
					junval = salesValueGenerator(junMap, "Jun");
					julval = salesValueGenerator(julMap, "Jul");
					augval = salesValueGenerator(augMap, "Aug");
					sepval = salesValueGenerator(sepMap, "Sep");
					octval = salesValueGenerator(octMap, "Oct");
					novval = salesValueGenerator(novMap, "Nov");
					decval = salesValueGenerator(decMap, "Dec");

				}
			}

		} catch (Exception e) {
			log.error("<<====  ERROR ::::", e);
		}

		log.info("<<=======  SALES VALUE SUMMARY ======== ## ENDS");
	}

	private String salesValueGenerator(Map<String, String> monthMap, String month) {
		log.info("<<====  salesValueGenerator  ====>>");
		StringBuilder sb = new StringBuilder();
		sb.append("['" + month + "',");
		Set<String> yearKeySet = monthMap.keySet();
		int count = 0;
		for (String key : yearKeySet) {
			sb.append(monthMap.get(key));
			if (count < yearKeySet.size()) {
				sb.append(",");
			}
			count++;
		}
		sb.append("]");

		// salesValues=sb.toString();
		log.info(":: " + sb.toString());
		return sb.toString();
	}

	private String yearValueGenerator() {
		StringBuilder sb = new StringBuilder();
		sb.append("['Month',");
		Set<String> yearKeySet = janMap.keySet();
		int count = 0;
		for (String key : yearKeySet) {
			sb.append("'" + key.toString() + "'");
			if (count < yearKeySet.size()) {
				sb.append(",");
			}
			count++;
		}
		sb.append("]");
		return sb.toString();
	}

	public void salesProgressSummary() {
		log.info("<<=================  dashboardBean --  salesProgressSummary   =======## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = PORTAL_SERVER_URL + "/dashboard/salesprogress/"+entityCode;
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				log.info("<===================" + baseDTO.getStatusCode());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				salesValueComparison = mapper.readValue(jsonResponse, new TypeReference<List<DashBoardChartDTO>>() {
				});
				log.info("<<===============  salesValueComparison::  " + salesValueComparison);
				if (salesValueComparison != null) {
					for (DashBoardChartDTO dto : salesValueComparison) {
						switch (dto.getMonth()) {
						case "Jan":
							janProgressval = salesProgressGenerator(dto);
							break;
						case "Feb":
							febProgressval = salesProgressGenerator(dto);
							break;
						case "Mar":
							marProgressval = salesProgressGenerator(dto);
							break;
						case "Apr":
							aprProgressval = salesProgressGenerator(dto);
							break;
						case "May":
							mayProgressval = salesProgressGenerator(dto);
							break;
						case "Jun":
							junProgressval = salesProgressGenerator(dto);
							break;
						case "Jul":
							julProgressval = salesProgressGenerator(dto);
							break;
						case "Aug":
							augProgressval = salesProgressGenerator(dto);
							break;
						case "Sep":
							sepProgressval = salesProgressGenerator(dto);
							break;
						case "Oct":
							octProgressval = salesProgressGenerator(dto);
							break;
						case "Nov":
							novProgressval = salesProgressGenerator(dto);
							break;
						case "Dec":
							decProgressval = salesProgressGenerator(dto);
							break;
						}
					}

				}
			}

		} catch (Exception e) {
			log.info("<<======  ERROR::: ", e);
		}
		log.info("<<=================  dashboardBean --  salesProgressSummary   =======## ENDS");
	}

	private String salesProgressGenerator(DashBoardChartDTO dto) {
		log.info("<<====  salesProgressGenerator  ====>>");
		StringBuilder sb = new StringBuilder();
		sb.append("['" + dto.getMonth() + "',");
		sb.append("" + dto.getTarget() + ",");
		sb.append("" + dto.getPrice() + "");
		sb.append("]");

		// salesValues=sb.toString();
		return sb.toString();
	}

	public void dailyActivityReport() {
		log.info("<<=========  dashboardBean ----- dailyActivityReport =======## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = PORTAL_SERVER_URL + "/dashboard/dailyactivitysummary/"+entityCode;
			baseDTO = httpService.get(url);
			dailyActivities.clear();
			if (baseDTO != null) {
				log.info("<===================" + baseDTO.getStatusCode());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				dailyActivities = mapper.readValue(jsonResponse, new TypeReference<List<DashBoardChartDTO>>() {
				});
				// activityString = prepareActivityChart(dailyActivities);
			}
		} catch (Exception e) {
			log.error("<<=====  Error:::: ", e);
		}
		log.info("<<=========  dashboardBean ----- dailyActivityReport =======## ENDS");
	}

	/*
	 * private String prepareActivityChart(List<DashBoardChartDTO> activities) {
	 * StringBuffer sb = new StringBuffer(); for(DashBoardChartDTO dto : activities)
	 * { sb.append("['"+dto.getName()+"',"+dto.getPrice()+"],"); } return
	 * sb.toString(); }
	 */

	public void topMovingProducts() {
		log.info("<<=========  dashboardBean ----- topMovingProducts =======## STARTS");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = PORTAL_SERVER_URL + "/dashboard/topmovingproduct";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				log.info("<===================" + baseDTO.getStatusCode());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				topMovingProductsList = mapper.readValue(jsonResponse, new TypeReference<List<DashBoardChartDTO>>() {
				});

			}else {
				topMovingProductsList = new ArrayList<>();
			}
			log.info("<<==========topMovingProductsList ::: " + topMovingProductsList);
		} catch (Exception e) {
			log.error("<<=====  Error:::: ", e);
		}
		log.info("<<=========  dashboardBean ----- topMovingProducts =======## ENDS");
	}

	
	public void getEntityTypeCodeByUserId() {
		log.info("<<================= dashboardBean --- getEntityTypeCodeByUserId ====## STARTS  ");
		try {
			String URL = PORTAL_SERVER_URL + "/dashboard/getentitytypebyuserid";
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityCode = mapper.readValue(jsonResponse, String.class);
				log.info("entityCode ----> "+entityCode);
			}
		} catch (Exception e) {
			log.error("<<========  ERROR in getEntityTypeCodeByUserId() ::: ", e);												
		}

		log.info("<<================= dashboardBean --- getEntityTypeCodeByUserId() ====## ENDS  ");
	}
	
	public void loadRegionwiseShowroomData() {
		log.info("<<================= dashboardBean --- loadRegionwiseShowroomData ====## STARTS  ");
		try {
			// regionList = commonDataService.getProducingRegion();
			// regionList = commonDataService.loadRegionalOfficeFinance();
			String URL = PORTAL_SERVER_URL + "/entitymaster/region/getall";
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				regionList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("<<========  ERROR in loadRegionwiseShowroomData ::: ", e);												
		}

		log.info("<<================= dashboardBean --- loadRegionwiseShowroomData ====## ENDS  ");
	}

	public void onchangeRegion() {
		log.info("<<============ dashboardBean ---- onchangeRegion =========## STARTS  " );
		try {
			showroomList = commonDataService.loadShowroomListForRegion(selectedRegion.getId());
		} catch (Exception e) {
			log.error("<<============  ERROR in onchangeRegion :::: ", e);
		}
		log.info("<<============ dashboardBean ---- onchangeRegion =========## ENDS");
	}
	
	public void loadShowroomData() {
		log.info("<<============ dashboardBean --------  loadShowroomData ======##STARTS");
		try {
			if(searchDate != null) {
				String salesInvoiceDate = DATE_FORMAT.format(searchDate);
				String URL = PORTAL_SERVER_URL + "/dashboard/home/showroomData/"+selectedShowroom.getId()+"/"+salesInvoiceDate;
				log.info("URL - "+URL);
				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					showroomInfoDTO = mapper.readValue(jsonResponse, DashboardDTO.class);
					log.info("showroomInfoDTO list size "+showroomInfoDTO.getSellingProductList().size());
					Calendar cal = Calendar.getInstance();
					cal.setTime(searchDate);
					showroomInfoDTO.setCurrentYear(cal.get(Calendar.YEAR));
					Integer month = cal.get(Calendar.MONTH) + 1;
					currentMonthYearMap = Util.getMonthIdsByDetailName(); 
					for(Map.Entry<String,Integer> entry: currentMonthYearMap.entrySet()){
			            if(month.equals(entry.getValue())){
			            	showroomInfoDTO.setCurrentMonth(entry.getKey()); 
			            }
			        }
					RequestContext context = RequestContext.getCurrentInstance();
					context.update("dashboardform:showroomdatapnl");
				}
				log.info("<<==================  Showroom DATA:::  "+showroomInfoDTO);
			}
		}catch(Exception e) {
			log.error("<<=========  ERROR in loadShowroomData :::: ",e);
		}
		log.info("<<============ dashboardBean --------  loadShowroomData ======##ENDS");
	}
	
	public String formatDecimalNumber(Double number) {
		log.info("Number:::"+number);
		if(number==null)
			return "0.00";
		DecimalFormat decim = new DecimalFormat("0.00");
	    String price = decim.format(number);
	    return price;
	}
	
	
}
