package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;

import in.gov.cooptex.core.model.DiscountMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.SaleTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("discountMasterBean")
public class DiscountMasterBean {

	/**
	 * 
	 */
	private final String LIST_PAGE = "/pages/masters/listDiscountMaster.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE = "/pages/masters/createDiscountMaster.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/viewDiscountMaster.xhtml?faces-redirect=true;";
	public static String serverURL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private String pageAction;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	DiscountMaster discountMasterValue = new DiscountMaster();

	@Getter
	@Setter
	List<DiscountMaster> discountMasterList = new ArrayList<>();

	@Getter
	@Setter
	private int discountMasterListSize;

	boolean forceLogin = false;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<>();

	@Getter
	@Setter
	StateMaster stateMaster = new StateMaster();

	@Getter
	@Setter
	List<EntityMaster> showRoomList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Autowired
	CommonDataService commonDataService;
	// =====Product category, group, variety =========//

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductCategory> selectProductCategoryList = new ArrayList<>();

	@Getter
	@Setter
	ProductCategory productCategory = new ProductCategory();

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster = new ProductGroupMaster();

	@Getter
	@Setter
	List<ProductGroupMaster> selectProductGroupMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> selectProductVarietyMasterList;

	@Getter
	@Setter
	LazyDataModel<DiscountMaster> lazyDiscountMasterList;
	@Getter
	@Setter
	DiscountMaster selectedDiscountMaster = new DiscountMaster();

	@Getter
	@Setter
	DiscountMaster discountMaster;

	DiscountMaster disMaster = null;

	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	List<SaleTypeMaster> saleTypeMasterList = new ArrayList<>();
	

	// ---Product-----

	ObjectMapper mapper;

	String jsonResponse;

	@PostConstruct
	public void init() {

		log.info(".......discountMaster Init is executed.....................");
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loadDiscountValue();

	}

	public void loadDiscountValue() {
		try {
			stateList = loadStatesBasedOnEntity();
			saleTypeMasterList=commonDataService.loadActiveSalesTypes();
			productCategoryList = commonDataService.loadProductCategories();
			log.info("loadDiscountValue state list-------" + stateList.size());
			log.info("productCategoryList state list-------" + productCategoryList.size());
		} catch (Exception e) {
			log.error("loadDiscountValue Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public String loadDiscountMasterPage() {
		try {
			if (pageAction.equalsIgnoreCase("LIST")) {
				addButtonFlag = false;
				setSelectedDiscountMaster(null);

				loadLazyDiscountMasterList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				discountMasterValue = new DiscountMaster();
				setEntityMasterList(null);
				setProductVarietyMasterList(null);
				setProductGroupMasterList(null);
				setProductVarietyMasterList(null);
				discountMasterValue.setSelectedProductGroupMasterList(null);
				discountMasterValue.setSelectedProductVarietyMasterList(null);
				discountMasterValue.setSelectedProductCategoryList(null);
				
				setDiscountMasterList(null);
				return CREATE_PAGE;
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				setEntityMasterList(null);
				setProductVarietyMasterList(null);
				setProductGroupMasterList(null);
				setProductVarietyMasterList(null);
				discountMasterValue.setSelectedProductGroupMasterList(null);
				discountMasterValue.setSelectedProductVarietyMasterList(null);
				discountMasterValue.setSelectedProductCategoryList(null);
				setDiscountMasterList(null);
				return loadDiscountMasterViewPage();
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				return loadDiscountMasterViewPage();

			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				if (selectedDiscountMaster == null || selectedDiscountMaster.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return null;
			}
		} catch (Exception e) {
			log.error("showViewPage Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String deleteDiscount() {
		try {

			if (selectedDiscountMaster == null || selectedDiscountMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("delete discount  Id : : " + selectedDiscountMaster.getId());
			String getUrl = AppUtil.getPortalServerURL() + "/discount/delete/id/:id";
			String url = getUrl.replace(":id", selectedDiscountMaster.getId().toString());
			log.info("deleteDiscount Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				AppUtil.addInfo("Discount master deleted successfully");
				loadLazyDiscountMasterList();
				return null;
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				AppUtil.addWarn("Discount master not able deleted");
				return null;
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Discount Master  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;
	}

	public String loadDiscountMasterViewPage() {
		try {
			log.info("<=loadDiscountMasterViewPage=>");
			if (selectedDiscountMaster == null || selectedDiscountMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String apiURL = AppUtil.getPortalServerURL() + "/discount/get/" + selectedDiscountMaster.getId();
			log.info("loadDiscountMasterViewPage apiURL==>" + apiURL);
			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				discountMasterValue = mapper.readValue(jsonValue, DiscountMaster.class);
				
				if(discountMasterValue!=null && discountMasterValue.getShowRoom()!=null) {
					  stateMaster =commonDataService.getStateMasterByEntity(discountMasterValue.getShowRoom().getId());
				}
				if(discountMasterValue!=null && discountMasterValue.getProductVariety()!=null) {
					  productGroupMaster = commonDataService.getGroupByProduct(discountMasterValue.getProductVariety().getId());
				}
				if(productGroupMaster!=null && productGroupMaster.getId()!=null) {
					  productCategory = commonDataService.getCategoryByGroupId(productGroupMaster.getId());
				}
				 
				if (pageAction.equalsIgnoreCase("EDIT")) {
					showListOfShowRooms();
					showListOfProductCategory();
					getProductByGroup();

					return CREATE_PAGE;
				} else if (pageAction.equalsIgnoreCase("VIEW")) {
					return VIEW_PAGE;
				}
			}

		} catch (Exception e) {
			log.error("loadDiscountMasterViewPage Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public void showListOfShowRooms() {
		try {

			log.info("......DiscountMaster showListOfShowRooms begin ........");
			entityMasterList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			String stateId = null;
			if(stateMaster == null || stateMaster.getId() == null) {
				log.error("State master not found");
				return;
			}
			stateId = stateMaster.getId().toString();
			log.info("showListOfShowRooms :: stateId==> "+stateId);
			String url = AppUtil.getPortalServerURL() + "/discount/getShowroomByState/" + stateMaster.getId();
			log.info("showListOfShowRooms url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("entityMasterList size==>" + entityMasterList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("showListOfShowRooms Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void showListOfProductCategory() {
		try {
			log.info("......DiscountMaster showListOfProductCategory begin ........");
			productGroupMasterList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			String proCatId = null;
			proCatId = productCategory.getId().toString();
			String url = AppUtil.getPortalServerURL() + "/discount/getProductbyCategory/" + proCatId;
			log.info("url-------------------" + url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
				log.info("ProductGroup list-------" + productGroupMasterList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("showListOfProductCategory Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts DiscountMasterBean.onRowSelect ========>" + event);
		selectedDiscountMaster = ((DiscountMaster) event.getObject());

		log.info("selectedDiscountMaster id" + selectedDiscountMaster.getId());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		log.info("<===Ends DiscountMasterBean.onRowSelect ========>");
	}

	public void loadLazyDiscountMasterList() {
		log.info("<===== Starts DiscountMasterBean.loadLazyDiscountMasterList ======>");
		lazyDiscountMasterList = new LazyDataModel<DiscountMaster>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<DiscountMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/discount/lazyload/search";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						discountMasterList = mapper.readValue(jsonResponse, new TypeReference<List<DiscountMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyDiscountMasterList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return discountMasterList;
			}

			@Override
			public Object getRowKey(DiscountMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public DiscountMaster getRowData(String rowKey) {
				try {
					for (DiscountMaster discountMaster : discountMasterList) {
						if (discountMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedDiscountMaster = discountMaster;
							return discountMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== ends DiscountMasterBean.loadLazyDiscountMasterList ======>");
	}

	public void getProductByGroup() {
		try {

			log.info("......DiscountMaster showListOfShowRooms begin ........");
			productVarietyMasterList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			String groupId = null;
			groupId = productGroupMaster.getId().toString();
			log.info("getProductByGroup groupId==>" + groupId);
			String url = AppUtil.getPortalServerURL() + "/discount/getProductGroup/" + groupId;
			log.info("showListOfShowRooms url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				log.info("productVarietyMasterList size==>" + productVarietyMasterList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("showListOfShowRooms Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void showListOfProductGroup() {
		try {
			log.info("......DiscountMaster showListProductGroup begin ........");
			BaseDTO baseDTO = new BaseDTO();
			for (ProductGroupMaster productGroup : selectProductGroupMasterList) {
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/discountmaster/getProductGroup/" + productGroup.getCode() + productGroup.getName();
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productVarietyMasterList = mapper.readValue(jsonResponse,
							new TypeReference<ProductVarietyMaster>() {
							});
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("inside exception----", e);
		}
		log.info("product group-------" + productVarietyMasterList.size());
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start DiscountMasterBean . clear ----->");
		selectedDiscountMaster = new DiscountMaster();
		discountMasterValue = new DiscountMaster();
		stateMaster = new StateMaster();
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		discountMasterList = new ArrayList<>();
		loadLazyDiscountMasterList();
		addButtonFlag = false;
		log.info("<---- End DiscountMasterBean . clear ----->");

		return LIST_PAGE;

	}

	public void clearVal() {
		discountMasterValue = new DiscountMaster();
		stateMaster = new StateMaster();
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		
	}

	public void loadEntitysBySelectedStates() {
		log.info("<======== DiscountMasterBean.loadEntitysBySelectedStates() calling ======>");
		entityMasterList = new ArrayList<>();

		BaseDTO baseDTO = null;
		try {
			String url = AppUtil.getPortalServerURL() + "/entitymaster/getentitysbyselectedstates";
			log.info("url" + url);
			baseDTO = httpService.post(url, discountMasterValue);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					entityMasterList = mapper.readValue(jsonValue, new TypeReference<List<EntityMaster>>() {
					});
					log.info("Total no. of Entitys : " + entityMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}

		} catch (Exception e) {
			log.error("<========== Exception DiscountMasterBean.loadEntitysBySelectedStates()==========>", e);
		}
	}

	public void loadAllGroupsByProductCategorys() {
		log.info("<======== DiscountMasterBean.loadAllGroupsByProductCategorys() calling ======>");
		productGroupMasterList = new ArrayList<>();
		try {

			BaseDTO baseDTO = null;
			try {
				String url = AppUtil.getPortalServerURL() + "/group/getgroupsbyproductcategoryidlist";
				log.info("url" + url);
				baseDTO = httpService.post(url, discountMasterValue);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContents() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
						productGroupMasterList = mapper.readValue(jsonValue,
								new TypeReference<List<ProductGroupMaster>>() {
								});
						log.info("Total no. of Groups : " + productGroupMasterList.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} catch (Exception e) {
				log.error("DiscountMasterBean loadAllGroupsByProductCategorys Error", e);
			}

		} catch (Exception e) {
			log.error("<========== Exception DiscountMasterBean.loadAllGroupsByProductCategorys()==========>", e);
		}
	}

	public void loadAllProductVarietysByProductGroups() {
		log.info("<======== DiscountMasterBean.loadAllProductVarietysByProductGroups() calling ======>");
		productVarietyMasterList = new ArrayList<>();
		try {

			BaseDTO baseDTO = null;
			try {
				String url = AppUtil.getPortalServerURL() + "/productvariety/getproductVarietysbyproductgroupidlist";
				log.info("url" + url);
				baseDTO = httpService.post(url, discountMasterValue);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContents() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
						productVarietyMasterList = mapper.readValue(jsonValue,
								new TypeReference<List<ProductVarietyMaster>>() {
								});
						log.info("Total no. of Groups : " + productVarietyMasterList.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} catch (Exception e) {
				log.error("DiscountMasterBean loadAllProductVarietysByProductGroups Error", e);
			}

		} catch (Exception e) {
			log.error("<========== Exception DiscountMasterBean.loadAllProductVarietysByProductGroups()==========>", e);
		}
	}

	public void loadProductDiscountInfo() {
		try {
			discountMasterList = new ArrayList<>();
			log.info("DiscountMasterBean :: loadProductDiscountInfo start==>" + discountMasterValue);
			for (EntityMaster showRoom : discountMasterValue.getSelectedShowRoomsList()) {
				log.info("ShowRooms====listsize()==" + discountMasterValue.getSelectedShowRoomsList().size());
			//	for (ProductCategory productCategory : discountMasterValue.getSelectedProductCategoryList()) {
				/*if(discountMasterValue.getProductCategory() != null) {
					discountMasterValue.setProductCategory(productCategory);
				}*/
					
				//}
				for (ProductGroupMaster productGroup : discountMasterValue.getSelectedProductGroupMasterList()) {
					discountMasterValue.setProductGroupMaster(productGroup);
				}
				for (ProductVarietyMaster productVariety : discountMasterValue.getSelectedProductVarietyMasterList()) {
					log.info("ProductVarietyMaster====listsize()=="
							+ discountMasterValue.getSelectedProductVarietyMasterList().size());
					discountMasterValue.setShowRoom(showRoom);
					discountMasterValue.setProductVariety(productVariety);
//					discountMasterValue.setSalesTypeMaster(salesTypeMaster);
					/*
					 * if (discountMasterValue == null || discountMasterValue.getId() == null) {
					 * discountMasterValue.setActiveStatus(true); }
					 */
					disMaster = new DiscountMaster();
					BeanUtils.copyProperties(discountMasterValue, disMaster);
					discountMasterList.add(disMaster);

				}
			}
			log.info("discountMasterList============>" + discountMasterList.size());
			discountMasterValue.setName(null);
			discountMasterValue.setCode(null);
			log.info("DiscountMasterBean :: loadProductDiscountInfo end");
		} catch (Exception e) {
			log.error("loadProductDiscountInfo error==>", e);
		}
	}

	public String saveUpdateDiscount() {
		try {
			log.info("<=call saveUpdateDiscount=>");
			if(discountMasterList ==null || discountMasterList.isEmpty() ) {
				errorMap.notify(ErrorDescription.DISCOUNT_MASTER_EMPTY.getErrorCode());
				return null;
			}
			log.info("<=discountMasterList=>" + discountMasterList.size());
			
			for(DiscountMaster discountMaster:discountMasterList) {
				long showroomId=discountMaster.getShowRoom().getId();
				discountMaster.setShowRoom(new EntityMaster());
				discountMaster.getShowRoom().setId(showroomId);
			
				long productVarietyId=discountMaster.getProductVariety().getId();
				discountMaster.setProductVariety(new ProductVarietyMaster());
				discountMaster.getProductVariety().setId(productVarietyId);
				
				discountMaster.setProductCategory(null);
				discountMaster.setProductGroupMaster(null);
				discountMaster.setSelectedStatesList(null);
				discountMaster.setSelectedProductCategoryList(null);
				discountMaster.setSelectedProductGroupMasterList(null);
				discountMaster.setSelectedProductVarietyMasterList(null);
				discountMaster.setSelectedShowRoomsList(null);
				discountMaster.setSelectedStatesList(null);
			}
			
			String url = AppUtil.getPortalServerURL() + "/discount/create";
			BaseDTO baseDTO = httpService.put(url, discountMasterList);
			log.info("saveUpdateDiscount Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				AppUtil.addInfo("Discount master Saved successfully");
				return LIST_PAGE;
			} else {
				log.info("Error while saveUpdateDiscount error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in saveUpdateDiscount  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private DiscountMaster getDiscountMasterDetails(Long id) {

		log.info("<====== DiscountMasterBean.getDiscountMasterDetails starts====>" + id);
		discountMaster = new DiscountMaster();
		try {
			if (selectedDiscountMaster == null || selectedDiscountMaster.getId() == null) {
				log.error(" No DiscountMasterBean selected ");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			}
			log.info("Selected DiscountMasterBean  Id : : " + selectedDiscountMaster.getId());
			String url = serverURL + "/discount/get/" + selectedDiscountMaster.getId();

			log.info("DiscountMasterBean Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			log.info("view response" + baseDTO.getResponseContent());
			discountMaster = mapper.readValue(jsonResponse, DiscountMaster.class);

			if (baseDTO.getStatusCode() == 0) {
				log.info(" DiscountMaster Retrived  SuccessFully ");
			} else {
				log.error(" DiscountMaster Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return discountMaster;
		} catch (Exception e) {
			log.error("Exception Ocured while get DiscountMaster Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editDiscountMasterDetails() {
		log.info("editDiscountMasterDetails started");
		try {
			if (selectedDiscountMaster == null || selectedDiscountMaster.getId() == null) {
				log.info("----- selectedDiscountMaster----ID- null--");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			} else {

				String apiURL = AppUtil.getPortalServerURL() + "/discount/get/" + selectedDiscountMaster.getId();
				log.info("loadDiscountMasterViewPage apiURL==>" + apiURL);
				BaseDTO baseDTO = httpService.get(apiURL);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					discountMasterValue = mapper.readValue(jsonValue, DiscountMaster.class);
					if(discountMasterValue != null) {
						if(discountMasterValue.getShowRoom() != null && discountMasterValue.getShowRoom().getId() != null) {
							stateMaster = commonDataService.getStateMasterByEntity(discountMasterValue.getShowRoom().getId());
						}else {
							log.error("Showroom not found");
						}
						if(discountMasterValue.getProductVariety() != null && discountMasterValue.getProductVariety().getId() != null) {
							productGroupMaster = commonDataService
									.getGroupByProduct(discountMasterValue.getProductVariety().getId());
						}else {
							log.error("Product variety not found");
						}
						if(productGroupMaster != null && productGroupMaster.getId() != null) {
							productCategory = commonDataService.getCategoryByGroupId(productGroupMaster.getId());
						}else {
							log.error("Product group not found");
						}
					}else {
						log.error("Discount value not found");
					}
					
					
					if (pageAction.equalsIgnoreCase("EDIT")) {
						showListOfShowRooms();
						showListOfProductCategory();
						getProductByGroup();
						return CREATE_PAGE;
					}

				}

			}

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		return null;

	}

	public List<DiscountMaster> loadAllDiscountMasterlist(Long discountCode, String discountName) {
		log.info("loadAllDiscountMasterlist started");
		try {
			discountMasterList = new ArrayList<>();
			String url = serverURL + "/discount/getalldiscountmasterbycodeandname/" + discountCode + "/" + discountName;
			log.info("url" + url);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				discountMasterList = mapper.readValue(jsonResponse, new TypeReference<List<DiscountMaster>>() {
				});
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occer at loadAllDiscountMasterlist", e);
		}
		log.info("loadAllDiscountMasterlist ends");
		return discountMasterList;
	}

	public String updateDiscountmaster() {
		log.info("<==== Starts DisconutMasterBean.UpdateDiscountMaster =======>");
		try {
			if (pageAction.equalsIgnoreCase("EDIT")) {
				BaseDTO response = httpService.post(serverURL + "/discount/update", discountMasterValue);
				if (response != null && response.getStatusCode() == 0) {
					log.info("Discont Master Updated successfully..........");
					AppUtil.addInfo("Discount master Updated successfully");
					pageAction = "LIST";
					selectedDiscountMaster = new DiscountMaster();
					return loadDiscountMasterPage();
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends DiscountMasterBean.UpdateDiscountMaster  =======>");
		return null;
	}

	public void fromDateChange() {
		log.info(":::::::::::Inside::::::validFrom:::::::::" + discountMasterValue.getValidFrom());
		try {
			if (discountMasterValue.getValidTo() != null) {
				if (discountMasterValue.getValidFrom().compareTo(discountMasterValue.getValidTo()) < 0) {
					log.info("inside condition validFromDate is Less then validToDate");
				} else {
					log.info("inside condition validFromDate is Greater then validToDate");
					discountMasterValue.setValidTo(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in  validFrom...", e);
		}
	}
	
	public List<StateMaster> loadStatesBasedOnEntity() {
		log.info("Inside loadStateList()");
		BaseDTO baseDTO = null;
		try {
			String url = AppUtil.getPortalServerURL() + "/stateMaster/getStatesbasedonentityaddress";
			log.info("loadStateList url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stateList = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
				});
			} else {
				stateList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("Error loadStateList() --==>", e);
		}
		return stateList;
	}

}
