package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.GeneralErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.personnel.hrms.dto.DeathRegistrationDTO;
import in.gov.cooptex.personnel.hrms.model.EmpDeathRegister;
import in.gov.cooptex.personnel.hrms.model.EmpDeathRegisterNote;
import in.gov.cooptex.personnel.hrms.model.NatureOfDeath;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("employeeDeathRegistrationBean")
@Scope("session")
@Log4j2
public class EmployeeDeathRegistrationBean extends CommonBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1210201368645552035L;
	
	private static final String EMPLOYEE_DEATH_REGISTRATION_CREATE_PAGE = "/pages/personnelHR/createEmployeeDeathRegistration.xhtml?faces-redirect=true";
	
	private static final String EMPLOYEE_DEATH_REGISTRATION_LIST_PAGE = "/pages/personnelHR/listEmployeeDeathRegistration.xhtml?faces-redirect=true";
	
	private static final String EMPLOYEE_DEATH_REGISTRATION_VIEW_PAGE = "/pages/personnelHR/viewEmployeeDeathRegistration.xhtml?faces-redirect=true";
	
	private static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Getter @Setter
	boolean enableFlag,disableFlag,addButtonFlag,editButtonFlag,deleteButtonFlag = true;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	String action,status;
	
	@Autowired
	ErrorMap errorMap;
	
	String jsonResponse;
	
	@Autowired
	HttpService httpService;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Getter @Setter
	Integer totalRecords;
	
	@Getter @Setter
	LazyDataModel<EmpDeathRegister> employeeDeathRegisterLazyList;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	List<EntityMaster> headOfficeAndRegionEntityList;
	
	@Getter @Setter
	EntityMaster horoEntity = new EntityMaster();
	
	@Getter @Setter
	List<EntityTypeMaster> entityTypeMasterList;
	
	@Getter @Setter
	List<EntityMaster> entityMasterList;
	
	@Getter @Setter
	List<EmployeeMaster> employeeList;
	
	@Getter @Setter
	List<NatureOfDeath> natureOfDeathList;
	
	@Getter @Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Getter @Setter
	DeathRegistrationDTO deathRegistrationDTO = new DeathRegistrationDTO();
	
	@Getter @Setter
	private UploadedFile uploadedFile;
	
	@Getter @Setter
	EmpDeathRegister selectedEmpDeathRegister;
	
	@Getter	@Setter
	EmpDeathRegisterNote deathRegisterLastNote = new EmpDeathRegisterNote();
	
	@Getter @Setter
	List<DeathRegistrationDTO> viewNoteEmployeeDetails = new ArrayList<>();
	
	@Getter @Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();
	
	@Getter @Setter
	Date currentDate = new Date();
	
	@Getter	@Setter
	private StreamedContent file;
	
	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Getter
	@Setter
	EmpDeathRegisterNote empDeathRegisterNote = new EmpDeathRegisterNote();

	public String showDeathRegistrationListPage() {
		log.info("<<====== EmployeeDeathRegistrationBean:showDeathRegistrationListPage() ======>> ");
		try {
			addButtonFlag = true;
			editButtonFlag = true;
			selectedEmpDeathRegister = new EmpDeathRegister();
			deathRegistrationDTO = new DeathRegistrationDTO();
			loadDeathRegistrationLazyList();
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:showDeathRegistrationListPage() - ",e);
		}
		return EMPLOYEE_DEATH_REGISTRATION_LIST_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<<======= EmployeeDeathRegistrationBean:onRowSelect() Calling =======>>");
		try {
			addButtonFlag = false;
			
			selectedEmpDeathRegister = ((EmpDeathRegister) event.getObject());
			Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();
			if (selectedEmpDeathRegister != null && (selectedEmpDeathRegister.getStage().equalsIgnoreCase("REJECTED")
					|| selectedEmpDeathRegister.getStage().equalsIgnoreCase("INPROGRESS"))) {
				if (loginUserId != null && selectedEmpDeathRegister.getCreatedBy()!=null
						&& selectedEmpDeathRegister.getCreatedBy().getId() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId().equals(loginUserId)) {
					editButtonFlag = true;
				} else {
					editButtonFlag = false;
				}
			} else {
				editButtonFlag = false;
			}
			if (selectedEmpDeathRegister.getStage().equals(ApprovalStage.FINAL_APPROVED)) {
				if (loginUserId != null && selectedEmpDeathRegister.getCreatedBy() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId().equals(loginUserId)) {
					deleteButtonFlag = false;
				}else {
					deleteButtonFlag = false;
				}
				addButtonFlag = false;
				editButtonFlag = false;
			} else if (selectedEmpDeathRegister.getStage().equalsIgnoreCase("INPROGRESS")) {
				if (loginUserId != null && selectedEmpDeathRegister.getCreatedBy() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId().equals(loginUserId)) {
					deleteButtonFlag = false;
				}else {
					deleteButtonFlag = false;
				}
				addButtonFlag = false;
		//		editButtonFlag = true;
			} else if (selectedEmpDeathRegister.getStage().equals(ApprovalStage.REJECTED)) {
				if (loginUserId != null && selectedEmpDeathRegister.getCreatedBy() !=null
						&& selectedEmpDeathRegister.getCreatedBy().getId() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId().equals(loginUserId)) {
					deleteButtonFlag = true;
				} else {
					deleteButtonFlag = false;
				}
				addButtonFlag = false;
			} else if (selectedEmpDeathRegister.getStage().equals(ApprovalStage.APPROVED)) {
				if (loginUserId != null && selectedEmpDeathRegister.getCreatedBy() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId() != null
						&& selectedEmpDeathRegister.getCreatedBy().getId().equals(loginUserId)) {
					deleteButtonFlag = false;
				}else {
					deleteButtonFlag = false;
				}
				editButtonFlag = false;
				addButtonFlag = false;
			} else {
				addButtonFlag = false;
				editButtonFlag = false;
				deleteButtonFlag = false;
			}

		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:onRowSelect()",e);
		}
	}
	
	
	public void loadEmployeeLoggedInUserDetails() {
		log.info("<<======= EmployeeDeathRegistrationBean:loadEmployeeLoggedInUserDetails() Starts =======>>");
//		BaseDTO baseDTO = null;
		try {
//			String url = SERVER_URL + "/employee/findempdetailsbyloggedinuser/"
//					+ loginBean.getUserMaster().getId();
//			baseDTO = httpService.get(url);
//			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
//				ObjectMapper mapper = new ObjectMapper();
//				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
//			} else {
//				log.info(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
//				return;
//			}
			
			employeeMaster = new EmployeeMaster();
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
		} catch (Exception e) {
			log.error("Exception Occured While EmployeeDeathRegistrationBean:loadEmployeeLoggedInUserDetails() :: ", e);
		}
		log.info("<<======= EmployeeDeathRegistrationBean:loadEmployeeLoggedInUserDetails() Ends =======>>");
	}
	
	public void loadValues() {
		log.info("<====== EmployeeDeathRegistrationBean:loadValues() Starts =====>");
		try {
			headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
			loadNatureOfDeathList();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.
									EMPLOYEE_DEATH_REGISTRATION.toString());
			loadEmployeeLoggedInUserDetails();
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:loadValues()",e);
		}
	}
	
	public void horoValueChange() {
		log.info("<====== EmployeeDeathRegistrationBean:horoValueChange() Starts =====>");
		EntityTypeMaster entityTypeMaster = null;
		String entityTypeCode = null;
		Long horoId = null;
		entityTypeMasterList = new ArrayList<>();
		if(action.equals("CREATE")) {
			deathRegistrationDTO.getEmpDeathRegister().setEntityTypeMaster(null);
		}
		try {
			horoId = horoEntity == null ? null : horoEntity.getId();
			if(horoId != null) {
				entityTypeMaster = commonDataService.getEntityType(horoId);
				if(entityTypeMaster != null) {
					entityTypeCode = entityTypeMaster.getEntityCode();
					if(StringUtils.isNotEmpty(entityTypeCode)) {
						if(entityTypeMaster.getEntityCode().equals(EntityType.HEAD_OFFICE)) {
							deathRegistrationDTO.setEntityTypeId(entityTypeMaster.getId());
							loadEmployeeByEntityId();
						}else if(!entityTypeMaster.getEntityCode().equals(EntityType.HEAD_OFFICE)) {
							entityTypeMasterList = commonDataService.loadEntityTypeBasedOnHoRo(horoId);
						}
					}else {
						log.info("EntityTypeMaster Code Not Found For EntityType Id - "+ entityTypeMaster.getId());
					}
				}else {
					log.info("Entity Type is Not Found For Entity Id - "+ horoEntity.getId());
				}
			}
			entityTypeMasterList = commonDataService.getActiveEntityType();
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:horoValueChange()",e);
		}
		log.info("<====== EmployeeDeathRegistrationBean:horoValueChange() Ends =====>");
	}
	
	public void entityTypeValueChange() {
		log.info("<====== EmployeeDeathRegistrationBean:entityTypeValueChange() Starts =====>");
		Long regionId = null;
		Long entityTypeId = null;
		entityMasterList = new ArrayList<>();
		try {
			regionId = horoEntity == null ? null : horoEntity.getId();
			entityTypeId = deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster() == null ? null : 
							deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster().getId();
			deathRegistrationDTO.setEntityTypeId(entityTypeId);
			if(regionId != null && entityTypeId != null) {
				entityMasterList = commonDataService.loadActiveEntityByregionOrentityTypeId(regionId,entityTypeId);
			}
			if(entityMasterList == null) {
				log.info("EntityMasterList Size is Zero For Entity Id - "+regionId+" And EntityType Id - "+entityTypeId);
			}
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:entityTypeValueChange()",e);
		}
		log.info("<====== EmployeeDeathRegistrationBean:entityTypeValueChange() Ends =====>");
	}
	
	public void loadEmployeeByEntityId() {
		log.info("<<======= EmployeeDeathRegistrationBean:loadEmployeeByEntityId() Starts =======>> ");
		employeeList = new ArrayList<>();
		EntityTypeMaster entityTypeMaster = null;
		String entityTypeCode  = null;
		Long entityId = null;
		try {
			Long horoId = horoEntity == null ? null : horoEntity.getId();
			if(horoId != null) {
				entityTypeMaster = commonDataService.getEntityType(horoId);
				if(entityTypeMaster != null) {
					entityTypeCode = entityTypeMaster.getEntityCode();
					if(StringUtils.isNotEmpty(entityTypeCode)) {
						if(entityTypeCode.equals(EntityType.HEAD_OFFICE)) {
							entityId = horoId;
						}else {
							entityId = deathRegistrationDTO.getEmpDeathRegister().getEntityMaster() == null ? null : 
								deathRegistrationDTO.getEmpDeathRegister().getEntityMaster().getId();
						}
					}
				}
			}
			if(entityId != null) {
				deathRegistrationDTO.setEntityId(entityId);
				loadEmployeeDetailsByEntityId();
			}
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:loadEmployeeByEntityId() ",e);
		}
	}
	
	public void loadEmployeeDetailsByEntityId() {
		log.info("<<======= EmployeeDeathRegistrationBean:loadEmployeeDetailsByEntityId() Starts =======>> ");
		Long entityId = null;
		try {
			BaseDTO baseDTO = null;
			entityId = deathRegistrationDTO.getEntityId() == null ? null : deathRegistrationDTO.getEntityId();
			if(entityId != null) {
				String url = SERVER_URL + "/employee/findempdetailsbyentityid/"
						+ entityId +"/"+action;
				baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeList= mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
				} else {
					log.info(" Employee Details Not Found for the Entity Id - " +entityId);
					return;
				}
			}
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:loadEmployeeDetailsByEntityId() ",e);
		}
	}
	
	
	public void loadNatureOfDeathList() {
		log.info("<<======= EmployeeDeathRegistrationBean:loadNatureOfDeathList() Starts =======>> ");
		try {
			String requestPath = SERVER_URL + "/natureofdeath/getAll";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					natureOfDeathList = mapper.readValue(jsonResponse, new TypeReference<List<NatureOfDeath>>() {
					});
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in EmployeeDeathRegistrationBean:loadNatureOfDeathList() ", exp);
		}
		log.info("<<======= EmployeeDeathRegistrationBean:loadNatureOfDeathList() Ends =======>> ");
	}
	
	public void fileUpload(FileUploadEvent event) {
		log.info("<<======= EmployeeDeathRegistrationBean:fileUpload() Starts =======>> ");
		EmployeePersonalInfoEmployment personalInfoEmployment = null;
		String pfNumber = null;
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadedFile = event.getFile();
			String type = FilenameUtils.getExtension(uploadedFile.getFileName());
			log.info(" File Type :: " + type);
			boolean validFileType = AppUtil.isValidFileType(type,
					new String[] { "png", "jpg", "JPG", "doc", "jpeg", "gif", "docx", "pdf", "xlsx", "xls","PNG" });
			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
				return;
			}
			long size = uploadedFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			long fileSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigEnum.DEATH_REGISTRATION_UPLOAD_FILE_SIZE.toString()));
			fileSize = fileSize / 1000;
			if (size > fileSize) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_RECOMMANDATION_LETTER_SIZE.getErrorCode());
				return;
			}
		//	String docPathName = commonDataService.getFilePath(AppConfigEnum.DEATH_REGISTRATION_UPLOAD_PATH.toString(), "FILE");
			String docPathName = commonDataService.getAppKeyValue(AppConfigEnum.DEATH_REGISTRATION_UPLOAD_PATH.toString());
			if(deathRegistrationDTO.getEmpDeathRegister().getEmpMaster() != null) {
				personalInfoEmployment = deathRegistrationDTO.getEmpDeathRegister().getEmpMaster().getPersonalInfoEmployment();
				if(personalInfoEmployment != null) {
					pfNumber = personalInfoEmployment.getPfNumber();
				}
			}
			StringBuilder sb = new StringBuilder(docPathName);
			docPathName = sb.append(pfNumber == null ? "" :"/"+pfNumber).toString();
			log.info("docPathName ------------" + docPathName);

			String filePath = Util.fileUpload(docPathName, uploadedFile);
			deathRegistrationDTO.getEmpDeathRegisterDocs().setDocName(uploadedFile.getFileName());
			deathRegistrationDTO.getEmpDeathRegisterDocs().setDocPath(filePath);
			errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
		}catch(Exception e) {
			log.info("Exception at EmployeeDeathRegistrationBean:fileUpload() ",e);
		}
		log.info("<<======= EmployeeDeathRegistrationBean:fileUpload() Ends =======>> ");
	}
	
	public String employeeDeathRegistrationListAction() {
		log.info("<====== EmployeeDeathRegistrationBean:employeeDeathRegistrationListAction() Starts =====>");
		try {
			if(action.equalsIgnoreCase("CREATE")) {
				log.info("<====== Employee Death Registration Create Page Calling =====>");
				deathRegistrationDTO = new DeathRegistrationDTO();
				horoEntity = new EntityMaster();
				deathRegistrationDTO.setEmpDeathRegister(new EmpDeathRegister());
				loadValues();
				employeeList = new ArrayList<>();
				return EMPLOYEE_DEATH_REGISTRATION_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception Occured in EmployeeDeathRegistrationBean:employeeVoluntaryRequestListAction", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EmployeeDeathRegistrationBean:employeeVoluntaryRequestListAction Ends====>");
		return null;
	}
	
	public String saveDeathRegistration() {
		log.info("<<======= EmployeeDeathRegistrationBean:saveDeathRegistration() Starts ======>> ");
		BaseDTO baseDTO = null;
		log.info("Note :" +deathRegistrationDTO.getNote());
		try {
			if(StringUtils.isEmpty(deathRegistrationDTO.getEmpDeathRegisterDocs().getDocName())) {
				errorMap.notify(ErrorDescription.CER_DOC_EMPTY.getErrorCode());
				return null;
			} 
			if(action.equalsIgnoreCase("EDIT")) {
				if (selectedEmpDeathRegister.getStage() != null) {
					if (selectedEmpDeathRegister.getStage().equalsIgnoreCase("INPROGRESS") || 
							selectedEmpDeathRegister.getStage().equalsIgnoreCase("REJECTED")) {
						if (deathRegistrationDTO.getUserMaster() == null
								|| deathRegistrationDTO.getUserMaster().getEmpName() == null
								|| StringUtils.isEmpty(deathRegistrationDTO.getUserMaster().getEmpName())) {
							AppUtil.addWarn("Forward To is empty");
							return null;
						}
						if (deathRegistrationDTO.getFinalApproval() == null) {
							AppUtil.addWarn("Forward For is empty");
							return null;
						}
						if(StringUtils.isEmpty(deathRegistrationDTO.getNote())) {
							AppUtil.addWarn("Note is empty");
							return null;
						}
					}
				}
			}
			if(StringUtils.isEmpty(deathRegistrationDTO.getNote())) {
				AppUtil.addWarn("Note is empty");
				return null;
			}
			String URL = SERVER_URL + "/empdeathregistration/save";
			baseDTO = httpService.post(URL, deathRegistrationDTO);
			if (baseDTO.getStatusCode() == 0) {
				if ("CREATE".equalsIgnoreCase(action)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_SAVE).getErrorCode());
				} else if ("EDIT".equalsIgnoreCase(action)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_UPDATE).getErrorCode());
				}
				return showDeathRegistrationListPage();
			}else {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception at saveDeathRegistration()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	private void loadDeathRegistrationLazyList() {
		log.info("<<============== EmployeeDeathRegistrationBean:loadDeathRegistrationLazyList Strats ===========>> ");
		employeeDeathRegisterLazyList = new LazyDataModel<EmpDeathRegister>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 2559910409973223615L;

			public List<EmpDeathRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<EmpDeathRegister> empDeathRegisterList = null;
				try {
					empDeathRegisterList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url = AppUtil.getPortalServerURL() + "/empdeathregistration/loadLazyList";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						empDeathRegisterList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpDeathRegister>>() {
								});
						log.info("EmpdeathRegister List Size =======>>" + empDeathRegisterList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in EmployeeDeathRegistrationBean : loadDeathRegistrationLazyList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("<<======== Ends lazy load ======>> ");
				return empDeathRegisterList;
			}

			@Override
			public Object getRowKey(EmpDeathRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpDeathRegister getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmpDeathRegister> responseList = (List<EmpDeathRegister>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmpDeathRegister empDeathRegisterObj : responseList) {
					if (empDeathRegisterObj.getId().longValue() == value.longValue()) {
						selectedEmpDeathRegister = empDeathRegisterObj;
						return empDeathRegisterObj;
					}
				}
				return null;
			}
		};
	}
	
	public String deathRegistrationViewPage() {
		log.info("<<========= EmployeeDeathRegistrationBean:deathRegistrationViewPage() =======>> ");
		BaseDTO baseDTO = new BaseDTO();
		enableFlag = false;
		String stage = null;
		try {
			if (selectedEmpDeathRegister == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String URL = SERVER_URL+"/empdeathregistration/getById/"+selectedEmpDeathRegister.getId()+"/"+ (selectedNotificationId!=null?selectedNotificationId:0);
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				deathRegistrationDTO = mapper.readValue(jsonResponse, DeathRegistrationDTO.class);

				deathRegisterLastNote = deathRegistrationDTO.getEmpDeathRegisterNote();

				if(deathRegisterLastNote != null) {
					deathRegistrationDTO.setNote(deathRegisterLastNote.getNote());
				}
				
				String jsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = mapper.readValue(jsonResponses,
						new TypeReference<List<DeathRegistrationDTO>>() {
				});

				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_DEATH_REGISTRATION.toString());
				if(deathRegisterLastNote.getForwardTo() != null && loginBean.getUserMaster() != null) {
					if (deathRegisterLastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
						enableFlag = true;
					}else {
						disableFlag = true;
					}
				}
				if(deathRegisterLastNote.getForwardTo() != null) {
					deathRegistrationDTO.setUserMaster(deathRegisterLastNote.getForwardTo());
				}
				
				if (deathRegistrationDTO.getEmpDeathRegister() != null ) {
					
					if(deathRegistrationDTO.getEmpDeathRegister().getHoroName()!=null) {
						selectedEmpDeathRegister.setHoroName(deathRegistrationDTO.getEmpDeathRegister().getHoroName());
					}
					if (deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster() != null) {
						selectedEmpDeathRegister
								.setEntityTypeMaster(deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster());
						if (selectedEmpDeathRegister.getEntityTypeMaster() != null
								&& selectedEmpDeathRegister.getEntityTypeMaster().getEntityName() != null) {
							selectedEmpDeathRegister.getEntityTypeMaster()
									.setEntityName(selectedEmpDeathRegister.getEntityTypeMaster().getEntityName());
						}
					}

					if (deathRegistrationDTO.getEmpDeathRegister().getEntityMaster() != null) {
						selectedEmpDeathRegister
								.setEntityMaster(deathRegistrationDTO.getEmpDeathRegister().getEntityMaster());
					}
					if (selectedEmpDeathRegister.getEntityMaster() != null
							&& selectedEmpDeathRegister.getEntityMaster().getName() != null) {
						selectedEmpDeathRegister.getEntityMaster()
								.setName(selectedEmpDeathRegister.getEntityMaster().getName());
					}

					if (deathRegistrationDTO.getEmpDeathRegister().getEmpMaster() != null) {
						selectedEmpDeathRegister
								.setEmpMaster(deathRegistrationDTO.getEmpDeathRegister().getEmpMaster());
						if (selectedEmpDeathRegister.getEmpMaster() != null
								&& selectedEmpDeathRegister.getEmpMaster().getFirstName() != null) {
							selectedEmpDeathRegister.getEmpMaster()
									.setFirstName(selectedEmpDeathRegister.getEmpMaster().getFirstName());
	//						deathRegistrationDTO.getEmpDeathRegister().setEmpMaster(selectedEmpDeathRegister.getEmpMaster());
	//						deathRegistrationDTO.getEmpDeathRegister().getEmpMaster().setFirstName(deathRegistrationDTO.getEmpDeathRegister().getEmpMaster().getFirstName());
							
						}

						if (selectedEmpDeathRegister.getEmpMaster() != null
								&& selectedEmpDeathRegister.getEmpMaster().getLastName() != null) {
							selectedEmpDeathRegister.getEmpMaster()
									.setLastName(selectedEmpDeathRegister.getEmpMaster().getLastName());
						} else {
							selectedEmpDeathRegister.getEmpMaster().setLastName("");
						}
					}

				}
				
				if(action.equals("VIEW")) {
					return EMPLOYEE_DEATH_REGISTRATION_VIEW_PAGE;
				}
				if(action.equals("EDIT")) {
					Long entityId = deathRegistrationDTO.getEmpDeathRegister() == null ? null : deathRegistrationDTO.getEmpDeathRegister().getEntityMaster() == null ?
							null : deathRegistrationDTO.getEmpDeathRegister().getEntityMaster().getId();
					if(entityId != null) {
						EntityMaster entityMasterRegion = commonDataService.findRegionByShowroomId(entityId);
						if(entityMasterRegion != null) {
							horoEntity = entityMasterRegion;
						}else {
							horoEntity = deathRegistrationDTO.getEmpDeathRegister() == null ? null : 
									deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster() == null ? null : 
									deathRegistrationDTO.getEmpDeathRegister().getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE) ?
									deathRegistrationDTO.getEmpDeathRegister().getEntityMaster() : null;
						}
						deathRegistrationDTO.setEntityId(horoEntity == null ? null : horoEntity.getId());
						loadValues();
						horoValueChange();
						loadEmployeeDetailsByEntityId();
						entityTypeValueChange();
						loadEmployeeByEntityId();
//						deathRegistrationDTO.setNote(deathRegistrationDTO.getNote());
						log.info("<---note---> " + deathRegistrationDTO.getNote());
						//add the employee master in list
						employeeList.add(deathRegistrationDTO.getEmpDeathRegister().getEmpMaster());
					}
					return EMPLOYEE_DEATH_REGISTRATION_CREATE_PAGE;
				}
			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			
			if(action.equals("EDIT")){
				if (selectedEmpDeathRegister != null) {
					stage = selectedEmpDeathRegister.getStage();
					if (!stage.equals(ApprovalStatus.REJECTED.toString())) {
						if(stage.equals(ApprovalStatus.INPROGRESS.toString())) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_SUBMITTED_RECORD).getErrorCode());
						}else if(stage.equals(ApprovalStage.APPROVED)) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_APPROVED_RECORD).getErrorCode());
						}else if(stage.equals(ApprovalStatus.FINAL_APPROVED.toString())) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_FINAL_APPROVED_RECORD).getErrorCode());
						}
						log.info("<========== Inside Cannot Delete Record =========>");
						return null;
					}
				}
			}
			
		}catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at EmployeeDeathRegistrationBean:deathRegistrationViewPage()", e);
		}
		return null;
	}
	
	public void statusUpdate(String value) {
		log.info("<--- EmployeeDeathRegistrationBean:statusUpdate() ---> " + value);
		status = value;
	}
	
	public String approveRejectEmpDeathRegister() {
		log.info("<<======= EmployeeDeathRegistrationBean:approveRejectEmpDeathRegister() Starts ==========>>");
		BaseDTO baseDTO = null;
		try {
			if (status.equals(ApprovalStage.APPROVED)) {
				if (deathRegistrationDTO.getUserMaster() == null
						|| deathRegistrationDTO.getUserMaster().getEmpName() == null 
						|| StringUtils.isEmpty(deathRegistrationDTO.getUserMaster().getEmpName())) {
					//errorMap.notify(ErrorDescription.FORWARD_TO_USER_EMPTY.getErrorCode());
					Util.addWarn("Forward To is empty");
					return null;
				}
				if (deathRegistrationDTO.getFinalApproval() == null) {
					errorMap.notify(ErrorDescription.FORWARD_FOR_ISEMPTY.getErrorCode());
					return null;
				}
				if (StringUtils.isEmpty(deathRegistrationDTO.getApproveComments())) {
					Util.addWarn("Remarks is empty");
					return null;
				}
			}
			if (status.equalsIgnoreCase("FINAL_APPROVED")) {
				if (StringUtils.isEmpty(deathRegistrationDTO.getFinalApproveComments())) {
					Util.addWarn("Remarks is empty");
					return null;
				}
			}
			if (status.equalsIgnoreCase("REJECTED")) {
				if (StringUtils.isEmpty(deathRegistrationDTO.getRejectComments())) {
					Util.addWarn("Remarks is empty");
					return null;
				}
			}
			
			deathRegistrationDTO.getEmpDeathRegister().setId(selectedEmpDeathRegister == null ?  null : selectedEmpDeathRegister.getId());
			deathRegistrationDTO.setStatus(status);
			if(StringUtils.isEmpty(deathRegistrationDTO.getNote())) {
				deathRegistrationDTO.setNote(deathRegisterLastNote.getNote());
			}
			String URL = SERVER_URL + "/empdeathregistration/approverejectdeathregister";
			baseDTO = httpService.post(URL, deathRegistrationDTO);
			if (baseDTO != null) {
				if(status.equals(ApprovalStage.APPROVED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_APPROVED).getErrorCode());
				}else if(status.equals(ApprovalStatus.FINAL_APPROVED.toString())) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_FINAL_APPROVED).getErrorCode());
				}else if(status.equals(ApprovalStage.REJECTED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_REJECTED).getErrorCode());
				}
				return showDeathRegistrationListPage();
			}
		}catch (Exception e) {
			log.error("<<====== EmployeeDeathRegistrationBean:approveRejectEmpDeathRegister() =======>>", e);
		}
		log.info("<<======= EmployeeDeathRegistrationBean:approveRejectEmpDeathRegister() Ends ==========>>");
		return null;
	}
	
	public String deleteDeathRegistration() {
		log.info("<<========= EmployeeDeathRegistrationBean:deleteDeathRegistration() =======>> ");
		String stage = null;
		try {
			if (selectedEmpDeathRegister == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedEmpDeathRegister != null) {
				stage = selectedEmpDeathRegister.getStage();
				if (!stage.equals(ApprovalStatus.REJECTED.toString())) {
					if(stage.equals(ApprovalStatus.INPROGRESS.toString())) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_SUBMITTED_RECORD).getErrorCode());
					}else if(stage.equals(ApprovalStage.APPROVED)) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_APPROVED_RECORD).getErrorCode());
					}else if(stage.equals(ApprovalStatus.FINAL_APPROVED.toString())) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_FINAL_APPROVED_RECORD).getErrorCode());
					}
					log.info("<========== Inside Cannot Delete Record =========>");
					return null;
				}
			}
			RequestContext.getCurrentInstance().execute("PF('confirmDelete').show();");
		}catch(Exception e) {
			log.error("Exception at EmployeeDeathRegistrationBean:deleteDeathRegistration()",e);
		}
		return null;
	}
	
	public String deleteConfirmDeathRegistration() {
		log.info("<<========= EmployeePfSelfContributionBean:deleteConfirmDeathRegistration() Starts =======>> ");
		String url;
		try {
			BaseDTO baseDTO = new BaseDTO();
			Long registerId = selectedEmpDeathRegister == null ? null : selectedEmpDeathRegister.getId();
			if(registerId != null) {
				url = SERVER_URL+"/empdeathregistration/deleteById/"+selectedEmpDeathRegister.getId();
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DEATH_REGISTRATION_DELETE).getErrorCode());
					RequestContext.getCurrentInstance().execute("PF('confirmDelete').hide();");
					addButtonFlag = true;
					return showDeathRegistrationListPage();
				} else {
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at EmployeePfSelfContributionBean:deleteConfirmDeathRegistration()", e);
		}
		log.info("<<========= EmployeePfSelfContributionBean:deleteConfirmDeathRegistration() Ends =======>> ");
		return null;
	}
	
	public void downloadFile() {
		log.info("<<========= EmployeePfSelfContributionBean:downloadFile() Starts =======>> ");
		InputStream input = null;
		try {
			File file = new File(deathRegistrationDTO.getEmpDeathRegisterDocs().getDocPath());
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException in EmployeePfSelfContributionBean:downloadFile()" + e);
		} catch (Exception e) {
			log.error("Exception at EmployeePfSelfContributionBean:downloadFile()" + e);
		}
		log.info("<<========= EmployeePfSelfContributionBean:downloadFile() Ends =======>> ");
	}
	
	@PostConstruct
	public String showViewListPage() {
		log.info("EmployeeDeathRegistration List Bean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String employeeDeathRegistrationId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			employeeDeathRegistrationId = httpRequest.getParameter("employeeDeathRegistrationId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(employeeDeathRegistrationId)) {
			selectedEmpDeathRegister = new EmpDeathRegister();
			selectedEmpDeathRegister.setId(Long.parseLong(employeeDeathRegistrationId));
		    
			selectedNotificationId = Long.parseLong(notificationId);
			action= "VIEW";
			deathRegistrationViewPage();
		}
		log.info("EmployeeDeathRegistration List Bean showViewListPage() Ends");
		return "/pages/personnelHR/viewEmployeeDeathRegistration.xhtml?faces-redirect=true";
	}
	
	public List<UserMaster> getForwardToList(String query) {
		log.info("<=employeeDeathRegistrationBean :: getForwardToList=>");
		List<UserMaster> userMasterList=new ArrayList<UserMaster>();
		try {
			String url = SERVER_URL + "/increment/forwardtousers/"+query;
			log.info("loadIncrementCycle :: url ==> "+url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					userMasterList = mapper.readValue(jsonValue, new TypeReference<List<UserMaster>>() {});
					log.info("employeeDeathRegistrationBean :: getForwardToList==> " + userMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		}catch (Exception e) {
			log.error("Exception in loadIncrementCycle  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return userMasterList;
	}
}