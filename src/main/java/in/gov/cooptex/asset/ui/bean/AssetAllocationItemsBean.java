package in.gov.cooptex.asset.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.enums.AllocationTypeEnum;
import in.gov.cooptex.asset.model.AssetAllocation;
import in.gov.cooptex.asset.model.AssetAllocationItems;
import in.gov.cooptex.asset.model.AssetAllocationLog;
import in.gov.cooptex.asset.model.AssetAllocationNote;
import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.asset.model.AssetRegister;
import in.gov.cooptex.asset.model.dto.AssetRegisterDTO;
import in.gov.cooptex.asset.model.dto.AssetRequestListDTO;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;

import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.GeneralErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.ChequeToBankResponseDTO;
import in.gov.cooptex.operation.production.dto.AssetAllocationDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("assetAllocationItemsBean")
public class AssetAllocationItemsBean extends CommonBean implements Serializable {

	@Getter
	@Setter
	Long Quantity;
	@Getter
	@Setter
	Long Reason;

	@Setter
	@Getter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	AssetAllocationLog assetAllocationLog;

	@Getter
	@Setter
	AssetAllocationNote assetAllocationNote;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Setter
	@Getter
	List<AllocationTypeEnum> allocationTypeEnumList;

	@Setter
	@Getter
	List<EmployeeMaster> employeeMasterList;
	@Setter
	@Getter
	EmployeeMaster employeeMaster;
	@Setter
	@Getter
	SectionMaster sectionMaster;

	@Setter
	@Getter
	List<SectionMaster> sectionMasterList;

	@Setter
	@Getter
	List<EntityTypeMaster> entityTypeList;
	@Autowired
	CommonDataService commonDataService;
	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	LazyDataModel<AssetAllocation> lazyAssetAllocationList;
	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	String action;

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper;

	String jsonResponse;
	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc;
	@Getter
	@Setter
	EntityMaster selectedEntityMaster;
	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;
	@Getter
	@Setter
	AssetRegisterDTO assetRegisterDTO = new AssetRegisterDTO();
	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	AssetAllocationItems assetAllocationItems, selectedAssetAllocationItems;

	@Getter
	@Setter
	AssetAllocation assetAllocation, selectedAssetAllocation;

	@Getter
	@Setter
	List<AssetAllocation> assetAllocationList = new ArrayList<AssetAllocation>();
	@Getter
	@Setter
	List<AssetAllocationDTO> assetAllocationDTOList = new ArrayList<AssetAllocationDTO>();

	@Getter
	@Setter
	AssetRegister assetRegister;
	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;
	@Getter
	@Setter
	List<EntityMaster> entityList;
	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;
	@Getter
	@Setter
	AssetAllocationDTO assetAllocationDTO = new AssetAllocationDTO();

	@Getter
	@Setter
	List<AssetAllocationItems> assetAllocationDetailsList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<AssetRegister> assetNameList = new ArrayList<AssetRegister>();
	// assetNameList
	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterList;

	@Getter
	@Setter
	AssetCategoryMaster assetCategoryMaster;

	@Getter
	@Setter
	AssetCategoryMaster assetSubCategoryMaster;

	@Getter
	@Setter
	List<AssetCategoryMaster> assetSubCategoryMasterList;

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	private String buttonLable;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_PAGE = "/pages/assetManagement/listAssetAllocation.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE = "/pages/assetManagement/createAssetAllocation.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/assetManagement/viewAssetAllocation.xhtml?faces-redirect=true;";

	public void loadAssetDropdowns() {
		log.info("<--- Start AssetAllocationItemsBean.loadAssetDropdowns ---->");
		try {

			entityMaster = new EntityMaster();

			updateEntityList();

			// entityTypeList = commonDataService.getEntityTypeByStatus();

			String url = SERVER_URL + "/loanandadvance/loadEntityTypeMaster";
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			log.info("<=Starts headRegionOfficeList =>" + headRegionOfficeList.size());

			log.info("<---- End AssetAllocationItemsBean.loadAssetDropdowns ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
	}

	@Getter
	@Setter
	Boolean finalApproval;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalRenderFlag = true;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("asset_allocation_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// init();
			selectedAssetAllocation = new AssetAllocation();
			action = "View";
			// assetAllocation.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedAssetAllocation.getId());
			selectedAssetAllocation.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedAssetAllocation.getId());
			selectedNotificationId = Long.parseLong(notificationId);

			showAssetAllocationListPageAction();
		}
		log.info("ModerizationBean showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public String showAssetAllocationListPageAction() {
		log.info("<===== Starts showAssetAllocationListPageAction ===========>" + action);
		// buttonLable = null;
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedAssetAllocation == null || selectedAssetAllocation.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ASSET.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {

				log.info("create assetAllocationItems called..");
				assetRegisterDTO = new AssetRegisterDTO();
				entityMasterList = new ArrayList<>();
				assetAllocationItems = new AssetAllocationItems();
				assetAllocation = new AssetAllocation();
				assetCategoryMaster = new AssetCategoryMaster();
				assetSubCategoryMaster = new AssetCategoryMaster();
				assetRegister = new AssetRegister();
				entityTypeList = new ArrayList<>();
				entityTypeList = commonDataService.getEntityTypeByStatus();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				allocationTypeEnumList = Arrays.asList(AllocationTypeEnum.values());
				forwardToUsersList = new ArrayList<>();
				forwardToUsersList = commonDataService
						.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_ALLOCATION.name());

				loadAssetDropdowns();

				loadEmployeeMasterList();
				loadSectionMasterList();

				loadAssetCategoryMasterList();

				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();

				assetAllocationDTO.setAssetAllocationItemsList(new ArrayList<>());
				assetAllocationDTO.setAssetAllocationItems(new AssetAllocationItems());
				assetAllocationDTO.setAssetAllocation(new AssetAllocation());

				assetAllocationDTO.setAssetRegister(new AssetRegister());

				assetAllocationLog = new AssetAllocationLog();
				assetAllocationDTO.setAssetAllocationLog(new AssetAllocationLog());
				assetAllocationDTO.setAssetAllocationNote(new AssetAllocationNote());

				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				visibility = true;
				String stage = selectedAssetAllocation.getStage();
				if (StringUtils.isNotEmpty(stage) && !stage.equals(ApprovalStatus.REJECTED.toString())) {
					if (stage.equals(ApprovalStatus.INPROGRESS.toString())) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_SUBMITTED_RECORD)
								.getErrorCode());
					} else if (stage.equals(ApprovalStage.APPROVED)) {
						errorMap.notify(
								ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_APPROVED_RECORD).getErrorCode());
					} else if (stage.equals(ApprovalStatus.FINAL_APPROVED.toString())) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_FINAL_APPROVED_RECORD)
								.getErrorCode());
					}
					log.info("<========== Inside Cannot Delete Record =========>");
					// return null;
				}

				assetRegisterDTO = new AssetRegisterDTO();
				entityMaster = new EntityMaster();
				entityMasterList = new ArrayList<>();
				assetAllocationItems = new AssetAllocationItems();
				assetAllocation = new AssetAllocation();

				assetCategoryMaster = new AssetCategoryMaster();
				assetSubCategoryMaster = new AssetCategoryMaster();
				assetRegister = new AssetRegister();

				assetRegister.setEntityMaster(entityMaster);

				entityTypeList = commonDataService.getEntityTypeByStatus();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				allocationTypeEnumList = Arrays.asList(AllocationTypeEnum.values());
				forwardToUsersList = new ArrayList<>();
				forwardToUsersList = commonDataService
						.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_ALLOCATION.name());
				loadAssetDropdowns();
				loadEmployeeMasterList();
				loadSectionMasterList();
				loadAssetCategoryMasterList();
				log.info("edit assetAllocationItems called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/assetallocation/getbyAssetAllocationID/" + selectedAssetAllocation.getId() + "/"
						+ (selectedNotificationId != null ? selectedNotificationId : 0);
				log.info("URL-->" + url);
				BaseDTO response = httpService.get(url);
				assetAllocationDTO = new AssetAllocationDTO();

				assetAllocationDTO.setAssetAllocationItemsList(new ArrayList<>());
				assetAllocationDTO.setAssetAllocation(new AssetAllocation());
				assetAllocationDTO.setAssetAllocationLog(new AssetAllocationLog());
				assetAllocationDTO.setAssetAllocationNote(new AssetAllocationNote());
				assetAllocationDTO.setAssetRegister(new AssetRegister());

				selectedHeadRegionOfc = new EntityMaster();

				selectedEntityTypeMaster = new EntityTypeMaster();

				log.info("Region Head Office============>" + assetRegisterDTO.getRegionHeadOffice());
				assetRegisterDTO.setEntityType(assetRegister.getEntityMaster().getEntityTypeMaster());
				log.info("Entity Type============>" + assetRegisterDTO.getEntityType());
				selectedHeadRegionOfc = new EntityMaster();

				selectedEntityTypeMaster = new EntityTypeMaster();
				entityMaster = assetRegister.getEntityMaster();
				log.info("entityMaster  inside  assetRegister.getEntityMaster() " + entityMaster);

				assetAllocationDTO.setAssetRegister(assetRegister);

				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					assetAllocationDTO = mapper.readValue(jsonResponse, new TypeReference<AssetAllocationDTO>() {
					});
					if (assetAllocationDTO.getAssetAllocation().getEntityMaster() != null) {
						EntityTypeMaster entityTypeMaster = assetAllocationDTO.getAssetAllocation().getEntityMaster()
								.getEntityTypeMaster();
						log.info("entityTypeMaster id================>" + entityTypeMaster.getId());
						setEntityTypeMaster(entityTypeMaster);
						if (assetAllocationDTO.getAssetAllocation().getEntityMaster().getName()
								.equalsIgnoreCase("Head Office Entity")) {
							log.info("inside if");
							assetRegisterDTO
									.setRegionHeadOffice(assetAllocationDTO.getAssetAllocation().getEntityMaster());
						} else {
							assetRegisterDTO
									.setRegionHeadOffice(assetAllocationDTO.getAssetRegister().getEntityMaster());
						}
						// assetRegisterDTO.setRegionHeadOffice(assetAllocationDTO.getAssetRegister().getEntityMaster());

						onEntityChg();
					}

					log.info("AssetAllocationItemsList-------Size----------->"
							+ assetAllocationDTO.getAssetAllocationItemsList().size());
					log.info("AssetAllocationLOG-------stage----------->"
							+ assetAllocationDTO.getAssetAllocationLog().getStage());
					log.info("AssetAllocationNote-------forwardTo Name----------->"
							+ assetAllocationDTO.getAssetAllocationNote().getForwardTo().getUsername());
					assetAllocationDTO.setAssetAllocationItems(new AssetAllocationItems());
					// assetAllocationDTO.getAssetAllocation().setAllocationDate(new Date());
					visibility = true;
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}

			} else if (action.equalsIgnoreCase("View")) {

//				if(assetAllocationDTO.getAssetAllocationNote().getFinalApproval().equals(false)) {
//					buttonLable = "Approve";
//					finalapprovalRenderFlag = false;
//				}else if(assetAllocationDTO.getAssetAllocationNote().getFinalApproval().equals(true)) {
//					buttonLable = "Final Approve";
//					finalapprovalRenderFlag = true;
//				}

				log.info("View assetAllocationItems called..");
				// forwardToUsersList
				// =commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_ALLOCATION.name());
				assetAllocationDTO.setAssetAllocationItemsList(new ArrayList<>());
				assetAllocationDTO.setAssetAllocation(new AssetAllocation());
				assetAllocationDTO.setAssetAllocationLog(new AssetAllocationLog());
				assetAllocationDTO.setAssetAllocationNote(new AssetAllocationNote());

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/assetallocation/getbyAssetAllocationID/" + selectedAssetAllocation.getId() + "/"
						+ (selectedNotificationId != null ? selectedNotificationId : 0);
				log.info("URL-->" + url);
				BaseDTO response = httpService.get(url);

				log.info("responseeee" + response);
				if (response != null && response.getStatusCode() == 0) {

					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					assetAllocationDTO = mapper.readValue(jsonResponse, new TypeReference<AssetAllocationDTO>() {
					});

					// ObjectMapper mapper = new ObjectMapper();
					// String jsonResponse =
					// mapper.writeValueAsString(response.getResponseContent());
					// assetAllocationDTO = mapper.readValue(jsonResponse, new
					// TypeReference<AssetAllocationDTO>() {
					// });

					// if(assetAllocationDTO.getAssetAllocationNote().getForwardTo() != null) {
					// assetAllocationDTO.getAssetAllocationNote().getForwardTo().setId(assetAllocationDTO.getAssetAllocationNote().getId());
					// }

					if (assetAllocationDTO.getAssetAllocation() != null
							&& assetAllocationDTO.getAssetAllocation().getEntityMaster() != null) {
						EntityTypeMaster entityTypeMaster = assetAllocationDTO.getAssetAllocation().getEntityMaster()
								.getEntityTypeMaster();
						log.info("entityTypeMaster id================>" + entityTypeMaster.getId());
						setEntityTypeMaster(entityTypeMaster);

						assetRegisterDTO.setRegionHeadOffice(assetAllocationDTO.getAssetRegister().getEntityMaster());
						onEntityChg();
					}

					if (assetAllocationDTO.getAssetAllocationNote() != null) {
						if (assetAllocationDTO.getAssetAllocationNote().getForwardTo().getId()
								.equals(loginBean.getUserDetailSession().getId())) {
							log.info("============approval flag===============");
							setApprovalFlag(true);
							setFinalapprovalFlag(false);

						}

						else {
							setFinalapprovalFlag(true);
							setApprovalFlag(false);

						}

						previousApproval = assetAllocationDTO.getAssetAllocationNote().getFinalApproval();
						if (assetAllocationDTO.getAssetAllocationNote().getFinalApproval().equals(true)) {
							finalapprovalRenderFlag = false;
							setFinalapprovalFlag(true);
						}

						else {
							finalapprovalRenderFlag = true;

						}
					} else {
						setFinalapprovalFlag(true);
						setApprovalFlag(false);
					}
					if (!assetAllocationDTO.getAssetAllocationNote().getForwardTo().getId()
							.equals(loginBean.getUserDetailSession().getId())) {
						log.info("============approval flag===============");
						finalapprovalRenderFlag = false;

					}
					showassetAllocationListPage();
					return VIEW_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete assetAllocationItems called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAssetAllocationItemsDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showAssetAllocationListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends AssetAllocationBean.showAssetAllocationListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts AssetAllocationItemsBean.deleteConfirm ===========>" + selectedAssetAllocation.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/assetallocation/deletebyid/" + selectedAssetAllocation.getId());

			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ASSETALLOCATION_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAssetAllocationItemsDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete AssetAllocationItems....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends AssetAllocationItemsBean.deleteConfirm ===========>");
		return showassetAllocationListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("select event----");
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		selectedAssetAllocation = (AssetAllocation) event.getObject();

		log.info("stagessss" + selectedAssetAllocation.getStage());

		if (selectedAssetAllocation.getStage().equalsIgnoreCase("FINAL APPROVED")
				|| selectedAssetAllocation.getStage().equalsIgnoreCase("APPROVED")) {
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = true;

		}

		if (selectedAssetAllocation.getStage().equalsIgnoreCase("SUBMITTED")
				&& !loginBean.getUserMaster().getUsername().equals("hosuper")) {
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = true;

		}

		if (selectedAssetAllocation.getStage().equalsIgnoreCase("REJECTED")) {
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = true;

		}
		log.info("selectedAssetAllocation-----" + selectedAssetAllocation.getId());
	}

	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		selectedAssetAllocation = null;
		loadLazyAssetAllocationList();
		return LIST_PAGE;
	}

	public void updateEntityList() {
		log.info("<=Starts loadAssetDropdowns.updateEntityList =>");
		try {
			assetRegisterDTO.setEmpId(null);
			if (assetRegisterDTO.getRegionHeadOffice() != null
					&& assetRegisterDTO.getRegionHeadOffice().getEntityTypeMaster() != null
					&& assetRegisterDTO.getRegionHeadOffice().getEntityTypeMaster().getEntityCode()
							.equalsIgnoreCase("HEAD_OFFICE")) {
				assetRegisterDTO.setEntityId(assetRegisterDTO.getRegionHeadOffice().getId());
				visibility = true;
				assetRegisterDTO.setEntityType(null);
			} else {
				visibility = false;
			}
			if (assetRegisterDTO.getRegionHeadOffice() != null && assetRegisterDTO.getRegionHeadOffice().getId() != null
					&& assetRegisterDTO.getEntityType() != null && assetRegisterDTO.getEntityType().getId() != null) {
				Long regionId = assetRegisterDTO.getRegionHeadOffice().getId();
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId,
						assetRegisterDTO.getEntityType().getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends loadAssetDropdowns.updateEntityList =>");
	}

	public void onEntityChg() {
		selectedHeadRegionOfc = new EntityMaster();
		selectedEntityTypeMaster = new EntityTypeMaster();

		entityMasterList = commonDataService.loadEntityByregionOrentityTypeId(
				assetRegisterDTO.getRegionHeadOffice().getId(), entityTypeMaster.getId());
		// log.info("<====== AssetAllocationItemsBean.onEntityTypeSelection Ends====> "
		// + entityMasterList.size());
	}

	public void onEnumListChange() {
		assetRegisterDTO.setRegionHeadOffice(null);
		setEntityTypeMaster(null);
		assetAllocationDTO.getAssetAllocation().setEntityMaster(null);
		assetAllocationDTO.getAssetAllocation().setSectionMaster(null);
		assetAllocationDTO.getAssetAllocation().setEmpMaster(null);
		assetAllocationDTO.getAssetAllocation().setAllocationDate(null);

		log.info("<====== onEnumListChange");
	}

	public void loadSectionMasterList() {

		log.info("loadSectionMasterList start===>");
		try {
			sectionMasterList = new ArrayList<>();
			sectionMasterList = commonDataService.getSectionMaster();
			log.info("loadSectionMasterList console check ===>" + sectionMasterList.size());

		} catch (Exception e) {
			log.error("loadSectionMasterList error==>", e);
		}

	}

	public void loadEmployeeMasterList() {

		log.info("loadEmployeeMasterList start===>");
		try {
			employeeMasterList = new ArrayList<>();
			employeeMasterList = commonDataService.getAllEmployee();

			log.info("loadEmployeeMasterList console check ===>" + employeeMasterList.size());

		} catch (Exception e) {
			log.error("loadEmployeeMasterList error==>", e);
		}

	}

	private void loadLazyAssetAllocationList() {
		log.info("<===== Starts AssetAllocationBean.loadLazyAssetAllocationItemsList ======>");
		lazyAssetAllocationList = new LazyDataModel<AssetAllocation>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<AssetAllocation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/assetallocation/getassetallocationlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						assetAllocationList = mapper.readValue(jsonResponse,
								new TypeReference<List<AssetAllocation>>() {

								});

						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyAssetAllocationItemsList ...", e);
					log.info("====asetallocationitemslazyload  list size======" + assetAllocationList.size());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return assetAllocationList;
			}

			@Override
			public Object getRowKey(AssetAllocation res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AssetAllocation getRowData(String rowKey) {
				try {
					for (AssetAllocation assetAllocation : assetAllocationList) {
						if (assetAllocation.getId().equals(Long.valueOf(rowKey))) {
							selectedAssetAllocation = assetAllocation;
							return assetAllocation;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};

		log.info("<===== Ends AssetAllocationItemsBean.loadLazyAssetAllocationList ======>");
	}

	public void loadEntityTypeMaster() {

		entityTypeMasterList = new ArrayList<EntityTypeMaster>();
		entityMasterList = new ArrayList<EntityMaster>();

		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getActiveEntityTypeMasterList";
			baseDTO = httpService.get(url);
			log.info("::: get loadEntityTypeMaster MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});
			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	public void loadAssetCategoryMasterList() {
		log.info("loadAssetCategoryMasterList start===>");
		try {
			assetCategoryMasterList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/assetcategorymaster/getActiveAssetCategoryMaster";
			log.info("loadAssetCategoryMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});

			}

			log.info("loadAssetCategoryMasterList end===>");

		} catch (Exception e) {
			log.error("loadAssetCategoryMasterList error==>", e);
		}

	}

	public void loadAssetSubCategoryMasterList() {
		log.info("loadAssetSubCategoryMasterList start===>" + assetCategoryMaster.getId());
		try {
			assetSubCategoryMasterList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/assetcategorymaster/getActiveAssetSubCategoryMaster/"
					+ assetCategoryMaster.getId();

			log.info("loadAssetSubCategoryMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetSubCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});

				log.info("assetSubCategoryMasterList console check ===>" + assetSubCategoryMasterList.size());
			}

			log.info("loadAssetSubCategoryMasterList end===>");

		} catch (Exception e) {
			log.error("loadAssetSubCategoryMasterList error==>", e);
		}

	}

	public void loadAssetNamesByAssetSubcategoryList() {

		try {
			assetNameList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			if (assetSubCategoryMaster.getId() != null) {
				String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/assetregister/getActiveAssetNamesBySubCategory/" + assetSubCategoryMaster.getId();

				log.info("loadAssetNamesByAssetSubcategoryList url==>" + url);
				relationShipBaseDTO = httpService.get(url);
			}
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetNameList = mapper.readValue(jsonResponse, new TypeReference<List<AssetRegister>>() {
				});

			}
			log.info("loadAssetNamesByAssetSubcategoryList console check ===>" + assetNameList.size());
			log.info("loadAssetNamesByAssetSubcategoryList end===>");

		} catch (Exception e) {
			log.error("loadAssetNamesByAssetSubcategoryList error==>", e);
		}

	}

	public void loadEntityMasterList() {

		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;

		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getSelectedEntityList";
			baseDTO = httpService.post(url, entityTypeMaster);
			log.info("::: get loadEntityTypeMaster MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});

			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	public String addAssetAllocationList() {
		assetAllocationItems = new AssetAllocationItems();
		assetAllocation = new AssetAllocation();
		Double allocationQty = (Double) 0.00;
		Double availableQty = (Double) 0.00;
		Double assetAllocatedQty = (Double) 0.00;
		// assetCategoryMaster = new AssetCategoryMaster();
		// assetSubCategoryMaster = new AssetCategoryMaster();
		log.info("AssetAllocationList Add Received ");
		try {

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetallocation/getusedqtyofasset/"
					+ assetRegisterId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("valueeee" + baseDTO.getResponseContent());

					assetAllocatedQty = mapper.readValue(jsonValue, new TypeReference<Double>() {
					});

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}

			if (assetAllocationDTO.getAssetAllocationItems().getQuantity() == 0) {
				AppUtil.addWarn("Quantity Should Not be Zero");

				return null;

			}

			Double qty = assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getQuantity();

			if (assetAllocatedQty + assetAllocationDTO.getAssetAllocationItems().getQuantity() > qty) {
				AppUtil.addWarn("Allocation Already Completed For this Asset");
				return null;
			}

			assetAllocationDetailsList = assetAllocationDTO.getAssetAllocationItemsList();

			allocationQty = assetAllocationDetailsList.stream().filter(o -> o.getQuantity() != null)
					.collect(Collectors.summingDouble(AssetAllocationItems::getQuantity))
					+ assetAllocationDTO.getAssetAllocationItems().getQuantity();

			if (assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getQuantity() >= allocationQty) {
				assetAllocationDTO.getAssetAllocationItemsList().add(assetAllocationDTO.getAssetAllocationItems());
			} else {
				allocationQty = assetAllocationDetailsList.stream().filter(o -> o.getQuantity() != null)
						.collect(Collectors.summingDouble(AssetAllocationItems::getQuantity));
				availableQty = assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getQuantity()
						- allocationQty;
				AppUtil.addWarn("Available Quantity is : " + availableQty);
				return null;
			}

			assetAllocationDTO.setAssetAllocationItems(new AssetAllocationItems());
			assetCategoryMaster = new AssetCategoryMaster();
			assetSubCategoryMaster = new AssetCategoryMaster();

			log.info("AssetAllocationList Add Done");

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.ASSET_ALLOCATION_ITEM_ADD_FAILURE.getErrorCode());
			log.error("AssetAllocationList Add Error " + e);
		}
		return null;

	}

	public String saveOrUpdate() {
		log.info("----Start saveOrUpdate-----");
		try {

			if (assetAllocationDTO.getAssetAllocationItemsList().size() == 0) {
				errorMap.notify(ErrorDescription.ASSET_ALLOCATION_ITEM_ADD_FAILURE.getErrorCode());
				return null;
			}

			if (assetAllocationDTO.getAssetAllocationNote().getNote().equals("")
					|| assetAllocationDTO.getAssetAllocationNote().getNote().equals(null)) {
				errorMap.notify(ErrorDescription.ASSET_ALLOCATION_NOTE_EMPTY.getErrorCode());
				return null;
			}
			log.info("assetAllocationDTO.getAssetAllocation().getAllocationType() at this line is"
					+ assetAllocationDTO.getAssetAllocation().getAllocationType());
			if (assetAllocationDTO.getAssetAllocation().getAllocationType().equalsIgnoreCase("Entity")
					&& assetRegisterDTO.getRegionHeadOffice().getName().equalsIgnoreCase("Head Office Entity")) {
				assetAllocationDTO.getAssetAllocation().setEntityMaster(assetRegisterDTO.getRegionHeadOffice());
			}

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetallocation/saveorupdate";
			BaseDTO response = httpService.post(url, assetAllocationDTO);

			if (response != null && response.getStatusCode() == 0) {
				log.info("assetallocation saved successfully.........." + action);
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.ASSETALLOCATION_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ASSETALLOCATION_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends assetAllocation.saveOrUpdate =======>");
		return showassetAllocationListPage();
	}

	public String showassetAllocationListPage() {
		log.info("<==== Starts assetAllocation.showassetAllocationListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		assetAllocationItems = new AssetAllocationItems();
		selectedAssetAllocation = new AssetAllocation();
		loadLazyAssetAllocationList();
		log.info("<==== Ends assetAllocation.showassetAllocationListPage =====>");
		return LIST_PAGE;
	}

	@Getter
	@Setter
	String stage;

	@Getter
	@Setter
	Boolean previousApproval = false;

	Long assetRegisterId;

	@Getter
	@Setter
	Double usedquantity = 0D;

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		setStage(value);
	}

	public String statusUpdate() {
		log.info("inside statusUpdate AssetAllocationItemsBean------->" + selectedAssetAllocation.getId() + "====>"
				+ assetAllocationDTO.getAssetAllocationNote().getForwardTo().getId());
		try {

			Validate.objectNotNull(assetAllocationDTO.getAssetAllocationNote().getForwardTo(),
					ErrorDescription.ASSETALLOCATION_APPROVAL_FORWARD_TO_EMPTY);
			log.info("notttt" + assetAllocationDTO.getAssetAllocationNote().getNote());

			assetAllocationDTO.getAssetAllocationNote().setNote(assetAllocationDTO.getAssetAllocationNote().getNote());

			log.info("assetAllocationDTO AssetAllocationNote forwrd to value check------->"
					+ assetAllocationDTO.getAssetAllocationNote().getForwardTo());
			assetAllocationDTO.getAssetAllocationLog().setStage(getStage());
			if (assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("REJECTED")) {
				assetAllocationDTO.getAssetAllocationLog().setStage("REJECTED");
			} else {
				if (assetAllocationDTO.getAssetAllocationNote().getFinalApproval() == true && previousApproval == true
						&& assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("APPROVED")) {
					assetAllocationDTO.getAssetAllocationLog().setStage("FINAL APPROVED");
				} else if (assetAllocationDTO.getAssetAllocationNote().getFinalApproval() == true
						|| assetAllocationDTO.getAssetAllocationNote().getFinalApproval() == false
								&& previousApproval == false
								&& assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("APPROVED")) {
					assetAllocationDTO.getAssetAllocationLog().setStage("APPROVED");
				}
			}
			assetAllocationDTO.setFinalapprovalFlag(finalapprovalFlag);
			log.info("finalapprovestatusssss" + assetAllocationDTO.isFinalapprovalFlag());
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/assetallocation/assetallocationForwardStage";
			BaseDTO response = httpService.post(url, assetAllocationDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("FINAL APPROVED")
						|| assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.ASSETALLOCATION_APPROVAL_APPROVED_SUCCESSFULLY.getCode());
				} else if (assetAllocationDTO.getAssetAllocationLog().getStage().equalsIgnoreCase("REJECTED")) {
					errorMap.notify(ErrorDescription.ASSETALLOCATION_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}

		return showassetAllocationListPage();
	}

	public String addassetlist() {

		try {
			String url = SERVER_URL + "/assetrequest/getusedqtyofasset/" + assetRegisterId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("valueeee" + baseDTO.getResponseContent());

					usedquantity = mapper.readValue(jsonValue, new TypeReference<Double>() {
					});

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in getusedqtyofasset() --->", exp);
		}

		log.info("appp" + usedquantity);
		return CREATE_PAGE;

	}

	public String quantityCheck() {

		assetRegisterId = assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getId();

		addassetlist();

		if (assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getQuantity() != null) {
			if (assetAllocationDTO.getAssetAllocationItems().getQuantity() - usedquantity > assetAllocationDTO
					.getAssetAllocationItems().getAssetRegister().getQuantity()) {
				Double availableQty = assetAllocationDTO.getAssetAllocationItems().getAssetRegister().getQuantity()
						- usedquantity;
				assetAllocationDTO.getAssetAllocationItems().setQuantity(0l);
				AppUtil.addWarn("Please Enter the Quantity Based On Available :" + availableQty);
			}
		}

		return null;
	}

}
