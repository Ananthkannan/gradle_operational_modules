package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.TaxMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import in.gov.cooptex.operation.dto.ProductVarietyTaxDto;
import in.gov.cooptex.operation.exceptions.UIException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("productVarietyTaxBean")
@Scope("session")
public class ProductVarietyTaxBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	ProductVarietyTax productVarietyTax;

	@Getter
	@Setter
	ProductVarietyTaxDto selectedProductVarietyTax;

	boolean forceLogin = false;

	@Getter
	@Setter
	TaxMaster taxMaster;

	@Getter
	@Setter
	ProductVarietyTaxDto productVarietyTaxDto;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Double minimunTotal;

	@Getter
	@Setter
	Double maximumTotal;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableDatatable = true;

	@Setter
	@Getter
	LazyDataModel<ProductVarietyTaxDto> productVarietyTaxList;

	@Autowired
	CommonDataService commonDataService;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	boolean saveButton = true;

	@Getter
	@Setter
	boolean updateButton = false;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<ProductCategory> listOFProductCategorylist = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	List<ProductGroupMaster> listOfProductGroupMasterByProductCategoryId = new ArrayList<ProductGroupMaster>();

	@Getter
	@Setter
	List<ProductVarietyMaster> listOfProductVarietyMasterByProductGroupMasterId = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<TaxMaster> listOfTaxMater = new ArrayList<TaxMaster>();

	@Getter
	@Setter
	List<ProductVarietyTaxDto> listOfProductVarietyTax = new ArrayList<ProductVarietyTaxDto>();

	@Getter
	@Setter
	List<ProductVarietyTaxDto> listOfProductVarietyTaxDto = new ArrayList<ProductVarietyTaxDto>();

	@Getter
	@Setter
	String appKay;

	private String serverURL = null;

	final String ECOMM_PRODUCT_CONFIG_URL = AppUtil.getPortalServerURL() + "/ecommProductConfig";

	private final String CREATE_ProDuctVariety_PAGE = "/pages/masters/productVarietyTax/createProductVarietyTax.xhtml?faces-redirect=true;";

	private final String View_ProDuctVariety_PAGE = "/pages/masters/productVarietyTax/viewProductVarietyTax.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_LIST_URL = "/pages/masters/productVarietyTax/listProductVarietyTax.xhtml?faces-redirect=true;";

	public ProductVarietyTaxBean() {
		log.info("Inside ProductVarietyTaxBean()");
	}

	@PostConstruct
	public void init() {
		log.info("the post construct method of ProductVarietyTaxBean is get Started");
		log.info("Inside init()");
		productVarietyTaxDto = new ProductVarietyTaxDto();
		loadValues();
		loadValuesForCreate();
		loadLazyProductVarietyList();

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			productVarietyTax = new ProductVarietyTax();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	private void getAllTaxMasterAsList() {

		try {
			BaseDTO baseDTO = new BaseDTO();
			log.info("taxMasterUrl" + appPreference.getOperationApiUrl());
			String url = serverURL + appPreference.getOperationApiUrl() + "/productVarietyTaxControler/getall";
			baseDTO = httpService.get(url);
			taxMaster = new TaxMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfTaxMater = mapper.readValue(jsonResponse, new TypeReference<List<TaxMaster>>() {
				});
			}

			log.info("list of tax master returened from database" + listOfTaxMater);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

	}

	private void loadValuesForCreate() {
		log.info("Inside loadValuesForCreate()");
		productVarietyTax = new ProductVarietyTax();
		listOFProductCategorylist = commonDataService.loadProductCategories();
		getAllTaxMasterAsList();

	}

	public void onProductCategoryChange() {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		if (productVarietyTaxDto.getProductCategory() != null) {
			productVarietyTaxDto.getSelectedCategoryList().add(productVarietyTaxDto.getProductCategory());
			Long id = productVarietyTaxDto.getProductCategory().getId();
			log.info("the selected the product category Id is" + id);
			productGroupList(id);
		}
	}

	public void productGroupList(Long categoryId) {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductGroup/" + categoryId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfProductGroupMasterByProductCategoryId = mapper.readValue(jsonValue,
						new TypeReference<List<ProductGroupMaster>>() {
						});
				log.info(" listOfProductGroupMasterByProductCategoryId is returned from data base is  "
						+ listOfProductGroupMasterByProductCategoryId);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getGropByCategory() --->", exp);
		}
	}

	public void onProductGroupChange() {
		log.info("EcommProductConfigBean onProductGroupChange------#Start");
		if (productVarietyTaxDto.getProductGroupMaster() != null
				&& productVarietyTaxDto.getProductGroupMaster().getId() != null) {
			productVarietyTaxDto.getSelectedGroupList().add(productVarietyTaxDto.getProductGroupMaster());
			productVarietyTaxDto.getSelectedProductVarietyList().add(productVarietyTaxDto.getProductVarietyMaster());
			Long id = productVarietyTaxDto.getProductGroupMaster().getId();
			log.info("the selected the product group  Id is" + id);
			productVarietyList(id);
		}
	}

	public void productVarietyList(Long groupId) {
		log.info("---getProductVarietyList-----");

		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductVariety/" + groupId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfProductVarietyMasterByProductGroupMasterId = mapper.readValue(jsonValue,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				log.info("listOfProductVarietyMasterByProductGroupMasterId is returned from database"
						+ listOfProductVarietyMasterByProductGroupMasterId);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getCategoryVarity() --->", exp);
		}
	}

	public void addItem() {
		log.info("Inside addItem()");

		try {

			validateInput();

			try {
				log.info("checking if duplicate is there for  productVarietyCode and Tax master Code");

				BaseDTO baseDTO = null;
				String url = serverURL + appPreference.getOperationApiUrl()
						+ "/productVarietyTaxControler/checkTheCombinationOfProductVarietyCodeAndTaxMasterCode";
				baseDTO = httpService.post(url, productVarietyTaxDto);
				log.info(" baseDTo return From Data Base" + baseDTO.toString());
				if (baseDTO.getTotalRecords() > 1) {
					log.info("the totalRecords is " + baseDTO.getTotalRecords());
					log.info("the combination is Already Exist");
					throw new UIException("This combination Already Exists");
				} else {
					ProductVarietyTaxDto item = new ProductVarietyTaxDto();
					String urll = serverURL + appPreference.getOperationApiUrl()
							+ "/productVarietyTaxControler/getproductVarietys";
					log.info("url" + urll);
					baseDTO = httpService.post(urll, productVarietyTaxDto);
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						if (baseDTO.getResponseContents() != null) {
							String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
							List<ProductVarietyTaxDto> productvatList = mapper.readValue(jsonValue,
									new TypeReference<List<ProductVarietyTaxDto>>() {
									});
							log.info("Total no. of ProductVarietys : " + productvatList.size());
							for (ProductVarietyTaxDto mas : productvatList) {
								item = new ProductVarietyTaxDto();
								item.setProductVarietyMaster(new ProductVarietyMaster());
								item.setProductGroupMaster(new ProductGroupMaster());
								item.setProductCategory(new ProductCategory());
								item.getProductVarietyMaster().setId(mas.getVarietyId());
								item.getProductVarietyMaster().setCode(mas.getVarietyCode());
								item.getProductVarietyMaster().setName(mas.getVarietyName());
								item.getProductGroupMaster().setId(mas.getGroupId());
								item.getProductGroupMaster().setCode(mas.getGroupCode());
								item.getProductGroupMaster().setName(mas.getGroupName());
								item.getProductCategory().setId(mas.getCategoryId());
								item.getProductCategory().setProductCatCode(mas.getCategoryCode());
								item.getProductCategory().setProductCatName(mas.getCategoryName());
								item.setTaxMaster(productVarietyTaxDto.getTaxMaster());
								item.setMinValue(productVarietyTaxDto.getMinValue());
								item.setMaxValue(productVarietyTaxDto.getMaxValue());
								item.setTaxPercent(productVarietyTaxDto.getTaxPercent());
								Boolean checkDuplicateRecord = false;
								if (listOfProductVarietyTaxDto.size() > 0) {
									for (ProductVarietyTaxDto pvt : listOfProductVarietyTaxDto) {
										if (item.equals(pvt)) {
											checkDuplicateRecord = true;
											AppUtil.addError("Duplicate Value Not Allowed");	

										} 
									}
									
								}
								if (checkDuplicateRecord == false) {
									listOfProductVarietyTaxDto.add(item);
								}
							}

							minimunTotal = listOfProductVarietyTaxDto.stream().filter(n -> n.getMinValue() != null)
									.mapToDouble(n -> n.getMinValue()).sum();
							maximumTotal = listOfProductVarietyTaxDto.stream().filter(n -> n.getMaxValue() != null)
									.mapToDouble(n -> n.getMaxValue()).sum();

						} else {
							String msg = baseDTO.getErrorDescription();
							log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						}
					}

					// log.info("the ProductCategory is "+productVarietyTaxDto.get);
					// log.info("the ProductGroupMaster is
					// "+productVarietyTaxDto.getProductGroupMaster());
					// log.info("the ProductVarityMaster is
					// "+productVarietyTaxDto.getProductVarietyMaster());
					// log.info("the TaxMaster is "+productVarietyTaxDto.getTaxMaster());
					// log.info("the MinValue is "+productVarietyTaxDto.getMinValue());
					// log.info("the MaxValue is "+productVarietyTaxDto.getMaxValue());
					// log.info("the TaxPercent is "+productVarietyTaxDto.getTaxPercent());
					// ProductVarietyTaxDto item = new ProductVarietyTaxDto();
					// item.setProductCategory(productVarietyTaxDto.getProductCategory());
					// item.setProductGroupMaster(productVarietyTaxDto.getProductGroupMaster());
					// item.setProductVarietyMaster(productVarietyTaxDto.getProductVarietyMaster());
					// item.setTaxMaster(productVarietyTaxDto.getTaxMaster());
					// item.setMinValue(productVarietyTaxDto.getMinValue());
					// item.setMaxValue(productVarietyTaxDto.getMaxValue());
					// item.setTaxPercent(productVarietyTaxDto.getTaxPercent());
					// listOfProductVarietyTaxDto.add(item);

				}
				productVarietyTaxDto = new ProductVarietyTaxDto();
			} catch (Exception e) {
				log.info(e.getMessage());
				log.info(e.getMessage().contains("The combination is Already Exist"));
				if (e.getMessage().contains("The combination is Already Exist")) {
					showClientError(e.getMessage());
				}
				// e.getMessage().contains(s)

				log.error("Error while saving  Product Variety List" + e);
			}

		} catch (UIException ex) {
			log.info("the combination is exist ");
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
		}
	}

	private void validateInput() throws UIException {
		if (productVarietyTaxDto == null) {
			throw new UIException("Invalid input");
		} else if (productVarietyTaxDto.getSelectedCategoryList() == null) {
			throw new UIException("Please select Product Category Code/Name");
		} else if (productVarietyTaxDto.getSelectedGroupList() == null) {
			throw new UIException("Please select Product Group Code/Name");
		} else if (productVarietyTaxDto.getSelectedProductVarietyList() == null) {
			throw new UIException("Please select Product Variety Code/Name");
		} else if (productVarietyTaxDto.getTaxMaster() == null) {
			throw new UIException("Please Select Tax Master Code/Name");
		} else if (productVarietyTaxDto.getMinValue() == null) {
			throw new UIException("Please Enter the Minimun Value");
		} else if (productVarietyTaxDto.getMaxValue() == null) {
			throw new UIException("Please Enter the Maximum Value");
		} else if (productVarietyTaxDto.getTaxPercent() == null) {
			throw new UIException("Please Enter Tax Percentage");
		}

	}

	private void showClientError(String errorMessage) {
		AppUtil.addError(errorMessage);
	}

	public String saveProductVarietyList() { 
		log.info("inside the Save Method");
 		log.info("the  listOfProductVarietyTaxDto" + listOfProductVarietyTaxDto.toString());
		try {
			if (listOfProductVarietyTaxDto.isEmpty()) {
				AppUtil.addWarn("ProductVareity List Table Is Empty");
				return null;
			} else {

				log.info("list is not empty");
				try {
					log.info("...... filemovementconfig submit begin ....");
					BaseDTO baseDTO = null;
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/productVarietyTaxControler/saveProductVarietyList";
					baseDTO = httpService.post(url, listOfProductVarietyTaxDto);
					log.info(" baseDTo return From Data Base" + baseDTO.toString());
					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {
							log.info("Product Variety List is saved successfully");
							// errorMap.notify(baseDTO.getStatusCode());
							AppUtil.addInfo("Product Variety Saved successfully");
							listOfProductVarietyTaxDto = new ArrayList<ProductVarietyTaxDto>();

						}
					}
				} catch (Exception e) {
					log.error("Error while saving  Product Variety List" + e);
				}

				log.info("...... Product Variety List submit ended ....");

			}
		} catch (Exception e1) {
			showClientError(e1.getMessage());
			e1.printStackTrace();
		}
		if (!(listOfProductVarietyTaxDto.isEmpty())) {
		}

		return INPUT_FORM_LIST_URL;

	}

	public void loadLazyProductVarietyList() {
		disableAddButton = false;
		log.info("<===== Starts productVarietyTax Bean.loadLazyProductVarietyList======>");
		productVarietyTaxList = new LazyDataModel<ProductVarietyTaxDto>() {

			private static final long serialVersionUID = -2007656114950592854L;

			@Override
			public List<ProductVarietyTaxDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/productVarietyTaxControler/getProductVarietyTaxListByLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get productVarietyTax list");
					log.info("productVarietyTax Total records" + response.getTotalRecords());

					log.info("the object returnend from productVarietyTaxControler" + response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						listOfProductVarietyTax = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarietyTaxDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e.getCause());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method" + listOfProductVarietyTax);
				log.info("Ends lazy load....");

				return listOfProductVarietyTax;
			}

			@Override
			public Object getRowKey(ProductVarietyTaxDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductVarietyTaxDto getRowData(String rowKey) {
				try {
					for (ProductVarietyTaxDto productVarietyTax : listOfProductVarietyTax) {
						if (productVarietyTax.getId().equals(Long.valueOf(rowKey))) {
							selectedProductVarietyTax = productVarietyTax;
							return productVarietyTax;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};

	}

	public String goToProductVarietyCreatePage() {

		updateButton = false;
		saveButton = true;
		minimunTotal = 0.0;
		maximumTotal = 0.0;
		listOfProductVarietyTaxDto = new ArrayList<>();
		loadValuesForCreate();
		productVarietyTaxDto = new ProductVarietyTaxDto();
		disableDatatable = true;
		return CREATE_ProDuctVariety_PAGE;
	}

	public String editProductVarietyTax() {
		disableDatatable = false;
		updateButton = true;
		saveButton = false;
		log.info("<<<<< -------Start editProductVariety called ------->>>>>> ");

		appKay = commonDataService.getAppKeyValue("PRODUCT_VARIETY_TAX_EDIT_ENABLED");
		if (appKay.equalsIgnoreCase("false")) {
			AppUtil.addWarn("Product variety tax edit option not enabled");

		} else {
			if (productVarietyTaxDto.getId() == null) {
				Util.addWarn("Please select one Product Variety Tax");
				return null;
			}

			log.info("the selected  ProductVarietyTax is" + productVarietyTaxDto);

			log.info("<<<<< -------End editEmployeeOt called ------->>>>>> ");
			disableAddButton = false;
			// onProductCategoryChange();
			// onProductGroupChange() ;
			listOfProductVarietyTaxDto.clear();
			minimunTotal = (double) 0;
			maximumTotal = (double) 0;
			productVarietyTaxDto.getSelectedCategoryList().add(productVarietyTaxDto.getProductCategory());
			loadAllGroupsByProductCategorys();
			productVarietyTaxDto.getSelectedGroupList().add(productVarietyTaxDto.getProductGroupMaster());
			loadAllProductVarietysByProductGroups();
			productVarietyTaxDto.getSelectedProductVarietyList().add(productVarietyTaxDto.getProductVarietyMaster());
			return CREATE_ProDuctVariety_PAGE;
		}
		selectedProductVarietyTax = new ProductVarietyTaxDto();

		return appKay;

	}

	public String gotoProductVarietyTaxViewPage() {
		log.info("<----Redirecting to user view page---->");
		if (productVarietyTaxDto.getId() == null) {
			Util.addWarn("Please select one Product Variety Tax");
			return null;
		}
		log.info("the selected  ProductVarietyTax is" + productVarietyTaxDto);
		disableAddButton = false;
		disableDatatable = false;
		return View_ProDuctVariety_PAGE;

	}

	public String updateProductVariety() {

		log.info("inside the Update Method");
		try {
			validateInput();
		} catch (UIException e) {

			showClientError(e.getMessage());
			return null;
		}

		try {
			log.info("...... productVareity update begin ....");
			BaseDTO baseDTO = null;

			productVarietyTaxDto.setAppKay("PRODUCT_VARIETY_TAX_EDIT_ENABLED");
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/upDateProductVareityTax";
			baseDTO = httpService.post(url, productVarietyTaxDto);
			log.info(" productVareity id" + productVarietyTaxDto.getId());
			log.info("return from productVareity  controler" + baseDTO.toString());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("productVareity updated successfully");
					AppUtil.addInfo("ProductVareity Updated Successfully");
					// errorMap.notify(baseDTO.getStatusCode());
				} else if (baseDTO.getStatusCode() == 50141 || baseDTO.getStatusCode() == 50142
						|| baseDTO.getStatusCode() == 50143) {
					log.error(" FuelCoupon exception occured while saving or updating .... " + baseDTO.getStatusCode());
					// errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addInfo("ProductVareity Updated Failed");

				}
			}
		} catch (Exception e) {
			// log.error("Error while creating CountryMaster" + e);
		}
		log.info("...... productVareity submit ended ....");
		clear();
		updateButton = false;
		saveButton = true;
		return INPUT_FORM_LIST_URL;

	}

	public String deleteProDuctVarietyTaX() {
		try {

			if (productVarietyTaxDto.getId() == null) {
				Util.addWarn("Please select one Product Variety Tax");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProduCtVarietyDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}

	public String deletetProDuctVareityListConfirm() {

		try {
			log.info(
					"Delete deletetProDuctVareityListConfirm started the id is [" + productVarietyTaxDto.getId() + "]");
			ProductVarietyTax productVarietyTaxId = new ProductVarietyTax();
			productVarietyTaxId.setId(productVarietyTaxDto.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/deleteProDuctVareityTaxById";
			baseDTO = httpService.post(url, productVarietyTaxId);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("ProDuctVareity deleted successfully....!!!");
					// errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addInfo("Product Variety Deleted successfully");
					productVarietyTax = new ProductVarietyTax();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;
					return forceLogin();
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting EmployeeOtConfirm  ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String goToListPageOfProductVarietyTax() {
		return INPUT_FORM_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		productVarietyTaxDto = ((ProductVarietyTaxDto) event.getObject());
		disableAddButton = true;
	}

	public String clear() {
		log.info("<---- Start productVarietyTax . clear ----->");
		loadLazyProductVarietyList();
		selectedProductVarietyTax = new ProductVarietyTaxDto();
		log.info("<---- End productVarietyTax . clear ----->");
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;

	}

	public void removeListData(Double minValue, Double maxValue) {
		log.info("<---- Start productVarietyTax .removeListData ----->");
		log.info("<---- Minimum Value is ----->" + minValue);
		log.info("<---- Maximum Value is ----->" + maxValue);
		minimunTotal = minimunTotal - minValue;
		maximumTotal = maximumTotal - maxValue;
		log.info("<---- End productVarietyTax . removeListData ----->");

	}

	public void loadAllGroupsByProductCategorys() {
		log.info("<======== ProductVarietyTaxBean.loadAllGroupsByProductCategorys() calling ======>");
		listOfProductGroupMasterByProductCategoryId = new ArrayList<>();
		try {

			BaseDTO baseDTO = null;
			try {
				String url = serverURL + appPreference.getOperationApiUrl()
						+ "/productVarietyTaxControler/getproductGroupbycategory";
				log.info("url" + url);
				baseDTO = httpService.post(url, productVarietyTaxDto);
				if (baseDTO != null) {
					if (baseDTO.getResponseContents() != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
						listOfProductGroupMasterByProductCategoryId = mapper.readValue(jsonValue,
								new TypeReference<List<ProductGroupMaster>>() {
								});
						log.info("Total no. of Groups : " + listOfProductGroupMasterByProductCategoryId.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} catch (Exception e) {
				log.error("ProductVarietyTaxBean loadAllGroupsByProductCategorys Error", e);
			}

		} catch (Exception e) {
			log.error("<========== Exception ProductVarietyTaxBean.loadAllGroupsByProductCategorys()==========>", e);
		}
	}

	public void loadAllProductVarietysByProductGroups() {
		log.info("<======== ProductVarietyTaxBean.loadAllProductVarietysByProductGroups() calling ======>");
		listOfProductVarietyMasterByProductGroupMasterId = new ArrayList<>();
		try {

			BaseDTO baseDTO = null;
			listOfProductVarietyMasterByProductGroupMasterId = new ArrayList<>();
			try {
				String url = serverURL + appPreference.getOperationApiUrl()
						+ "/productVarietyTaxControler/getproductVarietysforproductgroup";
				log.info("url" + url);
				baseDTO = httpService.post(url, productVarietyTaxDto);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContents() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
						listOfProductVarietyMasterByProductGroupMasterId = mapper.readValue(jsonValue,
								new TypeReference<List<ProductVarietyMaster>>() {
								});
						log.info("Total no. of ProductVarietys : "
								+ listOfProductVarietyMasterByProductGroupMasterId.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} catch (Exception e) {
				log.error("ProductVarietyTaxBean loadAllProductVarietysByProductGroups Error", e);
			}

		} catch (Exception e) {
			log.error("<========== Exception ProductVarietyTaxBean.loadAllProductVarietysByProductGroups()==========>",
					e);
		}
	}

}
