package in.gov.cooptex.operation.rest.ui.master;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.model.SocietyClassMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.InfoCodeDescription;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
@Data
@EqualsAndHashCode(callSuper = false)
public class SocietyClassBean extends BaseDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	String statusValue = null;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	/** for client server connection */
	RestTemplate restTemplate;

	String serverURL = null;

	/** To maintain button values */
	String action = null;

	SocietyClassMaster societyClassMaster;
	/** Object referred while selecting a record */
	SocietyClassMaster selectedSocietyClassMaster;
	/** To maintain Society List */
	List<SocietyClassMaster> societyClassList;

	Set<SocietyClassMaster> selectedSocietyClassSet;

	WrapperDTO wrapperDto = new WrapperDTO();

	boolean visible;

	// @PostConstruct
	public void init() {
		log.info("SocietyClass Bean ---init()");
		loadSocietyList();

	}

	public SocietyClassBean() {
		log.info("---SocietyClassBean Constructor---");
		restTemplate = new RestTemplate();
		societyClassMaster = new SocietyClassMaster();
		selectedSocietyClassMaster = new SocietyClassMaster();
		loadValues();
	}

	private void loadValues() {
		log.info("---loadValues----");
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String showSocietyClass() {

		log.info("---showSocietyClass()----");
		action = "ADD";
		loadSocietyList();
		log.info("---showSocietyClass()----" + societyClassMaster);
		societyClassMaster = new SocietyClassMaster();
		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true";
	}

	public void clear() {
		log.info("Society Class bean   clear--");
		societyClassMaster = new SocietyClassMaster();
		selectedSocietyClassMaster = new SocietyClassMaster();
		statusValue = null;
		loadSocietyList();
	}

	public String addSociety() {
		log.info("<--- addSociety() --->");
		log.info("button value   " + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {

				log.info("in condition---------" + selectedSocietyClassMaster.getCode());

				if (selectedSocietyClassMaster.getName().trim().length() < 3
						|| selectedSocietyClassMaster.getName().trim().length() > 50) {

					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.SOCIETY_CLASS_NAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}

				if (selectedSocietyClassMaster.getLname().trim().length() < 3
						|| selectedSocietyClassMaster.getLname().trim().length() > 50) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.SOCIETY_CLASS_LNAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}
				if (selectedSocietyClassMaster.getTurnoverMin().toString().trim().length() < 3
						|| selectedSocietyClassMaster.getTurnoverMin().toString().trim().length() > 9) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.SOCIETY_CLASS_MIN_TURN_OVER.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}
				if (selectedSocietyClassMaster.getTurnoverMax().toString().trim().length() < 3
						|| selectedSocietyClassMaster.getTurnoverMax().toString().trim().length() > 9) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.SOCIETY_CLASS_MAX_TURN_OVER.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}
				if (selectedSocietyClassMaster.getTurnoverMin() > selectedSocietyClassMaster.getTurnoverMax()) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ERROR_SOCIETY_CLASS_MIN_MAX_VALIDATION.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}

				log.info("selectedSocietyClassMaster " + selectedSocietyClassMaster);

				UserMaster userMaster = new UserMaster();
				userMaster.setId(loginBean.getUserDetailSession().getId());
				selectedSocietyClassMaster.setCreatedBy(userMaster);
				selectedSocietyClassMaster.setCreatedDate(new Date());

				String url = serverURL + "/societyClass/addFormData";

				RestTemplate restTemplate = new RestTemplate();
				HttpEntity<SocietyClassMaster> requestEntity = new HttpEntity<SocietyClassMaster>(
						selectedSocietyClassMaster, loginBean.headers);
				log.info(url + "<--- requestEntity ---> " + requestEntity);
				HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
						BaseDTO.class);
				log.info(url + "<--- responseEntity ---> " + responseEntity);
				BaseDTO baseDTO = responseEntity.getBody();

				log.info(url + "<--- baseDTO ---> " + baseDTO);

				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info(url + "<--- baseDTO.getStatusCode() == 0 --->");
						Util.addInfo(InfoCodeDescription.getDescription(
								InfoCodeDescription.INFO_SOCIETY_CLASS_MASTER_ADD.getInfoCode(),
								languageBean.getLocaleCode()));
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

						loadSocietyList();

					} else {
						String msg = ErrorCodeDescription.getDescription(baseDTO.getStatusCode());
						Util.addError(ErrorCodeDescription.getDescription(baseDTO.getStatusCode(),
								languageBean.getLocaleCode()));
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						return null;
					}

				}

			}

		} catch (Exception e) {
			log.info("error in add society  ");

			e.printStackTrace();
		}
		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true";
	}

	public void loadSocietyList() {
		RestTemplate restTemplate = new RestTemplate();
		societyClassList = new ArrayList<SocietyClassMaster>();
		try {
			String url = serverURL + "/societyClass/societyList";
			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			ObjectMapper mapper = new ObjectMapper();
			societyClassList = mapper.readValue(result.getBody(),
					mapper.getTypeFactory().constructCollectionType(List.class, SocietyClassMaster.class));
			if (societyClassList.isEmpty()) {
				log.info("#==--> Society class list Not Available <--==#" + societyClassList);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error in loading Data in ------loadSocietyList()" + e);
		}
	}

	/* showForm will validate on entity and redirect to respective xhtml */

	public String showForm() {
		log.info("<---Inside society class showForm--->" + action);

		if (action.equalsIgnoreCase("EDIT")) {

			if (selectedSocietyClassMaster == null) {
				log.info("Please Select atleast One Society class");
				Util.addWarn(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.SELECT_ANY_SOCIETY_CLASS.getErrorCode(), languageBean.getLocaleCode()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return null;
			}

		} else if (action.equalsIgnoreCase("VIEW")) {

			log.info("<---Inside society class showForm(view)--->" + action);
			if (selectedSocietyClassMaster == null) {
				log.info("Please Select atleast One Society class");
				Util.addWarn(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.SELECT_ANY_SOCIETY_CLASS.getErrorCode(), languageBean.getLocaleCode()));
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return null;
			}
		} else if (action.equalsIgnoreCase("ADD")) {

			log.info("<---Inside society class showForm(ADD)--->" + action);

			selectedSocietyClassMaster = new SocietyClassMaster();
			// selectedSocietyClassMaster.setActiveStatus(true);
		}

		return "/pages/master/addSocietyClass.xhtml?faces-redirect=true";

	}

	/* Here data will be stored/updated/viewed */

	public String submitForm() {

		if (action.equalsIgnoreCase("EDIT")) {
			UserMaster userMaster = new UserMaster();
			userMaster.setId(loginBean.getUserDetailSession().getId());
			selectedSocietyClassMaster.setModifiedBy(userMaster);
			selectedSocietyClassMaster.setModifiedDate(new Date());
			userMaster.setUserMasterVersion(selectedSocietyClassMaster.getVersion());

			String url = serverURL + "/societyClass/update";
			HttpEntity<SocietyClassMaster> requestEntity = new HttpEntity<SocietyClassMaster>(
					selectedSocietyClassMaster, loginBean.headers);
			HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					BaseDTO.class);
			BaseDTO baseDTO = responseEntity.getBody();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					Util.addInfo(InfoCodeDescription.getDescription(
							InfoCodeDescription.INFO_SOCIETY_CLASS_MASTER_UPDATED.getInfoCode(),
							languageBean.getLocaleCode()));
					log.info("Society Class master has been Updated");
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				} else {
					String msg = ErrorCodeDescription.getDescription(baseDTO.getStatusCode());
					Util.addError(msg);
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					return null;
				}
			}
		}

		selectedSocietyClassMaster = new SocietyClassMaster();
		loadSocietyList();

		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true";

	}

	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (selectedSocietyClassMaster == null) {
			log.info("<---if(selectedSocietyClassMaster == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			log.info("Please Select atleast One Organization");
			Util.addWarn(ErrorCodeDescription.getDescription(
					ErrorCodeDescription.SELECT_ANY_SOCIETY_CLASS.getErrorCode(), languageBean.getLocaleCode()));
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return;
		} else {
			log.info("<---else(customerMasterDTO == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called--->");
		UserMaster userMaster = new UserMaster();
		userMaster.setId(loginBean.getUserDetailSession().getId());
		selectedSocietyClassMaster.setModifiedBy(userMaster);
		selectedSocietyClassMaster.setModifiedDate(new Date());
		userMaster.setUserMasterVersion(selectedSocietyClassMaster.getVersion());

		String url = serverURL + "/societyClass/delete";
		HttpEntity<SocietyClassMaster> requestEntity = new HttpEntity<SocietyClassMaster>(selectedSocietyClassMaster,
				loginBean.headers);
		HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, BaseDTO.class);
		BaseDTO baseDTO = responseEntity.getBody();
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				Util.addInfo(InfoCodeDescription.getDescription(
						InfoCodeDescription.INFO_SOCIETY_CLASS_MASTER_DELETED.getInfoCode(),
						languageBean.getLocaleCode()));
				log.info("Society Class master has been Deleted");
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				loadSocietyList();
			} else {
				String msg = ErrorCodeDescription.getDescription(baseDTO.getStatusCode());
				Util.addError(msg);
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return null;
			}
		}
		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true";
	}

	public String cancelSocietyClass() {
		log.info(" cancelSocietyClass called ");
		selectedSocietyClassMaster = new SocietyClassMaster();

		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true;";
	}

	public String search() {

		log.info("<---Society Class  search() --->");
		log.info("button value   " + action);
		try {
			if (action.equals("search")) {

				String searchUrl = serverURL + "/societyClass/search";
				RestTemplate restTemplate = new RestTemplate();
				HttpEntity<SocietyClassMaster> requestEntity = new HttpEntity<SocietyClassMaster>(societyClassMaster,
						loginBean.headers);
				ResponseEntity<String> responseEntity = restTemplate.exchange(searchUrl, HttpMethod.POST, requestEntity,
						String.class);
				ObjectMapper mapper = new ObjectMapper();
				societyClassList = mapper.readValue(responseEntity.getBody(),
						mapper.getTypeFactory().constructCollectionType(List.class, SocietyClassMaster.class));
			}
		} catch (Exception e) {
			log.error("<--- Error in Search --->" + e);
		}
		return "/pages/master/createSocietyClass.xhtml?faces-redirect=true";

	}
}
