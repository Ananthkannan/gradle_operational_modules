package in.gov.cooptex.personnel.rest.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.model.UploadedFile;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class Util {

	/*
	 * Singleton Class
	 */
	private static Util util = null;

	@Getter
	@Setter
	private static Properties prop;

	public Util() {

	}

	public static Util getInstance() {
		if (util == null) {
			util = new Util();
		}
		return util;
	}

	public String getPropValues(String value) throws IOException {
		if (prop == null) {
			getInstance().load();
		}
		return prop.getProperty(value);
	}

	public void load() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("application.properties");
		prop = new Properties();
		prop.load(inputStream);
	}

	public static boolean isEmpty(String str) {
		return str == null || str.trim().length() == 0;
	}

	public static void addWarn(String str) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, str, " "));
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	}

	public static void addError(String str) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, str, ""));
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	}

	public static void addInfo(String str) {
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, str, ""));
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	}

	public String getPortalServerURL() {
		try {
			return getInstance().getPropValues("portal.server.url");
		} catch (IOException e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
		return null;
	}

	public static String fileUpload(String path, UploadedFile relievingLetterFile) {
		String photoPath = null;
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			// String candiPhotoPath =
			// getInstance().getPropValues("jobapplication.photo.upload.path");
			long size = relievingLetterFile.getSize();
			log.info("size==>" + size);
			if (size > 0) {
				// String photoPathName =
				// commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");

				log.info("path==>" + path);
				/*
				 * In this regex, '^' not, so below given statement will replace all other
				 * characters not matching given expression will be replaced with '_'
				 */
				photoPath = path + "/uploaded/" + relievingLetterFile.getFileName().replaceAll("[^A-Za-z0-9.]", "_");

				log.info("photoPath==>" + photoPath);
				Path pathName = Paths.get(path + "/uploaded/");
				File outputFile = new File(photoPath);
				if (Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				inputStream = relievingLetterFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) relievingLetterFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath==>" + photoPath);
			}
		} catch (Exception e) {
			log.error("fileUpload error==>", e);
		}
		return photoPath;
	}

	/**
	 * File name with saved dynamically
	 */

	public static String uploadDocument(String path, UploadedFile relievingLetterFile, String fileName) {
		String photoPath = null;
		String fileStatus = new String();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStamp = Long.toString(timestamp.getTime());
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			long size = relievingLetterFile.getSize();
			log.info("size==>" + size);
			if (size > 0) {
				log.info("path==>" + path);
				log.info("fileName==>" + fileName);
				if (fileName == null || fileName.equals(""))
					fileName = timeStamp;

				photoPath = path + "/uploaded/" + fileName + "_" + relievingLetterFile.getFileName();
				;
				File checkFile = new File(photoPath);
				if (checkFile.exists()) {
					log.error("The same file name already exist in this directory");
					fileStatus = "Error";
					return fileStatus;
				}
				log.info("photoPath==>" + photoPath);
				Path pathName = Paths.get(path + "/uploaded/");
				File outputFile = new File(photoPath);
				if (Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				inputStream = relievingLetterFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) relievingLetterFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath==>" + photoPath);
				fileStatus = photoPath;
			}
		} catch (Exception e) {
			log.error("fileUpload error==>", e);
		}
		return fileStatus;
	}

	/**
	 * 
	 * @param parentpath,subPath,uploadFile,fileName
	 * @param uploadedFile
	 * @return
	 */

	public static String uploadDocument(String parentPath, String subPath, UploadedFile uploadFile, String fileName) {
		String photoPath = null;
		String fileStatus = new String();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStamp = Long.toString(timestamp.getTime());
		String supFolderFile = new String();
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			long size = uploadFile.getSize();
			log.info("size==>" + size);
			if (size > 0) {
				log.info("patent path==>" + parentPath);
				log.info("subfolder path==>" + subPath);
				log.info("fileName==>" + fileName);
				if (fileName == null || fileName.equals(""))
					fileName = timeStamp;

				photoPath = parentPath + subPath + '/' + fileName + "_" + uploadFile.getFileName();
				supFolderFile = subPath + '/' + fileName + "_" + uploadFile.getFileName();// Get subFolder with file
																							// name
				File checkFile = new File(photoPath);
				if (checkFile.exists()) {
					log.error("The same file name already exist in this directory");
					fileStatus = "Error";
					return fileStatus;
				}
				log.info("photoPath==>" + photoPath);

				Path pathName = Paths.get(parentPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				inputStream = uploadFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) uploadFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath full Path==>" + photoPath);
				log.info("photoPath sub folder path==>" + supFolderFile);
				fileStatus = photoPath;
			}
		} catch (Exception e) {
			log.error("fileUpload error==>"+ e);
		}
		return supFolderFile;
	}

	public static String excelFileUpload(String path, UploadedFile uploadedFile) {
		String photoPath = null;
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			long size = uploadedFile.getSize();
			log.info("size==>" + size);
			if (size > 0) {

				log.info("path==>" + path);
				photoPath = path + uploadedFile.getFileName();

				log.info("photoPath==>" + photoPath);
				Path pathName = Paths.get(path);
				File outputFile = new File(photoPath);
				if (!Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				inputStream = uploadedFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) uploadedFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath==>" + photoPath);
			}
		} catch (Exception e) {
			log.error("fileUpload error==>", e);
		}
		return photoPath;
	}

	/**
	 * @author Saranya
	 * @param fromDate
	 * @param toDate
	 * @return DIfference days between from date and to date
	 */
	public static String diffDaysBetweenTwoDates(Date fromDate, Date toDate) {
		String dayTime = "";
		try {
			log.info("Incoming from date and to date...." + fromDate + "..." + toDate);
			LocalDateTime localDate = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime todayDate = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

			long days = ChronoUnit.DAYS.between(localDate, todayDate);
			long hours = ChronoUnit.HOURS.between(localDate, todayDate);
			long minutes = ChronoUnit.MINUTES.between(localDate, todayDate);

			dayTime = "Days:" + days + " Time:" + hours % 24 + ":" + minutes % 60;
			log.info("Days:" + days + " Time:" + hours % 24 + ":" + minutes % 60);
		} catch (Exception e) {
			log.error("Exception occured while finding days difference between two dates...", e);
		}
		return dayTime;
	}

	public static List<Long> generateNextFiftyYears() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int year = cal.get(Calendar.YEAR);
		List<Long> yearList = new ArrayList<>();
		yearList.add((long) year);
		for (int i = 0; i < 50; i++) {
			year = year + 1;
			yearList.add((long) year);
		}
		return yearList;
	}

	/**
	 * @author prabhu
	 * @param fromDate
	 * @param toDate
	 * @return
	 */
	public static Integer noOfDaysBetweenTwoDates(Date fromDate, Date toDate) {
		log.info("Incoming from date and to date...." + fromDate + "..." + toDate);
		Integer days = 0;
		try {
			if (fromDate == toDate)
				return 1;
			LocalDateTime localDate = fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			LocalDateTime todayDate = toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			days = (int) ChronoUnit.DAYS.between(localDate, todayDate);
			log.info("Days:" + days);
		} catch (Exception e) {
			log.error("Exception occured in  noOfDaysBetweenTwoDates...", e);
		}
		return days;
	}
}
