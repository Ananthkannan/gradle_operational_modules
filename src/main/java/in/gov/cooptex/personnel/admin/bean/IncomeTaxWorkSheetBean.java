package in.gov.cooptex.personnel.admin.bean;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmpDTO;
import in.gov.cooptex.core.dto.EmployeeSearchResponse;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.EmpIncomeTaxWorksheet;
import in.gov.cooptex.core.model.EmpIncomeTaxWorksheetIncome;
import in.gov.cooptex.core.model.EmpIncomeTaxWorksheetSavings;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.finance.dto.PettyCashReceiptDTO;
import in.gov.cooptex.operation.dto.SalesInvoiceItemDTO;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.personnel.hrms.dto.EmpIncomeTaxWorksheetDTO;
import in.gov.cooptex.personnel.hrms.rest.ui.EmployeePayRollBean;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */
@Log4j2
@Service("incomeTaxWorkSheetBean")
@Scope("session")
public class IncomeTaxWorkSheetBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 2642858814973759713L;

	private final String LIST_INCOME_TAX_WORKSHEET_PAGE = "/pages/personnelHR/payRoll/listIncomeTaxWorkSheet.xhtml?faces-redirect=true;";

	private final String CREATE_INCOME_TAX_WORKSHEET_PAGE = "/pages/personnelHR/payRoll/createIncomeTaxWorkSheet.xhtml?faces-redirect=true;";

	private final String VIEW_INCOME_TAX_WORKSHEET_PAGE = "/pages/personnelHR/payRoll/viewIncomeTaxWorkSheet.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	EmployeePayRollBean employeePayRollBean;

	@Getter
	@Setter
	IncomeTaxWorkSheetBean incomeTaxWorkSheetBean;

	@Autowired
	HttpService httpService;
	@Getter
	@Setter
	private String action;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Getter
	@Setter
	int totalRecords = 0;
	ObjectMapper mapper;
	@Autowired
	AppPreference appPreference;
	@Setter
	@Getter
	List<EntityMaster> headRegionOfficeList;
	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc = new EntityMaster();
	@Autowired
	CommonDataService commonDataService;
	@Setter
	@Getter
	List<EntityTypeMaster> entityTypeList;
	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;
	@Getter
	@Setter
	EntityMaster selectedEntity;
	@Setter
	@Getter
	List<EntityMaster> entityList;
	@Getter
	@Setter
	String createNote;
	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<>();
	@Getter
	@Setter
	EmployeeMaster selectedEmployeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	EmpIncomeTaxWorksheet empIncomeTaxWorksheet;

	@Getter
	@Setter
	EmpIncomeTaxWorksheetDTO empIncomeTaxWorksheetdto, slabDTO;

	@Getter
	@Setter
	EmpIncomeTaxWorksheet selectedEmpIncomeTaxWorksheet;

	@Getter
	@Setter
	EmpIncomeTaxWorksheetSavings empIncomeTaxWorksheetSavings;

	@Getter
	@Setter
	EmpIncomeTaxWorksheetIncome empIncomeTaxWorksheetIncome;

	@Setter
	@Getter
	List<GlAccount> glAccountList;

	@Setter
	@Getter
	List<GlAccount> glAccountListDeduction;

	@Setter
	@Getter
	List<GlAccount> glAccountListSavings;

	@Setter
	@Getter
	GlAccount glAccount;

	@Setter
	@Getter
	GlAccount glAccountDeduction;

	@Getter
	@Setter
	List<FinancialYear> financialYearList;

	String jsonResponse;

	@Getter
	@Setter
	Double deductionAmount;

	@Getter
	@Setter
	Double savingsamount;

	@Getter
	@Setter
	Double totalsavingsamount;

	@Getter
	@Setter
	Double incomeamount;

	@Getter
	@Setter
	Double totaltentativeamount;

	@Getter
	@Setter
	Double eplsalaryarrears;

	@Getter
	@Setter
	String description;

	@Getter
	@Setter
	List<EmpIncomeTaxWorksheetSavings> empIncomeTaxWorksheetSavingsList = new ArrayList<>();

	@Getter
	@Setter
	List<EmpIncomeTaxWorksheetSavings> empIncomeTaxWorksheetDeductionList = new ArrayList<>();

	@Getter
	@Setter
	List<EmpIncomeTaxWorksheetIncome> empIncomeTaxWorksheetIncomeList = new ArrayList<>();

	@Getter
	@Setter
	private LazyDataModel<EmpIncomeTaxWorksheet> lazyModel;

	@Getter
	@Setter
	private String slab1_value;

	@Getter
	@Setter
	private EmpDTO empDTO = new EmpDTO();

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	Map<Integer, String> monthList;

	@Getter
	@Setter
	List<Integer> yearList;

	@Getter
	@Setter
	Integer incomeyear;

	@Getter
	@Setter
	Integer incomemonth;

	@Getter
	@Setter
	FinancialYear financialyear = new FinancialYear();

	@Getter
	@Setter
	boolean employeevisibility = false;

	@Getter
	@Setter
	boolean lockscreen = false;

	@Getter
	@Setter
	boolean employeecheck = false;

	@Getter
	@Setter
	Double totaldeduction;

	@Getter
	@Setter
	String deductiondescription;

	@PostConstruct
	public String init() {
		incomemonth = 0;
		incomeyear = 0;
		incomeamount = 0.0;
		// savingsamount=0.0;
		totalsavingsamount = 0.0;
		// setTotaltentativeamount(0.0);
		// empIncomeTaxWorksheet.setTotalSavings(0.0);
		eplsalaryarrears = 0.0;
		monthList = new HashMap<>();
		yearList = new ArrayList<>();
		selectedEntity = new EntityMaster();
		selectedEntityTypeMaster = new EntityTypeMaster();
		empIncomeTaxWorksheet = new EmpIncomeTaxWorksheet();
		empIncomeTaxWorksheet.setRebateAmount(0D);
		selectedHeadRegionOfc = new EntityMaster();
		slabDTO = new EmpIncomeTaxWorksheetDTO();
		headRegionOfficeList = commonDataService.loadHeadAndRegionalOfficeOrderByName();
		entityTypeList = commonDataService.getAllEntityType();
		glAccountList = commonDataService.findAllGlAccountOperationServer();
		glAccountListDeduction = commonDataService.loadOnlyDeductionGlaccount();
		glAccountListSavings = commonDataService.loadOnlySavingsGlaccount();
		empIncomeTaxWorksheetdto = new EmpIncomeTaxWorksheetDTO();
		empIncomeTaxWorksheet.setEplSalary(0.0);
		empIncomeTaxWorksheet.setOthers(0.0);
		empIncomeTaxWorksheet.setSurcharge(0.0);
		empIncomeTaxWorksheet.setTaxOtherThanSalary(0.0);
		empIncomeTaxWorksheet.setTotalSavings(0.0);
		empIncomeTaxWorksheet.setTaxableIncome(0.0);
		empIncomeTaxWorksheet.setEplSalaryArrears(0.0);

		loadAllFinancialYear();
		loadIncomeTaxWorksheet();
		loadslabValues();
		loadslabValueswithItConstantValue();
		loadValues();
		setSelectedEntity(null);
		setSelectedEmployeeMaster(null);
		empDTO.setDesName(null);
		empDTO.setGender(null);
		empDTO.setPfNumber(null);
		setTotaltentativeamount(0.0);
		empIncomeTaxWorksheet.setEplSalary(0.0);
		empIncomeTaxWorksheet.setEplSalaryArrears(0.0);
		employeevisibility = false;
		employeecheck = false;
		empIncomeTaxWorksheetSavingsList.clear();
		empIncomeTaxWorksheetIncomeList.clear();
		empIncomeTaxWorksheetDeductionList.clear();
		setTotaldeduction(0.0);
		setDeductionAmount(0.0);

		return LIST_INCOME_TAX_WORKSHEET_PAGE;
	}

	private void loadValues() {

		monthList = employeePayRollBean.loadMonths();
		yearList = AppUtil.getPrevCurrentYearList();
	}

	// public Map<Integer, String> loadMonths() {
	// log.info("Server url...." + SERVER_URL + "/payroll/getmonths/");
	// BaseDTO response = httpService.get(SERVER_URL + "/payroll/getmonths");
	// if (response.getStatusCode() != 0) {
	// errorMap.notify(response.getStatusCode());
	// return null;
	// }
	// try {
	// mapper = new ObjectMapper();
	// jsonResponse = mapper.writeValueAsString(response.getResponseContent());
	// monthList = mapper.readValue(jsonResponse, new TypeReference<Map<Integer,
	// String>>() {
	// });
	// log.info("month list");
	// } catch (JsonProcessingException jpEx) {
	// log.info("Exception while parsing ...", jpEx);
	// } catch (IOException e) {
	// log.info("IOException occured ... ", e);
	// }
	// return monthList;
	// }
	/**
	 * purpose: Add page Method
	 * 
	 */
	public String addPage() {
		action = "ADD";

		init();
		return CREATE_INCOME_TAX_WORKSHEET_PAGE;
	}

	/**
	 * purpose:used to load Entity list in IncomeTax WorkSheet add and list page
	 * 
	 */
	public void loadEntityList() {
		log.info("<=IncomeTaxWorkSheetBean ::  loadEntityList=>");
		try {
			if (selectedHeadRegionOfc != null && selectedHeadRegionOfc.getId() != null
					&& selectedEntityTypeMaster != null && selectedEntityTypeMaster.getId() != null) {
				entityList = new ArrayList<>();
				Long regionId = selectedHeadRegionOfc.getId();
				Long entityTypeId = selectedEntityTypeMaster.getId();
				log.info("loadEntityList :: regionId==> " + regionId);
				log.info("loadEntityList :: entityTypeId==> " + entityTypeId);
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
				if (entityList != null && !entityList.isEmpty()) {
					log.info("entity list size==> " + entityList.size());
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
					return;
				}
			} else {
				log.info("Head / Region office or entity type not selected");

			}
		} catch (Exception exp) {
			log.error("loadEntityList==> ", exp);

		}
	}

	/**
	 * purpose:used to load Entity list based on region and entity Type in IncomeTax
	 * WorkSheet add and list page
	 * 
	 */
	public void onSelectEntityType() {

		log.info("<<<< ----------Starts onSelectEntityType ------- >>>>");

		try {
			entityList = new ArrayList<>();
			if (selectedHeadRegionOfc != null && selectedHeadRegionOfc.getId() != null
					&& selectedEntityTypeMaster != null && selectedEntityTypeMaster.getId() != null) {
				Long regionId = selectedHeadRegionOfc.getId();
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId,
						selectedEntityTypeMaster.getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<<<< ----------End loadMeretinEntityList ------- >>>>");
	}

	/**
	 * purpose:used to load Employee List based on entity Id in IncomeTax WorkSheet
	 * add and list page
	 */

	public void loadEmployeeByEntity() {
		log.info("<=IncomeTaxWorkSheetBean :: loadEmployeeByEntity==>");
		try {
			if (selectedEntity == null || selectedEntity.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
				return;
			}
			employeeMasterList = new ArrayList<>();
			employeeMasterList = commonDataService.getEmployeeMasterByEntityId(selectedEntity.getId());

		} catch (Exception e) {
			log.error("Exception in loadEmployeeByEntity  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadslabValues() {
		log.info("<--- Inside loadslabValues() --->");
		try {
			String requestPath = AppUtil.getPortalServerURL() + "/incometaxworksheet/getslabvalue";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					slabDTO = mapper.readValue(jsonValue, new TypeReference<EmpIncomeTaxWorksheetDTO>() {
					});
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadslabValues() --->", exp);
		}
	}

	/*
	 * public List<FinancialYear> loadAllFinancialYear() {
	 * log.info("loadAllFinancialYear==>"); try { BaseDTO baseDTO = new BaseDTO();
	 * String url = SERVER_URL + "/incometaxworksheet/getallfinancialyear";
	 * log.info("loadAllFinancialYear url==>" + url); baseDTO =
	 * httpService.get(url); if (baseDTO != null) { mapper = new ObjectMapper();
	 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	 * jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * financialYearList = mapper.readValue(jsonResponse, new
	 * TypeReference<List<FinancialYear>>() { }); }
	 * 
	 * } catch (Exception e) { log.info("Error in loadAllowanceGradeCity()>>>>>>>>"
	 * + e); } return financialYearList; }
	 */

	public void loadAllFinancialYear() {
		log.info("loadAllFinancialYear :: started");
		try {

			String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
			log.info("loadAllFinancialYear :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				financialYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
				});
				if (financialYearList != null && !financialYearList.isEmpty()) {
					log.info("blockYearList list size is " + financialYearList.size());
				}
			}
		} catch (Exception e) {
			log.error("loadAllFinancialYear :: Exception=> " + e);
		}

	}

	public void generate() {
		log.info("To calculate the employee Income Tax WOrksheet");
		try {

			if (selectedEmployeeMaster != null) {
				log.info("employee idddddddddddd==> " + selectedEmployeeMaster.getId());
				empIncomeTaxWorksheet.setEmpMaster(selectedEmployeeMaster);
				loadEmpDetails(selectedEmployeeMaster.getId());
			}

			setIncomeamount(null);
			setIncomeyear(null);
			setIncomeamount(null);

			empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheetIncomeList(null);

			// clearIncomeValues();
			// clearSavings();

			setTotaltentativeamount(0.0);
			empIncomeTaxWorksheet.setEplSalary(0.0);
			empIncomeTaxWorksheet.setSalesCommission(0.0);
			empIncomeTaxWorksheet.setIncentive(0.0);
			empIncomeTaxWorksheet.setBonusSpecialSalesCommission(0.0);
			empIncomeTaxWorksheet.setEplSalaryArrears(0.0);
			empIncomeTaxWorksheet.setOthers(0.0);
			setIncomeamount(0.0);
			setIncomemonth(null);
			setIncomeyear(null);
			empIncomeTaxWorksheetIncomeList = new ArrayList<>();
			empIncomeTaxWorksheet.setTotalSavings(0.0);

			log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());
			String url = SERVER_URL + "/incometaxworksheet/getemployeepayrolldetails";
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheet);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empIncomeTaxWorksheet = mapper.readValue(jsonResponse, new TypeReference<EmpIncomeTaxWorksheet>() {

				});
			} else {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			employeevisibility = true;
			lockscreen = false;
			visibility = true;

		} catch (Exception e) {
			log.error("generate :: exception ==> ", e);
		}
	}

	public void loadEmpDetails(Long empId) {
		log.info("loadEmpDetails start");
		try {
			log.info("empId==> " + empId);
			String url = SERVER_URL + "/employee/getempbasicinformation/" + empId;
			log.info("loadEmpDetails :: URL=> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				List<EmpDTO> empDTOList = mapper.readValue(jsonResponse, new TypeReference<List<EmpDTO>>() {
				});
				if (empDTOList != null && !empDTOList.isEmpty()) {
					empDTO = empDTOList.get(0);
					log.info("loadEmpDetails :: empDTO=> " + empDTO);
				} else {
					log.error("empDTO null or empty");
				}

			} else {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

		} catch (Exception e) {
			log.error("loadEmpDetails :: exception ==> ", e);
		}
	}

	public void generateSavings() {
		log.info("To generateSavings of the employee" + empIncomeTaxWorksheet.getTotalGrossIncome());

		try {
			if (glAccount != null) {
				empIncomeTaxWorksheet.setGlaccountid(glAccount.getId());
			}

			log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());
			String url = SERVER_URL + "/incometaxworksheet/getemployeesavingsdetails";
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheet);
			if (baseDTO != null) {
				savingsamount = baseDTO.getSumTotal();
				empIncomeTaxWorksheet = mapper.readValue(jsonResponse, new TypeReference<EmpIncomeTaxWorksheet>() {
				});
			} else {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

		} catch (Exception e) {

		}
	}

	public void generateSavings80D() {
		log.info("To generateSavings80D of the employee" + empIncomeTaxWorksheet.getTotalGrossIncome());

		try {
			if (glAccountDeduction != null) {
				empIncomeTaxWorksheet.setGlaccountid(glAccountDeduction.getId());
			}

			log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());
			String url = SERVER_URL + "/incometaxworksheet/getemployeesavingsdetails";
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheet);
			if (baseDTO != null) {
				deductionAmount = baseDTO.getSumTotal();
				// empIncomeTaxWorksheet = mapper.readValue(jsonResponse, new
				// TypeReference<EmpIncomeTaxWorksheet>() {
				// });
			} else {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

		} catch (Exception e) {

		}
	}

	public void generateSavings80C() {
		log.info("To generateSavings80C of the employee" + empIncomeTaxWorksheet.getTotalGrossIncome());

		try {
			if (glAccount != null) {
				empIncomeTaxWorksheet.setGlaccountid(glAccount.getId());
			}

			log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());
			String url = SERVER_URL + "/incometaxworksheet/getemployeesavingsdetails";
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheet);
			if (baseDTO != null) {
				savingsamount = baseDTO.getSumTotal();
				// empIncomeTaxWorksheet = mapper.readValue(jsonResponse, new
				// TypeReference<EmpIncomeTaxWorksheet>() {
				// });
			} else {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

		} catch (Exception e) {

		}
	}

	public void calculate() {
		log.info("To addSavings of the employee" + empIncomeTaxWorksheet.getTotalGrossIncome());
		empIncomeTaxWorksheetSavings = new EmpIncomeTaxWorksheetSavings();
		Double actualincometax;
		try {
			if (glAccount == null) {
				AppUtil.addError("Tax filling Items Should Not Be Empty");

				return;
			}

			if (savingsamount == null || savingsamount == 0) {
				AppUtil.addError("Amount Should Not Be Empty");

				return;
			}

			if (empIncomeTaxWorksheetSavingsList.stream()
					.anyMatch(e -> e.getGlAccount().getId() == (glAccount != null ? glAccount.getId() : 0)))

			{

				AppUtil.addError("Tax filling Items Already Exist");
				return;
			}

			Double totsavin = empIncomeTaxWorksheetSavingsList.stream()
					.filter(account -> account != null && account.getAmount() != null)
					.collect(Collectors.summingDouble(o -> o.getAmount()));

			empIncomeTaxWorksheet.setTotalSavings(totsavin);
			log.info("slabvalueee" + slabDTO.getSlab_constant_total_saving());

			// log.info("totalsavingssssssssamt" + totalsavingsamount);

			if (glAccount != null) {
				empIncomeTaxWorksheetSavings.setGlAccount(glAccount);
				empIncomeTaxWorksheetSavings.setAmount(savingsamount);
				empIncomeTaxWorksheetSavings.setDescription(description);
				empIncomeTaxWorksheetSavingsList.add(empIncomeTaxWorksheetSavings);
			}

			/*
			 * Double totsavingamount=empIncomeTaxWorksheet.getTotalSavings()+savingsamount;
			 * log.info("totalsavingssssssssamt" + empIncomeTaxWorksheet.getTotalSavings());
			 * 
			 * empIncomeTaxWorksheet.setTotalSavings(totsavingamount);
			 */

			// Long totalgrossround = Math.round( empIncomeTaxWorksheet.getIncomeSalary() +
			// empIncomeTaxWorksheet.getEplSalary() +
			// empIncomeTaxWorksheet.getSalesCommission() +
			// empIncomeTaxWorksheet.getIncentive() +
			// empIncomeTaxWorksheet.getBonusSpecialSalesCommission()+
			// empIncomeTaxWorksheet.getEplSalaryArrears() +
			// empIncomeTaxWorksheet.getOthers() );
			log.info("grosssss" + empIncomeTaxWorksheet.getTotalGrossIncome());
			Long totalgrossround = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());

			Long totaldeductions = Math.round(getTotaldeduction());

			Long totalsavinground = Math.round(empIncomeTaxWorksheet.getTotalSavings() + savingsamount);

			/// empIncomeTaxWorksheet.setTotalSavings(totalsavinground.doubleValue());

			log.info("taxxxinnnnnnn" + empIncomeTaxWorksheet.getTaxableIncome());

			log.info("grosssssssss" + totalgrossround);

			log.info("Slab value" + slabDTO.getSlab_constant_total_saving());

			if (totalsavinground >= slabDTO.getSlab_constant_total_saving()) {

				empIncomeTaxWorksheet.setTotalSavings(slabDTO.getSlab_constant_total_saving());
				empIncomeTaxWorksheet.setTaxableIncome(
						totalgrossround.doubleValue() - (slabDTO.getSlab_constant_total_saving() + totaldeductions));

			} else {
				empIncomeTaxWorksheet.setTotalSavings(totalsavinground.doubleValue());
				empIncomeTaxWorksheet.setTaxableIncome(
						totalgrossround.doubleValue() - (totalsavinground.doubleValue() + totaldeductions));
			}

			calculateActualIncomeTax();
			// if (empIncomeTaxWorksheet.getRebateAmount() != null ||
			// empIncomeTaxWorksheet.getRebateAmount() <= 0) {
			// actualincometax = empIncomeTaxWorksheet.getTaxableIncome() -
			// empIncomeTaxWorksheet.getRebateAmount();
			// } else {
			// actualincometax = empIncomeTaxWorksheet.getTaxableIncome();
			// }

			// if (actualincometax != null || actualincometax <= 0D) {
			//
			// if (actualincometax <= 0 && actualincometax <= slabDTO.getSlab0_value()) {
			//
			// empIncomeTaxWorksheet.setActualIncomeTax(0D);
			//
			// } else if (actualincometax > slabDTO.getSlab1_value()) {
			//
			// actualincometax = actualincometax - slabDTO.getSlab1_value();
			//
			// empIncomeTaxWorksheet.setActualIncomeTax(0D);
			//
			// if (actualincometax > slabDTO.getSlab1_value()) {
			//
			// empIncomeTaxWorksheet
			// .setActualIncomeTax(slabDTO.getSlab1_value() *
			// slabDTO.getSlab1_percentage());
			//
			// actualincometax = actualincometax - slabDTO.getSlab1_value();
			//
			// if (actualincometax > slabDTO.getSlab2_value()) {
			// empIncomeTaxWorksheet
			// .setActualIncomeTax(slabDTO.getSlab2_value() *
			// slabDTO.getSlab2_percentage());
			//
			// actualincometax = actualincometax - slabDTO.getSlab2_value();
			//
			// if (actualincometax > 1000000) {
			// empIncomeTaxWorksheet.setActualIncomeTax(1000000 *
			// slabDTO.getSlab3_percentage());
			//
			// } else {
			// empIncomeTaxWorksheet
			// .setActualIncomeTax(actualincometax * slabDTO.getSlab3_percentage());
			// }
			// } else {
			// empIncomeTaxWorksheet.setActualIncomeTax(actualincometax *
			// slabDTO.getSlab2_percentage());
			// }
			// } else {
			// empIncomeTaxWorksheet.setActualIncomeTax(actualincometax *
			// slabDTO.getSlab1_percentage());
			// }
			// }
			// }

			// empIncomeTaxWorksheet.setTaxEducationalCess(
			// empIncomeTaxWorksheet.getActualIncomeTax() *
			// slabDTO.getEdu_cess_tax_percentage());
			// if (empIncomeTaxWorksheet.getSurcharge() != null &&
			// empIncomeTaxWorksheet.getSurcharge() > 0) {
			// empIncomeTaxWorksheet.setTaxPayable(empIncomeTaxWorksheet.getActualIncomeTax()
			// + empIncomeTaxWorksheet.getTaxEducationalCess() +
			// empIncomeTaxWorksheet.getSurcharge());
			// } else {
			// empIncomeTaxWorksheet.setTaxPayable(
			// empIncomeTaxWorksheet.getActualIncomeTax() +
			// empIncomeTaxWorksheet.getTaxEducationalCess());
			// }
			// if (empIncomeTaxWorksheet.getTaxOtherThanSalary() != null
			// && empIncomeTaxWorksheet.getTaxOtherThanSalary() > 0) {
			// empIncomeTaxWorksheet.setTotalTaxPaid(
			// empIncomeTaxWorksheet.getTaxOtherThanSalary() +
			// empIncomeTaxWorksheet.getTaxDeductedSalary());
			// } else {
			// empIncomeTaxWorksheet.setTotalTaxPaid(empIncomeTaxWorksheet.getTaxDeductedSalary());
			// }
			// empIncomeTaxWorksheet.setBalanceTaxToBePaid(
			// empIncomeTaxWorksheet.getTaxPayable() -
			// empIncomeTaxWorksheet.getTotalTaxPaid());

			glAccount = null;
			savingsamount = null;
			description = null;

		} catch (Exception e) {
			log.info("Exeception in Calculate WOrksheet==========>>>>" + e);
		}
	}

	/**
	 * To Submit form data
	 */
	public String submit() {
		log.info("Save employee WorkSheet");
		try {
			if (empIncomeTaxWorksheet != null) {
				if (selectedEntity != null) {
					empIncomeTaxWorksheet.setEntityMaster(selectedEntity);
				}

				empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheet(empIncomeTaxWorksheet);
				empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheetSavingsList(empIncomeTaxWorksheetSavingsList);

				log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());
				String url = SERVER_URL + "/incometaxworksheet/add";
				log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
				BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheetdto);
				if (baseDTO != null) {
					mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					empIncomeTaxWorksheet = mapper.readValue(jsonResponse, new TypeReference<EmpIncomeTaxWorksheet>() {
					});
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}

		} catch (Exception e) {
			log.error("submit :: exception ==> ", e);
		}
		// loadIncomeTaxWorksheet();
		return clearPage();
	}

	public String viewIncomeTaxWorksheet() {
		// log.info(": viewIncomeTaxWorksheet Started " +
		// selectedEmpIncomeTaxWorksheet.getId());
		try {
			if (selectedEmpIncomeTaxWorksheet == null) {
				Util.addWarn("Please Select atleast One");
				return null;
			}

			String url = AppUtil.getPortalServerURL() + "/incometaxworksheet/getincometaxworksheet/"
					+ selectedEmpIncomeTaxWorksheet.getId();
			log.info("getEmployeeMasterByEntityId url==>" + url);
			BaseDTO relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
			empIncomeTaxWorksheetdto = mapper.readValue(jsonResponse, new TypeReference<EmpIncomeTaxWorksheetDTO>() {
			});
			/*
			 * Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
			 * .filter(account -> account != null && account.getAmount() != null)
			 * .collect(Collectors.summingDouble(o -> o.getAmount()));
			 * 
			 * setTotaltentativeamount(totaltentamt);
			 */

			selectedHeadRegionOfc = empIncomeTaxWorksheetdto.getRegion();

			/*
			 * selectedEntityTypeMaster =
			 * empIncomeTaxWorksheet.getEntityMaster().getEntityTypeMaster(); selectedEntity
			 * = empIncomeTaxWorksheet.getEntityMaster();
			 */

			log.info("headofff" + selectedHeadRegionOfc);
			empIncomeTaxWorksheet = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheet();
			empIncomeTaxWorksheetSavingsList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetSavingsList();
			empIncomeTaxWorksheetIncomeList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetIncomeList();
			empIncomeTaxWorksheetDeductionList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetDeductionList();
			Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
					.filter(account -> account != null && account.getAmount() != null)
					.collect(Collectors.summingDouble(o -> o.getAmount()));

			setTotaltentativeamount(totaltentamt);

			Double totaldeductionamount = empIncomeTaxWorksheetDeductionList.stream()
					.filter(account -> account != null && account.getAmount() != null)
					.collect(Collectors.summingDouble(o -> o.getAmount()));

			setTotaldeduction(totaldeductionamount);
			selectedEmployeeMaster = empIncomeTaxWorksheet.getEmpMaster();

			loadEmpDetails(selectedEmployeeMaster.getId());
			log.info("viewIncomeTaxWorksheet end ===>" + empIncomeTaxWorksheet);
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		return VIEW_INCOME_TAX_WORKSHEET_PAGE;
	}

	public String editIncomeTaxWorksheet() {
		action = "EDIT";
		// log.info("getting into editIncomeTaxWorksheet Starteddddd " +
		// selectedEmpIncomeTaxWorksheet.getId());
		try {
			log.info("listsizeeeeeeeeee" + empIncomeTaxWorksheetIncomeList.size());

			if (selectedEmpIncomeTaxWorksheet == null) {
				Util.addWarn("Please Select atleast One");
				return null;
			}
			employeevisibility = true;
			lockscreen = true;
			employeecheck = true;

			String url = AppUtil.getPortalServerURL() + "/incometaxworksheet/getincometaxworksheet/"
					+ selectedEmpIncomeTaxWorksheet.getId();
			log.info("getEmployeeMasterByEntityId url==>" + url);
			BaseDTO relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
			empIncomeTaxWorksheetdto = mapper.readValue(jsonResponse, new TypeReference<EmpIncomeTaxWorksheetDTO>() {
			});
			if (empIncomeTaxWorksheetdto != null) {
				empIncomeTaxWorksheet = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheet();
				log.info("region name ==> " + empIncomeTaxWorksheetdto.getRegion().getName());
				selectedHeadRegionOfc = empIncomeTaxWorksheetdto.getRegion();
				selectedEntityTypeMaster = empIncomeTaxWorksheet.getEntityMaster().getEntityTypeMaster();
				selectedEntity = empIncomeTaxWorksheet.getEntityMaster();
				selectedEmployeeMaster = empIncomeTaxWorksheet.getEmpMaster();
				onSelectEntityType();
				loadEmployeeByEntity();
				loadEmpDetails(selectedEmployeeMaster.getId());
				empIncomeTaxWorksheetSavingsList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetSavingsList();
				empIncomeTaxWorksheetIncomeList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetIncomeList();
				empIncomeTaxWorksheetDeductionList = empIncomeTaxWorksheetdto.getEmpIncomeTaxWorksheetDeductionList();

				Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
						.filter(account -> account != null && account.getAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getAmount()));

				setTotaltentativeamount(totaltentamt);

				Double totaldeductionamount = empIncomeTaxWorksheetDeductionList.stream()
						.filter(account -> account != null && account.getAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getAmount()));

				setTotaldeduction(totaldeductionamount);

				/*
				 * Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
				 * .filter(account -> account != null && account.getAmount() != null)
				 * .collect(Collectors.summingDouble(o -> o.getAmount()));
				 * 
				 * setTotaltentativeamount(totaltentamt);
				 */
			} else {
				log.error("empIncomeTaxWorksheetdto null or empty");
			}
			log.info("editIncomeTaxWorksheet end ===>" + empIncomeTaxWorksheet);

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		return CREATE_INCOME_TAX_WORKSHEET_PAGE;
	}

	public void onRowSelect(SelectEvent event) {

		log.info("rowselecttttt");

		visibility = true;

	}

	public void loadIncomeTaxWorksheet() {

		lazyModel = new LazyDataModel<EmpIncomeTaxWorksheet>() {

			private static final long serialVersionUID = 1L;

			List<EmpIncomeTaxWorksheet> data = new ArrayList<>();

			@Override
			public List<EmpIncomeTaxWorksheet> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					mapper = new ObjectMapper();
					JobAdvertisement request = new JobAdvertisement();
					request.setFirst(first / pageSize);
					request.setPagesize(pageSize);
					request.setSortField(sortField);
					request.setSortOrder(sortOrder.toString());
					request.setFilters(filters);

					log.info("::EmpIncomeTaxWorksheet Search request data ::" + request);

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/incometaxworksheet/loadlazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					data = mapper.readValue(jsonResponse, new TypeReference<List<EmpIncomeTaxWorksheet>>() {
					});
					if (data == null) {
						log.info(" :: EmpIncomeTaxWorksheet Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: EmpIncomeTaxWorksheet Search totalRecords ::" + totalRecords);
						log.info(":: EmpIncomeTaxWorksheet Search Successfully Completed ::" + data.toString());
					} else {
						log.error(":: EmpIncomeTaxWorksheet Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					// return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading EmpIncomeTaxWorksheet data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return data;
			}

			@Override
			public Object getRowKey(EmpIncomeTaxWorksheet res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpIncomeTaxWorksheet getRowData(String rowKey) {
				for (EmpIncomeTaxWorksheet sheet : data) {
					if (sheet.getId().equals(Long.valueOf(rowKey))) {
						selectedEmpIncomeTaxWorksheet = sheet;
						return sheet;
					}
				}
				return null;
			}
		};
	}

	public String clearPage() {
		selectedEmpIncomeTaxWorksheet = new EmpIncomeTaxWorksheet();
		empIncomeTaxWorksheet = new EmpIncomeTaxWorksheet();
		empIncomeTaxWorksheetSavingsList = new ArrayList<>();
		empIncomeTaxWorksheetIncomeList = new ArrayList<>();
		empIncomeTaxWorksheetDeductionList = new ArrayList<>();
		visibility = false;
		loadIncomeTaxWorksheet();
		return LIST_INCOME_TAX_WORKSHEET_PAGE;
	}

	public void clearEmpAddnlEarningDeduction() {
		empIncomeTaxWorksheet = new EmpIncomeTaxWorksheet();
		// financialYearList = new ArrayList<>();
		selectedEmployeeMaster = new EmployeeMaster();
		employeeMasterList = new ArrayList<>();
		selectedEntity = new EntityMaster();
		entityList = new ArrayList<>();
		selectedEntityTypeMaster = new EntityTypeMaster();
		selectedHeadRegionOfc = new EntityMaster();
		setTotaltentativeamount(0.0);
		empIncomeTaxWorksheet.setActualIncomeTax(0.0);
		empIncomeTaxWorksheet.setTotalSavings(0.0);
		empIncomeTaxWorksheet.setTaxableIncome(0.0);
		setGlAccount(null);
		setSavingsamount(0.0);
		setDescription("");
		setTotaldeduction(0.0);
		setDeductionAmount(0.0);

		setDeductiondescription("");

		setIncomeamount(0.0);
		setIncomemonth(null);
		setIncomeyear(null);

		empIncomeTaxWorksheetIncomeList = new ArrayList<>();
		empIncomeTaxWorksheetSavingsList = new ArrayList<>();
		empIncomeTaxWorksheetDeductionList = new ArrayList<>();
		employeevisibility = false;
		visibility = false;

		empDTO = new EmpDTO();
		empDTO.setDesName(null);
		empDTO.setGender(null);
		empDTO.setPfNumber(null);
	}

	public void clearSavings() {
		description = null;
		savingsamount = null;
		glAccount = new GlAccount();
		// empIncomeTaxWorksheetSavingsList = new ArrayList<>();
		// empIncomeTaxWorksheet.setTaxableIncome(0.0);
		// empIncomeTaxWorksheet.setTotalSavings(0.0);
	}

	public void clearIncomeValues() {

		setIncomemonth(null);
		setIncomeyear(null);
		setIncomeamount(0.0);

		Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
				.filter(account -> account != null && account.getAmount() != null)
				.collect(Collectors.summingDouble(o -> o.getAmount()));
		log.info("checking" + totaltentamt);
		empIncomeTaxWorksheet.setIncomeSalary(empIncomeTaxWorksheet.getIncomeSalary() - totaltentamt);
		// setTotaltentativeamount(0.0);
		// empIncomeTaxWorksheetIncomeList = new ArrayList<>();

	}

	public void updateEntityTypeandEntity() {
		log.info("<=Starts updateEntityTypeandEntity =>");
		try {
			if (selectedHeadRegionOfc != null && selectedHeadRegionOfc.getName().equalsIgnoreCase("HEAD OFFICE")) {
				log.info("regionnnnnnnn" + selectedHeadRegionOfc);

				selectedEntityTypeMaster = null;
				selectedEntity = null;

				employeeMasterList = new ArrayList<>();
				employeeMasterList = commonDataService
						.getEmployeeMasterByEntityIdOrderByName(selectedHeadRegionOfc.getId());
				visibility = true;
			} else {
				visibility = false;
			}
		} catch (Exception exp) {
			log.error("found exception in updateEntityTypeandEntity: ", exp);
		}

	}

	public void addincome() {

		log.info("IncomeTaxWorkSheetBean. addincome() - START");

		empIncomeTaxWorksheetIncome = new EmpIncomeTaxWorksheetIncome();
		log.info("gettingintoincomeeeeee222q");
		Double actualincometax = 0.0;
		try {
			log.info("gettingintoincomeeeeeewdd222");
			log.info("yearrrr" + incomeyear);
			if (incomeyear == 0 || incomeyear == null) {
				AppUtil.addError("Year Should Not Be Empty");
				return;

			}
			log.info("monthhh" + incomemonth);
			if (incomemonth == 0 || incomemonth == null) {
				AppUtil.addError("Month Should Not Be Empty");
				return;

			}
			log.info("amountttt" + incomeamount);
			// if (incomeamount == 0.0 || incomeamount == null) {
			if (incomeamount == null) {
				AppUtil.addError("Amount Should Not Be Empty");
				return;

			}

			long yr1 = (long) getIncomeyear();
			String mnth1 = AppUtil.getMonthName(getIncomemonth());
			if (empIncomeTaxWorksheetIncomeList.stream().anyMatch(
					e -> e.getYear() == yr1 && e.getMonth().equals(mnth1) && e.getAmount().equals(getIncomeamount()))) {

				AppUtil.addError("Duplicate Amount For Same Year and Month ");
				return;
			}

			int i = empIncomeTaxWorksheetIncomeList.size();

			String mnth = String.valueOf(getIncomemonth());
			long yr = (long) getIncomeyear();

			empIncomeTaxWorksheetIncome.setId((long) (i + 1));

			empIncomeTaxWorksheetIncome.setMonth(AppUtil.getMonthName(getIncomemonth()));
			empIncomeTaxWorksheetIncome.setYear(yr);
			empIncomeTaxWorksheetIncome.setAmount(getIncomeamount());

			// totaltentativeamount= totaltentativeamount+ getIncomeamount();

			// log.info("tentativeamounttt"+ totaltentativeamount );
			empIncomeTaxWorksheetIncomeList.add(empIncomeTaxWorksheetIncome);

			/*
			 * Double total=empIncomeTaxWorksheetIncomeList.stream() .filter(account
			 * ->account!=null&& account.getAmount()!=null).mapToDouble(Account::
			 * getIncomeamount()) .sum();
			 */

			/*
			 * totaltentativeamount=empIncomeTaxWorksheetIncomeList.stream() .filter(account
			 * ->account!=null&&
			 * account.getAmount()!=null).collect(Collectors.summingDouble(o->
			 * o.getAmount()));
			 */

			// Double listAmt=

			Double value = empIncomeTaxWorksheet.getIncomeSalary() + getIncomeamount();
			empIncomeTaxWorksheet.setIncomeSalary(value);

			setTotaltentativeamount(totaltentativeamount + getIncomeamount());

			incomemonth = 0;
			incomeamount = 0.0;
			incomeyear = 0;

			log.info("gettingintoincomeesizeeeeeeee" + empIncomeTaxWorksheetIncomeList.size());
			Double totalten = totaltentativeamount + getIncomeamount();
			log.info("gettingintototaltennnn" + totalten);
			setTotaltentativeamount(totalten);

		}

		catch (Exception e) {
			log.info("Exception income WOrksheet==========>>>>" + e);
		}

	}

	public void removeincome(EmpIncomeTaxWorksheetIncome income) {
		log.info("::::::::11111");

		empIncomeTaxWorksheetIncomeList.remove(income);

		Double value = empIncomeTaxWorksheet.getIncomeSalary() - income.getAmount();
		empIncomeTaxWorksheet.setIncomeSalary(value);

		setTotaltentativeamount(totaltentativeamount - income.getAmount());

		log.info("::::::::2222");
	}

	public void removesavings(EmpIncomeTaxWorksheetSavings savings) {
		log.info("::::::::removesavings");

		empIncomeTaxWorksheetSavingsList.remove(savings);

		// totalsavingsamount=totalsavingsamount - savings.getAmount();

		log.info("grosss " + empIncomeTaxWorksheet.getTotalGrossIncome());
		log.info("savingssss" + empIncomeTaxWorksheet.getTotalSavings());
		log.info("taxincomeeee" + empIncomeTaxWorksheet.getTaxableIncome());
		log.info("removeeeamountttt" + savings.getAmount());

		Double totsav = empIncomeTaxWorksheetSavingsList.stream()
				.filter(account -> account != null && account.getAmount() != null)
				.collect(Collectors.summingDouble(o -> o.getAmount()));

		log.info("sumofsavingamounttttttt" + totsav);

		if (totsav >= slabDTO.getSlab_constant_total_saving()) {

			empIncomeTaxWorksheet.setTotalSavings(slabDTO.getSlab_constant_total_saving());

			log.info("ttttot" + empIncomeTaxWorksheet.getTotalSavings());
		}

		else {

			Long totalsavinground = Math.round(empIncomeTaxWorksheet.getTotalSavings() - savings.getAmount());

			empIncomeTaxWorksheet.setTotalSavings(totsav);

			double totalgrossAmount = empIncomeTaxWorksheet.getTotalGrossIncome() == null ? 0.0
					: empIncomeTaxWorksheet.getTotalGrossIncome();

			Long removeamount = Math.round(totalsavinground.doubleValue());

			log.info("savingggg" + removeamount);

			Long removededuction = Math.round(getTotaldeduction().doubleValue());
			log.info("deductionnn" + removededuction);
			empIncomeTaxWorksheet.setTaxableIncome(doubleFormat(totalgrossAmount) - (removeamount + removededuction));

		}

		if (empIncomeTaxWorksheetSavingsList.size() == 0) {

			Long totalgrossround = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());

			empIncomeTaxWorksheet.setTaxableIncome(totalgrossround.doubleValue());
			empIncomeTaxWorksheet.setTotalSavings(0.0);
		}

		calculateActualIncomeTax();

		log.info(":::: ::::removesavingsmethod" + totalsavingsamount);
	}

	public void removesavingsvalue(EmpIncomeTaxWorksheetSavings savings) {
		log.info("::::::::removesavingsvalue");
		empIncomeTaxWorksheetSavingsList.remove(savings);
		log.info("grosss " + empIncomeTaxWorksheet.getTotalGrossIncome());
		log.info("savingssss" + empIncomeTaxWorksheet.getTotalSavings());
		log.info("taxincomeeee" + empIncomeTaxWorksheet.getTaxableIncome());
		log.info("removeeeamountttt" + savings.getAmount());

		Double totsav = empIncomeTaxWorksheetSavingsList.stream()
				.filter(account -> account != null && account.getAmount() != null)
				.collect(Collectors.summingDouble(o -> o.getAmount()));
		log.info("sumofsavingamounttttttt" + totsav);
		if (totsav >= slabDTO.getSlab_constant_total_saving()) {
			log.info("ttttot" + empIncomeTaxWorksheet.getTotalSavings());
		} else {
			Long totalsavinground = Math.round(empIncomeTaxWorksheet.getTotalSavings() - savings.getAmount());
			empIncomeTaxWorksheet.setTotalSavings(totsav);
			double totalgrossAmount = empIncomeTaxWorksheet.getTotalGrossIncome() == null ? 0.0
					: empIncomeTaxWorksheet.getTotalGrossIncome();
			Long removededuction = Math.round(getTotaldeduction().doubleValue());
			Double taxincome = totalgrossAmount - (removededuction + empIncomeTaxWorksheet.getTotalSavings());
			Long removeamount = Math.round(totalsavinground.doubleValue());
			log.info("savingggg" + removeamount);
			log.info("deductionnn" + removededuction);
			empIncomeTaxWorksheet.setTaxableIncome(taxincome);
		}

		if (empIncomeTaxWorksheetSavingsList.size() == 0) {
			Long totalgrossround = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());
			empIncomeTaxWorksheet.setTaxableIncome(totalgrossround.doubleValue());
			empIncomeTaxWorksheet.setTotalSavings(0.0);
		}
		calculateActualIncomeTax();
		log.info(":::: ::::removesavingsmethod" + totalsavingsamount);
	}

	public void updateamount() {
		// empIncomeTaxWorksheet.getTaxDeductedSalary()==null?0.0

		if (empIncomeTaxWorksheet.getIncomeSalary() == null) {
			empIncomeTaxWorksheet.setIncomeSalary(0.0);
		}
		if (empIncomeTaxWorksheet.getEplSalary() == null) {
			empIncomeTaxWorksheet.setEplSalary(0.0);
		}
		if (empIncomeTaxWorksheet.getBonusSpecialSalesCommission() == null) {
			empIncomeTaxWorksheet.setBonusSpecialSalesCommission(0.0);
		}
		if (empIncomeTaxWorksheet.getIncentive() == null) {
			empIncomeTaxWorksheet.setIncentive(0.0);
		}
		if (empIncomeTaxWorksheet.getSalesCommission() == null) {
			empIncomeTaxWorksheet.setSalesCommission(0.0);
		}
		if (empIncomeTaxWorksheet.getEplSalaryArrears() == null) {
			empIncomeTaxWorksheet.setEplSalaryArrears(0.0);
		}
		if (empIncomeTaxWorksheet.getOthers() == null) {
			empIncomeTaxWorksheet.setOthers(0.0);
		}

		Double totalincome = empIncomeTaxWorksheet.getIncomeSalary() + empIncomeTaxWorksheet.getEplSalary()
				+ empIncomeTaxWorksheet.getSalesCommission() + empIncomeTaxWorksheet.getIncentive()
				+ empIncomeTaxWorksheet.getBonusSpecialSalesCommission() + empIncomeTaxWorksheet.getOthers()
				+ empIncomeTaxWorksheet.getEplSalaryArrears();

		log.info("grosssssssroundddbbbbbefore" + totalincome);
		Long totalgrossround = Math.round(totalincome);

		log.info("grosssssssroundddafter" + totalgrossround);

		empIncomeTaxWorksheet.setTotalGrossIncome(totalgrossround.doubleValue());

		// empIncomeTaxWorksheet.setTotalGrossIncome(totalincome);

		Long totalsavinground = Math.round(empIncomeTaxWorksheet.getTotalSavings());

		Double totdeduct = getTotaldeduction();

		if (totdeduct == null) {

			setTotaldeduction(0.0);
		}
		Long totaldeduction = Math.round(totdeduct);

		empIncomeTaxWorksheet.setTotalSavings(totalsavinground.doubleValue());

		empIncomeTaxWorksheet
				.setTaxableIncome(totalgrossround.doubleValue() - (totalsavinground.doubleValue() + totaldeduction));

		calculateActualIncomeTax();

	}

	public String submitsaveworksheet() {
		log.info("SaveemployeeWorkSheet");
		try {
			if (empIncomeTaxWorksheet != null) {

				log.info("emppppid" + selectedEmployeeMaster.getId());
				log.info("workkkk" + empIncomeTaxWorksheet.getId());

				log.info("yearrr" + empIncomeTaxWorksheet.getFinYear().getId());
				log.info("Status : " + checkEmpFinancialYearStatus(selectedEmployeeMaster.getId(),
						empIncomeTaxWorksheet.getFinYear().getId()));
				log.info("emmmpppvisibilityyyyy" + employeevisibility);
				if (employeecheck == false) {
					if (checkEmpFinancialYearStatus(selectedEmployeeMaster.getId(),
							empIncomeTaxWorksheet.getFinYear().getId())) {
						AppUtil.addError("Employee Already Exists For This Year");
						return CREATE_INCOME_TAX_WORKSHEET_PAGE;
					}
				}
				log.info("Saawsdve employee WorkSheet1");
				if (selectedHeadRegionOfc.getId() == null) {
					Util.addWarn("Head Office Should Not be Null");

					return CREATE_INCOME_TAX_WORKSHEET_PAGE;

				}
				log.info("Save employee WorkSheet1");
				if (selectedEmployeeMaster.getId() == null) {

					Util.addWarn("Employee Should Not be Null");

					return CREATE_INCOME_TAX_WORKSHEET_PAGE;

				}
				log.info("Save employee WorkSheet2");
				if (selectedEntity != null) {
					empIncomeTaxWorksheet.setEntityMaster(selectedEntity);
				}
				log.info("Save employee WorkSheet3");
				if (selectedEntity == null) {
					empIncomeTaxWorksheet.setEntityMaster(selectedHeadRegionOfc);
				}
				log.info("Save employee WorkSheet4");
				if (empIncomeTaxWorksheetIncomeList.size() == 0) {
					Util.addWarn("Income Should Not Be Empty");

					return CREATE_INCOME_TAX_WORKSHEET_PAGE;

				}
				log.info("Save employee WorkSheet5");

				if (empIncomeTaxWorksheetDeductionList.size() == 0) {
					Util.addWarn("80D Should Not Be Empty");
					return CREATE_INCOME_TAX_WORKSHEET_PAGE;
				}
				if (empIncomeTaxWorksheetSavingsList.size() == 0) {
					Util.addWarn("80C Should Not Be Empty");
					return CREATE_INCOME_TAX_WORKSHEET_PAGE;
				}

				else {
					calculateActualIncomeTax();
					log.info("Save employee WorkSheet6");
					empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheet(empIncomeTaxWorksheet);
					empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheetSavingsList(empIncomeTaxWorksheetSavingsList);
					empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheetIncomeList(empIncomeTaxWorksheetIncomeList);
					empIncomeTaxWorksheetdto.setEmpIncomeTaxWorksheetDeductionList(empIncomeTaxWorksheetDeductionList);

					log.info("Selected Employee  Id : : " + selectedEmployeeMaster.getId());

					String url = SERVER_URL + "/incometaxworksheet/saveworksheet";
					log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
					BaseDTO baseDTO = httpService.post(url, empIncomeTaxWorksheetdto);
					if (baseDTO != null) {
						mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						empIncomeTaxWorksheet = mapper.readValue(jsonResponse,
								new TypeReference<EmpIncomeTaxWorksheet>() {
								});
						errorMap.notify(baseDTO.getStatusCode());
					} else {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}
				visibility = false;

				selectedEmpIncomeTaxWorksheet = new EmpIncomeTaxWorksheet();
			}
		} catch (Exception e) {
			log.error("submit :: exception ==> ", e);
		}
		loadIncomeTaxWorksheet();
		return LIST_INCOME_TAX_WORKSHEET_PAGE;
	}

	public boolean checkEmpFinancialYearStatus(Long empId, Long finYrId) {
		boolean checkEmpFinStatus = false;
		try {
			String url = SERVER_URL + "/incometaxworksheet/checkEmpFinancialYearStatus/" + empId + "/" + finYrId;
			log.info("=====check===== : " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getTotalRecords() == 0) {
					checkEmpFinStatus = false;
				} else {
					checkEmpFinStatus = true;
				}
			}
		} catch (Exception e) {
			log.error(" Exception in checkEmpFinancialYearStatus Method : " + e);
		}
		return checkEmpFinStatus;
	}

	public String fulltentativeincome() {
		Double actualincometax = 0.0;
		if (empIncomeTaxWorksheetIncomeList.size() == 0) {
			Util.addWarn("Tentative Income Should Not Be Empty");

		}

		// Double totaltentamt = empIncomeTaxWorksheetIncomeList.stream()
		// .filter(account -> account != null && account.getAmount() != null)
		// .collect(Collectors.summingDouble(o -> o.getAmount()));

		// setTotaltentativeamount(totaltentamt);
		// log.info("tentativeincomeeee" + totaltentamt);
		// log.info("tentativeincomeeee1" + totaltentativeamount);

		// Double incomesalary = totaltentativeamount +
		// empIncomeTaxWorksheet.getIncomeSalary();
		// empIncomeTaxWorksheet.setIncomeSalary(incomesalary);

		// Double totalincome = empIncomeTaxWorksheet.getIncomeSalary() +
		// empIncomeTaxWorksheet.getEplSalary()
		// + empIncomeTaxWorksheet.getSalesCommission() +
		// empIncomeTaxWorksheet.getIncentive()
		// + empIncomeTaxWorksheet.getBonusSpecialSalesCommission() +
		// empIncomeTaxWorksheet.getOthers()
		// + empIncomeTaxWorksheet.getEplSalaryArrears();
		//
		// empIncomeTaxWorksheet.setTotalGrossIncome(totalincome);
		//
		//
		//
		//
		//
		//
		//
		// empIncomeTaxWorksheet.setTaxableIncome(totalincome-empIncomeTaxWorksheet.getTotalSavings());

		Double totalincome = empIncomeTaxWorksheet.getIncomeSalary() + empIncomeTaxWorksheet.getEplSalary()
				+ empIncomeTaxWorksheet.getSalesCommission() + empIncomeTaxWorksheet.getIncentive()
				+ empIncomeTaxWorksheet.getBonusSpecialSalesCommission() + empIncomeTaxWorksheet.getOthers()
				+ empIncomeTaxWorksheet.getEplSalaryArrears();

		log.info("grosssssssroundddbbbbbefore" + totalincome);
		Long totalgrossround = Math.round(totalincome);

		log.info("grosssssssroundddafter" + totalgrossround);

		empIncomeTaxWorksheet.setTotalGrossIncome(totalgrossround.doubleValue());

		Long totalsavinground = Math.round(empIncomeTaxWorksheet.getTotalSavings());

		Long totaldeductions = Math.round(getTotaldeduction());

		empIncomeTaxWorksheet.setTotalSavings(totalsavinground.doubleValue());

		empIncomeTaxWorksheet
				.setTaxableIncome(totalgrossround.doubleValue() - (totalsavinground.doubleValue() + totaldeductions));

		calculateActualIncomeTax();

		return null;
	}

	public void loadslabValueswithItConstantValue() {
		log.info("<--- Inside loadslabValues() --->");
		try {
			String requestPath = AppUtil.getPortalServerURL()
					+ "/incometaxworksheet/getAllslabValuewithItConstantValue";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					slabDTO = mapper.readValue(jsonValue, new TypeReference<EmpIncomeTaxWorksheetDTO>() {
					});
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadslabValueswithItConstant() --->", exp);
		}
	}

	public void calculatetaxpaid() {

		// Long taxpayable = Math.round(empIncomeTaxWorksheet.getTaxPayable());

		// Long totaltaxpaid = Math.round(empIncomeTaxWorksheet.getTotalTaxPaid());

		// Long otherthanslary =
		// Math.round(empIncomeTaxWorksheet.getTaxOtherThanSalary());

		// log.info("taxpayable"+taxpayable) ;
		//
		// log.info("totaltaxpaidddd"+totaltaxpaid) ;

		empIncomeTaxWorksheet.setTotalTaxPaid(
				empIncomeTaxWorksheet.getTaxDeductedSalary() + empIncomeTaxWorksheet.getTaxOtherThanSalary());

		empIncomeTaxWorksheet
				.setBalanceTaxToBePaid(empIncomeTaxWorksheet.getTaxPayable() - empIncomeTaxWorksheet.getTotalTaxPaid());

		calculateActualIncomeTax();
	}

	public String loadlistpage() {

		visibility = false;

		loadIncomeTaxWorksheet();

		return LIST_INCOME_TAX_WORKSHEET_PAGE;
	}

	public String calculateActualIncomeTax() {

		Double actualincometax = 0.0;

		Long taxinc = Math.round(empIncomeTaxWorksheet.getTaxableIncome());

		log.info("taxincommeeee" + taxinc);

		Double taxincome = taxinc == null ? 0.0 : taxinc;

		log.info("tttttt" + taxincome);
		if (taxinc <= slabDTO.getSlab1_value()) {
			log.info("GETTING INTO 1 LOOP");

			empIncomeTaxWorksheet.setActualIncomeTax(0.0);
		} else if (taxinc > slabDTO.getSlab1_value() && taxinc <= slabDTO.getSlab2_value()) {
			log.info("GETTING INTO 2 LOOP");

			log.info("GETTING INTO 2 LOOP" + slabDTO.getSlab1_value() + "-" + slabDTO.getSlab1_percentage() + "-"
					+ slabDTO.getSlab1_it_constant());

			actualincometax = (((taxinc - slabDTO.getSlab1_value()) * slabDTO.getSlab1_percentage()))
					- slabDTO.getSlab1_it_constant();

			empIncomeTaxWorksheet.setActualIncomeTax(actualincometax);

			log.info("Actualllll Tax" + empIncomeTaxWorksheet.getActualIncomeTax());

			if (empIncomeTaxWorksheet.getActualIncomeTax() <= 0) {

				empIncomeTaxWorksheet.setActualIncomeTax(0.0);
				empIncomeTaxWorksheet.setTaxEducationalCess(0.0);
				empIncomeTaxWorksheet.setTaxPayable(0.0);

			}

		} else if (taxinc > slabDTO.getSlab2_value() && taxinc <= slabDTO.getSlab3_value()) {
			log.info("GETTING INTO 3 LOOP");

			actualincometax = (((taxinc - slabDTO.getSlab2_value()) * slabDTO.getSlab2_percentage()))
					+ slabDTO.getSlab2_it_constant();

			empIncomeTaxWorksheet.setActualIncomeTax(actualincometax);
			log.info("Actualllll Tax" + empIncomeTaxWorksheet.getActualIncomeTax());

			if (empIncomeTaxWorksheet.getActualIncomeTax() <= 0) {

				empIncomeTaxWorksheet.setActualIncomeTax(0.0);
				empIncomeTaxWorksheet.setTaxEducationalCess(0.0);
				empIncomeTaxWorksheet.setTaxPayable(0.0);

			}
			log.info("3loppppp" + empIncomeTaxWorksheet.getActualIncomeTax());
		}

		else if (taxinc > slabDTO.getSlab3_value()) {
			log.info("GETTING INTO 4 LOOP");
			actualincometax = (((taxinc - slabDTO.getSlab3_value()) * slabDTO.getSlab3_percentage()))
					+ slabDTO.getSlab3_it_constant();

			empIncomeTaxWorksheet.setActualIncomeTax(actualincometax);
			log.info("Actualllll Tax" + empIncomeTaxWorksheet.getActualIncomeTax());
			if (empIncomeTaxWorksheet.getActualIncomeTax() <= 0) {

				empIncomeTaxWorksheet.setActualIncomeTax(0.0);
				empIncomeTaxWorksheet.setTaxEducationalCess(0.0);
				empIncomeTaxWorksheet.setTaxPayable(0.0);

			}

		}

		Long actualtax = Math.round(empIncomeTaxWorksheet.getActualIncomeTax());
		Long educationcess4percent = Math.round(empIncomeTaxWorksheet.getTaxEducationalCess());

		Long taxpyable = actualtax + educationcess4percent;

		log.info("actualllllroundvalue" + actualtax);

		log.info("educationtaxvaluecess" + educationcess4percent);

		log.info("taxpayableee" + taxpyable);

		// Long educationcess4percent =
		// Math.round(empIncomeTaxWorksheet.getTaxEducationalCess());

		// log.info("educationnnnnnn" + educationcess4percent);

		// Long taxpayable = Math.round(empIncomeTaxWorksheet.getTaxPayable());

		// log.info("taxpayableeee" + taxpayable);
		// Long totaltaxpaid = Math.round( empIncomeTaxWorksheet.getTotalTaxPaid());

		// log.info("totaltaxpaid" + totaltaxpaid);

		empIncomeTaxWorksheet.setActualIncomeTax(actualtax.doubleValue());
		Long education = Math.round(actualtax.doubleValue() * slabDTO.getEdu_cess_tax_percentage());
		// empIncomeTaxWorksheet.setTaxEducationalCess(
		// actualtax.doubleValue() * slabDTO.getEdu_cess_tax_percentage());

		empIncomeTaxWorksheet.setTaxEducationalCess(education.doubleValue());

		log.info("educationcesssss" + empIncomeTaxWorksheet.getTaxEducationalCess().longValue());

		log.info("taxxxeducation" + empIncomeTaxWorksheet.getTaxEducationalCess());

		empIncomeTaxWorksheet.setTaxPayable(actualtax.doubleValue() + education.doubleValue());

		log.info("taxxxpayable   " + empIncomeTaxWorksheet.getTaxPayable());

		empIncomeTaxWorksheet.setTotalTaxPaid((empIncomeTaxWorksheet.getTaxDeductedSalary() == null ? 0.0
				: empIncomeTaxWorksheet.getTaxDeductedSalary())
				+ (empIncomeTaxWorksheet.getTaxOtherThanSalary() == null ? 0.0
						: empIncomeTaxWorksheet.getTaxOtherThanSalary()));

		Long totaltaxpaid = Math.round(empIncomeTaxWorksheet.getTotalTaxPaid());

		log.info("taxpayableenew" + taxpyable.doubleValue());

		log.info("totaltaxpaidnewww" + totaltaxpaid.doubleValue());

		empIncomeTaxWorksheet.setBalanceTaxToBePaid(empIncomeTaxWorksheet.getTaxPayable() - totaltaxpaid.doubleValue());

		log.info("balanceeee" + empIncomeTaxWorksheet.getBalanceTaxToBePaid());

		return "";
	}

	final public double doubleFormat(double demandAmt) {
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#.##", symbols);
		// df.setMaximumFractionDigits(2);
		return Double.valueOf(df.format(demandAmt));
	}

	public void changinamount() {

		log.info("changingamountttt");

		if (savingsamount == null) {

			setSavingsamount(0.0);
		}
	}

	public void addDeduction() {
		log.info("deductionssss");
		empIncomeTaxWorksheetSavings = new EmpIncomeTaxWorksheetSavings();

		try {

			if (glAccountDeduction == null) {
				AppUtil.addError("Tax filling Items Should Not Be Empty");

				return;
			}

			if (deductionAmount == null || deductionAmount == 0) {
				AppUtil.addError("Amount Should Not Be Empty");

				return;
			}

			if (empIncomeTaxWorksheetDeductionList.stream().anyMatch(
					e -> e.getGlAccount().getId() == (glAccountDeduction != null ? glAccountDeduction.getId() : 0)))

			{

				AppUtil.addError("Tax filling Items Already Exist");
				return;
			}

			if (glAccountDeduction != null) {
				empIncomeTaxWorksheetSavings.setGlAccount(glAccountDeduction);
				empIncomeTaxWorksheetSavings.setAmount(deductionAmount);
				empIncomeTaxWorksheetSavings.setDescription(deductiondescription);
				empIncomeTaxWorksheetDeductionList.add(empIncomeTaxWorksheetSavings);
			}

			Double tototaldeductions = empIncomeTaxWorksheetDeductionList.stream()
					.filter(account -> account != null && account.getAmount() != null)
					.collect(Collectors.summingDouble(o -> o.getAmount()));
			log.info("tototaldeductions" + tototaldeductions);
			setTotaldeduction(tototaldeductions);

			Long totalgrossround = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());
			log.info("gggg" + totalgrossround);

			Long totalsaving = Math.round(empIncomeTaxWorksheet.getTotalSavings());

			// Long totaldeductionround =
			// Math.round(empIncomeTaxWorksheet.getTaxableIncome() + deductionAmount);

			log.info("savvv" + totalsaving);

			empIncomeTaxWorksheet
					.setTaxableIncome(totalgrossround.doubleValue() - (totalsaving.doubleValue() + tototaldeductions));

			glAccountDeduction = null;
			deductionAmount = null;
			deductiondescription = null;

		}

		catch (Exception e) {
			log.info("Exeception in Calculate WOrksheet==========>>>>" + e);
		}

	}

	public void removeDeductions(EmpIncomeTaxWorksheetSavings deduction) {
		log.info("::::::::removeDeductions");

		empIncomeTaxWorksheetDeductionList.remove(deduction);

		log.info("grosss " + empIncomeTaxWorksheet.getTotalGrossIncome());
		log.info("savingssss" + empIncomeTaxWorksheet.getTotalSavings());
		log.info("taxincomeeee" + empIncomeTaxWorksheet.getTaxableIncome());
		log.info("removeeeamountttt" + deduction.getAmount());

		Long totaldeductionround = Math.round(getTotaldeduction() - deduction.getAmount());

		setTotaldeduction(totaldeductionround.doubleValue());

		Long totalsavings = Math.round(empIncomeTaxWorksheet.getTotalSavings());
		Long totalgross = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());

		empIncomeTaxWorksheet.setTaxableIncome(totalgross.doubleValue() - (totaldeductionround + totalsavings));
		// empIncomeTaxWorksheet.setTaxableIncome(taxableIncome);

		// empIncomeTaxWorksheet.setTotalSavings(totalsavinground.doubleValue());

		// double
		// totalgrossAmount=empIncomeTaxWorksheet.getTotalGrossIncome()==null?0.0:empIncomeTaxWorksheet.getTotalGrossIncome();

		// Long removeamount = Math.round(totalsavinground.doubleValue());

		// empIncomeTaxWorksheet.setTaxableIncome(doubleFormat(totalgrossAmount) -
		// removeamount);

		// if (empIncomeTaxWorksheetSavingsList.size() == 0) {

		// Long totalgrossround =
		// Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());
		//
		// empIncomeTaxWorksheet.setTaxableIncome(totalgrossround.doubleValue());
		// empIncomeTaxWorksheet.setTotalSavings(0.0);
		// }

		// calculateActualIncomeTax();

		log.info(":::: ::::removedeductionsmethod" + totaldeduction);
	}

	public void removeDeductionsvalue(EmpIncomeTaxWorksheetSavings deduction) {
		log.info("::::::::removeDeductions");
		empIncomeTaxWorksheetDeductionList.remove(deduction);
		log.info("grosss " + empIncomeTaxWorksheet.getTotalGrossIncome());
		log.info("savingssss" + empIncomeTaxWorksheet.getTotalSavings());
		log.info("taxincomeeee" + empIncomeTaxWorksheet.getTaxableIncome());
		log.info("removeeeamountttt" + deduction.getAmount());
		Double tototaldeductions = empIncomeTaxWorksheetDeductionList.stream()
				.filter(account -> account != null && account.getAmount() != null)
				.collect(Collectors.summingDouble(o -> o.getAmount()));
		Long totaldeductionround = Math.round(tototaldeductions);
		setTotaldeduction(totaldeductionround.doubleValue());
		// A-(B+C)
		Long totalgross = Math.round(empIncomeTaxWorksheet.getTotalGrossIncome());
		Long totalsavings = Math.round(empIncomeTaxWorksheet.getTotalSavings());
		Double taxableIncome = totalgross.doubleValue()
				- (totaldeductionround.doubleValue() + totalsavings.doubleValue());
		empIncomeTaxWorksheet.setTaxableIncome(taxableIncome);
		calculateActualIncomeTax();
		log.info(":::: ::::removedeductionsmethod" + totaldeduction);
	}

	public void clearDeductions() {
		description = null;
		deductionAmount = null;
		glAccountDeduction = new GlAccount();

	}

}
