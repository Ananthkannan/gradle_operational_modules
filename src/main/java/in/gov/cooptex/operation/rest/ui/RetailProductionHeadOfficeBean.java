package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.Region;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.operation.dto.RegionWiseSalesData;
import in.gov.cooptex.operation.dto.RoPlanDetailsDto;
import in.gov.cooptex.operation.dto.ShowroomPlanDetailsDto;
import in.gov.cooptex.operation.enums.RetailProductionPlanStage;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.production.dto.ProductWiseProductionPlanDto;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanDetails;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailProductionHeadOfficeBean")
@Scope("session")
public class RetailProductionHeadOfficeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String REGIONALOFFICE_LIST_URL = "/pages/operation/retailproduction/ROProductionPlanList.xhtml?faces-redirect=true;";

	private final String REGIONALOFFICE_SHOWROOM_LIST_URL = "/pages/operation/retailproduction/listInitialProductionPlanRo.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_LIST_FINAL_URL = "/pages/operation/retailproduction/listRetailSalesProductionPlanFinal.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_LIST_FINAL_VIEW_URL = "/pages/operation/retailproduction/viewHOProductionPlan.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	LazyDataModel<RetailProductionPlanResponse> retailProductionPlanResponseLazyList;

	@Getter
	@Setter
	LazyDataModel<RoPlanDetailsDto> showroomPlanDetailsDtoList;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;
	@Getter
	@Setter
	RoPlanDetailsDto roPlanDetailsDtoResponse;

	@Getter
	@Setter
	Integer resultSize;

	@Getter
	@Setter
	String serverURL;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	RoPlanDetailsDto roPlanDetailsDto;

	@Getter
	@Setter
	RoPlanDetailsDto roPlanDetailsDtoFilter;

	@Getter
	@Setter
	Map<String, Map<String, ProductWiseProductionPlanDto>> regionWiseMap;

	@Getter
	@Setter
	Map<String, List<ProductWiseProductionPlanDto>> collect;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Date stockAsOnDate;
	
	@Getter
	@Setter
	Map<String, Double> totalCategoryDraftValueMap,totalGroupDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalCategoryShowroomValueMap,totalGroupShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalCategoryRoValueMap,totalGroupRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalCategoryFinalValueMap,totalGroupFinalValueMap;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> productWiseProductionPlanDtoList;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Region region;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	ShowroomPlanDetailsDto selectedShowroomPlanDetailsDto;

	@Getter
	@Setter
	RetailProductionPlanDetails retailProductionPlanDetails;

	@Getter
	@Setter
	ShowroomPlanDetailsDto productWisePlanDetailsDTO;
	
	@Getter
	@Setter
	ShowroomPlanDetailsDto categoryWisePlanDetailsDTO;
	
	@Getter
	@Setter
	ShowroomPlanDetailsDto groupWisePlanDetailsDTO;

	@Getter
	@Setter
	RegionWiseSalesData regionWiseSalesData;

	@Getter
	@Setter
	RetailProductionPlanRegion retailProductionPlanRegion;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> selectedProductWiseProductionPlanDtoList;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> regionWiseProductDetails;
	
	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> regionWiseCategoryDetails;
	
	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> regionWiseGroupDetails;

	@Getter
	@Setter
	Integer roPercentage;

	@Getter
	@Setter
	Integer totalCountRegion;

	@Getter
	@Setter
	Integer notSubmittedShowrromCount;

	@Autowired
	RetailProductionPlanLogBean retailProductionPlanLogBean;

	@Getter
	@Setter
	private String currentPlanStatus = "INITIATED";

	@Getter
	@Setter
	String commentsReject;

	@Getter
	@Setter
	String commentsApprove;

	@Getter
	@Setter
	BaseDTO baseDTO;

	@Getter
	@Setter
	Integer submittedRegionCount;

	@Getter
	@Setter
	Map<String, Double> totalDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMap;

	@Getter
	@Setter
	String searchText;

	@Getter
	@Setter
	boolean approveOrRejectEnable;

	@Getter
	@Setter
	Map<String, Double> totalProductDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductFinalValueMap;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	String stage = null;

	@Autowired
	RetailProductionRegionalOfficeBean retailProductionRegionalOfficeBean;

	@Getter
	@Setter
	StringBuilder regionValues = new StringBuilder();

	@Getter
	@Setter
	Map<String, Double> totalShowroomDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomProductValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomFinalValueMap;

	@Getter
	@Setter
	List statusAndStageValues;

	@Getter
	@Setter
	StringBuilder productCategoryValues = new StringBuilder();

	public RetailProductionHeadOfficeBean() {
		log.info("<#======Inside RetailProductionHeadOfficeBean Bean======#>");
		loadServerURL();
	}

	private void loadStatusValues() {
		Object[] statusArray = RetailProductionPlanStatus.class.getEnumConstants();
		Object[] stageArray = RetailProductionPlanStage.class.getEnumConstants();

		statusAndStageValues = new ArrayList<>();

		for (Object stage : stageArray) {
			for (Object status : statusArray) {
				statusAndStageValues.add(stage + "-" + status);
			}
		}

		log.info("statusAndStageValues " + statusAndStageValues);
	}

	private void loadServerURL() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public String getRegionalOfficePlanList() {

		log.info(" inside getRegionalOfficePlanList");

		loadStatusValues();
		loadServerURL();
		loadLazyData();

		return REGIONALOFFICE_LIST_URL;
	}

	private void loadLazyData() {
		log.info(" Inside LoadLazyData");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				log.info("load is called.");
				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse,
							new TypeReference<List<RetailProductionPlanResponse>>() {
							});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse response) {
				log.info("getRowKey is called.");
				return response != null ? response.getPlanId() : null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RetailProductionPlanResponse response : responseList) {
					if (response.getPlanId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	public boolean filterMapDetails() {

		collect = roPlanDetailsDtoFilter.getRoWisePlanDetailsMap().entrySet().stream()
				.filter(map -> map.getKey().contains(searchText))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

		roPlanDetailsDto.setRoWisePlanDetailsMap(collect);

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : roPlanDetailsDto.getRoWisePlanDetailsMap()
				.entrySet()) {
			productWiseProductionPlanDtoList = entry.getValue();
		}
		return true;
	}

	public BaseDTO getSearchDataFinal(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();
		log.info("<---Inside search called--->");
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		retailProductionPlanResponse = new RetailProductionPlanResponse();

		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("codeOrName")) {
				log.info("codeOrName : " + value);
				request.setCodeOrName(value);
			}
			if (entry.getKey().equals("planFrom")) {
				log.info("planFromSearch : " + value);
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("planTo")) {
				log.info("planToSearch : " + value);
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("stageOrStatus")) {
				log.info("selectedStatus : " + value);
				request.setStageOrStatus(value);
			}
		}
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	public String viewFinalPlan() {
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			log.info("retailProductionPlanResponse : " + retailProductionPlanResponse);
			errorMap.notify(8525);
			return null;
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getfinalplandetails/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
		} catch (Exception e) {
			log.error("Error in viewPlan", e);
			errorMap.notify(1);
			return null;
		}

		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				retailProductionPlan = mapper.readValue(jsonValue, RetailProductionPlan.class);

				regionValues.setLength(0);
				log.info("<--- retailProductionPlan.getRetailProductionPlanRegionList() ---> "
						+ retailProductionPlan.getRetailProductionPlanRegionList());
				for (RetailProductionPlanRegion regionValue : retailProductionPlan
						.getRetailProductionPlanRegionList()) {
					if (regionValue != null && regionValue.getEntityMaster().getCode() != null
							&& regionValue.getEntityMaster().getName() != null)
						regionValues.append(regionValue.getEntityMaster().getCode()).append("/")
								.append(regionValue.getEntityMaster().getName()).append(",<br/>");
				}
				regionValues.deleteCharAt(regionValues.length() - 6);
				productCategoryValues.setLength(0);
				log.info("<--- retailProductionPlan.getProductCategoryList() ---> "
						+ retailProductionPlan.getProductCategoryList());
				for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
					if (categoryValue != null && categoryValue.getProductCatCode() != null
							&& categoryValue.getProductCatName() != null)
						productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
								.append(categoryValue.getProductCatName()).append(",<br/>");
				}

				productCategoryValues.deleteCharAt(productCategoryValues.length() - 6);
			} catch (IOException e) {
				log.error(e);
			}
		}

		if (retailProductionPlan == null) {
			log.info("#==--> RetailProductionPlan is Not Available <--==#");
			errorMap.notify(9612);
			return null;
		}

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);
		searchText = null;
		//getRegionalOfficePlanDetails();
		getregionPlanDetails();
		getRegionalDetails();
		getROCountDetails();
		return HEAD_OFFICE_LIST_FINAL_VIEW_URL;
	}

	public String getHeadOfficePlanList() {

		retailProductionRegionalOfficeBean.setOriginatedFrom("FINAL");

		retailProductionPlanResponse = new RetailProductionPlanResponse();

		loadStatusValues();
		loadLazyDraftProductionListFinal();

		return HEAD_OFFICE_LIST_FINAL_URL;

	}

	public void loadLazyDraftProductionListFinal() {

		log.info("starts loadLazyDraftProductionList====>");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchDataFinal(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetailProductionPlanResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse res) {
				return res != null ? res.getPlanId() : null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RetailProductionPlanResponse res : responseList) {
					if (res.getPlanId().longValue() == value.longValue()) {
						retailProductionPlanResponse = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		retailProductionPlanResponse = new RetailProductionPlanResponse();
		BaseDTO baseDTO = new BaseDTO();

		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			request.setId(retailProductionPlanResponse.getPlanId());
			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planTo")) {
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("receivedDate")) {
				request.setReceivedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dueDate")) {
				request.setDueDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("stageOrStatus")) {
				request.setStageOrStatus(value);
			}

			if (entry.getKey().equals("netEligibilityValue")) {
				request.setNetEligibilityValue(Double.parseDouble(value));
			}
		}

		try {

			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	/*
	 * 
	 * RO WISE Showroom deatils list
	 * 
	 */

	private BaseDTO getShowRoomSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		BaseDTO baseDTO = new BaseDTO();

		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			if (entry.getKey().equals("showRoomCodeOrName")) {
				request.setShowRoomCodeOrName(value);
			}

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planTo")) {
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("receivedDate")) {
				request.setReceivedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dueDate")) {
				request.setDueDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("contactNumber")) {
				request.setContactNumber(value);
			}

			if (entry.getKey().equals("stageOrStatus")) {
				request.setStageOrStatus(value);
			}

		}

		log.info("response obj -- " + retailProductionPlanResponse);

		request.setRegionId(retailProductionPlanResponse.getRegionId());

		log.info("request obj -- " + request);

		log.info("request obj retailProductionPlanResponse.getRegionId() -- "
				+ retailProductionPlanResponse.getRegionId());

		try {

			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/showroom/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	/*
	 * RO WISE Showroom deatils list
	 * 
	 * 
	 * 
	 */
	private void loadShowRoomLazyData() {
		log.info(" Inside loadShowRoomLazyData");
		showroomPlanDetailsDtoList = new LazyDataModel<RoPlanDetailsDto>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RoPlanDetailsDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				log.info("load is called.");
				List<RoPlanDetailsDto> data = new ArrayList<RoPlanDetailsDto>();
				try {
					BaseDTO baseDTO = getShowRoomSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse, new TypeReference<List<RoPlanDetailsDto>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RoPlanDetailsDto response) {
				log.info("getRowKey is called.");
				return response != null ? response.getPlanId() : null;
			}

			@Override
			public RoPlanDetailsDto getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RoPlanDetailsDto> responseList = (List<RoPlanDetailsDto>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RoPlanDetailsDto response : responseList) {
					if (response.getPlanId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	public String viewRegionalOffice() {
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}
		selectedShowroomPlanDetailsDto = new ShowroomPlanDetailsDto();
		getRegionalOfficePlanDetails();
		getRegionalDetails();
		getROCountDetails();
		getregionPlanDetails();
		return "/pages/operation/retailproduction/viewRoProductionPlan.xhtml?faces-redirect=true;\";";
	}

	public String cancelHOView() {
		return HEAD_OFFICE_LIST_FINAL_URL;
	}

	public String getRegionalOfficePlanDetails() {
		log.info("Inside get region method input userid >>>>>>>>>>>" + retailProductionPlanResponse);
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/get/"
					+ retailProductionPlanResponse.getPlanId();
			baseDTO = httpService.get(URL);
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			retailProductionPlan = objectMapper.readValue(jsonResponse, RetailProductionPlan.class);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return null;
	}

	public void getRegionalDetails() {

		log.info("Inside get region method input userid >>>>>>>>>>>");
		Long userId = loginBean.getUserDetailSession().getId();
		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/getRegion/" + userId;
		retailProductionPlanDetails = new RetailProductionPlanDetails();

		try {
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = null;
			URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/gethoplandetails/"
					+ retailProductionPlanResponse.getPlanId();
			baseDTO = httpService.get(URL);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			retailProductionPlanDetails = mapper.readValue(jsonResponse, RetailProductionPlanDetails.class);
			log.info("Inside get region method input userid >>>>>>>>>>>" + retailProductionPlanDetails);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		currentPlanStatus = value;
		commentsApprove = null;
		commentsReject = null;
	}

	public void getROCountDetails() {
		roPercentage = 0;
		approveOrRejectEnable = false;
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getrocount/"
					+ retailProductionPlanResponse.getPlanId();
			baseDTO = httpService.get(URL);
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			submittedRegionCount = objectMapper.readValue(jsonResponse, Integer.class);
			if (totalCountRegion != 0 && submittedRegionCount != null) {
				roPercentage = submittedRegionCount * 100 / totalCountRegion;
			}
			notSubmittedShowrromCount = totalCountRegion - submittedRegionCount;
			if (roPercentage != null && roPercentage.equals(100)) {
				approveOrRejectEnable = true;
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public void getPlan(Long regionId, Long planId) {

		log.info("Get Plan Method Called ==>");

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getplandetails/"
				+ regionId + "/" + planId;

		BaseDTO baseDTO = httpService.get(url);

		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				selectedShowroomPlanDetailsDto = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : selectedShowroomPlanDetailsDto
						.getProductWiseProductionPlanMap().entrySet()) {
					selectedProductWiseProductionPlanDtoList = entry.getValue();
				}
				log.info("showroomPlanDetailsDto == >>" + selectedShowroomPlanDetailsDto);
				calculateTotalShowroomValue();
				RequestContext.getCurrentInstance().update("retailPlan:myPanel");
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");

			} catch (IOException e) {
				log.error(e);
			}
		}

	}

	public void calculateTotalShowroomValue() {

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : selectedShowroomPlanDetailsDto
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalShowroomDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalShowroomProductValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalShowroomRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalShowroomFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));

	}

	public String clearROSearch() {
		retailProductionPlanResponse = new RetailProductionPlanResponse();
		return REGIONALOFFICE_LIST_URL;
	}

	public String clearROShowroomSearch() {
		roPlanDetailsDtoResponse = new RoPlanDetailsDto();
		return REGIONALOFFICE_SHOWROOM_LIST_URL;
	}

	public boolean filterByPrice(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null || filterText.equals("")) {
			return true;
		}

		if (value == null) {
			return false;
		}

		return ((Comparable) value).compareTo(filterText) > 0;
	}

	public void getProductWisePlanDetails() {

		log.info("Get getProductWisePlanDetails Called ==>");

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getproductdetails/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			try {
				
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				productWisePlanDetailsDTO = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : productWisePlanDetailsDTO
						.getProductWiseProductionPlanMap().entrySet()) {
					regionWiseProductDetails = entry.getValue();
				}
				calculateTotalProductValue();
				log.info("productWisePlanDetailsDTO == >>" + productWisePlanDetailsDTO);

			} catch (IOException e) {
				log.error(e);
			}
		}
		RequestContext.getCurrentInstance().execute("PF('dlg2').show();");

	}
	
	public void getCategoryWisePlanDetails() {

		log.info("Get getCategoryWisePlanDetails Called ==>");

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getcategory/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				categoryWisePlanDetailsDTO = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				if (categoryWisePlanDetailsDTO != null) {
					
					if (categoryWisePlanDetailsDTO.getProductWiseProductionPlanMap() != null) {
						for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : categoryWisePlanDetailsDTO
								.getProductWiseProductionPlanMap().entrySet()) {
							regionWiseCategoryDetails = entry.getValue();
						}
						calculateTotalCategoryValue();
					}
					
				}
				
				log.info("getCategoryWisePlanDetails == >>" + categoryWisePlanDetailsDTO);

			} catch (IOException e) {
				log.error(e);
			}
		}
		RequestContext.getCurrentInstance().execute("PF('categorywidget').show();");

	}
	
	
	public void getGroupWisePlanDetails() {

		log.info("Get getGroupWisePlanDetails Called ==>");

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getgroup/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				groupWisePlanDetailsDTO = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);
				if(groupWisePlanDetailsDTO != null) {
					
					for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : groupWisePlanDetailsDTO
							.getProductWiseProductionPlanMap().entrySet()) {
						regionWiseGroupDetails = entry.getValue();
					}
					calculateTotalGroupValue();
				}

				
				log.info("getGroupWisePlanDetails == >>" + groupWisePlanDetailsDTO);

			} catch (IOException e) {
				log.error("============>>>>>>Exception Occured In ===========>>>>",e);
			}
		}
		RequestContext.getCurrentInstance().execute("PF('groupwidget').show();");

	}
	
	public void calculateTotalGroupValue() {
		log.info("Get calculateTotalGroupValue Called ==>");
		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : groupWisePlanDetailsDTO
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}
		
		/* changed code because null pointer Exception , See below changed code

		totalGroupDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalGroupShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalGroupRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalGroupFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));
		*/
		
		if (totalListFromMap != null) {
			totalGroupDraftValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getDraftValue() == null ? 0D : x.getDraftValue())));
			totalGroupShowroomValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getShowroomValue() == null ? 0D : x.getShowroomValue())));
			totalGroupRoValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getRoValue() == null ? 0D : x.getRoValue())));
			totalGroupFinalValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getFinalValue() == null ? 0D : x.getFinalValue())));
		}
		log.info("END calculateTotalGroupValue  ==>");

	}

	
	public void calculateTotalCategoryValue() {
		log.info("Get calculateTotalGroupValue Called ==>");
		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : categoryWisePlanDetailsDTO
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		/* changed code because null pointer Exception , See below changed code
		totalCategoryDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalCategoryShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalCategoryRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalCategoryFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));
		*/
		
		if (totalListFromMap != null ) {
			totalCategoryDraftValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getDraftValue() == null ? 0D : x.getDraftValue())));
			totalCategoryShowroomValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getShowroomValue() == null ? 0D : x.getShowroomValue())));
			totalCategoryRoValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getRoValue() == null ? 0D : x.getRoValue())));
			totalCategoryFinalValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getFinalValue() == null ? 0D : x.getFinalValue())));
		}
		
		log.info("END calculateTotalGroupValue ==>");

	}



	public void calculateTotalProductValue() {

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : productWisePlanDetailsDTO
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		/* changed code because null pointer Exception , See below changed code
		totalProductDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalProductShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalProductRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalProductFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));
		*/
		if (totalListFromMap != null) {
			
			totalProductDraftValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getDraftValue() == null ? 0D : x.getDraftValue())));
			totalProductShowroomValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getShowroomValue() == null ? 0D : x.getShowroomValue())));
			totalProductRoValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getRoValue() == null ? 0D : x.getRoValue())));
			totalProductFinalValueMap = totalListFromMap.stream()
					.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
							Collectors.summingDouble(x -> x.getFinalValue() == null ? 0D : x.getFinalValue())));
		}
		

	}

	public void setSelectedColumn(Map.Entry<String, List<ProductWiseProductionPlanDto>> value) {
		log.info("Get Plan Method Called ==>" + value);
		String showroomCode = null;
		try {
			if (value != null && value.getKey() != null) {
				String[] keyList = value.getKey().split("/");
				showroomCode = keyList[0];
				String url = serverURL + "/entitymaster/getByCode/" + showroomCode;
				BaseDTO baseDTO = httpService.get(url);
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				EntityMaster entityMaster = mapper.readValue(jsonValue, EntityMaster.class);
				if (entityMaster != null) {
					getPlan(entityMaster.getId(), retailProductionPlanResponse.getPlanId());
				}
			}
		} catch (IOException e) {
			log.error(e);
		}

	}

	public String statusUpdate() {
		log.info("Status Updated is called");
		log.info(currentPlanStatus);
		log.info(retailProductionPlanResponse.getPlanId());
		String comments = "";

		if (currentPlanStatus.equals("APPROVED")) {
			comments = commentsApprove;
		} else {
			comments = commentsReject;
		}

		BaseDTO baseDTO = null;
		RetailProductionPlanRequest request = new RetailProductionPlanRequest(retailProductionPlanResponse.getPlanId(),
				currentPlanStatus, comments);

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/approvefinalplan";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/rejectfinalplan";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return HEAD_OFFICE_LIST_FINAL_URL;
	}

	public void getregionPlanDetails() {

		log.info("Get Plan Method Called ==>");

		Map<String, List<ProductWiseProductionPlanDto>> roWisePlanDetailsMap = new LinkedHashMap<>();
		totalCountRegion = 0;

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/getRegionsUnderPlan/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				roPlanDetailsDto = mapper.readValue(jsonValue, RoPlanDetailsDto.class);

				roPlanDetailsDtoFilter = mapper.readValue(jsonValue, RoPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : roPlanDetailsDto
						.getRoWisePlanDetailsMap().entrySet()) {
					productWiseProductionPlanDtoList = entry.getValue();
					totalCountRegion++;
				}
				calculateTotalValue();
				log.info("showroomPlanDetailsDto 	== >>" + roPlanDetailsDto);

			} catch (IOException e) {
				log.error(e);
			}
		}

	}

	public void calculateTotalValue() {

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : roPlanDetailsDto.getRoWisePlanDetailsMap()
				.entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));
	}

	public String getShowRoomList() {

		log.info(" inside getShowRoomList");

		loadStatusValues();
		loadServerURL();
		loadShowRoomLazyData();

		return REGIONALOFFICE_SHOWROOM_LIST_URL;
	}
}