package in.gov.cooptex.admin.filemovement.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.filemovement.model.FileMovement;
import in.gov.cooptex.admin.filemovement.model.FileMovementFiles;
import in.gov.cooptex.admin.filemovement.model.FileMovementLog;
import in.gov.cooptex.admin.filemovement.model.FileMovementNote;
import in.gov.cooptex.admin.tapal.model.IncomingTapal;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AddnlEarningDeductionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DashboardDTO;
import in.gov.cooptex.core.dto.FileMovementDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dashboard.ui.bean.DashboardBean;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("fileMovementBean")
@Scope("session")
public class FileMovementBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String CREATE_PAGE = "/pages/admin/createFileMovement.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/admin/listFileMovement.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/admin/viewFileMovement.xhtml?faces-redirect=true;";
	

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private FileMovement fileMovement = new FileMovement();
	@Getter
	@Setter
	List<IncomingTapal> referenceNumberList;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	@Getter
	@Setter
	private FileMovement selectedFileMovement;

	@Getter
	@Setter
	String movementNumber;

	@Getter
	@Setter
	Boolean approvalFlag = false, finalApprovalFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag1 = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	private String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String lastApprovalStatus;

	@Getter
	@Setter
	LazyDataModel<FileMovement> lazyFileMovementList;

	List<FileMovement> fileMovementList;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityMaster entity;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@Getter
	@Setter
	IncomingTapal incomingTapal;

	@Getter
	@Setter
	List<SectionMaster> sectionList;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Getter
	@Setter
	List<FileMovementFiles> fileMovementFilesList = new ArrayList<FileMovementFiles>();

	@Getter
	@Setter
	FileMovementFiles fileMovementFile = new FileMovementFiles();

	@Setter
	@Getter
	String fileName;

	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Getter
	@Setter
	FileMovementNote filemovementNote;

	@Getter
	@Setter
	FileMovementLog fileMovementLog = new FileMovementLog();

	@Setter
	@Getter
	String note;

	/*
	 * @Setter
	 * 
	 * @Getter UserMaster userMaster = new UserMaster();
	 */

	@Setter
	@Getter
	String content;

	@Setter
	@Getter
	String subject;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Boolean finalApproval = false;

	@Setter
	@Getter
	String approvedRemark;

	@Setter
	@Getter
	String rejectedRemark;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	List<FileMovementDTO> viewLogResponseList = new ArrayList<>();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Getter
	@Setter
	private String stage;

	@Getter
	@Setter
	FileMovementFiles selectedFile;

	@Autowired
	DashboardBean dashBoardBean;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	FileMovementFiles viewPageSeletedFile = new FileMovementFiles();

	@Getter
	@Setter
	AddnlEarningDeductionDTO addnlEarningDeductionDTO = new AddnlEarningDeductionDTO();

	public String showListPage() {

		log.info("<==== Starts FileMovement-showFileMovementListPage =====>");

		mapper = new ObjectMapper();

		fileMovement = new FileMovement();
		selectedFileMovement = new FileMovement();
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		departmentList = commonDataService.getDepartment();
		sectionList = commonDataService.getSectionMaster();
		stage = null;
		entityTypeList = commonDataService.getAllEntityType();
		loadLazyFileMovementList();
		getNextSequenceConfigValueForFileNumber();
		log.info("<==== Ends FileMovement-showFileMovementListPage =====>");
		addButtonFlag = true;
		return LIST_PAGE;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts FileMovement-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String fileMovementId = "";
		String stage = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			fileMovementId = httpRequest.getParameter("fileMovementId");
			stage = httpRequest.getParameter("stage");
			if (stage != null && (stage.equals("FINAL APPROVED") || stage.equals("REJECTED"))) {
				buttonFlag1 = false;
			}
			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		if (fileMovementId != null) {

			Long fileId = Long.parseLong(fileMovementId);
			selectedNotificationId = Long.parseLong(notificationId);
			selectedFileMovement = new FileMovement();
			selectedFileMovement.setId(fileId);
			FileMovementLog fileMovementLog = new FileMovementLog();
			fileMovementLog.setStage(stage);
			selectedFileMovement.setFileMovementLog(fileMovementLog);
			// selectedFileMovement.sets
			showViewPage();

		}
		log.info("<==== Ends FileMovement-showFileMovementListPage =====>");
		addButtonFlag = true;
		return VIEW_PAGE;
	}

	public String loadFileMovementDetails() {
		log.info("FileMovement:==> loadFileMovementDetails Method Called---------");
		try {
			action = "ADD";
			incomingTapal = new IncomingTapal();
			fileMovement = new FileMovement();
			entityMaster = new EntityMaster();
			entity = new EntityMaster();
			employeeMaster = new EmployeeMaster();
			department = new Department();
			entityMasterList = new ArrayList<EntityMaster>();
			departmentList = new ArrayList<Department>();
			sectionList = new ArrayList<SectionMaster>();
			entityTypeList = new ArrayList<EntityTypeMaster>();
			fileMovementFilesList = new ArrayList<FileMovementFiles>();
			employeeMasterList = new ArrayList<>();
			filemovementNote = new FileMovementNote();
			userMaster = new UserMaster();
			selectedFileMovement = new FileMovement();
			fileName = "";
			content = "";
			note = "";
			subject = "";
			// fileNumber = "";
			movementNumber = "";

			entityMasterList = commonDataService.loadHeadAndRegionalOffice();

			departmentList = commonDataService.getDepartment();
			// getAllEmployeeMaster();
			userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.FILE_MOVEMENT.toString());
			loadAllReferenceNumbers();

			getNextSequenceConfigValueForFileNumber();
			// loadForwardToUsers();

			createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();

		} catch (Exception exp) {
			log.info("FileMovement:==> Error in loadFileMovementDetails---------", exp);
		}
		return CREATE_PAGE;
	}

	public void headOfficeChange() {
		log.info("FileMovement:==> headOfficeChange Method Called---------");
		try {
			entityTypeList = new ArrayList<>();
			if (entityMaster != null) {
				if (!"Head Office Entity".equalsIgnoreCase(entityMaster.getName())) {
					// entityFlag=true;
					BaseDTO baseDTO = new BaseDTO();
					String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
							+ "/outgoingtapal/headOfficeChange/" + entityMaster.getName();
					baseDTO = httpService.get(url);
					if (baseDTO.getStatusCode() == 0) {
						mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
						});
						log.info("entityTypeList---------" + entityTypeList.size());
						
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} else {
					// entityFlag=false;
				}
			}
			entityTypeMaster=new EntityTypeMaster();
			entity=new EntityMaster();
			employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster,
					entityTypeMaster, entity, department, sectionMaster);

		} catch (Exception e) {
			log.info("inside headOfficeChange method--", e);
		}
	}

	public void entityTypeChange() {
		log.info("FileMovement:==> entityTypeChange Method Called---------");
		try {
			entityList = new ArrayList<>();
			if (entityTypeMaster != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/outgoingtapal/getEntityByTypeAndOffice/" + entityTypeMaster.getEntityName() + "/"
						+ entityMaster.getName();
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					log.info("entityList---------" + entityList.size());
					
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}
			entity=new EntityMaster();
			employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster,
					entityTypeMaster, entity, department, sectionMaster);
		} catch (Exception e) {
			log.info("inside entityTypeChange method--", e);
		}
	}

	public void departmentChange() {
		log.info("FileMovement:==> departmentChange Method Called---------");
		try {
			if (department != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/incomingtapal/getSectionByDesignation/" + department.getName();
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					sectionList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
					});
					log.info("List---------" + sectionList.size());
					
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}
			employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster,
					entityTypeMaster, entity, department, sectionMaster);
		} catch (Exception e) {
			log.info("inside departmentChange method--", e);
		}
	}

	public void loadEmployeeBySectionId() {
		employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster, entityTypeMaster,
				entity, department, sectionMaster);
	}

	public void sectionChange() {
		try {
			if (sectionMaster != null) {
				employeeMasterList = new ArrayList<EmployeeMaster>();
				log.info("sectionMaster------------" + sectionMaster.getId());
				log.info("entity id-----------------" + entity.getId());
				log.info("department id--------------" + department.getId());
				employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster,
						entityTypeMaster, entity, department, sectionMaster);
				log.info("employeeMasterList---------" + employeeMasterList.size());
				EmployeeMaster employeeMaster = commonDataService.loadEmployeeByUser(loginBean.getUserMaster().getId());
				employeeMasterList.remove(employeeMaster);
			}
		} catch (Exception e) {
			log.info("sectionChange exception--------", e);
		}
	}

	public List<EmployeeMaster> employeeAutoComplete(String empName) {
		log.info("employeeAutoComplete starts----------" + empName);
		List<EmployeeMaster> employeeMasters = null;
		try {
			employeeMasters = new ArrayList<>();
			if (employeeMasterList != null && !employeeMasterList.isEmpty()) {
				for (EmployeeMaster employeeMaster : employeeMasterList) {
					if (employeeMaster.getFirstName().toUpperCase().contains(empName.toUpperCase())
							|| employeeMaster.getFirstName().contains(empName)) {
						employeeMaster.setEmpNameWithPfNumber(employeeMaster.getPersonalInfoEmployment().getPfNumber()
								+ "/" + employeeMaster.getFirstName());
						employeeMasters.add(employeeMaster);
					}
				}
			}
		} catch (Exception e) {
			log.error("employeeAutoComplete exception-----", e);
		}
		return employeeMasters;
	}

	public void getAllEmployeeMaster() {
		log.info("FileMovement:==> getEmployeeMaster Method Called---------" + employeeMaster);
		try {
			// log.info("cepartment--------"+department.getName());
			employeeMasterList = new ArrayList<EmployeeMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/employee/getall";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("employeeMasterList---------" + employeeMasterList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside getEmployeeMaster method--", e);
		}
	}

	public void loadLazyFileMovementList() {

		log.info("<===== loadLazyFileMovementList() Starts ======>");

		lazyFileMovementList = new LazyDataModel<FileMovement>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -1604168262204819249L;

			@Override
			public List<FileMovement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					BaseDTO response = httpService.post(SERVER_URL + "/filemovement/getall", paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						fileMovementList = mapper.readValue(jsonResponse, new TypeReference<List<FileMovement>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFileMovement List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return fileMovementList;
			}

			@Override
			public Object getRowKey(FileMovement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FileMovement getRowData(String rowKey) {
				try {
					for (FileMovement fileMovementObj : fileMovementList) {
						if (fileMovementObj.getId().equals(Long.valueOf(rowKey))) {
							selectedFileMovement = fileMovementObj;
							return fileMovementObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== loadLazyFileMovementList() Ends ======>");

	}

	public String showAddPage() {
		action = "ADD";
		stage = null;
		fileMovement = new FileMovement();
		selectedFileMovement = new FileMovement();
		employeeMasterList = new ArrayList<>();
		return CREATE_PAGE;
	}

	public String showEditPage() {

		log.info("<----Redirecting to user Edit page---->");
		if (selectedFileMovement == null) {
			Util.addWarn("Please Select One FileMovement");
			return null;
		}
		filemovementNote = new FileMovementNote();

		loadAllReferenceNumbers();
		// getAllEmployeeMaster();
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		departmentList = commonDataService.getDepartment();
		sectionList = commonDataService.getSectionMaster();
		entityTypeList = commonDataService.getAllEntityType();
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.FILE_MOVEMENT.toString());

		getFileMovementByFileMovementId(selectedFileMovement);
		viewFileMovementNoteById(selectedFileMovement);

		/*employeeMasterList = commonDataService.getEmpByWorkLocationCurrentDeptCurrentSec(entity.getId(),
				department.getId(), sectionMaster.getId());*/
		
		employeeMasterList = loadEmployeesByHoroAndAllPossibleQueries(entityMaster,
				entityTypeMaster, entity, department, sectionMaster);

		/*
		 * if (selectedFileMovement.getId() != null) {
		 * getFileMovementByFileMovementId(selectedFileMovement.getId());
		 * viewFileMovementNoteById(selectedFileMovement.getId());
		 * 
		 * }
		 */

		// viewFileMovement(selectedFileMovement);
		// viewFileMovementNote(selectedFileMovement);
		action = "EDIT";
		return CREATE_PAGE;
	}

	public String showViewPage() {

		action = "VIEW";

		log.info("<----Redirecting to user view page---->");
		if (selectedFileMovement == null) {
			Util.addWarn("Please Select One FileMovement");
			return null;
		}
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.FILE_MOVEMENT.toString());
		setLastApprovalStatus(selectedFileMovement.getFileMovementLog().getStage());
		viewFileMovement(selectedFileMovement);
		viewFileMovementNote(selectedFileMovement);
		loadAllReferenceNumbers();
		return VIEW_PAGE;
	}

	public String cancel() {
		entityMaster = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		entity = new EntityMaster();
		department = new Department();
		sectionMaster = new SectionMaster();
		employeeMaster = new EmployeeMaster();
		userMaster = new UserMaster();
		fileMovementFilesList = new ArrayList<FileMovementFiles>();
		fileMovement = new FileMovement();
		fileName = null;
		return LIST_PAGE;
	}

	public String goToPage(String pageName) {

		log.info("Goto Page [" + pageName + "]");

		if (pageName.equalsIgnoreCase("LIST")) {
			return showListPage();
		} else if (pageName.equalsIgnoreCase("ADD")) {
			return showAddPage();
		} else if (pageName.equalsIgnoreCase("EDIT")) {
			return showEditPage();
		} else if (pageName.equalsIgnoreCase("VIEW")) {
			return showViewPage();
		} else {
			return null;
		}
	}

	public String submitFileMovement() {

		String apiURL = appPreference.getPortalServerURL() + "/filemovement/create";
		try {

			BaseDTO baseDTO = httpService.post(apiURL, fileMovement);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Saved Successfully");
					errorMap.notify(ErrorDescription.FILE_MOVEMENT_ADDED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}

			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_PAGE;
	}

	public FileMovement getFileMovementById(Long id) {

		String apiURL = appPreference.getPortalServerURL() + "/filemovement/view/" + id;
		FileMovement fileMovement = null;
		try {

			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				fileMovement = mapper.readValue(jsonValue, FileMovement.class);
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return fileMovement;

	}

	// edit page functionality
	public String getFileMovementByFileMovementId(FileMovement fileMovement) {

		try {

			/*
			 * String URL = appPreference.getPortalServerURL() + "/filemovement/get/" + id;
			 * log.info("=======apiURL== getFileMovementByFileMovementId====" + URL);
			 * 
			 * BaseDTO baseDTO = httpService.get(URL);
			 */
			String url = SERVER_URL + "/filemovement/view";
			BaseDTO baseDTO = httpService.post(url, fileMovement);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedFileMovement = mapper.readValue(jsonValue, new TypeReference<FileMovement>() {
				});

				entityMaster = selectedFileMovement.getEntityMaster();
				employeeMaster = selectedFileMovement.getEmpMaster();
				entityTypeMaster = selectedFileMovement.getEntityMaster().getEntityTypeMaster();
				entityTypeChange();
				entity = selectedFileMovement.getEntityMaster();
				sectionMaster = selectedFileMovement.getSectionMaster();
				department = selectedFileMovement.getDepartmentMaster();
				incomingTapal = selectedFileMovement.getIncomingTapal();
				content = selectedFileMovement.getContent();
				subject = selectedFileMovement.getSubject();
				movementNumber = selectedFileMovement.getMovementNumber();
				userMaster = new UserMaster();
				log.info("====selectedFileMovement.getFileMovementFilesList()===="
						+ selectedFileMovement.getFileMovementFilesList().size());
				fileMovementFilesList = selectedFileMovement.getFileMovementFilesList();

			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());

		}
		return CREATE_PAGE;

	}

	public void viewFileMovementNoteById(FileMovement fileMovement) {

		try {
			/*
			 * BaseDTO baseDTO = new BaseDTO(); String url = SERVER_URL +
			 * "/filemovement/viewnote/" + id; baseDTO = httpService.get(url);
			 */
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/filemovement/viewnote";
			baseDTO = httpService.post(url, fileMovement);
			filemovementNote = new FileMovementNote();

			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				filemovementNote = mapper.readValue(jsonResponse, new TypeReference<FileMovementNote>() {
				});

				setNote(filemovementNote.getNote());
				userMaster = filemovementNote.getForwardTo();
				log.info("===filemovementNote= viewFileMovementNoteById=====" + filemovementNote);
				previousApproval = filemovementNote.getFinalApproval();
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------End FileMovement-viewFileMovement ------- >>>>");
	}

	public void myMethod(FileMovementFiles aa) {
		log.info("");
	}

	public void fileUpload(FileUploadEvent event) { 
		log.info("FileMovement:==> fileUpload Method Called---------");
		fileMovementFile = new FileMovementFiles();
		try {

			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadedFile = event.getFile();
			String type = FilenameUtils.getExtension(uploadedFile.getFileName());
			log.info(" File Type :: " + type);
			boolean validFileType = AppUtil.isValidFileType(type,
					new String[] { "png", "jpg", "JPG", "doc", "jpeg", "gif", "docx", "pdf", "xlsx", "xls" });
			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
				return;
			}
			long size = uploadedFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = (size / AppUtil.ONE_MB_IN_KB);
			log.info("file size---------------" + size);
            if(size>5000) {
            	Util.addWarn("File size should be less than 5 MB");
				return ;
               }
			String docPathName = commonDataService.getAppKeyValue("FILEMOVEMENT_FILE_UPLOAD_PATH");

			String filePath = Util.fileUpload(docPathName, uploadedFile);
			setFileName(uploadedFile.getFileName());
			fileMovementFile.setFileType(filePath.substring(filePath.lastIndexOf(".")));
			fileMovementFile.setFilePath(filePath);
			fileMovementFile.setFileName(uploadedFile.getFileName().replaceAll("[^A-Za-z0-9.]", "_"));
			fileMovementFile.setFileSize((double) size);
			if (action.equals("VIEW")) {
				viewPageSeletedFile = (FileMovementFiles) event.getComponent().getAttributes().get("selectedFilesId");
				FileMovementFiles removedFileName = selectedFileMovement.getFileMovementFilesList().stream()
						.filter(i -> i.getParentId() != null && viewPageSeletedFile.getId() != null
								&& i.getParentId().getId().equals(viewPageSeletedFile.getId()))
						.findAny().orElse(null);
				if (removedFileName != null) {
					selectedFileMovement.getFileMovementFilesList().remove(removedFileName);
				}
				fileMovementFile.setParentId(viewPageSeletedFile);
				fileMovementFile.setFileMovement(selectedFileMovement);
				selectedFileMovement.getFileMovementFilesList().add(fileMovementFile);
			} else {
				fileMovementFilesList.add(fileMovementFile);
			}

		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}
		// errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE.getErrorCode());
		log.info("FileMovement:==> fileUpload Method End---------");
	}

	public String createFileMovements() {
		log.info("<...... FileMovement createFileMovements begin ....>#Start");

		try {
			if (filemovementNote.getNote() == null) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				return null;
			}

			if (fileMovementFilesList == null || fileMovementFilesList.isEmpty()) {
				AppUtil.addWarn("Please upload documents");
				return null;
			}

			filemovementNote.setForwardTo(userMaster);
			fileMovement.setActiveStatus(true);
			fileMovement.setEmpMaster(employeeMaster);
			fileMovement.setEntityMaster(entityMaster);

			fileMovement.setSectionMaster(sectionMaster);
			fileMovement.setDepartmentMaster(department);
			fileMovement.setFileMovementFilesList(fileMovementFilesList);
			fileMovement.setFileMovementNote(filemovementNote);
			fileMovement.setIncomingTapal(incomingTapal);
			fileMovement.setContent(content);
			fileMovement.setSubject(subject);
			fileMovement.setMovementNumber(movementNumber);
			fileMovement.setForwardTo(userMaster);
			if (selectedFileMovement != null) {
				fileMovement.setId(selectedFileMovement.getId());
			}
			BaseDTO baseDTO = null;

			baseDTO = httpService.post(SERVER_URL + "/filemovement/createupdate", fileMovement);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("FileMovement saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.FILE_MOVEMENT_CREATED_SUCCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.FILE_MOVEMENT_UPDATED_SUCCESSFULLY.getCode());
					showListPage();

				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating FileMovement" + e);
		}
		log.info("...... FileMovement createFileMovements ended ....");
		return LIST_PAGE;
	}

	public void submitFileNote() {
		log.info("<...... FileMovement submitFileNote begin ....>#Start");
		try {
			filemovementNote.setNote(note);
			// filemovementNote.setForwardTo(userMaster);
		} catch (Exception exp) {
			log.error("Error while creating submitFileNote", exp);
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FileMovement.onRowSelect ========>" + event);
		selectedFileMovement = ((FileMovement) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		Long fileMovementCreatedById = selectedFileMovement.getCreatedBy() == null ? null
				: selectedFileMovement.getCreatedBy().getId();
		Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();
		if (selectedFileMovement != null) {
			setStage(selectedFileMovement.getFileMovementLog().getStage());
			if (selectedFileMovement.getFileMovementLog().getStage().equalsIgnoreCase(AmountMovementType.REJECTED)
					&& fileMovementCreatedById.equals(loginUserId)) {
				editButtonFlag = false;
			}
		}
		log.info("<===Ends FileMovement.onRowSelect ========>");
	}

	public void viewFileMovement(FileMovement fileMovement) {
		log.info("<===Starts FileMovement.viewFileMovement  ========>" + fileMovement.getId());
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/filemovement/view";
			fileMovement.setNotificationId(selectedNotificationId);
			baseDTO = httpService.post(url, fileMovement);
			selectedFileMovement = new FileMovement();

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedFileMovement = mapper.readValue(jsonResponse, new TypeReference<FileMovement>() {
				});
				log.info("===selectedFileMovement=======" + selectedFileMovement);

				String employeeDatajsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<FileMovementDTO>>() {
						});
				// log.info("<======= view Note Employee Details List ==========>" +
				// viewLogResponseList.size());

			}
			selectedNotificationId=null;
			systemNotificationBean.loadTotalMessages();
			entityMaster = selectedFileMovement.getEntityMaster();
			movementNumber = selectedFileMovement.getMovementNumber();
			if (selectedFileMovement.getFileViewCount()) {
				Integer fileMovementViewed = dashBoardBean.getHeaderDTO().getFileMovementViewed();
				Integer fileMovementPending = dashBoardBean.getHeaderDTO().getFileMovementPending();
				DashboardDTO headerDTO = dashBoardBean.getHeaderDTO();
				headerDTO.setFileMovementPending(fileMovementPending - 1);
				headerDTO.setFileMovementViewed(fileMovementViewed + 1);
				dashBoardBean.setHeaderDTO(headerDTO);
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedFileMovement != null) {
			log.info("FileMovement viewFileMovement");
		} else {
			log.info("FileMovement viewFileMovement is null");
		}

		log.info("<<<< ----------End FileMovement-viewFileMovement ------- >>>>");
	}

	public void viewFileMovementNote(FileMovement fileMovement) {
		log.info("<===Starts FileMovement.viewFileMovement  ========>" + fileMovement.getId());
		try {
			BaseDTO baseDTO = new BaseDTO();
			approvalFlag = false;
			finalApprovalFlag = false;
			buttonFlag1 = false;
			String url = SERVER_URL + "/filemovement/viewnote";
			baseDTO = httpService.post(url, fileMovement);
			filemovementNote = new FileMovementNote();
			fileMovementLog.setRemarks(null);

			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				filemovementNote = mapper.readValue(jsonResponse, new TypeReference<FileMovementNote>() {
				});
				previousApproval = filemovementNote.getFinalApproval();

				if (filemovementNote.getForwardTo() != null) {
					if (loginBean.getUserMaster().getId().equals(filemovementNote.getForwardTo().getId())) {
						approvalFlag = true;
						buttonFlag1 = true;
						if (previousApproval) {
							finalApprovalFlag = true;
							approvalFlag = false;
							buttonFlag1 = true;
						}
					} else {
						approvalFlag = false;
					}
				}
				// setFinalApproveFlag(finalApproveFlag);
				// if (true == previousApproval && "REJECTED".equals(lastApprovalStatus)) {
				// buttonFlag1 = true;
				// }

			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------End FileMovement-viewFileMovement ------- >>>>");
	}

	public String approveFileMovement() {
		BaseDTO baseDTO = null;
		try {
			log.info("FileMovement id----------" + selectedFileMovement.getId());
			fileMovementLog.setRemarks(approvedRemark);
			fileMovement.setId(selectedFileMovement.getId());
			fileMovement.setFileMovementNote(filemovementNote);
			baseDTO = new BaseDTO();
			if(!finalApprovalFlag && filemovementNote.getForwardTo()==null)
			{
				Util.addWarn("Forward To is Required");
				return null;
			}
			if (previousApproval == false && filemovementNote.getFinalApproval() == true) {
				fileMovementLog.setStage(AmountMovementType.APPROVED);
				fileMovement.setFileMovementLog(fileMovementLog);
			} else if (previousApproval == true && filemovementNote.getFinalApproval() == true) {
				fileMovementLog.setStage(AmountMovementType.FINAL_APPROVED);
				fileMovement.setFileMovementLog(fileMovementLog);
			} else {
				fileMovementLog.setStage(AmountMovementType.APPROVED);
				fileMovement.setFileMovementLog(fileMovementLog);
			}
			log.info("approve remarks=========>" + fileMovementLog.getRemarks());
			fileMovement.setFileMovementFilesList(selectedFileMovement.getFileMovementFilesList());
			String url = SERVER_URL + "/filemovement/approve";
			baseDTO = httpService.post(url, fileMovement);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FILE_MOVEMENT_APPROVED_SUCCESSFULLY.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				selectedFileMovement = null;
				return showListPage();
			}
		} catch (Exception e) {
			log.error("approveFileMovement method inside exception-------", e);
		}
		return null;
	}

	public String rejectCashToBank() {
		BaseDTO baseDTO = null;
		try {
			log.info("FileMovement id----------" + selectedFileMovement.getId());
			baseDTO = new BaseDTO();
			fileMovementLog.setRemarks(rejectedRemark);
			fileMovement.setId(selectedFileMovement.getId());
			fileMovementLog.setStage(AmountMovementType.REJECTED);
			fileMovement.setFileMovementLog(fileMovementLog);
			String url = SERVER_URL + "/filemovement/reject";
			baseDTO = httpService.post(url, fileMovement);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FILE_MOVEMENT_REJECTED_SUCCESSFULLY.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("FileMovement method inside exception-------", e);
		}
		return null;
	}

	public void deleteFile(FileMovementFiles file) {
		log.info("<===Ends FileMovement.deleteFile Start ========>");
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmFileDeleteOption').show();");
			selectedFile = file;
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<===Ends FileMovement.deleteFile ========>");
	}

	public void confirmDelete() {
		log.info("<===Ends FileMovement.confirmDelete Start ========>");
		try {
			if (fileMovementFilesList.contains(selectedFile)) {
				fileMovementFilesList.remove(selectedFile);
				File existingFile = new File(selectedFile.getFilePath());
				if (existingFile.isFile()) {
					// delete the existing file
					existingFile.delete();
				}
			}
			setFileName((fileMovementFilesList != null && !fileMovementFilesList.isEmpty())
					? fileMovementFilesList.get(0).getFileName()
					: "");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<===Ends FileMovement.confirmDelete ========>");
	}

	public void downloadfile(FileMovementFiles fileObj) {
		InputStream input = null;
		try {
			File files = new File(fileObj.getFilePath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public String clear() {
		log.info("FileMovementBean:clear Method Start====>");
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		selectedFileMovement = new FileMovement();
		log.info("FileMovementBean:clear Method End====>");
		return LIST_PAGE;
	}

	public List<IncomingTapal> loadAllReferenceNumbers() {
		log.info("<======== FileMovementBean.loadAllReferenceNumbers calling ======>");
		referenceNumberList = new ArrayList<>();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/gettapalreferencenumbers";

			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				referenceNumberList = mapper.readValue(jsonResponse, new TypeReference<List<IncomingTapal>>() {
				});
				if (referenceNumberList.size() > 0) {
					log.info("FileMovementBean loadAllReferenceNumbers size" + referenceNumberList.size());
					log.info("FileMovementBean loadAllReferenceNumbers size" + referenceNumberList.get(0));
				} else {
					log.info("FileMovementBean loadAllReferenceNumbers size ====>>    0  ");
					log.info("FileMovementBean loadAllReferenceNumbers size  ====>>   0 ");
				}
			} else {
				referenceNumberList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("<========== Exception FileMovementBean.loadAllReferenceNumbers ==========>", e);
		}
		return referenceNumberList;
	}

	public String getNextSequenceConfigValueForFileNumber() {
		log.info("getNextSequenceConfigValueForFileNumber called......");
		try {

			String url = SERVER_URL + "/filemovement/nextsequenceconfigvalue";
			BaseDTO response = httpService.get(url);
			log.info("BaseDTO Dto :" + response.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			movementNumber = mapper.readValue(jsonResponse, String.class);

			log.info("<<=======  INFORMATIONS::  " + movementNumber);
		} catch (Exception e) {
			log.error("<<<=======  Error in view getNextSequenceConfigValueForFileNumber =====>>");
		}

		return movementNumber;

	}

	// public List<UserMaster> loadAutoCompleteUserMaster(String query) {
	// log.info("Institute Autocomplete query==>" + query);
	// List<String> results = new ArrayList<String>();
	// autoEmployeeMasterList =
	// commonDataService.loadAutoCompleteForwardToUser(SERVER_URL,query);
	// return autoEmployeeMasterList;
	// }
	
	public List<EmployeeMaster> loadEmployeesByHoroAndAllPossibleQueries(EntityMaster horo,EntityTypeMaster entityTypeMaster,EntityMaster entityMaster,Department department,SectionMaster sectionMaster) {
		List<EmployeeMaster> employeeMasterList=new ArrayList<>();
		try {
			
			AddnlEarningDeductionDTO addnlEarningDeductionDTO = new AddnlEarningDeductionDTO();
			Long horoId = horo != null && horo.getId() != null
					? horo.getId()
					: null;
			Long entityTypeId = entityTypeMaster != null && entityTypeMaster.getId() != null
					? entityTypeMaster.getId()
					: null;
			Long entityId = entityMaster != null && entityMaster.getId() != null ? entityMaster.getId() : null;
			Long departmentId = department != null && department.getId() != null
					? department.getId()
					: null;
			Long sectionId = sectionMaster != null && sectionMaster.getId() != null ? sectionMaster.getId()
					: null;
			log.info("HORO Id       ====  " + horoId);
			log.info("EntityType Id ====  " + entityTypeId);
			log.info("Entity Id     ====  " + entityId);
			log.info("Department Id ====  " + departmentId);
			log.info("Section Id    ====  " + sectionId);
			addnlEarningDeductionDTO.setHoroId(horoId);
			addnlEarningDeductionDTO.setEntityTypeId(entityTypeId);
			addnlEarningDeductionDTO.setEntityId(entityId);
			addnlEarningDeductionDTO.setDepartmentId(null);
			addnlEarningDeductionDTO.setSectionId(null);
			String url = AppUtil.getPortalServerURL() + "/employee/getactiveemployeelistbysearchcriteria";
			log.info("getactiveemployeelistbysearchcriteria url==> " + url);
			BaseDTO baseDTO = httpService.post(url, addnlEarningDeductionDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			RequestContext.getCurrentInstance().update("inputpanelentityid");
			log.info("Employee Master List Size ===>" + employeeMasterList != null && !employeeMasterList.isEmpty()
					? employeeMasterList.size()
					: 0);
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		return employeeMasterList;
	}
	

}