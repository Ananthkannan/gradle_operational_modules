package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.WrittenExamMark;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("writtenExaminationBean")
@Scope("session")
public class WrittenExaminationBean {

	ObjectMapper mapper;
	String jsonResponse;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Boolean addButtonFlag = false;
	@Getter
	@Setter
	Boolean approveButtonFlag = false;
	@Getter
	@Setter
	Boolean viewButtonFlag = false;
	@Getter
	@Setter
	Boolean deleteFlag = false;
	@Getter
	@Setter
	Boolean generateFlag = false;
	@Getter
	@Setter
	Boolean enableSubmit = false;
	@Getter
	@Setter
	List<Designation> designationList;
	@Getter
	@Setter
	List<Community> communityList;
	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	String status;

	@Getter
	@Setter
	List<JobAdvertisement> jobAdvertisementList = new ArrayList<>();

	@Getter
	@Setter
	WrittenExamMark examMarkList = new WrittenExamMark();

	@Getter
	@Setter
	List<WrittenExamMark> examMarkLists = new ArrayList<>();

	@Getter
	@Setter
	LazyDataModel<WrittenExamMark> examMarkListLazy;

	@Setter
	@Getter
	Designation applicationPost;

	@Getter
	@Setter
	WrittenExamMark selectedWrittenExamMark = new WrittenExamMark();

	@Setter
	@Getter
	Integer applicationYear;

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	Boolean isUploaded = false;

	@Getter
	@Setter
	private UploadedFile file = null;

	public String loadWrittenExamMarkLists() {
		log.info("<------Loading written examination list page----->");
		mapper = new ObjectMapper();
		applicationPost = new Designation();
		applicationYear = null;
		fileName = null;
		isUploaded = false;
		selectedWrittenExamMark = new WrittenExamMark();
		action = "LIST";
		addButtonFlag = false;
		approveButtonFlag = false;
		generateFlag = false;
		deleteFlag = false;
		viewButtonFlag = false;
		loadMasterValues();
		loadWrittenExamMarkListLazy();
		/*
		 * try{ String url = SERVER_URL+"/writtenexam/getallmarklist"; BaseDTO response
		 * = httpService.get(url);
		 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		 * jsonResponse = mapper.writeValueAsString(response.getResponseContents());
		 * examMarkLists = mapper.readValue(jsonResponse, new
		 * TypeReference<List<WrittenExamMark>>() { });
		 * log.info("examMarkLists size==> " + examMarkLists.size()); } catch (Exception
		 * e) { log.error("Exception Occured in JobApplicationApproval search ::", e);
		 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); }
		 */
		return "/pages/personnelHR/listWrittenExaminationMarks.xhtml?faces-redirect=true";
	}

	public void loadMasterValues() {
		designationList = commonDataService.loadDesignation();
		communityList = commonDataService.getAllCommunity();
	}

	public void loadWrittenExamMarkListLazy() {
		log.info("<-----Written exam mark list lazy load starts------>");
		examMarkListLazy = new LazyDataModel<WrittenExamMark>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<WrittenExamMark> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<WrittenExamMark> writtenExamList = mapper.readValue(jsonResponse,
							new TypeReference<List<WrittenExamMark>>() {
							});
					if (writtenExamList == null) {
						log.info(" writtenExamList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Written exam mark list search Successfully Completed");
					} else {
						log.error(
								" Written exam mark list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return writtenExamList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Written exam mark list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(WrittenExamMark res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public WrittenExamMark getRowData(String rowKey) {
				List<WrittenExamMark> responseList = (List<WrittenExamMark>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (WrittenExamMark res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedWrittenExamMark = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			WrittenExamMark request = new WrittenExamMark();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("jobAdvertisement.notificationNumber")) {
					log.info("Notification Number : " + value);
					searchFilters.put("notificationNumber", value);
				}
				if (filter.getKey().equals("recruitmentPost")) {
					log.info("recruitment for post : " + value);
					searchFilters.put("recruitmentPost", value);
				}
				if (filter.getKey().equals("jobAdvertisement.recuritmentYear")) {
					log.info("Recruitment year : " + value);
					searchFilters.put("recruitmentYear", Integer.valueOf(value));

				}
				if (filter.getKey().equals("writtenExaminationDate")) {
					log.info("Written exam Date : " + value);
					searchFilters.put("writtenExaminationDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/writtenexam/getallmarklistlazy", request);
			log.info("Search request response " + dataList);

		} catch (Exception e) {
			log.error("Exception Occured in Written exam marklist load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String addWrittenExamMarkList() {
		log.info("<----Loading written exam add page.....---->");
		examMarkList = new WrittenExamMark();
		isUploaded = false;
		enableSubmit = false;
		getNotficationNumber();
		return "/pages/personnelHR/createWrittenExaminationMark.xhtml?faces-redirect=true";
	}

	public String writtenExamMarkListAction() {
		try {
			if (selectedWrittenExamMark == null) {
				errorMap.notify(ErrorDescription.SELECT_WRIITEN_EXAM.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("DELETE")) {
				if (selectedWrittenExamMark.getStatus().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.APPROVED_WRIITEN_EXAM_LIST_CANNOT_DELETE.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmWrittenExamDelete').show();");
			} else {
				log.info("<----Loading written exam view page.....---->" + selectedWrittenExamMark.getId());
				examMarkList = new WrittenExamMark();
				BaseDTO response = httpService
						.get(SERVER_URL + "/writtenexam/getmarklist/" + selectedWrittenExamMark.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					examMarkList = mapper.readValue(jsonResponse, new TypeReference<WrittenExamMark>() {
					});
					if (action.equalsIgnoreCase("VIEW"))
						return "/pages/personnelHR/viewWrittenExamMarkList.xhtml?faces-redirect=true";
					else if (action.equalsIgnoreCase("APPROVE")) {
						return "/pages/personnelHR/writtenExamMarkListApproval.xhtml?faces-redirect=true";
					}
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in JobApplicationApproval search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void getNotficationNumber() {
		BaseDTO baseDTO = new BaseDTO();
		log.info("Fecthing notification numbers......");
		try {
			String url = SERVER_URL + "/jobadvertisement/getnotification";
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			jobAdvertisementList = mapper.readValue(jsonResponse, new TypeReference<List<JobAdvertisement>>() {
			});
			log.info("jobAdvertisementList size==> " + jobAdvertisementList.size());
		} catch (Exception e) {
			log.error("Exception Occured in JobApplicationApproval search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void getPostAndYear() {
		log.info("Loading application post and year.....");
		applicationPost = examMarkList.getJobAdvertisement().getRosterReservation().getStaffEligibility()
				.getRecruitmentForPost();
		applicationYear = examMarkList.getJobAdvertisement().getRecuritmentYear();

		log.info("applicationPost--->" + applicationPost.getName() + " applicationYear--->" + applicationYear);
	}

	public void clearData() {
		log.info("<==== Clearing the data.....");
		examMarkList = new WrittenExamMark();
		applicationPost = new Designation();
		applicationYear = null;
		fileName = null;
		isUploaded = false;
		enableSubmit = false;

	}

	public void generateMarkList() {
		log.info("<==== Generating mark list........");
		try {

			if (examMarkList.getJobAdvertisement() == null) {
				Util.addWarn("Please select a notification number");
				return;
			}

			if (examMarkList.getMarklistDocumnet() == null) {
				Util.addWarn("Please upload mark list");
				return;
			}
			BaseDTO response = httpService.post(SERVER_URL + "/writtenexam/generatemarklist", examMarkList);

			errorMap.notify(response.getStatusCode());

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());

			WrittenExamMark markList = mapper.readValue(jsonResponse, new TypeReference<WrittenExamMark>() {
			});
			if(markList != null) {
				if (response.getStatusCode() == 90011 && markList.getWrittenExamMarkDetails().size() > 0) {
	
					enableSubmit = true;
				}
	
				log.info("Mark lisst ssize======>" + markList.getWrittenExamMarkDetails().size());
				examMarkList.setWrittenExamMarkDetails(markList.getWrittenExamMarkDetails());
				examMarkList.setWrittenExaminationDate(markList.getWrittenExaminationDate());
			}else {
				log.error("markList is null");
			}
		} catch (Exception e) {
			log.error("Exception occured while Generating mark list....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String scannedDocumentUpload(FileUploadEvent event) {
		log.info("Upload alled");
		try {
			if (event == null || event.getFile() == null) {
				log.error("Uploaded Document is Empty");
				Util.addWarn("Uploaded Document is Empty");
				return null;
			}
			file = event.getFile();
			String type = FilenameUtils.getExtension(file.getFileName());
			log.info(" File Type :: " + type);
			if (!type.contains("xslx") && !type.contains("xls")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				Util.addWarn("Please Upload a proper document");
				return null;
			}
			long size = file.getSize();
			size = size / 1000;
			log.info(" Uploaded Employee Scanned Document Size in KB " + size);

			if (size > 51200) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				Util.addWarn("Documnet size exceeds ");
				return null;
			}
			fileName = file.getFileName();
			isUploaded = true;
			byte[] doc = IOUtils.toByteArray(file.getInputstream());
			examMarkList.setFileName(file.getFileName());
			examMarkList.setMarklistDocumnet(doc);

		} catch (Exception e) {
			log.error(" Exception Occured While Upload Document  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String submitMarklist() {
		log.info("<---Mark list submit called---->");
		try {

			if (examMarkList.getJobAdvertisement() == null) {
				Util.addWarn("Please select a notification number");
				return null;
			}
			if (examMarkList.getWrittenExamMarkDetails().size() == 0
					|| examMarkList.getWrittenExamMarkDetails().isEmpty()
					|| examMarkList.getWrittenExamMarkDetails() == null) {
				Util.addWarn("Please upload mark list");
				return null;
			}
			examMarkList.setCreatedBy(loginBean.getUserDetailSession());
			examMarkList.setCreatedDate(new Date());
//			examMarkList.setWrittenExaminationDate(examMarkList.getJobAdvertisement().getTentativeWrittenExamDate());
			examMarkList.setRecruitmentYear(
					Long.parseLong(examMarkList.getJobAdvertisement().getRecuritmentYear().toString()));
			examMarkList.setRecruitmentPost(examMarkList.getJobAdvertisement().getRosterReservation()
					.getStaffEligibility().getRecruitmentForPost().getName());
			examMarkList.setStatus("IN PROGRESS");
			BaseDTO response = httpService.post(SERVER_URL + "/writtenexam/addmarklist", examMarkList);
			errorMap.notify(response.getStatusCode());
			if (response.getStatusCode() == 0) {
				//Util.addInfo("Written examination inserted successfully");
				return loadWrittenExamMarkLists();
			}

		} catch (Exception e) {
			log.error(" Exception Occured saving mark list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String approveOrRejectMarklist() {
		log.info("<---Mark list submit called---->");
		try {
			examMarkList.setModifiedBy(loginBean.getUserDetailSession());
			examMarkList.setModifiedDate(new Date());
			examMarkList.setStatus(status);
			BaseDTO response = httpService.post(SERVER_URL + "/writtenexam/addmarklist", examMarkList);
			errorMap.notify(response.getStatusCode());
			if (response.getStatusCode() == 0) {
				return loadWrittenExamMarkLists();
			}

		} catch (Exception e) {
			log.error(" Exception Occured saving mark list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null; 
	}

	public String deleteConfirm() {
		log.info("<--- Exam mark delete confirmed---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/writtenexam/delete/" + selectedWrittenExamMark.getId());
			errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadWrittenExamMarkLists();
	}

	public void onRowSelect(SelectEvent event) {
		selectedWrittenExamMark = ((WrittenExamMark) event.getObject());
		addButtonFlag = true;
		if (selectedWrittenExamMark.getStatus().equalsIgnoreCase("APPROVED")) {
			deleteFlag = true;
			approveButtonFlag = true;
			generateFlag = false;

		} else if (selectedWrittenExamMark.getStatus().equalsIgnoreCase("REJECTED")) {
			approveButtonFlag = true;
			generateFlag = true;
			deleteFlag = false;
		} else if (selectedWrittenExamMark.getStatus().equalsIgnoreCase("IN PROGRESS")) {
			generateFlag = true;
			approveButtonFlag = false;
			deleteFlag = false;
		}
	}

}
