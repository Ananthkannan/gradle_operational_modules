package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeSearchRequest;
import in.gov.cooptex.core.dto.EmployeeSearchResponse;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BloodGroupMaster;
import in.gov.cooptex.core.model.CadreMaster;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeAchievements;
import in.gov.cooptex.core.model.EmployeeAdditionalCharge;
import in.gov.cooptex.core.model.EmployeeContactDetails;
import in.gov.cooptex.core.model.EmployeeDisciplinaryAction;
import in.gov.cooptex.core.model.EmployeeEducationQualification;
import in.gov.cooptex.core.model.EmployeeExperienceDetails;
import in.gov.cooptex.core.model.EmployeeFamilyDetails;
import in.gov.cooptex.core.model.EmployeeIncrementDetails;
import in.gov.cooptex.core.model.EmployeeInterchangeDetails;
import in.gov.cooptex.core.model.EmployeeLeaveDetails;
import in.gov.cooptex.core.model.EmployeeLeaveTravelConcession;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayscaleInformation;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeePersonalInfoGeneral;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EmployeeScannedDocument;
import in.gov.cooptex.core.model.EmployeeSuspensionDetails;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.model.EmployeeTypeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.IncrementCycle;
import in.gov.cooptex.core.model.InsuranceMaster;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.model.Nationality;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.PunishmentTypeMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.model.SalutationMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.SuspensionDetailsMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.VillageMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorCode;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.operation.rest.util.FormValidation;
import in.gov.cooptex.personnel.hrms.ESRLeaveType;
import in.gov.cooptex.personnel.hrms.ESRPageName;
import in.gov.cooptex.personnel.hrms.dto.ESRPrintDTO;
import in.gov.cooptex.personnel.rest.util.PrintESR;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author finatel
 *
 */
@Log4j2
@Scope("session")
@Service("employeeMasterBean")
public class EmployeeMasterBean {

	private final int employeeEducationMaxRow = 15;

	private final int employeePayscaleMaxRow = 50;

	private final int employeeAchievementMaxRow = 50;

	@Getter
	@Setter
	String action, tabName, docFileName, menuName, transferFlag, familyFlag, empInnerFlag, viewFlag, suspensionFlag,
			incrementFlag, visiableFamButton;

	@Getter
	@Setter
	String employeeTypeName, exFlag = "NO";

	@Getter
	@Setter
	private String serverURL;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	EmployeeMaster employeeForLogin = new EmployeeMaster();

	@Getter
	@Setter
	EmployeePersonalInfoEmployment employeePersonalInfo = new EmployeePersonalInfoEmployment();

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	List<EmployeeTypeMaster> employeeTypeMasterList;

	@Getter
	@Setter
	List<GenderMaster> genderList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> entityMasterAllList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityMasterTypeList;

	@Setter
	@Getter
	List<Designation> designationCadreList;

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	List<Department> departmentList;

	@Setter
	@Getter
	List<EntityMaster> regionList;

	@Setter
	@Getter
	String disableStatus;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Long designationId = null;

	@Getter
	@Setter
	List<EmployeeEducationQualification> educationQualificationList = new ArrayList<EmployeeEducationQualification>();

	@Getter
	@Setter
	EmployeeEducationQualification selectedEducation;

	@Getter
	@Setter
	List<EmployeePromotionDetails> promotionDetrailsList;

	@Getter
	@Setter
	List<EmployeeFamilyDetails> familyDetailsList = new ArrayList<EmployeeFamilyDetails>();

	@Getter
	@Setter
	List<EmployeeFamilyDetails> employeeFamilyDetailsList = new ArrayList<EmployeeFamilyDetails>();

	@Getter
	@Setter
	LazyDataModel<EmployeeSearchResponse> employeeMasterResponseLazyList;

	@Getter
	@Setter
	EmployeeSearchResponse selectedEmployee;

	@Getter
	@Setter
	EmployeeSearchRequest emmployeeMasterRequest;

	@Getter
	@Setter
	Boolean activeStatus, isPermanent = false;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private UploadedFile file = null;

	@Getter
	@Setter
	CadreMaster cadreMaster;

	@Getter
	@Setter
	EmployeeFamilyDetails employeeFamilyDetail = new EmployeeFamilyDetails();

	@Getter
	@Setter
	List<QualificationMaster> qualificationMasterList;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	List<SalutationMaster> salutationList;

	@Getter
	@Setter
	private StreamedContent uploadedDocument = null;

	@Getter
	@Setter
	private List<RelationshipMaster> relationShipMasterList = new ArrayList<RelationshipMaster>();

	@Getter
	@Setter
	private List<InsuranceMaster> insuranceMasterList = new ArrayList<InsuranceMaster>();

	@Getter
	@Setter
	List<SectionMaster> sectionMasterAllList = new ArrayList<SectionMaster>();

	@Getter
	@Setter
	List<EntityMaster> toEntityMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> transferFromEntityMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> transferToEntityMasterList = new ArrayList<>();

	long employeePhotoSize;

	@Getter
	@Setter
	int genderListSize, relationShipListSize, insuranceListSize, departmentListSize, regionListSize,
			entityMasterListSize, dob, employeeMasterListSize, entityListSize, entityTypeMasterListSize,
			entityMasterAllListSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<CadreMaster> cadreMaterList;

	@Getter
	@Setter
	List<EmployeePayscaleInformation> employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();

	@Getter
	@Setter
	List<PayScaleMaster> payScaleMasterList;

	@Getter
	@Setter
	PayScaleMaster payScaleMaster;

	@Getter
	@Setter
	List<MaritalStatus> maritalStatusList;

	@Getter
	@Setter
	List<Nationality> nationalityList;

	@Getter
	@Setter
	List<Religion> religionList;

	@Getter
	@Setter
	List<Community> communityList;

	@Getter
	@Setter
	List<CasteMaster> casteMasterList;

	@Getter
	@Setter
	List<BloodGroupMaster> bloodGroupMasterList;

	@Getter
	@Setter
	EmployeePersonalInfoGeneral employeePersonalInfoGeneral;

	@Getter
	@Setter
	private UploadedFile communityFile;

	@Getter
	@Setter
	private UploadedFile panDetailsFile;

	@Getter
	@Setter
	private UploadedFile exServicemanFile;

	@Getter
	@Setter
	private UploadedFile differentlyAbledFile;

	@Getter
	@Setter
	private UploadedFile freedomFighterFile;

	@Getter
	@Setter
	private String panDetailsFileName;

	@Getter
	@Setter
	private String exServicemanFileName;

	@Getter
	@Setter
	private String differentlyAbledFileName;

	@Getter
	@Setter
	private String freedomFighterFileName;

	@Getter
	@Setter
	private String communityFileName;

	@Setter
	@Getter
	List<EmployeeAchievements> achievementsList = new ArrayList<EmployeeAchievements>();

	@Setter
	@Getter
	EmployeeExperienceDetails experienceDetails = new EmployeeExperienceDetails();

	@Setter
	@Getter
	List<EmployeeExperienceDetails> experienceDetailsList = new ArrayList<EmployeeExperienceDetails>();

	@Getter
	@Setter
	Integer rowIndex = null;

	@Getter
	@Setter
	private UploadedFile relievingLetterFile;

	@Getter
	@Setter
	private String relievingLetterFileName = null;

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();

	@Getter
	@Setter
	List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();

	@Getter
	@Setter
	List<CityMaster> cityMasterList = new ArrayList<CityMaster>();

	@Getter
	@Setter
	DistrictMaster districtMaster = new DistrictMaster();

	@Getter
	@Setter
	TalukMaster talukMaster = new TalukMaster();

	@Getter
	@Setter
	CityMaster cityMaster = new CityMaster();

	@Getter
	@Setter
	List<TalukMaster> talukMasterList = new ArrayList<TalukMaster>();

	@Getter
	@Setter
	VillageMaster villageMaster = new VillageMaster();

	@Getter
	@Setter
	List<VillageMaster> villageMasterList = new ArrayList<VillageMaster>();

	@Getter
	@Setter
	EmployeeContactDetails employeeContactDetails = new EmployeeContactDetails();

	@Getter
	@Setter
	List<StateMaster> stateMasterListPresent = new ArrayList<StateMaster>();

	@Getter
	@Setter
	List<DistrictMaster> districtMasterListPresent = new ArrayList<DistrictMaster>();

	@Getter
	@Setter
	List<TalukMaster> talukMasterListPresent = new ArrayList<TalukMaster>();

	@Getter
	@Setter
	List<CityMaster> cityMasterListPresent = new ArrayList<CityMaster>();

	@Getter
	@Setter
	List<VillageMaster> villageMasterListPresent = new ArrayList<VillageMaster>();

	@Getter
	@Setter
	EmployeeSuspensionDetails employeeSuspensionDetails = new EmployeeSuspensionDetails();

	@Getter
	@Setter
	List<EmployeeSuspensionDetails> employeeSuspensionDetailsList = new ArrayList<EmployeeSuspensionDetails>();

	@Getter
	@Setter
	List<SuspensionDetailsMaster> suspensionDetailsList = new ArrayList<SuspensionDetailsMaster>();

	@Setter
	@Getter
	List<EntityMaster> suspensionEntityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EmployeeInterchangeDetails> interchangeDetailsList = new ArrayList<EmployeeInterchangeDetails>();

	@Getter
	@Setter
	List<EmployeeTransferDetails> employeeTransferDetailsList = new ArrayList<EmployeeTransferDetails>();

	@Getter
	@Setter
	EmployeeInterchangeDetails employeeInterchangeDetails = new EmployeeInterchangeDetails();

	@Getter
	@Setter
	List<PayScaleMaster> payscaleMasterDesList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeTransferDetails employeeTransferDetails = new EmployeeTransferDetails();

	@Getter
	@Setter
	PayScaleMaster payScaleMasterDes;

	@Getter
	@Setter
	String fileNameAddressProof;

	@Getter
	@Setter
	String landlineCode = "";

	@Getter
	@Setter
	String landlineNumber = "";

	@Getter
	@Setter
	boolean addButtonFlag = false;

	public static final String EMP_SERVER_URL = AppUtil.getPortalServerURL() + "/employee";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	long employeeScannedMaxDocSize;

	long employeeScannedMinDocSize;

	@Setter
	private DefaultStreamedContent downloadScannedDoc;

	@Getter
	@Setter
	List<EmployeeScannedDocument> employeeScannedDocumentList = new ArrayList<EmployeeScannedDocument>();

	@Getter
	@Setter
	EmployeePromotionDetails employeePromotionDetail = new EmployeePromotionDetails();

	@Getter
	@Setter
	List<EmployeePromotionDetails> employeePromotionDetailsList = new ArrayList<EmployeePromotionDetails>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypetList;

	@Getter
	@Setter
	String visibleButton;

	String jsonResponse;

	String url;

	ObjectMapper mapper;

	boolean isValid;

	@Setter
	@Getter
	String reportingTopfNumber;

	@Getter
	@Setter
	EmployeeDisciplinaryAction employeeDisciplinaryAction = new EmployeeDisciplinaryAction();

	@Getter
	@Setter
	List<EmployeeDisciplinaryAction> employeeDisciplinaryActionList = new ArrayList<>();

	@Getter
	@Setter
	List<PunishmentTypeMaster> punishmentTypeMasterList;

	@Getter
	@Setter
	EmployeeLeaveDetails employeeLeaveDetail;

	@Getter
	@Setter
	List<EmployeeLeaveDetails> employeeLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();

	@Getter
	@Setter
	List<EmployeeLeaveDetails> leaveDetailsList = new ArrayList<EmployeeLeaveDetails>();

	@Getter
	@Setter
	private List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<LeaveTypeMaster>();

	@Getter
	@Setter
	private List<LeaveTypeMaster> leaveTypeMasterLTCList = new ArrayList<LeaveTypeMaster>();

	@Getter
	@Setter
	EmployeeLeaveDetails employeeEarnedLeaveDetail;

	@Getter
	@Setter
	List<EmployeeLeaveDetails> employeeEarnedLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();

	@Getter
	@Setter
	EmployeeLeaveTravelConcession empLeaveTravelConcession = new EmployeeLeaveTravelConcession();

	@Getter
	@Setter
	List<EmployeeLeaveTravelConcession> employeeLeaveTravelConcessionList = new ArrayList<EmployeeLeaveTravelConcession>();

	@Getter
	@Setter
	List<EntityTypeMaster> suspensionEntityTypeList = new ArrayList<EntityTypeMaster>();

	@Getter
	@Setter
	List<EntityMaster> suspensionHeadAndRegionalOfficeList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EmployeeAdditionalCharge> employeeAdditionalChargeList = new ArrayList<EmployeeAdditionalCharge>();

	@Getter
	@Setter
	EmployeeAdditionalCharge employeeAdditionalCharge = new EmployeeAdditionalCharge();

	@Getter
	@Setter
	EmployeeAdditionalCharge tempEmployeeAdditionalCharge = new EmployeeAdditionalCharge();

	@Getter
	@Setter
	List<EmployeeIncrementDetails> employeeIncrementDetailsList = new ArrayList<EmployeeIncrementDetails>();

	@Getter
	@Setter
	EmployeeIncrementDetails employeeIncrementDetails = new EmployeeIncrementDetails();

	@Getter
	@Setter
	EmployeeIncrementDetails tempEmployeeincrementDetils = new EmployeeIncrementDetails();

	@Getter
	@Setter
	List<IncrementCycle> incrementCycleList = new ArrayList<IncrementCycle>();

	@Getter
	@Setter
	List<String> listYear = new ArrayList<>();

	@Getter
	@Setter
	List<String> blockYearList = new ArrayList<>();

	@Getter
	@Setter
	List<Integer> selectedYearList = new ArrayList<Integer>();

	@Getter
	@Setter
	Boolean dntNtfyMesgDis = false;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	IncrementCycle incrementCycle = new IncrementCycle();

	@Getter
	@Setter
	List<EmployeeMaster> reportingToEmployeeList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	List<EmployeeMaster> reportingToEmployeeNameList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	String employeePassword;

	@Getter
	@Setter
	String empPFNumber;

	@Setter
	@Getter
	private StreamedContent esrDownloadFile;

	@Getter
	@Setter
	String esrFilePath;

	@Getter
	@Setter
	String downloadOption;

	@Getter
	@Setter
	List<EntityMaster> allRegionList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> entityMasterObjList = new ArrayList<>();

	@PostConstruct
	public void init() {
		log.info("Init is executed.....................");
		employeeMaster = new EmployeeMaster();
		loadLazyEmployeeList();
		loadSearchListOfValues();
		loadAppConfigValues();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
			employeeScannedMaxDocSize = Long
					.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_SCANNED_DOC_MAXSIZE));
			employeeScannedMinDocSize = Long
					.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_SCANNED_DOC_MINSIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public EntityMasterDTO loadEntityByEntityId(Long id) {
		EntityMasterDTO entityMaster = new EntityMasterDTO();
		try {
			String URL = AppUtil.getPortalServerURL() + "/entitymaster/getentitymasterbyentityId/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			entityMaster = mapper.readValue(jsonResponse, EntityMasterDTO.class);

			if (entityMaster == null) {
				log.info("EmployeeMasterBean loadEntityByEntityId value is null");
			}

			log.info("SupplyRateConfirmationBean entityMasterObject " + entityMaster.toString());
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
		return entityMaster;
	}

	public void loadLazyEmployeeList() {

		log.info("< --  Start Employee Lazy Load -- > ");
		employeeMasterResponseLazyList = new LazyDataModel<EmployeeSearchResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<EmployeeSearchResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				EntityMaster workLocationEntity = null;
				EmployeePersonalInfoEmployment personalInfoEmployment = null;
				try {
					Long regionId = null;
					String entityTypeCode = null;
					EntityMasterDTO entityMaster = null;
					EmployeeMaster employeMaster = loginBean.getEmployee();
					if (employeMaster != null) {
						personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
						if (personalInfoEmployment != null) {
							workLocationEntity = personalInfoEmployment.getWorkLocation();
						} else {
							log.error("EmployeePersonalInfoEmployment is empty");
						}
					} else {
						log.error("EmployeeMaster is empty");
					}
					if (workLocationEntity != null) {
						entityMaster = loadEntityByEntityId(workLocationEntity.getId());
						if (entityMaster.getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE)
								|| entityMaster.getEntityTypeMaster().getEntityCode()
										.equals(EntityType.REGIONAL_OFFICE)) {
							regionId = personalInfoEmployment.getWorkLocation().getId();
							entityTypeCode = personalInfoEmployment.getWorkLocation().getEntityTypeMaster()
									.getEntityCode();
						} else {
							regionId = entityMaster.getEntityMasterRegion().getId();
							entityTypeCode = entityMaster.getEntityMasterRegion().getEntityTypeMaster().getEntityCode();
						}
					} else {
						log.info("<======== workLocationEntity is null ===========> ");
					}

					log.info("<======== Employee Region Id:   " + regionId);
					log.info("<======== Employee entityTypeName:   " + entityTypeCode);

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters, regionId,
							entityTypeCode);

					if (baseDTO == null || baseDTO.getStatusCode() == ErrorDescription
							.getError(MastersErrorCode.ENTITY_TYPE_NAME_EMPTY).getErrorCode()) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<EmployeeSearchResponse> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeeSearchResponse>>() {
							});
					if (data == null) {
						log.info(" EmployeeSearchResponse Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Employee Search Successfully Completed");
					} else {
						log.error(" Employee Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading employee data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeSearchResponse getRowData(String rowKey) {
				List<EmployeeSearchResponse> responseList = (List<EmployeeSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeSearchResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployee = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	/**
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @param regionId
	 * @param entityTypeCode
	 * @return
	 */
	private BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters, Long regionId, String entityTypeCode) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			EmployeeSearchRequest request = new EmployeeSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);
			request.setEntityTypeCode(entityTypeCode);
			request.setEmployeeRegionId(regionId);
			request.setPageAction(action);
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("empCode")) {
					log.info("employeeId : " + value);
					request.setEmpCode(value);
				}

				if (entry.getKey().equals("firstName")) {
					log.info("employeeName : " + value);
					request.setFirstName(value);
				}

				if (entry.getKey().equals("pfNumber")) {
					log.info("PF Number : " + value);
					request.setPfNumber(value);
				}
				if (entry.getKey().equals("designationName")) {
					log.info("designationName : " + value);
					request.setDesignationName(value);
				}
				if (entry.getKey().equals("regionName")) {
					log.info("regionName : " + value);
					request.setRegionName(value);
				}
				if (entry.getKey().equals("department")) {
					log.info("department : " + value);
					request.setDepartment(value);
				}
				if (entry.getKey().equals("dateOfJoining")) {
					log.info("dateOfJoining : " + value);
					request.setDateOfJoining(AppUtil.serverDateFormat(value));
				}
				if (entry.getKey().equals("activeStatus")) {
					log.info("activeStatus : " + value);
					Boolean status = value.equalsIgnoreCase("true") ? true : false;
					request.setActiveStatus(status);
				}

			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(EMP_SERVER_URL + "/search", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in employee search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void loadSearchListOfValues() {
		log.info("start loadSearchListOfValues ");
		try {
			designationList = commonDataService.loadDesignation();
			regionList = commonDataService.loadEntityRegion();
			departmentList = commonDataService.getDepartment();
			employeeTypeMasterList = commonDataService.getAllEmployeeType();
			genderList = commonDataService.getAllGenderType();
			cadreMaterList = commonDataService.loadCadres();
			salutationList = commonDataService.getAllSalutations();
			maritalStatusList = commonDataService.getAllMaritalStatus();
			nationalityList = commonDataService.getAllNationality();
			religionList = commonDataService.getAllReligion();
			communityList = commonDataService.getAllCommunity();
			casteMasterList = commonDataService.getAllCasteMaster();
			bloodGroupMasterList = commonDataService.getAllbloodGroupMaster();
			entityMasterTypeList = commonDataService.getAllEntityType();
			suspensionDetailsList = commonDataService.loadSuspensionDetails();
			insuranceMasterList = commonDataService.getAllInsurance();
			sectionMasterAllList = commonDataService.getSectionMaster();
			relationShipMasterList = commonDataService.getAllRelationShip();
			qualificationMasterList = commonDataService.loadAllActiveQualifications();
			stateMasterList = commonDataService.getStateList();
			entityTypetList = commonDataService.loadEntityTypes();
			payScaleMasterList = commonDataService.loadPayscale();
			punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
			leaveTypeMasterList = commonDataService.getAllActveLeaveTupeMasters();

			leaveTypeMasterLTCList = commonDataService.getActiveLeaveTypeByLTC();

			designationList = commonDataService.loadDesignation();
			suspensionEntityTypeList = commonDataService.getAllEntityType();
			suspensionHeadAndRegionalOfficeList = commonDataService.loadAllHeadAndRegionalOffice();
			incrementCycleList = commonDataService.loadAllIncrementCycle();
			entityMasterList = commonDataService.loadAllHeadAndRegionalOffice();
			allRegionList = commonDataService.loadAllHeadAndRegionalOffice();
			loadBlockYearList();
			loadYears();
		} catch (Exception exp) {
			log.error(" Exception Occured in while loading masters :: ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String clear() {
		log.info("clear method called..");
		selectedEmployee = new EmployeeSearchResponse();
		employeeLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
		employeeEarnedLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
		employeeMaster = new EmployeeMaster();

		employeeFamilyDetail = new EmployeeFamilyDetails();
		if (employeeFamilyDetail.getDependentName() != null) {
			employeeFamilyDetailsList = new ArrayList<>();
		}
		log.info("employeeFamilyDetailsList==>" + employeeFamilyDetailsList);

		employeeContactDetails = new EmployeeContactDetails();
		selectedEducation = new EmployeeEducationQualification();
		educationQualificationList = new ArrayList<EmployeeEducationQualification>();
		educationQualificationList.add(new EmployeeEducationQualification());
		promotionDetrailsList = new ArrayList<>();
		promotionDetrailsList.add(new EmployeePromotionDetails());
		entityTypeList = new ArrayList<>();
		designationCadreList = new ArrayList<>();
		employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();
		employeePayscaleInformationList.add(new EmployeePayscaleInformation());
		achievementsList = new ArrayList<EmployeeAchievements>();
		achievementsList.add(new EmployeeAchievements());
		experienceDetailsList = new ArrayList<>();
		experienceDetails = new EmployeeExperienceDetails();
		employeePersonalInfoGeneral = new EmployeePersonalInfoGeneral();
		employeeLeaveDetail = new EmployeeLeaveDetails();
		employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
		employeeSuspensionDetails = new EmployeeSuspensionDetails();
		employeeSuspensionDetailsList = new ArrayList<EmployeeSuspensionDetails>();
		landlineCode = "";
		landlineNumber = "";
		addButtonFlag = false;
		return "/pages/personnelHR/listEmployeeServiceRegister.xhtml?faces-redirect=true";
	}

	/**
	 * @return
	 */
	public String showLoginCredentialForm() {

		log.info("ShowLoginCredentialForm Called with Action " + action);
		if ("CREATE_LOGIN".equalsIgnoreCase(action)) {
			employeeForLogin = getEmployeeForLoginCredentialById();
			EmployeePersonalInfoEmployment employeePersonalInfoEmployment = employeeForLogin
					.getPersonalInfoEmployment();
			if (employeePersonalInfoEmployment == null) {
				employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();
				employeePersonalInfoEmployment.setCurrentDesignation(new Designation());
				employeeForLogin.setPersonalInfoEmployment(employeePersonalInfoEmployment);
			} else {
				Designation currentDesignation = employeePersonalInfoEmployment.getCurrentDesignation();
				if (currentDesignation == null) {
					employeePersonalInfoEmployment.setCurrentDesignation(new Designation());
				}

				this.empPFNumber = employeePersonalInfoEmployment.getPfNumber();
			}
		}

		return null;
	}

	public String createLoginCredentialsForEmployee() {
		log.info("<======== EmployeeMasterBean.createLoginCredentialsForEmployee() calling ======>");
		try {
			log.info("Employee pfNumber " + employeeForLogin.getPersonalInfoEmployment().getPfNumber());
			if (employeeForLogin.getPersonalInfoEmployment() != null) {
				if (getEmpPFNumber() == null || getEmpPFNumber().isEmpty()) {
					log.info("Employee Pf Number is Empty");
					errorMap.notify(ErrorDescription.getError(ErrorCode.PF_NUMBER_EMPTY).getCode());
				} else {
					log.info("Selected Employee Pf Number is - " + getEmpPFNumber());
					String getUrl = EMP_SERVER_URL + "/createlogincredentialsforemployee/" + employeeForLogin.getId()
							+ "/" + getEmpPFNumber();

					log.info("CreateLoginCredentialsForEmployee URL: - " + getUrl);

					BaseDTO baseDTO = httpService.get(getUrl);
					log.info("CreateLoginCredentialsForEmployee Response : - " + baseDTO);
					if (baseDTO != null) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						employeePassword = mapper.readValue(jsonResponse, String.class);
						log.info("employeePassword is " + employeePassword);
						RequestContext.getCurrentInstance().execute("PF('createLogin').hide();");
						RequestContext.getCurrentInstance().execute("PF('loginDetails').show();");
					}
				}
			}
		} catch (Exception e) {
			log.error("<========== Exception EmployeeMasterBean.createLoginCredentialsForEmployee()==========>", e);
		}
		return null;
	}

	/**
	 * @return
	 */
	private EmployeeMaster getEmployeeForLoginCredentialById() {
		try {
			if (selectedEmployee == null || selectedEmployee.getId() == null) {
				log.error("No employee selected");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("getEmployeeForLoginCredentialById() - Selected EmpId : " + selectedEmployee.getId());
			/*
			 * Controller: EmployeeMasterController.java
			 */
			String getUrl = EMP_SERVER_URL + "/getempbyidforcredentials/:id";
			url = getUrl.replace(":id", selectedEmployee.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			/*
			 * Get Employee by Id
			 */
			BaseDTO baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("getByEmployeeId-Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
			});

			if (employee != null) {
				employeeForLogin = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeForLogin;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public String showEmployeeForm() {

		log.info(" App Prefference file uploader Path ::::::::::::>>>>> " + appPreference.getPrimefaceUploaderType());
		try {

			setReportingTopfNumber(null);
			log.info("Show Employee Form Called with Action " + action);
			viewFlag = "EDITEMPLOYEE";
			visibleButton = "ADDPROMOTION";
			visibleButton = "ADDDISCIPLINARY";
			visiableFamButton = "ADDBUTTON";
			isPermanent = false;
			if (action.equalsIgnoreCase("ADD")) {
				employeeMaster = new EmployeeMaster();

				employeeFamilyDetail = new EmployeeFamilyDetails();
				if (employeeFamilyDetail.getDependentName() != null) {
					employeeFamilyDetailsList = new ArrayList<>();
				}
				log.info("employeeFamilyDetailsList==>" + employeeFamilyDetailsList);

				employeeContactDetails = new EmployeeContactDetails();
				selectedEducation = new EmployeeEducationQualification();
				educationQualificationList = new ArrayList<EmployeeEducationQualification>();
				educationQualificationList.add(new EmployeeEducationQualification());
				promotionDetrailsList = new ArrayList<>();
				promotionDetrailsList.add(new EmployeePromotionDetails());
				entityTypeList = new ArrayList<>();
				designationCadreList = new ArrayList<>();
				employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();
				employeePayscaleInformationList.add(new EmployeePayscaleInformation());
				achievementsList = new ArrayList<EmployeeAchievements>();
				achievementsList.add(new EmployeeAchievements());
				experienceDetailsList = new ArrayList<>();
				experienceDetails = new EmployeeExperienceDetails();
				employeePersonalInfoGeneral = new EmployeePersonalInfoGeneral();
				visibleButton = "ADDPROMOTION";
				visibleButton = "ADDDISCIPLINARY";
				employeeLeaveDetail = new EmployeeLeaveDetails();
				employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
				employeeSuspensionDetails = new EmployeeSuspensionDetails();
				employeeSuspensionDetailsList = new ArrayList<EmployeeSuspensionDetails>();
				landlineCode = "";
				landlineNumber = "";
			} else if (action.equalsIgnoreCase("EDIT")) {
				if (selectedEmployee == null) {
					errorMap.notify(ErrorDescription.SELECT_EMPLOYEE_ID.getErrorCode());
					return null;
				}
				log.info("showEmployeeForm - EDIT - Selected Employee  Id : : " + selectedEmployee.getId());
				employeeMaster = getByEmployeeId();
				if (employeeMaster != null) {
					Long userId = employeeMaster.getUserId();
					designationId = employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getId();
					employeeMaster.setDesignationId(designationId);
					log.info("Selected Employee-userId : : " + userId);
					if (userId == null) {
						errorMap.notify(PersonnalErrorCode.EMPLOYEE_LOGIN_NOT_FOUND);
						return null;
					}
					log.info("user id found ");
					familyFlag = "ADDFAMILY";
					educationQualificationList = employeeMaster.getEducationList();
					employeePayscaleInformationList = employeeMaster.getEmployeePayscaleInformationList();
					achievementsList = employeeMaster.getAchievementList();
					employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
					employeeLeaveTravelConcessionList = employeeMaster.getEmployeeLeaveTravelConcession();
					if (employeeLeaveTravelConcessionList != null && employeeLeaveTravelConcessionList.size() > 0) {
						visibleButton = "UPDATELeaveTravel";
						// empLeaveTravelConcession = employeeLeaveTravelConcessionList.get(0);
					}
					if (employeePersonalInfo != null) {
						if (employeePersonalInfo.getEntityType() != null) {
							entityMasterList = commonDataService.getEntityMaster(employeePersonalInfo.getEntityType());
							entityMasterObjList = commonDataService
									.getEntityMaster(employeePersonalInfo.getEntityType());
							allRegionList = commonDataService.loadAllHeadAndRegionalOffice();
						}

						if (employeePersonalInfo.getWorkLocation() != null) {
							reportingToEmployeeList = loadEmployeeList(employeePersonalInfo.getWorkLocation());
						}
						setReportingTopfNumber(employeePersonalInfo.getReportingTopfNumber());
						log.info("entityMasterList=====>" + entityMasterList);
					}
					experienceDetailsList = employeeMaster.getEmpExperienceDetailsList();
					experienceDetails = new EmployeeExperienceDetails();
					employeeContactDetails = employeeMaster.getContactDetails();
					if (employeeContactDetails != null) {
						onStateChangePermanent();
						if (employeeContactDetails.getTalukIdPermanent() != null)
							onDistrictChangePermanent();
						if (employeeContactDetails.getTalukIdPermanent() != null)
							onTalukChangePermanent();
						onStateChangePresent();
						if (employeeContactDetails.getTalukIdPresent() != null)
							onDistrictChangePresent();
						if (employeeContactDetails.getTalukIdPresent() != null)
							onTalukChangePresent();
					}
					employeeSuspensionDetails = new EmployeeSuspensionDetails();
					employeeSuspensionDetailsList = employeeMaster.getEmpSuspensionDetailsList();
					employeePromotionDetailsList = employeeMaster.getEmployeePromotionDetails();
					employeeDisciplinaryActionList = employeeMaster.getDisciplinaryActionList();
					employeeLeaveTravelConcessionList = employeeMaster.getEmployeeLeaveTravelConcession();

					employeeLeaveDetail = new EmployeeLeaveDetails();
					employeeEarnedLeaveDetail = new EmployeeLeaveDetails();

					employeeIncrementDetailsList = employeeMaster.getIncrementDetailsList();
					employeeAdditionalChargeList = employeeMaster.getAdditionalChargeList();
					employeeLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
					employeeEarnedLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
					// entityMasterList = commonDataService.loadAllHeadAndRegionalOffice();
					entityMasterList = commonDataService.loadAllHeadAndRegionalOffice();
					allRegionList = commonDataService.loadAllHeadAndRegionalOffice();
					log.info("<<<<<<=========Employee Increment Details List========>>>>>>>>>>"
							+ employeeIncrementDetailsList.toString());

					if (employeeMaster.getEmployeeLeaveDeatailsList() != null
							&& employeeMaster.getEmployeeLeaveDeatailsList().size() > 0) {
						log.info("Leave Details list -" + employeeMaster.getEmployeeLeaveDeatailsList().size());
						for (EmployeeLeaveDetails details : employeeMaster.getEmployeeLeaveDeatailsList()) {
							LeaveTypeMaster leaveTypeMasters = details.getLeaveTypeMaster();
							String leaveTypeCode = null;
							if (leaveTypeMasters != null) {
								leaveTypeCode = leaveTypeMasters.getCode();
								if (ESRLeaveType.EL.equals(leaveTypeCode)) {
									employeeEarnedLeaveDetailsList.add(details);
								} else {
									employeeLeaveDetailsList.add(details);
								}
							} else {
								employeeLeaveDetailsList.add(details);
							}
						}
					}
					log.info("Earned Leave Details list -" + employeeEarnedLeaveDetailsList.size());
					log.info("Leave Details list -" + employeeLeaveDetailsList.size());
					visibleButton = "ADDPROMOTION";
					if (employeePromotionDetailsList.size() > 0) {
						visibleButton = "UPDATEPROMOTION";
						// employeePromotionDetail = employeePromotionDetailsList.get(0);
						// onEntityTypeChange();
					}
					visibleButton = "ADDDISCIPLINARY";
					if (employeeDisciplinaryActionList != null && employeeDisciplinaryActionList.size() > 0) {
						visibleButton = "UPDATEDISCIPLINARY";
						// employeeDisciplinaryAction = employeeDisciplinaryActionList.get(0);
						// onEntityMasterChange();
					}
					visibleButton = "ADDLeaveTravel";
					if (employeeLeaveTravelConcessionList.size() > 0) {
						visibleButton = "UPDATELeaveTravel";
						empLeaveTravelConcession = employeeLeaveTravelConcessionList.get(0);
					}
					employeeSuspensionDetails = new EmployeeSuspensionDetails();
					if (employeeContactDetails == null)
						employeeContactDetails = new EmployeeContactDetails();

					if (employeeContactDetails != null && employeeContactDetails.getLandLineNumber() != null
							&& !StringUtils.isEmpty(employeeContactDetails.getLandLineNumber())) {
						log.info("landlinenumber edit==>" + employeeContactDetails.getLandLineNumber());
						try {
							landlineCode = employeeContactDetails.getLandLineNumber().split("-")[0];
							landlineNumber = employeeContactDetails.getLandLineNumber().split("-")[1];
						} catch (Exception e) {
							log.error(":: Exception occured while splitiong landline number ::");
						}
					} else {
						landlineCode = "";
						landlineNumber = "";
					}
					if (employeeMaster.getEmployeeScannedDocumentList() != null
							&& employeeMaster.getEmployeeScannedDocumentList().size() > 0) {
						employeeScannedDocumentList = employeeMaster.getEmployeeScannedDocumentList();
					} else {
						employeeScannedDocumentList = new ArrayList<EmployeeScannedDocument>();
					}
				} else {
					log.info("EmployeeMaster is Null");
				}
			} else {
				return "/pages/personnelHR/listEmployeeServiceRegister.xhtml?faces-redirect=true";
			}
			return "/pages/personnelHR/creatEmployeeServicePrimary.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author krishnakumar
	 * @return
	 */
	public String viewEmployee() {
		setReportingTopfNumber(null);
		try {

			if (selectedEmployee == null) {
				errorMap.notify(ErrorDescription.SELECT_EMPLOYEE_ID.getErrorCode());
				return null;
			}
			log.info("Selected Employee  Id : : " + selectedEmployee.getId());
			viewFlag = "VIEWEMPLOYEE";
			employeeMaster = getByEmployeeId();
			log.info("employeeMaster==>");
			if (employeeMaster.getEmployeeType() != null) {
				employeeTypeName = employeeMaster.getEmployeeType().getName();
			}

			employeePersonalInfoGeneral = employeeMaster.getPersonaInfoGeneral();
			employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
			if (employeePersonalInfo != null) {
				setReportingTopfNumber(employeePersonalInfo.getReportingTopfNumber());
			}
			employeeContactDetails = employeeMaster.getContactDetails();
			if (employeeContactDetails == null) {
				employeeContactDetails = new EmployeeContactDetails();
			}

			log.info("getLandLineNumber==>" + employeeContactDetails.getLandLineNumber());
			if (employeeContactDetails != null && employeeContactDetails.getLandLineNumber() != null
					&& !StringUtils.isEmpty(employeeContactDetails.getLandLineNumber())) {
				log.info("landlinenumber edit==> " + employeeContactDetails.getLandLineNumber());

				try {
					String[] landlineNumberValue = employeeContactDetails.getLandLineNumber().split("-");
					if (landlineNumberValue != null && landlineNumberValue.length > 0) {
						landlineCode = employeeContactDetails.getLandLineNumber().split("-")[0];
						if (landlineNumberValue.length > 1) {
							landlineNumber = employeeContactDetails.getLandLineNumber().split("-")[1];
						}
					}
				} catch (Exception e) {
					log.error(":: Exception occured while splitiong landline number ::", e);
				}
			}
			employeePayscaleInformationList = employeeMaster.getEmployeePayscaleInformationList();
			employeeFamilyDetailsList = employeeMaster.getFamilyDetailsList();
			employeeIncrementDetailsList = employeeMaster.getIncrementDetailsList();
			educationQualificationList = employeeMaster.getEducationList();
			experienceDetailsList = employeeMaster.getEmpExperienceDetailsList();
			achievementsList = employeeMaster.getAchievementList();
			employeePromotionDetailsList = employeeMaster.getEmployeePromotionDetails();
			employeeTransferDetailsList = employeeMaster.getTransferDetailsList();
			interchangeDetailsList = employeeMaster.getInterchangeDetailsList();
			employeeDisciplinaryActionList = employeeMaster.getDisciplinaryActionList();
			employeeSuspensionDetailsList = employeeMaster.getEmpSuspensionDetailsList();
			employeeLeaveDetailsList = new ArrayList<>();
			employeeEarnedLeaveDetailsList = new ArrayList<>();
			log.info("Leave Details list -" + employeeMaster.getEmployeeLeaveDeatailsList().size());
			for (EmployeeLeaveDetails details : employeeMaster.getEmployeeLeaveDeatailsList()) {
				LeaveTypeMaster leaveTypeMasters = details.getLeaveTypeMaster();
				String leaveTypeCode = null;
				if (leaveTypeMasters != null) {
					leaveTypeCode = leaveTypeMasters.getCode();
					if (ESRLeaveType.EL.equals(leaveTypeCode)) {
						employeeEarnedLeaveDetailsList.add(details);
					} else {
						employeeLeaveDetailsList.add(details);
					}
				} else {
					employeeLeaveDetailsList.add(details);
				}
			}
			log.info("Earned Leave Details list -" + employeeEarnedLeaveDetailsList.size());
			log.info("Leave Details list -" + employeeLeaveDetailsList.size());

			employeeLeaveTravelConcessionList = employeeMaster.getEmployeeLeaveTravelConcession();
			employeeScannedDocumentList = employeeMaster.getEmployeeScannedDocumentList();
			//
			return "/pages/personnelHR/createEmployeePrimaryInfo.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Bijay
	 * @return string
	 */
	public String updateEmployee() {

		try {
			log.info("<---- : Update Employee Details :------>");
			if (employeeMaster == null) {
				log.error("Update faild Due to employee master is null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (employeeMaster.getPhoto() == null) {
				log.error("Update faild due to photo is not available");
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_NOT_NULL.getErrorCode());
				return null;
			}

			if (employeeMaster.getDob() != null) {
				int age = AppUtil.getAge(employeeMaster.getDob());
				log.info("Employee Age :: " + age);
				if (age < 18) {
					log.error("Employee age shold be in between 18 to 60 years");
					errorMap.notify(ErrorDescription.EMPLOYEE_AGE_INVALID.getErrorCode());
					return null;
				}
			}
			/*
			 * if (employeeMaster.getMiddleName() != null &&
			 * !employeeMaster.getMiddleName().isEmpty() &&
			 * !FormValidation.validateLength(3,50, employeeMaster.getMiddleName())) {
			 * log.error(" Employee middle name length sould be 3 to 5 character ");
			 * errorMap.notify(ErrorDescription.EMP_MIDDLENAME_LENGTH_VALIDATION.
			 * getErrorCode()); return null; } if (employeeMaster.getLocalMiddleName() !=
			 * null && !employeeMaster.getLocalMiddleName().isEmpty() &&
			 * !FormValidation.validateLength(3,50, employeeMaster.getLocalMiddleName())) {
			 * log.error(" Employee local middle name length sould be 3 to 5 character ");
			 * errorMap.notify(ErrorDescription.EMP_LOCAL_MIDDLENAME_LENGTH_VALIDATION.
			 * getErrorCode()); return null; }
			 */
			if (!EmployeeValidator.validateMobileNo(employeeMaster.getContactNumber())) {
				log.error(" Employee Invalid Contact Number ");
				errorMap.notify(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getErrorCode());
				return null;
			}
			if (!EmployeeValidator.validateEmail(employeeMaster.getEmailId())) {
				log.error(" Employee Invalid Email Id ");
				errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getErrorCode());
				return null;
			}
//			employeeMaster.setModifiedBy(loginBean.getUserDetailSession());
//			employeeMaster.setModifiedDate(new Date());
			log.info("page name------------>" + employeeMaster.getPageName());
			String url = EMP_SERVER_URL + "/update";
			log.info("Update Employee Service called : - " + url);
			BaseDTO baseDTO = httpService.put(url, employeeMaster);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			log.info(" Deserialized Employee Object::");
			log.info("Employee Update Successfully for Employee id  ::  " + employeeMaster.getId() + " Employee Code ::"
					+ employeeMaster.getEmpCode());
			employeeTypeName = employeeMaster.getEmployeeType().getName();
			employeeFamilyDetailsList = employeeMaster.getFamilyDetailsList();
			if (educationQualificationList.size() == 0) {
				educationQualificationList = new ArrayList<EmployeeEducationQualification>();
				educationQualificationList.add(new EmployeeEducationQualification());
			}
			if (employeePayscaleInformationList.size() == 0) {
				employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();
				employeePayscaleInformationList.add(new EmployeePayscaleInformation());
			}
			if (achievementsList.size() == 0) {
				achievementsList = new ArrayList<EmployeeAchievements>();
				achievementsList.add(new EmployeeAchievements());
			}
			employeeTransferDetailsList = employeeMaster.getTransferDetailsList();
			log.info("employeeTransferDetailsList====>" + employeeTransferDetailsList);
			if (employeeTransferDetailsList.size() == 0) {
				employeeTransferDetailsList = new ArrayList<EmployeeTransferDetails>();
			}
			interchangeDetailsList = employeeMaster.getInterchangeDetailsList();
			log.info("interchangeDetailsList====>" + interchangeDetailsList);
			if (interchangeDetailsList.size() == 0) {
				interchangeDetailsList = new ArrayList<EmployeeInterchangeDetails>();
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee Update SuccessFully " + dntNtfyMesgDis);
				if (!dntNtfyMesgDis)
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.PRIMARY_INFO_UPDATED_SUCCESSFULLY).getCode());
				else
					dntNtfyMesgDis = false;

				loadEmployeeTab();
				return "/pages/personnelHR/createEmployeeAdditionalInfo.xhtml?faces-redirect=true";
			} else {
				log.error("Employee Update failed with error code" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Bijay
	 * @return string
	 */
	public String saveEmployee() {
		log.info("<---- : Save Employee Details :------>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (employeeMaster == null) {
				log.error("Update faild Due to employee master is null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (employeeMaster.getPhoto() == null) {
				log.error("Update faild due to photo is not available");
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_NOT_NULL.getErrorCode());
				return null;
			}

			if (employeeMaster.getDob() != null) {
				int age = AppUtil.getAge(employeeMaster.getDob());
				log.info("Employee Age :: " + age);
				if (age < 18) {
					log.error("Employee age should be more than 18 years");
					errorMap.notify(ErrorDescription.EMPLOYEE_AGE_INVALID.getErrorCode());
					return null;
				}
			}
			/*
			 * if (employeeMaster.getMiddleName() != null &&
			 * !employeeMaster.getMiddleName().isEmpty() &&
			 * !FormValidation.validateLength(3,50, employeeMaster.getMiddleName())) {
			 * log.error(" Employee middle name length sould be 3 to 5 character ");
			 * errorMap.notify(ErrorDescription.EMP_MIDDLENAME_LENGTH_VALIDATION.
			 * getErrorCode()); return null; } if (employeeMaster.getLocalMiddleName() !=
			 * null && !employeeMaster.getLocalMiddleName().isEmpty() &&
			 * !FormValidation.validateLength(3,50, employeeMaster.getLocalMiddleName())) {
			 * log.error(" Employee local middle name length sould be 3 to 5 character ");
			 * errorMap.notify(ErrorDescription.EMP_LOCAL_MIDDLENAME_LENGTH_VALIDATION.
			 * getErrorCode()); return null; }
			 */
			if (!EmployeeValidator.validateMobileNo(employeeMaster.getContactNumber())) {
				log.error(" Employee Invalid Contact Number ");
				errorMap.notify(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getErrorCode());
				return null;
			}

			if (!EmployeeValidator.validateEmail(employeeMaster.getEmailId())) {
				log.error(" Employee Invalid Email Id ");
				errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getErrorCode());
				return null;
			}
//			employeeMaster.setCreatedBy(loginBean.getUserDetailSession());
//			employeeMaster.setCreatedDate(new Date());
			employeeMaster.setActiveStatus(true);
			url = EMP_SERVER_URL + "/create";
			log.info(" Create Employee Service called : : " + url);
			baseDTO = httpService.post(url, employeeMaster);
			log.info("Create Employee Service REsponse :: " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);

			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			log.info("Employee Created Successfully for Employee  ::  ");

			if (employeeMaster.getId() != null) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dialogCreate').show();");
			}
			educationQualificationList = new ArrayList<EmployeeEducationQualification>();
			educationQualificationList.add(new EmployeeEducationQualification());

			employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();
			employeePayscaleInformationList.add(new EmployeePayscaleInformation());

			employeeFamilyDetailsList = new ArrayList<EmployeeFamilyDetails>();
			employeeFamilyDetailsList = employeeMaster.getFamilyDetailsList();
			employeeTypeName = employeeMaster.getEmployeeType().getName();
			employeeTransferDetailsList = employeeMaster.getTransferDetailsList();
			log.info("employeeTransferDetailsList====>" + employeeTransferDetailsList);
			if (employeeTransferDetailsList.size() == 0) {
				employeeTransferDetailsList = new ArrayList<EmployeeTransferDetails>();
			}
			interchangeDetailsList = employeeMaster.getInterchangeDetailsList();
			log.info("interchangeDetailsList====>" + interchangeDetailsList);
			if (interchangeDetailsList.size() == 0) {
				interchangeDetailsList = new ArrayList<EmployeeInterchangeDetails>();
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee Created SuccessFully ");
				errorMap.notify(ErrorDescription.EMPLOYEE_CREATED_SUCCESSFULLY.getErrorCode());
				loadEmployeeTab();
				return null;
			} else {
				log.error("Employee Update failed with status code" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(" Exception Occured while saving employee", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public boolean validateEducationDetails() {
		Iterator<EmployeeEducationQualification> employeeEducationQualificationItr = educationQualificationList
				.iterator();
		while (employeeEducationQualificationItr.hasNext()) {
			EmployeeEducationQualification educationDetails = employeeEducationQualificationItr.next();

			if (educationDetails.getRegisterNo() == null || StringUtils.isEmpty(educationDetails.getRegisterNo())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_REGISTER_NO_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getInstitutionName() == null
					|| StringUtils.isEmpty(educationDetails.getInstitutionName())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_INSTITUTE_NAME_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getQualificationMaster() == null
					|| StringUtils.isEmpty(educationDetails.getQualificationMaster())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_QUALIFICATION_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getUniversityName() == null
					|| StringUtils.isEmpty(educationDetails.getUniversityName())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_UNIVERSITY_NAME_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getPercentageCgpa() == null
					|| StringUtils.isEmpty(educationDetails.getPercentageCgpa())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_PERCENTAGE_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getYearOfPassing() == null
					|| StringUtils.isEmpty(educationDetails.getYearOfPassing())) {
				errorMap.notify(ErrorDescription.EDU_DETAILS_YEAR_OF_PASSING_NOT_NULL.getErrorCode());
				return false;
			}
			if (educationDetails.getRegisterNo() != null && !StringUtils.isEmpty(educationDetails.getRegisterNo())) {
				EmployeeValidator.validateMinLength(educationDetails.getRegisterNo(),
						ErrorDescription.EDU_DETAILS_REGISTER_NUM_MIN_LENGTH, 3);
				EmployeeValidator.validateMaxLength(educationDetails.getRegisterNo(),
						ErrorDescription.EDU_DETAILS_REGISTER_NUM_NOT_MORE_THAN_10, 10);
			}
			if (educationDetails.getInstitutionName() != null
					&& !StringUtils.isEmpty(educationDetails.getInstitutionName())) {
				EmployeeValidator.validateMinLength(educationDetails.getInstitutionName(),
						ErrorDescription.EDU_DETAILS_INSTITUTE_NAME_MIN_LENGTH, 3);
				EmployeeValidator.validateMaxLength(educationDetails.getInstitutionName(),
						ErrorDescription.EDU_DETAILS_INSTITUTE_NAME_NOT_MORE_THAN_50, 50);
			}
			if (educationDetails.getUniversityName() != null
					&& !StringUtils.isEmpty(educationDetails.getUniversityName())) {
				EmployeeValidator.validateMinLength(educationDetails.getUniversityName(),
						ErrorDescription.EDU_DETAILS_UNIVERSITY_NAME_MIN_LENGTH, 3);
				EmployeeValidator.validateMaxLength(educationDetails.getUniversityName(),
						ErrorDescription.EDU_DETAILS_UNIVERSITY_NAME_NOT_MORE_THAN_50, 50);
			}

		}
		return true;
	}

	public String educationDetailsSaveContinue() {
		log.info(" Employee Education Save & Continue Called with Eductatin Qualification List :: "
				+ educationQualificationList);
		try {
			boolean validEntries = true;
			// validEntries = validateEducationDetails();
			if (validEntries) {
				employeeMaster.setEducationList(educationQualificationList);
				log.info("Employee Education Save & Continue Called For  Employee Master :: ");
				String url = EMP_SERVER_URL + "/update";
				log.info(" Employee EducationDetails Called for :: " + url);
				BaseDTO educationDTO = httpService.put(url, employeeMaster);
				log.info(" Employee EducationDetails Rssponse Retured :: " + educationDTO);
				if (educationDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(educationDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;
					educationQualificationList = employeeMaster.getEducationList();
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(educationDTO.getStatusCode());
					return null;
				}
				log.info("Employee Education Details Saved Successfully for Employee  ::  ");
				if (educationDTO.getStatusCode() == 0) {
					if (educationQualificationList.get(0).getEducationId() == null) {
						log.info(" Employee Education Details Saved SuccessFully ");
						errorMap.notify(ErrorDescription.EMP_EDUCATION_DETAILS_SAVED.getErrorCode());
					} else {
						log.info(" Employee Education Details Updated SuccessFully ");
						errorMap.notify(ErrorDescription.EMP_EDUCATION_DETAILS_UPDATED.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeeWorkExperience.xhtml?faces-redirect=true";
				} else {
					log.error(" Employee Education Details Failed to  Save with status code "
							+ educationDTO.getStatusCode());
					errorMap.notify(educationDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("[Exception in Education Save Continue :]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @return
	 */
	public String personalInfoSaveContinue() {
		log.info(" Employee Personal Info Save & Continue Method Called With Personal Info General "
				+ employeePersonalInfoGeneral);
		try {

			if (employeePersonalInfoGeneral.getMaritalStatus() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_MARITAL_STATUS_NOT_NULL.getErrorCode());
				return null;
			}
			if (employeePersonalInfoGeneral.getFatherName() == null
					&& employeePersonalInfoGeneral.getSpouseName() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_FATHER_OR_SPOUSE_NAME_REQUIRED.getErrorCode());
				return null;
			}
			if (employeePersonalInfoGeneral.getFatherName() != null
					&& !employeePersonalInfoGeneral.getFatherName().isEmpty()) {

				if (employeePersonalInfoGeneral.getFatherName().length() < 3
						|| employeePersonalInfoGeneral.getFatherName().length() > 150) {
					errorMap.notify(ErrorDescription.EMPLOYEE_FATHERNAME_LENGTH_MAX.getErrorCode());
					return null;
				}
			}

			if (employeePersonalInfoGeneral.getLocalFatherName() != null
					&& !employeePersonalInfoGeneral.getLocalFatherName().isEmpty()) {

				if (employeePersonalInfoGeneral.getLocalFatherName().length() < 3
						|| employeePersonalInfoGeneral.getLocalFatherName().length() > 50) {
					errorMap.notify(ErrorDescription.EMPLOYEE_LOCALFATHERNAME_LENGTH_MAX.getErrorCode());
					return null;
				}
			}

			if (employeePersonalInfoGeneral.getSpouseName() != null
					&& !employeePersonalInfoGeneral.getSpouseName().isEmpty()) {

				if (employeePersonalInfoGeneral.getSpouseName().length() < 3
						|| employeePersonalInfoGeneral.getSpouseName().length() > 50) {
					errorMap.notify(ErrorDescription.EMPLOYEE_SPOUSENAME_LENGTH_MAX.getErrorCode());
					return null;
				}
			}
			if (employeePersonalInfoGeneral.getLocalSpouseName() != null
					&& !employeePersonalInfoGeneral.getLocalSpouseName().isEmpty()) {
				if (employeePersonalInfoGeneral.getLocalSpouseName().length() < 3
						|| employeePersonalInfoGeneral.getLocalSpouseName().length() > 50) {
					errorMap.notify(ErrorDescription.EMPLOYEE_LOCALSPOUSENAME_LENGTH_MAX.getErrorCode());
					return null;
				}
			}
			if (employeePersonalInfoGeneral.getNationality() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_NATIONALITY_NOT_NULL.getErrorCode());
				return null;
			}

			if (employeePersonalInfoGeneral.getReligion() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_RELIGION_REQUIRED.getErrorCode());
				return null;
			}

			if (employeePersonalInfoGeneral.getCommunity() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_COMMUNITY_REQUIRED.getErrorCode());
				return null;
			}
			if (employeePersonalInfoGeneral.getCaste() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_CASTE_REQUIRED.getErrorCode());
				return null;
			}

			if (employeePersonalInfoGeneral.getPersonalIdentification1() != null
					&& !employeePersonalInfoGeneral.getPersonalIdentification1().isEmpty()) {

				if (employeePersonalInfoGeneral.getPersonalIdentification1().length() < 3
						|| employeePersonalInfoGeneral.getPersonalIdentification1().length() > 150) {
					errorMap.notify(ErrorDescription.EMPLOYEE_PERSONALIDENTIFICATION1_LENGTH_MAX.getErrorCode());
					return null;
				}
			}

			if (employeePersonalInfoGeneral.getPersonalIdentification2() != null
					&& !employeePersonalInfoGeneral.getPersonalIdentification2().isEmpty()) {

				if (employeePersonalInfoGeneral.getPersonalIdentification2().length() < 3
						|| employeePersonalInfoGeneral.getPersonalIdentification2().length() > 250) {
					errorMap.notify(ErrorDescription.EMPLOYEE_PERSONALIDENTIFICATION2_LENGTH_MAX.getErrorCode());
					return null;
				}
			}

			if (employeePersonalInfoGeneral.getPanNumber() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_PAN_NUM_REQUIRED.getErrorCode());
				return null;
			}

			if (employeePersonalInfoGeneral.getAadharNumber() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_AADHAR_REQUIRED.getErrorCode());
				return null;
			}
//			employeePersonalInfoGeneral.setCreatedBy(loginBean.getUserDetailSession());
//			employeePersonalInfoGeneral.setCreatedDate(new Date());
			employeeMaster.setPersonaInfoGeneral(employeePersonalInfoGeneral);
			log.info(" Employee Personal Info Save & Continue Method Called  For Employee Master:: ");
			url = EMP_SERVER_URL + "/update";
			log.info(" Employee Personal Info Save & Continue Method URL " + url);
			BaseDTO personalInfoDto = httpService.put(url, employeeMaster);
			log.info(" Employee Personal Info Save & Continue Response  " + personalInfoDto);
			if (personalInfoDto == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(personalInfoDto.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(personalInfoDto.getStatusCode());
				return null;
			}
			log.info("Employee Personal Info Saved Successfully for Employee  ::  ");

			if (personalInfoDto.getStatusCode() == 0) {
				log.info(" Employee Additional Info Saved SuccessFully ");
				errorMap.notify(ErrorDescription.EMP_ADDITIONAL_INFO_SAVED_SUCCESS.getErrorCode());
				return "/pages/personnelHR/createEmployeeEmploymentDetails.xhtml?faces-redirect=true";
			} else {
				log.error(" Employee Additional Info Failed to  Save with status code "
						+ personalInfoDto.getStatusCode());
				errorMap.notify(personalInfoDto.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error(" Exception in Employee Personal Info Save Continue :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @param event
	 * @author Bijay
	 * @return
	 */
	public String handleFileUpload(FileUploadEvent event) {
		log.info(" App Prefference file uploader Path ::::::::::::>>>>> " + appPreference.getPrimefaceUploaderType());

		try {
			file = event.getFile();
			log.info(" Emplyee Photo Uploaded file name :: " + file.getFileName());
			log.info("Employee Photo Uploaded file Content Type : " + file.getContentType());
			String type = file.getFileName();
			if (!type.contains("png") && !type.contains("jpg") && !type.contains("gif") && !type.contains("jpeg")) {
				employeeMaster.setPhoto(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_FORMAT.getErrorCode());
				log.info(ErrorCodeDescription.EMPLOYEE_PHOTO_FORMAT.getErrorDescription());
				return null;
			}
			long size = file.getSize();
			size = size / 1000;
			log.info(" Uploaded Employee Photo File Size in KB " + size);
			log.info(" Expected Photo Size ::   " + employeePhotoSize);
			if (size > employeePhotoSize) {
				employeeMaster.setPhoto(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return null;
			}

			byte[] foto = IOUtils.toByteArray(file.getInputstream());
			employeeMaster.setPhoto(foto);
		} catch (Exception e) {
			log.error(" Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.EMP_PHOTO_UPLOAD_FAILED.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Bijay
	 * @return
	 */
	public StreamedContent getUploadedImage() {
		try {
			if (employeeMaster == null || employeeMaster.getPhoto() == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				return new DefaultStreamedContent(
						externalContext.getResourceAsStream("/assets/images/profile-photo.png"), "image/png");
			}
			return new DefaultStreamedContent(new ByteArrayInputStream(employeeMaster.getPhoto()), "image/png");
		} catch (Exception e) {
			log.error(" Error in while getting uploaded employee Image", e);
			errorMap.notify(ErrorDescription.EMP_PHOTO_DISPLAY_FAILED.getErrorCode());
			return null;
		}
	}

	public StreamedContent getEducationDocument() {
		try {
			if (educationQualificationList == null || educationQualificationList.get(0).getDocumentPath() == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				return new DefaultStreamedContent(
						externalContext.getResourceAsStream("/assets/images/profile-photo.png"), "image/png");
			}
			return new DefaultStreamedContent(new ByteArrayInputStream(employeeMaster.getPhoto()), "image/png");
		} catch (Exception e) {
			log.error(" Error in while getting uploaded Education Document", e);
			errorMap.notify(ErrorDescription.EMP_EDU_DOC_DISPLAY_FAILED.getErrorCode());
			return null;
		}
	}

	/**
	 * @author Bijay
	 * @return
	 */
	public String deleteEmployee() {
		try {
			viewFlag = "DELETEEMPLOYEE";
			if (selectedEmployee == null) {
				errorMap.notify(ErrorDescription.SELECT_EMPLOYEE_ID.getErrorCode());
				addButtonFlag = false;
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
		}
		addButtonFlag = false;
		return null;
	}

	public String deleteEmployeeConfirm() {
		try {
			log.info("deleteEmployeeConfirm - Selected Employee  Id : : " + selectedEmployee.getId());
			String getUrl = EMP_SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedEmployee.getId().toString());
			log.info("EmployeeType Master Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EMPLOYEE_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @return employeeMaster
	 */
	public EmployeeMaster getByEmployeeId() {
		try {
			if (selectedEmployee == null || selectedEmployee.getId() == null) {
				log.error("No employee selected");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("getByEmployeeId - Selected Employee  Id : : " + selectedEmployee.getId());
			/*
			 * String getUrl = EMP_SERVER_URL + "/get/id/:id"; url = getUrl.replace(":id",
			 * selectedEmployee.getId().toString());
			 */

			String url = EMP_SERVER_URL + "/get/id/" + selectedEmployee.getId();
			log.info("Employee Get By Id  URL: - " + url);
			/*
			 * Get Employee by Id
			 */
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("getByEmployeeId-Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
			});

			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeMaster;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public void clearEducationDetails() {
		educationQualificationList = new ArrayList<EmployeeEducationQualification>();
		educationQualificationList.add(new EmployeeEducationQualification());
	}

	public void addEmployeeEducationRow() {
		log.info("[ Add Education Row List Size : {} ]", educationQualificationList.size());
		int i = 1;
		if (educationQualificationList.size() < employeeEducationMaxRow) {
			EmployeeEducationQualification educationQualification = new EmployeeEducationQualification();
			educationQualification.setRowIndex(i);
			educationQualificationList.add(educationQualification);
		} else {
			errorMap.notify(ErrorDescription.getError(ErrorCode.MAX_RECORD_EXCEED).getCode());
			log.error("Education Qualification max row " + employeeEducationMaxRow + " exceeded");
		}
		i++;
	}

	public void addEmployeePayscaleRow() {
		log.info("[ Add Employee Payscale Row List Size : {} ]", employeePayscaleInformationList.size());
		if (employeePayscaleInformationList.size() < employeePayscaleMaxRow) {
			employeePayscaleInformationList.add(new EmployeePayscaleInformation());
		} else {
			errorMap.notify(ErrorDescription.getError(ErrorCode.MAX_RECORD_EXCEED).getCode());
			log.error("Payscale Information max row " + employeePayscaleMaxRow + " exceeded");
		}
	}

	/**
	 * Delete Education Qualification Record
	 * 
	 * @param employeeEducationQualification
	 * @return
	 */
	public void deleteEducationRecord(EmployeeEducationQualification employeeEducationQualification) {
		log.info("Delete Single Record in Employee Education Qualification :: " + employeeEducationQualification);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				educationQualificationList.remove(employeeEducationQualification);
			}
			if (action.equals("EDIT")) {
				log.info("Education Qualificatin id to delete  : :" + employeeEducationQualification.getEducationId());
				if (employeeEducationQualification.getEducationId() != null) {
					String url = EMP_SERVER_URL + "/deleteemployeeeducation/id/"
							+ employeeEducationQualification.getEducationId();
					log.info(" Employee Delete Education URL :- " + url);
					BaseDTO baseDTO = httpService.delete(url);
					log.info(" Employee Delete Education response :- " + url);
					if (baseDTO == null) {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return;
					}
					if (baseDTO.getStatusCode() == 0) {
						log.info(" Education record deleted Successfully");
						educationQualificationList.remove(employeeEducationQualification);
						errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
						RequestContext.getCurrentInstance().update("educationForm:educationPanel");
					} else {
						log.error(" Error Occured while deleting education record with status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
				} else {
					educationQualificationList.remove(employeeEducationQualification);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					RequestContext.getCurrentInstance().update("educationForm:educationPanel");
				}
			}

		} catch (Exception e) {
			log.error("[ Exception in Delete Employee Record : ]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * Purpose : to view or hidden the insurance dropdown
	 * 
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return void
	 */
	public void disableInsurance(EmployeeFamilyDetails family) {
		log.info(" Disable Family Insurance Value is " + family.getInsuranceAvailable());
		if (family.getInsuranceAvailable() == true) {
			activeStatus = true;
		} else {
			activeStatus = false;
		}
	}

	/**
	 * Purpose : to save the employee family
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public boolean validateFamilyDetails() {
		Iterator<EmployeeFamilyDetails> employeeFamilyDetailsItr = employeeFamilyDetailsList.iterator();
		while (employeeFamilyDetailsItr.hasNext()) {
			EmployeeFamilyDetails empFamilyDetails = employeeFamilyDetailsItr.next();
			if (empFamilyDetails.getDependentName() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_DEPENDENT_NAME.getErrorCode());
				return false;
			}
			if (empFamilyDetails.getRelationshipMaster() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_RELATIONSHIP.getErrorCode());
				return false;
			}
			if (empFamilyDetails.getGenderMaster() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_GENDER_MASTER.getErrorCode());
				return false;
			}
			if (empFamilyDetails.getDateOfBirth() == null) {
				errorMap.notify(ErrorDescription.FAMILY_DOB_MANDATORY.getErrorCode());
				return false;
			}

			if (empFamilyDetails.getDependentName() != null
					&& !StringUtils.isEmpty(empFamilyDetails.getDependentName())) {
				if (empFamilyDetails.getDependentName().length() < 3
						|| 95 < empFamilyDetails.getDependentName().length()) {
					errorMap.notify(ErrorDescription.EMPLOYEE_DEPENDENT_MAX_MIN.getErrorCode());
					return false;
				}
			}
			if (StringUtils.isEmpty(empFamilyDetails.getLocalDependentName())) {
				empFamilyDetails.setLocalDependentName(null);
			}
			if (empFamilyDetails.getLocalDependentName() != null
					&& !StringUtils.isEmpty(empFamilyDetails.getLocalDependentName())) {
				if (empFamilyDetails.getLocalDependentName().length() < 3
						|| 95 < empFamilyDetails.getLocalDependentName().length()) {
					errorMap.notify(ErrorDescription.EMPLOYEE_I_DEPENDENT_MAX_MIN.getErrorCode());
					return false;
				}
			}
		}
		return true;
	}

	public String saveFamily() {
		log.info("<---- : Save Employee Family Details :------>" + employeeFamilyDetailsList);
		try {
			if (employeeFamilyDetailsList == null || employeeFamilyDetailsList.isEmpty()) {
				log.error(" Employee Family List is Empty ");
				errorMap.notify(ErrorDescription.EMP_FAMILY_LIST_EMPTY.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validateFamilyDetails();
			if (validEntries) {
				employeeMaster.setFamilyDetailsList(employeeFamilyDetailsList);
				log.info("Save Employee Family Details for Employee : ");
				url = EMP_SERVER_URL + "/update";
				log.info("Save Employee Family Details URL : ");
				BaseDTO baseDTO = httpService.put(url, employeeMaster);
				log.info("Save Employee Family Details Response : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;
					employeeFamilyDetailsList = employeeMaster.getFamilyDetailsList();
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
				log.info("Family Details Saved For Employee ::");
				if (employeeMaster.getEducationList() != null && employeeMaster.getEducationList().size() > 0) {
					educationQualificationList = employeeMaster.getEducationList();
				} else {
					educationQualificationList = new ArrayList<EmployeeEducationQualification>();
					educationQualificationList.add(new EmployeeEducationQualification());
				}
				if (baseDTO.getStatusCode() == 0) {

					if (employeeFamilyDetailsList.get(0).getId() == null) {
						log.info("Employee Family Details Saved Succuessfully ");
						errorMap.notify(ErrorDescription.EMP_FAMILY_DETAILS_SAVED.getErrorCode());
					} else {
						log.info("Employee Family Details Updated Succuessfully ");
						errorMap.notify(ErrorDescription.EMP_FAMILY_DETAILS_UPDATED.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeeEducationDetails.xhtml?faces-redirect=true";
				} else {
					log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception in Family Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Purpose : to load family details buffer table
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void loadFamilyDetails() {
		log.info("loadFamilyDetails Start==>" + action);
		visiableFamButton = "ADDBUTTON";
		employeeFamilyDetail = new EmployeeFamilyDetails();
		log.info("loadFamilyDetails End==>");
	}

	public void loadNewFamilyDetails() {
		log.info("loadFamilyDetails Start==>" + action);
		visiableFamButton = "ADDBUTTON";
		employeeFamilyDetailsList.add(employeeFamilyDetail);
		employeeFamilyDetail = new EmployeeFamilyDetails();
		log.info("loadFamilyDetails End==>");
	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 */
	public void addFamily(EmployeeFamilyDetails empFamily) {
		log.info("addFamily Start==>");
		employeeFamilyDetail = empFamily;
		familyFlag = "ADDFAMILY";
		visiableFamButton = "EDITBUTTON";
		log.info("addFamily employeeFamilyDetails==>" + empFamily);
		// employeeFamilyDetailsList.remove(empFamily);
		log.info("addFamily End==>");
	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void viewFamily(EmployeeFamilyDetails empFamily) {
		log.info("viewFamily Start==>");
		employeeFamilyDetail = empFamily;
		familyFlag = "VIEWFAMILY";
		log.info("viewFamily employeeFamilyDetails==>" + empFamily);
		log.info("viewFamily End==>");
	}

	/**
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void closeFamily() {
		log.info("closeFamily Start==>");
		employeeFamilyDetail = new EmployeeFamilyDetails();
		familyFlag = "EDITFAMILY";
		RequestContext.getCurrentInstance().update("empFamilyForm:addpanel");
		log.info("closeFamily End==>");
	}

	/**
	 * Propose : delete the table row
	 * 
	 * @author krishnakumar
	 * @param employeeFamilyDetails
	 * @return String
	 */
	public void deleteFamily(EmployeeFamilyDetails employeeFamilyDetails) {
		log.info(" Delete Single Record  for Employee Family Details :: " + employeeFamilyDetails);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				if (employeeFamilyDetailsList != null && employeeFamilyDetailsList.size() > 0) {
					employeeFamilyDetailsList.remove(employeeFamilyDetails);
					log.info(" Employee Family Row Deleted ");
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				} else {
					log.error(" Employee Family Details List Is Empty ");
					errorMap.notify(ErrorDescription.EMP_FAMILY_LIST_EMPTY.getErrorCode());
				}

			}
			if (action.equals("EDIT")) {
				if (employeeFamilyDetails.getId() != null) {
					url = EMP_SERVER_URL + "/deleteEmployeeFamily/id/" + employeeFamilyDetails.getId();
					log.info("Delete Single Record  for Employee Family URL ::  " + url);
					BaseDTO baseDTO = httpService.delete(url);
					log.info("Delete Single Record  for Employee Family Response  :: " + baseDTO);
					if (baseDTO == null) {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return;
					}
					if (baseDTO.getStatusCode() == 0) {
						employeeFamilyDetailsList.remove(employeeFamilyDetails);
						errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
						RequestContext.getCurrentInstance().update("educationForm:familyPanel");
						log.info("Employee Family Details Saved Succuessfully s");
					} else {
						log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
				} else {
					log.error("Employee family deletion failed  due to id null");
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				}

			}

		} catch (Exception e) {
			log.error(" Exception in Delete Employee Family Record : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * Purpose : calculate age passing date
	 * 
	 * @author krishnakumar
	 */
	public void onDateSelect() {
		try {
			log.info("onDateSelect Method Called :: " + employeeFamilyDetail.getDateOfBirth());
			Date birthDay = employeeFamilyDetail.getDateOfBirth();
			int age = AppUtil.getAge(birthDay);
			log.error(" Employee Family Age :: " + age);
			employeeFamilyDetail.setAge((long) age);
		} catch (Exception e) {
			log.error(" Exception In EmployeeMasterBean onDateSelect Method ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	/**
	 * @author bijay
	 */
	public void changeSalutation() {
		log.info("employeeMaster Change Salutation Called----->");
		if (employeeMaster != null && employeeMaster.getGender() != null
				&& employeeMaster.getGender().getName().equalsIgnoreCase("Male") && salutationList.size() > 0) {
			for (SalutationMaster salutation : salutationList) {
				if (salutation.getSalutationName().equalsIgnoreCase("Mr")) {
					employeeMaster.setSalutation(salutation);
				}

			}
		} else if (employeeMaster != null && employeeMaster.getGender() != null
				&& employeeMaster.getGender().getName().equalsIgnoreCase("Female") && salutationList.size() > 0) {
			for (SalutationMaster salutation : salutationList) {
				if (salutation.getSalutationName().equalsIgnoreCase("Mrs")) {
					employeeMaster.setSalutation(salutation);
				}

			}
		}
	}

	/**
	 * @author bijay
	 */
	public void changeGender() {
		log.info("employeeMaster Change Gender Called----->");
		if (employeeMaster != null && employeeMaster.getSalutation() != null
				&& employeeMaster.getSalutation().getSalutationName().equalsIgnoreCase("Mr") && genderList.size() > 0) {
			for (GenderMaster gender : genderList) {
				if (gender.getName().equalsIgnoreCase("Male")) {
					employeeMaster.setGender(gender);
				}
			}

		} else if (employeeMaster != null && employeeMaster.getSalutation() != null
				&& (employeeMaster.getSalutation().getSalutationName().equalsIgnoreCase("Mrs")
						|| employeeMaster.getSalutation().getSalutationName().equalsIgnoreCase("Miss"))
				&& genderList.size() > 0) {
			for (GenderMaster gender : genderList) {
				if (gender.getName().equalsIgnoreCase("Female")) {
					employeeMaster.setGender(gender);
				}
			}
		}
	}

	/**
	 * @author bijay
	 */
	public String loadEmployeeTab() {
		log.info("Load Employee Tab Called withs property Action :: " + action + " & Employee Master ");
		try {
			if (action.equalsIgnoreCase("ADD")) {
				employeePersonalInfoGeneral = new EmployeePersonalInfoGeneral();
				employeeContactDetails = new EmployeeContactDetails();
				employeePersonalInfo = new EmployeePersonalInfoEmployment();
			}
			if (employeeMaster.getId() != null) {
				if (employeeMaster.getPersonaInfoGeneral() != null
						&& employeeMaster.getPersonaInfoGeneral().getEmployeeMaster() != null) {
					log.info(" Personal info Additional tab value set for employee id " + employeeMaster.getId());
					employeePersonalInfoGeneral = employeeMaster.getPersonaInfoGeneral();
				} else {
					employeePersonalInfoGeneral = new EmployeePersonalInfoGeneral();
				}

				if (employeeMaster.getPersonalInfoEmployment() != null
						&& employeeMaster.getPersonalInfoEmployment().getEmployeeMaster().getId() != null) {
					log.info(" Personal info Employement for employee id " + employeeMaster.getId());
					employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
					employeeTypeName = employeeMaster.getEmployeeType().getName();
				} else {
					employeePersonalInfo = new EmployeePersonalInfoEmployment();
				}
			}
		} catch (Exception e) {
			log.error(" Error Occured while loading Employee Tab :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return "/pages/personnelHR/createEmployeeAdditionalInfo.xhtml?faces-redirect=true";
	}

	/**
	 * Documents Upload
	 * 
	 * @param event
	 */
	public void documentUpload(FileUploadEvent event) {
		log.info("Seletcted Employee Education To Upload Document :");
		try {
			if (selectedEducation == null) {
				log.error(" Selected Empoyee Education Object is null :: ");
				errorMap.notify(ErrorDescription.EMP_SELECTED_EDU_DETAIL_EMPTY.getErrorCode());
				return;
			}
			if (event == null || event.getFile() == null) {
				log.error(" Selected Empoyee Document is null :: ");
				errorMap.notify(ErrorDescription.EMP_EDU_DOC_EMPTY.getErrorCode());
				return;
			}
			docFileName = FilenameUtils.getName(event.getFile().getFileName());
			file = event.getFile();
			byte[] doc = IOUtils.toByteArray(file.getInputstream());
			log.info(" Empoyee Education File Name is :: ", docFileName);
			if (educationQualificationList != null && educationQualificationList.size() > 0) {
				for (EmployeeEducationQualification education : educationQualificationList) {
					if (selectedEducation.getRegisterNo().equals(education.getRegisterNo())) {
						education.setDocumentPath(docFileName);
						education.setEducationDocument(doc);
						/*
						 * educationQualificationList.get(education.getRowIndex()).setDocumentPath(
						 * docFileName);
						 * educationQualificationList.get(education.getRowIndex()).setEducationDocument(
						 * doc);
						 */
					}
				}
			} else {
				log.error(" Education Qualification List  is empty :: ");
				errorMap.notify(ErrorDescription.EMP_SELECTED_EDU_LIST_EMPTY.getErrorCode());
				return;
			}

			log.info("  Employee Education Uploaded Document List :: ", educationQualificationList.size());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dialog').hide();");
			context.update("educationForm:uploadedDocumnets");
		} catch (Exception e) {
			log.error(" Exception Occured While Uploading education Document ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	public boolean validatePayscaleInformation() {
		Iterator<EmployeePayscaleInformation> employeePayscaleInformationItr = employeePayscaleInformationList
				.iterator();
		while (employeePayscaleInformationItr.hasNext()) {
			EmployeePayscaleInformation employeePayscale = employeePayscaleInformationItr.next();

			if (employeePayscale.getDesignation() == null || StringUtils.isEmpty(employeePayscale.getDesignation())) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return false;
			}
			if (employeePayscale.getRevisedBasicPay() == null
					|| StringUtils.isEmpty(employeePayscale.getRevisedBasicPay())) {
				errorMap.notify(ErrorDescription.EMP_REVISED_BASICPAY_EMPTY.getErrorCode());
				return false;
			}
			if (employeePayscale.getPayScaleMaster() == null
					|| StringUtils.isEmpty(employeePayscale.getPayScaleMaster())) {
				errorMap.notify(ErrorDescription.EMP_PAY_SCALE_EMPTY.getErrorCode());
				return false;
			}
			if (employeePayscale.getEffectiveDate() == null
					|| StringUtils.isEmpty(employeePayscale.getEffectiveDate())) {
				errorMap.notify(ErrorDescription.EMP_PAYSCALE_EFFECTIVE_DATE_EMPTY.getErrorCode());
				return false;
			}
		}
		if (employeePayscaleInformationList == null || employeePayscaleInformationList.size() == 0) {
			log.info(" Employee PayScale List is Empty");
			errorMap.notify(ErrorDescription.EMP_PAYSCALE_LIST_EMPTY.getErrorCode());
			return false;
		}
		for (EmployeePayscaleInformation employeePayscale : employeePayscaleInformationList) {
			if (employeePayscale.getReferenceNumber() != null) {
				boolean isValid = FormValidation.validateLength(3, 50, employeePayscale.getReferenceNumber());
				if (!isValid) {
					errorMap.notify(ErrorDescription.REFERENCE_NO_LENGTH.getErrorCode());
					return false;
				}
			}
			if (employeePayscale.getDesignation() == null) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				log.error(" Employee PayScale Designation is Empty ");
				return false;
			}
			if (employeePayscale.getPayScaleMaster() == null) {
				errorMap.notify(ErrorDescription.EMP_PAY_SCALE_EMPTY.getErrorCode());
				log.error(" Employee PayScale is Empty ");
				return false;
			}
			if (employeePayscale.getEffectiveDate() == null) {
				errorMap.notify(ErrorDescription.EMP_PAYSCALE_EFFECTIVE_DATE_EMPTY.getErrorCode());
				log.error(" Employee PayScale Effective Date is Empty ");
				return false;
			}
		}
		return true;
	}

	public String payscaleInfoSaveUpdate() {
		log.info("Employee Master PayScale Info List " + employeePayscaleInformationList);

		try {
			boolean validEntries = true;
			// validEntries = validatePayscaleInformation();
			if (validEntries) {
				employeeMaster.setEmployeePayscaleInformationList(employeePayscaleInformationList);
				log.info("Request Employee Master Object For PayScale Info Save =====");
				url = EMP_SERVER_URL + "/update";

				log.info("Request URL For PayScale Info Save :: " + url);
				BaseDTO payscaleDTO = httpService.put(url, employeeMaster);
				log.info("Response For PayScale Info Save :: " + payscaleDTO);

				if (payscaleDTO == null) {
					log.error("Base dto returned null values");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(payscaleDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;
					employeePayscaleInformationList = employeeMaster.getEmployeePayscaleInformationList();
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(payscaleDTO.getStatusCode());
					return null;
				}
				if (payscaleDTO.getStatusCode() == 0) {
					if (employeePayscaleInformationList.get(0).getId() == null) {
						log.info(":: Employee PayScale Saved SuccessFully ::");
						errorMap.notify(ErrorDescription.EMP_PAYSCALE_SAVED.getErrorCode());
					} else {
						log.info("Employee PayScale Saved SuccessFully ");
						errorMap.notify(ErrorDescription.EMP_PAYSCALE_UPDATED.getErrorCode());
					}

				} else {
					log.error("Employee PayScale Add failed with error code" + payscaleDTO.getStatusCode());
					errorMap.notify(payscaleDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(" Exception in Payscale Info Save & Update : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeAddressContact.xhtml?faces-redirect=true";
	}

	public void addPayscaleRow() {
		log.info("Payscale List Size ::", employeePayscaleInformationList.size());
		if (employeeMaster.getId() != null && employeeMaster.getEmployeePayscaleInformationList() != null
				&& employeeMaster.getEmployeePayscaleInformationList().size() > 0) {
			employeePayscaleInformationList = employeeMaster.getEmployeePayscaleInformationList();
			if (employeePayscaleInformationList.size() < employeePayscaleMaxRow) {
				employeePayscaleInformationList.add(new EmployeePayscaleInformation());
				log.info("[ Education List On " + employeePayscaleInformationList);
			} else {
				errorMap.notify(ErrorDescription.getError(ErrorCode.MAX_RECORD_EXCEED).getCode());
				log.error("EmployeeMaster is not null Payscale Information max row " + employeePayscaleMaxRow
						+ " exceeded");
			}
		} else {
			if (employeePayscaleInformationList.size() < employeePayscaleMaxRow) {
				employeePayscaleInformationList.add(new EmployeePayscaleInformation());
				log.info("[ Education List ]" + employeePayscaleInformationList);
			} else {
				errorMap.notify(ErrorDescription.getError(ErrorCode.MAX_RECORD_EXCEED).getCode());
				log.error(
						"EmployeeMaster is null Payscale Information max row " + employeePayscaleMaxRow + " exceeded");
			}
		}
	}

	public void deletePascaleRecord(EmployeePayscaleInformation employeePayscaleInformation) {
		log.info(" Delete Employee Payscale Record Id :: " + employeePayscaleInformation.getId());
		try {
			if (employeePayscaleInformationList.isEmpty() && employeePayscaleInformationList.size() == 0) {
				log.info(" Employee Payscale Info List Is Empty  :");
				errorMap.notify(ErrorDescription.EMP_PAYSCALE_LIST_EMPTY.getErrorCode());
				return;
			}
			if (action.equalsIgnoreCase("ADD")) {
				employeePayscaleInformationList.remove(employeePayscaleInformation);
				log.info(" Employee Payscale Information Row Delete :: ");
			}
			if (action.equals("EDIT")) {
				if (employeePayscaleInformation.getId() != null) {
					url = EMP_SERVER_URL + "/deleteemployeepayscale/id/" + employeePayscaleInformation.getId();
					log.info("Delete Employee Payscale Record URL :: ", url);
					BaseDTO baseDTO = httpService.delete(url);
					log.info(" Delete Employee Payscale Record Response  :: " + baseDTO);
					if (baseDTO == null) {
						log.info(" Base DTO Returned null value :: " + baseDTO);
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
						employeePayscaleInformationList.remove(employeePayscaleInformation);

						log.info(" Employee PaySacle Info Record Deleted SuccessFully");
					} else {
						log.error(" Employee PaySacle Info Record Deletion Failed with error status :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
				} else {
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeePayscaleInformationList.remove(employeePayscaleInformation);
				}
				RequestContext.getCurrentInstance().update("payscaleForm:payscaleInfoPannel");
			}
		} catch (Exception e) {
			log.error("[ Exception in Delete Employee Record : ]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * Propose : load all Entity
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void getEntityMaster(EntityTypeMaster entityType) {
		log.info(": getEntityMaster Started " + entityType);
		try {
			if (entityType == null) {
				log.error("Entity Type Id Not Exists");
				errorMap.notify(ErrorDescription.ENTITY_TYPE_ID_NOT_EXIST.getErrorCode());
			}
			entityMasterObjList = commonDataService.getEntityMaster(entityType);
			entityMasterList = commonDataService.getEntityMaster(entityType);
			log.info("getEntityMaster end entityTypeMasterList===>" + entityTypeMasterList);

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
	}

	/**
	 * Purpose : to save employee Employment details
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveEmployeeDetails() {
		try {

			if (employeePersonalInfo.getEntityType() == null || employeePersonalInfo.getEntityType().getId() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getWorkLocation() == null
					|| employeePersonalInfo.getWorkLocation().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_WORKLOCATION_MANDATORY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getCurrentDepartment() == null
					|| employeePersonalInfo.getCurrentDepartment().getId() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_DEPENDENT_NAME.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getCurrentDesignation() == null
					|| employeePersonalInfo.getCurrentDesignation().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getSeniority() == null) {
				errorMap.notify(ErrorDescription.EMP_PROMOTION_SENIORITY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getDateOfAppointment() == null) {
				errorMap.notify(ErrorDescription.EMP_APPOINTMENT_DATE_MANDATORY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getDateOfJoining() == null) {
				errorMap.notify(ErrorDescription.EMP_JOIN_DATE_MANDATORY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getRetirementDate() == null) {
				errorMap.notify(ErrorDescription.EMP_RETIREMENT_DATE_MANDATORY.getErrorCode());
				return null;
			} else if (employeePersonalInfo.getPfNumber() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.PF_NUMBER_SHOULD_BE_MANDATORTY).getCode());
				return null;
			} else if (employeePersonalInfo.getCurrentSection() == null) {
				errorMap.notify(ErrorDescription.FILENUMBERING_SECTION_EMPTY.getErrorCode());
				return null;
			}
			log.info("<---- : Save Employee Primary Info Details :------>");

			log.info("<---- : saveEmployeeDetails employeePersonalInfo ===>" + employeePersonalInfo);
			BaseDTO baseDTO = new BaseDTO();
			isValid = FormValidation.validateTwoDates(employeePersonalInfo.getDateOfJoining(),
					employeePersonalInfo.getRetirementDate());
			log.info(" :: Date isValid : {}", isValid);
			if (!isValid) {
				log.info(" :: Invalid date ");
				errorMap.notify(ErrorDescription.EMPLOYMENT_DATE_OF_JOINING.getErrorCode());
				return null;
			}
			if (reportingTopfNumber != null && !StringUtils.isEmpty(reportingTopfNumber)) {
				if (reportingTopfNumber.split(" / ")[0] != null) {
					employeePersonalInfo.setReportingTopfNumber(reportingTopfNumber);
				}
			}

			employeeMaster.setPersonalInfoEmployment(employeePersonalInfo);
			log.info("<---- : saveEmployeeDetails :------>");
			url = EMP_SERVER_URL + "/update";
			log.info("saveEmployeeDetails url==>" + url);
			baseDTO = httpService.put(url, employeeMaster);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			employeePayscaleInformationList = employeeMaster.getEmployeePayscaleInformationList();
			if (employeePayscaleInformationList.size() == 0) {
				employeePayscaleInformationList = new ArrayList<EmployeePayscaleInformation>();
				employeePayscaleInformationList.add(new EmployeePayscaleInformation());
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employement Details Saved Succuessfully ");
				errorMap.notify(ErrorDescription.EMP_EMPLOYEEMENT_DETAILS_SAVED.getErrorCode());
				return "/pages/personnelHR/createEmployeePayscaleInformation.xhtml?faces-redirect=true";
			} else {
				log.error("Employee Details Update failed with error code" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee Details", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void onCadrePayscaleChange() {
		try {
			if (employeePayscaleInformationList == null || employeePayscaleInformationList.size() == 0
					|| employeePayscaleInformationList.get(0).getCadreMaster() == null
					|| employeePayscaleInformationList.get(0).getCadreMaster().getId() == null) {
				log.error("employeePayscaleInformationList is empty onCadrePayscaleChange");
				errorMap.notify(ErrorDescription.EMP_PAYSCALE_LIST_EMPTY.getErrorCode());
				return;
			}
			String cadreID = employeePayscaleInformationList.get(0).getCadreMaster().getId().toString();

			url = SERVER_URL + "/cadremaster/get/id/" + cadreID;
			log.info(" Cadre Master get By Id URL" + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info(" Cadre Master get By Id Response" + baseDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			cadreMaster = mapper.readValue(jsonResponse, CadreMaster.class);
			if (cadreMaster == null) {
				log.error("Employee Payscale InformationList cadreMaster failed to Deserialize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (baseDTO.getStatusCode() == 0) {
				onCadreDepartmentChange();
				onPayscaleChange();
				log.info(" Cadre Retrive Successfully");
			} else {
				log.error(" Cadre Retrive Falide with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("[ Exception in On Cadre Change : ]", e);
		}
	}

	public void onCadreDepartmentChange() {
		if (cadreMaster.getId() == null) {
			log.error("Cadre Master is null");
			errorMap.notify(ErrorDescription.EMP_CADRE_IS_NULL.getErrorCode());
			return;
		}
		try {
			url = SERVER_URL + "/designation/load/cadreid/" + cadreMaster.getId();
			log.info("Designation list based on cadre Id URL :: " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("Designation list based on cadre Id Response :: " + baseDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});
			if (designationList == null) {
				log.error("Employee Payscale InformationList designationList failed to Deserialize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("[ Designation List Retrived Sucessfully :: ");
			} else {
				log.error(" Designation List Failed to retrive  with status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("[ Exception in On Cadre Change : ]", e);
		}
	}

	public void onPayscaleChange() {

		if (cadreMaster == null || cadreMaster.getPayScaleMaster() == null
				|| cadreMaster.getPayScaleMaster().getId() == null) {
			log.error("Cadre Master is null");
			errorMap.notify(ErrorDescription.EMP_CADRE_IS_NULL.getErrorCode());
			return;
		}
		log.info(" Employee Cadre Master Payscale Id " + cadreMaster.getPayScaleMaster().getId());
		payScaleMaster = new PayScaleMaster();
		payScaleMasterList = new ArrayList<PayScaleMaster>();
		try {
			String payscaleId = cadreMaster.getPayScaleMaster().getId().toString();
			url = SERVER_URL + "/payscale/get/id/" + payscaleId;

			log.info("On Payscale change URL :" + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("On Payscale change Response DTO  :" + baseDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			payScaleMaster = mapper.readValue(jsonResponse, PayScaleMaster.class);
			if (payScaleMaster == null) {
				log.error("Employee Payscale Master failed to Deserialize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (baseDTO.getStatusCode() == 0) {
				payScaleMasterList.add(payScaleMaster);
				log.info("[ Payscale List Master Sucessfully :: ");
			} else {
				log.error(" Payscale Master Failed to retrive  with status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("[ Exception in Payscale : ]", e);
		}
	}

	public void communityFileUpload(FileUploadEvent event) {
		try {
			if (event == null || event.getFile() == null) {
				errorMap.notify(ErrorDescription.EMP_COMMUNITY_FILE_IS_EMPTY.getErrorCode());
				log.error(" Uploading Document is emprty ");
				return;
			}
			communityFile = event.getFile();
			communityFileName = communityFile.getFileName();
			log.info("Community file Name :: " + communityFile.getFileName());
			byte[] communityByte = IOUtils.toByteArray(communityFile.getInputstream());
			employeePersonalInfoGeneral.setCommunityDoc(communityByte);
			errorMap.notify(ErrorDescription.EMP_COMMUNITY_FILE_UPLOAD_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error(" Exception Occured Wile Uploading Community File :");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void panDetailsUpload(FileUploadEvent event) {
		try {
			if (event == null || event.getFile() == null) {
				errorMap.notify(ErrorDescription.EMP_PAN_FILE_IS_EMPTY.getErrorCode());
				log.error(" Uploading Document is emprty ");
				return;
			}
			panDetailsFile = event.getFile();
			panDetailsFileName = panDetailsFile.getFileName();
			log.info("Pan card file Name :: " + panDetailsFileName);
			byte[] panDetailsByte = IOUtils.toByteArray(panDetailsFile.getInputstream());
			employeePersonalInfoGeneral.setPanDoc(panDetailsByte);
			errorMap.notify(ErrorDescription.EMP_PAN_FILE_UPLOAD_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error(" Exception Occured Wile Uploading Pan Details Document :");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void exServicemanFileUpload(FileUploadEvent event) {
		try {
			if (event == null || event.getFile() == null) {
				errorMap.notify(ErrorDescription.EMP_EX_SERVICE_FILE_IS_EMPTY.getErrorCode());
				log.error(" Uploading Document is emprty ");
				return;
			}
			exServicemanFile = event.getFile();
			exServicemanFileName = exServicemanFile.getFileName();
			log.info("Ex Service Man file Name :: " + exServicemanFileName);
			byte[] exServicemanByte = IOUtils.toByteArray(exServicemanFile.getInputstream());
			employeePersonalInfoGeneral.setExServicemanDoc(exServicemanByte);
			errorMap.notify(ErrorDescription.EMP_EX_SERVICE_FILE_UPLOAD_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error(" Exception Occured Wile Uploading Ex Serviceman Document :");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void differentlyAbledFileUpload(FileUploadEvent event) {
		try {
			if (event == null || event.getFile() == null) {
				errorMap.notify(ErrorDescription.EMP_DIFFENENTLYABLED_FILE_IS_EMPTY.getErrorCode());
				log.error(" Uploading Document is emprty ");
				return;
			}
			differentlyAbledFile = event.getFile();
			differentlyAbledFileName = differentlyAbledFile.getFileName();
			log.info("Differently Abled file Name :: " + differentlyAbledFileName);
			byte[] differentlyAbledByte = IOUtils.toByteArray(differentlyAbledFile.getInputstream());
			employeePersonalInfoGeneral.setDifferentlyAbledDoc(differentlyAbledByte);
			errorMap.notify(ErrorDescription.EMP_DIFFENENTLYABLED_FILE_UPLOAD_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error(" Exception Occured Wile Uploading Differently Abled Document :");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void destituteWidowDocUpload(FileUploadEvent event) {

		try {
			if (event == null || event.getFile() == null) {
				errorMap.notify(ErrorDescription.EMP_FREEDOM_FIGHTER_FILE_IS_EMPTY.getErrorCode());
				log.error(" Uploading Document is emprty ");
				return;
			}
			freedomFighterFile = event.getFile();
			freedomFighterFileName = freedomFighterFile.getFileName();
			log.info("Freedom Fighter file Name :: " + freedomFighterFileName);
			byte[] freedomFighterByte = IOUtils.toByteArray(freedomFighterFile.getInputstream());
			employeePersonalInfoGeneral.setDestituteWidowDoc(freedomFighterByte);
			errorMap.notify(ErrorDescription.EMP_DESTITUTE_WIDOW_FILE_UPLOAD_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error(" Exception Occured Wile Uploading Freedom Fighter Document :" + e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * @Author Manjunath G
	 * 
	 *         This method is for create the new row for adding the
	 *         achievementsDetails
	 */
	public void addNewRowForAchievements() {
		log.info("Add New Row For Achievement Action Button Name is  :: ", action);
		if (achievementsList == null) {
			log.error(" Achievements list is null");
			return;
		}
		if (achievementsList.size() < employeeAchievementMaxRow) {
			achievementsList.add(new EmployeeAchievements());
			log.error(" Achievemnet List Added");
		} else {
			log.error(" Achievemnet List Exceeds ");
			errorMap.notify(ErrorDescription.getError(ErrorCode.MAX_RECORD_EXCEED).getCode());
		}
	}

	/**
	 * @author Manjunath G
	 * 
	 *         This method is for deleted EmployeementAchievements details in DB
	 * 
	 * @param employeeAchievements
	 * @return
	 */
	public String deleteAchievementsRecord(EmployeeAchievements employeeAchievements) {
		log.info(" Delete Single Record fOr employee achievement  :: " + employeeAchievements);
		try {

			if (employeeAchievements.getAchievementId() == null) {
				achievementsList.remove(employeeAchievements);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			} else {
				if (employeeAchievements == null || employeeAchievements.getAchievementId() == null) {
					log.error(" Employee Achievement is empty");
					errorMap.notify(ErrorDescription.EMP_ACHIEVEMENT_IS_EMPTY.getErrorCode());
					return null;
				}
				url = EMP_SERVER_URL + "/deleteAchievement/id/" + employeeAchievements.getAchievementId();
				log.info(" Delete Achievement Called FOr URL " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info(" Response Base Dto for Delete Achievement :: " + baseDTO);
				if (baseDTO.getStatusCode() == 0) {
					achievementsList.remove(employeeAchievements);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					log.info("Delete Single Record fOr employee achievement Successfully ");
				} else {
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					log.error("Delete Single Record fOr employee achievement Failed with status code :: "
							+ baseDTO.getStatusCode());
				}

			}

		} catch (Exception e) {
			log.error("Exception in Delete Achievements Record ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeEducationDetails.xhtml?faces-redirect=true";
	}

	/**
	 * @author Manjunath G
	 * 
	 *         This methods for save the EmployeeAchiebements details in DB.
	 * 
	 * @return baseDTO
	 */
	public String achievementsDetailsSaveContinue() {
		log.info("====>> EmployeeMasterBean inside achievementsDetailsSaveContinue() <<===" + achievementsList);
		try {

			for (EmployeeAchievements employeeAchievements : achievementsList) {
				if (employeeAchievements.getAwardName().isEmpty() && employeeAchievements.getAwardedBy().isEmpty()
						&& employeeAchievements.getAwardDate() == null) {
					errorMap.notify(ErrorDescription.ENTER_ATLEAST_ONE_VALUE.getErrorCode());
					return null;
				} else {
					if (employeeAchievements.getAwardName().trim().length() != 0
							&& employeeAchievements.getAwardName().trim().length() < 3
							&& employeeAchievements.getAwardName().trim().length() > 150) {
						errorMap.notify(ErrorDescription.VALID_AWARD_NAME.getErrorCode());
						return null;
					}
					if (employeeAchievements.getAwardedBy().trim().length() != 0
							&& employeeAchievements.getAwardedBy().trim().length() < 3
							&& employeeAchievements.getAwardName().trim().length() > 150) {
						errorMap.notify(ErrorDescription.VALID_AWARDED_BY.getErrorCode());
						return null;
					}
				}
			}
			employeeMaster.setAchievementList(achievementsList);
			log.info("Update Achievments Called For URL " + url);
			url = EMP_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, employeeMaster);
			log.info(" Update Achievments Response " + baseDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
				achievementsList = employeeMaster.getAchievementList();
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee Update SuccessFully ");
				errorMap.notify(ErrorDescription.EMPLOYEE_UPDATED_SUCCESSFULLY.getErrorCode());
				loadEmployeeTab();
				return "/pages/personnelHR/createEmployeeIncrementDetails.xhtml?faces-redirect=true";
			} else {
				log.error("Employee Update failed with error code" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("[Exception in Achievements Save Continue :]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to add work experience in experienceDetails List
	 * 
	 * @return
	 */

	public String addworkExperience() {
		log.info("Add work Experience called for  ");
		try {
			if (validateWorkExperience()) {
				if (experienceDetails != null && experienceDetailsList != null) {
					experienceDetailsList.add(experienceDetails);
					log.info("experienceDetails list added successfully: " + experienceDetailsList);
					experienceDetails = new EmployeeExperienceDetails();
					loadPanel("workForm:workExperiencePanel");
					return null;
				} else {
					log.info("Employee work Experience Details not added");
					errorMap.notify(ErrorDescription.EMP_EXP_LIST_IS_EMPTY.getErrorCode());
					return null;
				}
			} else {
				log.info("Employee work Experience validation falied ");
			}

		} catch (Exception exp) {
			log.error("found exception in work experience details while adding list : ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public boolean validateWorkExperience() {
		if (experienceDetails != null && experienceDetails.getCompanyName().trim().isEmpty()
				&& experienceDetails.getJoinDesignation().trim().isEmpty() && experienceDetails.getJoinDate() == null
				&& experienceDetails.getRelieveDate() == null && experienceDetails.getLastDrawnCtc() == null
				&& experienceDetails.getContactRefPerson().trim().isEmpty()
				&& experienceDetails.getContactRefMobile() == null
				&& experienceDetails.getReasonForLeaving().trim().isEmpty()
				&& experienceDetails.getDocumentPath() == null) {
			errorMap.notify(ErrorDescription.EMP_EXPERIENCE_DETAILS_ATLEAST_ADD_ONE_RECORD.getErrorCode());
			log.info("Enter at least one input to add");
			return false;
		}
		if (experienceDetails != null && experienceDetails.getCompanyName().trim() != null
				&& experienceDetails.getCompanyName().trim().length() != 0) {
			if (!FormValidation.validateLength(3, 150, experienceDetails.getCompanyName())) {
				errorMap.notify(ErrorDescription.EMP_EXPERIENCE_DETAILS_COMPANY_MIN_MAX_LENGTH.getErrorCode());
				log.info("company name size should be minimum 3 and maximum 200");
				return false;
			}
			log.info("getJoinDesignation==>" + experienceDetails.getJoinDesignation());
			if (experienceDetails.getJoinDesignation() == null || experienceDetails.getJoinDesignation().isEmpty()) {
				errorMap.notify(ErrorDescription.DES_NOT_EMPTY.getErrorCode());
				log.info("Designation not empty");
				return false;
			}
			if (experienceDetails.getJoinDate() == null) {
				errorMap.notify(ErrorDescription.JOIN_DATE_NOT_EMPTY.getErrorCode());
				log.info("Join date not empty");
				return false;
			}
			if (experienceDetails.getRelieveDate() == null) {
				errorMap.notify(ErrorDescription.RELIEVE_DATE_NOT_EMPTY.getErrorCode());
				log.info("Relieve date not empty");
				return false;
			}
			if (experienceDetails.getLastDrawnCtc() == null) {
				errorMap.notify(ErrorDescription.LAST_DRAWN_SALARY_NOT_EMPTY.getErrorCode());
				log.info("Last drawn salary not empty");
				return false;
			}
			log.info("lev==>" + experienceDetails.getReasonForLeaving());
			if (experienceDetails.getReasonForLeaving() == null || experienceDetails.getReasonForLeaving().isEmpty()) {
				errorMap.notify(ErrorDescription.REASON_FOR_LEAVING_NOT_EMPTY.getErrorCode());
				log.info("Reason for leaving not empty");
				return false;
			}
		}
		if (experienceDetails.getJoinDesignation().trim() != null
				&& experienceDetails.getJoinDesignation().trim().length() != 0
				&& !FormValidation.validateLength(3, 150, experienceDetails.getJoinDesignation())) {

			errorMap.notify(ErrorDescription.EMP_EXPERIENCE_DETAILS_DESIGNATION_MIN_MAX_LENGTH.getErrorCode());
			log.info("designation name size should be minimum 3 and maximum 150");
			return false;

		}
		return true;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to edit slelected work experience record
	 * 
	 * @param index,employeeExperienceDetails
	 * @return
	 */
	public void editWorkExperienc(int idx, EmployeeExperienceDetails expDetail) {
		log.info("WorkExperienc EDIT is pressed with index :: " + idx + " , " + " & Experience Details :: " + expDetail
				+ "]");
		action = "EDITWORKEXP";
		rowIndex = idx;
		try {
			if (rowIndex != null && experienceDetails != null && expDetail != null) {
				experienceDetails = expDetail;

			} else {
				errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
				log.error(" Experience details is empty");
			}
			loadPanel("workForm:workExperiencePanel");
		} catch (Exception exp) {
			log.error("found exception in edit work experience details : ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to update slelected work experience record
	 * 
	 * @return
	 */
	public void updateWorkExperience() {
		log.info("WorkExperienc UPDATE is pressed with index [ " + rowIndex + " ]" + "[" + experienceDetails + "]");
		try {
			if (experienceDetails != null && experienceDetailsList.size() != 0) {
				experienceDetailsList.set(rowIndex, experienceDetails);
			}
			experienceDetails = new EmployeeExperienceDetails();
			loadPanel("workForm:workExperiencePanel");

		} catch (Exception exp) {
			log.error("found Exception in update WorkExperience", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to delete slelected work experience record
	 * 
	 * @param index,employeeExperienceDetails
	 * @return
	 */

	public String deleteWorkExperience(int idx, EmployeeExperienceDetails expDetail) {
		try {
			log.info("WorkExperienc Delete is called : [" + expDetail + "]");
			if (expDetail == null) {
				log.info(" Employee expDetail Is Null ");
				errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
			}
			if (expDetail.getId() == null && experienceDetailsList != null) {
				experienceDetailsList.remove(idx);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				loadPanel("workForm:workExperiencePanel");
				return null;
			} else {
				url = EMP_SERVER_URL + "/delexpdetail/" + expDetail.getId();
				log.info(" Delete Work Experience Called For URL :: " + url);
				BaseDTO baseDto = httpService.delete(url);
				log.info(" Delete Work Experience Retured Response :: " + baseDto);
				if (baseDto == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}

				if (baseDto.getStatusCode() == 0) {
					// errorMap.notify(ErrorDescription.EMP_EXPERIENCE_DETAILS_DELETED_SUCCESSFULLY.getErrorCode());
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					experienceDetailsList.remove(idx);
					log.info("Employee Work Experience Deleted Succuessfully ");
					loadPanel("workForm:workExperiencePanel");
					return null;
				} else {
					log.error("Error while Deleting Work Experience with error code :: " + baseDto.getStatusCode());
					errorMap.notify(baseDto.getStatusCode());
				}
			}

		} catch (Exception exp) {
			log.error("deletion failed in work experienc", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to view slelected work experience record
	 * 
	 * @param employeeExperienceDetails
	 * @return
	 */
	public void viewWorkExperienc(EmployeeExperienceDetails expDetail) {

		if (expDetail == null) {
			log.error(" Employee Experience Details is Empty :: " + expDetail);
			errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
			return;
		}
		try {
			action = "VIEWEXP";
			experienceDetails = expDetail;
			loadPanel("workForm:workExperiencePanel");
		} catch (Exception e) {
			log.error("Exception Occured wile view employee experience details", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to save or update work experience list of the selected
	 *         employee.
	 * 
	 * 
	 * @return BaseDTO
	 */
	public String saveWorkExperience() {
		try {
			log.info("Employee Work Experience Save Called For employee Experience list: " + experienceDetailsList);
			if (employeeMaster == null || experienceDetailsList == null) {
				log.error(" Employee Experience is Empty");
				errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
				return null;
			}
			experienceDetailsList.forEach(expDetails -> {

				if (expDetails.getCompanyName() != null && expDetails.getCompanyName().length() != 0) {
					EmployeeValidator.validateMinLength(expDetails.getCompanyName(),
							ErrorDescription.EMP_COMPANY_NAME_MIN_LENGTH, 3);
				}
				if (expDetails.getJoinDesignation() != null && expDetails.getJoinDesignation().length() != 0) {
					EmployeeValidator.validateMinLength(expDetails.getJoinDesignation(),
							ErrorDescription.EMP_JOIN_DESIGNATION_MIN_LENGTH, 3);
				}
			});
			employeeMaster.setEmpExperienceDetailsList(experienceDetailsList);

			log.info(" Employee Work Experience Url :: " + url);
			url = EMP_SERVER_URL + "/update";
			BaseDTO baseDto = httpService.put(url, employeeMaster);
			log.info(" Employee Work Experience response :: " + baseDto);
			if (baseDto == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);

			if (employee != null) {
				employeeMaster = employee;
				experienceDetailsList = employeeMaster.getEmpExperienceDetailsList();
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDto.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMP_EXPERIENCE_DETAILS_SAVED_SUCCESSFULLY.getErrorCode());
				experienceDetailsList = employeeMaster.getEmpExperienceDetailsList();
				log.info("Employee Family Work Experience Saved Succuessfully ");
				return "/pages/personnelHR/createEmployeeAchievement.xhtml?faces-redirect=true";
			} else {
				log.error("Error while saving Family Details with error code :: " + baseDto.getStatusCode());
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found Exception in working experience", exp);
		}
		return null;
	}

	public void closeWorkExp() {
		action = "ADD";
		experienceDetails = new EmployeeExperienceDetails();

	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to upload Relieving letter document of the selected work
	 *         experience list .
	 * 
	 * @param FileUploadEvent
	 * @return
	 */
	public void uplaodRelievingDoc(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.EMP_RELIEVING_DOC_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			experienceDetails.setRelievingLetterDoc(relievingLetterFile.getContents());
			experienceDetails.setDocumentPath(relievingLetterFile.getFileName());
			relievingLetterFileName = experienceDetails.getDocumentPath();
			errorMap.notify(ErrorDescription.EMP_RELIEVING_DOC_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	/****** Contact Details code Starts here ******/

	public void onDistrictChangePresent() {
		log.info("<==== Employee Contact Creation inside getTalukList() Start ======>");
		try {
			if (employeeContactDetails == null || employeeContactDetails.getTalukIdPermanent() == null
					|| employeeContactDetails.getTalukIdPermanent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}

			if (employeeContactDetails.getDistrictIdPresent() != null) {
				cityMasterListPresent = commonDataService
						.loadActiveCitiesByDistrict(employeeContactDetails.getDistrictIdPresent().getId());
				url = SERVER_URL + "/taluk/getbydistrict/" + employeeContactDetails.getDistrictIdPresent().getId();
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				talukMasterListPresent = mapper.readValue(jsonResponse, new TypeReference<List<TalukMaster>>() {
				});
				if (talukMasterListPresent == null) {
					log.error("Taluk Master failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
			}
			/*
			 * if (baseDTO.getStatusCode() == 0) {
			 * log.info(" Taluk Masters Rerived Successfully " + talukMasterListPresent); }
			 * else { log.info(" Taluk Masters Failed To Retrive with Status Code ::  " +
			 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); }
			 */

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on onStateChangePresent :: ", e);
		}

		log.info("<======Inside Employee Contact Creation getTalukList()  End =====>", talukMasterList);
	}

	public void onTalukChangePresent() {
		log.info("<==== Employee Contact Creation inside getVillageList() Start ======>");
		try {
			if (employeeContactDetails == null || employeeContactDetails.getTalukIdPermanent() == null
					|| employeeContactDetails.getTalukIdPermanent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}
			if (employeeContactDetails.getTalukIdPresent() != null) {
				url = SERVER_URL + "/village/getallvillagebytaluk/"
						+ employeeContactDetails.getTalukIdPresent().getId();
				BaseDTO baseDTO = httpService.get(url);
				log.info(" On District Change Response :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				villageMasterListPresent = mapper.readValue(jsonResponse, new TypeReference<List<VillageMaster>>() {
				});
				if (villageMasterListPresent == null) {
					log.error("Village Master failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
			}
			/*
			 * if (baseDTO.getStatusCode() == 0) {
			 * log.info(" Village Masters Rerived Successfully " +
			 * villageMasterListPresent); } else {
			 * log.info(" Village Masters Failed To Retrive with Status Code ::  " +
			 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); }
			 */

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on onStateChangePresent :: ", e);
		}

		log.info("<======Inside Employee Contact Creation getVillageList()  End =====>", villageMasterList);
	}

	public void onStateChangePresent() {
		log.info("<==== Employee Contact Creation inside OnStateChange() Start ======>");
		try {
			if (employeeContactDetails == null || employeeContactDetails.getStateIdPresent() == null
					|| employeeContactDetails.getStateIdPresent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}
			if (employeeContactDetails.getStateIdPresent() != null) {
				url = SERVER_URL + "/districtMaster/getalldistrictbystate/"
						+ employeeContactDetails.getStateIdPresent().getId();
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				districtMasterListPresent = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
				});
				if (districtMasterListPresent == null) {
					log.error("District Master failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
			}
			/*
			 * if (baseDTO.getStatusCode() == 0) {
			 * log.info(" Taluk Masters Rerived Successfully " + districtMasterListPresent);
			 * } else { log.info(" Taluk Masters Failed To Retrive with Status Code ::  " +
			 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); }
			 */
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on onStateChangePresent :: ", e);
		}
		log.info("<======Inside Employee Contact Creation OnStateChange()  End =====>", talukMasterList);
	}

	public void onStateChangePermanent() {
		log.info("Employee Contact Creation inside OnStateChange() Start for Contact Details :: "
				+ employeeContactDetails);
		try {
			if (employeeContactDetails == null || employeeContactDetails.getStateIdPermanent() == null
					|| employeeContactDetails.getStateIdPermanent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}
			if (employeeContactDetails.getStateIdPermanent() != null) {
				url = SERVER_URL + "/districtMaster/getalldistrictbystate/"
						+ employeeContactDetails.getStateIdPermanent().getId();
				log.info(" On State Change URL Called :: " + url);
				BaseDTO baseDTO = httpService.get(url);
				log.info(" On State Change Response Recieved  :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				districtMasterList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
				});
			}
			if (districtMasterList == null) {
				log.error("districtMaster failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			log.info(" District Master List Based on State Id :: " + districtMasterList);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on State Change Permanent :: ", e);
		}
	}

	public void onDistrictChangePermanent() {
		log.info("<==== Employee Contact Creation inside onTalukChangePermanent() Start ======>"
				+ employeeContactDetails);

		try {
			if (employeeContactDetails == null || employeeContactDetails.getDistrictIdPermanent() == null
					|| employeeContactDetails.getDistrictIdPermanent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}
			if (employeeContactDetails != null && employeeContactDetails.getDistrictIdPermanent().getId() != null) {
				cityMasterList = commonDataService
						.loadActiveCitiesByDistrict(employeeContactDetails.getDistrictIdPermanent().getId());
				if (employeeContactDetails.getDistrictIdPermanent() != null) {
					url = SERVER_URL + "/taluk/getbydistrict/"
							+ employeeContactDetails.getDistrictIdPermanent().getId();
					log.info(" On District Change URL Called :: " + url);
					BaseDTO baseDTO = httpService.get(url);
					log.info(" On District Change Response :: " + baseDTO);
					if (baseDTO == null) {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

					talukMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TalukMaster>>() {
					});

					if (talukMasterList == null) {
						log.error("Taluk Master failed to desearlize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return;
					}
				}
				/*
				 * if (baseDTO.getStatusCode() == 0) {
				 * log.info(" Taluk Masters Rerived Successfully " + talukMasterList); } else {
				 * log.info(" Taluk Masters Failed To Retrive with Status Code ::  " +
				 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); }
				 */

			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on District Change Permanent :: ", e);
		}
	}

	public void onTalukChangePermanent() {
		log.info("<==== Employee Contact Creation inside onTalukChangePermanent() Start ======>");
		try {
			if (employeeContactDetails == null || Objects.isNull(employeeContactDetails.getTalukIdPermanent())
					|| employeeContactDetails.getTalukIdPermanent().getId() == null) {
				log.error(" Employee Contact Details Is Empty ");
				// errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
			}
			if (employeeContactDetails.getTalukIdPermanent() != null) {
				url = SERVER_URL + "/village/getallvillagebytaluk/"
						+ employeeContactDetails.getTalukIdPermanent().getId();
				log.info(" On Taluk Change URL Called :: " + url);
				BaseDTO baseDTO = httpService.get(url);
				log.info(" On Taluk Change Response :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				villageMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VillageMaster>>() {
				});

				if (villageMasterList == null) {
					log.error("Village Master failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				if (baseDTO.getStatusCode() == 0) {
					log.info(" Village Masters Rerived Successfully " + villageMasterList);
				} else {
					log.info(" :: Village Masters Failed To Retrive with Status Code :: ");
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on Taluk Change Permanent :: ", e);
		}

	}

	// TODO

	public String createContactDetails() {

		try {
			log.info("<========   Employee Contact Details Started ============>" + employeeContactDetails);

			if (Objects.isNull(employeeContactDetails)) {
				log.error("Create Contact Details Is Empty");
				errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());

			}
			if (Objects.isNull(landlineCode) && landlineCode.isEmpty()) {
				log.error("Create Contact Details Land Line Code Is Empty");
				errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_LAND_LINE_CODE_EMPTY.getErrorCode());
			}
			if (Objects.isNull(landlineNumber) && landlineNumber.isEmpty()) {
				log.error("Create Contact Details Land Line Number Is Empty");
				errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_LAND_LINE_NUMBER_EMPTY.getErrorCode());
			}
			employeeContactDetails.setLandLineNumber(landlineCode + "-" + landlineNumber);

			employeeMaster.setContactDetails(employeeContactDetails);

			log.info(" Employee Contct Details Save Called ");

			url = EMP_SERVER_URL + "/update";
			log.info("Save Employee Contact Details URL : " + url);

			BaseDTO baseDTO = httpService.put(url, employeeMaster);
			log.info(" Save Employee Contact Details Response" + baseDTO);
			if (baseDTO == null) {
				log.error(" BaseDTO return null value ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
				employeeContactDetails = employeeMaster.getContactDetails();
			} else {
				log.info("[ Employee Object filed to Deserialize]");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			employeeFamilyDetailsList = employeeMaster.getFamilyDetailsList();
			if (employeeFamilyDetailsList.size() == 0) {
				employeeFamilyDetailsList = new ArrayList<EmployeeFamilyDetails>();
			}
			if (baseDTO.getStatusCode() == 0) {

				if (employeeContactDetails.getId() == null) {
					log.info("Employee Family Details Saved Succuessfully ");
					errorMap.notify(ErrorDescription.EMP_ADDRESS_DETAILS_SAVED.getErrorCode());
				} else {
					log.info("Employee Family Details Updated Succuessfully ");
					errorMap.notify(ErrorDescription.EMP_ADDRESS_DETAILS_UPDATED.getErrorCode());
				}

				return "/pages/personnelHR/createEmployeeFamily.xhtml?faces-redirect=true";
			} else {
				log.error("Employee Contact Details failed with error code" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("[ Exception in Save Employee Contact Details ]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void copyAddress() {
		log.info("Inside ContactDetails copyAddress for employee Contact"
				+ employeeContactDetails.isPermanentAndPresent());
		try {

			log.info("getIsPermanentAndPresent==>" + employeeContactDetails.isPermanentAndPresent());
			if (employeeContactDetails.isPermanentAndPresent()) {

				if (employeeContactDetails == null) {
					log.error(" Employee Contact Details is Null ");
					errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
				}

				log.info("Inside ContactDetails copyAddress for employee Contact" + employeeContactDetails);

				employeeContactDetails.setAddressLineOnePresent(employeeContactDetails.getAddressLineOnePermanent());
				employeeContactDetails.setAddressLineTwoPresent(employeeContactDetails.getAddressLineTwoPermanent());
				employeeContactDetails
						.setAddressLineThreePresent(employeeContactDetails.getAddressLineThreePermanent());
				employeeContactDetails.setPostalCodePresent(employeeContactDetails.getPostalCodePermanent());
				employeeContactDetails.setStateIdPresent(employeeContactDetails.getStateIdPermanent());
				employeeContactDetails.setLandmarkPresent(employeeContactDetails.getLandmarkPermanent());

				districtMasterListPresent = districtMasterList;
				employeeContactDetails.setDistrictIdPresent(employeeContactDetails.getDistrictIdPermanent());
				talukMasterListPresent = talukMasterList;
				cityMasterListPresent = cityMasterList;
				villageMasterListPresent = villageMasterList;
				employeeContactDetails.setTalukIdPresent(employeeContactDetails.getTalukIdPermanent());
				employeeContactDetails.setCityIDPresent(employeeContactDetails.getCityIDPermanent());
				if (employeeContactDetails.getTalukIdPermanent() == null) {
					log.error("Taluk id is null");
					errorMap.notify(ErrorDescription.TALUK_ID_NULL.getErrorCode());
				}
				employeeContactDetails.setVillageIDPresent(employeeContactDetails.getVillageIdPermanent());

			} else {
				log.info("Inside ContactDetails copyAddress for employee Contact" + employeeContactDetails);
				Util.addWarn("Please Fill above all the fields");
				employeeContactDetails.setAddressLineOnePresent(null);
				employeeContactDetails.setAddressLineTwoPresent(null);
				employeeContactDetails.setAddressLineThreePresent(null);
				employeeContactDetails.setPostalCodePresent(null);
				employeeContactDetails.setStateIdPresent(null);
				employeeContactDetails.setDistrictIdPresent(null);
				employeeContactDetails.setTalukIdPresent(null);
				employeeContactDetails.setLandmarkPresent(null);
				employeeContactDetails.setVillageIDPresent(null);
			}
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void handleFileUploadForAddressProof(FileUploadEvent event) {
		try {

			if (event == null || event.getFile() == null) {
				log.error(" Employee Adress Proof Document is Empty");
				errorMap.notify(ErrorDescription.EMPLOYEE_ADDRESS_PROOF_DOC_IS_EMPTY.getErrorCode());
				return;

			}
			file = event.getFile();
			log.info("Address Upload Starts With File Name :: " + file.getFileName() + " And File Size :: "
					+ file.getSize());
			String type = file.getFileName();
			if (!type.contains("png") && !type.contains("jpg") && !type.contains("pdf") && !type.contains("jpeg")
					&& !type.contains("docx")) {
				employeeContactDetails.setAddressProof(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_ADDRESS_PROOF_FORMAT.getErrorCode());
				return;
			}
			byte[] foto = IOUtils.toByteArray(file.getInputstream());
			employeeContactDetails.setAddressProof(foto);
			employeeContactDetails.setFileName(type);
			log.info(" Employee Address Proof Uploaded Successfully :: ");
		} catch (Exception e) {
			log.error("Exception Occured While Upload For AddressProof", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public Date getToday() {
		return new Date();
	}

	public String clearEmployeeContactDetails() {
		log.info(" ClearEmployeeContactDetails Called ");
		employeeContactDetails = new EmployeeContactDetails();
		landlineNumber = "";
		landlineCode = "";
		return null;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to get entity list based on selected head or regional
	 *         offfice and entity type
	 *
	 * @param entitytype id, HeadAndRegionalOffice id
	 * 
	 *
	 * @return EntityMasterList
	 */

	public void onchangeRegionAndEntityType() {
		log.info("onchangeEntityType called..");
		try {

			if (employeeSuspensionDetails.getHeadAndRegionalOffice() != null
					&& employeeSuspensionDetails.getHeadAndRegionalOffice().getEntityTypeMaster() != null
					&& employeeSuspensionDetails.getHeadAndRegionalOffice().getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
				employeeSuspensionDetails.setWorkLocation(employeeSuspensionDetails.getHeadAndRegionalOffice());
				suspensionFlag = "DISABLEDENTITY";
				employeeSuspensionDetails.setEntityTypeMaster(null);
			} else {
				suspensionFlag = null;
			}

			if (employeeSuspensionDetails != null && employeeSuspensionDetails.getHeadAndRegionalOffice() != null
					&& employeeSuspensionDetails.getHeadAndRegionalOffice().getId() != null
					&& employeeSuspensionDetails.getEntityTypeMaster() != null
					&& employeeSuspensionDetails.getEntityTypeMaster().getId() != null) {
				Long regionId = employeeSuspensionDetails.getHeadAndRegionalOffice().getId();
				Long entityTypeId = employeeSuspensionDetails.getEntityTypeMaster().getId();
				suspensionEntityMasterList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
			} else {
				log.info("entity not found.");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_ENTITY_TYPE_NOT_SELECTED.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to add the Employee Suspension Detail list.
	 * 
	 */
	public String addEmployeeSuspensionDetails() {
		log.info("Add Employee Suspension  Details called ");
		try {
			log.info("Employee Suspension  Details : [" + employeeMaster.getEmpSuspensionDetailsList() + "]");
			if (employeeSuspensionDetails != null && employeeSuspensionDetailsList != null) {
				employeeSuspensionDetailsList.add(employeeSuspensionDetails);
				employeeSuspensionDetails = new EmployeeSuspensionDetails();
				loadPanel("suspensionForm:suspensionDetailsPanel");
				log.info("Employee Suspension  Details list added successfully: " + employeeSuspensionDetailsList);
			} else {
				log.info("Employee Suspension  Details list not added");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_LIST_IS_EMPTY.getErrorCode());
				return null;
			}
		} catch (Exception exp) {
			log.error("found exception in Employee Suspension  Details while adding list : ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to edit the selected Employee Suspension Detail list.
	 * 
	 * @param index,EmployeeSuspensionDetails
	 * 
	 */
	public void editEmployeeSuspensionDetails(int idx, EmployeeSuspensionDetails suspensionDetails) {
		log.info("Employee Suspension  Details EDIT is pressed :: " + suspensionDetails);
		employeeSuspensionDetails = new EmployeeSuspensionDetails();
		action = "EDITSUSPENSION";
		suspensionFlag = null;
		rowIndex = idx;

		try {
			if (suspensionDetails != null) {

				employeeSuspensionDetails = suspensionDetails;
				log.info("employeeSuspensionDetails - " + employeeSuspensionDetails);
				if (employeeSuspensionDetails.getWorkLocation().getEntityTypeMaster().getEntityCode()
						.equals("HEAD_OFFICE")) {
					suspensionFlag = "DISABLEDENTITY";
				}
				if (employeeSuspensionDetails != null && employeeSuspensionDetails.getHeadAndRegionalOffice() != null
						&& employeeSuspensionDetails.getEntityTypeMaster() != null) {
					Long regionId = employeeSuspensionDetails.getHeadAndRegionalOffice().getId();
					Long entityTypeId = employeeSuspensionDetails.getEntityTypeMaster().getId();
					suspensionEntityMasterList = commonDataService.loadEntityByregionOrentityTypeId(regionId,
							entityTypeId);
				} else {
					if (employeeSuspensionDetails.getWorkLocation().getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
						employeeSuspensionDetails.setHeadAndRegionalOffice(employeeSuspensionDetails.getWorkLocation());
					}

					else {
						employeeSuspensionDetails.setHeadAndRegionalOffice(
								employeeSuspensionDetails.getWorkLocation().getEntityMasterRegion());
						employeeSuspensionDetails
								.setEntityTypeMaster(employeeSuspensionDetails.getWorkLocation().getEntityTypeMaster());
					}
				}
			} else {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_IS_EMPTY.getErrorCode());
				log.error(" Employee Suspension details is empty");
			}
			loadPanel("suspensionForm:suspensionDetailsPanel");
		} catch (Exception exp) {
			log.error("found exception in edit Employee Suspension details : ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to update the selected Employee Suspension Detail list.
	 * 
	 */
	public void updateEmployeeSuspensionDetails() {
		log.info("Employee Suspension Details UPDATE is pressed with index [ " + rowIndex + " ]" + "["
				+ employeeSuspensionDetails + "]");
		try {
			if (employeeSuspensionDetails != null && employeeSuspensionDetailsList.size() != 0) {
				employeeSuspensionDetailsList.set(rowIndex, employeeSuspensionDetails);
			}
			action = "ADD";
			employeeSuspensionDetails = new EmployeeSuspensionDetails();
			suspensionFlag = null;
			loadPanel("suspensionForm:suspensionDetailsPanel");

		} catch (Exception exp) {
			log.error("found Exception in update Employee Suspension Details", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to delete the selected Employee Suspension Detail list.
	 * 
	 * @param index,EmployeeSuspensionDetails
	 * 
	 */
	public String deleteEmployeeSuspension(int idx, EmployeeSuspensionDetails suspensionDetail) {
		try {
			log.info("Employee Suspension Details Delete is called : [" + suspensionDetail + "]");
			if (suspensionDetail == null) {
				log.info(" Employee Suspension Details is Null ");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_IS_EMPTY.getErrorCode());
				return null;
			}
			if (suspensionDetail != null && suspensionFlag != null && suspensionFlag.equals("DELETESUSPENSION")) {
				if (suspensionDetail.getId() == null) {
					employeeSuspensionDetailsList.remove(idx);
					errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_DELETED_SUCCESSFULLY.getErrorCode());
					loadPanel("suspensionForm:suspensionDetailsPanel");
					return null;
				} else {
					url = EMP_SERVER_URL + "/deletesuspension/id/" + suspensionDetail.getId();
					log.info(" Delete Employee Suspension Details Called For URL :: " + url);
					BaseDTO baseDto = httpService.delete(url);
					log.info(" Delete Employee Suspension Details Retured Response :: " + baseDto);
					if (baseDto == null) {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					if (baseDto.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_DELETED_SUCCESSFULLY.getErrorCode());
						employeeSuspensionDetailsList.remove(idx);
						employeeSuspensionDetails = new EmployeeSuspensionDetails();
						loadPanel("suspensionForm:suspensionDetailsPanel");
						log.info("Employee Suspension Details Deleted Succuessfully ");
					} else {
						log.error("Error while Deleting Employee Suspension Details with error code :: "
								+ baseDto.getStatusCode());
						errorMap.notify(baseDto.getStatusCode());
					}
				}
			}

		} catch (Exception exp) {
			log.error("deletion failed in Employee Suspension Details", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to view of the selected Employee Suspension Detail list.
	 * 
	 * @param EmployeeSuspensionDetails
	 * 
	 */
	public void viewEmployeeSuspensionDetails(EmployeeSuspensionDetails suspensionDetails) {

		if (suspensionDetails == null) {
			log.error(" Employee Suspension Details is Empty :: " + suspensionDetails);
			errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_IS_EMPTY.getErrorCode());
			return;
		}
		try {
			action = null;
			suspensionFlag = "VIEWSUSPENSION";
			employeeSuspensionDetails = suspensionDetails;
			if (employeeSuspensionDetails != null && employeeSuspensionDetails.getHeadAndRegionalOffice() != null
					&& employeeSuspensionDetails.getEntityTypeMaster() != null) {
				Long regionId = employeeSuspensionDetails.getHeadAndRegionalOffice().getId();
				Long entityTypeId = employeeSuspensionDetails.getEntityTypeMaster().getId();
				suspensionEntityMasterList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
			} else {
				employeeSuspensionDetails.setHeadAndRegionalOffice(employeeSuspensionDetails.getWorkLocation());
				employeeSuspensionDetails
						.setEntityTypeMaster(employeeSuspensionDetails.getWorkLocation().getEntityTypeMaster());
			}

			loadPanel("suspensionForm:suspensionDetailsPanel");
		} catch (Exception e) {
			log.error("Exception Occured wile view employee Suspension Details", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Praveen Kunwar
	 * 
	 *         this method to save or update Employee Suspension Detail list of the
	 *         selected employee.
	 * 
	 * 
	 * @return BaseDTO
	 */
	public boolean validateSuspensionDetails() {
		Iterator<EmployeeSuspensionDetails> employeeSuspensionDetailsItr = employeeSuspensionDetailsList.iterator();
		while (employeeSuspensionDetailsItr.hasNext()) {
			EmployeeSuspensionDetails esd = employeeSuspensionDetailsItr.next();
			if (esd.getSuspensionReferenceNumber() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_SUSP_REF_NUMBER_REQUIRED.getErrorCode());
				return false;
			} else if (esd.getSuspensionReferenceDate() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_SUSP_REF_DATE_REQUIRED.getErrorCode());
				return false;
			} else if (esd.getWorkLocation() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_ENTITY_TYPE_REQUIRED.getErrorCode());
				return false;
			} else if (esd.getSuspensionDetailsMaster() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_REQUIRED.getErrorCode());
				return false;
			} else if (esd.getFromDate() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_REQUIRED.getErrorCode());
				return false;
			}
			if (esd.getRegularizationReferenceNumber() == null
					|| esd.getRegularizationReferenceNumber().trim().isEmpty()) {
				esd.setRegularizationReferenceNumber(null);
			}
			if (esd.getExtensionReferenceNumber() == null || esd.getExtensionReferenceNumber().trim().isEmpty()) {
				esd.setExtensionReferenceNumber(null);
			}
			if (esd.getRevisionReferenceNumber() == null || esd.getRevisionReferenceNumber().trim().isEmpty()) {
				esd.setRevisionReferenceNumber(null);
			}
			if (esd.getSuspensionReferenceNumber() == null || esd.getSuspensionReferenceNumber().trim().isEmpty()) {
				esd.setSuspensionReferenceNumber(null);
			}
		}
		return true;
	}

	public String saveEmployeeSuspensionDetails() {
		BaseDTO baseDto = new BaseDTO();
		try {
			log.info("Employee Suspension Details Save Called For employee Suspension Details list: "
					+ employeeSuspensionDetailsList);
			if (employeeMaster == null || employeeSuspensionDetailsList == null) {
				log.error(" Employee Suspension Details is Empty");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_LIST_IS_EMPTY.getErrorCode());
				return null;
			}

			boolean validEntries = true;

			// validEntries = validateSuspensionDetails();

			if (validEntries) {
				employeeMaster.setEmpSuspensionDetailsList(employeeSuspensionDetailsList);

				log.info(" Employee Suspension Details Url :: " + url);
				url = EMP_SERVER_URL + "/update";
				baseDto = httpService.put(url, employeeMaster);
				log.info(" Employee Suspension Details response :: " + baseDto);
				if (baseDto == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);

				if (employee != null) {
					employeeMaster = employee;
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}

				if (baseDto.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_SAVED_SUCCESSFULLY.getErrorCode());
					employeeSuspensionDetailsList = employeeMaster.getEmpSuspensionDetailsList();
					employeeSuspensionDetails = new EmployeeSuspensionDetails();
					suspensionFlag = null;
					log.info("Employee Suspension Details Saved Succuessfully ");
					return "/pages/personnelHR/createEmployeeLeaveDetails.xhtml?faces-redirect=true";
				} else {
					log.error("Error while saving Employee Suspension Details Details with error code :: "
							+ baseDto.getStatusCode());
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			errorMap.notify(baseDto.getStatusCode());
			log.error("found Exception in Employee Suspension Details", exp);
		}
		return null;
	}

	public void loadPanel(String value) {
		RequestContext.getCurrentInstance().update(value);
	}

	public void closeSuspensionDetails() {
		action = "ADD";
		suspensionFlag = "ADDSUSPENSION";
		employeeSuspensionDetails = new EmployeeSuspensionDetails();

	}

	public void clearSuspensionDetails() {
		suspensionFlag = null;
		employeeSuspensionDetails = new EmployeeSuspensionDetails();
		loadPanel("suspensionForm:suspensionDetailsPanel");
	}

	/**
	 * Purpose : to calculate past year
	 * 
	 * @author krishnakumar
	 * @param
	 * @return Date
	 */
	public Date getTodayPlusThree() {
		Date date = getCurrentDate();
		return date;
	}

	public Date getCurrentDate() {
		Date currentDate = new Date();
		currentDate.setYear(currentDate.getYear() - 1);
		return currentDate;
	}

	/**
	 * Propose : load all Entity
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void getToEntityMaster(EntityTypeMaster entityTypeId) {
		log.info("getToEntityMaster Start===>");
		log.info(": getToEntityMaster Started " + entityTypeId);
		try {
			if (entityTypeId == null) {
				log.error("Entity Type Id Not Exists");
				errorMap.notify(ErrorDescription.ENTITY_TYPE_ID_NOT_EXIST.getErrorCode());
			}
			transferToEntityMasterList = commonDataService.getEntityMaster(entityTypeId);
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		log.info("getToEntityMaster End===>");
	}

	public void getFromEntityMaster(EntityTypeMaster entityTypeId) {
		log.info("getToEntityMaster Start===>");
		log.info(": getToEntityMaster Started " + entityTypeId);
		try {
			if (entityTypeId == null) {
				log.error("Entity Type Id Not Exists");
				errorMap.notify(ErrorDescription.ENTITY_TYPE_ID_NOT_EXIST.getErrorCode());
			}
			transferFromEntityMasterList = commonDataService.getEntityMaster(entityTypeId);

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		log.info("getToEntityMaster End===>");
	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void addTransfer(EmployeeTransferDetails transferDetails) {

		employeeTransferDetails = transferDetails;
		transferFlag = "ADDTRANSFER";
		visiableFamButton = "EDITBUTTON";
		log.info("addTransfer transferDetails==>" + transferDetails);
		transferToEntityMasterList = commonDataService.getEntityMaster(transferDetails.getEntityTo());
		transferFromEntityMasterList = commonDataService.getEntityMaster(transferDetails.getEntityFrom());
		employeeTransferDetailsList.remove(transferDetails);
	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void viewTransfer(EmployeeTransferDetails transferDetails) {

		employeeTransferDetails = transferDetails;
		transferFlag = "VIEWTRANSFER";
		visiableFamButton = "ADDBUTTON";
		log.info("viewTransfer transferDetails==>" + transferDetails);

	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void closeTransfer() {
		transferFlag = "ADDTRANSFER";
		employeeTransferDetails = new EmployeeTransferDetails();
	}

	/**
	 * Purpose : to load transfer details buffer table
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void loadTransferDetails() {
		log.info("Employee Master Bean :: loadTransferDetails start");
		visiableFamButton = "EDITBUTTON";
		employeeTransferDetailsList.add(employeeTransferDetails);
		log.info("employeeTransferDetailsList size - " + employeeTransferDetailsList.size());
		employeeTransferDetails = new EmployeeTransferDetails();
		log.info("Employee Master Bean :: loadTransferDetails end");
	}

	/**
	 * Purpose : to save the employee transfer
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public boolean validateTransferDetails() {
		Iterator<EmployeeTransferDetails> employeeTransferDetailsItr = employeeTransferDetailsList.iterator();
		while (employeeTransferDetailsItr.hasNext()) {
			EmployeeTransferDetails transferDetailsList = employeeTransferDetailsItr.next();

			if (transferDetailsList.getTransferReferenceDate() == null) {
				errorMap.notify(ErrorDescription.TRANSFER_REFERENCE_DATE.getErrorCode());
				return false;
			}
			if (transferDetailsList.getTransferReferenceNo() != null
					&& !StringUtils.isEmpty(transferDetailsList.getTransferReferenceNo())) {
				if (transferDetailsList.getTransferReferenceNo().length() < 3
						|| transferDetailsList.getTransferReferenceNo().length() > 50) {
					errorMap.notify(ErrorDescription.EMPLOYEE_TRANSFER_REF_NO_MAX_MIN.getErrorCode());
					return false;
				}
			}
			if (transferDetailsList.getDesignation() == null || transferDetailsList.getDesignation().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getEntityFrom() == null
					|| transferDetailsList.getEntityFrom().getId() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getEntityTo() == null || transferDetailsList.getEntityTo().getId() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getTransferFrom() == null
					|| transferDetailsList.getTransferFrom().getId() == null) {
				errorMap.notify(ErrorDescription.TRANSFER_FROM_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getTransferTo() == null
					|| transferDetailsList.getTransferTo().getId() == null) {
				errorMap.notify(ErrorDescription.TRANSFER_TO_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getSectionFrom() == null
					|| transferDetailsList.getSectionFrom().getId() == null) {
				errorMap.notify(ErrorDescription.SECTION_FROM_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getSectionTo() == null
					|| transferDetailsList.getSectionTo().getId() == null) {
				errorMap.notify(ErrorDescription.SECTION_TO_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getRelievedOn() == null) {
				errorMap.notify(ErrorDescription.RELIEVED_ON_EMPTY.getErrorCode());
				return false;
			} else if (transferDetailsList.getAssumptionOfCharge() == null) {
				errorMap.notify(ErrorDescription.ASSUMTION_OF_CHANGE_EMPTY.getErrorCode());
				return false;
			}
		}
		return true;
	}

	public String saveTransfer() {
		log.info("<---- : saveTransfer start :------>");
		try {
			log.info("<---- : saveTransfer employeeTransferDetailsList :------>" + employeeTransferDetailsList);
			if (employeeTransferDetailsList == null || employeeTransferDetailsList.isEmpty()) {
				log.error(" Employee Transfer List is Empty ");
				errorMap.notify(ErrorDescription.EMP_TRANSFER_LIST_EMPTY.getErrorCode());
				return null;
			}

			boolean validEntries = true;

			// validEntries = validateTransferDetails();

			if (validEntries) {
				designationId = employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getId();

				employeeMaster.setDesignationId(designationId);

				log.info("saveTransfer url==>" + designationId);
				employeeMaster.setTransferDetailsList(employeeTransferDetailsList);
				log.info("employeeTransferDetailsList:===>" + employeeTransferDetailsList);
				url = EMP_SERVER_URL + "/update";
				BaseDTO baseDTO = httpService.put(url, employeeMaster);

				log.info("saveTransfer url==>" + url);
				log.info("Save Employee Transfer Details Response : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;
					employeeTransferDetailsList = employeeMaster.getTransferDetailsList();
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
				if (baseDTO.getStatusCode() == 0) {

					if (employeeTransferDetailsList.get(0).getId() == null) {
						log.info("Employee Transfer Details Saved Succuessfully ");
						errorMap.notify(ErrorDescription.TRANSFER_SAVED.getErrorCode());
					} else {
						log.info("Employee Transfer Details Updated Succuessfully ");
						errorMap.notify(ErrorDescription.TRANSFER_UPDATE.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeeAdditionalCharge.xhtml?faces-redirect=true";
				} else {
					log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("saveTransfer error==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---- : saveTransfer end :------>");
		return null;
	}

	/**
	 * Propose : load all Entity
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void getPayscaleByDesignation(Designation designation) {
		log.info("getPayscale Started ::" + designation);
		try {
			log.info("getPayscaleByDesignation designation payscale==>"
					+ designation.getCadreMaster().getPayScaleMaster().getId());
			if (payscaleMasterDesList.size() > 0) {
				payscaleMasterDesList = new ArrayList<>();
			}
			if (designation.getCadreMaster().getPayScaleMaster() == null) {
				log.error("Payscale Id Not Exists");
				errorMap.notify(ErrorDescription.PAYSCALE_ID_NOT_EXIST.getErrorCode());
			}
			payScaleMasterDes = commonDataService.getPayscaleByDesignation(designation);
			if (payScaleMasterDes != null) {
				log.info("getPayscaleByDesignation commonDataService==>" + payScaleMasterDes);
				payscaleMasterDesList.add(payScaleMasterDes);
			} else {
				log.info(" :: Payscale null ::");
				errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
			}

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
	}

	/**
	 * Purpose : to load transfer details buffer table
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void loadInterchangeDetails() {
		log.info("Employee Master Bean :: loadInterchangeDetails start");
		if (!action.equalsIgnoreCase("EDIT")) {
			interchangeDetailsList.add(employeeInterchangeDetails);
		} else {
			int index = 0;
			if (interchangeDetailsList.contains(employeeInterchangeDetails)) {
				index = interchangeDetailsList.indexOf(employeeInterchangeDetails);
				interchangeDetailsList.remove(employeeInterchangeDetails);
				interchangeDetailsList.add(index, employeeInterchangeDetails);
			}

		}
		visiableFamButton = "ADDBUTTON";
		employeeInterchangeDetails = new EmployeeInterchangeDetails();
		log.info("Employee Master Bean :: loadInterchangeDetails end");
	}

	/**
	 * @author krishnakumar
	 * @param Employee Interchange
	 * @return
	 */
	public void addInterchange(EmployeeInterchangeDetails employeeInterchange) {

		employeeInterchangeDetails = employeeInterchange;
		getPayscaleByDesignation(employeeInterchangeDetails.getCurrentDesignation());
		entityMasterList = commonDataService.getEntityMaster(employeeInterchangeDetails.getEntityType());
		empInnerFlag = "ADDEMPINNER";
		visiableFamButton = "EDITBUTTON";
		log.info("addInterchange employeeInterchangeDetails==>" + employeeInterchangeDetails);
		// interchangeDetailsList.remove(employeeInterchange);
	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void viewInterchange(EmployeeInterchangeDetails employeeInterchange) {

		employeeInterchangeDetails = employeeInterchange;
		empInnerFlag = "VIEWEMPINNER";
		log.info("viewInterchange employeeInterchange==>" + employeeInterchange);

	}

	/**
	 * @author krishnakumar
	 * @param EmployeeFamilyDetails
	 * @return
	 */
	public void closeInterchange() {
		empInnerFlag = "ADDEMPINNER";
		employeeInterchangeDetails = new EmployeeInterchangeDetails();

	}

	/**
	 * Purpose : to save the employee transfer
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public boolean validateInterchange() {
		Iterator<EmployeeInterchangeDetails> employeeInterchangeDetailsItr = interchangeDetailsList.iterator();
		while (employeeInterchangeDetailsItr.hasNext()) {
			EmployeeInterchangeDetails interchangeDetails = employeeInterchangeDetailsItr.next();

			if (interchangeDetails.getInterchangeReferenceNo() == null) {
				errorMap.notify(ErrorDescription.INTERCHANGE_REFERENCE_NO.getErrorCode());
				return false;
			} else if (interchangeDetails.getInterchangeReferenceDate() == null) {
				errorMap.notify(ErrorDescription.INTERCHANGE_REFERENCE_DATE.getErrorCode());
				return false;
			} else if (interchangeDetails.getEntityType() == null
					|| interchangeDetails.getEntityType().getId() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return false;
			}
			if (interchangeDetails.getInterchangeReferenceNo() != null
					&& !StringUtils.isEmpty(interchangeDetails.getInterchangeReferenceNo())) {

				if (interchangeDetails.getInterchangeReferenceNo().length() < 3
						|| interchangeDetails.getInterchangeReferenceNo().length() > 50) {
					errorMap.notify(ErrorDescription.EMPLOYEE_INTERCHANGE_REF_NO_MAX_MIN.getErrorCode());
					return false;
				}
			}
			if (interchangeDetails.getEntityType() == null || interchangeDetails.getEntityType().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_ENTITY_EMPTY.getErrorCode());
				return false;
			} else if (interchangeDetails.getCurrentDepartment() == null
					|| interchangeDetails.getCurrentDepartment().getId() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_DEPENDENT_NAME.getErrorCode());
				return false;
			} else if (interchangeDetails.getCurrentDesignation() == null
					|| interchangeDetails.getCurrentDesignation().getId() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_DEPENDENT_NAME.getErrorCode());
				return false;
			} else if (interchangeDetails.getInterDepartment() == null
					|| interchangeDetails.getInterDepartment().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return false;
			} else if (interchangeDetails.getPayscale() == null || interchangeDetails.getPayscale().getId() == null) {
				errorMap.notify(ErrorDescription.EMP_PAY_SCALE_EMPTY.getErrorCode());
				return false;
			}
		}
		return true;
	}

	public String saveInterchange() {
		log.info("<---- : saveInterchange start :------>");
		try {
			log.info("<---- : saveInterchange interchangeDetailsList :------>" + interchangeDetailsList);
			if (interchangeDetailsList == null || interchangeDetailsList.isEmpty()) {
				log.error(" Employee Interchange List is Empty ");
				errorMap.notify(ErrorDescription.EMP_INTERCHANGE_LIST_EMPTY.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validateInterchange();
			if (validEntries) {
				employeeMaster.setInterchangeDetailsList(interchangeDetailsList);
				log.info("interchangeDetailsList:===>" + interchangeDetailsList);
				url = EMP_SERVER_URL + "/update";
				BaseDTO baseDTO = httpService.put(url, employeeMaster);

				log.info("saveInterchange url==>" + url);
				log.info("Save Employee Transfer Details Response : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;
					interchangeDetailsList = employeeMaster.getInterchangeDetailsList();
				} else {
					log.error("employee object failed to desearlize==>" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
				if (baseDTO.getStatusCode() == 0) {

					if (interchangeDetailsList.get(0).getId() == null) {
						log.info("Employee Interchange Details Saved Succuessfully ");
						errorMap.notify(ErrorDescription.INTERCHANGE_SAVED.getErrorCode());
					} else {
						log.info("Employee Interchange Details Updated Succuessfully ");
						errorMap.notify(ErrorDescription.INTERCHANGE_UPDATE.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeeTransferDetails.xhtml?faces-redirect=true";
				} else {
					log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("saveInterchange error==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<---- : saveInterchange end :------>");
		return null;
	}

	/**
	 * Propose : delete the table row
	 * 
	 * @author krishnakumar
	 * @param employeeFamilyDetails
	 * @return String
	 */
	public void deleteTransfer(EmployeeTransferDetails transferDetails) {
		log.info("[ ----- Delete Single Record -----]");
		try {
			if (action.equalsIgnoreCase("ADD")) {
				employeeTransferDetailsList.remove(transferDetails);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}
			if (action.equals("EDIT")) {
				log.info("deleteTransfer transfer id==>" + transferDetails.getId());
				if (transferDetails.getId() != null) {
					String url = EMP_SERVER_URL + "/deletetransfer/id/" + transferDetails.getId();
					httpService.delete(url);
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				}
				employeeTransferDetailsList.remove(transferDetails);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}
		} catch (Exception e) {
			log.error("[ Exception in Delete Employee Record : ]", e);
		}
	}

	/**
	 * Propose : delete the table row
	 * 
	 * @author krishnakumar
	 * @param employeeFamilyDetails
	 * @return String
	 */
	public void deleteInterchange(EmployeeInterchangeDetails employeeInterchange) {
		log.info(" Delete Single Record  for deleteInterchange :: " + employeeInterchange);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				if (interchangeDetailsList != null && interchangeDetailsList.size() > 0) {
					interchangeDetailsList.remove(employeeInterchange);
					log.info(" Employee Interchange Row Deleted ");
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				} else {
					log.error(" Employee Interchange List Is Empty ");
					errorMap.notify(ErrorDescription.EMP_INTERCHANGE_LIST_EMPTY.getErrorCode());
				}

			}
			if (action.equals("EDIT")) {
				if (employeeInterchange.getId() != null) {
					url = EMP_SERVER_URL + "/deleteinterchange/id/" + employeeInterchange.getId();
					log.info("Delete Single Record  for Employee Family URL ::  " + url);
					BaseDTO baseDTO = httpService.delete(url);
					log.info("Delete Single Record  for Employee Family Response  :: " + baseDTO);
					if (baseDTO == null) {
						log.error("Base DTO Returned Null Value");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return;
					}
					if (baseDTO.getStatusCode() == 0) {
						interchangeDetailsList.remove(employeeInterchange);
						errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
						log.info("Employee Family Details Saved Succuessfully ");
					} else {
						log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
				}
				interchangeDetailsList.remove(employeeInterchange);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}

		} catch (Exception e) {
			log.error(" Exception in Delete Employee Interchange Record : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * @param event
	 * @author Bijay
	 * @return
	 */
	public String scannedDocumentUpload(FileUploadEvent event) {
		EmployeeScannedDocument scannedDocument = new EmployeeScannedDocument();
		try {
			if (event == null || event.getFile() == null) {
				log.error("Uploaded Document is Empty");
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_IS_EMPTY.getErrorCode());
			}

			file = event.getFile();
			String type = FilenameUtils.getExtension(file.getFileName());
			log.info(" File Type :: " + type);
			if (!type.equalsIgnoreCase("png") && !type.equalsIgnoreCase("jpg") && !type.equalsIgnoreCase("doc")
					&& !type.equalsIgnoreCase("jpeg") && !type.equalsIgnoreCase("docx")
					&& !type.equalsIgnoreCase("pdf")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_TYPE_ERROR.getErrorCode());
				return null;
			}
			long size = file.getSize();
			size = size / 1000;
			log.info(" Uploaded Employee Scanned Document Size in KB " + size);

			if (size < employeeScannedMinDocSize || size > employeeScannedMaxDocSize) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_SIZE_ERROR.getErrorCode());
				return null;
			}

			byte[] doc = IOUtils.toByteArray(file.getInputstream());
			scannedDocument.setDocumentName(file.getFileName());
			scannedDocument.setDocumentType(type);
			scannedDocument.setDocument(doc);
			scannedDocument.setDocumentSize(size);
			/// scannedDocument.setCreatedBy(loginBean.getUserDetailSession());
			scannedDocument.setCreatedDate(new Date());
			employeeScannedDocumentList.add(scannedDocument);

			log.info(" Employee Uploaded Scanned Document List :: " + employeeScannedDocumentList);

		} catch (Exception e) {
			log.error(" Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void deleteScannedDocument(EmployeeScannedDocument document) {
		log.info(" Selected Employee Scanned Document :: " + document);
		try {
			if (document == null) {
				log.error(" Selected Employee Scanned Document is Empty");
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_IS_EMPTY.getErrorCode());
				return;
			}
			if (document.getId() == null) {
				log.info(" Deleting from table list ");
				employeeScannedDocumentList.remove(document);
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_DELETE_SUCCESS.getErrorCode());

			} else {
				url = EMP_SERVER_URL + "/deletescanneddocument/id/" + document.getId();
				log.info(" Employee Delete Scanned Document URL :- " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info(" Employee Delete Scanned Document Response :- " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				if (baseDTO.getStatusCode() == 0) {
					log.info(" Scanned document record deleted Successfully");
					errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_DELETE_SUCCESS.getErrorCode());
					employeeScannedDocumentList.remove(document);
				} else {
					log.error(" Error Occured while deleting employee Scanned document with status code :: "
							+ baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception Ocuured While Deleting employee Sacnned Document");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String saveEmployeeScannedDocument() {
		log.info("Employee Scanned Document List:: " + employeeScannedDocumentList);
		try {
			if (employeeMaster == null || employeeScannedDocumentList == null) {
				log.error(" Employee Scanned Document Is Empty");
				errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_IS_EMPTY.getErrorCode());
				return null;
			}
			employeeMaster.setEmployeeScannedDocumentList(employeeScannedDocumentList);
			url = EMP_SERVER_URL + "/update";
			log.info(" Save Employee Scanned Document Url :: " + url);
			BaseDTO baseDto = httpService.put(url, employeeMaster);
			log.info(" Save Employee Scanned Document Response :: " + url);

			if (baseDto == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);

			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}

			if (baseDto.getStatusCode() == 0) {
				log.info(" Employee scanned document saved successfully");
				employeeScannedDocumentList = employeeMaster.getEmployeeScannedDocumentList();
				errorMap.notify(ErrorDescription.EMPLOYEE_UPDATED_SUCCESSFULLY.getErrorCode());
				selectedEmployee = new EmployeeSearchResponse();
				addButtonFlag = false;
				return "/pages/personnelHR/listEmployeeServiceRegister.xhtml?faces-redirect=true";
			} else {
				log.error(" Employee scanned document failed to save with status code :: " + baseDto.getStatusCode());
				errorMap.notify(baseDto.getStatusCode());
			}

		} catch (Exception e) {
			log.error("Exception Ocuured While saving employee Sacnned Document");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void downloadScannedDocumment(EmployeeScannedDocument document) {
		log.info(" Download Scanned Document :: " + document);
		if (document == null) {
			log.error(" Selected Employee Scanned Document is Empty");
			errorMap.notify(ErrorDescription.EMP_SCANNED_DOC_IS_EMPTY.getErrorCode());
			return;
		}
		byte[] doc = document.getDocument();
		String name = document.getDocumentName();
		String type = document.getDocumentType();
		downloadScannedDoc = new DefaultStreamedContent(new ByteArrayInputStream(doc), type, name);
		log.info(" Scanned Cocument :: " + downloadScannedDoc);
	}

	public DefaultStreamedContent getDownloadScannedDoc() {
		log.info(" Download Stream content called :: " + downloadScannedDoc.getName());
		return downloadScannedDoc;
	}

	public void onEntityTypeChange() {
		try {

			if (employeePromotionDetail.getEntityTypeMaster() != null) {
				Long entityTypeId = employeePromotionDetail.getEntityTypeMaster().getId();
				String url = AppUtil.getPortalServerURL() + "/entitymaster/getbyid/" + entityTypeId;
				log.info("Get Entity Masters on Entity Type Change URL :: " + url);
				BaseDTO entityTypeDTO = httpService.get(url);
				log.info("Get Entity Masters on Entity Type Change Response Value :: " + entityTypeDTO);
				if (entityTypeDTO == null) {
					log.error("Get Entity Masters on Entity Type Change Response null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				} else {
					jsonResponse = mapper.writeValueAsString(entityTypeDTO.getResponseContent());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					if (entityMasterList == null) {
						log.info("Get Entity Masters on Entity Type Change List null :: ");
					} else {
						log.info("Get Entity Masters on Entity Type Change List Size :: {} ", entityMasterList.size());
					}
					log.info("Get Entity Masters on Entity Type Change Response Value :: " + entityTypeDTO);
				}
			} else {
				log.error("Get Entity Masters on Entity Type Change Id not found");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("Exception While Load Entity Master on Entity Type Change ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public boolean validatePromotionDetails() {
		Iterator<EmployeePromotionDetails> empPromotionItr = employeePromotionDetailsList.iterator();
		while (empPromotionItr.hasNext()) {
			EmployeePromotionDetails promotionDetail = empPromotionItr.next();
			if (promotionDetail.getPromotionReferenceNo() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_REF_NO.getErrorCode());
				return false;
			}
			if (promotionDetail.getPromotionReferenceDate() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_REF_DATE.getErrorCode());
				return false;
			}

			if (promotionDetail.getEntityTypeMaster() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return false;
			}

			if (promotionDetail.getDepartment() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_DEPARTMENT.getErrorCode());
				return false;
			}

			if (promotionDetail.getCurruntDesignation() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_CURR_DESIGNATION.getErrorCode());
				return false;
			}

			if (promotionDetail.getCurruntPayscale() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_CURR_PAYSCALE.getErrorCode());
				return false;
			}

			if (promotionDetail.getPromotedDesignation() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTED_DESIG_EMPTY.getErrorCode());
				return false;
			}

			if (promotionDetail.getRevisedPayscale() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_REF_PAYSCALE.getErrorCode());
				return false;
			}

			if (promotionDetail.getPromotionEffectiveDate() == null) {
				errorMap.notify(ErrorDescription.WARN_EMP_PROMOTION_EFFECTIVE_DATE.getErrorCode());
				return false;
			}
		}
		return true;
	}

	public String promotionDetailsSaveContinue() {
		BaseDTO promotionDTO = new BaseDTO();
		try {
			log.info(" Employee Promotion Details Request :: " + employeePromotionDetailsList);
			if (employeePromotionDetailsList == null || employeePromotionDetailsList.isEmpty()) {
				log.info(" Save - Employee Promotion Details Return null :: " + employeePromotionDetailsList);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validatePromotionDetails();
			if (validEntries) {
				employeeMaster.setEmployeePromotionDetails(employeePromotionDetailsList);
				String url = EMP_SERVER_URL + "/update";
				log.info(" Save - Employee Promotion Details URL  :: {}", url);
				promotionDTO = httpService.put(url, employeeMaster);
				log.info(" Save - Employee Promotion Details BaseDTO Response  :: " + promotionDTO);
				if (promotionDTO == null) {
					log.info(" Save - Employee Promotion Details BaseDTO null  :: " + promotionDTO);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(promotionDTO.getResponseContent());
				EmployeeMaster promotionEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (promotionEmployee != null) {
					employeeMaster = promotionEmployee;
					employeePromotionDetailsList = employeeMaster.getEmployeePromotionDetails();
				} else {
					log.error("Employee Promotion Details Failed to Deserialize ");
					errorMap.notify(promotionDTO.getStatusCode());
					return null;
				}
				if (promotionDTO.getStatusCode() == 0) {
					if (employeePromotionDetailsList.get(0).getPromotionId() == null) {
						log.info(" Save - Employee Promotion Details Succuss Response" + promotionDTO);
						errorMap.notify(ErrorDescription.EMP_PROMOTION_DETAILS_CREATED.getErrorCode());
					} else {
						log.info(" Update - Employee Promotion Details Succuss Response" + promotionDTO);
						errorMap.notify(ErrorDescription.EMP_PROMOTION_DETAILS_UPDATED.getErrorCode());
					}

				} else {
					log.info(" Save - Employee Promotion Details Failure Response");
					errorMap.notify(promotionDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception While Saving Employee Promotion Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeInterchange.xhtml?faces-redirect=true;";
	}

	public void clearPromotionDetails() {
		employeePromotionDetail = new EmployeePromotionDetails();
		visibleButton = "ADDPROMOTION";
		RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel");
	}

	public String addPromotionDetails() {
		/*
		 * In cooptex, 'PromotionReferenceNo' and 'ProceedingReferenceNumber' are
		 * repeated. Unique cannot be checked for this.
		 * 
		 * For this, unique check is disabled using a boolean value
		 * 'checkProceedingRefNoForUnique'
		 */

		boolean checkProceedingRefNoForUnique = false;
		boolean checkPromotionRefNoForUnique = false;
		boolean nonUniqueCheck = true;

		log.info(" Employee Promotion Details :: " + employeePromotionDetail);
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (employeePromotionDetail == null) {
				log.error("Employee Promotion Details Value is null ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (checkPromotionRefNoForUnique) {
				if (employeePromotionDetail.getPromotionReferenceNo() != null) {
					String url = AppUtil.getPortalServerURL() + "/promotion/exist";
					baseDTO = recordExist(url, employeePromotionDetail);
					if (baseDTO.getStatusCode() != 0) {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			}

			if (checkProceedingRefNoForUnique) {
				if (employeePromotionDetail.getProceedingReferenceNumber() != null) {
					String url = AppUtil.getPortalServerURL() + "/promotion/exist";
					baseDTO = recordExist(url, employeePromotionDetail);
					if (baseDTO.getStatusCode() != 0) {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			}

			if (nonUniqueCheck) {
				Set<String> uniqueNo = new HashSet<String>();
				employeePromotionDetailsList.add(employeePromotionDetail);
				for (EmployeePromotionDetails uniquePromotion : employeePromotionDetailsList) {
					if (!uniqueNo.add(uniquePromotion.getPromotionReferenceNo())) {
						employeePromotionDetailsList.remove(uniquePromotion);
						errorMap.notify(ErrorDescription.EMP_PROMOTION_REF_NO_EXIST.getErrorCode());
						RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel:promotionTable");
						return null;
					}
				}
			}
			employeePromotionDetail = new EmployeePromotionDetails();
			log.info("Employee Promotion Details List while adding :: {} ", employeePromotionDetailsList.size());
			RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel:promotionTable");
		} catch (Exception e) {
			log.error("Exception While Adding Employee Promotion Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * Delete Promotion Details Record
	 * 
	 * @param promotionDetail
	 * @return
	 */
	public void deletePromotionRecord(EmployeePromotionDetails promotionDetail) {
		log.info(" Employee Promotion Details Delete record :: " + promotionDetail);

		try {
			if (promotionDetail == null) {
				log.info(" Employee Promotion Details Delete id null ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (promotionDetail.getPromotionId() != null) {
				log.info(" Employee Promotion Details Delete id :: " + promotionDetail.getPromotionId());
				String url = AppUtil.getPortalServerURL() + "/promotion/delete/" + promotionDetail.getPromotionId();
				log.info(" Delete - Employee Promotion Details URL :: " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info(" Delete - Employee Promotion Details Response :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Delete - Employee Promotion Deatil null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				if (baseDTO != null) {
					log.info(" Deleted - Employee Promotion Record Id : {} ", baseDTO.getStatusCode());
					employeePromotionDetailsList.remove(promotionDetail);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel:promotionTable");
				}
			} else {
				log.info(" :: Deleted - Employee Promotion Details :: ");
				employeePromotionDetailsList.remove(promotionDetail);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				if (employeePromotionDetailsList.size() == 0) {
					employeePromotionDetailsList = new ArrayList<EmployeePromotionDetails>();
				}
			}
			employeePromotionDetail = new EmployeePromotionDetails();
			RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel:promotionTable");
		} catch (Exception e) {
			log.error("Exception Delete Employee Promotion Deatil", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void viewPromotionDetails(EmployeePromotionDetails viewPromotionDetails) {
		log.info(" View Employee Promotion Deatil :: " + viewPromotionDetails);
		try {
			if (viewPromotionDetails == null) {
				log.error(" :: View Employee Promotion Deatil null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeePromotionDetail = viewPromotionDetails;
			visibleButton = "VIEWPROMOTION";
			RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel");
		} catch (Exception e) {
			log.error("Exception View Employee Promotion Deatil", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void editPromotionDetails(EmployeePromotionDetails editPromotionDetails) {
		try {
			if (editPromotionDetails == null) {
				log.error(" :: Edit Employee Promotion Deatil null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeePromotionDetail = editPromotionDetails;
			log.info(" Selected Employee Promotion Deatil :: " + employeePromotionDetail);
			visibleButton = "UPDATEPROMOTION";
			RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel");
		} catch (Exception e) {
			log.error("Exception Edit Employee Promotion Deatil", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void updatePromotionDetails() {
		log.info(" Upade Employee Promotion Deatil :: " + employeePromotionDetail);
		try {
			if (employeePromotionDetail == null) {
				log.error(" :: Upade Employee Promotion Deatil Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (employeePromotionDetailsList.size() > 0) {
				for (EmployeePromotionDetails deatails : employeePromotionDetailsList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(employeePromotionDetail.getRowIndex())) {
						log.info(" :: Upadeted - Employee Promotion Deatil ::");
						employeePromotionDetailsList.set(deatails.getRowIndex(), employeePromotionDetail);
					}
				}
			} else {
				log.info(" :: Upadeted - Employee Promotion Deatil ::");
				employeePromotionDetailsList.add(employeePromotionDetail);
			}
			employeePromotionDetail = new EmployeePromotionDetails();
			visibleButton = "ADDPROMOTION";
			RequestContext.getCurrentInstance().update("empPromotionForm:promotionPanel");
		} catch (Exception e) {
			log.error("Exception Upade Employee Promotion Deatil", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public BaseDTO recordExist(String url, Object object) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("Record Exist URL ::" + url);
			baseDTO = httpService.post(url, object);
			log.info("Record Exist Response BaseDTO ::" + baseDTO);
			if (baseDTO == null) {
				log.info("Record Exist BaseDTO Response null value");
				return baseDTO;
			}
		} catch (Exception e) {
			log.error("Exception in Record Exist", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String addDisciplinaryAction() {
		log.info(" :: ADD - Employee Disciplinary Action :: " + employeeDisciplinaryAction);
		try {
			if (employeeDisciplinaryAction == null) {
				log.error(" :: Add - Employee Disciplinary Action Object is null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			Set<String> uniqueNo = new HashSet<String>();
			employeeDisciplinaryActionList.add(employeeDisciplinaryAction);
			for (EmployeeDisciplinaryAction uniqueDisciplinary : employeeDisciplinaryActionList) {
				if (!uniqueNo.add(uniqueDisciplinary.getDisciplinaryReferenceNumber())) {
					employeeDisciplinaryActionList.remove(uniqueDisciplinary);
					errorMap.notify(ErrorDescription.EMP_DISCIPLINARY_REF_EXIST.getErrorCode());
					RequestContext.getCurrentInstance()
							.update("empDisciplinaryForm:disciplinaryPanel:disciplinaryTable");
					return null;
				}
			}
			employeeDisciplinaryAction = new EmployeeDisciplinaryAction();
			log.info("Employee Disciplinary Action List while adding :: {} ", employeeDisciplinaryActionList.size());
			RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel:disciplinaryTable");
		} catch (Exception e) {
			log.error("Exception While Adding Disciplinary Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void editDisciplinaryAction(EmployeeDisciplinaryAction editDisciplinaryAction) {
		log.info(":: Edit - Employee Disciplinary Action :: " + editDisciplinaryAction);
		try {
			if (editDisciplinaryAction == null) {
				log.error(":: Edit - Employee Disciplinary Action Object null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeDisciplinaryAction = editDisciplinaryAction;
			visibleButton = "UPDATEDISCIPLINARY";
			log.info(" Selected Employee Disciplinary Action :: " + employeeDisciplinaryAction);
			RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel");
		} catch (Exception e) {
			log.error("Exception While Editing Disciplinary Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void updateDisciplinaryAction() {
		log.info(" :: Upadate - Employee Disciplinary Action :: ");
		try {
			if (employeeDisciplinaryAction == null) {
				log.error(" :: Update - Employee Disciplinary Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (employeeDisciplinaryActionList.size() > 0) {
				for (EmployeeDisciplinaryAction deatails : employeeDisciplinaryActionList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(employeeDisciplinaryAction.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						employeeDisciplinaryActionList.set(deatails.getRowIndex(), employeeDisciplinaryAction);
					}
				}
			} else {
				log.info(" :: Updated - Employee Disciplinary Action :: ");
				employeeDisciplinaryActionList.add(employeeDisciplinaryAction);
			}
			employeeDisciplinaryAction = new EmployeeDisciplinaryAction();
			visibleButton = "ADDDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel");
		} catch (Exception e) {
			log.error("Exception occured while Update Employee Disciplinary Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @param viewDisciplinaryAction
	 */
	public void viewDisciplinaryAction(EmployeeDisciplinaryAction viewDisciplinaryAction) {
		log.info(" :: View - Employee Disciplinary Action :: ");
		try {
			log.info(" View - Employee Disciplinary Action :: " + viewDisciplinaryAction);
			if (viewDisciplinaryAction == null) {
				log.error(" :: View - Employee Disciplinary Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeDisciplinaryAction = viewDisciplinaryAction;
			visibleButton = "VIEWDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel");
		} catch (Exception e) {
			log.error("Exception occured while saving Employee Disciplinary Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearDisciplinary() {
		employeeDisciplinaryAction = new EmployeeDisciplinaryAction();
		visibleButton = "ADDDISCIPLINARY";
		RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel");
	}

	/**
	 * @return
	 */
	public String saveDisciplinaryAction() {
		log.info(" :: Save - Employee Disciplinary Action :: " + employeeDisciplinaryActionList);
		try {
			if (employeeDisciplinaryActionList == null || employeeDisciplinaryActionList.isEmpty()) {
				log.info(" :: Save - Employee Disciplinary Action Object Null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			employeeMaster.setDisciplinaryActionList(employeeDisciplinaryActionList);
			String url = EMP_SERVER_URL + "/update";
			log.info("Save - Employee Disciplinary Action URL :: " + url);
			BaseDTO disciplinaryDTO = httpService.put(url, employeeMaster);
			log.info("Save - Employee Disciplinary Action BaseDTO Response Value :: " + disciplinaryDTO);
			if (disciplinaryDTO == null) {
				log.error("Save - Employee Disciplinary Action BaseDTO Response null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(disciplinaryDTO.getResponseContent());
			EmployeeMaster disciplinaryEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (disciplinaryEmployee != null) {
				employeeMaster = disciplinaryEmployee;
			} else {
				log.error("Employee Disciplinary Action Failed to Deserialize ");
				errorMap.notify(disciplinaryDTO.getStatusCode());
				return null;
			}
			if (disciplinaryDTO.getStatusCode() == 0) {
				if (employeeDisciplinaryActionList.get(0).getDisciplinaryId() == null) {
					log.info("Save - Employee Disciplinary Action Successfully");
					errorMap.notify(ErrorDescription.EMP_DISCIPLINARY_ACTION_CREATED.getErrorCode());
				} else {
					log.info("Update - Employee Disciplinary Action Successfully");
					errorMap.notify(ErrorDescription.EMP_DISCIPLINARY_ACTION_UPDATED.getErrorCode());
				}
			} else {
				log.info("Save - Employee Disciplinary Action error " + disciplinaryDTO.getStatusCode());
				errorMap.notify(disciplinaryDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while saving Employee Disciplinary Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeSuspensionDetails.xhtml?faces-redirect=true;";
	}

	/**
	 * @param disciplinaryAction
	 */
	public void deleteDisciplinaryAction(EmployeeDisciplinaryAction disciplinaryAction) {
		try {
			log.info(" Employee Disciplinary Action Delete record :: " + disciplinaryAction);
			if (disciplinaryAction == null) {
				log.error(" :: Employee Disciplinary Action Delete Object null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (disciplinaryAction.getDisciplinaryId() != null) {
				String url = SERVER_URL + "/disciplinaryaction/delete/id/" + disciplinaryAction.getDisciplinaryId();
				log.info(" Delete - Employee Disciplinary Action URL :: " + url);
				BaseDTO deleteDisciplinaryDTO = httpService.delete(url);
				log.info(" Delete - Employee Disciplinary Action Response :: " + deleteDisciplinaryDTO);
				if (deleteDisciplinaryDTO != null) {
					log.info(" Deleted - Employee Disciplinary Action Record Id : {} ",
							disciplinaryAction.getDisciplinaryId());
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeeDisciplinaryActionList.remove(disciplinaryAction);
					disciplinaryAction = new EmployeeDisciplinaryAction();
				}
			} else {
				log.info(" :: Deleted - Employee Disciplinary Action :: ");
				employeeDisciplinaryActionList.remove(disciplinaryAction);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				if (employeeDisciplinaryActionList.size() == 0) {
					employeeDisciplinaryAction = new EmployeeDisciplinaryAction();
				}
			}
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(false);
			RequestContext.getCurrentInstance().update("empDisciplinaryForm:disciplinaryPanel:disciplinaryTable");
		} catch (Exception e) {
			log.error("Exception occured while Delete Employee Disciplinary Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onEntityMasterChange() {
		log.info("onEntityMasterChange Start");
		try {
			Long entityTypeId = employeeDisciplinaryAction.getEntityTypeMaster().getId();
			if (entityTypeId == null) {
				log.error("Get Entity Masters on Entity Type Change Id not found");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			} else {
				BaseDTO relationShipBaseDTO = new BaseDTO();
				String url = AppUtil.getPortalServerURL() + "/entitymaster/getbyid/" + entityTypeId;
				relationShipBaseDTO = httpService.get(url);
				if (relationShipBaseDTO == null) {
					log.error("Get Entity Masters on Entity Type Change Response null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				if (relationShipBaseDTO != null) {
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while fetching EntityMasterType ... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void addLeaveDetail() {
		log.info("<------Leave added to list ---->" + employeeLeaveDetail);
		if (employeeLeaveDetail.getFromDate().after(employeeLeaveDetail.getToDate())) {
			Util.addWarn("TO date Should greater than From date");
			return;
		}
		for (EmployeeLeaveDetails employeeLeaveDetails : employeeLeaveDetailsList) {

			if (employeeLeaveDetails != null && employeeLeaveDetails.getFromDate() != null
					&& employeeLeaveDetails.getToDate() != null) {
				if (employeeLeaveDetails.getFromDate().equals(employeeLeaveDetail.getFromDate())
						|| employeeLeaveDetails.getToDate().equals(employeeLeaveDetail.getFromDate())
						|| (employeeLeaveDetails.getFromDate().before(employeeLeaveDetail.getFromDate())
								&& employeeLeaveDetails.getToDate().after(employeeLeaveDetail.getFromDate()))) {
					Util.addWarn("Leave Already Applied For This Date");
					return;
				}
			}
		}
		employeeLeaveDetailsList.add(employeeLeaveDetail);
		employeeLeaveDetail = new EmployeeLeaveDetails();
	}

	public void addEarnedLeaveDetail() {
		log.info("<------Leave added to list ---->" + employeeEarnedLeaveDetail);

		if (employeeEarnedLeaveDetail.getDutyFromDate().after(employeeEarnedLeaveDetail.getDutyToDate())) {
			Util.addWarn("Duty TO date Should greater than Duty From date");
			return;
		}
		if (employeeEarnedLeaveDetail.getFromDate().after(employeeEarnedLeaveDetail.getToDate())) {
			Util.addWarn("Leave TO date Should greater than Leave From date");
			return;
		}
//		employeeEarnedLeaveDetail.setConsumedLeave(employeeEarnedLeaveDetail.getCreditedLeave()-employeeEarnedLeaveDetail.getEarnedLeaveBalance());
		employeeEarnedLeaveDetailsList.add(employeeEarnedLeaveDetail);
		employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
	}

	public void editLeaveDetail(int idx, EmployeeLeaveDetails leaveDetails) {
		log.info("<---Edit leave called rowIndex---->" + idx + "  " + leaveDetails);
		employeeLeaveDetail = leaveDetails;
		rowIndex = idx;
		action = "EDITLEAVEDETAIL";
		// RequestContext.getCurrentInstance().update("leaveDetailPannel");
	}

	public void editEarnedLeaveDetail(int idx, EmployeeLeaveDetails leaveDetails) {
		log.info("<---Edit earned leave called rowIndex---->" + idx + "  " + leaveDetails);
		employeeEarnedLeaveDetail = leaveDetails;
		rowIndex = idx;
		action = "EDITEARNEDLEAVEDETAIL";
		// RequestContext.getCurrentInstance().update("leaveDetailPannel");
	}

	public void updateLeaveDetail() {
		log.info("leave details UPDATE is pressed with index [ " + rowIndex + " ]" + "[" + employeeLeaveDetail + "]");
		action = "ADD";
		if (employeeLeaveDetail != null && employeeLeaveDetailsList.size() != 0) {
			employeeLeaveDetailsList.set(rowIndex, employeeLeaveDetail);
		}
		employeeLeaveDetail = new EmployeeLeaveDetails();
		// loadPanel("workForm:workExperiencePanel");

	}

	public void updateEarnedLeaveDetail() {
		log.info("leave details UPDATE is pressed with index [ " + rowIndex + " ]" + "[" + employeeEarnedLeaveDetail
				+ "]");
		action = "ADD";
		/*
		 * employeeEarnedLeaveDetail.setConsumedLeave
		 * (employeeEarnedLeaveDetail.getCreditedLeave()-employeeEarnedLeaveDetail.
		 * getEarnedLeaveBalance());
		 */
		if (employeeEarnedLeaveDetail != null && employeeEarnedLeaveDetailsList.size() != 0) {
			employeeEarnedLeaveDetailsList.set(rowIndex, employeeEarnedLeaveDetail);
		}
		employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
		// loadPanel("workForm:workExperiencePanel");

	}

	public void clearLeaveDetail() {
		log.info("clear called");
		action = "ADD";
		employeeLeaveDetail = new EmployeeLeaveDetails();
		employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
		RequestContext.getCurrentInstance().update("empLeaveDetailsForm:leaveDetailPannel");
	}

	public void deleteLeaveDetail(int idx, EmployeeLeaveDetails leaveDetails) {
		try {
			log.info("Leave Details Delete is called : [" + leaveDetails + "]");
			if (leaveDetails == null) {
				log.info(" Employee Leave Details Is Null ");
				errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
			}
			if (leaveDetails.getId() == null && employeeLeaveDetailsList != null) {
				employeeLeaveDetailsList.remove(idx);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				log.info("<-----------Deleted-------->");
			} else {
				url = EMP_SERVER_URL + "/delleavedetail/" + leaveDetails.getId();
				log.info(" Delete Leave Details Called For URL :: " + url);
				BaseDTO baseDto = httpService.delete(url);
				log.info(" Delete Work Experience Retured Response :: " + baseDto);
				if (baseDto == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}

				if (baseDto.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeeLeaveDetailsList.remove(idx);
					log.info("Employee Leave Details Deleted Succuessfully ");
				} else {
					log.error("Error while Deleting Leave Details with error code :: " + baseDto.getStatusCode());
					errorMap.notify(baseDto.getStatusCode());
				}
			}

		} catch (Exception exp) {
			log.error("deletion failed in work experienc", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void deleteEarnedLeaveDetail(int idx, EmployeeLeaveDetails leaveDetails) {
		try {
			log.info("Leave Details Delete is called : [" + leaveDetails + "]");
			if (leaveDetails == null) {
				log.info(" Employee Leave Details Is Null ");
				errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
			}
			if (leaveDetails.getId() == null && employeeEarnedLeaveDetailsList != null) {
				employeeEarnedLeaveDetailsList.remove(idx);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				log.info("<-----------Deleted-------->");
				// loadPanel("workForm:workExperiencePanel");
			} else {
				url = EMP_SERVER_URL + "/delleavedetail/" + leaveDetails.getId();
				log.info(" Delete Leave Details Called For URL :: " + url);
				BaseDTO baseDto = httpService.delete(url);
				log.info(" Delete Work Experience Retured Response :: " + baseDto);
				if (baseDto == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}

				if (baseDto.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeeEarnedLeaveDetailsList.remove(idx);
					log.info("Employee Leave Details Deleted Succuessfully ");
				} else {
					log.error("Error while Deleting Leave Details with error code :: " + baseDto.getStatusCode());
					errorMap.notify(baseDto.getStatusCode());
				}
			}

		} catch (Exception exp) {
			log.error("deletion failed in work experienc", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void closeViewPanel() {
		action = "ADD";
		employeeLeaveDetail = new EmployeeLeaveDetails();
		employeeEarnedLeaveDetail = new EmployeeLeaveDetails();
	}

	public void viewLeaveDetail(EmployeeLeaveDetails leaveDetails) {
		log.error(" Employee leave Details is Empty :: " + leaveDetails);
		if (leaveDetails == null) {
			log.error(" Employee Experience Details is Empty :: " + leaveDetails);
			errorMap.notify(ErrorDescription.EMP_EXP_IS_EMPTY.getErrorCode());
			return;
		}
		try {
			action = "VIEWLEAVEDETAIL";
			employeeLeaveDetail = leaveDetails;
			RequestContext.getCurrentInstance().update("empLEaveDetailsForm:leaveDetailPannel");
			// loadPanel("empLEaveDetailsForm:leaveDetailPannel");
		} catch (Exception e) {
			log.error("Exception Occured wile view employee experience details", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void viewEarnedLeaveDetail(EmployeeLeaveDetails leaveDetails) {
		log.error(" Employee earned  Details is Empty :: " + leaveDetails);
		action = "VIEWEARNEDLEAVEDETAIL";
		employeeEarnedLeaveDetail = leaveDetails;
		// loadPanel("empLEaveDetailsForm:leaveDetailPannel");

	}

	public boolean validateLeaveDetails() {
		Iterator<EmployeeLeaveDetails> employeeLeaveDetailsItr = leaveDetailsList.iterator();
		while (employeeLeaveDetailsItr.hasNext()) {
			EmployeeLeaveDetails leaveDetail = employeeLeaveDetailsItr.next();

			if (ESRPageName.EMPLOYEE_EARNED_LEAVE_DETAILS.equals(employeeMaster.getPageName())) {
				log.info("Inside Earned Leave Type Page");
			} else {
				if (leaveDetail.getLeaveTypeMaster() == null) {
					errorMap.notify(ErrorDescription.EMP_LEAVE_TYPE_MANDATORY.getErrorCode());
					return false;
				}
			}
			/*
			 * if (leaveDetail.getLeaveTypeMaster() == null) {
			 * errorMap.notify(ErrorDescription.EMP_LEAVE_TYPE_MANDATORY.getErrorCode());
			 * return null; } else
			 */
			if (leaveDetail.getRefferenceNumber() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_NUMBER_MANDATORY.getErrorCode());
				return false;
			} else if (leaveDetail.getRefferenceNumber() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_NUMBER_MAX_LENGTH.getErrorCode());
				return false;
			} else if (leaveDetail.getRefferenceDate() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (leaveDetail.getFromDate() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_FROM_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (leaveDetail.getToDate() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_TO_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (leaveDetail.getNoOfDays() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_NO_OF_DAYS_MANDATORY.getErrorCode());
				return false;
			}
			if (ESRPageName.EMPLOYEE_EARNED_LEAVE_DETAILS.equals(employeeMaster.getPageName())) {
				log.info("Inside Earned Leave Type Page");
			} else {
				LeaveTypeMaster leaveTypeMasters = leaveDetail.getLeaveTypeMaster();
				String leaveTypeCode = leaveDetail.getLeaveTypeMaster().getCode();
				if (leaveTypeMasters != null) {
					if ("L002".equals(leaveTypeCode) || "L005".equals(leaveTypeCode)) {
						if (leaveDetail.getProgressiveTotal() == null) {
							errorMap.notify(ErrorDescription.EMP_LEAVE_PROGRESSIVE_TOTAL_MANDATORY.getErrorCode());
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public String saveLeaveDetails() {
		leaveDetailsList = new ArrayList<>();

		/*
		 * LeaveTypeMaster leaveTypeMaster = new LeaveTypeMaster();
		 * leaveTypeMaster.setCode("L006"); for (EmployeeLeaveDetails earnedLeaveDetail
		 * : employeeEarnedLeaveDetailsList) {
		 * earnedLeaveDetail.setLeaveTypeMaster(leaveTypeMaster);
		 * leaveDetailsList.add(earnedLeaveDetail); }
		 */
		if (employeeMaster != null) {
			if (ESRPageName.EMPLOYEE_EARNED_LEAVE_DETAILS.equals(employeeMaster.getPageName())) {
				leaveDetailsList.addAll(employeeEarnedLeaveDetailsList);
			} else {
				leaveDetailsList.addAll(employeeLeaveDetailsList);
			}
		}
		try {
			log.info(" Employee Leave Details Request List size :: " + leaveDetailsList.size());
			if (leaveDetailsList == null || leaveDetailsList.isEmpty() || leaveDetailsList.size() == 0) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_DETAILS_LIST_EMPTY.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validateLeaveDetails();
			if (validEntries) {
				employeeMaster.setEmployeeLeaveDeatailsList(leaveDetailsList);
				log.info("Update Leave Called For URL " + url);
				url = EMP_SERVER_URL + "/update";
				BaseDTO baseDTO = httpService.put(url, employeeMaster);
				log.info(" Update Leave Response " + baseDTO);
				if (baseDTO == null) {
					log.error("Base dto returned null values");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (employee != null) {
					employeeMaster = employee;

					employeeLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
					employeeEarnedLeaveDetailsList = new ArrayList<EmployeeLeaveDetails>();
					for (EmployeeLeaveDetails details : employeeMaster.getEmployeeLeaveDeatailsList()) {
						LeaveTypeMaster leaveTypeMasters = details.getLeaveTypeMaster();
						String leaveTypeCode = null;
						if (leaveTypeMasters != null) {
							leaveTypeCode = leaveTypeMasters.getCode();
							if ("EL".equals(leaveTypeCode)) {
								employeeEarnedLeaveDetailsList.add(details);
							} else {
								employeeLeaveDetailsList.add(details);
							}
						} else {
							employeeLeaveDetailsList.add(details);
						}
					}

				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
				log.info("===============LEAVE DETAILS Status Code is========" + baseDTO.getStatusCode());
				if (baseDTO.getStatusCode() == 0) {
					log.info("Employee Update SuccessFully ");
					errorMap.notify(ErrorDescription.EMPLOYEE_UPDATED_SUCCESSFULLY.getErrorCode());
					// employeeLeaveDetail = new EmployeeLeaveDetails();
					// employeeEarnedLeaveDetail = new EmployeeLeaveDetails();

					// loadEmployeeTab();
					log.info("tab------------>" + visibleButton);
					if (visibleButton.equals("first")) {
						return "/pages/personnelHR/createEmployeeEarnedLeave.xhtml?faces-redirect=true";
					} else if (visibleButton.equals("second")) {
						return "/pages/personnelHR/createEmployeeLeaveTravelConcesstionDetails.xhtml?faces-redirect=true";
					}

				} else {
					log.error("Employee Update failed with error code" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("[Exception in Achievements Save Continue :]", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void addLeaveTravel() {
		log.info(" :: Employee Leave Travel Concession :: " + empLeaveTravelConcession);
		try {
			if (empLeaveTravelConcession == null) {
				log.error(" :: Add - Employee Leave Travel Concession Object is null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			Set<String> uniqueRefNo = new HashSet<String>();
			employeeLeaveTravelConcessionList.add(empLeaveTravelConcession);
			for (EmployeeLeaveTravelConcession travelConcession1 : employeeLeaveTravelConcessionList) {
				if (!uniqueRefNo.add(travelConcession1.getRefferenceNumber())) {
					employeeDisciplinaryActionList.remove(travelConcession1);
					errorMap.notify(ErrorDescription.EMP_DISCIPLINARY_REF_EXIST.getErrorCode());
					RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel:leaveTravelTable");
					return;
				}
			}
			empLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			log.info("Employee Leave Travel Concession List while adding :: {} ",
					employeeDisciplinaryActionList.size());
			RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel:leaveTravelTable");
		} catch (Exception e) {
			log.error("Exception While Adding Employee Leave Travel Concession Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void updateLeaveTravel() {
		log.info(" Upade Employee Leave Travel Concession :: " + empLeaveTravelConcession);
		try {
			if (empLeaveTravelConcession == null) {
				log.error(" :: Upade Employee Leave Travel Concession Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (employeeLeaveTravelConcessionList.size() > 0) {
				for (EmployeeLeaveTravelConcession deatails : employeeLeaveTravelConcessionList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(empLeaveTravelConcession.getRowIndex())) {
						log.info(" :: Upadeted - Employee Leave Travel Concession ::");
						employeeLeaveTravelConcessionList.set(deatails.getRowIndex(), empLeaveTravelConcession);
					}
				}
			} else {
				log.info(" :: Upadeted - Employee Leave Travel Concession ::");
				employeeLeaveTravelConcessionList.add(empLeaveTravelConcession);
			}
			empLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			visibleButton = "ADDLeaveTravel";
			RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel");
		} catch (Exception e) {
			log.error("Exception Upade Employee Promotion Deatil", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void editLeaveTravel(EmployeeLeaveTravelConcession editLeave) {
		log.info(" :: Edit Employee Leave Travel Concession ::");
		try {
			if (editLeave == null) {
				log.error(" :: Edit Employee Leave Travel Concession null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			empLeaveTravelConcession = editLeave;
			log.info(" Selected Employee Leave Travel Concession :: " + empLeaveTravelConcession);
			visibleButton = "UPDATELeaveTravel";
			// RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel");
		} catch (Exception e) {
			log.error(" :: Exception Edit Employee Leave Travel Concession :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public boolean validateLeaveTravel() {
		Iterator<EmployeeLeaveTravelConcession> employeeLeaveTravelConcessionItr = employeeLeaveTravelConcessionList
				.iterator();
		while (employeeLeaveTravelConcessionItr.hasNext()) {
			EmployeeLeaveTravelConcession employeeLeaveTravelConcession = employeeLeaveTravelConcessionItr.next();
			if (employeeLeaveTravelConcession.getRefferenceNumber() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_NUMBER_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getRefferenceNumber().length() > 75) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_NUMBER_MAX_LENGTH.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getRefferenceDate() == null) {
				errorMap.notify(ErrorDescription.EMP_LEAVE_REFFERENCE_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getBlockYearFrom() == null) {
				errorMap.notify(ErrorDescription.LTC_BLOCK_YEAR_FROM_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getBlockYearTo() == null) {
				errorMap.notify(ErrorDescription.LTC_BLOCK_YEAR_TO_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getBlockFromDate() == null) {
				errorMap.notify(ErrorDescription.LTC_BLOCK_YEAR_FROM_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getBlockToDate() == null) {
				errorMap.notify(ErrorDescription.LTC_BLOCK_YEAR_TO_DATE_MANDATORY.getErrorCode());
				return false;
			} else if (employeeLeaveTravelConcession.getBlockNumberOfDays() == null) {
				errorMap.notify(ErrorDescription.LTC_BLOCK_NO_OF_DAYS_MANDATORY.getErrorCode());
				return false;
			}
		}
		return true;
	}

	public String leaveTravelSaveContinue() {
		log.info(" :: Employee Leave Travel Concession :: ");
		BaseDTO leaveTravelDTO = new BaseDTO();
		try {
			log.info(" Employee Leave Travel Concession Request :: " + employeeLeaveTravelConcessionList);
			if (employeeLeaveTravelConcessionList == null || employeeLeaveTravelConcessionList.isEmpty()) {
				log.info(
						" Save - Employee Leave Travel Concession Return null :: " + employeeLeaveTravelConcessionList);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validateLeaveTravel();
			if (validEntries) {
				employeeMaster.setEmployeeLeaveTravelConcession(employeeLeaveTravelConcessionList);
				String url = EMP_SERVER_URL + "/update";
				log.info(" Save - Employee Leave Travel Concession URL  :: {}", url);
				leaveTravelDTO = httpService.put(url, employeeMaster);
				log.info(" Save - Employee Leave Travel Concession BaseDTO Response  :: " + leaveTravelDTO);
				if (leaveTravelDTO == null) {
					log.info(" Save - Employee Leave Travel Concession BaseDTO null  :: " + leaveTravelDTO);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(leaveTravelDTO.getResponseContent());
				EmployeeMaster leaveTravelEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (leaveTravelEmployee != null) {
					employeeMaster = leaveTravelEmployee;
				} else {
					log.error("Employee Leave Travel Concession Failed to Deserialize ");
					errorMap.notify(leaveTravelDTO.getStatusCode());
					return null;
				}
				if (leaveTravelDTO.getStatusCode() == 0) {
					if (employeeLeaveTravelConcessionList.get(0).getId() == null) {
						log.info(" Save - Employee Leave Travel Concession Succuss Response"
								+ leaveTravelDTO.getStatusCode());
						errorMap.notify(ErrorDescription.EMP_LEAVE_TRAVEL_CONCESSION_CREATED.getErrorCode());
					} else {
						log.info(" Update - Employee Leave Travel Concession Succuss Response"
								+ leaveTravelDTO.getStatusCode());
						errorMap.notify(ErrorDescription.EMP_LEAVE_TRAVEL_CONCESSION_UPDATED.getErrorCode());
					}

				} else {
					log.info(" Save - Employee Leave Travel Concession Failure Response"
							+ leaveTravelDTO.getStatusCode());
					errorMap.notify(leaveTravelDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(" :: Exception While Saving Employee Leave Travel Concession :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeScannedDoc.xhtml?faces-redirect=true;";
	}

	public void deleteLeaveTravel(EmployeeLeaveTravelConcession deleteLeaveTravel) {
		log.info(" :: Employee Leave Travel Concession Delete record :: ");
		try {
			if (deleteLeaveTravel == null) {
				log.info(" Employee Leave Travel Concession Delete null ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (deleteLeaveTravel.getId() != null) {
				log.info(" Employee Leave Travel Concession Delete id :: " + deleteLeaveTravel.getId());
				String url = AppUtil.getPortalServerURL() + "/leavetravelconcession/delete/id/"
						+ deleteLeaveTravel.getId();
				log.info(" Delete - Employee Leave Travel Concession URL :: " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info(" Delete - Employee Leave Travel Concession Response :: " + baseDTO);
				if (baseDTO != null) {
					log.info(" Deleted - Employee Leave Travel Concession Id : {} ", deleteLeaveTravel.getId());
					employeeLeaveTravelConcessionList.remove(deleteLeaveTravel);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					empLeaveTravelConcession = new EmployeeLeaveTravelConcession();
				}
			} else {
				log.info(" :: Deleted - Employee Leave Travel Concession :: ");
				employeeLeaveTravelConcessionList.remove(deleteLeaveTravel);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				empLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			}
			RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel:leaveTravelTable");
		} catch (Exception e) {
			log.error(" :: Exception Delete Employee Leave Travel Concession ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void viewLeaveTravel(EmployeeLeaveTravelConcession viewLeaveTravel) {
		log.info(" View Employee Leave Travel Concession :: " + viewLeaveTravel);
		try {
			if (viewLeaveTravel == null) {
				log.error(" :: View Employee Leave Travel Concession null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			empLeaveTravelConcession = viewLeaveTravel;
			visibleButton = "VIEWLeaveTravel";
			RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel");
		} catch (Exception e) {
			log.error("Exception View Employee Leave Travel Concession", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadYears() {
		try {
			int currentYear = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, 3);
			int futureYear = cal.get(Calendar.YEAR);
			List<String> years = new ArrayList<String>();
			for (int i = -2; i < 1; i++) {
				if (i != 0) {
					cal = Calendar.getInstance();
					cal.add(Calendar.YEAR, i);
					years.add(Integer.toString(cal.get(Calendar.YEAR)));
				} else if (currentYear != futureYear) {
					years.add(Integer.toString(currentYear));
					years.add(Integer.toString(futureYear));
				} else if (currentYear == futureYear) {
					years.add(Integer.toString(currentYear));
				}
			}
			listYear = years;
			log.info(":: Load Years List :: " + listYear);
		} catch (Exception e) {
			log.info("Exception in load years", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * 
	 */
	public void loadBlockYearList() {
		try {
			String blockYearFromStr = commonDataService.getAppKeyValue("BLOCK_YEAR_FROM");
			log.info("blockYearFromStr 1 :: " + blockYearFromStr);
			Integer blockYearFrom = AppUtil.stringToInteger(blockYearFromStr, 2000);
			int blockYearFromInt = blockYearFrom == null ? new Integer(2000).intValue() : blockYearFrom.intValue();

			log.info("blockYearFromInt 2 :: " + blockYearFromInt);

			int currentYear = Calendar.getInstance().get(Calendar.YEAR);

			log.info("currentYear :: " + currentYear);

			// blockYearList
			for (int year = blockYearFromInt; year <= currentYear; year++) {
				blockYearList.add(String.valueOf(year));
			}
		} catch (Exception ex) {
			log.info("Exception in loadBlockYearList", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onYearChange() {
		List<String> yearsInString = new ArrayList<String>();
		try {
			selectedYearList = new ArrayList<>();
			Integer toYear = empLeaveTravelConcession.getBlockYearFrom();
			if (empLeaveTravelConcession.getBlockYearFrom() != null) {
				for (int i = 1; i < 4; i++) {
					toYear = toYear + 1;
					selectedYearList.add(toYear);
				}
			} else {
				log.info(" :: Years List String Not FOund :: ");
				selectedYearList = new ArrayList<>();
			}

			log.info("Ten Years List String -----;;;;;" + yearsInString);
		} catch (Exception e) {
			log.info("Exception in load years", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearLeaveTravel() {
		empLeaveTravelConcession = new EmployeeLeaveTravelConcession();
		visibleButton = "ADDLeaveTravel";
		RequestContext.getCurrentInstance().update("empLeaveTravelForm:leaveTravelPanel");
	}

	/**
	 * @author Manjunath G
	 */
	public void loadEmployeeIncrementDetailsPanel() {
		RequestContext.getCurrentInstance().update("employeeIncrementDetailsForm:employeeIncrementDetailsPanel ");
	}

	/**
	 * @author Manjunath G This method for calculate the revisedbasic pay of
	 *         emplyeeincrement
	 * @return
	 */
	public Double updateEmployeeRevisedBasicPay() {
		Double revisedBasicPay = 0.00;
		log.info("==>>EmployeeMasterBean inside updateEmployeeRevisedBasicPay <<==");
		try {
			if (employeeIncrementDetails.getBasicPay() != null
					&& employeeIncrementDetails.getIncrementAmount() != null) {
				revisedBasicPay = employeeIncrementDetails.getBasicPay()
						+ employeeIncrementDetails.getIncrementAmount();
				employeeIncrementDetails.setRevisedBasicPay(revisedBasicPay);
			} else {
				employeeIncrementDetails.setRevisedBasicPay(revisedBasicPay);
			}
		} catch (Exception e) {
			log.error("===>> EmployeeMasterBean exception occured inside updateEmployeeRevisedBasicPay ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("==>> EmployeeMasterBean inside updateEmployeeRevisedBasicPay <<===END" + revisedBasicPay);
		return revisedBasicPay;
	}

	/**
	 * @author Manjunath G This method for delete the employeeincremental details
	 *         from database.
	 * @param idx
	 * @param selectedEmployeeIncrementDetail
	 * @return
	 */

	public String deleteEmployeeIncrementDetail(EmployeeIncrementDetails selectedEmployeeIncrementDetail) {
		try {
			if (selectedEmployeeIncrementDetail.getId() == null) {
				employeeIncrementDetailsList.remove(selectedEmployeeIncrementDetail);
				loadEmployeeIncrementDetailsPanel();
			} else {
				String url = SERVER_URL + "/employeeIncrementalDetils/deleteEmployeeIncremental/id/"
						+ selectedEmployeeIncrementDetail.getId();
				BaseDTO baseDTO = httpService.delete(url);
				if (baseDTO == null) {
					log.error("==>> Base DTO Returned Null Value <<==");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				if (baseDTO.getStatusCode() == 0) {
					employeeIncrementDetailsList.remove(selectedEmployeeIncrementDetail);
					loadEmployeeIncrementDetailsPanel();
					log.info("==>> EmployeeIncrementDetails deleted SuccessFully <<==");
					errorMap.notify(baseDTO.getStatusCode());
					return "/pages/personnelHR/createEmployeeIncrementDetails.xhtml?faces-redirect=true";
				} else {
					log.error("EmployeeIncremental detetion is failed with status code :" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.info(" ===>> Exception in incrementdetails  while deleting <<===", exp);
		}
		loadEmployeeIncrementDetailsPanel();
		log.info("===>>>EmployeeMasterBean inside deleteEmployeeIncrementDetail <<<===END");
		return "/pages/personnelHR/createEmployeeIncrementDetails.xhtml?faces-redirect=true";
	}

	public void clearEmployeeIncrementDetails() {

		employeeIncrementDetails = new EmployeeIncrementDetails();
		incrementFlag = "ADDINC";
		RequestContext.getCurrentInstance().update("employeeIncrementDetailsForm:incrementPanel");

	}

	/**
	 * @author Manjunath G This method for add employeeAdditionalChareDetails into
	 *         the list.
	 * 
	 */
	public String addEmployeeAdditionalChargeDetails() {
		log.info("===>>EmployeeMasterBean inside addEmployee AdditionalCharges.. <<==== START "
				+ employeeAdditionalCharge);
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (employeeAdditionalCharge == null) {
				log.error(" ===>> employee Additional Charges detaails null values <<===");
				errorMap.notify(ErrorDescription.EMP_ADDDITIONAL_CHARGE_NULL.getErrorCode());
				return null;
			}

			if (employeeAdditionalCharge.getReferenceNumber() != null) {
				String url = AppUtil.getPortalServerURL() + "/employeeAdditionalCharge/exist";
				baseDTO = recordExist(url, employeeAdditionalCharge);
				if (baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}

			employeeAdditionalChargeList.add(employeeAdditionalCharge);
			Set<String> uniqueNo = new HashSet<String>();
			for (EmployeeAdditionalCharge uniqueIncrement : employeeAdditionalChargeList) {
				if (!uniqueNo.add(uniqueIncrement.getReferenceNumber())) {
					employeeAdditionalChargeList.remove(uniqueIncrement);
					errorMap.notify(ErrorDescription.EMP_ADDITIONAL_REF_NO_EXIST.getErrorCode());
					loadEmployeeAdditionalChargeDetailsPanel();
					return null;
				}
			}
			employeeAdditionalCharge = new EmployeeAdditionalCharge();
			loadEmployeeAdditionalChargeDetailsPanel();
			log.info("===>>EmployeeMasterBean inside addEmployeeIncrementDetails.. <<=== END ");
		} catch (Exception exp) {
			log.error("====>>Exception occured in EmployeeMasterBean inside addEmployeeIncrementDetails.. <<====", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunath G
	 */
	public void loadEmployeeAdditionalChargeDetailsPanel() {
		RequestContext.getCurrentInstance().update("employeeIncrementDetailsForm:employeeIncrementDetailsPanel ");
	}

	/**
	 * @author Manjunath G This method for edit the selected
	 *         employeeincrementdetails.
	 * @param idx
	 * @param employeeIncrementDetail
	 */
	public String editEmployeeAdditionalChargeDetails(int idx, EmployeeAdditionalCharge employeeAdditionalCharges) {
		log.info("===>>EmployeeMasterBean inside editEmployeeincrementDetails.. <<===START" + employeeAdditionalCharge);
		rowIndex = idx;
		try {
			if (rowIndex == null || employeeAdditionalCharge == null) {
				log.error(" Employee Increment List is Empty ");
				errorMap.notify(ErrorDescription.EMP_INCREMENRT_NULL.getErrorCode());
				return null;
			} else {
				employeeAdditionalCharge = employeeAdditionalCharges;
			}
			loadEmployeeAdditionalChargeDetailsPanel();
		} catch (Exception exp) {
			log.error("===>>Exception occured in EmployeeMasterBean inside editEmployeeincrementDetails.. <<===END");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunath G This method for update the selected
	 *         employeeincrementtaldetails.
	 */
	public String updateEmployeeAdditionalChargeDetails() {
		try {
			log.info("===>>EmployeeMasterBean inside updateEmployeeIncrementDetails.. <<===START");
			if (rowIndex == null || employeeAdditionalCharge == null) {
				log.error(" Employee Increment List is Empty ");
				errorMap.notify(ErrorDescription.EMP_INCREMENT_LIST_EMPTY.getErrorCode());
				return null;
			} else {
				employeeAdditionalChargeList.set(rowIndex, employeeAdditionalCharge);
			}
			employeeAdditionalCharge = new EmployeeAdditionalCharge();
			loadEmployeeAdditionalChargeDetailsPanel();
		} catch (Exception exp) {
			log.error("===>>Exception occured in EmployeeMasterBean inside updateEmployeeIncrementDetails.. <<===END");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunath G This method for display the selected
	 *         employeeincrementaldetails
	 * @param selectedEmployeeIncrementDetails
	 */
	public void viewEmployeeAdditionalChargeDetails(EmployeeAdditionalCharge selectedEmployeeAdditionalCharge) {
		employeeAdditionalCharge = selectedEmployeeAdditionalCharge;
		loadEmployeeAdditionalChargeDetailsPanel();
	}

	/**
	 * @author Manjunath G This method for save and uppdate the employeeincremet
	 *         details in DataBase.
	 * @return baseDTO
	 */
	public String saveAndUpdateEmployeeAdditionalChargeDetails() {
		try {
			log.info("===>> EmployeeMasterBean inside saveAndUpdate EmployeeAdditionalCharge.. <<==="
					+ employeeAdditionalCharge);
			if (employeeAdditionalCharge == null || employeeAdditionalCharge == null
					|| employeeAdditionalChargeList.size() == 0) {
				log.error(" Employee AdditionalCharge List is Empty or null");
				errorMap.notify(ErrorDescription.EMP_INCREMENT_LIST_EMPTY.getErrorCode());
				return null;
			}
			employeeMaster.setAdditionalChargeList(employeeAdditionalChargeList);
			url = EMP_SERVER_URL + "/update";
			log.info(" Employee Service called Url :: " + url);
			BaseDTO baseDto = httpService.put(url, employeeMaster);
			log.info(" ==>> Employee AdditionalCharge List response <<==" + baseDto);
			if (baseDto == null) {
				log.info(" ===>> response baseDTO is null <<==");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
			EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error(" ==>> employee object failed to desearlize <<==");
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}
			if (baseDto.getStatusCode() == 0) {
				errorMap.notify(baseDto.getStatusCode());
				employeeAdditionalChargeList = employeeMaster.getAdditionalChargeList();
				log.info("==>> Employee additionalChargesa Saved Succuessfully <<==");
			} else {
				log.error(" ==>> Error while saving AdditionalCharge Details with error code :: "
						+ baseDto.getStatusCode());
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" ==>> found Exception in additionalcharge details <<==", exp);
		}
		return "/pages/personnelHR/createEmployeeDisciplinaryActnDetails.xhtml?faces-redirect=true;";
	}

	/**
	 * @author Manjunath G This method for delete the additional Charges details
	 *         from database.
	 * @param idx
	 * @param selectedEmployeeAdditionalCharges
	 * @return
	 */
	public String deleteEmployeeAdditionalChargeDetails(EmployeeAdditionalCharge employeeAdditionalCharge) {
		try {
			if (employeeAdditionalCharge.getId() == null) {
				employeeAdditionalChargeList.remove(employeeAdditionalCharge);
				loadEmployeeAdditionalChargeDetailsPanel();
			} else {
				String url = SERVER_URL + "/employeeAdditionalCharge/deleteAdditionalCharge/id/"
						+ employeeAdditionalCharge.getId();
				BaseDTO baseDTO = httpService.delete(url);
				if (baseDTO == null) {
					log.error("==>> Base DTO Returned Null Value <<==");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				if (baseDTO.getStatusCode() == 0) {
					employeeAdditionalChargeList.remove(employeeAdditionalCharge);
					loadEmployeeIncrementDetailsPanel();
					log.info("==>> Employee Additional charge deleted SuccessFully <<==");
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Employee AdditionalCharge detetion is failed with status code :"
							+ baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.info(" ===>> Exception in AdditionalCharge  while deleting <<===", exp);
		}
		loadEmployeeIncrementDetailsPanel();
		log.info("===>>>EmployeeMasterBean inside deleteEmployee additional charge Detail <<<===END");
		return "/pages/personnelHR/createEmployeeAdditionalCharge.xhtml?faces-redirect=true";
	}

	public String clearEmployeeAdditionalCharges() {
		log.info("===>>>EmployeeMasterBean inside clearEmployeeAdditiionalCharges <<<");
		employeeAdditionalCharge = new EmployeeAdditionalCharge();
		return "/pages/personnelHR/createEmployeeAdditionalCharge.xhtml?faces-redirect=true";
	}

	public void onAdditionalEntityTypeChange() {
		try {

			Long entityTypeId = null;

			if (employeeAdditionalCharge.getEntityTypeMaster().getId() != null) {
				entityTypeId = employeeAdditionalCharge.getEntityTypeMaster().getId();
			}

			String url = AppUtil.getPortalServerURL() + "/entitymaster/getbyid/" + entityTypeId;
			log.info("Get Entity Masters on Entity Type Change URL :: " + url);
			BaseDTO entityTypeDTO = httpService.get(url);
			log.info("Get Entity Masters on Entity Type Change Response Value :: " + entityTypeDTO);
			if (entityTypeDTO == null) {
				log.error("Get Entity Masters on Entity Type Change Response null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			} else {
				jsonResponse = mapper.writeValueAsString(entityTypeDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				if (entityMasterList == null) {
					log.info("Get Entity Masters on Entity Type Change List null :: ");
				} else {
					log.info("Get Entity Masters on Entity Type Change List Size :: {} ", entityMasterList.size());
				}
				log.info("Get Entity Masters on Entity Type Change Response Value :: " + entityTypeDTO);
			}
		} catch (Exception e) {
			log.error("Exception While Load Entity Master on Entity Type Change ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void disableButton() {
		log.info("<=cal function=>");
	}

	public void clearFamilyDetails() {
		log.info(":: Clear - Family Details ::");
		employeeFamilyDetail = new EmployeeFamilyDetails();
		RequestContext.getCurrentInstance().update("empFamilyForm:editPanel");
	}

	public void clearInterchangeDetails() {
		log.info(":: Clear - Interchange Details ::");
		employeeInterchangeDetails = new EmployeeInterchangeDetails();

	}

	public void clearTransferDetails() {
		log.info(":: Clear - Transfer Details ::");
		employeeTransferDetails = new EmployeeTransferDetails();
	}

	public void onContactLoad() {
		log.info(" :: Address Contact Page Load ::");
		stateMasterList = commonDataService.getStateList();
	}

	public void onRowSelect(SelectEvent event) {
		selectedEmployee = ((EmployeeSearchResponse) event.getObject());
		addButtonFlag = true;
	}

	/**
	 * @author Manjunath G new for adding incrementDetails into datatble
	 * @return
	 */
	public String loadIncrementDetails() {
		log.info("loadIncrementDetails calling");
		// employeeIncrementDetails=new EmployeeIncrementDetails();
		log.info("loadIncrementDetails() : employeeIncrementDetails - " + employeeIncrementDetails);
		return "/pages/personnelHR/createEmployeeIncrementDetails.xhtml";
	}

	public String addIncrementalDetails() {
		log.info(" :: ADD - Employee incremental details Action :: " + employeeIncrementDetails);
		try {
			if (employeeIncrementDetails == null) {
				log.error(" :: Add - Employee incremental details Action Object is null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info("incrementReferenceNumber" + employeeIncrementDetails.getIncrementReferenceNumber());
			// Set<String> uniqueNo = new HashSet<String>();
			/*
			 * EmployeeIncrementDetailsDto employeeIncrementDetailsDto = new
			 * EmployeeIncrementDetailsDto();
			 * employeeIncrementDetailsDto.setIncrementCycle(incrementCycle);
			 * employeeIncrementDetails.setEmployeeIncrementDetailsDto(
			 * employeeIncrementDetailsDto);
			 */
			employeeIncrementDetailsList.add(employeeIncrementDetails);
			log.info("employeeIncrementDetailsList" + employeeIncrementDetailsList);
			/*
			 * for (EmployeeIncrementDetails uniqueIncrementalDetails :
			 * employeeIncrementDetailsList) { log.info("addIncrementalDetails uniqueNo - "
			 * + uniqueNo);
			 * log.info("uniqueIncrementalDetails.getIncrementReferenceNumber() - " +
			 * uniqueIncrementalDetails.getIncrementReferenceNumber()); if
			 * (uniqueIncrementalDetails.getIncrementReferenceNumber() != null &&
			 * !uniqueNo.add(uniqueIncrementalDetails.getIncrementReferenceNumber())) {
			 * log.info("employeeIncrementDetailsList" + uniqueNo);
			 * employeeIncrementDetailsList.remove(uniqueIncrementalDetails);
			 * errorMap.notify(ErrorDescription.EMP_INCREMENT_REFERENCE_NUM_EXIST.
			 * getErrorCode()); RequestContext.getCurrentInstance() .update(
			 * "empIncrementalDetailsForm:IncremenrtalDetailsPanel:IncremenrtalDetailsTable"
			 * ); return null; } }
			 */
			employeeIncrementDetails = new EmployeeIncrementDetails();
			incrementCycle = new IncrementCycle();
			log.info("Employee incremental details Action List while adding :: {} ",
					employeeIncrementDetailsList.size());
			RequestContext.getCurrentInstance()
					.update("empIncrementalDetailsForm:IncremenrtalDetailsPanel:IncremenrtalDetailsTable");
		} catch (Exception e) {
			log.error("Exception While Adding incremental details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunath G new Clear the incremental details fields
	 */
	public void clearIncrementalDetails() {
		employeeIncrementDetails = new EmployeeIncrementDetails();
		visibleButton = "ADDDISCIPLINARY";
		RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
	}

	/**
	 * @author Manjunath G new update the incremental details into the datatable
	 */
	public void updateIncrementalDetails() {
		log.info(" :: Upadate - Employee IncremenrtalDetails Action :: ");
		try {
			if (employeeIncrementDetails == null) {
				log.error(" :: Update - Employee IncremenrtalDetails Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (employeeIncrementDetailsList.size() > 0) {
				for (EmployeeIncrementDetails deatails : employeeIncrementDetailsList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex() == (employeeIncrementDetails.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						/*
						 * EmployeeIncrementDetailsDto employeeIncrementDetailsDto = new
						 * EmployeeIncrementDetailsDto();
						 * employeeIncrementDetailsDto.setIncrementCycle(incrementCycle);
						 * employeeIncrementDetails.setEmployeeIncrementDetailsDto(
						 * employeeIncrementDetailsDto);
						 */
						employeeIncrementDetailsList.set(deatails.getRowIndex(), employeeIncrementDetails);
					}
				}
				incrementCycle = new IncrementCycle();
			} else {
				log.info(" :: Updated - Employee Disciplinary Action :: ");
				employeeIncrementDetailsList.add(employeeIncrementDetails);
			}
			employeeIncrementDetails = new EmployeeIncrementDetails();
			visibleButton = "ADDDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
		} catch (Exception e) {
			log.error("Exception occured while Update Employee incremental details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new
	 * @param editIncremenrtalDetailsAction
	 */
	public void editIncrementDetails(EmployeeIncrementDetails editIncremenrtalDetailsAction) {
		log.info(":: Edit - Employee IncremenrtalDetails Action :: " + editIncremenrtalDetailsAction);
		try {
			if (editIncremenrtalDetailsAction == null) {
				log.error(":: Edit - Employee incremental details Action Object null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeIncrementDetails = editIncremenrtalDetailsAction;
			visibleButton = "UPDATEDISCIPLINARY";
			log.info(" Selected Employee incremental details Action :: " + employeeIncrementDetails);
			RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
		} catch (Exception e) {
			log.error("Exception While Editing incremental details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new for view incremental details
	 * @param viewIncremenrtalDetailsAction
	 */
	public void viewIncrementalDetails(EmployeeIncrementDetails viewIncremenrtalDetailsAction) {
		log.info(" :: View - Employee incremental details Action :: ");
		try {
			log.info(" View - Employee incremental details Action :: " + viewIncremenrtalDetailsAction);
			if (viewIncremenrtalDetailsAction == null) {
				log.error(" :: View - Employee incremental details Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeIncrementDetails = viewIncremenrtalDetailsAction;
			visibleButton = "VIEWDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
		} catch (Exception e) {
			log.error("Exception occured while view Employee incremental details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new
	 * 
	 *         for delete incremental details
	 * 
	 * @param incremenrtalDetailsAction
	 */
	public void deleteIncrementalDetails(EmployeeIncrementDetails incremenrtalDetailsAction) {
		try {
			log.info(" Employee incremental details Action Delete record :: " + incremenrtalDetailsAction);
			if (incremenrtalDetailsAction == null) {
				log.error(" :: Employee incremental details Action Delete Object null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (incremenrtalDetailsAction.getId() != null) {
				String url = SERVER_URL + "/employeeIncrementalDetils/deleteEmployeeIncremental/id/"
						+ incremenrtalDetailsAction.getId();
				log.info(" Delete - Employee IncremenrtalDetails Action URL :: " + url);
				BaseDTO incrementDTO = httpService.delete(url);
				log.info(" Delete - Employee IncremenrtalDetails Action Response :: " + incrementDTO);

				if (incrementDTO == null) {
					log.error(" :: Employee incremental details Action Delete Object null :: ");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}

				if (incrementDTO.getStatusCode() == 0) {
					log.info(" Deleted - Employee incremental details Action Record Id : {} ",
							incremenrtalDetailsAction.getId());
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeeIncrementDetailsList.remove(incremenrtalDetailsAction);
					incremenrtalDetailsAction = new EmployeeIncrementDetails();
				} else {
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				}
			} else {
				log.info(" :: Deleted - Employee incremental details Action :: ");
				employeeIncrementDetailsList.remove(incremenrtalDetailsAction);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}
			RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
		} catch (Exception e) {
			log.error("Exception occured while Delete Employee incrementalDetails Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public boolean validateIncrementalDetails() {
		Iterator<EmployeeIncrementDetails> incrementDetailsItr = employeeIncrementDetailsList.iterator();
		while (incrementDetailsItr.hasNext()) {
			EmployeeIncrementDetails incrementDetails = incrementDetailsItr.next();
			if (incrementDetails.getPayscaleId() == null || StringUtils.isEmpty(incrementDetails.getPayscaleId())) {
				errorMap.notify(ErrorDescription.EMP_PAY_SCALE_EMPTY.getErrorCode());
				return false;
			}
			/*
			 * if (incrementDetails.getIncrementCycle() == null ||
			 * StringUtils.isEmpty(incrementDetails.getIncrementCycle())) {
			 * errorMap.notify(ErrorDescription.EMP_INCREMENT_CYCLE_NOT_NULL.getErrorCode())
			 * ; return false; }
			 */
			if (incrementDetails.getBasicPay() == null || StringUtils.isEmpty(incrementDetails.getBasicPay())) {
				errorMap.notify(ErrorDescription.EMP_BASIC_PAY_NOT_NULL.getErrorCode());
				return false;
			}
			if (incrementDetails.getIncrementReferenceNumber() == null
					|| StringUtils.isEmpty(incrementDetails.getIncrementReferenceNumber())) {
				errorMap.notify(ErrorDescription.EMP_INCREMENT_REFERENCE_NUMBER_NOT_NULL.getErrorCode());
				return false;
			}
			if (incrementDetails.getIncrementReferenceDate() == null
					|| StringUtils.isEmpty(incrementDetails.getIncrementReferenceDate())) {
				errorMap.notify(ErrorDescription.EMP_INCREMENT_REFERENCE_DATE_NOT_NULL.getErrorCode());
				return false;
			}

			if (incrementDetails.getIncrementEffectiveDate() == null
					|| StringUtils.isEmpty(incrementDetails.getIncrementEffectiveDate())) {
				errorMap.notify(ErrorDescription.EMP_INCREMENT_EFFECTIVE_DATE_NOT_NULL.getErrorCode());
				return false;
			}

		}

		return true;
	}

	/**
	 * @author Manjunath G new for save and update the incremental details into the
	 *         DB
	 * 
	 * @return
	 */
	public String saveIncrementalDetails() {
		log.info(" :: Save - Employee incremental details Action :: " + employeeIncrementDetailsList);
		try {
			if (employeeIncrementDetailsList == null || employeeIncrementDetailsList.isEmpty()) {
				log.info(" :: Save - Employee IncremenrtalDetails Action Object Null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				Util.addWarn("Please Fill Above Fields");
				return null;
			}
			boolean validEntries = true;

			// validEntries = validateIncrementalDetails();

			if (validEntries) {

				employeeMaster.setIncrementDetailsList(employeeIncrementDetailsList);

				String url = EMP_SERVER_URL + "/update";
				log.info("Save - Employee IncremenrtalDetails Action URL :: " + url);

				BaseDTO incremenrtalDetailsDTO = httpService.put(url, employeeMaster);

				log.info("Save - Employee incremental details Action BaseDTO Response Value :: "
						+ incremenrtalDetailsDTO);

				if (incremenrtalDetailsDTO == null) {
					log.error("Save - Employee incremental details Action BaseDTO Response null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(incremenrtalDetailsDTO.getResponseContent());
				EmployeeMaster incremenrtalDetailsEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);

				if (incremenrtalDetailsEmployee != null) {
					employeeMaster = incremenrtalDetailsEmployee;
					employeeIncrementDetailsList = employeeMaster.getIncrementDetailsList();
				} else {
					log.error("Employee incremental details Action Failed to Deserialize ");
					errorMap.notify(incremenrtalDetailsDTO.getStatusCode());
					return null;
				}
				if (incremenrtalDetailsDTO.getStatusCode() == 0) {

					if (employeeIncrementDetailsList.size() > 0) {
						if (employeeIncrementDetailsList.get(0).getId() == null) {
							log.info("Employee Incremental Details Saved Succuessfully ");
							errorMap.notify(ErrorDescription.INCREMENRTAL_SAVED.getErrorCode());
						}
					} else {
						log.info("Employee Incremental Details Updated Succuessfully ");
						errorMap.notify(ErrorDescription.INCREMENRTAL_UPDATED.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeePromotionDetails.xhtml?faces-redirect=true";
				} else {
					log.info("Save - Employee incremental details Action error "
							+ incremenrtalDetailsDTO.getStatusCode());
					errorMap.notify(incremenrtalDetailsDTO.getStatusCode());
				}
				RequestContext.getCurrentInstance().update("empIncrementalDetailsForm:IncremenrtalDetailsPanel");
				return "/pages/personnelHR/createEmployeePromotionDetails.xhtml?faces-redirect=true;";
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured while saving Employee incremental details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunnath G new
	 * 
	 *         for add the additionnal charge details into the Datatable
	 * 
	 * @return
	 */
	public String addAdditionalChargeDetails() {
		log.info(" :: ADD - Employee additionl charge details Action :: " + employeeAdditionalCharge);
		try {
			if (employeeAdditionalCharge == null) {
				log.error(" :: Add - Employee Employee Additional Charge details Action Object is null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			Set<String> uniqueNo = new HashSet<String>();
			employeeAdditionalChargeList.add(employeeAdditionalCharge);
			for (EmployeeAdditionalCharge uniqueadditionalCharge : employeeAdditionalChargeList) {
				if (!uniqueNo.add(uniqueadditionalCharge.getReferenceNumber())) {
					employeeAdditionalChargeList.remove(uniqueadditionalCharge);
					errorMap.notify(ErrorDescription.EMP_INCREMENT_REFERENCE_NUM_EXIST.getErrorCode());
					RequestContext.getCurrentInstance()
							.update("empAdditionalChargesForm:empAdditionalChargePanel:empAdditionalChargeTable");
					return null;
				}
			}
			employeeAdditionalCharge = new EmployeeAdditionalCharge();
			log.info("Employee additionalCharge details Action List while adding :: {} ",
					employeeAdditionalChargeList.size());
			RequestContext.getCurrentInstance()
					.update("empAdditionalChargesForm:empAdditionalChargePanel:empAdditionalChargeTable");
		} catch (Exception e) {
			log.error("Exception While Adding Employee AdditionalCharge details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author Manjunath G new
	 *
	 *         for clear the additional charges
	 */
	public void clearAdditionalChargeDetails() {
		employeeAdditionalCharge = new EmployeeAdditionalCharge();
		visibleButton = "ADDDISCIPLINARY";
		RequestContext.getCurrentInstance().update("empAdditionalChargesForm:empAdditionalChargePanel");
	}

	/**
	 * @author Manjunath G new
	 *
	 *         for update the additionL charge details into the datatable
	 */
	public void updateAdditionalChargeDetails() {
		log.info(" :: Upadate - Employee AdditionalCharge Action :: ");
		try {
			if (employeeAdditionalCharge == null) {
				log.error(" :: Update - employeeAdditionalCharge Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (employeeAdditionalChargeList.size() > 0) {
				for (EmployeeAdditionalCharge deatails : employeeAdditionalChargeList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(employeeAdditionalCharge.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						employeeAdditionalChargeList.set(deatails.getRowIndex(), employeeAdditionalCharge);
					}
				}
			} else {
				log.info(" :: Updated - Employee AdditionalCharge Action :: ");
				employeeAdditionalChargeList.add(employeeAdditionalCharge);
			}
			employeeAdditionalCharge = new EmployeeAdditionalCharge();
			visibleButton = "ADDDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empAdditionalChargesForm:empAdditionalChargePanel");
		} catch (Exception e) {
			log.error("Exception occured while Update employee AdditionalCharge details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new for edit the additional charges into the datatable
	 * @param editEmployeeAdditionalChargeAction
	 */
	public void editAdditionalChargeDetails(EmployeeAdditionalCharge editEmployeeAdditionalChargeAction) {
		log.info(":: Edit - Employee IncremenrtalDetails Action :: " + editEmployeeAdditionalChargeAction);
		try {
			if (editEmployeeAdditionalChargeAction == null) {
				log.error(":: Edit - Employee AdditionalCharge details Action Object null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeAdditionalCharge = editEmployeeAdditionalChargeAction;
			entityMasterList = commonDataService.getEntityMaster(employeeAdditionalCharge.getEntityTypeMaster());
			visibleButton = "UPDATEDISCIPLINARY";
			log.info(" Selected Employee incremental details Action :: " + employeeAdditionalCharge);
			RequestContext.getCurrentInstance().update("empAdditionalChargesForm:empAdditionalChargePanel");
		} catch (Exception e) {
			log.error("Exception While Editing Employee AdditionalCharge details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new for view the additional charge details
	 * @param viewEmployeeAdditionalChargeAction
	 */
	public void viewAdditionalChargeDetails(EmployeeAdditionalCharge viewEmployeeAdditionalChargeAction) {
		log.info(" :: View - Employee AdditionalCharge details Action :: ");
		try {
			log.info(" View - Employee AdditionalCharge details Action :: " + viewEmployeeAdditionalChargeAction);
			if (viewEmployeeAdditionalChargeAction == null) {
				log.error(" :: View - Employee AdditionalCharge details Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			employeeAdditionalCharge = viewEmployeeAdditionalChargeAction;
			visibleButton = "VIEWDISCIPLINARY";
			RequestContext.getCurrentInstance().update("empAdditionalChargesForm:empAdditionalChargePanel");
		} catch (Exception e) {
			log.error("Exception occured while view Employee AdditionalCharge details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G new
	 *
	 *         for delete the employee additonla charges details
	 *
	 * @param employeeAdditionalChargeAction
	 */
	public void deleteAdditionalChargeDetails(EmployeeAdditionalCharge employeeAdditionalChargeAction) {
		try {
			log.info(" Employee incremental details Action Delete record :: " + employeeAdditionalChargeAction);
			if (employeeAdditionalChargeAction == null) {
				log.error(" :: Employee AdditionalChargedetails Action Delete Object null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (employeeAdditionalChargeAction.getId() != null) {
				String url = SERVER_URL + "/employeeAdditionalCharge/deleteAdditionalCharge/id/"
						+ employeeAdditionalChargeAction.getId();
				log.info(" Delete - Employee AdditionalChargeAction URL :: " + url);
				BaseDTO incrementDTO = httpService.delete(url);
				log.info(" Delete - Employee AdditionalCharge Action Response :: " + incrementDTO);

				if (incrementDTO == null) {
					log.error(" :: Employee AdditionalChargedetails Action Delete Object null :: ");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}

				if (incrementDTO.getStatusCode() == 0) {
					log.info(" Deleted - Employee AdditionalChargedetails Action Record Id : {} ",
							employeeAdditionalChargeAction.getId());
					errorMap.notify(incrementDTO.getStatusCode());
					employeeAdditionalChargeList.remove(employeeAdditionalChargeAction);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					employeeAdditionalChargeAction = new EmployeeAdditionalCharge();
				} else {
					errorMap.notify(incrementDTO.getStatusCode());
				}
			} else {
				log.info(" :: Deleted - Employee employeeAdditionalCharge details Action :: ");
				employeeAdditionalChargeList.remove(employeeAdditionalChargeAction);
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
				if (employeeAdditionalChargeList.size() == 0) {
					employeeAdditionalCharge = new EmployeeAdditionalCharge();
				}
			}
			RequestContext.getCurrentInstance().update("empAdditionalChargesForm:empAdditionalChargePanel");
		} catch (Exception e) {
			log.error("Exception occured while Delete Employee AdditionalChargeAction", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * @author Manjunath G : new
	 *
	 *         for save the additional charge details into the DB
	 *
	 * @return
	 */
	public boolean validateAdditionalChargeDetails() {
		Iterator<EmployeeAdditionalCharge> employeeAdditionalChargeItr = employeeAdditionalChargeList.iterator();
		while (employeeAdditionalChargeItr.hasNext()) {
			EmployeeAdditionalCharge additionalCharge = employeeAdditionalChargeItr.next();
			if (additionalCharge.getDesignation() == null || StringUtils.isEmpty(additionalCharge.getDesignation())) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return false;
			}
			if (additionalCharge.getCurrentDesignation() == null
					|| StringUtils.isEmpty(additionalCharge.getCurrentDesignation())) {
				errorMap.notify(ErrorDescription.EMP_DESIGNATION_EMPTY.getErrorCode());
				return false;
			}
			if (additionalCharge.getEntityMaster() == null || StringUtils.isEmpty(additionalCharge.getEntityMaster())) {
				errorMap.notify(ErrorDescription.EMP_ENTITY_NOT_NULL.getErrorCode());
				return false;
			}
			if (additionalCharge.getEntityTypeMaster() == null
					|| StringUtils.isEmpty(additionalCharge.getEntityTypeMaster())) {
				errorMap.notify(ErrorDescription.EMP_ENTITY_NOT_NULL.getErrorCode());
				return false;
			}
			if (additionalCharge.getReferenceDate() == null
					|| StringUtils.isEmpty(additionalCharge.getReferenceDate())) {
				errorMap.notify(ErrorDescription.EMP_ADDITIONAL_CHARGE_REFERENCE_DATE_NOT_NULL.getErrorCode());
				return false;
			}
			if (additionalCharge.getReferenceNumber() == null
					|| StringUtils.isEmpty(additionalCharge.getReferenceNumber())) {
				errorMap.notify(ErrorDescription.EMP_ADDITIONAL_CHARGE_REFERENCE_NUM_NOT_NULL.getErrorCode());
				return false;
			}
			if (additionalCharge.getDateOfAditionaalCharge() == null
					|| StringUtils.isEmpty(additionalCharge.getDateOfAditionaalCharge())) {
				errorMap.notify(ErrorDescription.EMP_DATE_OF_ADDITIONAL_CAHRGE_NOT_NILL.getErrorCode());
				return false;
			}
		}
		return true;
	}

	public String saveemployeeAdditionalChargeDetails() {
		log.info(" :: Save - Employee AdditionalChargedetails Action :: " + employeeAdditionalChargeList);
		try {
			if (employeeAdditionalChargeList == null || employeeAdditionalChargeList.isEmpty()) {
				log.error(" :: Save - Employee AdditionalChargeAction Object Null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			boolean validEntries = true;
			// validEntries = validateAdditionalChargeDetails();
			if (validEntries) {
				employeeMaster.setAdditionalChargeList(employeeAdditionalChargeList);
				String url = EMP_SERVER_URL + "/update";
				log.info("Save - Employee AdditionalChargeDetails Action URL :: " + url);
				BaseDTO additionalChargeDetailsDTO = httpService.put(url, employeeMaster);
				log.info("Save - Employee AdditionalChargedetails Action BaseDTO Response Value :: "
						+ additionalChargeDetailsDTO);
				if (additionalChargeDetailsDTO == null) {
					log.error("Save - Employee AdditionalChargedetails Action BaseDTO Response null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(additionalChargeDetailsDTO.getResponseContent());
				EmployeeMaster AdditionalChargeEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (AdditionalChargeEmployee != null) {
					employeeMaster = AdditionalChargeEmployee;
					employeeAdditionalChargeList = employeeMaster.getAdditionalChargeList();
				} else {
					log.error("Employee Additional Chargedetails Action Failed to Deserialize ");
					errorMap.notify(additionalChargeDetailsDTO.getStatusCode());
					return null;
				}

				if (additionalChargeDetailsDTO.getStatusCode() == 0) {

					if (employeeAdditionalChargeList.get(0).getId() == null) {
						log.info("Employee Additional Charge Details Saved Succuessfully ");
						errorMap.notify(ErrorDescription.EMP_ADDITIONAL_CHARGE_DETAILS_SAVED.getErrorCode());
					} else {
						log.info("Employee Additional Charge Details Updated Succuessfully ");
						errorMap.notify(ErrorDescription.EMP_ADDITIONAL_CHARGE_DETAILS_UPDATED.getErrorCode());
					}

					return "/pages/personnelHR/createEmployeeDisciplinaryActnDetails.xhtml?faces-redirect=true";
				} else {
					log.info("Save - Employee AdditionalCharge details Action error "
							+ additionalChargeDetailsDTO.getStatusCode());
					errorMap.notify(additionalChargeDetailsDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while saving Employee Additional Charge details Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/createEmployeeDisciplinaryActnDetails.xhtml?faces-redirect=true;";
	}

	/**
	 * Purpose : calculate age passing date
	 * 
	 * @author krishnakumar
	 */
	public void employeeAgeCalculation() {
		try {
			log.info("employeeAgeCalculation Method Called :: " + employeeMaster.getDob());
			Date birthDay = employeeMaster.getDob();
			int age = AppUtil.getAge(birthDay);
			log.error(" Employee Family Age :: " + age);
			employeeMaster.setAge((long) age);
		} catch (Exception e) {
			log.error(" Exception In EmployeeMasterBean employeeAgeCalculation Method ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	public void addInfoWidget(EmployeePersonalInfoGeneral employeePersonalInfoGen) {
		log.info("<=addInfoWidget=>" + employeePersonalInfoGen.getIsExservicemen());
		employeePersonalInfoGeneral.setIsExservicemen(employeePersonalInfoGen.getIsExservicemen());
		employeePersonalInfoGeneral.setIsDifferentlyAbled(employeePersonalInfoGen.getIsDifferentlyAbled());
		employeePersonalInfoGeneral.setIsFreedomFighter(employeePersonalInfoGen.getIsFreedomFighter());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addInfoWidget').show();");
	}

	public List<EmployeeMaster> loadEmployeeList(EntityMaster entityMaster) {
		log.info("<======== EmployeeAdditionalChargeBean.loadEmployeeList() calling ======>");
		try {
			entityMaster = employeePersonalInfo.getWorkLocation();
			if (entityMaster != null && entityMaster.getId() != null) {
				String url = AppUtil.getPortalServerURL() + "/employee/getemployeebyentityandentitytypeid/"
						+ entityMaster.getId();
				log.info("loadEmployeeList url is" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					reportingToEmployeeList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
				}
			}
			log.info("meetingentitytypelist size" + entityTypeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception EmployeeAdditionalChargeBean.loadEmployeeList()==========>", e);
		}
		return reportingToEmployeeList;
	}

	public String printEmployee() {
		log.info("<======== EmployeeAdditionalChargeBean.printEmployee() START ======>");
		try {
			if (selectedEmployee == null) {
				errorMap.notify(ErrorDescription.SELECT_EMPLOYEE_ID.getErrorCode());
				return null;
			}
			InputStream input = null;
			String url = EMP_SERVER_URL + "/printesr/" + selectedEmployee.getId();
			log.info("<======== printesr URL is ======> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				String dirPath = baseDTO.getGeneralContent();

				log.info("dirPath =========== " + dirPath);

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				Map<String, ESRPrintDTO> recordNameDataMap = mapper.readValue(jsonResponse,
						new TypeReference<Map<String, ESRPrintDTO>>() {
						});
				String filePath = "";
				PrintESR printESR = new PrintESR(dirPath);
				if (downloadOption.equals("DOWNLOAD_PDF")) {
					filePath = printESR.printData(recordNameDataMap);
				} else {
					filePath = printESR.printData(recordNameDataMap);
				}
				File files = new File(filePath);
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				esrDownloadFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
				log.error("exception in filedownload method------- " + files.getName());
			}
		} catch (Exception e) {
			log.info("<========Exception occured in EmployeeAdditionalChargeBean.printEmployee() ======>");
			log.info("<========Exception is ======>" + e);
		}
		log.info("<======== EmployeeAdditionalChargeBean.printEmployee() END ======>");
		return action;

	}

	/**
	 * This method used to find the difference days from two dates
	 * 
	 * @throws ParseException
	 */
	public void calculateNoOfDays() throws ParseException {
		if (employeeLeaveDetail.getFromDate() != null && employeeLeaveDetail.getToDate() != null) {

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			String fromDate = dateFormat.format(employeeLeaveDetail.getFromDate());
			String toDate = dateFormat.format(employeeLeaveDetail.getToDate());
			Date firstDate = dateFormat.parse(fromDate);
			Date secondDate = dateFormat.parse(toDate);

			long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
			long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

			log.info("diff------------->" + diff);

			if (secondDate.before(firstDate)) {
				Util.addWarn("To date should not be allowed for before from date");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			if (firstDate.compareTo(secondDate) == 0) {
				log.info("Number of days for leave is : " + 1);
				employeeLeaveDetail.setNoOfDays(1.0);
			} else {
				log.info("Number of days for leave is : " + diff);
				double leaveDays = AppUtil.calculateNoOfDays(firstDate, secondDate);
				employeeLeaveDetail.setNoOfDays(leaveDays);
			}

			/*
			 * long diffInMillies = Math.abs(employeeLeaveDetail.getFromDate().getTime() -
			 * employeeLeaveDetail.getToDate().getTime());
			 * employeeLeaveDetail.setNoOfDays((double) TimeUnit.DAYS.convert(diffInMillies,
			 * TimeUnit.MILLISECONDS));
			 */
		}
	}

	public void runPayroll() {
		log.info("EmployeeMasterBean. runPayroll() Calling");
		try {
			String url = AppUtil.getSchedulerServerURL() + "/executescheduler/runpayroll";
			log.info("URL is" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getErrorCode())) {
					log.info("Payroll Successfully executed for All Employees");
					errorMap.notify(
							ErrorDescription.getError(PersonnalErrorCode.PAYROLL_GENERATE_SUCCESS).getErrorCode());
				} else {
					log.info("Exception at EmployeeMasterBean. runPayroll()");
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception at EmployeeMasterBean. runPayroll()", e);
		}

	}

	// Load ReportingTo Employee Auto Complete List
	public List<String> reportinToEmployeeAutoComplete(String input) {
		log.info("<<========= EmployeeMasterBean. reportinToEmployeeAutoComplete() Starts ===========>> ");
		List<String> employeeList = new ArrayList<>();
		try {
			reportingToEmployeeNameList = commonDataService.getActiveEmployeeNameAndPfNumaberOrderList();
			if (reportingToEmployeeNameList != null && !reportingToEmployeeNameList.isEmpty()
					&& reportingToEmployeeNameList.size() > 0) {
				log.info("Employee Pf Number Or Name" + input);
				for (EmployeeMaster employeeMaster : reportingToEmployeeNameList) {
					if (employeeMaster.getPersonalInfoEmployment().getPfNumber().contains(input)
							|| employeeMaster.getFirstName().toUpperCase().contains(input.toUpperCase())) {
						employeeList.add(new StringBuilder()
								.append(employeeMaster.getPersonalInfoEmployment().getPfNumber()).append(" / ")
								.append(employeeMaster.getFirstName() + " " + employeeMaster.getLastName())
								.append(" / ")
								.append(employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getName())
								.toString());
					}
				}
			} else {
				log.info("Employee List Size is Empty");
			}
		} catch (Exception e) {
			log.error("Exception at EmployeeMasterBean. reportinToEmployeeAutoComplete()", e);
		}
		log.info("<<========= EmployeeMasterBean. reportinToEmployeeAutoComplete() Ends ===========>> ");
		return employeeList;
	}

}
