package in.gov.cooptex.admin.training.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.training.model.StudentTrainingRequest;
import in.gov.cooptex.admin.training.model.StudentTrainingRequestLog;
import in.gov.cooptex.admin.training.model.StudentTrainingRequestNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.IncomingTapalStatus;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.StudentTraingDTO;
import in.gov.cooptex.core.dto.StudentTrainingRequestDTO;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.InstitutionType;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("studentTrainingRequestBean")
@Scope("session")
public class StudentTrainingRequestBean extends CommonBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = -3692509443246631999L;

	private final String STUDENT_TRAINING_LIST_PAGE = "/pages/admin/training/listStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAINING_ADD_PAGE = "/pages/admin/training/createStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAINING_VIEW_PAGE = "/pages/admin/training/viewStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAINING_APPROVE_PAGE = "/pages/admin/training/approvalStudentTraining.xhtml?faces-redirect=true;";
	public static final String STUDENT_TRAINING_URL = AppUtil.getPortalServerURL() + "/student/training/request";
	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Setter
	@Getter
	String requestLeterFileName;

	@Getter
	@Setter
	Boolean previousApproval = false;

	@Getter
	@Setter
	StudentTrainingRequest selectedStudentTrainingRequest = new StudentTrainingRequest();
	
	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	String remarkscomment;
	
	@Setter
	@Getter
	private StreamedContent trainingFile;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	StudentTrainingRequestLog studentTrainingRequestLog;

	@Getter
	@Setter
	StudentTrainingRequest studentTraining = new StudentTrainingRequest();

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;
	
	@Getter
	@Setter
	private String downloadFilePath = null;

	@Getter
	@Setter
	LazyDataModel<StudentTrainingRequest> studentTraininglazyLoadList;

	List<StudentTrainingRequest> studentTrainingRequestList = null;
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Setter
	@Getter
	List<InstitutionType> institutionTypeList;

	@Setter
	@Getter
	AppConfig appconfig;

	@Setter
	@Getter
	InstitutionType institutionType;

	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Getter
	@Setter
	private UploadedFile studenttrainingFile;

	@Autowired
	LoginBean loginBean;

	long employeePhotoSize;

	@Getter
	@Setter
	long noOfDays;

	@Getter
	@Setter
	private UploadedFile file = null;

	@Getter
	@Setter
	private UploadedFile relievingLetterFile;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String originalFileName;

	@Getter
	@Setter
	private Date startTime, endTime;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	private StudentTrainingRequestNote studentTrainingRequestNote;

	@Getter
	@Setter
	private StudentTrainingRequestDTO studentTrainingRequestDTO;
	
	@Getter
	@Setter
	private List<StudentTrainingRequestDTO> viewNoteEmployeeDetails = new ArrayList<>();
	
	@Getter
	@Setter
	Boolean approvalFlag = false;
	
	@Getter
	@Setter
	Boolean buttonFlag = false;
	
	@Getter
	@Setter
	Date startDate = null;
	
	@Getter
	@Setter
	Date endDate = null;
	
	@Getter
	@Setter
	String approveRemarks,rejectRemarks;

	@PostConstruct
	public void init() {
		appconfig = new AppConfig();
		employeephotosize();
		showListStudentTrainingPage();
	}

	public void employeephotosize() {
		log.info("<<<< ----------Start employeephotosize ------- >>>>");
		appconfig = new AppConfig();
		try {
			BaseDTO baseDTO = new BaseDTO();

			String url = STUDENT_TRAINING_URL + "/getphotosize";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				appconfig = mapper.readValue(jsonResponse, new TypeReference<AppConfig>() {
				});
				// employeePhotoSize=(Long)appconfig.getAppValue();
				employeePhotoSize = Long.parseLong(appconfig.getAppValue(), 10);

			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

	}

	/**
	 * @return
	 */

	public String showListStudentTrainingPage() {
		employeephotosize();
		log.info("showListStudentTrainingPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		approveButtonFlag = true;
		selectedStudentTrainingRequest = new StudentTrainingRequest();
		//forwardToUsersList = new ArrayList<>();
		//forwardToUsersList = commonDataService.loadForwardToUsers();
		//forwardToUsersList = commonDataService.loadForwardToUsersByFeature("STUDENT_TRAINING");
		studentTrainingRequestLog = new StudentTrainingRequestLog();
		studentTrainingRequestNote = new StudentTrainingRequestNote();
		approveRemarks = "";
		rejectRemarks = "";
		loadInstitutionTypeList();
		loadStudentTrainingListLazy();
		return STUDENT_TRAINING_LIST_PAGE;
	}

	public void loadStudentTrainingListLazy() {

		log.info("<-----STUDENTTraining list lazy load starts------>");
		studentTraininglazyLoadList = new LazyDataModel<StudentTrainingRequest>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StudentTrainingRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = STUDENT_TRAINING_URL + "/loadlazylist";
					BaseDTO baseDTO = httpService.post(url, paginationRequest);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<StudentTrainingRequest> studentTrainingList = mapper.readValue(jsonResponse,
							new TypeReference<List<StudentTrainingRequest>>() {
							});
					if (studentTrainingList == null) {
						log.info(" STUDENTTrainingList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" STUDENTTraining list search Successfully Completed");
					} else {
						log.error(" STUDENTTraining list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return studentTrainingList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading STUDENTTraining list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(StudentTrainingRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StudentTrainingRequest getRowData(String rowKey) {
				List<StudentTrainingRequest> responseList = (List<StudentTrainingRequest>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StudentTrainingRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedStudentTrainingRequest = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			studenttrainingFile = event.getFile();
			String type = FilenameUtils.getExtension(studenttrainingFile.getFileName());
			log.info(" File Type :: " + type);
			//
			boolean validFileType = AppUtil.isValidFileType(type,
					new String[] { "png", "jpg", "JPG", "doc", "jpeg", "docx", "pdf", "xlsx", "xls" });

			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_UPLOAD_FORMAT).getCode());
				return;
			}
			long size = studenttrainingFile.getSize();
			log.info("uploadSignature size==>" + size);
			/* size = size / 1000; */
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			if (size > 5) {
				// selectedSocietyRegistration.setRequestLeterFilePath(null);
				studentTraining.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_FILE_SIZE).getCode());
				return;
			}
			String docPathName = commonDataService.getAppKeyValue("STUDENT_TRAINING_REQUEST_DOC");

			String filePath = Util.fileUpload(docPathName, studenttrainingFile);
			log.info("uploadDocument filePath===>" + filePath);
			// selectedSocietyRegistration.setRequestLeterFilePath(filePath);
			studentTraining.setDocumentPath(filePath);
			requestLeterFileName = studenttrainingFile.getFileName();
			errorMap.notify(ErrorDescription.STUDENT_TRAINING_DOCUMENT_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String studentTrainingListAction() {
		log.info("<====== StudentTrainingBean.studentTrainingListAction Starts====> action   :::" + action);
		try {
			originalFileName = null;
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== STUDENTTraining add page called.......====>");
				selectedStudentTrainingRequest = new StudentTrainingRequest();
				studentTraining = new StudentTrainingRequest();
				institutionType = new InstitutionType();
				forwardTo = null;
				requestLeterFileName = null;
				startDate = null;
				endDate = null; 
				studentTrainingRequestNote = new StudentTrainingRequestNote();
				employeephotosize();

				if (employeeMaster == null) {
					employeeMaster = new EmployeeMaster();
					employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
				}
				
				return STUDENT_TRAINING_ADD_PAGE;
			}
			if (selectedStudentTrainingRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_STUDENT_TRAINING.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStudenetTrainingDelete').show();");
			} else {
				log.info("<----Loading STUDENTTraining Edit page.....---->" + selectedStudentTrainingRequest.getId());
				studentTraining = new StudentTrainingRequest();
				BaseDTO response = httpService
						.post(STUDENT_TRAINING_URL + "/getById", selectedStudentTrainingRequest);
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					studentTrainingRequestDTO = mapper.readValue(jsonResponse,
							new TypeReference<StudentTrainingRequestDTO>() {
							});
					
					if (response.getTotalListOfData() != null) {
						String jsonResponses = mapper.writeValueAsString(response.getTotalListOfData());
						viewNoteEmployeeDetails = mapper.readValue(jsonResponses,
								new TypeReference<List<StudentTrainingRequestDTO>>() {
								});
					}
					
					setStudentTraining(studentTrainingRequestDTO.getStudentTrainingRequest());
					setStudentTrainingRequestLog(studentTrainingRequestDTO.getStudentTrainingRequestLog());
					StudentTrainingRequestNote studentTrainingRequestNote = studentTrainingRequestDTO.getStudentTrainingRequestNote();
					UserMaster forwardToUser = null;
					if(studentTrainingRequestNote != null) {
						setStudentTrainingRequestNote(studentTrainingRequestNote);
//						setForwardTo(studentTrainingRequestDTO.getStudentTrainingRequestNote().getUserMaster());
						forwardToUser = studentTrainingRequestNote.getUserMaster();
						setPreviousApproval(studentTrainingRequestNote.getFinalApproval());
						setFinalapprovalFlag(studentTrainingRequestNote.getFinalApproval());
					}
					
					startDate = studentTrainingRequestDTO.getStudentTrainingRequest().getScheduledStartTime();
					endDate = studentTrainingRequestDTO.getStudentTrainingRequest().getScheduledEndTime();
					
					log.info("final approval ==> "+studentTrainingRequestDTO.getStudentTrainingRequestNote().getFinalApproval());
					if(studentTrainingRequestDTO.getStudentTrainingRequestLog() !=null) {
						log.info("Student trainging log ==>"+studentTrainingRequestDTO.getStudentTrainingRequestLog());
						//setPreviousApproval(tapalDTO.getForwardFor());
						
						if ("VIEW".equalsIgnoreCase(action)) {
							if (forwardToUser != null) {
								log.info("forwardTo==> "+forwardToUser.getId());
								log.info("loginBean.getUserMaster().getId()==> "+loginBean.getUserMaster().getId());
								if (loginBean.getUserMaster().getId().equals(forwardToUser.getId())) {
									if (IncomingTapalStatus.REJECTED.toString().equals(studentTrainingRequestLog.getStage())) {
										approvalFlag = false;
									} else {
										approvalFlag = true;
									}
								} else {
									approvalFlag = false;
								}
							}
							//setFinalApproveFlag(tapalDTO.getForwardFor());
							if(loginBean.getUserMaster().getId().equals(forwardToUser.getId())) {
								if(studentTrainingRequestLog.getStage().equalsIgnoreCase("FINALAPPROVED")) {
									finalapprovalFlag=false;
									buttonFlag=false;
									approvalFlag=false;
								}else if (finalapprovalFlag.equals(true)) {
									buttonFlag = true;
								}
							}
							//tapalDTO.setRemarks(null);
							selectedStudentTrainingRequest.setNotificationId(null);
							systemNotificationBean.loadTotalMessages();
						}
					}

					downloadFilePath = null;
					if (studentTraining.getDocumentPath() != null) {
						setDownloadFilePath(studentTraining.getDocumentPath());
						String filePathValue[] = studentTraining.getDocumentPath().split("uploaded/");
						log.info("0==>" + filePathValue[0]);
						// log.info("1==>" + filePathValue[1]);
						originalFileName = filePathValue[0];
						studentTraining.setDocumentPath(filePathValue[0]);
					} else {
						log.info("1==>" + originalFileName);
					}
					if (action.equalsIgnoreCase("EDIT")) {
						if (employeeMaster == null) {
							employeeMaster = new EmployeeMaster();
							employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
						}
						return STUDENT_TRAINING_ADD_PAGE;
					}
					if (action.equalsIgnoreCase("VIEW")) {
						return STUDENT_TRAINING_VIEW_PAGE;
					}
					if (action.equalsIgnoreCase("APPROVE")) {
						return STUDENT_TRAINING_APPROVE_PAGE;
					}

				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== StudentTrainingBean.studentTrainingListAction Ends====>");
					return null;
				}
			}

			systemNotificationBean.loadTotalMessages();
		} catch (Exception e) {
			log.error("Exception Occured in StudentTrainingBean.studentTrainingListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== studentTrainingBean.studentTrainingListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts StudentTrainingBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(STUDENT_TRAINING_URL + "/delete/" + selectedStudentTrainingRequest.getId());
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_DELETE_SUCCESS.getErrorCode());
			}else {
				errorMap.notify(response.getStatusCode());
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			}
		} catch (Exception e) {
			log.error("Exception occured while Deleting STUDENTTraining....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends StudentTrainingBean.deleteConfirm ---->");
		return showListStudentTrainingPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts StudentTrainingBean.onRowSelect ========>" + event);
		selectedStudentTrainingRequest = ((StudentTrainingRequest) event.getObject());
		String stage = selectedStudentTrainingRequest.getStudentTrainingRequestLog().getStage();
		log.info("stage ==> " +stage);
		if (stage.equalsIgnoreCase("APPROVED") || stage.equalsIgnoreCase("FINALAPPROVED")) {
			addButtonFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
			deleteButtonFlag = false;
		} else if (stage.equalsIgnoreCase("REJECTED")) {
			addButtonFlag = false;
			approveButtonFlag = false;
			deleteButtonFlag = true;
			UserMaster createdBy = selectedStudentTrainingRequest.getCreatedBy();
			UserMaster loginUser = loginBean.getUserMaster();
			Long loginUserId = loginUser != null ? loginUser.getId() : null;
			Long createdById = createdBy != null ? createdBy.getId() : null;
			if(loginUserId != null && createdById != null &&  loginUserId.longValue() == createdById.longValue()) {
				editButtonFlag = true;
			} else {
				editButtonFlag = false;
			}
		} else if (stage.equalsIgnoreCase("SUBMITTED")) {
			addButtonFlag = false;
			approveButtonFlag = true;
			editButtonFlag = false;
			deleteButtonFlag = true;
		}
		//addButtonFlag = false;
		// log.info("<===Starts StudentTrainingBean.onRowSelect ========>" +
		// selectedStudentTraining.getStatus());
		/*
		 * if (selectedStudentTraining.getStatus().equals("APPROVED") ||
		 * selectedStudentTraining.getStatus().equals("REJECTED")) { editButtonFlag =
		 * false; approveButtonFlag = false; editButtonFlag = false; } else if
		 * (selectedStudentTraining.getStatus().equals("INPROGRESS")) {
		 * approveButtonFlag = true; editButtonFlag = true; } else { editButtonFlag =
		 * true; }
		 */
		log.info("<===Ends StudentTrainingBean.onRowSelect ========>");
	}

	public String submit() {
		log.info("<=======Starts  StudentTrainingBean.submit ======>" + studentTraining);
		
		if(studentTraining.getInstitutionType() == null) {
			AppUtil.addError("Institution type not found");
			return null;
		}
		if(studentTraining.getInstitutionName() == null) {
			AppUtil.addError("Institution name not found");
			return null;
		}
		if(studentTraining.getScheduledStartDate() == null) {
			AppUtil.addError("scheduled start date not found");
			return null;
		}
		if(studentTraining.getScheduledEndDate() == null) {
			AppUtil.addError("Scheduled end date not found");
			return null;
		}
		if(studentTraining.getScheduledStartDate().after(studentTraining.getScheduledEndDate())) {
			AppUtil.addError("Scheduled start date should less than Scheduled end date");
			return null;
		}
		if(startDate == null) {
			AppUtil.addError("scheduled start time not found");
			return null;
		}
		if(endDate == null) {
			AppUtil.addError("Scheduled end time not found");
			return null;
		}
		if(startDate.after(endDate)) {
			AppUtil.addError("scheduled start time should less than Scheduled end time");
			return null;
		}
		if(studentTraining.getPurposeOfTraining() == null) {
			AppUtil.addError("Purpose of Training not found");
			return null;
		}
		if(studentTraining.getSubject() == null) {
			AppUtil.addError("Topic not found");
			return null;
		}
		if(studentTraining.getInstitutionAddress1() == null) {
			AppUtil.addError("Institution address 1 not found");
			return null;
		}
		
		if(studentTraining.getInstitutionAddress2() == null) {
			AppUtil.addError("Institution address 2 not found");
			return null;
		}
		if(studentTraining.getInstitutionAddress3() == null) {
			AppUtil.addError("Institution address 3 not found");
			return null;
		}
		if(studentTraining.getInstitutionRepresentativeName() == null) {
			AppUtil.addError("Institution representative name not found");
			return null;
		}
		if(studentTraining.getContactNumber() == null) {
			AppUtil.addError("Contact number not found");
			return null;
		}
		if(studentTraining.getEmailId() == null) {
			AppUtil.addError("Email id not found");
			return null;
		}
		if(studentTraining.getNoOfStudents() == null) {
			AppUtil.addError("Number of students not found");
			return null;
		}
		if(forwardTo == null) {
			AppUtil.addError("Forward to not found");
			return null;
		}
		if(studentTrainingRequestNote.getFinalApproval() == null) {
			AppUtil.addError("Forward for not found");
			return null;
		}
		if(studentTrainingRequestNote.getNote() == null || studentTrainingRequestNote.getNote().isEmpty()) {
			AppUtil.addError("Note not found");
			return null;
		}
		log.info("startDate ==> "+startDate);
		log.info("endDate ==> "+endDate);
		log.info("startDate time==> "+startDate.getTime());
		log.info("endDate ==> "+endDate.getTime());
		log.info("forwardTo ==> "+forwardTo);
		
		 Timestamp startTime=new Timestamp(startDate.getTime());  
		 Timestamp endTime=new Timestamp(endDate.getTime());  
		
		studentTraining.setScheduledStartTime(startTime);
		studentTraining.setScheduledEndTime(endTime);
		
		studentTraining.setCreatedDate(new Date());

		studentTrainingRequestLog.setStage("SUBMITTED");
		studentTraining.setStudentTrainingRequestLog(studentTrainingRequestLog);

		studentTrainingRequestNote.setUserMaster(forwardTo);
		studentTraining.setStudentTrainingRequestNote(studentTrainingRequestNote);

		/*
		 * if (studentTraining.getScheduledStartTime() == null) {
		 * errorMap.notify(ErrorDescription.STUDTRAINING_SHEDULESTART_EMPTY.getErrorCode
		 * ()); return null; } if (studentTraining.getScheduledEndTime() == null) {
		 * errorMap.notify(ErrorDescription.STUDTRAINING_SHEDULESTART_EMPTY.getErrorCode
		 * ()); return null; }
		 */
		/*
		 * if (action.equalsIgnoreCase("ADD")) {
		 * studentTraining.setCreatedBy(loginBean.getUserDetailSession());
		 * //studentTraining.setStatus("INPROGRESS"); studentTraining.setCreatedDate(new
		 * Date()); } else if (action.equalsIgnoreCase("EDIT")) {
		 * studentTraining.setModifiedBy(loginBean.getUserDetailSession());
		 * studentTraining.setModifiedDate(new Date()); } else if
		 * (action.equalsIgnoreCase("APPROVE")) {
		 * //studentTraining.setStatus("APPROVED"); studentTraining.setModifiedDate(new
		 * Date()); //studentTraining.setApprovedDate(new Date());
		 * studentTraining.setModifiedBy(loginBean.getUserDetailSession()); } else if
		 * (action.equalsIgnoreCase("REJECT")) {
		 * //studentTraining.setStatus("REJECTED");
		 * //studentTraining.setApprovedDate(new Date());
		 * studentTraining.setModifiedDate(new Date());
		 * studentTraining.setModifiedBy(loginBean.getUserDetailSession()); }
		 */
		BaseDTO response = httpService.put(STUDENT_TRAINING_URL + "/saveorupdate", studentTraining);
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_SAVE_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("EDIT"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_UPDATE_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("REJECT"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_REJECTED_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("APPROVE"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_APPROVED_SUCCESS.getErrorCode());
			return showListStudentTrainingPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends StudentTrainingBean.submit ======>");
		return null;
	}

	public String statusUpdate() {
		try {
			log.info("inside statusUdate studenttrainingRequest------->" + selectedStudentTrainingRequest.getId());
			StudentTrainingRequestDTO studReqDTO = new StudentTrainingRequestDTO();
			studentTrainingRequestLog.setRemarks(remarkscomment);
			studReqDTO.setStudentTrainingRequestNote(studentTrainingRequestNote);
			studReqDTO.setStudentTrainingRequestLog(studentTrainingRequestLog);
			studReqDTO.setStudentTrainingRequest(selectedStudentTrainingRequest);
			if (studentTrainingRequestLog.getStage().equalsIgnoreCase("REJECTED")) {
				studentTrainingRequestLog.setStage("REJECTED");
			} else {
				if (studentTrainingRequestNote.getFinalApproval() == true && previousApproval == true
						&& studentTrainingRequestLog.getStage().equalsIgnoreCase("APPROVED")) {
					studentTrainingRequestLog.setStage("FINAL_APPROVED");
				} else if (studentTrainingRequestNote.getFinalApproval() == true
						|| studentTrainingRequestNote.getFinalApproval() == false && previousApproval == false
								&& studentTrainingRequestLog.getStage().equalsIgnoreCase("APPROVED")) {
					studentTrainingRequestLog.setStage("APPROVED");
				}
			}
			String url = STUDENT_TRAINING_URL + "/studenttrainingReqForwardStage";
			BaseDTO response = httpService.post(url, studReqDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (studentTrainingRequestLog.getStage().equalsIgnoreCase("FINAL_APPROVED")
						&& studentTrainingRequestLog.getStage().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_APPROVED_SUCCESSFULLY.getCode());
				} else if (studentTrainingRequestLog.getStage().equalsIgnoreCase("REJECTED")) {
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return showListStudentTrainingPage();
	}

	public void getDayDifference() {
		try {

			Date startDate = studentTraining.getScheduledStartDate();
			Date endtDate = studentTraining.getScheduledEndDate();

			if (startDate != null || endtDate != null) {
				long difference = endtDate.getTime() - startDate.getTime();
				Long daydifference = (difference / (1000 * 60 * 60 * 24) + 1);
				studentTraining.setNoOfDays(daydifference);
			}
		} catch (Exception e) {
			log.error(" Exception In EmployeeMasterBean employeeAgeCalculation Method ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	/*
	 * public void uplaodTrainingDocuments(FileUploadEvent event) {
	 * log.info("uplaodCommunityCertificate Upload button is pressed.."); try { if
	 * (event == null || event.getFile() == null) {
	 * log.error(" Upload Document is null ");
	 * errorMap.notify(ErrorDescription.STUDTRAINING_DOCUMENTS_NOT_EMPTY.
	 * getErrorCode()); return; } relievingLetterFile = event.getFile(); long size =
	 * relievingLetterFile.getSize(); log.info("uploadFile size==>" + size); size =
	 * size / 1000; if (size > employeePhotoSize) {
	 * studentTraining.setDocumentPath(null);
	 * FacesContext.getCurrentInstance().getExternalContext().getFlash().
	 * setKeepMessages(true);
	 * errorMap.notify(ErrorDescription.STUDTRAINING_PHOTO_SIZE.getErrorCode());
	 * return; } String photoPathName =
	 * commonDataService.getAppKeyValue("STUDENT_TRAINING_DOCUMENT_UPLOAD_PATH");
	 * String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
	 * studentTraining.setDocumentPath(filePath); originalFileName =
	 * relievingLetterFile.getFileName();
	 * errorMap.notify(ErrorDescription.STUDTRAINING_DOCUMENT_SUCCESS.getErrorCode()
	 * ); log.info(" Relieving Document is uploaded successfully"); } catch
	 * (Exception exp) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * log.error("found exception in file upload", exp); } }
	 */

	public void validateMobileNo(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validate method");

		String mobileNo = (String) value;
		Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		Matcher matcher = pattern.matcher(mobileNo);
		if (!matcher.matches()) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Invalid contact number.");
			context.addMessage(comp.getClientId(context), message);
		}
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		studentTrainingRequestLog.setStage(value);
		setRemarkscomment("");
	}

	public void confirmStatus() {
		if (studentTraining == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public void loadInstitutionTypeList() {
		log.info("<<<< ----------Start loadVehicleDriver ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			institutionTypeList = new ArrayList<InstitutionType>();

			String url = STUDENT_TRAINING_URL + "/loadinstitutiontypelist";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				institutionTypeList = mapper.readValue(jsonResponse, new TypeReference<List<InstitutionType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------loadVehicleDriver-------end----- >>>> " + institutionTypeList);
	}
	
	
	public String approveStudentTrainging() {
    	//This is the View Page ForwardTo approved payment
    	log.error("<---- Start approveIncomingTapal final approval: ----->"+approveRemarks);
		BaseDTO baseDTO = null;
		try {
						
			StudentTraingDTO studentTraingDTO = new StudentTraingDTO();
			if(studentTraining == null && studentTraining.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_ID_NOT_FOUND).getCode());
				return null;
			}
			if(studentTrainingRequestNote == null && studentTrainingRequestNote.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_NOTE_ID_NOT_FOUND).getCode());
				return null;
			}
			if(studentTrainingRequestLog == null && studentTrainingRequestLog.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_LOG_ID_NOT_FOUND).getCode());
				return null;
			}
			if (previousApproval == true && finalapprovalFlag == true) {
				log.info("Inside final approve.");
			} else {
				if(forwardTo == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
					return null;
				}
			}
			if(approveRemarks.equalsIgnoreCase("") || approveRemarks.isEmpty()) {
				errorMap.notify(ErrorDescription.PLEASE_ENTER_APPROVE_COMMENTS.getErrorCode());
				return null;
			}
		
			/*if(approvalFlag.equals(true) && finalapprovalFlag.equals(false)) {
				if(forwardTo == null) {
					AppUtil.addError("Forward to not found");
					return null;
				}
				if(studentTrainingRequestNote.getFinalApproval() == null) {
					AppUtil.addError("Forward for not found");
					return null;
				}
			}*/
			
			log.info("Student traing id ==> " + (studentTraining != null ? studentTraining.getId() : null));
			log.info("Student note id ==> " + (studentTrainingRequestNote != null ? studentTrainingRequestNote.getId() : null));
			log.info("Student log id ==> " + (studentTrainingRequestLog != null ? studentTrainingRequestLog.getId() : null));
			log.info("previousApproval ---------- " + previousApproval);
			log.info("current approveal ---------- " + finalapprovalFlag);
			log.info("Forward to ---------- " + (forwardTo != null ? forwardTo.getId() : null));
			
			studentTraingDTO.setTrainingId(studentTraining != null ? studentTraining.getId() : null);
			studentTraingDTO.setNoteId(studentTrainingRequestNote != null ? studentTrainingRequestNote.getId() : null); 
			studentTraingDTO.setLogId(studentTrainingRequestLog != null ? studentTrainingRequestLog.getId() : null);
			studentTraingDTO.setForwardTo(forwardTo != null ? forwardTo.getId() : null);
			studentTraingDTO.setForwardFor(finalapprovalFlag);
			baseDTO = new BaseDTO();
			if (previousApproval == false && finalapprovalFlag == true) {
				log.error("<---- Approve Status change on Approved  ----->");
				studentTraingDTO.setStatus("APPROVED");
			} else if (previousApproval == true && finalapprovalFlag == true) {
				log.error("<---- Approve Status change on Final Approved  ----->");
				studentTraingDTO.setStatus("FINALAPPROVED");
			} else {
				log.error("<---- Approve Status change on Approved  ----->");
				studentTraingDTO.setStatus("APPROVED");
			}
			log.info("approve remarks=========>" + approveRemarks);
			studentTraingDTO.setRemarks(approveRemarks); 
			studentTraingDTO.setNote(studentTrainingRequestDTO.getNote());
			String url = STUDENT_TRAINING_URL+"/approverejectstudenttraining";
			log.info("approveRejectStudentTrainging url ==> " + url);
			baseDTO = httpService.post(url, studentTraingDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.RETAIL_PRODUCTION_PLAN_APPROVED.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().execute("PF('dlgComments4').hide();");
				RequestContext.getCurrentInstance().update("growls");
				return showListStudentTrainingPage();
			}
		} catch (Exception e) {
			log.error("approveStudentTrainging method inside exception-------", e);
		}
		return null;
	}

	public String rejectStudentTrainging() {
		//This is the View Page ForwardTo rejected payment
		log.error("<----Start IncomingTapalBean.rejected: ----->");
		BaseDTO baseDTO = null;
		try {
			StudentTraingDTO studentTraingDTO = new StudentTraingDTO();
			if(studentTraining == null && studentTraining.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_ID_NOT_FOUND).getCode());
				return null;
			}
			if(studentTrainingRequestNote == null && studentTrainingRequestNote.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_NOTE_ID_NOT_FOUND).getCode());
				return null;
			}
			if(studentTrainingRequestLog == null && studentTrainingRequestLog.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STUDENT_TRAINING_REQUEST_LOG_ID_NOT_FOUND).getCode());
				return null;
			}
			if(approvalFlag.equals(true) && finalapprovalFlag.equals(false)) {
				if(forwardTo == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
					return null;
				}
			}
			if(rejectRemarks == null || rejectRemarks.isEmpty()) {
				errorMap.notify(ErrorDescription.PLEASE_ENTER_REJECT_COMMENTS.getErrorCode());
				return null;
			}
			
			/*if(approvalFlag.equals(true) && finalapprovalFlag.equals(false)) {
				if(forwardTo == null) {
					AppUtil.addError("Forward to not found");
					return null;
				}
				if(studentTrainingRequestNote.getFinalApproval() == null) {
					AppUtil.addError("Forward for not found");
					return null;
				}
			}*/
			
			log.info("Student traing id ==> " + studentTraining.getId());
			log.info("Student note id ==> " + studentTrainingRequestNote.getId());
			log.info("Student log id ==> " + studentTrainingRequestLog.getId());
			log.info("previousApproval ---------- " + previousApproval);
			log.info("current approveal ---------- " + finalapprovalFlag);
			if(forwardTo != null) {
				log.info("Forward to ---------- " + forwardTo.getId());
			}
			
			
			studentTraingDTO.setTrainingId(studentTraining.getId());
			studentTraingDTO.setNoteId(studentTrainingRequestNote.getId()); 
			studentTraingDTO.setLogId(studentTrainingRequestLog.getId());
			if(forwardTo != null) {
				studentTraingDTO.setForwardTo(forwardTo.getId());
			}
			studentTraingDTO.setForwardFor(finalapprovalFlag);
			studentTraingDTO.setStatus("REJECTED");
			studentTraingDTO.setRemarks(rejectRemarks); 
			String url = STUDENT_TRAINING_URL+"/approverejectstudenttraining";
			baseDTO = httpService.post(url,studentTraingDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.RETAIL_PRODUCTION_PLAN_REJECTED.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().execute("PF('dlgComments5').hide();");
				RequestContext.getCurrentInstance().update("growls");
				return showListStudentTrainingPage();
			}
		} catch (Exception e) {
			log.error("rejectStudentTrainging method inside exception-------", e);
		}
		return null;
	}
	
	public void downloadFile() {
		log.info("StudentTrainingRequestBean.downloadFile() - START");
		InputStream input = null;
		try {
			File files = new File(downloadFilePath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			trainingFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.info("File name: " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException at downloadFile()" , e);
		} catch (Exception exp) {
			log.error("Exception at downloadFile()", exp);
		}
		log.info("StudentTrainingRequestBean.downloadFile() - END");
	}
	
	@PostConstruct
	public String showViewListPage() {
		log.info("StudentTrainingRequestBean.showViewListPage() - START");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String requestId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			requestId = httpRequest.getParameter("requestId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("StudentTrainingRequestBean.showViewListPage() - " + requestId);
			log.info("StudentTrainingRequestBean.showViewListPage() - " + notificationId);

		}
		if (StringUtils.isNotEmpty(requestId)) {
			selectedStudentTrainingRequest = new StudentTrainingRequest();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedStudentTrainingRequest.setNotificationId(Long.parseLong(notificationId));
			}
			selectedStudentTrainingRequest.setId(Long.parseLong(requestId));
			action = "VIEW";
			studentTrainingListAction();
		}
		return null;
	}

}
