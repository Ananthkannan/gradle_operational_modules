package in.gov.cooptex.operation.rest.ui.master;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CustomerMasterDTO;
import in.gov.cooptex.core.dto.CustomerMasterSearchRequestDTO;
import in.gov.cooptex.core.dto.CustomerMasterSearchResponseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.BusinessTypeMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("customerMasterBean")
@Log4j2
@Scope("session")
public class CustomerMasterBean {
	/**
	 * 
	 */
	// private static final long serialVersionUID = 654646L;
	private static final String LIST_PAGE = "/pages/masters/listCustomerMaster.xhtml?faces-redirect=true";
	private static final String ADD_PAGE = "/pages/masters/createCustomerMaster.xhtml?faces-redirect=true";
	private static final String VIEW_PAGE = "/pages/masters/viewCustomerMaster.xhtml?faces-redirect=true";

	@Getter
	@Setter
	private CustomerMasterDTO customerMaster;

	@Getter
	@Setter
	private List<BusinessTypeMaster> listBusinessTypeMaster;

	@Getter
	@Setter
	private List<CustomerTypeMaster> listCustomerTypeMaster;

	@Getter
	@Setter
	private List<BankMaster> listBankMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private List<BankBranchMaster> listBankBranchMaster;

	@Getter
	@Setter
	private List<StateMaster> listStateMaster;

	@Getter
	@Setter
	private List<DistrictMaster> listDistrictMaster;

	@Getter
	@Setter
	private List<TalukMaster> listTalukMaster;

	@Getter
	@Setter
	private Long talukMasterId;

	@Getter
	@Setter
	private AddressMaster customerAddress;

	@Getter
	@Setter
	private Long stateMasterId, bankMasterId;

	@Getter
	@Setter
	private List<CityMaster> listCityMaster;

	@Getter
	@Setter
	private Long districtMasterId;

	@Getter
	@Setter
	private AddressMaster billingAddress;

	@Getter
	@Setter
	private AddressMaster shippingAddress;

	@Getter
	@Setter
	private String addressType, action, ifsc;

	@Getter
	@Setter
	CustomerMasterSearchResponseDTO customerSearchResponse;

	@Getter
	@Setter
	LazyDataModel<CustomerMasterSearchResponseDTO> customerResponseLazyList;

	@Getter
	@Setter
	List<CustomerMasterDTO> parentCustomerList = new ArrayList<CustomerMasterDTO>();

	@Getter
	@Setter
	Integer totalRecords;

	@Getter
	@Setter
	private String officeAddressTextArea, billingAddressTextArea, shippingAddressTextArea;

	@Getter
	@Setter
	private Boolean addButtonFlag = true;

	@Getter
	private Long[] installMonths = { 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L, 12L };

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	String serverURL;

	BaseDTO baseDTO;

	@Autowired
	CommonBean commonBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private List<CountryMaster> listCountryMaster;

	@Getter
	@Setter
	List<CustomerMaster> customerParentAutoSearchList = new ArrayList<>();

	@Getter
	@Setter
	CustomerMaster selectedAutoCustomerParent;

	@Getter
	@Setter
	Boolean countryflag = false;
	@Getter
	@Setter
	String customerCode;

	private Pattern pattern;
	private Matcher matcher;

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Getter
	@Setter
	private boolean billingCheckBox;

	@Getter
	@Setter
	private boolean shippingCheckBox;

	public CustomerMasterBean() {
		customerMaster = new CustomerMasterDTO();
		baseDTO = new BaseDTO();
		loadValues();
		listBusinessTypeMaster = new ArrayList<BusinessTypeMaster>();
		listCustomerTypeMaster = new ArrayList<CustomerTypeMaster>();
		listBankMaster = new ArrayList<BankMaster>();
		listBankBranchMaster = new ArrayList<BankBranchMaster>();
		listStateMaster = new ArrayList<StateMaster>();
		listDistrictMaster = new ArrayList<DistrictMaster>();
		listTalukMaster = new ArrayList<TalukMaster>();
		selectedAutoCustomerParent = new CustomerMaster();
		pattern = Pattern.compile(EMAIL_PATTERN);
	}

	public String showList() {
		customerSearchResponse = new CustomerMasterSearchResponseDTO();
		showAddPage();
		loadCustomerLazySList();
		addButtonFlag = true;
		return LIST_PAGE;
	}

	public String showAddPage() {
		action = "ADD";
		bankMasterId = null;
		customerMaster = new CustomerMasterDTO();
		customerAddress = new AddressMaster();
		billingAddress = new AddressMaster();
		shippingAddress = new AddressMaster();
		listCountryMaster = new ArrayList<>();
		BankBranchMaster branch = new BankBranchMaster();
		customerMaster.setBankBranchMaster(branch);
		loadValues();
		loadBusinessTypeMaster();
		loadCustomerTypeMaster();
		loadBankMaster();
		loadStateMaster();
		listCountryMaster = commonDataService.loadCountry();
		loadParentCustomerMaster();
		officeAddressTextArea = "";
		billingAddressTextArea = "";
		shippingAddressTextArea = "";
		selectedAutoCustomerParent = new CustomerMaster();
		getNextSequenceConfigValueForCustomerCode();
		billingCheckBox = false;
		shippingCheckBox = false;
		return ADD_PAGE;
	}

	public String showEditPage() {
		action = "EDIT";
		if (customerSearchResponse == null || customerSearchResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		log.info("<<=========   showEditPage " + customerSearchResponse.getId());

		customerMaster = new CustomerMasterDTO();
		try {
			loadBusinessTypeMaster();
			loadCustomerTypeMaster();
			loadBankMaster();
			loadStateMaster();
			loadParentCustomerMaster();
			bankMasterId = customerSearchResponse.getBankId();
			loadBranchMaster();

			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/get/"
					+ customerSearchResponse.getId();
			baseDTO = httpService.get(url);
			log.info("BaseDTO Dto :" + baseDTO.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			customerMaster = mapper.readValue(jsonResponse, CustomerMasterDTO.class);
			log.info("<<=======  INFORMATIONS::  " + customerMaster);

			// bankMasterId =
			// customerMaster.getBankBranchMaster().getBankMaster().getId();
			// loadBranchMaster();
			if (customerMaster != null) {
				if (customerMaster.getBankBranchMaster() != null
						&& customerMaster.getBankBranchMaster().getIfscCode() != null) {
					ifsc = customerMaster.getBankBranchMaster().getIfscCode();
				}
				customerAddress = customerMaster.getAddressMasterToCreate();
				billingAddress = customerMaster.getBillingAddressMaster();
				shippingAddress = customerMaster.getShippingAddressMaster();

				CustomerMaster parent = new CustomerMaster();
				parent.setId(customerMaster.getParentCustomerId());
				parent.setCode(customerMaster.getParentCustomerCode());
				parent.setName(customerMaster.getParentCustomerName());
				selectedAutoCustomerParent = parent;
			}
			listCountryMaster = commonDataService.loadCountry();

			prepareAddressText(customerMaster);

		} catch (Exception e) {
			log.error("<<<=======   ERROR IN EDIT CUSTOMER MASTER =========>>", e);
		}
		return ADD_PAGE;
	}

	public String showViewPage() {
		action = "VIEW";
		if (customerSearchResponse == null || customerSearchResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		log.info("<<=========   showViewPage " + customerSearchResponse.getId());

		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/get/"
					+ customerSearchResponse.getId();
			baseDTO = httpService.get(url);
			log.info("BaseDTO Dto :" + baseDTO.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			customerMaster = mapper.readValue(jsonResponse, CustomerMasterDTO.class);

			prepareAddressText(customerMaster);

			log.info("<<=======  INFORMATIONS::  " + customerMaster);
		} catch (Exception e) {
			log.error("<<<=======  Error in view customer master =====>>");
		}
		return VIEW_PAGE;
	}

	public String getNextSequenceConfigValueForCustomerCode() {
		log.info("getNextSequenceConfigValueForCustomerCode called......");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/nextsequenceconfigvalue";
			baseDTO = httpService.get(url);
			log.info("BaseDTO Dto :" + baseDTO.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			customerCode = mapper.readValue(jsonResponse, String.class);

			prepareAddressText(customerMaster);

			log.info("<<=======  INFORMATIONS::  " + customerMaster);
		} catch (Exception e) {
			log.error("<<<=======  Error in view customer master =====>>");
		}

		return customerCode;

	}

	private void loadValues() {
		log.info("---loadValues----");
		try {
			serverURL = AppUtil.getPortalServerURL();
			log.info("<<====== serverURL:: " + serverURL);
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());

		}
	}

	public String submitCustomerMaster() {
		log.info("<<=======  customerMasterBean --- submitCustomerMaster === STARTS");

		if (checkValidation()) {

			log.info("<<==========   ADD SUCCESS ##############");
			// log.info("<<==:::: "+customerMaster);

			if (StringUtils.isEmpty(customerMaster.getPrimaryContactNumber().trim())) {
				customerMaster.setPrimaryContactNumber(null);
			}

			if (action.equalsIgnoreCase("ADD")) {
				String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/create";
				baseDTO = httpService.post(url, customerMaster);
			} else if (action.equalsIgnoreCase("EDIT")) {
				String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/update";
				baseDTO = httpService.post(url, customerMaster);
			}
			log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					if (action.equalsIgnoreCase("ADD")) {
						errorMap.notify(ErrorDescription.CUST_CREATE_SUCCESS.getErrorCode());
					} else {
						errorMap.notify(ErrorDescription.CUST_EDIT_SUCCESS.getErrorCode());
					}
					log.info("Customer Master has been Added");
				} else {
					log.error("Status code:" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
 
		} else {
			return null;
		}
		addButtonFlag = true;
		return LIST_PAGE;
	}

	public String deleteCustomerMaster() {
		log.info("<<====  Delete Customer master =====>>");
		if (customerSearchResponse == null || customerSearchResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/delete/"
					+ customerSearchResponse.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				// errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				log.info("Customer Deleted Successfuly");
				AppUtil.addInfo("Customer Deleted Successfuly");
			} else {
				log.info("Customer Cannot be Deleted ");
				// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				AppUtil.addWarn("Customer Cannot be Deleted");
			}

		} catch (Exception e) {
			log.error("<<==== ERROR IN DELETE" + e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		addButtonFlag = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmDepartmentDelete').show();");
		return showList();
	}

	public void loadBusinessTypeMaster() {
		List<Map> list = new ArrayList<>();
		listBusinessTypeMaster = new ArrayList<>();
		try {
			String url = serverURL + "/businesstypemaster/all";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			list = mapper.readValue(jsonResponse, new TypeReference<List<Map>>() {
			});
			if (list != null) {
				for (Map mp : list) {
					String ids = mp.get("businessTypeId").toString();
					Long id = Long.parseLong(ids);
					String code = mp.get("businessTypeCode").toString();
					String name = mp.get("businessTypeName").toString();
					BusinessTypeMaster busines = new BusinessTypeMaster();
					busines.setId(id);
					busines.setCode(code);
					busines.setName(name);
					listBusinessTypeMaster.add(busines);
				}
			}
			log.info("<<=== listBusinessTypeMaster size..." + listBusinessTypeMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void loadCustomerTypeMaster() {
		try {
			String url = serverURL + "/customertypemaster/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			listCustomerTypeMaster = mapper.readValue(jsonResponse, new TypeReference<List<CustomerTypeMaster>>() {
			});
			log.info("<<=========  customerTypelist:: " + listCustomerTypeMaster);
		} catch (Exception e) {
			log.error("<<==============  error in loadCustomerTypeMaster" + e);
		}
	}

	public void loadBankMaster() {
		try {
			String url = serverURL + "/bankmaster/getAllBanks";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBankMaster = mapper.readValue(jsonResponse, new TypeReference<List<BankMaster>>() {
			});
			listBankBranchMaster.clear();
		} catch (Exception e) {
			log.error("<<==============  error in loadBankMaster " + e);
		}
	}

	public void loadBranchMaster() {
		try {
			String url = serverURL + "/bankBranchMaster/getbybank/" + bankMasterId;
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBankBranchMaster = mapper.readValue(jsonResponse, new TypeReference<List<BankBranchMaster>>() {
			});

			customerMaster.setBankBranchMaster(new BankBranchMaster());
			log.info("<<=========  branch list:: " + listBankBranchMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBranchMaster " + e);
		}
	}

	public void loadStateMaster() {
		try {
			String url = serverURL + "/stateMaster/getAllStates";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listStateMaster = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
			});
			listDistrictMaster.clear();
			listTalukMaster.clear();
		} catch (Exception e) {
			log.error("<<==============  error in loadStateMaster  " + e);
		}
	}

	public void onchangeState(Long id) {
		log.info("<<===  selected state ::: " + id);
		try {
			String url = serverURL + "/districtMaster/getalldistrictbystate/" + id;
			log.info("<<=== get District URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listDistrictMaster = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
			});
			listTalukMaster.clear();
			log.info("<<====  Districts::: " + listDistrictMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in onchangeState  " + e);
			listDistrictMaster.clear();
			listTalukMaster.clear();
			listCityMaster.clear();
		}
	}

	public void onchangeCountry(Long id) {
		log.info("<<=== onchangeCountry selected Country id::: " + id);
		try {
			listStateMaster = new ArrayList<>();
			listStateMaster = commonDataService.loadActiveStates(id);
			log.info("<<:::::::::::stateMaster List:::::::::::>>" + listStateMaster.size());
			listDistrictMaster = new ArrayList<>();
			listTalukMaster = new ArrayList<>();
			listCityMaster = new ArrayList<>();
		} catch (Exception e) {
			log.error("<<==============  error in onchangeCountry  " + e);
			listDistrictMaster.clear();
			listTalukMaster.clear();
			listCityMaster.clear();
		}
	}

	public void onchangeDistrict(Long id) {
		log.info("<<===  selected district ::: " + id);
		if (id == null) {
			listTalukMaster.clear();
			listCityMaster.clear();
		} else {
			try {
				listCityMaster = commonDataService.getCityByDistrictId(id);
				commonBean.loadActiveTaluksByDistrict(id);
				listTalukMaster = commonBean.getTalukMasterList();

				log.info("<<====  Cisties::: " + listCityMaster.size());
				log.info("<<====  Taluks::: " + listTalukMaster.size());
			} catch (Exception e) {
				log.error("<<==============  error in onchangeDistrict  " + e);
				listTalukMaster.clear();
				listCityMaster.clear();
			}
		}
	}

	public void loadParentCustomerMaster() {
		try {

			/*
			 * String url = serverURL+ appPreference.getOperationApiUrl() +
			 * "/customerMaster/customerlist/parents"; baseDTO = httpService.get(url);
			 * jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			 * parentCustomerList = mapper.readValue(jsonResponse, new
			 * TypeReference<List<CustomerMaster>>() { });
			 */
		} catch (Exception e) {
			log.error("<<======  error in load parent customer =======>>>>>");
		}
	}

	private void prepareAddressText(CustomerMasterDTO customerMaster) {
		if (customerMaster != null && customerMaster.getAddressMasterToCreate() != null) {
			// customerAddress = customerMaster.getAddressMasterToCreate();
			// String officCountry="";
			//
			// if(customerAddress!=null && customerAddress.getCountryMaster()!=null &&
			// customerAddress.getCountryMaster().getName()!=null) {
			// officCountry=", " +customerAddress.getCountryMaster().getName()+",";
			// }
			//
			// officeAddressTextArea = customerAddress.getAddressLineOne() + ", " +
			// customerAddress.getAddressLineTwo()
			// + ", " + customerAddress.getAddressLineThree() + ", "
			// + customerAddress.getDistrictMaster().getName() + ", " +
			// customerAddress.getStateMaster().getName()
			// +officCountry
			// + ", Pin: " + customerAddress.getPostalCode() + ", Landmark: " +
			// customerAddress.getLandmark();
			officeAddressTextArea = prepareAddress(customerMaster.getAddressMasterToCreate());
		}
		if (customerMaster != null && customerMaster.getBillingAddressMaster() != null) {
			// billingAddress = customerMaster.getBillingAddressMaster();
			// String billingCountry="";
			// if(billingAddress!=null && billingAddress.getCountryMaster()!=null &&
			// billingAddress.getCountryMaster().getName()!=null) {
			// billingCountry=", " +billingAddress.getCountryMaster().getName()+",";
			// }
			// billingAddressTextArea = billingAddress.getAddressLineOne() + ", " +
			// billingAddress.getAddressLineTwo()
			// + ", " + billingAddress.getAddressLineThree() + ", " +
			// billingAddress.getDistrictMaster().getName()
			// + ", " + billingAddress.getStateMaster().getName()+ billingCountry + ", Pin:
			// " + billingAddress.getPostalCode()
			// + ", Landmark: " + billingAddress.getLandmark();
			billingAddressTextArea = prepareAddress(customerMaster.getBillingAddressMaster());
		}
		if (customerMaster != null && customerMaster.getShippingAddressMaster() != null) {
			// shippingAddress = customerMaster.getShippingAddressMaster();
			// String shippingCountry="";
			// if(shippingAddress!=null && shippingAddress.getCountryMaster()!=null &&
			// shippingAddress.getCountryMaster().getName()!=null) {
			// shippingCountry=", " +shippingAddress.getCountryMaster().getName()+",";
			// }
			// shippingAddressTextArea = shippingAddress.getAddressLineOne() + ", " +
			// shippingAddress.getAddressLineTwo()
			// + ", " + shippingAddress.getAddressLineThree() + ", "
			// + shippingAddress.getDistrictMaster().getName() + ", " +
			// shippingAddress.getStateMaster().getName()
			// +shippingCountry
			// + ", Pin: " + shippingAddress.getPostalCode() + ", Landmark: " +
			// shippingAddress.getLandmark();
			shippingAddressTextArea = prepareAddress(customerMaster.getShippingAddressMaster());

		}
	}

	public void addAddress(String addressType) {
		addressType = addressType;
		Long districtid = 0l;
		Long stateid = 0l;
		Long countryid = 0l;
		countryflag = false;
		log.info("<<== addAddress() - action " + action);

		if (action.equalsIgnoreCase("EDIT") && customerMaster != null && customerMaster.getId() != null) {
			customerAddress = new AddressMaster();
			billingAddress = new AddressMaster();
			shippingAddress = new AddressMaster();

			if (addressType.equalsIgnoreCase("office") && customerMaster.getAddressMasterToCreate() != null
					&& customerMaster.getAddressMasterToCreate().getId() != null
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equalsIgnoreCase("Export")) {
				customerAddress = customerMaster.getAddressMasterToCreate();
				stateid = customerAddress.getStateMaster().getId();
				districtid = customerAddress.getDistrictMaster().getId();
			} else if (addressType.equalsIgnoreCase("billing") && customerMaster.getBillingAddressMaster() != null
					&& customerMaster.getBillingAddressMaster().getId() != null
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equalsIgnoreCase("Export")) {
				billingAddress = customerMaster.getBillingAddressMaster();
				stateid = billingAddress.getStateMaster().getId();
				districtid = billingAddress.getDistrictMaster().getId();
			} else if (addressType.equalsIgnoreCase("shipping") && customerMaster.getShippingAddressMaster() != null
					&& customerMaster.getShippingAddressMaster().getId() != null
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equalsIgnoreCase("Export")) {
				shippingAddress = customerMaster.getShippingAddressMaster();
				stateid = shippingAddress.getStateMaster().getId();
				districtid = shippingAddress.getDistrictMaster().getId();
			}

			if (customerMaster != null && customerMaster.getBusinessTypeMasterToCreate() != null
					&& customerMaster.getBusinessTypeMasterToCreate().getName() != null) {
				/*
				 * if (addressType.equalsIgnoreCase("office") &&
				 * customerMaster.getBusinessTypeMasterToCreate().getName().equalsIgnoreCase(
				 * "Export") &&
				 * customerMaster.getAddressMasterToCreate().getCountryMaster()!=null) {
				 * customerAddress = customerMaster.getAddressMasterToCreate(); countryid =
				 * customerAddress.getCountryMaster().getId(); onchangeCountry(countryid); }
				 * else if (addressType.equalsIgnoreCase("billing") &&
				 * customerMaster.getBusinessTypeMasterToCreate().getName().equalsIgnoreCase(
				 * "Export") &&
				 * customerMaster.getAddressMasterToCreate().getCountryMaster()!=null) {
				 * billingAddress = customerMaster.getBillingAddressMaster(); countryid =
				 * billingAddress.getCountryMaster().getId(); onchangeCountry(countryid); } else
				 * if (addressType.equalsIgnoreCase("shipping") &&
				 * customerMaster.getBusinessTypeMasterToCreate().getName().equalsIgnoreCase(
				 * "Export") &&
				 * customerMaster.getAddressMasterToCreate().getCountryMaster()!=null) {
				 * shippingAddress = customerMaster.getShippingAddressMaster(); countryid =
				 * shippingAddress.getCountryMaster().getId(); onchangeCountry(countryid); }
				 */

				AddressMaster addressMaster = customerMaster.getAddressMasterToCreate();
				
 
				if (addressMaster != null) {
					if (addressType.equalsIgnoreCase("office") && addressMaster.getCountryMaster() != null) {
						customerAddress = addressMaster;
						countryid = customerAddress.getCountryMaster().getId();
						onchangeCountry(countryid);
					} else if (addressType.equalsIgnoreCase("billing") && addressMaster.getCountryMaster() != null) {
						billingAddress = customerMaster.getBillingAddressMaster();
						countryid = billingAddress.getCountryMaster().getId();
						onchangeCountry(countryid);
					} else if (addressType.equalsIgnoreCase("shipping") && addressMaster.getCountryMaster() != null) {
						shippingAddress = customerMaster.getShippingAddressMaster();
						countryid = shippingAddress == null ? null
								: (shippingAddress.getCountryMaster() == null ? null
										: shippingAddress.getCountryMaster().getId());
						if (countryid != null) {
							onchangeCountry(countryid);
						}
					}
				}
			}

			// if
			// (customerMaster.getBusinessTypeMasterToCreate().getName().equalsIgnoreCase("Export"))
			// {
			if (customerMaster.getBusinessTypeMasterToCreate().getCode().equalsIgnoreCase("Export")) {
				countryflag = true;
			}
			customerAddress = customerMaster.getAddressMasterToCreate();
			billingAddress = customerMaster.getBillingAddressMaster();
			shippingAddress = customerMaster.getShippingAddressMaster();
			onchangeState(stateid);
			onchangeDistrict(districtid);
		}

		if (customerMaster.getBusinessTypeMasterToCreate() == null
				|| customerMaster.getBusinessTypeMasterToCreate().getId() == null) {
			log.info("<<=======   Business Type null ==========>>");
			errorMap.notify(ErrorDescription.CUST_BUSINESS_TYPE_REQUIRED.getErrorCode());
			RequestContext.getCurrentInstance().execute("PF('addresspanel').hide();");
			RequestContext.getCurrentInstance().execute("PF('billingaddresspanel').hide();");
			RequestContext.getCurrentInstance().execute("PF('shippingaddresspanel').hide();");
			return;
		} else {
			if (addressType.equalsIgnoreCase("office")) {
				RequestContext.getCurrentInstance().execute("PF('addresspanel').show();");
			} else if (addressType.equalsIgnoreCase("billing")) {
				RequestContext.getCurrentInstance().execute("PF('billingaddresspanel').show();");
			} else if (addressType.equalsIgnoreCase("shipping")) {
				RequestContext.getCurrentInstance().execute("PF('shippingaddresspanel').show();");
			}
		}
		// if (action.equalsIgnoreCase("ADD") &&
		// customerMaster.getBusinessTypeMasterToCreate().getName().equals("Export")) {
		if (action.equalsIgnoreCase("ADD")
				&& customerMaster.getBusinessTypeMasterToCreate().getCode().equals("Export")) {
			countryflag = true;
			/*
			 * customerAddress.setCountryMaster(null); listStateMaster.clear();
			 * listTalukMaster.clear(); listDistrictMaster.clear(); listCityMaster.clear();
			 */

		}

	}

	public void createAddress() {

		if (customerAddress != null) {
			if (customerAddress.getCountryMaster() != null && customerAddress.getCountryMaster().getName() != null
					&& "India".equals(customerAddress.getCountryMaster().getName())
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equals("EXPORT")) {
				if (customerAddress.getStateMaster() == null || customerAddress.getStateMaster().getId() == null) {
					errorMap.notify(ErrorDescription.STATE_NAME_REQUIRED.getErrorCode());
				} else if (customerAddress.getDistrictMaster() == null
						|| customerAddress.getDistrictMaster().getId() == null) {
					errorMap.notify(ErrorDescription.DISTRICT_MASTER_NAME_REQUIRED.getErrorCode());
				}
			}

			String country = "";
			String addressForTextArea = "";
			String addressTwo = "";
			String addressThree = "";
			if (customerAddress.getCountryMaster() != null) {
				country = customerAddress.getCountryMaster().getName();
			} else {
				country = "";
			}

			/*
			 * String addressForTextArea = customerAddress.getAddressLineOne() + ", " +
			 * customerAddress.getAddressLineTwo() + ", " +
			 * customerAddress.getAddressLineThree() + ", " +
			 * customerAddress.getDistrictMaster().getName() + ", " +
			 * customerAddress.getStateMaster().getName()+", "+country+" Pin: " +
			 * customerAddress.getPostalCode() + " Landmark: " +
			 * customerAddress.getLandmark();
			 */
			if (customerAddress.getAddressLineTwo() != null && !customerAddress.getAddressLineTwo().isEmpty()) {
				addressTwo = ", " + customerAddress.getAddressLineTwo();
			}
			if (customerAddress.getAddressLineThree() != null && !customerAddress.getAddressLineThree().isEmpty()) {
				addressThree = ", " + customerAddress.getAddressLineThree();
			}

			String districtName = customerAddress.getDistrictMaster() == null ? ""
					: customerAddress.getDistrictMaster().getName();
			String stateName = customerAddress.getStateMaster() == null ? ""
					: customerAddress.getStateMaster().getName();

			StringBuilder sb = new StringBuilder();

			sb.append(customerAddress.getAddressLineOne());
			sb.append(addressTwo);
			sb.append(addressThree);

			if (StringUtils.isNotEmpty(districtName)) {
				sb.append(", ");
				sb.append(districtName);
			}

			if (StringUtils.isNotEmpty(country)) {
				sb.append(", ");
				sb.append(country);
			}

			if (StringUtils.isNotEmpty(stateName)) {
				sb.append(", ");
				sb.append(stateName);
			}

			if (StringUtils.isNotEmpty(customerAddress.getPostalCode())) {
				sb.append(" Pin: ");
				sb.append(customerAddress.getPostalCode());
			}
			if (StringUtils.isNotEmpty(customerAddress.getLandmark())) {
				sb.append(" Landmark: ");
				sb.append(customerAddress.getLandmark());
			}
			sb.append(".");
			addressForTextArea = sb.toString();

			log.info("<<===== addressType:::::::>>>>>> " + addressType);
			if (addressType.equalsIgnoreCase("office")) {
				log.info("<<=====  Inside office address ");
				customerMaster.setAddressMasterToCreate(customerAddress);
				officeAddressTextArea = addressForTextArea;

			} else if (addressType.equalsIgnoreCase("billing")) {
				log.info("<<== Inside billing ADDRESS " + customerAddress);
				customerMaster.setBillingAddressMaster(customerAddress);
				billingAddressTextArea = addressForTextArea;

			} else if (addressType.equalsIgnoreCase("shipping")) {
				log.info("<<== Inside shipping ADDRESS " + customerAddress);
				customerMaster.setShippingAddressMaster(customerAddress);
				shippingAddressTextArea = addressForTextArea;
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addresspanel').hide();");
		}
	}

	public void createBillAddress() {

		if (billingAddress != null) {

			if (billingAddress.getCountryMaster() != null && billingAddress.getCountryMaster().getName() != null
					&& "India".equals(billingAddress.getCountryMaster().getName())
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equals("EXPORT")) {
				if (billingAddress.getStateMaster() == null || billingAddress.getStateMaster().getId() == null) {
					errorMap.notify(ErrorDescription.STATE_NAME_REQUIRED.getErrorCode());
				} else if (billingAddress.getDistrictMaster() == null
						|| billingAddress.getDistrictMaster().getId() == null) {
					errorMap.notify(ErrorDescription.DISTRICT_MASTER_NAME_REQUIRED.getErrorCode());
				}
			}

			customerMaster.setBillingAddressMaster(billingAddress);

			// prepareAddressText(customerMaster);
			String country = "";
			String billAddressTwo = "";
			String billAddressThree = "";
			if (billingAddress != null && billingAddress.getCountryMaster() != null
					&& billingAddress.getCountryMaster().getName() != null) {
				country = billingAddress.getCountryMaster().getName();
			}
			if (billingAddress.getAddressLineTwo() != null && !billingAddress.getAddressLineTwo().isEmpty()) {
				billAddressTwo = ", " + billingAddress.getAddressLineTwo();
			}
			if (billingAddress.getAddressLineThree() != null && !billingAddress.getAddressLineThree().isEmpty()) {
				billAddressThree = ", " + billingAddress.getAddressLineThree();
			}
			/*
			 * String addressForTextArea = billingAddress.getAddressLineOne() + ", " +
			 * billingAddress.getAddressLineTwo() + ", " +
			 * billingAddress.getAddressLineThree() + ", " +
			 * billingAddress.getDistrictMaster().getName() + ", " +
			 * billingAddress.getStateMaster().getName()+", "+country+" Pin: " +
			 * billingAddress.getPostalCode() + " Landmark: " +
			 * customerAddress.getLandmark();
			 */

			String districtName = billingAddress.getDistrictMaster() == null ? ""
					: billingAddress.getDistrictMaster().getName();
			String stateName = billingAddress.getStateMaster() == null ? "" : billingAddress.getStateMaster().getName();

			StringBuilder sb = new StringBuilder();

			sb.append(billingAddress.getAddressLineOne());
			sb.append(billAddressTwo);
			sb.append(billAddressThree);

			if (StringUtils.isNotEmpty(districtName)) {
				sb.append(", ");
				sb.append(districtName);
			}

			if (StringUtils.isNotEmpty(stateName)) {
				sb.append(", ");
				sb.append(stateName);
			}

			if (StringUtils.isNotEmpty(country)) {
				sb.append(", ");
				sb.append(country);
			}
			if (StringUtils.isNotEmpty(billingAddress.getPostalCode())) {
				sb.append("Pin: ");
				sb.append(billingAddress.getPostalCode());
			}
			if (StringUtils.isNotEmpty(billingAddress.getLandmark())) {
				sb.append(" Landmark: ");
				sb.append(billingAddress.getLandmark());
			}

			// String addressForTextArea = billingAddress.getAddressLineOne() +
			// billAddressTwo + billAddressThree + ", "
			// + billingAddress.getDistrictMaster().getName() + ", " +
			// billingAddress.getStateMaster().getName()
			// + ", " + country + " Pin: " + billingAddress.getPostalCode() + " Landmark: "
			// + customerAddress.getLandmark();

			sb.append(".");
			billingAddressTextArea = sb.toString();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('billingaddresspanel').hide();");
		}
	}

	public void createShipAddress() {
		if (shippingAddress != null) {

			if (shippingAddress.getCountryMaster() != null && shippingAddress.getCountryMaster().getName() != null
					&& "India".equals(shippingAddress.getCountryMaster().getName())
					&& !customerMaster.getBusinessTypeMasterToCreate().getCode().equals("EXPORT")) {
				if (shippingAddress.getStateMaster() == null || shippingAddress.getStateMaster().getId() == null) {
					errorMap.notify(ErrorDescription.STATE_NAME_REQUIRED.getErrorCode());
				} else if (shippingAddress.getDistrictMaster() == null
						|| shippingAddress.getDistrictMaster().getId() == null) {
					errorMap.notify(ErrorDescription.DISTRICT_MASTER_NAME_REQUIRED.getErrorCode());
				}
			}

			customerMaster.setShippingAddressMaster(shippingAddress);
			String country = "";
			String shippingAddressTwo = "";
			String shippingAddressThree = "";
			if (shippingAddress != null && shippingAddress.getCountryMaster() != null
					&& shippingAddress.getCountryMaster().getName() != null) {
				country = shippingAddress.getCountryMaster().getName();
			}
			if (shippingAddress.getAddressLineTwo() != null && !shippingAddress.getAddressLineTwo().isEmpty()) {
				shippingAddressTwo = ", " + shippingAddress.getAddressLineTwo();
			}
			if (shippingAddress.getAddressLineThree() != null && !shippingAddress.getAddressLineThree().isEmpty()) {
				shippingAddressThree = ", " + shippingAddress.getAddressLineThree();
			}
			/*
			 * String addressForTextArea = shippingAddress.getAddressLineOne() + ", " +
			 * shippingAddress.getAddressLineTwo() + ", " +
			 * shippingAddress.getAddressLineThree() + ", " +
			 * shippingAddress.getDistrictMaster().getName() + ", " +
			 * shippingAddress.getStateMaster().getName()+", "+country+" Pin: " +
			 * shippingAddress.getPostalCode() + " Landmark: " +
			 * shippingAddress.getLandmark();
			 */

			String districtName = shippingAddress.getDistrictMaster() == null ? ""
					: shippingAddress.getDistrictMaster().getName();
			String stateName = shippingAddress.getStateMaster() == null ? ""
					: shippingAddress.getStateMaster().getName();

			StringBuilder sb = new StringBuilder();

			sb.append(shippingAddress.getAddressLineOne());
			sb.append(shippingAddressTwo);
			sb.append(shippingAddressThree);

			if (StringUtils.isNotEmpty(districtName)) {
				sb.append(", ");
				sb.append(districtName);
			}

			if (StringUtils.isNotEmpty(stateName)) {
				sb.append(", ");
				sb.append(stateName);
			}

			if (StringUtils.isNotEmpty(country)) {
				sb.append(", ");
				sb.append(country);
			}
			if (StringUtils.isNotEmpty(shippingAddress.getPostalCode())) {
				sb.append("Pin: ");
				sb.append(shippingAddress.getPostalCode());
			}
			if (StringUtils.isNotEmpty(shippingAddress.getLandmark())) {
				sb.append(" Landmark: ");
				sb.append(shippingAddress.getLandmark());
			}

			// String addressForTextArea = shippingAddress.getAddressLineOne() +
			// shippingAddressTwo + shippingAddressThree
			// + ", " + shippingAddress.getDistrictMaster().getName() + ", "
			// + shippingAddress.getStateMaster().getName() + ", " + country + " Pin: "
			// + shippingAddress.getPostalCode() + " Landmark: " +
			// shippingAddress.getLandmark();

			// shippingAddressTextArea = addressForTextArea;
			sb.append(".");
			shippingAddressTextArea = sb.toString();
			// prepareAddressText(customerMaster);
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('shippingaddresspanel').hide();");
		}
	}

	public void loadCustomerLazySList() {

		log.info("Lazy load == Customer Master  ==>>");
		customerResponseLazyList = new LazyDataModel<CustomerMasterSearchResponseDTO>() {

			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<CustomerMasterSearchResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				List<CustomerMasterSearchResponseDTO> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<CustomerMasterSearchResponseDTO>>() {
								});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(CustomerMasterSearchResponseDTO res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public CustomerMasterSearchResponseDTO getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<CustomerMasterSearchResponseDTO> responseList = (List<CustomerMasterSearchResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (CustomerMasterSearchResponseDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		customerSearchResponse = new CustomerMasterSearchResponseDTO();

		BaseDTO baseDTO = new BaseDTO();

		CustomerMasterSearchRequestDTO request = new CustomerMasterSearchRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("code")) {
				request.setCode(value);
			} else if (key.equals("name")) {
				request.setName(value);
			} else if (key.equals("email")) {
				request.setEmail(value);
			} else if (key.equals("contactNo")) {
				request.setContact(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equalsIgnoreCase("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {

			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/search";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Customer Master onRowSelect method started");
		customerSearchResponse = ((CustomerMasterSearchResponseDTO) event.getObject());
		addButtonFlag = false;
	}

	public boolean checkValidation() {
		if (customerMaster != null) {
			if (customerMaster.getBusinessTypeMasterToCreate() == null
					|| customerMaster.getBusinessTypeMasterToCreate().getId() == null) {
				log.info("<<=======   Business Type null");
				errorMap.notify(ErrorDescription.CUST_BUSINESS_TYPE_REQUIRED.getErrorCode());
				return false;
			}
			if (customerMaster.getCustomerTypeMasterToCreate() == null
					|| customerMaster.getCustomerTypeMasterToCreate().getId() == null) {
				log.info("<<=======   Customer Type null");
				errorMap.notify(ErrorDescription.CUST_CUSTOMER_TYPE_REQUIRED.getErrorCode());
				return false;
			}
			/*
			 * if (customerMaster.getCode() == null) { log.info("<<=======   Code null");
			 * errorMap.notify(ErrorDescription.CUST_CODE_REQUIRED.getErrorCode()); return
			 * false; }
			 */
			if (customerMaster.getName() == null) {
				log.info("<<=======   Name null");
				errorMap.notify(ErrorDescription.CUST_NAME_REQUIRED.getErrorCode());
				return false;
			}
			/*
			 * if (customerMaster.getGstNumber() == null) {
			 * log.info("<<=======   GST null");
			 * errorMap.notify(ErrorDescription.CUST_GSTIN_REQUIRED.getErrorCode()); return
			 * false; }
			 */
			if (customerMaster.getAddressMasterToCreate() == null) {
				log.info("<<=======   Address null");
				errorMap.notify(ErrorDescription.CUST_ADDRESS_REQUIRED.getErrorCode());
				return false;
			}
			/*
			 * if (customerMaster.getRegistrationDate() == null) {
			 * log.info("<<=======   registration null");
			 * errorMap.notify(ErrorDescription.CUST_REGISTRATION_DATE_REQUIRED.getErrorCode
			 * ()); return false; }
			 */
			if (customerMaster.getActiveStatus() == null) {
				log.info("<<=======  active status null");
				errorMap.notify(ErrorDescription.CUST_STATUS_REQUIRED.getErrorCode());
				return false;
			}
			/*
			 * if (customerMaster.getCreditAllowed() == null) {
			 * log.info("<<=======  active status null");
			 * errorMap.notify(ErrorDescription.CUST_CREDIT_ALLOW_REQUIRED.getErrorCode());
			 * return false; } if (customerMaster.getMaxInstallmentMonth() == null) {
			 * log.info("<<=======  install month null");
			 * errorMap.notify(ErrorDescription.CUST_INSTALLMENT_REQUIRED.getErrorCode());
			 * return false; } if (customerMaster.getCreditLimit() == null) {
			 * log.info("<<=======  credit Limit null");
			 * errorMap.notify(ErrorDescription.CUST_CREADIT_LIMIT_REQUIRED.getErrorCode());
			 * return false; }
			 * 
			 * if (customerMaster.getInterestAlloewd() == null) {
			 * log.info("<<=======  interest Allowed null");
			 * errorMap.notify(ErrorDescription.CUST_INTEREST_APPLICABLE_REQUIRED.
			 * getErrorCode()); return false; } else { if
			 * (customerMaster.getInterestAlloewd() == true) { if
			 * (customerMaster.getInterestPercentage() == null) {
			 * log.info("<<=======  credit Limit null");
			 * errorMap.notify(ErrorDescription.CUST_INTEREST_PERCENTAGE_REQUIRED.
			 * getErrorCode()); return false; } } } if (customerMaster.getReturnAllowed() ==
			 * null) { log.info("<<=======  return Allowed null");
			 * errorMap.notify(ErrorDescription.CUST_RETURN_ALLOW_REQUIRED.getErrorCode());
			 * return false; }
			 */
			/*
			 * if (customerMaster.getBankBranchMaster() == null) {
			 * log.info("<<=======  credit Limit null");
			 * errorMap.notify(ErrorDescription.CUST_BRANCH_NAME_REQUIRED.getErrorCode());
			 * return false; }
			 */

			/*
			 * if (customerMaster.getAccountNumber() == null) {
			 * log.info("<<=======  credit Limit null");
			 * errorMap.notify(ErrorDescription.CUST_ACCOUNT_NUM_REQUIRED.getErrorCode());
			 * return false; }
			 */
			/*
			 * if (customerMaster.getPrimaryContactName() == null ||
			 * customerMaster.getPrimaryContactName().isEmpty()) {
			 * log.info("<<=======  primary contact name null");
			 * errorMap.notify(ErrorDescription.CUST_CONTACT_PERSON_REQUIRED.getErrorCode())
			 * ; return false; } if (customerMaster.getPrimaryContactNumber() == null ||
			 * customerMaster.getPrimaryContactNumber().isEmpty()) {
			 * log.info("<<======= primary contact number null");
			 * errorMap.notify(ErrorDescription.CUST_CONTACT_NUMBER_REQUIRED.getErrorCode())
			 * ; return false; } if (customerMaster.getPrimaryEmail() == null ||
			 * customerMaster.getPrimaryEmail().isEmpty()) {
			 * log.info("<<=======  primary email null");
			 * errorMap.notify(ErrorDescription.CUST_EMAIL_REQUIRED.getErrorCode()); return
			 * false; }
			 */

			if (StringUtils.isNotEmpty(customerMaster.getPrimaryEmail().trim())) {
				if (!validateEmail(customerMaster.getPrimaryEmail())) {
					AppUtil.addError("Invalid Primary Email ID");
					return false;
				}
			}

			if (StringUtils.isNotEmpty(customerMaster.getSecondaryEmail().trim())) {
				if (!validateEmail(customerMaster.getSecondaryEmail())) {
					AppUtil.addError("Invalid Secondary Email ID");
					return false;
				}
			}

			if (customerMaster.getBillingAddressMaster() == null) {
				log.info("<<=======  Billing master null");
				errorMap.notify(ErrorDescription.CUST_BILLING_ADDRESS_REQUIRED.getErrorCode());
				return false;
			}
			if (customerMaster.getShippingAddressMaster() == null) {
				log.info("<<=======  shipping master null");
				errorMap.notify(ErrorDescription.CUST_SHIPPING_ADDRESS_REQUIRED.getErrorCode());
				return false;
			}
			if (officeAddressTextArea != null && customerAddress.getCountryMaster() != null
					&& customerAddress.getCountryMaster().getName() == null) {
				log.info("<<=======  Address country null");
				AppUtil.addError("Address country is not empty");
				return false;
			}
			if (billingAddressTextArea != null && billingAddress.getCountryMaster() != null
					&& billingAddress.getCountryMaster().getName() == null) {
				log.info("<<=======  Billing address country null");
				AppUtil.addError("Billing address country is not empty");
				return false;
			}
			if (shippingAddressTextArea != null && shippingAddress.getCountryMaster() != null
					&& shippingAddress.getCountryMaster().getName() == null) {
				log.info("<<=======  shipping address country null");
				AppUtil.addError("Shipping address country is not empty");
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	public boolean validateEmail(final String hex) {
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}

	public List<CustomerMaster> customerParentAutocomplete(String query) {
		log.info("Institute Autocomplete query==>" + query);

		try {

			String url = serverURL + appPreference.getOperationApiUrl() + "/customerMaster/parents/autoSearch/" + query;
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			customerParentAutoSearchList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
			});

		} catch (Exception e) {
			log.error("<<======  error in load parent customer =======>>>>>");
		}
		return customerParentAutoSearchList;
	}

	public void selectAutoSearch() {
		log.info("<<=== inside selectAutoSearch");
		if (customerParentAutoSearchList.contains(selectedAutoCustomerParent)) {
			log.info("<<=== selected customer", selectedAutoCustomerParent.getId());
			customerMaster.setParentCustomerId(selectedAutoCustomerParent.getId());
		}
	}

	public void showDeletePage() {
		action = "DELETE";
		if (customerSearchResponse == null) {
			log.info("<<=====   please slect a group =====>>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		} else {

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmDepartmentDelete').show();");
		}
	}

	public void resetinterestPercentage() {
		log.info("<<<<<<<<:::::::::resetinterestPercentage:::::::::::>>>>>>>>");
		customerMaster.setInterestPercentage(null);
		RequestContext.getCurrentInstance().update("interestpercentage");
	}

	public void billAddressCheckBox() {
		if (billingCheckBox) {
			String addressTextArea = prepareAddress(customerAddress);
			customerMaster.setBillingAddressMaster(customerAddress);
			billingAddressTextArea = addressTextArea;
		} else {
			if(null != billingAddress){
				String addressTextArea = prepareAddress(billingAddress);
				customerMaster.setBillingAddressMaster(billingAddress);
				billingAddressTextArea = addressTextArea;
			}else{
			billingAddressTextArea = null;
			}
		}
	}

	public void shippAddressCheckBox() {
		if (shippingCheckBox) {
			String addressTextArea = prepareAddress(customerAddress);
			customerMaster.setShippingAddressMaster(customerAddress);
			shippingAddressTextArea = addressTextArea;
		}else {
			if(null != shippingAddress){
				String addressTextArea = prepareAddress(shippingAddress);
				customerMaster.setBillingAddressMaster(shippingAddress);
				shippingAddressTextArea = addressTextArea;
			}else{
				shippingAddressTextArea = "";
			}
		}
	}

	private String prepareAddress(AddressMaster customerAddress) {
		String country = "";
		String addressForTextArea = "";
		String districtName = "";
		String stateName = "";
		String billAddressTwo = "";
		String billAddressThree = "";
		try {
			if (customerAddress != null && customerAddress.getCountryMaster() != null
					&& customerAddress.getCountryMaster().getName() != null) {
				country = customerAddress.getCountryMaster().getName();
			}
			if (customerAddress.getAddressLineTwo() != null && !customerAddress.getAddressLineTwo().isEmpty()) {
				billAddressTwo = ", " + customerAddress.getAddressLineTwo();
			}
			if (customerAddress.getAddressLineThree() != null && !customerAddress.getAddressLineThree().isEmpty()) {
				billAddressThree = ", " + customerAddress.getAddressLineThree();
			}

			districtName = customerAddress.getDistrictMaster() != null ? customerAddress.getDistrictMaster().getName()
					: "";
			stateName = customerAddress.getStateMaster() != null ? customerAddress.getStateMaster().getName() : "";

			StringBuilder sb = new StringBuilder();

			sb.append(customerAddress.getAddressLineOne());
			sb.append(billAddressTwo);
			sb.append(billAddressThree);

			if (StringUtils.isNotEmpty(districtName)) {
				sb.append(", ");
				sb.append(districtName);
			}

			if (StringUtils.isNotEmpty(stateName)) {
				sb.append(", ");
				sb.append(stateName);
			}

			if (StringUtils.isNotEmpty(country)) {
				sb.append(", ");
				sb.append(country);
			}
			if (StringUtils.isNotEmpty(customerAddress.getPostalCode())) {
				sb.append(" Pin: ");
				sb.append(customerAddress.getPostalCode());
			}
			if (StringUtils.isNotEmpty(customerAddress.getLandmark())) {
				sb.append(" Landmark: ");
				sb.append(customerAddress.getLandmark());
			}
			sb.append(".");
			addressForTextArea = sb.toString();
		} catch (Exception e) {
			log.error("Exception at prepareAddress() ", e);
		}
		return addressForTextArea;
	}

}
