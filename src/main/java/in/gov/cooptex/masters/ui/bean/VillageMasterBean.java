package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.VillageMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.VillageMasterErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("villageMasterBean")
@Scope("session")
@Log4j2
public class VillageMasterBean {

	final String LIST_PAGE = "/pages/masters/village/listVillageMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/village/createVillageMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	// final String VILLAGE_MASTER_URL = AppUtil.getPortalServerURL() +
	// "/villagemaster";

	final String VILLAGE_MASTER_URL = AppUtil.getPortalServerURL() + "/village";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	VillageMaster villageMaster, selectedVillageMaster;

	@Getter
	@Setter
	LazyDataModel<VillageMaster> lazyVillageMasterList;

	List<VillageMaster> villageMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showVillageMasterListPage() {
		log.info("<==== Starts VillageMasterBean.showVillageMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		villageMaster = new VillageMaster();
		selectedVillageMaster = new VillageMaster();
		loadLazyVillageMasterList();
		log.info("<==== Ends VillageMasterBean.showVillageMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyVillageMasterList() {
		log.info("<===== Starts VillageMasterBean.loadLazyVillageMasterList ======>");

		lazyVillageMasterList = new LazyDataModel<VillageMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<VillageMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = VILLAGE_MASTER_URL + "/getallvillagemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						villageMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VillageMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return villageMasterList;
			}

			@Override
			public Object getRowKey(VillageMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VillageMaster getRowData(String rowKey) {
				for (VillageMaster villageMaster : villageMasterList) {
					if (villageMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedVillageMaster = villageMaster;
						return villageMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends VillageMasterBean.loadLazyVillageMasterList ======>");
	}

	public String showVillageMasterListPageAction() {
		log.info("<===== Starts VillageMasterBean.showVillageMasterListPageAction ===========>" + action);
		log.info("<===== selectedVillageMaster ===========>" + selectedVillageMaster);
		try {
			if (!action.equalsIgnoreCase("Create")
					&& (selectedVillageMaster == null || selectedVillageMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(VillageMasterErrorCode.SELECT_VILLAGE_MASTER).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create villageMaster called..");
				villageMaster = new VillageMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit villageMaster called..");
				String url = VILLAGE_MASTER_URL + "/getbyid/" + selectedVillageMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					villageMaster = mapper.readValue(jsonResponse, new TypeReference<VillageMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete villageMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVillageMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showVillageMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends VillageMasterBean.showVillageMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts VillageMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(VILLAGE_MASTER_URL + "/deletebyid/" + selectedVillageMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVillageMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete villageMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends VillageMasterBean.deleteConfirm ===========>");
		return showVillageMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts VillageMasterBean.saveOrUpdate =======>");
		try {
			if (villageMaster.getCode() == null || villageMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_CODE_EMPTY).getCode());
				return null;
			}
			if (villageMaster.getName() == null || villageMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_NAME_EMPTY).getCode());
				return null;
			}
			if (villageMaster.getName().trim().length() < 3 || villageMaster.getName().trim().length() > 75) {
				errorMap.notify(
						ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (villageMaster.getLocalName() == null || villageMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_LNAME_EMPTY).getCode());
				return null;
			}
			if (villageMaster.getLocalName().trim().length() < 3 || villageMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(
						ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_LNAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (villageMaster.getActiveStatus() == null) {
				errorMap.notify(
						ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			String url = VILLAGE_MASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, villageMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("villageMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(
							ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(
							ErrorDescription.getError(VillageMasterErrorCode.VILLAGE_MASTER_UPDATE_SUCCESS).getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends VillageMasterBean.saveOrUpdate =======>");
		return showVillageMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts VillageMasterBean.onRowSelect ========>" + event);
		selectedVillageMaster = ((VillageMaster) event.getObject());
		log.info("<=== selectedVillageMaster ========> " + selectedVillageMaster);
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends VillageMasterBean.onRowSelect ========>");
	}

}
