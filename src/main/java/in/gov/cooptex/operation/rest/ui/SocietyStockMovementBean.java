package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DeliveryChallanDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.OrderTypeEnum;
import in.gov.cooptex.core.enums.TaxTypes;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.ProductVarietyPrice;
import in.gov.cooptex.core.model.SocietyProductAppraisal;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.intend.model.StockItemInwardPNSDTO;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("societystockmovementbean")
@Scope("session")
public class SocietyStockMovementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/weaversModule/listSocietyStockMovement.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/weaversModule/createSocietyStockMovement.xhtml?faces-redirect=true;";

	private final String PREVIEW_SOCIETY_STOCK_MOVEMENT_URL = "/pages/weaversModule/previewSocietyStockMovement.xhtml?faces-redirect=true;";

	private final String DELIVERY_CHALLAN_ACKNOWLEDGE = "/pages/weaversModule/deliveryChellanAcknowledgement.xhtml?faces-redirect=true;";

	private final String STOCK_TRANSFER_SALES_INVOICE_ACKNOWLEDGE = "/pages/weaversModule/stockTransferSalesInvoiceAck.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Setter
	String action;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	Integer stockSize;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;
	@Getter
	@Setter
	StockTransfer stockTransfer = new StockTransfer();

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	SupplierMaster loginSupplierMaster;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	Double deliverychallanTotalAmount = 0.0;

	@Getter
	@Setter
	List<DeliveryChallanDTO> transferItemsList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> hsnSummaryList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> productCodeSummaryList;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean atNumberRequired = false;

	@Getter
	@Setter
	Boolean taxApplicableFlag = true;

	@Getter
	@Setter
	Boolean invoiceTaxApplicableFlag = true;

	@Getter
	@Setter
	String pageaction;

	@PostConstruct
	private void init() {
		log.info("init is called.");
		societyStockMovementListPage();

		List<String> taxCodeList = new ArrayList<>();
		Field[] interfaceFields = TaxTypes.class.getFields();
		for (Field taxCode : interfaceFields) {
			taxCodeList.add(taxCode.getName());
		}
		taxCodeListSize = taxCodeList == null ? 0 : taxCodeList.size();
	}

	public String societyStockMovementListPage() {
		log.info("societyStockMovementListPage is called.");
		// SERVER_URL = stockTransferUtility.loadServerUrl();
		taxApplicableFlag = true;
		addButtonFlag = false;
		pageaction = "";
		stockTransferResponse = new StockTransferResponse();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		// stockTransferResponse = new StockTransferResponse();
		// addButtonFlag = false;

		loadLazyList();

		SupplierMaster loginSuppleirMaster = getsupplierByuser();
		if (loginSuppleirMaster != null) {
			Boolean taxExemptedFlag = loginSuppleirMaster.getTaxExempted();
			taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
			if (taxExemptedFlag) {
				taxApplicableFlag = false;
			} else {
				taxApplicableFlag = true;
			}
		} else {
			taxApplicableFlag = true;
		}

		return LIST_PAGE_URL;
	}

	// List Page Method
	private void loadLazyList() {
		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();
				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}
		};
	}

	private BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/search";
		baseDTO = httpService.post(URL, request);
		log.info("SocietyStockMovementBean.getSearchData Method Complted");

		return baseDTO;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("dnpCodeOrName")) {
				request.setDnpCodeOrName(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}

			if (entry.getKey().equals("invoiceNumber")) {
				request.setInvoiceNumber(value);
			}

			if (entry.getKey().equals("totalQty")) {
				request.setTotalQty(value);
			}

			if (entry.getKey().equals("totalAmount")) {
				request.setTotalAmount(value);
			}
		}

		return request;
	}

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster selectedproductWareHouse;

	public void loadActiveProductwarehouseList() {
		log.info("loadActiveProductwarehouseList method start");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societystockmovement/loadActiveProductwarehouseList";

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			} else {
				log.warn("loadActiveProductwarehouseList not success error in service side *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadActiveProductwarehouseList ", ee);
		}

	}

	@Getter
	@Setter
	String purchaseOrderNumber;

	@Getter
	@Setter
	Integer invoiceNumber;

	@Getter
	@Setter
	Date invoiceDate;

	@Getter
	@Setter
	String orderType;

	@Getter
	@Setter
	List<String> orderTypeList = new ArrayList<>();

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	String hsnCode;

	@Getter
	@Setter
	String uom;

	@Getter
	@Setter
	ProductVarietyPrice productVarietyPrice;

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;
	@Getter
	@Setter
	List<Object> payToPayList;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	private int taxCodeListSize;

	public String addSocietystockmovement() {
		log.info("addSocietystockmovement method start");
		clear();
		pageaction = "ADD";
		taxApplicableFlag = true;
		supplierMaster = getsupplierByuser();
		if (supplierMaster == null || supplierMaster.getId() == null) {
			log.info("loggedIn user not a societyUser");
			AppUtil.addError("Available only for the society user");
			return null;
		} else {
			log.info("loggedIn user is societyUser");

			Boolean taxExemptedFlag = supplierMaster.getTaxExempted();
			taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
			if (taxExemptedFlag) {
				taxApplicableFlag = false;
			} else {
				taxApplicableFlag = true;
			}
		}

		String value = commonDataService
				.getAppKeyValueforPortal(AppConfigEnum.SOCIETY_INVOICE_CHECK_FOR_SHIPPING.toString());
		if (value != null) {
			boolean shipping = Boolean.parseBoolean(value);
			if (shipping) {
				loadActiveProductwarehouseListBySupplier(supplierMaster.getId());
			} else {
				loadActiveProductwarehouseList();
			}
		} else {
			loadActiveProductwarehouseList();
		}

		loadTransportTypeMasterList();
		loadOrderTypeList();
		orderType = OrderTypeEnum.GEN.name();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
		transportTypeMasterList = commonDataService.getAllTransportTypes();

		selectedTransportTypeMaster = new TransportTypeMaster();
		stockTransferItemsMovementList = new ArrayList<>();

		log.info("addSocietystockmovement method End");
		return ADD_PAGE_URL;
	}

	public void loadActiveProductwarehouseListBySupplier(Long supplierid) {
		log.info("loadActiveProductwarehouseList method start");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societystockmovement/loadActiveProductwarehouseListBySupplier/" + supplierid;

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			} else {
				log.warn("loadActiveProductwarehouseList not success error in service side *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadActiveProductwarehouseList ", ee);
		}

	}

	public void loadOrderTypeList() {
		log.info("loadOrderTypeList method start");
		orderTypeList = new ArrayList<>();
		orderTypeList.add(OrderTypeEnum.FDS.name());
		orderTypeList.add(OrderTypeEnum.GEN.name());
		orderTypeList.add(OrderTypeEnum.NMP.name());
		orderTypeList.add(OrderTypeEnum.OAP.name());
		log.info("loadOrderTypeList method ends");
	}

	public void clear() {
		selectedproductWareHouse = new EntityMaster();
		purchaseOrderNumber = "";
		invoiceNumber = null;
		invoiceDate = null;
		orderType = null;
		hsnCode = "";
		uom = "";
		selectedProductVarietyMaster = new ProductVarietyMaster();
		societyProductAppraisal = new SocietyProductAppraisal();
		societyProductAppraisalList = new ArrayList<>();
		stockTransfer = new StockTransfer();
		stockTransferItems = new StockTransferItems();
		supplierMaster = new SupplierMaster();
		pageaction = "";
	}

	// product category -> 4
	public List<ProductVarietyMaster> productVarietyAutoComplete(String varietyCodeOrName) {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			BaseDTO baseDTO = null;
			String value = commonDataService
					.getAppKeyValueforPortal(AppConfigEnum.SOCIETY_INVOICE_CHECK_FOR_SHIPPING.toString());
			if (value != null) {
				boolean shipping = Boolean.parseBoolean(value);
				if (shipping) {
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/societystockmovement/getAllProductVarietyMasterListBySupplier/" + supplierMaster.getId()
							+ "/" + selectedproductWareHouse.getId() + "/" + varietyCodeOrName;
					baseDTO = httpService.get(url);
					log.info("::: Retrieved productVarietyMasterList :");
					if (baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						productVarietyMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarietyMaster>>() {
								});
					} else {
						log.warn("productVarietyMasterList Error *** :");
					}
				} else {
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/societystockmovement/getAllProductVarietyMasterList/" + varietyCodeOrName;
					baseDTO = httpService.get(url);
					log.info("::: Retrieved productVarietyMasterList :");
					if (baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						productVarietyMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarietyMaster>>() {
								});
					} else {
						log.warn("productVarietyMasterList Error *** :");
					}
				}
			}

		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
		return productVarietyMasterList;
	}

	public void productVarietyUpdate() {
		try {
			log.info("productvarity master id-----------" + selectedProductVarietyMaster.getId());
			// collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			stockTransferItems.setOrderedQty(null);
			stockTransferItems.setAtNumber(null);
			String uomCode = null;
			societyProductAppraisal = new SocietyProductAppraisal();
			RequestContext.getCurrentInstance().update("customerPanel");
			hsnCode = selectedProductVarietyMaster.getHsnCode();
			if (selectedProductVarietyMaster.getUomMaster() != null) {
				uom = selectedProductVarietyMaster.getUomMaster().getName();
				uomCode = selectedProductVarietyMaster.getUomMaster().getCode();
			}

			if (selectedProductVarietyMaster.getProductCategoryCode().equalsIgnoreCase("AJ")
					|| selectedProductVarietyMaster.getProductCategoryCode().equalsIgnoreCase("A")) {
				if (!"METR".equals(uomCode)) {
					stockTransferItems.setOrderedQty(1D);
				}
				atNumberRequired = true;
			} else {
				atNumberRequired = false;
			}
			// BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL +
			// "/getall/productvarietyRate/"
			// + selectedProductVarietyMaster.getId());

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/productvarietyRate/"
					+ selectedProductVarietyMaster.getId();
			log.info("::: productVarietyUpdate :::");
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productVarietyPrice = mapper.readValue(jsonResponse, new TypeReference<ProductVarietyPrice>() {
				});

				if (productVarietyPrice != null) {
					uom = productVarietyPrice.getProductVarietyMaster().getUomMaster().getName();
					log.info("uom==> " + uom);
				}

				String jsonResponseDesing = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponseDesing,
						new TypeReference<List<ProductDesignMaster>>() {
						});

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

			log.info("Produect Variety Update Get Success :");
		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	@Getter
	@Setter
	SocietyProductAppraisal societyProductAppraisal = new SocietyProductAppraisal();

	@Getter
	@Setter
	List<SocietyProductAppraisal> societyProductAppraisalList;

	Long index = 0l;

	public void loadProductVerietyTaxPercentage() {
		log.info("loadProductVerietyTaxPercentage :: start");
		try {

			if (societyProductAppraisal != null) {
				if (societyProductAppraisal.getProductCost() != null && stockTransferItems.getUnitRate() != null) {
					if (!societyProductAppraisal.getProductCost().equals(stockTransferItems.getUnitRate())) {
						stockTransferItems.setUnitRate(null);
						AppUtil.addError(" Product Cost And Unit Rate In Item Receive Details Should be Same");
						return;
					}
				}
			}

			Double totalTax = 0.0;
			if (selectedProductVarietyMaster.getId() == null) {
				// AppUtil.addError("Please select Product Variety Master");
				stockTransferItems.setUnitRate(null);
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
				return;
			}
			if (stockTransferItems.getUnitRate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.UNIT_RATE_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			Long ProductId = selectedProductVarietyMaster.getId();
			Double unitRate = stockTransferItems.getUnitRate();
			String atNumber = stockTransferItems.getAtNumber();

			log.info("loadProductVerietyTaxPercentage :: ProductId==> " + ProductId);
			log.info("loadProductVerietyTaxPercentage :: unitRate==> " + unitRate);
			log.info("loadProductVerietyTaxPercentage :: atNumber==> " + atNumber);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/gettaxlistbyproductidunitrate/" + ProductId + "/" + unitRate;
			log.info("loadProductVerietyTaxPercentage :: url==> " + url);
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				List<ProductVarietyTax> proTaxList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyTax>>() {
						});
				if (proTaxList != null && !proTaxList.isEmpty()) {
					log.info("proTaxList size==> " + proTaxList.size());
					for (ProductVarietyTax proTax : proTaxList) {
						log.info("tax==> " + proTax.getTaxPercent());
						if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("SGST")) {
							stockTransferItems.setSgstTaxPercent(proTax.getTaxPercent());
						} else if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("CGST")) {
							stockTransferItems.setCgstTaxPercent(proTax.getTaxPercent());
						}

						totalTax = totalTax + proTax.getTaxPercent();

					}
				} else {
					totalTax = 0.0;
					log.error("proTaxList is null or empty");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
				}
				log.info("totalTax==> " + totalTax);
				stockTransferItems.setTaxPercent(totalTax);
			} else {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
			}

		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public String handleKeyEvent() {
		log.info("minimum 00   to  maximum  100   ==>" + stockTransferItems.getTaxPercent());

		if (stockTransferItems.getTaxPercent() > 100) {
			AppUtil.addWarn(" Allow for Tax field value minimum 0 maximum 100");
			stockTransferItems.setTaxPercent(null);
		}
		return null;
	}

	public void clearItemTable() {
		selectedProductVarietyMaster = new ProductVarietyMaster();
		stockTransferItems = new StockTransferItems();
		societyProductAppraisal = new SocietyProductAppraisal();
		uom = null;
		hsnCode = null;
		RequestContext.getCurrentInstance().update("customerPanel");
	}

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsMovementList;

	@Getter
	@Setter
	int countAtNumberGenerator = 1;

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	// add Item Details
	public void addStockItems() {
		log.info("itemCollectionAddNew Inward received :");
		try {

			if (selectedProductVarietyMaster == null || selectedProductVarietyMaster.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_SELECT.getErrorCode());
				return;
			}

			String categoryCode = selectedProductVarietyMaster != null
					? selectedProductVarietyMaster.getProductCategoryCode()
					: null;
			log.info("itemCollectionAddNew() categoryCode: " + categoryCode);

			if ("A".equals(categoryCode) || "AJ".equals(categoryCode)) {
				if (StringUtils.isEmpty(stockTransferItems.getAtNumber())) {
					errorMap.notify(ErrorDescription.ECOMM_PRODUCT_CONFIG_AT_NUMBER_REQ.getErrorCode());
					return;
				}
			}

			if (stockTransferItems.getAtNumber() != null && !stockTransferItems.getAtNumber().isEmpty()
					&& (stockTransferItemsMovementList.stream().filter(sti -> sti.getAtNumber() != null)
							.anyMatch(s -> s.getAtNumber().equals(stockTransferItems.getAtNumber())))) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getErrorCode());
				return;
			}

			if (StringUtils.isNotEmpty(stockTransferItems.getAtNumber()) && supplierMaster != null) {
				BaseDTO baseDTO = checkATNumberDuplicateBasedFinYear(stockTransferItems.getAtNumber(),
						supplierMaster.getId());
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() != null && baseDTO.getStatusCode()
							.longValue() == ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getErrorCode()) {
						stockTransferItems.setAtNumber(null);
						errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getErrorCode());
						return;
					}
				}
			}

			if (stockTransferItems.getOrderedQty() == null || stockTransferItems.getOrderedQty() <= 0) {
				log.info("Invalid Item Datails quantity:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_QUANTITY.getErrorCode());
				return;
			}
			if (stockTransferItems.getUnitRate() == null || stockTransferItems.getUnitRate() <= 0) {
				log.info("Invalid Item Datails unitRate:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_UNIT.getErrorCode());
				return;
			}

			if (stockTransferItems.getDiscountPercent() != null && stockTransferItems.getDiscountPercent() < 0) {
				log.info("Invalid Item Datails discount :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_DISCOUNT.getErrorCode());
				return;
			}
			if (stockTransferItems.getTaxPercent() != null && stockTransferItems.getTaxPercent() < 0) {
				log.info("Invalid Item Datails tax:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_TAX.getErrorCode());
				return;
			}
			if (selectedProductVarietyMaster == null || selectedProductVarietyMaster.getCode() == null) {
				log.info("Invalid ProductVarietyMaster Selection");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_SELECT.getErrorCode());
				return;
			}
			if (societyProductAppraisal != null && societyProductAppraisal.getProductCost() != null) {
				if (doubleFormat(societyProductAppraisal.getProductCost()) != doubleFormat(
						stockTransferItems.getUnitRate())) {
					log.info("Invalid ProductVarietyMaster Selection");
					stockTransferItems.setUnitRate(null);
					AppUtil.addWarn("Generated Product Cost Should be Same to the unit rate");
					return;
				}

			}
			/*
			 * if(stockTransferItemsInwardUpdateList != null &&
			 * !stockTransferItemsInwardUpdateList.isEmpty() &&
			 * stockTransferItems.getAtNumber()!=null &&
			 * !stockTransferItems.getAtNumber().isEmpty()) { boolean atNumberExists =
			 * stockTransferItemsInwardUpdateList.stream() .filter(st -> st.getAtNumber() !=
			 * null) .anyMatch(sti ->
			 * sti.getAtNumber().equalsIgnoreCase(stockTransferItems.getAtNumber()));
			 * if(atNumberExists) { AppUtil.addError("AT Number already exists"); return; }
			 * }
			 */
			StockTransferItems stockTransferItemsObj = new StockTransferItems();
			stockTransferItemsObj.setProductVarietyMaster(selectedProductVarietyMaster);

			if (selectedProductVarietyMaster.getUomMaster() != null) {
				stockTransferItemsObj.setUomMaster(selectedProductVarietyMaster.getUomMaster());
			} else {
				Util.addWarn("UOM is Empty");
			}
			stockTransferItemsObj.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
			stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemTotal());
			stockTransferItemsObj.setDispatchedQty((double) stockTransferItems.getOrderedQty());
			stockTransferItemsObj.setUnitRate(stockTransferItems.getUnitRate());
			stockTransferItemsObj.setPurchasePrice(stockTransferItems.getUnitRate());
			// stockTransferItems.setDescription(description);
			double total = stockTransferItemsObj.getItemTotal();
			if (stockTransferItems.getDiscountPercent() != null) {
				if (stockTransferItems.getDiscountPercent() > 0) {
					double discountV = (total * stockTransferItems.getDiscountPercent()) / 100;
					stockTransferItemsObj.setDiscountValue(discountV);
					stockTransferItemsObj.setItemNetTotal(
							stockTransferItems.getItemNetTotal() - stockTransferItems.getDiscountValue());
				}
			}

			if (stockTransferItems.getTaxPercent() != null) {
				if (stockTransferItems.getTaxPercent() > 0) {

					Double cgstPercentage = stockTransferItems.getCgstTaxPercent();
					cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
					Double cgstValue = total * (cgstPercentage / 100);
					cgstValue = cgstValue == null ? 0D : cgstValue;
					/**
					 * Don't round off. Trim after two decimal.
					 */
					cgstValue = AppUtil.getTrimmedValue(cgstValue);

					Double sgstPercentage = stockTransferItems.getSgstTaxPercent();
					sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
					Double sgstValue = total * (sgstPercentage / 100);
					sgstValue = sgstValue == null ? 0D : sgstValue;
					/**
					 * Don't round off. Trim after two decimal.
					 */
					sgstValue = AppUtil.getTrimmedValue(sgstValue);

					Double totalTax = Double.sum(cgstValue, sgstValue);
					stockTransferItemsObj.setTaxValue(totalTax);

					stockTransferItemsObj.setTaxPercent(stockTransferItems.getTaxPercent());
					stockTransferItemsObj.setCgstTaxPercent(cgstPercentage);
					stockTransferItemsObj.setSgstTaxPercent(sgstPercentage);
					stockTransferItemsObj.setItemNetTotal(
							stockTransferItemsObj.getItemNetTotal() + stockTransferItemsObj.getTaxValue());
				}
			}
			if (societyProductAppraisal != null && societyProductAppraisal.getWeaversName() != null
					&& supplierMaster != null) {
				stockTransferItemsObj.setAtNumber(stockTransferItems.getAtNumber());
				societyProductAppraisal.setAtNumber(stockTransferItems.getAtNumber());
				stockTransferItemsObj.setProductVarietyMaster(selectedProductVarietyMaster);
				societyProductAppraisalList.add(societyProductAppraisal);
			}
			societyProductAppraisal = new SocietyProductAppraisal();
			index++;
			stockTransferItemsObj.setId(index);
			stockTransferItemsMovementList.add(stockTransferItemsObj);
			log.info("Item Collection add Success :");
			stockTransferItems = new StockTransferItems();

			if (selectedProductVarietyMaster != null
					&& (selectedProductVarietyMaster.getProductCategoryCode().equalsIgnoreCase("AJ")
							|| selectedProductVarietyMaster.getProductCategoryCode().equalsIgnoreCase("A"))) {
				stockTransferItems.setOrderedQty(1D);
			}

			// description=null;
			countAtNumberGenerator = countAtNumberGenerator + 1;
			clearItemTable();
		} catch (Exception e) {
			log.error("StockItemInward Collection Add Error :", e);
		}
	}

	public double doubleFormat(double amount) {
		DecimalFormatSymbols symbols = DecimalFormatSymbols.getInstance();
		symbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat("#.##", symbols);
		// df.setMaximumFractionDigits(2);
		return Double.valueOf(df.format(amount));
	}

	// Transport Service Type transportTypeMasterList
	public void loadTransportTypeMasterList() {

		log.info(":::<- Load TransportTypeMasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportTypeMasterList";
			log.info("::: TransportTypeMasterList Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get TransportTypeMasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportTypeMasterList = new ArrayList<TransportTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<TransportTypeMaster>>() {
						});
				log.info("TransportTypeMasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportTypeMasterList Page Convert Succes -->");
			} else {
				log.warn("TransportTypeMasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportTypeMasterList load ", ee);
		}
	}

	public void loadTransportByTranportType() {
		transportMasterList = null;
		// stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void validationTransportMaster() {
		// stockTransfer.setTransportMaster(null);
		if (stockTransfer != null && stockTransfer.getTransportMaster() != null
				&& stockTransfer.getTransportMaster().getCode().equalsIgnoreCase("PERDEV")) {
			stockTransfer.setWaybillAvailable(false);
			stockTransfer.setTransportChargeAvailable(false);
			stockTransfer.setWaybillNumber(null);
			stockTransfer.setTransportChargeType(null);
			stockTransfer.setTransportChargeAmount(null);

		} else {
			log.info("stockTransfer TransportMaster is null");
		}
	}

	public String previewSocietyStockMovementPage() {
		log.info("starts SocietyStockMovementBean.previewSocietyStockMovementPage");
		try {
			if (supplierMaster != null && invoiceNumber != null) {
				if (checkInvoiceNumber(supplierMaster, invoiceNumber)) {
					log.info("InvoiceNumber already exists----");
					AppUtil.addError("Invoice Number Already Exists");
					return null;
				}
			}
			if (stockTransferItemsMovementList == null || stockTransferItemsMovementList.size() == 0) {
				AppUtil.addError("Product wise Details List should not be empty");
				return null;
			}

		} catch (Exception e) {
			log.error("Exception Occured in previewSocietyStockMovementPage", e);
		}

		return PREVIEW_SOCIETY_STOCK_MOVEMENT_URL;
	}

	public String previewSocietyStockMovementBackbutton() {
		log.info("starts SocietyStockMovementBean.previewSocietyStockMovementPage");
		return ADD_PAGE_URL;
	}

	private boolean checkInvoiceNumber(SupplierMaster supplierMaster, Integer invoiceNumber) {
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/checkinvoicenumber/";
		PurchaseInvoice invoice = new PurchaseInvoice();
		invoice.setSupplierMaster(supplierMaster);
		invoice.setInvoiceNumber(invoiceNumber);
		BaseDTO baseDTO = httpService.post(url, invoice);
		if (baseDTO != null) {
			if (baseDTO.getStatusCode().equals(ErrorDescription.INVOICE_NUMBER_EMPTY.getErrorCode())) {
				return true;
			}
		}
		return false;
	}

	private BaseDTO checkATNumberDuplicateBasedFinYear(String atNumber, Long supplierId) {
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/checkatnumber/" + atNumber
				+ "/" + supplierId;

		BaseDTO baseDTO = httpService.get(url);
		return baseDTO;

	}

	private SupplierMaster getsupplierByuser() {
		SupplierMaster supplierMaster = null;
		try {
			String url = AppUtil.getPortalServerURL() + "/supplier/master/getSupplierByUserId/"
					+ loginBean.getUserMaster().getId();
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			supplierMaster = mapper.readValue(jsonResponse, new TypeReference<SupplierMaster>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return supplierMaster;
	}

	public String submit(String action) {
		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}
		return savesocietyOutwardItem();
	}

	private String savesocietyOutwardItem() {
		log.info("<-start savesocietyOutwardItem >");
		try {
			// start transport

			if (selectedTransportTypeMaster == null) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
				return null;
			}
			if (stockTransfer.getTransportMaster() == null) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
				return null;
			}
			if (stockTransfer.getWaybillAvailable() == null) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
				return null;
			}

			if (stockTransfer.getWaybillAvailable() == true && (stockTransfer.getWaybillNumber() == null
					|| stockTransfer.getWaybillNumber().trim().length() <= 0)) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
				return null;
			}

			if (stockTransfer.getTransportChargeAvailable() == null) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
				return null;
			}
			if (stockTransfer.getTransportChargeAvailable() == true
					&& (stockTransfer.getTransportChargeType() == null)) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
				return null;
			}

			if (stockTransfer.getTransportChargeAvailable() == true) {
				if (stockTransfer.getTransportChargeAmount() == null
						|| this.stockTransfer.getTransportChargeAmount() < 0) {
					errorMap.notify(
							ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
					return null;
				}
			}
			if (stockTransfer.getTransportChargeAvailable() == true
					&& (this.stockTransfer.getTransportChargeAmount() == 0)) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
				return null;
			}
			if (stockTransfer.getTransportChargeType() != null) {
				if (stockTransfer.getTransportChargeType().equals("Cash")) {
					this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
				} else {
					this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
				}
			}

			this.stockTransfer.setTransferType(StockTransferType.SOCIETY_OUTWARD);

			DecimalFormat df = new DecimalFormat("#0.000");

			if (stockTransferItemsMovementList != null) {
				stockTransfer.setStockTransferItemsList(new ArrayList<StockTransferItems>());
				List<StockTransferItems> stockTransferItemsListTemp = new ArrayList<StockTransferItems>();
				for (StockTransferItems items : stockTransferItemsMovementList) {
					StockTransferItems itemsObj = new StockTransferItems();
					Double totalDispatchedQty = items.getDispatchedQty() != null ? items.getDispatchedQty() : 0D;
					Double itemTotalTaxValue = items.getTaxValue() != null ? items.getTaxValue() : 0D;

					itemsObj = items;
					if (itemsObj.getProductVarietyMaster().getUomMaster().getCode().equals("METR")) {
						itemsObj.setItemAmount(items.getUnitRate() * items.getDispatchedQty());
						itemsObj.setItemTotal(itemsObj.getUnitRate() * itemsObj.getDispatchedQty());

						// double taxV = (itemsObj.getItemTotal() * items.getTaxPercent()) / 100;
						// itemsObj.setTaxValue(roundOffTaxValue(taxV));
						// itemsObj.setTaxValue(AppUtil.roundOffTaxValue(taxV));

						Double total = itemsObj.getItemTotal();
						total = total == null ? 0D : total;

						Double cgstPercentage = items.getCgstTaxPercent();
						cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
						Double cgstValue = total * (cgstPercentage / 100);
						cgstValue = cgstValue == null ? 0D : cgstValue;
						/**
						 * Don't round off. Trim after two decimal.
						 */
						cgstValue = AppUtil.getTrimmedValue(cgstValue);

						Double sgstPercentage = items.getSgstTaxPercent();
						sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
						Double sgstValue = total * (sgstPercentage / 100);
						sgstValue = sgstValue == null ? 0D : sgstValue;
						/**
						 * Don't round off. Trim after two decimal.
						 */
						sgstValue = AppUtil.getTrimmedValue(sgstValue);

						Double totalTax = Double.sum(cgstValue, sgstValue);
						itemsObj.setTaxValue(totalTax);

						// double taxValue = AppUtil.roundOffTaxValue(taxV);

						itemsObj.setItemNetTotal(itemsObj.getItemTotal() + itemsObj.getTaxValue());
						itemsObj.setAtNumber(items.getAtNumber());
						stockTransferItemsListTemp.add(itemsObj);
						// sequenceConfigService.generateNextSequenceByName(SequenceName.AT_NUMBER);
					} else {
						if (itemsObj.getAtNumber() == null || itemsObj.getAtNumber().equals("")) {
							int count = itemsObj.getDispatchedQty().intValue();

							for (int i = 1; i <= count; i++) {
								itemsObj.setItemAmount(itemsObj.getUnitRate());
								itemsObj.setDispatchedQty(1D);
								itemsObj.setItemTotal(itemsObj.getUnitRate() * itemsObj.getDispatchedQty());
								// double taxV = (itemsObj.getItemTotal() * items.getTaxPercent()) / 100;
								// itemsObj.setTaxValue(roundOffTaxValue(taxV));

								Double total = itemsObj.getItemTotal();
								total = total == null ? 0D : total;

								Double cgstPercentage = items.getCgstTaxPercent();
								cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
								Double cgstValue = total * (cgstPercentage / 100);
								cgstValue = cgstValue == null ? 0D : cgstValue;

								/**
								 * Don't round off. Trim after two decimal.
								 */
								cgstValue = AppUtil.getTrimmedValue(cgstValue);

								Double sgstPercentage = items.getSgstTaxPercent();
								sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
								Double sgstValue = total * (sgstPercentage / 100);
								sgstValue = sgstValue == null ? 0D : sgstValue;

								/**
								 * Don't round off. Trim after two decimal.
								 */
								sgstValue = AppUtil.getTrimmedValue(sgstValue);

								Double totalTax = Double.sum(cgstValue, sgstValue);
								itemsObj.setTaxValue(totalTax);

								// itemsObj.setTaxValue(perQtyTaxValue);
								itemsObj.setItemNetTotal(itemsObj.getItemTotal() + itemsObj.getTaxValue());
								stockTransferItemsListTemp.add(itemsObj);

							}
						} else {
							itemsObj.setItemAmount(itemsObj.getUnitRate() * itemsObj.getDispatchedQty());
							itemsObj.setItemTotal(itemsObj.getUnitRate() * itemsObj.getDispatchedQty());

							Double total = itemsObj.getItemTotal();
							total = total == null ? 0D : total;

							Double cgstPercentage = items.getCgstTaxPercent();
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;

							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = items.getSgstTaxPercent();
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;

							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							itemsObj.setTaxValue(totalTax);

							itemsObj.setItemNetTotal(itemsObj.getItemTotal() + itemsObj.getTaxValue());
							itemsObj.setAtNumber(itemsObj.getAtNumber());
							stockTransferItemsListTemp.add(itemsObj);

							// sequenceConfigService.generateNextSequenceByName(SequenceName.AT_NUMBER);
						}
					}
				}

				stockTransfer.setStockTransferItemsList(stockTransferItemsListTemp);
				// stockTransfer.setStockTransferItemsList(stockTransferItemsMovementList);
			} else {
				AppUtil.addError("Product wise Details List should not be empty");
				return null;
			}
			if (supplierMaster == null || supplierMaster.getId() == null) {
				log.info("loggedIn user not a societyUser");
				AppUtil.addError("available only for the society user");
				return null;
			} else {
				log.info("loggedIn user is societyUser");
			}

			StockItemInwardPNSDTO stockItemInwardPNSDTO = new StockItemInwardPNSDTO();

			stockTransfer.setSupplierMaster(supplierMaster);

			stockTransfer.setToEntityMaster(selectedproductWareHouse != null ? selectedproductWareHouse : null);

			stockItemInwardPNSDTO.setStockTransfer(stockTransfer);
			if (societyProductAppraisalList != null && societyProductAppraisalList.size() > 0) {
				stockItemInwardPNSDTO.setSocietyProductAppraisalList(societyProductAppraisalList);
			}
			stockItemInwardPNSDTO.setInvoiceDate(invoiceDate);
			stockItemInwardPNSDTO.setInvoiceNum(invoiceNumber);
			stockItemInwardPNSDTO.setPurchaseOrderNumber(purchaseOrderNumber);
			stockItemInwardPNSDTO.setOrderType(orderType);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societystockmovement/createsocietystockoutward";
			log.info("saveSateOutwardItem URL ==> " + url);
			BaseDTO baseDTO = httpService.post(url, stockItemInwardPNSDTO);
			log.info("Save Employee Family Details Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee Family Details Saved Succuessfully ");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INSERT_SUCCUSSFULLY.getErrorCode());
				return societyStockMovementListPage();
			} else {
				log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public Double getCgstValue() {
		Double cgstValue = 0D;
		try {
			if (stockTransferItemsMovementList != null && stockTransferItemsMovementList.size() > 0) {
				for (StockTransferItems stockTransferItemsObj : stockTransferItemsMovementList) {
					Double cgstPercentage = stockTransferItemsObj.getCgstTaxPercent();
					Double total = stockTransferItemsObj.getItemTotal();
					total = total == null ? 0D : total;
					log.info("getCgstValue() - total: " + total);
					cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
					Double cgstValueTemp = total * (cgstPercentage / 100);
					cgstValueTemp = cgstValueTemp == null ? 0D : cgstValueTemp;

					/**
					 * Don't round off. Trim after two decimal.
					 */
					cgstValueTemp = AppUtil.getTrimmedValue(cgstValueTemp);
					log.info("getSgstValue() - cgstValueTemp: " + cgstValueTemp);

					cgstValue = cgstValue + cgstValueTemp;
				}
				log.info("getCgstValue() - cgstValue: " + cgstValue);
			}
		} catch (Exception e) {
			log.error("Exception getCgstValue()", e);
		}
		return cgstValue;
	}

	public Double getSgstValue() {
		Double sgstValue = 0D;
		try {
			if (stockTransferItemsMovementList != null && stockTransferItemsMovementList.size() > 0) {
				for (StockTransferItems stockTransferItemsObj : stockTransferItemsMovementList) {
					Double sgstPercentage = stockTransferItemsObj.getSgstTaxPercent();
					Double total = stockTransferItemsObj.getItemTotal();
					total = total == null ? 0D : total;
					log.info("getSgstValue() - total: " + total);
					sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
					Double sgstValueTemp = total * (sgstPercentage / 100);
					sgstValueTemp = sgstValueTemp == null ? 0D : sgstValueTemp;

					/**
					 * Don't round off. Trim after two decimal.
					 */
					sgstValueTemp = AppUtil.getTrimmedValue(sgstValueTemp);
					log.info("getSgstValue() - sgstValueTemp: " + sgstValueTemp);

					sgstValue = sgstValue + sgstValueTemp;
				}
				log.info("getSgstValue() - sgstValue: " + sgstValue);
			}
		} catch (Exception e) {
			log.error("Exception at ", e);
		}
		return sgstValue;
	}

	/*
	 * private Integer generateInvoiceNumber() { Integer invoiceNumber=null; try {
	 * String url = SERVER_URL + appPreference.getOperationApiUrl() +
	 * "/societystockmovement/geninvoicenumbysupplierid/"+supplierMaster.getId();
	 * BaseDTO baseDTO = httpService.get(url); ObjectMapper mapper = new
	 * ObjectMapper();
	 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	 * String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); invoiceNumber =
	 * mapper.readValue(jsonResponse, new TypeReference<Integer>() { }); } catch
	 * (JsonProcessingException e) { log.error("JsonProcessingException ", e); }
	 * catch (Exception e) { log.error("Exception ", e); } return invoiceNumber; }
	 */

	public String viewSocietyStockMovement() {
		log.info("start viewSocietyStockMovement:");
		stockTransfer = new StockTransfer();
		selectedproductWareHouse = new EntityMaster();
		selectedTransportTypeMaster = new TransportTypeMaster();
		pageaction = "VIEW";
		invoiceTaxApplicableFlag = true;
		try {

			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			// stockTransfer =
			// commonDataService.getStockTransferById(stockTransferResponse.getId());
			stockTransfer = getStockTranferById(stockTransferResponse.getId());
			if (stockTransfer != null) {
				stockTransferItemsMovementList = new ArrayList<>();
				if (stockTransfer.getToEntityMaster() != null) {
					selectedproductWareHouse = stockTransfer.getToEntityMaster();
				}
				stockTransferResponse = new StockTransferResponse();
				invoiceNumber = Integer.parseInt(stockTransfer.getPurchaseInvoiceNumber());
				invoiceDate = stockTransfer.getInvoiceDate();

				Boolean invoiceTaxExemptedFlag = stockTransfer.getTaxExempted();
				invoiceTaxExemptedFlag = invoiceTaxExemptedFlag == null ? false : invoiceTaxExemptedFlag;
				if (invoiceTaxExemptedFlag) {
					invoiceTaxApplicableFlag = false;
				} else {
					invoiceTaxApplicableFlag = true;
				}

				orderType = stockTransfer.getOrderType();
				selectedTransportTypeMaster = stockTransfer.getTransportMaster() != null
						? stockTransfer.getTransportMaster().getTransportTypeMaster()
						: null;

				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {
						stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getPurchasePrice());
						StockTransferItems stockTransferItemsObj = new StockTransferItems();
						stockTransferItemsObj.setAtNumber(stockTransferItemslocalObj.getAtNumber());
						stockTransferItemsObj
								.setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster());

						if (stockTransferItemslocalObj.getUomMaster() != null) {
							stockTransferItemsObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							Util.addWarn("UOM is Empty");
						}
						stockTransferItemsObj.setItemTotal(stockTransferItemslocalObj.getUnitRate()
								* stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemTotal());
						stockTransferItemsObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemsObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
						stockTransferItemsObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItems.setDescription(description);
						double total = stockTransferItemsObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemsObj.setDiscountValue(discountV);
							stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {

							Double cgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							stockTransferItemsObj.setTaxValue(totalTax);

							// stockTransferItemsObj.setTaxValue(taxValue);
							stockTransferItemsObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							loadProductVerietyTaxPercentage();
							stockTransferItemsObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemsObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemsObj.setItemNetTotal(
									stockTransferItemsObj.getItemNetTotal() + stockTransferItemsObj.getTaxValue());
						}
						index++;
						stockTransferItemsObj.setId(index);
						stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return PREVIEW_SOCIETY_STOCK_MOVEMENT_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		addButtonFlag = true;
	}

	public String deleteConfirmation() {
		log.info("starts deleteConfirmation");
		try {
			pageaction = "DELETE";
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlg1').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("ends deleteConfirmation");
		return null;
	}

	public String deleteSocietyStockOutward() {
		log.info("starts deleteSocietyStockOutward");
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/delete/"
				+ stockTransferResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(URL);
			if (baseDTO != null && baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				addButtonFlag = false;
				log.info("ends deleteSocietyStockOutward");
				return societyStockMovementListPage();
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}
			RequestContext context = RequestContext.getCurrentInstance();
			// context.execute("PF('statusDialog').hide();");
			context.execute("PF('dlg1').hide();");
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		log.info("ends deleteSocietyStockOutward");
		return null;
	}

	public String createDeliveryChallan() {
		log.info("<==== Starts createDeliveryChallan ====>");
		try {
			deliverychallanTotalAmount = 0.0;
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				return null;
			}

			if (!stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
				// errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				AppUtil.addWarn("Delivery challan only for submitted records");
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

			transferItemsList = new ArrayList<>();

			if (stockTransfer != null) {

				Map<Long, List<StockTransferItems>> stockTransferItems = stockTransfer.getStockTransferItemsList()
						.stream().collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));

				if (stockTransferItems != null) {

					for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItems.entrySet()) {

						Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));

						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount
								.entrySet()) {

							DeliveryChallanDTO dto = new DeliveryChallanDTO();

							dto.setId(entrynew.getValue().get(0).getId());
							dto.setHsnCode(entrynew.getValue().get(0).getProductVarietyMaster().getHsnCode());
							dto.setItem(entrynew.getValue().get(0).getProductVarietyMaster().getCode());
							dto.setProductName(entrynew.getValue().get(0).getProductVarietyMaster().getName());
							dto.setUnitRate(entrynew.getValue().get(0).getUnitRate());
							Double Qty = entrynew.getValue().stream().mapToDouble(t -> t.getDispatchedQty()).sum();
							dto.setQuantity(Qty);
							// dto.setQuantity(entrynew.getValue().size());

							Double rate = entrynew.getValue().get(0).getUnitRate() * dto.getQuantity();
							dto.setProductRate(rate);

							DecimalFormat decim = new DecimalFormat("0.00");

							String price = decim.format(dto.getUnitRate());
							dto.setUnitPrice(price.split("\\.")[0]);
							dto.setUnitPaisa(price.split("\\.")[1]);

							String p = decim.format(rate);
							dto.setTotalPriceValue(rate);
							dto.setTotalPrice(p.split("\\.")[0]);
							dto.setTotalPaisa(p.split("\\.")[1]);
							deliverychallanTotalAmount = deliverychallanTotalAmount + (rate != null ? rate : 0.0);
							transferItemsList.add(dto);
						}
					}
					log.info("transferItemsList size is ::::::" + transferItemsList.size());
					Map<String, List<DeliveryChallanDTO>> hsnCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(r -> r.getHsnCode()));
					hsnSummaryList = new ArrayList<>();
					productCodeSummaryList = new ArrayList<>();
					if (hsnCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : hsnCodeWiseGroupingDTO.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setHsnCode(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							hsnSummaryList.add(deliveryChallanDTO);
						}
					}

					Map<String, List<DeliveryChallanDTO>> productCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(c -> c.getItem()));
					if (productCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : productCodeWiseGroupingDTO
								.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setItem(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							productCodeSummaryList.add(deliveryChallanDTO);
						}
					}
					transferItemsList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					hsnSummaryList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					productCodeSummaryList.sort((s1, s2) -> s1.getItem().compareTo(s2.getItem()));
				}

				return DELIVERY_CHALLAN_ACKNOWLEDGE;
			} else {
				log.info("Object is null");
			}
			log.info("<==== Ends createDeliveryChallan ====>");
			return null;
		} catch (Exception e) {
			// log.info("abstractList size is ::::::" + abstractList.size());
			log.error("Error in createDeliveryChallan Method", e);
		}
		return null;
	}

	@SuppressWarnings("unused")
	public String createStockTransferInvoice() {
		log.info("<==== Starts createStockTransferInvoice ====>");
		try {
			deliverychallanTotalAmount = 0.0;
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				return null;
			}

			if (!stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
				// errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				errorMap.notify(OperationErrorCode.TRANSFER_INVOICE_ONLY_FOR_SUBMITED_RECORDS);
				return null;
			}

			SupplierMaster loginSuppleirMaster = getsupplierByuser();
			if (loginSuppleirMaster != null) {
				Boolean taxExemptedFlag = loginSuppleirMaster.getTaxExempted();
				taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
				if (taxExemptedFlag) {
					taxApplicableFlag = false;
				} else {
					taxApplicableFlag = true;
				}
			} else {
				taxApplicableFlag = true;
			}

			/*
			 * if(stockTransferRequest.getSenderGstNumber()!=null &&
			 * stockTransferRequest.getReceiverGstNumber()!=null) {
			 * if(stockTransferRequest.getSenderGstNumber().equalsIgnoreCase(
			 * stockTransferRequest.getReceiverGstNumber())) { AppUtil.
			 * addWarn("Sender and Receiver Gst Number both is same so transfer invoice is not applicable"
			 * ); return null; } }
			 */
			/*
			 * if(stockTransferRequest.getSenderGstNumber()==null ||
			 * stockTransferRequest.getReceiverGstNumber()==null) {
			 * AppUtil.addWarn("Need Sender and Receiver Gst Number"); return null; }
			 */

			stockTransfer = commonDataService.getStockTransferDetailsByStockTransferId(stockTransferResponse.getId());

			if (stockTransfer.getToEntityMaster() != null) {
				EntityMaster entityMaster = commonDataService
						.findRegionByShowroomId(stockTransfer.getToEntityMaster().getId());
				stockTransfer.getToEntityMaster().setEntityMasterRegion(entityMaster);
			}
			transferItemsList = new ArrayList<>();

			if (stockTransfer != null) {

				Boolean invoiceTaxExemptedFlag = stockTransfer.getTaxExempted();
				invoiceTaxExemptedFlag =invoiceTaxExemptedFlag == null ? false : invoiceTaxExemptedFlag;
				if (invoiceTaxExemptedFlag) {
					taxApplicableFlag = false;
				} else {
					taxApplicableFlag = true;
				}

				Map<Long, List<StockTransferItems>> stockTransferItems = stockTransfer.getStockTransferItemsList()
						.stream().collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));

				if (stockTransferItems != null) {

					for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItems.entrySet()) {

						Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));

						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount
								.entrySet()) {

							DeliveryChallanDTO dto = new DeliveryChallanDTO();

							ProductVarietyMaster productVarietyMaster = entrynew.getValue().get(0)
									.getProductVarietyMaster();
							String categoryCode = productVarietyMaster.getProductCategoryCode();
							if ("A".equals(categoryCode) || "AJ".equals(categoryCode)) {
								dto.setAtNumber(entrynew.getValue().get(0).getAtNumber());
							} else {
								dto.setAtNumber("");
							}
							dto.setId(entrynew.getValue().get(0).getId());
							dto.setHsnCode(productVarietyMaster.getHsnCode());
							dto.setItem(productVarietyMaster.getCode());
							dto.setProductName(productVarietyMaster.getName());
							dto.setUnitRate(entrynew.getValue().get(0).getUnitRate());
							dto.setTaxableUnit(entrynew.getValue().get(0).getPurchasePrice());
							Double Qty = entrynew.getValue().stream().mapToDouble(t -> t.getDispatchedQty()).sum();
							dto.setQuantity(Qty);
							// dto.setQuantity(entrynew.getValue().size());
							dto.setGstQuantity(dto.getQuantity());

							DecimalFormat decim = new DecimalFormat("0.00");
							dto.setTaxableUnit(dto.getTaxableUnit() != null ? dto.getTaxableUnit() : 0D);

							Double unitPrice = dto.getUnitRate() * dto.getQuantity();
							unitPrice = (unitPrice == null ? 0D : unitPrice);
							dto.setTotalPriceValue(unitPrice);
							Double taxableValue = dto.getTaxableUnit() * dto.getGstQuantity();
							if (taxableValue == null) {
								taxableValue = 0.00;
							}
							dto.setTaxableValue(taxableValue);
							dto.setTaxPercentage(entrynew.getValue().get(0).getTaxPercent());

							Double igstValue = (dto.getTaxableUnit() * dto.getTaxPercentage() / 100)
									* dto.getQuantity();
							if (igstValue == null) {
								igstValue = 0.00;
							}
							dto.setIgstValue(igstValue);

							Double totValue = taxableValue + igstValue;
							if (totValue == null) {
								totValue = 0.00;
							}

							Double totalTaxPercentage = entrynew.getValue().get(0).getTaxPercent();
							totalTaxPercentage = totalTaxPercentage != null ? totalTaxPercentage : 0D;
							// Double totalTaxValue = taxableValue * (totalTaxPercentage / 100);
							// totalTaxValue = totalTaxValue != null ?
							// AppUtil.roundOffTaxValue(totalTaxValue) : 0D;

							Double cgstPercentage = entrynew.getValue().get(0).getCgstTaxPercent();
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = taxableValue * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = entrynew.getValue().get(0).getSgstTaxPercent();
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = taxableValue * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTaxValue = Double.sum(cgstValue, sgstValue);

							//
							log.info("tax value----" + totalTaxValue);

							dto.setTotalValue(taxableValue + totalTaxValue);

							if (sgstPercentage > 0) {
								// Double sgstValueTemp = ((dto.getTotalPriceValue() != null ?
								// dto.getTotalPriceValue()
								// : 0.0) * dto.getSgstPercentage()) / 100;
								// Double sgstValueTemp = totalTaxValue / taxCodeListSize;
								// log.info("sgst value----" + sgstValueTemp);

								Double sgstValueTemp = taxableValue * (sgstPercentage / 100);
								sgstValueTemp = AppUtil.getTrimmedValue(sgstValueTemp);
								log.info("sgst value: " + sgstValueTemp);

								dto.setSgstValue(
										(dto.getSgstValue() != null ? dto.getSgstValue() : 0.0) + sgstValueTemp);
							}
							if (cgstPercentage > 0) {
								// Double cgstValueTemp = ((dto.getTotalPriceValue() != null ?
								// dto.getTotalPriceValue()
								// : 0.0) * dto.getCgstPercentage()) / 100;
								// Double cgstValueTemp = totalTaxValue / taxCodeListSize;
								// log.info("cgst value----" + cgstValueTemp);
								Double cgstValueTemp = taxableValue * (cgstPercentage / 100);
								cgstValueTemp = AppUtil.getTrimmedValue(cgstValueTemp);
								dto.setCgstValue(
										(dto.getCgstValue() != null ? dto.getCgstValue() : 0.0) + cgstValueTemp);
							}

							dto.setCgstPercentage(cgstPercentage);
							dto.setSgstPercentage(sgstPercentage);

							dto.setTotalTaxValue(totalTaxValue);

							deliverychallanTotalAmount = deliverychallanTotalAmount
									+ (unitPrice != null ? unitPrice : 0.0);
							transferItemsList.add(dto);
						}
					}

					log.info("transferItemsList size is ::::::" + transferItemsList.size());

					Map<String, List<DeliveryChallanDTO>> hsnCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(r -> r.getHsnCode()));
					hsnSummaryList = new ArrayList<>();
					productCodeSummaryList = new ArrayList<>();
					if (hsnCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : hsnCodeWiseGroupingDTO.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setHsnCode(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							Double totalTaxableValue = entry.getValue().stream().mapToDouble(q -> q.getTotalTaxValue())
									.sum();
							totalTaxableValue = totalTaxableValue != null ? totalTaxableValue : 0D;
							// deliveryChallanDTO.setCgstValue(totalTaxableValue / taxCodeListSize);
							// deliveryChallanDTO.setSgstValue(totalTaxableValue / taxCodeListSize);

							deliveryChallanDTO
									.setCgstValue(entry.getValue().stream().mapToDouble(q -> q.getCgstValue()).sum());
							deliveryChallanDTO
									.setSgstValue(entry.getValue().stream().mapToDouble(q -> q.getSgstValue()).sum());

							deliveryChallanDTO.setTotalValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum()
											+ deliveryChallanDTO.getCgstValue() + deliveryChallanDTO.getSgstValue());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							hsnSummaryList.add(deliveryChallanDTO);
						}
					}

					Map<String, List<DeliveryChallanDTO>> productCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(c -> c.getItem()));
					if (productCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : productCodeWiseGroupingDTO
								.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setItem(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							Double totalTaxableValue = entry.getValue().stream().mapToDouble(q -> q.getTotalTaxValue())
									.sum();
							totalTaxableValue = totalTaxableValue != null ? totalTaxableValue : 0D;
							// deliveryChallanDTO.setCgstValue(totalTaxableValue / taxCodeListSize);
							// deliveryChallanDTO.setSgstValue(totalTaxableValue / taxCodeListSize);
							deliveryChallanDTO
									.setCgstValue(entry.getValue().stream().mapToDouble(q -> q.getCgstValue()).sum());
							deliveryChallanDTO
									.setSgstValue(entry.getValue().stream().mapToDouble(q -> q.getSgstValue()).sum());

							deliveryChallanDTO.setTotalValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum()
											+ deliveryChallanDTO.getCgstValue() + deliveryChallanDTO.getSgstValue());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());

							productCodeSummaryList.add(deliveryChallanDTO);
						}
					}
					transferItemsList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					hsnSummaryList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					productCodeSummaryList.sort((s1, s2) -> s1.getItem().compareTo(s2.getItem()));
				}

			}
		} catch (Exception e) {
			// log.info("abstractList size is ::::::" + abstractList.size());
			log.error("Error in createDeliveryChallan Method", e);
		}
		log.info("<==== Ends createStockTransferInvoice ====>");
		return STOCK_TRANSFER_SALES_INVOICE_ACKNOWLEDGE;
	}

	public String getValue(Double p) {
		DecimalFormat decim = new DecimalFormat("0.00");
		String price = decim.format(p);
		return price;
	}

	public String generateATNumber() {
		BaseDTO baseDTO = null;
		try {

			// if (validateSocietyData()) {
			updateDesignMaster();

			String atNumber = stockTransferItems.getAtNumber();
			atNumber = StringUtils.isNotEmpty(atNumber) ? atNumber.toUpperCase() : null;
			stockTransferItems.setAtNumber(atNumber);

			if (StringUtils.isNotEmpty(stockTransferItems.getAtNumber()) && supplierMaster != null) {
				BaseDTO response = checkATNumberDuplicateBasedFinYear(stockTransferItems.getAtNumber(),
						supplierMaster.getId());
				if (response != null) {
					if (response.getStatusCode() != null && response.getStatusCode()
							.longValue() == ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getErrorCode()) {
						stockTransferItems.setAtNumber(null);
						errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getErrorCode());
						RequestContext.getCurrentInstance().execute("PF('atnumberdialog').show();");
						return null;
					}
				}
			}

			if (supplierMaster != null) {
				if (StringUtils.isEmpty(stockTransferItems.getAtNumber())) {
					log.info("=========>>>>>>>>>>> Inside generateATNumber============>>>>>>>");
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/stockItemInwardPNS/generateATNumber/" + supplierMaster.getCode() + "/"
							+ countAtNumberGenerator;
					baseDTO = httpService.get(url);
					if (baseDTO.getGeneralContent() != null) {
						log.warn("generateATNumber Successfully Completed ******");

						stockTransferItems.setAtNumber(baseDTO.getGeneralContent());
						loadProductVerietyTaxPercentage();
						return ADD_PAGE_URL;
					} else {
						log.warn("========>>>>>>>generateATNumber Error *** :");
					}
				} else {
					loadProductVerietyTaxPercentage();
					return ADD_PAGE_URL;
				}
			}
			// }

			String atNumbers = stockTransferItems.getAtNumber();
			atNumbers = StringUtils.isNotEmpty(atNumbers) ? atNumbers.toUpperCase() : null;
			stockTransferItems.setAtNumber(atNumbers);

		} catch (Exception e) {
			log.error("=========>>>>>>>>>>> Exception Ocured In generateATNumber============>>>>>>>", e);
		}

		return null;

	}

	public void updateDesignMaster() {
		if (productDesignMaster != null) {
			societyProductAppraisal.setDesignName(productDesignMaster.getDesignName());
			societyProductAppraisal.setDesignNumber(productDesignMaster.getDesignCode().toString());
		}
		if (supplierMaster != null) {
			societyProductAppraisal.setSociety(supplierMaster);
		}
		societyProductAppraisal.setProductVariety(selectedProductVarietyMaster);
		stockTransferItems.setUnitRate(societyProductAppraisal.getProductCost());

		if (StringUtils.isNotEmpty(stockTransferItems.getAtNumber()) && supplierMaster != null) {
			checkATNumberDuplicateBasedFinYear(stockTransferItems.getAtNumber(), supplierMaster.getId());
		}
	}

	private StockTransfer getStockTranferById(Long stockTrasnferId) {
		log.info("Start method getStockTranferById()");
		StockTransfer stockTransferLocal = new StockTransfer();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/getStockTransferbyId/"
					+ stockTrasnferId;
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.get(url);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferLocal = mapper.readValue(jsonResponse, new TypeReference<StockTransfer>() {
				});
				log.warn("Size of StockItemLis is--->:" + stockTransferLocal != null
						? stockTransferLocal.getStockTransferItemsList().size()
						: "null");
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_VIEW_URL);
				setPurchaseOrderNumber(baseDTO.getGeneralContent());
				return stockTransferLocal;
			} else {
				log.warn("Item Retrieved Details Error *** :");
				return null;
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
			return null;
		}
	}

}
