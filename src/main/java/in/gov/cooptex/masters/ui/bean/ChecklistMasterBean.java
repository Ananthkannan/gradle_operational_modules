package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("checklistMasterBean")
@Scope("session")
public class ChecklistMasterBean implements Serializable {
	

}
