package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("areaMasterBean")
@Scope("session")
@Log4j2
public class AreaMasterBean {

	final String LIST_PAGE = "/pages/masters/area/listAreaMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/area/createAreaMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String AREA_MASTER_URL = AppUtil.getPortalServerURL()+"/area";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	AreaMaster areaMaster,selectedAreaMaster;

	@Getter @Setter
	LazyDataModel<AreaMaster> lazyAreaMasterList;

	List<AreaMaster> areaMasterList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter
	@Setter
	List<CityMaster> citymasterList=new ArrayList<>();

	@Getter
	@Setter
	List<StateMaster> stateMasterList=new ArrayList<>();

	@Getter
	@Setter
	List<DistrictMaster> districtMasterList = new ArrayList<>();


	@Getter
	@Setter
	StateMaster selectedStateMaster;
	@Getter
	@Setter
	DistrictMaster selectedDistrictMaster;


	@Autowired
	CommonDataService commonDataService;
	
	public String showAreaMasterListPage() {
		log.info("<==== Starts AreaMasterBean.showAreaMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		areaMaster = new AreaMaster();
		selectedAreaMaster = new AreaMaster();
		loadLazyAreaMasterList();
		log.info("<==== Ends AreaMasterBean.showAreaMasterListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyAreaMasterList() {
		log.info("<===== Starts AreaMasterBean.loadLazyAreaMasterList ======>");

		lazyAreaMasterList = new LazyDataModel<AreaMaster>() {
			
			/**
			 * 
			 */
			private static final long serialVersionUID = -7836693548474369283L;

			@Override
			public List<AreaMaster> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = AREA_MASTER_URL + "/getallareamasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						areaMasterList = mapper.readValue(jsonResponse, new TypeReference<List<AreaMaster>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return areaMasterList;
			}

			@Override
			public Object getRowKey(AreaMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AreaMaster getRowData(String rowKey) {
				for (AreaMaster areaMaster : areaMasterList) {
					if (areaMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedAreaMaster = areaMaster;
						return areaMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends AreaMasterBean.loadLazyAreaMasterList ======>");
	}


	public String showAreaMasterListPageAction() {
		log.info("<===== Starts AreaMasterBean.showAreaMasterListPageAction ===========>"+action);
		try{
			citymasterList = new ArrayList<>();
			stateMasterList =new ArrayList<>();
			districtMasterList=new ArrayList<>();
			selectedStateMaster= new StateMaster();
			selectedDistrictMaster= new DistrictMaster();
			if(!action.equalsIgnoreCase("create") && (selectedAreaMaster == null || selectedAreaMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SELECT_AREA_MASTER).getErrorCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create areaMaster called..");
				areaMaster = new AreaMaster();
				stateMasterList = commonDataService.loadStateList();
				log.info("<<::::::stateMaster:::::>>"+citymasterList.size());
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit areaMaster called..");
				String url=AREA_MASTER_URL+"/getbyid/"+selectedAreaMaster.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					areaMaster = mapper.readValue(jsonResponse,new TypeReference<AreaMaster>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete areaMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAreaMasterDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showAreaMasterListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends AreaMasterBean.showAreaMasterListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts AreaMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(AREA_MASTER_URL+"/deletebyid/"+selectedAreaMaster.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAreaMasterDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete areaMaster ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends AreaMasterBean.deleteConfirm ===========>");
		return  showAreaMasterListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts AreaMasterBean.saveOrUpdate =======>");
		try {
			/*if(areaMaster.getCode() == null || areaMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_CODE_EMPTY).getCode());
				return null;
			}*/
			if(areaMaster.getName() == null || areaMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_NAME_EMPTY).getCode());
				return null;
			}
			if(areaMaster.getName().trim().length() < 3 || areaMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if(areaMaster.getLocalName() == null || areaMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_LNAME_EMPTY).getCode());
				return null;
			}
			if(areaMaster.getLocalName().trim().length() < 3 || areaMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_LNAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if(areaMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			if(areaMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			String url = AREA_MASTER_URL+"/saveorupdate";
			BaseDTO response = httpService.post(url,areaMaster);
			if(response != null && response.getStatusCode() == 0) {
				log.info("areaMaster saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.AREA_MASTER_UPDATE_SUCCESS).getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends AreaMasterBean.saveOrUpdate =======>");
		return showAreaMasterListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts AreaMasterBean.onRowSelect ========>"+event);
		selectedAreaMaster = ((AreaMaster) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends AreaMasterBean.onRowSelect ========>");
    }
	
	public void loadDistrict() {
		if (selectedStateMaster != null) {
			districtMasterList = commonDataService.loadActiveDistrictsByState(selectedStateMaster.getId());
			citymasterList.clear();
		}
	}
	public void loadCity() {
		if (selectedDistrictMaster != null) {
			citymasterList =commonDataService.getCityByDistrictId(selectedDistrictMaster.getId());
			
		}
	}
}
