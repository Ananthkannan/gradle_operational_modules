/**
 * 
 */
package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.MediaMaster;
import in.gov.cooptex.core.model.MediaType;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Log4j2
@Scope("session")
@Service("mediaMasterBean")
public class MediaMasterBean {

	private final String CREATE_MEDIA_MASTER_PAGE = "/pages/masters/media/createMediaMaster.xhtml?faces-redirect=true;";
	private final String LIST_MEDIA_MASTER_PAGE = "/pages/masters/media/listMediaMaster.xhtml?faces-redirect=true;";
	private final String VIEW_MEDIA_MASTER_PAGE = "/pages/masters/media/viewMediaMaster.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	AddressMasterBean addressMasterBean = new AddressMasterBean();

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	MediaMaster mediaMaster, selectedMediaMaster;

	@Getter
	@Setter
	LazyDataModel<MediaMaster> lazyMediaMasterList;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	List<MediaMaster> mediaMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean statusButtonFlag = false;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<MediaType> activeMediaTypeList = null;

	@Getter
	@Setter
	MediaType selectedMediaTypeList;

	@Getter
	@Setter
	String address = "";

	@Getter
	@Setter
	private List<StateMaster> selectedStateMasterList;

	public String showMediaMasterListPage() {
		log.info("<==== Starts MediaMaster-showMediaMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		mediaMaster = new MediaMaster();
		selectedMediaMaster = new MediaMaster();
		action = "";
		loadLazyMediaMasterList();
		log.info("<==== Ends MediaMaster-showMediaMasterListPage =====>");
		return LIST_MEDIA_MASTER_PAGE;
	}

	public void loadLazyMediaMasterList() {
		log.info("<===== Starts Marital Ststus Bean.loadLazyMediaMasterList List ======>");
		String MEDIA_MASTER_LIST_URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/mediamaster/getallMediaMasterlistlazy";

		lazyMediaMasterList = new LazyDataModel<MediaMaster>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<MediaMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for Media Master List=============>" + MEDIA_MASTER_LIST_URL);
					BaseDTO response = httpService.post(MEDIA_MASTER_LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						mediaMasterList = mapper.readValue(jsonResponse, new TypeReference<List<MediaMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyMediaMaster List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return mediaMasterList;
			}

			@Override
			public Object getRowKey(MediaMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MediaMaster getRowData(String rowKey) {
				try {
					for (MediaMaster mediaMaster : mediaMasterList) {
						if (mediaMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedMediaMaster = mediaMaster;
							return mediaMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Marital Ststus Bean.loadLazyMaritalStstus List ======>");
	}

	public String mediaMasterListPageAction() {
		log.info("<========inside mediaMasterBean.mediaMasterListPageAction=======>");
		try {

			if (action.equalsIgnoreCase("Create")) {
				log.info("create mediaMaster called..");
				mediaMaster = new MediaMaster();
				addressMasterBean.setAddressMaster(new AddressMaster());
				address = "";
				selectedMediaTypeList = new MediaType();
				getActiveMediaType();

				return CREATE_MEDIA_MASTER_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("view mediaMaster called..");
				mediaMasterValuebyId();
				return VIEW_MEDIA_MASTER_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				mediaMaster = new MediaMaster();
				addressMasterBean.setAddressMaster(new AddressMaster());
				selectedMediaTypeList = new MediaType();

				getActiveMediaType();
				mediaMasterValuebyId();
				address = addressMasterBean.prepareAddress(selectedMediaMaster.getAddressMaster());

				return CREATE_MEDIA_MASTER_PAGE;
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete mediaMaster called.." + selectedMediaMaster.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmMediaMasterDelete').show();");
			}

		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void mediaMasterValuebyId() {
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/mediamaster/get/"
					+ selectedMediaMaster.getId();
			BaseDTO baseDTO = httpService.get(url);
			selectedMediaMaster = new MediaMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedMediaMaster = mapper.readValue(jsonResponse, new TypeReference<MediaMaster>() {
				});
				log.info("=============selectedMediaMaster=====================" + selectedMediaMaster.getName());

				if (action.equalsIgnoreCase("Edit")) {
					mediaMaster = selectedMediaMaster;
					selectedMediaTypeList = selectedMediaMaster.getMediaType();
					addressMasterBean.setAddressMaster(selectedMediaMaster.getAddressMaster());

				}
				address = addressMasterBean.prepareAddress(selectedMediaMaster.getAddressMaster());
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public void getActiveMediaType() {
		BaseDTO baseDTO = new BaseDTO();
		activeMediaTypeList = new ArrayList<>();
		log.info("<===  getActiveMediaType====>");
		try {

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/mediatype/getActiveMediaType";
			log.info("URL===MEDIA_MASTER_LIST_URL================>" + url);
			baseDTO = httpService.get(url);
			log.info("<=========== after URL =======>" + baseDTO);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				activeMediaTypeList = mapper.readValue(jsonResponse, new TypeReference<List<MediaType>>() {
				});
				log.info("=============activeMediaTypeList=====================" + activeMediaTypeList.size());
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public String createMediaMaster() {
		log.info("<==== Starts MediaMaster.createMediaMaster =======>" + selectedMediaTypeList.getName());
		try {
			mediaMaster.setMediaType(selectedMediaTypeList);
			if (mediaMaster.getMediaType().getId() == null) {
				errorMap.notify(ErrorDescription.MEDIA_TYPE_EMPTY.getCode());
				return null;
			}
			if (mediaMaster.getName() == null || mediaMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.MEDIA_NAME_EMPTY.getCode());
				return null;
			}
			if (mediaMaster.getName().trim().length() < 3 || mediaMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.MEDIA_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (mediaMaster.getLocName() == null || mediaMaster.getLocName().trim().isEmpty()) {
//				errorMap.notify(ErrorDescription.MEDIA_NAME_EMPTY.getCode());
				Util.addError("Media Name in Tamil is required");
			
				return null;
			}
			if (mediaMaster.getLocName().trim().length() < 3 || mediaMaster.getLocName().trim().length() > 75) {
				Util.addError("Media Name in Tamil should be minimum of 3 characters and maximum of 76 characters");
				return null;
			}
			if (mediaMaster.getEmailId() != null&&mediaMaster.getEmailId().trim().length()>0) {
				if (!EmployeeValidator.validateEmail(mediaMaster.getEmailId())) {
					log.error("  Invalid Email Id ");
					errorMap.notify(ErrorDescription.INAVALID_SUPPORT_EMAIL.getErrorCode());
					return null;
				}
			}
			if (mediaMaster.getCode() != null) {
				if (mediaMaster.getCode().trim().length() < 3 || mediaMaster.getCode().trim().length() > 75) {
					errorMap.notify(ErrorDescription.MEDIA_CODE_MIN_MAX_ERROR.getCode());
					return null;
				}

			}

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/mediamaster/saveOrUpdateMediaMaster";
			BaseDTO response = httpService.post(url, mediaMaster);

			if (response != null && response.getStatusCode() == 0) {

				log.info("media saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.MEDIA_INSERTED_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.MEDIA_UPDATE_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends MediaMaster.createMediaMaster  =======>");
		return showMediaMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts MediaMasterBean.onRowSelect ========>" + event);
		selectedMediaMaster = ((MediaMaster) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends MediaMasterBean.onRowSelect ========>" + selectedMediaMaster.getId());
	}

	public String deleteConfirm() {
		log.info("<===== Starts MediaMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/mediamaster/deletebyid/" + selectedMediaMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MEDIA_DELETE_SUCCESSFULLY.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmMediaMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete community ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends MediaMasterBean.deleteConfirm ===========>");
		return showMediaMasterListPage();
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		// String addressArray[] =
		// AppUtil.prepareAddress(addressMasterBean.getAddressMaster());
		mediaMaster.setAddressMaster(addressMasterBean.getAddressMaster());
		// adressMaster = circleMaster.getAddressMaster();
		address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("createMediaMasterForm");
	}

}
