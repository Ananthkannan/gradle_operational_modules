package in.gov.cooptex.admin.vehicle.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.Insurance;
import in.gov.cooptex.core.model.InsuranceMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.VehicleColor;
import in.gov.cooptex.core.model.VehicleGearType;
import in.gov.cooptex.core.model.VehicleMake;
import in.gov.cooptex.core.model.VehicleModel;
import in.gov.cooptex.core.model.VehicleRegister;
import in.gov.cooptex.core.model.VehicleType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.intend.model.StockItemInwardPNSDTO;
import in.gov.cooptex.operation.model.PurchaseOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("vehicleRegisterBean")
@Scope("session")
public class VehicleRegisterBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String CREATE_PAGE = "/pages/masters/vehicle/createVehicleRegister.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/vehicle/listVehicleRegister.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/vehicle/viewVehicleRegister.xhtml?faces-redirect=true;";

	private String serverURL = null;

	private final String LIST_API_URL = "";

	@Autowired
	AppPreference appPreference;
	
	@Getter
	@Setter
	private VehicleColor vehiclecolor=new VehicleColor();
	

	@Getter
	@Setter
	private VehicleRegister vehicleRegister=new VehicleRegister();

	@Getter
	@Setter
	private VehicleRegister selectedVehicleRegister=new VehicleRegister();

	@Getter
	@Setter
	private String action;

	
	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<VehicleRegister> lazyVehicleRegisterList;

	@Getter
	@Setter
	List<VehicleRegister> vehicleRegisterList;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true, viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	List<VehicleType> vehicleTypeList;

	@Getter
	@Setter
	private int vehicleTypeListSize;

	@Getter
	@Setter
	List<VehicleMake> vehicleMakeList;

	@Getter
	@Setter
	private int vehicleMakeListSize;

	@Getter
	@Setter
	List<VehicleModel> vehicleModelList;

	@Getter
	@Setter
	List<VehicleColor> vehicleColorList;

	@Getter
	@Setter
	List<VehicleGearType> vehicleGearTypelList;

	@Getter
	@Setter
	List<PurchaseOrder> purhcaseOrderList;

	@Getter
	@Setter
	List<InsuranceMaster> insuranceMasterList;

	@Getter
	@Setter
	String[] fuelTypeList = { "Diesel", "Gas", "Petrol" };
	
	@Getter
	@Setter
	private String pageHead = "";

	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	List<EntityMaster> entityList;
	
	@Autowired
	CommonDataService commonDataService;
	
	public VehicleRegisterBean() {
	}

	@Getter
	@Setter
	int size;

	@PostConstruct
	public void init() {
		log.info(" VehicleRegisterBean init");
		vehiclecolor=new VehicleColor();
		loadValues();

	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
			loadLazyVehicleRegisterList();
			// loadVehicleModelList();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
	}

	public String showListPage() {

		log.info("<==== Starts VehicleRegister-showVehicleRegisterListPage =====>");

		mapper = new ObjectMapper();
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		vehicleRegister = new VehicleRegister();
		vehicleRegister.setVehicleType(new VehicleType());
		selectedVehicleRegister = new VehicleRegister();

		log.info("<==== Ends VehicleRegister-showVehicleRegisterListPage =====>");

		return LIST_PAGE;
	}

	@SuppressWarnings("serial")
	// public void loadLazyVehicleRegisterList() {
	//
	// log.info("<===== loadLazyVehicleRegisterList() Starts ======>");
	//
	// lazyVehicleRegisterList = new LazyDataModel<VehicleRegister>() {
	//
	// @Override
	// public List<VehicleRegister> load(int first, int pageSize, String sortField,
	// SortOrder sortOrder,
	// Map<String, Object> filters) {
	// try {
	// PaginationDTO paginationRequest = new PaginationDTO(first / pageSize,
	// pageSize, sortField,
	// sortOrder.toString(), filters);
	// log.info("Pagination request :::" + paginationRequest);
	// String URL = serverURL + appPreference.getOperationApiUrl() +
	// "/vehicleregister/searchData";
	// BaseDTO response = httpService.post(URL, paginationRequest);
	// if (response != null && response.getStatusCode() == 0) {
	// jsonResponse = mapper.writeValueAsString(response.getResponseContents());
	// vehicleRegisterList = mapper.readValue(jsonResponse,
	// new TypeReference<List<VehicleRegister>>() {
	// });
	// this.setRowCount(response.getTotalRecords());
	// totalRecords = response.getTotalRecords();
	// } else {
	// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	// }
	// } catch (Exception e) {
	// log.error("Exception occured in loadLazyVehicleRegister List ...", e);
	// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	// }
	// log.info("Ends lazy load....");
	// return vehicleRegisterList;
	// }
	//
	// @Override
	// public Object getRowKey(VehicleRegister res) {
	// return res != null ? res.getId() : null;
	// }
	// };
	// log.info("<===== loadLazyVehicleRegisterList() Ends ======>");
	//
	// }

	public void loadLazyVehicleRegisterList() {
		log.info("<--- loadLazyVehicleRegister ---> ");
		lazyVehicleRegisterList = new LazyDataModel<VehicleRegister>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<VehicleRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<VehicleRegister> data = new ArrayList<VehicleRegister>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count VehicleRegister--->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyVehicleRegisterList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(VehicleRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VehicleRegister getRowData(String rowKey) {
				List<VehicleRegister> responseList = (List<VehicleRegister>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (VehicleRegister res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							vehicleRegister = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		vehicleRegister = new VehicleRegister();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		VehicleRegister vehicleRegistersearch = new VehicleRegister();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		vehicleRegistersearch.setPaginationDTO(paginationDTO);

		vehicleRegistersearch.setFilters(filters);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/searchData";
			baseDTO = httpService.post(URL, vehicleRegistersearch);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public String showAddPage() {
		action = "ADD";
		pageHead = "Create";

		vehicleRegister = new VehicleRegister();
		vehicleRegister.setInsuranceMaster(new InsuranceMaster());
		vehicleRegister.setEntityMaster(new EntityMaster());
		loadVehicleTypeList();

		loadVehicleMakeList();
		loadGearTypeList();
		loadInsuranceCompanyList();
		loadPurchaseOrderNoList();
		// loadVehicleModelList();
		// loadVehicleColorList();
		// loadInsuranceCompanyList();
		
		if(entityList==null) {
			entityList = commonDataService.loadHeadAndRegionalOffice();
		}
		
		return CREATE_PAGE;
	}

	public void listenerLoad(String type) {
		log.info("<<<< ---------- Recieved Listener ------- >>>> " + type);
		if (type.equalsIgnoreCase("Make"))
			loadVehicleMakeList();
		else if (type.equalsIgnoreCase("purchase"))
			loadPurchaseOrderNoList();
		else if (type.equalsIgnoreCase("model"))
			loadVehicleModelList();
		else if (type.equalsIgnoreCase("color"))
			loadVehicleColorList();
		else if (type.equalsIgnoreCase("insurance"))
			loadInsuranceCompanyList();
		else if (type.equalsIgnoreCase("gear"))
			loadGearTypeList();
		else
			log.error("<<<< ---------- Recieved Listener UNKNOW------- >>>> " + type);
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Item List onRowSelect method started");
		log.info("Selected ID is :" + selectedVehicleRegister.getId());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
	}

	public String clearButton() {
		log.info("Clear Action Called");
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		selectedVehicleRegister = new VehicleRegister();
		return LIST_PAGE;
	}

	private void loadVehicleTypeList() {
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleTypeList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/getActiveVehicleTypeList";
			long start = System.currentTimeMillis();
			baseDTO = httpService.get(url);
			long end = System.currentTimeMillis();
			log.info("<<====  TIME TAKEN  loadVehicleTypeList :: " + (end - start));
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				vehicleTypeList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicleTypeList != null) {
			vehicleTypeListSize = vehicleTypeList.size();
		} else {
			vehicleTypeListSize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-vehicleTypeListSize ------- >>>> " + vehicleTypeListSize);
	}

	public void loadVehicleMakeList() {
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleMakeList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemake/getActiveVehicleMake";
			long start = System.currentTimeMillis();
			baseDTO = httpService.get(url);
			long end = System.currentTimeMillis();
			log.info("<<====  TIME TAKEN  loadVehicleMakeList :: " + (end - start));
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleMakeList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleMake>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicleMakeList != null) {
			vehicleMakeListSize = vehicleMakeList.size();
		} else {
			vehicleMakeListSize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-loadVehicleMakeList - Size ------- >>>> " + vehicleMakeList);
	}

	// # Load Purchase Order List
	public void loadPurchaseOrderNoList() {
		int purchaseOrderListSize;
		log.info("<<<< ----------Start VehicleRegisterBean-loadPurchaseOrderNoList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/getActiveVehicleRegister";
			long start = System.currentTimeMillis();
			baseDTO = httpService.get(url);
			long end = System.currentTimeMillis();
			log.info("<<====  TIME TAKEN  loadPurchaseOrderNoList :: " + (end - start));
			if (baseDTO != null) {
//				ObjectMapper mapper = new ObjectMapper();
//				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
//				purhcaseOrderList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrder>>() {
//				});
				
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				purhcaseOrderList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrder>>() {
				});	
				
				
				
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting purchaseOrderNoList .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured loadPurchaseOrderNoList.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured loadPurchaseOrderNoList.... ", e);
		}
		if (purhcaseOrderList != null) {
			purchaseOrderListSize = purhcaseOrderList.size();
		} else {
			purchaseOrderListSize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-purchaseOrderNoList - Size ------- >>>> " + purchaseOrderListSize);
	}

	// # Load Vehicle Model List
	public void loadVehicleModelList() {
		int purchaseOrderListSize;
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleModelList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/getActiveModelList/"
					+ vehicleRegister.getVehicleMake().getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				vehicleModelList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleModel>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting loadVehicleModelList .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured loadVehicleModelList.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured loadVehicleModelList.... ", e);
		}
		vehicleRegister.setVehicleColor(null);
		if (vehicleModelList != null) {
			purchaseOrderListSize = vehicleModelList.size();
		} else {
			purchaseOrderListSize = 0;
		}
		log.info(
				"<<<< ----------VehicleRegisterBean-loadVehicleModelList - Size ------- >>>> " + purchaseOrderListSize);
	}

	// # Load Vehicle Color List
	public void loadVehicleColorList() {
		int purchaseOrderListSize;
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleColorList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/getActiveColorList/"
					+ vehicleRegister.getVehicleModel().getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				vehicleColorList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleColor>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting loadVehicleColorList .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured loadVehicleColorList.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured loadVehicleColorList.... ", e);
		}
		if (vehicleColorList != null) {
			purchaseOrderListSize = vehicleColorList.size();
		} else {
			purchaseOrderListSize = 0;
		}
		log.info(
				"<<<< ----------VehicleRegisterBean-loadVehicleColorList - Size ------- >>>> " + purchaseOrderListSize);
	}

	// # Load Insurance Company List
	public void loadGearTypeList() {
		int purchaseOrderListSize;
		log.info("<<<< ----------Start VehicleRegisterBean-loadGearTypeList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/getActiveGearTypeList";
			long start = System.currentTimeMillis();
			baseDTO = httpService.get(url);
			long end = System.currentTimeMillis();
			log.info("<<====  TIME TAKEN  loadGearTypeList :: " + (end - start));
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleGearTypelList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleGearType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting loadGearTypeList .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured loadGearTypeList.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured loadGearTypeList.... ", e);
		}
		if (vehicleGearTypelList != null) {
			purchaseOrderListSize = vehicleGearTypelList.size();
		} else {
			purchaseOrderListSize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-loadGearTypeList - Size ------- >>>> " + purchaseOrderListSize);
	}

	// # Load Insurance Company List
	public void loadInsuranceCompanyList() {
		int purchaseOrderListSize;
		log.info("<<<< ----------Start VehicleRegisterBean-loadInsuranceCompanyList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/vehicleregister/getActiveInsuranceCompanyList";
			long start = System.currentTimeMillis();
			baseDTO = httpService.get(url);
			long end = System.currentTimeMillis();
			log.info("<<====  TIME TAKEN  loadInsuranceCompanyList :: " + (end - start));
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				insuranceMasterList = mapper.readValue(jsonResponse, new TypeReference<List<InsuranceMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting loadInsuranceCompanyList .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured loadInsuranceCompanyList.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured loadInsuranceCompanyList.... ", e);
		}
		/*
		 * if (insuranceMasterList != null) { purchaseOrderListSize =
		 * insuranceMasterList.size(); } else { purchaseOrderListSize = 0; } log.
		 * info("<<<< ----------VehicleRegisterBean-loadInsuranceCompanyList - Size ------- >>>> "
		 * + purchaseOrderListSize);
		 */
	}

	// # View Selected Vehicle Register
	public void viewSelectedVehicleRegister() {
		log.info("<<<< ----------Start VehicleRegisterBean-viewSelectedVehicleRegister ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/vehicleregister/getSelectedVehicleRegiterById/" + selectedVehicleRegister.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedVehicleRegister = mapper.readValue(jsonResponse, new TypeReference<VehicleRegister>() {
				});
			} else
				log.error(" VehicleRegisterBean- ViewVehicleRegister Not Response");
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting viewSelectedVehicleRegister .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured viewSelectedVehicleRegister.... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured viewSelectedVehicleRegister.... ", e);
		}
	}

	public String showEditPage() {
		vehiclecolor=new VehicleColor();
		action = "EDIT";
		pageHead = "Edit";
		log.info("<<===  showEditPage  =====>>");
		if (selectedVehicleRegister == null || selectedVehicleRegister.getId() == null) {
			errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			BaseDTO baseDTO = new BaseDTO();

			loadVehicleTypeList();

			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/vehicleregister/getSelectedVehicleRegiterById/" + selectedVehicleRegister.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleRegister = mapper.readValue(jsonResponse, new TypeReference<VehicleRegister>() {
				});
				vehiclecolor=vehicleRegister.getVehicleColor();
			} else
				log.error(" VehicleRegisterBean- showEditPage Not Response");

			loadVehicleMakeList();
			loadPurchaseOrderNoList();
			loadVehicleModelList();
			loadVehicleColorList();
			loadGearTypeList();
			loadInsuranceCompanyList();
			if(entityList==null) {
				entityList = commonDataService.loadHeadAndRegionalOffice();
			}
		} catch (Exception e) {
			log.info("<<===  Error in showEditPage ===>>");
			e.printStackTrace();
		}

		return CREATE_PAGE;
	}

	public String showViewPage() {
		if (selectedVehicleRegister == null || selectedVehicleRegister.getId() == null) {
			errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}

		action = "VIEW";
		viewSelectedVehicleRegister();
		return VIEW_PAGE;
	}

	public String goToPage(String pageName) {

		log.info("Goto Page [" + pageName + "]");

		if (pageName.equalsIgnoreCase("LIST")) {
			return showListPage();
		} else if (pageName.equalsIgnoreCase("ADD")) {
			return showAddPage();
		} else if (pageName.equalsIgnoreCase("EDIT")) {
			return showEditPage();
		} else if (pageName.equalsIgnoreCase("VIEW")) {
			return showViewPage();
		} else {
			return null;
		}
	}

	public String createVehicleRegister() {
		log.info("<<<< ----------Start VehicleRegisterBean-saveVehicleRegister ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl();

			if (action.equalsIgnoreCase("ADD"))
				url = url + "/vehicleregister/createVehicleRegister";
			if (action.equalsIgnoreCase("EDIT"))
				url = url + "/vehicleregister/updateVehicleRegister";

			// VehicleRegisterDTO vehicleRegisterDTO = new VehicleRegisterDTO();
			if (action.equalsIgnoreCase("ADD")) {
				vehicleRegister.setCreatedDate(new Date());
				vehicleRegister.setActiveStatus(true);
			}
			vehicleRegister.setModifiedDate(new Date());
			// vehicleRegisterDTO.setVehicleRegister(vehicleRegister);
			vehicleRegister.setVehicleColor(vehiclecolor);
			baseDTO = httpService.post(url, vehicleRegister);
			if (baseDTO.getStatusCode() == 0) {
				log.info("saveVehicleRegister Succes -->");
				if (action.equalsIgnoreCase("ADD")) {
					errorMap.notify(ErrorDescription.VEHICLE_REGISTER_CREATE_SUCCESS.getErrorCode());
				}else {
					errorMap.notify(ErrorDescription.VEHICLE_REGISTER_UPDATE_SUCCESS.getErrorCode());
				// FacesContext.getCurrentInstance().getExternalContext().redirect(LIST_PAGE);
				}
			} else {
				log.warn("saveVehicleRegister Error *** :");
				errorMap.notify(ErrorDescription.VEHICLE_REGISTER_CREATE_FAILED.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured saveVehicleRegister.... ", e);
		}

		return showListPage();
	}

	public String submitVehicleRegister() {

		log.info("Receive Vehicle Register Save");
		// String apiURL =serverURL + appPreference.getPortalServerURL() +
		// "/vehicleregister/create";
		String url = serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/create";
		try {

			BaseDTO baseDTO = httpService.post(url, vehicleRegister);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Saved Successfully");
					// errorMap.notify(ErrorDescription.VEHICLE_REGISTER_ADDED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}

			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_PAGE;
	}

	public VehicleRegister getVehicleRegisterById(Long id) {

		String apiURL = appPreference.getPortalServerURL() + "/vehicleregister/view/" + id;
		VehicleRegister vehicleRegister = null;
		try {

			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleRegister = mapper.readValue(jsonValue, VehicleRegister.class);
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return vehicleRegister;

	}
	
	public String deleteVehicleRegister() {
		log.info("<<====   DELETE vehicle register =====>>");
		if (selectedVehicleRegister == null || selectedVehicleRegister.getId() == null) {
			errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			String apiURL =  serverURL + appPreference.getOperationApiUrl() + "/vehicleregister/delete/" + selectedVehicleRegister.getId();
			try {
				BaseDTO baseDTO = httpService.delete(apiURL);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.VEHICLE_REGISTER_DELETE_SUCCESS.getCode());
					} else {
						errorMap.notify(baseDTO.getStatusCode());
					}
				}
			} catch (Exception exception) {
				log.error("Exception ", exception);
				errorMap.notify(ErrorDescription.VEHICLE_REGISTER_DELETE_FAILED.getCode());
			}
		}catch(Exception e) {
			log.error("<<===  vehicle register delete error:: "+e);
			}
		return showListPage();
	}

}