package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayRollDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.esr.dto.RetiredEmployeeRegisterDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class RetiredEmployeeRegisterBean {
	private final String INPUT_FORM_LIST_URL = "/pages/personnelHR/listRtdEmployeeRegister.xhtml?faces-redirect=true;";
	private final String CREATE_RETIRED_EMPLY_REGISTER_PAGE = "/pages/personnelHR/createRtdEmployeeRegister.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/personnelHR/viewRtdEmployeeRegister.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	RetiredEmployeeRegisterDTO retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();

	@Getter
	@Setter
	EmployeeRetirement employeeRetirement = new EmployeeRetirement();

	@Getter
	@Setter
	List<EmployeePayRollDetails> employeePayrollDetails = new ArrayList<EmployeePayRollDetails>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMaster = new ArrayList<EmployeeMaster>();

	@Setter
	@Getter
	List<Designation> designationList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();

	@Setter
	@Getter
	List<Department> departmentList;

	@Setter
	@Getter
	List<SectionMaster> sectionMasterList;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	LazyDataModel<RetiredEmployeeRegisterDTO> retiredEmployeeRegisterDTOlist = null;
	
	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@PostConstruct
	public void init() {
		log.info(" retiredEmployeeRegisterBean init --->" + serverURL);
		serverURL = appPreference.getPortalServerURL();
		loadLazyList();
	}

	public String addretiredEmployeeRegister() {
		log.info("<---- Start retiredEmployeeRegisterBean.addretiredEmployeeRegister ---->");
		try {
			action = "ADD";
			retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		log.info("<---- End retiredEmployeeRegisterBean.addretiredEmployeeRegister ---->");
		loadSearchListOfValues();
		return CREATE_RETIRED_EMPLY_REGISTER_PAGE;
	}

	public String showList() {
		retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();
		log.info("<---------- Loading retiredEmployeeRegisterBean showList list page---------->");
		addButtonFlag = false;
		return INPUT_FORM_LIST_URL;
	}

	public String submit() {
		log.info("...... retiredEmployeeRegisterBean submit begin ....");
		try {

			log.info("object::" + retiredEmployeeRegisterDTO.getFirstName());
			BaseDTO baseDTO = null;
			String url = serverURL + "/employee/retirement/register";
			log.info("...... Url::" + url);
			baseDTO = httpService.post(url, retiredEmployeeRegisterDTO);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("retiredEmployeeRegisterBean saved successfully");
					showList();

					if (action == "ADD") {
						// AppUtil.addInfo("Retired Employee Register Created Successfully");
						errorMap.notify(MastersErrorCode.RETIREDEMPLOYEEREGISTER_ADDED_SUCCESSFULLY);
					} else {
						AppUtil.addInfo("Retired Employee Register Updated Successfully");
					}

				} else {
					log.error("Retired Employee Register name:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					// errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addError(baseDTO.getErrorDescription());
				}
			}
		} catch (Exception e) {
			log.error("Error while creating retiredEmployeeRegister" + e);
		}
		log.info("...... retiredEmployeeRegisterBean submit ended ....");
		clear();
		return INPUT_FORM_LIST_URL;
	}

	public void loadSearchListOfValues() {
		log.info("start loadSearchListOfValues ");
		try {
			designationList = commonDataService.loadDesignation();
			entityMasterList = commonDataService.loadAllHeadAndRegionalOffice();
			departmentList = commonDataService.getDepartment();
			sectionMasterList = commonDataService.loadSectionList();

		} catch (Exception exp) {
			log.error(" Exception Occured in while loading masters :: ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String clear() {
		log.info("<---- Start retiredEmployeeRegisterBean . clear ----->");
		retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();
		log.info("<---- End retiredEmployeeRegisterBean . clear ----->");
		addButtonFlag = false;
		return INPUT_FORM_LIST_URL;
	}

	public String cancel() {
		addButtonFlag = false;
		return INPUT_FORM_LIST_URL;
	}

	public void loadLazyList() {
		retiredEmployeeRegisterDTOlist = new LazyDataModel<RetiredEmployeeRegisterDTO>() {
			private static final long serialVersionUID = 7742507957928805313L;

			@Override
			public List<RetiredEmployeeRegisterDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<RetiredEmployeeRegisterDTO> data = new ArrayList<RetiredEmployeeRegisterDTO>();

				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetiredEmployeeRegisterDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(RetiredEmployeeRegisterDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public RetiredEmployeeRegisterDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<RetiredEmployeeRegisterDTO> responseList = (List<RetiredEmployeeRegisterDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (RetiredEmployeeRegisterDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();

		BaseDTO basadtoObj = new BaseDTO();

		RetiredEmployeeRegisterDTO request = new RetiredEmployeeRegisterDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);
		
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);
		
			if (key.equals("firstName")) {
				request.setFirstName(value);
			} else if (key.equals("lastName")) {
				request.setLastName(value);
			} else if (key.equals("dateOfRetiremente")) {
			//	DateFormat date1 = new SimpleDateFormat("yyyy-MM-dd");
				request.setDateOfRetiremente((Date)entry.getValue());
			} else if (key.equals("entityCode")) {
				request.setEntityCode(value);
			} else if (key.equals("basicPay")) {
				request.setBasicPay(Double.parseDouble(value));
			} 
		}
		
		try {
			String url = serverURL + "/employee/retirement/list";
			log.info("<<=====  URL::: " + url);
			basadtoObj = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			basadtoObj.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return basadtoObj;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		retiredEmployeeRegisterDTO = ((RetiredEmployeeRegisterDTO) event.getObject());
		addButtonFlag = true;
	}

	public String backRiteiredEmployeeRegister() {
		showList();
		return INPUT_FORM_LIST_URL;
	}

	public String clearRetiredEmployeeRegister() {
		log.info("<---- Start retiredEmployeeRegisterBean . clear ----->");
		retiredEmployeeRegisterDTO = new RetiredEmployeeRegisterDTO();
		log.info("<---- End retiredEmployeeRegisterBean . clear ----->");
		addButtonFlag = false;
		return INPUT_FORM_LIST_URL;
	}

	public String editRetiredEmployeeRegister() {
		log.info("<<<<< -------Start retiredEmployeeRegister called ------->>>>>> ");
		action = "EDIT";
		if (retiredEmployeeRegisterDTO == null) {
			Util.addWarn("Please select one Retired Employee Register");
			return null;
		}

		log.info("<<<<< -------End retiredEmployeeRegister called ------->>>>>> ");

		return CREATE_RETIRED_EMPLY_REGISTER_PAGE;

	}

	public String viewRetiredEmployeeRegister() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (retiredEmployeeRegisterDTO == null) {
			Util.addWarn("Please Select One Retired Employee Register");
			return null;
		}
		return INPUT_FORM_VIEW_URL;

	}
}
