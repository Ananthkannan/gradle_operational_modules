package in.gov.cooptex.operation.testinglab.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.production.model.TestLabReport;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("testReportBean")
@Scope("session")
public class TestReportBean {

	
	final String LIST_PAGE = "/pages/inspectionCenter/TestingLab/listTestReport.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/inspectionCenter/TestingLab/createTestReport.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/inspectionCenter/TestingLab/viewTestReport.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

//	final String TEST_REPORT_URL = AppUtil.getPortalServerURL()+"/testreport/";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;
	
	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	TestLabReport testReport,selectedTestReport;

	@Getter @Setter
	LazyDataModel<TestLabReport> lazyTestReportList;

	List<TestLabReport> testReportList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = false;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = false;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = false;
	
	@Getter
	@Setter
	List<StockTransfer> stockInwardList;
	
	@Getter
	@Setter
	StockTransfer stockInward = new StockTransfer(); 
	
	@Getter
	@Setter
	List<StockTransferItems> productSampleList;
	
	@Getter
	@Setter
	StockTransferItems productSampleNumber =  new StockTransferItems();
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfObject = new SimpleDateFormat("dd-MMM-yyyy");
	
	public String showTestingLabReportListPage() {
		log.info("<==== Starts testReportBean.showTestingLabReportListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		viewButtonFlag = false;
		testReport = new TestLabReport();
		selectedTestReport = new TestLabReport();
		loadLazyTestingLabReportList();
		loadTestingInwardList();
		log.info("<==== Ends testReportBean.showTestingLabReportListPage =====>");
		return LIST_PAGE;
	}
	
	
	public void  loadLazyTestingLabReportList() {
		log.info("<===== Starts testReportBean.loadLazyTestingLabReportList ======>");

		lazyTestReportList = new LazyDataModel<TestLabReport>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TestLabReport> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL+appPreference.getOperationApiUrl()+"/testlabreport/getalltestlabreportlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						testReportList = mapper.readValue(jsonResponse, new TypeReference<List<TestLabReport>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return testReportList;
			}

			@Override
			public Object getRowKey(TestLabReport res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TestLabReport getRowData(String rowKey) {
				for (TestLabReport testingLab : testReportList) {
					if (testingLab.getId().equals(Long.valueOf(rowKey))) {
						selectedTestReport = testingLab;
						return testingLab;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends testReportBean.loadLazyTestingLabReportList ======>");
	}
	
	
	
	public String showTestReportListPageAction() {
		log.info("<===== Starts testReportBean.showTestReportListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedTestReport == null || selectedTestReport.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create test report called..");
				stockInward = new StockTransfer();
				productSampleNumber = new StockTransferItems();
				testReport = new TestLabReport();
				productSampleList = new ArrayList<>();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit") || action.equalsIgnoreCase("View")) {
				log.info("edit test report called..");
				String url=SERVER_URL+appPreference.getOperationApiUrl()+"/testlabreport//get/"+selectedTestReport.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					testReport = mapper.readValue(jsonResponse,new TypeReference<TestLabReport>(){
					});
					if(testReport != null) {
						stockInward = testReport.getStockTransfer();
						
						updateProductSampleList();
						productSampleNumber = testReport.getStockTransferItems();
					}
					if(action.equalsIgnoreCase("Edit"))
						return ADD_PAGE;
					else {
						stockInward.setMessage(sdfObject.format(stockInward.getCreatedDate()));
						return VIEW_PAGE;
					}
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete test report called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTestReportDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showTestReportListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends testReportBean.showTestReportListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts testReportBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL+appPreference.getOperationApiUrl()+"/testlabreport/deletebyid/"+selectedTestReport.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TEST_REPORT_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTestReportDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete testLabReport ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends testReportBean.deleteConfirm ===========>");
		return  showTestingLabReportListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts testReportBean.saveOrUpdate =======>");
		try {
			if(!inputFieldsValidation())
				return null;
			String url = SERVER_URL+appPreference.getOperationApiUrl()+"/testlabreport/create";
			testReport.setStockTransferItems(productSampleNumber);
			testReport.setProductVarietyMaster(productSampleNumber.getProductVarietyMaster());
			testReport.setActiveStatus(true);
			BaseDTO response = httpService.post(url,testReport);
			if(response != null && response.getStatusCode() == 0) {
				log.info("testReport saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.TEST_REPORT_CREATE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.TEST_REPORT_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends testReportBean.saveOrUpdate =======>");
		return showTestingLabReportListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts testReportBean.onRowSelect ========>"+event);
		selectedTestReport = ((TestLabReport) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		viewButtonFlag = true;
		log.info("<===Ends testReportBean.onRowSelect ========>");
    }
	
	
	public void loadTestingInwardList() {
		log.info("<===Starts testReportBean.loadTestingInwardList ========>");
		try {
			
			String url = SERVER_URL + appPreference.getOperationApiUrl()+"/stock/transfer/gettestinginwardlist";
			BaseDTO response  = httpService.get(url); 
			if(response != null && response.getStatusCode() == 0) {
				log.info("list retrieved  successfully..........");
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				stockInwardList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
				});
				if(stockInwardList != null) {
					log.info("stockInwardList size is ........."+stockInwardList.size());
					for(StockTransfer s:stockInwardList) {
						s.setMessage(sdf.format(s.getCreatedDate()));
					}
				}
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured in loadTestingInwardList...........",e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<===Ends testReportBean.loadTestingInwardList ========>");
	}
	
	
	public void updateProductSampleList() {
		log.info("<===Starts testReportBean.updateProductSampleList ========>"+stockInward);
		if(stockInward == null || stockInward.getId() == null) {
			productSampleList = new ArrayList<>();
			productSampleNumber = new StockTransferItems();
			return;
		}
		try {
			log.info("stockInward id is .........."+stockInward.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl()+"/stock/transfer/items/byitemid/"+stockInward.getId();
			BaseDTO response  = httpService.get(url); 
			if(response != null && response.getStatusCode() == 0) {
				log.info("list retrieved  successfully..........");
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productSampleList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferItems>>() {
				});
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured in updateProductSampleList...........",e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<===Ends testReportBean.updateProductSampleList ========>");
	}
	
	public void searchProductDetails() {
		log.info("<===Starts testReportBean.searchProductDetails ========>"+stockInward);
		if(stockInward == null || stockInward.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_INWARD_NUMBER_REQUIRED.getCode());
			return;
		}
		if(productSampleNumber == null || productSampleNumber.getId() == null) {
			errorMap.notify(ErrorDescription.PRODUCT_SAMPLE_NUMBER_REQUIRED.getCode());
			return;
		}
		testReport.setStockTransferItems(productSampleNumber);
		testReport.setProductVarietyMaster(productSampleNumber.getProductVarietyMaster());
		log.info("<===Ends testReportBean.searchProductDetails ========>"+stockInward.getId()+"\t Product sample number id... "+productSampleNumber.getId());
	}
	
	public void clearSearchFields() {
		log.info("<== Starts  testReportBean.clearSearchFields ===>");
		stockInward = new StockTransfer();
		productSampleNumber = new StockTransferItems();
		productSampleList = new ArrayList<>();
		testReport.setStockTransferItems(new StockTransferItems());
		testReport.setProductVarietyMaster(new ProductVarietyMaster());
		
		log.info("<== Ends testReportBean.clearSearchFields ===>");
	}
	
	public Boolean inputFieldsValidation() {
		log.info("<== Starts  testReportBean.inputFieldsValidation ===>");
		if(stockInward == null || stockInward.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_INWARD_NUMBER_REQUIRED.getCode());
			return false;
		}
		if(productSampleNumber == null || productSampleNumber.getId() == null) {
			errorMap.notify(ErrorDescription.PRODUCT_SAMPLE_NUMBER_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWarpCount() == null) {
			errorMap.notify(ErrorDescription.WARP_COUNT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWrapCountResult() == null) {
			errorMap.notify(ErrorDescription.WARP_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWeftCount() == null) {
			errorMap.notify(ErrorDescription.WEFT_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWeftCountResult() == null) {
			errorMap.notify(ErrorDescription.WEFT_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getEndsPerInch() == null) {
			errorMap.notify(ErrorDescription.ENDS_PERINCH_COUNT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getEndsPerInchResult() == null) {
			errorMap.notify(ErrorDescription.ENDS_PERINCH_RESULT_REQUIRED.getCode());
			return false;
		}
		//picks per inch result
		if(testReport.getEndsPerInch() == null) {
			errorMap.notify(ErrorDescription.PICKS_PERINCH_COUNT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getEndsPerInchResult() == null) {
			errorMap.notify(ErrorDescription.PICKS_PERINCH_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWarpShrinkPercentage() == null) {
			errorMap.notify(ErrorDescription.WARP_SHRINKAGE_PERCENT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWarpShrinkResult() == null) {
			errorMap.notify(ErrorDescription.WARP_SHRINKAGE_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWeftShrinkPercent() == null) {
			errorMap.notify(ErrorDescription.WEFT_SHRINKAGE_PERCENT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWeftShrinkResult() == null) {
			errorMap.notify(ErrorDescription.WEFT_SHRINKAGE_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getChangeColor() == null) {
			errorMap.notify(ErrorDescription.CHANGE_IN_COLOR_REQUIRED.getCode());
			return false;
		}
		if(testReport.getChageColorResult() == null) {
			errorMap.notify(ErrorDescription.CHANGE_IN_COLOR_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getStatiningCotton() == null) {
			errorMap.notify(ErrorDescription.STAINING_ON_COTTON_REQUIRED.getCode());
			return false;
		}
		if(testReport.getStainingCottonResult() == null) {
			errorMap.notify(ErrorDescription.STAINING_ON_COTTON_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getDry() == null) {
			errorMap.notify(ErrorDescription.DRY_REQUIRED.getCode());
			return false;
		}
		if(testReport.getDryResult() == null) {
			errorMap.notify(ErrorDescription.DRY_RESULT_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWet() == null) {
			errorMap.notify(ErrorDescription.WET_REQUIRED.getCode());
			return false;
		}
		if(testReport.getWetResult() == null) {
			errorMap.notify(ErrorDescription.WET_RESULT_REQUIRED.getCode());
			return false;
		}
		log.info("<== Ends testReportBean.inputFieldsValidation ===>");
		return true;
	}
	
	
}
