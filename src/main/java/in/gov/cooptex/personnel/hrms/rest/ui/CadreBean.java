package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CadreMaster;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("cadreBean")
public class CadreBean {


	private final String LIST_URL = "/pages/masters/listCadre.xhtml?faces-redirect=true;";

	private final String ADD_URL = "/pages/masters/addCadre.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();
	
	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	public String action = null;
	

	@Getter
	@Setter
	CadreMaster cadreMaster=new CadreMaster();
	
	@Getter
	@Setter
	PayScaleMaster payScaleMaster=new PayScaleMaster();
	
	@Setter
	@Getter
	List<PayScaleMaster> payScaleLists = new ArrayList<>();

	@Setter
	@Getter
	List<CadreMaster> cadreMastersLists = new ArrayList<>();
	
	@Setter
	@Getter
	String name;
	
	@Setter
	@Getter
	String lname;

	@Getter
	@Setter
	CadreMaster selectCadre=new CadreMaster();
	
	@Setter
	@Getter
	int listSizes;
	
	@Setter
	@Getter
	Long id;

	@Autowired
	ErrorMap errorMap;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;
	
	@PostConstruct
	public String init() {
		log.info("<<=====Inside init()======>");
		loadCadreMasterLists();
		loadPayScale();
		return LIST_URL;
	}

	
	public void loadCadreMasterLists(){
		log.info("=====loadPayScaleList====");
		BaseDTO baseDTO = new BaseDTO();
		String URL = serverURL + "/cadremaster/getallcadres";
		log.info("calling button" + URL);
		baseDTO = httpService.get(URL);
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			try {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				cadreMastersLists = mapper.readValue(jsonResponse, new TypeReference<List<CadreMaster>>() {
				});
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("No of cadreMastersLists ======= " + cadreMastersLists.size());
	

		if (cadreMastersLists != null)
			listSizes = cadreMastersLists.size();
		else
			listSizes = 0;
	}

	
	
	public String showCadreForm() {
		log.info("<<=====showPayScaleForm=====>>");

		if (action.equalsIgnoreCase("ADD")) {
			log.info("action :: " + action.toString());
			cadreMaster=new CadreMaster();
			name = null;
			lname =null;

		} else if (action.equalsIgnoreCase("EDIT")) {
			log.info("action :: " + action.toString());
			if (selectCadre == null) {
				log.info("===select atleast one  field");
				errorMap.notify(ErrorDescription.CADRE_SELECT_ERROR.getCode());
				return null;

			}
			
			cadreMaster=selectCadre;
			cadreMaster.setPayScaleMaster(selectCadre.getPayScaleMaster());

			
		} else if (action.equalsIgnoreCase("DELETE")) {
			log.info("action :: " + action.toString());
			if (selectCadre == null) {
				log.info("===select atleast one  field");
				errorMap.notify(ErrorDescription.CADRE_SELECT_ERROR.getCode());
				return null;
			}
//			deleteCadre();
			RequestContext.getCurrentInstance().execute("PF('confirmCadreDelete').show();");
			return null;
		}else if(action.equalsIgnoreCase("List")) {
			addButtonFlag = false;
			editButtonFlag = true;
			loadCadreMasterLists();
			return LIST_URL;
		}

		return ADD_URL;

	}
	
	
	public  String submitCadreForm(){
		log.info("====submitCadreForm======");
		try {
			if(action.equalsIgnoreCase("ADD")){
				log.info("===add cadre====");
				UserMaster userMaster = loginBean.getUserDetailSession();
				cadreMaster.setCreatedBy(userMaster);
				if(getPayScaleMaster().getId()!=null){
					cadreMaster.setId(cadreMaster.getPayScaleMaster().getId());
				}
				log.info("name =="+cadreMaster.getName());
				log.info("lame "+cadreMaster.getLname());

				cadreMaster.setCreatedDate(new Date());
				String URL = serverURL + "/cadremaster/createcadre";
				BaseDTO baseDTO = httpService.post(URL, cadreMaster);
				if(baseDTO!=null && baseDTO.getStatusCode() == 0){
					errorMap.notify(ErrorDescription.CADRE_SAVE_SUCCESS.getCode());
				}else if(baseDTO!=null && baseDTO.getStatusCode() != 0){
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}else if(action.equalsIgnoreCase("EDIT")){
				log.info("===update PayScale====");

				UserMaster userMaster = loginBean.getUserDetailSession();
				cadreMaster=selectCadre;
				cadreMaster.setModifiedBy(userMaster);
				String URL = serverURL + "/cadremaster/updatecadre";
				BaseDTO baseDTO = httpService.put(URL, cadreMaster);

				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.CADRE_UPDATE_SUCCESS.getCode());
				}else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		}catch (Exception e) {
			log.error("Exception occured in submit cadre.......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); 
			return null;
		}
		selectCadre = new CadreMaster();
		loadCadreMasterLists();
		//payScaleMaster=new PayScaleMaster();
		addButtonFlag = false;
		editButtonFlag = true;
		return  LIST_URL;
	}
	
	public void loadPayScale(){
		
		addButtonFlag = false;
		editButtonFlag = true;
		log.info("=====loadPayScaleList====");
		BaseDTO baseDTO = new BaseDTO();
		String URL = serverURL + "/payscale/getPayScale";
		log.info("calling button" + URL);
		baseDTO = httpService.get(URL);
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			try {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				payScaleLists = mapper.readValue(jsonResponse, new TypeReference<List<PayScaleMaster>>() {
				});
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("No of payScaleList ======= " + payScaleLists.size());

	}
	
	
	public String deleteCadre() {
		log.info("===delete=====");
		try {
			if (action.equalsIgnoreCase("DELETE")) {
				if (selectCadre == null) {
					errorMap.notify(ErrorDescription.CADRE_SELECT_ERROR.getCode());
					return null;
				}
				cadreMaster.setId(selectCadre.getId());
				log.info("===Id is == " + cadreMaster.getId());
				BaseDTO baseDTO = new BaseDTO();
				Long id = cadreMaster.getId();
				String URL = serverURL + "/cadremaster/delete/" + id;
				log.info("URL IS " + URL);

				baseDTO = httpService.delete(URL);
				log.info("==Status code is " + baseDTO.getStatusCode());
				log.info("Record deleted sussesfully");
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.CADRE_DELETED_SUCCESS.getCode());
					RequestContext.getCurrentInstance().execute("PF('confirmCadreDelete').hide();");
				}else if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		}catch(Exception e) {
			log.error("Exception occured in deleteCadre ....",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		selectCadre = new CadreMaster();
		loadPayScale();
		loadCadreMasterLists();
		return LIST_URL;
	}
	
	
	public String cancle() {
		name = null;
		lname = null;
		selectCadre = new CadreMaster();
		loadCadreMasterLists();
		addButtonFlag = false;
		editButtonFlag = true;
		return LIST_URL;

	}
	
	public void onRowSelect(SelectEvent event) {
		addButtonFlag = true;
		editButtonFlag = false;
	}
	
}

