/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.util.WeaversSocietyEnrollmentStatus;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeLogNoteSignatureDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyFieldVerificationDTO;
import in.gov.cooptex.core.dto.SocietyRegistrationReqDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.weavers.model.SocietyEnrollment;
import in.gov.cooptex.weavers.model.SocietyFieldVerification;
import in.gov.cooptex.weavers.model.SocietyFieldVerificationEmp;
import in.gov.cooptex.weavers.model.SocietyRegRequestLog;
import in.gov.cooptex.weavers.model.SocietyRegRequestNote;
import in.gov.cooptex.weavers.model.SocietyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("societyBoardApprovalBean")
@Scope("session")
@Log4j2
public class SocietyBoardApprovalBean {
	@Autowired
	AppPreference appPreference;

	private final String CREATE_BOARD_PAGE = "/pages/weavers/societyEnrollment/createBoardApproval.xhtml?faces-redirect=true;";
	private final String LIST_BOARD_PAGE = "/pages/weavers/societyEnrollment/listBoardApproval.xhtml?faces-redirect=true;";
	private final String VIEW_BOARD_PAGE = "/pages/weavers/societyEnrollment/viewBoardApproval.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	SocietyRegistrationRequest societyRegistrationRequest, selectedSocietyRegistration;
	@Getter
	@Setter
	List<SocietyRegistrationRequest> societyRegReqList = null;

	@Getter
	@Setter
	SocietyEnrollment societyEnrollment;
	@Getter
	@Setter
	List<SocietyEnrollment> societyEnrollmentList = null;

	@Getter
	@Setter
	SocietyFieldVerification societyFieldVerification;

	@Getter
	@Setter
	SocietyFieldVerification selectedsocietyFieldVerification;

	// ================Collecter list==========
	@Getter
	@Setter
	SocietyFieldVerificationEmp committeeMembers;

	@Getter
	@Setter
	List<SocietyFieldVerificationEmp> committeeMembersList = null;

	@Getter
	@Setter
	SocietyFieldVerification loomVerification;

	@Getter
	@Setter
	List<SocietyFieldVerification> loomVerificationList = null;
	// ==============================================

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action = "";

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	Boolean contentFlag = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	SocietyRegRequestNote societyRegReqNote;

	@Getter
	@Setter
	SocietyRegRequestLog societyRegReqLog;

	@Getter
	@Setter
	String remarkscomment;

	@Getter
	@Setter
	String stage;

	@Getter
	@Setter
	List<EmployeeLogNoteSignatureDTO> viewLogResponseList;

	@Getter
	@Setter
	EmployeeMaster loggedInEmployeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	String jsonResponse;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	Boolean finalApproval;

	public String showsocietyBoardApprovalListPage() {
		log.info("<==== Starts societyBoardApprovalBean-showSocietyFieldVerificationListPage =====>");
		mapper = new ObjectMapper();
		societyRegistrationLogList = new ArrayList<>();
		selectedSocietyRegistrationLog = new SocietyRegRequestLog();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		action = "";
		lazySocietyRegistrationList();
		log.info("<==== Ends societyBoardApproval-showSocietyFieldVerificationListPage =====>");
		return LIST_BOARD_PAGE;
	}

	public String societysBoardApprovalListPageAction() {
		log.info("<========inside societyBoardApproval.societysBoardApprovalListPageAction=======>");
		societyFieldVerification = new SocietyFieldVerification();
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("create societysBoardApprovalListPageAction called..");
				societyRegReqList = new ArrayList<>();
				societyRegistrationRequest = new SocietyRegistrationRequest();
				societyFieldVerification = new SocietyFieldVerification();
				contentFlag = false;
				societyRegReqList = loadSocietyRegReqbyDepfinalApproval();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("BOARD_APPROVAL");
				log.info("=======societyregreq List size=============>" + societyRegReqList.size());
				loadEmployeeLoggedInUserDetails();
				return CREATE_BOARD_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View Society Field Verificationcalled.."
						+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
				/* getsocietyFieldVerificationbyId(); */
				societyRegReqList = new ArrayList<>();
				societyRegistrationRequest = new SocietyRegistrationRequest();
				societyFieldVerification = new SocietyFieldVerification();
				societyEnrollment = new SocietyEnrollment();
				societyRegistrationRequest
						.setId(selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
				if (societyRegistrationRequest.getId() != null) {
					societyEnrollment = commonDataService
							.getSocietyEnrollmentbySocRegReqId(societyRegistrationRequest.getId());
					attached = 0;
					remaining = 0;
					if (societyEnrollment != null && societyEnrollment.getSocietyEnrollmentFilesList() != null)
						attached = societyEnrollment.getSocietyEnrollmentFilesList().size();
					remaining = total - attached;
					log.info("Total files ..." + total + "\t Attached..." + attached + "\t Remaining..." + remaining);
					getsocietyFieldVerificationbyReqId();
					societyRegReqValuebyId();
					if (societyRegReqNote.getUserMaster().getId().equals(loginBean.getUserDetailSession().getId())) {
						log.info("============approval flag===============");
						setApprovalFlag(true);
					}
					setFinalapprovalFlag(societyRegReqNote.getFinalApproval());
				}
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("BOARD_APPROVAL");

				return VIEW_BOARD_PAGE;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts bean.onRowSelect ========>" + event);
		selectedSocietyRegistrationLog = ((SocietyRegRequestLog) event.getObject());
		addButtonFlag = true;
		viewButtonFlag = false;
		log.info("<===Ends bean.onRowSelect ========>"
				+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
	}

	public String subString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
			subStringAddress = subStringAddress.substring(0, 19);
			subStringAddress = subStringAddress + "........";
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	@Getter
	@Setter
	int total = 12, attached = 0, remaining = 0;

	private StreamedContent file;

	@Getter
	@Setter
	private String fileName = "";

	public void loadAllrecords() {
		log.info("==============loadAllrecords===============");
		contentFlag = false;
		try {
			societyEnrollment = new SocietyEnrollment();
			if (societyRegistrationRequest.getId() != null) {
				societyEnrollment = commonDataService
						.getSocietyEnrollmentbySocRegReqId(societyRegistrationRequest.getId());
				attached = 0;
				remaining = 0;
				if (societyEnrollment != null && societyEnrollment.getSocietyEnrollmentFilesList() != null)
					attached = societyEnrollment.getSocietyEnrollmentFilesList().size();
				remaining = total - attached;
				log.info("Total files ..." + total + "\t Attached..." + attached + "\t Remaining..." + remaining);
				getsocietyFieldVerificationbyReqId();
				contentFlag = true;
				societyRegReqNote = new SocietyRegRequestNote();
				societyRegReqLog = new SocietyRegRequestLog();
			} else {
				return;
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
		log.info("==============societyEnrollment===============" + societyEnrollment);
	}

	public void getsocietyFieldVerificationbyReqId() {
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyFieldVerification/getSocietyFieldbyReqID/" + societyRegistrationRequest.getId();
			log.info("after Url declared====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			SocietyFieldVerificationDTO societyFieldVerificationDTO = new SocietyFieldVerificationDTO();

			selectedsocietyFieldVerification = new SocietyFieldVerification();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyFieldVerificationDTO = mapper.readValue(jsonResponse,
						new TypeReference<SocietyFieldVerificationDTO>() {
						});
				societyFieldVerification = societyFieldVerificationDTO.getSocietyFieldVerification();
				committeeMembersList = societyFieldVerificationDTO.getCommitteeMembersList();
				loomVerificationList = societyFieldVerificationDTO.getLoomVerificationList();
				log.info("value of date ----->" + societyFieldVerification.getDateOfVisit());
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	@Getter
	@Setter
	Boolean previousApproval = false;

	public void societyRegReqValuebyId() {
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societyregistrationrequest/getbyReqID/"
					+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId();
			log.info("after Url declared====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			SocietyRegistrationReqDTO societyRegReqDTO = new SocietyRegistrationReqDTO();

			selectedSocietyRegistration = new SocietyRegistrationRequest();
			societyRegReqNote = new SocietyRegRequestNote();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyRegReqDTO = mapper.readValue(jsonResponse, new TypeReference<SocietyRegistrationReqDTO>() {
				});

				ObjectMapper mapperEmployee = new ObjectMapper();
				String employeeDatajsonResponses = mapperEmployee.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapperEmployee.readValue(employeeDatajsonResponses,
						new TypeReference<List<EmployeeLogNoteSignatureDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());
				previousApproval = societyRegReqDTO.getSocietyRegRequestNote().getFinalApproval();
				selectedSocietyRegistration = societyRegReqDTO.getSocietyRegistrationRequest();
				societyRegReqNote = societyRegReqDTO.getSocietyRegRequestNote();
				societyRegReqLog = societyRegReqDTO.getSocietyRegRequestLog();

			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		setStage(value);
		setRemarkscomment("");
	}

	public void getDownloadFile(String path) {
		try {

			byte[] downloadName = null;
			try {
				File file = new File(path);

				downloadName = new byte[(int) file.length()];

				log.info("downloadName==>" + downloadName);

				FileInputStream fis = new FileInputStream(file);
				fis.read(downloadName); // read file into bytes[]
				fis.close();
			} catch (Exception e) {
				log.error("convertImage error==>" + e);
			}
			/*
			 * log.info(" Value :: " + value); if (value == null || value == null) {
			 * errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode()); return;
			 * }
			 */
			if (path.contains("uploaded")) {
				String filePathValue[] = path.split("uploaded/");
				fileName = filePathValue[1];
			}
			log.info(" FIle Name :: " + fileName);
			String ext = FilenameUtils.getExtension(fileName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "image/jpg", fileName);
			} else if (ext.contains("pdf")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "pdf", fileName);
			} else if (ext.contains("doc")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			} else if (ext.contains("docx")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			}
			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded file", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
	}

	public StreamedContent getFile() {
		return file;
	}

	public void getNote() {
		RequestContext context = RequestContext.getCurrentInstance();
		try {
			if (!societyRegReqNote.getNote().equalsIgnoreCase("") || societyRegReqNote.getNote() != null) {
				context.execute("PF('notedialog').hide();");
			} else {
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getCode());
				context.execute("PF('notedialog').show();");
			}
		} catch (Exception e) {
			log.error("Error Occured ::", e);
		}
	}

	public String saveSocietyBoardApproval() {
		log.info("inside BOARD APPROVAL Request()");
		try {

			Validate.objectNotNull(forwardTo, ErrorDescription.DEPARTMENT_APPROVAL_FORWARD_TO_EMPTY);
			Validate.assertFalse(societyRegReqNote.getNote().isEmpty(), ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY);
			Validate.objectNotNull(societyRegReqNote.getNote(), ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY);
			Validate.objectNotNull(societyRegistrationRequest, ErrorDescription.BOARD_APPROVAL_SOCIETY_REG_REQ_EMPTY);

			societyRegReqLog.setStage("BOARD_SUBMITTED");
			societyRegistrationRequest.setSocietyRegRequestLog(societyRegReqLog);

			societyRegReqNote.setUserMaster(forwardTo);
			societyRegReqNote.setFinalApproval(finalApproval);
			societyRegistrationRequest.setSocietyRegRequestNote(societyRegReqNote);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/saveBoardApproval";
			BaseDTO response = httpService.post(url, societyRegistrationRequest);
			if (response != null && response.getStatusCode() == 0) {
				log.info("selectedSocietyRegistration saved successfully..........");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_INSERTED_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Error Occured ::", e);
		}
		return showsocietyBoardApprovalListPage();
	}

	@Getter
	@Setter
	LazyDataModel<SocietyRegRequestLog> lazySocietyRegistrationLogList;
	@Getter
	@Setter
	List<SocietyRegRequestLog> societyRegistrationLogList = null;
	@Getter
	@Setter
	SocietyRegRequestLog selectedSocietyRegistrationLog;

	public void lazySocietyRegistrationList() {
		log.info("<===== Starts SocietyBoardApprovalBean.lazySocietyRegistrationList List ======>");
		String lazyType = "BOARD";
		String lazylistUrl = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/societyregistrationrequest/getallSocietyRegforApprovallazy/" + lazyType;

		lazySocietyRegistrationLogList = new LazyDataModel<SocietyRegRequestLog>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1951941586686529834L;

			@Override
			public List<SocietyRegRequestLog> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for lazySocietyRegistrationList List=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						societyRegistrationLogList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyRegRequestLog>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazySocietyRegistrationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + societyRegistrationLogList.size());
				return societyRegistrationLogList;
			}

			@Override
			public Object getRowKey(SocietyRegRequestLog res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyRegRequestLog getRowData(String rowKey) {
				try {
					for (SocietyRegRequestLog societyRegReqLog : societyRegistrationLogList) {
						if (societyRegReqLog.getId().equals(Long.valueOf(rowKey))) {
							selectedSocietyRegistrationLog = societyRegReqLog;
							return societyRegReqLog;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.lazySocietyRegistrationList List ======>");
	}

	public String statusUpdate() {
		log.info("inside statusUdate socreqreg------->" + societyRegistrationRequest.getId() + "====>"
				+ societyRegReqNote.getUserMaster().getId());
		try {

			Validate.objectNotNull(societyRegReqNote.getUserMaster(), ErrorDescription.BOARD_APPROVAL_FORWARD_TO_EMPTY);

			SocietyRegistrationReqDTO socRegReqDTO = new SocietyRegistrationReqDTO();
			societyRegReqLog.setRemarks(remarkscomment);
			societyRegReqLog.setStage(stage);
			socRegReqDTO.setSocietyRegRequestNote(societyRegReqNote);
			log.info("society note forwrd to value check------->"
					+ socRegReqDTO.getSocietyRegRequestNote().getUserMaster());
			socRegReqDTO.setSocietyRegRequestLog(societyRegReqLog);
			socRegReqDTO.setSocietyRegistrationRequest(selectedSocietyRegistration);
			if (societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_REJECTED)) {
				societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.BOARD_REJECTED);
			} else {
				if (societyRegReqNote.getFinalApproval() == true && previousApproval == true && societyRegReqLog
						.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_APPROVED)) {
					societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.BOARD_FINAL_APPROVED);
				} else if (societyRegReqNote.getFinalApproval() == true || societyRegReqNote.getFinalApproval() == false
						&& previousApproval == false && societyRegReqLog.getStage()
								.equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_APPROVED)) {
					societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.BOARD_APPROVED);
				}
			}
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/societyRegReqForwardStage";
			BaseDTO response = httpService.post(url, socRegReqDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_FINAL_APPROVED)
						|| societyRegReqLog.getStage()
								.equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_APPROVED)) {
					errorMap.notify(ErrorDescription.BOARD_APPROVAL_APPROVED_SUCCESSFULLY.getCode());
				} else if (societyRegReqLog.getStage()
						.equalsIgnoreCase(WeaversSocietyEnrollmentStatus.BOARD_REJECTED)) {
					errorMap.notify(ErrorDescription.BOARD_APPROVAL_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}

		return showsocietyBoardApprovalListPage();
	}

	public List<SocietyRegistrationRequest> loadSocietyRegReqbyDepfinalApproval() {
		log.info("<=== Starts SocietyBoardApprovalBean.loadSocietyRegReqbyDepfinalApproval ===>");
		List<SocietyRegistrationRequest> societyRegistrationRequestList = null;
		try {
			BaseDTO baseDTO = new BaseDTO();
			societyRegistrationRequestList = new ArrayList<>();
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/loadSocietyRegReqOfDepfinalApproved";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyRegistrationRequestList = mapper.readValue(jsonResponse,
						new TypeReference<List<SocietyRegistrationRequest>>() {
						});
			}
		} catch (Exception e) {
			log.error("Exception in SocietyBoardApprovalBean.loadSocietyRegReqbyDepfinalApproval >>>>>>>>" + e);
		}
		log.info("<=== Ends SocietyBoardApprovalBean.loadSocietyRegReqbyDepfinalApproval ===>");
		return societyRegistrationRequestList;
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("<<<<<<<<<<:::::Inside loadEmployeeLoggedInUserDetails():::::>>>>>>>>>");
		try {
			loggedInEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			if (loggedInEmployeeMaster == null) {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	@Getter
	@Setter
	public List<UserMaster> autoEmployeeMasterList = new ArrayList<UserMaster>();

	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(loginBean.getServerURL(), query);
		return autoEmployeeMasterList;
	}
	
}
