package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.ModernizationMBookEntry;
import in.gov.cooptex.asset.model.ModernizationReportItems;
import in.gov.cooptex.asset.model.ModernizationReportNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.MeasurementBookEntryDTO;
import in.gov.cooptex.dto.pos.ModernizationReportDTO;
import in.gov.cooptex.dto.pos.ModernizationRequestDTO;
import in.gov.cooptex.exceptions.AssetErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("measurementBookEntryBean")
public class MeasurementBookEntryBean extends CommonBean implements Serializable {

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/measurementBookEntryController";

	final String LIST_PAGE = "/pages/assetManagement/listMeasurementBookEntry.xhtml?faces-redirect=true";
	final String CREATE_PAGE = "/pages/assetManagement/measurementBookEntiry.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/assetManagement/viewMeasurementBookEntry.xhtml?faces-redirect=true";

	ObjectMapper objectMapper = new ObjectMapper();

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	LazyDataModel<ModernizationRequestDTO> modRequestLazyList;

	@Getter
	@Setter
	private ModernizationRequestDTO selectedModRequest = new ModernizationRequestDTO();

	@Getter
	@Setter
	private List<ModernizationRequestDTO> modRequestList;

	@Getter
	@Setter
	private List<ModernizationReportItems> modernizationReportItemsList = new ArrayList<ModernizationReportItems>();

	@Getter
	@Setter
	private List<MeasurementBookEntryDTO> measurementBookEntryDTOList = new ArrayList<MeasurementBookEntryDTO>();

	@Getter
	@Setter
	private MeasurementBookEntryDTO measurementBookEntryDTO = new MeasurementBookEntryDTO();

	@Getter
	@Setter
	private List<ModernizationMBookEntry> modernizationMBookEntryList = new ArrayList<ModernizationMBookEntry>();

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	private Date reportSubDate;

	@Getter
	@Setter
	private String note;

	@Getter
	@Setter
	ModernizationReportNote modernizationReportNote = new ModernizationReportNote();

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Getter
	@Setter
	Boolean submitButtonFlag = true;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	Boolean rejectButtonFlag = true;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@PostConstruct

	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		objectMapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("modernization_request_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("Selected circular iD:" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			measurementBookEntryDTO = new MeasurementBookEntryDTO();
			action = "VIEW";
			selectedModRequest.setReportId(Long.parseLong(circularId));
			selectedNotificationId = Long.parseLong(notificationId);
			showViewPage();
		}
		log.info("ModerizationBean showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public String loadList() {
		log.info("<==== Starts MeasurementBookEntryBean.loadList =====>");
		loadLazyModernizationRequestList();
		log.info("<==== End MeasurementBookEntryBean.loadList =====>");
		return LIST_PAGE;
	}

	public void loadLazyModernizationRequestList() {
		log.info("<==== Starts MeasurementBookEntryBean.loadLazyModernizationRequestList =====>");
		modRequestLazyList = new LazyDataModel<ModernizationRequestDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ModernizationRequestDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
						modRequestList = objectMapper.readValue(jsonResponse,
								new TypeReference<List<ModernizationRequestDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return modRequestList;
			}

			@Override
			public Object getRowKey(ModernizationRequestDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ModernizationRequestDTO getRowData(String rowKey) {
				for (ModernizationRequestDTO modernizationRequestDTO : modRequestList) {
					if (modernizationRequestDTO.getId().equals(Long.valueOf(rowKey))) {
						selectedModRequest = modernizationRequestDTO;
						return modernizationRequestDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends MeasurementBookEntryBean.loadLazyModernizationRequestList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts MeasurementBookEntryBean.onRowSelect ========>" + event);
		selectedModRequest = ((ModernizationRequestDTO) event.getObject());
		addButtonFlag = false;
		viewButtonFlag = true;
		editButtonFlag = true;
		if (ApprovalStage.MBOOKENTRY_REJECTED.equalsIgnoreCase(selectedModRequest.getStage())) {
			editButtonFlag = false;
		}

		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED")
				&& !loginBean.getUserMaster().getUsername().equals("hosuper")) {
			addButtonFlag = true;
			editButtonFlag = true;
			submitButtonFlag = true;
		}

		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			addButtonFlag = true;
			editButtonFlag = true;
			submitButtonFlag = false;

		}

		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED")
				&& !loginBean.getUserMaster().getUsername().equals("hosuper")) {
			addButtonFlag = true;
			editButtonFlag = true;
			submitButtonFlag = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			addButtonFlag = true;
			editButtonFlag = true;
			submitButtonFlag = true;
		}
//		if (ApprovalStage.FINAL_APPROVED.equalsIgnoreCase(selectedModRequest.getStage() ) || ApprovalStage.MBOOKENTRY_SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())) {
//			addButtonFlag = true;
//			editButtonFlag=false;
//		}

		if (selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			addButtonFlag = false;
			editButtonFlag = true;
			submitButtonFlag = true;

		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED")) {
			addButtonFlag = true;
			editButtonFlag = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRYFINAL-REJECTED")) {
			addButtonFlag = true;
			editButtonFlag = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-APPROVED")) {
			addButtonFlag = true;
			editButtonFlag = true;
		}
		log.info("<===Ends MeasurementBookEntryBean.onRowSelect ========>");
		log.info("boolenflaggg" + editButtonFlag);
	}

	public List<ModernizationReportItems> getReportItemListByReportId() {
		log.info("MeasurementBookEntryBean:getReportItemListByReportId Method Start====>");
		BaseDTO baseDTO = new BaseDTO();
		List<ModernizationReportItems> modernizationReportItemsList = new ArrayList<ModernizationReportItems>();
		try {
			log.info("MeasurementBookEntryBean:Report Id=", selectedModRequest.getReportId());
			if (selectedModRequest != null && selectedModRequest.getReportId() != null) {
				String url = AppUtil.getPortalServerURL() + "/modernizationreport/getreportItem/"
						+ selectedModRequest.getReportId();
				log.info("ModernizationReportBean:url==>", url);
				baseDTO = httpService.get(url);
				objectMapper = new ObjectMapper();
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
					modernizationReportItemsList = objectMapper.readValue(jsonResponse,
							new TypeReference<List<ModernizationReportItems>>() {
							});
				}
			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error In getReportItemListByReportId Method ==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:getReportItemListByReportId Method End====>");
		return modernizationReportItemsList;
	}

	public String getModenizationMbookEntry() {
		log.info("MeasurementBookEntryBean:getModenizationEbookEntry Method Start==>");
		try {
			action = "ADD";
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
				return null;
			}
			modernizationReportItemsList = getReportItemListByReportId();
			reportSubDate = modernizationReportItemsList.get(0).getReportSubmissionDate();
			if (selectedModRequest.getReportId() != null) {
				modernizationMBookEntryList = getMeasurementBookEntryByReportId();
				measurementBookEntryDTOList = setMBookEntryToMBookEntrydto(modernizationMBookEntryList);
			}
			if (measurementBookEntryDTOList == null || measurementBookEntryDTOList.isEmpty()) {
				measurementBookEntryDTOList = setReportItemsToMBookEntrydto(modernizationReportItemsList);
			}
			// forwardToUsersList=commonDataService.loadForwardToUsers();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REPORT_FOR_MODERNIZATION");
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error In getReportItemListByReportId Method ==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return CREATE_PAGE;
	}

	public String createModernizationMBookEntry() {
		log.info("MeasurementBookEntryBean:createModernizationMBookEntry Method Start");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (measurementBookEntryDTO.getNote() == null) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				return null;
			}
			if (measurementBookEntryDTO.getForwardto() != null) {
				measurementBookEntryDTO.setForwardToId(measurementBookEntryDTO.getForwardto().getId());
			}
			measurementBookEntryDTO.setReportId(selectedModRequest.getReportId());
			measurementBookEntryDTO.setMeasurementBookEntryDTOList(measurementBookEntryDTOList);
			String url = SERVER_URL + "/create";
			baseDTO = httpService.post(url, measurementBookEntryDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("MeasurementBookEntryBean Entry Created Successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(
								ErrorDescription.getError(AssetErrorCode.MEASUREMENT_BOOK_ENTEY_SAVE).getErrorCode());
					else
						errorMap.notify(
								ErrorDescription.getError(AssetErrorCode.MEASUREMENT_BOOK_ENTEY_UPDATE).getErrorCode());
					selectedModRequest = new ModernizationRequestDTO();
					return LIST_PAGE;

				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}

			}
			selectedModRequest = new ModernizationRequestDTO();

		}

		catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in createModernizationMBookEntry Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:createModernizationMBookEntry Method End");
		return null;
	}

	public List<MeasurementBookEntryDTO> setReportItemsToMBookEntrydto(
			List<ModernizationReportItems> modernizationReportItemsList) {
		log.info("MeasurementBookEntryBean:setReportItemsToMBookEntrydto Method Start");
		MeasurementBookEntryDTO modernizationMBookEntryDTO = null;
		List<MeasurementBookEntryDTO> measurementBookEntryDTOList = new ArrayList<MeasurementBookEntryDTO>();
		try {
			for (ModernizationReportItems modernizationReportItems : modernizationReportItemsList) {
				modernizationMBookEntryDTO = new MeasurementBookEntryDTO();
				modernizationMBookEntryDTO
						.setItemDescription(modernizationReportItems.getModernizationEstimation().getItemDescription());
				modernizationMBookEntryDTO.setRate(modernizationReportItems.getModernizationEstimation().getRate());
				modernizationMBookEntryDTO
						.setUomMaster(modernizationReportItems.getModernizationEstimation().getUomMaster());
				modernizationMBookEntryDTO.setReportId(modernizationReportItems.getModernizationReport().getId());
				modernizationMBookEntryDTO.setPreMeasurementAmt(0.0);
				modernizationMBookEntryDTO.setPreQuantity(0.0);
				measurementBookEntryDTOList.add(modernizationMBookEntryDTO);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in setReportItemsToMBookEntrydto Method ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:setReportItemsToMBookEntrydto Method End");
		return measurementBookEntryDTOList;
	}

	public String showViewPage() {
		log.info("MeasurementBookEntryBean:showViewPage Method Start===>");
		action = "VIEW";
		if (selectedModRequest == null) {
			errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
			return null;
		}

		log.info("statusss" + selectedModRequest.getStage());
		if (loginBean.getUserMaster().getUsername().equals("hosuper")) {
			editButtonFlag = true;

		}

//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED") ) {
//			editButtonFlag=true;
//			submitButtonFlag=true;
//		}
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED") ) {
//			editButtonFlag=true;
//			submitButtonFlag=false;
//			hideflag=false;
//		}
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//			editButtonFlag=true;
//		}
//		log.info("forwarddddd" + measurementBookEntryDTO.getForwardToId() );
//		
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//			editButtonFlag=true;
//		}
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED") && !loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//			editButtonFlag=false;
//			hideflag=true;
//		}
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//			editButtonFlag=true;
//		}
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED") && !loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//			editButtonFlag=true;
//			hideflag=true;
//		}

		if (measurementBookEntryDTO.getForwardToId() == null) {

			editButtonFlag = true;
		}

//		 if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//				
//				submitButtonFlag=false;
//				hideflag=false;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && !loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//				submitButtonFlag=true;
//				hideflag=true;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")) {
//				submitButtonFlag=true;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("APPROVED")) {
//					submitButtonFlag=false;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("REJECTED")) {
//					submitButtonFlag=false;
//				}

//		if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED") ) {
//			approveButtonFlag=true;
//		}
		modernizationReportItemsList = getReportItemListByReportId();
		reportSubDate = modernizationReportItemsList.get(0).getReportSubmissionDate();
		modernizationMBookEntryList = getMeasurementBookEntryByReportId();
		measurementBookEntryDTOList = setMBookEntryToMBookEntrydto(modernizationMBookEntryList);
		// forwardToUsersList=commonDataService.loadForwardToUsers();

		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REPORT_FOR_MODERNIZATION");
		getModernizationReportNoteDetailsByReportId();
		measurementBookEntryDTO.setReportId(selectedModRequest.getReportId());
//		if (ApprovalStage.MBOOKENTRY_REJECTED.equalsIgnoreCase(selectedModRequest.getStage()) 
//				|| ApprovalStage.MBOOKENTRY_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())
//				||ApprovalStage.MBOOKENTRY_SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())
//				||ApprovalStage.MBOOKENTRY_FINAL_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())) {
//		if(modernizationReportNote.getForwardTo()!=null) {
//			if (modernizationReportNote.getForwardTo().getId() == loginBean.getUserMaster().getId()) {
//			
//				approveButtonFlag = false;
//	
//				rejectButtonFlag = false;
//			}
//			
//		}
//		}
		if (!modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {

			submitButtonFlag = false;

			hideflag = false;
		}

		if (modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {

			submitButtonFlag = true;

			hideflag = true;
		}
		if (modernizationReportNote.getFinalApproval().equals(true)
				&& modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
			submitButtonFlag = true;

			hideflag = false;
			finalapprovalFlag = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED")) {
			submitButtonFlag = false;
			hideflag = false;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-REJECTED")) {
			submitButtonFlag = false;
			hideflag = false;
		}

		return VIEW_PAGE;
	}

	public List<ModernizationMBookEntry> getMeasurementBookEntryByReportId() {
		log.info("MeasurementBookEntryBean:viewMeasurementBookEntry Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		List<ModernizationMBookEntry> modernizationMBookEntryList = new ArrayList<ModernizationMBookEntry>();
		try {
			String url = SERVER_URL + "/view/" + selectedModRequest.getReportId();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
				modernizationMBookEntryList = objectMapper.readValue(jsonResponse,
						new TypeReference<List<ModernizationMBookEntry>>() {
						});
			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in viewMeasurementBookEntry Method ===>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:viewMeasurementBookEntry Method End===>");
		return modernizationMBookEntryList;
	}

	public List<MeasurementBookEntryDTO> setMBookEntryToMBookEntrydto(
			List<ModernizationMBookEntry> modernizationMBookEntryList) {
		log.info("MeasurementBookEntryBean:setMBookEntryToMBookEntrydto Method Start");
		MeasurementBookEntryDTO modernizationMBookEntryDTO = null;
		List<MeasurementBookEntryDTO> measurementBookEntryDTOList = new ArrayList<MeasurementBookEntryDTO>();
		try {
			for (ModernizationMBookEntry modernizationMBookEntry : modernizationMBookEntryList) {
				modernizationMBookEntryDTO = new MeasurementBookEntryDTO();
				modernizationMBookEntryDTO.setId(modernizationMBookEntry.getId());
				modernizationMBookEntryDTO.setItemDescription(modernizationMBookEntry.getDescriptionOfWork());
				modernizationMBookEntryDTO.setReportId(modernizationMBookEntry.getModernizationReport().getId());
				modernizationMBookEntryDTO.setRate(modernizationMBookEntry.getRate());
				modernizationMBookEntryDTO.setUomMaster(modernizationMBookEntry.getUomMaster());
				if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("EDIT")) {
					if (modernizationMBookEntry.getNetQuantity() != null) {
						modernizationMBookEntryDTO
								.setPreQuantity(Double.valueOf(modernizationMBookEntry.getNetQuantity()));
					}
					if (modernizationMBookEntry.getNetValue() != null) {
						modernizationMBookEntryDTO
								.setPreMeasurementAmt(Double.valueOf(modernizationMBookEntry.getNetValue()));
					}
				}

				if (action.equalsIgnoreCase("VIEW")) {
					modernizationMBookEntryDTO.setMeasurementDate(modernizationMBookEntry.getMeasurementDate());
					modernizationMBookEntryDTO.setContentofArea(modernizationMBookEntry.getContentOrArea());
					modernizationMBookEntryDTO.setNumber(modernizationMBookEntry.getNumber());
					modernizationMBookEntryDTO.setLength(modernizationMBookEntry.getLength());
					modernizationMBookEntryDTO.setBreath(modernizationMBookEntry.getBreadth());
					modernizationMBookEntryDTO.setDepth(modernizationMBookEntry.getDepth());
					modernizationMBookEntryDTO.setToalValue(modernizationMBookEntry.getTotalValue());
					modernizationMBookEntryDTO.setRemark(modernizationMBookEntry.getRemarks());
					modernizationMBookEntryDTO.setPreQuantity(modernizationMBookEntry.getPreviousQuantity());
					modernizationMBookEntryDTO.setPreMeasurementAmt(modernizationMBookEntry.getPreviousAmount());
					modernizationMBookEntryDTO.setNetQuantity(modernizationMBookEntry.getNetQuantity());
					modernizationMBookEntryDTO.setNetValue(modernizationMBookEntry.getNetValue());
				}
				measurementBookEntryDTOList.add(modernizationMBookEntryDTO);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in setMBookEntryToMBookEntrydto Method ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:setMBookEntryToMBookEntrydto Method End");
		return measurementBookEntryDTOList;
	}

	public void calculateNumber(MeasurementBookEntryDTO bookEntry) {
		log.info("MeasurementBookEntryBean:calculateNumber Method Start");
		try {
			if (measurementBookEntryDTOList.contains(bookEntry)) {

				bookEntry.setContentofArea(bookEntry.getNumber().toString());
				Double tot = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getRate();
				bookEntry.setToalValue(tot);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in calculateNumber Method==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:calculateNumber Method End");
	}

	public void calculateLength(MeasurementBookEntryDTO bookEntry) {
		log.info("MeasurementBookEntryBean:calculateLength Method Start");
		try {
			if (measurementBookEntryDTOList.contains(bookEntry)) {
				Double len = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getLength();
				bookEntry.setContentofArea(len.toString());
				Double tot = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getRate();
				bookEntry.setToalValue(tot);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in calculateLength Method==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:calculateLength Method End");
	}

	public void calculateBreath(MeasurementBookEntryDTO bookEntry) {
		log.info("MeasurementBookEntryBean:calculateBreath Method Start");
		try {
			if (measurementBookEntryDTOList.contains(bookEntry)) {
				Double breath = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getBreath();
				bookEntry.setContentofArea(breath.toString());
				Double tot = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getRate();
				bookEntry.setToalValue(tot);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in calculateBreath Method End");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:calculateBreath Method End");
	}

	public void calculateDepth(MeasurementBookEntryDTO bookEntry) {
		log.info("MeasurementBookEntryBean:calculateDepth Method Start");
		try {
			if (measurementBookEntryDTOList.contains(bookEntry)) {
				Double depth = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getDepth();
				bookEntry.setContentofArea(depth.toString());
				Double tot = Double.valueOf(bookEntry.getContentofArea()) * bookEntry.getRate();
				bookEntry.setToalValue(tot);

			}
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in calculateDepth Method End");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:calculateDepth Method End");
	}

	public String cancel() {
		log.info("MeasurementBookEntryBean:cancel Method Start===>");
		measurementBookEntryDTO = new MeasurementBookEntryDTO();
		note = "";
		action = "";
		editButtonFlag = true;
		selectedModRequest = new ModernizationRequestDTO();
		log.info("MeasurementBookEntryBean:cancel Method End===>");

		return LIST_PAGE;
	}

	public String deleteBookEntry() {
		log.info("MeasurementBookEntryBean:delete Method Start===>");
		action = "DELETE";
		if (selectedModRequest == null) {
			errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
			return null;
		}
		deleteMeasurementBookEntry();
		return LIST_PAGE;
	}

	public void deleteMeasurementBookEntry() {
		log.info("MeasurementBookEntryBean:deleteMeasurementBookEntry Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = SERVER_URL + "/delete";
			baseDTO = httpService.post(url, selectedModRequest);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AssetErrorCode.MEASUREMENT_BOOK_ENTEY_DELETE).getErrorCode());
				log.info("Deleted Successfully -----------------");
				RequestContext.getCurrentInstance().update("growls");

			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In deleteMeasurementBookEntry Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:calculateDepth Method End===>");
	}

	public String showEditPage() {
		log.info("MeasurementBookEntryBean:showEditPage Method Start===>");
		action = "EDIT";
		if (selectedModRequest == null) {
			errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
			return null;
		}
		modernizationReportItemsList = getReportItemListByReportId();
		reportSubDate = modernizationReportItemsList.get(0).getReportSubmissionDate();
		modernizationMBookEntryList = getMeasurementBookEntryByReportId();
		measurementBookEntryDTOList = setMBookEntryToMBookEntrydto(modernizationMBookEntryList);
		forwardToUsersList = commonDataService.loadForwardToUsers();
		getModernizationReportNoteDetailsByReportId();
		return CREATE_PAGE;
	}

	public String clear() {
		log.info("MeasurementBookEntryBean:clear Method Start");
		selectedModRequest = new ModernizationRequestDTO();
		log.info("MeasurementBookEntryBean:clear Method End");
		return LIST_PAGE;
	}

	public void createNote() {
		log.info("MeasurementBookEntryBean:createNote Method Start==>");
		measurementBookEntryDTO.setNote(note);
		log.info("MeasurementBookEntryBean:createNote Method End==>");
	}

	public void getModernizationReportNoteDetailsByReportId() {
		log.info("MeasurementBookEntryBean:getModernizationReportNoteDetailsByReportId Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("MeasurementBookEntryBean:Report Id=", selectedModRequest.getReportId());
			String url = AppUtil.getPortalServerURL() + "/modernizationreport/getmeasurementBookLog/"
					+ selectedModRequest.getReportId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			log.info("ModernizationReportBean:url==>", url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
//				modernizationReportNote = objectMapper.readValue(jsonResponse,
//						new TypeReference<ModernizationReportNote>() {
//						});
				List<ModernizationReportDTO> modernizationReportDTOList = objectMapper.readValue(jsonResponse,
						new TypeReference<List<ModernizationReportDTO>>() {
						});
				if (selectedNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}

				if (!CollectionUtils.isEmpty(modernizationReportDTOList)) {
					ModernizationReportDTO modernizationReportDTO = modernizationReportDTOList.get(0);
					if (modernizationReportDTO != null) {
						modernizationReportNote = modernizationReportDTO.getModernizationReportNote();
						selectedModRequest.setStage(modernizationReportDTO.getStage());
						selectedModRequest.setHeadRegionOffice(modernizationReportDTO.getHeadRegionOffice());
						selectedModRequest.setEntityType(modernizationReportDTO.getEntityType());
						selectedModRequest.setEntity(modernizationReportDTO.getEntity());
						selectedModRequest.setNameOfBuinding(modernizationReportDTO.getNameOfBuinding());
					}
				}
				String jsonResponse1 = objectMapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterData = objectMapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
				});
			}
			note = modernizationReportNote.getNote();
			if (modernizationReportNote.getForwardTo() != null) {
				measurementBookEntryDTO.setForwardToId(modernizationReportNote.getForwardTo().getId());
			}
			measurementBookEntryDTO.setForwardfor(modernizationReportNote.getFinalApproval());
			measurementBookEntryDTO.setNote(note);
			previousApproval = modernizationReportNote.getFinalApproval();
		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error in approveModernizationReport Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:getModernizationReportNoteDetailsByReportId Method End===>");
	}

	public String approveMeasurementBookEntry() {
		log.info("MeasurementBookEntryBean:approveMeasurementBookEntry Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (measurementBookEntryDTO.getForwardto() != null) {
				measurementBookEntryDTO.setForwardToId(measurementBookEntryDTO.getForwardto().getId());
			}
			if (previousApproval == false && measurementBookEntryDTO.getForwardfor() == true) {
				measurementBookEntryDTO.setStage(ApprovalStage.MBOOKENTRY_APPROVED);
			} else if (previousApproval == true && measurementBookEntryDTO.getForwardfor() == true) {
				measurementBookEntryDTO.setStage(ApprovalStage.MBOOKENTRY_FINAL_APPROVED);
			} else {
				measurementBookEntryDTO.setStage(ApprovalStage.MBOOKENTRY_APPROVED);
			}
			String url = SERVER_URL + "/approve";
			baseDTO = httpService.post(url, measurementBookEntryDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(AssetErrorCode.MEASUREMENT_BOOK_ENTEY_APPROVE).getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				loadList();
				return LIST_PAGE;

			}

		} catch (Exception exp) {
			log.info("MeasurementBookEntryBean:Error In approveMeasurementBookEntry Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:approveMeasurementBookEntry Method End====>");
		return null;
	}

	public String rejectMeasurementBookEntry() {
		log.info("MeasurementBookEntryBean:rejectMeasurementBookEntry Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			measurementBookEntryDTO.setStage(ApprovalStage.MBOOKENTRY_REJECTED);
			String url = SERVER_URL + "/reject";
			baseDTO = httpService.post(url, measurementBookEntryDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AssetErrorCode.MEASUREMENT_BOOK_ENTEY_REJECT).getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return LIST_PAGE;
			}
		} catch (Exception ex) {
			log.info("MeasurementBookEntryBean:Error In rejectMeasurementBookEntry Method", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("MeasurementBookEntryBean:rejectMeasurementBookEntry Method End===>");
		return null;
	}
}
