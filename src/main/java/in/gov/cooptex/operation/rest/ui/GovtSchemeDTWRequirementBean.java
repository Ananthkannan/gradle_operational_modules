package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
//import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.GovtSchemeDTWRequestDTO;
import in.gov.cooptex.core.dto.GovtSchemeDTWResponseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StatusMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.enums.DAndpProcurementPlanStatus;
import in.gov.cooptex.operation.enums.GovtSchemePlanStatus;
import in.gov.cooptex.operation.model.GovtDtwRequirementsNote;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemePlanItems;
import in.gov.cooptex.operation.model.GovtSchemeType;
import in.gov.cooptex.operation.production.dto.GovtSchemeDTWRequirementDTO;
import in.gov.cooptex.operation.production.dto.GovtSchemeDTWRequirementDetails;
import in.gov.cooptex.operation.production.dto.GovtSchemeDTWRequirementDetailsDTO;
import in.gov.cooptex.operation.production.model.GovtDtwRequirements;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("govtSchemeDTWRequirementBean")
@Scope("session")
public class GovtSchemeDTWRequirementBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/govtSchemeProduction/listDistrictTalukWiseRequirement-New.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/govtSchemeProduction/createGSDistTalukWiseRequirement-Upload.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/govtSchemeProduction/viewDistrictTalukWiseRequirement.xhtml?faces-redirect=true;";

	@Autowired
	LanguageBean languageBean;

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Setter
	@Getter
	StreamedContent excelFile;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	String districtNames;

	@Getter
	@Setter
	String forwardFor;

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlan;

	@Getter
	@Setter
	List<GovtSchemePlan> govtSchemePlanList;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	LazyDataModel<GovtSchemeDTWResponseDTO> responseList;

	@Getter
	@Setter
	List<GovtSchemeDTWRequirementDetails> govtSchemeDTWRequirementDetailsList;

	@Getter
	@Setter
	List<GovtSchemePlan> planList;

	@Getter
	@Setter
	Long govtSchemePlanId;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String employeeName;

	@Getter
	@Setter
	UploadedFile file;

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	GovtSchemePlanItems govtSchemePlanItems;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<DistrictMaster> selectedDistrictList;

	@Getter
	@Setter
	List<DistrictMaster> districtList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	GovtSchemeDTWResponseDTO govtSchemeDTWResponseDTO;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String SERVER_URL;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean showForwardUser = false;

	@Getter
	@Setter
	GovtDtwRequirementsNote govtDtwRequirementsNote = new GovtDtwRequirementsNote();

	@Getter
	@Setter
	GovtDtwRequirementsNote lastNote = new GovtDtwRequirementsNote();

	@Getter
	@Setter
	List<GovtDtwRequirementsNote> noteList;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	CircleMaster circleMaster;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	StatusMaster statusMaster;

	@Getter
	@Setter
	List<StatusMaster> statusMasterList;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<GovtDtwRequirements> requirementList;

	@Getter
	@Setter
	private String currentPlanStatus = "SUBMITTED";

	@Getter
	@Setter
	String comments;

	@Getter
	@Setter
	String dialogHeader;

	@Getter
	@Setter
	Set<String> schemePeriodList = new LinkedHashSet<String>();

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String disableAction = null;

	@Getter
	@Setter
	List<GovtSchemeType> govtSchemeTypeList;

	@Getter
	@Setter
	GovtSchemeType govtSchemeType;

	@Getter
	@Setter
	String schemePeriodCode;

	@Getter
	@Setter
	private Date planCreatedFrom;

	@Getter
	@Setter
	private Date planCreatedTo;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	Set<GovtSchemePlanItems> planItems;

	@Getter
	@Setter
	Map<String, Set<GovtSchemePlanItems>> circleMap;

	@Getter
	@Setter
	List<GovtSchemeDTWResponseDTO> viewLogResponseList;

	private Long govtschemeDTWrequirmentId;

	private Long notificationId;

	public GovtSchemeDTWRequirementBean() {
		log.info("Inside FreeDistributionSystemBean()>>>>>>>>>");
	}

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>");
		loadUrl();
		loadStatusValues();
		productCategoryList = commonDataService.loadProductCategories();
		circleMasterList = commonDataService.loadCircles();
		govtSchemeDTWResponseDTO = new GovtSchemeDTWResponseDTO();
		// for notification
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String notificationId = "";

		if (requestObj instanceof HttpServletRequest) {

			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			notificationId = httpRequest.getParameter("id");
			String notifiId = httpRequest.getParameter("notificationId");
			log.info("Selected  Id :" + notificationId);
			log.info("Notification" + notifiId);

			if (notificationId != null) {
				action = "VIEW";
				this.govtschemeDTWrequirmentId = Long.valueOf(notificationId);
				this.notificationId = Long.valueOf(notifiId);
				govtSchemeDTWResponseDTO.setId(govtschemeDTWrequirmentId);
				getViewPage();
			}
		}
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	@Getter
	@Setter
	List statusValues;

	@SuppressWarnings("unchecked")
	private void loadStatusValues() {
		Object[] statusArray = DAndpProcurementPlanStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			String st = status.toString();
			if (!st.equalsIgnoreCase("INITIATED")) {
				statusValues.add(status);
			}
		}
		statusValues.add("FINAL_APPROVED");
		log.info("<--- statusValues ---> " + statusValues);
	}

	public String getAddPage() {

		govtSchemePlan = new GovtSchemePlan();

		govtSchemeType = new GovtSchemeType();

		getAllGovtSchemeType();

		loadEmployeeLoggedInUserDetails();

		selectedDistrictList = new ArrayList<>();

		govtDtwRequirementsNote = new GovtDtwRequirementsNote();

		groupOneTotal = null;

		groupTwoTotal = null;

		getAllDistrict();

		loadForwardToUsers();

		govtSchemeDTWRequirementDetailsList = new ArrayList<>();

		fileName = null;
		districtNames = "";

		return ADD_PAGE_URL;
	}

	public String getListPage() {

		govtSchemeDTWResponseDTO = new GovtSchemeDTWResponseDTO();
		loadStatusValues();
		loadLazyList();

		return LIST_PAGE_URL;
	}

	public void getAllGovtSchemeType() {

		String url = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/govtSchemePlan/dtwrequirement/getallgovtschemetype";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			govtSchemeTypeList = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemeType>>() {
			});
			log.info("govtSchemeTypeList", govtSchemeTypeList);
		} catch (Exception exp) {
			log.error("Exception in getAllGovtSchemeType method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String getViewPage() {

		if (govtSchemeDTWResponseDTO == null) {
			errorMap.notify(ErrorDescription.SELECT_COMPASSIONATE_APPOINTMENT.getCode());
			return null;
		}

		govtDtwRequirementsNote = new GovtDtwRequirementsNote();

		enableFlag = false;

		govtSchemeDTWRequirementDetailsList = new ArrayList<>();

		notificationId = notificationId == null ? 0 : notificationId;

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/getallgovtreqs";
		GovtSchemeDTWRequirementDTO request = new GovtSchemeDTWRequirementDTO();

		GovtSchemeDTWRequirementDetailsDTO govtSchemeDTWRequirementDetailsDTO = new GovtSchemeDTWRequirementDetailsDTO();
		request.setId(govtSchemeDTWResponseDTO.getId());

		if (govtschemeDTWrequirmentId != null && govtschemeDTWrequirmentId != 0) {
			request.setId(govtschemeDTWrequirmentId);
			request.setNotificationId(notificationId);
		}
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, request);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			govtSchemeDTWRequirementDetailsDTO = mapper.readValue(jsonResponse,
					GovtSchemeDTWRequirementDetailsDTO.class);
			govtSchemeDTWRequirementDetailsList = govtSchemeDTWRequirementDetailsDTO.getRequirementList();
			govtSchemePlan = govtSchemeDTWRequirementDetailsDTO.getGovtSchemePlan();
			loadForwardToUsers();

			noteList = govtSchemeDTWRequirementDetailsDTO.getNoteList();

			Collections.sort(noteList, new ComparatorId());

			lastNote = noteList.get(noteList.size() - 1);

			govtDtwRequirementsNote.setFinalApproval(lastNote.getFinalApproval());

			govtDtwRequirementsNote.setNote(lastNote.getNote());

			if (lastNote.getForwardTo() != null
					&& lastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
				enableFlag = true;
			}

			eventLogList = new ArrayList<>();

			approvedEventLogList = new ArrayList<>();

			rejectedEventLogList = new ArrayList<>();

			/*
			 * if (govtSchemeDTWRequirementDetailsDTO.getLogList() != null &&
			 * !govtSchemeDTWRequirementDetailsDTO.getLogList().isEmpty()) { for
			 * (GovtDtwRequirementsLog planLog :
			 * govtSchemeDTWRequirementDetailsDTO.getLogList()) {
			 * 
			 * EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
			 * planLog.getCreatedByDesignation(), planLog.getCreatedDate(),
			 * planLog.getRemarks());
			 * if(planLog.getStage().equals(PlanStage.INITIATED.toString()) ||
			 * planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
			 * eventLog.setTitle("Created By"); } else
			 * if(planLog.getStage().equals(PlanStage.APPROVED.toString())){
			 * eventLog.setTitle("Approved By"); approvedEventLogList.add(eventLog); } else
			 * if(planLog.getStage().equals(PlanStage.REJECTED.toString())){
			 * eventLog.setTitle("Rejected By"); rejectedEventLogList.add(eventLog); } else
			 * if(planLog.getStage().equals(GovtSchemePlanStatus.FINAL_APPROVED.toString()))
			 * { eventLog.setTitle("Final Approved By"); approvedEventLogList.add(eventLog);
			 * } eventLogList.add(eventLog);
			 * 
			 * } }
			 * 
			 * log.info("eventLogList", eventLogList);
			 */
			String employeeDatajsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
			viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
					new TypeReference<List<GovtSchemeDTWResponseDTO>>() {
					});
			log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size()
					+ viewLogResponseList);

			/*
			 * Collections.sort(eventLogList, new Comparator<EventLog>() { public int
			 * compare(EventLog m1, EventLog m2) { return
			 * m1.getTitle().compareTo(m2.getTitle()); } });
			 */

			if (govtSchemeDTWResponseDTO.getGovtSchemePlanStatus()
					.equals(GovtSchemePlanStatus.FINAL_APPROVED.toString())
					|| govtSchemeDTWResponseDTO.getGovtSchemePlanStatus()
							.equals(GovtSchemePlanStatus.REJECTED.toString())) {
				enableFlag = false;
			}

			submit();
			log.info("govtSchemeDTWRequirementDetailsList", govtSchemeDTWRequirementDetailsList);
		} catch (Exception exp) {
			log.error("Exception in getViewPage method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return VIEW_PAGE_URL;
	}

	// inner class
	static class ComparatorId implements Comparator<GovtDtwRequirementsNote> {
		@Override
		public int compare(GovtDtwRequirementsNote obj1, GovtDtwRequirementsNote obj2) {
			return obj1.getId().compareTo(obj2.getId());
		}
	}

	public void onChangeGovtSchemeType() {

		String url = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/govtSchemePlan/dtwrequirement/getallgovtschemeplan";
		GovtSchemeDTWRequirementDTO request = new GovtSchemeDTWRequirementDTO();
		request.setGovtSchemeType(govtSchemeType);
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, request);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			planList = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemePlan>>() {
			});
			log.info("planList", planList);
		} catch (Exception exp) {
			log.error("Exception in onChangeGovtSchemeType method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void getAllDistrict() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/getalldistrict";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			districtList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
			});
			log.info("districtList", districtList);
		} catch (Exception exp) {
			log.error("Exception in getAllDistrict method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void changedistrictNames() {
		log.info("govtDtwRequirementsBean changeDistricts method  started");
		districtNames = "";
		for (DistrictMaster region : selectedDistrictList) {
			if (districtNames != "") {
				districtNames = districtNames + ",<br/>";
			}
			districtNames = districtNames + region.getCode() + "/" + region.getName();
		}
		log.info("govtDtwRequirementsBean changeDistricts method completed : " + districtNames);
	}

	public String saveDTWRequirement() {

		if (govtDtwRequirementsNote.getForwardTo() == null) {
			log.info("PLEASE_SELECT_FORWARD_TO Error");
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getCode());
			return null;
		}

		if (govtDtwRequirementsNote.getFinalApproval() == null) {
			log.info("PLEASE_SELECT_FORWARD_FOR Error");
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getCode());
			return null;
		}

		GovtSchemeDTWRequirementDTO govtSchemeDTWRequirementDTO = new GovtSchemeDTWRequirementDTO();

		govtSchemeDTWRequirementDTO.setRequirementList(govtSchemeDTWRequirementDetailsList);

		log.info("<====== govtSchemeDTWRequirementDetailsList size =====> "
				+ govtSchemeDTWRequirementDetailsList.size());

		log.info("govtSchemePlan " + govtSchemePlan);

		govtSchemeDTWRequirementDTO.setPlanId(govtSchemePlan.getId());

		log.info("<====== govtSchemePlan plan id =====> " + govtSchemePlan.getId());

		govtSchemeDTWRequirementDTO.setNoteEntity(govtDtwRequirementsNote);

		log.info("<====== govtDtwRequirementsNote =====> " + govtDtwRequirementsNote);

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/create";
		BaseDTO baseDTO = null;
		try {

			baseDTO = httpService.post(url, govtSchemeDTWRequirementDTO);
			log.info("<====== baseDTO =====> " + baseDTO);
			log.info("<====== baseDTO statuscode=====> " + baseDTO.getStatusCode());
		} catch (Exception exp) {
			log.info("Exception in saveDTWRequirement method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("saveDTWRequirement Saved successfully");
				errorMap.notify(ErrorDescription.DISTRICT_TALUK_SAVED_SUCCESSFULLY.getCode());
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} else {
			log.info("failiure in saveDTWRequirement method ");
		}
		return LIST_PAGE_URL;
	}

	private GovtSchemeDTWRequestDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		GovtSchemeDTWRequestDTO request = new GovtSchemeDTWRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("stockInwardNumber")) {
				log.info("stock Inward Number : " + value);
				request.setSchemeCodeOrName(value);
			}

			if (entry.getKey().equals("senderCodeOrName")) {
				log.info("Sender Code Or Name : " + value);
				request.setPlanCodeOrName(value);
			}
			if (entry.getKey().equals("receiverCodeOrName")) {
				log.info("Receiver Code Or Name : " + value);
				request.setPlanFromDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("status")) {
				request.setPlanToDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("receiverCodeOrName")) {
				log.info("Receiver Code Or Name : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		return request;
	}

	public void loadLazyList() {

		responseList = new LazyDataModel<GovtSchemeDTWResponseDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<GovtSchemeDTWResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<GovtSchemeDTWResponseDTO> data = new ArrayList<GovtSchemeDTWResponseDTO>();

				try {

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemeDTWResponseDTO>>() {
					});

					// if (data != null) {
					this.setRowCount(baseDTO.getTotalRecords());
					size = baseDTO.getTotalRecords();
					// }
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(GovtSchemeDTWResponseDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public GovtSchemeDTWResponseDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<GovtSchemeDTWResponseDTO> responseList = (List<GovtSchemeDTWResponseDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (GovtSchemeDTWResponseDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		govtSchemeDTWResponseDTO = new GovtSchemeDTWResponseDTO();
		GovtSchemeDTWRequestDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		request.setFilters(filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	/**
	 * @param event
	 * @return
	 */
	public String uploadFile(FileUploadEvent event) {

		String outFilePath = commonDataService.getAppKeyValue("EXCEL_FILE_PATH");
		try {

			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return null;
			}

			file = event.getFile();

			fileName = file.getFileName();

			log.info("Content Type " + file.getContentType());

			if (file == null) {
				throw new RestException("File is empty");
			}
			if (!fileName.contains(".xlsx")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.DISTRICT_TALUK_REQUIREMENT_EXCEL_UPLOAD)
						.getErrorCode());
				log.info("<<====  error type in file upload");
				fileName = null;
				return null;
			}

			try {

				copyFile(file.getInputstream(), fileName, outFilePath);

				List<GovtSchemeDTWRequirementDetails> objList = extractGovtSchmeRequirements(
						new File(outFilePath, fileName));

				if (objList == null || objList.isEmpty()) {
					throw new RestException("File content is empty");
				}

				log.info("Content From File ", objList);

				govtSchemeDTWRequirementDetailsList = objList;

			} catch (RestException exp) {
				if (exp.getMessage() == null) {
					log.error("RestException in uploadFile method", exp);
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				} else {
					AppUtil.addWarn(exp.getMessage());
					fileName = null;
				}
				return null;
			} catch (Exception exp) {
				log.error("Exception in uploadFile method", exp);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				return null;
			}

			log.info(" File Uploaded Successfully :: ");
		} catch (Exception e) {
			log.error("Exception Occured While uploadFile", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}

		return null;
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		// BaseDTO baseDTO=null;
		try {
			forwardToUsersList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.DTW_REQUIREMENT.toString());
			log.info("<============ forwardToUsersList size ===========>" + forwardToUsersList.size());
			/*
			 * String url = appPreference.getPortalServerURL() +
			 * "/user/getallforwardtousers"; baseDTO = httpService.get(url); if (baseDTO !=
			 * null) { ObjectMapper mapper = new ObjectMapper(); String jsonResponse =
			 * mapper.writeValueAsString(baseDTO.getResponseContent()); forwardToUsersList =
			 * mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() { }); }
			 */
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	/**
	 * @param inputFile
	 * @return
	 */
	public List<GovtSchemeDTWRequirementDetails> extractGovtSchmeRequirements(File inputFile) {

		List<GovtSchemeDTWRequirementDetails> detailsList = null;

		ArrayList<ArrayList<Object>> list = new ArrayList<ArrayList<Object>>();
		int maxDataCount = 0;
		XSSFWorkbook workbook = null;
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(inputFile);

			// Create Workbook instance holding reference to .xlsx file
			workbook = new XSSFWorkbook(fileInputStream);

			DataFormatter objDefaultFormat = new DataFormatter();
			FormulaEvaluator objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);

			// Get first/desired sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			if (sheet == null) {
				log.error("extractGovtSchmeRequirements-sheet is empty");
				// workbook.close();
				throw new RestException("Sheet is empty");
				// return null;
			}

			XSSFRow headerRow = sheet.getRow(3);

			if (headerRow == null) {
				throw new RestException("extractGovtSchmeRequirements-sheet is empty");
			}

			final int SNO_COLUMN_NUMBER = 0;
			final int DIST_COLUMN_NUMBER = 1;
			final int TLK_COLUMN_NUMBER = 2;
			final int GROUP_ONE_COLUMN_NUMBER = 3;
			final int GROUP_TWO_COLUMN_NUMBER = 4;

			String groupOneId = null;
			String groupTwoId = null;

			XSSFCell snoHeaderCell = headerRow.getCell(SNO_COLUMN_NUMBER);
			if (snoHeaderCell != null) {
				String sno = snoHeaderCell.getStringCellValue();
				log.info("extractGovtSchmeRequirements-sno " + sno);
				if (!"S.N".equals(sno)) {
					throw new RestException("'S.N'- Column name is wrong. It is given as " + sno);
				}
			}

			XSSFCell districtHeaderCell = headerRow.getCell(DIST_COLUMN_NUMBER);
			if (districtHeaderCell != null) {
				String districtHeader = districtHeaderCell.getStringCellValue();
				log.info("extractGovtSchmeRequirements-districtHeader " + districtHeader);
				if (!"District".equals(districtHeader)) {
					throw new RestException("'District'- Column name is wrong. It is given as " + districtHeader);
				}
			}

			XSSFCell talukHeaderCell = headerRow.getCell(TLK_COLUMN_NUMBER);
			if (talukHeaderCell != null) {
				String talukHeader = talukHeaderCell.getStringCellValue();
				log.info("extractGovtSchmeRequirements-talukHeader " + talukHeader);
				if (!"Taluk".equals(talukHeader)) {
					throw new RestException("'Taluk'- Column name is wrong. It is given as " + talukHeader);
				}
			}

			XSSFCell groupCodeOneHeaderCell = headerRow.getCell(GROUP_ONE_COLUMN_NUMBER);
			if (groupCodeOneHeaderCell != null) {
				String groupCodeOneHeader = groupCodeOneHeaderCell.getStringCellValue();

				String groupOneName = groupCodeOneHeader.substring(0, 6);
				groupOneId = groupCodeOneHeader.substring(7, 9);

				log.info("extractGovtSchmeRequirements-groupCodeOneHeader " + groupCodeOneHeader);
				if (!"Sarees".equals(groupOneName)) {
					throw new RestException("'Sarees'- Column name is wrong. It is given as " + groupCodeOneHeader);
				}
			}

			XSSFCell groupCodeTwoHeaderCell = headerRow.getCell(GROUP_TWO_COLUMN_NUMBER);
			if (groupCodeTwoHeaderCell != null) {
				String groupCodeTwoHeader = groupCodeTwoHeaderCell.getStringCellValue();

				String groupTwoName = groupCodeTwoHeader.substring(0, 7);
				groupTwoId = groupCodeTwoHeader.substring(8, 10);
				log.info("extractGovtSchmeRequirements-groupCodeTwoHeader " + groupCodeTwoHeader);
				if (!"Dhoties".equals(groupTwoName)) {
					throw new RestException("'Dhoties'- Column name is wrong. It is given as " + groupCodeTwoHeader);
				}
			}

			int lastRowNum = sheet.getLastRowNum();

			log.info("extractGovtSchmeRequirements-lastRowNum " + lastRowNum);

			final int FIELD_ROW_START_NUMBER = 4;

			String previousDistrictName = null;

			detailsList = new ArrayList<GovtSchemeDTWRequirementDetails>();

			boolean emptyRow = false;
			for (int r = FIELD_ROW_START_NUMBER; r <= lastRowNum; r++) {
				log.info("extractGovtSchmeRequirements-rownum " + r);
				XSSFRow row = sheet.getRow(r);
				if (row != null) {
					GovtSchemeDTWRequirementDetails govtSchemeDTWRequirementDetails = new GovtSchemeDTWRequirementDetails();

					String serialNumber = getCellValue(row, SNO_COLUMN_NUMBER, objDefaultFormat, objFormulaEvaluator);
					String districtName = getCellValue(row, DIST_COLUMN_NUMBER, objDefaultFormat, objFormulaEvaluator);
					if (previousDistrictName == null) {
						previousDistrictName = districtName;
					}
					String talukeName = getCellValue(row, TLK_COLUMN_NUMBER, objDefaultFormat, objFormulaEvaluator);
					String groupOneValue = getCellValue(row, GROUP_ONE_COLUMN_NUMBER, objDefaultFormat,
							objFormulaEvaluator);
					log.info("groupOneValue " + groupOneValue);
					if (!talukeName.isEmpty() && groupOneValue.isEmpty()) {
						log.info("Group Value Empty ");
						errorMap.notify(ErrorDescription.GROUP_VALUE_EMPTY.getErrorCode());
						// return null;
					}

					Integer groupOneCount = Integer.valueOf(groupOneValue);
					String groupTwoValue = getCellValue(row, GROUP_TWO_COLUMN_NUMBER, objDefaultFormat,
							objFormulaEvaluator);
					log.info("groupTwoValue " + groupTwoValue);
					if (!talukeName.isEmpty() && groupTwoValue.isEmpty()) {
						log.info("Group Value Empty ");
						errorMap.notify(ErrorDescription.GROUP_VALUE_EMPTY.getErrorCode());
						// return null;
					}

					Integer groupTwoCount = Integer.valueOf(groupTwoValue);

					if (serialNumber.isEmpty() && districtName.isEmpty() && talukeName.isEmpty()
							&& groupOneValue.isEmpty() && groupTwoValue.isEmpty()) {
						emptyRow = true;
					} else {
						emptyRow = false;
					}

					if (!emptyRow && !talukeName.isEmpty()) {
						if (districtName.isEmpty()) {
							districtName = previousDistrictName;
						} else {
							previousDistrictName = districtName;
						}
						log.info("serialNumber: " + serialNumber);
						log.info("districtName: " + districtName);
						log.info("talukeName: " + talukeName);
						log.info("groupOneCount: " + groupOneCount);
						log.info("groupTwoCount: " + groupTwoCount);

						govtSchemeDTWRequirementDetails.setSerialNumber(serialNumber);
						govtSchemeDTWRequirementDetails.setDistrictName(districtName);
						govtSchemeDTWRequirementDetails.setTalukeName(talukeName);
						if (govtSchemeDTWRequirementDetails.getTalukeName() != null) {
							govtSchemeDTWRequirementDetails.setGroupOneCount(groupOneCount);
							govtSchemeDTWRequirementDetails.setGroupTwoCount(groupTwoCount);
							govtSchemeDTWRequirementDetails.setGroupOneCode(groupOneId);
							govtSchemeDTWRequirementDetails.setGroupTwoCode(groupTwoId);
							detailsList.add(govtSchemeDTWRequirementDetails);
						}

					}

				}
			}

		} catch (Exception ex) {
			log.error("Exception at extractAsList() ", ex);
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (Exception ex) {

			}
			try {
				if (workbook != null) {
					// workbook.close();
				}
			} catch (Exception ex) {

			}
		}

		return detailsList;
	}

	private String getCellValue(XSSFRow row, int colNum, DataFormatter objDefaultFormat,
			FormulaEvaluator objFormulaEvaluator) throws Exception {
		String value = null;
		XSSFCell cellObj = row.getCell(colNum);
		if (cellObj != null) {
			objFormulaEvaluator.evaluate(cellObj);
			value = objDefaultFormat.formatCellValue(cellObj, objFormulaEvaluator);
		}
		value = value == null ? "" : value.trim();

		return value;
	}

	/**
	 * @param inputStream
	 * @param name
	 * @param outFilePath
	 * @return
	 */
	private Boolean copyFile(InputStream inputStream, String name, String outFilePath) {

		OutputStream outputStream = null;
		// String path = "/Users/barry/Desktop/";
		Boolean flag = false;
		try {

			outputStream = new FileOutputStream(new File(outFilePath, name));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}

			log.info("copyFile-Done!");
			flag = true;
		} catch (IOException ex) {
			log.error("Exception at copyFile() ", ex);
			flag = false;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (outputStream != null) {
				try {
					// outputStream.flush();
					outputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		}
		return flag;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current govtSchemeDTWResponseDTO Status ---> " + currentPlanStatus);
		log.info("<--- govtSchemeDTWResponseDTO Id ---> " + govtSchemeDTWResponseDTO.getId());
		log.info("<--- Comments ---> " + approveComments);
		log.info("<--- Comments ---> " + rejectComments);

		BaseDTO baseDTO = null;
		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {

			GovtSchemeDTWRequirementDTO request = new GovtSchemeDTWRequirementDTO(govtSchemeDTWResponseDTO.getId(),
					currentPlanStatus, approveComments);

			request.setNoteEntity(govtDtwRequirementsNote);

			request.setRemarks(approveComments);

			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/approve";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			GovtSchemeDTWRequirementDTO request = new GovtSchemeDTWRequirementDTO(govtSchemeDTWResponseDTO.getId(),
					currentPlanStatus, rejectComments);

			request.setNoteEntity(govtDtwRequirementsNote);

			request.setRemarks(rejectComments);

			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/reject";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		approveComments = null;
		rejectComments = null;

		return LIST_PAGE_URL;
	}

	public String downloadExcelTemplate() {
		log.info("Inside GovtSchemeDTWRequirementBean generatePDF ");
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/govtSchemePlan/dtwrequirement/downloadexcel";

		try {

			if (selectedDistrictList.size() == 0) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_DISTRICT.getErrorCode());
				excelFile = null;
				return null;
			}

			GovtSchemeDTWRequirementDTO request = new GovtSchemeDTWRequirementDTO();
			log.info("request" + request);
			request.setDistrictList(selectedDistrictList);
			request.setGovtSchemeType(govtSchemeType);
			request.setGovtSchemePlan(govtSchemePlan);
			InputStream inputStream = httpService.downloadFile(url, request);

			excelFile = new DefaultStreamedContent(inputStream, "application/vnd.ms-excel",
					"District_Wise_Taluk_Wise_requirements.xlsx");

		} catch (Exception exp) {
			log.error("Error in Pdf Download", exp);
			errorMap.notify(1);
		}
		return null;
	}

	public void changeCurrentStatus(String value) {

		log.info("Changing the status to " + value);

		if (value.equals("APPROVED")) {
			dialogHeader = "Approve Comments";
		} else {
			dialogHeader = "Reject Comments";
		}
		currentPlanStatus = value;
		approveComments = null;
		rejectComments = null;
	}

	public String deleteProduct(GovtSchemePlanItems planItem) {
		log.info("Inside deleteProduct()>>>>>>>>>" + planItem.getCircleMaster().getName());
		String circleName = null;
		if (planItem != null) {
			circleName = planItem.getCircleMaster().getName();
		}
		if (circleName != null) {
			circleMap.get(circleName).remove(planItem);
		}
		return "/pages/govtscheme/fds/createFDS.xhtml?faces-redirect=true;";
	}

	@Getter
	@Setter
	Integer groupOneTotal;

	@Getter
	@Setter
	Integer groupTwoTotal;

	public String submit() {
		log.info("Inside submit()>>>>>>>>>");

		if (govtSchemeDTWRequirementDetailsList.size() == 0) {
			log.info("Inside submit() govtSchemeDTWRequirementDetailsList list empty>>>>>>>>>");
			errorMap.notify(ErrorDescription.NO_RECORDS_FOUND_FROM_FILE.getErrorCode());
			return null;
		}

		groupOneTotal = 0;
		groupTwoTotal = 0;

		Map<String, Integer> groupOneTotalValue = govtSchemeDTWRequirementDetailsList.stream()
				.collect(Collectors.groupingBy(GovtSchemeDTWRequirementDetails::getDistrictName,
						Collectors.summingInt(GovtSchemeDTWRequirementDetails::getGroupOneCount)));

		Map<String, Integer> groupTwoTotalValue = govtSchemeDTWRequirementDetailsList.stream()
				.collect(Collectors.groupingBy(GovtSchemeDTWRequirementDetails::getDistrictName,
						Collectors.summingInt(GovtSchemeDTWRequirementDetails::getGroupTwoCount)));

		for (GovtSchemeDTWRequirementDetails govtSchemeDTWRequirementDetails : govtSchemeDTWRequirementDetailsList) {
			String key = null;
			for (Map.Entry<String, Integer> entry : groupOneTotalValue.entrySet()) {
				key = entry.getKey();
				if (govtSchemeDTWRequirementDetails.getDistrictName().equals(key)) {
					govtSchemeDTWRequirementDetails.setSareesCount(groupOneTotalValue.get(key));
					govtSchemeDTWRequirementDetails.setDhotiesCount(groupTwoTotalValue.get(key));
				}
			}
		}

		log.info("govtSchemeDTWRequirementDetailsList" + govtSchemeDTWRequirementDetailsList);

		for (GovtSchemeDTWRequirementDetails govtSchemeDTWRequirementDetails : govtSchemeDTWRequirementDetailsList) {
			if (govtSchemeDTWRequirementDetails.getGroupOneCount() != null) {
				groupOneTotal = groupOneTotal + govtSchemeDTWRequirementDetails.getGroupOneCount();
			}
			if (govtSchemeDTWRequirementDetails.getGroupTwoCount() != null) {
				groupTwoTotal = groupTwoTotal + govtSchemeDTWRequirementDetails.getGroupTwoCount();
			}
		}

		log.info("groupOneTotal" + groupOneTotal);
		log.info("groupTwoTotal" + groupTwoTotal);

		return null;
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		govtSchemeDTWResponseDTO = ((GovtSchemeDTWResponseDTO) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("onPageLoad method started");
		addButtonFlag = false;
	}

	public void getNote() {
		log.info("==================test======================= " + govtDtwRequirementsNote.getNote());
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + govtDtwRequirementsNote.getNote());
		log.info("=========================================");
	}

	/**
	 * @return
	 */
	public String planPreview() {
		log.info("Inside previewPlan()>>>>>>>");
		log.info("Group Size()>>>>>>>" + productGroupList.size());
		for (ProductGroupMaster g : productGroupList) {
			log.info("Group Name>>>>>>>" + g.getName());
			for (ProductVarietyMaster v : g.getVarietyList()) {
				log.info("Variety Name>>>>>>>" + v.getName());
			}
		}
		return "/pages/govtscheme/fds/previewFDS.xhtml?faces-redirect=true;";
	}

	public void clear() {
		govtSchemeType = new GovtSchemeType();
		govtSchemePlan = new GovtSchemePlan();
		selectedDistrictList = new ArrayList<DistrictMaster>();
		districtNames = "";
	}

}
