package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.personnel.hrms.model.EmpDaConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeDAArrearConfigBean")
public class EmployeeDAArrearConfigBean {

		private final String CREATE_PAGE_URL = "/pages/personnelHR/payRoll/createDAArearConfiguration.xhtml?faces-redirect=true;";
		
		private final String LIST_PAGE_URL = "/pages/personnelHR/payRoll/listDAArearConfiguration.xhtml?faces-redirect=true;";
		
		final String SERVER_URL = AppUtil.getPortalServerURL();

		ObjectMapper mapper=new ObjectMapper();

		String jsonResponse;

		@Getter @Setter
		String action;

		@Autowired
		HttpService httpService;

		@Autowired
		ErrorMap errorMap;

		@Getter	@Setter
		int totalRecords = 0;

		@Getter @Setter
		Boolean addButtonFlag = true;
		
		@Getter @Setter
		EmpDaConfig empDaConfig,selectedEmpDaConfig;
		
		@Getter
		@Setter
		LazyDataModel<EmpDaConfig> lazyEmployeeDaArrearList;
		
		List<EmpDaConfig> empDaConfigList = null;

		public String showDAArrearConfigListPage() {
			log.info("<==== Starts EmployeeDAArrearConfigBean.showDAArrearConfigListPage() Starts =====>");
			mapper = new ObjectMapper();
			empDaConfig = new EmpDaConfig();
			selectedEmpDaConfig = new EmpDaConfig();
			addButtonFlag = true;
			loadLazyDaArrearConfigList();
			log.info("<==== Ends EmployeeDAArrearConfigBean.showDAArrearConfigListPage() Ends =====>");
			return LIST_PAGE_URL;
		}

		public String showListPageAction() {
			log.info("<===== Starts EmployeeDAArrearConfigBean.showListPageAction() ======>");
			try {
				if (!action.equalsIgnoreCase("CREATE")
						&& (selectedEmpDaConfig == null || selectedEmpDaConfig.getId() == null)) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
					return null;
				}
				if (action.equalsIgnoreCase("CREATE")) {
					log.info("create EmpDaconfig called..");
					empDaConfig = new EmpDaConfig();
					return CREATE_PAGE_URL;
				} else if (action.equalsIgnoreCase("EDIT")) {
					log.info("edit EmpDaconfig called..");
					String url = SERVER_URL + "/daarrearconfig/getbyid/" + selectedEmpDaConfig.getId();
					BaseDTO response = httpService.get(url);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						empDaConfig = mapper.readValue(jsonResponse, new TypeReference<EmpDaConfig>() {
						});
						return CREATE_PAGE_URL;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					}
				} else if (action.equalsIgnoreCase("DELETE")) {
					log.info("delete EmpDaconfig called..");
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmDaArrearConfigDelete').show();");
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} catch (Exception e) {
				log.error("Exception occured in showListPageAction ..", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
			log.info("<===== Ends EmployeeDAArrearConfigBean.showListPageAction ===========>" + action);
			return null;
		}

		public String saveOrUpdate() {
			log.info("<====== Starts EmployeeDAArrearConfigBean.saveOrUpdate() =======>");
			try {
				if (empDaConfig.getDearnessAllowancePercentage() == null) {
					errorMap.notify(ErrorDescription.EDU_DETAILS_PERCENTAGE_NOT_NULL.getCode());
					return null;
				}else if (empDaConfig.getNoOfInstallment() == null) {
					errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_NO_OF_INSTALLMENTS_REQUIRED.getCode());
					return null;
				}else if (empDaConfig.getEffectFromDate() == null) {
					errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_FROM_DATE_REQUIRED.getCode());
					return null;
				}else if (empDaConfig.getDearnessAllowancePercentage() == 0.0) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DA_PERCENTAGE_SHOULD_NOT_BE_ZERO).getErrorCode());
					return null;
				}else if (empDaConfig.getNoOfInstallment() == 0) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.NO_OF_INSTALLMENT_SHOULD_NOT_BE_ZERO).getErrorCode());
					return null;
				}else if (empDaConfig.getTobeApplied() == null) {
					AppUtil.addWarn("To be applied is required.");
					return null;
				}
				String url = SERVER_URL + "/daarrearconfig/create";
				BaseDTO response = httpService.post(url, empDaConfig);
				if (response != null && response.getStatusCode() == 0) {
					log.info("account category saved successfully..........");
					if (action.equalsIgnoreCase("CREATE")) {
						errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DA_ARREAR_CONFIG_SAVE_SUCCESS).getErrorCode());
					}else {
						errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DA_ARREAR_CONFIG_UPDATE_SUCCESS).getErrorCode());
					}
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} catch (Exception e) {
				log.error("Exception occured while save or update .......", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				return null;
			}
			log.info("<==== Ends EmployeeDAArrearConfigBean.saveOrUpdate =======>");
			return showDAArrearConfigListPage();
		}

		public void onRowSelect(SelectEvent event) {
			log.info("<===Starts EmployeeDAArrearConfigBean.onRowSelect() ========>" + event);
			selectedEmpDaConfig = ((EmpDaConfig) event.getObject());
			addButtonFlag = false;
			log.info("<===Ends EmployeeDAArrearConfigBean.onRowSelect() ========>");
		}

		public String deleteConfirm() {
			log.info("<===== Starts GlAccountCategoryBean.deleteConfirm ===========>");
			try {
				BaseDTO response = httpService.delete(SERVER_URL + "/daarrearconfig/deletebyid/" + selectedEmpDaConfig.getId());
				if (response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.DA_ARREAR_CONFIG_DELETE_SUCCESS).getErrorCode());
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmAccountCategoryDelete').hide();");
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} catch (Exception e) {
				log.error("Exception occured while deleteConfirm....", e);
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			}
			log.info("<===== Ends GlAccountCategoryBean.deleteConfirm ===========>");
			return showDAArrearConfigListPage();
		}
		
		public void loadLazyDaArrearConfigList() {
			log.info("<===== Starts employeeDAArrearConfigBean.loadLazyDaArrearConfigList ======>");

			lazyEmployeeDaArrearList = new LazyDataModel<EmpDaConfig>() {
				private static final long serialVersionUID = 8422543223567350599L;

				@Override
				public List<EmpDaConfig> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, Object> filters) {
					try {
						PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
								sortOrder.toString(), filters);
						log.info("Pagination request :::" + paginationRequest);
						String url = SERVER_URL + "/daarrearconfig/loadlazylist";
						BaseDTO response = httpService.post(url, paginationRequest);
						if (response != null && response.getStatusCode() == 0) {
							jsonResponse = mapper.writeValueAsString(response.getResponseContents());
							empDaConfigList = mapper.readValue(jsonResponse,
									new TypeReference<List<EmpDaConfig>>() {
									});
							this.setRowCount(response.getTotalRecords());
							totalRecords = response.getTotalRecords();
						} else {
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}
					} catch (Exception e) {
						log.error("Exception occured in lazyload ...", e);
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					log.info("Ends lazy load....");
					return empDaConfigList;
				}

				@Override
				public Object getRowKey(EmpDaConfig res) {
					return res != null ? res.getId() : null;
				}

				@Override
				public EmpDaConfig getRowData(String rowKey) {
					for (EmpDaConfig empDaConfig : empDaConfigList) {
						if (empDaConfig.getId().equals(Long.valueOf(rowKey))) {
							selectedEmpDaConfig = empDaConfig;
							return empDaConfig;
						}
					}
					return null;
				}
			};
			log.info("<===== Ends employeeDAArrearConfigBean.loadLazyDaArrearConfigList ======>");
		}
}
