package in.gov.cooptex.operation.tender.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.tendernegotiation.dto.TenderNegotiationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.tender.model.Tender;
import in.gov.cooptex.operation.tender.model.TenderEvaluation;
import in.gov.cooptex.operation.tender.model.TenderItemDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("tenderNegotiationBean")
@Scope("session")
public class TenderNegotiationBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private final String TENDER_NEGOTIATION_PAGE = "/pages/operation/tender/tenderNegotiation.xhtml?faces-redirect=true";

	private final String LIST_TENDER_NEGOTIATION_PAGE = "/pages/operation/tender/listTenderNegotiation.xhtml?faces-redirect=true";

	private final String VIEW_PAGE_TENDERNEGOTIATION = "/pages/operation/tender/viewTenderNegotiation.xhtml?faces-redirect=true";

	@Autowired
	AppPreference appPreference;

	@Autowired
	private HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	List<TenderNegotiationDTO> tenderList = new ArrayList<>();

	@Getter
	@Setter
	LazyDataModel<TenderNegotiationDTO> lazyTenderList;

	@Getter
	@Setter
	private TenderNegotiationDTO tenderNegotiationDTO = new TenderNegotiationDTO();
	@Getter
	@Setter
	private List<TenderNegotiationDTO> tenderNegotiationDTOList;

	@Getter
	@Setter
	AddressMaster bidopeningAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster bidMeetingAddress = new AddressMaster();

	@Getter
	@Setter
	private List<TenderEvaluation> tenderEvaluationList;

	private final String LIST_URL = "/tenderNegotiation/getall";

	private String serverURL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	Tender selectedTenderr;

	private final String VIEW_URL = "/tender/view";

	@Getter
	@Setter
	Tender tender;

	@Getter
	@Setter
	TenderNegotiationDTO selectedTender = new TenderNegotiationDTO();

	@Getter
	@Setter
	private int tenderListSize;

	@Getter
	@Setter
	Boolean applyButton = false, renderTenderCreate;

	@Getter
	@Setter
	String pageAction;

	public String showTenderNegotiationPage() {
		tenderNegotiationDTO = new TenderNegotiationDTO();
		tenderEvaluationList = new ArrayList<>();
		loadTenderType();
		return LIST_TENDER_NEGOTIATION_PAGE;
	}

	public void loadTenderType() {

		try {
			log.info("<============= tenderNegotiationBean.loadTenderType=====================>");
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + "/tender/getTenderListByNegotiationAmount";
			log.info("<<==  url:: " + url);
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				log.info(jsonResponse);
				tenderList = mapper.readValue(jsonResponse, new TypeReference<List<Tender>>() {
				});
				log.info("tenderList---------" + tenderList.size());
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void loadTenderName() {

		try {
			log.info("<============= tenderNegotiationBean.loadTenderName=====================>");
			BaseDTO baseDTO = new BaseDTO();
			Tender tender = new Tender();
			tender.setId(tenderNegotiationDTO.getId());
			log.info(tender.getId());
			String url = appPreference.getPortalServerURL() + "/tender/view";
			log.info("<<==  url:: " + url);
			baseDTO = httpService.post(url, tender);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				log.info(jsonResponse);
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});
				tenderNegotiationDTO.setTenderName(tender.getTenderName());
			}

			log.info(tenderNegotiationDTO.getTenderName());

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String goToPage() {

		log.info("Goto Page [" + pageAction + "]");

		if (pageAction.equalsIgnoreCase("LIST")) {
			// return showListPage();
		} else if (pageAction.equalsIgnoreCase("ADD")) {

			return showAddPage();
			// return
			// "/pages/operation/tender/createApplyingTender.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String loadTenderListPage() {
		log.info("tenderNegotiationBean loadTenderListPage.....");
		try {
			tenderEvaluationList = new ArrayList<>();
			selectedTender = new TenderNegotiationDTO();
			getAllTenderList();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_TENDER_NEGOTIATION_PAGE;

	}

	public String search() {
		try {

			tenderEvaluationList = new ArrayList<>();
			BaseDTO baseDTO = null;
			String url = appPreference.getPortalServerURL() + "/tenderNegotiation/getTenderDetails";
			baseDTO = httpService.post(url, tenderNegotiationDTO);
			if (baseDTO != null) {
				log.info("inside");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tenderEvaluationList = mapper.readValue(jsonResponse, new TypeReference<List<TenderEvaluation>>() {
				});
				log.info("reportData" + tenderEvaluationList.size() + tenderEvaluationList);

			}
		} catch (Exception e) {
			log.error("searchReport", e);
		}
		return "";
	}

	public String createTenderNegotiation() {
		log.info("<===== Start tenderNegotiationBean.generateTenderEval ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			tenderNegotiationDTO.setTenderEvaluationTableList(tenderEvaluationList);
			String url = AppUtil.getPortalServerURL() + "/tenderNegotiation/create";
			baseDTO = httpService.post(url, tenderNegotiationDTO);
			log.info("saved");
			baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TENDER_NEGOTIATION_INSERTED_SUCCESSFULLY.getErrorCode());

				return loadTenderListPage();
			}

		} catch (Exception e) {
			log.info("<--- Exception in createTenderNegotiation() --->", e);
		}

		return loadTenderListPage();
	}

	public void clear() {
		selectedTender = new TenderNegotiationDTO();
		tenderEvaluationList = new ArrayList<>();
	}

	public String cancel() {
		selectedTender = new TenderNegotiationDTO();
		tenderEvaluationList = new ArrayList<>();

		return LIST_TENDER_NEGOTIATION_PAGE;
	}

	private void getAllTenderList() {
		log.info("<===== Starts tenderNegotiationBean.lazyTenderList ======>");
		lazyTenderList = new LazyDataModel<TenderNegotiationDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TenderNegotiationDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + LIST_URL;
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderList = mapper.readValue(jsonResponse, new TypeReference<List<TenderNegotiationDTO>>() {
						});

						this.setRowCount(response.getTotalRecords());
						tenderListSize = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
				}
				log.info("Ends lazy load...." + tenderList.size());
				return tenderList;
			}

			@Override
			public Object getRowKey(TenderNegotiationDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TenderNegotiationDTO getRowData(String rowKey) {
				try {
					for (TenderNegotiationDTO tenderObj : tenderList) {
						if (tenderObj.getId().equals(Long.valueOf(rowKey))) {
							selectedTender = tenderObj;
							return tenderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends tenderNegotiationBean.lazyTenderList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts tenderNegotiationBean.onRowSelect ========>" + event);
		selectedTender = ((TenderNegotiationDTO) event.getObject());
		log.info("tender status=> " + selectedTender.getStatus());
		if (selectedTender.getStatus().equalsIgnoreCase("Tender Evaluated")) {
			applyButton = true;
		} else {
			applyButton = false;
		}

		log.info("<===Ends tenderNegotiationBean.onRowSelect ========>");
	}

	public String showAddPage() {
		log.info("<=tenderNegotiationBean showAddPage start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (selectedTender == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String url = AppUtil.getPortalServerURL() + "/tender/get/" + selectedTender.getId();
			log.info("tenderNegotiationBean showAddPage url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});
				tenderNegotiationDTO.setId(tender.getId());
				tenderNegotiationDTO.setTenderName(tender.getTenderName());

				return TENDER_NEGOTIATION_PAGE;

			} else {
				tender = new Tender();
			}

		} catch (Exception e) {
			log.error("Exception in showAddPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String viewTender() {
		// action = "View";
		selectedTenderr = new Tender();
		log.info("<----Redirecting to user view page---->");
		if (selectedTender == null) {
			Util.addWarn("Please Select One Tender");
			return null;
		}
		selectedTenderr.setId(selectedTender.getId());

		if (selectedTender.getStatus().equalsIgnoreCase("Tender Evaluated")) {
			renderTenderCreate = true;
		} else {
			renderTenderCreate = false;
		}

		getTenderView(selectedTenderr);
		search();
		return VIEW_PAGE_TENDERNEGOTIATION;

	}

	private void getTenderView(Tender selectedTender) {
		log.info("<<<< ----------Start tenderNegotiationBean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + VIEW_URL;
			baseDTO = httpService.post(url, selectedTender);
			selectedTenderr = new Tender();
			Double totalAmount = 0.0;

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedTenderr = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

				tenderNegotiationDTO.setId(selectedTenderr.getId());
				tenderNegotiationDTO.setTenderName(selectedTenderr.getTenderName());
				bidopeningAddress = selectedTenderr.getBidMeetingAddressId();
				log.info("bidopeningAddress id==> " + bidopeningAddress.getId());

				bidMeetingAddress = selectedTenderr.getBidMeetingAddressId();

				log.info("bidMeetingAddress id==> " + bidMeetingAddress.getId());

			}
			for (TenderItemDetails item : selectedTenderr.getTenderItemDetailsList()) {
				totalAmount += item.getItemAmount();
			}
			selectedTenderr.setTotalAmount(totalAmount);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedTender != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End tenderNegotiationBean-getTenderView ------- >>>>");
	}

}
