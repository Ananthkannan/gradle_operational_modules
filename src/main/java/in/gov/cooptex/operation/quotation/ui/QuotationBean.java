/**
 * 
 */
package in.gov.cooptex.operation.quotation.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.QuotationDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.QuotationItemTax;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.model.TaxMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Praveen
 *
 */
@Log4j2
@Scope("session")
@Service("quotationBean")
public class QuotationBean {

	private final String CREATE_PURCHASE_QUOTATION_PAGE = "/pages/operation/purchaseQuotation/createPurchaseQuotation.xhtml?faces-redirect=true;";
	private final String LIST_PURCHASE_QUOTATION_PAGE = "/pages/operation/purchaseQuotation/listPurchaseQuotation.xhtml?faces-redirect=true;";
	private final String VIEW_PURCHASE_QUOTATION_PAGE = "/pages/operation/purchaseQuotation/viewPurchaseQuotation.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Quotation quotation;

	@Getter
	@Setter
	Quotation selectedQuotation;

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	TaxMaster taxMaster;

	@Getter
	@Setter
	UomMaster uomMaster;

	@Getter
	@Setter
	QuotationDTO quotationDTO = new QuotationDTO();

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	String productType;

	@Getter
	@Setter
	String taxApplicable;

	@Getter
	@Setter
	String discountApplicable;

	@Getter
	@Setter
	String discountType;

	@Getter
	@Setter
	Double unitRate;

	@Getter
	@Setter
	Double quantity;

	@Getter
	@Setter
	Double itemValueTotal;

	@Getter
	@Setter
	Double discountValueTotal;

	@Getter
	@Setter
	Double taxValueTotal;

	@Getter
	@Setter
	Double netAmountTotal;

	@Getter
	@Setter
	Boolean productFlag = false;

	@Getter
	@Setter
	Boolean itemFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButton = false;

	@Getter
	@Setter
	Boolean statusButtonFlag = false;

	@Getter
	@Setter
	LazyDataModel<Quotation> quotationLazyList;

	@Getter
	@Setter
	List<Quotation> quotationList;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;
	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	List<TaxMaster> taxMasterLists;

	@Getter
	@Setter
	List<ProductVarietyTax> productVarietyTaxs;
	@Getter
	@Setter
	List<UomMaster> uomMasterLists;
	@Getter
	@Setter
	List<QuotationDTO> quotationDTOList;

	@Getter
	@Setter
	List<QuotationDTO> taxSummaryList;

	@Getter
	@Setter
	List<QuotationDTO> quotationItemTaxList;

	@Getter
	@Setter
	Double taxValueGridTotal;

	@Getter
	@Setter
	Integer taxPercentTotal;

	@Getter
	@Setter
	Double cgstValue;

	@Getter
	@Setter
	Double sgstValue;

	@Getter
	@Setter
	Double cgstValueTotal;

	@Getter
	@Setter
	Double sgstValueTotal;

	@Getter
	@Setter
	Double unitRateAmountTotal;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	Long supplierTypeId;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList = new ArrayList<>();

	@Getter
	@Setter
	CircleMaster circleMaster = new CircleMaster();

	@Getter
	@Setter
	Boolean isCircleFlag = false;
	
	@Getter
	@Setter
	Double quantityTotal=0.00;

	@PostConstruct
	private void init() {
		quotationDTOList = new ArrayList<QuotationDTO>();
		taxSummaryList = new ArrayList<QuotationDTO>();
		quotationItemTaxList = new ArrayList<QuotationDTO>();
		quotation = new Quotation();
		isCircleFlag = false;
		loadLazyQuotationList();
	}

	public void getAllQuotationList() {
		try {
			selectedQuotation = new Quotation();
			quotation = new Quotation();
			quotationList = new ArrayList<Quotation>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/getAll";
			log.info(" Quotation Master ==>>> getAllQuotationList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				quotationList = mapper.readValue(jsonResponse, new TypeReference<List<Quotation>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationMaster . getAllQuotationList ------- >>>>");
	}

	/*** Edit and View Quotation here ***/
	public void editQuotationByQuotationId() {
		try {
			log.info("called editQuotationByQuotationId Start..");
			String url = SERVER_URL + "/quotation/getQuotationById/" + selectedQuotation.getId();
			log.info("editQuotationByQuotationId URL ====>>>  " + url);
			BaseDTO baseDTO = httpService.get(url);
			quotation = new Quotation();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				quotation = mapper.readValue(jsonResponse, new TypeReference<Quotation>() {
				});

				if (action.equalsIgnoreCase("View")) {
					log.info("SOCIETY code==>" + quotation.getSupplier().getSupplierTypeMaster().getCode());
					if (quotation.getSupplier() != null && quotation.getSupplier().getSupplierTypeMaster() != null) {
						if (quotation.getSupplier().getSupplierTypeMaster().getCode().equals("SOCIETY")) {
							isCircleFlag = true;
						} else {
							isCircleFlag = false;
						}
					} else {
						log.info("Supplier type not found");
					}
				}

				else if (action.equalsIgnoreCase("Edit")) {
					getAllSupplierTypeList();
					if (quotation != null) {
						supplierTypeMaster = quotation.getSupplier().getSupplierTypeMaster();
						supplierTypeId = quotation.getSupplier().getSupplierTypeMaster().getId();
						getSupplierMasterListBySupplierTypeId();
						getUomMasterList();
					}

				}
				loadCircleMasterBySupplier();

			}
		} catch (Exception e) {
			log.error(" Error Occured In Edit Quotation Details :: ", e);
		}
	}

	/**
	 * @author krishnakumar
	 */
	public void loadCircleMasterBySupplier() {
		log.info("<=QuotationBean :: loadCircleMasterBySupplier start=>");
		try {

			if (quotation.getSupplier() == null && quotation.getSupplier().getId() == null) {
				errorMap.notify(ErrorDescription.SUPPLIER_MASTER_SHOULD_NOT_BE_EMPTY.getErrorCode());
				return;
			}
			Long supplierId = quotation.getSupplier().getId();
			log.info("loadCircleMasterBySupplier :: supplierId==> " + supplierId);
			String url = SERVER_URL + "/circleMaster/loadcircleidbysupplier/" + supplierId;
			log.info("loadCircleMasterBySupplier url ==> " + url);

			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				circleMaster = mapper.readValue(jsonResponse, new TypeReference<CircleMaster>() {
				});
				if (circleMaster != null) {
					isCircleFlag = true;
					log.info("circle master id==> " + circleMaster.getId());
					getSupplierMasterListByCircleMaster();
				} else {
					log.info("Circle master not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in loadCircleMasterBySupplier ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void viewQuotationDetails() {
		double itemAmount = 0;
		double discountValue = 0;
		double taxValue = 0;
		double totalAmount = 0;
		try {
			if (quotation != null) {
				log.info(" Called....viewQuotationDetails....Total Count Calculation..Started..");
				List<QuotationItem> quotationItemList = quotation.getQuotationItemList();
				for (QuotationItem item : quotationItemList) {
					if (item.getItemAmount() != null) {
						itemAmount += item.getItemAmount();
					}
					if (item.getDiscountValue() != null) {
						discountValue += item.getDiscountValue();
					}
					if (item.getTaxValue() != null) {
						taxValue += item.getTaxValue();
					}
					if (item.getTotalAmount() != null) {
						totalAmount += item.getTotalAmount();
					}
				}
				itemValueTotal = itemAmount;
				discountValueTotal = discountValue;
				taxValueTotal = taxValue;
				netAmountTotal = totalAmount;

			} else {
				log.info(" viewQuotationDetails ===>> EMPTY List ");
			}
		} catch (Exception e) {
			log.error("Error Occured at Quotation view method.....Total Count Calculation QuotationItem ... " + e);
		}

	}

	public void getAllSupplierTypeList() {
		try {
			// loadSupplierTypeMaster();
			supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/suppliertypemaster/getall";
			log.info(" Quotation Masterr ==>>> getAllSupplierTypeList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End Quotation . getAllSupplierTypeList ------- >>>>");
	}

	public void getSupplierMasterListBySupplierTypeId() {
		try {
			// supplierTypeId = quotation.getSupplier().getSupplierTypeMaster().getId();
			if (getSupplierTypeMaster().getCode().equals("SOCIETY")) {
				supplierMasterList = new ArrayList<>();
				circleMasterList = commonDataService.loadCircles();
				isCircleFlag = true;
				if (circleMasterList != null && !circleMasterList.isEmpty()) {
					log.info("QuotationBean :: circleMasterList size==> " + circleMasterList.size());
				}
			} else {
				isCircleFlag = false;
				circleMasterList = new ArrayList<>();
				if (getSupplierTypeMaster().getId() != null) {
					supplierTypeId = getSupplierTypeMaster().getId();
				} else {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.SELECT_SUPPLIER_TYPE_MASTER).getCode());
					return;
				}
				log.info("supplierTypeId==> " + supplierTypeId);
				log.info("Socity code==> " + getSupplierTypeMaster().getCode());
				supplierMasterList = new ArrayList<>();
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/supplier/master/getBySupplierType/" + supplierTypeId;
				log.info(" Quotation Bean :: getSupplierMasterListBySupplierTypeId URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});

					if (supplierMasterList != null && supplierMasterList.size() > 0) {
						log.info(" Supplier master List SIZE ===>>>> " + supplierMasterList.size());
					}
				}
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationMaster . getSupplierMasterListBySupplierTypeId ------- >>>>");
	}

	/**
	 * @author krishnakumar
	 */
	public void getSupplierMasterListByCircleMaster() {
		log.info("<=QuotationBean :: getSupplierMasterListByCircleMaster=>");
		try {
			if (circleMaster.getId() == null) {
				errorMap.notify(ErrorDescription.CIRCLE_NOT_FOUND.getErrorCode());
				return;
			}
			log.info("circle master id==> " + circleMaster.getId());
			String url = SERVER_URL + "/supplier/master/getsuppliermasterbycirclemaster/" + circleMaster.getId();
			log.info(" Quotation Bean :: getSupplierMasterListByCircleMaster URL===>>> " + url);
			supplierMasterList = new ArrayList<>();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});

				if (supplierMasterList != null && supplierMasterList.size() > 0) {
					log.info(" Supplier master List SIZE ===>>>> " + supplierMasterList.size());
				}
			}

		} catch (Exception e) {
			log.error("Exception in getSupplierMasterListByCircleMaster  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void setGSTINNumber() {
		// quotation = new Quotation();
		if (quotation.getSupplier() != null) {
			quotation.setQuotationNumberPrefix(quotation.getSupplier().getGstNumber());
		}
	}

	public void changeProductType() {
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		if (productType != null) {
			if (productType.equalsIgnoreCase("Purchase")) {
				productFlag = false;
				itemFlag = true;
				getProductCategoryGrpList();
				productCategoryList.clear();
				productGroupList.clear();
				productVarietyMasterList.clear();
			} else if (productType.equalsIgnoreCase("Service")) {
				productFlag = true;
				itemFlag = false;
			}
		}
	}

	public void getProductCategoryGrpList() {
		try {
			productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/productCategoryGroup/getall";
			log.info(" Quotation Master ==>>> getProductCategoryGrpList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationMaster . getProductCategoryGrpList ------- >>>>");
	}

	public void getProductCategoryListByProductCategoryGRPId() {
		try {
			// group/getAllActiveProductGroups
			productCategoryList = new ArrayList<ProductCategory>();
			if (productCategoryGroup != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/quotation/productCategory/getByProductCategoryGrp/"
						+ productCategoryGroup.getId();
				log.info(" Quotation Master BEAN ==>>> getProductCategoryListByProductCategoryGRPId URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
					});
				}
				productGroupList.clear();
				productVarietyMasterList.clear();
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationMaster . getProductCategoryListByProductCategoryGRPId ------- >>>>");
	}

	public void getProductGroupListByProductCategoryId() {
		productGroupList.clear();
		productGroupList = new ArrayList<ProductGroupMaster>();
		try {
			log.info("<<<< ----------STARAT QuotationBean . getProductGroupListByProductCategoryId ------- >>>>");
			if (productCategory != null) {
				log.info(" getProductGroupListByProductCategoryId ==>>> " + productCategory.getId());
				productGroupList = commonDataService.loadActiveProductGroups(productCategory);
			}
		} catch (Exception e) {
			log.error(" Exception occurred..getProductGroupListByProductCategoryId... " + e);
		}
		log.info("<<<< ----------End QuotationBean . getProductGroupListByProductCategoryId ------- >>>>");
	}

	public void getProductVarietyListByProductGroupId() {
		try {
			log.info(" STARAT  getProductVarietyListByProductGroupId ");
			if (productGroupMaster != null) {
				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				log.info(" getProductVarietyListByProductGroupId ==>>> " + productGroupMaster.getId());
				productVarietyMasterList = commonDataService.loadActiveProductVarieties(productGroupMaster);
			}
		} catch (Exception e) {
			log.error("Exception occured ....getProductVarietyListByProductGroupId.. ", e);
		}
		log.info(" END getProductVarietyListByProductGroupId ");
	}

	public void getTaxListByProductVarietyTax() {
		try {

			Long productVarietyId = null;
			productVarietyTaxs = new ArrayList<ProductVarietyTax>();
			if (quotationDTO != null) {
				if (quotationDTO.getQuotationItem().getProductVarietyMaster() != null) {
					productVarietyId = quotationDTO.getQuotationItem().getProductVarietyMaster().getId();
					quotationDTO.getQuotationItem().setUom(quotationDTO.getQuotationItem().getProductVarietyMaster().getUomMaster());
				}

				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/quotation/productVarietyTax/getByProductVarietyMaster/" + productVarietyId;
				log.info(" Quotation Bean ==>>> getTaxListByProductVarietyTax URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productVarietyTaxs = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyTax>>() {
					});

					/*
					 * taxMasterLists = new ArrayList<TaxMaster>(); BaseDTO baseDTO = new BaseDTO();
					 * String url = SERVER_URL + "/quotation/taxMaster/getallTax";
					 * log.info(" Quotation Bean ==>>> getTaxMasterList URL===>>> " + url); baseDTO
					 * = httpService.get(url); if (baseDTO != null) { ObjectMapper mapper = new
					 * ObjectMapper();
					 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					 * String jsonResponse =
					 * mapper.writeValueAsString(baseDTO.getResponseContents()); taxMasterLists =
					 * mapper.readValue(jsonResponse, new TypeReference<List<TaxMaster>>() { });
					 */

				}
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationBean ... getTaxListByProductVarietyTax ------- >>>>");
	}

	public void getUomMasterList() {
		try {
			uomMasterLists = new ArrayList<UomMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/uomMaster/getAllUom";
			log.info(" Quotation Bean ==>>> getUomMasterList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				uomMasterLists = mapper.readValue(jsonResponse, new TypeReference<List<UomMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End QuotationBean ... getUomMasterList ------- >>>>");
	}

	public String addQuotationDetails() {
		log.info("Called addQuotationDetails");
		try {
			if (quotationDTO != null) {
				log.info("Called STARAT...addQuotationDetails");
				if (taxSummaryList == null) {
					log.info(" Tax Summary deatils..EMPTY..");
					errorMap.notify(ErrorDescription.PURCHASEQUOTATION_ITEMTAX_EMPTY.getCode());
					return null;
				}
				
				if(quotationDTO.getQuotationItem().getUnitRate() == 0D) {
					AppUtil.addWarn("Unitrate should not be zero");
					return null;
				}

				QuotationDTO quotationDTOLocal = new QuotationDTO();

				quotationDTOLocal.setBalanceAmount(quotationDTO.getBalanceAmount());
				quotationDTOLocal.setItemDescription(quotationDTO.getItemDescription());

				/**** Quotation Item Set Here ***/
				// quotationDTOLocal.getQuotationItem().getProductVarietyMaster().setName(quotationDTO.getQuotationItem().getProductVarietyMaster().getName());

				if (quotationDTO.getQuotationItem().getItemName() != null) {
					quotationDTOLocal.getQuotationItem().setItemName(quotationDTO.getQuotationItem().getItemName());
				} else {
					quotationDTOLocal.getQuotationItem()
							.setItemName(quotationDTO.getQuotationItem().getProductVarietyMaster().getName());
				}

				quotationDTOLocal.getQuotationItem().setUnitRate(quotationDTO.getQuotationItem().getUnitRate());
				quotationDTOLocal.getQuotationItem().setItemQuantity(quotationDTO.getQuotationItem().getItemQuantity());
				quotationDTOLocal.getQuotationItem().setItemAmount(quotationDTO.getQuotationItem().getItemAmount());

				log.info("discount value==> " + quotationDTO.getQuotationItem().getDiscountValue());
				if (quotationDTO.getQuotationItem().getDiscountValue() != null) {
					quotationDTOLocal.getQuotationItem()
							.setDiscountValue(quotationDTO.getQuotationItem().getDiscountValue());
				}
				log.info("discount percentage ==> " + quotationDTO.getQuotationItem().getDiscountPercent());
				if (quotationDTO.getQuotationItem().getDiscountPercent() != null) {
					Double itemAmount = quotationDTO.getQuotationItem().getItemAmount();
					Double percentage = quotationDTO.getQuotationItem().getDiscountPercent();
					Double percentageValue = (itemAmount * percentage) / 100;

					log.info("itemAmount ==> " + itemAmount);
					log.info("percentage ==> " + percentage);
					log.info("percentageValue ==> " + percentageValue);

					quotationDTOLocal.getQuotationItem().setDiscountValue(percentageValue);

					quotationDTOLocal.getQuotationItem()
							.setDiscountPercent(quotationDTO.getQuotationItem().getDiscountPercent());
				}
				quotationDTOLocal.getQuotationItem().setTotalAmount(quotationDTO.getQuotationItem().getTotalAmount());
				quotationDTOLocal.getQuotationItem().setUom(quotationDTO.getQuotationItem().getUom());
				quotationDTOLocal.getQuotationItem()
						.setProductVarietyMaster(quotationDTO.getQuotationItem().getProductVarietyMaster());
				quotationDTOLocal.getQuotationItem().setNoOfPersons(quotationDTO.getQuotationItem().getNoOfPersons());
				log.info(" Quotation Item....Tax Value ===>>>  " + taxValueGridTotal);
				if (taxValueGridTotal != null) {
					quotationDTOLocal.getQuotationItem().setTaxValue(taxValueGridTotal);

				}
				log.info(" Quotation Item....Tax Percentage ===>>>  " + taxPercentTotal);
				if (taxPercentTotal != null) {
					quotationDTOLocal.getQuotationItem().setTaxPercent(taxPercentTotal);

				}
				/**** Quotation Item Tax Set Here ***/
				if (taxSummaryList != null) {
					quotationItemTaxList = new ArrayList<QuotationDTO>();
					for (QuotationDTO taxItem : taxSummaryList) {
						// QuotationItemTax itemTax = new QuotationItemTax();
						QuotationDTO itemTax = new QuotationDTO();
						itemTax.getQuotationItemTax().setTaxValue(taxItem.getQuotationItemTax().getTaxValue());
						itemTax.getQuotationItemTax()
								.setProductVarietyTax(taxItem.getQuotationItemTax().getProductVarietyTax());
						if (taxItem.getTaxType().equalsIgnoreCase("CGST")) {
							itemTax.setCgstValue(taxItem.getQuotationItemTax().getTaxValue());
							quotationDTO.setCgstValue(taxItem.getQuotationItemTax().getTaxValue());
							cgstValue = taxItem.getQuotationItemTax().getTaxValue();
						} else if (taxItem.getTaxType().equalsIgnoreCase("SGST")) {
							itemTax.setSgstValue(taxItem.getQuotationItemTax().getTaxValue());
							quotationDTO.setSgstValue(taxItem.getQuotationItemTax().getTaxValue());
							sgstValue = taxItem.getQuotationItemTax().getTaxValue();
						}
						quotationItemTaxList.add(itemTax);
					}
					quotationDTOLocal.setQuotationItemTaxList(quotationItemTaxList);
				} else {
					quotationDTOLocal.setQuotationItemTaxList(new ArrayList<QuotationDTO>());
				}
				quotationDTOLocal.setCgstValue(cgstValue);
				quotationDTOLocal.setSgstValue(sgstValue);

				// quotationDTOLocal.getQuotationItemTax().setTaxValue(quotationDTO.getQuotationItemTax().getTaxValue());
				// quotationDTOLocal.getQuotationItemTax().setProductVarietyTax(quotationDTO.getQuotationItemTax().getProductVarietyTax());

				quotationDTOList.add(quotationDTOLocal);
				sumQuotationDetails();
				resetAllProductForms();
				log.info("Quotation Bean :::  Add Quotation Deatils Done.. ");
				// quotationDTOList.stream();
			}
		} catch (Exception e) {
			log.error(" Quotation Bean....Add Quotation Details Error.. " + e);
		}

		return null;
	}

	public void editProductVarietyDetailsListForQuotation() {
		double gstValue = 0;
		double cgstValue = 0;
		double sgstValue = 0;
		try {
			log.info(" Called....editProductVarietyDetailsListForQuotation");
			if (quotation != null) {
				log.info(" STARAT....editProductVarietyDetailsListForQuotation");
				quotationDTOList = new ArrayList<QuotationDTO>();
				List<QuotationItem> itemList = quotation.getQuotationItemList();
				for (int i = 0; i < itemList.size(); i++) {
					QuotationDTO dto = new QuotationDTO();
					dto.getQuotationItem().setId(itemList.get(i).getId());
					dto.getQuotationItem().setProductVarietyMaster(itemList.get(i).getProductVarietyMaster());
					dto.getQuotationItem().setUom(itemList.get(i).getUom());
					dto.getQuotationItem().setItemName(itemList.get(i).getItemName());
					dto.getQuotationItem().setItemQuantity(itemList.get(i).getItemQuantity());
					dto.getQuotationItem().setUnitRate(itemList.get(i).getUnitRate());
					dto.getQuotationItem().setItemAmount(itemList.get(i).getItemAmount());
					dto.getQuotationItem().setTotalAmount(itemList.get(i).getTotalAmount());

					dto.getQuotationItem().setDiscountPercent(itemList.get(i).getDiscountPercent());
					dto.getQuotationItem().setDiscountValue(itemList.get(i).getDiscountValue());

					dto.getQuotationItem().setTaxPercent(itemList.get(i).getTaxPercent());
					dto.getQuotationItem().setTaxValue(itemList.get(i).getTaxValue());

					List<QuotationItemTax> itemTaxList = itemList.get(i).getQuotationItemTaxList();

					if (itemTaxList != null) {
						gstValue = 0;
						cgstValue = 0;
						sgstValue = 0;
						quotationItemTaxList = new ArrayList<QuotationDTO>();
						for (QuotationItemTax itemTax : itemTaxList) {
							String taxCode = "";
							QuotationDTO itemTaxLocal = new QuotationDTO();
							itemTaxLocal.getQuotationItemTax().setId(itemTax.getId());
							itemTaxLocal.getQuotationItemTax().setProductVarietyTax(itemTax.getProductVarietyTax());
							itemTaxLocal.getQuotationItemTax().setTaxValue(itemTax.getTaxValue());
							taxCode = itemTax.getProductVarietyTax().getTaxMaster().getTaxCode();
							if (taxCode.equalsIgnoreCase("CGST")) {
								cgstValue = itemTax.getTaxValue();
								itemTaxLocal.setCgstValue(cgstValue);
							} else if (taxCode.equalsIgnoreCase("SGST")) {
								sgstValue = itemTax.getTaxValue();
								itemTaxLocal.setSgstValue(sgstValue);
							}

							else if (taxCode.equalsIgnoreCase("GST")) {
								gstValue = itemTax.getTaxValue();
								itemTaxLocal.setGstValue(gstValue);
							}
							quotationItemTaxList.add(itemTaxLocal);
						}
						dto.setQuotationItemTaxList(quotationItemTaxList);
					}
					dto.setCgstValue(cgstValue);
					dto.setSgstValue(sgstValue);
					dto.setGstValue(gstValue);
					quotationDTOList.add(dto);
				}
				sumQuotationDetails();
				log.info(" END..editProductVarietyDetailsListForQuotation");
			}
		} catch (Exception e) {
			log.error(" Error at editProductVarietyDetailsListForQuotation.. " + e);
		}

	}

	public void addTaxSummaryDetails() {
		log.info("addTaxSummaryDetails ::  start");
		if (quotationDTO != null) {
			QuotationDTO tax = new QuotationDTO();
			log.info("Taxpercentage ==> " + quotationDTO.getTaxPercentage());
			tax.setTaxPercentage(quotationDTO.getTaxPercentage());
			log.info("Tax value ==> " + quotationDTO.getQuotationItemTax().getTaxValue());
			tax.getQuotationItemTax().setTaxValue(quotationDTO.getQuotationItemTax().getTaxValue());
			log.info("Product variety tax ==> " + quotationDTO.getQuotationItemTax().getProductVarietyTax().getId());
			tax.getQuotationItemTax().setProductVarietyTax(quotationDTO.getQuotationItemTax().getProductVarietyTax());
			ProductVarietyTax varietyTax = quotationDTO.getQuotationItemTax().getProductVarietyTax();
			if (varietyTax != null) {
				tax.setTaxType(varietyTax.getTaxMaster().getTaxCode());
			} else {
				tax.setTaxType("");
			}
			taxSummaryList.add(tax);
			sumTaxDetails();
			quotationDTO.setTaxPercentage(null);
			quotationDTO.getQuotationItemTax().setTaxValue(null);
		}
	}

	public void removeTaxSummaryList(QuotationDTO dto) {

		if (taxSummaryList != null) {
			taxSummaryList.remove(dto);
			sumTaxDetails();
			// taxSummaryList.stream().collect(Collectors.summingDouble(QuotationDTO ::
			// getTaxValue ));
		}

	}

	public void removeQuotationItemAndTaxDtls(QuotationDTO dto) {
		if (quotationDTOList != null) {
			quotationDTOList.remove(dto);
			sumQuotationDetails();
			if (action.equalsIgnoreCase("Edit")) {
				deleteQuotationItem(dto);
			}
		}
	}

	public void sumTaxDetails() {
		double taxTotalValue = 0;
		int taxPercentTotalLocal = 0;
		if (taxSummaryList != null && taxSummaryList.size() > 0) {
			for (QuotationDTO quotationDTO : taxSummaryList) {
				if (quotationDTO.getQuotationItemTax().getTaxValue() != null) {
					taxTotalValue += quotationDTO.getQuotationItemTax().getTaxValue();
				} else {
					taxValueGridTotal = null;
				}
				taxValueGridTotal = taxTotalValue;
				taxPercentTotalLocal += quotationDTO.getTaxPercentage();
				taxPercentTotal = taxPercentTotalLocal;
			}
		} else {
			taxValueGridTotal = null;
			taxPercentTotal = 0;
		}

	}

	public void sumQuotationDetails() {
		double itemTotal = 0.00;
		double discountTotal = 0.00;
		// double taxTotal = 0.00;
		double netTotal = 0.00;
		double unitRateTotal = 0;
		double sgstTotal = 0;
		double cgstTotal = 0;
		double qtyTotal = 0;
		try {
			log.info(" called sumQuotationDetails");
			if (quotationDTOList != null) {
				log.info(" Quotation Bean ::: STARAT sumQuotationDetails");
				for (int i = 0; i < quotationDTOList.size(); i++) {
					if (quotationDTOList.get(i).getQuotationItem().getItemAmount() != null) {
						itemTotal += quotationDTOList.get(i).getQuotationItem().getItemAmount();
					}
					if (quotationDTOList.get(i).getQuotationItem().getDiscountValue() != null) {
						discountTotal += quotationDTOList.get(i).getQuotationItem().getDiscountValue();
					}

					if (quotationDTOList.get(i).getQuotationItem().getTotalAmount() != null) {
						netTotal += quotationDTOList.get(i).getQuotationItem().getTotalAmount();
					}

					if (quotationDTOList.get(i).getQuotationItem().getUnitRate() != null) {
						unitRateTotal += quotationDTOList.get(i).getQuotationItem().getUnitRate();
					}
					if (quotationDTOList.get(i).getQuotationItem().getItemQuantity() != null) {
						qtyTotal += quotationDTOList.get(i).getQuotationItem().getItemQuantity();
					}
					// taxTotal += dto.getQuotationItemTax().getTaxValue();
					List<QuotationDTO> taxGSTList = quotationDTOList.get(i).getQuotationItemTaxList();
					if (taxGSTList != null && taxGSTList.size() > 0) {
						for (int j = 0; j < taxGSTList.size(); j++) {
							if (taxGSTList.get(j).getCgstValue() != null) {
								cgstTotal += taxGSTList.get(j).getCgstValue();
							}
							if (taxGSTList.get(j).getSgstValue() != null) {
								sgstTotal += taxGSTList.get(j).getSgstValue();
							}

						}
					} else {
						// quotationDTOList.get(i).getGstValue();
						if (quotationDTOList.get(i).getSgstValue() != null) {
							sgstTotal += quotationDTOList.get(i).getSgstValue();
						}
						if (quotationDTOList.get(i).getCgstValue() != null) {
							cgstTotal += quotationDTOList.get(i).getCgstValue();
						}
					}
				}
				quotation.setMaterialValue(itemTotal);
				quotation.setNetTotal(netTotal);
				itemValueTotal = itemTotal;
				discountValueTotal = discountTotal;
				unitRateAmountTotal = unitRateTotal;
				quantityTotal=qtyTotal;
				// taxValueTotal = taxTotal;
				netAmountTotal = netTotal;
				cgstValueTotal = cgstTotal;
				sgstValueTotal = sgstTotal;
			}
			log.info(" Quotation Bean ::: END sumQuotationDetails");
		} catch (Exception e) {
			log.error(" Quotation Bean....Sum of quotation Error occurred.. " + e);
		}
	}

	public void resetAllProductForms() {
		productType = null;
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productCategoryGroup = new ProductCategoryGroup();
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		quotationDTO = new QuotationDTO();
		taxMaster = new TaxMaster();
		productVarietyTaxs = new ArrayList<ProductVarietyTax>();
		taxSummaryList = new ArrayList<QuotationDTO>();
		// quotationItemTaxList = new ArrayList<QuotationDTO>();
		taxPercentTotal = null;
		taxValueGridTotal = null;
		productFlag = false;
		itemFlag = false;
	}

	public void resetAllQuotationForms() {
		productType = null;
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productCategoryGroup = new ProductCategoryGroup();
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		quotationDTO = new QuotationDTO();
		taxMaster = new TaxMaster();
		productVarietyTaxs = new ArrayList<ProductVarietyTax>();
		taxSummaryList = new ArrayList<QuotationDTO>();
		quotationItemTaxList = new ArrayList<QuotationDTO>();
		quotationDTOList = new ArrayList<QuotationDTO>();
		supplierTypeMaster = new SupplierTypeMaster();
		quotation = new Quotation();
		selectedQuotation = new Quotation();
		/*** Total Counts ***/
		taxPercentTotal = null;
		taxValueGridTotal = null;
		unitRateAmountTotal = null;
		discountValueTotal = null;
		itemValueTotal = null;
		netAmountTotal = null;
		cgstValueTotal = null;
		sgstValueTotal = null;
		productFlag = false;
		itemFlag = false;
	}

	public String submitQuotation() {

		EntityMaster entityMaster = null;
		try {
			// entityMaster =
			// loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();
			Long userId = loginBean.getUserDetailSession().getId();
			log.info("loadGiftCouponRegister :: loggin userId ==> " + userId);
			entityMaster = commonDataService.getEntityInfoByLoggedInUser(SERVER_URL, userId);
			if (entityMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
				return null;
			}
			if (entityMaster != null) {
				quotation.setDandpOffice(entityMaster);
			}
			quotation.setQuotationType("Purchase");
			if (quotationDTOList != null && quotationDTOList.size() > 0) {
				quotationDTO.setQuotation(quotation);
				quotationDTO.setQuotationList(quotationDTOList);

				String url = SERVER_URL + "/quotation/saveQuotation";
				log.info(" Quotation Bean ====> submitQuotation ==>> URL " + url);
				BaseDTO response = httpService.post(url, quotationDTO);
				if (response != null && response.getStatusCode() == 0) {
					log.info(" Quotation Bean ===>> submitQuotation ::: ACTION URL ===>>>  " + action);
					if (action.equalsIgnoreCase("Create")) {
						log.info(" Quotation Bean :: successfully saved..");
						errorMap.notify(ErrorDescription.PURCHASEQUOTATION_SAVE_SUCCESS.getCode());
					} else {
						log.info(" Quotation Bean :: successfully updated..");
						errorMap.notify(ErrorDescription.PURCHASEQUOTATION_UPDATE_SUCCESS.getCode());
					}
				} else if (response != null && response.getStatusCode() != 0) {
					if (action.equalsIgnoreCase("Create")) {
						log.info(" Quotation Bean :: Save failed");
						errorMap.notify(ErrorDescription.PURCHASEQUOTATION_SAVE_FAILED.getCode());
					} else {
						log.info(" Quotation Bean :: update failed");
						errorMap.notify(ErrorDescription.PURCHASEQUOTATION_UPDATE_FAILED.getCode());
					}
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					log.info("Quotation save failed .....");
				}
			} else {
				log.info(" Product Details Quotation List is EMPTY ====>>>> Quotation ITEM...");
				errorMap.notify(ErrorDescription.PURCHASEQUOTATION_PRODUCTITEM_EMPTY.getCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}

		return showAllListForQuotation();
	}

	public void deleteQuotationItem(QuotationDTO quotationDto) {
		Long quotationItemId = null;
		quotationItemId = quotationDto.getQuotationItem().getId();
		try {
			if (quotationItemId != null) {
				log.info(" Called ..QuotationBean.. deleteQuotationItem  ID==  >>> " + quotationItemId);
				BaseDTO response = httpService
						.delete(SERVER_URL + "/quotation/deleteQuotationItemById/" + quotationItemId);
				if (response != null && response.getStatusCode() == 0) {
					log.info(" Quotation Item deleted successfully..");
					errorMap.notify(ErrorDescription.PURCHASEQUOTATION_ITEM_DELETE_SUCCESS.getErrorCode());
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Quotation Item Grid ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends QuotationBean.deleteQuotationItem ===========>");
	}

	public String quotationListPageAction() {
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("called add Quotation page");
				resetAllQuotationForms();
				getAllSupplierTypeList();
				getUomMasterList();
				return CREATE_PURCHASE_QUOTATION_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("called view Quotation  page");

				editQuotationByQuotationId();
				viewQuotationDetails();

				return VIEW_PURCHASE_QUOTATION_PAGE;

			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("called Edit Quotation page");
				editQuotationByQuotationId();
				editProductVarietyDetailsListForQuotation();
				return CREATE_PURCHASE_QUOTATION_PAGE;

			} else if (action.equalsIgnoreCase("Delete")) {
				log.info("delete Quotation  called.." + quotation.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmQuotationDelete').show();");
			} else if (action.equalsIgnoreCase("Clear")) {
				log.info("called Clear Quotation page");
				return showAllListForQuotation();
			}
		} catch (Exception e) {
			log.error(" Exception at quotationListPageAction " + e);
		}

		return null;
	}

	/*** Quotation Grid OnRow Select Here ***/
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Quotation.onRowSelect ========>" + event);
		selectedQuotation = ((Quotation) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends Quotation.onRowSelect ========>");
	}

	public String showAllListForQuotation() {
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		resetAllQuotationForms();
		loadLazyQuotationList();
		return LIST_PURCHASE_QUOTATION_PAGE;
	}

	public String backToListPage() {
		resetAllQuotationForms();
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		isCircleFlag = false;
		return LIST_PURCHASE_QUOTATION_PAGE;
	}

	public void loadLazyQuotationList() {
		log.info("<===== Starts Marital Ststus Bean.loadLazyMediaMasterList List ======>");
		String QUOTATION_LIST_URL = SERVER_URL + "/quotation/getallQuotationlistlazy";

		quotationLazyList = new LazyDataModel<Quotation>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<Quotation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for Quotation List=============>" + QUOTATION_LIST_URL);
					BaseDTO response = httpService.post(QUOTATION_LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						quotationList = mapper.readValue(jsonResponse, new TypeReference<List<Quotation>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyQuotationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return quotationList;
			}

			@Override
			public Object getRowKey(Quotation res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Quotation getRowData(String rowKey) {
				try {
					for (Quotation quotationObj : quotationList) {
						if (quotationObj.getId().equals(Long.valueOf(rowKey))) {
							quotation = quotationObj;
							return quotationObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Quotation Bean.loadLazyQuotationList List ======>");
	}

	public String deleteQuotationDetails() {
		Long quotationId = null;
		quotationId = quotation.getId();
		try {
			if (quotationId != null) {
				log.info(" Called ..QuotationBean.. deleteQuotationDetails  ID==  >>> " + quotationId);
				BaseDTO response = httpService.delete(SERVER_URL + "/quotation/deleteQuotationById/" + quotationId);
				if (response != null && response.getStatusCode() == 0) {
					log.info(" Quotation Details deleted successfully..");
					errorMap.notify(ErrorDescription.PURCHASEQUOTATION_DELETE_SUCCESS.getErrorCode());
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmQuotationDelete').hide();");
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Quotation  ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends QuotationBean.deleteQuotationDetails ===========>");
		return showAllListForQuotation();
	}

	public void calculateItemAmount() {
		if (quotationDTO != null) {
			if(quotationDTO.getQuotationItem()!=null) {
				if(quotationDTO.getQuotationItem().getItemQuantity()!=null && quotationDTO.getQuotationItem().getUnitRate()!=null) {
					log.info("Quotation Item Qunatity::::  " + quotationDTO.getQuotationItem().getItemQuantity());
					log.info("Quotation Item UNIT RATE::::  " + quotationDTO.getQuotationItem().getUnitRate());
					quotationDTO.getQuotationItem().setItemAmount(quotationDTO.getQuotationItem().getUnitRate()*quotationDTO.getQuotationItem().getItemQuantity());
					log.info("Quotation Item Total ::::  " + quotationDTO.getQuotationItem().getItemAmount());
				}
			}
			
		}
	}
	
	public void validateTDSPercentage() {
		if(quotation.getTdsPercentage()>100) {
		AppUtil.addWarn("Please give valid TDS percentage");
		}
	}
}
