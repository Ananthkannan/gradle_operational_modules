package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.OralTestSelection;
import in.gov.cooptex.core.model.OralTestSelectionDetails;
import in.gov.cooptex.core.model.OralTestSelectionSummary;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("oralTestSelectionBean")
public class OralExamSelectionBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Getter
	@Setter
	OralTestSelection oralTestSelection;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	LazyDataModel<OralTestSelection> oralTestSelectionList;

	@Getter
	@Setter
	OralTestSelection selectedOralTestSelection;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;

	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	List<OralTestSelectionDetails> oralTestSelectionDetails;

	@Getter
	@Setter
	List<OralTestSelectionDetails> oralTestSelectedApplins = new ArrayList<>();

	@Getter
	@Setter
	List<Designation> designationList;

	@Autowired
	WrittenExaminationBean writtenExaminationBean;

	/**
	 * purpose:Initial data once Oral test selection based on written exam is done
	 * 
	 * @return redirect to create Oral Test Selection page
	 */
	public String loadInitialData() {
		mapper = new ObjectMapper();
		log.info("OralTestSelectionBean.loadInitialData");
		try {
			action = "INITIAL";
			oralTestSelection = new OralTestSelection();
			if (writtenExaminationBean.selectedWrittenExamMark == null) {
				Util.addWarn("Please select one Written Exam");
				return null;
			}
			if (writtenExaminationBean.selectedWrittenExamMark.getStatus().equalsIgnoreCase("InProgress")) {
				errorMap.notify(ErrorDescription.ORAL_SELECTION_ALREADY_APPROVED.getErrorCode());
				return null;
			}

			log.info("Server url...." + SERVER_URL + "/oralselection/getintial/"
					+ writtenExaminationBean.selectedWrittenExamMark.getId());
			BaseDTO response = httpService.get(
					SERVER_URL + "/oralselection/getintial/" + writtenExaminationBean.selectedWrittenExamMark.getId());

			log.info("basedto..." + response);
			if (response.getStatusCode() != 90109) {
				errorMap.notify(response.getStatusCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			oralTestSelection = mapper.readValue(jsonResponse, new TypeReference<OralTestSelection>() {
			});

			return "/pages/personnelHR/generateOralExamSelection.xhtml?faces-redirect=true";

		} catch (Exception e) {

			log.error("Exception occured while fetching initial data...", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	/**
	 * purpose:post Oral Test details generated to server
	 * 
	 * @return redirect to list page or notify any errors if occured
	 */
	public String saveOralTestSelection() {
		log.info("RosterReservationBean.saveRoster called ");
		try {
			mapper = new ObjectMapper();
			UserMaster userMaster = loginBean.getUserDetailSession();
			if (oralTestSelection == null) {
				AppUtil.addWarn("Roster is empty");
				return null;
			}

			oralTestSelection.setCreatedBy(userMaster);
			log.info("roster reservation remarks...." + oralTestSelection.getRemarks());
			BaseDTO response = httpService.post(SERVER_URL + "/oralselection/add", oralTestSelection);
			if (response != null) {
				log.info("Response recieved from RestAPI ..." + response);
				errorMap.notify(response.getStatusCode());
				loadOralTestSelectionList();
			} else {
				log.info("Response from Rest API is null....");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("Exception occured while saving roster .... ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return "/pages/personnelHR/listOralExamSelection.xhtml?faces-redirect=true";
	}

	/**
	 * purpose : load oral test selection list
	 * 
	 * @return redirect to list page
	 */
	public String loadOralTestSelectionList() {
		log.info("RosterReservationBean.loadReservationList called");
		try {
			selectedOralTestSelection = new OralTestSelection();
			loadLazyOralTestSelection();
			loadAllDesignationList();
		} catch (Exception e) {
			log.error("Exception Occured while fetching rosterList ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listOralExamSelection.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:load oral test selection list using lazy datamodel
	 * 
	 */
	public void loadLazyOralTestSelection() {

		log.info("< --  Start Oral test Lazy Load -- > ");
		oralTestSelectionList = new LazyDataModel<OralTestSelection>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<OralTestSelection> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<OralTestSelection> data = mapper.readValue(jsonResponse,
							new TypeReference<List<OralTestSelection>>() {
							});
					if (data == null) {
						log.info(" Roster search response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Roster search Successfully Completed");
					} else {
						log.error(" Roster search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading roster data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(OralTestSelection res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OralTestSelection getRowData(String rowKey) {
				List<OralTestSelection> responseList = (List<OralTestSelection>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (OralTestSelection res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedOralTestSelection = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	/**
	 * purpose will call server when search filtered
	 * 
	 * @param first
	 *            - page number
	 * @param pageSize
	 *            - how many records need to be fetched
	 * @param sortField
	 *            - which filed to be sorted
	 * @param sortOrder
	 *            - sort order that to be followed (ascending or descending)
	 * @param filters
	 *            - search filters
	 * @return BaseDTO with list of roster reservation based filters and pages size
	 */
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			OralTestSelection request = new OralTestSelection();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("writtenExamMarkList.jobAdvertisement.notificationNumber")) {
					log.info("Reference NUmber : " + value);
					searchFilters.put("notificationNumber", value);
				}

				if (entry.getKey().equals("writtenExamMarkList.recruitmentPost")) {
					log.info("recruitment for post : " + value);

					searchFilters.put("recruitmentPost", value);
				}

				if (entry.getKey().equals("writtenExamMarkList.recruitmentYear")) {
					log.info("Recruitment year : " + value);
					searchFilters.put("recruitmentYear", Integer.valueOf(value));

				}
				if (entry.getKey().equals("createdDate")) {
					log.info("created Date : " + value);

					searchFilters.put("createdDate", value);

				}
				if (entry.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/oralselection/getalllazy", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Roster search generate data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void loadAllDesignationList() {
		log.info("... Load Designation List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/designation/getalldesignation");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public String cancel() {
		return loadOralTestSelectionList();
	}

	public String clear() {
		action = "";
		selectedOralTestSelection = new OralTestSelection();
		oralTestSelection = new OralTestSelection();
		approveButtonFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		return loadOralTestSelectionList();
	}

	public String viewOralDetails(OralTestSelectionSummary oralTestSelectionSummary) {
		log.info("VIew.....");
		oralTestSelectionDetails = new ArrayList<OralTestSelectionDetails>();
		if (oralTestSelectionSummary.getOralTestSelectionDetails() != null
				&& oralTestSelectionSummary.getOralTestSelectionDetails().size() > 0) {
			log.info("Size.........." + oralTestSelectionSummary.getOralTestSelectionDetails().size());
			oralTestSelectionDetails = oralTestSelectionSummary.getOralTestSelectionDetails();
			if (oralTestSelectionDetails != null && oralTestSelectionDetails.size() > 0) {
				oralTestSelectedApplins = new ArrayList<>();
				oralTestSelectedApplins = oralTestSelectionDetails.stream()
						.filter(article -> article.getIsWrittenPassed() != null && article.getIsWrittenPassed())
						.collect(Collectors.toList());
			}
			action = "SelectedCandidates";
		} else {
			oralTestSelectedApplins = new ArrayList<>();
		}
		return null;
	}

	public String viewOralTestSelection() {
		try {
			log.info("View Oral Test Selection...... called");
			if (selectedOralTestSelection == null) {
				AppUtil.addWarn("Please select one oral test selection");
				return null;
			}

			oralTestSelection = selectedOralTestSelection;
			log.info("Server url...." + SERVER_URL + "/oralselection/get/" + oralTestSelection.getId());
			BaseDTO response = httpService.get(SERVER_URL + "/oralselection/get/" + oralTestSelection.getId());

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			oralTestSelection = mapper.readValue(jsonResponse, new TypeReference<OralTestSelection>() {
			});

		} catch (Exception e) {
			log.error("Exception occured while approving Oral Test Selection", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return "/pages/personnelHR/viewOralExamSelection.xhtml?faces-redirect=true";
	}

	/**
	 * purpose: get selected roster and details will be displayed in approval page
	 * 
	 * @return to roster approval page
	 */
	public String approveOralTestSelection() {
		try {
			log.info("Approve Oral Test Selection...... called");
			if (selectedOralTestSelection == null) {
				AppUtil.addWarn("Please select one Oral Test Selection");
				return null;
			}
			if (selectedOralTestSelection.getStatus().equals("Approved")) {
				AppUtil.addWarn("Roster already approved");
				return null;
			}

			oralTestSelection = selectedOralTestSelection;
			log.info("Server url...." + SERVER_URL + "/oralselection/get/" + oralTestSelection.getId());
			BaseDTO response = httpService.get(SERVER_URL + "/oralselection/get/" + oralTestSelection.getId());

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			oralTestSelection = mapper.readValue(jsonResponse, new TypeReference<OralTestSelection>() {
			});

		} catch (Exception e) {
			log.error("Exception occured while approving Oral Test Selection", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return "/pages/personnelHR/approveOralExamSelection.xhtml?faces-redirect=true";
	}

	/**
	 * purpose : roster will be approved once confirmation is given
	 * 
	 * @return redirect to roster list page
	 */
	public String confirmApproveOralTestSelection() {
		log.info("confirm approval....." + oralTestSelection.getStatus());
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.put(SERVER_URL + "/oralselection/approveorreject", oralTestSelection);
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while loading department....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadOralTestSelectionList();
	}

	public String beforeOralSelectionApproval() {
		int count = 0;
		log.info("before roster reservation...." + selectedOralTestSelection.getId());
		try {
			if (selectedOralTestSelection == null) {
				Util.addWarn("Please select one roster");
				return null;
			} else {

				if (oralTestSelection != null) {

					if (oralTestSelection.getOralTestSelectionSummary() == null
							|| oralTestSelection.getOralTestSelectionSummary().size() <= 0) {
						
						Long sum = oralTestSelection
								.getOralTestSelectionSummary().stream().filter(o -> o.getTotalNumberOfCandidate() > 0).mapToLong(o -> o.getTotalNumberOfCandidate()).sum();
						
						if (sum == 0) {
							Util.addWarn("No Candidates are available for Selection");
						}
					}
				}

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmRosterApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/**
	 * purpose:each time row selected in list page this method will be called to
	 * enable,disable buttons in list page
	 * 
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedOralTestSelection = ((OralTestSelection) event.getObject());

		addButtonFlag = true;
		viewButtonFlag = true;
		if (selectedOralTestSelection.getStatus().equalsIgnoreCase("Approved")) {
			deleteFlag = false;
			approveButtonFlag = false;
			generateFlag = false;

		} else if (selectedOralTestSelection.getStatus().equalsIgnoreCase("Rejected")) {
			approveButtonFlag = false;
			generateFlag = true;
			deleteFlag = false;
		} else if (selectedOralTestSelection.getStatus().equalsIgnoreCase("InProgress")) {
			generateFlag = true;
			approveButtonFlag = true;

			deleteFlag = true;
		}
	}

	/**
	 * purpose : will display pop up when delete clicked in list page
	 * 
	 * @return
	 */
	public String deleteOralTestSelection() {
		try {
			if (selectedOralTestSelection == null) {
				AppUtil.addWarn("Please select one oral test selection");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmOralTestDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting .... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose:will delete the selected record if confirmed in dialog and redirect
	 * to list page
	 * 
	 * @return
	 */
	public String confirmDeleteOralTestSelection() {
		log.info("... delete Oral Test called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.delete(SERVER_URL + "/oralselection/delete/" + selectedOralTestSelection.getId());
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadOralTestSelectionList();
	}

}
