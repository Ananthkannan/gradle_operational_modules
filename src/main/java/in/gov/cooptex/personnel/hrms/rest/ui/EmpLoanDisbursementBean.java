package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SalaryDTO;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.hrms.dto.EmpLoanDisbursementRequestDto;
import in.gov.cooptex.personnel.hrms.loans.model.EmpLoanDisbursement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("empLoanDisbursementBean")
public class EmpLoanDisbursementBean {

	private final String LOAN_DISBURSEMENT_CREATE_PAGE = "/pages/admin/loanAdvance/createLoanDisbursement.xhtml?faces-redirect=true;";
	private final String LOAN_DISBURSEMENT_LIST_URL = "/pages/admin/loanAdvance/listLoanDisbursement.xhtml?faces-redirect=true;";
	private final String LOAN_DISBURSEMENT_VIEW_PAGE = "/pages/admin/loanAdvance/viewLoanDisbursement.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails, selectedEmployeeLoanAndAdvanceDetails;

	@Getter
	@Setter
	LazyDataModel<EmployeeLoanAndAdvanceDetails> lazyEmployeeLoanAndAdvanceDetailsList;

	List<EmployeeLoanAndAdvanceDetails> employeeLoanAndAdvanceDetailsList = new ArrayList<EmployeeLoanAndAdvanceDetails>();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	List<BankMaster> bankMasterList = new ArrayList<BankMaster>();

	@Getter
	@Setter
	BankMaster selectedBank = new BankMaster();

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	EmployeeMaster suretyDetails = new EmployeeMaster();

	@Getter
	@Setter
	BankBranchMaster selectedBankBranch = new BankBranchMaster();

	@Getter
	@Setter
	List<BankBranchMaster> bankBranchMasterList = new ArrayList<BankBranchMaster>();

	@Getter
	@Setter
	List<PaymentMethod> paymentMethodList = new ArrayList<PaymentMethod>();

	@Getter
	@Setter
	EmpLoanDisbursement empLoanDisbursement = new EmpLoanDisbursement();

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	EmpLoanDisbursementRequestDto empLoanDisbursementRequestDto = new EmpLoanDisbursementRequestDto();
	
	

	@Getter
	@Setter
	Long loanTenure = 0L;

	@Autowired
	AppPreference appPreference;
	
	private static final double BTF_LOAN_AMOUNT = Double.valueOf(100000);
	
	private static final Integer BTF_LOAN_INSTALLMENT = 60;

	public String showLoanDisbursementListPage() {
		log.info("<==== Starts LoanDisbursementBean.showLoanDisbursementListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
		selectedEmployeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
		loadLazyLoanDisbursementList();
		log.info("<==== Ends LoanDisbursementBean.showLoanDisbursementListPage =====>");
		return LOAN_DISBURSEMENT_LIST_URL;
	}

	public String showCreatePage() {
		if (selectedEmployeeLoanAndAdvanceDetails == null) {
			log.info("<==== Selected Loan Advance Value is Empty =====>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		selectedBank = new BankMaster();
		selectedBankBranch = new BankBranchMaster();
		log.info("<==== Starts LoanDisbursementBean.showCreatePage =====>");
		bankMasterList = commonDataService.getBankDetails();
		loadPaymentMethodList();
		getLoanAndAdvancementById();
		log.info("<==== End LoanDisbursementBean.showCreatePage =====>");
		if (action.equals("ADD")) {
			return LOAN_DISBURSEMENT_CREATE_PAGE;
		} else {
			employeeLoanAndAdvanceDetails=new EmployeeLoanAndAdvanceDetails();
			try
			{
				Long disbursementId = selectedEmployeeLoanAndAdvanceDetails.getId();
				log.info("===Disbursement Id====" + disbursementId);
				String url = serverURL + "/emploandisbursement/getbyLoanDisbursementId/" + disbursementId;
				log.info("====Url is=====" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					empLoanDisbursement = mapper.readValue(jsonResponse, new TypeReference<EmpLoanDisbursement>() {
					});
					log.info("===employeeLoanAndAdvanceDetails is===="+employeeLoanAndAdvanceDetails.toString());
					return LOAN_DISBURSEMENT_VIEW_PAGE;
				}
			}catch (Exception e) {
				log.error("===Exception  is===="+e.getMessage());
			}
			return null;
		}

	}

	public void loadPaymentMethodList() {
		log.info("<===== Starts LoanDisbursementBean.loadPaymentMethodList; ===========>");
		String url = serverURL + appPreference.getOperationApiUrl() + "/accounts/payment/getpaymentmethod";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			paymentMethodList = mapper.readValue(jsonResponse, new TypeReference<List<PaymentMethod>>() {
			});
			log.info("paymentMethodList", paymentMethodList);
		} catch (Exception exp) {
			log.error("Exception in loadPaymentMode method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("<===== End LoanDisbursementBean.loadPaymentMethodList ===========>");

	}

	public void loadBankBranchList() {
		log.info("<===== Starts LoanDisbursementBean.loadBankBranchList; ===========>");
		bankBranchMasterList = commonDataService.getBranchDetails(selectedBank.getId());
		log.info("<===== End LoanDisbursementBean.loadBankBranchList; ===========>");
	}

	public String getLoanAndAdvancementById() {
		log.info("<===== Starts LoanDisbursementBean.getLoanAndAdvancementById; ===========>");
		try {
			String url = serverURL + "/loanandadvance/getbyid/" + selectedEmployeeLoanAndAdvanceDetails.getId();
			log.info("Requested URL : ==>" + url);
			BaseDTO baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeLoanAndAdvanceDetails = mapper.readValue(jsonResponse, EmployeeLoanAndAdvanceDetails.class);
			log.info("designation Object Employee Master==> " + employeeLoanAndAdvanceDetails.getEmployeeMaster());
			employeeMaster = employeeLoanAndAdvanceDetails.getEmployeeMaster();

		} catch (Exception e) {
			log.error("Exception occured getLoanAndAdvancementById() ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== End LoanDisbursementBean.getLoanAndAdvancementById; ===========>");
		return null;
	}

	public String saveLoanDisbursement() {
		log.info("<==== Starts LoanDisbursementBean.saveLoanDisbursement =======>");
		try {
			if (empLoanDisbursement.getEcsAmount() != null) {
				if (empLoanDisbursement.getEcsAmount() > employeeLoanAndAdvanceDetails.getLoanAmountRequired()) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.AMOUNT_NOT_GREATER_LOAN_AMOUNT).getCode());
					return null;
				}
			}
			if(empLoanDisbursement.getPaymentMethod().getCode().equalsIgnoreCase("CHEQUE")) {
				if(empLoanDisbursement.getChequeAmount() > employeeLoanAndAdvanceDetails.getSanctionAmount()) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.AMOUNT_NOT_GREATER_LOAN_AMOUNT).getCode());
					return null;
				}
			}
			String url = serverURL + "/emploandisbursement/saveloandisbursement";
			if (selectedBank.getId() != null && selectedBankBranch.getId() != null) {
				empLoanDisbursement.setBankMaster(selectedBank);
				empLoanDisbursement.setBankBranchMaster(selectedBankBranch);
			}
			Long loanAndvanceId = employeeLoanAndAdvanceDetails.getId();
			empLoanDisbursementRequestDto.setLoanAdvanceId(loanAndvanceId);
			empLoanDisbursementRequestDto.setEmpLoanDisbursement(empLoanDisbursement);
			empLoanDisbursementRequestDto.setLoanType(selectedEmployeeLoanAndAdvanceDetails.getLoanType());
			BaseDTO response = httpService.post(url, empLoanDisbursementRequestDto);
			if (response != null && response.getStatusCode() == 0) {
				log.info("LoanDisbursement saved successfully..........");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.LOAN_DISBURSEMENT_SAVE_SUCCESS).getCode());
				employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
				selectedEmployeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
				empLoanDisbursement = new EmpLoanDisbursement();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends LoanDisbursementBean.saveLoanDisbursement =======>");
		return showLoanDisbursementListPage();
	}

	public void loadLazyLoanDisbursementList() {
		log.info("<===== Starts LoanDisbursementBean.loadLazyLoanDisbursementList ======>");
		lazyEmployeeLoanAndAdvanceDetailsList = new LazyDataModel<EmployeeLoanAndAdvanceDetails>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -7079199200346366750L;

			@Override
			public List<EmployeeLoanAndAdvanceDetails> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/emploandisbursement/searchandload";
					log.info("Lazy load url is----------->>>>>" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeeLoanAndAdvanceDetailsList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeLoanAndAdvanceDetails>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("Total Record Count----------->>>>>" + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyCityCompensatoryAllowanceList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeeLoanAndAdvanceDetailsList;
			}

			@Override
			public Object getRowKey(EmployeeLoanAndAdvanceDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeLoanAndAdvanceDetails getRowData(String rowKey) {
				try {
					for (EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails : employeeLoanAndAdvanceDetailsList) {
						if (employeeLoanAndAdvanceDetails.getId().equals(Long.valueOf(rowKey))) {
							selectedEmployeeLoanAndAdvanceDetails = employeeLoanAndAdvanceDetails;
							return employeeLoanAndAdvanceDetails;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends LoanDisbursementBean.loadLazyLoanDisbursementList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts LoanDisbursementBean.onRowSelect ========>" + event);
		selectedEmployeeLoanAndAdvanceDetails = ((EmployeeLoanAndAdvanceDetails) event.getObject());
		if (selectedEmployeeLoanAndAdvanceDetails.getStatus().equals("LOAN DISBURSED") || selectedEmployeeLoanAndAdvanceDetails.getStatus().equals("ADVANCE DISBURSED")) {
			addButtonFlag = false;
			viewButtonFlag=true;
		} else {
			addButtonFlag = true;
			viewButtonFlag=false;
		}
		log.info("<===Ends GlAccountCategoryBean.onRowSelect ========>");
	}

	public String clear() {
		log.info("<===== Start LoanDisbursementBean.clear ======>");
		employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
		selectedEmployeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
		empLoanDisbursement = new EmpLoanDisbursement();
		log.info("<===== Ends LoanDisbursementBean.clear ======>");
		return showLoanDisbursementListPage();
	}

	/**
	 * Purpose : to calculate loan EMI
	 * 
	 * @author krishnakumar
	 */

	public void calculateEMIAmount() {
		log.info("<=EmpLoanDisbursementBean :: calculateLoanEMI=>");
		try {
			if (employeeLoanAndAdvanceDetails.getLoanAmountRequired() == null
					&& employeeLoanAndAdvanceDetails.getAdvanceAmountRequired() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.LOAN_AMOUNT_IS_MANDATORY).getCode());
				return;
			}
			if (empLoanDisbursement.getLoanTenure() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.LOAN_TENURE_IS_MANDATORY).getCode());
				return;
			}
			if(empLoanDisbursement.getFirstInstallmentAmount()==null) {
				AppUtil.addWarn("first installment amount is Empty");
				return;
			}
			if(employeeLoanAndAdvanceDetails.getSanctionAmount() < empLoanDisbursement.getFirstInstallmentAmount()) {
				AppUtil.addWarn("first installment amount not greater than sanction amount");
				return;
			}
			Double loanAmount = 0.0;
			Integer loanInstallment = 0;
			if (employeeLoanAndAdvanceDetails.getSanctionAmount() != null) {
				loanAmount = employeeLoanAndAdvanceDetails.getSanctionAmount();
				loanInstallment = empLoanDisbursement.getLoanTenure().intValue();
			} 
			
			Double employeeEmiAmount = 0.00;
			
			//double firstInstallmentAmount = employeeLoanAndAdvanceDetails.getSanctionAmount() == null ? 0D :
			//	employeeLoanAndAdvanceDetails.getSanctionAmount();
			
			double firstInstallmentAmount = empLoanDisbursement.getFirstInstallmentAmount() == null ? 0D :
				 empLoanDisbursement.getFirstInstallmentAmount();
			
			double installmentAmount = loanAmount - firstInstallmentAmount;
			
			employeeEmiAmount = installmentAmount / (loanInstallment-1);
			
			//employeeEmiAmount = loanAmount / loanInstallment;
			
			empLoanDisbursement.setLoanEmiAmount(employeeEmiAmount);
		} catch (Exception e) {
			log.error("Exception occured in checkEmployeeSalary ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
}
