package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.NominalDetailsNotificationDto;
import in.gov.cooptex.core.enums.NominalDetailsStatus;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ExamCentre;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.JobApplicationApproval;
import in.gov.cooptex.core.model.NominalDetails;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("nominalDetailsBean")
public class NominalDetailsBean {
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();

	String url;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	NominalDetails selectedNominal;

	@Setter
	@Getter
	JobAdvertisement jobAdvertisement;

	@Setter
	@Getter
	List<NominalDetails> viewnominalList, nominalDetailsList, approveNominalList, allocatedNominalList;

	@Setter
	@Getter
	List<CasteMaster> casteList;

	@Setter
	@Getter
	List<Community> communityList;

	@Setter
	@Getter
	List<Designation> designations;

	@Setter
	@Getter
	List<ExamCentre> examCentreList;

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	List<Designation> appliedPostList;

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	Designation designation = new Designation();

	@Setter
	@Getter
	EntityMaster regLocation = new EntityMaster();

	@Setter
	@Getter
	List<EntityMaster> preExamCentreList, examCentreLocationList, selectedExamCentreLocation;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Boolean addButtonFlag, aprvButtonFlag, htButtonFlag = false;

	@Getter
	@Setter
	String action = "";

	@Getter
	@Setter
	LazyDataModel<NominalDetails> nominalLazyList;

	@Getter
	@Setter
	String notificationNo;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	List<JobAdvertisement> nominalNotificationList;

	@Getter
	@Setter
	List<NominalDetailsNotificationDto> notificationDtoList;

	@Getter
	@Setter
	NominalDetailsNotificationDto notificationDto, nominalDetailsRequestDto;

	@Getter
	@Setter
	List<ExamCentre> oralExamVenueList, selectedExamVenue;

	@Getter
	@Setter
	List<JobApplicationApproval> approvalList;

	int totalSeat = 0;
	
	@Setter
	@Getter
	private StreamedContent file;

	@PostConstruct
	public void init() {
		loadLazyNominalList();
		loadData();
	}

	public void loadData() {
		designationList = commonDataService.loadDesignation();
		casteList = commonDataService.getAllCasteMaster();
		communityList = commonDataService.getAllCommunity();
		examCentreList = commonDataService.loadExamCentre();
		preExamCentreList = commonDataService.getPreExamMasterList();
		notificationDtoList = commonDataService.loadJobApprovalNotificationNo();
	}

	public void onchangeNotoficationNo() {
		try {
			if (notificationDto != null) {
				log.info("selected notificationNo : [ " + notificationDto.getNotificationNo() + "]");
				selectedExamCentreLocation = new ArrayList<>();
				onchangeLocation();
				notificationDto.setPostName(notificationDto.getPostName());
				notificationDto.setReqYear(notificationDto.getReqYear());
				designation = commonDataService.loadDesignationById(notificationDto.getPostId());
			}
		} catch (Exception e) {
			log.info("found exception in onchangeNotoficationNo : ", e);
		}
	}

	public void onchangeLocation() {
		log.info("Selected Locations  getexamcentrelistbyentitymaster :: " + selectedExamCentreLocation);
		try {

			if (selectedExamCentreLocation != null && selectedExamCentreLocation.size() == 0) {
				oralExamVenueList = new ArrayList<>();
				selectedExamVenue = new ArrayList<>();
				return;
			}
			url = SERVER_URL + "/nominaldetails/getexamcentrelistbyentitymaster";
			NominalDetails request = new NominalDetails();
			request.setSelectedLocationList(selectedExamCentreLocation);

			BaseDTO response = httpService.post(url, request);

			if (response == null) {
				log.info("response in onchangeLocation:" + response);
				return;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			List<ExamCentre> examList = mapper.readValue(jsonResponse, new TypeReference<List<ExamCentre>>() {
			});
			log.info("examList size"+examList.size());
			if (examList != null) {
				oralExamVenueList = examList;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(" Exception Occured ", e);
		}

		log.info(" Exam Venue ::" + oralExamVenueList);

	}

	public String loadNominalList() {
		log.info(" Inside Load Nominal List Page");
		clear();
		addButtonFlag = true;
		htButtonFlag = true;
		aprvButtonFlag = true;
		loadLazyNominalList();
		return "/pages/personnelHR/listNominalList.xhtml?faces-redirect=true";
	}

	public String clearNominal() {
		log.info(" Inside Nominal Details Clear" + action);
		designation = null;
		regLocation = null;
		allocatedNominalList = new ArrayList<>();
		notificationDto = new NominalDetailsNotificationDto();
		selectedExamCentreLocation = new ArrayList<>();
		selectedExamVenue = new ArrayList<>();
		onchangeLocation();
		designation = new Designation();
		return null;

	}

	public String clear() {
		log.info(" Inside Nominal Details Clear" + action);
		selectedNominal = new NominalDetails();
		addButtonFlag = true;
		htButtonFlag = true;
		aprvButtonFlag = true;
		return "/pages/personnelHR/listNominalList.xhtml?faces-redirect=true";

	}

	public void onRowSelect(SelectEvent event) {
		selectedNominal = ((NominalDetails) event.getObject());
		log.info("selectedNominal ::" + selectedNominal);
		if (selectedNominal.getStatus().equals(NominalDetailsStatus.APPROVED)) {
			aprvButtonFlag = false;
			addButtonFlag = false;
			htButtonFlag = true;
		} else if (selectedNominal.getStatus().equals(NominalDetailsStatus.INPROGRESS)) {
			aprvButtonFlag = true;
			htButtonFlag = false;
			addButtonFlag = false;
		}
	}

	/**
	 * @author Bhaskar
	 * 
	 */
	public String loadNominalListApproval(NominalDetails nominal) {
		if (nominal == null || nominal.getJobApplication() == null
				|| nominal.getJobApplication().getJobAdvertisement() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber().isEmpty()) {
			log.error(" Selected Hall Ticket Notification Number is empty");
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return null;
		}

		if (nominal.getStatus().equals(NominalDetailsStatus.APPROVED)) {
			log.info(" Nominal Details is already Approved ");
			errorMap.notify(ErrorDescription.NOMINAL_IS_ALREADY_APPROVED.getErrorCode());
			return null;
		}
		log.info("Inside loadNominalListApproval :: Selected Nominal Details Notification number  :- "
				+ nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		approveNominalList = loadNominalListByAppliedPost(
				nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		if (approveNominalList == null) {
			log.error(" Nominal List Returned Null Values");
			errorMap.notify(ErrorDescription.SELECTED_NOMINAL_IS_EMPTY.getErrorCode());
		}
		return "/pages/personnelHR/nominalListApproval.xhtml?faces-redirect=true";
	}

	public String viewAllNominalList(NominalDetails nominal) {
		if (nominal == null || nominal.getJobApplication() == null
				|| nominal.getJobApplication().getJobAdvertisement() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber().isEmpty()) {
			log.error(" Selected Hall Ticket Notification Number is empty");
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return null;
		}
		log.info(" ::  Inside LOad Hall Ticket List :: "
				+ nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		viewnominalList = viewNominalListByAppliedPost(
				nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		if (viewnominalList == null) {
			log.error(" Nominal List Returned Null Values");
			errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
		}
		return "/pages/personnelHR/viewNominalList.xhtml?faces-redirect=true";
	}

	/**
	 * @author Bhaskar
	 * 
	 */
	public List<NominalDetails> viewNominalListByAppliedPost(String notificationNo) {
		if (notificationNo == null || notificationNo.isEmpty()) {
			log.info(" Selected Notification Number is empty ");
			errorMap.notify(ErrorDescription.NOMINAL_DETAILS_NOTIFICATION_IS_EMPTY.getErrorCode());
			return null;
		}
		log.info("Selected Notification Number is  :- " + notificationNo);
		nominalDetailsList = new ArrayList<>();
		try {
			NominalDetails request = new NominalDetails();
			request.setNotificationNo(notificationNo);
			url = SERVER_URL + "/nominaldetails/viewnominallistbynotificationno";
			log.info(" Get Hall Tickt By Notification No Id URL :: " + url);
			log.info(" Get Hall Tickt By Applied Notification No Request :: " + url);
			BaseDTO baseDTO = httpService.post(url, request);
			// log.info(" Response Get Hall Tickt By Applied Post Id URL:: " + baseDTO);
			if (baseDTO == null) {
				log.error(" BaseDto Returned Null Values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getResponseContents() != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				nominalDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<NominalDetails>>() {
				});
			} else {
				log.error(" Nominal List By Desgnation no record found");
				errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Nominal Details By Desgnation :: " + nominalDetailsList);
			} else {
				log.info("Error in Nominal Details By Desgnation with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured while Getting HallTicket Details ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return nominalDetailsList;
	}

	/**
	 * @author Bhaskar
	 * 
	 */
	public List<NominalDetails> loadNominalListByAppliedPost(String notificationNumber) {
		if (notificationNumber == null || notificationNumber.isEmpty()) {
			log.info(" Selecyted Nominal Details is empty ");
			errorMap.notify(ErrorDescription.SELECTED_NOMINAL_IS_EMPTY.getErrorCode());
		}
		log.info("<<---  Inside loadNominalListByAppliedPost :: Selected Nominal Details Notification Number  :- "
				+ notificationNumber);
		nominalDetailsList = new ArrayList<>();
		try {
			NominalDetails nominalDetails = new NominalDetails();
			nominalDetails.setNotificationNo(notificationNumber);
			url = SERVER_URL + "/nominaldetails/getnominallistbynotificationno";
			log.info(" Get Hall Tickt By Notification Number URL :: " + url);
			log.info(" Get Hall Tickt By Notification Number Request :: " + nominalDetails);
			BaseDTO baseDTO = httpService.post(url, nominalDetails);
			log.info(" Response Get Hall Tickt By Applied Post  Id URL:: " + baseDTO);
			if (baseDTO == null) {
				log.error(" BaseDto Returned Null Values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getResponseContents() != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				nominalDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<NominalDetails>>() {
				});
			} else {
				log.error(" Nominal List By Desgnation no record found");
				errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Nominal Details By Desgnation :: " + nominalDetailsList);
			} else {
				log.info("Error in Nominal Details By Desgnation with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured while Getting HallTicket Details ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return nominalDetailsList;
	}

	/**
	 * @author Bhaskar
	 * 
	 */
	public String approveNominalList() {
		BaseDTO baseDTO = new BaseDTO();
		if (approveNominalList == null || approveNominalList.size() == 0) {
			log.info(" Nominal Lsit To approbval Is empty");
			errorMap.notify(ErrorDescription.NOMINAL_LIST_EMPTY.getErrorCode());
			return null;
		}
		selectedNominal = new NominalDetails();
		selectedNominal.setNominallist(approveNominalList);
		try {
			log.info("<< ---- Nominal List Details for Approval ----->> " + selectedNominal);
			url = SERVER_URL + "/nominaldetails/approvenominallist";
			log.info(" Approve Nominal List Url :: " + url);
			baseDTO = httpService.put(url, selectedNominal);
			if (baseDTO == null) {
				log.error(" Base Dto Returned Null Values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			approveNominalList = mapper.readValue(jsonResponse, new TypeReference<List<NominalDetails>>() {
			});
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Nominal Details Approved Successfully ");
				errorMap.notify(ErrorDescription.NOMINAL_APPROVED_SUCCESSFULLY.getErrorCode());
				loadNominalList();
			} else {
				log.error(" Job Application Faild to Approve with Status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception Occured while approving nominal list", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}

		return "/pages/personnelHR/listNominalList.xhtml?faces-redirect=true";
	}

	public void confirmStatus() {
		if (approveNominalList == null || approveNominalList.size() == 0) {
			log.info(" Nominal Lsit To approbval Is empty");
			errorMap.notify(ErrorDescription.NOMINAL_LIST_EMPTY.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	/**
	 * @author Praveen
	 * 
	 */
	public void generateNominalDetails() {
		log.info("NominalListBean :: generateNominalDetails start :: ");
	

		try {
			approvalList = new ArrayList<>();
			totalSeat = 0;
			NominalDetails request = new NominalDetails();
			request.setSelectedLocationList(selectedExamCentreLocation);
			request.setNotificationNo(notificationDto.getNotificationNo());
            if(!selectedExamVenue.isEmpty())
            {
            	List<EntityMaster> entityMasterList=new ArrayList<EntityMaster>();
            	Iterator<ExamCentre> selectedExamVenueItr=selectedExamVenue.iterator();
            	while(selectedExamVenueItr.hasNext())
            	{
            		ExamCentre examCentre=selectedExamVenueItr.next();
            		entityMasterList.add(examCentre.getPreferedRegion());
            	}
            	request.setSelectedLocationList(entityMasterList);
            }
			url = SERVER_URL + "/nominaldetails/fetchapprovedjonapplication";
			log.info("getNominalRegionList URL :: " + url);
			log.info("getNominalRegionList Request:: " + request);
			
			BaseDTO response = httpService.post(url, request);
			if (response == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			} else if (response.getStatusCode() != 0) {
				approvalList = new ArrayList<>();
				errorMap.notify(response.getStatusCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			approvalList = mapper.readValue(jsonResponse, new TypeReference<List<JobApplicationApproval>>() {
			});

			if (approvalList != null) {
				log.info(" Nominal Details List  size:: " + approvalList);

			} else {
				log.error("getNominalJobApprovalList failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			selectedExamVenue.forEach(venue -> {
				if (venue.getNoOfSeatsAvailable() != null)
					totalSeat = totalSeat + venue.getNoOfSeatsAvailable();
			});

			if (totalSeat > 0 && totalSeat >= approvalList.size() && selectedExamVenue.size() > 0) {
				allocatedNominalList = allocateVenue(approvalList, selectedExamVenue);
			} else {
				log.info("  list exceeded  ");
				errorMap.notify(ErrorDescription.EXAM_CENTER_INSUFFICIENT_SEATS.getErrorCode());
				allocatedNominalList = new ArrayList<>();
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured in generateNominalDetails :: ", e);
		}

	}

	private List<NominalDetails> allocateVenue(List<JobApplicationApproval> approvalList,
			List<ExamCentre> examVenueList) {
       log.info("======START  NominalDetailsBean.allocateVenue=======");
       List<NominalDetails> nList = new ArrayList<NominalDetails>();
		try {
			Iterator<ExamCentre> examCentreItr = selectedExamVenue.iterator();
			while (examCentreItr.hasNext()) {
				ExamCentre centre = examCentreItr.next();
				SimpleDateFormat localDateFormat = new SimpleDateFormat("hh:mm a");
				
				Set<ExamCentre> centreSet = new TreeSet<>();
				int index = 0;
				int avSeat = centre.getNoOfSeatsAvailable();
				Iterator<JobApplicationApproval> jobApprovalItr=approvalList.iterator();
				while(jobApprovalItr.hasNext())
				{
					NominalDetails nd = new NominalDetails();
					JobApplicationApproval data=jobApprovalItr.next();
					log.info("====== data.getJobApplication()======="+data.getApplication().toString());
					log.info("====== data.getJobApplication().getPreferredExamCenter()======="+data.getApplication().getPreferredExamCenter().toString());
					log.info("====== data.getJobApplication().getPreferredExamCenter().getName()======="+data.getApplication().getPreferredExamCenter().getName().toString());
					if (data.getApplication() != null && data.getApplication().getPreferredExamCenter() != null
							&& data.getApplication().getPreferredExamCenter().getName()
									.equals(centre.getPreferedRegion().getName())) {
						if (avSeat > 0) {
							log.info("data.getJobApplication()  :: " + data.getPrimaryKey().getJobAppId());
							log.info("data.getJobApplication()  :: " + data.getApplication());
							log.info("application number==> "+data.getApplication().getApplicationNo());
							nd.setJobApplication(data.getApplication());
							nd.setApplicationNo(data.getApplication().getApplicationNo());
							nd.setRegisterNumber(data.getRegisterNumber());
							nd.setCandidateName(
									data.getApplication().getFirstName() + " " + data.getApplication().getLastName());
							nd.setCandidateDateOfBirth(data.getApplication().getDateOfBirth());
							nd.setAppliedPost(data.getApplication().getAppliedPost());
							if (data.getApplication().getAdditionalInfo() != null) {
								nd.setCasteMaster(data.getApplication().getAdditionalInfo().getCaste());
							}
							nd.setContactNumber(data.getApplication().getMobileNumber());
							nd.setFatherName(data.getApplication().getFatherOrSpouseName());
							nd.setPhotograph(data.getApplication().getPhotographPath());
							nd.setSignature(data.getApplication().getSignatureDocPath());
							nd.setRecuritmentYear(data.getApplication().getJobAdvertisement().getRosterReservation()
									.getStaffEligibility().getRecruitmentYear());
							nd.setExamCentre(centre);
							String startTime = localDateFormat.format(centre.getExamStartTime());
							String endTime = localDateFormat.format(centre.getExamEndTime());
							log.info("TIme----------------->" + startTime + " - " + endTime);
							nd.setTimeSlot(startTime + " - " + endTime);
							nd.setExaminationDate(centre.getExamDate());
							centre.setNoOfSeatsAvailable(--avSeat);
						} else {
							centre = examCentreList.get(index);
							nd.setJobApplication(data.getApplication());
							nd.setApplicationNo(data.getApplication().getApplicationNo());
							nd.setRegisterNumber(data.getRegisterNumber());
							nd.setCandidateName(
									data.getApplication().getFirstName() + " " + data.getApplication().getLastName());
							nd.setCandidateDateOfBirth(data.getApplication().getDateOfBirth());
							nd.setAppliedPost(data.getApplication().getAppliedPost());
							nd.setCasteMaster(data.getApplication().getAdditionalInfo().getCaste());
							nd.setContactNumber(data.getApplication().getMobileNumber());
							nd.setFatherName(data.getApplication().getFatherOrSpouseName());
							nd.setPhotograph(data.getApplication().getPhotographPath());
							nd.setSignature(data.getApplication().getSignatureDocPath());
							nd.setRecuritmentYear(data.getApplication().getJobAdvertisement().getRosterReservation()
									.getStaffEligibility().getRecruitmentYear());
							nd.setExamCentre(centre);
							String startTime = localDateFormat.format(centre.getExamStartTime());
							String endTime = localDateFormat.format(centre.getExamEndTime());
							log.info("TIme----------------->" + startTime + " - " + endTime);
							nd.setTimeSlot(startTime + " - " + endTime);
							nd.setExaminationDate(centre.getExamDate());
							centre.setNoOfSeatsAvailable(--avSeat);

						}
						nList.add(nd);
						centreSet.add(centre);
					}
					
				}
				log.info(" Nominal Details List " + nList);
				log.info(" Nominal Details tree Set " + centreSet);
				log.info("======eND  NominalDetailsBean.allocateVenue=======");
				
			}
			return nList;
		} catch (Exception ex) {
			log.info("Error in allocateVenue", ex);
		}
		log.info("======END  NominalDetailsBean.allocateVenue=======");
		return nList;
	}

	/**
	 * @author Praveen
	 * 
	 */

	public String saveNominalDetails() {
		log.info("<---- : Save Details nominalDetailsList :------>" + allocatedNominalList);
		try {
			if (allocatedNominalList.size() == 0 || allocatedNominalList.isEmpty()) {
				log.info("Nominal list is empty");
				errorMap.notify(ErrorDescription.NOMINAL_LIST_EMPTY.getErrorCode());
				return null;
			}

			allocatedNominalList.forEach(nominalData -> {
				nominalData.setCreatedBy(loginBean.getUserDetailSession());
				nominalData.setCreatedDate(new Date());
				nominalData.setJobApplication(nominalData.getJobApplication());
			});

			url = SERVER_URL + "/nominaldetails/create";
			BaseDTO response = httpService.post(url, allocatedNominalList);
			if (response == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (response.getStatusCode() == 0) {
				log.info("<----Nominal List successfully saved---->");
				Util.addInfo("Nominal List created successfully");
				loadNominalList();
				return "/pages/personnelHR/listNominalList.xhtml?faces-redirect=true";
			} else {
				log.info("<----Nominal List save Failed---->");
				//Util.addError("Nominal List save Failed");
				errorMap.notify(response.getStatusCode());
				//errorMap.notify(ErrorDescription.NOMINAL_IS_ALREADY_EXIST.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}

		return null;
	}

	/**
	 * @author Praveen
	 * 
	 */
	public String loadCreateNominalList() {
		log.info(" Load Create Nominal List");
		designation = new Designation();
		regLocation = new EntityMaster();
		allocatedNominalList = new ArrayList<>();
		notificationDto = new NominalDetailsNotificationDto();
		selectedExamCentreLocation = new ArrayList<>();
		selectedExamVenue = new ArrayList<>();
		designation = new Designation();
		return "/pages/personnelHR/createNominalList.xhtml?faces-redirect=true";
	}

	public void loadLazyNominalList() {
		log.info("< --  Start Nominal Lazy Load -- > ");
		nominalLazyList = new LazyDataModel<NominalDetails>() {
			private static final long serialVersionUID = 1L;

			public List<NominalDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<NominalDetails> data = mapper.readValue(jsonResponse,
							new TypeReference<List<NominalDetails>>() {
							});
					if (data == null) {
						log.info(" Nominal Response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Nominal Search Successfully Completed");
					} else {
						log.error(" Nominal Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("loadLazyNominalList exception==>", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(NominalDetails res) {
				return res != null ? res.getNominalId() : null;
			}

			@Override
			public NominalDetails getRowData(String rowKey) {
				List<NominalDetails> responseList = (List<NominalDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (NominalDetails res : responseList) {
					if (res.getNominalId().longValue() == value.longValue()) {
						selectedNominal = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	/**
	 * purpose will call server when search filtered
	 * 
	 * @param first
	 *            - page number
	 * @param pageSize
	 *            - how many records need to be fetched
	 * @param sortField
	 *            - which filed to be sorted
	 * @param sortOrder
	 *            - sort order that to be followed (ascending or descending)
	 * @param filters
	 *            - search filters
	 * @return BaseDTO with list of Nominal based filters and pages size
	 */
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			NominalDetails request = new NominalDetails();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);
				if (entry.getKey().contains("appliedPost") && entry.getKey().equals("appliedPost.name")) {
					log.info("recruitment for post : " + value);
					Designation des = new Designation();
					des.setName(value);
					request.setAppliedPost(des);
				}

				if (entry.getKey().contains("recuritmentYear")) {
					log.info("Requitment Year : " + value);
					request.setRecuritmentYear(Integer.parseInt(value));
				}

				if (entry.getKey().contains("createdDate")) {
					log.info("Created Date : " + AppUtil.serverDateFormat(value));
					request.setCreatedDate(AppUtil.serverDateFormat(value));
				}

				if (entry.getKey().equals("status")) {
					log.info("status : " + value);
					if (value.equalsIgnoreCase("INPROGRESS")) {
						request.setStatus(NominalDetailsStatus.INPROGRESS);
					} else {
						request.setStatus(NominalDetailsStatus.APPROVED);
					}
				}

				if (entry.getKey().contains("jobApplication.jobAdvertisement.notificationNumber")) {
					log.info("Notification No : " + value);
					JobAdvertisement adv = new JobAdvertisement();
					JobApplication ja = new JobApplication();
					adv.setNotificationNumber(value);
					ja.setJobAdvertisement(adv);
					request.setJobApplication(ja);
				}

			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/nominaldetails/getnominallistlazy", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Nominal List search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}
	
	public void downloadExcel() {
		log.info("DownloadeExcel Method Start=======>");
		InputStream input = null;
		try {
			String[] columns = {"Application Number", "Registration No","Candidate Name","Caste","Date Of Birth","Exam date","Exam Time","Exam Center Name"};
			Workbook workbook = new XSSFWorkbook();
			CreationHelper createHelper = workbook.getCreationHelper();
			Sheet sheet = workbook.createSheet("AllocatedNominal");
			
			Font headerFont = workbook.createFont();
	        headerFont.setBold(true);
	        headerFont.setFontHeightInPoints((short) 14);
	        
	        CellStyle headerCellStyle = workbook.createCellStyle();
			headerCellStyle.setFont(headerFont);
			
			 CellStyle dateCellStyle = workbook.createCellStyle();
		     dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MMM-yyyy"));
		     
		     Row row1 = sheet.createRow(0);
		     row1.createCell(0).setCellValue("Applied For Post:");
		     if(designation.getName()!=null) {
		     row1.createCell(1).setCellValue(designation.getName());
		     }
		     
		     row1.createCell(2).setCellValue("Recruitment year:");
		     if(notificationDto.getReqYear()!=null) {
		     row1.createCell(3).setCellValue(notificationDto.getReqYear());
		     }
		     
		     Row headerRow = sheet.createRow(2);
				
				for(int i = 0; i < columns.length; i++) {
		            Cell cell = headerRow.createCell(i);
		            cell.setCellValue(columns[i]);
		      }
		     
		     int rowNum = 3;
		     for(NominalDetails allNominal: allocatedNominalList) {
		            Row row = sheet.createRow(rowNum++);
                     
		            if(allNominal.getApplicationNo()!=null) {
		            row.createCell(0).setCellValue(allNominal.getApplicationNo());
		            }
                   if(allNominal.getRegisterNumber() !=null) {
		           row.createCell(1).setCellValue(allNominal.getRegisterNumber());
		           }
                   
                   if(allNominal.getCandidateName()!=null) {
    		           row.createCell(2).setCellValue(allNominal.getCandidateName());
    		       }
                   
                   if(allNominal.getCasteMaster()!=null && allNominal.getCasteMaster().getName() !=null) {
    		           row.createCell(3).setCellValue(allNominal.getCasteMaster().getName());
    		       }
                   
                   if(allNominal.getCandidateDateOfBirth() !=null) {
                	   Cell dateOfBirthCell = row.createCell(4);
                	   dateOfBirthCell.setCellValue(allNominal.getCandidateDateOfBirth());
                	   dateOfBirthCell.setCellStyle(dateCellStyle);
    		       }
                   
                   if(allNominal.getExaminationDate() !=null) {
                	   Cell examDateCell = row.createCell(5);
                	   examDateCell.setCellValue(allNominal.getExaminationDate());
                	   examDateCell.setCellStyle(dateCellStyle);
    		       }
                   
                   if(allNominal.getTimeSlot() !=null) {
    		           row.createCell(6).setCellValue(allNominal.getTimeSlot().toString());
    		       }
                   
                   if(allNominal.getExamCentre()!=null && allNominal.getExamCentre().getExamCentreName() !=null) {
    		           row.createCell(7).setCellValue(allNominal.getExamCentre().getExamCentreName());
    		       }

		      
		        }
		     for(int i = 0; i < columns.length; i++) {
		            sheet.autoSizeColumn(i);
		        }
		     String docPathName = commonDataService.getAppKeyValue("NOMINAL_LIST_DOWNLOAD_PATH");
		     FileOutputStream fileOut = new FileOutputStream(docPathName+"/NominalList.xlsx");
		        workbook.write(fileOut);
		        fileOut.close();
		        workbook.close();
		        
		        File files = new File(docPathName+"/NominalList.xlsx");
		        if(files.exists()) {
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
		        }
		}
		catch (Exception ex) {
			log.info("Error in downloadeExcel",ex);
		}
		log.info("DownloadeExcel Method End=======>");
	}
}
