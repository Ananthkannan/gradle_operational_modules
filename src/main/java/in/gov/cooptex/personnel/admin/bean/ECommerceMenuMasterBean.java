package in.gov.cooptex.personnel.admin.bean;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ECommerceMainMenuListDTO;
import in.gov.cooptex.core.dto.ECommerceMainMenuSaveDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.DisciplinaryActionComplaintStatus;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.ecomm.model.EcommMainMenu;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;


@Log4j2
@Service("eCommerceMenuMasterBean")
@Scope("session")
public class ECommerceMenuMasterBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private final String ECOMMERCE_MENU_MASTER_LIST_PAGE = "/pages/ecommerce/admin/listEcommerceMenuMaster.xhtml?faces-redirect=true;";

    private final String ECOMMERCE_MENU_MASTER_CREATE_PAGE = "/pages/ecommerce/admin/createEcommerceMenuMaster.xhtml?faces-redirect=true;";

    private final String ECOMMERCE_MENU_MASTER_VIEW_PAGE = "/pages/ecommerce/admin/viewEcommerceMenuMaster.xhtml?faces-redirect=true;";

    public static final String SERVER_URL = AppUtil.getPortalServerURL();

    @Getter
    @Setter
    String action;

    @Autowired
    ErrorMap errorMap;

    @Getter
    @Setter
    EcommMainMenu eCommMainMenu;


    @Getter
    @Setter
    ECommerceMainMenuListDTO selectedECommMainMenu ;


    @Getter
    @Setter
    Long selectedParentECommMainMenuId ;

    @Getter
    @Setter
    List<EcommMainMenu> ecommMainMenuList = new ArrayList<EcommMainMenu>();;

    @Autowired
    AppPreference appPreference;

    @Autowired
    HttpService httpService;


    String jsonResponse;

    @Getter
    @Setter
    String code,name,activeStatus;

    @Getter
    @Setter
    ECommerceMainMenuSaveDTO eCommerceMainMenuSaveDTO = null;

    @Getter
    @Setter
    LazyDataModel<ECommerceMainMenuListDTO> lazyEcommMainMenuList;

    ObjectMapper mapper;

    @Getter @Setter
    List<ECommerceMainMenuListDTO> ecommMainMenuListLazy = new ArrayList<>();

    @Getter
    @Setter
    int totalRecords = 0;

    @Getter
    @Setter
    Boolean addButtonFlag = true;

    @Getter
    @Setter
    Boolean editButtonFlag = true;


    @Getter
    @Setter
    Boolean viewButtonFlag = true;

    @Getter
    @Setter
    Boolean editable = true;

    @Getter
    @Setter
    Boolean clearButtonFlag = true;

    @PostConstruct
    public String showEcommerceMenuMasterListPage() {
        log.info("<==== Starts CommunityMasterBean.showEcommerceMenuMasterListPage =====>");
        mapper = new ObjectMapper();
        addButtonFlag = true;
        editButtonFlag = false;
        viewButtonFlag = false;
        clearButtonFlag = true;
        eCommMainMenu = new EcommMainMenu();
        selectedECommMainMenu = new ECommerceMainMenuListDTO();
        loadLazyMenuMasterList();
        log.info("<==== Ends CommunityMasterBean.showEcommerceMenuMasterListPage =====>");
        return ECOMMERCE_MENU_MASTER_LIST_PAGE;
    }

    public String eCommerceMenuMasterListAction() {
        log.info("<====== eCommerceMenuMasterBean.eCommerceMenuMasterListAction Starts====> action   :::" + action);
        try {
            if (action.equalsIgnoreCase("ADD")) {

                log.info("<====== Create E-Commerce Menu Master page called.......====>");
                clear();
                activeStatus = "active";
                getAllActiveECommerceMenu();
                return ECOMMERCE_MENU_MASTER_CREATE_PAGE;
            }
            if (action.equalsIgnoreCase("VIEW")) {
                log.info("<====== View E-Commerce Menu Master page called.......====>");

                return ECOMMERCE_MENU_MASTER_VIEW_PAGE;


            }
            if(action.equalsIgnoreCase("EDIT")){
                log.info("<====== Edit functionality called.......====>");
                editable = false;
                getAllActiveECommerceMenu();
                code=selectedECommMainMenu.getCode();
                name=selectedECommMainMenu.getName();
                selectedParentECommMainMenuId=selectedECommMainMenu.getParentId();
//                selectedParentECommMainMenuId=selectedECommMainMenu.getParentMenuCodeName();
                activeStatus=selectedECommMainMenu.getActiveStatus();

                return ECOMMERCE_MENU_MASTER_CREATE_PAGE;
            }
            if(action.equalsIgnoreCase("CLEAR")){
                log.info("<====== Clear functionality called.......====>");
                showEcommerceMenuMasterListPage();
            }

        } catch (Exception e) {
            log.error("Exception Occured in eCommerceMenuMasterBean.eCommerceMenuMasterListAction:", e);
            errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
        }
        log.info("<====== eCommerceMenuMasterBean.eCommerceMenuMasterListAction Ends====>");
        return null;
    }


    public String saveOrUpdate(){
        log.info("<==== Starts eCommerceMenuMasterBean.saveOrUpdate =======>");
        try {
            eCommerceMainMenuSaveDTO = new ECommerceMainMenuSaveDTO();
            if(code == null ) {
                errorMap.notify(ErrorDescription.ECOMMERCE_MENU_CODE_EMPTY.getCode());
                return null;
            }
            if(name == null ) {
                errorMap.notify(ErrorDescription.ECOMMERCE_MENU_NAME_EMPTY.getCode());
                return null;
            }

            if(activeStatus == null) {
                errorMap.notify(ErrorDescription.ECOMMERCE_MENU_STATUS_EMPTY.getCode());
                return null;
            }
            if(selectedECommMainMenu != null){
                if(selectedECommMainMenu.getMenuId() != null){
                    eCommerceMainMenuSaveDTO.setMenuId(selectedECommMainMenu.getMenuId());
                }
            }

            eCommerceMainMenuSaveDTO.setCode(code);
            eCommerceMainMenuSaveDTO.setName(name);
            eCommerceMainMenuSaveDTO.setActiveStatus(activeStatus);
            eCommerceMainMenuSaveDTO.setParentMenuId(selectedParentECommMainMenuId);
            String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
                    + "/ecommercemenu/saveorupdate";
            BaseDTO response = httpService.post(url,eCommerceMainMenuSaveDTO);
            if(response != null && response.getStatusCode() == 0) {
                log.info("Ecommerce menu saved successfully..........");
                if(action.equalsIgnoreCase("ADD"))
                    errorMap.notify(ErrorDescription.ECOMMERCE_MENU_SAVE_SUCCESS.getCode());
                else
                    errorMap.notify(ErrorDescription.ECOMMERCE_MENU_UPDATE_SUCCESS.getCode());
            }else if(response != null && response.getStatusCode() != 0){
                if(response.getStatusCode() == 826442)
                    errorMap.notify(ErrorDescription.ECOMMERCE_MENU_CODE_EXIST.getCode());
                else if(response.getStatusCode() == 172535)
                    errorMap.notify(ErrorDescription.ECOMMERCE_MENU_NAME_ALREADY_EXIST.getCode());
                return null;
            }else {
                errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
            }
        }catch(Exception e) {
            log.error("Exception occured while save or update in eCommerceMenuMasterBean.saveOrUpdate.......",e);
            errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
            return null;
        }
        log.info("<==== Ends eCommerceMenuMasterBean.saveOrUpdate =======>");
            return ECOMMERCE_MENU_MASTER_LIST_PAGE;
    }


    public void getAllActiveECommerceMenu() {
        log.info("Inside eCommerceMenuMasterBean.getAllActiveECommerceMenu()");
        ecommMainMenuList = new ArrayList<EcommMainMenu>();
        String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
                + "/ecommercemenu/getall";
        try {

            BaseDTO response = httpService.get(url);
            if (response != null && response.getStatusCode() == 0) {
                ObjectMapper mapper = new ObjectMapper();
                jsonResponse = mapper.writeValueAsString(response.getResponseContents());
                ecommMainMenuList = mapper.readValue(jsonResponse,
                        new TypeReference<List<EcommMainMenu>>() {
                        });
                if (ecommMainMenuList != null)
                    log.info("eCommMainMenuList size ::::::::::::::::" + ecommMainMenuList.size());

            } else if (response != null && response.getStatusCode() != 0) {
                errorMap.notify(response.getStatusCode());
            }
        } catch (Exception e) {
            log.error("Exception occured in eCommerceMenuMasterBean.getAllActiveECommerceMenu ...", e);
            errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
        }
        log.info("<===== Ends eCommerceMenuMasterBean.getAllActiveECommerceMenu ======>");

    }


    public void testCode(){
        log.info("inside testcode " , code);
    }


    public void  loadLazyMenuMasterList() {
        log.info("<===== Starts eCommerceMenuMasterBean.loadLazyMenuMasterList ======>");

        lazyEcommMainMenuList = new LazyDataModel<ECommerceMainMenuListDTO>() {
            private static final long serialVersionUID = 8422543223567350599L;

            @Override
            public List<ECommerceMainMenuListDTO> load(int first , int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters){
                try {
                    PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
                    log.info("Pagination request :::"+paginationRequest);
                    String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/ecommercemenu/getallecommenulistlazy";
                    BaseDTO response = httpService.post(url, paginationRequest);
                    if(response != null && response.getStatusCode() == 0) {
                        jsonResponse = mapper.writeValueAsString(response.getResponseContents());
                        ecommMainMenuListLazy = mapper.readValue(jsonResponse, new TypeReference<List<ECommerceMainMenuListDTO>>() {});
                        this.setRowCount(response.getTotalRecords());
                        totalRecords = response.getTotalRecords();
                    }else {
                        errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
                    }
                }catch(Exception e) {
                    log.error("Exception occured in lazyload ...",e);
                    errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
                }
                log.info("Ends lazy load....");
                    return ecommMainMenuListLazy;
            }

            @Override
            public Object getRowKey(ECommerceMainMenuListDTO res) {
                return res != null ? res.getMenuId() : null;
            }

            @Override
            public ECommerceMainMenuListDTO getRowData(String rowKey) {
                for (ECommerceMainMenuListDTO eCommerceMainMenuListDTO : ecommMainMenuListLazy) {
                    if (eCommerceMainMenuListDTO.getMenuId().equals(Long.valueOf(rowKey))) {
                        selectedECommMainMenu = eCommerceMainMenuListDTO;
                        return eCommerceMainMenuListDTO;
                    }
                }
                return null;
            }
        };
        log.info("<===== Ends eCommerceMenuMasterBean.loadLazyMenuMasterList ======>");
    }


    public void clear(){
        code = null;
        name = null;
        selectedParentECommMainMenuId = null;
        eCommerceMainMenuSaveDTO = null;
    }

    public void onRowSelect(SelectEvent event) {
        log.info("eCommerceMenuMasterBean onRowSelect method started");
        editButtonFlag = true;
        viewButtonFlag = true;
        addButtonFlag = false;
    }


}
