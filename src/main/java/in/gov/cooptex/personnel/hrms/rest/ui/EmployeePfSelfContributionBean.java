package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.policynote.model.PolicyNoteIntimation;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.GeneralErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.operation.dto.GovtSchemePlanResponseDTO;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.production.model.GovtSchemePlanNote;
import in.gov.cooptex.personnel.hrms.dto.EmpSelfContributionDTO;
import in.gov.cooptex.personnel.hrms.model.EmpPfSelfContributionNote;
import in.gov.cooptex.personnel.payroll.model.EmpPfSelfContribution;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("employeePfSelfContributionBean")
@Scope("session")
@Log4j2
public class EmployeePfSelfContributionBean implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = -3279760861029833330L;
	
	private static final String EMPLOYEE_REQUEST_CREATE_PAGE = "/pages/personnelHR/employeeProfile/createVoluntaryProvidentFundRequest.xhtml?faces-redirect=true;";
	
	private static final String EMPLOYEE_REQUEST_LIST_PAGE = "/pages/personnelHR/employeeProfile/listVoluntaryProvidentFundRequest.xhtml?faces-redirect=true;";
	
	private static final String EMPLOYEE_REQUEST_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewVoluntaryProvidentFundRequest.xhtml?faces-redirect=true;";
	
	private static final String EMPLOYEE_REQUEST_PROCESS_LIST_PAGE = "/pages/personnelHR/employeeProfile/listVoluntaryProvidentFundProcess.xhtml?faces-redirect=true;";
	
	private static final String EMPLOYEE_REQUEST_PROCESS_VIEW_PAGE = "/pages/personnelHR/employeeProfile/voluntaryProvidentFundProcess.xhtml?faces-redirect=true;";
	
	private static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Autowired
	LoginBean loginBean;
	
	@Autowired
	HttpService httpService;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Getter @Setter
	String action,status;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter @Setter
	EmpSelfContributionDTO empSelfContributionDTO = new EmpSelfContributionDTO();
	
	@Getter @Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	LazyDataModel<EmpPfSelfContribution> lazyEmployeeRequestList;

	String jsonResponse;
	
	@Getter @Setter
	Integer totalRecords,totalProcessRecords = 0;
	
	@Getter @Setter
	EmpPfSelfContribution selectedEmpPfSelfContribution;
	
	@Getter @Setter
	EmpSelfContributionDTO selectedEmpSelfContributionDTO;
	
	@Getter @Setter
	boolean enableFlag,addButtonFlag = true,editButtonFlag = true,deleteButtonFlag = true;
	
	@Getter @Setter
	EmpPfSelfContributionNote contributionNote = new EmpPfSelfContributionNote();
	
	@Getter	@Setter
	EmpPfSelfContributionNote contributionLastNote = new EmpPfSelfContributionNote();
	
	@Getter @Setter
	EmpPfSelfContribution empPfSelfContribution = new EmpPfSelfContribution();
	
	@Getter @Setter
	List<EmpSelfContributionDTO> viewNoteEmployeeDetails = new ArrayList<>();
	
	@Getter @Setter
	LazyDataModel<EmpSelfContributionDTO> lazyEmployeeRequestProcessList;
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	public String showEmployeeRequestListPage() {
		log.info("<<====== EmployeePfSelfContributionBean:showEmployeeRequestListPage() ======>> ");
		try {
			empSelfContributionDTO = new EmpSelfContributionDTO();
			selectedEmpPfSelfContribution = new EmpPfSelfContribution();
			loadEmployeeRequestLazyList();
			addButtonFlag = true;
			editButtonFlag = false;
			deleteButtonFlag = false;
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		}catch(Exception e) {
			log.error("Exception at showEmployeeRequestListPage() - ",e);
		}
		return EMPLOYEE_REQUEST_LIST_PAGE;
	}
	
	public void getEmployeeDetailsByUserId() {
		log.info("<<======= EmployeePfSelfContributionBean:getEmployeeDetailsByUserId() =======>> ");
		try {
			Long userId = loginBean.getUserDetailSession().getId();
			if(userId != null) {
				String url = SERVER_URL+ "/employee/getemployeedetailsbyloginuser/"+userId;
				log.info("url" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					EmployeeMaster employeeMaster = mapper.readValue(jsonValue, EmployeeMaster.class);
					if(employeeMaster == null) {
						log.info("Employee Master Not Found");
					}
					empSelfContributionDTO.setEmpId(employeeMaster.getId());
					empSelfContributionDTO.setEmployeeName(employeeMaster.getFirstName()+" "+employeeMaster.getLastName());
				}
			}
		}catch(Exception e) {
			log.error("Exception at getEmployeeDetailsByUserId() - ",e);
		}
	}
	
	public String employeeVoluntaryRequestListAction() {
		log.info("<====== EmployeePfSelfContributionBean:employeeVoluntaryRequestListAction Starts =====>");
		try {
			if(action.equalsIgnoreCase("CREATE")) {
				log.info("<====== Voluntary Contribution Request add page called =====>");
				empSelfContributionDTO = new EmpSelfContributionDTO();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_VOLUNTRY_CONTRIBUTION_REQUEST.toString());
				getEmployeeDetailsByUserId(); 
				return EMPLOYEE_REQUEST_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception Occured in EmployeePfSelfContributionBean:employeeVoluntaryRequestListAction", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EmployeePfSelfContributionBean:employeeVoluntaryRequestListAction Ends====>");
		return null;
	}

	public String saveEmployeeContributionRequest() {
		log.info("<<======= EmployeePfSelfContributionBean:saveEmployeeContributionRequest ======>> ");
		BaseDTO baseDTO = null;
		try {
			if (StringUtils.isEmpty(empSelfContributionDTO.getNote())) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_CREATENOTE.getErrorCode());
				return null;
			}
			if (empSelfContributionDTO.getEmpPfSelfContribution().getContributedAmount() != null) {
				
				log.info("Login User Employee Id - " + empSelfContributionDTO.getEmpId());
				if (empSelfContributionDTO.getEmpId() != null) {
					String url = SERVER_URL + "/paydetails/getpreviousmonthpayrolldetails";
					baseDTO = httpService.post(url, empSelfContributionDTO);
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						log.info("Voluntary Contribution Amount does not Exceed");
						String URL = SERVER_URL + "/empvoluntarycontribution/createRequest";
						baseDTO = httpService.post(URL, empSelfContributionDTO);
						if (baseDTO.getStatusCode() == 0) {
							errorMap.notify(ErrorDescription
									.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_SAVE_SUCCESS).getErrorCode());
							return showEmployeeRequestListPage();
						} else {
							errorMap.notify(baseDTO.getStatusCode());
							return null;
						}
					} else if (baseDTO.getStatusCode().equals(ErrorDescription
							.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_AMOUNT_EXCEED).getErrorCode())) {
						log.info("Voluntary Contribution Amount is Exceeded");
						errorMap.notify(ErrorDescription
								.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_AMOUNT_EXCEED).getErrorCode());
					} else if (baseDTO.getStatusCode().equals(ErrorDescription
							.getError(PersonnalErrorCode.PREVIOUS_MONTH_PAYROLL_NOT_FOUND).getErrorCode())) {
						errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.PREVIOUS_MONTH_PAYROLL_NOT_FOUND)
								.getErrorCode());
					}

					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception at EmployeePfSelfContributionBean:saveEmployeeContributionRequest()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	private void loadEmployeeRequestLazyList() {
		lazyEmployeeRequestList = new LazyDataModel<EmpPfSelfContribution>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 9092821910693795627L;

			public List<EmpPfSelfContribution> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<EmpPfSelfContribution> empPfSelfContributionList = null;
				try {
					empPfSelfContributionList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					getEmployeeDetailsByUserId();
					paginationRequest.setEmpId(empSelfContributionDTO.getEmpId());
					url = AppUtil.getPortalServerURL() + "/empvoluntarycontribution/loadLazyEmployeeRequest";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						empPfSelfContributionList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpPfSelfContribution>>() {
								});
						log.info("list----------" + empPfSelfContributionList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadEmployeeRequestLazyList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return empPfSelfContributionList;
			}

			@Override
			public Object getRowKey(EmpPfSelfContribution res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpPfSelfContribution getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmpPfSelfContribution> responseList = (List<EmpPfSelfContribution>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmpPfSelfContribution empPfSelfContributionObj : responseList) {
					if (empPfSelfContributionObj.getId().longValue() == value.longValue()) {
						selectedEmpPfSelfContribution = empPfSelfContributionObj;
						return empPfSelfContributionObj;
					}
				}
				return null;
			}
		};
	}
	
	public String deleteEmployeeContribution() {
		log.info("<<========= EmployeePfSelfContributionBean:deleteEmployeeContribution() =======>> ");
		try {
			if (selectedEmpPfSelfContribution == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedEmpPfSelfContribution != null) {
				if (!selectedEmpPfSelfContribution.getStage().equals(ApprovalStatus.REJECTED.toString())) {
					if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStage.SUBMITTED)) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_SUBMITTED_RECORD).getErrorCode());
					}else if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStage.APPROVED)) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_APPROVED_RECORD).getErrorCode());
					}else if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStatus.FINAL_APPROVED.toString())) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_FINAL_APPROVED_RECORD).getErrorCode());
					}
					log.info("<========== Inside Cannot Delete Record =========>");
					return null;
				}
			}
			RequestContext.getCurrentInstance().execute("PF('confirmContributionDelete').show();");
		}catch(Exception e) {
			log.error("Exception at EmployeePfSelfContributionBean:deleteEmployeeContribution()",e);
		}
		return null;
	}
	
	public String deleteConfirmEmployeeContribution() {
		log.info("<<========= EmployeePfSelfContributionBean:deleteConfirmEmployeeContribution() =======>> ");
		String url;
		try {
			log.info("Request id ----------->" + selectedEmpPfSelfContribution.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = SERVER_URL+"/empvoluntarycontribution/deleteById/"+selectedEmpPfSelfContribution.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_DELETE_SUCCESS).getErrorCode());
				RequestContext.getCurrentInstance().execute("PF('confirmContributionDelete').hide();");
				addButtonFlag = true;
				return showEmployeeRequestListPage();
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at EmployeePfSelfContributionBean:deleteConfirmEmployeeContribution()", e);
		}
		return null;
	}

	public String gotoVoluntaryProcessViewPage() {
		log.info("<<========= EmployeePfSelfContributionBean:gotoVoluntaryProcessViewPage() =======>> ");
		BaseDTO baseDTO = new BaseDTO();
		enableFlag = false;
		try {
			if (selectedEmpSelfContributionDTO == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String URL = SERVER_URL+"/empvoluntarycontribution/getById/"+selectedEmpSelfContributionDTO.getContributionId();
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				empSelfContributionDTO = objectMapper.readValue(jsonResponse, EmpSelfContributionDTO.class);
				empPfSelfContribution = empSelfContributionDTO.getEmpPfSelfContribution();
				contributionNote = empSelfContributionDTO.getEmpPfSelfContributionNote();

				contributionLastNote = empSelfContributionDTO.getEmpPfSelfContributionNote();

				if(contributionLastNote != null) {
					empSelfContributionDTO.setNote(contributionLastNote.getNote());
				}
				
				String jsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = objectMapper.readValue(jsonResponses,
						new TypeReference<List<EmpSelfContributionDTO>>() {
						});

				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_VOLUNTRY_CONTRIBUTION_REQUEST.toString());
				if(contributionLastNote.getUserMaster() != null && loginBean.getUserMaster() != null) {
					if (contributionLastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
						enableFlag = true;
					}
				}
				
			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at EmployeePfSelfContributionBean:gotoVoluntaryProcessViewPage()", e);
		}
		return EMPLOYEE_REQUEST_PROCESS_VIEW_PAGE;
	}
	
	public String gotoViewPage() {
		log.info("<<========= EmployeePfSelfContributionBean:gotoViewPage() =======>> ");

		enableFlag = false;
		contributionNote = new EmpPfSelfContributionNote();
		BaseDTO baseDTO = null;
		try {
			if (selectedEmpPfSelfContribution == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (action.equals("EDIT")) {
				if (selectedEmpPfSelfContribution != null) {
					if (!selectedEmpPfSelfContribution.getStage().equals(ApprovalStatus.REJECTED.toString())) {
						if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStage.SUBMITTED)) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_SUBMITTED_RECORD).getErrorCode());
						}else if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStage.APPROVED)) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_APPROVED_RECORD).getErrorCode());
						}else if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStatus.FINAL_APPROVED.toString())) {
							errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_FINAL_APPROVED_RECORD).getErrorCode());
						}
						log.info("<========== Inside Cannot Edit Record =========>");
						return null;
					}
				}
			}
			String URL = SERVER_URL+"/empvoluntarycontribution/getById/"+selectedEmpPfSelfContribution.getId();
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				empSelfContributionDTO = objectMapper.readValue(jsonResponse, EmpSelfContributionDTO.class);
				empPfSelfContribution = empSelfContributionDTO.getEmpPfSelfContribution();
				contributionNote = empSelfContributionDTO.getEmpPfSelfContributionNote();

				contributionLastNote = empSelfContributionDTO.getEmpPfSelfContributionNote();

				if(contributionLastNote != null) {
					empSelfContributionDTO.setNote(contributionLastNote.getNote());
				}
				
				String jsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = objectMapper.readValue(jsonResponses,
						new TypeReference<List<EmpSelfContributionDTO>>() {
						});

				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_VOLUNTRY_CONTRIBUTION_REQUEST.toString());
				if(contributionLastNote.getUserMaster() != null && loginBean.getUserMaster() != null) {
					if (contributionLastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
						enableFlag = true;
					}
				}
				if (action.equals("VIEW")) {
					return EMPLOYEE_REQUEST_VIEW_PAGE;

				} else if (action.equals("EDIT")) {
					getEmployeeDetailsByUserId();
					return EMPLOYEE_REQUEST_CREATE_PAGE;
				}
			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return null;
	}
	
	public void statusUpdate(String value) {
		log.info("<--- EmployeePfSelfContributionBean:statusUpdate() ---> " + value);
		status = value;
	}
	
	public String approveRejectEmpRequestContribution() {
		log.info("<<======= EmployeePfSelfContributionBean:approveRejectEmpRequestContribution() ==========>>");
		BaseDTO baseDTO = null;
		try {
			if(status.equals(ApprovalStage.APPROVED)) {
				if(empSelfContributionDTO.getUserMaster() == null) {
					errorMap.notify(ErrorDescription.FORWARD_TO_USER_EMPTY.getErrorCode());
					return null;
				}else if(empSelfContributionDTO.getFinalApproval() == null) {
					errorMap.notify(ErrorDescription.FORWARD_FOR_ISEMPTY.getErrorCode());
					return null;
				}
			}
			if("PROCESS_VIEW".equals(action)) {
				empSelfContributionDTO.setContributionId(selectedEmpSelfContributionDTO != null ? selectedEmpSelfContributionDTO.getContributionId() : null);
			}else {
				empSelfContributionDTO.setContributionId(selectedEmpPfSelfContribution != null ? selectedEmpPfSelfContribution.getId() : null);
			}
			empSelfContributionDTO.setStatus(status);
			String URL = SERVER_URL + "/empvoluntarycontribution/approverejectrequest";
			baseDTO = httpService.post(URL, empSelfContributionDTO);
			if (baseDTO != null) {
				if(status.equals(ApprovalStage.APPROVED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_APPROVED_SUCCESS).getErrorCode());
				}else if(status.equals(ApprovalStatus.FINAL_APPROVED.toString())) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_FINAL_APPROVED_SUCCESS).getErrorCode());
				}else if(status.equals(ApprovalStage.REJECTED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.VOLUNTARY_CONTRIBUTION_REJECTED_SUCCESS).getErrorCode());
				}
				if("PROCESS_VIEW".equals(action)) {
					return showEmployeeRequestProcessListPage();
				}else {
					return showEmployeeRequestListPage();
				}
			}
		}catch (Exception e) {
			log.error("<<====== EmployeePfSelfContributionBean:approveRejectEmpRequestContribution() =======>>", e);
		}
		return null;
	}
	
	public void onRowSelect(SelectEvent event) {
		addButtonFlag = false;
		editButtonFlag = false;
		deleteButtonFlag = false;
		if(selectedEmpPfSelfContribution.getStage().equals(ApprovalStage.SUBMITTED.toString())) {
			editButtonFlag = true;
			deleteButtonFlag = true;
		}
	}
	
	public String showEmployeeRequestProcessListPage() {
		log.info("<<====== EmployeePfSelfContributionBean:showEmployeeRequestProcessListPage() ======>> ");
		try {
			empSelfContributionDTO = new EmpSelfContributionDTO();
			selectedEmpSelfContributionDTO = new EmpSelfContributionDTO();
			loadEmployeeRequestProcessLazyList();
		}catch(Exception e) {
			log.error("Exception at showEmployeeRequestProcessListPage() - ",e);
		}
		return EMPLOYEE_REQUEST_PROCESS_LIST_PAGE;
	}
	
	private void loadEmployeeRequestProcessLazyList() {
		lazyEmployeeRequestProcessList = new LazyDataModel<EmpSelfContributionDTO>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 7453565032028834578L;

			public List<EmpSelfContributionDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<EmpSelfContributionDTO> empPfSelfContributionList = null;
				try {
					empPfSelfContributionList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					getEmployeeDetailsByUserId();
					paginationRequest.setEmpId(empSelfContributionDTO.getEmpId());
					url = AppUtil.getPortalServerURL() + "/empvoluntarycontribution/loadLazyEmployeeRequestProcessList";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						empPfSelfContributionList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpSelfContributionDTO>>() {
								});
						log.info("list----------" + empPfSelfContributionList.size());
						this.setRowCount(response.getTotalRecords());
						totalProcessRecords = response.getTotalRecords();
						log.info("total records-----------"+totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadEmployeeRequestLazyList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return empPfSelfContributionList;
			}

			@Override
			public Object getRowKey(EmpSelfContributionDTO res) {
				return res != null ? res.getContributionId() : null;
			}

			@Override
			public EmpSelfContributionDTO getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmpSelfContributionDTO> responseList = (List<EmpSelfContributionDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmpSelfContributionDTO empPfSelfContributionObj : responseList) {
					if (empPfSelfContributionObj.getContributionId().longValue() == value.longValue()) {
						selectedEmpSelfContributionDTO = empPfSelfContributionObj;
						return empPfSelfContributionObj;
					}
				}
				return null;
			}
		};
	}
	
	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		List<UserMaster> autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(SERVER_URL,query);
		return autoEmployeeMasterList;
	}
	
	
}
