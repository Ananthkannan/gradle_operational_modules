package in.gov.cooptex.operation.rest.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.ModernizationRequest;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityInfoDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ItemDistribution;
import in.gov.cooptex.core.model.ItemDistributionDetails;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.GovtDtwRequirementsItems;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemeType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("distributionBean")
public class DistributionBean {

	final String ADD_PAGE = "/pages/ISSR/createDistrictTalukWiseDistribution.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/ISSR/viewDistrictTalukWiseDistribution.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/ISSR/listDistrictTalukWiseDistribution.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String url, jsonResponse;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmp;

	@Getter
	@Setter
	List<GovtSchemeType> govtSchemeTypeList;

	@Getter
	@Setter
	List<GovtSchemePlan> govtSchemePlanList;

	@Getter
	@Setter
	List<TalukMaster> talukMasterList;

	@Getter
	@Setter
	List<DistrictMaster> districtMasterList;

	@Getter
	@Setter
	DistrictMaster districtMaster = new DistrictMaster();

	@Getter
	@Setter
	TalukMaster talukMaster = new TalukMaster();

	@Getter
	@Setter
	GovtSchemeType govtSchemeType = new GovtSchemeType();

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlan = new GovtSchemePlan();

	@Getter
	@Setter
	String action, visiableFamButton = "ADDBUTTON";

	@Getter
	@Setter
	List<GovtDtwRequirementsItems> govtDtwItemList;

	@Getter
	@Setter
	GovtDtwRequirementsItems govtDtwRequirementsItems = new GovtDtwRequirementsItems();

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	String bundle, quantity;

	@Getter
	@Setter
	List<Map<String, Object>> proVerDistributionList = new ArrayList<>();

	@Getter
	@Setter
	ProductVarietyMaster productVarMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	LazyDataModel<ItemDistribution> distributionLazyList;

	@Getter
	@Setter
	List<ItemDistribution> itemDistributionList = new ArrayList<>();

	@Getter
	@Setter
	ItemDistribution selectedItemDistribution = new ItemDistribution();

	@Getter
	@Setter
	ItemDistribution itemDistribution = new ItemDistribution();

	@Getter
	@Setter
	Integer totalRecords;
	
	@Getter @Setter
	Long groupId;
	
	@PostConstruct
	public void init() {
		log.info(".......DistributionBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadDistTalukInfo();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public String showDistricTalukDistribution() {
		try {
			if (pageAction.equalsIgnoreCase("LIST")) {
				loadLazyDistributionList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				loadProductByGroup();
				return ADD_PAGE;
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedItemDistribution == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewDistribution();
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: Job Application Delete Response :: ");
				if (selectedItemDistribution == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			}

		} catch (Exception e) {
			log.error("Exception in showDistricTalukDistribution  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String viewDistribution() {
		log.info("<<============ Distribution bean --- viewDistribution ====##STARTS  ");
		try {
			Long disId = selectedItemDistribution.getId();
			log.info("disId==>" + disId);
			String url = SERVER_URL + "/distribution/getbyid/" + disId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null || baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemDistribution = mapper.readValue(jsonResponse, ItemDistribution.class);
				loginEmp = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
				
				talukMaster = itemDistribution.getTalukMaster();
				districtMaster = commonDataService.loadDistricBytaluk(talukMaster.getId());
				govtSchemePlan = itemDistribution.getGovtSchemePlan();
				log.info("planId==>"+govtSchemePlan.getId());
				getSchemeType(govtSchemePlan.getId());
				loadDisDetails();
				loadDisVarityForView(itemDistribution.getItemDistributionList());
			}
			return VIEW_PAGE;
		} catch (Exception e) {
			log.error("Exception in viewDistribution  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	

	public void getSchemeType(Long planId) {
		log.info("<--- Inside loadDistrictByState() --->");
		
		try {
			String requestPath = AppUtil.getPortalServerURL() + "/distribution/getgovtschemetypebyplan/" + planId;
			log.info("URL==>"+requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<GovtSchemeType> govtSchemeTypeList = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemeType>>() {});
					if(govtSchemeTypeList != null) {
						govtSchemeType = govtSchemeTypeList.get(0);
						if(govtSchemeType != null) {
							log.info("govtSchemeType code : " + govtSchemeType.getCode());
						}
					}
					
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("<--- Exception in loadDistrictByState() --->", e);
		}
		
	}

	public String deleteDistribution() {
		try {

			if (selectedItemDistribution == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected Employee  Id : : " + selectedItemDistribution.getId());
			String getUrl = SERVER_URL + "/distribution/delete/id/:id";
			url = getUrl.replace(":id", selectedItemDistribution.getId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Distribution Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.DISTRIBUTION_DELETED_SUCCUESSFULLY.getErrorCode());
			} else {
				log.error("Distribution Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			loadLazyDistributionList();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;
	}

	public void loadLazyDistributionList() {
		log.info("<==== Starts DistributionBean.loadLazyDistributionList =====>");
		distributionLazyList = new LazyDataModel<ItemDistribution>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ItemDistribution> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/distribution/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						itemDistributionList = mapper.readValue(jsonResponse,
								new TypeReference<List<ItemDistribution>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return itemDistributionList;
			}

			@Override
			public Object getRowKey(ItemDistribution res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ItemDistribution getRowData(String rowKey) {
				for (ItemDistribution itemDistribution : itemDistributionList) {
					if (itemDistribution.getId().equals(Long.valueOf(rowKey))) {
						selectedItemDistribution = itemDistribution;
						return itemDistribution;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends DistributionBean.loadLazyDistributionList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedItemDistribution = ((ItemDistribution) event.getObject());

	}

	public void loadDistTalukInfo() {
		try {
			log.info(".......DistributionBean loadDistTalukInfo..............");
			loginEmp = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
			govtSchemeTypeList = commonDataService.govtSchemeTypeList();

		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadProductionPlan() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......DistributionBean loadDistTalukInfo..............");
			if (govtSchemeType == null && govtSchemeType.getCode() == null) {
				AppUtil.addWarn("Please Select Scheme Type Code / Name");
				return;
			}
			url = SERVER_URL + "/govtSchemePlan/getAllGovtSchemePlan";
			log.info("getNotficationNumber==>" + url);
			baseDTO = httpService.post(url, govtSchemeType);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				govtSchemePlanList = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemePlan>>() {
				});
				log.info("govtSchemePlanList size==> " + govtSchemePlanList.size());
			} else {
				govtSchemePlanList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadProPlanDistrict() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......DistributionBean loadProPlanDistrict..............");
			if (govtSchemePlan == null && govtSchemePlan.getId() == null) {
				AppUtil.addWarn("Please Select Production Plan");
				return;
			}
			url = SERVER_URL + "/districtMaster/loadproplandistrict/" + govtSchemePlan.getId();
			log.info("getNotficationNumber==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				districtMasterList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
				});
				log.info("districtMasterList size==> " + districtMasterList.size());
			} else {
				districtMasterList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadProPlanTaluk() {
		try {
			log.info(".......DistributionBean loadProPlanDistrict..............");
			if (districtMaster == null || districtMaster.getId() == null) {
				AppUtil.addWarn("Please Select District");
				return;
			}
			talukMasterList = commonDataService.loadActiveTaluksByDistrict(districtMaster.getId());
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearForm() {
		govtSchemeType = new GovtSchemeType();
		govtSchemePlanList = new ArrayList<>();
		govtSchemePlan = new GovtSchemePlan();
		districtMasterList = new ArrayList<>();
		districtMaster = new DistrictMaster();
		talukMasterList = new ArrayList<>();
		talukMaster = new TalukMaster();
		RequestContext.getCurrentInstance().update("desForm:disPanel");
	}

	public void loadDisDetails() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......DistributionBean loadDisDetails..............");
			if (govtSchemeType == null || govtSchemeType.getCode() == null) {
				errorMap.notify(ErrorDescription.SELECT_TYPE_AND_CODE.getErrorCode());
				return;
			}
			if (govtSchemePlan == null || govtSchemePlan.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_PRODUCTION_PLAN.getErrorCode());
				return;
			}
			if (districtMaster == null || districtMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_DISTRICT.getErrorCode());
				return;
			}
			if (talukMaster == null || talukMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_TALUK.getErrorCode());
				return;
			}

			log.info("loadDisDetails planId==>" + govtSchemePlan.getId());
			log.info("loadDisDetails talukId==>" + talukMaster.getId());

			url = SERVER_URL + "/distribution/loadproductvarietydetails/" + govtSchemePlan.getId() + "/"
					+ talukMaster.getId();
			log.info("getNotficationNumber==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				govtDtwItemList = mapper.readValue(jsonResponse, new TypeReference<List<GovtDtwRequirementsItems>>() {
				});
				log.info("govtDtwItemList size==> " + govtDtwItemList.size());
			} else {
				govtDtwItemList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception in loadDisDetails  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadProductByGroup() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......DistributionBean loadProductByGroup..............");
			if (groupId == null) {
				AppUtil.addWarn("Please Select Group");
				return;
			}
			log.info("loadProductByGroup groupId==>" + groupId);
			url = SERVER_URL + "/distribution/loadproductbygroup/" + groupId;
			log.info("getNotficationNumber==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productList = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyMaster>>() {
				});
			} else {
				productList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("Exception in loadProductByGroup  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	
	
	private void loadDisVarityForView(List<ItemDistributionDetails> itemDistributList) {
		log.info("itemDistributList=>"+itemDistributList.size());
		
		if(itemDistributList != null && !itemDistributList.isEmpty()) {
			for(ItemDistributionDetails details : itemDistributList) {
				Map<String, Object> map = new HashMap<>();
				map.put("planId", govtSchemePlan.getId());
				map.put("talukId", talukMaster.getId());
				map.put("productId", details.getItemId().getId());
				map.put("code", details.getItemId().getCode());
				map.put("gName", details.getItemId().getName());
				map.put("productMaster", details.getItemId());
				map.put("bundle", bundle);
				map.put("quantity", details.getDistributionQuantity());
				proVerDistributionList.add(map);
			}
		}
		
	}

	public void loadDisVarity() {
		try {
			if (govtDtwRequirementsItems == null && govtDtwRequirementsItems.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_GOV_SCHEME_PLAN.getErrorCode());
				return;
			}
			else if (productVarMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_PRODUCT_NAME_CODE.getErrorCode());
				return;
			}
			else if (bundle == null || StringUtils.isEmpty(bundle)) {
				errorMap.notify(ErrorDescription.ENTER_BUNDLE.getErrorCode());
				return;
			}
			else if (quantity == null || StringUtils.isEmpty(quantity)) {
				errorMap.notify(ErrorDescription.ENTER_QTY.getErrorCode());
				return;
			}else {
				log.info("bundle==>" + bundle);
				log.info("quantity==>" + quantity);
				log.info("productVarMaster==>" + productVarMaster);
				log.info("plan id==>" + govtSchemePlan.getId());
				log.info("taluk id==>" + talukMaster.getId());
				Map<String, Object> map = new HashMap<>();
				if (proVerDistributionList != null || !proVerDistributionList.isEmpty()) {
					map.put("planId", govtSchemePlan.getId());
					map.put("talukId", talukMaster.getId());
					map.put("productId", productVarMaster.getId());
					map.put("code", productVarMaster.getCode());
					map.put("gName", productVarMaster.getName());
					map.put("productMaster", productVarMaster);
					map.put("bundle", bundle);
					map.put("quantity", quantity);
					proVerDistributionList.add(map);
				}
				log.info("proVerDistributionList==>" + proVerDistributionList);
			}
		} catch (Exception e) {
			log.error("Exception in loadDisVarity  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String loadAcknowledgement() {
		try {
			log.info("call loadAcknowledgement==>");
			if (govtSchemePlan == null) {
				log.error("Goverment scheme plan is empty ");
				errorMap.notify(ErrorDescription.SELECT_GOV_SCHEME_PLAN.getErrorCode());
				return null;
			}
			if (talukMaster == null) {
				log.error("Taluk master is empty ");
				errorMap.notify(ErrorDescription.TALUK_EMPTY.getErrorCode());
				return null;
			}
			if (proVerDistributionList == null && proVerDistributionList.isEmpty()) {
				log.error("product is empty ");
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_EMPTY.getErrorCode());
				return null;
			}
			loginEmp = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
			log.info("proVerDistributionList==>" + proVerDistributionList);
			if (proVerDistributionList != null && !proVerDistributionList.isEmpty()) {

				List<ItemDistributionDetails> distributionList = new ArrayList<>();

				ItemDistribution itemDistribution = new ItemDistribution();
				itemDistribution.setGovtSchemePlan(govtSchemePlan);
				itemDistribution.setTalukMaster(talukMaster);
				itemDistribution.setEntityMaster(loginEmp.getPersonalInfoEmployment().getWorkLocation());
				for (Map<String, Object> itMap : proVerDistributionList) {
					ItemDistributionDetails itemDistributionDetails = new ItemDistributionDetails();

					itemDistributionDetails.setItemDistribution(itemDistribution);

					ProductVarietyMaster productVarietyMaster = new ProductVarietyMaster();
					productVarietyMaster.setId((Long) itMap.get("productId"));
					itemDistributionDetails.setItemId(productVarietyMaster);

					itemDistributionDetails
							.setDistributionQuantity(Double.parseDouble(itMap.get("quantity").toString()));
					distributionList.add(itemDistributionDetails);
				}
				itemDistribution.setItemDistributionList(distributionList);
				String url = SERVER_URL + "/distribution/create";
				BaseDTO baseDTO = httpService.put(url, itemDistribution);
				log.info("Save Modernization Request : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ItemDistribution disRequest = mapper.readValue(jsonResponse, ItemDistribution.class);
				/*
				 * if (modRequest != null) { modernizationRequest = modRequest; }
				 */
				if (baseDTO.getStatusCode() == 0) {
					
					errorMap.notify(ErrorDescription.DISTRIBUTION_INSERTED_SUCCUESSFULLY.getErrorCode());
					return "/pages/ISSR/stockOurwardISSRAcknowledgement.xhtml?faces-redirect=true";
				} else {
					log.info("Error while saving DistributionRequest with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadAcknowledgement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String callListPage() {
		clearDistribution();
		loadLazyDistributionList();
		return LIST_PAGE;
	}

	public void clearDistribution() {
		govtSchemeType = new GovtSchemeType();
		govtSchemePlanList = new ArrayList<>();
		govtSchemePlan = new GovtSchemePlan();
		districtMasterList = new ArrayList<>();
		districtMaster = new DistrictMaster();
		talukMasterList = new ArrayList<>();
		talukMaster = new TalukMaster();
		selectedItemDistribution = new ItemDistribution();

		govtDtwRequirementsItems = new GovtDtwRequirementsItems();
		productVarMaster = new ProductVarietyMaster();
		bundle = null;
		quantity = null;
		govtDtwItemList = new ArrayList<>();
		proVerDistributionList = new ArrayList<>();
	}

	public String loadDistrictTalukWiseDistribution() {
		clearDistribution();
		return ADD_PAGE;
	}

	public void editDisTalukFromList(Map<String, Object> obj) {
		productVarMaster = (ProductVarietyMaster) obj.get("productMaster");
		bundle = obj.get("bundle").toString();
		quantity = obj.get("quantity").toString();
	}

	public void clearPVDistribution() {

		govtDtwRequirementsItems = new GovtDtwRequirementsItems();
		productVarMaster = new ProductVarietyMaster();
		bundle = null;
		quantity = null;
		RequestContext.getCurrentInstance().update("desForm:disPanel");
	}

	public void removeDisTalukFromList(Map<String, Object> obj) {

		log.info("<=call removeDisTalukFromList=>");
		try {
			log.info(" proVerDistributionList :: " + proVerDistributionList);
			if (proVerDistributionList != null && proVerDistributionList.size() > 0) {
				proVerDistributionList.remove(obj);
				log.info("Distribution successfully removed");
			}

			

		} catch (Exception e) {
			log.error(" Exception in Delete Employee Family Record : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

}
