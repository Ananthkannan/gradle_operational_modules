/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Column and Waterfall charts
 *
 *  Demo JS code for echarts_columns_waterfalls.html page
 *
 * ---------------------------------------------------------------------------- */

// Configure examples
// ------------------------------
document.addEventListener('DOMContentLoaded', function() {

	// Define elements
	var columns_stacked_element = document
			.getElementById('exhibition_columns_stacked');
	var columns_stacked_element_for_Showroom = document
			.getElementById('exhibition_columns_stacked_showroom');
	//
	// Charts configuration
	//

	// Stacked columns chart
	if (columns_stacked_element) {

		// Initialize chart
		var exhibition_columns_stacked = echarts.init(columns_stacked_element);

		//
		// Chart config
		//

		// Options
		var option = {
			color : [ '#3398DB' ],
			tooltip : {
				trigger : 'axis',
				padding : [ 5, 10 ],
				axisPointer : {
					type : 'shadow'
				}
			},

			// Add legend
			legend : {
				type : 'scroll',
				data : [ 'Total Sales Value', 'Total Products Sold' ],
				top : 10,
				itemHeight : 8,
				itemGap : 20
			},

			grid : {
				left : 80,
				right : 10,
				top : 50,
				bottom : 20,
				containLabel : true
			},
			xAxis : [ {
				name : 'All Regions',
				nameTextStyle : {
					lineHeight : 96,
					fontSize : 14,
					color : '#2F6497',
					rich : {
						a : {
						// `lineHeight` is not set, then it will be 56
						}
					}
				},
				nameLocation : 'center',
				scale : true,
				type : 'category',
				boundaryGap : true,
				data : [ 'Coimbatore', 'Cuddalore', 'Chennai', 'Madurai',
						'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore',
						'Bangalore', 'Mumbai', 'Vijayawada' ],
				// data: ['Com', 'Cud', 'Chn', 'Mad', 'Sal', 'Tha', 'Tir',
				// 'Vel', 'Ban', 'Mum', 'Vij'],
				axisTick : {
					alignWithLabel : true
				},
				axisLabel : {
					rotate : 45,
					color : '#333'
				}

			} ],
			yAxis : [ {
				name : 'Total sales in lakhs',
				nameTextStyle : {
					lineHeight : 136,
					color : '#2F6497',
					fontSize : 14,
					rich : {
						a : {
						// `lineHeight` is not set, then it will be 56
						}
					}
				},
				axisLabel : {
					color : '#333'
				},
				nameLocation : 'center',
				type : 'value'
			} ],
			series : [
					{
						name : 'Total Sales Value',
						type : 'bar',
						itemStyle : {
							color : '#00897b'
						},
						data : [ 320, 432, 101, 334, 390, 430, 520, 410, 220,
								320, 425 ]
					},
					{
						name : 'Total Products Sold',
						type : 'bar',
						itemStyle : {
							color : '#d87c7c'
						},
						data : [ 120, 382, 401, 354, 310, 380, 420, 410, 220,
								320, 425 ]
					} ]

		};
		// use configuration item and data specified to show chart
		exhibition_columns_stacked.setOption(option);
		exhibition_columns_stacked.on('click', function(params) {
			$("#exhibitionSalesModalPopup").modal('show');
		});
		$('#exhibitionSalesModalPopup').on(
				'shown.bs.modal',
				function() {
					if (columns_stacked_element_for_Showroom) {

						// Initialize chart
						var exhibition_columns_stacked_showroom = echarts
								.init(columns_stacked_element_for_Showroom);

						//
						// Chart config
						//

						// Options
						var option = {
							color : [ '#3398DB' ],
							tooltip : {
								trigger : 'axis',
								padding : [ 5, 10 ],
								axisPointer : {
									type : 'shadow'
								}
							},

							// Add legend
							legend : {
								type : 'scroll',
								data : [ 'Total Sales Value',
										'Total Products Sold' ],
								top : 0,
								itemHeight : 8,
								itemGap : 20
							},

							grid : {
								left : '4%',
								right : '4%',
								bottom : '5.5%',
								top : '6%',
								containLabel : true
							},
							xAxis : [ {
								interval : 0,
								name : 'All Showrooms',
								nameTextStyle : {
									lineHeight : 260,
									fontSize : 14,
									rich : {
										a : {
										// `lineHeight` is not set, then it will
										// be 56
										}
									}
								},
								nameLocation : 'center',
								scale : true,
								type : 'category',
								boundaryGap : true,
								// data: ['Coimbatore', 'Cuddalore', 'Chennai',
								// 'Madurai', 'Salem', 'Thanjavur',
								// 'Tirunelveli',
								// 'Vellore', 'Bangalore', 'Mumbai',
								// 'Vijayawada'],
								data : [ 'Textiles', 'Cool Brands',
										'SSK Silks And Sarees', 'The Store',
										'Your Choice', 'Best Collections',
										'Showroom 19', 'Adidas',
										'Style and Style', 'Hifi', 'Vij',
										'Com', 'Cud', 'Chn', 'Mad', 'Sal',
										'Tha', 'Tir', 'Vel', 'Ban' ],
								axisTick : {
									alignWithLabel : true
								},
								axisLabel : {
									rotate : 45
								}

							} ],
							yAxis : [ {
								name : 'Total sales in lakhs',
								nameTextStyle : {
									lineHeight : 126,
									fontSize : 14,
									rich : {
										a : {
										// `lineHeight` is not set, then it will
										// be 56
										}
									}
								},
								nameLocation : 'center',
								type : 'value'
							} ],
							series : [
									{
										name : 'Total Sales Value',
										type : 'bar',
										itemStyle : {
											color : '#00897b'
										},
										data : [ 100000, 520000, 2000000,
												3340000, 3900000, 3300000,
												2200000, 520000, 280000, 60000,
												500000, 2500000, 3500000,
												5800000, 9800000, 5400000,
												5400000, 6800000, 7400000,
												6854545 ]
									},
									{
										name : 'Total Products Sold',
										type : 'bar',
										itemStyle : {
											color : '#d87c7c'
										},
										data : [ 100000, 520000, 2000000,
												3340000, 3900000, 3300000,
												2200000, 520000, 280000, 60000,
												500000, 2500000, 3500000,
												5800000, 9800000, 5400000,
												5400000, 6800000, 7400000,
												6854545 ]
									} ]
						};
						// use configuration item and data specified to show
						// chart
						exhibition_columns_stacked_showroom.setOption(option);
					}
				});

	}
});
