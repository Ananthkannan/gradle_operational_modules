package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.SupplierType;
import in.gov.cooptex.operation.intend.model.StockItemInwardPNSDTO;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockItemInwardYarnBean")
@Scope("session")
public class StockInwardYarnBean {
	private static final long serialVersionUID = 4578136862113789205L;
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String INPUT_FORM_LIST_URL = "/pages/printingAndStationary/listStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/printingAndStationary/viewStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_ADD_URL = "/pages/printingAndStationary/createStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_Edit_URL = "/pages/printingAndStationary/editStockItemInwardPNS.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	StockTransfer selectedStockTransferPNS;

	@Getter
	@Setter
	StockTransferItems selectedStockTransferItems;

	@Getter
	@Setter
	ArrayList<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplierTypeMaster;

	@Getter
	@Setter
	SupplierMaster selectedSupplierMaster;

	@Getter
	@Setter
	List<PurchaseOrderItems> collectorSupplierItemList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsInwardUpdateList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsInwardDeleteList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyList;

	@Getter
	@Setter
	PurchaseOrderItems purchaseOrderItems;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsList;

	@Getter
	@Setter
	long selectedProductVariety;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private boolean renderedInward;

	@Getter
	@Setter
	List<PurchaseOrder> purchaseOrderList;

	@Getter
	@Setter
	PurchaseOrder selectedPurchaseOrder;

	@Getter
	@Setter
	SupplierMaster selectedSupplierMasterTest;

	@Getter
	@Setter
	LazyDataModel<StockItemInwardPNSDTO> stockTransferLazyList;

	@Getter
	@Setter
	StockItemInwardPNSDTO stockTransferLazy;

	@Getter
	@Setter
	StockItemInwardPNSDTO selectedStockItemInwardPNSDTO;

	@Getter
	@Setter
	String supplierType;

	@Getter
	@Setter
	String supplierCodeName;

	@Getter
	@Setter
	String gstNumber;

	@Getter
	@Setter
	private String inwardType;

	@Getter
	@Setter
	String purchaseOrderNumber;

	@Getter
	@Setter
	String hsnCode;

	@Getter
	@Setter
	String purchaseDate;

	@Getter
	@Setter
	String purhaseOrderValidityDate;

	@Getter
	@Setter
	Long uom;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int size;

	/*@Getter
	@Setter
	List<SupplierType> supplierTypeEnumList;*/

	@Getter
	@Setter
	List<StockTransferType> stockTransferTypeEnumList;

	@Getter
	@Setter
	List<StockTransferStatus> stockTransferEnumStatus;

	@Getter
	@Setter
	String description;

	@Getter
	@Setter
	StockInwardYarnBean stockInwardYarnBean;

	@Getter
	@Setter
	StockInwardYarnBean selectedStockInwardYarnBean;

	@Getter
	@Setter
	String interestApplicablePayment;

	@Getter
	@Setter
	public byte[] uploadDocumentsBytesArray;

	@Getter
	@Setter
	public StreamedContent downloadFile;

	@Getter
	@Setter
	String fileExtension;

	@Getter
	@Setter
	public InputStream fileInputStream;

	// Transport Details

	@Getter
	@Setter
	String transportServiceType;

	@Getter
	@Setter
	String transportServiceName;

	@Getter
	@Setter
	String wayBillAvailable;

	@Getter
	@Setter
	String wayBillNumber;

	@Getter
	@Setter
	String transportChargeAvailble;

	@Getter
	@Setter
	String transportChargeType;

	@Getter
	@Setter
	double transchargeChargeAmount;

	@Getter
	@Setter
	boolean addItemReceiveDetails;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplier_Type_Master;

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportServiceMasterList;

	@Getter
	@Setter
	boolean waybillRendered;

	@Getter
	@Setter
	boolean transportChargeRendered;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	TransportMaster selectedTransportMaster;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	StockTransferType stockTransferType;

	@Getter
	@Setter
	List<StockTransferItems> selectedStockTransferItemsList;

	@Getter
	@Setter
	private boolean renderedEdit;

	@Getter
	@Setter
	private boolean renderedCreate;

	@Getter
	@Setter
	PurchaseInvoice purchaseInvoice;

	@Getter
	@Setter
	PurchaseInvoice selectedPurchaseInvoice;

	@Getter
	@Setter
	List<PurchaseInvoice> purchaseInvoiceList;

	@Getter
	@Setter
	List<PurchaseInvoice> selectedPurchaseInvoiceList;

	@Getter
	@Setter
	private UploadedFile uploadDocumentFile = null;

	@Getter
	@Setter
	JobApplication jobApplication;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();;

	@Getter
	@Setter
	ProductCategory selectedProductCategory = new ProductCategory();

	@Getter
	@Setter
	ProductGroupMaster selectedProductGroupMaster = new ProductGroupMaster();

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	Long selectedProductVarietyMasterID;

	@Getter
	@Setter
	String currentFilePath;

	@Getter
	@Setter
	String currentFileType;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	@PostConstruct
	public void init() {

		waybillRendered = false;
		transportChargeRendered = false;

		wayBillAvailable = "Select Waybill Type";
		transportChargeAvailble = "Select Type";
		waybillRendered = false;
		transportChargeRendered = false;
		renderedEdit = false;
		renderedCreate = true;

		reset();
		loadTransportTypeMasterList();
		loadSupplierType();
		loadLazyIntRequirementList();
		renderedInwardTypeSelection();
		productCategoryGroupList();
	}

	public void reset() {

		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		stockTransfer = new StockTransfer();

		purchaseOrderItems = new PurchaseOrderItems();
		supplierMasterList = new ArrayList<SupplierMaster>();
		supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
		selectedSupplierMaster = new SupplierMaster();
		renderedInward = true;
		selectedSupplierMasterTest = new SupplierMaster();
		inwardType = "Direct Inward";
		stockTransferItems = new StockTransferItems();

		stockInwardYarnBean = new StockInwardYarnBean();
		selectedSupplier_Type_Master = new SupplierTypeMaster();
		selectedTransportTypeMaster = new TransportTypeMaster();
		selectedTransportMaster = new TransportMaster();
		purchaseOrderList = new ArrayList<PurchaseOrder>();
		productVarietyList = new ArrayList<ProductVarietyMaster>();
		//supplierTypeEnumList = new ArrayList<SupplierType>();
		stockTransferTypeEnumList = new ArrayList<StockTransferType>(Arrays.asList(StockTransferType.values()));
		stockTransferEnumStatus = new ArrayList<StockTransferStatus>(Arrays.asList(StockTransferStatus.values()));
		stockTransferItemsInwardUpdateList = new ArrayList<StockTransferItems>();

		purchaseInvoice = new PurchaseInvoice();
		purchaseInvoiceList = new ArrayList<PurchaseInvoice>();
		jobApplication = new JobApplication();
		selectedStockTransferPNS = new StockTransfer();
		selectedSupplierTypeMaster = null;
		transportChargeType = "Select Type";
		log.info("Default Value Set Successfully");
	}

	// Load Supplier Type List
	public void loadSupplierType() {
		log.info(":::<- Load Supplier TypeStart ->:::");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllSupplierType";
			log.info("::: Supplier Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get Supplier Response :");
			if (baseDTO.getStatusCode() == 0) {
				supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
				log.info("Supplier load Successfully " + baseDTO.getTotalRecords());

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
				log.info("StockItemInwardList Page Convert Error -->");
			} else {
				log.warn("Supplier Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Supplier load ", ee);
		}

	}

	// Load Supplier Code / Name
	public void loadSupplierCodeName() {
		try {
			log.info(":::<- Load loadSupplierCodeName Called ->::: " + selectedSupplierTypeMaster.getCode());
			BaseDTO baseDTO = null;
			selectedSupplierMaster = new SupplierMaster();
			// selectedSupplierMaster.setId(selectedSupplierTypeMaster.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedSupplierMasterDetails/" + selectedSupplierTypeMaster.getId();
			log.info("::: supplierMaster Controller calling  :::");
			baseDTO = httpService.get(url);
			log.info("::: get supplierMaster Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				supplierMasterList = new ArrayList<SupplierMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
			} else {
				log.warn("supplierMaster Load Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public void loadPurchaseOrderNumber() {

		log.info(":::<- Load loadPurchaseOrderNumber TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getPurhaseOrder/1";
			log.info("::: PurchaseOrderNumber Controller calling :::");
			// PurchaseOrderItems items = new PurchaseOrderItems();
			baseDTO = httpService.get(url);
			log.info("::: get PurchaseOrderNumber Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				purchaseOrderList = new ArrayList<PurchaseOrder>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrderList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrder>>() {

				});
				log.info("loadPurchaseOrderNumber load Successfully " + baseDTO.getTotalRecords());
				log.info("loadPurchaseOrderNumber Page Convert Succes -->");
			} else {
				log.warn("loadPurchaseOrderNumber Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadPurchaseOrderNumber load ", ee);
		}
	}

	public void loadProductVarietyDetails() {

		log.info(":::<- Load loadProductVarietyDetails TypeStart ->::: ");
		BaseDTO baseDTO = null;
		// selectedProductVarietyMaster =new ProductVarietyMaster();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllProductVariety/1";
			log.info("::: loadProductVarietyDetails Controller calling  1 :::");
			// PurchaseOrderItems items = new PurchaseOrderItems();
			baseDTO = httpService.get(url);
			log.info("::: get loadProductVarietyDetails Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				productVarietyList = new ArrayList<ProductVarietyMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyList = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyMaster>>() {
				});
				log.info("loadProductVarietyDetails load Successfully " + baseDTO.getTotalRecords());
				log.info("loadProductVarietyDetails Page Convert Succes -->");
			} else {
				log.warn("loadPurchaseOrderNumber Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadPurchaseOrderNumber load ", ee);
		}
	}

	// add Item Edit Details
	public void itemCollectiorAddNewInward() {
		log.info("itemCollectiorAddNew Inward received :");
		try {

			if (stockTransferItems.getOrderedQty() == null || stockTransferItems.getOrderedQty() <= 0) {
				log.info("Invalid Item Datails quantity:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_QUANTITY.getErrorCode());
			} else if (stockTransferItems.getUnitRate() == null || stockTransferItems.getUnitRate() <= 0) {
				log.info("Invalid Item Datails unitRate:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_UNIT.getErrorCode());
			} else if (stockTransferItems.getDiscountPercent() == null || stockTransferItems.getDiscountPercent() < 0) {
				log.info("Invalid Item Datails discount :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_DISCOUNT.getErrorCode());
			} else if (stockTransferItems.getTaxPercent() == null || stockTransferItems.getTaxPercent() < 0) {
				log.info("Invalid Item Datails tax:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_TAX.getErrorCode());
			} else if (selectedProductVarietyMaster == null || selectedProductVarietyMaster.getCode() == null) {
				log.info("Invalid ProductVarietyMaster Selection");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_SELECT.getErrorCode());
			} else {
				// loadPurchaseOrderNumber();
				stockTransferItems.setProductVarietyMaster(selectedProductVarietyMaster);
				stockTransferItems.setUomMaster(selectedProductVarietyMaster.getUomMaster());

				stockTransferItems.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
				stockTransferItems.setItemNetTotal(stockTransferItems.getItemTotal());

				double total = stockTransferItems.getItemTotal();
				if (stockTransferItems.getDiscountPercent() > 0) {
					double discountV = (total * stockTransferItems.getDiscountPercent()) / 100;
					stockTransferItems.setDiscountValue(discountV);
					stockTransferItems.setItemNetTotal(
							stockTransferItems.getItemNetTotal() - stockTransferItems.getDiscountValue());
				}

				if (stockTransferItems.getTaxPercent() > 0) {
					double tax = stockTransferItems.getTaxPercent();
					double taxV = (total * tax) / 100;
					stockTransferItems.setTaxValue(taxV);
					stockTransferItems
							.setItemNetTotal(stockTransferItems.getItemNetTotal() + stockTransferItems.getTaxValue());
				}

				// stockTransferItems.setItemNetTotal( (stockTransferItems.getItemTotal() +
				// stockTransferItems.getTaxValue() ) -
				// (stockTransferItems.getDiscountValue()));
				stockTransferItemsInwardUpdateList.add(stockTransferItems);

				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_ITEM_ADDED_SUCCESSFULLY.getErrorCode());
				log.info("Item Collection add Success :");
				stockTransferItems = new StockTransferItems();

			}
		} catch (Exception e) {
			log.error("StockItemInward Collection Add Error :" + e);
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_ADDED_FAILED.getErrorCode());
		}

	}

	public void onRowEdit(RowEditEvent event) {
		try {
			StockTransferItems stockTransferItems = ((StockTransferItems) event.getObject());
			stockTransferItems.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
			double total = stockTransferItems.getItemTotal();
			if (stockTransferItems.getDiscountPercent() != 0) {
				double discountV = (total * stockTransferItems.getDiscountPercent()) / 100;
				stockTransferItems.setDiscountValue(discountV);
			}

			if (stockTransferItems.getTaxPercent() != 0) {
				double tax = stockTransferItems.getTaxPercent();
				double taxV = (total * tax) / 100;
				stockTransferItems.setTaxValue(taxV);
			}
			stockTransferItems.setItemNetTotal((stockTransferItems.getItemTotal() + stockTransferItems.getTaxValue())
					- (stockTransferItems.getDiscountValue()));

			RequestContext.getCurrentInstance().update(":itemPNS:itemTablePanel");
			Util.addInfo("Edited :" + stockTransferItems.getProductVarietyMaster().getName());
		} catch (Exception see) {
			log.error("OnRow Edit Inward DataTable Error :" + see);
		}

	}

	public void onRowCancel(RowEditEvent event) {
		try {
			Util.addInfo("Cancelled " + ((StockTransferItems) event.getObject()).getProductVarietyMaster().getName());
		} catch (Exception see) {
			log.error("OnRowCancel Inward DataTable Error :" + see);
		}
	}

	public void removeItemInwardListCollector(StockTransferItems stockTransferItems) {
		log.info("StockInward Item Remove Received :");
		stockTransferItemsInwardUpdateList.remove(stockTransferItems);
		log.info("StockInward Item Remove Done:");
	}

	public void clearItemTable() {
		purchaseOrderItems = new PurchaseOrderItems();
	}

	public String reinit() {
		// stockItemInwardPNSBean = new StockItemInwardPNSBean();
		// enable if you want update null values after add values
		// Util.addInfo("New Item Added Successfully");
		return null;
	}

	public String reinitUpdate(StockTransferItems items) {
		log.info("Received Collector remove " + items.getId());
		try {
			stockTransferItemsInwardDeleteList.add(items);
			log.info("Received Collector remove Add Success");
		} catch (Exception e) {
			log.error("StockItems remove Add Error " + e);
			// TODO: handle exception
		}

		return null;
	}

	public void gstNumberUpdate() {
		try {
			log.info("***** Gst Number Set Received supplierMasterByID ******* ");
			gstNumber = selectedSupplierMaster.getGstNumber();
		} catch (Exception se) {
			log.error("Set Gst_Number Get Error " + se);
		}
	}

	public void purchaseOrderUpdate() {
		try {
			log.info("*****Received PurchaseOrderUpdate ******* ");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			purchaseDate = "";
			purhaseOrderValidityDate = "";

			purchaseDate = sdf.format(selectedPurchaseOrder.getOrderDate());
			purhaseOrderValidityDate = sdf.format(selectedPurchaseOrder.getValidDate());

			log.info("Done Purchase Order Update");
			purchaesOrderItemList(selectedPurchaseOrder.getId());
		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public void purchaesOrderItemList(Long id) {

		try {
			purchaseOrderItems = new PurchaseOrderItems();
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			purchaseOrderItems.setId(id);
			log.info("Received PurchaseOrderList :");
			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllPurchaseOrderItem";
			log.info("::: PurchaseOrderList Controller calling  1 :::");
			baseDTO = httpService.post(url, purchaseOrderItems);
			log.info("::: get PurchaseOrderList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				collectorSupplierItemList = mapper.readValue(jsonResponse,
						new TypeReference<List<PurchaseOrderItems>>() {
						});
				log.info("PurchaseOrderList load Successfully " + baseDTO.getTotalRecords());
				log.info("PurchaseOrderList Page Convert Succes -->");
			} else {
				log.warn("PurchaseOrderList Error ");
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_PURCHASE_ORDER_FAILED.getErrorCode());
			log.error("Get List PurchaseOrderList Error :" + ee);
		}

		log.info("Start Product Selected List :******************** ");

	}

	public void productVarietyUpdate() {
		try {
			log.info("***** Gst Number Set Received productVarietyUpdate ******* ");
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			hsnCode = selectedProductVarietyMaster.getHsnCode();
			uom = selectedProductVarietyMaster.getUomMaster().getId();

			log.info("Produect Variety Update Get Success :");
		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	// invoice method start
	public void viewPdfFile() {
		try {
			String fileType;
			String localFilePath;
			log.info("View PdfFile Received ");

			String workingDir = System.getProperty("user.dir");
			String path = selectedPurchaseInvoice.getFilePath();
			workingDir += "/bin/main/invoiceFiles/";
			log.info("File Path is ==> " + path);
			log.info("Local Path is ==> " + workingDir);

			File fileName = new File(selectedPurchaseInvoice.getFilePath());
			log.info("Local File Name is ==> " + fileName.getName());
			currentFilePath = "/invoiceFiles/" + fileName.getName();
			String extension = FilenameUtils.getExtension(fileName.getName());
			if (extension.contains("pdf"))
				currentFileType = "application/pdf";
			else
				currentFileType = "jpg/jpg";
			log.info("Current FilePath is  ==> " + currentFilePath);
			log.info("Current File Type  is  ==> " + currentFileType);
			InputStream inputStream = null;
			OutputStream outputStream = null;
			try {
				inputStream = new FileInputStream(new File(selectedPurchaseInvoice.getFilePath()));
				outputStream = new FileOutputStream(new File(workingDir + fileName.getName()));
				byte[] buffer = new byte[(int) uploadDocumentFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("File Export to Local Directory to  Success ==> " + fileName.getName());
			} catch (Exception e) {
				log.error("File Export to Local Directory to  Error ==> " + fileName.getName());
				log.error("error to download file :" + e);
				// TODO: handle exception
			}

		} catch (Exception e) {
			log.error("View PdfFile Error " + e);
			// TODO: handle exception
		}
	}

	public void deleteOpenFile(String filePath) {
		try {
			log.info("File Delete Received ");
			File file = new File(filePath);
			if (file.exists()) {
				file.delete();
				log.info("File Delete Successfully ");
			} else
				log.info("File Delete Failed " + file.getAbsolutePath());

		} catch (Exception e) {
			log.error("File Delete Error " + e);
			// TODO: handle exception
		}
	}

	public void uploadInvoiceDocument() {
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (size > 0) {
				String extension = FilenameUtils.getExtension(uploadDocumentFile.getFileName());
				String photoPathName = commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_PATH");
				log.info("Invoice PathName==>" + photoPathName);
				SimpleDateFormat sdf = new SimpleDateFormat("ddss");
				String photoPath = photoPathName + "/uploaded/" + purchaseInvoice.getInvoiceNumber()
						+ purchaseInvoice.getInvoiceNumberPrefix() + sdf.format(new Date()) + "." + extension;
				Path path = Paths.get(photoPathName + "/uploaded/");
				log.info(" Invoice Upload Path  : " + photoPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(path)) {
					Files.createDirectories(path);
				}
				double kilobytes = outputFile.length() / 1024;
				log.info("File kilobytes :" + kilobytes);
				inputStream = uploadDocumentFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) uploadDocumentFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				jobApplication.setPhotographPath(photoPath);
				purchaseInvoice.setFilePath(photoPath);

				log.info(" Invoice Upload Path  Set Success fully : " + purchaseInvoice.getFilePath());
			}
		} catch (Exception e) {
			log.error("Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String handleFileUpload(FileUploadEvent event) {
		try {
			Long invoiceFileSize = Long
					.parseLong(commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_FILE_SIZE"));
			log.info("invoiceFileSize " + invoiceFileSize);
			InputStream inputStream = null;
			OutputStream outputStream = null;
			uploadDocumentFile = event.getFile();
			log.info(" Invoice file name :: " + uploadDocumentFile.getFileName());
			log.info(" Invoice Uploaded file Content Type : " + uploadDocumentFile.getContentType());
			String extension = FilenameUtils.getExtension(uploadDocumentFile.getFileName());
			if (!extension.equalsIgnoreCase("png") && !extension.equalsIgnoreCase("jpg")
					&& !extension.equalsIgnoreCase("gif") && !extension.equalsIgnoreCase("jpeg")
					&& !extension.equalsIgnoreCase("pdf")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVOICE_FORMAT_NOT_VALID.getErrorCode());
				log.info("File Format Not Valid ");
				return null;
			}
			long size = uploadDocumentFile.getSize();
			log.info("size==>" + size);
			size = size / 1000;
			if (size > invoiceFileSize) {
				jobApplication.setPhotographPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVOICE_FILE_SIZE.getErrorCode());
				return null;
			}
			if (size > 0) {
				String photoPathName = commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_PATH");
				log.info("Invoice PathName==>" + photoPathName);
				SimpleDateFormat sdf = new SimpleDateFormat("ddss");
				String photoPath = photoPathName + "/uploaded/" + purchaseInvoice.getInvoiceNumber()
						+ purchaseInvoice.getInvoiceNumberPrefix() + sdf.format(new Date()) + "." + extension;
				Path path = Paths.get(photoPathName + "/uploaded/");
				log.info(" Invoice Upload Path  : " + photoPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(path)) {
					Files.createDirectories(path);
				}
				double kilobytes = outputFile.length() / 1024;
				log.info("File kilobytes :" + kilobytes);
				if (kilobytes > 100) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_FILE_SIZE_INVALID.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String getInvoiceFileExtenstion(PurchaseInvoice purchaseInvoice) {
		String extension = FilenameUtils.getExtension(purchaseInvoice.getFilePath());
		return extension;
	}

	public void selectInvoiceRows(PurchaseInvoice stockIn) {
		log.info("************ Purchase Invoice Set Called : ********** ");
		selectedPurchaseInvoice = stockIn;
		// viewPdfFile();
		try {
			selectedStockInwardYarnBean = new StockInwardYarnBean();
			log.info("************ path " + stockIn.getFilePath());
			FileInputStream fin = new FileInputStream(stockIn.getFilePath());
			selectedStockInwardYarnBean.setFileInputStream(fin);
			selectedStockInwardYarnBean.setPurchaseInvoice(stockIn);
			String extension = FilenameUtils.getExtension(new File(stockIn.getFilePath()).getName());
			selectedStockInwardYarnBean.setFileExtension(extension);
			log.info("************ Purchase Invoice Set Called 2: ********** ");
			invoiceFileDownload();
		} catch (Exception e) {
			log.error("************ Purchase Invoice Error " + e);
		}
	}

	// this is method is download uploaded files temp.
	public void invoiceFileDownload() {
		log.info("File Download Received ");
		InputStream targetStream = selectedStockInwardYarnBean.fileInputStream;

		String ext = selectedStockInwardYarnBean.fileExtension;
		String fileName = selectedStockInwardYarnBean.purchaseInvoice.getInvoiceNumber() + "."
				+ selectedStockInwardYarnBean.getFileExtension();
		if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
			log.info("IMG File Download Stream");
			selectedStockInwardYarnBean.downloadFile = new DefaultStreamedContent(targetStream, "image/jpg", fileName);
		} else if (ext.contains("pdf")) {
			log.info("PDF File Download Stream");
			selectedStockInwardYarnBean.downloadFile = new DefaultStreamedContent(targetStream, "application/pdf",
					fileName);
		} else if (ext.contains("doc")) {
			log.info("DOC File Download Stream");
			selectedStockInwardYarnBean.downloadFile = new DefaultStreamedContent(targetStream, "doc", fileName);
		} else if (ext.contains("txt")) {
			log.info("TXT File Download Stream");
			selectedStockInwardYarnBean.downloadFile = new DefaultStreamedContent(targetStream, "txt", fileName);
		}
		log.info("File Download Received Set done ******** Streamed Set Success ******* ");

	}

	public String generateRandomIdForNotCaching() {
		return java.util.UUID.randomUUID().toString();
	}

	public String itemInvoiceUpdateNew() {

		log.info("Invoice add received :");
		if (purchaseInvoice.getInvoiceNumber() == null) {
			log.info("Invalid Invoice Number");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getInvoiceDate() == null) {
			log.info("Invoice Date Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DATE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getDcNumber() == null) {
			log.info("dcNumber Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DC.getErrorCode());
			return null;
		} else if (purchaseInvoice.getPaymentDuedate() == null) {
			log.info("Payment DueDate Not Valid");

			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_PAYMENT.getErrorCode());
			return null;
		} else if (interestApplicablePayment == null || interestApplicablePayment.equals("Select")) {
			log.info("Application for Delay Payment Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DELAY.getErrorCode());
			return null;
		} else if (purchaseInvoice.getRateOfInterest() < 0) {
			log.info("Rate of Interenst Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_INTEREST.getErrorCode());
			return null;
		} else if (uploadDocumentFile == null) {
			log.info("Invoice Document Not Found");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DOC.getErrorCode());
			return null;
		}
		try {
			boolean interestApplicable = false;
			if (interestApplicablePayment.equalsIgnoreCase("Yes")) {
				interestApplicable = true;
			}
			purchaseInvoice.setInterestApplicable(interestApplicable);

			SimpleDateFormat sdf = new SimpleDateFormat("MMMyyyy");
			purchaseInvoice.setInvoiceNumberPrefix("PI".concat(sdf.format(new Date())));
			selectedPurchaseInvoiceList.add(purchaseInvoice);
			uploadInvoiceDocument();

			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_ADDED_SUCCESS.getErrorCode());

			log.info("Invoice add Success :");
			purchaseInvoice = new PurchaseInvoice();
			uploadDocumentFile = null;
			setInterestApplicablePayment("Select");
		} catch (Exception e) {
			log.info("Invoice add Error :" + e);
			// TODO: handle exception
		}
		return null;
	}

	public String itemInvoiceAddNew() {
		log.info("Invoice add received :");

		log.info("Invoice add received :");
		if (purchaseInvoice.getInvoiceNumber() == null) {
			log.info("Invalid Invoice Number");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getInvoiceDate() == null) {
			log.info("Invoice Date Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DATE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getDcNumber() == null) {
			log.info("dcNumber Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DC.getErrorCode());
			return null;
		} else if (purchaseInvoice.getPaymentDuedate() == null) {
			log.info("Payment DueDate Not Valid");

			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_PAYMENT.getErrorCode());
			return null;
		} else if (interestApplicablePayment == null || interestApplicablePayment.equals("Select")) {
			log.info("Application for Delay Payment Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DELAY.getErrorCode());
			return null;
		} else if ((interestApplicablePayment.equalsIgnoreCase("Yes"))
				&& (purchaseInvoice.getRateOfInterest() == null || purchaseInvoice.getRateOfInterest() <= 0)) {
			log.info("Rate of Interenst Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_INTEREST.getErrorCode());
			return null;
		} else if (uploadDocumentFile == null) {
			log.info("Invoice Document Not Found");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DOC.getErrorCode());
			return null;
		}
		try {
			log.info("New Invoice Details Start to Add");
			if (interestApplicablePayment.equalsIgnoreCase("Yes"))
				purchaseInvoice.setInterestApplicable(true);
			else
				purchaseInvoice.setInterestApplicable(false);
			SimpleDateFormat sdf = new SimpleDateFormat("MMMyyyy");
			purchaseInvoice.setInvoiceNumberPrefix("PI".concat(sdf.format(new Date())));
			purchaseInvoiceList.add(purchaseInvoice);
			uploadInvoiceDocument();
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_ADDED_SUCCESS.getErrorCode());
			log.info("Invoice add Success ");
			purchaseInvoice = new PurchaseInvoice();
			uploadDocumentFile = null;
			setInterestApplicablePayment("Select");
		} catch (Exception e) {
			log.info("New Invoice Details add Error :" + e);
			// TODO: handle exception
		}
		return null;
		// }
	}

	public void clearInvoiceTable() {
		purchaseInvoice = new PurchaseInvoice();
		uploadDocumentFile = null;
		setInterestApplicablePayment("Select");
		log.info("Invoice Clear Success:");
	}

	public void renderedInwardTypeSelectionEdit() {
		log.info("Rendered Inwared Change Listener Received ");
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();

		if (inwardType.equals("Direct Inward")) {
			renderedInward = false;
			addItemReceiveDetails = true;
		} else if (inwardType.equals("PO Based Inward")) {
			renderedInward = true;
			addItemReceiveDetails = false;
			log.info("size..." + purchaseOrderList.size());
		} else {
			renderedInward = false;
			addItemReceiveDetails = false;
			log.warn("rendered inwared Invalid selection");
		}
	}

	public void renderedInwardTypeSelection() {
		log.info("Rendered Inwared Change Listener Received ");
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();

		if (inwardType.equals("Direct Inward")) {
			loadProductVarietyDetails();
			renderedInward = false;
			addItemReceiveDetails = true;
		} else if (inwardType.equals("PO Based Inward")) {
			loadPurchaseOrderNumber();

			renderedInward = true;
			addItemReceiveDetails = false;
			log.info("size..." + purchaseOrderList.size());
		} else {
			renderedInward = false;
			addItemReceiveDetails = false;
			log.warn("rendered inwared Invalid selection");
		}
	}

	public void test() {
		log.info("Received supplier MasterTest ");
		try {
			log.info(selectedSupplierMasterTest.getName());
		} catch (Exception e) {
			log.info("Supplier MasterTest Error " + e);
			// TODO: handle exception
		}
	}

	// Transport Service Type transportTypeMasterList
	public void loadTransportTypeMasterList() {

		log.info(":::<- Load TransportTypeMasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportTypeMasterList";
			log.info("::: TransportTypeMasterList Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get TransportTypeMasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportTypeMasterList = new ArrayList<TransportTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<TransportTypeMaster>>() {
						});
				log.info("TransportTypeMasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportTypeMasterList Page Convert Succes -->");
			} else {
				log.warn("TransportTypeMasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportTypeMasterList load ", ee);
		}
	}

	public void transportTypeMasterListener() {
		log.info("Received Listener of selectedTransportTypeMaster **************");
		try {
			transportServiceMasterList = new ArrayList<TransportMaster>();
			loadTransportServiceList(selectedTransportTypeMaster.getId());
		} catch (Exception se) {
			log.error("SET selectedTransportTypeMaster Get Error " + se);
		}
		log.info("Done selectedTransportTypeMaster **************");
	}

	// Transport Service Name
	public void loadTransportServiceList(Long id) {

		log.info(":::<- Load TransportService MasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportServiceMasterList";
			log.info("::: TransportService MasterList Controller calling  1 :::");
			TransportMaster mas = new TransportMaster();
			mas.setId(id);
			baseDTO = httpService.post(url, mas);
			log.info("::: get TransportService MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportServiceMasterList = new ArrayList<TransportMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportServiceMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TransportMaster>>() {
				});
				log.info("TransportService MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportService MasterList Page Convert Succes -->");
			} else {
				log.warn("TransportService MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportService MasterList load ", ee);
		}
	}

	public String submitData() {

		log.info("Received Submit Data ******************** ");
		// transport check
		log.info("wayBillAvailable :" + waybillRendered + ",");
		if (selectedTransportTypeMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
			return null;
		}
		if (selectedTransportMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
			return null;
		}
		if (wayBillAvailable == null || wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
			return null;
		}

		if (waybillRendered == true && (wayBillNumber == null || wayBillNumber.trim().length() <= 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble == null || transportChargeAvailble.equalsIgnoreCase("Select Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
			return null;
		}
		if (transportChargeAvailble.equalsIgnoreCase("Yes")
				&& (transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			if (stockTransfer.getTransportChargeAmount() == null || this.stockTransfer.getTransportChargeAmount() < 0) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
				return null;
			}
		}

		if (transportChargeType.equalsIgnoreCase("Yes") && (this.stockTransfer.getTransportChargeAmount() == 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeType.equals("Cash")) {
			this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
		} else {
			this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
		}

		if (this.stockTransfer == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_STOCK_NULL.getErrorCode());
			return null;
		}
		this.stockTransfer.setTransportChargeAvailable(transportChargeRendered);
		this.stockTransfer.setWaybillAvailable(waybillRendered);
		if (waybillRendered) {
			stockTransfer.setWaybillNumber(wayBillNumber);
			log.info("WayBill Value Set Success ******************** ");
		}

		if (inwardType.equals("Direct Inward")) {
			this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_INWARD);
			log.info("Direct Inward Save Processed ******************** ");
			if (stockTransferItemsInwardUpdateList == null || stockTransferItemsInwardUpdateList.size() == 0) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TABLE_NULL.getErrorCode());
				return null;
			}
		} else if (inwardType.equals("PO Based Inward")) {
			this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_PO_BASED);
			log.info("PO Based Inward Save Processed ******************** ");
			if (collectorSupplierItemList == null || collectorSupplierItemList.size() == 0) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SUPPLIER_ITEM_NULL.getErrorCode());
				return null;
			}
		}
		selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);
		this.stockTransfer.setTransportMaster(selectedTransportMaster); // Type of Transport serviceType
		this.stockTransfer.setSupplierMaster(selectedSupplierMaster); // Supplier Type

		StockItemInwardPNSDTO stock = new StockItemInwardPNSDTO();

		this.stockTransfer.setDateReceived(new Date());
		this.stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		this.stockTransfer.setMessage(this.description);

		// invoice object creation start

		for (PurchaseInvoice invoice : purchaseInvoiceList) {
			invoice.setSupplierMaster(selectedSupplierMaster);
			invoice.setStockTransfer(this.stockTransfer);
			invoice.setStatus(StockTransferStatus.SUBMITTED.toString());
		}

		log.info("Invoice Set Successfully -->");

		log.info("Received Submit Data Start ******************** ");
		stockTransferItemsList = new ArrayList<StockTransferItems>();
		if (inwardType.equals("PO Based Inward")) {
			for (PurchaseOrderItems items : collectorSupplierItemList) {
				StockTransferItems stockTransferItems = null;
				stockTransferItems = new StockTransferItems();
				stockTransferItems.setOrderedQty(items.getItemQty());
				stockTransferItems.setItemAmount(items.getItemAmount());
				if (items.getUnitRate() != null)
					stockTransferItems.setUnitRate(items.getUnitRate());
				if (items.getItemTotal() != null) {
					stockTransferItems.setItemTotal(items.getItemTotal());
					stockTransferItems.setItemNetTotal(items.getItemTotal());
				}

				if (items.getTaxPercent() != null)
					stockTransferItems.setTaxPercent(items.getTaxPercent());
				if (items.getTaxValue() != null) {
					stockTransferItems.setTaxValue(items.getTaxValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() + items.getTaxValue());
				}

				if (items.getDiscountPercent() != null)
					stockTransferItems.setDiscountPercent(items.getDiscountPercent());
				if (items.getDiscountValue() != null) {
					stockTransferItems.setDiscountValue(items.getDiscountValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() - items.getDiscountValue());
				}
				if (items.getUomMaster() != null)
					stockTransferItems.setUomMaster(items.getUomMaster());

				stockTransferItems.setSupplierMaster(selectedSupplierMaster);
				stockTransferItems.setProductVarietyMaster(items.getProductVarietyMaster());
				stockTransferItems.setStockTransfer(stockTransfer);

				stockTransferItems.setPurchaseOrder(selectedPurchaseOrder);

				stockTransferItemsList.add(stockTransferItems);
				log.info(":::<- Add List data -> :::" + items.getProductVarietyMaster().getCode() + " "
						+ items.getProductVarietyMaster().getName() + ",ID ->" + items.getId());

			}
			stock.setStockTransferItemsList(stockTransferItemsList);

		} else if (inwardType.equals("Direct Inward")) {
			for (StockTransferItems stockItems : stockTransferItemsInwardUpdateList) {

				stockItems.setSupplierMaster(selectedSupplierMaster);
				stockItems.setStockTransfer(stockTransfer);
			}
			stock.setStockTransferItemsList(stockTransferItemsInwardUpdateList);
		}

		stock.setStockTransfer(stockTransfer);
		stock.setPurchaseInvoiceList(purchaseInvoiceList);
		log.info(":::<- Start to save Data->:::");

		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/saveStockTransferItems";
			log.info("::: saveStockInwardAll Controller calling  1 :::");
			baseDTO = httpService.post(url, stock);
			log.info("::: get saveStockInwardAll Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				log.info("saveStockInwardAll Page Convert Succes -->");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_SUCCESS.getErrorCode());
				return INPUT_FORM_LIST_URL;
			} else {
				log.warn("saveStockInwardAll Error *** :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in saveStockInwardAll load ", ee);
		}

		return null;
	}

	public String updateStockTransfer() {
		log.info("Received Update Data ******************** ");// selectedStockTransferPNS

		log.info("Received Update ID ******************** " + selectedStockTransferPNS.getId());
		if (selectedStockTransferPNS == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_STOCK_NULL.getErrorCode());
			return null;
		}
		// transport check
		if (selectedTransportTypeMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
			return null;
		}
		if (selectedTransportMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
			return null;
		}
		if (wayBillAvailable == null || wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
			return null;
		}

		if (waybillRendered == true && (wayBillNumber == null || wayBillNumber.trim().length() <= 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			return null;
		}
		if (transportChargeAvailble == null || transportChargeAvailble.equalsIgnoreCase("Select Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
			return null;
		}
		if (transportChargeAvailble.equalsIgnoreCase("Yes")
				&& (transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			if (selectedStockTransferPNS.getTransportChargeAmount() == null
					|| this.selectedStockTransferPNS.getTransportChargeAmount() < 0) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
				return null;
			}
		}

		StockItemInwardPNSDTO stock = new StockItemInwardPNSDTO();

		if (transportChargeType.equals("Cash")) {
			selectedStockTransferPNS.setTransportChargeType(TransportChargeType.Pay);
		} else {
			selectedStockTransferPNS.setTransportChargeType(TransportChargeType.ToPay);
		}
		selectedStockTransferPNS.setTransportChargeAvailable(transportChargeRendered);
		selectedStockTransferPNS.setWaybillAvailable(waybillRendered);
		if (waybillRendered) {
			selectedStockTransferPNS.setWaybillNumber(wayBillNumber);
			log.info("WayBill Value Set Success ******************** ");
		}
		selectedStockTransferPNS.setModifiedDate(new Date());

		String url = null;

		stockTransferItemsList = new ArrayList<StockTransferItems>();
		if (inwardType.equals("PO Based Inward")) {
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/updateStockTransferPOBased";
			log.info("PO Based Inward Save Processed ******************** ");

			for (PurchaseOrderItems items : collectorSupplierItemList) {

				stockTransferItems = new StockTransferItems();
				stockTransferItems.setOrderedQty(items.getItemQty());
				stockTransferItems.setItemAmount(items.getItemAmount());
				stockTransferItems.setId(items.getId());

				if (items.getUnitRate() != null)
					stockTransferItems.setUnitRate(items.getUnitRate());
				if (items.getItemTotal() != null) {
					stockTransferItems.setItemTotal(items.getItemTotal());
					stockTransferItems.setItemNetTotal(items.getItemTotal());
				}

				if (items.getTaxPercent() != null)
					stockTransferItems.setTaxPercent(items.getTaxPercent());
				if (items.getTaxValue() != null) {
					stockTransferItems.setTaxValue(items.getTaxValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() + items.getTaxValue());
				}

				if (items.getDiscountPercent() != null)
					stockTransferItems.setDiscountPercent(items.getDiscountPercent());
				if (items.getDiscountValue() != null) {
					stockTransferItems.setDiscountValue(items.getDiscountValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() - items.getDiscountValue());
				}
				if (items.getUomMaster() != null)
					stockTransferItems.setUomMaster(items.getUomMaster());

				stockTransferItems.setStockTransfer(selectedStockTransferPNS);

				log.info(":::<- Goin to set Purhase Order->:::");

				stockTransferItemsList.add(stockTransferItems);
				log.info(":::<- Add List data -> :::" + items.getProductVarietyMaster().getCode() + " "
						+ items.getProductVarietyMaster().getName());
			}
		} else if (inwardType.equals("Direct Inward")) {
			log.info("Direct Inward Save Processed ******************** ");
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/updateStockTransferInward";

			for (StockTransferItems stockItems : stockTransferItemsInwardUpdateList) {

				stockItems.setSupplierMaster(selectedSupplierMaster);
				stockItems.setStockTransfer(selectedStockTransferPNS);
			}
			stock.setStockTransferItemsList(stockTransferItemsInwardUpdateList);
		}
		selectedStockTransferPNS.setMessage(this.description);
		// add stock items end

		selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);

		stock.setStockTransfer(selectedStockTransferPNS);
		stock.setStockTransferItemsDeleteList(stockTransferItemsInwardDeleteList);

		for (PurchaseInvoice invoice : selectedPurchaseInvoiceList) {
			invoice.setSupplierMaster(selectedSupplierMaster);
			invoice.setStockTransfer(selectedStockTransferPNS);
		}
		stock.setPurchaseInvoiceList(selectedPurchaseInvoiceList);

		log.info(":::<- Start to update Data->:::");

		BaseDTO baseDTO = null;

		try {
			log.info("::: Update StockInwardAll Controller calling  1 :::");
			baseDTO = httpService.post(url, stock);
			log.info("::: get Update StockInwardAll Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				log.info("Update StockInwardAll Page Convert Succes -->");
				log.warn("Update Stock Item Inward Successfully Completed ******");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_SUCCESS.getErrorCode());
				// resetData();
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
				return INPUT_FORM_LIST_URL;
			} else {
				log.warn("Update StockInwardAll Error *** :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_ERROR.getErrorCode());
				Util.addError("Update StockItemInward Error");
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in Update StockInwardAll load ", ee);
		}
		return null;
	}

	public void resetData() {
		log.info("Reset Data Received ");
		try {
			collectorSupplierItemList.clear();

			wayBillAvailable = "Select Waybill Type";
			transportChargeAvailble = "Select Type";
			purchaseOrderItems = new PurchaseOrderItems();
			selectedProductVarietyMaster = new ProductVarietyMaster();
			selectedPurchaseOrder = new PurchaseOrder();
			selectedSupplierTypeMaster = new SupplierTypeMaster();
			selectedSupplierMaster = new SupplierMaster();
			selectedTransportTypeMaster = new TransportTypeMaster();
			this.wayBillNumber = null;
			loadLazyIntRequirementList();
			this.transchargeChargeAmount = 0d;
			selectedTransportMaster = new TransportMaster();

		} catch (Exception e) {
			log.info("Reset All Data Error " + e);
			// TODO: handle exception
		}
		log.info("Reset All Data Done");
	}

	public void renderedWaybillNumber() {
		log.info("rendered WaybillNumber received ");
		if (wayBillAvailable.equalsIgnoreCase("YES")) {
			waybillRendered = true;
		} else {
			waybillRendered = false;
		}
	}

	public void renderedTransportChargeRendered() {
		log.info("rendered transportChargeRendered received ");
		if (transportChargeAvailble.equalsIgnoreCase("YES")) {
			transportChargeRendered = true;
		} else {
			transportChargeRendered = false;
		}
	}
	// sum of bottom datatable total value calculation

	public String getUnitRateTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getUnitRateTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getUnitRateTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTotalRateTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal(); // Check total after
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTotalRateTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTotalRateTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getDiscountValueTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getDiscountValueTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getDiscountValueTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTaxValueTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTaxValueTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTaxValueTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getNetPriceTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
			// total += (total - purchaseOrderItems.discountValue) + (
			// purchaseOrderItems.taxValue ) ;
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getNetPriceTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getItemNetTotal() != null)
				total += collector.getItemNetTotal();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getNetPriceTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getItemNetTotal() != null)
				total += collector.getItemNetTotal();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public StockInwardYarnBean() {
		super();
	}

	// lazy search
	public void loadLazyIntRequirementList() {
		log.info("<--- loadLazyStockTransfer RequirementList ---> ");
		stockTransferLazyList = new LazyDataModel<StockItemInwardPNSDTO>() {
			// StockTransfer ssss =new StockTransfer();
			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<StockItemInwardPNSDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockItemInwardPNSDTO> data = new ArrayList<StockItemInwardPNSDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<StockItemInwardPNSDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyStockTransfer RequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(StockItemInwardPNSDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockItemInwardPNSDTO getRowData(String rowKey) {
				List<StockItemInwardPNSDTO> responseList = (List<StockItemInwardPNSDTO>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (StockItemInwardPNSDTO res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							stockTransferLazy = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		stockTransferLazy = new StockItemInwardPNSDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		StockItemInwardPNSDTO yarnRequirement = new StockItemInwardPNSDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/searchData";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public String clearButton() {
		log.info("Clear Action Called");
		renderedEdit = false;
		renderedCreate = true;
		selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
		// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
		return INPUT_FORM_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Item List onRowSelect method started");
		log.info("Selected ID is :" + selectedStockItemInwardPNSDTO.getId());
		renderedEdit = true;
		renderedCreate = false;
	}

	// View Selected StockItemInwardPNS
	public String itemReceiveRetrieveDetails() {
		log.info("Received Item Retrieved Details ");
		selectedStockTransferPNS = new StockTransfer();
		selectedPurchaseInvoice = new PurchaseInvoice();
		try {
			BaseDTO baseDTO = null;
			log.info("Retrieved SupplierCode :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockItemRetrieved";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				selectedStockTransferItemsList = new ArrayList<StockTransferItems>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedStockTransferItemsList = mapper.readValue(jsonResponse,
						new TypeReference<List<StockTransferItems>>() {
						});
				getStockTransferByID();
				log.warn("Size of StockItemLis is--->:" + selectedStockTransferItemsList.size());
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_VIEW_URL);
				return INPUT_FORM_VIEW_URL;
			} else {
				log.warn("Item Retrieved Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
		}
		log.info("Done Item Retrieved Details ");
		return null;
	}

	public void getStockTransferByID() {
		try {
			BaseDTO baseDTO = null;
			StockItemInwardPNSDTO stockItemInwardPNSDTO = new StockItemInwardPNSDTO();
			selectedPurchaseInvoiceList = new ArrayList<PurchaseInvoice>();
			log.info("Retrieved tockTransferByID :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockTransferRetrieved";
			log.info("::: Item tockTransferByID Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Item tockTransferByID Successfully Processed ******");
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockItemInwardPNSDTO = mapper.readValue(jsonResponse, new TypeReference<StockItemInwardPNSDTO>() {
				});

				selectedStockTransferPNS = stockItemInwardPNSDTO.getSelectedStockTransfer();

				log.info("Item tockTransferByID Successfully Completed ******");

				selectedStockItemInwardPNSDTO.setTrans_ser_type(
						selectedStockTransferPNS.getTransportMaster().getTransportTypeMaster().getName());
				selectedStockItemInwardPNSDTO
						.setTrans_ser_name(selectedStockTransferPNS.getTransportMaster().getName());

				selectedStockItemInwardPNSDTO.setWaybill_no(selectedStockTransferPNS.getWaybillNumber());
				selectedStockItemInwardPNSDTO.setWaybillAvailable(selectedStockTransferPNS.getWaybillAvailable());
				selectedStockItemInwardPNSDTO
						.setTransport_charge_available(selectedStockTransferPNS.getTransportChargeAvailable());
				if (selectedStockTransferPNS.getTransportChargeType().toString().equalsIgnoreCase("ToPay"))
					selectedStockItemInwardPNSDTO.setTrans_charge_type("Cheque");
				else
					selectedStockItemInwardPNSDTO.setTrans_charge_type("Cash");
				if (selectedStockTransferPNS.getTransportChargeAmount() != null)
					selectedStockItemInwardPNSDTO
							.setTrans_charge_amt(selectedStockTransferPNS.getTransportChargeAmount());

				selectedPurchaseInvoiceList = stockItemInwardPNSDTO.getSelectedPurchaseInvoiceList();
				log.info("Purchase Order List Successfully Completed ******");
			} else {
				log.error("tockTransferByID Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in tockTransferByID load ", ee);
		}
		log.info("Done Item tockTransferByID ");
	}

	// Edit selected ItemInward PNS
	public String itemReceiveRetrieveDetailsEdit() {
		log.info("Received Item Retrieved Details ");
		try {
			// reset();
			stockTransferItemsInwardDeleteList = new ArrayList<StockTransferItems>();
			selectedStockTransferItemsList = new ArrayList<StockTransferItems>();
			BaseDTO baseDTO = null;
			log.info("Retrieved SupplierCode :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockItemRetrieved";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedStockTransferItemsList = mapper.readValue(jsonResponse,
						new TypeReference<List<StockTransferItems>>() {
						});
				getStockTransferByID();
				log.info("Size of StockItemLis is--->:" + selectedStockTransferItemsList.size());
				getSelectedStockTransfer();
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_Edit_URL);
				return INPUT_FORM_Edit_URL;
			} else {
				log.error("Item Retrieved Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
		}
		log.info("Done Item Retrieved Details ");
		return null;
	}

	// update common data for selection
	public void getSelectedStockTransfer() {
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
		try {
			stockTransferItemsInwardUpdateList = new ArrayList<StockTransferItems>();
			log.info("Edit Method Called");
			stockTransfer = selectedStockTransferPNS;
			log.info("Edit Method Supplier Type:" + selectedStockItemInwardPNSDTO.getCode());
			log.info("Edit Method Name :" + selectedStockTransferPNS.getSupplierMaster().getCode() + " / "
					+ selectedStockTransferPNS.getSupplierMaster().getName());
			for (SupplierTypeMaster master : getSupplierTypeMasterList()) {
				log.info("Edit Method Check :" + master.getCode() + ","
						+ selectedStockItemInwardPNSDTO.getCode() + ":");
				if (master.getCode().toString().equals(selectedStockItemInwardPNSDTO.getCode())) {
					selectedSupplierTypeMaster = master;
					loadSupplierCodeName(); // ***
					log.info("Set successfully Supplier Master ");
					break;
				}
			}
			selectedSupplierMaster = selectedStockTransferPNS.getSupplierMaster();
			gstNumber = selectedStockTransferPNS.getSupplierMaster().getGstNumber();

			if (selectedStockTransferPNS.getTransferType().toString().equals("STOCK_ITEM_PO_BASED"))
				inwardType = "PO Based Inward";
			else
				inwardType = "Direct Inward";
			log.info("Edit Method Inward : " + inwardType);
			renderedInwardTypeSelectionEdit();
			Long purchaseOrderID = null;
			String poNumberPrefix = null;
			if (inwardType.equals("PO Based Inward")) {
				StockTransferItems stockTransfer = new StockTransferItems();
				log.info("PO Based Inward Processed  ---->");
				try {
					for (StockTransferItems order : selectedStockTransferItemsList) {
						purchaseOrderID = order.getPurchaseOrder().getId();
						stockTransfer = order;
						log.info("order DATA :" + purchaseOrderID);
						break;
					}
				} catch (Exception see) {
					log.error("Error to get data " + see);
				}
				getProductOrderSelected(stockTransfer);
				purchaseOrderUpdate();

			} else if (inwardType.equals("Direct Inward")) {
				log.info("Direct Inward Processed	---->");
				loadProductVarietyDetails(); // ***

				log.info("Size of Item :" + selectedStockTransferItemsList.size());
				if (selectedStockTransferItemsList.size() > 0)
					for (StockTransferItems order : selectedStockTransferItemsList) {
						PurchaseOrderItems items = new PurchaseOrderItems();

						items.setUnitRate(order.getUnitRate());
						items.setItemTotal(order.getItemTotal());
						items.setItemAmount(order.getItemAmount());
						items.setTaxPercent(order.getTaxPercent());
						items.setTaxValue(order.getTaxValue());

						items.setDiscountPercent(order.getDiscountPercent());
						items.setDiscountValue(order.getDiscountValue());
						items.setItemQty(order.getOrderedQty());
						items.setUomMaster(order.getUomMaster());
						items.setProductVarietyMaster(order.getProductVarietyMaster());

						items.setPurchaseOrder(order.getPurchaseOrder());

						items.setCreatedDate(order.getCreatedDate());

						items.setCreatedByName(order.getCreatedByName());

						selectedProductVarietyMaster = order.getProductVarietyMaster();
						log.info("leve 1:");
						collectorSupplierItemList.add(items);
						stockTransferItemsInwardUpdateList.add(order);
						log.info("Direct Inward Item Added :" + order.getProductVarietyMaster().getCode() + " / "
								+ order.getProductVarietyMaster().getName() + ", ID ->" + order.getId());
					}
				hsnCode = selectedProductVarietyMaster.getHsnCode();
				uom = selectedProductVarietyMaster.getUomMaster().getId();

				log.info("Edit Method Done Size is " + stockTransferItemsInwardUpdateList.size());
			}
			selectedTransportTypeMaster = selectedStockTransferPNS.getTransportMaster().getTransportTypeMaster();
			transportTypeMasterListener();
			selectedTransportMaster = selectedStockTransferPNS.getTransportMaster();
			waybillRendered = selectedStockTransferPNS.getWaybillAvailable();
			if (waybillRendered == true) {
				wayBillAvailable = "Yes";
				wayBillNumber = selectedStockTransferPNS.getWaybillNumber();
			} else
				wayBillAvailable = "No";
			if (selectedStockTransferPNS.getTransportChargeAvailable()) {
				transportChargeAvailble = "Yes";
				log.info("Transport Charge Available :" + transportChargeAvailble);
				renderedTransportChargeRendered();

				if (selectedStockTransferPNS.getTransportChargeType().toString().equalsIgnoreCase("Pay")) {
					transportChargeType = "Cash";
					stockTransfer.setTransportChargeType(TransportChargeType.Pay);
				} else {
					transportChargeType = "Cheque";
					stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
				}
				log.info("Transport Charge Type :" + transportChargeType);
				stockTransfer.setTransportChargeAmount(selectedStockTransferPNS.getTransportChargeAmount());
			} else {
				transportChargeAvailble = "No";
				log.info("Transport Charge NOT Available :" + transportChargeAvailble);
			}

			log.info("Edit Method St Done ");
		} catch (Exception se) {
			log.error("Edit Method Error " + se);
		}

	}

	public String delete() {
		log.info("Delete StockTransfer Received " + selectedStockItemInwardPNSDTO.getId());
		try {

			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/deleteStockTransfer";
			log.info("::: Item Delete Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: Retrieved Delete :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Delete StockTransfer Successfully Completed ******");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_DELETE_SUCCESS.getErrorCode());
				renderedEdit = false;
				renderedCreate = true;
				selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
				return INPUT_FORM_LIST_URL;
			} else {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_DELETE_ERROR.getErrorCode());
				log.warn("Delete StockTransfer Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Delete StockTransfer", ee);
		}
		return null;
	}

	public void getProductOrderSelected(StockTransferItems stockTransfer) {
		log.info("Received PurchaseOrderGet ");
		selectedPurchaseOrder = new PurchaseOrder();
		try {
			BaseDTO baseDTO = null;
			log.info("Retrieved PurchaseOrderGet :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getPurchaseOrderGetByID";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, stockTransfer);
			log.info("::: Retrieved PurchaseOrderGet :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("PurchaseOrderGet Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedPurchaseOrder = mapper.readValue(jsonResponse, new TypeReference<PurchaseOrder>() {
				});
			} else {
				log.warn("PurchaseOrderGet Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in PurchaseOrderGet load ", ee);
		}

	}

	public String createNewStockItemInward() {
		try {
			log.info("Received Create ItemInward ");
			init();
			// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_ADD_URL);
			return INPUT_FORM_ADD_URL;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public String backStockItemInward() {
		log.info("Received Back ItemInward ");
		renderedEdit = false;
		renderedCreate = true;
		selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
		return INPUT_FORM_LIST_URL;
	}

	// product category Group ->1
	public void productCategoryGroupList() {
		log.info("Received productCategoryGroupList ");
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryGroupList";
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productCategoryGroupList :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("productCategoryGroupList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();

				log.warn("productCategoryGroupList Successfully Converted ******");
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
			} else {
				log.warn("productCategoryGroupList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryGroupList load ", ee);
		}
	}

	// product category -> 2
	public void productCategoryListLoad() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
		log.info("Received productCategoryGroupList ID :" + selectedProductCategoryGroup.getId());
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryList/" + selectedProductCategoryGroup.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productCategoryList :");
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
			} else {
				log.warn("productCategoryList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	// product category -> 3
	public void getAllProductGroupMasterList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/"
					+ selectedProductCategory.getProductCategoryGroup().getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productGroupMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
			} else {
				log.warn("productGroupMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productGroupMasterList load ", ee);
		}
	}

	// product category -> 4
	public void productVarietyMasterList() {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductVarietyMasterList/" + selectedProductGroupMaster.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
	}

}
