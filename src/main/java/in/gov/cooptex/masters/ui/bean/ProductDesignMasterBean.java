package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.ProductDesignDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("productDesignMasterBean")
@Scope("session")
public class ProductDesignMasterBean implements Serializable {

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	ProductDesignDTO productDesignDTO, selectedProductDesignMaster;

	boolean forceLogin = false;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableDatatable = true;

	@Setter
	@Getter
	LazyDataModel<ProductDesignDTO> lazyproductDesignList;

	@Autowired
	CommonDataService commonDataService;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	boolean saveButton = true;

	@Getter
	@Setter
	boolean updateButton = false;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<ProductCategory> listOFProductCategorylist = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	List<ProductGroupMaster> listOfProductGroupMasterByProductCategoryId = new ArrayList<ProductGroupMaster>();

	@Getter
	@Setter
	List<ProductVarietyMaster> listOfProductVarietyMasterByProductGroupMasterId = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<ProductDesignDTO> listOfProductDesignMaster = new ArrayList<ProductDesignDTO>();

	@Getter
	@Setter
	List<ProductDesignDTO> listOfProductDesignDTO = new ArrayList<ProductDesignDTO>();
	
	@Getter
	@Setter
	List<ProductVarietyMaster> productVariatyAutoSearchList=new ArrayList<>();
	
	@Getter
	@Setter
	List<ProductVarietyMaster> productAutoSearchList=new ArrayList<>();
	
	@Getter
	@Setter
	ProductVarietyMaster selectedAutoProductVarity;
	
	

	private String serverURL = null;

	final String ECOMM_PRODUCT_CONFIG_URL = AppUtil.getPortalServerURL() + "/ecommProductConfig";

	private final String CREATE_ProDuctDesign_PAGE = "/pages/masters/productDesign/createProductDesign.xhtml?faces-redirect=true;";

	private final String View_ProDuctDesign_PAGE = "/pages/masters/productDesign/viewProductDesign.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_LIST_URL = "/pages/masters/productDesign/listProductDesign.xhtml?faces-redirect=true;";

	public ProductDesignMasterBean() {
		log.info("Inside ProductDesignMasterBean()");
	}

	@PostConstruct
	public void init() {
		log.info("the post construct method of ProductDesignMasterBean is get Started");
		log.info("Inside init()");
		productDesignDTO = new ProductDesignDTO();
		loadValues();
		loadValuesForCreate();
		selectedAutoProductVarity=new ProductVarietyMaster();
		loadLazyProductDesignList();

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			productDesignMaster = new ProductDesignMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	private void loadValuesForCreate() {

		log.info("Inside loadValuesForCreate()");
		productDesignMaster = new ProductDesignMaster();
		listOFProductCategorylist = commonDataService.loadProductCategories();
		setListOfProductVarietyMasterByProductGroupMasterId(new ArrayList<>());
		setListOfProductGroupMasterByProductCategoryId(new ArrayList<>());

	}

	public void onProductCategoryChange() {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		if (productDesignDTO.getProductCategory() != null) {

			Long id = productDesignDTO.getProductCategory().getId();
			log.info("the selected the product category Id is" + id);
			productGroupList(id);
		}
	}

	public void productGroupList(Long categoryId) {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductGroup/" + categoryId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfProductGroupMasterByProductCategoryId = mapper.readValue(jsonValue,
						new TypeReference<List<ProductGroupMaster>>() {
						});
				log.info(" listOfProductGroupMasterByProductCategoryId is returned from data base is  "
						+ listOfProductGroupMasterByProductCategoryId);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getGropByCategory() --->", exp);
		}
	}

	public void onProductGroupChange() {
		log.info("EcommProductConfigBean onProductGroupChange------#Start");
		if (productDesignDTO.getProductGroupMaster() != null
				&& productDesignDTO.getProductGroupMaster().getId() != null) {
			Long id = productDesignDTO.getProductGroupMaster().getId();
			log.info("the selected the product group  Id is" + id);
			productVarietyList(id);
		}
	}
	
	//Product design master group change time only get varityMaste by groupId
	public void getProdcutVarByGroup() {
		log.info("EcommProductConfigBean onProductGroupChange------#Start");
		if (productDesignDTO.getProductGroupMaster() != null
				&& productDesignDTO.getProductGroupMaster().getId() != null) {
			Long id = productDesignDTO.getProductGroupMaster().getId();
			log.info("the selected the product group  Id is" + id);
			productVarietyListByGroup(id);
		}
	}

	public void productVarietyList(Long groupId) {
		log.info("---getProductVarietyList-----");

		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductVariety/" + groupId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfProductVarietyMasterByProductGroupMasterId = mapper.readValue(jsonValue,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				log.info("listOfProductVarietyMasterByProductGroupMasterId is returned from database"
						+ listOfProductVarietyMasterByProductGroupMasterId);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getCategoryVarity() --->", exp);
		}
	}
	
	public void productVarietyListByGroup(Long groupId) {
		log.info("---getProductVarietyList-----");

		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductVarietyByGroup/" + groupId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				listOfProductVarietyMasterByProductGroupMasterId = mapper.readValue(jsonValue,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				log.info("listOfProductVarietyMasterByProductGroupMasterId is returned from database"
						+ listOfProductVarietyMasterByProductGroupMasterId);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getCategoryVarity() --->", exp);
		}
	}
	
	
	

	public String saveProductDesignList() {
		log.info("inside the Save Method");
		log.info("the  listOfProductDesignDTO" + productDesignDTO.toString());

		try {
			if(selectedAutoProductVarity==null ) {
				AppUtil.addError("Product Variety Code / Name  is required");
				return null;
			}

			BaseDTO baseDTO = null;
			String url = serverURL + "/productDesign/saveProductDesignList";
			BaseDTO response = httpService.post(url, productDesignDTO);

			if (response != null && response.getStatusCode() == 0) {
				log.info("ProductDesign saved successfully.........." + action);
				if (action.equalsIgnoreCase("Create"))
					AppUtil.addInfo("ProductDesign Details Added successfully");

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Error while saving  Product Design List" + e);
		}

		log.info("...... Product Design List submit ended ....");

		return INPUT_FORM_LIST_URL;

	}

	public void loadLazyProductDesignList() {
		disableAddButton = false;
		log.info("<===== Starts ProductDesign Bean.loadLazyProductDesignList======>");
		lazyproductDesignList = new LazyDataModel<ProductDesignDTO>() {

			private static final long serialVersionUID = -2007656114950592854L;

			@Override
			public List<ProductDesignDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL + "/productDesign/getProductDesignListByLazyLoad";
					log.info("=====url==== loadLazyProductDesignList " + url);
					BaseDTO response = httpService.post(url, paginationRequest);

					log.info("get ProductDesign list");
					log.info("ProductDesign Total records" + response.getTotalRecords());

					log.info("the object returnend from ProductDesignControler" + response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						listOfProductDesignMaster = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductDesignDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyProductDesignList List ...", e.getCause());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method" + listOfProductDesignMaster);
				log.info("Ends lazy load....");

				return listOfProductDesignMaster;
			}

			@Override
			public Object getRowKey(ProductDesignDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductDesignDTO getRowData(String rowKey) {
				log.info("===rowKey=====ProductDesignDTO====" + rowKey.toString());
				try {
					for (ProductDesignDTO productDesignDTO : listOfProductDesignMaster) {
						if (productDesignDTO.getId().equals(Long.valueOf(rowKey))) {
							selectedProductDesignMaster = productDesignDTO;
							return productDesignDTO;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};

	}

	public String goToProductDesignCreatePage() {

		log.info("goToProductDesignCreatePage Started");
		loadValuesForCreate();
		productDesignDTO = new ProductDesignDTO();
		disableDatatable = true;
		selectedAutoProductVarity=null;
		log.info("goToProductDesignCreatePage Ended");
		return CREATE_ProDuctDesign_PAGE;
	}

	public String editProductDesignMaster() throws IOException {

		log.info("<<<<< -------Start editProductDesignMaster called ------->>>>>> ");
		disableDatatable = false;
		updateButton = true;
		saveButton = false;

		if (selectedProductDesignMaster == null) {
			Util.addWarn("Please select one Record");
			return null;
		}
		disableAddButton = false;
		

		String url = serverURL + "/productDesign/get/" + selectedProductDesignMaster.getId();
		log.info("URL-->" + url);
		log.info("selectedProductDesignMaster.getId()-->" + selectedProductDesignMaster.getId());
		BaseDTO response = httpService.get(url);
		if (response != null && response.getStatusCode() == 0) {

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			productDesignDTO = mapper.readValue(jsonResponse, new TypeReference<ProductDesignDTO>() {
			});
			selectedAutoProductVarity=productDesignDTO.getProductVarietyMaster();
			onProductCategoryChange();
			onProductGroupChange();
			listOfProductDesignDTO.clear();
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		return CREATE_ProDuctDesign_PAGE;

	}

	public String gotoProductDesignMasterViewPage() throws IOException {

		log.info("<----Redirecting to user view page---->");
		if (selectedProductDesignMaster == null) {
			Util.addWarn("Please select one Record");
			return null;
		}

		log.info("the selected  ProductDesignMaster is" + productDesignDTO);

		String url = serverURL + "/productDesign/get/" + selectedProductDesignMaster.getId();
		log.info("URL-->" + url);
		log.info("selectedProductDesignMaster.getId()-->" + selectedProductDesignMaster.getId());
		BaseDTO response = httpService.get(url);
		if (response != null && response.getStatusCode() == 0) {

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			productDesignDTO = mapper.readValue(jsonResponse, new TypeReference<ProductDesignDTO>() {
			});

		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		return View_ProDuctDesign_PAGE;

	}

	public String updateProductDesign() {

		log.info("inside the Update Method");

		try {
			
			if(selectedAutoProductVarity==null ) {
				AppUtil.addError("Product Variety Code / Name  is required");
				return null;
			}
			
			log.info("...... ProductDesign update begin ....");
			BaseDTO baseDTO = null;

			log.info(" ProductDesign id" + productDesignDTO.getId());
			String url = serverURL + "/productDesign/updateProductDesign";

			baseDTO = httpService.post(url, productDesignDTO);
			// log.info(" ProductDesign id" + productDesignDTO.getId());
			log.info("return from ProductDesign  controler" + baseDTO.toString());

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {

					log.info("ProductDesign updated successfully");
					AppUtil.addInfo("ProductDesign Details Updated successfully");

					// errorMap.notify(ErrorDescription.SUPPLIER_MASTER_UPDATED.getCode());
					// showList();
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return CREATE_ProDuctDesign_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("Error while update ProductDesign" + e);
		}

		return INPUT_FORM_LIST_URL;

	}

	public String deleteProductDesign() {
		try {

			if (selectedProductDesignMaster == null) {
				Util.addWarn("Please select one Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProductDesignDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}

	public String deleteProductDesignConfirm() {

		try {
			log.info("Delete ProductDesignConfirm started the id is [" + selectedProductDesignMaster.getId() + "]");
			String url = serverURL + "/productDesign/delete/" + selectedProductDesignMaster.getId();
			log.info("URL-->" + url);
			log.info("selectedProductDesignMaster.getId()-->" + selectedProductDesignMaster.getId());

			BaseDTO response = httpService.delete(url);

			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("ProductDesign Details Deleted successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProductDesignDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete ProductDesignMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ProductDesignMasterBean.deleteProductDesignConfirm ===========>");
		selectedProductDesignMaster = new ProductDesignDTO();
		return INPUT_FORM_LIST_URL;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String goToListPageOfProductDesignMaster() {
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		productDesignMaster = new ProductDesignMaster();
		selectedProductDesignMaster = new ProductDesignDTO();
		loadLazyProductDesignList();
		return INPUT_FORM_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");

		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;

		selectedProductDesignMaster = ((ProductDesignDTO) event.getObject());
		disableAddButton = true;
	}

	public String clear() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;

		log.info("<---- Start productDesign.clear ----->");

		selectedProductDesignMaster = null;
		loadLazyProductDesignList();
		log.info("<---- End productDesign.clear ----->");

		return INPUT_FORM_LIST_URL;

	}
	
	public List<ProductVarietyMaster> productVarityAutocomplete(String query) {
		log.info("Institute Autocomplete query==>" + query);
		List<String> results = new ArrayList<String>();
		Long id=null;
		if (productDesignDTO.getProductGroupMaster() != null
				&& productDesignDTO.getProductGroupMaster().getId() != null) {
			 id = productDesignDTO.getProductGroupMaster().getId();
			log.info("the selected the product group  Id is" + id);
		}
		else {
			AppUtil.addWarn("Select product group master");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		productVariatyAutoSearchList = commonDataService.loadProductVarietyListByAutoComplete(serverURL,id,query);
		
		return productVariatyAutoSearchList;
	}
	
	public void selectAutoSearch() {
		log.info("<<=== inside selectAutoSearch");
		if(productVariatyAutoSearchList.contains(selectedAutoProductVarity)) {
			log.info("<<=== selected variety", selectedAutoProductVarity);
			productDesignDTO.setProductVarietyMaster(selectedAutoProductVarity);
		}
	}
	
}
