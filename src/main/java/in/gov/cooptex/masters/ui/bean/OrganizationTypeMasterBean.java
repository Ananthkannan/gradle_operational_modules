package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.OrganizationTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("organizationTypeMasterBean")
@Scope("session")
@Log4j2
public class OrganizationTypeMasterBean {

	
	final String LIST_PAGE = "/pages/masters/listOrganizationTypeMaster.xhtml?faces-redirect=true";
	
	final String ADD_PAGE = "/pages/masters/createOrganizationTypeMaster.xhtml?faces-redirect=true";
	
	final String VIEW_PAGE = "/pages/masters/viewOrganizationTypeMaster.xhtml?faces-redirect=true";
	
	final String serverURL = AppUtil.getPortalServerURL();
	
	
	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Autowired
	AppPreference appPreference;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	String pageAction;

	@Autowired
	ErrorMap errorMap;
	
	
	@Getter
	@Setter
	OrganizationTypeMaster organizationTypeMaster = new OrganizationTypeMaster() ;

	
	@Getter
	@Setter
	List<OrganizationTypeMaster> organizationTypeMasterList = new ArrayList<OrganizationTypeMaster>();
	
	@Getter
	@Setter
	LazyDataModel<OrganizationTypeMaster> lazyorganizationTypeMasterList;


	@Getter
	@Setter
	private int organizationTypeMasterListSize;
	
	@Getter
	@Setter
	Boolean addButton=true;
	
	@Getter
	@Setter
	int totalRecords = 0;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	String id;
	
	@Getter
	@Setter
	String code;
	
	@Getter
	@Setter
	String name;
	
	@Getter
	@Setter
	String localName;
	
	@Getter
	@Setter
	Boolean activeStatus;
	
	@Getter
	@Setter
	String version;
	
	@Getter
	@Setter
	OrganizationTypeMaster  organizationTypeMasterType,  selectedOrganizationTypeMasterType;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	String action;

	
	public String showOrganizationtypeMasterListPage() {
		log.info("<---------- Loading OrganizationTypeMasterBean showList list page---------->");
		
		addButton=true;
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		organizationTypeMaster = new OrganizationTypeMaster();
		selectedOrganizationTypeMasterType = new OrganizationTypeMaster();
		loadOrganizationtypeMasterList();
	//	getAllCountriesList();
		
		return LIST_PAGE;
	}
	
	public void loadOrganizationtypeMasterList() {
		log.info("<===== Starts OrganizationTypeMasterBean.loadOrganizationtypeMasterList ======>");

		lazyorganizationTypeMasterList = new LazyDataModel<OrganizationTypeMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<OrganizationTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = AppUtil.getPortalServerURL() + "/organizationtypemaster/getallorganizationtypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						organizationTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<OrganizationTypeMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("petitionTypeBean size---------------->" + lazyorganizationTypeMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return organizationTypeMasterList;
			}

			@Override
			public Object getRowKey(OrganizationTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OrganizationTypeMaster getRowData(String rowKey) {
				for (OrganizationTypeMaster organizationType : organizationTypeMasterList) {
					if (organizationType.getId().equals(Long.valueOf(rowKey))) {
						selectedOrganizationTypeMasterType = organizationType;
						return organizationType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  OrganizationTypeMasterBean.loadOrganizationtypeMasterList  ======>");
	}
	
	public String submit() {
		log.info("...... OrganizationTypeMasterBean submit begin ....");
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(AppUtil.getPortalServerURL() + "/organizationtypemaster/create", organizationTypeMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
				
					if (action.equalsIgnoreCase("ADD")) {
						log.info("OrganizationTypeMasterBean saved successfully");
						showOrganizationtypeMasterListPage();
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.ORGANIZATION_TYPE_CREATE_SUCCESSFULLY).getCode());
					}
					if (action.equalsIgnoreCase("EDIT")) {
						log.info("OrganizationTypeMasterBean Updated successfully");
						showOrganizationtypeMasterListPage();
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.ORGANIZATION_TYPE_UPDATED_SUCCESSFULLY).getCode());
					}
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating OrganizationTypeMaster" + e);
		}
		log.info("...... OrganizationTypeMasterBean submit ended ....");
		return LIST_PAGE;
	}
	
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts OrganizationTypeMasterBean.onRowSelect ========>" + event);
		selectedOrganizationTypeMasterType = ((OrganizationTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends OrganizationTypeMasterBean.onRowSelect ========>" + selectedOrganizationTypeMasterType.getId());
	}
	
	
	public String backPage() {

		showOrganizationtypeMasterListPage();
		addButton=true;
		return LIST_PAGE;
	}
	
	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		organizationTypeMaster = new OrganizationTypeMaster();
		addButton=true;
		return LIST_PAGE;

	}
	
	public String showOrganizationTypeMasterAction() {
		log.info("<===== Starts OrganizationTypeMasterBean.showOrganizationTypeMasterAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("ADD")
					&& (selectedOrganizationTypeMasterType == null || selectedOrganizationTypeMasterType.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create OrganizationTypeMasterBean called..");
				organizationTypeMasterType = new OrganizationTypeMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit OrganizationTypeMasterBean called..");
				String url = AppUtil.getPortalServerURL() + "/organizationtypemaster/get/"
						+ selectedOrganizationTypeMasterType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					organizationTypeMaster = mapper.readValue(jsonResponse, new TypeReference<OrganizationTypeMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View OrganizationTypeMasterBean called..");

				String url = AppUtil.getPortalServerURL() +  "/organizationtypemaster/get/"
						+ selectedOrganizationTypeMasterType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						organizationTypeMaster = mapper.readValue(jsonResponse, new TypeReference<OrganizationTypeMaster>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete petitionType called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmorganizationTypeMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in OrganizationTypeMasterBean.showOrganizationTypeMasterAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends OrganizationTypeMasterBean.showOrganizationTypeMasterAction ===========>" + action);
		return null;
	}
	
	public String deleteConfirm() {
		log.info("<===== Starts OrganizationTypeMasterBean.deleteConfirm ===========>" + selectedOrganizationTypeMasterType.getId());
		try {
			BaseDTO response = httpService.delete(AppUtil.getPortalServerURL() +  "/organizationtypemaster/deletebyid/"
					 + selectedOrganizationTypeMasterType.getId());
		
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.ORGANIZATION_TYPE__DELETE_SUCCESSFULLY).getCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmorganizationTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete OrganizationType....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends OrganizationTypeMasterBean.deleteConfirm ===========>");
		return showOrganizationtypeMasterListPage();
	}
	
}
