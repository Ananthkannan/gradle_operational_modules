package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.OutgoingTapal;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.LoomTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.LoomTypeMasterDTO;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("loomTypeMasterBean")
@Scope("session")
public class LoomTypeMasterBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4137008418779092082L;
	final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LOOMTYPE_CREATE_PAGE = "/pages/masters/createLoomType.xhtml?faces-redirect=true;";
	private final String LOOMTYPE_LIST_PAGE = "/pages/masters/listLoomType.xhtml?faces-redirect=true;";
	private final String LOOMTYPE_VIEW_PAGE = "/pages/masters/viewLoomType.xhtml?faces-redirect=true;";
	
	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	LoomTypeMaster loomTypeMaster;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;
	
	@Getter @Setter
	LazyDataModel<LoomTypeMaster> lazyLoomTypeList;
	
	@Getter @Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter @Setter
	String action;

	public String submit() {
		try {
			if(saveValidation(true)) {
				String url = SERVER_URL +"/loomtype/saveLoomTypeMaster";
				BaseDTO response = httpService.post(url, loomTypeMaster);
				log.info(response.getStatusCode());
				if (response != null && response.getStatusCode() == 0) {
					if("ADD".equalsIgnoreCase(action)) {
						errorMap.notify(ErrorDescription.LOOM_TYPEMASTER_SAVED_SUCCESSFULLY.getErrorCode());
					}else if("EDIT".equalsIgnoreCase(action)) {
						errorMap.notify(ErrorDescription.LOOM_TYPEMASTER_EDIT_SUCCESSFULLY.getErrorCode());
					}
					loomTypeMaster=null;
					editButtonFlag = true;
					deleteButtonFlag = true;
					addButtonFlag = false;
					return LOOMTYPE_LIST_PAGE;
				}else if(response.getStatusCode()==ErrorDescription.LOOM_TYPEMASTER_ALREADY_EXISTS.getErrorCode()) {
					errorMap.notify(ErrorDescription.LOOM_TYPEMASTER_ALREADY_EXISTS.getErrorCode());
				}else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error load submit function");
		}
		return null;
	}

	private boolean saveValidation(boolean valid) {
		valid=true;
		FacesContext fc=FacesContext.getCurrentInstance();
		if(StringUtils.isEmpty(loomTypeMaster.getCode())) {
			fc.addMessage("loomcode", new FacesMessage(ErrorCodeDescription.getDescription(35025)));
			valid=false;
		}
		if(StringUtils.isEmpty(loomTypeMaster.getName())) {
			fc.addMessage("loomname", new FacesMessage(ErrorCodeDescription.getDescription(35026)));
			valid=false;
		}
		if(StringUtils.isEmpty(loomTypeMaster.getLname())) {
			fc.addMessage("loomlname", new FacesMessage(ErrorCodeDescription.getDescription(35027)));
			valid=false;
		}
		if(loomTypeMaster.getActiveStatus()==null) {
			fc.addMessage("status", new FacesMessage(ErrorCodeDescription.getDescription(35028)));
			valid=false;
		}
		return valid;
	}

	public String loadLoomList() {
		try {
			action="";
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = false;
			loomTypeMaster=new LoomTypeMaster();
			loadLazyLoomTypeList();
		} catch (Exception e) {
			log.error("Error loadLoomList", e);
		}
		return LOOMTYPE_LIST_PAGE;
	}

	private void loadLazyLoomTypeList() {
		lazyLoomTypeList=new LazyDataModel<LoomTypeMaster>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6031193958171002989L;
			
			@Override

			public List<LoomTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url="";
				List<LoomTypeMaster> loomTypeList=null;
				try {
					loomTypeList=new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url=SERVER_URL+"/loomtype/getAllActiveLoomType";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						mapper=new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						loomTypeList = mapper.readValue(jsonResponse, new TypeReference<List<LoomTypeMaster>>() {
						});
						log.info("list----------"+loomTypeList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = loomTypeList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyIncomingTapal List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return loomTypeList;
			}

			@Override
			public Object getRowKey(LoomTypeMaster res) {
				return res != null ? res.getId() : null;
			}
			@Override
			public LoomTypeMaster getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<LoomTypeMaster> responseList = (List<LoomTypeMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (LoomTypeMaster obj : responseList) {
					if (obj.getId().longValue() == value.longValue()) {
						loomTypeMaster = obj;
						return obj;
					}
				}
				return null;
			}
		};
	}
	
	public String createLoomType() {
		loomTypeMaster=new LoomTypeMaster();
		return LOOMTYPE_CREATE_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		loomTypeMaster=(LoomTypeMaster)event.getObject();
	}
	
	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		loomTypeMaster=null;
		loadLazyLoomTypeList();
		return LOOMTYPE_LIST_PAGE;
	}

	public String loomView() {
		try {
			if("EDIT".equalsIgnoreCase(action)) {
				return LOOMTYPE_CREATE_PAGE;
			}
			else if("VIEW".equalsIgnoreCase(action)) {
				return LOOMTYPE_VIEW_PAGE;
			}
		}catch (Exception e) {
			log.error("inside exception view method",e);
		}
		return null;
	}
	
	public void loomDelete() {
		String url="";
		try {
			BaseDTO baseDTO=new BaseDTO();
			url=SERVER_URL+"/loomtype/loomDelete/"+loomTypeMaster.getId();
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				errorMap.notify(ErrorDescription.LOOM_TYPEMASTER_DELETE_SUCCESSFULLY.getErrorCode());
				action="";
				editButtonFlag = true;
				deleteButtonFlag = true;
				addButtonFlag = false;
				loomTypeMaster=null;
				loadLazyLoomTypeList();
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		}catch (Exception e) {
			log.info("inside delete method exception----",e);
		}
	}
}
