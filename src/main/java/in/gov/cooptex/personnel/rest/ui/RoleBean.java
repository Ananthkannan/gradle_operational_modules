 package in.gov.cooptex.personnel.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.AppFeatureDto;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.RoleDTO;
import in.gov.cooptex.core.model.AppFeature;
import in.gov.cooptex.core.model.Application;
import in.gov.cooptex.core.model.RoleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
@Scope("session")
public class RoleBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<RoleMaster> roleMasterList;

	@Getter
	@Setter
	int roleMasterListSize;

	@Getter
	@Setter
	String ADD_FORM_URL = "/pages/masters/createRole.xhtml?faces-redirect=true";

	@Getter
	@Setter
	RoleMaster selectedRole;

	@Getter
	@Setter
	String LIST_FORM_URL = "/pages/masters/listRole.xhtml?faces-redirect=true";

	@Getter
	@Setter
	List<AppFeatureDto> featureList = new ArrayList<>();

	@Getter
	@Setter
	private TreeNode root;

	@Getter
	@Setter
	private List<TreeNode> treeNodes = new ArrayList<TreeNode>();

	@Getter
	@Setter
	private TreeNode[] selectedNodes;

	@Getter
	@Setter
	private List<Application> applicationList = new ArrayList<>();

	@Getter
	@Setter
	RoleMaster role = new RoleMaster();

	@Getter
	@Setter
	List<AppFeature> features = new ArrayList<>();

	@Getter
	@Setter
	List<Long> featureListId = new ArrayList<>();

	@Getter
	@Setter
	RoleDTO roleDto = new RoleDTO();

	@Getter
	@Setter
	private Map<String, Boolean> leaf = new HashMap<String, Boolean>();

	@PostConstruct
	public void init() {
		log.info("<------Role Management Initialized---->");

		loadValues();

	}

	public String loadRoleListPage() {
		log.info("Loading role list page");
		loadValues();
		return "/pages/masters/listRole.xhtml?faces-redirect=true;";
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
		}
		loadAllRoleList();
	}

	public void deleteRole() {

		if (selectedRole == null) {
			Util.addWarn("Please Select One Role");
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmRoleDelete').show();");
		}
	}

	public String clear() {
		loadAllRoleList();
		log.info("clear called");
		return "/pages/masters/listRole.xhtml?faces-redirect=true";
	}

	public String deleteRoleConfirm() {
		log.info("<---------Role delte confirmed-------->");
		try {
			RoleMaster roleMaster = new RoleMaster();
			roleMaster.setId(selectedRole.getId());
			BaseDTO response = new BaseDTO();
			String url = serverURL + "/role/delete";
			response = httpService.post(url, roleMaster);
			if (response != null) {
				if (response.getStatusCode() == 0) {
					log.info("User deleted successfully....!!!");
					Util.addInfo("Role deleted successfully");
					loadAllRoleList();
					selectedRole = new RoleMaster();
				} else {
					log.info("Error occured in Rest api ... ");
					Util.addError("Error");
				}

			}
		} catch (Exception e) {
			log.error("Exception Occured while Deleting role .. ", e);
		}
		return null;
	}

	public void loadAllRoleList() {
		log.info("<---------Loading Role List.....--------->");
		try {
			// roleMasterList = new ArrayList<RoleMaster>();
			selectedRole = new RoleMaster();
			BaseDTO response = new BaseDTO();
			String url = serverURL + "/role/getall";
			response = httpService.get(url);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				roleMasterList = mapper.readValue(jsonResponse, new TypeReference<List<RoleMaster>>() {
				});
				if (roleMasterList != null) {
					log.info("<---------Role master list response content size = " + roleMasterList.size()
							+ "--------->");
					roleMasterListSize = roleMasterList.size();
				} else {
					log.info("<---------Role master list response content is empty-------->");
					roleMasterListSize = 0;
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while loadAllroleList ... ", e);
		}
	}

	public String addRole() {

		log.info("<<<<<<=======Start RoleBean.addROle called========>>>>");
		action = "ADD";
		role = new RoleMaster();
		roleDto = new RoleDTO();
		featureListId = new ArrayList<>();
		log.info("<<<<<<=======End RoleBean.addROle called========>>>>");
		getAllFeatrues();
		getAllApplicationType();
		root = new DefaultTreeNode("Root", null);

		treeNodes = new ArrayList<TreeNode>(); // used in edit

		for (AppFeatureDto uf : featureList) {
			if (uf.getParent() == null) {
				log.info("uf" + uf);
				addFeature(uf, root, false);
			}
		}

		selectedNodes = treeNodes.toArray(new TreeNode[0]);
		return ADD_FORM_URL;

	}

	public void getAllFeatrues() {
		log.info("<<<<=======Start ROleBean.getAllFeatrues ============>>>>>>>");
		BaseDTO baseDTO = new BaseDTO();
		log.info("ServerUrl -----" + serverURL + "/appfeature/getall");
		String url = serverURL + "/appfeature/getall";
		baseDTO = httpService.get(url);
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			try {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				featureList = mapper.readValue(jsonResponse, new TypeReference<List<AppFeatureDto>>() {
				});

			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		log.info("List of features=====>>>..");
		log.info("<<<<=======End ROleBean.getAllFeatrues ============>>>>>>>");
	}

	public void getAllApplicationType() {
		log.info("<<<<=======Start ROleBean.getAllApplicationType ============>>>>>>>");
		BaseDTO baseDTO = new BaseDTO();
		String url = serverURL + "/application/getall";
		baseDTO = httpService.get(url);
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			try {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				applicationList = mapper.readValue(jsonResponse, new TypeReference<List<Application>>() {
				});

			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		log.info("List of features=====>>>.." + applicationList);
		log.info("<<<<=======End ROleBean.getAllApplicationType ============>>>>>>>");
	}

	public void addFeature(AppFeatureDto parent, TreeNode root, boolean isexpandable) {

		TreeNode currNode = new DefaultTreeNode(parent, root);
		currNode.setSelectable(true);
		currNode.setExpanded(isexpandable);
		// only edit starts
		if (action.equals("EDIT") || action.equals("VIEW")) {

			currNode.setSelectable(false);

			// List<AppFeatureDto> selectedFeatures=roleDto.getFeatureList();
			// for (AppFeatureDto f : featureList) {
			// if (f.getId() == parent.getId()) {
			//
			// if (parent.getFeatures() == null
			// || parent.getFeatures().isEmpty()) {
			// log.info("------------zzzzzzzz----------");
			// if(selectedFeatures.contains(parent)){
			// currNode.setSelected(true);
			// parent.setIsAdd(true);
			// parent.setIsEdit(true);
			// parent.setIsDelete(true);
			// parent.setIsView(true);
			// }
			// treeNodes.add(currNode);
			// }
			// }
			// }
			// only edit end
		}
		// treeNodes.add(currNode);
		if (parent.getFeatures() != null) {
			for (AppFeatureDto appFeatureDto : parent.getFeatures()) {
				AppFeatureDto appFeatureDto2 = appFeatureDto;
				/*
				 * if(appFeatureDto.getFeatures()!=null) for(AppFeatureDto
				 * dto:appFeatureDto.getFeatures()){ if(appFeatureDto.equals(dto)){
				 * appFeatureDto2=dto; break; } }
				 */
				// log.info("child feature======>>>>>>>>"+appFeatureDto2.getFeatureName()+" is
				// ::::"+appFeatureDto2.getAction());
				if (appFeatureDto2.getAction().equals("top") || appFeatureDto2.getAction().equals("stmenu")) {
					// log.info("feature value::::::" + appFeatureDto2.getFeatureName());
					addFeature(appFeatureDto2, currNode, false);
				}

			}
		}

	}

	public void setParentStatus(TreeNode tn) {
		log.info("<<<<<<<<<========setParentStatus submit ===== >>>>>>>>>>");
		tn.setSelected(true);
		if (featureList.contains((AppFeatureDto) tn.getData())) {
			log.info("already contains");
		} else {
			featureList.add((AppFeatureDto) tn.getData());
		}
		if (tn.getParent() != null && tn.getParent().getParent() != null)
			setParentStatus(tn.getParent());
	}

	public void selectedAddFeature(String actionKey, Long subfeatureId, Boolean isAdd, Long featureId,
			AppFeatureDto node) {
		log.info("Action:::::" + actionKey + "subfeatureId:::::" + subfeatureId + "isAdd:::::" + isAdd + "featureId::::"
				+ featureId);
		log.info("feature list in add   " + featureListId);
		if (isAdd) {

			if (actionKey.equals("top") && featureId != null) {
				featureListId.add(featureId);
				onSelectOrUnselectParentAddFeatures(node, isAdd);
			} else if (subfeatureId != null) {
				featureListId.add(subfeatureId);
				featureListId.add(node.getId());
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			// log.info("Checkbox selected========="+featureListId);
		} else {
			if (actionKey.equals("top") && featureId != null) {
				if (!action.equals("EDIT"))
					featureListId.remove(featureId);
				onSelectOrUnselectParentAddFeatures(node, isAdd);
			} else if (subfeatureId != null)
				featureListId.removeAll(Collections.singleton(subfeatureId));
			log.info("Checkbox unselected=========");
			// log.info("Checkbox unselected========="+featureListId);
		}
		log.info("Checkbox Final=========" + featureListId);
		RequestContext.getCurrentInstance().update("role:roleFeature");

	}

	/**
	 * @param action
	 * @param subfeatureId
	 * @param isEdit
	 * @param featureId
	 * @param node
	 */
	public void selectedEditFeature(String action, Long subfeatureId, Boolean isEdit, Long featureId,
			AppFeatureDto node) {
		log.info("featureId...." + featureId + "subfeatureId..." + subfeatureId + "featureListId.." + featureListId);
		if (isEdit) {
			if (action.equals("top")) {
				featureListId.add(featureId);
				onSelectOrUnselectParentEditFeatures(node, isEdit);
			} else {
				featureListId.add(subfeatureId);
				featureListId.add(node.getId());
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			log.info("Checkbox selected=========" + featureListId);
		} else {
			if (action.equals("top") && featureId != null) {
				if (!action.equals("EDIT"))
					featureListId.remove(featureId);
				onSelectOrUnselectParentEditFeatures(node, isEdit);
			} else
				featureListId.removeAll(Collections.singleton(subfeatureId));
			log.info("Checkbox unselected=========");
			// log.info("Checkbox unselected========="+featureListId);
		}

	}

	/**
	 * @param action
	 * @param subfeatureId
	 * @param isView
	 * @param featureId
	 * @param node
	 */
	public void selectedViewFeature(String action, Long subfeatureId, Boolean isView, Long featureId,
			AppFeatureDto node) {
		log.info("Features before selection/unselected=========" + featureListId);
		if (isView) {

			if (action.equals("top")) {
				featureListId.add(featureId);

				onSelectOrUnselectParentViewFeatures(node, isView);
			} else {

				featureListId.add(subfeatureId);
				featureListId.add(node.getId());
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			log.info("Checkbox selected=========" + featureListId);
		} else {
			if (action.equals("top") && featureId != null) {
				if (!action.equals("EDIT"))
					featureListId.remove(featureId);
				onSelectOrUnselectParentViewFeatures(node, isView);
			} else
				featureListId.removeAll(Collections.singleton(subfeatureId));

			log.info("Checkbox unselected=========" + featureListId);
		}

	}

	public void checkParentExists(AppFeatureDto feature) {
		featureListId.add(feature.getId());
		if (feature.getParent() != null)
			checkParentExists(feature.getParent());
	}

	public void selectedApproveFeature(String action, Long subfeatureId, Boolean isApprove, Long featureId,
			AppFeatureDto node) {
		if (isApprove) {

			if (action.equals("top")) {
				featureListId.add(featureId);
				onSelectOrUnselectParentApproveFeatures(node, isApprove);
			} else {
				featureListId.add(subfeatureId);
				featureListId.add(node.getId());
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			log.info("Checkbox selected=========" + featureListId);
		} else {
			if (action.equals("top")) {
				featureListId.removeAll(Collections.singleton(featureId));
				onSelectOrUnselectParentApproveFeatures(node, isApprove);
			} else {
				for (Iterator<Long> iter = featureListId.listIterator(); iter.hasNext();) {
					Long id = iter.next();
					if (subfeatureId.equals(id)) {
						iter.remove();
					}
				}
				// featureListId.remove(subfeatureId);
			}
			log.info("Checkbox unselected=========");
			log.info("Checkbox unselected=========" + featureListId);
		}

	}

	public void selectedRejectedFeature(String action, Long subfeatureId, Boolean isRejected, Long featureId,
			AppFeatureDto node) {
		if (isRejected) {

			if (action.equals("top")) {
				featureListId.add(featureId);
				onSelectOrUnselectParentRejectFeatures(node, isRejected);
			} else {
				featureListId.add(subfeatureId);
				featureListId.add(node.getId());
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			log.info("Checkbox selected=========" + featureListId);
		} else {
			if (action.equals("top")) {
				featureListId.removeAll(Collections.singleton(featureId));
				onSelectOrUnselectParentRejectFeatures(node, isRejected);
			} else {
				for (Iterator<Long> iter = featureListId.listIterator(); iter.hasNext();) {
					Long id = iter.next();
					if (subfeatureId.equals(id)) {
						iter.remove();
					}
				}
				// featureListId.remove(subfeatureId);
			}

			log.info("Checkbox unselected=========");
			log.info("Checkbox unselected=========" + featureListId);
		}

	}

	public String cancel() {
		loadAllRoleList();
		return LIST_FORM_URL;
	}

	public void selectedDeleteFeature(String action, Long subfeatureId, Boolean isDelete, Long featureId,
			AppFeatureDto node) {
		if (isDelete) {

			if (action.equals("top")) {
				featureListId.add(featureId);
				onSelectOrUnselectParentDeleteFeatures(node, isDelete);
			} else {
				featureListId.add(subfeatureId);
				if (node.getParent() != null)
					checkParentExists(node.getParent());
			}
			log.info("Checkbox selected=========");
			log.info("Checkbox selected=========" + featureListId);
		} else {
			if (action.equals("top")) {
				onSelectOrUnselectParentDeleteFeatures(node, isDelete);
				if (!action.equals("EDIT"))
					featureListId.removeAll(Collections.singleton(featureId));
			} else
				featureListId.removeAll(Collections.singleton(subfeatureId));
			log.info("Checkbox unselected=========");
			log.info("Checkbox unselected=========" + featureListId);
		}

	}

	public String submit() {
		log.info("<<<<<<<<<========Start Role submit ===== >>>>>>>>>>");
		try {
			BaseDTO baseDTO = null;
			log.info("Add::::::" + action);
			if (action.equals("ADD")) {
				if (roleDto == null) {
					Util.addWarn("Role is empty");
					return null;
				}
				if (roleDto.getRoleName() == null || roleDto.getRoleName().isEmpty()) {
					Util.addWarn("Role name is required");
					return null;
				}
				if (roleDto.getApplication() == null) {
					log.info("Application:::::" + roleDto.getApplication());
					Util.addWarn("Application is required");
					return null;
				}
				if (featureListId == null || featureListId.size() == 0) {
					Util.addWarn("Features is mandatory");
					return null;
				}

				for (TreeNode tn : selectedNodes) {
					if (tn.isLeaf() && tn.isSelected()) {

						setParentStatus(tn);

					}
				}
				List<Long> featureList = featureListId.stream().distinct().collect(Collectors.toList());
				log.info("selected features::::::" + featureListId);
				UserMaster createdBy = loginBean.getUserDetailSession();
				roleDto.setCreatedBy(createdBy.getId());
				roleDto.setFeaturesList(featureList);
				String url = serverURL + "/role/add";
				baseDTO = httpService.post(url, roleDto);

			} else {
				if (roleDto == null) {
					Util.addWarn("Role is empty");
					return null;
				}
				if (roleDto.getRoleName() == null || roleDto.getRoleName().isEmpty()) {
					Util.addWarn("Role name is required");
					return null;
				}
				if (roleDto.getApplication() == null || roleDto.getApplication().getApplicationName().isEmpty()) {
					Util.addWarn("Application is required");
					return null;
				}
				if (featureListId == null || featureListId.size() == 0) {
					Util.addWarn("Features is mandatory");
					return null;
				}

				/*
				 * for (TreeNode tn : selectedNodes) { if (tn.isLeaf()) { if (tn.isSelected()) {
				 * setParentStatus(tn); } } }
				 */

				log.info("selected features::::::" + featureListId);
				List<Long> featureList = featureListId.stream().distinct().collect(Collectors.toList());
				log.info("selected features removing dups::::::" + featureList);
				UserMaster modifiedBy = loginBean.getUserDetailSession();
				roleDto.setId(selectedRole.getId());
				roleDto.setModifiedBy(modifiedBy.getId());
				roleDto.setFeaturesList(featureList);
				String url = serverURL + "/role/update";
				baseDTO = httpService.put(url, roleDto);
			}

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1022) {
					Util.addInfo("Role Added successfully");
				}
				if (baseDTO.getStatusCode() == 1023) {
					Util.addInfo("Role updated successfully");
				}

				if (baseDTO.getStatusCode() == 1021) {
					log.info("<<<< --- error code--->>>" + 1021);
					Util.addWarn("Duplicate Role name");
					return null;
				}
			}
			loadAllRoleList();
		} catch (Exception e) {
			log.info("Exception occured while submitting role....... ", e);
		}

		log.info("<<<<<<<<<========End Role submit ===== >>>>>>>>>>");
		log.info("Redirecting to Role List Page ..... ");
		return LIST_FORM_URL;
	}

	public String editRole() {
		featureListId = new ArrayList<>();
		log.info("<---- Start RoleBean . editForm  ---->");
		if (selectedRole == null) {
			log.info("Selected role Emtpy");
			Util.addWarn("Please Select One Role");
			return null;
		}
		action = "EDIT";
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/role/get";
			baseDTO = httpService.post(url, selectedRole);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;

			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			roleDto = mapper.readValue(jsonResponse, new TypeReference<RoleDTO>() {
			});
			getAllApplicationType();
			getAllFeatrues();
			root = new DefaultTreeNode("Root", null);
			List<Long> selectedFeatures = roleDto.getFeaturesList();
			treeNodes = new ArrayList<TreeNode>();
			log.info("selected role features:::" + selectedFeatures);
			List<AppFeatureDto> tempList = new ArrayList<>();
			for (AppFeatureDto uf : featureList) {
				if (uf.getParent() == null) {
					if (selectedFeatures.contains(uf.getId())) {
						featureListId.add(uf.getId());

					}
					List<AppFeatureDto> appList1 = new ArrayList<>();
					List<AppFeatureDto> appList2 = new ArrayList<>();
					List<AppFeatureDto> appList3 = new ArrayList<>();
					log.info("Top Level Feature:::::" + uf.getFeatureName());
					if (uf.getFeatures() != null)
						for (AppFeatureDto app : uf.getFeatures()) {
							log.info("Second level menu:::::::::" + app.getFeatureName());

							// if(app.getAction().equals("top"))
							// {
							log.info("Matched::::::::::::::::::::::;");

							if (selectedFeatures.contains(app.getId())) {
								featureListId.add(app.getId());
							}
							appList2 = new ArrayList<>();
							appList3 = new ArrayList<>();
							// }
							// if(app.getFeatures()!=null)
							// log.info("size not null :::::"+app.getFeatures().size() +"feature name"+
							// app.getFeatureName());
							if (app.getFeatures() != null) {
								Boolean selectAllFeat = true;

								for (AppFeatureDto a : app.getFeatures()) {

									// log.info("Second for loop feature name::::"+a.getFeatureName()+"action
									// :;;"+a.getAction()+":::::::::"+a.getId());
									log.info("Third for loop feature name::::" + a.getFeatureName() + "action :;;"
											+ a.getAction() + ":::::::::" + a.getId());
									appList3 = new ArrayList<>();
									if (selectedFeatures.contains(a.getId())) {
										featureListId.add(a.getId());
									}
									Boolean selectAll = true;
									if (a.getFeatures() != null)
										for (AppFeatureDto ab : a.getFeatures()) {
											List<AppFeatureDto> appList4 = new ArrayList<>();
											if (selectedFeatures.contains(ab.getId())) {
												featureListId.add(ab.getId());
												log.info("Fourth for loop feature name::::" + ab.getFeatureName()
														+ "action :;;" + ab.getAction() + ":::::::::" + ab.getId());
												if (ab.getAction().equals("Add")) {
													// log.info("Features Id matched::::::::a:::::::::::::::Action
													// Add");
													app.setIsAdd(true);
													uf.setIsAdd(true);
													ab.setIsAdd(true);
													a.setIsAdd(true);

													featureListId.add(ab.getId());
												} else if (ab.getAction().equals("Edit")) {
													app.setIsEdit(true);
													uf.setIsEdit(true);
													ab.setIsEdit(true);
													a.setIsEdit(true);
													featureListId.add(ab.getId());
												} else if (ab.getAction().equals("View")) {
													app.setIsView(true);
													uf.setIsView(true);
													ab.setIsView(true);
													a.setIsView(true);
													featureListId.add(ab.getId());
												} else if (ab.getAction().equals("Delete")) {
													app.setIsDelete(true);
													uf.setIsDelete(true);
													ab.setIsDelete(true);

													a.setIsDelete(true);
													featureListId.add(ab.getId());
												} else if (ab.getAction() != null && ab.getAction().equals("Approve")) {
													app.setIsApproved(true);
													uf.setIsApproved(true);
													ab.setIsApproved(true);

													a.setIsApproved(true);
													featureListId.add(ab.getId());
												} else if (ab.getAction() != null && ab.getAction().equals("Reject")) {
													app.setIsRejected(true);
													uf.setIsRejected(true);
													ab.setIsRejected(true);

													a.setIsRejected(true);
													featureListId.add(ab.getId());
												}
												// log.info("ab:::::::::::::::"+ab.getIsAdd()+"::::::;id::::"+ab.getId());
											} else {
												selectAll = false;
											}
											if (selectAll) {
												ab.setSelectAll(true);
												a.setSelectAll(true);
												app.setSelectAll(true);
												uf.setSelectAll(true);
											}
											appList4 = new ArrayList<>();
											for (AppFeatureDto c : ab.getFeatures()) {
												if (selectedFeatures.contains(c.getId())) {
													featureListId.add(c.getId());
													log.info("Fourth for loop feature name::::" + ab.getFeatureName()
															+ "action :;;" + ab.getAction() + ":::::::::" + ab.getId());
													if (c.getAction().equals("Add")) {
														// log.info("Features Id matched::::::::a:::::::::::::::Action
														// Add");
														app.setIsAdd(true);
														uf.setIsAdd(true);
														ab.setIsAdd(true);
														a.setIsAdd(true);
														c.setIsAdd(true);

														featureListId.add(c.getId());
													} else if (c.getAction().equals("Edit")) {
														app.setIsEdit(true);
														uf.setIsEdit(true);
														ab.setIsEdit(true);
														a.setIsEdit(true);
														c.setIsEdit(true);
														featureListId.add(c.getId());
													} else if (c.getAction().equals("View")) {
														app.setIsView(true);
														uf.setIsView(true);
														ab.setIsView(true);
														a.setIsView(true);
														c.setIsView(true);
														featureListId.add(c.getId());
													} else if (c.getAction().equals("Delete")) {
														app.setIsDelete(true);
														uf.setIsDelete(true);
														ab.setIsDelete(true);

														a.setIsDelete(true);
														c.setIsDelete(true);
														featureListId.add(c.getId());
													} else if (c.getAction() != null
															&& c.getAction().equals("Approve")) {
														app.setIsApproved(true);
														uf.setIsApproved(true);
														ab.setIsApproved(true);

														a.setIsApproved(true);
														c.setIsApproved(true);
														featureListId.add(c.getId());
													} else if (c.getAction() != null
															&& c.getAction().equals("Reject")) {
														app.setIsRejected(true);
														uf.setIsRejected(true);
														ab.setIsRejected(true);

														a.setIsRejected(true);
														c.setIsRejected(true);
														featureListId.add(c.getId());
													}
													// log.info("ab:::::::::::::::"+ab.getIsAdd()+"::::::;id::::"+ab.getId());
												} else {
													selectAll = false;
												}
												if (selectAll) {
													c.setSelectAll(true);

												}
												List<AppFeatureDto> appList5 = new ArrayList<>();
												for (AppFeatureDto app1 : c.getFeatures()) {

													if (selectedFeatures.contains(app1.getId())) {
														featureListId.add(app1.getId());
														log.info("Fourth for loop feature name::::"
																+ app1.getFeatureName() + "action :;;"
																+ app1.getAction() + ":::::::::" + ab.getId());
														if (c.getAction().equals("Add")) {
															// log.info("Features Id
															// matched::::::::a:::::::::::::::Action Add");
															app.setIsAdd(true);
															uf.setIsAdd(true);
															ab.setIsAdd(true);
															a.setIsAdd(true);
															c.setIsAdd(true);
															app1.setIsAdd(true);
															featureListId.add(app1.getId());
														} else if (app1.getAction().equals("Edit")) {
															app.setIsEdit(true);
															uf.setIsEdit(true);
															ab.setIsEdit(true);
															a.setIsEdit(true);
															c.setIsEdit(true);
															app1.setIsEdit(true);
															featureListId.add(app1.getId());
														} else if (app1.getAction().equals("View")) {
															app.setIsView(true);
															uf.setIsView(true);
															ab.setIsView(true);
															a.setIsView(true);
															c.setIsView(true);
															app1.setIsView(true);
															featureListId.add(app1.getId());
														} else if (app1.getAction().equals("Delete")) {
															app.setIsDelete(true);
															uf.setIsDelete(true);
															ab.setIsDelete(true);

															a.setIsDelete(true);
															c.setIsDelete(true);
															app1.setIsDelete(true);
															featureListId.add(app1.getId());
														} else if (app1.getAction() != null
																&& app1.getAction().equals("Approve")) {
															app.setIsApproved(true);
															uf.setIsApproved(true);
															ab.setIsApproved(true);

															a.setIsApproved(true);
															c.setIsApproved(true);
															app1.setIsApproved(true);
															featureListId.add(app1.getId());
														} else if (app1.getAction() != null
																&& app1.getAction().equals("Reject")) {
															app.setIsRejected(true);
															uf.setIsRejected(true);
															ab.setIsRejected(true);

															a.setIsRejected(true);
															c.setIsRejected(true);
															app1.setIsRejected(true);
															featureListId.add(app1.getId());
														}
														// log.info("ab:::::::::::::::"+ab.getIsAdd()+"::::::;id::::"+ab.getId());
													} else {
														selectAll = false;
													}
													if (selectAll) {
														app1.setSelectAll(true);

													}
													List<AppFeatureDto> arrList6 = new ArrayList<>();
													for (AppFeatureDto d : app1.getFeatures()) {
														if (selectedFeatures.contains(d.getId())) {
															featureListId.add(d.getId());
															log.info("Fourth for loop feature name::::"
																	+ d.getFeatureName() + "action :;;" + d.getAction()
																	+ ":::::::::" + ab.getId());
															if (d.getAction().equals("Add")) {
																// log.info("Features Id
																// matched::::::::a:::::::::::::::Action Add");
																app.setIsAdd(true);
																uf.setIsAdd(true);
																ab.setIsAdd(true);
																a.setIsAdd(true);
																c.setIsAdd(true);
																app1.setIsAdd(true);
																d.setIsAdd(true);
																featureListId.add(d.getId());
															} else if (d.getAction().equals("Edit")) {
																app.setIsEdit(true);
																uf.setIsEdit(true);
																ab.setIsEdit(true);
																a.setIsEdit(true);
																c.setIsEdit(true);
																app1.setIsEdit(true);
																d.setIsEdit(true);
																featureListId.add(d.getId());
															} else if (d.getAction().equals("View")) {
																app.setIsView(true);
																uf.setIsView(true);
																ab.setIsView(true);
																a.setIsView(true);
																c.setIsView(true);
																app1.setIsView(true);
																d.setIsView(true);
																featureListId.add(d.getId());
															} else if (d.getAction().equals("Delete")) {
																app.setIsDelete(true);
																uf.setIsDelete(true);
																ab.setIsDelete(true);

																a.setIsDelete(true);
																c.setIsDelete(true);
																app1.setIsDelete(true);
																d.setIsDelete(true);
																featureListId.add(d.getId());
															} else if (d.getAction() != null
																	&& d.getAction().equals("Approve")) {
																app.setIsApproved(true);
																uf.setIsApproved(true);
																ab.setIsApproved(true);

																a.setIsApproved(true);
																c.setIsApproved(true);
																app1.setIsApproved(true);
																d.setIsApproved(true);
																featureListId.add(d.getId());
															} else if (d.getAction() != null
																	&& d.getAction().equals("Reject")) {
																app.setIsRejected(true);
																uf.setIsRejected(true);
																ab.setIsRejected(true);

																a.setIsRejected(true);
																c.setIsRejected(true);
																app1.setIsRejected(true);
																d.setIsRejected(true);
																featureListId.add(d.getId());
															}
															// log.info("ab:::::::::::::::"+ab.getIsAdd()+"::::::;id::::"+ab.getId());
														} else {
															selectAll = false;
														}
														if (selectAll) {
															d.setSelectAll(true);

														}
														arrList6.add(d);
													}
													app1.setFeatures(arrList6);
													appList5.add(app1);
												}
												c.setFeatures(appList5);
												appList4.add(c);
											}
											ab.setFeatures(appList4);
											appList3.add(ab);

										}
									// log.info("Size of list::::::::"+appList3.size());
									// log.info("Size of list parent::::::::"+a.getFeatures().size());
									if (selectedFeatures.contains(app.getId())) {

										log.info("Third for loop feature name::::" + app.getFeatureName() + "action :;;"
												+ app.getAction() + ":::::::::" + app.getId());
										if (a.getAction().equals("Add")) {
											// log.info("Features Id matched::::::::a:::::::::::::::Action Add");
											app.setIsAdd(true);
											uf.setIsAdd(true);
											a.setIsAdd(true);
											a.setSelectAll(true);
											app.setSelectAll(true);
											uf.setSelectAll(true);
											featureListId.add(a.getId());
										} else if (a.getAction().equals("Edit")) {
											app.setIsEdit(true);
											uf.setIsEdit(true);
											a.setIsEdit(true);
											a.setSelectAll(true);
											app.setSelectAll(true);
											uf.setSelectAll(true);
											featureListId.add(a.getId());
										} else if (a.getAction().equals("View")) {
											app.setIsView(true);
											uf.setIsView(true);
											a.setIsView(true);
											app.setSelectAll(true);
											uf.setSelectAll(true);
											featureListId.add(a.getId());
										} else if (a.getAction().equals("Delete")) {
											app.setIsDelete(true);
											uf.setIsDelete(true);
											app.setSelectAll(true);
											a.setIsDelete(true);
											uf.setSelectAll(true);
											featureListId.add(a.getId());
										} else if (a.getAction() != null && a.getAction().equals("Approve")) {
											app.setIsApproved(true);
											uf.setIsApproved(true);

											a.setIsApproved(true);
											featureListId.add(a.getId());
										} else if (a.getAction() != null && a.getAction().equals("Reject")) {
											app.setIsRejected(true);
											uf.setIsRejected(true);

											a.setIsRejected(true);
											featureListId.add(a.getId());
										}
										// log.info("app:::::::::::::::"+app.getIsAdd()+"::::::;id::::"+app.getId());
									} else {
										selectAllFeat = false;
									}
									if (selectAllFeat) {
										a.setSelectAll(true);
										app.setSelectAll(true);
										uf.setSelectAll(true);
									}

									a.setFeatures(appList3);

									appList2.add(a);
									// log.info("a:::::::::::::::"+a.getIsAdd()+":::id::::"+a.getId());
								}
							}

							app.setFeatures(appList2);
							// log.info("app:::::::::::::::"+app.getIsAdd()+"::::id ::::"+app.getId());
							appList1.add(app);

						}
					uf.setFeatures(appList1);
					log.info("uf" + uf);
					// addFeature(uf, root,false);
				}

				tempList.add(uf);
			}
			featureList = new ArrayList<>();
			featureList.addAll(tempList);
			for (AppFeatureDto uf : featureList) {
				if (uf.getParent() == null) {
					log.info("uf" + uf);
					addFeature(uf, root, false);
				}
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... " + jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured ... " + ioEx.getMessage());
		} catch (Exception e) {
			log.error("Exception occured while redirecting to edit page ... ", e);
		}

		log.info("<---- End RoleBean .editForm  ---->");
		log.info("<---- Redirecting to Edit Role form  ---->");
		return "/pages/masters/createRole.xhtml?faces-redirect=true";

	}

	public void selectAll(String action, Long id, AppFeatureDto app) {
		log.info("<<<<<<< Start Feature Selection process >>>>>>>>>>>");

		if (app.getSelectAll()) {
			log.info("<<<<<<< Features selected >>>>>>>>>>>");
			app.setIsAdd(true);
			app.setIsEdit(true);
			app.setIsDelete(true);
			app.setIsView(true);
			app.setIsApproved(true);
			app.setIsRejected(true);
			app.setSelectAll(true);
			featureListId.add(app.getId());
			selectOrUnselectChildFeatures(app, app.getSelectAll());
		} else {
			log.info("<<<<<<< Features un selected >>>>>>>>>>>");
			app.setIsAdd(false);
			app.setIsEdit(false);
			app.setIsDelete(false);
			app.setIsView(false);
			app.setIsApproved(false);
			app.setIsRejected(false);
			app.setSelectAll(false);
			featureListId.removeAll(Collections.singleton(app.getId()));
			selectOrUnselectChildFeatures(app, app.getSelectAll());
		}
		log.info("Role bean ......features...." + featureListId);
	}

	public void selectOrUnselectChildFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process Starts-------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {

					app1.setIsAdd(true);
					app1.setIsEdit(true);
					app1.setIsView(true);
					app1.setIsDelete(true);
					app1.setSelectAll(true);
					app1.setIsApproved(true);
					app1.setIsRejected(true);
					featureListId.add(app1.getId());
					if (app1.getFeatures() != null)
						selectOrUnselectChildFeatures(app1, selectAll);

					log.info("featureListId::::::" + featureListId);

				} else {
					app1.setIsAdd(false);
					app1.setIsEdit(false);
					app1.setIsView(false);
					app1.setIsDelete(false);
					app1.setIsApproved(false);
					app1.setIsRejected(false);
					app1.setSelectAll(false);

					featureListId.removeAll(Collections.singleton(app1.getId()));
					if (app1.getFeatures() != null)
						selectOrUnselectChildFeatures(app1, selectAll);
					log.info("featureListId unselected::::::" + featureListId);
				}

			}
	}

	public void onSelectOrUnselectParentAddFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process for Add ------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {

					if (app1.getAction().equals("Add") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsAdd(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentAddFeatures(app1, selectAll);
					}

				} else {
					if (app1.getAction().equals("Add") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsAdd(false);
						if (app1.getAction().equals("Add"))
							featureListId.removeAll(Collections.singleton(app1.getId()));
						else if (!action.equals("EDIT") && !app1.getAction().equals("top"))
							featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentAddFeatures(app1, selectAll);
					}
				}

			}
	}

	public void onSelectOrUnselectParentEditFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process for Edit -------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				log.info("App id...." + app1.getId());
				if (selectAll) {

					if (app1.getAction().equals("Edit") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsEdit(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentEditFeatures(app1, selectAll);

					}

				} else {
					if (app1.getAction().equals("Edit") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsEdit(false);
						if (app1.getAction().equals("Edit"))
							featureListId.removeAll(Collections.singleton(app1.getId()));
						else if (!action.equals("EDIT") && !app1.getAction().equals("top"))
							featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentEditFeatures(app1, selectAll);
					}
				}

			}
	}

	public void onSelectOrUnselectParentViewFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process for view-------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {
					if (app1.getAction().equals("View") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsView(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentViewFeatures(app1, selectAll);
					}
				} else {
					if (app1.getAction().equals("View") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsView(false);
						if (app1.getAction().equals("View"))
							featureListId.removeAll(Collections.singleton(app1.getId()));
						else if (!action.equals("EDIT") && !app1.getAction().equals("top"))
							featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentViewFeatures(app1, selectAll);
					}
				}

			}
	}

	public void onSelectOrUnselectParentApproveFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process for view-------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {
					if (app1.getAction().equals("Approve") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsApproved(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentApproveFeatures(app1, selectAll);
					}
				} else {
					if (app1.getAction().equals("Approve") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsApproved(false);
						featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentApproveFeatures(app1, selectAll);
					}
				}

			}
	}

	public void onSelectOrUnselectParentRejectFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process for view-------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {
					if (app1.getAction().equals("Reject") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsRejected(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentRejectFeatures(app1, selectAll);
					}
				} else {
					if (app1.getAction().equals("Reject") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsRejected(false);
						featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentRejectFeatures(app1, selectAll);
					}
				}

			}
	}

	public void onSelectOrUnselectParentDeleteFeatures(AppFeatureDto app, Boolean selectAll) {
		log.info("<<<<< ---------- Child features selection unselection process For Delete -------->>>>>>>");
		if (app.getFeatures() != null)
			for (AppFeatureDto app1 : app.getFeatures()) {
				if (selectAll) {
					if (app1.getAction().equals("Delete") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsDelete(true);
						featureListId.add(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentDeleteFeatures(app1, selectAll);
					}
				} else {
					if (app1.getAction().equals("Delete") || app1.getAction().equals("top")
							|| app1.getAction().equals("stmenu")) {
						app1.setIsDelete(false);
						if (app1.getAction().equals("Delete"))
							featureListId.removeAll(Collections.singleton(app1.getId()));
						else if (!action.equals("EDIT") && !app1.getAction().equals("top"))
							featureListId.remove(app1.getId());
						if (app1.getFeatures() != null)
							onSelectOrUnselectParentDeleteFeatures(app1, selectAll);
					}
				}

			}
	}

	public String editRole_not_working() {

		featureListId = new ArrayList<>();
		log.info("<---- Start RoleBean . editForm  ---->");
		if (selectedRole == null) {
			log.info("Selected role Emtpy");
			Util.addWarn("Please Select One Role");
			return null;
		}
		action = "EDIT";
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/role/get";
			baseDTO = httpService.post(url, selectedRole);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;

			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			roleDto = mapper.readValue(jsonResponse, new TypeReference<RoleDTO>() {
			});
			getAllApplicationType();
			getAllFeatrues();
			root = new DefaultTreeNode("Root", null);
			List<Long> selectedFeatures = roleDto.getFeaturesList();
			treeNodes = new ArrayList<TreeNode>();
			log.info("selected role features:::" + selectedFeatures);
			List<AppFeatureDto> tempList = new ArrayList<>();
			for (AppFeatureDto uf : featureList) {
				log.info(
						"..........................................................................................................................................");
				// leaf=new HashMap<String, Boolean>();
				if (uf.getParent() == null) {
					// log.info("setFeaturesToParent(uf,selectedFeatures).getParent()...Action.."+setFeaturesToParent(uf,selectedFeatures).getParent().getIsAdd()+"Feature
					// name..."+setFeaturesToParent(uf,selectedFeatures).getParent().getFeatureName());
					uf = setFeaturesToParent(uf, selectedFeatures).getParent();
					uf.setFeatures(setFeaturesToParent(uf, selectedFeatures).getFeatures());

				}

				tempList.add(uf);
			}
			featureList = new ArrayList<>();
			featureList.addAll(tempList);
			for (AppFeatureDto uf : featureList) {
				if (uf.getParent() == null) {
					log.info("uf" + uf);
					addFeature(uf, root, false);
				}
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... " + jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured ... " + ioEx.getMessage());
		} catch (Exception e) {
			log.error("Exception occured while redirecting to edit page ... ", e);
		}

		log.info("<---- End RoleBean .editForm  ---->");
		log.info("<---- Redirecting to Edit Role form  ---->");
		return "/pages/masters/createRole.xhtml?faces-redirect=true";

	}

	/**
	 * purpose modifing role as recursive method requires some changes
	 * 
	 * @param appfeature
	 * @param selectedFeatures
	 * @return
	 */
	public AppFeatureDto setFeaturesToParent(AppFeatureDto appfeature, List<Long> selectedFeatures) {
		AppFeatureDto appFeatureWrapper = new AppFeatureDto();
		List<AppFeatureDto> featureList = new ArrayList<>();

		AppFeatureDto parent = appfeature;

		for (AppFeatureDto feature : appfeature.getFeatures()) {
			// log.info("Feature....."+feature.getFeatureName());
			if (selectedFeatures.contains(feature.getId())) {
				featureListId.add(feature.getId());

				if (feature.getAction().equals("Add")) {
					feature.setIsAdd(true);
					appfeature.setIsAdd(true);
					parent.setIsAdd(true);

				} else if (feature.getAction().equals("Edit")) {
					feature.setIsEdit(true);
					appfeature.setIsEdit(true);
					parent.setIsEdit(true);

				} else if (feature.getAction().equals("View")) {
					feature.setIsView(true);
					appfeature.setIsView(true);
					parent.setIsView(true);

				} else if (feature.getAction().equals("Delete")) {
					feature.setIsDelete(true);
					appfeature.setIsDelete(true);
					parent.setIsDelete(true);

				} else if (feature.getAction() != null && feature.getAction().equals("Approve")) {
					feature.setIsApproved(true);
					appfeature.setIsApproved(true);
					parent.setIsApproved(true);

				} else if (feature.getAction() != null && feature.getAction().equals("Reject")) {
					feature.setIsRejected(true);
					appfeature.setIsRejected(true);
					parent.setIsRejected(true);

				}

			}
			log.info("...........................................................................................");
			log.info("Feature......" + feature.getFeatureName());
			log.info("leaf before update...." + leaf);
			updateLeaf(feature);
			log.info("leaf after update......" + leaf);
			log.info("...........................................................................................");
			if (feature.getFeatures() != null)
				setFeaturesToParent(feature, selectedFeatures);
			featureList.add(feature);

		}
		if (appfeature.getAction().equals("top")) {
			// log.info("Top...."+appfeature.getFeatureName());
			// log.info("leaf...."+leaf);
			appfeature.setLeaf(leaf);
		}

		appFeatureWrapper.setParent(appfeature);
		appFeatureWrapper.setFeatures(featureList);
		return appFeatureWrapper;
	}

	public void updateLeaf(AppFeatureDto feature) {
		if (feature.getAction().equals("Add"))
			leaf.put("Add", true);
		else
			leaf.put("Add", false);
		if (feature.getAction().equals("Edit"))
			leaf.put("Edit", true);
		else
			leaf.put("Edit", false);
		if (feature.getAction().equals("View"))
			leaf.put("View", true);
		else
			leaf.put("View", false);
		if (feature.getAction().equals("Delete"))
			leaf.put("Delete", true);
		else
			leaf.put("Delete", false);
		if (feature.getAction() != null && feature.getAction().equals("Approve")) {
			log.info("Approve....");
			leaf.put("Approve", true);
		}

		if (feature.getAction() != null && feature.getAction().equals("Reject")) {
			log.info("Reject....");
			leaf.put("Reject", true);
		}

	}

	public boolean filterDate(Object value, Object filter, Locale locale) {
		if (filter == null) {
			return true;
		}

		if (value == null) {
			return false;
		}
		return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);

	}
}
