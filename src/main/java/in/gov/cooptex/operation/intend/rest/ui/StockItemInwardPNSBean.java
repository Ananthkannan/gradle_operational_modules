package in.gov.cooptex.operation.intend.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.OrderTypeEnum;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.ProductVarietyPrice;
import in.gov.cooptex.core.model.SocietyProductAppraisal;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.SupplierType;
import in.gov.cooptex.operation.intend.model.StockItemInwardPNSDTO;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.production.dto.ProductQRCodeResponse;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@SuppressWarnings("unused")
@Log4j2
@Service("stockItemInwardBean")
@Scope("session")
public class StockItemInwardPNSBean implements Serializable {

	private static final long serialVersionUID = 4578136862113789205L;
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String INPUT_FORM_LIST_URL = "/pages/printingAndStationary/listStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/printingAndStationary/viewStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_ADD_URL = "/pages/printingAndStationary/createStockItemInwardPNS.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_Edit_URL = "/pages/printingAndStationary/editStockItemInwardPNS.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	SocietyProductAppraisal societyProductAppraisal;

	@Getter
	@Setter
	private String inwardFrom;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;

	@Getter
	@Setter
	EntityMaster entityMaster;

	final String entity = "Entity";

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	StockTransfer selectedStockTransferPNS;

	@Getter
	@Setter
	StockTransferItems selectedStockTransferItems;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplierTypeMaster;

	@Getter
	@Setter
	SupplierMaster selectedSupplierMaster;

	@Getter
	@Setter
	CircleMaster selectedCircleMaster;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	List<PurchaseOrderItems> collectorSupplierItemList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsInwardUpdateList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsInwardList = new ArrayList<>();

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsInwardDeleteList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyList;

	@Getter
	@Setter
	PurchaseOrderItems purchaseOrderItems;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsList;

	String SOCIETY_PRODUCT_APPRAISAL_URL = "";

	@Getter
	@Setter
	long selectedProductVariety;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private boolean renderedInward;

	@Getter
	@Setter
	private boolean manualInwardrender;

	@Getter
	@Setter
	List<PurchaseOrder> purchaseOrderList;

	@Getter
	@Setter
	PurchaseOrder selectedPurchaseOrder;

	@Getter
	@Setter
	SupplierMaster selectedSupplierMasterTest;

	@Getter
	@Setter
	LazyDataModel<StockItemInwardPNSDTO> stockTransferLazyList;

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList;

	@Getter
	@Setter
	StockItemInwardPNSDTO stockTransferLazy;

	@Getter
	@Setter
	StockItemInwardPNSDTO selectedStockItemInwardPNSDTO;

	@Getter
	@Setter
	String supplierType;

	@Getter
	@Setter
	String supplierCodeName;

	@Getter
	@Setter
	String gstNumber;

	@Getter
	@Setter
	Boolean taxExempted;

	@Getter
	@Setter
	private String inwardType;
	
	@Getter
	@Setter
	String orderType;
	
	@Getter
	@Setter
	List<String> orderTypeList = new ArrayList<>();

	@Getter
	@Setter
	String purchaseOrderNumber;

	@Getter
	@Setter
	String hsnCode;

	@Getter
	@Setter
	String purchaseDate;

	@Getter
	@Setter
	String purhaseOrderValidityDate;

	@Getter
	@Setter
	String uom;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int size;

	/*
	 * @Getter
	 * 
	 * @Setter List<SupplierType> supplierTypeEnumList;
	 */

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	List<StockTransferType> stockTransferTypeEnumList;

	@Getter
	@Setter
	List<StockTransferStatus> stockTransferEnumStatus;

	@Getter
	@Setter
	String description;

	@Getter
	@Setter
	StockItemInwardPNSBean stockItemInwardPNSBean;

	@Getter
	@Setter
	StockItemInwardPNSBean selectedeStockItemInwardPNSBean;

	@Getter
	@Setter
	String interestApplicablePayment;

	@Getter
	@Setter
	int countAtNumberGenerator = 1;

	@Getter
	@Setter
	private byte[] uploadDocumentsBytesArray;

	@Getter
	@Setter
	private StreamedContent downloadFile;

	@Getter
	@Setter
	String fileExtension;

	@Getter
	@Setter
	InputStream fileInputStream;

	// Transport Details

	@Getter
	@Setter
	String transportServiceType;

	@Getter
	@Setter
	String transportServiceName;

	@Getter
	@Setter
	String wayBillAvailable;

	@Getter
	@Setter
	String wayBillNumber;

	@Getter
	@Setter
	Double weight;

	@Getter
	@Setter
	String transportChargeAvailble;

	@Getter
	@Setter
	String transportChargeType;

	@Getter
	@Setter
	String receiptNumber;

	@Getter
	@Setter
	double transchargeChargeAmount;

	@Getter
	@Setter
	boolean addItemReceiveDetails;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplier_Type_Master;

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportServiceMasterList;

	@Getter
	@Setter
	boolean waybillRendered;

	@Getter
	@Setter
	boolean transportChargeRendered;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	TransportMaster selectedTransportMaster;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	StockTransferType stockTransferType;

	@Getter
	@Setter
	List<StockTransferItems> selectedStockTransferItemsList;

	@Getter
	@Setter
	private boolean renderedEdit, renderedView, renderedDelete;

	@Getter
	@Setter
	private boolean renderedCreate;

	@Getter
	@Setter
	PurchaseInvoice purchaseInvoice;

	@Getter
	@Setter
	PurchaseInvoice selectedPurchaseInvoice;

	@Getter
	@Setter
	List<PurchaseInvoice> purchaseInvoiceList;

	@Getter
	@Setter
	List<PurchaseInvoice> selectedPurchaseInvoiceList;

	@Getter
	@Setter
	private UploadedFile uploadDocumentFile = null;

	@Getter
	@Setter
	JobApplication jobApplication;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();

	@Getter
	@Setter
	ProductCategory selectedProductCategory = new ProductCategory();

	@Getter
	@Setter
	ProductGroupMaster selectedProductGroupMaster = new ProductGroupMaster();

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	Long selectedProductVarietyMasterID;

	@Getter
	@Setter
	String currentFilePath;

	@Getter
	@Setter
	String currentFileType;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<EntityMaster> showroomentityList;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	final String society = SupplierType.SOCIETY;

	@Getter
	@Setter
	Boolean circleflag = false;

	@Getter
	@Setter
	ProductVarietyPrice productVarietyPrice;

	@Getter
	@Setter
	List<EntityMaster> headOfficeAndRegionEntityList;

	@Getter
	@Setter
	EntityMaster headOfficeAndRegionEntity = new EntityMaster();

	@Getter
	@Setter
	EntityMaster showroomentityMaster = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> hoentityList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster headOrRegionOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<>();

	@Getter
	@Setter
	Boolean isHeadOffice = false;

	String jsonResponse;

	ObjectMapper mapper;

	private String serverURL = null;

	@Getter
	@Setter
	List<EntityMaster> entityList = new ArrayList<>();

	Long index = 0l;

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	private Boolean parNumberGenerateBtnRenderedFlag;

	@Getter
	@Setter
	private Boolean parNumberGenerateBtnDisabledFlag;

	@PostConstruct
	public void init() {

		waybillRendered = false;
		transportChargeRendered = false;
		gstNumber = null;
		taxExempted = null;
		wayBillAvailable = "Select Waybill Type";
		transportChargeAvailble = "Select Type";
		waybillRendered = false;
		transportChargeRendered = false;
		renderedEdit = false;
		renderedView = false;
		renderedDelete = false;
		renderedCreate = true;
		headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
		circleMasterList = commonDataService.loadCircles();
		societyProductAppraisal = new SocietyProductAppraisal();
		selectedPurchaseOrder = new PurchaseOrder();
		entityTypeMaster = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		selectedCircleMaster = new CircleMaster();
		productDesignMaster = new ProductDesignMaster();
		wayBillNumber = null;
		countAtNumberGenerator = 1;
		headOrRegionOffice = new EntityMaster();
		uploadDocumentFile = null;
		fileName = null;
		reset();
		loadTransportTypeMasterList();
		loadSupplierType();
		loadLazyIntRequirementList();
		renderedInwardTypeSelection();
		productCategoryGroupList();
		loadOrderTypeList();
		hsnCode = null;
	}

	public String gotoItemInwardPage() {

		parNumberGenerateBtnDisabledFlag = true;
		parNumberGenerateBtnRenderedFlag = false;

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;
		} else {

			String loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();

			if (EntityType.INSTITUTIONAL_SALES_SHOWROOM.equals(loginEntityCode)
					|| EntityType.PRINTING_WARE_HOUSE.equals(loginEntityCode)) {
				parNumberGenerateBtnRenderedFlag = true;
			} else {
				parNumberGenerateBtnRenderedFlag = false;
			}
		}

		return INPUT_FORM_LIST_URL;
	}
	
	public String generatePAR() {
		log.info("StockItemInwardPNSBean. generatePAR() - START");
		String msg = "";
		try {
			
			String referenceNumber = selectedStockItemInwardPNSDTO != null ? selectedStockItemInwardPNSDTO.getReferenceNumber() : null;
			if(StringUtils.isEmpty(referenceNumber)) {
				errorMap.notify(ErrorDescription.REFERENCE_NUMBER_REQUIRED.getCode());
				return null;
			}
			
			if (StringUtils.isNotEmpty(referenceNumber)) {
				/**
				 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller/
				 * 
				 * ProductQRCodeController.java
				 */
				referenceNumber = referenceNumber.trim();
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/generatepar/"
						+ referenceNumber;
				//
				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						msg = "PAR Generated Successfully";
						AppUtil.addInfo(msg);
					} else if (baseDTO.getStatusCode().equals(ErrorDescription.PLAN_ALREADY_EXISTS.getCode())) {
						msg = "PAR Already Generated";
						AppUtil.addWarn(msg);
					} else {
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					}
					selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
				} else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
				parNumberGenerateBtnDisabledFlag = true;
			}
		} catch (Exception e) {
			log.error("Exception at StockItemInwardPNSBean. generatePAR()", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("StockItemInwardPNSBean. generatePAR() - END");
		return null;
	}

	public void reset() {

		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		stockTransfer = new StockTransfer();

		purchaseOrderItems = new PurchaseOrderItems();
		supplierMasterList = new ArrayList<SupplierMaster>();
		supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
		selectedSupplierMaster = null;
		renderedInward = true;
		manualInwardrender = true;
		selectedSupplierMasterTest = new SupplierMaster();
		inwardType = "Direct Inward";
		stockTransferItems = new StockTransferItems();

		stockItemInwardPNSBean = new StockItemInwardPNSBean();
		selectedSupplier_Type_Master = new SupplierTypeMaster();
		selectedTransportTypeMaster = new TransportTypeMaster();
		selectedTransportMaster = new TransportMaster();
		purchaseOrderList = new ArrayList<PurchaseOrder>();
		productVarietyList = new ArrayList<ProductVarietyMaster>();
		// supplierTypeEnumList = new ArrayList<SupplierType>();
		stockTransferTypeEnumList = new ArrayList<StockTransferType>(Arrays.asList(StockTransferType.values()));
		stockTransferEnumStatus = new ArrayList<StockTransferStatus>(Arrays.asList(StockTransferStatus.values()));
		stockTransferItemsInwardUpdateList = new ArrayList<StockTransferItems>();

		purchaseInvoice = new PurchaseInvoice();
		purchaseInvoiceList = new ArrayList<PurchaseInvoice>();
		jobApplication = new JobApplication();
		selectedStockTransferPNS = new StockTransfer();
		selectedSupplierTypeMaster = null;
		transportChargeType = "Select Type";
		inwardFrom = null;
		selectedProductCategoryGroup = new ProductCategoryGroup();
		selectedProductCategory = new ProductCategory();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		selectedProductGroupMaster = new ProductGroupMaster();
		countAtNumberGenerator = 1;
		loadOrderTypeList();
		log.info("Default Value Set Successfully");
	}

	public void loadAllHOEntityList() {
		log.info("... Load  HO/RO List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitymaster/getall/headandregionalOffice");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			hoentityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading HO/RO data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			mapper = new ObjectMapper();
			/*
			 * EntityTypeMasterController.java - Method: getEntityType()
			 */
			BaseDTO response = httpService.get(SERVER_URL + "/entitytypemaster/getall/entitytype");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllEntityByRegionList() {
		log.info("... Load  Entity List by region called ....");
		try {

			if (headOrRegionOffice != null && headOrRegionOffice.getEntityTypeMaster() != null
					&& headOrRegionOffice.getEntityTypeMaster().getEntityCode() != null
					&& headOrRegionOffice.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
				isHeadOffice = true;
				return;
			}
			isHeadOffice = false;

			log.info("loadAllEntityByRegionList :: Head or region office id==> " + headOrRegionOffice.getId());
			log.info("loadAllEntityByRegionList :: Entity type id==> " + headOrRegionOffice.getId());

			String url = SERVER_URL + "/entitymaster/getall/getmanualAttenRegion/" + headOrRegionOffice.getId() + "/"
					+ entityTypeMaster.getId();
			log.info("loadAllEntityByRegionList :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});

		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	// Load Supplier Type List
	public void loadSupplierType() {
		log.info(":::<- Load Supplier TypeStart ->:::");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllSupplierType";
			log.info("::: Supplier Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get Supplier Response :");
			if (baseDTO.getStatusCode() == 0) {
				supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
				log.info("Supplier load Successfully " + baseDTO.getTotalRecords());

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
				log.info("StockItemInwardList Page Convert Error -->");
			} else {
				log.warn("Supplier Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Supplier load ", ee);
		}

	}
	
	public void loadOrderTypeList(){
		log.info("loadOrderTypeList method start");
		orderTypeList = new ArrayList<>();
		orderTypeList.add(OrderTypeEnum.FDS.name());
		orderTypeList.add(OrderTypeEnum.GEN.name());
		orderTypeList.add(OrderTypeEnum.NMP.name());
		orderTypeList.add(OrderTypeEnum.OAP.name());
		log.info("loadOrderTypeList method ends");
	}


	// Load Supplier Code / Name
	public void loadSupplierCodeName() {
		try {

			if (!society.equals(selectedSupplierTypeMaster.getCode())) {
				circleflag = false;
				selectedCircleMaster = new CircleMaster();
				log.info(":::<- Load loadSupplierCodeName Called ->::: " + selectedSupplierTypeMaster.getCode());
				BaseDTO baseDTO = null;
				selectedSupplierMaster = new SupplierMaster();
				// selectedSupplierMaster.setId(selectedSupplierTypeMaster.getId());

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/stockItemInwardPNS/getSelectedSupplierMasterDetailswithGst/"
						+ selectedSupplierTypeMaster.getId();
				log.info("::: supplierMaster Controller calling  :::");
				baseDTO = httpService.get(url);
				log.info("::: get supplierMaster Response :");
				if (baseDTO.getStatusCode() == 0) {
					// ObjectMapper mapper = new ObjectMapper();
					// supplierMasterList = new ArrayList<SupplierMaster>();
					// String jsonResponse =
					// mapper.writeValueAsString(baseDTO.getResponseContents());
					// supplierMasterList = mapper.readValue(jsonResponse, new
					// TypeReference<List<SupplierMaster>>() {
					// });
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});

					log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
				} else {
					log.warn("supplierMaster Load Error ");
				}

				// supplierMasterList=commonDataService.loadSupplierBySupplierTypeId(SERVER_URL,
				// selectedSupplierTypeMaster.getId());
			} else {
				supplierMasterList = null;
				circleflag = true;
				loadAllActiveCircleMaster();
				// circleMasterList = commonDataService.loadCircles();
			}
		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public void loadAllActiveCircleMaster() {

		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllCircle";
			log.info("::: StockItemInwardPNS Controller calling  :::");
			BaseDTO baseDTO = httpService.get(url);
			log.info("::: get loadAllActiveCircleMaster Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				circleMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CircleMaster>>() {
				});

				log.info("circleMasterList load Successfully " + baseDTO.getTotalRecords());
			} else {
				log.warn("circleMasterList Load Error ");
			}
		} catch (Exception e) {
			log.error(" ==== >>>> Exception Occured In Circle MAster circleMasterList ==== >> ");
		}

	}

	// Load Supplier Code / Name
	public void loadSupplierCodeNameByCircle() {
		try {
			log.info(":::<- Load loadSupplierCodeName Called ->::: " + selectedSupplierTypeMaster.getCode());
			BaseDTO baseDTO = null;
			selectedSupplierMaster = new SupplierMaster();
			// selectedSupplierMaster.setId(selectedSupplierTypeMaster.getId());
			if (selectedCircleMaster.getId() != null) {
				String url = SERVER_URL + "/supplier/master/getsuppliermasterbycirclemaster/"
						+ selectedCircleMaster.getId();
				log.info("::: supplierMaster Controller calling  :::");
				baseDTO = httpService.get(url);
				log.info("::: get supplierMaster Response :");
				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					supplierMasterList = new ArrayList<SupplierMaster>();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});
					log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
				} else {
					log.warn("supplierMaster Load Error ");
				}
			}

		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public void updateDesignMaster() {
		if (productDesignMaster != null) {
			societyProductAppraisal.setDesignName(productDesignMaster.getDesignName());
			societyProductAppraisal.setDesignNumber(productDesignMaster.getDesignCode().toString());
		}
		if (selectedSupplierMaster != null) {
			societyProductAppraisal.setSociety(selectedSupplierMaster);
		}
		societyProductAppraisal.setProductVariety(selectedProductVarietyMaster);

	}

	public void loadPurchaseOrderNumber() {

		log.info(":::<- Load loadPurchaseOrderNumber TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getPurhaseOrder/1";
			log.info("::: PurchaseOrderNumber Controller calling :::");
			// PurchaseOrderItems items = new PurchaseOrderItems();
			baseDTO = httpService.get(url);
			log.info("::: get PurchaseOrderNumber Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				purchaseOrderList = new ArrayList<PurchaseOrder>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrderList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrder>>() {

				});
				log.info("loadPurchaseOrderNumber load Successfully " + baseDTO.getTotalRecords());
				log.info("loadPurchaseOrderNumber Page Convert Succes -->");
			} else {
				log.warn("loadPurchaseOrderNumber Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadPurchaseOrderNumber load ", ee);
		}
	}

	public void loadProductVarietyDetails() {

		log.info(":::<- Load loadProductVarietyDetails TypeStart ->::: ");
		BaseDTO baseDTO = null;
		// selectedProductVarietyMaster =new ProductVarietyMaster();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllProductVariety/1";
			log.info("::: loadProductVarietyDetails Controller calling  1 :::");
			// PurchaseOrderItems items = new PurchaseOrderItems();
			baseDTO = httpService.get(url);
			log.info("::: get loadProductVarietyDetails Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				productVarietyList = new ArrayList<ProductVarietyMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyList = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyMaster>>() {
				});
				log.info("loadProductVarietyDetails load Successfully " + baseDTO.getTotalRecords());
				log.info("loadProductVarietyDetails Page Convert Succes -->");
			} else {
				log.warn("loadPurchaseOrderNumber Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadPurchaseOrderNumber load ", ee);
		}
	}

	// add Item Edit Details
	public void itemCollectiorAddNewInward() {
		log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() STARTED");
		try {

			if (selectedProductVarietyMaster == null) {
				AppUtil.addWarn(" Please enter Product Variety Code / Name");
				return;
			}

			if (selectedProductCategory != null) {
				String categoryCode = selectedProductCategory.getProductCatCode();

				if (StringUtils.isEmpty(categoryCode)) {
					AppUtil.addWarn("Category code is empty.");
					return;
				}

				log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() - categoryCode: " + categoryCode);

				UomMaster uomMaster = selectedProductVarietyMaster.getUomMaster();

				if (uomMaster == null) {
					AppUtil.addWarn("Product uom is empty.");
					return;
				}

				String uomCode = uomMaster.getCode();

				if (StringUtils.isEmpty(uomCode)) {
					AppUtil.addWarn("Uom code is empty.");
					return;
				}

				log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() - uomCode: " + uomCode);

				if (!"METR".equals(uomCode)) {
					if ("A".equalsIgnoreCase(categoryCode) || "AJ".equalsIgnoreCase(categoryCode)) {
						stockTransferItems.setOrderedQty(1D);
					} else {
						log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() - category code is not A or AJ");
					}
				} else {
					log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() - uomCode is METR");
				}

				// if (selectedProductCategory.getProductCatCode() != null
				// && selectedProductVarietyMaster.getUomMaster() != null) {
				// if (selectedProductCategory.getProductCatCode().equalsIgnoreCase("A")
				// && !selectedProductVarietyMaster.getUomMaster().getCode().equals("METR")
				// || selectedProductCategory.getProductCatCode().equalsIgnoreCase("AJ")
				// && !selectedProductVarietyMaster.getUomMaster().getCode().equals("METR")) {
				// stockTransferItems.setOrderedQty(1D);
				// }
				// }
			} else {
				AppUtil.addWarn(" Please select Product Category Code / Name");
				return;
			}
			if (stockTransferItems.getOrderedQty() == null || stockTransferItems.getOrderedQty() <= 0) {
				log.info("Invalid Item Datails quantity:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_QUANTITY.getErrorCode());
			} else if (stockTransferItems.getUnitRate() == null || stockTransferItems.getUnitRate() <= 0) {
				log.info("Invalid Item Datails unitRate:");
				if (StringUtils.isNotEmpty(inwardType)) {
					if (!inwardType.equalsIgnoreCase("Warehouse Manual Inward")) {
						errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_UNIT.getErrorCode());
					} else {
						AppUtil.addWarn("Please enter the Purchase Price");
					}
				}
			} else if (stockTransferItems.getDiscountPercent() != null && stockTransferItems.getDiscountPercent() < 0) {
				log.info("Invalid Item Datails discount :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_DISCOUNT.getErrorCode());
			} else if (stockTransferItems.getTaxPercent() != null && stockTransferItems.getTaxPercent() < 0) {
				log.info("Invalid Item Datails tax:");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_INVALID_TAX.getErrorCode());
			} else if (selectedProductVarietyMaster == null || selectedProductVarietyMaster.getCode() == null) {
				log.info("Invalid ProductVarietyMaster Selection");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_ITEM_SELECT.getErrorCode());
			} else {

				String atNumber = stockTransferItems.getAtNumber();
				atNumber = StringUtils.isNotEmpty(atNumber) ? atNumber.toUpperCase() : null;
				
				stockTransferItems.setAtNumber(atNumber);
				if (stockTransferItemsInwardUpdateList != null && !stockTransferItemsInwardUpdateList.isEmpty()
						&& stockTransferItems.getAtNumber() != null && !stockTransferItems.getAtNumber().isEmpty()) {
					boolean atNumberExists = stockTransferItemsInwardUpdateList.stream()
							.filter(st -> st.getAtNumber() != null)
							.anyMatch(sti -> sti.getAtNumber().toUpperCase().equalsIgnoreCase(stockTransferItems.getAtNumber()));
					if (atNumberExists) {
						AppUtil.addError("AT Number already exists");
						return;
					}
				}

				/*
				 * for (StockTransferItems sti : stockTransferItemsInwardUpdateList) {
				 * if(sti.getProductVarietyMaster()!=null &&
				 * sti.getProductVarietyMaster().getId().longValue()==
				 * selectedProductVarietyMaster.getId().longValue()) {
				 * if(sti.getUnitRate().equals(stockTransferItems.getUnitRate())) {
				 * AppUtil.addError("This unit rate already given for this product"); return; }
				 * }
				 * 
				 * }
				 */

				stockTransferItems.setProductVarietyMaster(selectedProductVarietyMaster);

				if (selectedProductVarietyMaster.getUomMaster() != null) {
					stockTransferItems.setUomMaster(selectedProductVarietyMaster.getUomMaster());
					if (selectedProductVarietyMaster.getUomMaster().getCode().equals("METR")) {
						stockTransferItems
								.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
					} else {
						stockTransferItems
								.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
					}
				} else {
					Util.addWarn("UOM is Empty");
					stockTransferItems
							.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
				}
				stockTransferItems.setItemNetTotal(stockTransferItems.getItemTotal());
				stockTransferItems.setReceivedQty((double) stockTransferItems.getOrderedQty());
				stockTransferItems.setPurchasePrice(stockTransferItems.getUnitRate());
				stockTransferItems.setDescription(description);

				double total = stockTransferItems.getItemTotal();
				if (stockTransferItems.getDiscountPercent() != null) {
					if (stockTransferItems.getDiscountPercent() > 0) {
						double discountV = (total * stockTransferItems.getDiscountPercent()) / 100;
						stockTransferItems.setDiscountValue(discountV);
						stockTransferItems.setItemNetTotal(
								stockTransferItems.getItemNetTotal() - stockTransferItems.getDiscountValue());
					}
				}

				if (stockTransferItems.getTaxPercent() != null) {
					if (stockTransferItems.getTaxPercent() > 0) {
						double tax = stockTransferItems.getTaxPercent();
						double taxV = (total * tax) / 100;
						log.info("tax value----" + taxV);
						stockTransferItems.setTaxValue(taxV);
						double taxValue = roundOffTaxValue(taxV);
						stockTransferItems.setTaxValue(taxValue);
						stockTransferItems.setItemNetTotal(
								stockTransferItems.getItemNetTotal() + stockTransferItems.getTaxValue());
					}
				}

				// stockTransferItems.setItemNetTotal( (stockTransferItems.getItemTotal() +
				// stockTransferItems.getTaxValue() ) -
				// (stockTransferItems.getDiscountValue()));
				index++;
				stockTransferItems.setId(index);
				stockTransferItemsInwardUpdateList.add(stockTransferItems);

				// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_ITEM_ADDED_SUCCESSFULLY.getErrorCode());
				log.info("Item Collection add Success :");
				stockTransferItems = new StockTransferItems();
				description = null;
				// hsnCode = null;
				countAtNumberGenerator = countAtNumberGenerator + 1;
				// selectedProductVarietyMaster = new ProductVarietyMaster();
				// selectedProductGroupMaster = new ProductGroupMaster();
				// selectedProductCategory = new ProductCategory();
				// selectedProductCategoryGroup = new ProductCategoryGroup();
				// uom="";
			}
			// }
		} catch (Exception e) {
			log.error("StockItemInward Collection Add Error :", e);
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_NEW_ADDED_FAILED.getErrorCode());
		}
		log.info("StockItemInwardPNSBean. itemCollectiorAddNewInward() COMPLETED");
	}

	private double roundOffTaxValue(double taxV) {
		Double taxRoundOff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxV));
		log.info("tax round off value----" + taxRoundOff);
		String str = String.valueOf(taxRoundOff);
		String[] convert = str.split("\\.");
		int length = convert[1].length();
		double taxValue = 0.0;
		if (length == 2) {
			int splitValue = Integer.valueOf(convert[1].substring(1));
			log.info("split up value----------" + splitValue);
			if (splitValue % 2 == 0) {
				log.info("splitValue is even-----");
				taxValue = taxRoundOff;
			} else {
				log.info("splitValue is odd-----");
				taxValue = taxRoundOff + 0.01;
				taxValue = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue));
			}
		} else {
			taxValue = taxRoundOff;
		}
		return taxValue;
	}

	public void clearsocietyProductAppraisal() {
		societyProductAppraisal = new SocietyProductAppraisal();
		societyProductAppraisal.setProductCost(null);
	}

	public void headandRegional() {

		showroomentityList = new ArrayList<>();

		if (headOfficeAndRegionEntity != null) {
			log.info("<====== EmployeeAdditionalChargeBean.headandRegional Starts====>entity master",
					headOfficeAndRegionEntity.getId());
			try {
				if (headOfficeAndRegionEntity.getId() != null && headOfficeAndRegionEntity.getEntityTypeMaster() != null
						&& headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode()
								.equalsIgnoreCase("HEAD_OFFICE")) {
					showroomentityMaster.setId(headOfficeAndRegionEntity.getId());
				} else {
					showroomentityList = commonDataService.loadShowroomListForRegion(headOfficeAndRegionEntity.getId());
				}
			} catch (Exception exp) {
				log.error("found exception in onchangeEntityType: ", exp);
			}
			log.info("<======= EmployeeAdditionalChargeBean.headandRegiona Ends =========>");

		}
	}

	public void onRowEdit(RowEditEvent event) {
		try {
			StockTransferItems stockTransferItems = ((StockTransferItems) event.getObject());
			stockTransferItems.setItemTotal(stockTransferItems.getUnitRate() * stockTransferItems.getOrderedQty());
			double total = stockTransferItems.getItemTotal();
			if (stockTransferItems.getDiscountPercent() != null) {
				double discountV = (total * stockTransferItems.getDiscountPercent()) / 100;
				stockTransferItems.setDiscountValue(discountV);
			}

			if (stockTransferItems.getTaxPercent() != null) {
				double tax = stockTransferItems.getTaxPercent();
				double taxV = (total * tax) / 100;
				stockTransferItems.setTaxValue(taxV);
			}
			if (stockTransferItems.getTaxValue() == null) {
				stockTransferItems.setTaxValue(0D);
			}
			if (stockTransferItems.getDiscountValue() == null) {
				stockTransferItems.setDiscountValue(0D);
			}
			stockTransferItems.setItemNetTotal((stockTransferItems.getItemTotal() + stockTransferItems.getTaxValue())
					- (stockTransferItems.getDiscountValue()));

			RequestContext.getCurrentInstance().update(":itemPNS:itemTablePanel");
			Util.addInfo("Edited :" + stockTransferItems.getProductVarietyMaster().getName());
		} catch (Exception see) {
			log.error("OnRow Edit Inward DataTable Error :" + see);
		}

	}

	public void onRowCancel(RowEditEvent event) {
		try {
			Util.addInfo("Cancelled " + ((StockTransferItems) event.getObject()).getProductVarietyMaster().getName());
		} catch (Exception see) {
			log.error("OnRowCancel Inward DataTable Error :" + see);
		}
	}

	// public void removeItemInwardListCollector(StockTransferItems
	// stockTransferItems) {
	// log.info("StockInward Item Remove Received :");
	// stockTransferItemsInwardUpdateList.remove(stockTransferItems);
	// countAtNumberGenerator = countAtNumberGenerator - 1;
	// log.info("StockInward Item Remove Done:");
	// }

	public void deleteRow(StockTransferItems stockTransferItems) {

		try {
			if (stockTransferItemsInwardUpdateList.contains(stockTransferItems)) {
				log.info("<==== Starts deleteRow.deleteRow Inside If =====>");
				stockTransferItemsInwardUpdateList.remove(stockTransferItems);
				countAtNumberGenerator = countAtNumberGenerator - 1;
				log.info("<==== END StockItemInward PNS.deleteRow =====>");
			}
		} catch (Exception exp) {
			log.info("Error In deleteRow Metod", exp);
		}

	}

	public void clearItemTable() {
		purchaseOrderItems = new PurchaseOrderItems();
		selectedProductCategoryGroup = new ProductCategoryGroup();
		selectedProductCategory = new ProductCategory();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		selectedProductGroupMaster = new ProductGroupMaster();
		stockTransferItems = new StockTransferItems();
		uom = null;
		hsnCode = null;
		description = null;
	}

	public String reinit() {
		// stockItemInwardPNSBean = new StockItemInwardPNSBean();
		// enable if you want update null values after add values
		// Util.addInfo("New Item Added Successfully");
		return null;
	}

	public String reinitUpdate(StockTransferItems items) {
		log.info("Received Collector remove " + items.getId());
		try {
			stockTransferItemsInwardDeleteList.add(items);
			log.info("Received Collector remove Add Success");
		} catch (Exception e) {
			log.error("StockItems remove Add Error " + e);
		}

		return null;
	}

	public void gstNumberUpdate() {
		try {
			log.info("***** Gst Number Set Received supplierMasterByID ******* ");
			gstNumber = selectedSupplierMaster.getGstNumber();
		} catch (Exception se) {
			log.error("Set Gst_Number Get Error " + se);
		}
	}

	public void taxExemptedUpdate() {
		try {
			log.info("***** Gst Number Set Received supplierMasterByID ******* ");
			taxExempted = selectedSupplierMaster.getTaxExempted();
		} catch (Exception se) {
			log.error("Set ]taxExempted Get Error " + se);
		}
	}

	public void purchaseOrderUpdate() {
		try {
			log.info("*****Received PurchaseOrderUpdate ******* ");
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			purchaseDate = "";
			purhaseOrderValidityDate = "";

			purchaseDate = sdf.format(selectedPurchaseOrder.getOrderDate());
			purhaseOrderValidityDate = sdf.format(selectedPurchaseOrder.getValidDate());

			log.info("Done Purchase Order Update");
			purchaesOrderItemList(selectedPurchaseOrder.getId());
		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public void purchaesOrderItemList(Long id) {

		try {
			purchaseOrderItems = new PurchaseOrderItems();
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			purchaseOrderItems.setId(id);
			log.info("Received PurchaseOrderList :");
			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllPurchaseOrderItem";
			log.info("::: PurchaseOrderList Controller calling  1 :::");
			baseDTO = httpService.post(url, purchaseOrderItems);
			log.info("::: get PurchaseOrderList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				collectorSupplierItemList = mapper.readValue(jsonResponse,
						new TypeReference<List<PurchaseOrderItems>>() {
						});
				log.info("PurchaseOrderList load Successfully " + baseDTO.getTotalRecords());
				log.info("PurchaseOrderList Page Convert Succes -->");
			} else {
				log.warn("PurchaseOrderList Error ");
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_PURCHASE_ORDER_FAILED.getErrorCode());
			log.error("Get List PurchaseOrderList Error :" + ee);
		}

		log.info("Start Product Selected List :******************** ");

	}

	/**
	 * 
	 */
	private void setSlectedProduct() {
		Long selectedProductId = (selectedProductVarietyMaster != null ? selectedProductVarietyMaster.getId() : null);
		log.info("setSlectedProduct() - selectedProductVarietyId: " + selectedProductId);
		selectedProductVarietyMaster = commonDataService.loadProductVarietyMasterById(selectedProductId);
	}

	public void productVarietyUpdate() {

		setSlectedProduct();

		try {
			log.info("productvarity master id-----------" + selectedProductVarietyMaster.getId());
			collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
			hsnCode = selectedProductVarietyMaster.getHsnCode();
			if (selectedProductVarietyMaster.getUomMaster() != null) {
				uom = selectedProductVarietyMaster.getUomMaster().getName();
			}
			log.info("productvarity master hsnCode ----------- " + hsnCode);
			log.info("productvarity master uom ----------- " + uom);

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/productvarietyRate/"
					+ selectedProductVarietyMaster.getId();
			log.info("::: productVarietyUpdate Controller calling  1 :::");
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productVarietyPrice = mapper.readValue(jsonResponse, new TypeReference<ProductVarietyPrice>() {
				});

				if (productVarietyPrice != null) {
					uom = productVarietyPrice.getProductVarietyMaster().getUomMaster().getName();
					log.info("uom==> " + uom);
				}

				String jsonResponseDesing = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponseDesing,
						new TypeReference<List<ProductDesignMaster>>() {
						});

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

			log.info("Produect Variety Update Get Success :");
		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public void loadProductVerietyTaxPercentage() {
		log.info("loadProductVerietyTaxPercentage :: start");
		try {

			if (societyProductAppraisal != null) {
				if (societyProductAppraisal.getProductCost() != null && stockTransferItems.getUnitRate() != null) {
					if (!societyProductAppraisal.getProductCost().equals(stockTransferItems.getUnitRate())) {
						stockTransferItems.setUnitRate(null);
						AppUtil.addError(" Product Cost And Unit Rate In Item Receive Details Should be Same");
						return;
					}
				}
			}

			Double totalTax = 0.0;
			if (selectedProductVarietyMaster.getId() == null) {
				// AppUtil.addError("Please select Product Variety Master");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
				return;
			}
			if (stockTransferItems.getUnitRate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.UNIT_RATE_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			Long ProductId = selectedProductVarietyMaster.getId();
			Double unitRate = stockTransferItems.getUnitRate();

			log.info("loadProductVerietyTaxPercentage :: ProductId==> " + ProductId);
			log.info("loadProductVerietyTaxPercentage :: unitRate==> " + unitRate);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/gettaxlistbyproductidunitrate/" + ProductId + "/" + unitRate;
			log.info("loadProductVerietyTaxPercentage :: url==> " + url);
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				List<ProductVarietyTax> proTaxList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyTax>>() {
						});
				if (proTaxList != null && !proTaxList.isEmpty()) {
					log.info("proTaxList size==> " + proTaxList.size());
					for (ProductVarietyTax proTax : proTaxList) {
						log.info("tax==> " + proTax.getTaxPercent());
						totalTax = totalTax + proTax.getTaxPercent();

					}
				} else {
					totalTax = 0.0;
					log.error("proTaxList is null or empty");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
				}
				log.info("totalTax==> " + totalTax);
				stockTransferItems.setTaxPercent(totalTax);
			} else {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
			}

		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	// invoice method start
	public void viewPdfFile() {
		try {
			String fileType;
			String localFilePath;
			log.info("View PdfFile Received ");

			String workingDir = System.getProperty("user.dir");
			String path = selectedPurchaseInvoice.getFilePath();
			workingDir += "/bin/main/invoiceFiles/";
			log.info("File Path is ==> " + path);
			log.info("Local Path is ==> " + workingDir);

			File fileName = new File(selectedPurchaseInvoice.getFilePath());
			log.info("Local File Name is ==> " + fileName.getName());
			currentFilePath = "/invoiceFiles/" + fileName.getName();
			String extension = FilenameUtils.getExtension(fileName.getName());
			if (extension.contains("pdf"))
				currentFileType = "application/pdf";
			else
				currentFileType = "jpg/jpg";
			log.info("Current FilePath is  ==> " + currentFilePath);
			log.info("Current File Type  is  ==> " + currentFileType);
			InputStream inputStream = null;
			OutputStream outputStream = null;
			try {
				inputStream = new FileInputStream(new File(selectedPurchaseInvoice.getFilePath()));
				outputStream = new FileOutputStream(new File(workingDir + fileName.getName()));
				byte[] buffer = new byte[(int) uploadDocumentFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("File Export to Local Directory to  Success ==> " + fileName.getName());
			} catch (Exception e) {
				log.error("File Export to Local Directory to  Error ==> " + fileName.getName());
				log.error("error to download file :" + e);
			}

		} catch (Exception e) {
			log.error("View PdfFile Error " + e);
		}
	}

	public void deleteOpenFile(String filePath) {
		try {
			log.info("File Delete Received ");
			File file = new File(filePath);
			if (file.exists()) {
				file.delete();
				log.info("File Delete Successfully ");
			} else
				log.info("File Delete Failed " + file.getAbsolutePath());

		} catch (Exception e) {
			log.error("File Delete Error " + e);
		}
	}

	public void uploadInvoiceDocument() {
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			String extension = null;
			if (size > 0 && uploadDocumentFile != null) {
				if (uploadDocumentFile != null && uploadDocumentFile.getFileName() != null) {
					extension = FilenameUtils.getExtension(uploadDocumentFile.getFileName());
				}
				String photoPathName = commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_PATH");
				log.info("Invoice PathName==>" + photoPathName);

				File file = new File(photoPathName);
				if (!file.exists()) {
					// throw new Exception(" == File path not exit in your data base ====");
					AppUtil.addWarn("File path not exit in your data base");
					return;
				}

				SimpleDateFormat sdf = new SimpleDateFormat("ddss");
				String photoPath = photoPathName + "/uploaded/" + purchaseInvoice.getInvoiceNumber()
						+ purchaseInvoice.getInvoiceNumberPrefix() + sdf.format(new Date()) + "." + extension;
				Path path = Paths.get(photoPathName + "/uploaded/");
				log.info(" Invoice Upload Path  : " + photoPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(path)) {
					Files.createDirectories(path);
				}
				double kilobytes = outputFile.length() / 1024;
				log.info("File kilobytes :" + kilobytes);
				inputStream = uploadDocumentFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) uploadDocumentFile.getSize()];
				int bytesRead = 0;
				if (inputStream != null) {
					while ((bytesRead = inputStream.read(buffer)) != -1) {
						outputStream.write(buffer, 0, bytesRead);
					}
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				jobApplication.setPhotographPath(photoPath);
				purchaseInvoice.setFilePath(photoPath);

				log.info(" Invoice Upload Path  Set Success fully : " + purchaseInvoice.getFilePath());
			}
		} catch (Exception e) {
			log.error("Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	public String handleFileUpload(FileUploadEvent event) {
		try {
			Long invoiceFileSize = Long
					.parseLong(commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_FILE_SIZE"));
			log.info("invoiceFileSize " + invoiceFileSize);
			InputStream inputStream = null;
			OutputStream outputStream = null;
			uploadDocumentFile = event.getFile();

			log.info(" Invoice file name :: " + uploadDocumentFile.getFileName());
			log.info(" Invoice Uploaded file Content Type : " + uploadDocumentFile.getContentType());
			String extension = FilenameUtils.getExtension(uploadDocumentFile.getFileName());
			if (!extension.equalsIgnoreCase("png") && !extension.equalsIgnoreCase("jpg")
					&& !extension.equalsIgnoreCase("gif") && !extension.equalsIgnoreCase("jpeg")
					&& !extension.equalsIgnoreCase("pdf")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVOICE_FORMAT_NOT_VALID.getErrorCode());
				log.info("File Format Not Valid ");
				return null;
			}
			fileName = uploadDocumentFile.getFileName();
			long size = uploadDocumentFile.getSize();
			log.info("size==>" + size);
			size = size / 1000;
			if (size > invoiceFileSize) {
				jobApplication.setPhotographPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVOICE_FILE_SIZE.getErrorCode());
				return null;
			}
			if (size > 0) {
				String photoPathName = commonDataService.getAppKeyValue("STOCK_ITEM_INWARD_PNS_INVOICE_PATH");
				log.info("Invoice PathName==>" + photoPathName);
				SimpleDateFormat sdf = new SimpleDateFormat("ddss");
				String photoPath = photoPathName + "/uploaded/" + purchaseInvoice.getInvoiceNumber()
						+ purchaseInvoice.getInvoiceNumberPrefix() + sdf.format(new Date()) + "." + extension;
				Path path = Paths.get(photoPathName + "/uploaded/");
				log.info(" Invoice Upload Path  : " + photoPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(path)) {
					Files.createDirectories(path);
				}
				double kilobytes = outputFile.length() / 1024;
				log.info("File kilobytes :" + kilobytes);
				if (kilobytes > 100) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_FILE_SIZE_INVALID.getErrorCode());
					return null;
				} else {
					AppUtil.addInfo("File Uploaded Successfully");
				}
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Upload Photo  :: ", e);
			// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			AppUtil.addWarn("File path not exit in your data base");
		}
		return null;
	}

	public String getInvoiceFileExtenstion(PurchaseInvoice purchaseInvoice) {
		String extension = FilenameUtils.getExtension(purchaseInvoice.getFilePath());
		return extension;
	}

	public void selectInvoiceRows(PurchaseInvoice stockIn) {
		log.info("************ Purchase Invoice Set Called : ********** ");
		selectedPurchaseInvoice = stockIn;
		// viewPdfFile();
		try {
			selectedeStockItemInwardPNSBean = new StockItemInwardPNSBean();
			log.info("************ path " + stockIn.getFilePath());
			if (stockIn.getFilePath() != null) {
				FileInputStream fin = new FileInputStream(stockIn.getFilePath());
				selectedeStockItemInwardPNSBean.setFileInputStream(fin);
				selectedeStockItemInwardPNSBean.setPurchaseInvoice(stockIn);
				String extension = FilenameUtils.getExtension(new File(stockIn.getFilePath()).getName());
				selectedeStockItemInwardPNSBean.setFileExtension(extension);
				log.info("************ Purchase Invoice Set Called 2: ********** ");
				invoiceFileDownload();
			} else {
				errorMap.notify(ErrorDescription.FILE_MOVEMENT_FILE_PATH.getErrorCode());
			}
		} catch (FileNotFoundException fex) {
			log.error("************selectInvoiceRows Error " + fex);
			errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
		} catch (Exception e) {
			log.error("************ Purchase Invoice Error " + e);
		}
	}

	// this is method is download uploaded files temp.
	public void invoiceFileDownload() {
		log.info("File Download Received ");
		InputStream targetStream = selectedeStockItemInwardPNSBean.fileInputStream;

		String ext = selectedeStockItemInwardPNSBean.fileExtension;
		String fileName = selectedeStockItemInwardPNSBean.purchaseInvoice.getInvoiceNumber() + "."
				+ selectedeStockItemInwardPNSBean.getFileExtension();
		if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
			log.info("IMG File Download Stream");
			selectedeStockItemInwardPNSBean.downloadFile = new DefaultStreamedContent(targetStream, "image/jpg",
					fileName);
		} else if (ext.contains("pdf")) {
			log.info("PDF File Download Stream");
			selectedeStockItemInwardPNSBean.downloadFile = new DefaultStreamedContent(targetStream, "application/pdf",
					fileName);
		} else if (ext.contains("doc")) {
			log.info("DOC File Download Stream");
			selectedeStockItemInwardPNSBean.downloadFile = new DefaultStreamedContent(targetStream, "doc", fileName);
		} else if (ext.contains("txt")) {
			log.info("TXT File Download Stream");
			selectedeStockItemInwardPNSBean.downloadFile = new DefaultStreamedContent(targetStream, "txt", fileName);
		}
		log.info("File Download Received Set done ******** Streamed Set Success ******* ");

	}

	public String generateRandomIdForNotCaching() {
		return java.util.UUID.randomUUID().toString();
	}

	public String itemInvoiceUpdateNew() {

		log.info("Invoice add received :");
		if (purchaseInvoice.getInvoiceNumber() == null) {
			log.info("Invalid Invoice Number");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getInvoiceDate() == null) {
			log.info("Invoice Date Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DATE.getErrorCode());
			return null;
		} else if (purchaseInvoice.getDcNumber() == null) {
			log.info("dcNumber Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DC.getErrorCode());
			return null;
		} else if (purchaseInvoice.getPaymentDuedate() == null) {
			log.info("Payment DueDate Not Valid");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_PAYMENT.getErrorCode());
			return null;
		} else if (uploadDocumentFile == null) {
			log.info("Invoice Document Not Found");
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DOC.getErrorCode());
			return null;
		}
		try {
			boolean interestApplicable = false;
			if (interestApplicablePayment.equalsIgnoreCase("Yes")) {
				interestApplicable = true;
			}
			purchaseInvoice.setInterestApplicable(interestApplicable);

			SimpleDateFormat sdf = new SimpleDateFormat("MMMyyyy");
			purchaseInvoice.setInvoiceNumberPrefix("PI".concat(sdf.format(new Date())));
			selectedPurchaseInvoiceList.add(purchaseInvoice);
			uploadInvoiceDocument();

			// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_ADDED_SUCCESS.getErrorCode());

			log.info("Invoice add Success :");
			purchaseInvoice = new PurchaseInvoice();
			uploadDocumentFile = null;
			fileName = null;
			setInterestApplicablePayment("Select");
		} catch (Exception e) {
			log.info("Invoice add Error :" + e);
		}
		return null;
	}

	public String itemInvoiceAddNew() {
		log.info("Invoice add received :");

		log.info("Invoice add received :");
		// if (purchaseInvoice.getInvoiceNumber() == null) {
		// log.info("Invalid Invoice Number");
		// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE.getErrorCode());
		// return null;
		// } else if (purchaseInvoice.getInvoiceDate() == null) {
		// log.info("Invoice Date Not Valid");
		// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DATE.getErrorCode());
		// return null;
		// } else if (purchaseInvoice.getDcNumber() == null) {
		// log.info("dcNumber Not Valid");
		// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DC.getErrorCode());
		// return null;
		// }
		if(null == purchaseInvoice.getOrderType()){
			AppUtil.addError("Please select order type");
			return null;
		}
		if (inwardType != null) {
			if (inwardType.equalsIgnoreCase("Direct Inward")) {
				if (purchaseInvoice == null || purchaseInvoice.getInvoiceNumber() == null) {
					errorMap.notify(ErrorDescription.INVOICE_NUMBER_EMPTY.getErrorCode());
					return null;
				} else if (purchaseInvoice.getInvoiceDate() == null) {
					errorMap.notify(ErrorDescription.INVOICE_DATE_EMPTY.getErrorCode());
					return null;
				}
			} else {
				if (!inwardType.equalsIgnoreCase("Warehouse Manual Inward")) {
					if (purchaseInvoice == null || purchaseInvoice.getInvoiceNumber() == null) {
						errorMap.notify(ErrorDescription.INVOICE_NUMBER_EMPTY.getErrorCode());
						return null;
					} else if (purchaseInvoice.getInvoiceDate() == null) {
						errorMap.notify(ErrorDescription.INVOICE_DATE_EMPTY.getErrorCode());
						return null;
					} else if (purchaseInvoice.getPaymentDuedate() == null) {
						log.info("Payment Due Date Not Valid");

						errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_PAYMENT.getErrorCode());
						return null;
					} else if (interestApplicablePayment == null || interestApplicablePayment.equals("Select")) {
						log.info("Interest Applicable for Delay Payment Not Valid");
						errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DELAY.getErrorCode());
						return null;
					} else if ((interestApplicablePayment.equalsIgnoreCase("Yes"))
							&& (purchaseInvoice.getRateOfInterest() == null
									|| purchaseInvoice.getRateOfInterest() <= 0)) {
						log.info("Rate of Interest Not Valid");
						errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_INTEREST.getErrorCode());
						return null;
					} else if (uploadDocumentFile == null) {
						log.info("Invoice Document Not Found");
						errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_DOC.getErrorCode());
						return null;
					}
				}
			}
		}

		try {
			log.info("New Invoice Details Start to Add");
			if (inwardType != null && !inwardType.equalsIgnoreCase("Warehouse Manual Inward")) {
				if (interestApplicablePayment.equalsIgnoreCase("Yes"))
					purchaseInvoice.setInterestApplicable(true);
				else
					purchaseInvoice.setInterestApplicable(false);
			} else {
				purchaseInvoice.setInterestApplicable(false);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("MMMyyyy");
			purchaseInvoice.setInvoiceNumberPrefix("PI".concat(sdf.format(new Date())));
			purchaseInvoiceList.add(purchaseInvoice);
			uploadInvoiceDocument();
			// errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_INVALID_INVOICE_ADDED_SUCCESS.getErrorCode());
			log.info("Invoice add Success ");
			purchaseInvoice = new PurchaseInvoice();
			uploadDocumentFile = null;
			fileName = null;
			setInterestApplicablePayment("Select");
		} catch (Exception e) {
			log.info("New Invoice Details add Error :" + e);
		}
		return null;
		// }
	}

	public void clearInvoiceTable() {
		purchaseInvoice = new PurchaseInvoice();
		uploadDocumentFile = null;
		fileName = null;
		setInterestApplicablePayment("Select");
		log.info("Invoice Clear Success:");
	}

	public void renderedInwardTypeSelectionEdit() {
		log.info("Rendered Inwared Change Listener Received ");
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();

		if (inwardType.equals("Direct Inward")) {
			renderedInward = false;
			addItemReceiveDetails = true;
			manualInwardrender = true;
		} else if (inwardType.equals("PO Based Inward")) {
			renderedInward = true;
			addItemReceiveDetails = false;
			manualInwardrender = true;
			log.info("size..." + purchaseOrderList.size());
		} else if (inwardType.equals("Warehouse Manual Inward")) {
			renderedInward = false;
			addItemReceiveDetails = true;
			manualInwardrender = false;
		} else {
			renderedInward = false;
			addItemReceiveDetails = false;
			log.warn("rendered inwared Invalid selection");
		}
	}

	public void renderedInwardTypeSelection() {
		log.info("Rendered Inwared Change Listener Received ");
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();

		if (inwardType.equals("Direct Inward")) {
			loadProductVarietyDetails();
			renderedInward = false;
			addItemReceiveDetails = true;
			manualInwardrender = true;
		} else if (inwardType.equals("PO Based Inward")) {
			loadPurchaseOrderNumber();
			renderedInward = true;
			addItemReceiveDetails = false;
			manualInwardrender = true;
			log.info("size..." + purchaseOrderList.size());
		} else if (inwardType.equals("Warehouse Manual Inward")) {
			loadProductVarietyDetails();
			renderedInward = false;
			manualInwardrender = false;
			addItemReceiveDetails = true;
		} else {
			renderedInward = false;
			addItemReceiveDetails = false;
			log.warn("rendered inwared Invalid selection");
		}
	}

	public void test() {
		log.info("Received supplier MasterTest ");
		try {
			log.info(selectedSupplierMasterTest.getName());
		} catch (Exception e) {
			log.info("Supplier MasterTest Error " + e);
		}
	}

	// Transport Service Type transportTypeMasterList
	public void loadTransportTypeMasterList() {

		log.info(":::<- Load TransportTypeMasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportTypeMasterList";
			log.info("::: TransportTypeMasterList Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get TransportTypeMasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportTypeMasterList = new ArrayList<TransportTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<TransportTypeMaster>>() {
						});
				log.info("TransportTypeMasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportTypeMasterList Page Convert Succes -->");
			} else {
				log.warn("TransportTypeMasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportTypeMasterList load ", ee);
		}
	}

	public void transportTypeMasterListener() {
		log.info("Received Listener of selectedTransportTypeMaster **************");
		try {
			transportServiceMasterList = new ArrayList<TransportMaster>();
			loadTransportServiceList(selectedTransportTypeMaster.getId());
		} catch (Exception se) {
			log.error("SET selectedTransportTypeMaster Get Error " + se);
		}
		log.info("Done selectedTransportTypeMaster **************");
	}

	// Transport Service Name
	public void loadTransportServiceList(Long id) {

		log.info(":::<- Load TransportService MasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportServiceMasterList";
			log.info("::: TransportService MasterList Controller calling  1 :::");
			TransportMaster mas = new TransportMaster();
			mas.setId(id);
			baseDTO = httpService.post(url, mas);
			log.info("::: get TransportService MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportServiceMasterList = new ArrayList<TransportMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportServiceMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TransportMaster>>() {
				});
				log.info("TransportService MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportService MasterList Page Convert Succes -->");
			} else {
				log.warn("TransportService MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportService MasterList load ", ee);
		}
	}

	public String submitData() {

		log.info("Received Submit Data ******************** ");
		if (StringUtils.isNotEmpty(inwardFrom)) {
			if (inwardFrom.equals("Supplier")) {
				if (selectedSupplierTypeMaster == null || selectedSupplierTypeMaster.getId() == null) {
					errorMap.notify(
							ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_TYPE_MASTER.getErrorCode());
					return null;
				}
				if (circleflag) {
					if (selectedCircleMaster == null || selectedCircleMaster.getId() == null) {
						errorMap.notify(
								ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_CIRCLE_MASTER.getErrorCode());
						return null;
					}
				}
				if (selectedSupplierMaster == null || selectedSupplierMaster.getId() == null) {
					errorMap.notify(ErrorDescription.STOCK_TRANS_SUPPLIER_REQUIRED.getErrorCode());
					return null;
				}
			}
		} else {
			AppUtil.addWarn("Please Select Inward From");
			return null;
		}

		// transport check
		log.info("wayBillAvailable :" + waybillRendered + ",");
		if (selectedTransportTypeMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
			return null;
		}
		if (selectedTransportMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
			return null;
		}
		if (wayBillAvailable == null || wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
			return null;
		}

		if (waybillRendered == true && (wayBillNumber == null || wayBillNumber.trim().length() <= 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble == null || transportChargeAvailble.equalsIgnoreCase("Select Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
			return null;
		}
		if (transportChargeAvailble.equalsIgnoreCase("Yes")
				&& (transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			if (stockTransfer.getTransportChargeAmount() == null || this.stockTransfer.getTransportChargeAmount() < 0) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
				return null;
			}
		}

		if (transportChargeType.equalsIgnoreCase("Yes") && (this.stockTransfer.getTransportChargeAmount() == 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeType.equals("Cash")) {
			this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
		} else {
			this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
		}
		this.stockTransfer.setWeight(weight);
		this.stockTransfer.setReceiptNumber(receiptNumber);

		if (this.stockTransfer == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_STOCK_NULL.getErrorCode());
			return null;
		}
		this.stockTransfer.setTransportChargeAvailable(transportChargeRendered);
		this.stockTransfer.setWaybillAvailable(waybillRendered);
		if (waybillRendered) {
			stockTransfer.setWaybillNumber(wayBillNumber);
			log.info("WayBill Value Set Success ******************** ");
		}
		
		if (StringUtils.isNotEmpty(inwardType)) {
			if (inwardType.equals("Direct Inward")) {
				this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_INWARD);
				log.info("Direct Inward Save Processed ******************** ");
				if (stockTransferItemsInwardUpdateList == null || stockTransferItemsInwardUpdateList.size() == 0) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TABLE_NULL.getErrorCode());
					return null;
				}
			} else if (inwardType.equals("PO Based Inward")) {
				this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_PO_BASED);
				log.info("PO Based Inward Save Processed ******************** ");
				if (collectorSupplierItemList == null || collectorSupplierItemList.size() == 0) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SUPPLIER_ITEM_NULL.getErrorCode());
					return null;
				}
			} else if (inwardType.equals("Warehouse Manual Inward")) {
				this.stockTransfer.setTransferType(StockTransferType.WAREHOUSE_INWARD);
				if (selectedSupplierMaster != null && purchaseInvoice.getInvoiceNumber() != null) {
					if (checkInvoiceNumber(selectedSupplierMaster, purchaseInvoice.getInvoiceNumber())) {
						log.info("InvoiceNumber already exists----");
						AppUtil.addError("Invoice Number Already Exists");
						return null;
					}
				}
			}
		}
		if(null == purchaseInvoice.getOrderType()){
			AppUtil.addError("Please select order type");
			return null;
		}
		selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);
		this.stockTransfer.setTransportMaster(selectedTransportMaster); // Type of Transport serviceType
		this.stockTransfer.setSupplierMaster(selectedSupplierMaster); // Supplier Type
		if (entityMaster != null && entityMaster.getId() != null) {
			this.stockTransfer.setFromEntityMaster(entityMaster); // EntityMaster for other
		} else {
			this.stockTransfer.setFromEntityMaster(headOrRegionOffice); // EntityMaster for HeadOffice
		}

		StockItemInwardPNSDTO stock = new StockItemInwardPNSDTO();

		this.stockTransfer.setDateReceived(new Date());
		this.stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		this.stockTransfer.setMessage(this.description);

		// invoice object creation start

		int purchaseInvoiceListSize = purchaseInvoiceList != null ? purchaseInvoiceList.size() : 0;
		if (purchaseInvoiceListSize > 0) {
			for (PurchaseInvoice invoice : purchaseInvoiceList) {
				invoice.setSupplierMaster(selectedSupplierMaster);
				invoice.setStockTransfer(this.stockTransfer);
				invoice.setStatus(StockTransferStatus.SUBMITTED.toString());
			}
		}

		if ("Warehouse Manual Inward".equals(inwardType)) {
			if (purchaseInvoice == null || purchaseInvoice.getInvoiceNumber() == null) {
				errorMap.notify(ErrorDescription.INVOICE_NUMBER_EMPTY.getErrorCode());
				return null;
			}

			if (purchaseInvoice.getInvoiceDate() == null) {
				errorMap.notify(ErrorDescription.INVOICE_DATE_EMPTY.getErrorCode());
				return null;
			}
		} else {
			if (purchaseInvoiceListSize == 0) {
				AppUtil.addWarn("Please fill invoice details.");
				return null;
			}
		}

		log.info("Invoice Set Successfully -->");

		log.info("Received Submit Data Start ********************");
		stockTransferItemsList = new ArrayList<StockTransferItems>();

		if (inwardFrom.equals("Entity")) {
			this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_INWARD);
			stockTransferItemsInwardList = new ArrayList<>();
			stockTransferItemsInwardList = stockInwardFromEntity(stockTransferItemsInwardUpdateList);
			stock.setStockTransferItemsList(stockTransferItemsInwardList);
		}

		if (StringUtils.isNotEmpty(inwardType)) {
			if (inwardType.equals("PO Based Inward")) {
				for (PurchaseOrderItems items : collectorSupplierItemList) {
					StockTransferItems stockTransferItems = null;
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setOrderedQty(items.getItemQty());
					stockTransferItems.setItemAmount(items.getItemAmount());
					if (items.getUnitRate() != null)
						stockTransferItems.setUnitRate(items.getUnitRate());
					if (items.getItemTotal() != null) {
						stockTransferItems.setItemTotal(items.getItemTotal());
						stockTransferItems.setItemNetTotal(items.getItemTotal());
					}

					if (items.getTaxPercent() != null)
						stockTransferItems.setTaxPercent(items.getTaxPercent());
					if (items.getTaxValue() != null) {
						stockTransferItems.setTaxValue(items.getTaxValue());
						stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() + items.getTaxValue());
					}

					if (items.getDiscountPercent() != null)
						stockTransferItems.setDiscountPercent(items.getDiscountPercent());
					if (items.getDiscountValue() != null) {
						stockTransferItems.setDiscountValue(items.getDiscountValue());
						stockTransferItems
								.setItemNetTotal(stockTransferItems.getItemNetTotal() - items.getDiscountValue());
					}
					if (items.getUomMaster() != null)
						stockTransferItems.setUomMaster(items.getUomMaster());

					stockTransferItems.setSupplierMaster(selectedSupplierMaster);
					stockTransferItems.setProductVarietyMaster(items.getProductVarietyMaster());
					stockTransferItems.setStockTransfer(stockTransfer);

					stockTransferItems.setPurchaseOrder(selectedPurchaseOrder);

					stockTransferItemsList.add(stockTransferItems);
					log.info(":::<- Add List data -> :::" + items.getProductVarietyMaster().getCode() + " "
							+ items.getProductVarietyMaster().getName() + ",ID ->" + items.getId());

				}
				stock.setStockTransferItemsList(stockTransferItemsList);

			} else if (inwardType.equals("Direct Inward") || inwardType.equals("Warehouse Manual Inward")) {
				stockTransferItemsInwardList = new ArrayList<>();
				for (StockTransferItems stockItems : stockTransferItemsInwardUpdateList) {
					StockTransferItems items = new StockTransferItems();
					if (inwardType.equals("Warehouse Manual Inward") && stockItems.getAtNumber() == null
							|| inwardType.equals("Warehouse Manual Inward") && stockItems.getAtNumber().equals("")) {

						if (stockItems.getProductVarietyMaster().getUomMaster().getCode().equals("METR")) {

							items = new StockTransferItems();

							items.setSupplierMaster(selectedSupplierMaster);
							items.setStockTransfer(stockTransfer);

							items.setProductVarietyMaster(stockItems.getProductVarietyMaster());
							if (stockItems.getProductVarietyMaster().getUomMaster() != null) {
								items.setUomMaster(stockItems.getProductVarietyMaster().getUomMaster());
							}
							items.setOrderedQty(stockItems.getOrderedQty());
							items.setItemTotal(stockItems.getUnitRate());
							items.setUnitRate(stockItems.getUnitRate());
							items.setItemNetTotal(items.getItemTotal() * stockItems.getOrderedQty());
							items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());
							items.setReceivedQty((double) items.getOrderedQty());
							items.setPurchasePrice(stockItems.getUnitRate());
							items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());

							if (stockItems.getDescription() != null) {
								items.setDescription(stockItems.getDescription());
							}

							double total = stockItems.getItemTotal();
							if (stockItems.getDiscountPercent() != null) {
								if (stockItems.getDiscountPercent() > 0) {
									double discountV = (total * stockItems.getDiscountPercent()) / 100;
									items.setDiscountValue(discountV);
									items.setDiscountPercent(stockItems.getDiscountPercent());
									items.setItemNetTotal(stockItems.getItemNetTotal() - stockItems.getDiscountValue());
								}
							}

							if (stockItems.getTaxPercent() != null) {
								if (stockItems.getTaxPercent() > 0) {
									double tax = stockItems.getTaxPercent();
									double taxV = (total * tax) / 100;
									items.setTaxValue(taxV);
									double taxValue = roundOffTaxValue(taxV);
									items.setTaxValue(taxValue);
									items.setTaxPercent(stockItems.getTaxPercent());
									items.setItemNetTotal(stockItems.getItemNetTotal());
								}
							}
							//
							// if (stockItems.getAtNumber() == null || stockItems.getAtNumber().equals(""))
							// {
							items.setAtNumber("");

							stockTransferItemsInwardList.add(items);
						} else {

							int count = stockItems.getOrderedQty().intValue();
							for (int i = 1; i <= count; i++) {
								items = new StockTransferItems();

								items.setSupplierMaster(selectedSupplierMaster);
								items.setStockTransfer(stockTransfer);

								items.setProductVarietyMaster(stockItems.getProductVarietyMaster());
								if (stockItems.getProductVarietyMaster().getUomMaster() != null) {
									items.setUomMaster(stockItems.getProductVarietyMaster().getUomMaster());
								}
								items.setOrderedQty(1D);
								items.setItemTotal(stockItems.getUnitRate() * items.getOrderedQty());
								items.setUnitRate(stockItems.getUnitRate());
								items.setItemNetTotal(items.getItemTotal());
								items.setItemAmount(stockItems.getUnitRate() * items.getOrderedQty());
								items.setReceivedQty((double) items.getOrderedQty());
								items.setPurchasePrice(stockItems.getUnitRate());
								items.setItemAmount(stockItems.getUnitRate() * items.getOrderedQty());

								if (stockItems.getDescription() != null) {
									items.setDescription(stockItems.getDescription());
								}

								double total = items.getItemTotal();
								if (stockItems.getDiscountPercent() != null) {
									if (stockItems.getDiscountPercent() > 0) {
										double discountV = (total * stockItems.getDiscountPercent()) / 100;
										items.setDiscountValue(discountV);
										items.setDiscountPercent(stockItems.getDiscountPercent());
										items.setItemNetTotal(items.getItemNetTotal() - items.getDiscountValue());
									}
								}

								if (stockItems.getTaxPercent() != null) {
									if (stockItems.getTaxPercent() > 0) {
										double tax = stockItems.getTaxPercent();
										double taxV = 0.0;
										if (stockItems.getDiscountPercent() != null) {
											if (stockItems.getDiscountPercent() > 0) {
												taxV = (items.getItemNetTotal() * tax) / 100;
											} else {
												taxV = (total * tax) / 100;
											}
										} else {
											taxV = (total * tax) / 100;
										}
										items.setTaxValue(taxV);
										double taxValue = roundOffTaxValue(taxV);
										items.setTaxValue(taxValue);
										items.setTaxPercent(stockItems.getTaxPercent());
										items.setItemNetTotal(items.getItemNetTotal() + items.getTaxValue());
									}
								}
								//
								// if (stockItems.getAtNumber() == null || stockItems.getAtNumber().equals(""))
								// {
								items.setAtNumber("");
								// }else {
								// items.setAtNumber(stockItems.getAtNumber());
								// }
								// stockTransferItems.setItemNetTotal( (stockTransferItems.getItemTotal() +
								// stockTransferItems.getTaxValue() ) -
								// (stockTransferItems.getDiscountValue()));
								stockTransferItemsInwardList.add(items);
							}

						}

						//
					} else {

						items.setSupplierMaster(selectedSupplierMaster);
						items.setStockTransfer(stockTransfer);
						items.setProductVarietyMaster(stockItems.getProductVarietyMaster());
						if (stockItems.getProductVarietyMaster().getUomMaster() != null) {
							items.setUomMaster(stockItems.getProductVarietyMaster().getUomMaster());
						}
						items.setOrderedQty(stockItems.getOrderedQty());
						items.setItemTotal(stockItems.getUnitRate() * stockItems.getOrderedQty());
						items.setUnitRate(stockItems.getUnitRate());
						items.setItemNetTotal(items.getItemTotal());
						items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());
						items.setReceivedQty((double) items.getOrderedQty());
						items.setPurchasePrice(stockItems.getUnitRate());
						items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());
						if (stockItems.getDescription() != null) {
							items.setDescription(stockItems.getDescription());
						}
						double total = items.getItemTotal();
						if (stockItems.getDiscountPercent() != null) {
							if (stockItems.getDiscountPercent() > 0) {
								double discountV = (total * stockItems.getDiscountPercent()) / 100;
								items.setDiscountValue(discountV);
								items.setDiscountPercent(stockItems.getDiscountPercent());
								items.setItemNetTotal(items.getItemNetTotal() - items.getDiscountValue());
							}
						}

						if (stockItems.getTaxPercent() != null) {
							if (stockItems.getTaxPercent() > 0) {
								double tax = stockItems.getTaxPercent();
								double taxV = 0.0;
								if (stockItems.getDiscountPercent() != null) {
									if (stockItems.getDiscountPercent() > 0) {
										taxV = (items.getItemNetTotal() * tax) / 100;
									} else {
										taxV = (total * tax) / 100;
									}
								} else {
									taxV = (total * tax) / 100;
								}
								items.setTaxValue(taxV);
								double taxValue = roundOffTaxValue(taxV);
								items.setTaxValue(taxValue);
								items.setTaxPercent(stockItems.getTaxPercent());
								items.setItemNetTotal(items.getItemNetTotal() + items.getTaxValue());
							}
						}

						if (stockItems.getAtNumber() == null || stockItems.getAtNumber().equals("")) {
							items.setAtNumber("");
						} else {
							items.setAtNumber(stockItems.getAtNumber());
						}
						// stockTransferItems.setItemNetTotal( (stockTransferItems.getItemTotal() +
						// stockTransferItems.getTaxValue() ) -
						// (stockTransferItems.getDiscountValue()));
						stockTransferItemsInwardList.add(items);
					}
					// else {
					//
					// items.setProductVarietyMaster(selectedProductVarietyMaster);
					// items.setUomMaster(selectedProductVarietyMaster.getUomMaster());
					// items.setOrderedQty(stockItems.getOrderedQty());
					// items.setItemTotal(stockItems.getUnitRate() * stockItems.getOrderedQty());
					// items.setItemNetTotal(stockItems.getItemTotal());
					// items.setReceivedQty((double) stockItems.getOrderedQty());
					// items.setPurchasePrice(stockItems.getUnitRate());
					// items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());
					//
					// items.setUnitRate(stockItems.getUnitRate());
					//
					// double total = stockItems.getItemTotal();
					// if (stockItems.getDiscountPercent() != null) {
					// if (stockItems.getDiscountPercent() > 0) {
					// double discountV = (total * stockItems.getDiscountPercent()) / 100;
					// items.setDiscountValue(discountV);
					// items.setDiscountPercent(stockItems.getDiscountPercent());
					// items.setItemNetTotal(stockItems.getItemNetTotal() -
					// stockItems.getDiscountValue());
					// }
					// }
					//
					// if (stockItems.getTaxPercent() != null) {
					// if (stockItems.getTaxPercent() > 0) {
					// double tax = stockItems.getTaxPercent();
					// double taxV = (total * tax) / 100;
					// items.setTaxValue(taxV);
					// items.setTaxPercent(stockItems.getTaxPercent());
					// items.setItemNetTotal(stockItems.getItemNetTotal());
					// }
					// }
					// if (stockItems.getAtNumber() != null) {
					// items.setAtNumber(stockItems.getAtNumber());
					// }
					//
					// // stockTransferItems.setItemNetTotal( (stockTransferItems.getItemTotal() +
					// // stockTransferItems.getTaxValue() ) -
					// // (stockTransferItems.getDiscountValue()));
					// stockTransferItemsInwardList.add(items);
					//
					// }

				}
				stock.setStockTransferItemsList(stockTransferItemsInwardList);
			}
		}

		if (societyProductAppraisal != null) {
			if (societyProductAppraisal.getWeaversName() != null && selectedSupplierMaster != null) {
				stock.setSelectedSocietyProductAppraisal(societyProductAppraisal);
			}
		}
		if (StringUtils.isNotEmpty(inwardType)) {
			if (inwardType.equalsIgnoreCase("Warehouse Manual Inward")) {
				String validate = itemInvoiceAddNew();
				if (validate != null) {
					return validate;
				}
			}
		}

		stock.setStockTransfer(stockTransfer);
		stock.setPurchaseInvoiceList(purchaseInvoiceList);
		log.info(":::<- Start to save Data->:::");

		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/saveStockTransferItems";
			log.info("::: saveStockInwardAll Controller calling  1 :::");
			baseDTO = httpService.post(url, stock);
			log.info("::: get saveStockInwardAll Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				log.info("saveStockInwardAll Page Convert Succes -->");
				countAtNumberGenerator = 1;
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_SUCCESS.getErrorCode());
				return INPUT_FORM_LIST_URL;
			} else {
				log.warn("saveStockInwardAll Error *** :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in saveStockInwardAll load ", ee);
		}

		return null;
	}

	private List<StockTransferItems> stockInwardFromEntity(
			List<StockTransferItems> stockTransferItemsInwardUpdateList) {
		log.info("stockInwardFromEntity method starts----------");
		List<StockTransferItems> stockTransferItemsInwardList = new ArrayList<>();
		for (StockTransferItems stockItems : stockTransferItemsInwardUpdateList) {
			StockTransferItems items = new StockTransferItems();
			if (stockItems.getProductVarietyMaster().getUomMaster().getCode().equals("METR")) {
				items = new StockTransferItems();
				items.setStockTransfer(stockTransfer);
				items.setProductVarietyMaster(stockItems.getProductVarietyMaster());
				if (stockItems.getProductVarietyMaster().getUomMaster() != null) {
					items.setUomMaster(stockItems.getProductVarietyMaster().getUomMaster());
				}
				items.setOrderedQty(stockItems.getOrderedQty());
				items.setItemTotal(stockItems.getUnitRate());
				items.setUnitRate(stockItems.getUnitRate());
				items.setItemNetTotal(items.getItemTotal() * stockItems.getOrderedQty());
				items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());
				items.setReceivedQty((double) items.getOrderedQty());
				items.setPurchasePrice(stockItems.getUnitRate());
				items.setItemAmount(stockItems.getUnitRate() * stockItems.getOrderedQty());

				if (stockItems.getDescription() != null) {
					items.setDescription(stockItems.getDescription());
				}

				double total = stockItems.getItemTotal();
				if (stockItems.getDiscountPercent() != null) {
					if (stockItems.getDiscountPercent() > 0) {
						double discountV = (total * stockItems.getDiscountPercent()) / 100;
						items.setDiscountValue(discountV);
						items.setDiscountPercent(stockItems.getDiscountPercent());
						items.setItemNetTotal(stockItems.getItemNetTotal() - stockItems.getDiscountValue());
					}
				}

				if (stockItems.getTaxPercent() != null) {
					if (stockItems.getTaxPercent() > 0) {
						double tax = stockItems.getTaxPercent();
						double taxV = (total * tax) / 100;
						items.setTaxValue(taxV);
						double taxValue = roundOffTaxValue(taxV);
						items.setTaxValue(taxValue);
						items.setTaxPercent(stockItems.getTaxPercent());
						items.setItemNetTotal(stockItems.getItemNetTotal());
					}
				}
				if (stockItems.getAtNumber() == null || stockItems.getAtNumber().equals("")) {
					items.setAtNumber("");
				} else {
					items.setAtNumber(stockItems.getAtNumber());
				}
				stockTransferItemsInwardList.add(items);
			} else {
				int count = stockItems.getOrderedQty().intValue();
				for (int i = 1; i <= count; i++) {
					items = new StockTransferItems();

					items.setSupplierMaster(selectedSupplierMaster);
					items.setStockTransfer(stockTransfer);

					items.setProductVarietyMaster(stockItems.getProductVarietyMaster());
					if (stockItems.getProductVarietyMaster().getUomMaster() != null) {
						items.setUomMaster(stockItems.getProductVarietyMaster().getUomMaster());
					}
					items.setOrderedQty(1D);
					items.setItemTotal(stockItems.getUnitRate() * items.getOrderedQty());
					items.setUnitRate(stockItems.getUnitRate());
					items.setItemNetTotal(items.getItemTotal());
					items.setItemAmount(stockItems.getUnitRate() * items.getOrderedQty());
					items.setReceivedQty((double) items.getOrderedQty());
					items.setPurchasePrice(stockItems.getUnitRate());
					items.setItemAmount(stockItems.getUnitRate() * items.getOrderedQty());

					if (stockItems.getDescription() != null) {
						items.setDescription(stockItems.getDescription());
					}

					double total = items.getItemTotal();
					if (stockItems.getDiscountPercent() != null) {
						if (stockItems.getDiscountPercent() > 0) {
							double discountV = (total * stockItems.getDiscountPercent()) / 100;
							items.setDiscountValue(discountV);
							items.setDiscountPercent(stockItems.getDiscountPercent());
							items.setItemNetTotal(items.getItemNetTotal() - items.getDiscountValue());
						}
					}

					if (stockItems.getTaxPercent() != null) {
						if (stockItems.getTaxPercent() > 0) {
							double tax = stockItems.getTaxPercent();
							double taxV = 0.0;
							if (stockItems.getDiscountPercent() != null) {
								if (stockItems.getDiscountPercent() > 0) {
									taxV = (items.getItemNetTotal() * tax) / 100;
								} else {
									taxV = (total * tax) / 100;
								}
							} else {
								taxV = (total * tax) / 100;
							}
							items.setTaxValue(taxV);
							double taxValue = roundOffTaxValue(taxV);
							items.setTaxValue(taxValue);
							items.setTaxPercent(stockItems.getTaxPercent());
							items.setItemNetTotal(items.getItemNetTotal() + items.getTaxValue());
						}
					}
					if (stockItems.getAtNumber() == null || stockItems.getAtNumber().equals("")) {
						items.setAtNumber("");
					} else {
						items.setAtNumber(stockItems.getAtNumber());
					}
					stockTransferItemsInwardList.add(items);
				}
			}
		}
		log.info("stockInwardFromEntity method ends----------");
		return stockTransferItemsInwardList;
	}

	private boolean checkInvoiceNumber(SupplierMaster supplierMaster, Integer invoiceNumber) {
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/checkinvoicenumber/";
		PurchaseInvoice invoice = new PurchaseInvoice();
		invoice.setSupplierMaster(supplierMaster);
		invoice.setInvoiceNumber(invoiceNumber);
		BaseDTO baseDTO = httpService.post(url, invoice);
		if (baseDTO != null) {
			if (baseDTO.getStatusCode().equals(ErrorDescription.INVOICE_NUMBER_EMPTY.getErrorCode())) {
				return true;
			}
		}
		return false;
	}

	public String updateStockTransfer() {
		boolean valid = true;
		if (validateData(valid)) {

			StockItemInwardPNSDTO stock = new StockItemInwardPNSDTO();
			stockTransferItemsList = new ArrayList<StockTransferItems>();

			log.info(" ========>>>>  calling  in setAllValuesToStockTransferPNS in updateStockTransfer ======>>> ");
			setAllValuesToStockTransferPNS();

			log.info(" ========>>>> Calling checkInwardType to in updateStockTransfer ======>>> ");
			stock = checkInwardType();

			stock.setStockTransfer(selectedStockTransferPNS);
			stock.getStockTransfer().setTransportMaster(selectedTransportMaster);
			stock.setStockTransferItemsDeleteList(stockTransferItemsInwardDeleteList);
			if (selectedPurchaseInvoiceList != null && selectedPurchaseInvoiceList.size() > 0) {
				for (PurchaseInvoice invoice : selectedPurchaseInvoiceList) {
					invoice.setSupplierMaster(selectedSupplierMaster);
					invoice.setStockTransfer(selectedStockTransferPNS);
				}
			}

			stock.setPurchaseInvoiceList(selectedPurchaseInvoiceList);

			log.info(":::<- Start to update Data->:::");

			try {

				log.info("::: Update StockInwardAll Controller calling  1 :::");

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/stockItemInwardPNS/updateStockTransferInward";
				BaseDTO baseDTO = httpService.post(url, stock);
				log.info("::: get Update StockInwardAll Response :");
				if (baseDTO.getStatusCode() == 0) {
					// ObjectMapper mapper = new ObjectMapper();
					log.info("Update StockInwardAll Page Convert Succes -->");
					log.warn("Update Stock Item Inward Successfully Completed ******");
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_SUCCESS.getErrorCode());
					return INPUT_FORM_LIST_URL;
				} else {
					log.warn("Update StockInwardAll Error *** :");
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_ERROR.getErrorCode());
					Util.addError("Update StockItemInward Error");
				}
			} catch (Exception ee) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_UPDATE_EXCEPTION.getErrorCode());
				log.error("Exception Occured in Update StockInwardAll load ", ee);
			}
		}

		return null;
	}

	private Boolean validateData(Boolean valid) {
		log.info("::::::: Inside ValidationData Method:::::::");
		if (selectedStockTransferPNS == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_STOCK_NULL.getErrorCode());
			valid = false;
			return false;
		}
		log.info("Received Update ID ******************** " + selectedStockTransferPNS != null
				? selectedStockTransferPNS.getId()
				: "Null");
		// transport check
		if (selectedTransportTypeMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
			valid = false;
			return false;
		}
		if (selectedTransportMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
			valid = false;
			return false;
		}
		if (wayBillAvailable == null || wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
			valid = false;
			return false;
		}

		if (waybillRendered == true && (wayBillNumber == null || wayBillNumber.trim().length() <= 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			valid = false;
			return false;
		}
		if (transportChargeAvailble == null || transportChargeAvailble.equalsIgnoreCase("Select Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
			valid = false;
			return false;
		}
		if (transportChargeAvailble.equalsIgnoreCase("Yes")
				&& (transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			valid = false;
			return false;
		}

		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			if (selectedStockTransferPNS.getTransportChargeAmount() == null
					|| this.selectedStockTransferPNS.getTransportChargeAmount() < 0) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
				valid = false;
				return false;
			}
		}

		return valid;
	}

	public StockItemInwardPNSDTO checkInwardType() {
		StockItemInwardPNSDTO stockTransfer = new StockItemInwardPNSDTO();
		if (inwardType.equals("PO Based Inward")) {
			log.info(" === >>>>   Inside CheckInwardType  PO Based Inward ******************** ");
			setStockTransferItemsList();

		} else if (inwardType.equals("Direct Inward") || inwardType.equals("Warehouse Manual Inward")) {
			log.info(
					" === >>>>   Inside CheckInwardType  Direct Inward Or Warehouse Manual Inward  ******************** ");
			if (stockTransferItemsInwardUpdateList != null && stockTransferItemsInwardUpdateList.size() > 0) {
				for (StockTransferItems stockItems : stockTransferItemsInwardUpdateList) {

					stockItems.setSupplierMaster(selectedSupplierMaster);
					stockItems.setStockTransfer(selectedStockTransferPNS);
				}
				stockTransfer.setStockTransferItemsList(stockTransferItemsInwardUpdateList);
			}

		}
		return stockTransfer;

	}

	public void setStockTransferItemsList() {
		log.info(" === >>>>   Inside setStockTransferItemsList  PO Based Inward ******************** ");
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/updateStockTransferPOBased";
		log.info("PO Based Inward Save Processed ******************** ");
		if (collectorSupplierItemList != null && collectorSupplierItemList.size() > 0) {
			for (PurchaseOrderItems items : collectorSupplierItemList) {

				stockTransferItems = new StockTransferItems();
				stockTransferItems.setOrderedQty(items.getItemQty());
				stockTransferItems.setItemAmount(items.getItemAmount());
				stockTransferItems.setId(items.getId());

				if (items.getUnitRate() != null)
					stockTransferItems.setUnitRate(items.getUnitRate());
				if (items.getItemTotal() != null) {
					stockTransferItems.setItemTotal(items.getItemTotal());
					stockTransferItems.setItemNetTotal(items.getItemTotal());
				}

				if (items.getTaxPercent() != null)
					stockTransferItems.setTaxPercent(items.getTaxPercent());
				if (items.getTaxValue() != null) {
					stockTransferItems.setTaxValue(items.getTaxValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() + items.getTaxValue());
				}

				if (items.getDiscountPercent() != null)
					stockTransferItems.setDiscountPercent(items.getDiscountPercent());
				if (items.getDiscountValue() != null) {
					stockTransferItems.setDiscountValue(items.getDiscountValue());
					stockTransferItems.setItemNetTotal(stockTransferItems.getItemNetTotal() - items.getDiscountValue());
				}
				if (items.getUomMaster() != null)
					stockTransferItems.setUomMaster(items.getUomMaster());

				stockTransferItems.setStockTransfer(selectedStockTransferPNS);

				log.info(":::<- Going to set Purhase Order->:::");

				stockTransferItemsList.add(stockTransferItems);
				log.info(":::<- Add List data -> :::" + items.getProductVarietyMaster().getCode() + " "
						+ items.getProductVarietyMaster().getName());
			}
		}

	}

	public void setAllValuesToStockTransferPNS() {

		if (transportChargeType.equals("Cash")) {
			selectedStockTransferPNS.setTransportChargeType(TransportChargeType.Pay);
		} else {
			selectedStockTransferPNS.setTransportChargeType(TransportChargeType.ToPay);
		}
		selectedStockTransferPNS.setWaybillAvailable(waybillRendered);
		if (waybillRendered) {
			selectedStockTransferPNS.setWaybillNumber(wayBillNumber);
			selectedStockTransferPNS.setWeight(weight);
			log.info("WayBill Value Set Success ******************** ");
		}
		selectedStockTransferPNS.setModifiedDate(new Date());
		selectedStockTransferPNS.setMessage(this.description);
		selectedStockTransferPNS.setTransportChargeAvailable(transportChargeRendered);
		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			selectedStockTransferPNS.setReceiptNumber(receiptNumber);
		}
		selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);

	}

	public void resetData() {
		log.info("Reset Data Received ");
		try {
			collectorSupplierItemList.clear();

			wayBillAvailable = "Select Waybill Type";
			transportChargeAvailble = "Select Type";
			purchaseOrderItems = new PurchaseOrderItems();
			selectedProductVarietyMaster = new ProductVarietyMaster();
			selectedPurchaseOrder = new PurchaseOrder();
			selectedSupplierTypeMaster = new SupplierTypeMaster();
			selectedSupplierMaster = null;
			selectedTransportTypeMaster = new TransportTypeMaster();
			this.wayBillNumber = null;
			loadLazyIntRequirementList();
			this.transchargeChargeAmount = 0d;
			selectedTransportMaster = new TransportMaster();

		} catch (Exception e) {
			log.info("Reset All Data Error " + e);
		}
		log.info("Reset All Data Done");
	}

	public void renderedWaybillNumber() {
		log.info("rendered WaybillNumber received ");
		if (wayBillAvailable.equalsIgnoreCase("YES")) {
			waybillRendered = true;
		} else {
			waybillRendered = false;
		}
	}

	public void renderedTransportChargeRendered() {
		log.info("rendered transportChargeRendered received ");
		if (transportChargeAvailble.equalsIgnoreCase("YES")) {
			transportChargeRendered = true;
		} else {
			transportChargeRendered = false;
		}
	}
	// sum of bottom datatable total value calculation

	public String getUnitRateTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getUnitRateTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getUnitRateTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getUnitRate() != null)
				total += collector.getUnitRate();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getTotalRateTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal(); // Check total after
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getTotalRateTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getTotalRateTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getDiscountValueTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getDiscountValueTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getDiscountValueTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getDiscountValue() != null)
				total += collector.getDiscountValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTaxValueTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public String getTaxValueTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		log.info("total tax value---------" + total);
		Double totalTaxValue = roundOffTaxValue(total);
		return new DecimalFormat("###,###.00").format(totalTaxValue);
	}

	public String getTaxValueTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getTaxValue() != null)
				total += collector.getTaxValue();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getNetPriceTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getItemTotal() != null)
				total += collector.getItemTotal();
			// total += (total - purchaseOrderItems.discountValue) + (
			// purchaseOrderItems.taxValue ) ;
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getNetPriceTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getItemNetTotal() != null)
				total += collector.getItemNetTotal();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getNetPriceTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getItemNetTotal() != null)
				total += collector.getItemNetTotal();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getQuantityTotal() {
		double total = 0;
		for (PurchaseOrderItems collector : getCollectorSupplierItemList()) {
			if (collector.getItemQty() != null)
				total += collector.getItemQty();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getQuantityTotalView() {
		double total = 0;
		for (StockTransferItems collector : getSelectedStockTransferItemsList()) {
			if (collector.getOrderedQty() != null)
				total += collector.getOrderedQty();
		}
		return new DecimalFormat("###,###.00").format(total);
	}

	public String getQuantityTotalEdit() {
		double total = 0;
		for (StockTransferItems collector : getStockTransferItemsInwardUpdateList()) {
			if (collector.getOrderedQty() != null)
				total += collector.getOrderedQty();
		}
		return new DecimalFormat("###,###.###").format(total);
	}

	public StockItemInwardPNSBean() {
		super();
	}

	// lazy search
	public void loadLazyIntRequirementList() {
		log.info("<--- loadLazyStockTransfer RequirementList ---> ");
		stockTransferLazyList = new LazyDataModel<StockItemInwardPNSDTO>() {
			// StockTransfer ssss =new StockTransfer();
			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<StockItemInwardPNSDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockItemInwardPNSDTO> data = new ArrayList<StockItemInwardPNSDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<StockItemInwardPNSDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyStockTransfer RequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(StockItemInwardPNSDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockItemInwardPNSDTO getRowData(String rowKey) {
				List<StockItemInwardPNSDTO> responseList = (List<StockItemInwardPNSDTO>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (StockItemInwardPNSDTO res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							stockTransferLazy = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		stockTransferLazy = new StockItemInwardPNSDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		StockItemInwardPNSDTO yarnRequirement = new StockItemInwardPNSDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);
		if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null
				&& loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
			yarnRequirement
					.setLoginEntityId(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
		}

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/searchData";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public String clearButton() {
		log.info("Clear Action Called");
		renderedEdit = false;
		renderedView = false;
		renderedDelete = false;
		renderedCreate = true;
		selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
		parNumberGenerateBtnDisabledFlag = true;
		// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
		return INPUT_FORM_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Item List onRowSelect method started");
		log.info("Selected ID is :" + selectedStockItemInwardPNSDTO.getId());
		parNumberGenerateBtnDisabledFlag = true;

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
		
		String loginEntityCode = null;

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return;
		} else {
			loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();
		}

		selectedStockItemInwardPNSDTO = ((StockItemInwardPNSDTO) event.getObject());
		if (selectedStockItemInwardPNSDTO.getStatus().equals("SUBMITTED")) {
			renderedEdit = false;
			renderedView = true;
			renderedDelete = true;
			
			if (loginEntityCode.equals(EntityType.INSTITUTIONAL_SALES_SHOWROOM)
					|| loginEntityCode.equals(EntityType.PRINTING_WARE_HOUSE)) {
				parNumberGenerateBtnDisabledFlag = false;
			} else {
				parNumberGenerateBtnDisabledFlag = true;
			}
		} else if (StockTransferStatus.PAR_GENERATED.toString().equals(selectedStockItemInwardPNSDTO.getStatus())) {
			renderedEdit = false;
			renderedView = true;
			renderedDelete = false;
		} else {
			renderedEdit = true;
			renderedView = false;
			renderedDelete = false;
			parNumberGenerateBtnDisabledFlag = true;
		}
		renderedCreate = false;
	}

	// View Selected StockItemInwardPNS
	public String itemReceiveRetrieveDetails() {
		log.info("Received Item Retrieved Details ");
		selectedStockTransferPNS = new StockTransfer();
		selectedPurchaseInvoice = new PurchaseInvoice();
		try {
			if (selectedStockItemInwardPNSDTO == null) {
				AppUtil.addWarn("Please Select any one Record");
				return null;
			}
			BaseDTO baseDTO = null;
			log.info("Retrieved SupplierCode :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockItemRetrieved";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				selectedStockTransferItemsList = new ArrayList<StockTransferItems>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedStockTransferItemsList = mapper.readValue(jsonResponse,
						new TypeReference<List<StockTransferItems>>() {
						});
				getStockTransferByID();
				log.warn("Size of StockItemLis is--->:" + selectedStockTransferItemsList.size());
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_VIEW_URL);
				return INPUT_FORM_VIEW_URL;
			} else {
				log.warn("Item Retrieved Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
		}
		log.info("Done Item Retrieved Details ");
		return null;
	}

	public void getStockTransferByID() {
		try {
			BaseDTO baseDTO = null;
			StockItemInwardPNSDTO stockItemInwardPNSDTO = new StockItemInwardPNSDTO();
			selectedPurchaseInvoiceList = new ArrayList<PurchaseInvoice>();
			log.info("Retrieved tockTransferByID :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockTransferRetrieved";
			log.info("::: Item tockTransferByID Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Item tockTransferByID Successfully Processed ******");
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockItemInwardPNSDTO = mapper.readValue(jsonResponse, new TypeReference<StockItemInwardPNSDTO>() {
				});

				selectedStockTransferPNS = stockItemInwardPNSDTO.getSelectedStockTransfer();

				log.info("Item tockTransferByID Successfully Completed ******");

				selectedStockItemInwardPNSDTO.setTrans_ser_type(
						selectedStockTransferPNS.getTransportMaster().getTransportTypeMaster().getName());
				selectedStockItemInwardPNSDTO
						.setTrans_ser_name(selectedStockTransferPNS.getTransportMaster().getName());

				selectedStockItemInwardPNSDTO.setWaybill_no(selectedStockTransferPNS.getWaybillNumber());
				selectedStockItemInwardPNSDTO.setWaybillAvailable(selectedStockTransferPNS.getWaybillAvailable());
				selectedStockItemInwardPNSDTO.setGstNumber(selectedStockTransferPNS.getSupplierMaster().getGstNumber());

				selectedStockItemInwardPNSDTO.setWeight(selectedStockTransferPNS.getWeight());
				selectedStockItemInwardPNSDTO.setReceiptNumber(selectedStockTransferPNS.getReceiptNumber());

				selectedStockItemInwardPNSDTO
						.setTransport_charge_available(selectedStockTransferPNS.getTransportChargeAvailable());
				if (selectedStockTransferPNS.getTransportChargeType().toString().equalsIgnoreCase("ToPay"))
					selectedStockItemInwardPNSDTO.setTrans_charge_type("Cheque");
				else
					selectedStockItemInwardPNSDTO.setTrans_charge_type("Cash");
				if (selectedStockTransferPNS.getTransportChargeAmount() != null)
					selectedStockItemInwardPNSDTO
							.setTrans_charge_amt(selectedStockTransferPNS.getTransportChargeAmount());
				selectedStockItemInwardPNSDTO.setOrderType(selectedStockTransferPNS.getOrderType());
				// selectedPurchaseInvoiceList =
				// stockItemInwardPNSDTO.getSelectedPurchaseInvoiceList();
				if (stockItemInwardPNSDTO.getSelectedPurchaseInvoiceList() != null) {
					for (PurchaseInvoice purchaseInvoice : stockItemInwardPNSDTO.getSelectedPurchaseInvoiceList()) {
						if (purchaseInvoice.getInvoiceNumber() != null) {
							selectedPurchaseInvoiceList.add(purchaseInvoice);
						}
					}
				}
				log.info("Purchase Order List Successfully Completed ******");
			} else {
				log.error("tockTransferByID Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in tockTransferByID load ", ee);
		}
		log.info("Done Item tockTransferByID ");
	}

	// Edit selected ItemInward PNS
	public String itemReceiveRetrieveDetailsEdit() {
		log.info("Received Item Retrieved Details ");
		try {
			// reset();
			stockTransferItemsInwardDeleteList = new ArrayList<StockTransferItems>();
			selectedStockTransferItemsList = new ArrayList<StockTransferItems>();
			BaseDTO baseDTO = null;
			log.info("Retrieved SupplierCode :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getSelectedStockItemRetrieved";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedStockTransferItemsList = mapper.readValue(jsonResponse,
						new TypeReference<List<StockTransferItems>>() {
						});
				getStockTransferByID();
				log.info("Size of StockItemLis is--->:" + selectedStockTransferItemsList.size());
				getSelectedStockTransfer();
				loadSupplierCodeName();
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_Edit_URL);
				if (selectedStockItemInwardPNSDTO.getTransferType() != null
						&& selectedStockItemInwardPNSDTO.getTransferType().equalsIgnoreCase("WAREHOUSE_INWARD")) {
					inwardType = "Warehouse Manual Inward";
				} else if (selectedStockItemInwardPNSDTO.getTransferType() != null
						&& selectedStockItemInwardPNSDTO.getTransferType().equalsIgnoreCase("STOCK_ITEM_PO_BASED")) {
					inwardType = "PO Based Inward";
				} else if (selectedStockItemInwardPNSDTO.getTransferType() != null
						&& selectedStockItemInwardPNSDTO.getTransferType().equalsIgnoreCase("STOCK_ITEM_INWARD")) {
					inwardType = "Direct Inward";
				}
				return INPUT_FORM_Edit_URL;
			} else {
				log.error("Item Retrieved Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
		}
		log.info("Done Item Retrieved Details ");
		return null;
	}

	// update common data for selection
	public void getSelectedStockTransfer() {
		collectorSupplierItemList = new ArrayList<PurchaseOrderItems>();
		try {
			stockTransferItemsInwardUpdateList = new ArrayList<StockTransferItems>();
			log.info("Edit Method Called");
			stockTransfer = selectedStockTransferPNS;
			log.info("Edit Method Supplier Type:" + selectedStockItemInwardPNSDTO.getSupplierType());
			log.info("Edit Method Name :" + selectedStockTransferPNS.getSupplierMaster().getCode() + " / "
					+ selectedStockTransferPNS.getSupplierMaster().getName());
			for (SupplierTypeMaster master : getSupplierTypeMasterList()) {
				log.info("Edit Method Check :" + master.getCode() + ","
						+ selectedStockItemInwardPNSDTO.getSupplierType() + ":");
				if (master.getCode().toString().equals(selectedStockItemInwardPNSDTO.getSupplierType())) {
					selectedSupplierTypeMaster = master;
					loadSupplierCodeName(); // ***
					log.info("Set successfully Supplier Master ");
					break;
				}
			}
			selectedSupplierMaster = selectedStockTransferPNS.getSupplierMaster();
			gstNumber = selectedStockTransferPNS.getSupplierMaster().getGstNumber();
			taxExempted = selectedStockTransferPNS.getSupplierMaster().getTaxExempted();
			if (selectedStockTransferPNS.getTransferType().toString().equals("STOCK_ITEM_PO_BASED"))
				inwardType = "PO Based Inward";
			else
				inwardType = "Direct Inward";
			log.info("Edit Method Inward : " + inwardType);
			renderedInwardTypeSelectionEdit();
			Long purchaseOrderID = null;
			String poNumberPrefix = null;
			if (inwardType.equals("PO Based Inward")) {
				StockTransferItems stockTransfer = new StockTransferItems();
				log.info("PO Based Inward Processed  ---->");
				try {
					for (StockTransferItems order : selectedStockTransferItemsList) {
						purchaseOrderID = order.getPurchaseOrder().getId();
						stockTransfer = order;
						log.info("order DATA :" + purchaseOrderID);
						break;
					}
				} catch (Exception see) {
					log.error("Error to get data " + see);
				}
				getProductOrderSelected(stockTransfer);
				purchaseOrderUpdate();

			} else if (inwardType.equals("Direct Inward")) {
				log.info("Direct Inward Processed	---->");
				loadProductVarietyDetails(); // ***

				log.info("Size of Item :" + selectedStockTransferItemsList.size());
				if (selectedStockTransferItemsList.size() > 0)
					for (StockTransferItems order : selectedStockTransferItemsList) {
						PurchaseOrderItems items = new PurchaseOrderItems();

						items.setUnitRate(order.getUnitRate());
						items.setItemTotal(order.getItemTotal());
						items.setItemAmount(order.getItemAmount());
						items.setTaxPercent(order.getTaxPercent());
						items.setTaxValue(order.getTaxValue());

						items.setDiscountPercent(order.getDiscountPercent());
						items.setDiscountValue(order.getDiscountValue());
						items.setItemQty(order.getOrderedQty());
						items.setUomMaster(order.getUomMaster());
						items.setProductVarietyMaster(order.getProductVarietyMaster());

						items.setPurchaseOrder(order.getPurchaseOrder());

						items.setCreatedDate(order.getCreatedDate());

						items.setCreatedByName(order.getCreatedByName());

						selectedProductVarietyMaster = order.getProductVarietyMaster();
						log.info("leve 1:");
						collectorSupplierItemList.add(items);
						stockTransferItemsInwardUpdateList.add(order);
						log.info("Direct Inward Item Added :" + order.getProductVarietyMaster().getCode() + " / "
								+ order.getProductVarietyMaster().getName() + ", ID ->" + order.getId());
					}
				hsnCode = selectedProductVarietyMaster.getHsnCode();

				if (selectedProductVarietyMaster.getUomMaster() != null) {
					uom = selectedProductVarietyMaster.getUomMaster().getCode();
				}

				log.info("Edit Method Done Size is " + stockTransferItemsInwardUpdateList.size());
			}
			selectedTransportTypeMaster = selectedStockTransferPNS.getTransportMaster().getTransportTypeMaster();
			transportTypeMasterListener();
			selectedTransportMaster = selectedStockTransferPNS.getTransportMaster();
			waybillRendered = selectedStockTransferPNS.getWaybillAvailable();
			if (waybillRendered == true) {
				wayBillAvailable = "Yes";
				wayBillNumber = selectedStockTransferPNS.getWaybillNumber();
				weight = selectedStockTransferPNS.getWeight();
			} else
				wayBillAvailable = "No";
			if (selectedStockTransferPNS.getTransportChargeAvailable()) {
				transportChargeAvailble = "Yes";
				receiptNumber = selectedStockTransferPNS.getReceiptNumber();
				log.info("Transport Charge Available :" + transportChargeAvailble);
				renderedTransportChargeRendered();

				if (selectedStockTransferPNS.getTransportChargeType().toString().equalsIgnoreCase("Pay")) {
					transportChargeType = "Cash";
					stockTransfer.setTransportChargeType(TransportChargeType.Pay);
				} else {
					transportChargeType = "Cheque";
					stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
				}
				log.info("Transport Charge Type :" + transportChargeType);
				stockTransfer.setTransportChargeAmount(selectedStockTransferPNS.getTransportChargeAmount());
			} else {
				transportChargeAvailble = "No";
				log.info("Transport Charge NOT Available :" + transportChargeAvailble);
			}

			log.info("Edit Method St Done ");
		} catch (Exception se) {
			log.error("Edit Method Error " + se);
		}

	}

	public String delete() {
		log.info("Delete StockTransfer Received " + selectedStockItemInwardPNSDTO.getId());
		try {

			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/deleteStockTransfer";
			log.info("::: Item Delete Controller calling :::");
			baseDTO = httpService.post(url, selectedStockItemInwardPNSDTO);
			log.info("::: Retrieved Delete :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Delete StockTransfer Successfully Completed ******");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_DELETE_SUCCESS.getErrorCode());
				renderedEdit = false;
				renderedCreate = true;
				selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
				return INPUT_FORM_LIST_URL;
			} else {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_DELETE_ERROR.getErrorCode());
				log.warn("Delete StockTransfer Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Delete StockTransfer", ee);
		}
		return null;
	}

	public void getProductOrderSelected(StockTransferItems stockTransfer) {
		log.info("Received PurchaseOrderGet ");
		selectedPurchaseOrder = new PurchaseOrder();
		try {
			BaseDTO baseDTO = null;
			log.info("Retrieved PurchaseOrderGet :" + selectedStockItemInwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getPurchaseOrderGetByID";
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.post(url, stockTransfer);
			log.info("::: Retrieved PurchaseOrderGet :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("PurchaseOrderGet Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedPurchaseOrder = mapper.readValue(jsonResponse, new TypeReference<PurchaseOrder>() {
				});
			} else {
				log.warn("PurchaseOrderGet Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in PurchaseOrderGet load ", ee);
		}

	}

	public String generateATNumber() {
		BaseDTO baseDTO = null;
		try {

			// if (validateSocietyData()) {
			updateDesignMaster();

			if (selectedSupplierMaster != null) {
				log.info("=========>>>>>>>>>>> Inside generateATNumber============>>>>>>>");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/generateATNumber/"
						+ selectedSupplierMaster.getCode() + "/" + countAtNumberGenerator;
				baseDTO = httpService.get(url);
				if (baseDTO.getGeneralContent() != null) {
					log.warn("generateATNumber Successfully Completed ******");

					stockTransferItems.setAtNumber(baseDTO.getGeneralContent());
					return INPUT_FORM_ADD_URL;
				} else {
					log.warn("========>>>>>>>generateATNumber Error *** :");
				}

			}
			// }

		} catch (Exception e) {
			log.error("=========>>>>>>>>>>> Exception Ocured In generateATNumber============>>>>>>>", e);
		}

		return null;

	}

	private boolean validateSocietyData() {
		boolean valid = true;
		log.info("=========>>>>>>>>>>> Inside validateSocietyData============>>>>>>>");
		if (societyProductAppraisal.getWeaversName() == null || societyProductAppraisal.getWeaversName() == "") {
			AppUtil.addWarn("Enter Weaver Name");
			valid = false;
		} else if (societyProductAppraisal.getAppraisedBy() == null || societyProductAppraisal.getAppraisedBy() == "") {
			AppUtil.addWarn("Enter Apprised By");
			valid = false;
		} else if (societyProductAppraisal.getProductCost() == null) {
			AppUtil.addWarn("Enter Product Cost");
			valid = false;
		}

		return valid;
	}

	public String redirectSVInProgressPage() {
		// if(loginBean.getStockVerificationIsInProgess()) {
		// loginBean.setMessage("Inward");
		// loginBean.redirectSVInProgressPgae();
		// }

		// try {
		// if (loginBean.getStockVerificationIsInProgess()) {
		// loginBean.setMessage("Inward");
		// loginBean.redirectSVInProgressPgae();
		// }
		// } catch (Exception ex) {
		// log.error("Exception at redirectSVInProgressPage()", ex);
		// }

		return null;
	}

	public String createNewStockItemInward() {
		try {
			log.info("Received Create ItemInward ");
			// redirectSVInProgressPage();
			init();
			// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_ADD_URL);
			selectedSupplierMaster = null;
			uploadDocumentFile = null;
			fileName = null;
			return INPUT_FORM_ADD_URL;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public String backStockItemInward() {
		log.info("Received Back ItemInward ");
		renderedEdit = false;
		renderedCreate = true;
		renderedView = false;
		renderedDelete = false;
		selectedStockItemInwardPNSDTO = new StockItemInwardPNSDTO();
		return INPUT_FORM_LIST_URL;
	}

	// product category Group ->1
	public void productCategoryGroupList() {
		log.info("Received productCategoryGroupList ");
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryGroupList";
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productCategoryGroupList :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("productCategoryGroupList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();

				log.warn("productCategoryGroupList Successfully Converted ******");
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
			} else {
				log.warn("productCategoryGroupList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryGroupList load ", ee);
		}
	}

	// product category -> 2
	public void productCategoryListLoad() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
		log.info("Received productCategoryGroupList ID :" + selectedProductCategoryGroup.getId());
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryList/" + selectedProductCategoryGroup.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productCategoryList :");
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
			} else {
				log.warn("productCategoryList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	// product category -> 3
	public void getAllProductGroupMasterList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		try {
			BaseDTO baseDTO = null;
			// String url = SERVER_URL + appPreference.getOperationApiUrl()
			// + "/stockItemInwardPNS/getAllProductGroupMasterList/"
			// + selectedProductCategory.getProductCategoryGroup().getId();
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/" + selectedProductCategory.getId();

			baseDTO = httpService.get(url);
			log.info("::: Retrieved productGroupMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
			} else {
				log.warn("productGroupMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productGroupMasterList load ", ee);
		}
	}

	// product category -> 4
	public List<ProductVarietyMaster> productVarietyAutoComplete(String varietyCodeOrName) {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductVarietyMasterList/" + selectedProductGroupMaster.getId() + "/"
					+ varietyCodeOrName;
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
		return productVarietyMasterList;
	}

	public void inwardFromChange() {
		inwardType = null;
		try {
			entityMaster = new EntityMaster();
			selectedSupplierMaster = new SupplierMaster();
			entityTypeMasterList = new ArrayList<>();
			if (entity.equals(inwardFrom)) {
				// entityTypeMasterList = commonDataService.findAllEntityTypes();

				loadAllHOEntityList();
				loadAllEntityTypeList();
			}
		} catch (Exception e) {
			log.error("inwardFromChange method exception-----", e);
		}
	}

	public void entityTypeChange() {
		BaseDTO baseDTO = null;
		entityMasterList = new ArrayList<>();
		try {
			if (entityTypeMaster != null && entityTypeMaster.getId() != null) {
				baseDTO = new BaseDTO();
				String url = SERVER_URL + "/stockOutwardPS/loadActiveEntityListByTypeId/" + entityTypeMaster.getId();
				baseDTO = httpService.get(url);
				log.info("::: Retrieved entityMasterList :");
				if (baseDTO.getStatusCode() == 0) {
					log.warn("entityMasterList Successfully Completed ******");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
				}
			}
		} catch (Exception e) {
			log.error("inwardFromChange method exception-----", e);
		}
	}

	/*
	 * public List<SupplierMaster> supplierAutocomplete(String query) {
	 * log.info("supplierAutocomplete Autocomplete query==>" + query);
	 * List<SupplierMaster> results = new ArrayList<SupplierMaster>(); Long circleId
	 * = null; try { if (selectedSupplierMaster != null) { String supplierCode =
	 * query; if (selectedCircleMaster != null) { circleId =
	 * selectedCircleMaster.getId(); } if(circleId == null) { if(supplierMasterList
	 * != null) { for(SupplierMaster master : supplierMasterList) {
	 * if(master.getName().contains(supplierCode.toUpperCase()) ||
	 * master.getCode().contains(supplierCode)) { results.add(master); } } } }else {
	 * supplierMasterList = new ArrayList<>();
	 * 
	 * supplierMasterList =
	 * commonDataService.loadSupplierAutoCompleteCodeAndCircle(serverURL,
	 * supplierCode, circleId);
	 * 
	 * SupplierDetailsDTO supplierDetailsDTO = new SupplierDetailsDTO();
	 * supplierDetailsDTO.setSupplierCode(supplierCode);
	 * supplierDetailsDTO.setCircleId(circleId); BaseDTO baseDTO = new BaseDTO();
	 * String url = SERVER_URL +
	 * "/supplier/master/getactivesupplierforautocomplete"; if
	 * (supplierDetailsDTO.getSupplierCode() != null &&
	 * supplierDetailsDTO.getCircleId() != null) { baseDTO = httpService.post(url,
	 * supplierDetailsDTO); log.info("::: Retrieved entityMasterList :"); if
	 * (baseDTO.getStatusCode() == 0) {
	 * log.warn("supplierMasterList Successfully Completed ******"); ObjectMapper
	 * mapper = new ObjectMapper(); String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); supplierMasterList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
	 * }); } } if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
	 * results.addAll(supplierMasterList); } else { gstNumber = null;
	 * AppUtil.addWarn("Please enter valid code");
	 * RequestContext.getCurrentInstance().update("growls");
	 * RequestContext.getCurrentInstance().update("gst"); } } } else {
	 * AppUtil.addWarn("Select product group master");
	 * RequestContext.getCurrentInstance().update("growls"); return null; } }
	 * catch(Exception e) { log.error("Error in supplierAutocomplete Method: ",e); }
	 * 
	 * return results; }
	 */

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete Autocomplete query==>" + query);
		List<SupplierMaster> results = new ArrayList<SupplierMaster>();
		Long circleId = null;
		selectedSupplierMaster = new SupplierMaster();
		if (selectedSupplierMaster != null) {
			// log.info("supplierAutocomplete Autocomplete query==> step1" + query);
			// selectedSupplierMaster = null;
			String supplierCode = query;
			if (selectedCircleMaster != null) {
				circleId = selectedCircleMaster.getId();
			}
			// log.info("supplierAutocomplete Autocomplete query==> step2" + circleId);
			if (circleId == null) {
				// log.info("supplierAutocomplete Autocomplete query==> step3" + circleId);
				if (supplierMasterList != null) {
					// log.info("supplierAutocomplete Autocomplete query==> step4" +
					// supplierMasterList.size());
					for (SupplierMaster master : supplierMasterList) {
						// log.info("supplierAutocomplete Autocomplete query==> step5" +
						// master.getCode()+" name : "+master.getName());
						if (master.getCode() == null) {
							master.setCode("None");
						}
						if (master.getName() == null) {
							master.setName("None");
						}
						String name = master.getName().toUpperCase();
						String code = master.getCode().toUpperCase();
						if (name.contains(supplierCode.toUpperCase()) || code.contains(supplierCode)) {
							// log.info("supplierAutocomplete Autocomplete query==> step6" );
							results.add(master);
							if (master.getCode().equals("None")) {
								master.setCode(null);
							}

							if (master.getName().equals("None")) {
								master.setName(null);
							}
						}
					}
				}
			} else {
				log.info("supplierAutocomplete Autocomplete query==> step7" + " supplierCode : " + supplierCode
						+ " circleId : " + circleId + " serverURL : " + serverURL);
				supplierMasterList = new ArrayList<>();
				supplierMasterList = commonDataService.loadSupplierAutoCompleteCodeAndCircle(serverURL, supplierCode,
						circleId);
				log.info("supplierAutocomplete Autocomplete query==> step8 : " + supplierMasterList.size());
				results.addAll(supplierMasterList);
			}
		} else {
			AppUtil.addWarn("Select product group master");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

		log.info("=====================supplierMasterList.size():======================= " + results.size());
		// supplierMasterList.size() : 0));
		// log.info("selectedSupplierMaster: " + selectedSupplierMaster);

		return results;
	}

	private void setSlectedSupplier() {
		log.info("setSlectedSupplier() - selectedSupplierMaster: " + selectedSupplierMaster);
		if (selectedSupplierMaster != null) {
			Long selectedSupplierId = selectedSupplierMaster.getId();
			String selectedSupplierCode = selectedSupplierMaster.getCode();
			selectedSupplierMaster = commonDataService.loadSupplierByCode(serverURL, selectedSupplierCode);
			log.info("<<=== New SupplierMaster - gstNumber - " + selectedSupplierMaster.getGstNumber());
			log.info("<<=== New SupplierMaster - taxExempted - " + selectedSupplierMaster.getTaxExempted());
		}
	}

	public void selectAutoSearch() {

		setSlectedSupplier();

		log.info("<<=== inside selectAutoSearch");
		// if (supplierMasterList.contains(selectedSupplierMaster)) {
		log.info("<<=== selected SupplierMaster " + selectedSupplierMaster);
		if (selectedSupplierMaster != null) {
			gstNumber = selectedSupplierMaster.getGstNumber();
			taxExempted = selectedSupplierMaster.getTaxExempted();
		} else {
			gstNumber = null;
			taxExempted = null;
		}

		log.info("<<=== SupplierMaster - gstNumber - " + gstNumber);
		log.info("<<=== SupplierMaster - taxExempted - " + taxExempted);
		// }
	}

	public void resetProductVarietyValue() {
		selectedProductVarietyMaster = new ProductVarietyMaster();
		productVarietyMasterList = new ArrayList<>();
	}

	public String handleKeyEvent() {
		log.info(" ========================minimum 00   to  maximum  100   ==>" + stockTransferItems.getTaxPercent());

		if (stockTransferItems.getTaxPercent() > 100) {
			AppUtil.addWarn(" Allow for Tax field value minimum 0 maximum 100");
			stockTransferItems.setTaxPercent(null);
		}
		return null;
	}

	public void checkProductPrice() {
		if (societyProductAppraisal != null) {
			if (societyProductAppraisal.getProductCost() != null && stockTransferItems.getUnitRate() != null) {
				if (!societyProductAppraisal.getProductCost().equals(stockTransferItems.getUnitRate())) {
					AppUtil.addError(" Product Cost And Unit Rate In Item Receive Details Should be Same");
					societyProductAppraisal.setProductCost(null);
				}
			}
		}
	}

	public void autosearchClear() {
		log.info("======================Start autosearchClear method========================");
		if (selectedSupplierMaster == null) {
			selectedSupplierMaster = new SupplierMaster();
			gstNumber = null;
			taxExempted = null;
		}
		log.info("===================End autosearchClear method===================");
	}
	
}
