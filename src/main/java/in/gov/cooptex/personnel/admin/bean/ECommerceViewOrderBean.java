package in.gov.cooptex.personnel.admin.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ECommerceProductVarietiesDTO;
import in.gov.cooptex.core.dto.ECommerceViewOrderDetailsSearchDTO;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.EcommerceSalesOrderStatus;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.ecomm.model.SalesOrderMovement;
import in.gov.cooptex.exceptions.EcommerceErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.model.SalesOrder;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("eCommerceViewOrderBean")
@Scope("session")
public class ECommerceViewOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String ECOMMERCE_VIEW_ORDER_PAGE = "/pages/ecommerce/admin/viewOrder.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	private static final String ECOMMERCE_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/ecommercemenu";

	@Getter
	@Setter
	String action = "";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	String jsonResponse;

	ObjectMapper mapper;

	/*
	 * @Setter
	 * 
	 * @Getter List statusValues;
	 */

	@Getter
	@Setter
	Date orderFromDate, orderToDate, expectedDeliveryDate, deliveredDate;

	@Getter
	@Setter
	Long orderId, paymentId;

	@Getter
	@Setter
	String orderStatus, customerName, buttonName, currentOrderStatus, remarks;

	@Getter
	@Setter
	Long orderPopupId;

	@Getter
	@Setter
	List<PaymentMode> paymentModeList = new ArrayList<>();

	@Getter
	@Setter
	ECommerceViewOrderDetailsSearchDTO eCommerceViewOrderDetailsSearchDTO;

	@Getter
	@Setter
	List<ECommerceViewOrderDetailsSearchDTO> eCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<ECommerceViewOrderDetailsSearchDTO> geteCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<String> sequenceOrderStatusList;

	@Getter
	@Setter
	List<ECommerceProductVarietiesDTO> productVarietiesDTOList = new ArrayList<>();

	@Getter
	@Setter
	ECommerceProductVarietiesDTO ECommerceProductVarietiesDTOObj;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	SupplierMaster supplierMaster = new SupplierMaster();

	@Getter
	@Setter
	String vendorAcknowledgeNumber;

	@Getter
	@Setter
	Double shippingCharge;

	@Getter
	@Setter
	Boolean receivedFlag;

	@Getter
	@Setter
	Date receivedDate;

	@Getter
	@Setter
	String receivedBy;

	@Getter
	@Setter
	String receiverMobNumber;

	@Getter
	@Setter
	String status;

	@Getter
	@Setter
	Boolean deliveryStatusFlag;

	@Getter
	@Setter
	Boolean recivedStatusFlag,expectedDeliveryDateFlag,deliveryDateFlag, remarksFlag;

	@Getter
	@Setter
	Boolean shippingStatusformFlag;
	
	@Getter
	@Setter
	List<SalesOrderMovement> salesOrderMovementList;
	// @PostConstruct
	// public void init() {
	// log.info("<=== Starts ECommerceViewOrderBean.init() ========>");
	// eCommerceViewOrderDetailsSearchDTOList=new ArrayList<>();
	//// geteCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();
	// showEcommerceViewOrderListPage();
	// log.info("<=== Ends ECommerceViewOrderBean.init() ========>");
	// }

	public void updateOrderToDate() {
		if (orderToDate != null && orderToDate != null && orderToDate.before(orderFromDate)) {
			orderToDate = null;
		}
	}

	public void fromDateChange() {
		log.info(":::::::::::Inside::::::fromDateChange:::::::::" + orderFromDate);
		try {
			if (orderToDate != null) {
				if (orderFromDate.compareTo(orderToDate) < 0) {
					log.info("inside condition orderFromDate is Less then orderToDate");
				} else {
					log.info("inside condition orderFromDate is Greater then orderToDate");
					setOrderToDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in  fromDateChange...", e);
		}
	}

	public String showEcommerceViewOrderListPage() {
		log.info("<==== Starts eCommerceViewOrderBean.showEcommerceViewOrderListPage =====>");
		// mapper = new ObjectMapper();
		eCommerceViewOrderDetailsSearchDTO = new ECommerceViewOrderDetailsSearchDTO();
		eCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();
		loadStatus();
		clear();
		log.info("<==== Ends eCommerceViewOrderBean.showEcommerceViewOrderListPage =====>");
		return ECOMMERCE_VIEW_ORDER_PAGE;
	}

	public void clear() {
		eCommerceViewOrderDetailsSearchDTO = new ECommerceViewOrderDetailsSearchDTO();
		orderToDate = null;
		orderFromDate = null;
		orderId = null;
		paymentId = null;
		orderStatus = null;
		action = null;

		try {
		geteCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();
		BaseDTO response = httpService.post(ECOMMERCE_URL + "/getallecomorders", eCommerceViewOrderDetailsSearchDTO);

		if (response.getStatusCode() == 0) {
			
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				geteCommerceViewOrderDetailsSearchDTOList = mapper.readValue(jsonResponse, new TypeReference<List<ECommerceViewOrderDetailsSearchDTO>>() {
				});
			}
		}
		supplierMasterList = new ArrayList<>();
		setSupplierMasterList(loadSupplierMaster());
		
		} catch (Exception e) {
			log.error(" ==<Exception Occured on clear === > ");
		}
	}

	private void loadStatus() {
		log.info("<=== Starts eCommerceViewOrderBean.loadStatus() ======>");
		paymentModeList = commonDataService.loadPaymentModes();
		log.info("<=== Ends eCommerceViewOrderBean.loadStatus() =====> ");
	}

	public void searchOrderDetails() {
		log.info("<=== Starts eCommerceViewOrderBean.searchOrderDetails() ======>");
		
		try {
		eCommerceViewOrderDetailsSearchDTO.setOrderFromDate(orderFromDate);
		eCommerceViewOrderDetailsSearchDTO.setOrderToDate(orderToDate);

		geteCommerceViewOrderDetailsSearchDTOList = new ArrayList<>();
		eCommerceViewOrderDetailsSearchDTO.setOrderId(orderId);
		eCommerceViewOrderDetailsSearchDTO.setPaymentId(paymentId);
		if (!orderStatus.equals("")) {
			eCommerceViewOrderDetailsSearchDTO.setOrderStatus(orderStatus);
		} else {
			eCommerceViewOrderDetailsSearchDTO.setOrderStatus(null);
		}

		BaseDTO response = httpService.post(ECOMMERCE_URL + "/getallecomorders", eCommerceViewOrderDetailsSearchDTO);

		if (response.getStatusCode() == 0) {
			
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			geteCommerceViewOrderDetailsSearchDTOList = mapper.readValue(jsonResponse, new TypeReference<List<ECommerceViewOrderDetailsSearchDTO>>() {
			});
			
			supplierMasterList = new ArrayList<>();
			setSupplierMasterList(loadSupplierMaster());
		  }
		} catch (Exception e) {
			log.error("Exception Occured on searchOrderDetails >>");
		}

		
		log.info("<=== Ends eCommerceViewOrderBean.searchOrderDetails() ======>");
	}

	public List<SupplierMaster> loadSupplierMaster() {
		log.info(":::::::loadSupplierMaster:::::::::");
		List<SupplierMaster> supplierList = null;
		try {
			BaseDTO baseDTO = new BaseDTO();
			supplierList = new ArrayList<>();
			String url = AppUtil.getPortalServerURL() + "/supplier/master/getallnonsocietysupplierlist";
			log.info("loadAllSupplierMaster url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
			}

		} catch (Exception e) {
			log.info("Error in loadAllSupplierMaster>>>>>>>>" + e);
		}
		return supplierList;
	}

	public void clearShipmentDetailValue() {
		log.info("<<<::::::Starts clearShipmentDetailValue::::::::>>>");
		try {
			supplierMaster = new SupplierMaster();
			vendorAcknowledgeNumber = "";
			shippingCharge = null;
			receivedFlag = false;
			receivedDate = null;
			receiverMobNumber = "";
			receivedBy = "";
		} catch (Exception e) {
			log.info("Error in clearShipmentDetailValue>>>>>>>>" + e);
		}
		log.info("<<<<::::::::Ends clearShipmentDetailValue>>>>");
	}

	public void viewOrderDetails(List<ECommerceProductVarietiesDTO> salesInvoiceItems, String name, Long orderId,
			String status) {
		log.info("<<::::::::::Starts eCommerceViewOrderBean.viewOrderDetails()::::::::::>>" + supplierMaster
				+ "::Status::" + status + orderId);
		try {

			setShippingStatusformFlag(false);
			setDeliveryStatusFlag(false);
			setReceivedFlag(false);
			setExpectedDeliveryDateFlag(false);
			setDeliveryDateFlag(false);
			setRemarksFlag(false);
			deliveredDate = null;
			remarks = null;
			
			if (status.equalsIgnoreCase("WAITING")) {
				setShippingStatusformFlag(true);
				setDeliveryStatusFlag(true);
				setRecivedStatusFlag(false);
				setReceivedFlag(false);
			} else if (status.equals(EcommerceSalesOrderStatus.SHIPPED)) {
				setShippingStatusformFlag(true);
				setDeliveryStatusFlag(false);
				setReceivedFlag(true);
				setRecivedStatusFlag(true);
				deliveredDate = new Date();
				setDeliveryDateFlag(true);
			} else if (status.equals(EcommerceSalesOrderStatus.ORDERED)) {
				setExpectedDeliveryDateFlag(true);
			} else if (status.equals(EcommerceSalesOrderStatus.DELIVERED)) {
				setRemarksFlag(true);
			}
			log.info("<<<:::::::supplierMasterList::::::>>>" + supplierMasterList.size());
			productVarietiesDTOList = new ArrayList<>();
			
			// Get ProductVariety Details By SalesOrderId
			
			String urlForItemDetails = ECOMMERCE_URL + "/getItemDetailsOrderId"+"/"+orderId;
			BaseDTO itemDetailsResponse = httpService.get(urlForItemDetails);
			if (itemDetailsResponse != null && itemDetailsResponse.getStatusCode() == 0) {
				ObjectMapper objMapper = new ObjectMapper();
				String jsonValue = objMapper.writeValueAsString(itemDetailsResponse.getResponseContents());
				productVarietiesDTOList = mapper.readValue(jsonValue, new TypeReference<List<ECommerceProductVarietiesDTO>>() {
				});
			}
			
			if (buttonName.equalsIgnoreCase("UPDATESTATUS")) {
				currentOrderStatus = null;
				expectedDeliveryDate = null;
				sequenceOrderStatusList = new ArrayList<>();
				if (status.equalsIgnoreCase(EcommerceSalesOrderStatus.ORDERED)) {
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.PACKED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.SHIPPED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.DELIVERED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.CANCELLED);
				} else if (status.equalsIgnoreCase(EcommerceSalesOrderStatus.PACKED)) {
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.SHIPPED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.DELIVERED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.CANCELLED);
				} else if (status.equalsIgnoreCase(EcommerceSalesOrderStatus.SHIPPED)) {
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.DELIVERED);
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.CANCELLED);
				} else if (status.equalsIgnoreCase(EcommerceSalesOrderStatus.DELIVERED)) {
					sequenceOrderStatusList.add(EcommerceSalesOrderStatus.CANCELLED);
				}

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('modal_theme_info1').show();");
			} else {
				
				String url = ECOMMERCE_URL + "/getStatusByOrderId"+"/"+orderId;
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					ObjectMapper objMapper = new ObjectMapper();
					String jsonValue = objMapper.writeValueAsString(response.getResponseContents());
					salesOrderMovementList = mapper.readValue(jsonValue, new TypeReference<List<SalesOrderMovement>>() {
					});
				}
				
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('modal_theme_info').show();");
			}
			orderStatus = status;
			customerName = name;
			orderPopupId = orderId;

			log.info("<=== Ends eCommerceViewOrderBean.viewOrderDetails() ======>");
		} catch (Exception e) {
			log.error("Exception occured in  viewOrderDetails...", e);
		}
		clearShipmentDetailValue();
	}

	public void updateOrderStatus() {
		
		try {
		if (orderPopupId == null) {
			errorMap.notify(
					ErrorDescription.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_SHIPPED_TO_EMPTY).getErrorCode());
			return;
		}
		if (customerName == null) {
			errorMap.notify(ErrorDescription.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_SHIPPING_CHARGE_EMPTY)
					.getErrorCode());
			return;
		}
		if (orderStatus == null) {
			errorMap.notify(ErrorDescription.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_SHIPPING_CHARGE_EMPTY)
					.getErrorCode());
			return;
		}

		if (orderStatus.equalsIgnoreCase(EcommerceSalesOrderStatus.ORDERED)) {
			if (expectedDeliveryDate == null) {
				AppUtil.addWarn("Please Select Expected Delivery Date");
				return;
			}

		}

		ECommerceViewOrderDetailsSearchDTO requestDTO = new ECommerceViewOrderDetailsSearchDTO();
		requestDTO.setId(orderPopupId);
		requestDTO.setCustomerName(customerName);
		requestDTO.setOrderStatus(currentOrderStatus);
		requestDTO.setExpectedDeliveryDate(expectedDeliveryDate);
		requestDTO.setReceivedDate(deliveredDate);
		requestDTO.setRemarks(remarks);
		requestDTO.setButtonType(buttonName);

		String url = ECOMMERCE_URL + "/updateecommdeliverystatus";
		BaseDTO response = httpService.post(url, requestDTO);
		if (response != null && response.getStatusCode() == 0) {
			
			ObjectMapper objMapper = new ObjectMapper();
			String jsonValue = objMapper.writeValueAsString(response.getResponseContent());
			SalesOrder salesOrder = mapper.readValue(jsonValue, SalesOrder.class);
			
			if (salesOrder != null) {
				int index = geteCommerceViewOrderDetailsSearchDTOList.stream().map(user -> user.getOrderId())
					    .collect(Collectors.toList()).indexOf(orderPopupId);
				geteCommerceViewOrderDetailsSearchDTOList.get(index).setOrderResponseStatus(currentOrderStatus);
				geteCommerceViewOrderDetailsSearchDTOList.get(index).setExpectedDeliveryDate(salesOrder.getExpectedDeliveryDate());
				geteCommerceViewOrderDetailsSearchDTOList.get(index).setDeliveredDate(salesOrder.getReceivedDate());
				
			}
			
			errorMap.notify(ErrorDescription.getError(EcommerceErrorCode.ECOMMERCE_SALES_ORDER_STATUS_SUCCESSFULLY)
					.getErrorCode());
			
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('modal_theme_info1').hide();");
		  }
		} catch (Exception e) {
			log.error("Exception Occured on updateOrderStatus Method "+2);
		}
	}

	public String updateEcommerceDeliveryStatus() {
		log.info("<==== Starts updateEcommerceDeliveryStatus =======>" + deliveryStatusFlag);
		try {
			SalesOrder salesOrderObj = new SalesOrder();
			if (deliveryStatusFlag == true) {
				if (getSupplierMaster() == null) {
					errorMap.notify(ErrorDescription.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_SHIPPED_TO_EMPTY)
							.getErrorCode());
					return null;
				}
				if (getShippingCharge() == null) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_SHIPPING_CHARGE_EMPTY).getErrorCode());
					return null;
				}
				if (getVendorAcknowledgeNumber() == null) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_DELIVERY_CHELLAN_NUMBER_EMPTY)
							.getErrorCode());
					return null;
				}
			} else if (recivedStatusFlag == true && receivedFlag == true) {
				if (getReceivedDate() == null) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_ORDER_VIEW_RECEIVED_DATE_EMPTY).getErrorCode());
					return null;
				}
			}
			log.info(":::::::::supplier Master Id::::::::::::::" + getSupplierMaster().getId());
			salesOrderObj.setId(orderPopupId);
			salesOrderObj.setSupplier(getSupplierMaster());
			salesOrderObj.setShippingCharge(getShippingCharge());
			salesOrderObj.setVendorAcknowledgeNumber(getVendorAcknowledgeNumber());

			salesOrderObj.setReceivedDate(getReceivedDate());
			salesOrderObj.setReceivedBy(getReceivedBy());
			salesOrderObj.setReceiverContactNumber(getReceiverMobNumber());

			log.info("value Check:::::::>>>>>" + recivedStatusFlag);

			if (deliveryStatusFlag == true && recivedStatusFlag == false) {
				salesOrderObj.setOrderStatus(EcommerceSalesOrderStatus.SHIPPED);
			} else if (deliveryStatusFlag == false && recivedStatusFlag == true) {
				if (receivedFlag == true) {
					salesOrderObj.setOrderStatus(EcommerceSalesOrderStatus.RECEIVED);
				} else {
					salesOrderObj.setOrderStatus(EcommerceSalesOrderStatus.NOT_RECEIVED);
				}
			}

			String url = ECOMMERCE_URL + "/updateecommdeliverystatus";
			BaseDTO response = httpService.post(url, salesOrderObj);
			if (response != null && response.getStatusCode() == 0) {
				if (salesOrderObj.getOrderStatus().equals(EcommerceSalesOrderStatus.SHIPPED)) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_SALES_ORDER_STATUS_SUCCESSFULLY).getErrorCode());
				} else if (salesOrderObj.getOrderStatus().equals(EcommerceSalesOrderStatus.RECEIVED)) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_SALES_ORDER_STATUS_SUCCESSFULLY).getErrorCode());
				} else if (salesOrderObj.getOrderStatus().equals(EcommerceSalesOrderStatus.NOT_RECEIVED)) {
					errorMap.notify(ErrorDescription
							.getError(EcommerceErrorCode.ECOMMERCE_SALES_ORDER_STATUS_SUCCESSFULLY).getErrorCode());
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('modal_theme_info').hide();");

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Exception occured while status .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		clear();
		return "";
	}

}
