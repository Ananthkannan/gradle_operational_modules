package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EmployeeTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employmentTypeBean")
@Scope("session")
public class EmploymentTypeBean implements Serializable {/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private final String EMP_TYPE_LIST_PAGE = "/pages/masters/employmentType/listEmploymentType.xhtml?faces-redirect=true;";
	private final String EMP_TYPE_ADD_PAGE = "/pages/masters/employmentType/addEmploymentType.xhtml?faces-redirect=true;";
	private final String EMP_TYPE_VIEW_PAGE = "/pages/masters/employmentType/viewEmploymentType.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter @Setter
	EmployeeTypeMaster selectedEmployeeType = new EmployeeTypeMaster();

	@Getter @Setter
	EmployeeTypeMaster employeeType = new EmployeeTypeMaster();

	@Getter @Setter
	private RestTemplate restTemplate;

	@Getter @Setter
	private String Url = null;

	@Getter @Setter
	LazyDataModel<EmployeeTypeMaster> employeeTypelazyLoadList;

	@Getter @Setter
	private Map<String, Object> filterMap;

	@Getter @Setter
	private SortOrder sortingOrder;

	@Getter @Setter
	private String sortingField;

	@Getter @Setter
	private Integer resultSize;

	@Getter @Setter 
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;
	
	/**
	 * @return
	 */
	public String showListEmploymentTypePage() {
		log.info("showListEmploymentTypePage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedEmployeeType = new EmployeeTypeMaster();
		loadEmployeeTypeListLazy();
		return EMP_TYPE_LIST_PAGE;
	}


	public void loadEmployeeTypeListLazy() {

		log.info("<-----EmployeeType list lazy load starts------>");
		employeeTypelazyLoadList = new LazyDataModel<EmployeeTypeMaster>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<EmployeeTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters){
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<EmployeeTypeMaster> employeeTypeList = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeeTypeMaster>>() {
					});
					if (employeeTypeList == null) {
						log.info(" EmployeeType List is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" EmployeeType list search Successfully Completed");
					} else {
						log.error(" EmployeeType list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return employeeTypeList;

				}catch (Exception e) {
					log.error("ExceptionException Ocured while Loading EmployeeType list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}



			@Override
			public Object getRowKey(EmployeeTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeTypeMaster getRowData(String rowKey) {
				List<EmployeeTypeMaster> responseList = (List<EmployeeTypeMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeTypeMaster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeType = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	
	
	

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			EmployeeTypeMaster request = new EmployeeTypeMaster();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("name")) {
					log.info("employmentName : " + value);
					searchFilters.put("employeeTypeMasterName", value);
				}
				if (filter.getKey().equals("lname")) {
					log.info("employmentName in tamil : " + value);
					searchFilters.put("employeeTypeMasterNameTamil", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("createdDate : " + value);
					searchFilters.put("createdDate",value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status",value);
				}	

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/employeeTypeMaster/getallemploymenttypelistlazy", request);
			log.info("Search request response " + dataList);

		}catch(Exception e) {
			log.error("Exception Occured in employment load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String employeeTypeListAction() {
		log.info("<====== EmployeeTypeBean.employeeTypeListAction Starts====> action   :::"+action);
		try {
			if(action.equalsIgnoreCase("ADD")) {
				log.info("<====== employeeTypeList add page called.......====>");
				selectedEmployeeType = new EmployeeTypeMaster();
				employeeType = new EmployeeTypeMaster();
				return EMP_TYPE_ADD_PAGE;
			}
			if(selectedEmployeeType==null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if(action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmemployeeTypeDelete').show();");
			}else {
				log.info("<----Loading employeeTypeList Edit page.....---->" + selectedEmployeeType.getId());
				employeeType = new EmployeeTypeMaster();
				BaseDTO response = httpService
						.get(SERVER_URL + "/employeeTypeMaster/getemployment/" + selectedEmployeeType.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					employeeType = mapper.readValue(jsonResponse, new TypeReference<EmployeeTypeMaster>() {
					});
					log.info("<====== employeeTypeBean.employeeTypeListAction Ends====>");
					if(action.equalsIgnoreCase("EDIT")) {
						return EMP_TYPE_ADD_PAGE;
					}else if(action.equalsIgnoreCase("VIEW")) {
						return EMP_TYPE_VIEW_PAGE;
					}
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== employeeTypeBean.employeeTypeListAction Ends====>");
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in employeeTypeBean.employeeTypeListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EmployeeTypeBean.employeeTypeListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts EmployeeTypeBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/employeeTypeMaster/delete/" + selectedEmployeeType.getId());
			if(response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting employment....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends EmployeeTypeBean.deleteConfirm ---->");
		return showListEmploymentTypePage();
	}

	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeeTypeBean.onRowSelect ========>"+event);
		selectedEmployeeType = ((EmployeeTypeMaster) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		/*if(selectedemployment.getStatus() == true ){
			deleteButtonFlag=true;
		}else if(selectedemployment.getStatus() == false){
			deleteButtonFlag=false;
		}*/
		log.info("<===Ends EmployeeTypeBean.onRowSelect ========>");
    }

	
	public String submit() {
		log.info("<=======Starts  EmployeeTypeBean.submit ======>"+employeeType);
		if(employeeType.getName() == null || employeeType.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_NAME_EMPTY.getErrorCode());
			return null;
		}
		if(employeeType.getName().trim().length()<3  || employeeType.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(employeeType.getLname() == null || employeeType.getLname().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_LNAME_EMPTY.getErrorCode());
			return null;
		}
		if(employeeType.getLname().trim().length()<3  || employeeType.getLname().trim().length() > 75) {
			errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_LNAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(employeeType.getStatus() == null) {
			errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_STATUS_EMPTY.getErrorCode());
			return null;
		}
		if(action.equalsIgnoreCase("ADD")) {
			employeeType.setCreatedBy(loginBean.getUserDetailSession());
		}else if (action.equalsIgnoreCase("EDIT")) {
			employeeType.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response= httpService.post(SERVER_URL + "/employeeTypeMaster/save",employeeType);
//		errorMap.notify(response.getStatusCode());
		if(response.getStatusCode()==0) {
			if(action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_SAVE_SUCCESS.getErrorCode());
			else 
				errorMap.notify(ErrorDescription.EMPLOYMENT_TYPE_UPDATE_SUCCESS.getErrorCode());
			return showListEmploymentTypePage();
		}else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends EmployeeTypeBean.submit ======>");
		return null;
	}
	
}
