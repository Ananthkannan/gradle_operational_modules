/**
 * 
 */
package in.gov.cooptex.admin.tapal.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.internal.synchronization.work.AddWorkUnit;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.IncomingTapal;
import in.gov.cooptex.admin.tapal.model.IncomingTapalLog;
import in.gov.cooptex.admin.tapal.model.IncomingTapalNote;
import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.admin.tapal.model.TapalSenderType;
import in.gov.cooptex.asset.model.dto.AssetRequestListDTO;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.IncomingTapalStatus;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TapalDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.hrms.dto.IncomingTapalResponseDto;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
 
/**
 * @author ftuser
 *
 */
@Service("incomingTapalBean")
@Scope("session")
@Log4j2
public class IncomingTapalBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String INCOMING_TAPAL_CREATE_PAGE = "/pages/admin/tapal/createIncomingTapal.xhtml?faces-redirect=true;";
	private final String INCOMING_TAPAL_LIST_PAGE = "/pages/admin/tapal/listIncomingTapal.xhtml?faces-redirect=true;";
	private final String INCOMING_TAPAL_VIEW_PAGE = "/pages/admin/tapal/viewIncomingTapal.xhtml?faces-redirect=true;";

	final String APP_SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	private IncomingTapal incomingTapal;

	@Getter
	@Setter
	private IncomingTapalResponseDto selectedIncomingTapal;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	LazyDataModel<IncomingTapalResponseDto> lazyIncomingTapalList;

	@Getter
	@Setter
	List<IncomingTapalResponseDto> incomingTapalList;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<DistrictMaster> districtList = new ArrayList<>();

	@Getter
	@Setter
	List<TapalDeliveryType> tapalTypeDeliveryTypeList;

	@Getter
	@Setter
	TapalDeliveryType tapalDeliveryType;

	@Getter
	@Setter
	List<TapalSenderType> tapalSenderTypeList;

	@Getter
	@Setter
	TapalSenderType tapalSenderType;

	@Getter
	@Setter
	Boolean entityFlag = false;

	@Getter
	@Setter
	Boolean readOnlyFlag = false, supplierNameFlag = false;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	private String designation;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	List<Department> selectedDepartmentList;

	@Getter
	@Setter
	List<SectionMaster> sectionList;

	@Getter
	@Setter
	List<SectionMaster> selectedSectionList;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();;

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	EntityMaster addressEntityMaster;

	@Getter
	@Setter
	private String state;

	@Getter
	@Setter
	DistrictMaster districtMaster;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	long letterSize;

	@Setter
	@Getter
	String fileName;
	@Setter
	@Getter
	private StreamedContent file;

	@Setter
	@Getter
	private List<String> fileList = new ArrayList<>();

	@Setter
	@Getter
	private List<String> incomingFileList;

	@Getter
	@Setter
	Set<String> departmentNameList;

	@Getter
	@Setter
	List<String> sectionNameList;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean receivedFlag = false;

	@Getter
	@Setter
	Boolean finalapproval = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	UserMaster userMaster = new UserMaster();

	@Getter
	@Setter
	IncomingTapalLog incomeTapalLog;

	@Getter
	@Setter
	IncomingTapalNote incomeTapalNote;

	@Getter
	@Setter
	TapalDTO tapalDTO = new TapalDTO();

	@Getter
	@Setter
	private Boolean skipApprovalFlag = false;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String downloadFile;

	@Getter
	@Setter
	Boolean deleteUploadFlag = true;

	@Getter
	@Setter
	List<EmployeeMaster> empMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	SupplierMaster supplierInfo = new SupplierMaster();

	@Setter
	@Getter
	String incomingTapalNote;

	@Getter
	@Setter
	List<TapalDTO> incomingTapalNoteEmployeeList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster workLocationEntity = new EntityMaster();

	@Getter
	@Setter
	Boolean tapalNumberStatus = false;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		action = null;
		createdEmployeeMaster = new EmployeeMaster();
		createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("incoming_tapal_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// init();
			selectedIncomingTapal = new IncomingTapalResponseDto();
			action = "VIEW";

			log.info("valueeee" + selectedIncomingTapal.getId());
			selectedIncomingTapal.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedIncomingTapal.getId());
			selectedNotificationId = Long.parseLong(notificationId);

			viewincomeingTapal();
		}
		log.info("incomingtapal Ends");

		return INCOMING_TAPAL_VIEW_PAGE;
	}

	public String showListPage() {

		log.info("<==== Starts IncomingTapal-showIncomingTapalListPage =====>");

		mapper = new ObjectMapper();

		editButtonFlag = true;

		deleteButtonFlag = true;

		addButtonFlag = false;
		viewButtonFlag = true;
		tapalTypeDeliveryTypeList = commonDataService.loadTapalDeliveryType();
		incomingTapal = new IncomingTapal();
		selectedIncomingTapal = new IncomingTapalResponseDto();
		downloadFile = null;
		tapalDTO = new TapalDTO();
		loadLazyIncomingTapalList();
		log.info("<==== Ends IncomingTapal-showIncomingTapalListPage =====>");
		return INCOMING_TAPAL_LIST_PAGE;
	}

	public void loadLazyIncomingTapalList() {

		log.info("<===== loadLazyIncomingTapalList() Starts ======>");

		lazyIncomingTapalList = new LazyDataModel<IncomingTapalResponseDto>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -206372230414855092L;

			@Override

			public List<IncomingTapalResponseDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					EmployeeMaster employeMaster = loginBean.getEmployee();
					EmployeePersonalInfoEmployment personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
					paginationRequest.setWorkLocationId(personalInfoEmployment.getWorkLocation().getId());
					url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/getActiveIncomingTapal";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						incomingTapalList = mapper.readValue(jsonResponse,
								new TypeReference<List<IncomingTapalResponseDto>>() {
								});
						log.info("list----------" + incomingTapalList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyIncomingTapal List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return incomingTapalList;
			}

			@Override
			public Object getRowKey(IncomingTapalResponseDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public IncomingTapalResponseDto getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<IncomingTapalResponseDto> responseList = (List<IncomingTapalResponseDto>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (IncomingTapalResponseDto incomeTapal : responseList) {
					if (incomeTapal.getId().longValue() == value.longValue()) {
						selectedIncomingTapal = incomeTapal;
						return incomeTapal;
					}
				}
				return null;
			}
		};
		log.info("<===== loadLazyIncomingTapalList() Ends ======>");

	}

	public EntityMasterDTO loadEntityByEntityId(Long id) {
		EntityMasterDTO entityMaster = new EntityMasterDTO();
		try {
			String URL = AppUtil.getPortalServerURL() + "/entitymaster/getentitymasterbyentityId/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			entityMaster = mapper.readValue(jsonResponse, EntityMasterDTO.class);

			if (entityMaster == null) {
				log.info("EmployeeMasterBean loadEntityByEntityId value is null");
			}

			log.info("SupplyRateConfirmationBean entityMasterObject " + entityMaster.toString());
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
		return entityMaster;
	}

	public String createIncomeingTapal() {
		incomingTapal = new IncomingTapal();
		setState("");
		setDesignation("");
		setFileName("");
		incomingTapal.setFilePath("");
		entityTypeMaster = new EntityTypeMaster();
		districtMaster = new DistrictMaster();
		entityMaster = new EntityMaster();
		department = new Department();
		sectionMaster = new SectionMaster();
		userMaster = new UserMaster();

		incomeTapalLog = new IncomingTapalLog();
		incomeTapalNote = new IncomingTapalNote();
		incomingFileList = new ArrayList<>();
		tapalDTO = new TapalDTO();

		selectedDepartmentList = new ArrayList<>();
		selectedSectionList = new ArrayList<>();

		employeeMaster = new EmployeeMaster();
		tapalSenderType = new TapalSenderType();

		tapalDeliveryType = new TapalDeliveryType();
		setEntityFlag(false);
		entityTypeList = commonDataService.getActiveEntityType();

		deleteUploadFlag = true;

		empMasterList = new ArrayList<>();
		readOnlyFlag = false;
		supplierNameFlag = false;
		entityFlag = false;
		String appValue = commonDataService.getAppKeyValue("TAPAL_NUMBER_SHOW_FLAG");
		if (appValue != null) {
			tapalNumberStatus = Boolean.parseBoolean(appValue);
		}
		stateList = commonDataService.loadStateList();
		tapalTypeDeliveryTypeList = commonDataService.loadTapalDeliveryType();
		tapalSenderTypeList = commonDataService.loadTapalSenderType();
		departmentList = commonDataService.getDepartment();
		log.info("tapalSenderTypeList list----" + tapalSenderTypeList.size());
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.SALES_QUOTATION.toString());
		fileList = new ArrayList<>();
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();

		String s = new String();
		fileList.add(s);
		return INCOMING_TAPAL_CREATE_PAGE;
	}

	public void typeChange() {
		try {
			if (incomingTapal.getTapalType().equalsIgnoreCase("Personal")) {
				employeeList = commonDataService.getAllEmployee();

				// getLoginUserEntityEmployee();
			} else if (incomingTapal.getTapalType().equalsIgnoreCase("Offical")) {
				designationList = commonDataService.loadActiveDesignations();
				employeeList = new ArrayList<>();
				employeeMaster = new EmployeeMaster();
			}
		} catch (Exception e) {
			log.info("inside typechange method--", e);
		}
	}

	public void designationChange() {
		String url = "";
		log.info("desiction--------" + designation);
		try {
			Long workLocationId = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
			employeeList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/getEmployeeByDesignation/"
					+ workLocationId + "/" + designation;
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("employeee---------" + employeeList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside designationChange method--", e);
		}
	}

	public void departmentChange() {
		String url;
		try {
			log.info("cepartment--------" + selectedDepartmentList.size());
			BaseDTO baseDTO = new BaseDTO();
			TapalDTO tapalDTO = new TapalDTO();
			tapalDTO.setDepartmentLsit(selectedDepartmentList);
			url = APP_SERVER_URL + "/sectionmaster/getAllActiveByDepartmentList";
			baseDTO = httpService.post(url, selectedDepartmentList);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				sectionList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
				if (sectionList != null && !sectionList.isEmpty()) {
					log.info("sectionList---------" + sectionList.size());
				} else {
					log.error("Section not found");
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside departmentChange method--", e);
		}
	}

	public void sectionChange() {
		String url;
		try {
			employeeList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			log.info("selectedSection Master--------" + selectedSectionList.size());
			url = APP_SERVER_URL + "/employee/findEmployeeDetailsBySectionList";
			baseDTO = httpService.post(url, selectedSectionList);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("employeeList---------" + employeeList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside sectionChange method--", e);
		}
	}

	public void senderChange() {
		readOnlyFlag = false;
		incomingTapal.setAddressLine1("");
		incomingTapal.setAddressLine2("");
		state = "";
		entityMaster = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		incomingTapal.setPincode("");
		districtList = new ArrayList<DistrictMaster>();

		supplierMasterList = new ArrayList<>();
		supplierInfo = new SupplierMaster();
		incomingTapal.setSenderName("");
		empMasterList = new ArrayList<>();
		supplierInfo = new SupplierMaster();

		entityMaster = new EntityMaster();
		entityList = new ArrayList<>();

		try {
			log.info("sender type------" + tapalSenderType.getName());
			if (tapalSenderType.getName().equalsIgnoreCase("Entity")) {
				setEntityFlag(true);
				setReadOnlyFlag(true);
				setSupplierNameFlag(false);

				// entityTypeList = commonDataService.getEntityTypeByStatus();
				entityTypeList = commonDataService.getActiveEntityType();
			} else if (tapalSenderType.getName().equalsIgnoreCase("Society")) {

				setSupplierNameFlag(true);
				setEntityFlag(false);
				setReadOnlyFlag(true);
			} else {
				setEntityFlag(false);
				setSupplierNameFlag(false);
			}
		} catch (Exception e) {
			log.info("inside senderChange method--", e);
		}
	}

	public void entityTypeChange() {
		String url;
		try {

			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + "/entitymaster/getbyid/" + entityTypeMaster.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("entityList---------" + entityList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside entityTypeChange method--", e);
		}
	}

	public void fetchAddressForEntityId() {

		log.info("into the fetchAddressForEntityId method");
		String url;
		addressEntityMaster = new EntityMaster();
		try {
			log.info("entitytypename--------" + entityMaster.getName());
			log.info("entitytypeId--------" + entityMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/getAddress/"
					+ entityMaster.getId();
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				addressEntityMaster = mapper.readValue(jsonResponse, new TypeReference<EntityMaster>() {
				});

				try {
					if (addressEntityMaster.getAddressMaster() != null) {
						readOnlyFlag = true;
						incomingTapal.setAddressLine1(addressEntityMaster.getAddressMaster().getAddressLineOne());
						log.info("addressline" + " " + addressEntityMaster.getAddressMaster().getAddressLineOne());

						if (addressEntityMaster.getAddressMaster().getAddressLineTwo() != null) {
							incomingTapal.setAddressLine2(addressEntityMaster.getAddressMaster().getAddressLineTwo());
							log.info("addressLine2" + " " + addressEntityMaster.getAddressMaster().getAddressLineTwo());
						} else {
							log.info("address line 2 is null");

						}

						if (addressEntityMaster.getAddressMaster().getStateMaster() != null) {
							state = addressEntityMaster.getAddressMaster().getStateMaster().getName();
							log.info("the state" + " "
									+ addressEntityMaster.getAddressMaster().getStateMaster().getName());

						} else {
							log.info("stateMaster is null");
						}

						if (addressEntityMaster.getAddressMaster().getDistrictMaster() != null) {
							districtMaster = addressEntityMaster.getAddressMaster().getDistrictMaster();
							districtMaster.setId(addressEntityMaster.getAddressMaster().getDistrictMaster().getId());
							districtMaster
									.setName(addressEntityMaster.getAddressMaster().getDistrictMaster().getName());
							districtList.add(districtMaster);
							log.info("districName is" + " "
									+ addressEntityMaster.getAddressMaster().getDistrictMaster().getName());
						} else {
							log.info("DistrictMaster is null");
						}

						if (addressEntityMaster.getAddressMaster().getPostalCode() != null) {
							incomingTapal.setPincode(addressEntityMaster.getAddressMaster().getPostalCode());
							log.info("the pin code" + addressEntityMaster.getAddressMaster().getPostalCode());
						} else {
							log.info("postalCode is null");
						}
					}
					if (entityMaster.getId() != null) {
						getEmployee();
					}
				} catch (Exception e) {
					log.info("cause for exception" + e.getCause());
				}

				log.info("addressEntityMaster---------" + addressEntityMaster.toString());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside entityTypeChange method--", e);
		}

	}

	public void stateChange() {
		String url;
		try {
			log.info("statename--------" + state);
			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/getDistrictByState/" + state;
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				districtList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
				});
				log.info("districtList---------" + districtList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside stateChange method--", e);
		}
	}

	public void submitNote() {
		log.info("save method----" + tapalDTO.getNote());
	}

	public String saveIncomeingTapal() {
		log.info("save method----");
		String url = "";

		try {

			BaseDTO baseDTO = new BaseDTO();

			if (!skipApprovalFlag) {

				if (tapalDTO.getNote() == null || "".equalsIgnoreCase(tapalDTO.getNote())) {
					Util.addWarn("Note is Required");
					return null;
				}
				if (userMaster != null && userMaster.getId() != null) {
					tapalDTO.setForwardTo(userMaster.getId());
				}
			}
			if (saveValidation(true)) {
				incomingTapal.setTapalDeliveryType(tapalDeliveryType);
				incomingTapal.setTapalSenderType(tapalSenderType);
				if (employeeMaster != null) {
					incomingTapal.setEmployee(employeeMaster);
				} else {
					incomingTapal.setEmployee(null);
				}
				if (entityMaster != null && entityMaster.getId() != null) {
					incomingTapal.setEntityMaster(entityMaster);
				} else {
					incomingTapal.setEntityMaster(null);
				}
				/*
				 * if (sectionMaster != null) { incomingTapal.setSectionMaster(sectionMaster); }
				 * else { incomingTapal.setSectionMaster(null); }
				 */
				if (selectedSectionList != null && selectedSectionList.size() > 0) {
					tapalDTO.setSectionMasterList(selectedSectionList);
				} else {
					incomingTapal.setSectionMaster(null);
				}
				if (districtMaster != null) {
					incomingTapal.setDistrictMaster(districtMaster);
				} else {
					incomingTapal.setDistrictMaster(null);
				}

				if (incomingTapal.getPincode() != null) {
					String postalCode = incomingTapal.getPincode().replace(" ", "");
					incomingTapal.setPincode(postalCode);

					if (postalCode.length() > 6) {
						Util.addWarn("Pincode Must Be 6 Chracters Only");
					}
				}

				/*
				 * String fileName=""; for (int i = 0; i < fileList.size(); i++) {
				 * fileName=fileName+fileList.get(i); if (fileList.size()!=i+1) {
				 * fileName=fileName+","; } } incomingTapal.setFilePath(fileName);
				 * log.info("file name-----------"+fileName);
				 */
				log.info("incomingTapal sending to data base" + incomingTapal);
				if (incomingTapal.getReferenceDate() != null) {
					log.info("reference date" + " " + incomingTapal.getReferenceDate());
				}
				tapalDTO.setIncomingTapal(incomingTapal);
				EmployeeMaster employeMaster = loginBean.getEmployee();
				EmployeePersonalInfoEmployment personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
				tapalDTO.setWorkLocationId(personalInfoEmployment.getWorkLocation().getId());
				tapalDTO.setIncomeFileList(incomingFileList);
				url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/create";
				log.info("url...." + url);
				baseDTO = httpService.post(url, tapalDTO);
				if (baseDTO.getStatusCode() == 0) {
					if (action != null) {
						if (action.equalsIgnoreCase("ADD")) {
							errorMap.notify(ErrorDescription.INCOMINGTAPAL_SAVE.getErrorCode());
						} else if (action.equalsIgnoreCase("EDIT")) {
							selectedIncomingTapal = null;
							errorMap.notify(ErrorDescription.INCOMINGTAPAL_UPDATE.getErrorCode());
						}
					}
					return INCOMING_TAPAL_LIST_PAGE;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("inside save method--", e);
		}
		return null;
	}

	private boolean saveValidation(boolean valid) {
		valid = true;
		FacesContext fc = FacesContext.getCurrentInstance();
		if (incomingTapal.getReceivedDate() == null) {
			fc.addMessage("receivedate", new FacesMessage(ErrorCodeDescription.getDescription(35001)));
			valid = false;
		}
		if (tapalDeliveryType == null) {
			fc.addMessage("delivertype", new FacesMessage(ErrorCodeDescription.getDescription(35002)));
			valid = false;
		}
		if (StringUtils.isEmpty(incomingTapal.getToWhom())) {
			fc.addMessage("towhom", new FacesMessage(ErrorCodeDescription.getDescription(35003)));
			valid = false;
		}
		if (!StringUtils.isEmpty(incomingTapal.getToWhom())) {
			if (incomingTapal.getToWhom().equalsIgnoreCase("Individual")) {
				if (StringUtils.isEmpty(incomingTapal.getTapalType())) {
					fc.addMessage("typeid", new FacesMessage(ErrorCodeDescription.getDescription(35004)));
					valid = false;
				} else {
					if (incomingTapal.getTapalType().equalsIgnoreCase("Personal")) {
						if (employeeMaster == null) {
							fc.addMessage("peremp", new FacesMessage(ErrorCodeDescription.getDescription(35010)));
							valid = false;
						}
					} else if (incomingTapal.getTapalType().equalsIgnoreCase("Offical")) {
						if (StringUtils.isEmpty(designation)) {
							fc.addMessage("designation", new FacesMessage(ErrorCodeDescription.getDescription(35011)));
							valid = false;
						}
						if (employeeMaster == null) {
							fc.addMessage("offemp", new FacesMessage(ErrorCodeDescription.getDescription(35010)));
							valid = false;
						}
					}
				}
			} else if (incomingTapal.getToWhom().equalsIgnoreCase("Department")) {
				if (selectedDepartmentList == null) {
					fc.addMessage("department", new FacesMessage(ErrorCodeDescription.getDescription(35009)));
					valid = false;
				}
				if (selectedSectionList == null) {
					fc.addMessage("section", new FacesMessage(ErrorCodeDescription.getDescription(35012)));
					valid = false;
				}
			}
		}
		if (tapalSenderType == null) {
			fc.addMessage("senderType", new FacesMessage(ErrorCodeDescription.getDescription(35005)));
			valid = false;
		}
		if (tapalSenderType != null) {
			if (null != tapalSenderType.getName() && tapalSenderType.getName().equalsIgnoreCase("Entity")) {
				if (entityTypeMaster == null) {
					fc.addMessage("entityType", new FacesMessage(ErrorCodeDescription.getDescription(35006)));
					valid = false;
				}
				if (entityMaster == null) {
					fc.addMessage("entityid", new FacesMessage(ErrorCodeDescription.getDescription(35007)));
					valid = false;
				}
			}
		}
		if (!tapalSenderType.getName().equalsIgnoreCase("Entity")
				&& StringUtils.isEmpty(incomingTapal.getSenderName())) {
			fc.addMessage("senderName", new FacesMessage(ErrorCodeDescription.getDescription(35008)));
			valid = false;
		}

		if (!skipApprovalFlag) {
			if (tapalDTO.getForwardTo() == null) {
				fc.addMessage("fwdto", new FacesMessage(ErrorCodeDescription.getDescription(35029)));
				valid = false;
			}
			if (tapalDTO.getNote() == null && tapalDTO.getNote().equals("")) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				valid = false;
			}

			if (tapalDTO.getForwardFor() == null) {
				fc.addMessage("fwdfor", new FacesMessage(ErrorCodeDescription.getDescription(35030)));
				valid = false;
			}

		}
		return valid;

	}

	public void onRowSelect(SelectEvent event) {
		/*
		 * editButtonFlag = false; deleteButtonFlag = false; addButtonFlag = true;
		 */
		selectedIncomingTapal = (IncomingTapalResponseDto) event.getObject();
		log.info("onRowSelect Stage ==> " + selectedIncomingTapal.getStage());
		log.info("id-----------" + selectedIncomingTapal.getId());
		if (selectedIncomingTapal.getStage().equalsIgnoreCase("RECEIVED")) {
			deleteButtonFlag = true;
			addButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = false;
		}
		if (selectedIncomingTapal.getStage().equalsIgnoreCase("NOTRECEIVED")) {
			deleteButtonFlag = true;
			addButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = false;
		}
		if (selectedIncomingTapal.getStage().equalsIgnoreCase("FINALAPPROVED")) {
			deleteButtonFlag = true;
			addButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = false;
		}
		if (selectedIncomingTapal.getStage().equalsIgnoreCase("Approved")) {
			deleteButtonFlag = true;
			addButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = false;

		} else if (selectedIncomingTapal.getStage().equalsIgnoreCase("Rejected")) {
			editButtonFlag = false;
			addButtonFlag = true;
			deleteButtonFlag = true;
			viewButtonFlag = false;

		} else if (selectedIncomingTapal.getStage().equalsIgnoreCase("SUBMITTED")) {
			deleteButtonFlag = false;
			addButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = false;
		}
		log.info("deleteButtonFlag-----> " + deleteButtonFlag);
		log.info("addButtonFlag-----> " + addButtonFlag);
		log.info("editButtonFlag-----> " + editButtonFlag);
		log.info("viewButtonFlag-----> " + viewButtonFlag);
	}

	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		selectedIncomingTapal = null;
		loadLazyIncomingTapalList();
		return INCOMING_TAPAL_LIST_PAGE;
	}

	public String viewincomeingTapal() {
		String url;
		departmentNameList = new HashSet<>();
		sectionNameList = new ArrayList<>();
		selectedDepartmentList = new ArrayList<>();
		fileList = new ArrayList<>();
		incomingFileList = new ArrayList<>();
		tapalNumberStatus = false;
		setDesignation(null);
		setEntityFlag(false);

		try {
			log.info("id-----------" + selectedIncomingTapal.getId());
			// employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			employeeList = commonDataService.getAllEmployee();
			BaseDTO baseDTO = new BaseDTO();
			tapalDTO = new TapalDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/get/"
					+ selectedIncomingTapal.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				getViewNoteEmployeeDetails();
				incomingTapal = new IncomingTapal();
				editButtonFlag = true;
				deleteButtonFlag = true;
				addButtonFlag = false;
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalDTO = mapper.readValue(jsonResponse, new TypeReference<TapalDTO>() {
				});
				log.info("note==> " + tapalDTO.getNote());
				incomingTapalNote = tapalDTO.getNote();
				incomingTapal = tapalDTO.getIncomingTapal();
				if (incomingTapal.getEmployee() != null) {
					employeeMaster = incomingTapal.getEmployee();
					log.info("emp name==> " + employeeMaster.getFirstName());
				} else {
					log.error("Incoming tapal employee not found");
				}
				selectedSectionList = tapalDTO.getSectionMasterList();
				forwardToUsersList = commonDataService
						.loadForwardToUsersByFeature(AppFeatureEnum.SALES_QUOTATION.toString());
				if (forwardToUsersList != null && !forwardToUsersList.isEmpty()) {
					log.info("forwardToUsersList size==>" + forwardToUsersList.size());
				} else {
					log.error("forward to user not found");
				}

				if (incomingTapal.getFilePath() != null && !incomingTapal.getFilePath().isEmpty()) {
					String[] filelist = incomingTapal.getFilePath().split(",");
					String[] fileFinalList = new String[filelist.length];
					for (int fileSize = 0; fileSize < filelist.length; fileSize++) {
						String fileValue = filelist[fileSize];
						if (fileValue != null && !fileValue.isEmpty()) {
							String filePathValue[] = fileValue.split("uploaded/");
							if (filePathValue != null) {
								log.info("0==>" + filePathValue[0]);
								log.info("1==>" + filePathValue[1]);
								if (fileFinalList.length == 0) {
									fileFinalList[fileFinalList.length] = filePathValue[1];
								} else {
									fileFinalList[fileSize] = filePathValue[1];
								}
							} else {
								log.error("Uploaded file not found");
							}
						} else {
							log.error("Uploaded file not found");
						}
					}
					fileList = new ArrayList<String>(Arrays.asList(fileFinalList));
					incomingFileList = new ArrayList<String>(Arrays.asList(filelist));

					for (int i = 0; i < fileList.size(); i++) {
						log.info("file name-------------" + fileList.get(i));
					}
				}

				if (tapalDTO.getIncomingTapalLog() != null) {
					log.info("Tapal list.....:" + tapalDTO.getIncomingTapalLog());
					setPreviousApproval(tapalDTO.getForwardFor());

					if ("VIEW".equalsIgnoreCase(action)) {
						if (tapalDTO.getForwardTo() != null) {
							if (loginBean.getUserMaster().getId().equals(tapalDTO.getForwardTo())) {
								tapalDTO.setRemarks(null);
								if (IncomingTapalStatus.REJECTED.equals(tapalDTO.getIncomeTapalStatus())) {
									approvalFlag = false;
								} else {
									approvalFlag = true;
								}

							} else {
								approvalFlag = false;
							}
						}
						EmployeeMaster logEmployee = commonDataService.loadEmployeeLoggedInUserDetails();
						log.info("logged user id==> " + loginBean.getUserMaster().getId());
						log.info("logEmployee emp id==> " + logEmployee.getId());
						log.info("Incoming tapal emp id==> " + incomingTapal.getEmployee().getId());
						if (logEmployee.getId().equals(incomingTapal.getEmployee().getId())) {
							log.info("Tapal final Status ==> " + tapalDTO.getIncomeTapalStatus());
							if (IncomingTapalStatus.FINALAPPROVED.equals(tapalDTO.getIncomeTapalStatus())) {
								receivedFlag = true;
							} else {
								receivedFlag = false;
							}
						} else {
							EmployeeMaster employeMaster = loginBean.getEmployee();
							if (employeMaster != null && employeMaster.getPersonalInfoEmployment() != null
									&& employeMaster.getPersonalInfoEmployment().getCurrentSection() != null
									&& employeMaster.getPersonalInfoEmployment().getCurrentSection().getId() != null) {
								if (IncomingTapalStatus.FINALAPPROVED.equals(tapalDTO.getIncomeTapalStatus())
										&& selectedSectionList.contains(
												employeMaster.getPersonalInfoEmployment().getCurrentSection())) {
									receivedFlag = true;
								} else {
									receivedFlag = false;
								}
							}

						}
						setFinalApproveFlag(tapalDTO.getForwardFor());
						if (IncomingTapalStatus.FINALAPPROVED.equals(tapalDTO.getIncomeTapalStatus())) {
							buttonFlag = true;
							hideflag = true;
						}
						if (loginBean.getUserMaster().getId().equals(tapalDTO.getForwardTo())) {
							hideflag = false;

						}
						tapalDTO.setRemarks(null);
					}
				}

				for (SectionMaster sec : selectedSectionList) {
					departmentNameList.add(sec.getDepartment().getName());
					sectionNameList.add(sec.getName());
					selectedDepartmentList.add(sec.getDepartment());
				}
				if (tapalDTO.getIncomingTapal().getToWhom().equals("Department")) {
					departmentChange();
				}
				log.info("incomingTapal from data base" + incomingTapal);

				try {
					setSectionMaster(incomingTapal.getSectionMaster());
					setTapalDeliveryType(incomingTapal.getTapalDeliveryType());
					setTapalSenderType(incomingTapal.getTapalSenderType());
					if (incomingTapal.getEmployee() != null)
						setEmployeeMaster(incomingTapal.getEmployee());
					if (incomingTapal.getEntityMaster() != null) {
						setEntityMaster(incomingTapal.getEntityMaster());
						setEntityTypeMaster(incomingTapal.getEntityMaster().getEntityTypeMaster());
						setEntityFlag(true);
					}
					if (incomingTapal.getDistrictMaster() != null)
						setDistrictMaster(incomingTapal.getDistrictMaster());
					log.info("paymentMode......" + incomingTapal.getPaymentMode());
				} catch (Exception e) {
					log.info("Exception while get data incomingTapal------");
				}

				if (incomingTapal.getDistrictMaster() != null
						&& incomingTapal.getDistrictMaster().getStateMaster() != null) {
					setState(districtMaster.getStateMaster().getName());
				}

				if (incomingTapal.getSectionMaster() != null
						&& incomingTapal.getSectionMaster().getDepartment() != null) {
					setDepartment(incomingTapal.getSectionMaster().getDepartment());
				}

				if (incomingTapal.getTapalType() != null) {
					if (incomingTapal.getTapalType().equalsIgnoreCase("Offical")) {
						if (employeeMaster != null) {
							if (null != employeeMaster.getPersonalInfoEmployment()
									&& null != employeeMaster.getPersonalInfoEmployment().getCurrentDesignation()) {
								setDesignation(
										employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getName());
							}
						}
					}
				}
				if (action.equalsIgnoreCase("EDIT")) {

					deleteUploadFlag = false;
					if (fileList == null || fileList.isEmpty()) {
						fileList = new ArrayList<>();
						String s = new String();
						fileList.add(s);

						deleteUploadFlag = true;
						incomingFileList = new ArrayList<>();
					} else if (fileList.size() == 1) {
						deleteUploadFlag = true;

					}

					incomingTapalEdit(incomingTapal);
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside viewincomeingTapal method--", e);
		}
		return INCOMING_TAPAL_VIEW_PAGE;
	}

	public List<EmployeeMaster> loadAllActiveemployee(String searchParam) {
		try {
			if (searchParam != null) {
				String serverUrl = APP_SERVER_URL;
				employeeList = commonDataService.loadAutoCompletegetAllActiveEmployee(serverUrl, searchParam);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return employeeList;
	}

	private void incomingTapalEdit(IncomingTapal incomingTapal) {
		try {
			tapalTypeDeliveryTypeList = commonDataService.loadTapalDeliveryType();
			tapalSenderTypeList = commonDataService.loadTapalSenderType();
			if (getDistrictMaster() != null) {
				stateChange();
				stateList = commonDataService.loadStateList();
			}
			if (getEntityMaster() != null) {
				// entityTypeList = commonDataService.getEntityTypeByStatus();
				entityTypeList = commonDataService.getActiveEntityType();
				entityTypeChange();
			}
			if (getDepartment() != null) {
				departmentList = commonDataService.getDepartment();
				departmentChange();
			}
			if (incomingTapal.getTapalType() != null) {
				if (incomingTapal.getTapalType().equalsIgnoreCase("Personal")) {
					employeeList = commonDataService.getAllEmployee();
					log.info("employee list-----" + employeeList.size());
					if (incomingTapal.getEmployee() != null)
						setEmployeeMaster(incomingTapal.getEmployee());
				} else if (incomingTapal.getTapalType().equalsIgnoreCase("Offical")) {
					designationList = commonDataService.loadActiveDesignations();
					if (employeeMaster != null) {
						setDesignation(employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getName());
					}
				}
			}
			if (getDesignation() != null && !getDesignation().isEmpty()) {
				designationChange();
			}
			if (incomingTapal.getToWhom().equalsIgnoreCase("Department")) {
				departmentList = commonDataService.getDepartment();
				departmentChange();
				sectionChange();
			}

			if (tapalSenderType.getName().equalsIgnoreCase("Entity")) {
				setEntityFlag(true);
				setReadOnlyFlag(true);
				setSupplierNameFlag(false);

				setEntityMaster(incomingTapal.getEntityMaster());

				getEmployee();

			} else if (tapalSenderType.getName().equalsIgnoreCase("Society")) {

				setSupplierNameFlag(true);
				setEntityFlag(false);
				setReadOnlyFlag(true);
				supplierInfo.setName(incomingTapal.getSenderName());
			} else {
				setEntityFlag(false);
				setSupplierNameFlag(false);
			}

		} catch (Exception e) {
			log.info("inside edit method exception------", e);
		}
	}

	public void deleteIncomeingTapal() {
		String url;
		try {
			log.info("id-----------" + selectedIncomingTapal.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/deleteById/"
					+ selectedIncomingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INCOMINGTAPAL_DELETE.getErrorCode());
				loadLazyIncomingTapalList();
				editButtonFlag = true;
				deleteButtonFlag = true;
				addButtonFlag = false;
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("inside deleteIncomeingTapal method--", e);
		}
	}

	/**
	 * @param event
	 */
	public void tapalFileUpload(FileUploadEvent event) {
		log.info("<====== Incoming tapal Upload Event =====>" + event);
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		if (event != null)
			log.info("index" + event.getComponent().getAttributes().get("index"));
		String indexValue = event.getComponent().getAttributes().get("index").toString();
		int listIndex = Integer.parseInt(indexValue);
		uploadedFile = event.getFile();

		String type = FilenameUtils.getExtension(uploadedFile.getFileName());
		log.info(" File Type :: " + type);
		if (!type.equalsIgnoreCase("png") && !type.equalsIgnoreCase("jpg") && !type.equalsIgnoreCase("doc")
				&& !type.equalsIgnoreCase("jpeg") && !type.equalsIgnoreCase("gif") && !type.equalsIgnoreCase("docx")
				&& !type.equalsIgnoreCase("pdf") && !type.equalsIgnoreCase("xlsx") && !type.equalsIgnoreCase("xls")) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
			return;
		}

		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("TAPAL_UPLOAD_FILE_SIZE"));
		letterSize = letterSize / 1000;
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String incomingTapalUploadPath = commonDataService.getAppKeyValue("INCOMING_TAPAL_UPLOAD_PATH");

		// String incomingTapalUploadPath =
		// commonDataService.getFilePath("INCOMING_TAPAL_UPLOAD_PATH", "FILE");

		log.info("incomingTapalUploadPath >>>> " + incomingTapalUploadPath);

		String filePath = Util.fileUpload(incomingTapalUploadPath, uploadedFile);
		setFileName(uploadedFile.getFileName());
		incomingTapal.setFilePath(filePath);
		incomingFileList.add(filePath);
		for (int i = 0; i < fileList.size(); i++) {
			String name = new String();
			name = uploadedFile.getFileName();
			// fileList.set(fileList.size() - 1, name);
			fileList.set(listIndex, name);
		}
		RequestContext.getCurrentInstance().execute("PF('pridocupload').hide();");
		errorMap.notify(ErrorDescription.INCOMINGTAPAL_FILEUPLOAD.getErrorCode());
		log.info(" tapalFileUpload  is uploaded successfully");
	}

	public void addRow() {
		setFileName("");
		String s = new String();
		fileList.add(s);
		deleteUploadFlag = false;

	}

	public void deleteRow(int rowIndex) {
		log.info("row index------" + rowIndex);

		log.info("fileList size---------" + fileList.size());
		if (fileList.size() == 1) {

			deleteUploadFlag = true;

		} else {
			fileList.remove(rowIndex);
			incomingFileList.remove(rowIndex);
		}

	}

	public void downloadfile() {
		InputStream input = null;
		try {
			log.info("download file path==> " + incomingTapal.getFilePath());
			String incomingTapalUploadPath = commonDataService.getAppKeyValue("INCOMING_TAPAL_UPLOAD_PATH");
			log.info("download incomingTapalUploadPath==> " + incomingTapalUploadPath);
			File file = new File(incomingTapalUploadPath + incomingTapal.getFilePath());
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void downloadIncomingFile(String downloadFile) {
		InputStream input = null;
		try {
			log.info("downloadIncomingFile downloadFile==> " + downloadFile);
			String incomingTapalUploadPath = commonDataService.getAppKeyValue("INCOMING_TAPAL_UPLOAD_PATH");
			log.info("download incomingTapalUploadPath==> " + incomingTapalUploadPath);
			File files = new File(incomingTapalUploadPath + "/uploaded/" + downloadFile);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public String approveTapal() {
		// This is the View Page ForwardTo approved payment
		log.error("<---- Start approveIncomingTapal final approval: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("IncomeTapal id---------- " + tapalDTO.getIncomingTapal().getId());
			log.info("previousApproval ---------- " + previousApproval);
			log.info("current approveal ---------- " + tapalDTO.getForwardFor());
			log.info("Forward to ---------- " + tapalDTO.getForwardTo());

			baseDTO = new BaseDTO();
			if (previousApproval != true && tapalDTO.getForwardTo() == null) {
				errorMap.notify(ErrorDescription.FORWARD_TO_EMPTY.getErrorCode());
				log.info("Forward To is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (tapalDTO.getForwardFor() == null) {
				errorMap.notify(ErrorDescription.FORWARD_FOR_ISEMPTY.getErrorCode());
				log.info("Forward for is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (tapalDTO.getRemarks() == null || "".equalsIgnoreCase(tapalDTO.getRemarks())) {
				Util.addWarn("Remarks is required");
				log.info("Remarks is empty-----------------");
				return null;
			}
			if (previousApproval == false && tapalDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Approved  ----->");
				tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.APPROVED);
				tapalDTO.setForwardTo(userMaster.getId());
			} else if (previousApproval == true && tapalDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Final Approved  ----->");
				tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.FINALAPPROVED);
			} else {
				log.error("<---- Approve Status change on Approved  ----->");
				tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.APPROVED);
				tapalDTO.setForwardTo(userMaster.getId());
			}
			log.info("approve remarks=========>" + tapalDTO.getRemarks());
			String url = APP_SERVER_URL + appPreference.getOperationApiUrl()
					+ "/incomingtapal/approveOrRejectIncomeTapal";
			EmployeeMaster employeMaster = loginBean.getEmployee();
			EmployeePersonalInfoEmployment personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
			tapalDTO.setWorkLocationId(personalInfoEmployment.getWorkLocation().getId());
			baseDTO = httpService.post(url, tapalDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(AdminErrorCode.INCOMING_TAPAL_APPROVED_SUCCESSFULLY).getErrorCode());
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCOMING_TAPAL_LOG_INSERTED_SUCCESSFULLY).getCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				RequestContext.getCurrentInstance().execute("PF('dlgComments3').hide();");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("approveIncomingTapal method inside exception-------", e);
		}
		return null;
	}

	public String receivedTapal() {
		log.error("<----Start IncomingTapalBean.receivedTapal: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("IncomingTapal selected id----------" + tapalDTO.getIncomingTapal().getId());
			baseDTO = new BaseDTO();
			if (tapalDTO.getRemarks() == null || "".equalsIgnoreCase(tapalDTO.getRemarks())) {
				Util.addWarn("Remarks is required");
				log.info("Remarks is empty-----------------");
				return null;
			}
			tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.RECEIVED);
			String url = APP_SERVER_URL + appPreference.getOperationApiUrl()
					+ "/incomingtapal/approveOrRejectIncomeTapal";
			baseDTO = httpService.post(url, tapalDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(AdminErrorCode.INCOMING_TAPAL_RECEIVED_SUCCESSFULLY).getErrorCode());
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCOMING_TAPAL_LOG_INSERTED_SUCCESSFULLY).getCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().execute("PF('receivedId').hide();");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("rejectIncomingTapal method inside exception-------", e);
		}
		return null;
	}

	public String notReceivedTapal() {
		log.error("<----Start IncomingTapalBean.notReceivedTapal: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("IncomingTapal selected id----------" + tapalDTO.getIncomingTapal().getId());
			baseDTO = new BaseDTO();
			if (tapalDTO.getRemarks() == null || "".equalsIgnoreCase(tapalDTO.getRemarks())) {
				Util.addWarn("Remarks is required");
				log.info("Remarks is empty-----------------");
				return null;
			}
			tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.NOTRECEIVED);
			String url = APP_SERVER_URL + appPreference.getOperationApiUrl()
					+ "/incomingtapal/approveOrRejectIncomeTapal";
			baseDTO = httpService.post(url, tapalDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.INCOMING_TAPAL_NOT_RECEIVED).getErrorCode());
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCOMING_TAPAL_LOG_INSERTED_SUCCESSFULLY).getCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().execute("PF('notReceived').hide();");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("rejectIncomingTapal method inside exception-------", e);
		}
		return null;
	}

	public String rejectTapal() {
		// This is the View Page ForwardTo rejected payment
		log.error("<----Start IncomingTapalBean.rejected: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("IncomingTapal selected id----------" + tapalDTO.getIncomingTapal().getId());
			baseDTO = new BaseDTO();
			tapalDTO.setIncomeTapalStatus(IncomingTapalStatus.REJECTED);
			if (userMaster != null && userMaster.getId() != null) {
				tapalDTO.setForwardTo(userMaster.getId());
			}
			if (tapalDTO.getRemarks() == null || "".equalsIgnoreCase(tapalDTO.getRemarks())) {
				Util.addWarn("Remarks is Required");
				return null;
			}
			String url = APP_SERVER_URL + appPreference.getOperationApiUrl()
					+ "/incomingtapal/approveOrRejectIncomeTapal";
			baseDTO = httpService.post(url, tapalDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(AdminErrorCode.INCOMING_TAPAL_REJECTED_SUCCESSFULLY).getErrorCode());
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCOMING_TAPAL_LOG_INSERTED_SUCCESSFULLY).getCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().execute("PF('dlgComments4').hide();");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("rejectIncomingTapal method inside exception-------", e);
		}
		return null;
	}

	public void getEmployee() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......IncomingTapalBean getEmployee.. By selecting Entity...id........." + entityMaster.getId());

			String url = APP_SERVER_URL + "/employee/findactiveemplistbyentityid/" + entityMaster.getId();
			log.info("getEmployee url==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("empMasterList==> " + empMasterList);
			} else {
				empMasterList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("Exception in getEmployee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete query==>" + query);

		supplierMasterList = loadSupplierMaster(query);
		return supplierMasterList;
	}

	public List<SupplierMaster> loadSupplierMaster(String query) {
		BaseDTO baseDTO = new BaseDTO();
		List<SupplierMaster> supplierMasterList;
		try {
			log.info(".......IncomingTabal Bean loadSupplierMaster..............");

			String url = APP_SERVER_URL + appPreference.getOperationApiUrl()
					+ "/incomingtapal/loadsupplierautocomplete/" + query;

			log.info("loadSupplierMaster==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMasterList size==> " + supplierMasterList.size());
			} else {
				supplierMasterList = new ArrayList<>();
			}
			return supplierMasterList;
		} catch (Exception e) {
			log.error("Exception in loadSupplierMaster  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void fetchAddressForSupplierId() {

		log.info("into the fetchAddressForSupplierId method");

		try {
			incomingTapal.setSenderName(supplierInfo.getName());
			if (supplierInfo.getId() != null && supplierInfo.getAddressId().getId() != null) {

				readOnlyFlag = true;
				incomingTapal.setAddressLine1(supplierInfo.getAddressId().getAddressLineOne());
				log.info("addressline" + " " + supplierInfo.getAddressId().getAddressLineOne());

				if (supplierInfo.getAddressId().getAddressLineTwo() != null) {
					incomingTapal.setAddressLine2(supplierInfo.getAddressId().getAddressLineTwo());
					log.info("addressLine2" + " " + supplierInfo.getAddressId().getAddressLineTwo());
				} else {
					log.info("address line 2 is null");

				}

				if (supplierInfo.getAddressId().getStateMaster() != null) {
					state = supplierInfo.getAddressId().getStateMaster().getName();
					log.info("the state" + " " + supplierInfo.getAddressId().getStateMaster().getName());

				} else {
					log.info("stateMaster is null");
				}

				if (supplierInfo.getAddressId().getDistrictMaster() != null) {
					districtMaster = supplierInfo.getAddressId().getDistrictMaster();
					districtMaster.setId(districtMaster == null ? null : districtMaster.getId());
					districtMaster.setName(districtMaster == null ? "" : districtMaster.getName());
					districtList.add(districtMaster);
					log.info("districName is" + " " + supplierInfo.getAddressId().getDistrictMaster().getName());
				} else {
					log.info("DistrictMaster is null");
				}

				if (supplierInfo.getAddressId().getPostalCode() != null) {
					incomingTapal.setPincode(supplierInfo.getAddressId().getPostalCode());
					log.info("the pin code" + supplierInfo.getAddressId().getPostalCode());
				} else {
					log.info("postalCode is null");
				}
			}
		} catch (Exception e) {
			log.info("inside fetchAddressForSupplierId method--", e);
		}
	}

	public void getLoginUserEntityEmployee() {
		BaseDTO baseDTO = new BaseDTO();
		try {

			Long loginUserEntityId = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
			log.info(".......IncomingTapalBean getLoginUserEntityEmployee.. By selecting Entity...id........."
					+ loginUserEntityId);

			String url = APP_SERVER_URL + "/employee/getemployeebyentity/" + loginUserEntityId;
			log.info("getEmployee url==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("employeeList==> " + employeeList);
			} else {
				employeeList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("Exception in getLoginUserEntityEmployee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void getViewNoteEmployeeDetails() {
		String url = "";
		Long incomingTapalId = null;
		try {
			incomingTapalId = selectedIncomingTapal == null ? null : selectedIncomingTapal.getId();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/incomingtapal/viewNoteEmployeeDetails/"
					+ incomingTapalId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				incomingTapalNoteEmployeeList = mapper.readValue(jsonResponse, new TypeReference<List<TapalDTO>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception getViewNoteEmployeeDetails", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}
}
