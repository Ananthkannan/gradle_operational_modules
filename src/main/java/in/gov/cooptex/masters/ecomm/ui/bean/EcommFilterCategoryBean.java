package in.gov.cooptex.masters.ecomm.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.ecomm.model.EcommFilterCategory;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("ecommFilterCategoryBean")
@Scope("session")
@Log4j2
public class EcommFilterCategoryBean {

	final String LIST_PAGE = "/pages/masters/ecommerce/listEcommFilterCategory.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/ecommerce/createEcommFilterCategory.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ECOMM_FILTER_CATEGORY_URL = AppUtil.getPortalServerURL() + "/ecommfiltercategory";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EcommFilterCategory ecommFilterCategory, selectedEcommFilterCategory;

	@Getter
	@Setter
	LazyDataModel<EcommFilterCategory> lazyEcommFilterCategoryList;

	List<EcommFilterCategory> ecommFilterCategoryList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showEcommFilterCategoryListPage() {
		log.info("<==== Starts EcommFilterCategoryBean.showEcommFilterCategoryListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		ecommFilterCategory = new EcommFilterCategory();
		selectedEcommFilterCategory = new EcommFilterCategory();
		loadLazyEcommFilterCategoryList();
		log.info("<==== Ends EcommFilterCategoryBean.showEcommFilterCategoryListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyEcommFilterCategoryList() {
		log.info("<===== Starts EcommFilterCategoryBean.loadLazyEcommFilterCategoryList ======>");

		lazyEcommFilterCategoryList = new LazyDataModel<EcommFilterCategory>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EcommFilterCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = ECOMM_FILTER_CATEGORY_URL + "/getallecommfiltercategorylistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						ecommFilterCategoryList = mapper.readValue(jsonResponse,
								new TypeReference<List<EcommFilterCategory>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return ecommFilterCategoryList;
			}

			@Override
			public Object getRowKey(EcommFilterCategory res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EcommFilterCategory getRowData(String rowKey) {
				for (EcommFilterCategory ecommFilterCategory : ecommFilterCategoryList) {
					if (ecommFilterCategory.getId().equals(Long.valueOf(rowKey))) {
						selectedEcommFilterCategory = ecommFilterCategory;
						return ecommFilterCategory;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends EcommFilterCategoryBean.loadLazyEcommFilterCategoryList ======>");
	}

	public String showEcommFilterCategoryListPageAction() {
		log.info("<===== Starts EcommFilterCategoryBean.showEcommFilterCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedEcommFilterCategory == null || selectedEcommFilterCategory.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ECOMM_FILTER_CATEGORY.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create ecommFilterCategory called..");
				ecommFilterCategory = new EcommFilterCategory();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit ecommFilterCategory called..");
				String url = ECOMM_FILTER_CATEGORY_URL + "/getbyid/" + selectedEcommFilterCategory.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					ecommFilterCategory = mapper.readValue(jsonResponse, new TypeReference<EcommFilterCategory>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete ecommFilterCategory called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEcommFilterCategoryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showEcommFilterCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EcommFilterCategoryBean.showEcommFilterCategoryListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts EcommFilterCategoryBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(ECOMM_FILTER_CATEGORY_URL + "/deletebyid/" + selectedEcommFilterCategory.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEcommFilterCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete ecommFilterCategory ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends EcommFilterCategoryBean.deleteConfirm ===========>");
		return showEcommFilterCategoryListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts EcommFilterCategoryBean.saveOrUpdate =======>");
		try {
			if (ecommFilterCategory.getCode() == null) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_CODE_EMPTY.getCode());
				return null;
			}
			if (ecommFilterCategory.getName() == null || ecommFilterCategory.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_NAME_EMPTY.getCode());
				return null;
			}
			if (ecommFilterCategory.getName().trim().length() < 3
					|| ecommFilterCategory.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (ecommFilterCategory.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_STATUS_EMPTY.getCode());
				return null;
			}
			String url = ECOMM_FILTER_CATEGORY_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, ecommFilterCategory);
			if (response != null && response.getStatusCode() == 0) {
				log.info("ecommFilterCategory saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommFilterCategoryBean.saveOrUpdate =======>");
		return showEcommFilterCategoryListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EcommFilterCategoryBean.onRowSelect ========>" + event);
		selectedEcommFilterCategory = ((EcommFilterCategory) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends EcommFilterCategoryBean.onRowSelect ========>");
	}

}
