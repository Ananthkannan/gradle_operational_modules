package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.hrms.model.IncometaxSavingsScheme;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("incometaxSavingSchemeBean")
@Scope("session")
public class IncometaxSavingSchemeBean {
	
	private final String CREATE_INCOMETAXSAVINGSCHEME_PAGE = "/pages/masters/incometaxsavingscheme/createIncometaxSavingScheme.xhtml?faces-redirect=true;";
	private final String LIST_INCOMETAXSAVINGSCHEME_PAGE = "/pages/masters/incometaxsavingscheme/listIncometaxSavingScheme.xhtml?faces-redirect=true;";
	private final String VIEW_INCOMETAXSAVINGSCHEME_PAGE = "/pages/masters/incometaxsavingscheme/viewIncometaxSavingScheme.xhtml?faces-redirect=true;";
	
	final String SERVER_URL = AppUtil.getPortalServerURL();
	
	
	ObjectMapper mapper=new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	IncometaxSavingsScheme incometaxSavingsScheme, selectedIncometaxSavingsScheme; 
	
	@Getter
	@Setter
	List<IncometaxSavingsScheme> incometaxSavingsSchemeList = new ArrayList<IncometaxSavingsScheme>();
	
	
	
	@Getter
	@Setter
	LazyDataModel<IncometaxSavingsScheme> lazyIncometaxSavingsSchemeList;
	
	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;
	
	@Getter
	@Setter
	private int incometaxSavingsSchemeListSize;

	
	/*** Incometax Savings Scheme all list here ***/
	public String clear() {
		log.info("IncometaxSavingSchemeBean. clear() - START");
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		try {
			incometaxSavingsScheme = new IncometaxSavingsScheme();
            selectedIncometaxSavingsScheme = new IncometaxSavingsScheme();			
		}
		catch (Exception e) {
			log.error("Exception at showAllIncometaxSavinsSchemeList  " + e);
		}
		log.info("IncometaxSavingSchemeBean. clear() - END");
		return LIST_INCOMETAXSAVINGSCHEME_PAGE;
		
	}
	
	

	/*** Incometax Savings Scheme all list here ***/
	public String showAllIncometaxSavinsSchemeList() {
		log.info(" inside the showAll Incometax Savings SchemeList");
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		try {
			incometaxSavingsScheme = new IncometaxSavingsScheme();
            selectedIncometaxSavingsScheme = new IncometaxSavingsScheme();			
			loadLazyIncometaxSavingsSchemeList();
		}
		catch (Exception e) {
			log.error("Exception at showAllIncometaxSavinsSchemeList  " + e);
		}
		
		return LIST_INCOMETAXSAVINGSCHEME_PAGE;
		
	}
	

	private void loadLazyIncometaxSavingsSchemeList() {

		log.info("<===== Starts IncometaxSavingsSchemeBean.loadLazyIncometaxSavingsSchemeList ======>");
		lazyIncometaxSavingsSchemeList = new LazyDataModel<IncometaxSavingsScheme>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<IncometaxSavingsScheme> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL+"/incometaxsavingscheme/search";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						incometaxSavingsSchemeList = mapper.readValue(jsonResponse,new TypeReference<List<IncometaxSavingsScheme>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyIncometaxSavingsSchemeList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return incometaxSavingsSchemeList;
			}

			@Override
			public Object getRowKey(IncometaxSavingsScheme res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public IncometaxSavingsScheme getRowData(String rowKey) {
				try {
					for (IncometaxSavingsScheme incometaxData : incometaxSavingsSchemeList) {
						if (incometaxData.getId().equals(Long.valueOf(rowKey))) {
							selectedIncometaxSavingsScheme = incometaxData;
							return incometaxData;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends IncometaxSavingsSchemeBean.loadLazyIncometaxSavingsSchemeList======>");
	
		
	}
	
	/***IncometaxSavings Scheme Grid OnRow Select Here ***/
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts IncometaxSavingsScheme.onRowSelect ========>" + event);
		selectedIncometaxSavingsScheme = ((IncometaxSavingsScheme) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends IncometaxSavingsScheme.onRowSelect ========>");
	}

	
	/*** Incometax Savings Scheme all list action Here ***/
	public String incometaxSchemeListPageAction() {
		try {
		if(action.equalsIgnoreCase("Create")) {
			log.info("called add IncometaxScheme Savings page");
			incometaxSavingsScheme = new IncometaxSavingsScheme();	
			return CREATE_INCOMETAXSAVINGSCHEME_PAGE;
			
		}
		else if(action.equalsIgnoreCase("View")) {
			log.info("called view Incometax Savings Scheme  page");	
			incometaxSavingsSchemeValuebyId(selectedIncometaxSavingsScheme.getId());
			return VIEW_INCOMETAXSAVINGSCHEME_PAGE;
			
		}
		else if(action.equalsIgnoreCase("Edit")) {
			log.info("called Edit Incometax Savings Scheme page");	
			incometaxSavingsSchemeValuebyId(selectedIncometaxSavingsScheme.getId());
			return CREATE_INCOMETAXSAVINGSCHEME_PAGE;
			
		}
		else if(action.equalsIgnoreCase("Delete")) {
			log.info("called Delete Incometax Savings Scheme  ==>>> IncometaxScheme ID ==>> ");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmIncometaxSavingsSchemeDelete').show();");
		}
		else if(action.equalsIgnoreCase("Clear")) {
			log.info("called Clear incometaxSavingsScheme page");	
			showAllIncometaxSavinsSchemeList();
			//return LIST_INCOMETAXSAVINGSCHEME_PAGE;
		}
		}
		catch (Exception e) {
			log.error(" Exception at IncometaxSavingsScheme ListPageAction " + e);
		}
		
		return null;
	}
	

	
	/*** Delete Incometax Savings Scheme Confirm ***/
	public String deleteConfirmIncometaxSchemesSaving() {
		log.info("<===== Starts IncometaxSavingsScheme.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + "/incometaxsavingscheme/deleteById/" + selectedIncometaxSavingsScheme.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.INCOMETAXSAVINGSSCHEME_DELETED_SUCCESSFULLY).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmIncometaxSavingsSchemeDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete incometax Savings Scheme ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends IncometaxSavingsScheme.deleteConfirm ===========>");
		return showAllIncometaxSavinsSchemeList();
	}
	
	/*** Edit and View Incometax Savings Scheme here ***/
	public void incometaxSavingsSchemeValuebyId(Long id) {
		try {
			log.info("called incometaxSavingsSchemeValuebyId Start..");
			String url = SERVER_URL + "/incometaxsavingscheme/getIncometaxSavingsSchemeById/"
					+ id;
			log.info("incometaxSavingsSchemeValuebyId URL ====>>>  " + url);
			BaseDTO baseDTO = httpService.get(url);
			selectedIncometaxSavingsScheme = new IncometaxSavingsScheme();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				incometaxSavingsScheme = mapper.readValue(jsonResponse, new TypeReference<IncometaxSavingsScheme>() {
				});
				log.info("=============selected IncometaxSavingsScheme=====================" + incometaxSavingsScheme.getName());
				
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}
	
	public boolean validation() {
		boolean  valid =true;

		if(incometaxSavingsScheme.getCode()!=null  && incometaxSavingsScheme.getCode().length()>0){
			if(incometaxSavingsScheme.getCode().length()>=2 ) {
				
			}else {
				AppUtil.addError("Code minimumm 2 characters");
				return false;
			}
		}
		
		if(incometaxSavingsScheme.getName()!=null  && incometaxSavingsScheme.getName().length()>0){
			if(incometaxSavingsScheme.getName().length()>=5 ) {
				
			}else {
				AppUtil.addError("Name minimumm 5 characters");
				return false;
			}
		}
		
		if(incometaxSavingsScheme.getLocalName()!=null  && incometaxSavingsScheme.getLocalName().length()>0){
			if(incometaxSavingsScheme.getLocalName().length()>=5 ) {
				
			}else {
				AppUtil.addError("Name (In Tamil)  minimumm 5 characters");
				return false;
			}
		}
		
		if(incometaxSavingsScheme.getTaxBenefitPercentage()!=null){
			if(incometaxSavingsScheme.getTaxBenefitPercentage()>100) {
				AppUtil.addError("Tax Benefit not allow morethan 100 ");
				return false;
			}
		}
		
		return valid;
	}


	public String createIncometaxSavingScheme() {
		
		log.info("<==== Starts createIncometaxSavingScheme =======>");
		try {
			
			if(validation()){
			String url = SERVER_URL +"/incometaxsavingscheme/saveOrUpdateIncometaxSavingsScheme";
			log.info(" Incometax_Savings_Scheme ====> createIncometaxSavingScheme ==>> URL " + url);
			BaseDTO response = httpService.post(url, incometaxSavingsScheme);
			if (response != null && response.getMessage()!=null && response.getMessage().equalsIgnoreCase("SUCCESS") ) {
				log.info("Incometax Saving Scheme saved successfully..........");
				log.info(" createIncometaxSavingScheme ::::: ACTION URL ===>>>  " + action);
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.INCOMETAXSAVINGSSCHEME_INSERTED_SUCCESSFULLY).getErrorCode());
				else
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.INCOMETAXSAVINGSSCHEME_UPDATED_SUCCESSFULLY).getErrorCode());
				
				return LIST_INCOMETAXSAVINGSCHEME_PAGE;
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends IncometaxSavingScheme.createIncometaxSavingScheme  =======>");
		   
		return null;
		
	}
	
	public void checkTaxBenefitPercent(){
		if(incometaxSavingsScheme.getTaxBenefitPercentage()!=null){
			if(incometaxSavingsScheme.getTaxBenefitPercentage()>100) {
				AppUtil.addError("Tax Benefit not allow morethan 100 ");
			}
		}
	} 
	
	
}
