package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.ApproveEmployeeLoanAndAdvanceDto;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeSearchCodeDto;
import in.gov.cooptex.core.dto.InsuranceMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.AdvanceTypeMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceDetails;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceFiles;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.LoanMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.finance.dto.StaffPPPermitSearchDTO;
import in.gov.cooptex.personnel.hrms.LoanType;
import in.gov.cooptex.personnel.hrms.loans.model.EmployeeLoanAndAdvanceDetailsDTO;
import in.gov.cooptex.personnel.hrms.model.EmpLoanAdvanceSuretyDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeLoanAndAdvanceBean")
public class EmployeeLoanAndAdvanceBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8241114935803352169L;

	String url;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<LoanMaster> loanMasterList;

	@Getter
	@Setter
	LoanMaster loanMaster;

	@Getter
	@Setter
	EmployeeMaster employeeMaster, suretyDetails;

	@Getter
	@Setter
	EmployeeSearchCodeDto employeeSearchCodeDto;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	EntityMaster headOrRegion, entity;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	private List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	private List<Department> departmentList;

	@Getter
	ApproveEmployeeLoanAndAdvanceDto approveEmployeeLoanAndAdvanceDto;

	@Getter
	@Setter
	private List<EntityMaster> headOrRegionalOfficeFrom;

	@Getter
	@Setter
	private List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<EntityTypeMaster>();

	@Getter
	@Setter
	List<AdvanceTypeMaster> advanceTypeMasterList;

	@Getter
	@Setter
	List<EmployeeLoanAndAdvanceFiles> employeeLoanAndAdvanceFilesList;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceFiles employeeLoanAndAdvanceFiles;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String loanType = "loan";

	@Getter
	@Setter
	AdvanceTypeMaster advanceType;

	@Getter
	@Setter
	int totalLoanSize;

	@Getter
	@Setter
	LazyDataModel<EmployeeLoanAndAdvanceDetails> loanDetailsList;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails selectedEmployeeLoanAndAdvance, employeeSuretyDetails;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	private UploadedFile loanDocument;

	@Getter
	@Setter
	private String loanDocName;

	@Getter
	@Setter
	Boolean add, approve, delete, edit, buttonFlag, disableHooRo, entityVisible, loanVisible, showEntityPanel;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	public ActionTypeEnum actionType;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails request = new EmployeeLoanAndAdvanceDetails();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	//
	@Getter
	@Setter
	String byType = "byEmployeeId", selected;

	@Getter
	@Setter
	Long lastEmpId;

	@Getter
	@Setter
	private StreamedContent file;

	@Getter
	@Setter
	byte[] downloadDoc;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	private List<UserMaster> userMasters = new ArrayList<UserMaster>();

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	boolean forwardFor, diseabledApprove, diseabledReject;

	@Getter
	@Setter
	boolean renderedForwardforAndTo, renderedAppOrRej;

	@Autowired
	LoginBean loginBean;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private boolean panelFlag = false;

	@Getter
	@Setter
	List<EmployeeMaster> selectedSuretyEmployeeList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> suretyEmployeeList = new ArrayList<>();

	@Getter
	@Setter
	String pfNumber;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetailsDTO employeeLoanAndAdvanceDetailsDTO = new EmployeeLoanAndAdvanceDetailsDTO();

	@Getter
	@Setter
	LazyDataModel<EmployeeLoanAndAdvanceDetails> suretyLoanDetailsList;

	@Getter
	@Setter
	Long suretyEmpId;

	@Getter
	@Setter
	private boolean loanAdvflag = false;

	private final String EMP_LOAN_AND_ADVANCE_VIEW_PAGE = "/pages/admin/loanAdvance/loanApproval.xhtml?faces-redirect=true;";
	private final String EMP_LOAN_AND_ADVANCE_LIST_PAGE = "/pages/admin/loanAdvance/listLoanAdvanceAdmin.xhtml?faces-redirect=true;";
	private final String EMP_LOAN_AND_ADVANCE_CREATE_PAGE = "/pages/admin/loanAdvance/createLoanAndAdvanceAdmin.xhtml?faces-redirect=true;";

	private final String SURETY_ACCEPTANCE_LIST_PAGE = "/pages/personnelHR/suretyAcceptance/listSuretyAcceptance.xhtml?faces-redirect=true;";
	private final String SURETY_ACCEPTANCE_VIEW_PAGE = "/pages/personnelHR/suretyAcceptance/viewSuretyAcceptance.xhtml";

	public enum ActionTypeEnum {
		UPDATE, CREATE, APPROVE, VIEW, LIST, DELETE, REJECTED, ACCEPTED, FINALAPPROVE
	}

	@Getter
	@Setter
	Boolean essCreatePageCheck = false;

	@Getter
	@Setter
	Boolean loanEligible = false;

	@Getter
	@Setter
	String remark;

	@Getter
	@Setter
	String rejectedRemark;

	@Getter
	@Setter
	Boolean suretyFlag = false;

	@Getter
	@Setter
	Boolean isLoanAdvancedFlag = false;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceFiles loanAndAdvanceFile = new EmployeeLoanAndAdvanceFiles();

	public void onSelected() {
		if (selected.equals("byEmployee")) {
			log.info("change employee type:::" + selected);
			RequestContext.getCurrentInstance().update("loanForm:loanPanel");
		}
		if (selected.equals("byEntity")) {
			log.info("change employee type:::" + selected);
			RequestContext.getCurrentInstance().update("loanForm:loanPanel");
		}
	}

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@PostConstruct
	public void init() {
		log.info("Init is executed.....................");
		// loadSearchListOfValues();
		// employeeMaster= new EmployeeMaster();
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String loanAndAdvanceDetailsId = "";
		String loantype = "";
		String notificationId = "";
		String status = "";
		String empId = "";
		pfnum = null;
		selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			loanAndAdvanceDetailsId = httpRequest.getParameter("loanAndAdvanceDetailsId");
			status = httpRequest.getParameter("status");
			loantype = httpRequest.getParameter("loantype");
			empId = httpRequest.getParameter("empId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			actionType = ActionTypeEnum.VIEW;
		}
		if (StringUtils.isNotEmpty(loanAndAdvanceDetailsId)) {
			selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
			selectedEmployeeLoanAndAdvance.setId(Long.valueOf(loanAndAdvanceDetailsId));
			selectedEmployeeLoanAndAdvance.setLoanType(loantype);
			systemNotificationId = (notificationId != null ? Long.valueOf(notificationId) : 0l);
			if (status.equalsIgnoreCase("INPROGRESS") && empId != null) {
				suretyEmpId = Long.valueOf(empId);
				selectedEmployeeLoanAndAdvance.setStatus(status);
				suretyAccpetanceViewPage();
			} else {
				selectedEmployeeLoanAndAdvance.setStatus(status);
				showPage(actionType);
			}
		} else {
			employeeLoanAndAdvanceFilesList = new ArrayList<>();
			loadLazyLoanandAdvanceDetails();
			mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			lastEmpId = null;
		}
	}

	/*
	 * @PostConstruct public String showViewListPage() {
	 * log.info("<==== Starts EmployeeLoan And Advanced -showViewListPage =====>");
	 * mapper = new ObjectMapper(); ExternalContext context =
	 * FacesContext.getCurrentInstance().getExternalContext(); Object requestObj =
	 * context.getRequest(); String loanAndAdvanceDetailsId = ""; String loantype =
	 * ""; String notificationId = ""; String status = "";
	 * selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails(); if
	 * (requestObj instanceof HttpServletRequest) { HttpServletRequest httpRequest =
	 * (HttpServletRequest) requestObj; loanAndAdvanceDetailsId =
	 * httpRequest.getParameter("loanAndAdvanceDetailsId"); status =
	 * httpRequest.getParameter("status"); loantype =
	 * httpRequest.getParameter("loantype"); notificationId =
	 * httpRequest.getParameter("notificationId");
	 * log.info("Selected Notification Id :" + notificationId);
	 * actionType=ActionTypeEnum.VIEW; } if
	 * (StringUtils.isNotEmpty(loanAndAdvanceDetailsId)) {
	 * selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
	 * selectedEmployeeLoanAndAdvance.setId(Long.valueOf(loanAndAdvanceDetailsId));
	 * selectedEmployeeLoanAndAdvance.setStatus(status);
	 * selectedEmployeeLoanAndAdvance.setLoanType(loantype);
	 * systemNotificationId=(notificationId!=null?Long.valueOf(notificationId):0l);
	 * showPage(actionType); } return null; }
	 */

	public void loadSearchListOfValues() {
		log.info("start loadSearchListOfValues ");
		try {
			headOrRegionalOfficeFrom = commonDataService.loadHeadAndRegionalOffice();
			// entityTypeMasterList = commonDataService.getEntityTypeByStatus();
			url = SERVER_URL + "/loanandadvance/loadEntityTypeMaster";
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});
				/*
				 * int suretyLoanDetailsListSize = suretyLoanDetailsList == null ? 0 :
				 * suretyLoanDetailsList.size();
				 * log.info("Surety Loan Details List Size =======>>" +
				 * suretyLoanDetailsListSize); this.setRowCount(response.getTotalRecords());
				 * totalLoanSize = response.getTotalRecords();
				 */
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			loanMasterList = commonDataService.loadAllLoan();
			advanceTypeMasterList = commonDataService.loadAllAdvanceTypeLoan();
			departmentList = commonDataService.getDepartment();
			userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
			loadEmployeeLoggedInUserDetails();

			// Load Surety Employee List
			suretyEmployeeList = commonDataService.getActiveEmployeeNameOrderList();
			int suretyEmployeeListSize = suretyEmployeeList == null ? 0 : suretyEmployeeList.size();
			log.info("suretyEmployeeListSize - " + suretyEmployeeListSize);
		} catch (Exception exp) {
			log.error(" Exception Occured in while loading masters :: ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	// Load Surety Employee Auto Complete List
	public List<String> suretyEmployeeAutoComplete(String input) {
		log.info("<<========= EmployeeLoanAndAdvanceBean. suretyEmployeeAutoComplete() Starts ===========>> ");
		List<String> suretyList = new ArrayList<>();
		try {
			if (suretyEmployeeList != null && !suretyEmployeeList.isEmpty() && suretyEmployeeList.size() > 0) {
				log.info("Employee Pf Number Or Name" + input);
				for (EmployeeMaster employeeMaster : suretyEmployeeList) {
					if (employeeMaster.getPersonalInfoEmployment().getPfNumber().contains(input)
							|| employeeMaster.getFirstName().toUpperCase().contains(input.toUpperCase())) {
						suretyList.add(
								new StringBuilder().append(employeeMaster.getPersonalInfoEmployment().getPfNumber())
										.append(" / ").append(employeeMaster.getFirstName()).append(" ")
										.append(employeeMaster.getLastName()).toString());
					}
				}
			} else {
				log.info("Surety Employee List Size is Empty");
			}
		} catch (Exception e) {
			log.error("Exception at EmployeeLoanAndAdvanceBean. suretyEmployeeAutoComplete()", e);
		}
		log.info("<<========= EmployeeLoanAndAdvanceBean. suretyEmployeeAutoComplete() Ends ===========>> ");
		return suretyList;
	}

	public String addSuretyDetails() {
		log.info("<<========= EmployeeLoanAndAdvanceBean. addSuretyDetails() Starts ===========>> ");
		try {
			if (StringUtils.isEmpty(pfNumber)) {
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
				return null;
			}
			if (employeeLoanAndAdvanceDetails.getEmployeeMaster() != null
					&& employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment() != null) {
				if (pfNumber.split(" / ")[0].equalsIgnoreCase(
						employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment().getPfNumber())) {
					AppUtil.addWarn("This Employee not eligible for Surety");
					return null;
				}
			}
			if (selectedSuretyEmployeeList != null && !selectedSuretyEmployeeList.isEmpty()
					&& selectedSuretyEmployeeList.size() >= 2) {
				AppUtil.addWarn("Maximum 2 surety only");
				return null;

			}
			if (suretyEmployeeList != null && !suretyEmployeeList.isEmpty() && suretyEmployeeList.size() > 0) {

				if (selectedSuretyEmployeeList != null && selectedSuretyEmployeeList.size() > 0) {
					for (EmployeeMaster empMaster : selectedSuretyEmployeeList) {
						if (empMaster.getPersonalInfoEmployment().getPfNumber().equals(pfNumber.split(" / ")[0])) {
							errorMap.notify(ErrorDescription.EMPLOYEE_PF_NUM_IS_EXISTS.getErrorCode());
							return null;
						}
					}
				}

				suretyEmployeeList.stream().forEach(employee -> {
					if (employee.getPersonalInfoEmployment().getPfNumber().equals(pfNumber.split(" / ")[0])) {
						EmployeeMaster employeeMaster = new EmployeeMaster();
						employeeMaster = employee;
						selectedSuretyEmployeeList.add(employeeMaster);
					}
				});
			}
		} catch (Exception e) {
			log.error("Exception at EmployeeLoanAndAdvanceBean. addSuretyDetails()", e);
		} finally {
			pfNumber = null;
		}
		log.info("<<========= EmployeeLoanAndAdvanceBean. addSuretyDetails() Ends ===========>> ");
		return null;
	}

	public void onchangeLoan() {
		log.info("loan type is change :::" + loanType);
		try {
			if (StringUtils.isNotEmpty(loanType)) {
				if (loanType.equalsIgnoreCase("loan") || loanType.equalsIgnoreCase("advance")) {
					loanMaster = new LoanMaster();
					advanceType = new AdvanceTypeMaster();
					// employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
				}
				if (loanType.equalsIgnoreCase("advance")) {
					loanAdvflag = true;
				} else {
					String urll = SERVER_URL + "/loanandadvance/loancheck";
					BaseDTO baseDto = httpService.post(urll, employeeMaster);
					if (baseDto != null) {
						if (baseDto.getStatusCode() != 0) {
							Util.addWarn("Employee Not Eligible to Take Loan");
						}
					}
					loanAdvflag = false;
				}
			}
		} catch (Exception e) {
			log.error("error found in change loan:::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError("Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	@Getter
	@Setter
	String pfnum;

	public void searchEmployeeByCode() {
		try {
			employeeMaster = null;
			if (Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster())
					&& Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster().getEmpCode())) {
				log.info("Employee code is :" + employeeLoanAndAdvanceDetails.getEmployeeMaster().getEmpCode());
			}
			String employeeCode = employeeLoanAndAdvanceDetails.getEmployeeMaster().getEmpCode();
			log.info("Employee code is :" + employeeCode);
			String url = AppUtil.getPortalServerURL() + "/employee/get/getLoanAndAdvanceEligibleEmployees/" + pfnum;
			log.info("Employee code url is :" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				loanEligible = false;
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if (Objects.isNull(employeeMaster)) {
					log.info("Selected Employee is :" + employeeMaster);
					employeeMaster = new EmployeeMaster();
					employeeLoanAndAdvanceDetails.setEmployeeMaster(employeeMaster);
					errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
				}
				log.info("Selected Employee is :" + employeeMaster.getFirstName());
				employeeLoanAndAdvanceDetails.setEmployeeMaster(employeeMaster);
			} else if (employeeMaster == null) {
				errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
			} else {
				loanEligible = true;
			}
		} catch (Exception e) {
			log.info("Exception ", e);
		}
	}

	public void searchSuretyDetails() {
		log.info(" Search Surety Details By id ");
		if (suretyDetails != null && !suretyDetails.getEmpCode().isEmpty()) {
			try {
				String employeeCode = suretyDetails.getEmpCode();
				String url = AppUtil.getPortalServerURL() + "/employee/get/employeeid/" + employeeCode;
				log.info(" Url :: " + url);
				BaseDTO baseDto = httpService.get(url);
				if (baseDto.getStatusCode() == 0) {
					String jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
					suretyDetails = mapper.readValue(jsonResponse, EmployeeMaster.class);
					log.info(" Surety Employee Rtrived Successfully with employee code :: "
							+ suretyDetails.getEmpCode());
					employeeLoanAndAdvanceDetails.setSuretyId(suretyDetails);
					validateSuretyEmpByEmployee();
				} else {
					log.info(" No Employee Found with this employee code " + suretyDetails.getEmpCode());
					AppUtil.addWarn(" No Employee Found with employee code " + suretyDetails.getEmpCode());
					suretyDetails = new EmployeeMaster();
				}
			} catch (Exception e) {
				log.error(" Exception Occured in searchSuretyDetails ", e);
			}

		} else {
			AppUtil.addWarn(" Enter Employee Code ");
		}

	}

	public void clear() {
		log.info("<---------------  Inside Clear -------------------->");
		employeeSearchCodeDto = new EmployeeSearchCodeDto();
		employeeMaster = new EmployeeMaster();
		loanType = "";
		headOrRegion = new EntityMaster();
		department = new Department();
		selected = null;
		loanDocName = null;
		advanceType = new AdvanceTypeMaster();
		employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
		selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
		suretyDetails = new EmployeeMaster();
		employeeLoanAndAdvanceFilesList = new ArrayList<>();
		employeeList = new ArrayList<>();
		advanceTypeMasterList = new ArrayList<>();
		pfnum = null;
	}

	public void clearSurety() {
		suretyDetails = new EmployeeMaster();
		pfNumber = "";
	}

	public void employeeajax() {
		if (employeeLoanAndAdvanceDetails.getEmployeeMaster() != null) {
			String urll = SERVER_URL + "/loanandadvance/loancheck";
			BaseDTO baseDto = httpService.post(urll, employeeMaster);
			if (baseDto != null) {
				if (baseDto.getStatusCode() != 0) {
					errorMap.notify(baseDto.getStatusCode());
					employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
				} else if (baseDto.getStatusCode() == 0) {
					log.info("Selected Employee is :" + employeeMaster.getFirstName());
					employeeLoanAndAdvanceDetails.setEmployeeMaster(employeeMaster);
				}
			}
		}

	}

	public String createLoanAndAdvance() {
		log.info("start createLoanAndAdvance ");
		BaseDTO baseDto = new BaseDTO();
		employeeLoanAndAdvanceDetailsDTO = new EmployeeLoanAndAdvanceDetailsDTO();
		try {
			if (validateLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails)) {
				String urll = SERVER_URL + "/loanandadvance/create";
				// employeeLoanAndAdvanceDetails.setSuretyId(suretyDetails);
				// employeeLoanAndAdvanceDetails.setSuretyDetails(suretyDetails);
				if (loanMaster.getLoanInstallment() != null) {
					employeeLoanAndAdvanceDetails
							.setNoOfInstallments(Double.parseDouble(loanMaster.getLoanInstallment().toString()));
				}
				if (employeeLoanAndAdvanceFilesList != null) {
					employeeLoanAndAdvanceDetails
							.setEmployeeLoanAndAdvanceFileDetailsList(employeeLoanAndAdvanceFilesList);
				}
				log.info("employeeLoanAndAdvanceDetails Request" + employeeLoanAndAdvanceDetails);
				employeeLoanAndAdvanceDetails.setLoanType(loanType);
				EmployeeMaster employeeMaster = employeeLoanAndAdvanceDetails.getEmployeeMaster();
				employeeMaster.setEmployeePromotionDetails(new ArrayList<EmployeePromotionDetails>());
				if (essCreatePageCheck) {
					employeeLoanAndAdvanceDetails.setEmpMaster(loginBean.getEmployee());
				} else {
					employeeLoanAndAdvanceDetails.setEmpMaster(employeeMaster);
				}

				employeeLoanAndAdvanceDetails.setEmployeeMaster(null);

				employeeLoanAndAdvanceDetailsDTO.setEmployeeLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails);

				employeeLoanAndAdvanceDetailsDTO.setEmployeeLoanAndAdvanceFiles(employeeLoanAndAdvanceFilesList);

				employeeLoanAndAdvanceDetailsDTO.setEmpLoanAndAdvanceSuretyDetails(selectedSuretyEmployeeList);

				// baseDto = httpService.post(urll, employeeLoanAndAdvanceDetails);

				baseDto = httpService.post(urll, employeeLoanAndAdvanceDetailsDTO);

				if (baseDto.getStatusCode() == 0) {
					showPage(ActionTypeEnum.LIST);
					log.info("::LoanAndAdvanceType details saved successfully. ::" + baseDto.getResponseContent());
					errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_SAVED_SUCCESSFULLY.getErrorCode());
				} else {
					employeeLoanAndAdvanceDetails.setEmployeeMaster(employeeMaster);
					log.info("::LoanAndAdvanceType details failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					if (essCreatePageCheck) {
						return "/pages/personnelHR/employeeProfile/createLoansAdvance.xhtml?faces-redirect=true";
					} else {
						return EMP_LOAN_AND_ADVANCE_CREATE_PAGE;
					}

				}
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error(":: Exception in createLoanAndAdvance", e);
			errorMap.notify(baseDto.getStatusCode());
			return EMP_LOAN_AND_ADVANCE_CREATE_PAGE;
		}

		if (essCreatePageCheck) {
			return "/pages/personnelHR/employeeProfile/listLoansAdvance.xhtml?faces-redirect=true;";
		} else {
			return "/pages/admin/loanAdvance/listLoanAdvanceAdmin.xhtml?faces-redirect=true;";
		}

	}

	public String updateLoanAndAdvance() {
		log.info("update createLoanAndAdvance ");
		BaseDTO baseDto = new BaseDTO();
		try {

			if (validateLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails)) {
				String url = SERVER_URL + "/loanandadvance/update";
				log.info("Loan and Advance Requested URL : ==>" + url);
				log.info(" Employee Master :: " + employeeMaster);
				employeeLoanAndAdvanceDetails.setSuretyId(suretyDetails);
				employeeLoanAndAdvanceDetails.setSuretyDetails(suretyDetails);
				if (loanMaster.getLoanInstallment() != null) {
					employeeLoanAndAdvanceDetails
							.setNoOfInstallments(Double.parseDouble(loanMaster.getLoanInstallment().toString()));
				}
				log.info(" Employee Loan and advance Details ::  " + employeeLoanAndAdvanceDetails);
				baseDto = httpService.put(url, employeeLoanAndAdvanceDetails);

				if (baseDto.getStatusCode() == 0) {
					loadLazyLoanandAdvanceDetails();
					// addLoan = false;
					selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
					log.info("::LoanAndAdvanceType details updated successfully. ::" + baseDto.getResponseContent());
					errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_UPDATED_SUCCESSFULLY.getErrorCode());
					return EMP_LOAN_AND_ADVANCE_LIST_PAGE;
				} else {
					log.info("::LoanAndAdvanceType details failed to updated. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					return EMP_LOAN_AND_ADVANCE_CREATE_PAGE;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Exception Occured in updating loan and advance  ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	public boolean validateLoanAndAdvanceDetails(EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails) {

		if (loanMaster != null && loanType.equalsIgnoreCase("Loan")) {

			employeeLoanAndAdvanceDetails.setLoanMaster(loanMaster);
			employeeLoanAndAdvanceDetails.setAdvanceType(null);
			employeeLoanAndAdvanceDetails.setAdvanceAmountRequired(null);
			if (selectedSuretyEmployeeList.isEmpty() || selectedSuretyEmployeeList.size() == 0) {
				AppUtil.addWarn("Surety Employee List Cannot be Empty");
				return false;
			}
			int selectedSuretyEmployeeListSize = selectedSuretyEmployeeList == null ? 0
					: selectedSuretyEmployeeList.size();
			/*
			 * if (selectedSuretyEmployeeListSize < 2) {
			 * AppUtil.addWarn("Please Enter Minimum Two Surety Details"); return false; }
			 */
		} else if (advanceType != null && loanType.equalsIgnoreCase("Advance")) {
			employeeLoanAndAdvanceDetails.setAdvanceType(advanceType);
			employeeLoanAndAdvanceDetails.setLoanMaster(null);
			employeeLoanAndAdvanceDetails.setLoanAmountRequired(null);
		} else if (loanMaster != null && loanType.equalsIgnoreCase("Recovery")) {
			employeeLoanAndAdvanceDetails.setLoanMaster(loanMaster);
			employeeLoanAndAdvanceDetails.setAdvanceType(null);
			employeeLoanAndAdvanceDetails.setAdvanceAmountRequired(null);
			employeeLoanAndAdvanceDetails.setSanctionAmount(employeeLoanAndAdvanceDetails.getSanctionAmount());
		}
		return true;
	}

	public String saveOrUpdate() {
		log.info(" Employee Master ::");
		String reponsePage = null;
		int loanYearRatio = 0;
		if (!loanType.equals("Recovery")) {
			if (employeeLoanAndAdvanceFilesList.isEmpty()) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_UPLOADFILE_EMPTY.getErrorCode());
				return null;
			}
		}

		if (StringUtils.isEmpty(employeeLoanAndAdvanceDetails.getNote())) {
			AppUtil.addWarn("Create note is required");
			return null;
		}

		/*
		 * if(employeeLoanAndAdvanceDetails.getEmployeeMaster()!=null &&
		 * employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment()
		 * !=null &&
		 * employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment()
		 * .getRetirementDate()!=null) { int
		 * retirementYear=AppUtil.getYear(employeeLoanAndAdvanceDetails.
		 * getEmployeeMaster().getPersonalInfoEmployment().getRetirementDate());
		 * loanYearRatio=Integer.parseInt(commonDataService.getAppKeyValue(
		 * "LOAN_ADVANCE_YEAR_RATIO")); retirementYear=retirementYear-loanYearRatio;
		 * if(AppUtil.getYear(new Date()) > retirementYear) {
		 * AppUtil.addWarn("Minimun 3 years service need to apply for the Loan/Advance"
		 * ); return null; } }
		 */
		if (employeeLoanAndAdvanceDetails.getEmployeeMaster() != null
				&& employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment() != null
				&& employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment()
						.getDateOfJoining() != null) {
			int joiningYear = AppUtil.getYear(
					employeeLoanAndAdvanceDetails.getEmployeeMaster().getPersonalInfoEmployment().getDateOfJoining());
			loanYearRatio = Integer.parseInt(commonDataService.getAppKeyValue("LOAN_ADVANCE_YEAR_RATIO"));
			joiningYear = joiningYear + loanYearRatio;
			if (AppUtil.getYear(new Date()) < joiningYear) {
				AppUtil.addWarn("Minimun 3 years Experience need to apply for the Loan/Advance");
				return null;
			}
		}
		if (selectedEmployeeLoanAndAdvance != null && selectedEmployeeLoanAndAdvance.getId() != null) {
			reponsePage = updateLoanAndAdvance();
		} else {
			reponsePage = createLoanAndAdvance();
		}
		return reponsePage;
	}

	public void checkEmpLoanEligibility() {
		log.info("  Inside checkEmpLoanEligibility Method For Loan :: " + employeeLoanAndAdvanceDetails);
		// String URL = SERVER_URL +
		// "/loanandadvance/checkLoanEligilibityAndInstallment";
		try {
			// BaseDTO baseDTO = httpService.post(URL, loanMaster);
			//
			// if (baseDTO.getTotalRecords() >= 3) {
			// errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_NOT_ELIGIBLE.getErrorCode());
			// employeeLoanAndAdvanceDetails.setEmployeeMaster(new EmployeeMaster());
			// }

			log.info(" Inside Employee Master  checkEmpLoanEligibility ::");
		} catch (Exception e) {
			log.error("Exception Occured At checkEmpLoanEligibility" + e);
		}

	}

	public void onHeadOfficeChange() {

		if (Objects.isNull(employeeLoanAndAdvanceDetails.getHeadOrRegionOffice())) {
			showEntityPanel = false;
			employeeList = new ArrayList<>();
			employeeLoanAndAdvanceDetails.setEntityMaster(null);
			employeeLoanAndAdvanceDetails.setDepartment(null);
			employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
		} else {
			if (employeeLoanAndAdvanceDetails.getHeadOrRegionOffice().getName().equals("Head Office Entity")) {
				showEntityPanel = false;
				employeeLoanAndAdvanceDetails.setEntityMaster(null);
				employeeLoanAndAdvanceDetails.setEntityTypeMaster(null);
				employeeLoanAndAdvanceDetails.setDepartment(null);
				employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
			} else {
				showEntityPanel = true;
				employeeLoanAndAdvanceDetails.setEntityMaster(null);
				employeeLoanAndAdvanceDetails.setDepartment(null);
				employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
				employeeList = new ArrayList<>();
			}
		}
	}

	public void onEntityTypeChange() {
		try {
			if (headOrRegion != null) {
				Long entityId = headOrRegion.getId();
				Long entiyTypeId = employeeLoanAndAdvanceDetails.getEntityTypeMaster().getId();
				String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/entity/" + entityId + "/"
						+ entiyTypeId;
				log.info("URL request for entity :: " + resource);
				BaseDTO baseDto = httpService.get(resource);
				if (baseDto.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					log.info("Entity location list size is : " + entityMasterList.size());
					if (department != null) {
						onEntityDepartmentChange();
					}
				}

			}

		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		// log.info(":: Entity type change ::" + headOrRegion + " ::
		// employeeLoanAndAdvanceDetails.getEntityMaster() "
		// + employeeLoanAndAdvanceDetails.getEntityMaster());
		// try {
		//
		// if (Objects.isNull(headOrRegion) ||
		// Objects.isNull(employeeLoanAndAdvanceDetails.getEntityTypeMaster())) {
		// return;
		// }
		//
		// if (Objects.isNull(headOrRegion.getId())
		// &&
		// Objects.isNull(employeeLoanAndAdvanceDetails.getEntityTypeMaster().getId()))
		// {
		// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		// return;
		// }
		// Long entityId = headOrRegion.getId();
		// Long entiyTypeId =
		// employeeLoanAndAdvanceDetails.getEntityTypeMaster().getId();
		// String resource = AppUtil.getPortalServerURL() +
		// "/entitymaster/getall/entity/" + entityId + "/"
		// + entiyTypeId;
		// log.info("URL request for entity :: " + resource);
		// BaseDTO baseDto = httpService.get(resource);
		// if(baseDto.getStatusCode() == 0) {
		// jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
		// entityMasterList = mapper.readValue(jsonResponse, new
		// TypeReference<List<EntityMaster>>() {
		// });
		// log.info("Entity location list size is : " + entityMasterList.size());
		// }
		//
		//
		// } catch (Exception e) {
		// log.error(":: Exception while Entity type change ::", e);
		// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		// }
	}

	public void onEntityChange() {
		if (!Objects.isNull(employeeLoanAndAdvanceDetails)
				&& !Objects.isNull(employeeLoanAndAdvanceDetails.getEntityMaster())
				&& !Objects.isNull(employeeLoanAndAdvanceDetails.getEntityMaster().getId())) {
			employeeLoanAndAdvanceDetails.setEntityMaster(employeeLoanAndAdvanceDetails.getEntityMaster());
		}
	}

	public void onEntityOfficeChange() {

		if (headOrRegion.getName().equals("HEAD OFFICE")) {
			log.info("Selected Head office code is :" + headOrRegion.getEntityTypeMaster().getEntityCode());
			entityVisible = true;
		} else {
			log.info("Selected Regional office code is :" + headOrRegion.getName());
			entityVisible = false;
		}
		employeeLoanAndAdvanceDetails.setEntityMaster(null);
		employeeLoanAndAdvanceDetails.setEntityTypeMaster(null);
		RequestContext.getCurrentInstance().update("loanForm:loanPanel");

		// if (Objects.isNull(headOrRegion)) {
		// entityVisible = true;
		// employeeLoanAndAdvanceDetails.setEntityMaster(null);
		// department = new Department();
		// RequestContext.getCurrentInstance().update("loanForm:loanPanel");
		// } else if (headOrRegion.getName().equals("HEAD OFFICE")) {
		// log.info("Selected Head office code is :" +
		// headOrRegion.getEntityTypeMaster().getEntityCode());
		// entityVisible = true;
		// employeeLoanAndAdvanceDetails.setEntityMaster(null);
		// RequestContext.getCurrentInstance().update("loanForm:loanPanel");
		// } else {
		// log.info("Selected Regional office code is :" + headOrRegion.getName());
		// entityVisible = false;
		// employeeLoanAndAdvanceDetails.setEntityMaster(null);
		// RequestContext.getCurrentInstance().update("loanForm:loanPanel");
		// }
	}

	public void onEmployeeChange() {
		if (Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster())) {
			log.info(" Selected Employee is :" + employeeLoanAndAdvanceDetails.getEmployeeMaster());
		}
		log.info(" Employee id is :" + employeeLoanAndAdvanceDetails.getEmployeeMaster());
		if (!Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster())) {
			log.info(" Selected Employee id is :" + employeeLoanAndAdvanceDetails.getEmployeeMaster().getId());
			employeeLoanAndAdvanceDetails.setEmployeeMaster(employeeLoanAndAdvanceDetails.getEmployeeMaster());
		}
	}

	public void deleteFile(EmployeeLoanAndAdvanceFiles file) {
		log.info("<===Ends EmployeeLoanAndAdvance Bean deleteFile Start ========>");
		try {

			if (employeeLoanAndAdvanceFilesList.contains(file)) {
				employeeLoanAndAdvanceFilesList.remove(file);
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_LOAN_ADV_UPLOAD_PATH");
			String absoluteFilePath = uploadPath + "\\uploaded" + "\\" + file.getFileName();
			File fileobj = new File(absoluteFilePath);
			if (fileobj.delete()) {
				log.info("File Delected Successfully");
				AppUtil.addInfo("File Deleted Successfully");
			} else {
				log.info("Failed To Delecte File");
			}

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<===Ends EmployeeLoanAndAdvance.deleteFile ========>");
	}

	public void onEntityDepartmentChange() {
		BaseDTO baseDto = new BaseDTO();
		log.info("onchangeDepartment called..");
		Long entityId = null, departmentId = null;
		try {

			if (employeeLoanAndAdvanceDetails.getEntityTypeMaster() != null
					&& employeeLoanAndAdvanceDetails.getEntityTypeMaster().getId() != null) {
				entityId = employeeLoanAndAdvanceDetails.getEntityTypeMaster().getId();
				log.info("Employee Entity from id is : " + entityId + " Name is "
						+ employeeLoanAndAdvanceDetails.getEntityTypeMaster().getEntityName());
			} else if (headOrRegion.getId() != null) {
				entityId = headOrRegion.getEntityTypeMaster().getId();
			}

			if (department.getId() != null) {
				departmentId = department.getId();
			}
			if (entityId != null && departmentId != null) {
				String resource = AppUtil.getPortalServerURL() + "/leaverequest/employeelist/" + entityId + "/"
						+ departmentId;
				log.info("onchangeDepartment Request URL ::::::::::" + resource);
				baseDto = httpService.get(resource);

				if (baseDto.getStatusCode() == 0) {

					jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
					employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info(":: Employee list size is :" + employeeList.size());

				} else {
					employeeList = new ArrayList<>();
				}

			}
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}

		// Long entityId = null, departmentId = null;
		// try {
		//
		// if (headOrRegion != null) {
		// entityId = headOrRegion.getEntityTypeMaster().getId();
		// log.info("Employee Entity from id is : " + entityId + " Name is "
		// + headOrRegion.getEntityTypeMaster().getEntityName());
		// }
		// if (employeeLoanAndAdvanceDetails.getEntityMaster() != null
		// && employeeLoanAndAdvanceDetails.getEntityMaster().getId() != null) {
		// entityId =
		// employeeLoanAndAdvanceDetails.getEntityMaster().getEntityTypeMaster().getId();
		// log.info("Employee Entity from id is : " + entityId + " Name is "
		// +
		// employeeLoanAndAdvanceDetails.getEntityMaster().getEntityTypeMaster().getEntityName());
		// }
		//
		// if (department.getId() != null) {
		// departmentId = department.getId();
		// }
		// if(entityId != null && departmentId != null) {
		//
		// String resource = AppUtil.getPortalServerURL() +
		// "/leaverequest/employeelist/" + entityId + "/"
		// + departmentId;
		// log.info("onchangeDepartment Request URL ::::::::::" + resource);
		// baseDto = httpService.get(resource);
		//
		// if (baseDto.getStatusCode() == 0) {
		//
		// jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
		// employeeList = mapper.readValue(jsonResponse, new
		// TypeReference<List<EmployeeMaster>>() {
		// });
		// log.info(":: Employee list size is :" + employeeList.size());
		//
		// } else {
		// employeeList = new ArrayList<>();
		// }
		//
		// }
		// } catch (Exception e) {
		// log.info("error found in onchangeDepartment::::::", e);
		// }

	}

	public void onDepartmentChange() {
		try {
			if (Objects.isNull(employeeLoanAndAdvanceDetails.getDepartment())) {
				employeeList = new ArrayList<>();
				return;
			}
			Long entityId = null;
			if (Objects.isNull(employeeLoanAndAdvanceDetails.getHeadOrRegionOffice().getEntityTypeMaster().getId())
					&& Objects.isNull(employeeLoanAndAdvanceDetails.getDepartment().getId())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			} else {
				entityId = employeeLoanAndAdvanceDetails.getHeadOrRegionOffice().getEntityTypeMaster().getId();
			}
			Long departmentId = employeeLoanAndAdvanceDetails.getDepartment().getId();
			String resource = SERVER_URL + "/leaverequest/employeelist/" + entityId + "/" + departmentId;
			BaseDTO baseDto = httpService.get(resource);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if (!Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster())
					&& !Objects.isNull(employeeLoanAndAdvanceDetails.getEmployeeMaster().getId())) {
				employeeList.stream().forEach(employee -> {
					if (employee.getId().equals(employeeLoanAndAdvanceDetails.getEmployeeMaster().getId())) {
						log.info(":: Selected employee id is {} & name is {} ::",
								employeeLoanAndAdvanceDetails.getEmployeeMaster().getId(),
								employeeLoanAndAdvanceDetails.getEmployeeMaster().getFirstName());
						employeeLoanAndAdvanceDetails.setEmployeeMaster(employee);
					}
				});
			}
			log.info(" Employee List size is :: " + employeeList.size());
		} catch (Exception e) {
			log.error(":: Exception while onDepartmentChange ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public String showPage(ActionTypeEnum actionType) {
		log.info(":: Loan and advance request page is :  {}LoanAdvanceAdmin.xhtml ", actionType);
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loanAdvflag = false;
		if (actionType.equals(ActionTypeEnum.CREATE)) {
			loanEligible = false;
			pfnum = null;
			action = actionType.toString();
			clear();
			loadSearchListOfValues();
			selectedSuretyEmployeeList = new ArrayList<>();
			if (essCreatePageCheck) {
				return "/pages/personnelHR/employeeProfile/createLoansAdvance.xhtml?faces-redirect=true";
			} else {
				return "/pages/admin/loanAdvance/createLoanAndAdvanceAdmin.xhtml?faces-redirect=true;";
			}

		} else if (actionType.equals(ActionTypeEnum.UPDATE)) {
			suretyDetails = new EmployeeMaster();
			if (checkUserPriv(true)) {
				action = actionType.toString();
				findEmployeeLoanandAdvanceDetailsById();
				fetchapproveLoanAdvance();
				if (Objects.isNull(employeeLoanAndAdvanceDetails.getDocumentPath())
						|| employeeLoanAndAdvanceDetails.getDocumentPath().isEmpty()) {
					loanDocName = "";
				} else {
					if (employeeLoanAndAdvanceDetails.getDocumentPath().contains("uploaded")) {
						String filePathValue[] = employeeLoanAndAdvanceDetails.getDocumentPath().split("uploaded/");
						loanDocName = filePathValue[1];
					} else {
						String filePathValue[] = employeeLoanAndAdvanceDetails.getDocumentPath()
								.split("LoanAndAdvanceDocument/");
						loanDocName = filePathValue[1];
					}
				}
				selectedSuretyEmployeeList = approveEmployeeLoanAndAdvanceDto.getSuretyDetailsList();
				employeeLoanAndAdvanceFilesList = approveEmployeeLoanAndAdvanceDto.getEmployeeLoanAndAdvanceFiles();
				return EMP_LOAN_AND_ADVANCE_CREATE_PAGE;
			} else {

				return null;
			}

		} else if (actionType.equals(ActionTypeEnum.VIEW)) {
			if (selectedEmployeeLoanAndAdvance != null) {
				isLoanAdvancedFlag = false;
				if ("Loan".equalsIgnoreCase(selectedEmployeeLoanAndAdvance.getLoanType())) {
					if (selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("SURETY_ACCEPTED")) {
						suretyFlag = true;
						isLoanAdvancedFlag = true;
					} else if (selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("APPROVED")) {
						suretyFlag = true;
						isLoanAdvancedFlag = true;
					} else {
						suretyFlag = false;
					}
				} else {
					suretyFlag = true;
					isLoanAdvancedFlag = true;
				}

				fetchapproveLoanAdvance();
				// if (approveEmployeeLoanAndAdvanceDto.getDocumentPath() != null) {
				// downloadDoc =
				// downloadFile(approveEmployeeLoanAndAdvanceDto.getDocumentPath());
				// }
				// if (employeeLoanAndAdvanceDetails != null &&
				// employeeLoanAndAdvanceDetails.getDocumentPath() == null
				// || employeeLoanAndAdvanceDetails.getDocumentPath().isEmpty()) {
				// loanDocName = "";
				// } else {
				// if (employeeLoanAndAdvanceDetails.getDocumentPath().contains("uploaded")) {
				// String filePathValue[] =
				// employeeLoanAndAdvanceDetails.getDocumentPath().split("uploaded/");
				// loanDocName = filePathValue[1];
				// } else {
				// String filePathValue[] = employeeLoanAndAdvanceDetails.getDocumentPath()
				// .split("LoanAndAdvanceDocument/");
				// loanDocName = filePathValue[1];
				// }
				// }

				systemNotificationBean.loadTotalMessages();
				return EMP_LOAN_AND_ADVANCE_VIEW_PAGE;

			} else {
				AppUtil.addWarn("PLease Select A Record To View");
				return EMP_LOAN_AND_ADVANCE_LIST_PAGE;
			}

		} else if (actionType.equals(ActionTypeEnum.DELETE)) {
			if (selectedEmployeeLoanAndAdvance != null) {
				if (!selectedEmployeeLoanAndAdvance.getStatus().equals("INPROGRESS")
						&& !selectedEmployeeLoanAndAdvance.getStatus().equals("REJECTED")) {
					log.info(" Seleted record status is : " + selectedEmployeeLoanAndAdvance.getStatus());
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmDelete').show();");

				}

			} else {
				log.info(" Record not selected ");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			return "";
		} else {
			loadLazyLoanandAdvanceDetails();
			selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
			return EMP_LOAN_AND_ADVANCE_LIST_PAGE;
		}
	}

	private boolean checkUserPriv(boolean valid) {
		valid = true;
		if (selectedEmployeeLoanAndAdvance != null) {
			if (selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase(ApprovalStage.SURETY_REJECTED)) {
				valid = true;
			} else {
				AppUtil.addWarn("Cannot Edit " + selectedEmployeeLoanAndAdvance.getStatus());
				valid = false;
			}

		} else {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			valid = false;
		}

		return valid;
	}

	private EmployeeLoanAndAdvanceDetails findEmployeeLoanandAdvanceDetailsById() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = SERVER_URL + "/loanandadvance/getbyid/" + selectedEmployeeLoanAndAdvance.getId();
			log.info("Requested URL : ==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeLoanAndAdvanceDetails = mapper.readValue(jsonResponse, EmployeeLoanAndAdvanceDetails.class);
			log.info("designation Object Employee Master==> " + employeeLoanAndAdvanceDetails.getEmployeeMaster());
			employeeMaster = employeeLoanAndAdvanceDetails.getEmployeeMaster();
			if (employeeLoanAndAdvanceDetails.getLoanAmountRequired() != null
					&& employeeLoanAndAdvanceDetails.getLoanAmountRequired() > 0) {
				loanType = "Loan";
				loanMaster = employeeLoanAndAdvanceDetails.getLoanMaster();
			} else {
				loanType = "Advance";
				advanceType = employeeLoanAndAdvanceDetails.getAdvanceType();
			}
			if (employeeLoanAndAdvanceDetails.getSuretyDetails() != null) {
				suretyDetails = employeeLoanAndAdvanceDetails.getSuretyDetails();
			}
		} catch (Exception e) {
			log.error("Exception Occured in loadDesignationById  ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return employeeLoanAndAdvanceDetails;
	}

	/**
	 * Loan And Advance
	 * 
	 * @author Pratap
	 * @return
	 */

	public void loadLazyLoanandAdvanceDetails() {
		loanDetailsList = new LazyDataModel<EmployeeLoanAndAdvanceDetails>() {

			private static final long serialVersionUID = -9122090322809059767L;

			List<EmployeeLoanAndAdvanceDetails> dataList = new ArrayList<EmployeeLoanAndAdvanceDetails>();

			@Override
			public List<EmployeeLoanAndAdvanceDetails> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);

					PaginationDTO request = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					// request.setPaginationDTO(
					// new PaginationDTO(first / pageSize, pageSize, sortField,
					// sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/loanandadvance/loadlazylist", request);

					if (baseDTO != null && baseDTO.getStatusCode() == 0) {

						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						dataList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeLoanAndAdvanceDetails>>() {
								});

						if (dataList != null) {

							dataList.forEach(loan -> {
								if (loan.getLoanMaster() != null && loan.getLoanMaster().getId() != null) {
									// loan.setLoanType("Loan");
									loan.setLoanOrAdvanceTypeName(loan.getLoanMaster().getName());
									loan.setLoanOrAdvanceAmount(loan.getLoanAmountRequired());
								} else if (loan.getAdvanceType() != null && loan.getAdvanceType().getId() != null) {
									loan.setLoanType("Advance");
									loan.setLoanOrAdvanceTypeName(loan.getAdvanceType().getName());
									loan.setLoanOrAdvanceAmount(loan.getAdvanceAmountRequired());
								}

							});

							this.setRowCount(baseDTO.getTotalRecords());
							totalLoanSize = baseDTO.getTotalRecords();
							log.info(":: Loan and Advance search Result size ::" + totalLoanSize);
						} else {
							log.error(":: Employee Loan and Advance Search Failed With status code :: "
									+ baseDTO.getStatusCode());
							errorMap.notify(baseDTO.getStatusCode());
						}
						return dataList;
					}
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeLoanAndAdvanceDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeLoanAndAdvanceDetails getRowData(String rowKey) {
				Long value = Long.valueOf(rowKey);
				for (EmployeeLoanAndAdvanceDetails res : dataList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeLoanAndAdvance = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void fetchapproveLoanAdvance() {
		try {
			log.info(" Inside fetchapproveLoanAdvance ");
			approveEmployeeLoanAndAdvanceDto = new ApproveEmployeeLoanAndAdvanceDto();
			BaseDTO baseDTO = null;
			if (selectedEmployeeLoanAndAdvance.getId() != null) {
				url = SERVER_URL + "/loanandadvance/getapprovebyid/" + selectedEmployeeLoanAndAdvance.getId() + "/"
						+ systemNotificationId;
				baseDTO = httpService.get(url);

				if (baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					approveEmployeeLoanAndAdvanceDto = mapper.readValue(jsonResponse,
							ApproveEmployeeLoanAndAdvanceDto.class);
					lastEmpId = approveEmployeeLoanAndAdvanceDto.getForwardToId();

					userMasters = commonDataService
							.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
					// commonDataService.loadEmployeeByUser(loginBean.getUserMaster().getId());

					if ((!selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("FINAL APPROVED")
							|| !selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("REJECTED"))
							&& (approveEmployeeLoanAndAdvanceDto.getForwardTo().getId()
									.equals(loginBean.getUserDetailSession().getId()))) {
						selectedEmployeeLoanAndAdvance.setForwardTo(approveEmployeeLoanAndAdvanceDto.getForwardTo());
						selectedEmployeeLoanAndAdvance.setForwardFor(approveEmployeeLoanAndAdvanceDto.isForwardFor());
						if (approveEmployeeLoanAndAdvanceDto.isForwardFor() == true) {
							renderedForwardforAndTo = false;
							renderedAppOrRej = true;

						} else {
							renderedForwardforAndTo = true;
							renderedAppOrRej = true;
						}

					} else {
						renderedForwardforAndTo = false;
						renderedAppOrRej = false;
					}

					if (!selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("REJECTED")) {
						panelFlag = true;
					} else {
						panelFlag = false;
					}
					//
					// if (approveEmployeeLoanAndAdvanceDto.getForwardTo().getId()
					// .equals(loginBean.getUserDetailSession().getId())) {
					// diseabledApprove = false;
					// diseabledReject = false;
					// renderedForwardforAndTo = true;
					// } else if
					// ((approveEmployeeLoanAndAdvanceDto.getForwardTo().getId().longValue() !=
					// loginBean
					// .getUserDetailSession().getId().longValue())) {
					// diseabledApprove = true;
					// diseabledReject = true;
					//
					// }

				}
			}

		} catch (Exception e) {
			log.error(" Exception Occured ", e);
		}
	}

	// public void checkEmp() {
	//
	// lastEmpId = approveEmployeeLoanAndAdvanceDto.getForwardToId();
	// }
	public String approveOrReject(String action) {

		log.info("Inside  approveOrReject " + action);
		try {
			ActionTypeEnum actionType = ActionTypeEnum.valueOf(action);
			if (!"REJECTED".equalsIgnoreCase(actionType.toString())) {
				if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() == null) {
					Util.addWarn("Sanctioned Amount is Required");
					return null;
				}
				if (approveEmployeeLoanAndAdvanceDto.getLoanStartDate() == null) {
					Util.addWarn("Loan Start Date is Required");
					return null;
				}
				if (approveEmployeeLoanAndAdvanceDto.getWefDate() == null) {
					Util.addWarn("Date W.e.f is Required");
					return null;
				}
			}

			log.info("----------remark--------------" + remark);
			if (remark != null && !remark.isEmpty()) {
				approveEmployeeLoanAndAdvanceDto.setRemark(remark);
			} else {
				approveEmployeeLoanAndAdvanceDto.setRemark(rejectedRemark);
			}
			log.info("----------remark value--------------" + approveEmployeeLoanAndAdvanceDto.getRemark());

			double amount = approveEmployeeLoanAndAdvanceDto.getIsLoanOrAdvance()
					? approveEmployeeLoanAndAdvanceDto.getLoanAmountRequired()
					: approveEmployeeLoanAndAdvanceDto.getAdvAmountRequired();
			if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() != null
					&& approveEmployeeLoanAndAdvanceDto.getSanctionAmount() > amount) {
				errorMap.notify(ErrorDescription
						.getError(OperationErrorCode.SANCTION_AMOUNT_SHOULD_LESS_THAN_REQUIRED_AMOUNT).getCode());
				return "";
			}
			if (validateUserApproval(actionType)) {
				log.info(" Request Object :: " + approveEmployeeLoanAndAdvanceDto);
				url = SERVER_URL + "/loanandadvance/approveorreject";
				BaseDTO baseDTO = httpService.post(url, approveEmployeeLoanAndAdvanceDto);

				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info(" Approved Or Reject Successfully");
					showPage(ActionTypeEnum.LIST);
					if (approveEmployeeLoanAndAdvanceDto.getStatus().equalsIgnoreCase("FINAL APPROVED")
							|| approveEmployeeLoanAndAdvanceDto.getStatus().equalsIgnoreCase("APPROVED")) {
						AppUtil.addInfo("Loan and advance approved successfully");
					} else if (approveEmployeeLoanAndAdvanceDto.getStatus().equalsIgnoreCase("REJECT")) {
						AppUtil.addInfo("Loan and advance reject successfully");
					}
					return "/pages/admin/loanAdvance/listLoanAdvanceAdmin.xhtml?faces-redirect=true;";
				} else {
					log.info(" Approved Or Reject Failed");
				}
			}
		} catch (Exception e) {
			log.error(" Exception Occured ", e);
		}

		return null;

	}

	private boolean validateUserApproval(ActionTypeEnum actionType) {
		boolean valid = true;
		if (approveEmployeeLoanAndAdvanceDto.getForwardTo() != null
				&& lastEmpId.longValue() != approveEmployeeLoanAndAdvanceDto.getForwardTo().getId().longValue()
				&& approveEmployeeLoanAndAdvanceDto.isForwardFor() == false) {
			approveEmployeeLoanAndAdvanceDto.setStatus("APPROVED");
		} /*
			 * else if (approveEmployeeLoanAndAdvanceDto.getForwardTo()!=null &&
			 * lastEmpId.longValue() ==
			 * approveEmployeeLoanAndAdvanceDto.getForwardTo().getId().longValue() &&
			 * approveEmployeeLoanAndAdvanceDto.isForwardFor() == false) { AppUtil.
			 * addWarn("It Is Already Forward To You Please Reject Or Send For Final Approval"
			 * ); valid = false; }
			 */ else if (approveEmployeeLoanAndAdvanceDto.getForwardTo() != null
				&& lastEmpId.longValue() != approveEmployeeLoanAndAdvanceDto.getForwardTo().getId().longValue()
				&& approveEmployeeLoanAndAdvanceDto.isForwardFor() == true) {
			approveEmployeeLoanAndAdvanceDto.setStatus("APPROVED");
		} else if (approveEmployeeLoanAndAdvanceDto.getForwardTo() != null
				&& lastEmpId.longValue() == approveEmployeeLoanAndAdvanceDto.getForwardTo().getId().longValue()
				&& approveEmployeeLoanAndAdvanceDto.isForwardFor() == true) {
			approveEmployeeLoanAndAdvanceDto.setStatus("FINAL APPROVED");
		}
		if ("REJECTED".equalsIgnoreCase(actionType.toString())) {
			approveEmployeeLoanAndAdvanceDto.setStatus("REJECTED");
			approveEmployeeLoanAndAdvanceDto.setForwardTo(selectedEmployeeLoanAndAdvance.getForwardTo());
			valid = true;
		}

		lastEmpId = null;
		return valid;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed....");
		loanDocName = null;
		try {

			employeeLoanAndAdvanceFiles = new EmployeeLoanAndAdvanceFiles();
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			String fileExtension = Files.getFileExtension(event.getFile().getFileName());
			if (!fileExtension.equalsIgnoreCase("png") && !fileExtension.equalsIgnoreCase("jpg")
					&& !fileExtension.equalsIgnoreCase("gif") && !fileExtension.equalsIgnoreCase("jpeg")
					&& !fileExtension.equalsIgnoreCase("pdf") && !fileExtension.equalsIgnoreCase("doc")
					&& !fileExtension.equalsIgnoreCase("docx") && !fileExtension.equalsIgnoreCase("xls")) {
				AppUtil.addWarn("Please upload valid File");
				log.info("File Format Not Valid ");
				return;
			}
			loanDocument = event.getFile();
			long size = loanDocument.getSize();
			if (size == 0) {
				AppUtil.addWarn("File is Empty");
				return;
			}
			log.info("Employee Loan request document size is : " + size);
			/*
			 * long filesize = size / 1000; if (size > filesize) {
			 * errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode())
			 * ; return; }
			 */

			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_LOAN_ADV_UPLOAD_PATH");

			String filePath = Util.fileUpload(uploadPath, loanDocument);

			log.info(" Uploaded document oath is :" + uploadPath);
			loanDocName = loanDocument.getFileName();
			employeeLoanAndAdvanceFiles.setFileType(filePath.substring(filePath.lastIndexOf(".")));
			employeeLoanAndAdvanceFiles.setFilePath(filePath);
			employeeLoanAndAdvanceFiles.setFileName(loanDocName);
			employeeLoanAndAdvanceFilesList.add(employeeLoanAndAdvanceFiles);
			errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void checkLoanAmt() {

		if (loanType.equalsIgnoreCase("Loan") && employeeLoanAndAdvanceDetails.getLoanAmountRequired() != null
				&& employeeLoanAndAdvanceDetails.getLoanAmountRequired() <= 0) {
			AppUtil.addWarn("Invalid Amount");
			return;
		} else if (loanType.equalsIgnoreCase("Advance")
				&& employeeLoanAndAdvanceDetails.getAdvanceAmountRequired() != null
				&& employeeLoanAndAdvanceDetails.getAdvanceAmountRequired() <= 0) {
			AppUtil.addWarn("Invalid Amount");
			return;
		}

		if (loanMaster.getMinLoanAmount() != null && loanMaster.getLoanEligiblilityAmount() != null) {
			if (!(employeeLoanAndAdvanceDetails.getLoanAmountRequired() >= loanMaster.getMinLoanAmount())
					|| !(employeeLoanAndAdvanceDetails.getLoanAmountRequired() <= loanMaster
							.getLoanEligiblilityAmount())) {
				AppUtil.addWarn("Loan Required Amount Should be between Loan Min and Eligibility Of Loan");
				employeeLoanAndAdvanceDetails.setLoanAmountRequired(null);
			}
		} else if (advanceType.getMinAdvanceAmount() != null && advanceType.getMaxAdvanceAmount() != null) {
			if (!(employeeLoanAndAdvanceDetails.getAdvanceAmountRequired() >= advanceType.getMinAdvanceAmount())
					|| !(employeeLoanAndAdvanceDetails.getAdvanceAmountRequired() <= advanceType
							.getMaxAdvanceAmount())) {
				AppUtil.addWarn("Advance Required Amount Should be between Loan Min and Max Amount Of Advance");
				employeeLoanAndAdvanceDetails.setAdvanceAmountRequired(null);
			}
		}

	}

	public void deleteLoanAndAdvance() {
		log.info(":: Delete Loan and Advance request by id ::");
		try {
			if (selectedEmployeeLoanAndAdvance.getId() != null) {
				String url = SERVER_URL + "/loanandadvance/deletebyid/" + selectedEmployeeLoanAndAdvance.getId();
				log.info(":: Delete leave request URI by id is ::" + url);
				BaseDTO baseDTO = httpService.delete(url);
				if (baseDTO.getStatusCode() == 0) {
					log.info(":: Loan And  Advance Delete request BaseDTO Deleted ::" + baseDTO);
					errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_DELETED_SUCCESSFULLY.getErrorCode());
					showPage(ActionTypeEnum.LIST);
				} else {
					log.info(":: Loan And  Advance Delete response is ::" + baseDTO.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.error(":: Exception while delete leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void getDownloadFile(byte[] value) {
		try {
			log.info(" Value :: " + value);
			if (value == null || value == null) {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
				return;
			}
			log.info(" FIle Name :: " + loanDocName);
			String ext = FilenameUtils.getExtension(loanDocName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(value), "image/jpg", loanDocName);
			} else if (ext.contains("pdf")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(value), "pdf", loanDocName);
			}
			// return new DefaultStreamedContent(new ByteArrayInputStream(value),
			// "image/png","downloadimage");
			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded flie", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
	}
	//
	// public byte[] downloadFile(String path) {
	// byte[] downloadName = null;
	// try {
	// File file = new File(path);
	// downloadName = new byte[(int) file.length()];
	//
	// log.info("downloadName==>" + downloadName);
	//
	// FileInputStream fis = new FileInputStream(file);
	// fis.read(downloadName); // read file into bytes[]
	// fis.close();
	// } catch (Exception e) {
	// log.error("convertImage error==>" + e);
	// }
	// return downloadName;
	// }

	public void downloadfile(EmployeeLoanAndAdvanceFiles fileObj) {
		InputStream input = null;
		try {
			log.info("file path------>" + fileObj.getFilePath());
			File files = new File(fileObj.getFilePath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.info("filedownload filename------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public String validateSuretyEmpByEmployee() {
		log.info("=========>Start validateSuretyEmpByEmployee Method==========>");
		try {
			if (suretyDetails != null && suretyDetails.getEmpCode() != null && employeeLoanAndAdvanceDetails != null
					&& employeeLoanAndAdvanceDetails.getEmployeeMaster() != null) {
				if (suretyDetails.getEmpCode().equals(employeeLoanAndAdvanceDetails.getEmployeeMaster().getEmpCode())) {
					AppUtil.addWarn("Loan Employee and Surety Employee Same");
					return null;
				}
			}
		} catch (Exception ex) {
			log.error("Error in validateSuretyEmpByEmployee", ex);
		}
		log.info("=========>End validateSuretyEmpByEmployee Method==========>");
		return null;
	}

	public String gotoSuretyAcceptancePage() {
		log.info(" gotoSuretyAcceptancePage() Started ");
		selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
		loadSuretyLoanDetailsLazyList();
		return SURETY_ACCEPTANCE_LIST_PAGE;
	}

	// Load Surety Acceptance List Page
	private void loadSuretyLoanDetailsLazyList() {
		log.info("<<============== EmployeeLoanAndAdvanceBean:loadSuretyLoanDetailsLazyList() Strats ===========>> ");
		suretyLoanDetailsList = new LazyDataModel<EmployeeLoanAndAdvanceDetails>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 2559910409973223615L;

			public List<EmployeeLoanAndAdvanceDetails> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				String url = "";
				List<EmployeeLoanAndAdvanceDetails> suretyLoanDetailsList = null;
				try {
					suretyLoanDetailsList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					EmployeeMaster employeeMaster = loginBean.getEmployee();

					if (employeeMaster != null) {
						paginationRequest.setEmpId(employeeMaster == null ? null : employeeMaster.getId());
						suretyEmpId = paginationRequest.getEmpId();
						log.info("Login Employee Id : " + paginationRequest.getEmpId());
					} else {
						log.info("Login Employee Master is Empty");
					}

					log.info("Pagination request :::" + paginationRequest);
					url = SERVER_URL + "/loanandadvance/loadSuretyLazyList";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						suretyLoanDetailsList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeLoanAndAdvanceDetails>>() {
								});
						int suretyLoanDetailsListSize = suretyLoanDetailsList == null ? 0
								: suretyLoanDetailsList.size();
						log.info("Surety Loan Details List Size =======>>" + suretyLoanDetailsListSize);
						this.setRowCount(response.getTotalRecords());
						totalLoanSize = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in EmployeeLoanAndAdvanceBean : loadSuretyLoanDetailsLazyList  ...",
							e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("<<======== Ends lazy load ======>> ");
				return suretyLoanDetailsList;
			}

			@Override
			public Object getRowKey(EmployeeLoanAndAdvanceDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeLoanAndAdvanceDetails getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmployeeLoanAndAdvanceDetails> responseList = (List<EmployeeLoanAndAdvanceDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeLoanAndAdvanceDetails suretyLoanDetailsObj : responseList) {
					if (suretyLoanDetailsObj.getId().longValue() == value.longValue()) {
						selectedEmployeeLoanAndAdvance = suretyLoanDetailsObj;
						return suretyLoanDetailsObj;
					}
				}
				return null;
			}
		};
	}

	public String suretyClear() {
		selectedEmployeeLoanAndAdvance = null;
		return SURETY_ACCEPTANCE_LIST_PAGE;
	}

	public String suretyAccpetanceViewPage() {
		log.info("EmployeeLoanAndAdvanceBean. suretyAccpetanceViewPage() Starts");
		try {
			if (selectedEmployeeLoanAndAdvance == null || selectedEmployeeLoanAndAdvance.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
//			String url = SERVER_URL + "/loanandadvance/getapprovebyid/" + selectedEmployeeLoanAndAdvance.getId();
			String url = SERVER_URL + "/loanandadvance/getapprovebyid/" + selectedEmployeeLoanAndAdvance.getId() + "/"
					+ systemNotificationId;
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				approveEmployeeLoanAndAdvanceDto = mapper.readValue(jsonResponse,
						ApproveEmployeeLoanAndAdvanceDto.class);
			}
			systemNotificationBean.loadTotalMessages();
		} catch (Exception e) {
			log.error("Exception at EmployeeLoanAndAdvanceBean. suretyAccpetanceViewPage()", e);
		}
		log.info("EmployeeLoanAndAdvanceBean. suretyAccpetanceViewPage() Ends");
		return SURETY_ACCEPTANCE_VIEW_PAGE;
	}

	public String accpetRejectEmpLoanDetails() {
		log.info("<<======= EmployeeLoanAndAdvanceBean.accpetRejectEmpLoanDetails() Starts ==========>>");
		BaseDTO baseDTO = null;
		try {
			if (StringUtils.isEmpty(employeeLoanAndAdvanceDetailsDTO.getRemarks())) {
				AppUtil.addWarn("Remarks is required");
				return null;
			}
			if (actionType.equals(ActionTypeEnum.ACCEPTED)) {
				employeeLoanAndAdvanceDetailsDTO.setStage(ActionTypeEnum.ACCEPTED.toString());
			} else if (actionType.equals(ActionTypeEnum.REJECTED)) {
				employeeLoanAndAdvanceDetailsDTO.setStage(ActionTypeEnum.REJECTED.toString());
			}
			employeeLoanAndAdvanceDetailsDTO.setEmpId(suretyEmpId);
			employeeLoanAndAdvanceDetailsDTO.setLoanAdvanceDetailsId(selectedEmployeeLoanAndAdvance.getId());
			String URL = SERVER_URL + "/loanandadvance/suretyacceptorreject";
			baseDTO = httpService.post(URL, employeeLoanAndAdvanceDetailsDTO);
			if (baseDTO != null) {
				if (actionType.equals(ActionTypeEnum.ACCEPTED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.SURETY_ACCEPT_SUCCESS).getErrorCode());
				} else if (actionType.equals(ActionTypeEnum.REJECTED)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.SURETY_REJECT_SUCCESS).getErrorCode());
				}

				return gotoSuretyAcceptancePage();
			}
		} catch (Exception e) {
			log.error("<<====== EmployeeLoanAndAdvanceBean.accpetRejectEmpLoanDetails() =======>>", e);
		}
		log.info("<<======= EmployeeLoanAndAdvanceBean.accpetRejectEmpLoanDetails() Ends ==========>>");
		return null;
	}

	public String showDialogBox(ActionTypeEnum actionType) {

		log.info("EmployeeLoanAndAdvancedBean::showDialogBox Start");
		try {

			if (approveEmployeeLoanAndAdvanceDto.getIsLoanOrAdvance()) {
				log.info("inside if -------");
				if ("Loan".equals(approveEmployeeLoanAndAdvanceDto.getLoanType())) {
					if (approveEmployeeLoanAndAdvanceDto.getLoanEligibility() == null
							|| approveEmployeeLoanAndAdvanceDto.getLoanEligibility().doubleValue() == 0D) {
						AppUtil.addWarn("Eligibility Amount should not be empty");
						return null;
					}
					if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() == null || approveEmployeeLoanAndAdvanceDto
							.getSanctionAmount() < approveEmployeeLoanAndAdvanceDto.getMinLoanAmount()) {
						AppUtil.addWarn("Sanctioned Amount Cannot be Less than Minimum Amount");
						return null;
					} else if (approveEmployeeLoanAndAdvanceDto
							.getSanctionAmount() > (approveEmployeeLoanAndAdvanceDto.getLoanEligibility() == null ? 0.0
									: approveEmployeeLoanAndAdvanceDto.getLoanEligibility())) {
						AppUtil.addWarn("Sanctioned Amount Cannot be More than Maximum Amount");
						return null;
					}
					/*
					 * else if(approveEmployeeLoanAndAdvanceDto.getSanctionAmount() >
					 * approveEmployeeLoanAndAdvanceDto.getLoanAmountRequired()) {
					 * AppUtil.addWarn("Sanctioned Amount Cannot be More than Required Amount");
					 * return null; }
					 */
				}
			} else {
				log.info("inside else -------");
				if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() < approveEmployeeLoanAndAdvanceDto
						.getMinAdvAmount()) {
					AppUtil.addWarn("Sanctioned Amount Cannot be Less than Minimum Amount");
					return null;
				} else if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() > approveEmployeeLoanAndAdvanceDto
						.getMaxAdvAmount()) {
					AppUtil.addWarn("Sanctioned Amount Cannot be More than Maximum Amount");
					return null;
				}
				/*
				 * else if(approveEmployeeLoanAndAdvanceDto.getSanctionAmount() >
				 * approveEmployeeLoanAndAdvanceDto.getAdvAmountRequired()) {
				 * AppUtil.addWarn("Sanctioned Amount Cannot be More than Amount Required");
				 * return null; }
				 */
			}

			if (!"REJECTED".equalsIgnoreCase(actionType.toString())
					&& !"FINALAPPROVE".equalsIgnoreCase(actionType.toString())) {
				if (approveEmployeeLoanAndAdvanceDto.getSanctionAmount() == null) {
					Util.addWarn("Sanctioned Amount is Required");
					return null;
				}
				if (approveEmployeeLoanAndAdvanceDto.getLoanStartDate() == null) {
					Util.addWarn("Loan Start Date is Required");
					return null;
				}
				if (approveEmployeeLoanAndAdvanceDto.getWefDate() == null) {
					Util.addWarn("Date W.e.f is Required");
					return null;
				}
			}
			if ("APPROVE".equalsIgnoreCase(actionType.toString())
					|| "FINALAPPROVE".equalsIgnoreCase(actionType.toString())) {
				RequestContext.getCurrentInstance().execute("PF('dlgComments3').show();");
			} else {
				RequestContext.getCurrentInstance().execute("PF('dlgComments2').show();");
			}
		} catch (Exception e) {
			log.info("----------Exception  ", e);
		}
		log.info("EmployeeLoanAndAdvancedBean::showDialogBox End");
		return null;
	}

	public String showFileConfirmationDialog(EmployeeLoanAndAdvanceFiles file) {
		log.info("---------Start::showFileConfirmationDialog()--------");
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmDelete').show();");
			loanAndAdvanceFile = file;
		} catch (Exception ex) {
			log.error("-------------Exception-------", ex);
		}
		log.info("---------End::showFileConfirmationDialog()--------");
		return null;
	}

	public void advanceTypeChange() {
		log.info("advance type start--------->" + advanceType);
		try {
			if (advanceType != null && advanceType.getCode() != null) {
				String url = SERVER_URL + "/loanandadvance/getadvanceamount/" + advanceType.getCode();
				BaseDTO baseDto = httpService.get(url);
				if (baseDto != null) {
					if (baseDto.getStatusCode().intValue() == (ErrorDescription.SUCCESS_RESPONSE.getCode())
							.intValue()) {
						jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
						advanceType = mapper.readValue(jsonResponse, AdvanceTypeMaster.class);
					}
				}
			}
		} catch (Exception e) {
			log.error("advanceTypeChange excception----------->", e);
		}
		log.info("advance type ends--------->" + advanceType);
	}

	@Getter
	@Setter
	private Long loanId;

	@Getter
	@Setter
	private String status;

	@Getter
	@Setter
	private Long empId;

	@Getter
	@Setter
	private Long notificationId;

	public String showNotification() {
		log.info("id------------------------->" + getLoanId());
		try {
			if (!Objects.isNull(loanId)) {
				selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
				selectedEmployeeLoanAndAdvance.setId(loanId);
				selectedEmployeeLoanAndAdvance.setLoanType(loanType);
				systemNotificationId = (notificationId != null ? Long.valueOf(notificationId) : 0l);
				suretyEmpId = Long.valueOf(empId);
				selectedEmployeeLoanAndAdvance.setStatus(status);

				String url = SERVER_URL + "/loanandadvance/getapprovebyid/" + selectedEmployeeLoanAndAdvance.getId()
						+ "/" + systemNotificationId;
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					approveEmployeeLoanAndAdvanceDto = mapper.readValue(jsonResponse,
							ApproveEmployeeLoanAndAdvanceDto.class);
				}

				if (approveEmployeeLoanAndAdvanceDto.getEmpLoanAdvanceSuretyDetailsList() != null
						&& !approveEmployeeLoanAndAdvanceDto.getEmpLoanAdvanceSuretyDetailsList().isEmpty()) {
					for (EmpLoanAdvanceSuretyDetails details : approveEmployeeLoanAndAdvanceDto
							.getEmpLoanAdvanceSuretyDetailsList()) {
						if (details.getEmployeeMaster().getId() != null) {
							if (details.getEmployeeMaster().getId().equals(loginBean.getEmployee().getId())) {
								selectedEmployeeLoanAndAdvance.setStatus(details.getStage());
							}
						}
					}
				}
				systemNotificationBean.loadTotalMessages();
			}
		} catch (Exception e) {
			log.error("notification method error---", e);
		}
		return null;
	}
}
