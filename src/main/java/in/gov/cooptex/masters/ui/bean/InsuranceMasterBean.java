package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.InsuranceMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeFamilyDetails;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.InsuranceMaster;
import in.gov.cooptex.core.model.InsuranceTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("insuranceMasterBean")
public class InsuranceMasterBean {

	private final String CREATE_URL = "/pages/masters/insurance/createInsurance.xhtml?faces-redirect=true;";
	private final String LIST_URL = "/pages/masters/insurance/listInsurance.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/masters/insurance/viewInsurance.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String INSURANCE_URL = AppUtil.getPortalServerURL() + "/insuranceMaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	InsuranceMaster insuranceMaster;
	
	@Getter
	@Setter
	List<InsuranceTypeMaster> insuranceTypeMasterList = new ArrayList<InsuranceTypeMaster>();
	
	@Getter
	@Setter
	InsuranceTypeMaster selectedInsuranceType = new InsuranceTypeMaster();
	
	@Getter
	@Setter
	List<InsuranceMaster> insuranceMasterList = new ArrayList<InsuranceMaster>();

	@Getter
	@Setter
	List<InsuranceMaster> assetCategoryMasterList = new ArrayList<InsuranceMaster>();

	@Getter
	@Setter
	InsuranceMaster selectedInsuranceMaster;

	@Getter
	@Setter
	LazyDataModel<InsuranceMaster> lazyInsuranceMasterList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	private String pageHead = "";
	
	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	Integer maxPersonCount;
	
	@Getter
	@Setter
	Map<Integer,Double> insuranceMemberAmount;
	
	@Getter
	@Setter
	Map<Integer,String> insuranceMembers;
	
	@Getter
	@Setter
	Integer selectedInsurance;
	
	@Getter
	@Setter
	Integer selectedMeber;
	
	@Getter
	@Setter
	Double memberAmount;
	
	@Getter
	@Setter
	Double validationTotpremiumAmt=0.0;
	
	@Getter
	@Setter
	InsuranceMasterDTO insuranceMasterDTO;

	public String showInsuranceMasterListPage() {
		log.info("<==== Starts Insurance Master Bean.showInsuranceMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true; 
		addButtonFlag = true;
		viewButtonFlag = true;
		insuranceTypeMasterList = new ArrayList<>();
		insuranceMasterList = new ArrayList<>();
		selectedInsuranceMaster = new InsuranceMaster();
		insuranceMemberAmount=new LinkedHashMap<Integer,Double>();
		insuranceMaster=new InsuranceMaster();
		insuranceMembers=new LinkedHashMap<Integer,String>();
		insuranceMasterDTO=new InsuranceMasterDTO();
		loadLazyInsuranceMasterList();
		getAllInsuranceTypeMaster();
		log.info("<==== Ends Insurance Master Bean.showInsuranceMasterListPage =====>");
		return LIST_URL;
	}

	
	public void loadLazyInsuranceMasterList() {
		log.info("<===== Starts Asset Category Bean.load AssetCategory List ======>");
		lazyInsuranceMasterList = new LazyDataModel<InsuranceMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<InsuranceMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = INSURANCE_URL + "/getInsuranceMasterList";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get asset category list");
					if (response != null && response.getStatusCode() == 0) {
						log.info("get asset category list" + response.getTotalRecords());
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						insuranceMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<InsuranceMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyAssetCategory List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return insuranceMasterList;
			}

			@Override
			public Object getRowKey(InsuranceMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public InsuranceMaster getRowData(String rowKey) {
				try {
					for (InsuranceMaster insuranceMaster : insuranceMasterList) {
						if (insuranceMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedInsuranceMaster = insuranceMaster;
							return insuranceMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends AssetCategory Bean.loadLazyAssetCategory List ======>");
	}
	
	public void createMemberDropDown() {
		if(maxPersonCount!=null) {
			for(int i=1;i<=maxPersonCount;i++) {
				insuranceMembers.put(i, "Members "+i);
			}
		}
	}
	
	public void createMemberAmount() {
		log.info("<----- start inside the createMeber Amount");
		log.info("<----- selected insurance meber..."+selectedInsurance);
		log.info("<----- selected insurance memberAmount..."+memberAmount);
		log.info("<----- befor add meber amount size()..."+insuranceMemberAmount.size());
		if(validationTotpremiumAmt<=0) {
			validationTotpremiumAmt=insuranceMaster.getInsurancePremium();
		}
		if(insuranceMaster.getInsurancePremium()!=null && memberAmount>=validationTotpremiumAmt) {
			insuranceMemberAmount.put(selectedInsurance, memberAmount);
			validationTotpremiumAmt=memberAmount;
		}else {
			AppUtil.addWarn("Invalid Premium amount");
			return;
		}
		
		memberAmount=null;
		selectedInsurance=null;
		log.info("<----- after add meber amount size()..."+insuranceMemberAmount.size());
		log.info("<----- End insurance createMeber Amount");
	}
	
	public void removeMembers(Integer key) {
		log.info("<====Starts InsuranceBean.removeFamilyMembers()  ====>" + insuranceMemberAmount);
			if (insuranceMemberAmount != null) {
				log.info("insuranceMemberAmountDTOList before removed :" + insuranceMemberAmount.size());
				 Double value=insuranceMemberAmount.get(key);
				/* for(Map.Entry<Integer,Double> map:insuranceMemberAmount.entrySet()){  
					if(map.getValue()>=value) {
						AppUtil.addWarn("Remove last added member");
						return;
					}
				 }*/
				insuranceMemberAmount.remove(key);
				log.info("insuranceMemberAmountDTOList after removed size :" + insuranceMemberAmount.size());
			}
	    }

	public String showInsuranceMasterPageAction() {
		log.info("<===== Starts Asset Category Bean.showAssetCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedInsuranceMaster == null || selectedInsuranceMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Asset Category called..");
				statusButtonFlag = false;
				pageHead = "Add";
				insuranceMaster = new InsuranceMaster();
				validationTotpremiumAmt=0.0;
				return CREATE_URL;
			} else if (action.equalsIgnoreCase("EDIT")) {
				maxPersonCount = Integer.valueOf(commonDataService.getAppKeyValue("INSURANCE_MAX_PERSON"));
				createMemberDropDown();
				validationTotpremiumAmt=0.0;
				log.info("edit Asset Category called and selected Id.."+selectedInsuranceMaster.getId());
				statusButtonFlag = true;
				pageHead = "Edit";
				String url = INSURANCE_URL + "/get/" + selectedInsuranceMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					insuranceMasterDTO = mapper.readValue(jsonResponse, new TypeReference<InsuranceMasterDTO>() {
					});
					
					insuranceMemberAmount=insuranceMasterDTO.getInsuranceMemberAmount();
					insuranceMaster=insuranceMasterDTO.getInsuranceMaster();
					
					return CREATE_URL;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete insurance master category called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInsuranceMasterDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view Insurance Master called..");
				String viewurl = INSURANCE_URL + "/get/" + selectedInsuranceMaster.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					insuranceMasterDTO = mapper.readValue(jsonResponse, new TypeReference<InsuranceMasterDTO>() {
					});
					
					insuranceMemberAmount=insuranceMasterDTO.getInsuranceMemberAmount();
					insuranceMaster=insuranceMasterDTO.getInsuranceMaster();
					
					return VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAssetCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends AssetCategory Bean.showAssetCategoryListPageAction ===========>" + action);
		return null;
	}
	
	
	 	
	 public void getAllInsuranceTypeMaster() {
			try {
					String url = AppUtil.getPortalServerURL() + "/insuranceMaster/getAllInsuranceTypeMaster";
					BaseDTO baseDTO = httpService.get(url);
					
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
						insuranceTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<InsuranceTypeMaster>>() {
						});
						log.info("entitytypelistByHeadAndRegionalId list size is "+insuranceTypeMasterList.size());
					}
			}catch(Exception e) {
				log.error("Exception while get the insurance type master....", e );
			}
		}	
	 	
	 	

	public String saveOrUpdate() {
		log.info("<<<<----- Start Save Or Update funciton --->>>>");
		try {
			insuranceMaster.setMaxPerson(maxPersonCount);
			insuranceMaster.setStatus(true);
			
			insuranceMasterDTO.setInsuranceMemberAmount(insuranceMemberAmount);
			insuranceMasterDTO.setInsuranceMaster(insuranceMaster);
			String url = INSURANCE_URL + "/create";
			log.info("<--- url"+url);
			BaseDTO response = httpService.post(url, insuranceMasterDTO);
			if (response != null && response.getStatusCode() == 0) {
				log.info("Insurance Master saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					//errorMap.notify(ErrorDescription.ASSETCATEGORY_SAVE_SUCCESS.getCode());
					AppUtil.addInfo("Insurance Created Successfully");
				else
					//errorMap.notify(ErrorDescription.ASSETCATEGORY_UPDATE_SUCCESS.getCode());
				AppUtil.addInfo("Insurance Updated Successfully");
			} else if (response != null && response.getStatusCode() != 0) {
				log.info("Exception while Insurance Master save ..........");
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends Asset category Bean.saveOrUpdate =======>");
		return showInsuranceMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Insurance Master Bean.onRowSelect ========>" + event);
		selectedInsuranceMaster = ((InsuranceMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends Insurance Master Bean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts Insurance Master Bean.deleteConfirm ===========>"+selectedInsuranceMaster.getId());
		try {
			BaseDTO response = httpService.delete(INSURANCE_URL + "/delete/" + selectedInsuranceMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				//errorMap.notify(ErrorDescription.ASSETCATEGORY_DELETE_SUCCESS.getErrorCode());
				AppUtil.addInfo("Insurance Deleted Successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInsuranceMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete insurance master ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends Insurance Master Bean.deleteConfirm ===========>");
		return showInsuranceMasterListPage();
	}

	public String clear() {
		showInsuranceMasterListPage();
		return LIST_URL;
	}
	
	public void ongroupInsurance() {
		try {
			if(insuranceMaster.getGroupInsurance()) {
			maxPersonCount = Integer.valueOf(commonDataService.getAppKeyValue("INSURANCE_MAX_PERSON"));
			log.info("max perons count...."+maxPersonCount);
			createMemberDropDown();
			}else {
				maxPersonCount=1;
			}
		}
		catch(Exception ex) {
			log.info("Exception",ex);
		}
	}

}
