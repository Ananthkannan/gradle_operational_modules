package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.MedicalLeaveConfig;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.LeaveRequestDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.LeaveEligibilityMaster;
import in.gov.cooptex.core.model.LeaveRequest;
import in.gov.cooptex.core.model.LeaveRequest.LeaveCategory;
import in.gov.cooptex.core.model.LeaveRequest.LeaveRequestStatus;
import in.gov.cooptex.core.model.LeaveRequestNote;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author praveen
 */
@Log4j2
@Scope("session")
@Service("leaveRequestBean")
public class LeaveRequestBean extends CommonBean {

	@Getter
	@Setter
	String pageName, leaveDocumentFileName;

	@Getter
	@Setter
	Boolean showEntityPanel;

	@Getter
	@Setter
	int totalRecords = 0;

	static String URI = AppUtil.getPortalServerURL() + "/leaverequest";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private LeaveRequest leaveRequest, selectedLeaveRequest;
	
	@Getter @Setter
	Date currentDate = new Date();

	@Getter
	@Setter
	private List<LeaveRequest> leaveRequestList;

	@Getter
	@Setter
	private List<LeaveTypeMaster> leaveTypeMasterList;

	@Getter
	@Setter
	private List<EntityMaster> entityLocationList;

	@Getter
	@Setter
	private List<EmployeeMaster> employeeList;
	
	@Getter 
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	private List<Department> departmentList;

	@Getter
	@Setter
	private List<EntityMaster> headRegionalOfficeList;

	@Getter
	@Setter
	private List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	private LazyDataModel<LeaveRequest> lazyLeaveRequestDataModel;

	@Getter
	@Setter
	private UploadedFile leaveDocument;

	long employeePhotoSize;

	@Getter
	@Setter
	private StreamedContent file;

	@Getter
	@Setter
	byte[] downloadDoc;

	@Getter
	@Setter
	private Boolean viewAdd, viewApprove, viewEdit, viewDelete, fromToDatePanel, viewCancel, cancelDropdown;

	@Getter
	@Setter
	public ActionTypeEnum actionType;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	private LeaveRequestDTO leaveRequestDTO = new LeaveRequestDTO();

	@Getter
	@Setter
	LeaveRequestNote leaveRequestNote = new LeaveRequestNote();

	@Getter
	@Setter
	LeaveRequestNote leaveRequestLastNote = new LeaveRequestNote();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	boolean enableFlag;

	@Getter
	@Setter
	String approveComments, rejectComments, finalApproveComments;

	@Getter
	@Setter
	LeaveEligibilityMaster leaveEligibilityMaster = new LeaveEligibilityMaster();

	@Getter
	@Setter
	Double leaveConsumed = 0.0;

	@Getter
	@Setter
	Double leaveBalance = 0.0;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	Double leaveConsumedDouble = 0.0;

	@Getter
	@Setter
	Double leaveBalanceDouble = 0.0;

	@Getter
	@Setter
	Boolean earnedLeaveFlag = false;

	@Getter
	@Setter
	private Map<String, String> leaveTypeMap = null;

	public void init() {
		log.info("LeaveRequestBean Init is executed.....................");
		// loadAppConfigValues();
		// leaveRequestDTO.setLeaveRequestNote(new LeaveRequestNote());
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public enum ActionTypeEnum {
		UPDATE, CREATE, APPROVE, VIEW, LIST, DELETE, LEAVECANCEL
	}

	private void lazyDataModel() {

		log.info(":: Leave request lazy load ::");

		lazyLeaveRequestDataModel = new LazyDataModel<LeaveRequest>() {

			private static final long serialVersionUID = 5217282771687301545L;

			@Override
			public List<LeaveRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				log.info(
						":: Leave request lazy load pagination first{}, page size {}, sort field {}, sort order {}, filter {} ::",
						first, pageSize, sortField, sortOrder.toString(), filters);
				try {
					PaginationDTO paginationDTO = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					log.info(":: Leave request lazy load pagination request ::" + paginationDTO);

					String resource = URI.concat("/lazyload");

					log.info(":: Leave request lazy load pagination request URI ::" + resource);

					BaseDTO baseDTO = httpService.post(resource, paginationDTO);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					if (Objects.isNull(baseDTO)) {
						log.info(":: Leave request lazy load pagination response is null ::" + baseDTO);
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
						return null;
					} else {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						leaveRequestList = mapper.readValue(jsonResponse, new TypeReference<List<LeaveRequest>>() {
						});
						if (leaveRequestList.isEmpty() || leaveRequestList.size() == 0) {
							log.info(
									":: Leave request lazy load pagination list is Empty ::" + leaveRequestList.size());
							errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
						} else {
							this.setRowCount(baseDTO.getTotalRecords());
							totalRecords = baseDTO.getTotalRecords();
							log.info(":: Leave request lazy load pagination list size is ::" + leaveRequestList.size());
						}
					}
				} catch (Exception e) {
					log.error(":: Exception in Leave request lazy load pagination ::", e);
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
				return leaveRequestList;
			}

			@Override
			public Object getRowKey(LeaveRequest leaveRequest) {
				return leaveRequest.getId();
			}

			@Override
			public LeaveRequest getRowData(String rowKey) {
				log.info(":: Selected leave request id is ::" + rowKey);
				for (LeaveRequest leave : leaveRequestList) {
					if (leave.getId().equals(Long.valueOf(rowKey))) {
						selectedLeaveRequest = leave;
						return leave;
					}
				}
				return null;
			}
		};
	}

	private void loadEmployeeLoggedInUserDetails() {
		log.info("LeaveRequestBean. loadEmployeeLoggedInUserDetails() - START");
		try {
			employeeMaster = new EmployeeMaster();
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
		} catch (Exception e) {
			log.error("Exception at LeaveRequestBean. loadEmployeeLoggedInUserDetails() ", e);
		}
		log.info("LeaveRequestBean. loadEmployeeLoggedInUserDetails() - END");
	}
	
	/**
	 * Retuns page based on condition.
	 * 
	 * @return
	 */
	public String showPage(ActionTypeEnum actionType) {
		log.info(":: Leave request page is :  {}LeaveRequest.xhtml ", actionType);
		leaveDocumentFileName = null;
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loadAppConfigValues();
		leaveRequestDTO.setLeaveRequestNote(new LeaveRequestNote());
		if (actionType.equals(ActionTypeEnum.CREATE)) {
			leaveRequest = new LeaveRequest();
			selectedLeaveRequest = new LeaveRequest();
			loadLeaveTypes();
			headRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
			// entityTypeMasterList = commonDataService.getAllEntityType();
			leaveRequestDTO = new LeaveRequestDTO();
			leaveRequestDTO.setLeaveRequestNote(new LeaveRequestNote());
			loadAllEntityTypeList();
			departmentList = commonDataService.getDepartment();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LEAVE_REQUEST.toString());
			showEntityPanel = false;
			employeeList = new ArrayList<>();
			pageName = actionType.toString();
			leaveEligibilityMaster = new LeaveEligibilityMaster();
			leaveConsumed = null;
			leaveBalance = null;
			leaveConsumedDouble = null;
			leaveBalanceDouble = null;
			loadEmployeeLoggedInUserDetails();
			return "/pages/leaveManagement/createLeaveRequestAdmin.xhtml?faces-redirect=true;";
		} else if (actionType.equals(ActionTypeEnum.UPDATE)) {
			if (Objects.isNull(selectedLeaveRequest) || Objects.isNull(viewLeaveDetails())) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			leaveRequest = selectedRecordId();
			leaveRequestDTO = new LeaveRequestDTO();
			leaveRequestDTO.setLeaveRequestNote(leaveRequestNote);
			leaveRequestDTO.setNote(leaveRequestNote.getNote());
			leaveRequestDTO.setFinalApproval(leaveRequestNote.getFinalApproval());
			leaveRequestDTO.setUserMaster(leaveRequestNote.getUserMaster());
			leaveConsumedDouble = leaveRequest.getLeaveConsumedDouble();
			leaveBalanceDouble = leaveRequest.getLeaveBalanceDouble();
			leaveEligibilityMaster.setNumberOfDays(leaveRequest.getLeaveEligibleDouble());
			leaveRequest.setLeaveBalance(leaveRequest.getLeaveBalanceDouble());
			leaveBalance = leaveRequest.getLeaveBalanceDouble();
			// forwardToUsersList =
			// commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LEAVE_REQUEST.toString());
			loadLeaveTypes();
			// onHeadOfficeChange();
			headRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
			loadAllEntityTypeList();
			onEntityTypeChange();
			departmentList = commonDataService.getDepartment();
			onDepartmentChange();
			pageName = actionType.toString();
			loadEmployeeLoggedInUserDetails();
			return "/pages/leaveManagement/createLeaveRequestAdmin.xhtml?faces-redirect=true;";
		} else if (actionType.equals(ActionTypeEnum.APPROVE)) {
			if (Objects.isNull(selectedLeaveRequest) || Objects.isNull(viewLeaveDetails())) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			selectedLeaveRequest = new LeaveRequest();
			return "/pages/leaveManagement/approveLeaveRequestAdmin.xhtml?faces-redirect=true;";
		} else if (actionType.equals(ActionTypeEnum.VIEW)) {
			// log.info("View status : " + viewLeaveDetails());
			enableFlag = false;
			viewLeaveDetails();
			// forwardToUsersList =
			// commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LEAVE_REQUEST.toString());
			/*
			 * if (Objects.isNull(selectedLeaveRequest) ||
			 * Objects.isNull(viewLeaveDetails())) {
			 * errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode()); return null;
			 * }
			 */
			selectedLeaveRequest = new LeaveRequest();
			systemNotificationBean.loadTotalMessages();
			return "/pages/leaveManagement/viewLeaveRequestAdmin.xhtml?faces-redirect=true;";
		} else if (actionType.equals(ActionTypeEnum.DELETE)) {
			if (Objects.isNull(selectedLeaveRequest)) {
				log.info(" Record not selected ");
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.APPROVED)) {
				log.info(" Seleted record status is : " + selectedLeaveRequest.getStatus());
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDelete').show();");
			}
			return "";
		} else if (actionType.equals(ActionTypeEnum.LEAVECANCEL)) {
			log.info("leave Cancel status : " + viewLeaveDetails());

			// forwardToUsersList =
			// commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LEAVE_REQUEST.toString());
			if (Objects.isNull(selectedLeaveRequest) || Objects.isNull(viewLeaveDetails())) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			leaveRequest = selectedRecordId();
			if (leaveRequest.getStatus().equals(LeaveRequestStatus.LEAVE_CANCEL)
					&& leaveRequest.getStatus().equals(LeaveRequestStatus.CANCEL_APPROVED) && leaveRequestNote != null
					&& leaveRequestNote.getUserMaster() != null && leaveRequestNote.getUserMaster().getId() != null
					&& !leaveRequestNote.getUserMaster().getId().equals(loginBean.getUserMaster().getId())) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			leaveRequestDTO = new LeaveRequestDTO();
			RequestContext.getCurrentInstance().execute("PF('confirmLeaveRequestDelete').show();");
			return null;
		} else {
			lazyDataModel();
			selectedLeaveRequest = new LeaveRequest();
			viewAdd = true;
			viewEdit = true;
			viewApprove = true;
			viewDelete = true;
			viewCancel = true;
			return "/pages/leaveManagement/listLeaveRequestAdmin.xhtml?faces-redirect=true;";
		}
	}

	/*
	 * public String editLeaveRequest() { log.info("Edit leave request...........");
	 * leaveRequest=new LeaveRequest(); try { if (selectedLeaveRequest == null)
	 * AppUtil.addWarn("Please select one leave request");
	 * 
	 * String resource = URI.concat("/getbyid/" + selectedLeaveRequest.getId());
	 * log.info(":: Selected leave request URI is ::" + resource); BaseDTO baseDTO =
	 * httpService.get(resource); if (Objects.isNull(baseDTO)) {
	 * log.info(":: Selected Leave request BaseDTO response is null ::" + baseDTO);
	 * errorMap.notify(baseDTO.getStatusCode()); return null; } else {
	 * log.info(":: Selected Leave request success response is ::" +
	 * baseDTO.getStatusCode()); jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); leaveRequest =
	 * mapper.readValue(jsonResponse, LeaveRequest.class); }
	 * 
	 * } catch (Exception e) {
	 * log.error(":: Exception while getting selected leave request ::", e);
	 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); } return
	 * null; }
	 */

	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			mapper = new ObjectMapper();
			/*
			 * EntityTypeMasterController.java - Method: getEntityType()
			 */
			BaseDTO response = httpService.get(AppUtil.getPortalServerURL() + "/entitytypemaster/getall/entitytype");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public boolean viewLeaveDetails() {
		if (Objects.isNull(selectedLeaveRequest)) {
			log.info(" Selected Leave request id is null : " + selectedLeaveRequest);
			return false;
		}

		leaveRequest = selectedRecordId();
		/*
		 * if (Objects.isNull(leaveRequest.getHeadOrRegionOffice()) ||
		 * leaveRequest.getHeadOrRegionOffice().getName().equals("Head Office Entity"))
		 * { showEntityPanel = false; headRegionalOfficeList =
		 * commonDataService.loadHeadAndRegionalOffice(); onDepartmentChange(); } else {
		 * showEntityPanel = true; headRegionalOfficeList =
		 * commonDataService.loadHeadAndRegionalOffice(); entityTypeMasterList =
		 * commonDataService.getAllEntityType(); onEntityTypeChange(); onEntityChange();
		 * departmentList = commonDataService.getDepartment();
		 * onEntityDepartmentChange(); }
		 */
		if (!Objects.isNull(leaveRequest.getDocumentPath())) {
			downloadDoc = downloadFile(leaveRequest.getDocumentPath());
		}
		if (Objects.isNull(leaveRequest.getDocumentPath())) {
			leaveDocumentFileName = "";
		} else {
			if (leaveRequest.getDocumentPath().contains("uploaded")) {
				String filePathValue[] = leaveRequest.getDocumentPath().split("uploaded/");
				leaveDocumentFileName = filePathValue[1];
			} else {
				String filePathValue[] = leaveRequest.getDocumentPath().split("LeaveRequestDocument/");
				log.info("File Path" + filePathValue[0]);
				File file = new File(leaveRequest.getDocumentPath());
				leaveDocumentFileName = file.getName();
			}
		}
		loadLeaveTypes();
		return true;
	}

	public LeaveRequest selectedRecordId() {
		log.info(":: Selected leave request ::");
		LeaveRequest leaveRequest = null;
		try {
			if (Objects.isNull(selectedLeaveRequest.getId())) {
				log.info(" Selected Leave request id is null : " + selectedLeaveRequest.getId());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_ID_ISNULL.getErrorCode());
				return null;
			}

			String resource = URI.concat("/getbyid/" + selectedLeaveRequest.getId() + "/" + systemNotificationId);
			log.info(":: Selected leave request URI is ::" + resource);
			BaseDTO baseDTO = httpService.get(resource);
			if (Objects.isNull(baseDTO)) {
				log.info(":: Selected Leave request BaseDTO response is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				log.info(":: Selected Leave request success response is ::" + baseDTO.getStatusCode());
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				leaveRequest = mapper.readValue(jsonResponse, LeaveRequest.class);
				commentAndLogList = baseDTO.getCommentAndLogList();
				if (leaveRequest.getLeaveRequestNoteList() != null
						&& !leaveRequest.getLeaveRequestNoteList().isEmpty()) {
					leaveRequestNote = leaveRequest.getLeaveRequestNoteList()
							.get(leaveRequest.getLeaveRequestNoteList().size() - 1);
					previousApproval = leaveRequestNote.getFinalApproval();

					log.info("<===== leaveRequestNote ====> " + leaveRequestNote.getNote());
					log.info(" loginBean.getUserMaster() ====> " + loginBean.getUserMaster().getId());
					log.info(" loginBean.getUserMaster() ====> " + leaveRequestNote.getUserMaster().getId());
				}
				if (leaveRequestNote != null && leaveRequestNote.getNote() != null)
					leaveRequestDTO.setNote(leaveRequestNote.getNote());

				if (leaveRequestNote != null && leaveRequestNote.getId() != null && leaveRequestNote.getUserMaster()
						.getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
					enableFlag = true;
				}
			}
		} catch (Exception e) {
			log.error(":: Exception while getting selected leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return leaveRequest;
	}

	public String saveUpdateLeaveRequest() {
		log.info(":: Leave request save or update ::");
		try {
			BaseDTO baseDTO;
			if (leaveRequest.getNoOfDaysRequested().equals(0)) {
				log.info("===Total Number of Days is 0=====");
				Util.addWarn("You are not eligible for apply leave");
				return null;
			}
			leaveRequest.setLeaveConsumed(leaveConsumed);
			leaveRequest.setLeaveBalance(leaveBalance);
			leaveRequestDTO.setLeaveRequest(leaveRequest);

			if (leaveDocumentFileName == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_DOCUMENTS_NOT_EMPTY).getErrorCode());
				return null;
			}
			if (leaveRequestDTO.getLeaveRequestNote().getNote() == null
					|| leaveRequestDTO.getLeaveRequestNote().getNote().isEmpty()) {
				errorMap.notify(ErrorDescription.MEETING_REQUEST_NOTE_EMPTY.getErrorCode());
				return null;
			}

			if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("UEL")) {
				Calendar retirementDateCal = Calendar.getInstance();
				retirementDateCal
						.setTime(leaveRequest.getEmployeeMaster().getPersonalInfoEmployment().getRetirementDate());
				LocalDate retirementDate = LocalDate.of(retirementDateCal.get(Calendar.YEAR),
						retirementDateCal.get(Calendar.MONTH) + 1, retirementDateCal.get(Calendar.DAY_OF_MONTH));
				Calendar currentDateCal = Calendar.getInstance();
				currentDateCal.setTime(new Date());
				LocalDate currentDate = LocalDate.of(currentDateCal.get(Calendar.YEAR),
						currentDateCal.get(Calendar.MONTH) + 1, currentDateCal.get(Calendar.DAY_OF_MONTH));
				Period difference = Period.between(currentDate, retirementDate);
				Integer serviceYears = 0;
				serviceYears = difference.getYears();
				if (serviceYears > 2) {
					Util.addWarn("your Not eligible for this type leave");
					return null;
				}
			}

			if (Objects.isNull(leaveRequest)) {
				log.info(":: Leave request object is null ::" + leaveRequest);
				errorMap.notify();
				return null;
			}

			if (leaveRequest.getLeaveType() != null) {
				if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("EL")) {
					log.info("no of days requested============>" + leaveRequest.getNoOfDaysRequested());
					if (leaveRequest.getNoOfDaysRequested() != null) {
						if (leaveRequest.getType() == null || leaveRequest.getType() == "") {
							FacesContext fc = FacesContext.getCurrentInstance();
							fc.addMessage("type", new FacesMessage("Type is required"));
							return null;
						}
						if (leaveRequest.getType().equalsIgnoreCase("Surrendered")) {
							if (leaveRequest.getNoOfDaysRequested() > 15) {
								errorMap.notify(MastersErrorCode.LEAVE_REQUEST_EARNED_LEAVE_VALIDATION);
								return null;
							}
						}
					}
				}
			}

			if (leaveRequest.getType() != null && leaveRequest.getType().equalsIgnoreCase("Surrendered")
					&& checkAlreadyLoanPreclosured()) {
				Util.addWarn("Maximum 15 days only allowed to apply leave within one year for Surrendered");
				return null;
			}

			if (Objects.isNull(leaveRequest.getId())) {
				String resource = URI.concat("/create");
				log.info(":: Save leave request URI is ::" + resource);
				baseDTO = httpService.post(resource, leaveRequestDTO);
			} else {
				String resource = URI.concat("/update");
				log.info(":: Update leave request URI is ::" + resource);
				leaveRequest.setDateOfApplication(new Date());
				baseDTO = httpService.put(resource, leaveRequestDTO);
			} 
			if (Objects.isNull(baseDTO)) {
				log.info(":: Leave request BaseDTO response is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				log.info(":: Leave request success response is ::" + baseDTO.getStatusCode());
				if (baseDTO.getStatusCode() == 0) {
					if (Objects.isNull(leaveRequest.getId())) {
						log.info(":: Leave request saved successfully::");
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_SAVED_SUCCESS.getErrorCode());
					} else {
						log.info(":: Leave request updated successfully::");
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_UPDATED_SUCCESS.getErrorCode());
					}
				} else if (baseDTO.getStatusCode() == ErrorDescription
						.getError(PersonnalErrorCode.LEAVE_REQUEST_ALREADY_EXISTS).getCode()) {
					errorMap.notify(
							ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_ALREADY_EXISTS).getErrorCode());
					return null;
				} else if (baseDTO.getStatusCode().equals(MastersErrorCode.PREVIOUS_APPROVAL_IS_PENDING)) {
					AppUtil.addWarn("Previous approval is pending");
					return null;
				} else {
					log.info(":: Leave request failed response is ::" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(":: Exception while save leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		selectedLeaveRequest = new LeaveRequest();
		leaveRequest = new LeaveRequest();
		viewAdd = true;
		return "/pages/leaveManagement/listLeaveRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void deleteLeaveRequest() {
		log.info(":: Delete leave request by id ::");
		try {
			if (Objects.isNull(selectedLeaveRequest)) {
				log.info(":: Selected leave request is null ::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}
			String resource = URI.concat("/deletebyid/" + selectedLeaveRequest.getId());
			log.info(":: Delete leave request URI by id is ::" + resource);
			BaseDTO baseDTO = httpService.delete(resource);
			if (Objects.isNull(baseDTO)) {
				log.info(":: Leave request BaseDTO request is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return;
			} else {
				log.info(":: Leave request success response is ::" + baseDTO.getStatusCode());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_DELETED_SUCCESS.getErrorCode());
			}
		} catch (Exception e) {
			log.error(":: Exception while delete leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void onHeadOfficeChange() {

		if (Objects.isNull(leaveRequest.getHeadOrRegionOffice())) {
			showEntityPanel = false;
			employeeList = new ArrayList<>();
			leaveRequest.setEntityMaster(null);
			leaveRequest.setEntityTypeMaster(null);
			leaveRequest.setDepartment(null);
			leaveRequest.setLeaveType(null);
			leaveRequest.setEmployeeMaster(null);
			RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
		} else {
			if (leaveRequest.getHeadOrRegionOffice().getName().equals("Head Office Entity")) {
				showEntityPanel = false;
				leaveRequest.setEntityMaster(null);
				leaveRequest.setEntityTypeMaster(null);
				leaveRequest.setDepartment(null);
				leaveRequest.setLeaveType(null);
				leaveRequest.setEmployeeMaster(null);
				RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
			} else {
				showEntityPanel = true;
				leaveRequest.setEntityMaster(null);
				leaveRequest.setEntityTypeMaster(null);
				leaveRequest.setDepartment(null);
				leaveRequest.setLeaveType(null);
				leaveRequest.setEmployeeMaster(null);
				employeeList = new ArrayList<>();
				RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
			}
		}
	}

	
	public void onDepartmentChange() {
		log.info("... Load  Employee called ....");
		Long departmentId = 0L;
		employeeList = new ArrayList<>();
		try {
			mapper = new ObjectMapper();
			String headOfficeId = null;
			if (leaveRequest != null && leaveRequest.getHeadOrRegionOffice() != null)
				headOfficeId = leaveRequest.getHeadOrRegionOffice() != null
						? leaveRequest.getHeadOrRegionOffice().getId().toString()
						: null;
			else
				headOfficeId = leaveRequest.getEntityMaster() != null
						? leaveRequest.getEntityMaster().getId().toString()
						: null;
			departmentId = leaveRequest.getDepartment() != null ? leaveRequest.getDepartment().getId() : null;
			/*
			 * EmployeeMasterController.java - Method: getAllByWOrkLocationAndDepartment
			 */
			log.info("onDepartmentChange :: headOfficeId==> " + headOfficeId);
			log.info("onDepartmentChange :: departmentId==> " + departmentId);
			BaseDTO response = httpService.get(AppUtil.getPortalServerURL() + "/employee/getallbyworklocation/"
					+ headOfficeId + "/" + departmentId);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if (employeeList != null && !employeeList.isEmpty()) {
				log.info("employeeList size==> " + employeeList.size());
			}
		} catch (Exception e) {
			log.error("Exception occured while loading Employee  data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/*
	 * public void onDepartmentChange() { try { if
	 * (Objects.isNull(leaveRequest.getDepartment())) { employeeList = new
	 * ArrayList<>(); return; } Long entityId = null; if
	 * (Objects.isNull(leaveRequest.getHeadOrRegionOffice().getEntityTypeMaster())
	 * && Objects.isNull(leaveRequest.getDepartment())) {
	 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); return; }
	 * else { entityId =
	 * leaveRequest.getHeadOrRegionOffice().getEntityTypeMaster().getId(); } Long
	 * departmentId = leaveRequest.getDepartment().getId(); String resource = URI +
	 * "/employeelist/" + entityId + "/" + departmentId; BaseDTO baseDto =
	 * httpService.get(resource); jsonResponse =
	 * mapper.writeValueAsString(baseDto.getResponseContents()); employeeList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
	 * }); if (!Objects.isNull(leaveRequest.getEmployeeMaster()) &&
	 * !Objects.isNull(leaveRequest.getEmployeeMaster().getId())) {
	 * employeeList.stream().forEach(employee -> { if
	 * (employee.getId().equals(leaveRequest.getEmployeeMaster().getId())) {
	 * log.info(":: Selected employee id is {} & name is {} ::",
	 * leaveRequest.getEmployeeMaster().getId(),
	 * leaveRequest.getEmployeeMaster().getFirstName());
	 * leaveRequest.setEmployeeMaster(employee); } }); }
	 * log.info(" Employee List size is :: " + employeeList.size()); } catch
	 * (Exception e) { log.error(":: Exception while onDepartmentChange ::", e);
	 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); } }
	 */

	public void loadLeaveTypes() {
		log.info("Loading all active leave types");
		try {
			BaseDTO leaveTypeMasterDto = new BaseDTO();
			// String url = AppUtil.getPortalServerURL() + "/leave/getallactive";
			String url = AppUtil.getPortalServerURL() + "/leavetype/getallactive";
			log.info("leaveTypeMasterDto url==>" + url);
			leaveTypeMasterDto = httpService.get(url);
			if (leaveTypeMasterDto != null) {
				String jsonResponse = mapper.writeValueAsString(leaveTypeMasterDto.getResponseContents());
				leaveTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<LeaveTypeMaster>>() {
				});
				if (leaveTypeMasterList.isEmpty() || leaveTypeMasterList.size() == 0) {
					log.info("Leave types list size is empty :" + leaveTypeMasterList.size());
				} else {
					log.info("Leave types list size is :" + leaveTypeMasterList.size());
				}
			}
		} catch (Exception e) {
			log.error(": Exception while retiveing leave type masters :", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			leaveDocument = event.getFile();
			long size = leaveDocument.getSize();
			log.info("Leave uploaded document size is : " + size);
			size = size / 5000;
			if (size > employeePhotoSize) {
				leaveRequest.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				// errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				AppUtil.addWarn("Upload File Size Within 500 KB");
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("LEAVE_REQUEST_UPLOAD_PATH");
			String filePath = Util.fileUpload(uploadPath, leaveDocument);
			/*
			 * File file = new File(uploadPath); uploadPath = file.getAbsolutePath() + "/";
			 * Path path = Paths.get(uploadPath); if (Files.notExists(path)) {
			 * log.info(" Path Doesn't exixts"); Files.createDirectories(path); }
			 */
			log.info(" Uploaded document oath is :" + uploadPath);
			leaveDocumentFileName = leaveDocument.getFileName();
			leaveRequest.setDocumentPath(filePath);
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_DOCUMENT_UPLOADED.getErrorCode());
			/*
			 * inputStream = leaveDocument.getInputstream(); outputStream = new
			 * FileOutputStream(uploadPath + leaveDocumentFileName); byte[] buffer = new
			 * byte[(int) leaveDocument.getSize()]; int bytesRead = 0; while ((bytesRead =
			 * inputStream.read(buffer)) != -1) { outputStream.write(buffer, 0, bytesRead);
			 * } log.info(" upload Path is >>>" + uploadPath);
			 * log.info(" Leave Document is uploaded successfully"); if (outputStream !=
			 * null) { outputStream.close(); } if (inputStream != null) {
			 * inputStream.close(); }
			 */
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void onEntityTypeChange() {
		log.info(":: Entity type change ::");
		try {

			if (Objects.isNull(leaveRequest.getHeadOrRegionOffice())
					|| Objects.isNull(leaveRequest.getEntityTypeMaster())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}

			if (Objects.isNull(leaveRequest.getHeadOrRegionOffice().getId())
					&& Objects.isNull(leaveRequest.getEntityTypeMaster().getId())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}
			Long entityId = leaveRequest.getHeadOrRegionOffice().getId();
			Long entiyTypeId = leaveRequest.getEntityTypeMaster().getId();
			String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/activeEntity/" + entityId + "/"
					+ entiyTypeId;
			log.info("URL request for entity :: " + resource);
			BaseDTO baseDto = httpService.get(resource);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			entityLocationList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			log.info("Entity location list size is" + entityLocationList.size());

		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void onEntityChange() {
		if (!Objects.isNull(leaveRequest.getEntityMaster())
				&& !Objects.isNull(leaveRequest.getEntityMaster().getId())) {
			leaveRequest.setEntityMaster(leaveRequest.getEntityMaster());
		}
	}

	public void onheadOrRegionOfficeChange() {
		log.info(":: Entity type change ::");
		try {
			leaveRequest.setEntityTypeMaster(new EntityTypeMaster());
			leaveRequest.setEntityMaster(new EntityMaster());
			leaveRequest.setDepartment(new Department());
			leaveRequest.setEmployeeMaster(new EmployeeMaster());
		} catch (Exception e) {
			log.error(":: Exception while headOrRegionOffice Change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}
	
	public void onEntityDepartmentChange() {
		log.info(":: Entity and department change ::");
		try {
			if (leaveRequest.getEntityTypeMaster() == null) {
				errorMap.notify(ErrorDescription.ERROR_ENTITY_ID_DUPLICATE.getErrorCode());
				return;
			}
			if (leaveRequest.getEntityMaster() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_ENTITY_REQUIRED.getErrorCode());
				return;
			}
			if (leaveRequest.getDepartment() == null) {
				errorMap.notify(ErrorDescription.DEPARTMENT_REQUIRED.getErrorCode());
				return;
			}

			/*
			 * if (Objects.isNull(leaveRequest.getEntityMaster()) &&
			 * Objects.isNull(leaveRequest.getDepartment())) {
			 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); return; }
			 * 
			 * 
			 * if (Objects.isNull(leaveRequest.getEntityTypeMaster())) {
			 * log.info(":: Entity type master id is ::" +
			 * leaveRequest.getEntityTypeMaster());
			 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); return; }
			 */
			leaveRequest.setEmployeeMaster(null);
			Long entityId = leaveRequest.getEntityMaster().getId();
			log.info("entityId ::" + entityId);
			Long departmentId = leaveRequest.getDepartment().getId();
			log.info("departmentId ::" + departmentId);

			// String url = URI + "/employeelist/" + entityId + "/" + departmentId;
			String url = AppUtil.getPortalServerURL() + "/employee/getallbyworklocation/" + entityId + "/"
					+ departmentId;
			log.info("url==> " + url);
			BaseDTO baseDto = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if (!Objects.isNull(leaveRequest.getEmployeeMaster())
					&& !Objects.isNull(leaveRequest.getEmployeeMaster().getId())) {
				employeeList.stream().forEach(employee -> {
					log.info(":: Selected employee id is {} & name is {} ::", leaveRequest.getEmployeeMaster().getId(),
							leaveRequest.getEmployeeMaster().getFirstName());
					if (employee.getId().equals(leaveRequest.getEmployeeMaster().getId())) {
						leaveRequest.setEmployeeMaster(employee);
					}
				});
			}
			log.info(" Employee List size is :: " + employeeList.size());
		} catch (Exception e) {
			log.error(":: Exception while onEntityDepartmentChange() ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

	}

	public void onEmployeeChange() {
		if (Objects.isNull(leaveRequest.getEmployeeMaster())) {
			log.info(" Selected Employee is :" + leaveRequest.getEmployeeMaster());
			setNull();
		}
		log.info(" Employee id is :" + leaveRequest.getEmployeeMaster());
		if (!Objects.isNull(leaveRequest.getEmployeeMaster())) {
			log.info(" Selected Employee id is :" + leaveRequest.getEmployeeMaster().getId());
			leaveRequest.setEmployeeMaster(leaveRequest.getEmployeeMaster());
			setNull();
		}
	}

	public void onEmployeePfChange() {
		if (Objects.isNull(leaveRequest.getEmployeeMaster())) {
			log.info(" Selected Employee is :" + leaveRequest.getEmployeeMaster());
			setNull();
		}
		log.info(" Employee id is :" + leaveRequest.getEmployeeMaster());
		if (!Objects.isNull(leaveRequest.getEmployeeMaster())) {
			log.info(" Selected Employee id is :" + leaveRequest.getEmployeeMaster().getId());
			leaveRequest.setEmployeeMaster(leaveRequest.getEmployeeMaster());
			setNull();
		}
	}

	public void setNull() {
		leaveRequest.setLeaveType(null);
		leaveRequest.setLeaveEligibility(null);
		leaveRequest.setLeaveConsumed(null);
		leaveRequest.setLeaveBalance(null);
		leaveRequest.setLeaveFromDate(null);
		leaveRequest.setLeaveToDate(null);
		leaveRequest.setNoOfDaysRequested(null);
	}

	public void onLeaveTypeChange() {
		log.info(" On leave type change ");
		try {
			earnedLeaveFlag = false;
			leaveEligibilityMaster.setNumberOfDays(null);
			leaveConsumed = 0.0;
			leaveBalance = 0.0;
			if (Objects.isNull(leaveRequest.getLeaveType())) {
				log.info(" Employee leave type is :  " + leaveRequest.getLeaveType());
				fromToDatePanel = true;
				leaveRequest.setLeaveEligibility(null);
				leaveRequest.setLeaveConsumed(null);
				leaveRequest.setLeaveBalance(null);
				leaveRequest.setLeaveFromDate(null);
				leaveRequest.setLeaveToDate(null);
				leaveRequest.setNoOfDaysRequested(null);
				RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
				return;
			}
			addLeaveType(leaveRequest.getLeaveType().getCode());
			if (!Objects.isNull(leaveRequest.getLeaveType())) {
				if (!"ML".equals(leaveRequest.getLeaveType().getCode())) {

					if ("MAT".equals(leaveRequest.getLeaveType().getCode())) {
						if (leaveRequest.getEmployeeMaster().getGender().getCode().equals("M")) {
							AppUtil.addWarn("Maternity Leave only for Female Employees");
							leaveRequest.setLeaveType(null);
							return;
						}
					}

					if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("UEL")) {
						if (leaveRequest.getEmployeeMaster() != null
								&& leaveRequest.getEmployeeMaster().getPersonalInfoEmployment() != null && leaveRequest
										.getEmployeeMaster().getPersonalInfoEmployment().getDateOfJoining() != null) {
							LocalDate dateBefore = LocalDate.parse(AppUtil.REPORT_DATE_FORMAT.format(
									leaveRequest.getEmployeeMaster().getPersonalInfoEmployment().getDateOfJoining()));
							LocalDate dateAfter = LocalDate.parse(AppUtil.REPORT_DATE_FORMAT.format(new Date()));
							long noOfYearssBetween = ChronoUnit.YEARS.between(dateBefore, dateAfter);
							log.info("no of years between joining date------------>" + noOfYearssBetween);
							if (noOfYearssBetween < 15) {
								errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode());
								leaveRequest.setLeaveType(null);
								return;
							}
						}
					}

					log.info(" Employee leave type is :  " + leaveRequest.getLeaveType().getName());
					fromToDatePanel = false;
					leaveRequest.setLeaveCategory(null);
					log.info("Department Id==> " + leaveRequest.getDepartment().getId());
					log.info("Leave type Id==> " + leaveRequest.getLeaveType().getId());
					String url = AppUtil.getPortalServerURL() + "/leaveEligibility/getByDepartmentIdAndLeaveTypeId/"
							+ leaveRequest.getDepartment().getId() + "/" + leaveRequest.getLeaveType().getId();
					log.info("LeaveRequestBean :: onLeaveTypeChange :: url==> " + url);
					BaseDTO baseDto = httpService.get(url);
					if (baseDto != null && baseDto.getStatusCode() == 0 && baseDto.getResponseContent() != null) {
						jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
						leaveEligibilityMaster = mapper.readValue(jsonResponse,
								new TypeReference<LeaveEligibilityMaster>() {
								});
						log.info("=======leaveEligibilityMaster is==========" + leaveEligibilityMaster.toString());

						log.info("number of days==> " + leaveEligibilityMaster.getNumberOfDays());

						if ("Earned Leave".equalsIgnoreCase(leaveRequest.getLeaveType().getName())) {
							getLeaveDetails(leaveRequest.getEmployeeMaster().getId(),
									leaveRequest.getLeaveType().getId(), leaveRequest.getLeaveType().getName());
							log.info("leaveConsumed Earned Leave ==> " + leaveConsumed);
						} else {
							leaveConsumed = getleaveConsumedEmployee(leaveRequest.getEmployeeMaster().getId(),
									leaveRequest.getLeaveType().getId());
							log.info("leaveConsumed==> " + leaveConsumed);

							if (leaveEligibilityMaster.getNumberOfDays() != null && leaveConsumed != null) {
								leaveBalance = (leaveEligibilityMaster.getNumberOfDays() - leaveConsumed);
							}
						}
						log.info("leaveBalance=======> " + leaveBalance);

					} else {
						leaveEligibilityMaster.setNumberOfDays(null);
						leaveConsumed = 0.0;
						leaveBalance = 0.0;
					}

					RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
				}
			} else {
				log.info(" Employee leave type is :  " + leaveRequest.getLeaveType());
				fromToDatePanel = true;
				leaveRequest.setLeaveEligibility(null);
				leaveRequest.setLeaveConsumed(null);
				leaveRequest.setLeaveBalance(null);
				leaveRequest.setLeaveFromDate(null);
				leaveRequest.setLeaveToDate(null);
				leaveRequest.setNoOfDaysRequested(null);
				RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
				return;
			}

			if (Objects.isNull(leaveRequest.getEmployeeMaster())) {
				log.info(" Employee name is :  " + leaveRequest.getEmployeeMaster());
				// errorMap.notify(ErrorDescription.LEAVE_REQUEST_EMPLOYEE_NAME_REQUIRED.getErrorCode());
				return;
			}
			if (Objects.isNull(leaveRequest.getLeaveType())) {
				log.info(" Employee leave type is :  " + leaveRequest.getLeaveType().getName());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVETYPE_REQUIRED.getErrorCode());
				return;
			}

			log.info(" On leave type change leave type is :  " + leaveRequest.getLeaveType().getName());
			log.info(" On leave type change employee name is : " + leaveRequest.getEmployeeMaster().getFirstName());
			String resource = URI + "/leave/" + leaveRequest.getEmployeeMaster().getId() + "/"
					+ leaveRequest.getLeaveType().getId();
			log.info("onLeaveTypeChange url==>" + resource);
			BaseDTO baseDto = httpService.get(resource);
			if (baseDto != null) {
				if (baseDto.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
					jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
					LeaveRequest leaveDetail = mapper.readValue(jsonResponse, LeaveRequest.class);
					leaveConsumedDouble = leaveDetail.getLeaveConsumedDouble();
					leaveBalanceDouble = leaveDetail.getLeaveBalanceDouble();
					leaveEligibilityMaster.setNumberOfDays(leaveDetail.getLeaveEligibleDouble());
					if (!Objects.isNull(leaveDetail)) {
						if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("EL")
								|| leaveRequest.getLeaveType().getCode().equalsIgnoreCase("ML")) {
							leaveRequest.setLeaveEligibility(leaveDetail.getLeaveEligibleDouble());
							leaveRequest.setLeaveConsumed(leaveDetail.getLeaveConsumedDouble());
							leaveRequest.setLeaveBalance(leaveDetail.getLeaveBalanceDouble());
						} else {
							leaveRequest.setLeaveConsumed(leaveDetail.getLeaveConsumed());
							if (!Objects.isNull(leaveRequest.getLeaveType().getEligibility())) {
								leaveRequest.setLeaveBalance(
										leaveRequest.getLeaveType().getEligibility() - leaveDetail.getLeaveConsumed());
								leaveRequest.setLeaveEligibility(
										leaveRequest.getLeaveType().getEligibility().doubleValue());
							} else {
								leaveRequest.setLeaveBalance(0.0);
								leaveRequest.setLeaveEligibility(0.0);
							}
							log.info(" Employee consumed leave is :" + leaveDetail.getLeaveConsumed());
							log.info(" Employee leave balance is :" + leaveRequest.getLeaveBalance());
						}
					} else {
						leaveRequest.setLeaveConsumed(null);
						leaveRequest.setLeaveBalance(null);
						leaveRequest.setLeaveEligibility(null);
					}
				} else if (baseDto.getStatusCode().equals(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode())) {
					errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode());
					leaveRequest.setLeaveType(null);
				} else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
					leaveRequest.setLeaveType(null);
				}
			}

		} catch (Exception e) {
			log.error(":: Exception while On leave type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	private void getLeaveDetails(Long empId, Long leaveTypeId, String leaveTypeName) {
		log.info("getLeaveDetails methods starts-----");
		try {
			if (empId != null && leaveTypeId != null) {
				log.info("getLeaveDetails :: employeeId==> " + empId);
				log.info("getLeaveDetails :: leaveTypeId==> " + leaveTypeId);
				mapper = new ObjectMapper();
				String url = AppUtil.getPortalServerURL() + "/leaverequest/getLeaveDetailsEmployee/" + empId + "/"
						+ leaveTypeId + "/" + leaveTypeName;
				log.info("getleaveConsumedEmployee :: url==> " + url);
				BaseDTO baseDTO = httpService.get(url);
				log.info("response----" + baseDTO != null);
				if (baseDTO != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					LeaveRequest request = mapper.readValue(jsonResponse, new TypeReference<LeaveRequest>() {
					});
					if (request != null) {
						leaveConsumed = request.getLeaveConsumed();
						leaveBalance = request.getLeaveBalance();
						leaveEligibilityMaster.setNumberOfDays(Double.valueOf(request.getLeaveEligibility()));
						log.info("Earned leave consumed-------" + leaveConsumed);
						log.info("Earned leave balance-------" + leaveBalance);
						leaveRequest.setLeaveConsumed(leaveConsumed);
						leaveRequest.setLeaveBalance(leaveBalance);
						leaveRequest.setLeaveEligibility(request.getLeaveEligibility());
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in getLeaveDetails method", e);
		}
		log.info("getLeaveDetails methods ends-----");
	}

	private double getleaveConsumedEmployee(Long employeeId, Long leaveTypeId) {
		log.info("getleaveConsumedEmployee :: start");
		// Integer totalConsumedLeave = 0;
		double totalConsumedLeave = 0.0;
		try {
			log.info("getleaveConsumedEmployee :: employeeId==> " + employeeId);
			log.info("getleaveConsumedEmployee :: leaveTypeId==> " + leaveTypeId);
			mapper = new ObjectMapper();
			String url = AppUtil.getPortalServerURL() + "/leaverequest/getleaveConsumedEmployee/" + employeeId + "/"
					+ leaveTypeId;
			log.info("getleaveConsumedEmployee :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("response----" + baseDTO != null);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				totalConsumedLeave = mapper.readValue(jsonResponse, new TypeReference<Double>() {
				});
				log.info("totalConsumedLeave------- " + totalConsumedLeave);
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception in getCategoryCode method", e);
		}
		return totalConsumedLeave;
	}

	public void numberOfDaysBetweenTwoDates() {
		try {
			earnedLeaveFlag = false;
			if (Objects.isNull(leaveRequest.getLeaveFromDate())) {
				log.info("Leave From Date is : " + leaveRequest.getLeaveFromDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			} else {
				log.info("Leave From Date is : " + leaveRequest.getLeaveFromDate());
				leaveRequest.setLeaveFromDate(leaveRequest.getLeaveFromDate());
			}

			/*
			 * if (Objects.isNull(leaveRequest.getLeaveToDate())) {
			 * log.info("Leave to date is : " + leaveRequest.getLeaveToDate());
			 * errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_TO_DATE_REQUIRED.
			 * getErrorCode()); return; } else { log.info("Leave to date is : " +
			 * leaveRequest.getLeaveToDate());
			 * leaveRequest.setLeaveToDate(leaveRequest.getLeaveToDate()); }
			 */
			if (leaveRequest.getLeaveToDate() != null) {
				leaveRequest.setLeaveToDate(leaveRequest.getLeaveToDate());
			}

			if (leaveRequest.getLeaveToDate() != null && leaveRequest.getLeaveFromDate() != null) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String fromDate = dateFormat.format(leaveRequest.getLeaveFromDate());
				String toDate = dateFormat.format(leaveRequest.getLeaveToDate());

				log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

				Date firstDate = dateFormat.parse(fromDate);
				Date secondDate = dateFormat.parse(toDate);

				long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

				log.info("diff------------->" + diff);

				if (secondDate.before(firstDate)) {
					log.info(" :: Leave request from and to dates are invalid :: ");
					errorMap.notify(ErrorDescription.LEAVE_REQUEST_INVALID_FROM_TO_DATE.getErrorCode());
					return;
				}
				// Because edit time leaveBalance is null.so value set.
				if (leaveBalanceDouble == null) {
					log.info("Leave balance ..." + leaveRequest.getLeaveBalance());
					leaveBalanceDouble = leaveRequest.getLeaveBalance().doubleValue();
				}
				if (firstDate.compareTo(secondDate) == 0) {
					log.info("Number of days for leave is : " + 1);
					leaveRequest.setNoOfDaysRequested(1.0);
				} else {
					log.info("Number of days for leave is : " + diff);
					// int leaveDays=AppUtil.calculateWorkDays(firstDate, secondDate);
					double leaveDays = AppUtil.calculateWorkDayswithCurrentDate(firstDate, secondDate);
					leaveRequest.setNoOfDaysRequested(leaveDays);
				}
				String value = commonDataService.getAppKeyValue("EARNED_LEAVE_CLAIMS_DAYS");
				Integer appValue = Integer.parseInt(value);
				if (leaveRequest.getLeaveType() != null && leaveRequest.getLeaveType().getCode() != null
						&& leaveRequest.getLeaveType().getCode().equals("EL")) {
					if (Objects.isNull(leaveRequest.getLeaveBalance())
							&& leaveRequest.getLeaveBalance() < leaveRequest.getLeaveEligibility()) {
						log.info(" Leave balance is null ");
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
						leaveRequest.setLeaveFromDate(null);
						leaveRequest.setLeaveToDate(null);
						return;
					} else if ((leaveBalanceDouble == null || diff >= appValue) && leaveRequest.getType() != null
							&& leaveRequest.getType().equals("Surrendered")) {
						AppUtil.addWarn(
								"Earned Leave claims eligible Number of days is not more than 15 days in a year");
						earnedLeaveFlag = true;
						leaveRequest.setLeaveFromDate(null);
						leaveRequest.setLeaveToDate(null);
						return;
					} else if (leaveRequest.getNoOfDaysRequested() > leaveBalanceDouble
							&& leaveRequest.getType() != null && leaveRequest.getType().equals("Availed")) {
						earnedLeaveFlag = true;
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
						leaveRequest.setLeaveFromDate(null);
						leaveRequest.setLeaveToDate(null);
						return;
					}
				} else {
					earnedLeaveFlag = false;
					log.info("leavebalance------------>" + leaveBalanceDouble);
					if (Objects.isNull(leaveRequest.getLeaveBalance())) {
						log.info(" Leave balance is null ");
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
						leaveRequest.setLeaveFromDate(null);
						leaveRequest.setLeaveToDate(null);
						return;
					} else if (leaveBalanceDouble == null || diff >= leaveBalanceDouble) {
						log.info(" Not eligible to take leave ");
						// errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
						AppUtil.addWarn("Leave apply days not greater than " + leaveBalanceDouble);
						leaveRequest.setNoOfDaysRequested(0.0);
						leaveRequest.setLeaveFromDate(null);
						leaveRequest.setLeaveToDate(null);
						return;
					}

					if (leaveRequest.getLeaveType() != null) {
						if (leaveRequest.getLeaveType().getCode() != null
								&& leaveRequest.getLeaveType().getCode().equals("MAT")) {
							earnedLeaveFlag = false;
							Double eligibleDays = Double
									.valueOf(commonDataService.getAppKeyValue("MATERNITY_LEAVE_DAYS"));
							eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
							if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
								AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
								leaveRequest.setLeaveFromDate(null);
								leaveRequest.setLeaveToDate(null);
								leaveRequest.setNoOfDaysRequested(0.0);
							}
						}
					}

					if (leaveRequest.getLeaveType().getCode().equals("ML")) {
						Double eligibleDays = Double.valueOf(MedicalLeaveConfig.TOTAL_LEAVE);
						eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
						if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
							AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
							leaveRequest.setLeaveToDate(null);
							leaveRequest.setNoOfDaysRequested(null);
							return;
						}
						if (leaveConsumedDouble != null) {
							if (leaveConsumedDouble >= eligibleDays) {
								errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
								leaveRequest.setNoOfDaysRequested(null);
								leaveRequest.setLeaveToDate(null);
								return;
							}

							Double consumedLeave = leaveConsumedDouble + diff;
							if (consumedLeave >= eligibleDays) {
								AppUtil.addWarn("Your total leave eligiblity is " + eligibleDays);
								leaveRequest.setLeaveToDate(null);
								leaveRequest.setNoOfDaysRequested(null);
								return;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(":: Exception while On leave type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

	}

	public byte[] downloadFile(String path) {
		byte[] downloadName = null;
		try {
			File file = new File(path);
			downloadName = new byte[(int) file.length()];

			log.info("downloadName==>" + downloadName);

			FileInputStream fis = new FileInputStream(file);
			fis.read(downloadName); // read file into bytes[]
			fis.close();
		} catch (Exception e) {
			log.error("convertImage error==>" + e);
		}
		return downloadName;
	}

	public void getDownloadFile(byte[] value) {
		try {
			log.info(" Value :: " + value);
			if (value == null || value == null) {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
				return;
			}
			log.info(" FIle Name :: " + leaveDocumentFileName);
			String ext = FilenameUtils.getExtension(leaveDocumentFileName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png") || ext.contains("doc")
					|| ext.contains("docx") || ext.contains("txt") || ext.contains("gif") || ext.contains("xls")
					|| ext.contains("xlsx") || ext.contains("PNG")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(value), "image/jpg", leaveDocumentFileName);
			} else if (ext.contains("pdf")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(value), "pdf", leaveDocumentFileName);
			}

			// return new DefaultStreamedContent(new ByteArrayInputStream(value),
			// "image/png","downloadimage");
			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded flie", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
	}

	public StreamedContent getFile() {
		return file;
	}

	public String approveorRejectLeaveRequest() {
		log.info(":: Leave request Approve or Reject ::");
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (Objects.isNull(leaveRequest)) {
				log.info(":: Leave request object is null ::" + leaveRequest);
				errorMap.notify();
				return null;
			}
			if (Objects.isNull(leaveRequest.getId())) {
				log.info(":: Leave request object is null ::" + leaveRequest);
				errorMap.notify();
				return null;
			}
			if (pageName.equalsIgnoreCase("approved") && leaveRequest.getStatus().equals(LeaveRequestStatus.APPROVED)) {
				log.info(":: Leave request object is already approved ::" + leaveRequest);
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			}

			if (pageName.equalsIgnoreCase("approved")) {
				leaveRequest.setStatus(LeaveRequestStatus.APPROVED);
				leaveRequest.setApprovedDate(new Date());
			} else if (pageName.equalsIgnoreCase("Reject")) {
				leaveRequest.setStatus(LeaveRequestStatus.REJECTED);
				leaveRequest.setApprovedDate(new Date());
			}
			log.info(" Leave Request Approve Status :: " + leaveRequest.getStatus());

			log.info(" Leave Request Approve Reason :: " + leaveRequest.getReasonForApproval());

			String resource = URI.concat("/approveorreject");
			log.info(":: Update leave request URI is ::" + resource);
			baseDTO = httpService.put(resource, leaveRequest);

			if (Objects.isNull(baseDTO)) {
				log.info(":: Leave request BaseDTO response is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				if (baseDTO.getStatusCode() == 0) {
					if (leaveRequest.getStatus().equals(LeaveRequestStatus.APPROVED)) {
						log.info(":: Leave request approved success response is ::" + baseDTO.getStatusCode());
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_APPROVED_SUCCESS.getErrorCode());
					} else if (leaveRequest.getStatus().equals(LeaveRequestStatus.REJECTED)) {
						log.info(":: Leave request rejected success response is ::" + baseDTO.getStatusCode());
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_REJECTED_SUCCESS.getErrorCode());
					}
				} else {
					log.info(":: Leave request Approve Or reject failed with status code ::" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}

			}
		} catch (Exception e) {
			log.error(":: Exception while save leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		selectedLeaveRequest = new LeaveRequest();
		viewAdd = true;
		return "/pages/leaveManagement/listLeaveRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void onRowSelect(SelectEvent event) {
		selectedLeaveRequest = ((LeaveRequest) event.getObject());
		log.info(":: Selected leave request status is ::" + selectedLeaveRequest.getStatus());
		viewEdit = false;
		viewDelete = false;
		cancelDropdown = true;
		if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.APPROVED)
				|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.FINAL_APPROVED)
				|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.INPROGRESS)) {
			viewAdd = false;
			viewEdit = false;
			viewApprove = false;
			if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.INPROGRESS)
					|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.APPROVED)
					|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.FINAL_APPROVED)
					|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.REJECTED)
					|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.LEAVE_CANCEL)
					|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.LEAVE_CANCEL)) {
				viewDelete = false;
			}
			viewCancel = true;
		} else if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.LEAVE_CANCEL)
				|| selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.CANCEL_APPROVED)) {
			viewAdd = false;
			viewEdit = false;
			viewApprove = false;
			viewCancel = false;
			viewDelete = false;
			viewCancel = true;
			if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.CANCEL_APPROVED))
				cancelDropdown = false;
		} else if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.CANCEL_FINAL_APPROVED)) {
			viewAdd = false;
			viewEdit = false;
			viewApprove = false;
			viewCancel = false;
			viewDelete = false;
			cancelDropdown = false;
			viewCancel = true;
		}
		if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.REJECTED)
				&& selectedLeaveRequest.getCreatedBy() != null && selectedLeaveRequest.getCreatedBy().getId() != null
				&& loginBean.getUserMaster().getId().equals(selectedLeaveRequest.getCreatedBy().getId())) {
			viewAdd = false;
			viewEdit = true;
			viewApprove = false;
			viewDelete = false;
			viewCancel = false;
		} else {
			if (selectedLeaveRequest.getStatus().equals(LeaveRequestStatus.REJECTED)) {
				viewAdd = false;
			}

		}

	}

	public void onLeaveCategoryChange() {
		log.info("Leave category change :" + leaveRequest.getLeaveCategory());
		if (leaveRequest.getLeaveCategory().equals(LeaveCategory.SURRENDERLEAVE)) {
			fromToDatePanel = false;
		} else {
			fromToDatePanel = true;
		}
		RequestContext.getCurrentInstance().update("leaveRequestForm:leaveRequestPanel");
	}

	public void confirmStatus() {
		if (leaveRequest == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public String submitApprovedStatus(String status) {
		log.info("<=========== Starts LeaveRequestBean submitApprovedStatus =========>" + status);
		log.info("<===== Leave Request Id ========>" + leaveRequest.getId());
		leaveRequestDTO.setLeaveRequest(new LeaveRequest());
		leaveRequestDTO.getLeaveRequest().setId(leaveRequest.getId());
		if (status.equalsIgnoreCase(CreditSalesDemandStatus.FINAL_APPROVED.toString())) {
			leaveRequestDTO.setStage(CreditSalesDemandStatus.FINAL_APPROVED.toString());
			leaveRequestDTO.setRemarks(finalApproveComments);
			leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.FINAL_APPROVED);
		} else {
			leaveRequestDTO.setStage(CreditSalesDemandStatus.APPROVED.toString());
			leaveRequestDTO.setRemarks(approveComments);
			leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.APPROVED);
		}
		// leaveRequestDTO.setUserMaster(leaveRequestNote.getUserMaster());
		leaveRequestDTO.setFinalApproval(leaveRequestNote.getFinalApproval());
		leaveRequestDTO.setNote(leaveRequestNote.getNote());
		log.info("submited info", leaveRequestDTO);
		try {
			String url = URI.concat("/approveorreject");
			BaseDTO baseDTO = httpService.post(url, leaveRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("<========== Leave Request Final Approved Successfully ==========>");
				// errorMap.notify(ErrorDescription.CIRCULAR_APPROVE_SUCCESS.getCode());
				Util.addInfo("Leave Request Final Approved Successfully");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=========== Ends LeaveRequestBean submitApprovedStatus =========>");
		return showPage(ActionTypeEnum.LIST);
	}

	public String submitRejectedStatus() {
		log.info("<=========== Starts LeaveRequestBean submitRejectedStatus =========>");
		log.info("<===== LeaveRequest Id ========>" + leaveRequest.getId());
		leaveRequestDTO.setLeaveRequest(new LeaveRequest());
		leaveRequestDTO.getLeaveRequest().setId(leaveRequest.getId());
		leaveRequestDTO.setStage(CreditSalesDemandStatus.REJECTED.toString());
		leaveRequestDTO.setUserMaster(leaveRequestNote.getUserMaster());
		leaveRequestDTO.setFinalApproval(leaveRequestNote.getFinalApproval());
		leaveRequestDTO.setRemarks(rejectComments);
		leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.REJECTED);
		log.info("submited info", leaveRequestDTO);
		try {
			String url = URI.concat("/approveorreject");
			BaseDTO baseDTO = httpService.post(url, leaveRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("<========== Leave Request Rejected Successfully ==========>");
				errorMap.notify(ErrorDescription.CIRCULAR_REJECTED_SUCCESS.getCode());

			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=========== Ends LeaveRequestBean submitRejectedStatus =========>");
		return showPage(ActionTypeEnum.LIST);
	}

	public String cancelRequestLeave() {
		log.info("<===============Starts LeaveRequestBean cancelRequestLeave====================>");
		try {
			BaseDTO baseDTO = new BaseDTO();

			if (selectedLeaveRequest.getLeaveToDate().after(new Date())
					|| selectedLeaveRequest.getLeaveToDate().equals(new Date())) {
				String url = URI + "/cancelLeaveRequest";
				leaveRequestDTO.setLeaveRequest(selectedLeaveRequest);
				/*
				 * if (previousApproval == false && leaveRequestDTO.getFinalApproval() == true)
				 * { log.error("<---- Approve Status change on Approved  ----->");
				 * leaveRequestDTO.setStage(LeaveRequestStatus.CANCEL_APPROVED.toString());
				 * leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.CANCEL_APPROVED); }
				 * else if (previousApproval == true && leaveRequestDTO.getFinalApproval() ==
				 * true) { log.error("<---- Approve Status change on Final Approved  ----->");
				 * leaveRequestDTO.setStage(LeaveRequestStatus.CANCEL_FINAL_APPROVED.toString())
				 * ; leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.
				 * CANCEL_FINAL_APPROVED); } else if(leaveRequest.getStatus()!=null &&
				 * (!leaveRequest.getStatus().toString().equalsIgnoreCase(LeaveRequestStatus.
				 * CANCEL_REQUEST.toString())) ||
				 * !leaveRequest.getStatus().toString().equalsIgnoreCase(LeaveRequestStatus.
				 * CANCEL_APPROVED.toString()) ||
				 * !leaveRequest.getStatus().toString().equalsIgnoreCase(LeaveRequestStatus.
				 * CANCEL_FINAL_APPROVED.toString())) {
				 * log.error("<---- Other Status change on cancel request  ----->");
				 * leaveRequestDTO.setStage(LeaveRequestStatus.CANCEL_REQUEST.toString());
				 * leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.CANCEL_REQUEST); }
				 * String leaveRequestStatus=selectedLeaveRequest.getStatus().toString();
				 * if(!leaveRequestStatus.equalsIgnoreCase("CANCEL_REQUEST") ||
				 * !leaveRequestStatus.equalsIgnoreCase("CANCEL_APPROVED") ||
				 * !leaveRequestStatus.equalsIgnoreCase("CANCEL_FINAL_APPROVED")) {
				 * leaveRequestDTO.setStage(LeaveRequestStatus.CANCEL_REQUEST.toString());
				 * leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.CANCEL_REQUEST); }
				 */
				leaveRequestDTO.setStage(LeaveRequestStatus.LEAVE_CANCEL.toString());
				leaveRequestDTO.setLeaveRequestStatus(LeaveRequestStatus.LEAVE_CANCEL);
				baseDTO = httpService.post(url, leaveRequestDTO);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("<========== Leave Request Cancel Successfully ==========>");
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_CANCEL_SUCCESSFULLY)
							.getErrorCode());

				}
			} else {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_DATE_ERROR).getErrorCode());
			}

		} catch (Exception ex) {
			log.error("error in cancelRequestLeave", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===============End LeaveRequestBean cancelRequestLeave====================>");
		return "/pages/leaveManagement/listLeaveRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void updateSession() {
		if (leaveRequest.getType().equalsIgnoreCase("FULL_DAY")) {
			leaveRequest.setTimeSession(null);
		}

	}

	public void checkEarnedLeaveEligiblitity() {
		log.info("=== Start LeaveRequestBean.checkEarnedLeaveEligiblitity====");
		try {
			String appValue = commonDataService.getAppKeyValue("EARNED_LEAVE_CLAIMS_DAYS");
			if (leaveRequest.getLeaveType() != null && leaveRequest.getLeaveType().getCode() != null
					&& leaveRequest.getLeaveType().getCode().equals("EL")
					&& leaveRequest.getType().equals("Surrendered")
					&& leaveRequest.getNoOfDaysRequested() > Integer.parseInt(appValue)) {
				log.info(" Not eligible to take leave ");
				AppUtil.addWarn("Earned Leave claims eligible Number of days is not more than 15 days in a year");
				earnedLeaveFlag = true;
				return;
			} else {
				earnedLeaveFlag = false;
			}

		} catch (Exception e) {
			log.info("=== Exception is LeaveRequestBean.checkEarnedLeaveEligiblitity====  " + e.getMessage());
		}
		log.info("=== End LeaveRequestBean.checkEarnedLeaveEligiblitity====");
	}

	@PostConstruct
	public String showViewPage() {
		log.info("Leave RequestBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedLeaveRequest = new LeaveRequest();
			actionType = ActionTypeEnum.VIEW;
			selectedLeaveRequest.setId(Long.parseLong(id));
			systemNotificationId = Long.parseLong(notificationId);
			showPage(ActionTypeEnum.VIEW);
		}
		log.info("Leave RequestBean showViewListPage() Ends");
		return "/pages/leaveManagement/viewLeaveRequestAdmin.xhtml?faces-redirect=true;";
	}

	private boolean checkAlreadyLoanPreclosured() {

		boolean isSurrenderLeave = false;
		try {
			String getUrl = URI + "/checksurrenderLeaveavilable";
			log.info("submitLoanPreclosure URL : " + getUrl);
			BaseDTO baseDTO = httpService.post(getUrl, leaveRequestDTO);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				isSurrenderLeave = mapper.readValue(jsonValue, Boolean.class);
			}

		} catch (Exception e) {
			log.info("Exception occured at checkAlreadyLoanPreclosured() ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return isSurrenderLeave;
	}

	public void addLeaveType(String leavetype) {
		leaveTypeMap = new LinkedHashMap<String, String>();
		leaveTypeMap.put("Full Day", "FULL_DAY");
		leaveTypeMap.put("Half Day", "HALF_DAY");
		if ("ML".equalsIgnoreCase(leavetype)) {
			leaveTypeMap.put("full Pay", "FULL_PAY");
			leaveTypeMap.put("Half Pay", "HALF_PAY");
		}
	}

}
