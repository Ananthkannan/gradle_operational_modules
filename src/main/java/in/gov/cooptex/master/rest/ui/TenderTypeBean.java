package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.TenderType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import in.gov.cooptex.weavers.model.WeaversBenefitScheme;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("tenderTypeBean")
public class TenderTypeBean {

	private final String CREATE_PAGE = "/pages/masters/createTenderType.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/masters/listTenderType.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/viewTenderType.xhtml?faces-redirect=true";

	private final String LIST_URL = "/tenderType/getalltendertypelistlazy";
	private final String CREATE_URL = "/tenderType/create";
	private final String DELETE_URL = "/tenderType";

	private String serverURL = null;

	@Autowired
	HttpService httpService;
	

	ObjectMapper mapper;

	String jsonResponse;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	private Boolean addButtonFlag = true;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	TenderType tenderType;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<TenderType> tenderTypeList = new ArrayList<TenderType>();

	@Getter
	@Setter
	private int tenderTypeListSize;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	LazyDataModel<TenderType> lazyTenderTypeList;

	@PostConstruct
	public void init() {
		log.info(" TenderTypeBean init --->" + serverURL);
		loadValues();
		addTenderType();
		mapper=new ObjectMapper();
		loadLazytenderTypeList();
		addButtonFlag = true;
		getTenderTypeView(tenderType);
	}

	public String showList() {
		log.info("<---------- Loading TenderTypeBean showList list page---------->");
		loadValues();
		loadLazytenderTypeList();
		return LIST_PAGE;
	}

	public String addTenderType() {
		log.info("<---- Start TenderTypeBean.addTenderType ---->");
		try {
			action = "ADD";
			tenderType = new TenderType();
			tenderType.setActiveStatus(true);

			log.info("<---- End TenderType.addTenderType ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	private void getAllTenderTypeList() {
		log.info("<<<< ----------Start TenderTypeBean-getAllTenderTypeList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.get(url);
			tenderType = new TenderType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderTypeList = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderTypeList != null) {
			tenderTypeListSize = tenderTypeList.size();
		} else {
			tenderTypeListSize = 0;
		}
		log.info("<<<< ----------End TenderTypeBean . getAllTenderTypeList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			tenderType = new TenderType();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to TenderType List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... TenderTypeBean submit begin .... "+action+" " + tenderType);
		try {
			
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + CREATE_URL, tenderType);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("TenderTypeBean saved successfully");
					if(action.equalsIgnoreCase("ADD"))
				    errorMap.notify(ErrorDescription.TENDER_TYPE_CREATED_SUCCESSFULLY.getCode());
					else
				    errorMap.notify(ErrorDescription.TENDER_TYPE_UPDATED_SUCCESSFULLY.getCode());
					showList();
					//errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					 return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating TenderType" + e);
		}
		log.info("...... TenderTypeBean submit ended ....");
		return LIST_PAGE;
	}

	public String editTenderType() {
		log.info("<<<<< -------Start editTenderType called ------->>>>>> ");
		action = "EDIT";

		if (tenderType == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}

		if (tenderType.getActiveStatus().equals("ACTIVE")) {
			tenderType.setActiveStatus(tenderType.getActiveStatus());
		} else {
			tenderType.setActiveStatus(tenderType.getActiveStatus());
		}
		log.info("<<<<< -------End editTenderType called ------->>>>>> ");
		getRecordById();
		return CREATE_PAGE;

	}

	public String deleteTenderType() {
		try {
			action = "Delete";
			if (tenderType == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteTenderTypeConfirm() {

		try {
			log.info("Delete confirmed for tenderType [" + tenderType.getName() + "]"+tenderType.getId());
			TenderType tenderTypeDelete = new TenderType();
			tenderTypeDelete.setId(tenderType.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + DELETE_URL;
			baseDTO = httpService.delete(url+"/delete/"+tenderType.getId());

			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("TenderType deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					tenderType = new TenderType();
					addButtonFlag=true;
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					//errorMap.notify(baseDTO.getStatusCode());
					 errorMap.notify(ErrorDescription.TENDER_TYPE_DELETED_SUCCESSFULLY.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start tenderTypeBean . clear ----->");
		loadLazytenderTypeList();
		tenderType = new TenderType();
		log.info("<---- End tenderTypeBean . clear ----->");
addButtonFlag=true;
		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewTenderType() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (tenderType == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		getRecordById();
		return VIEW_PAGE;

	}

	private void getTenderTypeView(TenderType tenderType) {
		log.info("<<<< ----------Start tenderTypeBean . getTenderTypeView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.post(url, tenderType);
			tenderType = new TenderType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderType = mapper.readValue(jsonResponse, new TypeReference<TenderType>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderType != null) {
			log.info("tenderType getTenderTypeView" + tenderType);
		} else {
			log.info("tenderType getTenderTypeView" + tenderType);
		}

		log.info("<<<< ----------End TenderTypeBean . getTenderTypeView ------- >>>>");
	}
	
	public void loadLazytenderTypeList() {
		log.info("<===== Starts tendertypeBean.loadLazytenderTypeList ======>");

		lazyTenderTypeList = new LazyDataModel<TenderType>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<TenderType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + LIST_URL;
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderTypeList = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("tenderTypeBean size---------------->" + lazyTenderTypeList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tenderTypeList;
			}

			@Override
			public Object getRowKey(TenderType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TenderType getRowData(String rowKey) {
				for (TenderType tenderType : tenderTypeList) {
					if (tenderType.getId().equals(Long.valueOf(rowKey))) {
						tenderType = tenderType;
						return tenderType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  tenderTypeBean.loadLazySalesTypeList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TenderTypeBean.onRowSelect ========>" + event);
		tenderType = ((TenderType) event.getObject());
		addButtonFlag = false;
		log.info("<===Ends TenderTypeBean.onRowSelect ========>" + tenderType.getId());
	}
	
	
	public void getRecordById() {
		String URL = serverURL + "/tenderType/" + tenderType.getId();
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				tenderType = mapper.readValue(jsonValue, TenderType.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}


}
