package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EcommRateMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.EcommRateConfigDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("ecommProductRateConfigBean")
@Scope("session")
public class EcommProductRateConfigBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 6526752054633305390L;

	private final String RATE_CONFIG_LIST_PAGE = "/pages/ecommerce/admin/listEcommerceRateConfig.xhtml?faces-redirect=true;";
	private final String RATE_CONFIG_CREATE_PAGE = "/pages/ecommerce/admin/createEcommerceRateConfig.xhtml?faces-redirect=true;";
	private final String BRATE_CONFIG_VIEW_PAGE = "/pages/ecommerce/admin/viewEcommerceRateConfig.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	private EcommRateConfigDTO productRateDetail = new EcommRateConfigDTO();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductGroupMaster selectedproductGroup = new ProductGroupMaster();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	boolean notEligibleFoSsave = true, disablemodifyTime = false, showCancelButton = true;

	@Getter
	@Setter
	LazyDataModel<EcommRateConfigDTO> productRateList;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private EcommRateConfigDTO selectedProductRateDetail = new EcommRateConfigDTO();

	@Getter
	@Setter
	Boolean addButtonFlag = false;
	
	@Getter
	@Setter
	EcommRateMaster ecommRateMaster = new EcommRateMaster();
	
	@Getter
	@Setter
	List<Double> unitRateList = new ArrayList<>();
	
	// This method is start point of execution
	public String showEcommProductRateConfigListPage() {
		log.info("<==== Starts EcommProductRateConfigBean.showEcommProductRateConfigListPage =====>");
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();

		loadLazyList();
		log.info("<==== Ends EcommProductRateConfigBean.showEcommProductRateConfigListPage =====>");
		return RATE_CONFIG_LIST_PAGE;
	}

	// This method is used for loading all values which need for transaction
	public void loadValue() {
		loadProductCategory();
	}

	// product category
	public void loadProductCategory() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
	    List<ProductCategory> newList = new ArrayList<ProductCategory>();
		EmployeeMaster loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
		boolean isAppConfigValue = false;
		final String APP_CONFIG_SERVER_URL = AppUtil.getPortalServerURL();
		try {
			BaseDTO baseDTO = null;
			String appValue = commonDataService.getAppKeyValueWithserverurl("SHOWROOM_TO_SHOWROOM_OUTWARD_ALLOWED_ENTITY", APP_CONFIG_SERVER_URL);
			int entityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getCode();
			if(null != appValue  && Integer.valueOf(appValue) == entityCode){
				isAppConfigValue = true;
			}else{
				isAppConfigValue = false;
			}
			String requestPath = SERVER_URL + "/ecommProductRateConfig/getProductCategory";
			baseDTO = httpService.get(requestPath);
			log.info("::: Retrieved productCategoryList :");
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
				if(isAppConfigValue){
					productCategoryList.removeIf(product -> product.getProductCatCode().equalsIgnoreCase("F"));
					productCategoryList.removeIf(product -> product.getProductCatCode().equalsIgnoreCase("G"));
				}
			} else {
				log.warn("productCategoryList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	// product Group ->
	public void getProductGroupList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		try {
			notEligibleFoSsave = true;
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/" + productRateDetail.getCategoryid();

			baseDTO = httpService.get(url);
			log.info("::: Retrieved productGroupMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
			} else {
				log.warn("productGroupMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productGroupMasterList load ", ee);
		}
	}

	// This method is used to auto complete product detail on ui
	public List<ProductVarietyMaster> productAutoComplete(String productCodeOrName) {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			notEligibleFoSsave = true;
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductVarietyMasterList/" + productRateDetail.getProductgroupid()
					+ "/" + productCodeOrName;
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
		return productVarietyMasterList;
	}
	
	public void setgroup() {
		notEligibleFoSsave = true;
		productRateDetail.setProductgroupid(0);
		if (selectedproductGroup != null) {
			productRateDetail.setProductgroupid(selectedproductGroup.getId());
		}
	}

	public void disableoperation() {
		notEligibleFoSsave = true;
	}

	public void calculateGrossAmount() {
		double gross = 0;
		double total = productRateDetail.getBank_charge()
				+ productRateDetail.getBank_charge_tax_value() + productRateDetail.getCourier_charge()
				+ productRateDetail.getCourier_tax_value()
				+ productRateDetail.getPhoto_charge()
				+ productRateDetail.getPhoto_tax_value();
		double thirtyPercentValue = (total/70)*100;
		productRateDetail.setThirtyPercentValue(thirtyPercentValue);
		gross = thirtyPercentValue + productRateDetail.getUnit_rate();

		productRateDetail.setGross_value(getRoundOffGrossvalue(gross));
		productRateDetail.setCourierChargeTotal(productRateDetail.getCourier_charge()+productRateDetail.getCourier_tax_value());
		productRateDetail.setPhotoChargeTotal(productRateDetail.getPhoto_charge()+productRateDetail.getPhoto_tax_value());
		productRateDetail.setBankChargeTotal(productRateDetail.getBank_charge() + productRateDetail.getBank_charge_tax_value());
		calculateExpenseMarginValue();
	}

	public void calculateExpenseMarginValue() {
		double diffValue = 0;
		double percentValue = 0;
		if (null != productRateDetail) {
			if (productRateDetail.getGross_value() > 0) {
				diffValue = productRateDetail.getGross_value() - productRateDetail.getUnit_rate();
				percentValue = (productRateDetail.getGross_value() / productRateDetail.getUnit_rate())-1;
				productRateDetail.setExpense_margin_value(diffValue);
				if(percentValue > 0){
				productRateDetail.setExpense_margin_percent(percentValue * 100);
				}else{
					productRateDetail.setExpense_margin_percent(0);
				}
			} else {
				productRateDetail.setExpense_margin_value(0);
				productRateDetail.setExpense_margin_percent(0);
			}
		}

	}

	private double getRoundOffGrossvalue(double gross) {
		double result = 0;
		int roundoffGrossValueInt = (int) gross;
		double roundoffGrossValue = gross - roundoffGrossValueInt;
		if (roundoffGrossValue > 0) {
			result = ++roundoffGrossValueInt;
		} else {
			result = roundoffGrossValueInt;
		}

		return result;

	}

	public void searchDuplicateProductRate() {
		try {
			
			productRateDetail.setCourier_charge(0);
			productRateDetail.setCourier_tax_percent(0);
			productRateDetail.setCourier_tax_value(0);
			productRateDetail.setInternational_courier_charge(0);
			productRateDetail.setPhoto_charge(0);
			productRateDetail.setPhoto_tax_percent(0);
			productRateDetail.setPhoto_tax_value(0);
			productRateDetail.setBank_charge(0);
			productRateDetail.setBank_charge_tax_percent(0);
			productRateDetail.setBank_charge_tax_value(0);
			productRateDetail.setOther_charges(0);
			productRateDetail.setGross_value(0);
			productRateDetail.setExpense_margin_percent(0);
			productRateDetail.setExpense_margin_percent(0);
			if (searchValidation()) {
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommProductRateConfig/searchDuplicateProductRate";
				productRateDetail.setItem_code(selectedProductVarietyMaster.getCode());
				baseDTO = httpService.post(requestPath, productRateDetail);
				log.info("::: Retrieved productCategoryList :");
				if (baseDTO != null && baseDTO.getMessage() != null && baseDTO.getMessage().equalsIgnoreCase("yes")) {
					notEligibleFoSsave = true;
					disablemodifyTime = false;
					AppUtil.addWarn(" Product Details already present ");
				} else {
					notEligibleFoSsave = false;
					disablemodifyTime = true;
					//fetching rate master list  
					loadRateMasterValues();
				}
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	private void loadRateMasterValues(){
		log.info("enters loadRateMasterValues()");
		BaseDTO baseDTO = new BaseDTO();
		ecommRateMaster = new EcommRateMaster();
		try {
			Long catId = productRateDetail.getCategoryid();
			double ecommRateFrom ;
			String catCode = null;
			if(null != catId){
			ProductCategory selectedCategoryCode =	productCategoryList.stream().filter(cat -> cat.getId() == catId).findAny().orElse(null);
		    if(null != selectedCategoryCode){
		    	 catCode = selectedCategoryCode.getProductCatCode();
		    }
		    ecommRateFrom =  productRateDetail.getUnit_rate();
		    if(null != catCode){
		    String requestPath = SERVER_URL + "/ecommProductRateConfig/getEcommRateMaster"
				+"/"+catCode+"/"+ecommRateFrom;
		   baseDTO = httpService.get(requestPath);
		   if (null != baseDTO && baseDTO.getStatusCode() == 0) {
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecommRateMaster = mapper.readValue(jsonResponse, new TypeReference<EcommRateMaster>() {
			});
		
		//loading values to productRateDetail
		productRateDetail.setCourier_charge(ecommRateMaster.getCourierCharge());
		productRateDetail.setCourier_tax_percent(ecommRateMaster.getCourierTaxPercent());
		double courierTaxVal = (ecommRateMaster.getCourierCharge() * ecommRateMaster.getCourierTaxPercent())/100;
		productRateDetail.setCourier_tax_value(courierTaxVal);
		productRateDetail.setPhoto_charge(ecommRateMaster.getPhotoCharge());
		productRateDetail.setPhoto_tax_percent(ecommRateMaster.getPhotoTaxPercent());
		double photoTaxValue = (ecommRateMaster.getPhotoCharge()*ecommRateMaster.getPhotoTaxPercent())/100;
		productRateDetail.setPhoto_tax_value(photoTaxValue);
		double bankCharge = (productRateDetail.getUnit_rate()*ecommRateMaster.getBankChargePercentage())/100;
		productRateDetail.setBank_charge(bankCharge);
		productRateDetail.setBank_charge_tax_percent(ecommRateMaster.getBankChargeTaxPercent());
		double bankTaxValue =(bankCharge * ecommRateMaster.getBankChargeTaxPercent())/100;
		productRateDetail.setBank_charge_tax_value(bankTaxValue);
		productRateDetail.setOther_charges(ecommRateMaster.getOtherCharges());
		calculateGrossAmount();
					}
		    	}
			}
		}catch (Exception ee) {
			log.error("Exception Occured in loadRateMasterValues() ", ee);
		}
	}
	
	public void checkMandatoryValidation(){
		if (productRateDetail.getUnit_rate() == 0) {
			 AppUtil.addWarn("Please enter unit rate");
			 return;
		} 
//		else if(productRateDetail.getCourier_charge() == 0){
//			AppUtil.addWarn("Please enter courier charge");
//			return; 
//		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('clearConfirmDialog').show();");
 
	}
	
	private boolean searchValidation() {
		boolean valid = true;
		if (productRateDetail.getCategoryid() == 0) {
			AppUtil.addWarn("Please Select a Category");
			return valid = false;
		} else if (productRateDetail.getProductgroupid() == 0) {
			AppUtil.addWarn("Please Select a Product Group");
			return valid = false;
		} else if (selectedProductVarietyMaster == null || selectedProductVarietyMaster.getCode() == null
				|| selectedProductVarietyMaster.getCode().length() == 0) {
			AppUtil.addWarn("Please Select a Product ");
			return valid = false;
		}
		if (productRateDetail.getUnit_rate() == 0) {
			AppUtil.addWarn("Please Enter Unite Rate");
			return valid = false;
		}
		if(productRateDetail.getUnit_rate() != 0){
			if(!priceRateInStock()){
				AppUtil.addWarn("Product is not available in stock");
				return valid = false;
			}
		}

		return valid;
		
	}
	
	private boolean priceRateInStock(){
		boolean isexist = false;
		BaseDTO baseDTO = null;
		EmployeeMaster loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
		Long entityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
		String requestPath = SERVER_URL + "/ecommProductRateConfig/validateUnitPriceInStock/"
				+entityId+"/"+productRateDetail.getUnit_rate()+"/"+selectedProductVarietyMaster.getId() ;
		System.out.println(entityId+"--"+productRateDetail.getUnit_rate()+"--"+selectedProductVarietyMaster.getId());
		 baseDTO = httpService.get(requestPath);
		if (baseDTO != null && baseDTO.getResponseContent() != null && ((String) baseDTO.getResponseContent()).equalsIgnoreCase("true")) {
			isexist = true;
		}else{
			isexist = false;
		}
		return isexist;
	}
	
	public void fetchUnitPriceNotInEcommRate(){
		boolean isexist = false;
		BaseDTO baseDTO = null;
		try{
		EmployeeMaster loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
		Long entityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
		String requestPath = SERVER_URL + "/ecommProductRateConfig/fetchUnitPriceNotInEcommRate/"
				+entityId+"/"+selectedProductVarietyMaster.getId()+"/"+selectedProductVarietyMaster.getCode();
		System.out.println("requestPath ==> "+requestPath);
		 baseDTO = httpService.get(requestPath);
		  if (null != baseDTO && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				List<StockTransferItems> sti = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferItems>>() {
				});
				if(null != sti && sti.size() > 0){
				 unitRateList = sti.stream().map(StockTransferItems::getUnitRate).distinct().sorted().collect(Collectors.toList());
				}
		  }
		}catch(Exception e){
			log.error("Exception Occured in fetchUnitPriceNotInEcommRate load ", e);
		}
	}

	public void clearHeader() {
		productRateDetail = new EcommRateConfigDTO();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		selectedproductGroup = new ProductGroupMaster();
		selectedProductRateDetail = new EcommRateConfigDTO();
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
	}

	private boolean saveValidation() {
		boolean valid = true;
		valid = searchValidation();
//		if (valid) {
//			if (productRateDetail.getCourier_charge() == 0) {
//				AppUtil.addWarn("Please Enter Courier_charge");
//				return valid = false;
//			}
//		}

		return valid;
	}

	public String saveRateConfig() {
		try {
			if (saveValidation()) {
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommProductRateConfig/saveRateConfig";
				baseDTO = httpService.post(requestPath, productRateDetail);
				log.info("::: Retrieved productCategoryList :");
				if (baseDTO != null && baseDTO.getMessage() != null && baseDTO.getMessage().equalsIgnoreCase("yes")) {
					AppUtil.addInfo("Product details saved Successfully");
					loadLazyList();
					cancel();
					return RATE_CONFIG_LIST_PAGE;
				} else {
					AppUtil.addWarn("Product Rate Detailis not saved");
				}
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
		return null;
	}

	public String cancel() {
		productRateDetail = new EcommRateConfigDTO();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		selectedproductGroup = new ProductGroupMaster();
		selectedProductRateDetail = new EcommRateConfigDTO();
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		addButtonFlag = false;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		return RATE_CONFIG_LIST_PAGE;
	}

	public String addRateConfigDetail() {
		action = "ADD";
		productRateDetail = new EcommRateConfigDTO();
		productRateDetail.setStatus(true);
		selectedProductVarietyMaster = new ProductVarietyMaster();
		selectedproductGroup = new ProductGroupMaster();
		selectedProductRateDetail = new EcommRateConfigDTO();
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		loadValue();
		return RATE_CONFIG_CREATE_PAGE;
	}

	public String editRateConfigDetail() {
		if (selectedProductRateDetail != null) {
			try {
				productRateDetail = new EcommRateConfigDTO();
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommProductRateConfig/getProductRateconfihDetail";
				baseDTO = httpService.post(requestPath, selectedProductRateDetail);
				log.info("::: Retrieved productCategoryList :");

				if (baseDTO != null && baseDTO.getResponseContent() != null) {

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					EcommRateConfigDTO data = mapper.readValue(jsonResponse, new TypeReference<EcommRateConfigDTO>() {
					});

					if (data != null) {
						productRateDetail = data;
						selectedProductVarietyMaster = productRateDetail.getProduct();
						selectedproductGroup = productRateDetail.getGroup();
					    loadTempValues();
						loadProductCategory();
						getProductGroupList();
					}
					notEligibleFoSsave = false;
					disablemodifyTime = true;
					showCancelButton = false;
					return RATE_CONFIG_CREATE_PAGE;
				}

			} catch (Exception ee) {
				log.error("Exception Occured in productCategoryList load ", ee);
			}
		}
		return null;
	}

	public String viewRateConfigDetail() {
		if (selectedProductRateDetail != null) {
			try {
				productRateDetail = new EcommRateConfigDTO();
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommProductRateConfig/getProductRateconfihDetail";
				baseDTO = httpService.post(requestPath, selectedProductRateDetail);
				log.info("::: Retrieved productCategoryList :");

				if (baseDTO != null && baseDTO.getResponseContent() != null) {

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					EcommRateConfigDTO data = mapper.readValue(jsonResponse, new TypeReference<EcommRateConfigDTO>() {
					});

					if (data != null) {
						productRateDetail = data;
						selectedProductVarietyMaster = productRateDetail.getProduct();
						selectedproductGroup = productRateDetail.getGroup();
						loadTempValues();
						loadProductCategory();
						getProductGroupList();
					}
					notEligibleFoSsave = false;
					disablemodifyTime = true;
					showCancelButton = false;
					return BRATE_CONFIG_VIEW_PAGE;
				}

			} catch (Exception ee) {
				log.error("Exception Occured in productCategoryList load ", ee);
			}
		}
		return null;
	}
	
	private void loadTempValues(){
		productRateDetail.setCourierChargeTotal(productRateDetail.getCourier_charge()+productRateDetail.getCourier_tax_value());
		productRateDetail.setPhotoChargeTotal(productRateDetail.getPhoto_charge()+productRateDetail.getPhoto_tax_value());
		productRateDetail.setBankChargeTotal(productRateDetail.getBank_charge() + productRateDetail.getBank_charge_tax_value());
		double total = productRateDetail.getBank_charge()
				+ productRateDetail.getBank_charge_tax_value() + productRateDetail.getCourier_charge()
				+ productRateDetail.getCourier_tax_value()
				+ productRateDetail.getPhoto_charge() 
				+ productRateDetail.getPhoto_tax_value();
		double thirtyPercentValue = (total/70)*100;
		productRateDetail.setThirtyPercentValue(thirtyPercentValue);
	}

	public void loadLazyList() {
		log.info("< --  Start Employee Lazy Load -- > ");
		productRateList = new LazyDataModel<EcommRateConfigDTO>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<EcommRateConfigDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<EcommRateConfigDTO> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EcommRateConfigDTO>>() {
							});
					if (data == null) {
						log.info(" HelpdeskTicket search response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Ticket search Successfully Completed");
					} else {
						log.error(" Ticket search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading roster data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EcommRateConfigDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EcommRateConfigDTO getRowData(String rowKey) {
				List<EcommRateConfigDTO> responseList = (List<EcommRateConfigDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				if (responseList != null && responseList.size() > 0) {
					for (EcommRateConfigDTO res : responseList) {
						if (res.getId() == value.longValue()) {
							selectedProductRateDetail = res;
							return res;
						}
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			EcommRateConfigDTO request = new EcommRateConfigDTO();
			request.setFirst(first);
			request.setPageSize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("item_code")) {
					log.info("item_code : " + value);
					request.setItem_code(value.toString());
				}
				if (entry.getKey().contains("unit_rate")) {
					log.info("unit_rate : " + value);
					request.setUnit_rate(Double.parseDouble(entry.getValue().toString()));

				}
				if (entry.getKey().contains("gross_value")) {
					log.info("gross_value : " + value);
					request.setGross_value(Double.parseDouble(entry.getValue().toString()));

				}

				if (entry.getKey().contains("activeStatus")) {
					log.info("status : " + value);
					request.setStatus_str(value.toString());
				}
			}

			log.info("Search request data" + request);

			String url = SERVER_URL + "/ecommProductRateConfig/getalltProductRate";
			baseDTO = httpService.post(url, request);

			log.info("helpdeskTicketLazyList" + productRateList);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Helpdesk Ticket search generate data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void calculateCourierTaxValue() {
		if (productRateDetail.getCourier_charge() > 0) {
			double mainvalue = productRateDetail.getCourier_charge() / 100;
			double tax = 0;
			if (productRateDetail.getCourier_tax_percent() > 0) {
				tax = mainvalue * productRateDetail.getCourier_tax_percent();
				productRateDetail.setCourier_tax_value(tax);
			}else{
				productRateDetail.setCourier_tax_value(tax);
			}
		}
		calculateGrossAmount();
	}

	public void calculatePhotoTaxValue() {
		if (productRateDetail.getPhoto_charge() > 0) {
			double mainvalue = productRateDetail.getPhoto_charge() / 100;
			double tax = 0;
			if (productRateDetail.getPhoto_tax_percent() > 0) {
				tax = mainvalue * productRateDetail.getPhoto_tax_percent();
				productRateDetail.setPhoto_tax_value(tax);
			}else{
				productRateDetail.setPhoto_tax_value(tax);
			}
		}
		calculateGrossAmount();
	}

	public void calculateBankTaxValue() {
		if (productRateDetail.getBank_charge() > 0) {
			double mainvalue = productRateDetail.getBank_charge() / 100;
			double tax = 0;
			if (productRateDetail.getBank_charge_tax_percent() > 0) {
				tax = mainvalue * productRateDetail.getBank_charge_tax_percent();
				productRateDetail.setBank_charge_tax_value(tax);
			}else{
				productRateDetail.setBank_charge_tax_value(tax);
			}
		}
		calculateGrossAmount();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts onRowSelect() ========>" + event);
		addButtonFlag = true;
		log.info("<===Ends onRowSelect() ========>");
	}

}
