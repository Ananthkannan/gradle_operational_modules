package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.training.enums.JobTypeEnum;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.model.RosterReservation;
import in.gov.cooptex.core.model.RosterReservationDetails;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.operation.rest.util.FormValidation;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("jobAdvertisementBean")
public class JobAdvertisementBean {

	@Getter
	@Setter
	String action;

	boolean isValid;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	List<PayScaleMaster> payscaleList;

	@Getter
	@Setter
	List<PaymentMode> paymentModeList;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<Department> departments;

	@Getter
	@Setter
	List<Designation> designations;

	@Getter
	@Setter
	RosterReservation rosterReservation, selectedRosterReservation;

	@Getter
	@Setter
	List<QualificationMaster> qualificationList;

	@Getter
	@Setter
	List<JobAdvertisement> jobAdvertisementList;

	@Getter
	@Setter
	JobAdvertisement jobAdvertisement, selectedJobAdvertisement;

	@Autowired
	RosterReservationBean rosterReservationBean;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	// Print numbers from 18 to 38
	@Getter
	@Setter
	List<Integer> minScAge = IntStream.rangeClosed(18, 38).boxed().collect(Collectors.toList());

	@Getter
	@Setter
	List<Integer> minOcAge = IntStream.rangeClosed(18, 38).boxed().collect(Collectors.toList());
	// Print numbers from 38 to 58
	@Getter
	@Setter
	Set<Integer> maxScAge = IntStream.rangeClosed(39, 58).boxed().collect(Collectors.toSet());

	@Getter
	@Setter
	Set<Integer> maxOcAge = IntStream.rangeClosed(39, 58).boxed().collect(Collectors.toSet());

	@Getter
	@Setter
	Set<Integer> years = IntStream.rangeClosed(0, 30).boxed().collect(Collectors.toSet());

	@Getter
	@Setter
	Set<Integer> months = IntStream.rangeClosed(0, 11).boxed().collect(Collectors.toSet());

	@Getter
	@Setter
	List<String> paymentList;

	@Getter
	@Setter
	private LazyDataModel<JobAdvertisement> lazyModel;

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/jobadvertisement";

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean approveButtonFlag, deleteFlag, editButtonFlag, viewButtonFlag;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean dropdownFlag = true;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<String> jobTypeList = new ArrayList<>();

	@Getter
	@Setter
	private Boolean scOrstOrscaFlage = false;

	@Getter
	@Setter
	private Boolean bcOrmbcOrbcmFlag = false;

	@Getter
	@Setter
	private Boolean ocFlag = false;

	/**
	 * Returns xhtml page based on condition like Approve, Add, Edit, View, Delete,
	 * Cancel, List
	 * 
	 * @return
	 */
	public String showPage() {
		log.info(":: Page Navigation showPage() :: " + action);
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loadJobType();
		if (action.equalsIgnoreCase("APPROVE")) {
			if (selectedJobAdvertisement == null) {
				errorMap.notify(ErrorDescription.SELECT_JOB_ADVERTISEMENT_ID.getErrorCode());
				return null;
			}
			if (selectedJobAdvertisement.getAdvertisementStatus()
					.equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			}
			if (selectedJobAdvertisement.getAdvertisementStatus()
					.equalsIgnoreCase(ApprovalStatus.REJECTED.toString())) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			}
			jobAdvertisement = advertisementById();
			RosterReservation rosterDetails = loadRosterReservation();
			jobAdvertisement.setRosterReservation(rosterDetails);
			jobAdvertisement.setPaymentList(Arrays.asList(jobAdvertisement.getPaymentMode().split(",")));
			return "/pages/personnelHR/approvalJobAdvertisment.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("ADD")) {
			jobAdvertisement = new JobAdvertisement();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());

			if (rosterReservationBean.getSelectedRosterReservation() == null) {
				errorMap.notify(ErrorDescription.SELECT_JOB_ADVERTISEMENT_ID.getErrorCode());
				return null;
			}
			RosterReservation rosterDetails = loadRosterReservation();
			if (rosterDetails.getStatus().equals(ApprovalStatus.INPROGRESS.toString())) {
				errorMap.notify(ErrorDescription.INPROGRESS_ROSTER.getErrorCode());
				return null;
			}
			if (rosterDetails.getStatus().equals(ApprovalStatus.REJECTED.toString())) {
				errorMap.notify(ErrorDescription.REJECTED_ROSTER.getErrorCode());
				return null;
			}
			jobAdvertisement.setRosterReservation(rosterDetails);
			List<String> castCodeList = new ArrayList<String>();
			if (rosterDetails != null && rosterDetails.getRosterReservationList() != null) {
				for (RosterReservationDetails rosterReservationDetails : rosterDetails.getRosterReservationList()) {
					castCodeList.add(rosterReservationDetails.getCasteCode().trim());
				}
			}
			if (castCodeList.contains("SC") || castCodeList.contains("ST") || castCodeList.contains("SCA")) {
				scOrstOrscaFlage = true;
			}
			if (castCodeList.contains("BC") || castCodeList.contains("MBC") || castCodeList.contains("BCM")) {
				bcOrmbcOrbcmFlag = true;
			}
			if (castCodeList.contains("OC")) {
				ocFlag = true;
			}
			onload();
			return "/pages/personnelHR/createJobAdvertisment.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("EDIT")) {
			log.info(":: Job Advertisement Edit Response :: " + selectedJobAdvertisement);
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());

			if (selectedJobAdvertisement == null) {
				errorMap.notify(ErrorDescription.SELECT_JOB_ADVERTISEMENT_ID.getErrorCode());
				return null;
			}
			if (selectedJobAdvertisement.getAdvertisementStatus()
					.equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			}
			/*
			 * if (selectedJobAdvertisement.getAdvertisementStatus().equalsIgnoreCase(
			 * ApprovalStatus.REJECTED.toString())) {
			 * errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode()); return
			 * null; }
			 */
			jobAdvertisement = advertisementById();
			onScAgeChange();
			onOcAgeChange();
			RosterReservation rosterDetails = loadRosterReservation();
			jobAdvertisement.setRosterReservation(rosterDetails);
			List<String> paymentList = new ArrayList<String>();
			for (String data : jobAdvertisement.getPaymentMode().split(",")) {
				paymentList.add(data);
			}
			jobAdvertisement.setPaymentList(paymentList);
			onload();
			return "/pages/personnelHR/createJobAdvertisment.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("VIEW")) {

			log.info(":: Job Advertisement View Response :: " + selectedJobAdvertisement);
			if (selectedJobAdvertisement == null) {
				errorMap.notify(ErrorDescription.SELECT_JOB_ADVERTISEMENT_ID.getErrorCode());
				return null;
			}
			jobAdvertisement = advertisementById();
			RosterReservation rosterDetails = loadRosterReservation();
			jobAdvertisement.setRosterReservation(rosterDetails);
			List<String> paymentList = Arrays.asList(jobAdvertisement.getPaymentMode().split(","));
			jobAdvertisement.setPaymentList(paymentList);

			if (selectedJobAdvertisement != null) {
				setPreviousApproval(selectedJobAdvertisement.getFinalApprove());
				if ("VIEW".equalsIgnoreCase(action)) {
					if (selectedJobAdvertisement.getForwardToId() != null) {
						if (loginBean.getUserMaster().getId().equals(selectedJobAdvertisement.getForwardToId())) {
							if (ApprovalStatus.REJECTED.equals(selectedJobAdvertisement.getStatus())) {
								approvalFlag = false;
							} else {
								approvalFlag = true;
							}

							if (selectedJobAdvertisement.getFinalApprove()) {
							}

						} else {
							approvalFlag = false;
						}
					}
					setFinalApproveFlag(selectedJobAdvertisement.getFinalApprove());
					if (selectedJobAdvertisement.getFinalApprove()
							&& loginBean.getUserMaster().getId().equals(selectedJobAdvertisement.getForwardToId())
							&& !selectedJobAdvertisement.getStatus().equals(ApprovalStatus.FINAL_APPROVED.toString())) {

						buttonFlag = true;

						return "/pages/personnelHR/approvalJobAdvertisment.xhtml?faces-redirect=true";
					}
					selectedJobAdvertisement.setApproveremarks(null);
				}
			}

			return "/pages/personnelHR/viewJobAdvertisement.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("DELETE")) {
			log.info(":: Job Advertisement Delete Response :: " + selectedJobAdvertisement);
			if (selectedJobAdvertisement == null) {
				errorMap.notify(ErrorDescription.SELECT_JOB_ADVERTISEMENT_ID.getErrorCode());
				return null;
			} else if (selectedJobAdvertisement.getAdvertisementStatus()
					.equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
			return "";
		} else if (action.equalsIgnoreCase("JOBLIST")) {
			departments = commonDataService.getDepartment();
			designations = commonDataService.loadDesignation();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
			load();
			deleteFlag = true;
			approveButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = true;
			return "/pages/personnelHR/listJobAdvertisementEmploymentNotification.xhtml?faces-redirect=true";
		} else {
			jobAdvertisement = new JobAdvertisement();
			selectedJobAdvertisement = new JobAdvertisement();
			departments = commonDataService.getDepartment();
			designations = commonDataService.loadDesignation();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());

			load();
			deleteFlag = true;
			approveButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = true;
			return "/pages/personnelHR/listJobAdvertisementEmploymentNotification.xhtml?faces-redirect=true";
		}
	}

	public void loadJobType() {
		log.info("<=== Starts employeeMaster.loadStatus() ========>");
		jobTypeList = new ArrayList<>();
		Object[] statusArray = JobTypeEnum.class.getEnumConstants();
		for (Object status : statusArray) {
			String jobValue = status.toString();
			jobTypeList.add(jobValue);
		}
		log.info("<=== Ends JobAdvertisementBean.loadJobType() jobTypeList : " + jobTypeList);
	}

	/**
	 * Returns List Page.
	 * 
	 * This method creates Job Advertisement Details.
	 * 
	 * @return
	 */
	public String saveUpdateJobAdvertisement() {
		log.info(":: Save Job Advertisement ::" + jobAdvertisement);
		try {
			if (jobAdvertisement == null) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (jobAdvertisement.getOfflineStartDate() == null) {
				log.info("====OfflineStartDate is Empty====");
				Util.addWarn("Offline Application Start Date is Required");
				return null;
			}
			if (jobAdvertisement.getOfflineEndDate() == null) {
				log.info("====OfflineEndDate is Empty====");
				Util.addWarn("Offline Application End Date is Required");
				return null;
			}
			if (jobAdvertisement.getJobType() == null) {
				log.info("====Job Type is Empty====");
				Util.addWarn("Job Type is Required");
				return null;
			}

			if (jobAdvertisement.getExpInYears() == null) {
				log.info("====Experience in Year is Empty====");
				Util.addWarn("Experience Year is Required");
				return null;
			}

			if (jobAdvertisement.getExpInMonths() == null) {
				log.info("====Experience in Month is Empty====");
				Util.addWarn("Experience Month is Required");
				return null;
			}

			if (jobAdvertisement.getJobDescription() == null || jobAdvertisement.getJobDescription().equals("")) {
				log.info("====Job Description is Empty====");
				Util.addWarn("Job Description is Required");
				return null;
			}
			/*
			 * if(jobAdvertisement.getJobResponsibility()==null ||
			 * jobAdvertisement.getJobResponsibility().equals("")) {
			 * log.info("====Job Responsibility is Empty====");
			 * Util.addWarn("Job Responsibility is Required"); return null; }
			 */

			isValid = FormValidation.validateTwoDates(jobAdvertisement.getOfflineStartDate(),
					jobAdvertisement.getOfflineEndDate());
			if (!isValid) {
				errorMap.notify(ErrorDescription.JOB_ADV_OFFLINE_START_DATE_PAST.getErrorCode());
				return null;
			}
			isValid = FormValidation.validateTwoDates(jobAdvertisement.getOnlineStartDate(),
					jobAdvertisement.getOnlineEndDate());
			if (!isValid) {
				errorMap.notify(ErrorDescription.JOB_ADV_ONLINE_START_DATE_PAST.getErrorCode());
				return null;
			}
			String url = null;
			BaseDTO baseDto = null;
			if (jobAdvertisement.getId() != null) {
				jobAdvertisement.setAdvertisementStatus(ApprovalStatus.INPROGRESS.toString());
				url = AppUtil.getPortalServerURL() + "/jobadvertisement/update";
				log.info(":: Jobj Advertisement Update URL :: " + url);
				baseDto = httpService.put(url, jobAdvertisement);
				log.info(":: Jobj Advertisement Update BaseDTO Response :: " + baseDto);
			} else {
				selectedRosterReservation = rosterReservationBean.getSelectedRosterReservation();
				jobAdvertisement.setRosterReservation(selectedRosterReservation);
				url = AppUtil.getPortalServerURL() + "/jobadvertisement/create";
				log.info(":: Jobj Advertisement Create URL :: " + url);
				baseDto = httpService.post(url, jobAdvertisement);
				log.info(":: Jobj Advertisement Create BaseDTO Response :: " + baseDto);
			}
			if (baseDto == null) {
				log.info(":: Jobj Advertisement BaseDTO Response Null:: " + baseDto); 
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDto.getStatusCode() == 22001 || baseDto.getStatusCode() == 22002) {
					errorMap.notify(baseDto.getStatusCode());
				} else {
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(":: Exception Occuer while Saving Job Advertisement ::", e);
		}
		load();
		selectedJobAdvertisement = new JobAdvertisement();
		return "/pages/personnelHR/listJobAdvertisementEmploymentNotification.xhtml?faces-redirect=true";
	}

	/**
	 * Returns List Page.
	 * 
	 * This method delete Job Advertisement record based on id.
	 * 
	 * @return
	 */
	public String deleteRecord() {
		log.info(":: Delete Job Advertisement By Id ::" + selectedJobAdvertisement.getId());
		try {
			if (selectedJobAdvertisement.getId() == null) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			String url = AppUtil.getPortalServerURL() + "/jobadvertisement/deletebyid/"
					+ selectedJobAdvertisement.getId();
			log.info(":: Jobj Advertisement Delete By Id URL :: " + url);
			BaseDTO baseDto = httpService.delete(url);
			log.info(":: Jobj Advertisement Delete By Id BaseDTO Response :: " + baseDto);
			if (baseDto == null) {
				log.info(":: Jobj Advertisement Delete BaseDTO Response Null:: " + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			} else {
				if (baseDto.getStatusCode() == 0) {
					log.info(":: Jobj Advertisement Deleted Successfully :: " + baseDto);
					errorMap.notify(ErrorDescription.JOB_ADVERTISEMENT_DELETED.getErrorCode());
				} else {
					log.info(":: Jobj Advertisement Delete By Id Failuer Response :: " + baseDto);
					errorMap.notify(baseDto.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error(":: Exception Occuer while Deleting Job Advertisement ::", e);
		}
		selectedJobAdvertisement = new JobAdvertisement();
		load();
		return "/pages/personnelHR/listJobAdvertisementEmploymentNotification.xhtml?faces-redirect=true";
	}

	/**
	 * Get the Qualification, Payscale & Pamyment List on page load.
	 */
	public void onload() {
		log.info(":: Load Comobo list while onPageLoad ::");
		try {
			qualificationList = commonDataService.loadQualifications();
			payscaleList = commonDataService.loadPayscale();
			paymentModeList = commonDataService.loadPaymentMode();
		} catch (Exception e) {
			log.info("::Error :: ", e);
		}
	}

	public void confirmStatus() {
		if (jobAdvertisement == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	/**
	 * Approve and reject the status of generated job advertisement.
	 * 
	 * @return
	 */
	public String approveJobAdvertisement() {
		try {
			if (jobAdvertisement == null) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info(":: Jobj Advertisement Approve  :: " + jobAdvertisement);
			String url = SERVER_URL + "/approverejectstatus";
			log.info(":: Jobj Advertisement Approve URL :: " + url);
			BaseDTO baseDto = httpService.put(url, jobAdvertisement);
			if (baseDto == null) {
				log.info(":: Job Advertisement Approved BaseDTO Response Null:: " + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				log.info(":: Jobj Advertisement Approved Successfully :: " + baseDto);
				if (baseDto.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.JOB_ADVERTISEMENT_APPROVED.getErrorCode());
				} else {
					log.info(":: Jobj Advertisement Approve Failuer Response :: " + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while Approved....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		selectedJobAdvertisement = new JobAdvertisement();
		return "/pages/personnelHR/listJobAdvertisementEmploymentNotification.xhtml?faces-redirect=true";
	}

	/**
	 * Load roster reservation details, while generating job advertisement.
	 * 
	 * @return
	 */
	public RosterReservation loadRosterReservation() {
		try {
			Long rosterId = null;
			if (jobAdvertisement.getRosterReservation() != null) {
				rosterId = jobAdvertisement.getRosterReservation().getId();
			}
			if (rosterReservationBean.getSelectedRosterReservation() != null) {
				rosterId = rosterReservationBean.getSelectedRosterReservation().getId();
			}
			log.info(":: Roster Reservation Details Id ::" + rosterId);
			String url = AppUtil.getPortalServerURL() + "/rosterreservation/get/" + rosterId + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			log.info(":: Roster Reservation URL ::" + url);
			BaseDTO response = httpService.get(url);
			log.info(":: Roster Reservation Details BaseDTO ::" + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			rosterReservation = mapper.readValue(jsonResponse, new TypeReference<RosterReservation>() {
			});
			long sum = rosterReservation.getRosterReservationList().stream().filter(o -> o.getTotalCount() >= 0)
					.mapToLong(o -> o.getTotalCount()).sum();
			rosterReservation.setTotalCount(sum);
			if (rosterReservation.getStatus() != null
					&& rosterReservation.getStatus().equals(ApprovalStatus.INPROGRESS.toString())) {
				String diffDays = Util.diffDaysBetweenTwoDates(rosterReservation.getCreatedDate(), new Date());
				rosterReservation.setNoOfDaysForApprove(diffDays);
			}
			if (rosterReservation == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info(":: Load Roster Reservation Deatails Failuer Response :: ", e);
		}
		return rosterReservation;
	}

	public void load() {
		dropdownFlag = true;
		lazyModel = new LazyDataModel<JobAdvertisement>() {

			private static final long serialVersionUID = 1L;

			List<JobAdvertisement> data = new ArrayList<>();

			@Override
			public List<JobAdvertisement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					JobAdvertisement request = new JobAdvertisement();
					request.setFirst(first / pageSize);
					request.setPagesize(pageSize);
					request.setSortField(sortField);
					request.setSortOrder(sortOrder.toString());
					request.setFilters(filters);

					log.info("::Job Advertisement Search request data ::" + request);

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/loadlazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					data = mapper.readValue(jsonResponse, new TypeReference<List<JobAdvertisement>>() {
					});
					if (data == null) {
						log.info(" :: Job Advertisement Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: Job Advertisement Search Successfully Completed ::");
					} else {
						log.error(":: Job Advertisement Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					// return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return data;
			}

			@Override
			public Object getRowKey(JobAdvertisement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public JobAdvertisement getRowData(String rowKey) {
				for (JobAdvertisement job : data) {
					if (job.getId().equals(Long.valueOf(rowKey))) {
						selectedJobAdvertisement = job;
						return job;
					}
				}
				return null;
			}
		};
	}

	public JobAdvertisement advertisementById() {
		JobAdvertisement jobDetail = null;
		try {
			if (selectedJobAdvertisement == null) {
				log.error(":: Job Advertisement Details By Id not found ::");
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
			Long jobId = selectedJobAdvertisement.getId();
			log.info(":: Job Advertisement Details Id ::" + jobId);
			String url = SERVER_URL + "/findbyid/" + jobId;
			log.info(":: Job Advertisement By Id URL ::" + url);
			BaseDTO response = httpService.get(url);
			log.info(":: Job Advertisement By Id BaseDTO ::" + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			jobDetail = mapper.readValue(jsonResponse, JobAdvertisement.class);
			if (jobDetail == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info(":: Job Advertisement By Id Failuer Response :: ", e);
		}
		return jobDetail;
	}

	public void onRowSelect(SelectEvent event) {
		selectedJobAdvertisement = ((JobAdvertisement) event.getObject());

		if (selectedJobAdvertisement.getAdvertisementStatus()
				.equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
			deleteFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
		} else if (selectedJobAdvertisement.getAdvertisementStatus()
				.equalsIgnoreCase(ApprovalStatus.REJECTED.toString())) {
			approveButtonFlag = false;
			deleteFlag = true;
			editButtonFlag = true;
		} else if (selectedJobAdvertisement.getAdvertisementStatus()
				.equalsIgnoreCase(ApprovalStatus.INPROGRESS.toString())) {
			editButtonFlag = true;
			approveButtonFlag = true;
			deleteFlag = true;
			viewButtonFlag = true;
		}
	}

	public void onScAgeChange() {
		if (jobAdvertisement.getMinAgeForSc() != null) {
			log.info(":: On Age Change ::" + jobAdvertisement.getMinAgeForSc());
			maxScAge = IntStream.rangeClosed(jobAdvertisement.getMinAgeForSc() + 1, 58).boxed()
					.collect(Collectors.toSet());
		}

	}

	public void onOcAgeChange() {

		if (jobAdvertisement.getMinAgeForOc() != null) {
			log.info(":: On Age Change ::" + jobAdvertisement.getMinAgeForOc());
			maxOcAge = IntStream.rangeClosed(jobAdvertisement.getMinAgeForOc() + 1, 58).boxed()
					.collect(Collectors.toSet());
		}

	}

	public void experiance() {

	}

	public String approveFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info(" approveFLow JobAdvertisement id----------" + selectedJobAdvertisement.getId());
			baseDTO = new BaseDTO();
			if (previousApproval == false && selectedJobAdvertisement.getFinalApprove() == true) {
				selectedJobAdvertisement.setStatus(ApprovalStatus.APPROVED.toString());
			} else if (previousApproval == true && selectedJobAdvertisement.getFinalApprove() == true) {
				selectedJobAdvertisement.setStatus(ApprovalStatus.FINAL_APPROVED.toString());
			} else {
				selectedJobAdvertisement.setStatus(ApprovalStatus.APPROVED.toString());
			}
			log.info("approve remarks=========>" + selectedJobAdvertisement.getApproveremarks());
			String url = SERVER_URL + "/jobadvertisement/approveFlow";
			baseDTO = httpService.post(url, selectedJobAdvertisement);
			if (baseDTO.getStatusCode() == 0) {
				Util.addWarn("JobAdvertisement Approved Successfully");
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				action = "JOBLIST";
				return showPage();
			}
		} catch (Exception e) {
			log.error("approveFLow method inside exception-------", e);
		}
		return null;
	}

	public String rejectFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info("approveFLow JobAdvertisement id----------" + selectedJobAdvertisement.getId());
			baseDTO = new BaseDTO();
			selectedJobAdvertisement.setStatus(ApprovalStatus.REJECTED.toString());
			String url = SERVER_URL + "/jobadvertisement/rejectFlow";
			baseDTO = httpService.post(url, selectedJobAdvertisement);
			if (baseDTO.getStatusCode() == 0) {
				Util.addWarn("JobAdvertisement Rejected Successfully");
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				action = "JOBLIST";
				return showPage();
			}
		} catch (Exception e) {
			log.error("rejectjv method inside exception-------", e);
		}
		return null;
	}

}
