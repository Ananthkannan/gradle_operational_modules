package in.gov.cooptex.operational.intend.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntendDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.operation.intend.model.IntendDetails;
import in.gov.cooptex.operation.intend.model.IntendRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("intendDetailsBean")
@Scope("session")
public class IntendDetailsBean {

	private final String CREATE_PRINTING_REQUIRMENT = "/pages/printingAndStationary/createRequirements.xhtml?faces-redirect=true;";

	private final String REQUIREMENT_PAGE = "/pages/printingAndStationary/listRequirements.xhtml?faces-redirect=true;";

	private final String REQUIREMENT_VIEW = "/pages/printingAndStationary/viewRequirements.xhtml?faces-redirect=true;";

	final String APP_SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	List<IntendRequest> intendRequestList = null;

	@Getter
	@Setter
	IntendRequest intendRequestobj;

	@Getter
	@Setter
	boolean panelFlag = false;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = null;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@Getter
	@Setter
	List<IntendDetails> intendDetailsList = null;

	@Getter
	@Setter
	IntendDTO intendDTO;

	@Getter
	@Setter
	LazyDataModel<IntendDetails> lazyIntendDetailsList;

	@Getter
	@Setter
	IntendDetails intendDetails;

	@Getter
	@Setter
	int totalRecord;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Double totalRequiredQuantity;

	@Getter
	@Setter
	Double totalBalanceQuantity;

	@Getter
	@Setter
	Double totalPrice;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	public String loadRequirementpage() {
		loadLazyIntendDetailsList();
		addButtonFlag = false;
		totalRequiredQuantity = 0.0;
		totalBalanceQuantity = 0.0;
		totalPrice = 0.0;
		viewButtonFlag = true;
		return REQUIREMENT_PAGE;
	}

	private void loadLazyIntendDetailsList() {
		try {
			lazyIntendDetailsList = new LazyDataModel<IntendDetails>() {

				private static final long serialVersionUID = -1540942419672760421L;

				@Override
				public List<IntendDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, Object> filters) {
					List<IntendDetails> data = new ArrayList<IntendDetails>();
					try {
						BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

						data = mapper.readValue(jsonResponse, new TypeReference<List<IntendDetails>>() {
						});

						if (data != null) {
							this.setRowCount(baseDTO.getTotalRecords());
							log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
							totalRecord = baseDTO.getTotalRecords();
						}
					} catch (Exception e) {
						log.error("Error in loadLazyYarnRequirementList()  ", e);
					}
					return data;
				}

				@Override
				public Object getRowKey(IntendDetails res) {
					return res != null ? res.getId() : null;
				}

				@Override
				public IntendDetails getRowData(String rowKey) {
					List<IntendDetails> responseList = (List<IntendDetails>) getWrappedData();
					Long value = Long.valueOf(rowKey);
					for (IntendDetails res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							intendDetails = res;
							return res;
						}
					}
					return null;
				}

			};
		} catch (Exception e) {
			log.error("Exception in lazy load method", e);
		}
	}

	private BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		intendDetails = new IntendDetails();
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		IntendDetails intendDetailsObj = new IntendDetails();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString(), filters);
		intendDetailsObj.setPaginationDTO(paginationDTO);
		intendDetailsObj.setFilters(filters);
		try {
			String URL = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/searchData";
			baseDTO = httpService.post(URL, intendDetailsObj);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}
		return baseDTO;
	}

	public String createRequirments() {
		String url = "";
		totalRequiredQuantity = 0.0;
		totalBalanceQuantity = 0.0;
		totalPrice = 0.0;
		try {
			viewButtonFlag = true;
			addButtonFlag = false;
			setPanelFlag(false);
			productCategory = null;
			intendRequestobj = null;
			intendRequestList = new ArrayList<>();
			mapper = new ObjectMapper();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intendrequest/getRequirementByDueDate";
			BaseDTO response = httpService.get(url);
			log.info("Response of dto" + response == null);
			if (response != null) {
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				intendRequestList = mapper.readValue(jsonResponse, new TypeReference<List<IntendRequest>>() {
				});
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			getCategoryCode();
		} catch (Exception e) {
			log.error("Exception in create requirment method", e);
		}
		return CREATE_PRINTING_REQUIRMENT;
	}

	private void getCategoryCode() {
		String url = "";
		try {
			productCategoryList = new ArrayList<ProductCategory>();
			mapper = new ObjectMapper();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/intenddetails/getCategoryCode/";
			BaseDTO baseDTO = httpService.get(url);
			log.info("response----" + baseDTO != null);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
				log.info("log inside product list-------" + productCategoryList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception in getCategoryCode method", e);
		}
	}

	public void searchRequirement() {
		addButtonFlag = false;
		String url = "";
		totalRequiredQuantity = 0.0;
		totalBalanceQuantity = 0.0;
		totalPrice = 0.0;
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (searchValidation(true)) {
				intendDetailsList = new ArrayList<IntendDetails>();
				sectionMaster = commonDataService.getSectionCode();
				url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/getItemValues/"
						+ productCategory.getProductCatCode() + "/" + productCategory.getProductCatName() + "/"
						+ intendRequestobj.getRequirementName();
				baseDTO = httpService.get(url);
				log.info("baseDTO====>" + baseDTO);
				int alreadyExistCode = ErrorDescription.INTEND_REQUIREMENT_ALREADY_EXISTS.getErrorCode();
				log.info("alreadyExistCode==> "+alreadyExistCode);
				if (baseDTO.getStatusCode() == 0) {
					setPanelFlag(true);
					log.info("inside search method response---");
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					intendDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<IntendDetails>>() {
					});
				} else if (baseDTO.getStatusCode() == alreadyExistCode) {
					setPanelFlag(false);
					errorMap.notify(ErrorDescription.INTEND_REQUIREMENT_ALREADY_EXISTS.getErrorCode());
				} else if (baseDTO.getStatusCode().equals(OperationErrorCode.RECORDS_NOT_FOUND)) {
					log.info("no records found");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.RECORDS_NOT_FOUND).getErrorCode());
					errorMap.notify(ErrorDescription.ERROR_RECORDS_NOT_FOUND.getErrorCode());

				} else {
					setPanelFlag(false);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception in searchRequirement method", e);
		}
	}

	private boolean searchValidation(boolean valid) {
		valid = true;
		String msg = ErrorCodeDescription
				.getDescription(ErrorDescription.INTEND_REQUIREMENT_REQUIREMENTCODE.getErrorCode());
		String msg1 = ErrorCodeDescription
				.getDescription(ErrorDescription.INTEND_REQUIREMENT_CATEGORYCODE.getErrorCode());
		if (intendRequestobj == null) {
			FacesContext.getCurrentInstance().addMessage("requestcodename", new FacesMessage(msg));
			valid = false;
		}
		if (productCategory == null) {
			FacesContext.getCurrentInstance().addMessage("categorycode", new FacesMessage(msg1));
			valid = false;
		}
		return valid;
	}

	public void clear() {
		viewButtonFlag = true;
		addButtonFlag = false;
		setPanelFlag(false);
		intendRequestobj = null;
		productCategory = null;
	}

	public void quantityChange(IntendDetails obj) {
		log.info("inside value change------" + obj.getProductVarietyMaster().getCode());
		String url = "";
		Double requiredQuantity = 0.0, balanceQuantity = 0.0, totalPrice = 0.0;
		try {
			BaseDTO baseDTO = new BaseDTO();
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/quantityChange/"
					+ obj.getProductVarietyMaster().getCode() + "/" + obj.getProductVarietyMaster().getName();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				IntendDetails newObj = mapper.readValue(jsonResponse, IntendDetails.class);
				obj.setProductVarietyMaster(obj.getProductVarietyMaster());
				obj.setUom(obj.getUom());
				obj.setRequiredQuantity(obj.getRequiredQuantity());
				obj.setStockInHand(newObj.getStockInHand());
				obj.setBalanceQuantity(obj.getRequiredQuantity() - obj.getStockInHand());
				obj.setPurchasePrice(obj.getBalanceQuantity() * newObj.getPurchasePrice());
				if (action.equalsIgnoreCase("EDIT")) {
					obj.setId(obj.getId());
				}

				requiredQuantity = requiredQuantity + obj.getRequiredQuantity();
				balanceQuantity = balanceQuantity + obj.getBalanceQuantity();

				totalPrice = totalPrice + obj.getPurchasePrice();
Double trqty = intendDetailsList.stream().filter(o->o.getRequiredQuantity()!=null).collect(Collectors.summingDouble(IntendDetails::getRequiredQuantity));
Double balanceQty = intendDetailsList.stream().filter(o->o.getBalanceQuantity()!=null).collect(Collectors.summingDouble(IntendDetails::getBalanceQuantity));
Double tPrice = intendDetailsList.stream().filter(o->o.getPurchasePrice()!=null).collect(Collectors.summingDouble(IntendDetails::getPurchasePrice));
setTotalRequiredQuantity(trqty);
				setTotalBalanceQuantity(balanceQty);
				setTotalPrice(tPrice);
			}
		} catch (Exception e) {
			log.error("Exception in quantityChange method", e);
		}
	}

	public String saveRequirement() {
		String url = "";
		try {
			BaseDTO baseDTO = new BaseDTO();
			intendDTO = new IntendDTO();
			intendDTO.setIntendRequest(intendRequestobj);
			intendDTO.setSectionMaster(sectionMaster);
			intendDTO.setProductCategory(productCategory);
			intendDTO.setIntendDetailsList(intendDetailsList);
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/saveRequirement";
			log.info("saveRequirement :: url==> "+url);
			baseDTO = httpService.post(url, intendDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INTEND_REQUIREMENT_INSERTED_SUCCESSFULLY.getErrorCode());
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception in saveRequirement method", e);
		}
		return REQUIREMENT_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts IntendDetailsBean.onRowSelect ========>");
		addButtonFlag = true;
		viewButtonFlag = false;
		intendDetails = ((IntendDetails) event.getObject());
	}

	public String clearButton() {
		addButtonFlag = false;
		intendDetails = null;
		viewButtonFlag = true;
		loadLazyIntendDetailsList();
		return REQUIREMENT_PAGE;
	}

	public String requirementView() {
		String url = "";
		try {
			intendDetailsList = new ArrayList<>();
			log.info("date-----" + intendDetails.getCreatedDate() + intendDetails.getId());
			BaseDTO baseDTO = new BaseDTO();
			intendRequestobj = new IntendRequest();
			sectionMaster = new SectionMaster();
			productCategory = new ProductCategory();
			intendDTO = new IntendDTO();
			intendRequestobj.setRequirementCode(intendDetails.getIntendRequest().getRequirementCode());
			intendRequestobj.setRequirementName(intendDetails.getIntendRequest().getRequirementName());
			intendRequestobj.setFromDate(intendDetails.getIntendRequest().getFromDate());
			intendRequestobj.setToDate(intendDetails.getIntendRequest().getToDate());
			intendRequestobj.setDueDate(intendDetails.getIntendRequest().getDueDate());
			intendDTO.setIntendRequest(intendRequestobj);
			intendDTO.setIntendDetails(intendDetails);
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/requirementView";
			baseDTO = httpService.post(url, intendDTO);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				intendDTO = mapper.readValue(jsonResponse, new TypeReference<IntendDTO>() {
				});
				sectionMaster.setCode(intendDTO.getSectionMaster()!=null ? intendDTO.getSectionMaster().getCode():"");
				sectionMaster.setName(intendDTO.getSectionMaster()!=null ? intendDTO.getSectionMaster().getName():"");
				productCategory.setProductCatCode(intendDTO.getProductCategory()!=null ?intendDTO.getProductCategory().getProductCatCode():"");
				productCategory.setProductCatName(intendDTO.getProductCategory()!=null?intendDTO.getProductCategory().getProductCatName():"");
				intendDetailsList.addAll(intendDTO.getIntendDetailsList());
				setTotalBalanceQuantity(intendDTO.getTotalBalanceQuantity());
				setTotalRequiredQuantity(intendDTO.getTotalRequiredQuantity());
				setTotalPrice(intendDTO.getTotalPrice());
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		} catch (Exception e) {
			log.info("exception in view-----", e);
		}
		return REQUIREMENT_VIEW;
	}

	public String requirementUpdate() {
		String url = "";
		try {
			BaseDTO baseDTO = new BaseDTO();
			intendDTO = new IntendDTO();
			intendDTO.setIntendDetailsList(new ArrayList<>());
			intendDTO.getIntendDetailsList().addAll(intendDetailsList);
			url = APP_SERVER_URL + appPreference.getOperationApiUrl() + "/intenddetails/requirementUpdate";
			baseDTO = httpService.post(url, intendDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INTEND_REQUIREMENT_UPDATD_SUCCESSFULLY.getErrorCode());
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			loadLazyIntendDetailsList();
		} catch (Exception e) {
			log.info("exception in edit-----", e);
		}
		return REQUIREMENT_PAGE;
	}
}
