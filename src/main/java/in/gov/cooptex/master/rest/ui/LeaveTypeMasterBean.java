package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class LeaveTypeMasterBean {

	private final String CREATE_PAGE = "/pages/masters/createLeaveTypeMaster.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/masters/listLeaveTypeMaster.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/viewLeaveTypeMaster.xhtml?faces-redirect=true";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	LeaveTypeMaster leaveTypeMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<LeaveTypeMaster>();

	@Getter
	@Setter
	private int leavetypeMasterListSize;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	int totalRecords = 0;

	boolean forceLogin = false;

	@Getter
	@Setter
	LazyDataModel<LeaveTypeMaster> lazyLeaveTypeMasterList;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@PostConstruct
	public void init() {
		log.info(" LeaveTypeMasterBean init --->" + serverURL);
		loadValues();
		addLeaveType();
		addButtonFlag = true;
		getAllLeaveTypeList();
		loadLazyLeaveTypeMasterList();
		mapper = new ObjectMapper();
		// getLeaveTypeView(leaveTypeMaster);
	}

	public String showList() {
		log.info("<---------- Loading LeaveTypeMasterBean showList list page---------->");
		loadValues();
		getAllLeaveTypeList();
		addButtonFlag = true;
		loadLazyLeaveTypeMasterList();
		return LIST_PAGE;
	}

	public String addLeaveType() {
		log.info("<---- Start LeaveTypeMasterBean.addLeaveType ---->");
		try {
			action = "ADD";
			leaveTypeMaster = new LeaveTypeMaster();
			leaveTypeMaster.setActiveStatus(true);

			log.info("<---- End LeaveTypeMaster.addLeaveType ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	public void loadLazyLeaveTypeMasterList() {
		log.info("<===== Starts leaveTypeMasterBean.loadLazyLeaveTypeMasterList ======>");

		lazyLeaveTypeMasterList = new LazyDataModel<LeaveTypeMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<LeaveTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/leavetype/getallleavetypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						leaveTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<LeaveTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("leaveTypeMasterList size---------------->" + lazyLeaveTypeMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return leaveTypeMasterList;
			}

			@Override
			public Object getRowKey(LeaveTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public LeaveTypeMaster getRowData(String rowKey) {
				for (LeaveTypeMaster leaveTypeMasters : leaveTypeMasterList) {
					if (leaveTypeMasters.getId().equals(Long.valueOf(rowKey))) {
						leaveTypeMaster = leaveTypeMasters;
						return leaveTypeMasters;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  leaveTypeMasterBean.loadLazyLeaveTypeMasterList  ======>");
	}

	private void getAllLeaveTypeList() {
		log.info("<<<< ----------Start leavetypeMasterBean-getAllLeaveTypeList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/leavetype/getallactive";
			baseDTO = httpService.get(url);
			leaveTypeMaster = new LeaveTypeMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				leaveTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<LeaveTypeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (leaveTypeMasterList != null) {
			leavetypeMasterListSize = leaveTypeMasterList.size();
		} else {
			leavetypeMasterListSize = 0;
		}
		log.info("<<<< ----------End LeaveTypeMasterBean . getAllLeaveTypeList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			leaveTypeMaster = new LeaveTypeMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... LeaveTypeMasterBean submit begin ....");
		try {
			BaseDTO baseDTO = null;

			/*if (leaveTypeMaster.getCode().length() != 4) {
				Util.addWarn("Code Length should be minimum 4");
				return null;
			}*/

			baseDTO = httpService.post(serverURL + "/leavetype/create", leaveTypeMaster);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("petitiontype saved successfully..........");
				if (action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.LEAVETYPEMASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.LEAVETYPEMASTER_UPDATE_SUCCESS.getCode());
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Error while creating LeaveTypeMaster" + e);
		}
		log.info("...... LeaveTypeMasterBean submit ended ....");
		return LIST_PAGE;
	}

	public String editLeaveType() {
		log.info("<<<<< -------Start editLeaveType called ------->>>>>> ");
		action = "EDIT";

		if (leaveTypeMaster == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}

		if (leaveTypeMaster.getActiveStatus().equals("ACTIVE")) {
			leaveTypeMaster.setActiveStatus(leaveTypeMaster.getActiveStatus());
		} else {
			leaveTypeMaster.setActiveStatus(leaveTypeMaster.getActiveStatus());
		}
		log.info("<<<<< -------End editLeaveType called ------->>>>>> ");

		getrecordById();

		return CREATE_PAGE;

	}

	public String deleteLeaveType() {
		try {
			action = "Delete";
			if (leaveTypeMaster == null) {
				Util.addWarn("Select Atleast One Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmLeaveTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteLeaveTypeConfirm() {

		try {
			log.info("Delete confirmed for leavetype [" + leaveTypeMaster.getName() + "]");
			LeaveTypeMaster leavetypeMasterDelete = new LeaveTypeMaster();
			leavetypeMasterDelete.setId(leaveTypeMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/leavetype/delete/" + leaveTypeMaster.getId();
			baseDTO = httpService.delete(url);

			/*
			 * if (response != null && response.getStatusCode() == 0) {
			 * errorMap.notify(ErrorDescription.PETITIONTYPE_DELETE_SUCCESS.getErrorCode());
			 */
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("LeaveType deleted successfully....!!!");
					errorMap.notify(ErrorDescription.LEAVETYPEMASTER_DELETE_SUCCESS.getErrorCode());
					leaveTypeMaster = new LeaveTypeMaster();
				}else if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				}else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return showList();
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start leavetypeMasterBean . clear ----->");
		getAllLeaveTypeList();
		loadLazyLeaveTypeMasterList();
		addButtonFlag = true;
		leaveTypeMaster = new LeaveTypeMaster();
		log.info("<---- End LeaveTypeMasterBean . clear ----->");

		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewLeaveType() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (leaveTypeMaster == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}
		getrecordById();
		return VIEW_PAGE;

	}

	private void getLeaveTypeView(LeaveTypeMaster leaveTypeMaster) {
		log.info("<<<< ----------Start leavetypeMasterBean . getLeaveTypeView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/leavetype/getallactive";
			baseDTO = httpService.post(url, leaveTypeMaster);
			leaveTypeMaster = new LeaveTypeMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				leaveTypeMaster = mapper.readValue(jsonResponse, new TypeReference<LeaveTypeMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (leaveTypeMaster != null) {
			log.info("leavetypemaster getLeaveTypeView" + leaveTypeMaster);
		} else {
			log.info("leavetypemaster getLeaveTypeView" + leaveTypeMaster);
		}

		log.info("<<<< ----------End LeaveTypeMasterBean . getLeaveTypeView ------- >>>>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts InvestmentCategoryMasterBean.onRowSelect ========>" + event);
		leaveTypeMaster = ((LeaveTypeMaster) event.getObject());
		addButtonFlag = false;
		log.info("<===Ends Leavetypr.onRowSelect ========>");
	}

	public void getrecordById() {
		log.info("Inside getrecordById() ");
		BaseDTO baseDTO = null;
		try {
			String url = serverURL + "/leavetype/" + leaveTypeMaster.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				leaveTypeMaster = mapper.readValue(jsonResponse, new TypeReference<LeaveTypeMaster>() {
				});

			}
		} catch (Exception e) {
			log.error("Exception Occured While getrecordById() :: ", e);
		}

	}

}
