package in.gov.cooptex.operation.rest.ui.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.gov.cooptex.core.enums.ShowroomType;
import in.gov.cooptex.core.enums.StockMovementTo;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.enums.WarehouseType;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.YesNoType;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class StockTransferUtility {
	
	
	public String loadServerUrl() {
		String URL = null;
		try {
			URL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
		
		return URL;
	}
	
	public List<Object> loadStockTransferStatusList() {
		return Arrays.asList((Object[])StockTransferStatus.class.getEnumConstants());
	}

	public List<Object> loadTransportChargeType() {
		return Arrays.asList((Object[])TransportChargeType.class.getEnumConstants());
	}
	
	public List<Object> loadStockMovementToList(boolean loadSocietyFlag) {
		List<Object> list = new ArrayList<Object>();
		list.add(StockMovementTo.Warehouse);
		list.add(StockMovementTo.Showroom);
		list.add(StockMovementTo.ISSR);
		list.add(StockMovementTo.TestingLab);
		return list;
	}
	
	public List<Object> loadWarehouseTypeList(boolean loadProductWarehouseFlag) {	
		List<Object> list = new ArrayList<Object>();
		
		list.add(WarehouseType.Product_Warehouse);
		list.add(WarehouseType.Distribution_Warehouse);
		return list;
	}
	
	public List<Object> loadShowroomTypeList() {
		return Arrays.asList((Object[])ShowroomType.class.getEnumConstants());
	}
	
	public List<YesNoType> loadYesNoList() {
		List<YesNoType> yesOrNoList = new ArrayList<YesNoType>();
		yesOrNoList.add(new YesNoType("Yes", true));
		yesOrNoList.add(new YesNoType("No", false));
		return yesOrNoList;
	}
	
	public boolean isAtNumberDuplicated(String atNumber, List<StockTransferItems> stockTransferItemList) {
		if(stockTransferItemList != null && !stockTransferItemList.isEmpty()) {
			for(StockTransferItems items : stockTransferItemList) {
				if(items.getAtNumber().equals(atNumber)) {
					return true;
				}
			}
		}
		return false;
	}
	
	
	public boolean isQrCodeDuplicated(String qrCode, List<StockTransferItems> stockTransferItemList) {
		if(stockTransferItemList != null && !stockTransferItemList.isEmpty()) {
			for(StockTransferItems items : stockTransferItemList) {
				if(items.getQrCode().equals(qrCode)) {
					return true;
				}
			}
		}
		return false;
	}
	
	public ProductVarietyMaster getProductByAtNumber(String atNumber, List<StockTransferItems> list) {
		if (list != null && !list.isEmpty()) {
			for (StockTransferItems items : list) {
				if (items.getAtNumber() != null && items.getAtNumber().equals(atNumber)) {
					return items.getProductVarietyMaster();
				}
			}
		}
		return null;
	}
	
	

	public Double sumOfDispatchedQtyByProductVariety(StockTransferItems stockTransferItems, List<StockTransferItems> stockTransferItemsList) {
		Double sum = 0D;
		if(stockTransferItemsList != null && !stockTransferItemsList.isEmpty()) {
			for(StockTransferItems items : stockTransferItemsList) {
				if(items.getProductVarietyMaster().getId().equals(stockTransferItems.getProductVarietyMaster().getId())) {
					sum = sum + AppUtil.ifNullRetunZero(items.getDispatchedQty());
				}
			}
		}
		return sum;
	}
	

	
}
