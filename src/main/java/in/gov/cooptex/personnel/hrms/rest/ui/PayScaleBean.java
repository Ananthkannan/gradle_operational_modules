package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("payScaleBean")
public class PayScaleBean {

	private final String LIST_URL = "/pages/masters/listPayScale.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/masters/viewPayScale.xhtml?faces-redirect=true;";
	private final String ADD_URL = "/pages/masters/addPayScale.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();
	final String SERVER_URL = AppUtil.getPortalServerURL();
	ObjectMapper mapper;

	@Getter
	@Setter
	PayScaleMaster payScaleMaster = new PayScaleMaster();
	List<PayScaleMaster> payScaleMasterList = null;
	@Autowired
	AppPreference appPreference;
	String jsonResponse;

	@Getter
	@Setter
	PayScaleMaster payScaleSelect;
	@Getter
	@Setter
	LazyDataModel<PayScaleMaster> lazyPayScaleMasterList;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	Double fromAmount;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Double toAmount;

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	public String action = "";

	@Setter
	@Getter
	int listSize;

	@Setter
	@Getter
	Boolean addButton = true;

	@Setter
	@Getter
	List<PayScaleMaster> payScaleLists = new ArrayList<>();
	
	@Setter
	@Getter
	String payRange;

	@Autowired
	ErrorMap errorMap;

	@PostConstruct
	public void init() {
		log.info("<<=====Inside init()======>");

		loadValues();
		loadPayScaleList();
		loadLazyPayScaleMasterList();
	}

	private void loadValues() {

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PayScaleBean.onRowSelect ========>" + event);

		payScaleSelect = ((PayScaleMaster) event.getObject());
		// addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButton = false;
		log.info("<===Ends PayScaleBean.onRowSelect ========>");
	}

	public String showPayScaleForm() {
		log.info("<<=====showPayScaleForm=====>>" + action);

		if (action.equalsIgnoreCase("ADD")) {
			log.info("action :: " + action.toString());
			// payScaleMaster=new PayScaleMaster();
			fromAmount = null;
			toAmount = null;
			return ADD_URL;
		} else if (action.equalsIgnoreCase("EDIT")) {
			log.info("action :: " + action.toString());
			if (payScaleSelect == null) {
				log.info("===select atleast one  field");
				errorMap.notify(ErrorDescription.PAYSCALE_SELECT_EMPTY.getCode());
				return null;

			}
			/*
			 * String url = SERVER_URL + appPreference.getOperationApiUrl() +
			 * "/payscale/get/" + payScaleSelect.getId(); BaseDTO response =
			 * httpService.get(url); if (response != null && response.getStatusCode() == 0)
			 * jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			 * petitionType = mapper.readValue(jsonResponse, new
			 * TypeReference<PetitionType>() { });
			 */
			fromAmount = payScaleSelect.getFromAmount();
			toAmount = payScaleSelect.getToAmount();
			// return ADD_URL;
			return ADD_URL;
		}else if(action.equalsIgnoreCase("View")){
			getrecordById();
			return VIEW_URL;
		}
		else if (action.equalsIgnoreCase("DELETE")) {
			log.info("action :: " + action.toString());
			if (payScaleSelect == null) {
				log.info("===select atleast one  field");
				errorMap.notify(ErrorDescription.PAYSCALE_SELECT_EMPTY.getCode());
				return null;
			}
			RequestContext.getCurrentInstance().execute("PF('confirmPayScaleDelete').show();");
			return null;
			// deletePayScale();
			// return LIST_URL;
		} else if (action.equalsIgnoreCase("LIST")) {
			loadPayScaleList();
			loadLazyPayScaleMasterList();
			return LIST_URL;
		}
		return"";
	}
	
	public void getrecordById() {
		log.info("Inside getrecordById() "+payScaleSelect.getId());
		BaseDTO baseDTO = null;
		try {
			String url = serverURL+"/payscale/get/id/"+ payScaleSelect.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				payScaleMaster = mapper.readValue(jsonResponse, new TypeReference<PayScaleMaster>() {
				});
			}
			log.info("<<<<:::::::::::payscaleMaster::::::::::::::::>>>>"+payScaleMaster.getFromAmount());
		} catch (Exception e) {
			log.error("Exception Occured While getrecordById() :: ", e);
		}
	}
	public String submitPayScaleForm() {
		log.info("<<==== submitForm===>> ");
		try {
			PayScaleMaster payScaleMaster = new PayScaleMaster();
			if (action.equalsIgnoreCase("ADD")) {
				if (payScaleMaster.getId() == null) {
					log.info("===add PayScale====");

					if (fromAmount == null) {
						errorMap.notify(ErrorDescription.PAY_SCALE_GROUP_FROM_AMOUNT_NOT_NULL.getCode());
						return null;
					}
					if (toAmount == null) {
						errorMap.notify(ErrorDescription.PAY_SCALE_GROUP_TO_AMOUNT_NOT_NULL.getCode());
						return null;
					}
					if (fromAmount > toAmount) {
						errorMap.notify(ErrorDescription.FROM_AMOUNT_NOT_LESS_THAN_TO_AMOUNT.getCode());
						return null;
					}
					if(fromAmount.equals(toAmount)) {
						errorMap.notify(ErrorDescription.FROM_AMOUNT_NOT_Equal_TO_AMOUNT.getCode());
						return null;
					}

					UserMaster userMaster = loginBean.getUserDetailSession();
					payScaleMaster.setCreatedBy(userMaster);
					payScaleMaster.setFromAmount(fromAmount);
					payScaleMaster.setToAmount(toAmount);
					payScaleMaster.setPayRange(payRange);

					log.info("fromAmount  " + payScaleMaster.getFromAmount());
					log.info("toAmmount  " + payScaleMaster.getToAmount());

					String URL = serverURL + "/payscale/create";
					BaseDTO baseDTO = httpService.post(URL, payScaleMaster);

					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.PAYSCALE_SAVE_SUCCESS.getCode());
					} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}

				}
			} else if (action.equalsIgnoreCase("EDIT")) {

				payScaleMaster.setId(payScaleSelect.getId());
				long id = payScaleMaster.getId();
				log.info("==payScaleMaster.getId()===" + payScaleMaster.getId());
				if (payScaleMaster.getId() != null) {
					log.info("===update PayScale====");

					if (fromAmount == null) {
						errorMap.notify(ErrorDescription.PAY_SCALE_GROUP_FROM_AMOUNT_NOT_NULL.getCode());
						return null;
					}
					if (toAmount == null) {
						errorMap.notify(ErrorDescription.PAY_SCALE_GROUP_TO_AMOUNT_NOT_NULL.getCode());
						return null;
					}
					if (fromAmount > toAmount) {
						errorMap.notify(ErrorDescription.FROM_AMOUNT_NOT_LESS_THAN_TO_AMOUNT.getCode());
						return null;
					}
					if(fromAmount.equals(toAmount)) {
						errorMap.notify(ErrorDescription.FROM_AMOUNT_NOT_Equal_TO_AMOUNT.getCode());
						return null;
					}

					UserMaster userMaster = loginBean.getUserDetailSession();

					payScaleMaster.setFromAmount(fromAmount);
					payScaleMaster.setToAmount(toAmount);
					/*
					 * String URL = serverURL + "/payscale/updatepayscale"; BaseDTO baseDTO =
					 * httpService.put(URL, payScaleSelect);
					 */

					String URL = serverURL + "/payscale/updatepayscale";
					BaseDTO baseDTO = httpService.post(URL, payScaleMaster);

					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.PAYSCALE_UPDATE_SUCCESS.getCode());
					} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
					log.info("baseDto :::" + baseDTO);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in submitPayScale ....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		// payScaleSelect = new PayScaleMaster();
		loadPayScaleList();
		loadLazyPayScaleMasterList();
		addButton = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		// addButtonFlag = true;
		viewButtonFlag = true;
		return LIST_URL;

	}

	public String loadPayScaleList() {
		log.info("=====loadPayScaleList====");
		BaseDTO baseDTO = new BaseDTO();

		String URL = serverURL + "/payscale/getPayScale";
		log.info("calling button" + URL);
		baseDTO = httpService.get(URL);
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			try {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				payScaleLists = mapper.readValue(jsonResponse, new TypeReference<List<PayScaleMaster>>() {
				});
				if(payScaleLists != null && !payScaleLists.isEmpty()) {
					log.info("No of payScaleList ======= " + payScaleLists.size());
				}
				 payScaleSelect = new PayScaleMaster();
				 addButton = true;
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("No of payScaleList ======= " + payScaleLists.size());

		if (payScaleLists != null)
			listSize = payScaleLists.size();
		else
			listSize = 0;
		return LIST_URL;
	}

	public void loadLazyPayScaleMasterList() {
		log.info("<===== Starts petitiontypeBean.loadLazypetitionTypeList ======>");

		lazyPayScaleMasterList = new LazyDataModel<PayScaleMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<PayScaleMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);

					String url = SERVER_URL + "/payscale/getallpayscalemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);

					log.info("====response==========" + response);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						String jsonResponse;
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						payScaleMasterList = mapper.readValue(jsonResponse, new TypeReference<List<PayScaleMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("PayScaleMasterBean size---------------->" + lazyPayScaleMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return payScaleMasterList;
			}

			@Override
			public Object getRowKey(PayScaleMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PayScaleMaster getRowData(String rowKey) {
				for (PayScaleMaster payScaleMaster : payScaleMasterList) {
					if (payScaleMaster.getId().equals(Long.valueOf(rowKey))) {
						payScaleSelect = payScaleMaster;
						return payScaleMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  PayScaleMasterBean.loadLazyPayScaleMasterList  ======>");
	}

	public String cancel() {
		fromAmount = null;
		toAmount = null;
		payScaleSelect = new PayScaleMaster();
		loadPayScaleList();
		loadLazyPayScaleMasterList();
		addButton = true;
		return LIST_URL;

	}

	public String deletePayScale() {
		log.info("===delete=====");

		if (action.equalsIgnoreCase("DELETE")) {
			if (payScaleSelect == null) {
				errorMap.notify(ErrorDescription.PAYSCALE_SELECT_EMPTY.getCode());
				return null;
			}
			payScaleMaster.setId(payScaleSelect.getId());
			log.info("===Id is == " + payScaleMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			Long id = payScaleMaster.getId();
			String URL = serverURL + "/payscale/delete/" + id;
			log.info("URL IS " + URL);
			baseDTO = httpService.delete(URL);
			log.info("==Status code is " + baseDTO.getStatusCode());
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.PAYSCALE_DELETED_SUCCESS.getCode());
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}

		payScaleSelect = new PayScaleMaster();
		loadValues();
		loadPayScaleList();
		loadLazyPayScaleMasterList();
		return LIST_URL;

	}

}
