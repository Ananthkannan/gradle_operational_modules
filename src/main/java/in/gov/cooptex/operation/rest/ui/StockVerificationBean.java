package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ExcessDeficitStockUploadDTO;
import in.gov.cooptex.core.dto.QRCodeGenerationDTO;
import in.gov.cooptex.core.dto.StockVerificationDTO;
import in.gov.cooptex.core.enums.StockVerificationStatusEnum;
import in.gov.cooptex.core.inventory.model.InventoryItems;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.masters.ui.bean.ProductVarietyTaxBean;
import in.gov.cooptex.operation.stock.model.StockVerification;
import in.gov.cooptex.operation.stock.model.StockVerificationDetails;
import in.gov.cooptex.operation.stock.model.StockVerificationEmployees;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("stockVerificationBean")
@Scope("session")
@Log4j2
public class StockVerificationBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	ProductVarietyTaxBean productVarietyTaxBean;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Date closingDate;

	@Getter
	@Setter
	Date dueDate;

	@Getter
	@Setter
	String qrCode;

	@Getter
	@Setter
	Double quantity;

	@Getter
	@Setter
	String productCode, productName;

	@Getter
	@Setter
	String qrCodeStock;

	@Getter
	@Setter
	Double quantityStock;

	@Getter
	@Setter
	EntityMaster loginEntity;

	@Getter
	@Setter
	Long employeeID;

	@Getter
	@Setter
	Date startDate;

	@Getter
	@Setter
	Date endDate;

	@Getter
	@Setter
	List<InventoryItems> inventoryItemsList = new ArrayList<>();

	@Getter
	@Setter
	List<StockVerificationDTO> stockVerificationDTOList;

	@Getter
	@Setter
	List<StockVerificationDTO> stockVerficationQrCodeList;

	@Getter
	@Setter
	List<String> qrCodeList = new ArrayList<>();

	@Getter
	@Setter
	List<StockVerificationStatusEnum> stockVerificationStatusEnums;

	/*
	 * @Getter
	 * 
	 * @Setter StockVerificationDTO stockVerificationDTO;
	 */

	@Getter
	@Setter
	StockVerificationDTO saveStockVerificationDTO;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	StockVerification stockVerification;

	@Getter
	@Setter
	StockVerificationDetails stockVerificationDetails;

	@Getter
	@Setter
	StockVerificationEmployees stockVerificationEmployees;

	@Getter
	@Setter
	List<StockVerificationDetails> stockVerificationDetailsList;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	/*
	 * 20-06-2019
	 * 
	 * @author Praveen
	 */
	@Getter
	@Setter
	List<ExcessDeficitStockUploadDTO> inventoryItemsStockVerificationList;

	@Getter
	@Setter
	private Double qrCodeSystemQty = 0.00;

	@Getter
	@Setter
	private Double physicalQuantity = 0.00;

	@Getter
	@Setter
	private String scannedQRCode;

	@Getter
	@Setter
	private ExcessDeficitStockUploadDTO stockVerificationExcessData;

	@Getter
	@Setter
	String actualTotalQty;

	@Getter
	@Setter
	String bookedTotalQty;

	@Getter
	@Setter
	String excessTotalQty;

	@Getter
	@Setter
	String deficitTotalQty;

	@Getter
	@Setter
	String bookedTotalValue;

	@Getter
	@Setter
	String excessTotalValue;

	@Getter
	@Setter
	String deficitTotalValue;

	@Getter
	@Setter
	String actualTotalValue;

	@Getter
	@Setter
	Long stockVerificationId;

	@Getter
	@Setter
	ExcessDeficitStockUploadDTO stockVerificationData;

	@Getter
	@Setter
	StockVerificationDTO stockVerificationApprove = new StockVerificationDTO();

	// Newly added
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	StockVerificationDTO stockVerificationDto = new StockVerificationDTO();

	@Getter
	@Setter
	private String summaryBookedQty;

	@Getter
	@Setter
	private String summaryBookedValue;

	@Getter
	@Setter
	private String summaryExcessQty;

	@Getter
	@Setter
	private String summaryExcessValue;

	@Getter
	@Setter
	private String summaryDeficitQty;

	@Getter
	@Setter
	private String summaryDeficitValue;

	@Getter
	@Setter
	private String summaryActualQty;

	@Getter
	@Setter
	private String summaryActualValue;

	private final String STOCK_VERIFICATION_PAGE = "/pages/stock/stockVerification.xhtml?faces-redirect=true;";

	public String stockVerificationListPage() {
		log.info("<<<--- Start the stockVerificationListPage() --->>> ");
		try {
			stockVerificationDto = new StockVerificationDTO();
			productCategoryList = commonDataService.loadProductCategories();
			clearSearch();
		} catch (Exception ex) {
			log.error("Exception Occurred at stockVerificationListPage(), ", ex);
		}
		log.info("<<<--- End the stockVerificationListPage() --->>> ");
		return STOCK_VERIFICATION_PAGE;
	}

	private void checkStockVerificationClosingDate() {
		log.info("<<<--- Start the checkStockVerificationClosingDate() --->>> ");
		try {
			String url = SERVER_URL + "/stockverificationnew/checkStockVerificationClosingDate";
			log.info(" stockVerificationBean. checkStockVerificationClosingDate URL : " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {

				StockVerificationDTO stockVerificationDTOObj = null;

				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockVerificationDTOObj = mapper.readValue(jsonResponse, new TypeReference<StockVerificationDTO>() {
				});

				if (stockVerificationDTOObj != null) {
					stockVerificationDto
							.setStockVerificationClosingDate(stockVerificationDTOObj.getStockVerificationClosingDate());
					stockVerificationDto.setApprovalButtonFlag(stockVerificationDTOObj.getApprovalButtonFlag());
					stockVerificationDto.setStockMaxCloseDate(stockVerificationDTOObj.getStockMaxCloseDate());
				}
			}
		} catch (Exception ex) {
			log.error("Exception Occurred at checkStockVerificationClosingDate(), ", ex);
		}
		log.info("<<<--- End the checkStockVerificationClosingDate() --->>> ");
	}

	public void loadProductGroups() {
		log.info("<<< --- Inside loadProductGroups() --->>> ");
		if (stockVerificationDto != null && stockVerificationDto.getProductCategory() != null) {
			log.info("load productGroups with productCategory Code -- "
					+ stockVerificationDto.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(stockVerificationDto.getProductCategory());
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
		}
		log.info("<<< --- End loadProductGroups() --->>> ");
	}

	// Stock Verification

	public void fetchStockDetailsByClosingDate() {
		Long stockVerificationId = null;

		log.info("<<< ---- Start the fetchStockDetailsByClosingDate() ---->>> ");
		try {

			if (stockVerificationDto == null || stockVerificationDto.getStockDate() == null) {
				AppUtil.addWarn("Please select closing date");
				return;
			}

			String stockVerificationURL = SERVER_URL + "/stockverificationnew/fetchStockDetailsByDateAndProductDtls";
			log.info(" fetchStockDetailsByClosingDate URL -------->> " + stockVerificationURL);
			BaseDTO baseDTO = httpService.post(stockVerificationURL, stockVerificationDto);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					inventoryItemsStockVerificationList = mapper.readValue(jsonResponse,
							new TypeReference<List<ExcessDeficitStockUploadDTO>>() {
							});

					updateSummaryValue(baseDTO);
					checkStockVerificationClosingDate();
				} else {
					String errorMsg = baseDTO.getErrorDescription();
					// StringBuilder builder = new StringBuilder();
					// builder.append("Stock Verification :");
					// if (errorMsg != null) {
					// builder.append("Error : " + errorMsg);
					// }
					// errorMsg = builder.toString();
					log.info(errorMsg);
					AppUtil.addWarn(errorMsg);
					return;
				}

			}

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("InventoryItemsStockVerificationListSize :: [" + inventoryItemsStockVerificationListSize + "]");

			sumOfTotalQtyAndValueForStockVerificationData();

			if (inventoryItemsStockVerificationListSize == 0) {
				AppUtil.addWarn("Stock Verification data not available for given date");
				return;
			}
		} catch (Exception ex) {
			log.error("Exception Occurred at fetchStockDetailsByClosingDate() ", ex);
		}

		log.info("<<< ---- End the fetchStockDetailsByClosingDate() ---->>> ");
	}

	public void onSearchQRCode() {
		log.info("StockVerificationBean. onSearchQRCode() - START");
		try {
			qrCodeSystemQty = 0.00;
			scannedQRCode = null;
			physicalQuantity = null;
			stockVerificationData = new ExcessDeficitStockUploadDTO();

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("InventoryItemsStockVerificationList Size -- " + inventoryItemsStockVerificationListSize);

			if (inventoryItemsStockVerificationListSize == 0) {
				AppUtil.addWarn("Stock Verification data not available");
				qrCode = new String();
				qrCodeSystemQty = 0.00;
				return;
			}
			if (qrCode != null && !qrCode.equals("")) {
				QRCodeGenerationDTO qrCodeGenerationDTO = new QRCodeGenerationDTO();
				if (AppUtil.isJSONValid(qrCode)) {
					qrCodeGenerationDTO = new Gson().fromJson(qrCode, QRCodeGenerationDTO.class);
				} else {
					qrCodeGenerationDTO.setQRCode(qrCode);
				}
				String qrCode = qrCodeGenerationDTO.getQRCode();
				log.info("Scanned QRCode :: " + qrCode);

				// check the stock verification list for qrcode

				Double systemQty = 1D;
				stockVerificationAutoSubmit(qrCode);
				updateActualQty(qrCode, systemQty);
				// sumOfTotalQtyAndValueForStockVerificationData();
				this.qrCode = new String();

			} else {
				AppUtil.addWarn("Please enter QRCode");
			}
		} catch (Exception ex) {
			AppUtil.addWarn(ex.getMessage());
			log.error("Exception Occurred at onSearchQRCode(), ", ex);
		} finally {
			qrCode = new String();
			qrCodeSystemQty = null;
		}
		log.info("<<<--- End the onSearchQRCode() --->>>");
	}

	public void onSearchQRCodeOld() {
		log.info("<<<--- Inside the onSearchQRCode() --->>>");
		try {
			qrCodeSystemQty = 0.00;
			scannedQRCode = null;
			physicalQuantity = null;
			stockVerificationData = new ExcessDeficitStockUploadDTO();
			final String UOM_METER_CODE = "METR";

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("InventoryItemsStockVerificationList Size -- " + inventoryItemsStockVerificationListSize);

			if (inventoryItemsStockVerificationListSize == 0) {
				AppUtil.addWarn("Stock Verification data not available");
				qrCode = new String();
				qrCodeSystemQty = 0.00;
				return;
			}
			if (qrCode != null && !qrCode.equals("")) {
				QRCodeGenerationDTO qrCodeGenerationDTO = new QRCodeGenerationDTO();
				if (AppUtil.isJSONValid(qrCode)) {
					qrCodeGenerationDTO = new Gson().fromJson(qrCode, QRCodeGenerationDTO.class);
				} else {
					qrCodeGenerationDTO.setQRCode(qrCode);
				}
				String qrCode = qrCodeGenerationDTO.getQRCode();
				log.info("Scanned QRCode :: " + qrCode);
				String uomCode = "";
				double actualQty = 0;
				double bookedQty = 0;
				// check the stock verification list for qrcode
				if (inventoryItemsStockVerificationList != null && inventoryItemsStockVerificationList.size() > 0) {
					ExcessDeficitStockUploadDTO inventory = inventoryItemsStockVerificationList.stream()
							.filter(inventoryData -> qrCode.equals(inventoryData.getQrCode())).findAny().orElse(null);
					if (inventory != null) {
						this.stockVerificationData = inventory;
						qrCodeSystemQty = AppUtil.ifNullRetunZero(inventory.getBookedQty());
						bookedQty = AppUtil.ifNullRetunZero(inventory.getBookedQty());
						if (bookedQty < 0) {
							AppUtil.addWarn("Stock not available.");
							return;
						}
						actualQty = AppUtil.ifNullRetunZero(inventory.getActualQty());
						uomCode = inventory.getUomCode();

					} else {
						// AppUtil.addWarn("QRCode not found");

						Long categoryId = stockVerificationDto.getProductCategory() != null
								? stockVerificationDto.getProductCategory().getId()
								: null;
						Long groupId = stockVerificationDto.getProductGroupMaster() != null
								? stockVerificationDto.getProductGroupMaster().getId()
								: null;
						String status = checkScannedQRCodeWithServer(qrCode, categoryId, groupId);

						if (status == null) {
							return;
						}
						//
						if (stockVerificationExcessData != null) {
							RequestContext context = RequestContext.getCurrentInstance();
							context.execute("PF('stockVerficationExcessDialogVar').show();");
							context.update(":stockVerficationExcessDialog");
							qrCodeSystemQty = null;
							context.update(":stockVerficationExcessDialogPanelId");
							return;
						}
					}
				}

				if (UOM_METER_CODE.equals(uomCode)) {
					scannedQRCode = qrCode;
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('stockVerficationMeterDialogVar').show();");
					context.update(":stockVerficationMeterDialog");
					context.update(":qrcodeID");
					context.update(":meterQtyID");
				} else {
					if (bookedQty == 1 && actualQty == 0) {
						Double systemQty = 1D;
						stockVerificationAutoSubmit(qrCode);
						updateActualQty(qrCode, systemQty);
						// sumOfTotalQtyAndValueForStockVerificationData();
						this.qrCode = new String();
					} else {
						scannedQRCode = qrCode;
						RequestContext context = RequestContext.getCurrentInstance();
						context.execute("PF('stockVerficationMeterDialogVar').show();");
						context.update(":stockVerficationMeterDialog");
						context.update(":qrcodeID");
						context.update(":meterQtyID");
					}
				}
				// getQRCodeSystemQty(qrCode);
			} else {
				AppUtil.addWarn("Please enter QRCode");
			}
		} catch (Exception ex) {
			AppUtil.addWarn(ex.getMessage());
			log.error("Exception Occurred at onSearchQRCode(), ", ex);
		} finally {
			qrCode = new String();
			qrCodeSystemQty = null;
		}
		log.info("<<<--- End the onSearchQRCode() --->>>");
	}

	private void updateActualQty(String qrCode, double physicalQty) {

		ExcessDeficitStockUploadDTO inventory = inventoryItemsStockVerificationList.stream()
				.filter(inventoryData -> qrCode.equals(inventoryData.getQrCode())).findAny().orElse(null);
		if (inventory != null) {
			inventory.setActualQty(physicalQty);
			inventory.setActualValue(AppUtil.ifNullRetunZero(inventory.getActualQty())
					* AppUtil.ifNullRetunZero(inventory.getUnitRate()));
		}
	}

	private String checkScannedQRCodeWithServer(String qrCode, Long categoryId, Long groupId) {
		log.info("<<<--- Start the checkScannedQRCodeWithServer() --->>>");
		log.info("Scanned QRCode :: " + qrCode);
		String status = null;
		try {

			stockVerificationExcessData = new ExcessDeficitStockUploadDTO();

			String url = SERVER_URL + "/stockverificationnew/findQRCode/" + qrCode + "/" + categoryId + "/" + groupId;
			log.info("   Find ExcessItem  URL -------->> " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				String errorMsg = null;
				if (baseDTO.getStatusCode() == 0) {
					errorMsg = baseDTO.getErrorDescription();
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse;
					try {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						stockVerificationExcessData = mapper.readValue(jsonResponse,
								new TypeReference<ExcessDeficitStockUploadDTO>() {
								});
					} catch (JsonProcessingException e) {
						log.error("Exception occured while parsing genericDTO object...", e);
					} catch (IOException e) {
						log.error("Exception occured while parsing genericDTO object...", e);
					}

					log.info("StockVerification Excess Items :: " + errorMsg);
					status = "Success";
				} else {
					stockVerificationExcessData = null;
					AppUtil.addWarn("This scanned item does not belong to selected category/group.");

					this.qrCode = new String();
					status = null;
					return null;
				}
			}
		} catch (Exception ex) {
			status = null;
			AppUtil.addWarn(ex.getMessage());
			log.error("Exception Occurred at checkScannedQRCodeWithServer() ", ex);
		}
		log.info("<<<--- End the checkScannedQRCodeWithServer() --->>>");
		return status;
	}

	private void updateStockVerificationActualQty(String qrCode, double physicalQty) {
		BaseDTO response = new BaseDTO();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		if (!CollectionUtils.isEmpty(inventoryItemsStockVerificationList)) {
			ExcessDeficitStockUploadDTO inventoryStockVerification = inventoryItemsStockVerificationList.get(0);
			StockVerificationDTO stockVerificationDTO = new StockVerificationDTO();
			stockVerificationDTO.setQrCode(qrCode);
			stockVerificationDTO.setQuantity(physicalQty);
			stockVerificationDTO.setBookQuantity(stockVerificationData.getBookedQty());
			stockVerificationDTO.setStockVerificationId(inventoryStockVerification.getVerificationId());
			stockVerificationDTO.setStockCloseDate(simpleDateFormat.format(stockVerificationDto.getStockDate()));

			String saveUrl = SERVER_URL + "/stockverificationnew/saveStockVerificationNew";
			response = httpService.post(saveUrl, stockVerificationDTO);

			if (response != null) {
				updateSummaryValue(response);
			}
		}
		// fetchStockDetailsByClosingDate();
	}

	private void updateStockVerificationActualQtyOld(String qrCode, double physicalQty) {
		BaseDTO response = new BaseDTO();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (ExcessDeficitStockUploadDTO inventoryStockVerification : inventoryItemsStockVerificationList) {
			if (qrCode.equals(inventoryStockVerification.getQrCode())) {

				StockVerificationDTO stockVerificationDTO = new StockVerificationDTO();
				stockVerificationDTO.setDetailsId(inventoryStockVerification.getVerificationDetailsId());
				stockVerificationDTO.setQrCode(qrCode);
				stockVerificationDTO.setQuantity(physicalQty);
				stockVerificationDTO.setBookQuantity(inventoryStockVerification.getBookedQty());
				stockVerificationDTO.setStockVerificationId(inventoryStockVerification.getVerificationId());
				stockVerificationDTO.setStockCloseDate(simpleDateFormat.format(stockVerificationDto.getStockDate()));

				String saveUrl = SERVER_URL + "/stockverificationnew/saveStockVerification";
				response = httpService.post(saveUrl, stockVerificationDTO);

				if (response != null) {
					updateSummaryValue(response);
				}

				break;
			}
		}
		// fetchStockDetailsByClosingDate();
	}

	public void updateMeterProductActualQty() {

		log.info("<<< --- Start the updateMeterProductActualQty() --->>> ");
		try {
			log.info("Scanned QRCode :: " + scannedQRCode);
			log.info("physical Quantity (METER) :: " + physicalQuantity);
			// check qrCode null
			if (scannedQRCode == null) {
				AppUtil.addWarn("QRCode not found");
				qrCode = new String();
				qrCodeSystemQty = 0.00;
				return;
			}
			// check physical quantity not null condition
			if (physicalQuantity != null && physicalQuantity >= 0D) {
				updateStockVerificationActualQty(scannedQRCode, physicalQuantity);
				sumOfTotalQtyAndValueForStockVerificationData();
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('stockVerficationMeterDialogVar').hide();");
			} else {
				AppUtil.addWarn("Quantity should not be less than zero");
				qrCode = new String();
				qrCodeSystemQty = 0.00;
				return;
			}
		} catch (Exception ex) {
			log.error("Exception Occurred at updateMeterProductActualQty() ", ex);
		} finally {
			qrCode = new String();
			qrCodeSystemQty = 0.00;
		}
		log.info("<<< --- End the updateMeterProductActualQty() --->>> ");
	}

	private void getQRCodeSystemQty(String qrCode) {
		if (inventoryItemsStockVerificationList != null && inventoryItemsStockVerificationList.size() > 0) {
			ExcessDeficitStockUploadDTO inventory = inventoryItemsStockVerificationList.stream()
					.filter(inventoryData -> qrCode.equals(inventoryData.getQrCode())).findAny().orElse(null);
			if (inventory != null) {
				qrCodeSystemQty = inventory.getBookedQty();
			} else {
				AppUtil.addWarn("QRCode not found");
				return;
			}

		}

	}

	public void addExcessDataForStockVerification() {
		if (stockVerificationExcessData != null) {
			inventoryItemsStockVerificationList.add(stockVerificationExcessData);
			qrCode = stockVerificationExcessData.getQrCode();
			stockVerificationAutoSubmit(qrCode);
			// sumOfTotalQtyAndValueForStockVerificationData();
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('stockVerficationExcessDialogVar').hide();");
			this.qrCode = new String();
		}
	}

	public void sumOfTotalQtyAndValueForStockVerificationData() {

		log.info("<<< -- Start the sumOfTotalQtyAndValueForStockVerificationData() -- >> ");

		try {

			int stockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("StockVerificationListSize :: " + stockVerificationListSize);

			if (stockVerificationListSize > 0) {

				Double totalBookedQty = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getBookedQty() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getBookedQty));

				Double totalExcessQty = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getExcessQty() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getExcessQty));

				Double totalDeficitQty = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getDeficitQty() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getDeficitQty));

				Double totalActualQty = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getActualQty() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getActualQty));

				Double totalBookedValue = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getBookedValue() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getBookedValue));

				Double totalExcessValue = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getExcessValue() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getExcessValue));

				Double totalDeficitValue = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getDeficitValue() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getDeficitValue));

				Double totalActualValue = inventoryItemsStockVerificationList.stream()
						.filter(ii -> ii.getActualValue() != null)
						.collect(Collectors.summingDouble(ExcessDeficitStockUploadDTO::getActualValue));

				bookedTotalQty = AppUtil.getFormattedNumber(totalBookedQty);

				excessTotalQty = AppUtil.getFormattedNumber(totalExcessQty);

				deficitTotalQty = AppUtil.getFormattedNumber(totalDeficitQty);

				actualTotalQty = AppUtil.getFormattedNumber(totalActualQty);

				if (totalBookedValue != null) {
					bookedTotalValue = AppUtil.formatIndianCommaSeparated(totalBookedValue.longValue());
				}

				if (totalExcessValue != null) {
					excessTotalValue = AppUtil.formatIndianCommaSeparated(totalExcessValue.longValue());
				}

				if (totalDeficitValue != null) {
					deficitTotalValue = AppUtil.formatIndianCommaSeparated(totalDeficitValue.longValue());
				}

				if (totalActualValue != null) {
					actualTotalValue = AppUtil.formatIndianCommaSeparated(totalActualValue.longValue());
				}

			}
		} catch (Exception ex) {
			log.error("Exception Occurred at sumOfTotalQtyAndValueForStockVerificationData() ", ex);
		}

		log.info("<<< -- End the sumOfTotalQtyAndValueForStockVerificationData() -- >> ");

	}

	public void stockVerificationSubmit() {
		BaseDTO response = null;
		StockVerification stockVerification = null;
		try {

			if (stockVerificationDto == null || stockVerificationDto.getStockDate() == null) {
				AppUtil.addWarn("Please select the  stock date");
				return;
			}

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("Stock Verification Save List Size ::: " + inventoryItemsStockVerificationListSize);

			String url = SERVER_URL + "/stockverificationnew/findStockVerficationByStockDate";
			log.info("   findStockVerficationByStockDate URL -------->> " + url);
			BaseDTO baseDTO = httpService.post(url, stockVerificationDto);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockVerification = mapper.readValue(jsonResponse, new TypeReference<StockVerification>() {
				});
			}
			if (stockVerification != null) {
				stockVerificationId = stockVerification.getId();
				stockVerificationDto.setStockVerificationId(stockVerificationId);
				log.info("Stock Verfication Id :: " + stockVerificationId);
			}
			stockVerificationDto.setStockVerificationList(inventoryItemsStockVerificationList);

			if (inventoryItemsStockVerificationListSize > 0) {

				String saveUrl = SERVER_URL + "/stockverificationnew/saveStockVerification";

				response = httpService.post(saveUrl, stockVerificationDto);

				if (response != null) {
					if (response.getStatusCode() == 0) {
						AppUtil.addInfo("Stock Verification saved successfully");
						checkStockVerificationClosingDate();
						// cancelStockVerification();
					} else {
						String errorMsg = response.getErrorDescription();
						// StringBuilder builder = new StringBuilder();
						// builder.append("Stock Verification Failed .");
						// if (errorMsg != null) {
						// builder.append("Error : " + errorMsg);
						// }
						// errorMsg = builder.toString();
						log.info(errorMsg);
						AppUtil.addWarn(errorMsg);
					}
				}
			} else {
				AppUtil.addWarn("Stock Verification List data not available");
				return;
			}

		} catch (Exception ex) {
			AppUtil.addError(ex.getMessage());
			log.error("Exception Occurred at stockVerificationSubmit()", ex);
		}
	}

	public void stockVerificationAutoSubmit(String qrCode) {
		BaseDTO response = null;
		try {

			if (stockVerificationDto == null || stockVerificationDto.getStockDate() == null) {
				AppUtil.addWarn("Please select the  stock date");
				return;
			}

			ProductGroupMaster productGroupMaster = stockVerificationDto.getProductGroupMaster();
			Long groupId = productGroupMaster == null ? null : productGroupMaster.getId();
			log.info("stockVerificationAutoSubmit() - groupId: " + groupId);

			ProductCategory productCategory = stockVerificationDto.getProductCategory();
			Long categoryId = productCategory == null ? null : productCategory.getId();
			log.info("stockVerificationAutoSubmit() - categoryId: " + categoryId);

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			log.info("Stock Verification Save List Size ::: " + inventoryItemsStockVerificationListSize);

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

			if (inventoryItemsStockVerificationListSize > 0) {

				ExcessDeficitStockUploadDTO stockVerificationDTOObj = inventoryItemsStockVerificationList.get(0);

				StockVerificationDTO stockVerificationDTO = new StockVerificationDTO();
				stockVerificationDTO.setQrCode(qrCode);
				stockVerificationDTO.setGroupId(groupId);
				stockVerificationDTO.setCategoryId(categoryId);
				stockVerificationDTO.setQuantity(qrCodeSystemQty);
				stockVerificationDTO.setStockDate(stockVerificationDto.getStockDate());
				stockVerificationDTO.setStockVerificationId(stockVerificationDTOObj.getVerificationId());
				stockVerificationDTO.setStockCloseDate(simpleDateFormat.format(stockVerificationDto.getStockDate()));

				String saveUrl = SERVER_URL + "/stockverificationnew/saveStockVerification";
				response = httpService.post(saveUrl, stockVerificationDTO);

				if (response != null) {
					Integer statusCode = response.getStatusCode();
					if (statusCode == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
						String openDialogVarName = response.getGeneralContent();
						if ("stockVerficationMeterDialogVar".equals(openDialogVarName)) {
							if (response.getResponseContent() != null) {
								ObjectMapper mapper = new ObjectMapper();
								String jsonResponses = mapper.writeValueAsString(response.getResponseContent());
								ExcessDeficitStockUploadDTO excessDeficitStockUploadDTO = mapper
										.readValue(jsonResponses, ExcessDeficitStockUploadDTO.class);

								this.stockVerificationData = excessDeficitStockUploadDTO;
								qrCodeSystemQty = AppUtil.ifNullRetunZero(excessDeficitStockUploadDTO.getBookedQty());
								Double bookedQty = AppUtil.ifNullRetunZero(excessDeficitStockUploadDTO.getBookedQty());
								if (bookedQty < 0) {
									AppUtil.addWarn("Stock not available.");
									return;
								}
								// actualQty = AppUtil.ifNullRetunZero(inventory.getActualQty());

								scannedQRCode = qrCode;
								RequestContext context = RequestContext.getCurrentInstance();
								context.execute("PF('stockVerficationMeterDialogVar').show();");
								context.update(":stockVerficationMeterDialog");
								context.update(":qrcodeID");
								context.update(":meterQtyID");
							}
						} else {

							updateSummaryValue(response);
						}
					} else {
						AppUtil.addWarn(response.getErrorDescription());
						return;
					}
				}
				
			} else {
				AppUtil.addWarn("Stock Verification List data not available");
				return;
			}

		} catch (Exception ex) {
			AppUtil.addError(ex.getMessage());
			log.error("Exception Occurred at stockVerificationSubmit()", ex);
		}
	}

	private void updateSummaryValue(BaseDTO response) {
		try {
			if (response.getStatusCode() == 0) {
				if (response.getResponseContent() != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponses = mapper.writeValueAsString(response.getResponseContent());
					ExcessDeficitStockUploadDTO excessDeficitStockUploadDTO = mapper.readValue(jsonResponses,
							ExcessDeficitStockUploadDTO.class);

					if (excessDeficitStockUploadDTO != null) {
						summaryBookedQty = AppUtil.getFormattedNumber(excessDeficitStockUploadDTO.getBookedQty());
						summaryBookedValue = AppUtil
								.formatIndianCommaSeparated(excessDeficitStockUploadDTO.getBookedValue());
						summaryExcessQty = AppUtil.getFormattedNumber(excessDeficitStockUploadDTO.getExcessQty());
						summaryExcessValue = AppUtil
								.formatIndianCommaSeparated(excessDeficitStockUploadDTO.getExcessValue());
						summaryDeficitQty = AppUtil.getFormattedNumber(excessDeficitStockUploadDTO.getDeficitQty());
						summaryDeficitValue = AppUtil
								.formatIndianCommaSeparated(excessDeficitStockUploadDTO.getDeficitValue());
						summaryActualQty = AppUtil.getFormattedNumber(excessDeficitStockUploadDTO.getActualQty());
						summaryActualValue = AppUtil
								.formatIndianCommaSeparated(excessDeficitStockUploadDTO.getActualValue());
					}
				}
			}
		} catch (Exception ex) {
			AppUtil.addError(ex.getMessage());
			log.error("Exception Occurred at updateSummaryValue()", ex);
		}
	}

	public void approveSubmit() {
		log.info("<<<---- Stock Verfication Approve Submit  Started --->>> ");
		try {

			if (stockVerificationDto == null || stockVerificationDto.getStockDate() == null) {
				AppUtil.addWarn("Please select the  stock date");
				return;
			}

			if (stockVerificationApprove == null) {
				AppUtil.addWarn("Please select the approve details ");
				return;
			}

			if (stockVerificationApprove.getVerifiedByName() == null
					|| stockVerificationApprove.getVerifiedByName().isEmpty()) {
				AppUtil.addWarn("Please enter the verifiedby name ");
				return;
			}

			if (stockVerificationApprove.getStockVerificationStartDate() == null) {
				AppUtil.addWarn("Please select the start date ");
				return;
			}

			if (stockVerificationApprove.getStockVerificationEndDate() == null) {
				AppUtil.addWarn("Please select the end date ");
				return;
			}

			if (stockVerificationApprove.getRemarks() == null || stockVerificationApprove.getRemarks().isEmpty()) {
				AppUtil.addWarn("Please enter the remarks ");
				return;
			}

			stockVerificationApprove.setStockVerificationClosingDate(stockVerificationDto.getStockDate());
			stockVerificationApprove.setStockDate(stockVerificationDto.getStockDate());
			stockVerificationApprove.setStockVerificationId(stockVerificationId);

			int inventoryItemsStockVerificationListSize = inventoryItemsStockVerificationList != null
					? inventoryItemsStockVerificationList.size()
					: 0;

			if (inventoryItemsStockVerificationListSize > 0) {

				String inventoryApproveURL = SERVER_URL + "/stockverificationnew/stockVerificationApprove";

				BaseDTO inventoryResponse = httpService.post(inventoryApproveURL, stockVerificationApprove);

				if (inventoryResponse != null) {
					if (inventoryResponse.getStatusCode() == 0) {
						cancelStockVerification();
						RequestContext context = RequestContext.getCurrentInstance();
						context.execute("PF('stockVerficationApproveDialogVar').hide();");
						AppUtil.addInfo(
								"Stock Verification completed successfully.Actual stock updated in the current inventory");
						return;
					} else {
						String errorMsg = inventoryResponse.getErrorDescription();
						StringBuilder builder = new StringBuilder();
						builder.append("Stock Verification Approve Inventory Update Failed.");
						// if (errorMsg != null) {
						// builder.append("Error : " + errorMsg);
						// }
						errorMsg = builder.toString();
						log.info(errorMsg);
						AppUtil.addWarn(errorMsg);
						return;
					}
				}

			} else {
				AppUtil.addWarn("Stock Verification details not found");
				return;
			}
		} catch (Exception ex) {
			log.error("Exception Occurred at Stock Verification approveSubmit() ..", ex);
		}

		log.info("<<<---- Stock Verfication Approve Submit  Ended --->>> ");

	}

	public void cancelStockVerification() {
		stockVerificationExcessData = new ExcessDeficitStockUploadDTO();
		stockVerificationData = new ExcessDeficitStockUploadDTO();
		stockVerificationApprove = new StockVerificationDTO();
		inventoryItemsStockVerificationList = new ArrayList<>();
		stockVerificationId = null;
		qrCodeSystemQty = 0.00;
		physicalQuantity = 0.00;
		scannedQRCode = null;
		closingDate = null;
		bookedTotalQty = null;
		excessTotalQty = null;
		deficitTotalQty = null;
		actualTotalQty = null;
		bookedTotalValue = null;
		excessTotalValue = null;
		deficitTotalValue = null;
		actualTotalValue = null;
		summaryBookedQty = null;
		summaryBookedValue = null;
		summaryExcessQty = null;
		summaryExcessValue = null;
		summaryDeficitQty = null;
		summaryDeficitValue = null;
		summaryActualQty = null;
		summaryActualValue = null;
		stockVerificationDto.setProductCategory(null);
		stockVerificationDto.setProductGroupMaster(null);
	}

	public void cancelDialog() {

		qrCode = new String();
		qrCodeSystemQty = null;
		physicalQuantity = null;
		scannedQRCode = null;

	}

	public void clearSearch() {
		qrCode = new String();
		qrCodeSystemQty = null;
		physicalQuantity = null;
		scannedQRCode = null;
		stockVerificationDto = new StockVerificationDTO();
		stockVerificationExcessData = new ExcessDeficitStockUploadDTO();
		stockVerificationData = new ExcessDeficitStockUploadDTO();
		stockVerificationApprove = new StockVerificationDTO();
		inventoryItemsStockVerificationList = new ArrayList<>();
		stockVerificationId = null;
		qrCodeSystemQty = 0.00;
		physicalQuantity = 0.00;
		scannedQRCode = null;
		closingDate = null;
		bookedTotalQty = null;
		excessTotalQty = null;
		deficitTotalQty = null;
		actualTotalQty = null;
		bookedTotalValue = null;
		excessTotalValue = null;
		deficitTotalValue = null;
		actualTotalValue = null;
		summaryBookedQty = null;
		summaryBookedValue = null;
		summaryExcessQty = null;
		summaryExcessValue = null;
		summaryDeficitQty = null;
		summaryDeficitValue = null;
		summaryActualQty = null;
		summaryActualValue = null;
		checkStockVerificationClosingDate();
	}

}
