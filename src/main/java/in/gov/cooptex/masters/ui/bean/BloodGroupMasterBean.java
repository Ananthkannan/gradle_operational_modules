package in.gov.cooptex.masters.ui.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BloodGroupMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("bloodGroupMasterBean")
public class BloodGroupMasterBean {

	private final String CREATE_BLOOD_GROUP_PAGE = "/pages/masters/bloodGroup/addBloodGroup.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/bloodGroup/listBloodGroup.xhtml?faces-redirect=true;";
	final String SERVER_URL = AppUtil.getPortalServerURL();
	final String BLOODGROUP_URL = AppUtil.getPortalServerURL() + "/bloodgroupmaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	BloodGroupMaster bloodGroupMaster, selectedBloodGroupMaster;

	@Getter
	@Setter
	LazyDataModel<BloodGroupMaster> lazyBloodGroupMasterList;

	List<BloodGroupMaster> bloodGroupMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	/*
	 * showBloodGroupListPage method used to load all details in starting master
	 * page
	 */
	public String showBloodGroupListPage() {
		log.info("<==== Starts BloodGroupMasterBean.showBloodGroupListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		bloodGroupMaster = new BloodGroupMaster();
		selectedBloodGroupMaster = new BloodGroupMaster();
		loadLazyBloodGroupList();
		log.info("<==== Ends BloodGroupMasterBean.showBloodGroupListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	/* loadLazyBloodGroupList method used to load list using lazy loading */
	public void loadLazyBloodGroupList() {
		log.info("<===== Starts BloodGroupMasterBean.loadLazyBloodGroupList ======>");
		lazyBloodGroupMasterList = new LazyDataModel<BloodGroupMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<BloodGroupMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = BLOODGROUP_URL + "/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						bloodGroupMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<BloodGroupMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in loadLazyBloodGroupList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return bloodGroupMasterList;
			}

			@Override
			public Object getRowKey(BloodGroupMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BloodGroupMaster getRowData(String rowKey) {
				try {
					for (BloodGroupMaster bloodGroup : bloodGroupMasterList) {
						if (bloodGroup.getId().equals(Long.valueOf(rowKey))) {
							selectedBloodGroupMaster = bloodGroup;
							return bloodGroup;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends BloodGroupMasterBean.loadLazyBloodGroupList ======>");
	}

	/*
	 * showBloodGroupListPageAction method used to action based view pages and
	 * render viewing
	 */
	public String showBloodGroupMasterListPageAction() {
		log.info("<===== Starts BloodGroupMasterBean.showBloodGroupMasterListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedBloodGroupMaster == null || selectedBloodGroupMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ANY_ONE_BLOODGROUP.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Blood Group Master called..");
				bloodGroupMaster = new BloodGroupMaster();
				return CREATE_BLOOD_GROUP_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit Blood Group Master called..");
				String url = BLOODGROUP_URL + "/update/" + selectedBloodGroupMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					bloodGroupMaster = mapper.readValue(jsonResponse, new TypeReference<BloodGroupMaster>() {
					});
					return CREATE_BLOOD_GROUP_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Blood Group Master called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBloodGroupDelete').show();");
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showBloodGroupMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends BloodGroupMasterBean.showBloodGroupMasterListPageAction ===========>" + action);
		return null;
	}

	/* saveOrUpdate method used to save and update in HolidatMaster entity */
	public String saveOrUpdate() {
		log.info("<==== Starts BloodGroupMasterBean.saveOrUpdate =======>");
		try {

			if (bloodGroupMaster.getName() == null || bloodGroupMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BLOOD_GROUP_MASTER_NAME_EMPTY.getCode());
				return null;
			}
			if (bloodGroupMaster.getName().trim().length() < 2 || bloodGroupMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.BLOOD_GROUP_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (action.equalsIgnoreCase("CREATE")) {
				bloodGroupMaster.setCreatedBy(loginBean.getUserDetailSession());
				bloodGroupMaster.setCreatedDate(new Date());
			} else if (action.equalsIgnoreCase("EDIT")) {
				bloodGroupMaster.setModifiedBy(loginBean.getUserDetailSession());
				bloodGroupMaster.setModifiedDate(new Date());
				if (bloodGroupMaster.getActiveStatus() == null) {
					errorMap.notify(ErrorDescription.BLOOD_GROUP_MASTER_STATUS_EMPTY.getCode());
					return null;
				}
			}
			String url = BLOODGROUP_URL + "/saveorupdate";
			BaseDTO response = httpService.put(url, bloodGroupMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("Blood Group Master saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.BLOOD_GROUP_MASTER_SAVE_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.BLOOD_GROUP_MASTER_UPDATE_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends BloodGroupMasterBean.saveOrUpdate =======>");
		return showBloodGroupListPage();
	}

	/*
	 * onRowSelect method used to select particular row entity details in
	 * listbloodgroup page
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts BloodGroupMasterBean.onRowSelect ========>" + event);
		selectedBloodGroupMaster = ((BloodGroupMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends BloodGroupMasterBean.onRowSelect ========>");
	}

	/*
	 * deleteConfirm method used to delete particular row entity details in
	 * listbloodgroup page
	 */
	public String deleteConfirm() {
		log.info("<===== Starts BloodGroupMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(BLOODGROUP_URL + "/delete/" + selectedBloodGroupMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.BLOOD_GROUP_MASTER_DELETED_SUCCESSFULLY.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBloodGroupMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Blodd Group Master....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends BloodGroupMasterBean.deleteConfirm ===========>");
		return showBloodGroupListPage();
	}
}