package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ThreadLocalConfig;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.InfoCodeDescription;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Abhishek
 *
 */
@Log4j2
@Service
@Scope("session")
public class ProductCatagoryMasterBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	Long id;

	@Setter
	@Getter
	String productCatCode;

	@Setter
	@Getter
	String productCatName;

	@Getter
	@Setter
	String buttonName;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	String regionalName;

	RestTemplate restTemplate;

	String Url = null;

	Integer numberOfRecords = 0;
	@Setter
	@Getter
	List<ProductCategoryDTO> productCategoryListDto = new ArrayList<ProductCategoryDTO>();
	@Setter
	@Getter
	List<ProductCategoryDTO> productCategoryList = new ArrayList<ProductCategoryDTO>();

	@Setter
	@Getter
	ProductCategoryDTO productCategoryDto = new ProductCategoryDTO();

	@Setter
	@Getter
	WrapperDTO wrapperDto = new WrapperDTO();

	@Autowired
	LoginBean loginBean;

	@Setter
	@Getter
	boolean disable;

	@Autowired
	LanguageBean languageBean;

	@Setter
	@Getter
	private Boolean activeStatus = true;
	@Getter
	@Setter
	String statusValue = null;

	public ProductCatagoryMasterBean() {
		restTemplate = new RestTemplate();
		log.info("Inside ProductCatagoryMasterBean");
		loadValues();
	}

	private void loadValues() {
		try {			
			Url = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {
		getAllCategory();
	}

	/**
	 * 
	 */

	public String showProductCategory() {
		log.info("ProductCatagoryMasterBean  showProductCategory================#Start");
		action = "ADD";
		productCategoryDto = new ProductCategoryDTO();
		productCatCode = null;
		productCatName = null;
		regionalName = null;
		statusValue = null;
		productCategoryDto = null;

		getAllCategory();
		disable = false;
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	public void getAllCategory() {
		log.info("ProductCatagoryMasterBean  getAllCategory================#Start");
		RestTemplate restTemplate = new RestTemplate();
		productCategoryListDto = new ArrayList<ProductCategoryDTO>();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Track_Id", ThreadLocalConfig.getId());
			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			HttpEntity<WrapperDTO> response = restTemplate.exchange(Url + "/category/getAll", HttpMethod.GET, entity,
					WrapperDTO.class);
			log.info("<----Url--->" + Url);
			wrapperDto = response.getBody();
			log.info("Wrapper Dto :" + response.getBody());
			productCategoryListDto = (List<ProductCategoryDTO>) wrapperDto.getProductCategoryDTOCollections();
			productCategoryList = new ArrayList<ProductCategoryDTO>(productCategoryListDto);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * @return
	 */
	public String submitCtegory() {
		log.info("ProductCatagoryMasterBean  submitCtegory()=============#Start");
		try {
			if (buttonName.equals("Add")) {
				disable = false;

				if (productCatName.trim().length() < 3) {
					log.info(productCatName.length());
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.CATEGORY_NAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					log.info("Name should be greater than 3 characters");
					return null;
				}
				if (regionalName.trim().length() < 3) {
					log.info(productCatName.length());
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.CATEGORY_LOCAL_NAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					return null;
				}

				UserDTO userDto = new UserDTO();
				userDto.setId(loginBean.getUserDetailSession().getId());

				log.info("user id" + userDto.getId());

				log.info("button name-->" + buttonName.toString());
				ProductCategoryDTO productCategoryDto = new ProductCategoryDTO();
				restTemplate = new RestTemplate();
				productCategoryDto.setActiveStatus(activeStatus);
				productCategoryDto.setProductCatCode(productCatCode.trim());
				productCategoryDto.setProductCatName(productCatName.trim());
				productCategoryDto.setRegionalName(regionalName.trim());
				productCategoryDto.setCreatedBy(userDto);
				// productCategoryDto.setModifiedBy(userDto);
				// productCategoryDto.setModifiedDate(new Date());
				productCategoryDto.setCreatedDate(new Date());

				log.info(productCategoryDto.getRegionalName());
				log.info(productCategoryDto.getCreatedDate());

				log.info("productCatCode" + productCategoryDto.getProductCatCode());
				HttpHeaders headers = new HttpHeaders();
				headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
				headers.set("Track_Id", ThreadLocalConfig.getId());
				HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDto,
						loginBean.headers);
				HttpEntity<BaseDTO> response = restTemplate.exchange(Url + "/category/create", HttpMethod.POST,
						requestEntity, BaseDTO.class);
				BaseDTO basedto = response.getBody();
				log.info("BaseDto :" + response.getBody());
				if (basedto != null) {
					log.info("Status code is:" + basedto.getStatusCode());
					if (basedto.getStatusCode() == 0) {
						// String message = "product Category is Added";
						log.info("Status code is:" + basedto.getStatusCode());

						Util.addInfo(InfoCodeDescription.getDescription(
								InfoCodeDescription.PROD_CAT_ADDED.getInfoCode(), languageBean.getLocaleCode()));
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					} else {
						String message = ErrorCodeDescription.getDescription(basedto.getStatusCode());
						log.error("Category code:" + basedto.getStatusCode() + " Error Message: " + message);
						// Util.addInfo(message);
						Util.addError(ErrorCodeDescription.getDescription(basedto.getStatusCode(),
								languageBean.getLocaleCode()));
						log.error("Status code:" + basedto.getStatusCode() + " Error Message: " + message);
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						return null;

					}
				}
				getAllCategory();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		productCategoryDto = null;
		productCatName = null;
		productCatCode = null;
		regionalName = null;
		activeStatus = true;
		log.info("ProductCatagoryMasterBean  submitCtegory()=============#End");
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	/**
	 * @this method is used to search category
	 * @return
	 */
	public String search() {
		log.info("productCtegory search=============#Start");
		action = "ADD";
		try {
			if (buttonName.equals("Search")) {
				log.info("---button---->" + buttonName);
				if (Util.isEmpty(productCatCode) && Util.isEmpty(productCatName) && Util.isEmpty(regionalName)
						&& (statusValue == null || StringUtils.isEmpty(statusValue))) {

					Util.addWarn(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ENTER_ATLEAST_ONE_DETAIL.getErrorCode(),
							languageBean.getLocaleCode()));

					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					// return null;

					log.info("Select one fields");
					productCatName = null;
					productCatCode = null;
					regionalName = null;

					return null;

				}

				restTemplate = new RestTemplate();
				productCategoryDto = new ProductCategoryDTO();
				productCategoryDto.setProductCatCode(productCatCode);
				productCategoryDto.setProductCatName(productCatName);
				productCategoryDto.setRegionalName(regionalName);

				log.info("product category code" + productCategoryDto.getProductCatCode());
				log.info("product category Name" + productCategoryDto.getProductCatName());
				log.info("product category regionalName" + productCategoryDto.getRegionalName());
				if (statusValue != null) {
					if ("true".equals(statusValue)) {

						activeStatus = true;
						productCategoryDto.setActiveStatus(activeStatus);
					} else if ("false".equals(statusValue)) {

						activeStatus = false;
						productCategoryDto.setActiveStatus(activeStatus);
					}
				}
				productCategoryDto.setActiveStatus(activeStatus);
				HttpHeaders headers = new HttpHeaders();
				headers.set("Track_Id", ThreadLocalConfig.getId());
				HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDto,
						loginBean.headers);
				HttpEntity<WrapperDTO> response = restTemplate.exchange(Url + "/category/search", HttpMethod.POST,
						requestEntity, WrapperDTO.class);
				log.info("--Response" + response);
				wrapperDto = response.getBody();
				log.info("--wrapperDto--" + response.getBody());
				numberOfRecords = 0;
				if (wrapperDto != null) {
					if (wrapperDto.getProductCategoryDTOCollections() != null) {
						productCategoryListDto = (List<ProductCategoryDTO>) wrapperDto
								.getProductCategoryDTOCollections();
						numberOfRecords = productCategoryListDto.size();
						log.info("In search product category" + productCategoryListDto.size());
					} else {
						String message = ErrorCodeDescription.getDescription(wrapperDto.getStatusCode());

						log.info("<-----Data not available---->");

						productCategoryListDto = null;

						Util.addError(ErrorCodeDescription.getDescription(wrapperDto.getStatusCode(),
								languageBean.getLocaleCode()));
						log.error("Status code:" + wrapperDto.getStatusCode() + " Error Message: " + message);
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						return null;

					}
					if (productCategoryListDto != null) {
						productCategoryList = new ArrayList<ProductCategoryDTO>(productCategoryListDto);

						log.info("No of Records" + productCategoryList.size());
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// productCategoryDto = null;
		// productCatCode = null;
		// productCatName = null;
		// regionalName = null;
		log.info("productCtegory search=============#End");
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	/**
	 * update the fields data
	 */
	public String updateData() {
		log.info("productCtegory updateData===============#Start");
		try {

			if (Util.isEmpty(productCatCode) && Util.isEmpty(productCatName) && Util.isEmpty(regionalName)) {

				log.info("Select one atleast field ");

				productCatName = null;
				productCatCode = null;
				regionalName = null;

				return null;

			}

			if (productCatName.trim().length() < 3) {
				log.info(productCatName.length());
				Util.addError(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.CATEGORY_NAME_VALID_LENGTH.getErrorCode(), languageBean.getLocaleCode()));
				log.info("Name should be greater than 3 characters");
				return null;
			}
			if (regionalName.trim().length() < 3) {
				log.info(productCatName.length());
				Util.addError(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.CATEGORY_LOCAL_NAME_VALID_LENGTH.getErrorCode(),
						languageBean.getLocaleCode()));
				return null;
			}

			UserDTO userDto = new UserDTO();
			userDto.setId(loginBean.getUserDetailSession().getId());
			log.info("user id" + userDto.getId());
			log.info("---productCatagoryMasterBean  --->updateData()------> ");
			productCategoryDto.setProductCatCode(productCatCode.trim());
			productCategoryDto.setProductCatName(productCatName.trim());
			productCategoryDto.setRegionalName(regionalName.trim());
			productCategoryDto.setActiveStatus(activeStatus);
			productCategoryDto.setModifiedBy(userDto);
			productCategoryDto.setModifiedDate(new Date());

			// productCategoryDto.setCreatedBy(userDto);
			// log.info(productCategoryDto.getProductCatCode());

			// log.info(productCategoryDto.getProductCatName());
			String url = Url + "/category/update";
			HttpHeaders headers = new HttpHeaders();
			headers.set("Track_Id", ThreadLocalConfig.getId());
			HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDto,
					loginBean.headers);
			HttpEntity<BaseDTO> productResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					BaseDTO.class);
			BaseDTO baseDto = productResponse.getBody();
			log.info("-Update-BaseDto Object--  " + baseDto);
			if (baseDto != null) {
				if (baseDto.getStatusCode() == 0) {
					log.info("Data succesfully updated ");

					Util.addInfo(InfoCodeDescription.getDescription(InfoCodeDescription.PROD_CAT_UPDATE.getInfoCode(),
							languageBean.getLocaleCode()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					action = "ADD";

				} else {
					String message = ErrorCodeDescription.getDescription(baseDto.getStatusCode());
					log.error("Category code:" + baseDto.getStatusCode() + " Error Message: " + message);
					// Util.addInfo(message);
					Util.addError(
							ErrorCodeDescription.getDescription(baseDto.getStatusCode(), languageBean.getLocaleCode()));
					log.error("Status code:" + baseDto.getStatusCode() + " Error Message: " + message);
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					return null;
				}
			}
			getAllCategory();
		} catch (Exception e) {
			e.printStackTrace();
		}
		disable = false;
		productCategoryDto = null;
		productCatName = null;
		productCatCode = null;
		regionalName = null;

		log.info("productCtegory updateData===============#End");
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	public String updateFormData() {
		action = "EDIT";
		if (action.equals("EDIT")) {
			log.info("productCtegory updateFormData===============#Start");
			// action = "EDIT";
			disable = true;
			log.info("===productCategoryDto" + productCategoryDto);
			if (productCategoryDto == null) {
				Util.addError(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.SELECT_ANY_PRODUCT_CATEGORY.getErrorCode(), languageBean.getLocaleCode()));
				return null;
			}

			log.info("--- Selected Id is --->" + productCategoryDto.getId());
			log.info("--- Selected code is --->" + productCategoryDto.getProductCatCode());
			// productCategoryDto = new ProductCategoryDTO();
			// productCategoryDto.setId(productCategoryDTO.getId());
			productCatCode = productCategoryDto.getProductCatCode();
			productCatName = productCategoryDto.getProductCatName();
			regionalName = productCategoryDto.getRegionalName();
			activeStatus = productCategoryDto.getActiveStatus();
			log.info("code=====" + productCategoryDto.getProductCatCode());
			log.info("name=====" + productCategoryDto.getProductCatName());
			log.info("lname=====" + productCategoryDto.getRegionalName());
			productCategoryDto.setCreatedBy(productCategoryDto.getCreatedBy());
			productCategoryDto.setCreatedDate(productCategoryDto.getCreatedDate());

		}

		log.info("productCtegory updateFormData===============#End");
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	public String showViewPages() {

		if (action.equals("VIEW")) {
			if (productCategoryDto == null) {
				Util.addError(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.SELECT_ANY_PRODUCT_CATEGORY.getErrorCode(), languageBean.getLocaleCode()));
				return null;
			}

			updateFormData();
			log.info("Edit Religion is :");

		}
		return "/pages/master/viewProductCategory.xhtml?faces-redirect=true";
	}

	/**
	 * @param productCategoryDTO
	 * @return
	 */

	public String deleteAction(ProductCategoryDTO productCategoryDTO) {
		log.info("<---deleteAction()-->");
		BaseDTO baseDto = new BaseDTO();
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.set("Track_Id", ThreadLocalConfig.getId());
			String url = Url + "/category/delete";
			HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDTO,
					loginBean.headers);
			HttpEntity<BaseDTO> productResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					BaseDTO.class);
			baseDto = productResponse.getBody();
			log.info("BaseDto :" + productResponse.getBody());

			if (baseDto != null) {
				if (baseDto.getStatusCode() == 0) {
					Util.addWarn("Deleted Successfully");
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				} else {
					String msg = "This category Can't be deleted";
					Util.addWarn(msg);
					log.error("Status code:" + baseDto.getStatusCode() + " Error Message: " + msg);
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					return null;
				}
			}
			getAllCategory();

		} catch (Exception e) {

			e.printStackTrace();
		}

		productCategoryDto = null;
		productCatName = null;
		productCatCode = null;
		regionalName = null;

		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";

	}

	/**
	 * @clear the fields
	 * @return
	 */
	public String clear() {
		log.info("productCtegory clear===============#Start");
		disable = false;
		productCatCode = null;
		productCatName = null;
		regionalName = null;
		productCategoryDto = null;
		action = "ADD";

		// getAllCategory();
		showProductCategory();
		log.info("productCtegory clear===============#End");
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	public String backPage() {

		showProductCategory();
		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}

	public String showAddPage() {
		if (action.equals("ADD")) {
			productCategoryDto = new ProductCategoryDTO();
		} else if (action.equals("EDIT")) {
			if (productCategoryDto == null) {
				Util.addError(ErrorCodeDescription.getDescription(
						ErrorCodeDescription.SELECT_ANY_PRODUCT_CATEGORY.getErrorCode(), languageBean.getLocaleCode()));
				return null;
			}
			updateFormData();
		}

		return "/pages/master/createProductCategory.xhtml?faces-redirect=true";
	}

	public String cancel() {
		showProductCategory();

		return "/pages/master/listProductCategory.xhtml?faces-redirect=true";
	}
}