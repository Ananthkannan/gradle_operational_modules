package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CurrentStockStatusRequest;
import in.gov.cooptex.core.dto.ItemDistributionDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ItemDistribution;
import in.gov.cooptex.core.model.ItemDistributionDetails;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.enums.StockItemOutWardType;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.intend.model.IntendRequest;
import in.gov.cooptex.operation.intend.model.StockItemInwardPNSDTO;
import in.gov.cooptex.operation.intend.model.StockItemOutwardPNSDTO;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardBeanPNS")
@Scope("session")
public class StockOutwardBeanPNS {

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;
	
	@Autowired
	CommonDataService commonDataService;
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	String url, jsonResponse;
	
	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = new ArrayList<>();
	
	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();
	
	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();
	
	@Getter
	@Setter
	EntityMaster entity = null;
	
	@Getter
	@Setter
	List<SectionMaster> sectionMasterList = new ArrayList<>();
	
	@Getter
	@Setter
	List<TransportMaster> transportMasterList = new ArrayList<>();
	
	@Getter
	@Setter
	TransportMaster transportMaster = new TransportMaster();
	
	@Getter
	@Setter
	SectionMaster sectionMaster = null;
	
	@Getter
	@Setter
	StockTransfer stockTransfer = new StockTransfer();
	
	Set<String> setOfBundleNumbers = new HashSet<>();
	
	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	CurrentStockStatusRequest currentStockStatusRequest;

	@Getter
	@Setter
	boolean disableBtnProductAdd = true;
	
	@Getter
	@Setter
	boolean disableAddProduct = true;
	
	@Getter
	@Setter
	StockTransferItems stockTransferItems;
	
	@Getter
	@Setter
	List<String> transferChangeTypeList = new ArrayList<>();
	
	@Getter
	@Setter
	List<IntendRequest> intendRequestList = new ArrayList<>();
	
	@Getter
	@Setter
	List<ItemDistributionDTO> itemDistributionList = new ArrayList<>();
	
	@Getter
	@Setter
	IntendRequest intendRequest = new IntendRequest();
	
	@Getter
	@Setter
	String pageAction,entityTypeVal = "ent";
	
	@Getter
	@Setter
	ItemDistribution itemDistribution = new ItemDistribution();
	
	@Getter
	@Setter
	LazyDataModel<ItemDistribution> lazyItemDistributionList;
	
	@Getter	@Setter
	LazyDataModel<StockItemOutwardPNSDTO> lazyStockItemOutwardPNSDTOList;
	
	@Getter	@Setter
	List<StockTransferItems> stockTransferItemsDeleteList;
	
	@Getter
	@Setter
	private ItemDistribution selectedItemDistribution = new ItemDistribution();
	
	@Getter
	@Setter StockItemOutwardPNSDTO selectedItemOutwardPNSDTO;
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	String type;
	
	@Getter
	@Setter
	StockItemOutwardPNSDTO stockItemOutwardPNSDTO;
	
	@Getter	@Setter
	String transportServiceType;
	
	@Getter	@Setter
	String transportServiceName;
	
	@Getter	@Setter
	String wayBillAvailable;
	
	@Getter	@Setter
	String wayBillNumber;
	
	@Getter	@Setter
	String transportChargeAvailble;
	
	@Getter	@Setter
	String transportChargeType;
	
	@Getter	@Setter
	double transchargeChargeAmount;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	EmployeeMaster loginEmployee;
	
	@Getter	@Setter
	List<TransportTypeMaster> transportTypeMasterList;
	
	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<Object> payToPayList;
	
	@Getter	@Setter
	List<TransportMaster> transportServiceMasterList;
	
	private StockTransferUtility stockTransferUtility = new StockTransferUtility();
	
	@Getter	@Setter
	boolean waybillRendered;
	
	@Getter	@Setter
	boolean transportChargeRendered;
		
	@Getter	@Setter
	boolean editFlag;
	
	@Getter	@Setter
	boolean viewFlag;
	
	@Getter	@Setter
	boolean addFlag;
	
	@Getter	@Setter
	TransportTypeMaster selectedTransportTypeMaster;
	
	@Getter	@Setter
	TransportMaster selectedTransportMaster;
	
	@Getter	@Setter
	Double transportChargeAmount;
	
	@Autowired
	AppPreference appPreference;
	
	@Getter	@Setter
	StockItemOutwardPNSDTO selectedStockItemOutwardPNSDTO;
	
	@Getter	@Setter
	StockTransfer selectedStockTransferPNS;
	
	@Getter	@Setter
	Boolean withRequest;
	
	@Getter	@Setter
	List<EntityMaster> showroomList;
	
	@Getter @Setter
	List<String> statusList;
	
	@Getter @Setter
	Boolean stockOutwardPNSCategory;
	
	@Getter @Setter
	List<StockTransferItems> stockTransferItemsListUnitrate = new ArrayList<>();
	
	@Getter @Setter
	Double unitRate;
	
	@PostConstruct
	public void init() {
		log.info(".......StockOutwardBeanPNS Init is executed.....................");
		mapper = new ObjectMapper();
		waybillRendered = false;wayBillAvailable="Select Waybill Type";
		transportChargeAvailble="Select Type";
		editFlag = false;
		viewFlag = false;
		addFlag = true;
		stockOutwardPNSCategory = true;
		withRequest = true;
		stockTransfer=new StockTransfer();
		statusList = new ArrayList<>();
		showroomList = new ArrayList<>();
		statusList.add(StockTransferStatus.SUBMITTED.toString());
		statusList.add(StockTransferStatus.INITIATED.toString());
		stockTransferItemsDeleteList=new ArrayList<>();
		loadStockOutwardInfo();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public void loadStockOutwardInfo() {
		log.info(".......StockOutwardBeanPNS loadStockOutwardInfo is executed.....................");
		try {
			//entityTypeMasterList = commonDataService.loadEntityTypes();
			entityTypeMasterList = commonDataService.findAllEntityTypes(); //Active entity type only get before this get all entity
			transportMasterList = commonDataService.loadAllTransport();
			intendRequestList = commonDataService.loadAllIntendRequest();
			//loadTransferChangeType();
			
		}catch(Exception e) {
			log.error("Exception in StockOutwardBeanPNS  :: loadStockOutwardInfo ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	public void loadTransferChangeType() {
		log.info(".......StockOutwardBeanPNS loadTransferChangeType is executed.....................");
		try {
				for (TransportChargeType type : TransportChargeType.values()) {
					 transferChangeTypeList.add(type.name());
				}
			 
			  log.info("loadTransferChangeType transferChangeTypeList==>"+transferChangeTypeList);
			
		}catch(Exception e) {
			log.error("Exception in StockOutwardBeanPNS  :: loadTransferChangeType ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	public String redirectSVInProgressPage() {
		if(loginBean.getStockVerificationIsInProgess()) {
			loginBean.setMessage("Outward");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}
	
	public String showItemOutward() {
		log.info(".......StockOutwardBeanPNS showItemOutward is executed.....................");
		try {
			if ("ADD".equals(pageAction) || "EDIT".equals(pageAction)) {
				redirectSVInProgressPage();
			}
			
			
			if(pageAction.equals("LIST")) {
				loadLazyStockOutwardPNSList();
				return "/pages/printingAndStationary/listStockItemOutwardPNS.xhtml?faces-redirect=true";
			}else if(pageAction.equals("ADD")) {
				stockOutwardPNSCategory = true;
				stockTransfer =new StockTransfer();
				stockTransferItems=new StockTransferItems();
				currentStockStatusRequest = new CurrentStockStatusRequest();
				stockItemOutwardPNSDTO = new StockItemOutwardPNSDTO();
				unitRate=null;
				type=null;
				clearStockItemInword();
				itemDistribution = new ItemDistribution();
				loadTransportTypeMasterList();
				sectionMaster = new SectionMaster();
				entity = new EntityMaster();
				return "/pages/printingAndStationary/createStockItemOutwardPNS.xhtml?faces-redirect=true";
			}else if(pageAction.equals("EDIT")) {
				sectionMaster = new SectionMaster();
				entity = new EntityMaster();
				stockOutwardPNSCategory = false;
				return showViewPageStockTransfer();
			
			}else if(pageAction.equals("VIEW")) {
				sectionMaster = new SectionMaster();
				entity = new EntityMaster();
				return showViewPageStockTransfer(); 

			}else if(pageAction.equals("DELETE")) {
				if (selectedItemDistribution == null || selectedItemDistribution.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return null;
			}
		}catch(Exception e) {
			log.error("Exception in StockOutwardBeanPNS  :: loadStockOutwardInfo ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	//lazy search
	public void loadLazyStockOutwardPNSList() {
		log.info("<--- loadLazyStockTransfer RequirementList ---> ");
		lazyStockItemOutwardPNSDTOList = new LazyDataModel<StockItemOutwardPNSDTO>() {
//				StockTransfer ssss =new StockTransfer();
			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<StockItemOutwardPNSDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockItemOutwardPNSDTO> data = new ArrayList<StockItemOutwardPNSDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<StockItemOutwardPNSDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyStockTransfer RequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(StockItemOutwardPNSDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockItemOutwardPNSDTO getRowData(String rowKey) {
				List<StockItemOutwardPNSDTO> responseList = (List<StockItemOutwardPNSDTO>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (StockItemOutwardPNSDTO res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							selectedItemOutwardPNSDTO = res;
							return res;
						}
					}
				}catch(Exception see) {}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		selectedItemOutwardPNSDTO = new StockItemOutwardPNSDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		StockItemOutwardPNSDTO yarnRequirement = new StockItemOutwardPNSDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/searchData";
			URL = SERVER_URL + "/stockOutwardPS/lazyload/searchData";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
//	public void loadLazyStockOutwardPNSList() {
//
//		lazyStockItemOutwardPNSDTOList = new LazyDataModel<StockItemOutwardPNSDTO>() {
//
//			private static final long serialVersionUID = -9122090322809059767L;
//
//			List<StockItemOutwardPNSDTO> dataList = new ArrayList<StockItemOutwardPNSDTO>();
//
//			@Override
//			public List<StockItemOutwardPNSDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
//					Map<String, Object> filters) {
//
//				StockItemOutwardPNSDTO request = new StockItemOutwardPNSDTO();
//
//				try {
//
//					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
//							pageSize, sortField, sortOrder, filters);
//
//					request.setPaginationDTO(
//							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));
//
//					log.info("Search request data" + request);
//					String url = SERVER_URL + "/stockOutwardPS/lazyload/searchData";
//
//					log.info("url==>" + url);
//
//					BaseDTO baseDTO = httpService.post(url, request);
//
//					if (baseDTO == null) {
//						log.error(" Base DTO returned Null Values ");
//						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//						return null;
//					}
//
//					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//					dataList = mapper.readValue(jsonResponse, new TypeReference<List<StockItemOutwardPNSDTO>>() {
//					});
//					if (dataList == null) {
//						log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
//						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//					}
//					if (baseDTO.getStatusCode() == 0) {
//
//						this.setRowCount(baseDTO.getTotalRecords());
//						totalRecords = baseDTO.getTotalRecords();
//						log.info(":: totalRecords ::" + totalRecords);
//					} else {
//						log.error(":: Employee Loan and Advance Search Failed With status code :: "
//								+ baseDTO.getStatusCode());
//						errorMap.notify(baseDTO.getStatusCode());
//					}
//					return dataList;
//				} catch (Exception e) {
//					log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
//					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//				}
//				return null;
//			}
//
//			@Override
//			public Object getRowKey(StockItemOutwardPNSDTO res) {
//				return res != null ? res.getId() : null;
//			}
//
//			@Override
//			public StockItemOutwardPNSDTO getRowData(String rowKey) {
//				List<StockItemOutwardPNSDTO> responseList = (List<StockItemOutwardPNSDTO>) getWrappedData();
//				Long value = Long.valueOf(rowKey);
//				for (StockItemOutwardPNSDTO res : responseList) {
//					if (res.getId().longValue() == value.longValue()) {
//						selectedItemOutwardPNSDTO = res;
//						return res;
//					}
//				}
//				return null;
//			}
//		};
//	}
	
	//secondList
//	public void loadLazyStockTransferOutwardPNSList() {
//
//		lazyItemDistributionList = new LazyDataModel<ItemDistribution>() {
//
//			private static final long serialVersionUID = -9122090322809059767L;
//
//			List<ItemDistribution> dataList = new ArrayList<ItemDistribution>();
//
//			@Override
//			public List<ItemDistribution> load(int first, int pageSize, String sortField, SortOrder sortOrder,
//					Map<String, Object> filters) {
//
//				ItemDistribution request = new ItemDistribution();
//
//				try {
//
//					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
//							pageSize, sortField, sortOrder, filters);
//
//					request.setPaginationDTO(
//							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));
//
//					log.info("Search request data" + request);
//					String url = SERVER_URL + "/stockOutwardPS/lazyload/search";
//
//					log.info("url==>" + url);
//
//					BaseDTO baseDTO = httpService.post(url, request);
//
//					if (baseDTO == null) {
//						log.error(" Base DTO returned Null Values ");
//						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//						return null;
//					}
//
//					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//					dataList = mapper.readValue(jsonResponse, new TypeReference<List<ItemDistribution>>() {
//					});
//					if (dataList == null) {
//						log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
//						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//					}
//					if (baseDTO.getStatusCode() == 0) {
//
//						this.setRowCount(baseDTO.getTotalRecords());
//						totalRecords = baseDTO.getTotalRecords();
//						log.info(":: totalRecords ::" + totalRecords);
//					} else {
//						log.error(":: Employee Loan and Advance Search Failed With status code :: "
//								+ baseDTO.getStatusCode());
//						errorMap.notify(baseDTO.getStatusCode());
//					}
//					return dataList;
//				} catch (Exception e) {
//					log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
//					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//				}
//				return null;
//			}
//
//			@Override
//			public Object getRowKey(ItemDistribution res) {
//				return res != null ? res.getId() : null;
//			}
//
//			@Override
//			public ItemDistribution getRowData(String rowKey) {
//				List<ItemDistribution> responseList = (List<ItemDistribution>) getWrappedData();
//				Long value = Long.valueOf(rowKey);
//				for (ItemDistribution res : responseList) {
//					if (res.getId().longValue() == value.longValue()) {
//						selectedItemDistribution = res;
//						return res;
//					}
//				}
//				return null;
//			}
//		};
//	}
	
	public void loadEntity() {
		log.info(".......StockOutwardBeanPNS loadEntity is executed.....................");
		try {
			if(entityTypeMaster == null && entityTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_TYPE_MASTER.getErrorCode());
			}
			log.info("entity type id==>"+entityTypeMaster.getId());
			log.info("entity type name==>"+entityTypeMaster.getEntityName());
			String entityTypeName = entityTypeMaster.getEntityName().toLowerCase();
			log.info("entityTypeName"+entityTypeName);
			if(entityTypeName.equalsIgnoreCase("head office")) {
				entityTypeVal = "section";
				sectionMaster = new SectionMaster();
				sectionMasterList = commonDataService.loadSectionByEntityType(entityTypeMaster.getId()); 
			}else {
				entity = new EntityMaster();
				loadEntityListNew();
				entityTypeVal = "ent";
			}
			
			
		}catch(Exception e) {
			log.error("Exception in StockOutwardBeanPNS  :: loadEntity ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	  
	public void loadEntityListNew() {
		log.info("Received entityMasterList load");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + "/stockOutwardPS/loadActiveEntityListByTypeId/"+entityTypeMaster.getId();
			
			baseDTO = httpService.get(url);
			log.info("::: Retrieved entityMasterList :");
			if (baseDTO.getStatusCode()==0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
			else{
				log.warn("PurchaseOrderGet Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in PurchaseOrderGet load ", ee);
		}

	}
	public void loadEntityByEdit() {
		log.info(".......StockOutwardBeanPNS loadEntity is executed.....................");
		try {
			if(entityTypeMaster == null && entityTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_TYPE_MASTER.getErrorCode());
			}
			log.info("entity type id==>"+entityTypeMaster.getId());
			log.info("entity type name==>"+entityTypeMaster.getEntityName());
			String entityTypeName = entityTypeMaster.getEntityName().toLowerCase();
			log.info("entityTypeName"+entityTypeName);
			if(entityTypeName.equalsIgnoreCase("head office")) {
				entityTypeVal = "section";
				sectionMasterList = commonDataService.loadSectionByEntityType(entityTypeMaster.getId()); 
			}else {
				entityTypeVal = "ent";
				loadEntityListNew();
			}
			
			
		}catch(Exception e) {
			log.error("Exception in StockOutwardBeanPNS  :: loadEntity ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	public void clearStockItemInword() {
		entityTypeVal = "ent";
		sectionMasterList = new ArrayList<>();
		entityMasterList = new ArrayList<>();
		entityTypeMaster = new EntityTypeMaster();
		intendRequest = new IntendRequest();
		entity = new EntityMaster();
		sectionMaster = new SectionMaster();
		itemDistributionList = new ArrayList<>();
		selectedTransportTypeMaster = null;
		selectedTransportMaster = null;
		transportServiceMasterList = null;
		wayBillAvailable="Select Waybill Type";
		transportChargeAvailble="Select Type";
		waybillRendered = false;
		this.wayBillNumber = null;
		transportChargeRendered = false;
		stockTransfer.setTransportChargeAmount(null);
		selectedTransportMaster = new TransportMaster();
		
	}
	
	
	
	public void loadOutwardItem() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			Long entId=0L,secId = 0L;
			log.info(".......DistributionBean loadOutwardItem..............");
			if(intendRequest == null || intendRequest.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_REQUEST_ID.getErrorCode());
			}
			if(entityTypeMaster == null || entityTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_TYPE_MASTER.getErrorCode());
			}
			if((entity == null || entity.getId() == null) && (sectionMaster == null || sectionMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ENTITY_OR_SECTION.getErrorCode());
			}
			if(entity != null && entity.getId() != null) {
				entId = entity.getId();
			}else {
				entId=0L;
			}
			if(sectionMaster != null && sectionMaster.getId() != null) {
				secId = sectionMaster.getId();
			}else {
				secId=0L;
			}
			log.info("entity==>"+entity.getId());
			
			url = SERVER_URL + "/stockOutwardPS/loadstockoutwarddetails/"+intendRequest.getId()+"/"+entId+"/"+secId;
			log.info("loadOutwardItem url==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if(baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemDistributionList = mapper.readValue(jsonResponse, new TypeReference<List<ItemDistributionDTO>>() {});
				log.info("itemDistributionList size==> " + itemDistributionList.size());
			}else {
				itemDistributionList = new ArrayList<>();
			}
			
		}catch(Exception e) {
			log.error("Exception in loadOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	public void calBalanceQnt(ItemDistributionDTO itemDto) {
		try {
			
			log.info("itemDto getRemainintQuantity==>"+itemDto.getRemainintQuantity());
			log.info("itemDto getCurrentDispatched==>"+itemDto.getCurrentDispatched());
			log.info("itemDto getBalanceQnt==>"+itemDto.getBalanceQnt());
			double balVal = 0.0;
			double reQty = itemDto.getRemainintQuantity();
			double curQty = itemDto.getCurrentDispatched();
			if(pageAction.equals("ADD")) {
				if(reQty == 0.0) {
					reQty = itemDto.getQuantityRequired();
				}
				if(reQty<curQty) {
					errorMap.notify(ErrorDescription.DISPATCH_QTY_NOT_EXIST.getErrorCode());
					itemDto.setCurrentDispatched(null);
					balVal= reQty;
				}else {
					balVal = reQty-curQty;
				}
			}else if(pageAction.equals("EDIT")) {
				double reqQty = itemDto.getQuantityRequired();
				if(reqQty<curQty) {
					errorMap.notify(ErrorDescription.DISPATCH_QTY_NOT_EXIST.getErrorCode());
					itemDto.setCurrentDispatched(null);
					balVal= reqQty;
				}else {
					balVal = reqQty-curQty;
				}
			}
			itemDto.setBalanceQnt(balVal);
			log.info("final itemdto=>"+itemDto);
			
		}catch(Exception e) {
			log.error("Exception in calBalanceQnt  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	public String submit(String action) {

		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}
		
		return saveSateOutwardItem();
	}
	public String saveSateOutwardItem() {
		log.info("<---- : saveSateOutwardItem :------>" );
		try {
			if(withRequest == false)
			if(intendRequest == null || intendRequest.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_REQUEST_ID.getErrorCode());
				return null;
			}
			if(entityTypeMaster == null || entityTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_TYPE_MASTER.getErrorCode());
				return null;
			}
			if(withRequest == false)
			if((entity == null || entity.getId() == null) && (sectionMaster == null || sectionMaster.getId() == null)) {
				
				errorMap.notify(ErrorDescription.SELECT_ENTITY_OR_SECTION.getErrorCode());
				return null;
			}
			//start transport
			
			if(selectedTransportTypeMaster == null ) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
				return null;
			}
			if(selectedTransportMaster == null ) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
				return null;
			}
			if(wayBillAvailable == null || wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_WAYBILL_NOT_VALID.getErrorCode());
				return null;
			}
			
			if( waybillRendered == true  && (wayBillNumber == null ||  wayBillNumber.trim().length()<=0 )) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
				return null;
			}
			
			if(transportChargeAvailble == null || transportChargeAvailble.equalsIgnoreCase("Select Type")) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_AMOUNT_NOT_VALID.getErrorCode());
				return null;
			}
			if(transportChargeAvailble.equalsIgnoreCase("Yes") && ( transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
				return null;
			}
			
			if(transportChargeAvailble.equalsIgnoreCase("Yes")) {
				if(stockTransfer.getTransportChargeAmount() == null || this.stockTransfer.getTransportChargeAmount()<0) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
					return null;
				}
			}
			
			
			if(transportChargeAvailble.equalsIgnoreCase("Yes") && (this.stockTransfer.getTransportChargeAmount() == 0)) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
				return null;
			}
			if(transportChargeType!=null) {
			if(transportChargeType.equals("Cash")) {
				this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
			}else{
				this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
			}}
			
			this.stockTransfer.setTransportChargeAvailable(transportChargeRendered);
			this.stockTransfer.setWaybillAvailable(waybillRendered);
			this.stockTransfer.setTransferType(StockTransferType.STOCK_ITEM_OUTWARD);
			
			selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);
			this.stockTransfer.setTransportMaster(selectedTransportMaster);	// Type of Transport serviceType
			
			if(waybillRendered) {
				stockTransfer.setWaybillNumber(wayBillNumber);
				log.info("WayBill Value Set Success ******************** ");
			}
			//end transport
			StockItemOutwardPNSDTO stock = new StockItemOutwardPNSDTO();
			List<StockTransferItems> stockTransferItemsList = new ArrayList<StockTransferItems>();
			
			
			
			List<ItemDistributionDetails> disList = new ArrayList<>();
			if(withRequest == false) {
				itemDistribution.setIntendRequest(intendRequest);
				itemDistribution.setEntityMaster(entity);
				itemDistribution.setSectionMaster(sectionMaster);
				log.info("entity for with Request "+entity.getId());
				if(itemDistributionList==null || itemDistributionList.size()==0) {
					log.info("itemDistributionTable Null Values ");
					errorMap.notify(ErrorDescription.STOCK_ITEM_OUTWARDPNS_ITEM_TABLE_NULL.getErrorCode());
					return null;
				}
			}else {
				itemDistribution.setIntendRequest(null);
				stockTransfer.getToEntityMaster().setEntityTypeMaster(entityTypeMaster);
				itemDistribution.setEntityMaster(stockTransfer.getToEntityMaster());
				
				itemDistribution.setSectionMaster(sectionMaster);
				log.info("entity for with Out Request "+stockTransfer.getToEntityMaster().getId());
				
				if(stockTransfer.getStockTransferItemsList()==null || stockTransfer.getStockTransferItemsList().size()==0) {
					log.info("itemDistributionTable Null Values ");
					errorMap.notify(ErrorDescription.STOCK_ITEM_OUTWARDPNS_PRODUCT_TABLE_NULL.getErrorCode());
					return null;
				}
			}
			if(itemDistributionList != null) {
				for(ItemDistributionDTO itDto : itemDistributionList) {
					ItemDistributionDetails itemVal = new ItemDistributionDetails();
					ProductVarietyMaster product = new ProductVarietyMaster();
					itemVal.setItemDistribution(itemDistribution);
					product.setId(itDto.getProId()); 
					itemVal.setItemId(product);
					StockTransferItems stockTransferItems = null;
					stockTransferItems = new StockTransferItems();
					
					if(itDto.getCurrentDispatched() != null && itDto.getCurrentDispatched() != 0.0) {
						itemVal.setDistributionQuantity(itDto.getCurrentDispatched());
						stockTransferItems.setDispatchedQty(itDto.getCurrentDispatched());
						stockTransferItems.setUnitRate(itDto.getUnitRate());
						stockTransferItems.setCurrentDispatchedQty(itDto.getCurrentDespatchedQty());
					}else {
						errorMap.notify(ErrorDescription.ENTER_DISPATCH_QTY.getErrorCode());
						return null;
					}
					stockTransferItems.setProductVarietyMaster(product);
					stockTransferItemsList.add(stockTransferItems);
					disList.add(itemVal);
				}
				if(itemDistributionList.size()>0) {
					stock.setStockTransferItemsList(stockTransferItemsList);
				}else {
					stock.setStockTransferItemsList(stockTransfer.getStockTransferItemsList());
				}
			}else {
				stock.setStockTransferItemsList(stockTransfer.getStockTransferItemsList());
			}
			
			
			
			itemDistribution.setItemDistributionList(disList);
			
			
			stock.setStockTransfer(stockTransfer);
			stock.setItemDistribution(itemDistribution);
			stock.setEntityTypeMaster(entityTypeMaster);
			stock.setRequestType(type);
			stock.setStockTransferItemsDeleteList(stockTransferItemsDeleteList);
			stock.setAction(pageAction);
			stock.setLoginEnttityId(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
			url = SERVER_URL + "/stockOutwardPS/saveandupdateoutward";
			log.info("saveSateOutwardItem URL ==> " + url);
			BaseDTO baseDTO = httpService.put(url, stock);
			log.info("Save Employee Family Details Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if(pageAction.equalsIgnoreCase("ADD")) {
				log.info("Employee Family Details Saved Succuessfully ");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INSERT_SUCCUSSFULLY.getErrorCode());
				}else {
					AppUtil.addInfo("Stock Item Outward Updated Successfully");
				}
				return "/pages/printingAndStationary/listStockItemOutwardPNS.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}catch(Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null; 
	}
	
//	public String showViewPage() {
//
//		try {
//			log.info("<=showViewPage=>");
//			if (selectedItemDistribution == null || selectedItemDistribution.getId() == null) {
//				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
//				return null;
//			}
//			String apiURL = SERVER_URL + "/stockOutwardPS/getId/"+selectedItemDistribution.getId();
//			BaseDTO baseDTO = httpService.get(apiURL);
//			if (baseDTO != null) {
//				ObjectMapper mapper = new ObjectMapper();
//				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
//				itemDistribution = mapper.readValue(jsonValue, ItemDistribution.class);
//				intendRequest = itemDistribution.getIntendRequest();
//				entity = itemDistribution.getEntityMaster();
//				sectionMaster = itemDistribution.getSectionMaster();
//				getEntityType();
//				loadOutwardItem(); 
//				if(pageAction.equals("EDIT")) {
//					loadEntityByEdit();
//					return "/pages/printingAndStationary/createStockItemOutwardPNS.xhtml?faces-redirect=true";
//				}else if(pageAction.equals("VIEW")) {
//					return "/pages/printingAndStationary/viewStockItemOutwardPNS.xhtml?faces-redirect=true";
//				}
//				log.info("final entity==>"+entity);
//				
//			}
//
//		} catch (Exception e) {
//			log.error("showViewPage Exception==>", e);
//			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
//		}
//		return null;
//
//	}
	
	public String showViewPageStockTransfer() {

		try {
			log.info("<=showViewPageStockTransfer()=>");
			if (stockItemOutwardPNSDTO == null || stockItemOutwardPNSDTO.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String apiURL = SERVER_URL + "/stockOutwardPS/getStockTransferOutwardId/"+stockItemOutwardPNSDTO.getId();
			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockItemOutwardPNSDTO = mapper.readValue(jsonValue, StockItemOutwardPNSDTO.class);
				intendRequest = itemDistribution.getIntendRequest();
				entity = stockItemOutwardPNSDTO.getEntity();
				sectionMaster = itemDistribution.getSectionMaster();
				getEntityType();
				loadOutwardItem(); 
				if(pageAction.equals("EDIT")) {
					loadEntityByEdit();
					return "/pages/printingAndStationary/createStockItemOutwardPNS.xhtml?faces-redirect=true";
				}else if(pageAction.equals("VIEW")) {
					return "/pages/printingAndStationary/viewStockItemOutwardPNS.xhtml?faces-redirect=true";
				}
				log.info("final entity==>"+entity);
				
			}

		} catch (Exception e) {
			log.error("showViewPage Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;

	}
	
	
	
	public void getEntityType() {
		String apiURL = SERVER_URL + "/entitytypemaster/getEntityTypeByEntity/" + entity.getId();
		try {

			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMaster = mapper.readValue(jsonValue, EntityTypeMaster.class);
				log.info("entityTypeMaster==>"+entityTypeMaster);
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}
	
	public String backFun() {
		selectedItemDistribution = new ItemDistribution();
		intendRequest = new IntendRequest();
		entityTypeMaster = new EntityTypeMaster();
		sectionMaster = new SectionMaster();
		stockItemOutwardPNSDTO = null;
		addFlag = true;
		viewFlag = false;
		editFlag = false;
		return "/pages/printingAndStationary/listStockItemOutwardPNS.xhtml?faces-redirect=true";
	}
	
		
	public void onRowSelect(SelectEvent event) {
		try {
			addFlag = false;
			viewFlag = true;
			editFlag = false;
			if(stockItemOutwardPNSDTO.getStatus().equals(StockTransferStatus.INITIATED.toString())) {
				editFlag = true;
			}
		} catch (Exception e) {
			log.error("Error stockOutwardBean OnRowSelect Event ",e);
		}
		
		
	}
	
	//Transport Service Type transportTypeMasterList
	public void loadTransportTypeMasterList() {
		
		log.info(":::<- Load TransportTypeMasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getTransportTypeMasterList";
			log.info("::: TransportTypeMasterList Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get TransportTypeMasterList Response :");
			if (baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				transportTypeMasterList=new ArrayList<TransportTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TransportTypeMaster>>() {
				});
				log.info("TransportTypeMasterList load Successfully "+baseDTO.getTotalRecords());
				log.info("TransportTypeMasterList Page Convert Succes -->");
			}
			else{
				log.warn("TransportTypeMasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportTypeMasterList load ", ee);
		}
	}
	public void transportTypeMasterListener() {
		log.info("Received Listener of selectedTransportTypeMaster **************");
		try {
			transportServiceMasterList=new ArrayList<TransportMaster>();
			loadTransportServiceList(selectedTransportTypeMaster.getId());
		}catch(Exception se) {
			log.error("SET selectedTransportTypeMaster Get Error "+se);
		}
		log.info("Done selectedTransportTypeMaster **************");
	}
	
	public void loadTransportServiceList(Long id) {
		
		log.info(":::<- Load TransportService MasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getTransportServiceMasterList";
			log.info("::: TransportService MasterList Controller calling  1 :::");
			TransportMaster mas = new TransportMaster();
			mas.setId(id);
			baseDTO = httpService.post(url,mas);
			log.info("::: get TransportService MasterList Response :");
			if (baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				transportServiceMasterList=new ArrayList<TransportMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportServiceMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TransportMaster>>() {
				});
				log.info("TransportService MasterList load Successfully "+baseDTO.getTotalRecords());
				log.info("TransportService MasterList Page Convert Succes -->");
			}
			else{
				log.warn("TransportService MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportService MasterList load ", ee);
		}
	}
	public void renderedWaybillNumber() {
		log.info("rendered WaybillNumber received ");
			if(wayBillAvailable.equalsIgnoreCase("YES")) {
				waybillRendered=true;
			}else {
				waybillRendered=false;
			}
	}
	
	public void renderedTransportChargeRendered() {
		log.info("rendered transportChargeRendered received ");
			if(transportChargeAvailble.equalsIgnoreCase("YES")) {
				transportChargeRendered=true;
			}else {
				transportChargeRendered=false;
			}
	}
	
	public void getStockTransferByID() {
		try {
			BaseDTO baseDTO = null;
			StockItemInwardPNSDTO stockItemInwardPNSDTO = new StockItemInwardPNSDTO();
//			selectedPurchaseInvoiceList = new ArrayList<PurchaseInvoice>();
			log.info("Retrieved tockTransferByID :"+selectedStockItemOutwardPNSDTO.getSupplierCodeName());
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getSelectedStockTransferRetrieved";
			log.info("::: Item tockTransferByID Controller calling :::");
			baseDTO = httpService.post(url,selectedStockItemOutwardPNSDTO);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("Item tockTransferByID Successfully Processed ******");
				ObjectMapper mapper = new ObjectMapper();
				
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					stockItemInwardPNSDTO = mapper.readValue(jsonResponse, new TypeReference<StockItemInwardPNSDTO>() {
				});
				
				selectedStockTransferPNS = stockItemInwardPNSDTO.getSelectedStockTransfer();
				 
				log.info("Item tockTransferByID Successfully Completed ******");

				
				selectedStockItemOutwardPNSDTO.setTrans_ser_type(selectedStockTransferPNS.getTransportMaster().getTransportTypeMaster().getName());
				selectedStockItemOutwardPNSDTO.setTrans_ser_name(selectedStockTransferPNS.getTransportMaster().getName());
				
				selectedStockItemOutwardPNSDTO.setWaybill_no(selectedStockTransferPNS.getWaybillNumber());
				selectedStockItemOutwardPNSDTO.setWaybillAvailable(selectedStockTransferPNS.getWaybillAvailable());
				selectedStockItemOutwardPNSDTO.setTransport_charge_available(selectedStockTransferPNS.getTransportChargeAvailable());
				if(selectedStockTransferPNS.getTransportChargeType().toString().equalsIgnoreCase("ToPay"))
					selectedStockItemOutwardPNSDTO.setTrans_charge_type("Cheque");
				else
					selectedStockItemOutwardPNSDTO.setTrans_charge_type("Cash");
				if(selectedStockTransferPNS.getTransportChargeAmount() != null)
					selectedStockItemOutwardPNSDTO.setTrans_charge_amt(selectedStockTransferPNS.getTransportChargeAmount());
				
//				selectedPurchaseInvoiceList = stockItemInwardPNSDTO.getSelectedPurchaseInvoiceList();
				log.info("Purchase Order List Successfully Completed ******");
			}
			else{
				log.error("tockTransferByID Details Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in tockTransferByID load ", ee);
		}
		log.info("Done Item tockTransferByID ");
	}
	
	public void requestListener() {
		try {
			
			log.info("Request type changer Listener ");
			withRequest = true;
			if(type.equals(StockItemOutWardType.WITH_REQUEST.toString())) {
				withRequest = false;
			}
		} catch (Exception e) {
			log.error("Request type changer Listener Error ",e);
		}
	}
	
	//second type load
	public void onChangeEntityTypeMaster() {
		log.info("onChangeEntityTypeMaster is called ");

		if (entityTypeMaster != null) {
			showroomList = commonDataService.findEntityByEntityType(entityTypeMaster.getEntityCode());
			log.info("showroomList size :"+showroomList.size());
			
			removeCurrentLoggedInItem(showroomList, loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());

		} else {
			showroomList = null;
		}
	}

	public void removeCurrentLoggedInItem(List<EntityMaster> entityMasterList, Long idToRemove) {
		Iterator<EntityMaster> it = entityMasterList.iterator();
		while (it.hasNext()) {
			EntityMaster em = it.next();
			if (em.getId().longValue() == idToRemove.longValue()) {
				it.remove();
			}
		}
	}
	
	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}
	
	public void onChangeShowroom() {
		log.info("onChangeShowroom is called ");

		if (stockTransfer.getToEntityMaster() != null) {
			log.info("Selected ISSR Showroom is [" + stockTransfer.getToEntityMaster() + "]");

			productCategoryList = commonDataService.loadProductCategories();
			currentStockStatusRequest = new CurrentStockStatusRequest();
			disableBtnProductAdd = false;
			populateTransportFormData();
		}
	}
	public void populateTransportFormData() {
		transportTypeMasterList = commonDataService.getAllTransportTypes();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
	}
	
	public void processProductGroups() {
		log.info("Inside processProductGroups");
		currentStockStatusRequest.setProductGroupMaster(null);
		currentStockStatusRequest.setProductVarietyMaster(null);

		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductCategory() != null) {
			log.info("Selected Product Category " + currentStockStatusRequest.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(currentStockStatusRequest.getProductCategory());
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();

		}
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + currentStockStatusRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(currentStockStatusRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");
		stockTransferItems = new StockTransferItems();
		
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductVarietyMaster() != null && unitRate!=null || unitRate>0) {
			log.info("Selected Product Variety " + currentStockStatusRequest.getProductVarietyMaster().getCode());
			StockTransferItems items = findInventoryItems(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId(),
					currentStockStatusRequest.getProductVarietyMaster().getId(),unitRate);
			
			if (items != null) {
				stockTransferItems = items;
				stockTransferItems.setRemainingAmount(stockTransferItems.getReceivedQty()); //set receivedQty starting value because set to inventroy_Items table
				log.info("Size of items "+stockTransfer.getStockTransferItemsList().size());
				
				stockTransfer.getStockTransferItemsList().stream().forEach(itemData -> {
					if (stockTransferItems.getProductVarietyMaster().getId().equals(itemData.getProductVarietyMaster().getId())) {
						stockTransferItems.setRemainingAmount(itemData.getReceivedQty());
						log.info("StockTransferItems Id match and Set Already Added Data .Old:"+stockTransferItems.getProductVarietyMaster().getId()+",S:"+itemData.getProductVarietyMaster().getId());
					}
					
				});
				
				disableAddProduct = false;
				stockTransferItems.setUnitRate(unitRate);
			} else {
				disableAddProduct = true;
			}
		}
	}
	
	public StockTransferItems findInventoryItems(Long entityId, Long productId,Double unitPrice) {
		log.info("findItemByQRCode method is executing..");

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/stock/transfer/inspection/center/stock/qty/" + entityId + "/" + productId+ "/"+unitPrice;

		StockTransferItems stockTransferItems = null;

		try {
			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer Items get successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					stockTransferItems = mapper.readValue(jsonResponse, StockTransferItems.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				throw new RestException(ErrorDescription.FAILURE_RESPONSE);
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return stockTransferItems;
	}
	
	
	public void loadunitrate() {
		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
		+ "/stock/transfer/inspection/center/stock/unitrate/" + loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId() + "/" + currentStockStatusRequest.getProductVarietyMaster().getId();

		try {
			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer Items get successfully");
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					stockTransferItemsListUnitrate = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferItems>>() {
					});
					log.info("loadTrainingType finished");
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				throw new RestException(ErrorDescription.FAILURE_RESPONSE);
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}
	
	public String validateCurrentDispatchQty() {

		log.info("stockTransferItems.getDispatchedQty() " + stockTransferItems.getDispatchedQty());
		log.info("stockTransferItems.getReceivedQty() " + stockTransferItems.getReceivedQty());
		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getReceivedQty() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				return null;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getReceivedQty()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				return null;
			}
		}

		return null;
	}

	public void removeProduct(StockTransferItems stockTransferItems) {

		stockTransfer.getStockTransferItemsList().remove(stockTransferItems);
//		stockItemOutwardPNSDTO.getStockTransferItemsDeleteList().add(stockTransferItems);
		stockTransferItemsDeleteList.add(stockTransferItems);
		log.info("Product Deleted List added [" + stockTransferItems.getId() + "]");
		setOfBundleNumbers = new HashSet<>();
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				if (items.getBundleNumber() != null) {
					setOfBundleNumbers.add(items.getBundleNumber());
				}
			}
		}
		updateBundleNumbers(setOfBundleNumbers);
	}
	
	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	public void addProduct() {

		log.info("Product is being added [" + stockTransferItems + "]");

		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getRemainingAmount() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				return ;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getRemainingAmount()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				return ;
			}
		}

//		stockTransferItems.setReceivedQty(0.0);
		stockTransferItems.setUnitRate(unitRate);
		stockTransferItems.setProductVarietyMaster(getCurrentStockStatusRequest().getProductVarietyMaster());
		stockTransferItems.setUomMaster(getCurrentStockStatusRequest().getProductVarietyMaster().getUomMaster());
		stockTransferItems.setItemNetTotal(unitRate*stockTransferItems.getDispatchedQty());
		stockTransfer.getStockTransferItemsList().add(stockTransferItems);
//		stockTransferItems.setRemainingAmount(stockTransferItems.getRemainingAmount()-stockTransferItems.getDispatchedQty());
		
		if (stockTransferItems.getBundleNumber() != null) {
			setOfBundleNumbers.add(stockTransferItems.getBundleNumber());
			updateBundleNumbers(setOfBundleNumbers);
		}

//		stockTransferItems = new StockTransferItems();

//		currentStockStatusRequest.setProductVarietyMaster(null);
		clearProductDetails();

		//return null;
	}
	
	private void clearProductDetails()
	{
		//stockTransferItems.setBundleNumber(null);
		//stockTransferItems.setDispatchedQty(null);
		stockTransferItems.setReceivedQty(null);
		unitRate=null;
		//currentStockStatusRequest.productVarietyMaster
		currentStockStatusRequest.setProductVarietyMaster(null);
		//currentStockStatusRequest.productGroupMaster
		currentStockStatusRequest.setProductGroupMaster(null);
		
		currentStockStatusRequest.setProductCategory(null);
//		stockTransferItems.setBundleNumber(null);
//		stockTransferItems.setDispatchedQty(null);
		
	}
	
	public String viewStockTransfer() {
		
		log.info(":::<- viewStockTransfer received() ->::: ");
		BaseDTO baseDTO = null;
		stockOutwardPNSCategory = false;
		try {
			String url = SERVER_URL+ "/stockOutwardPS/viewStockTransferOutwardId/"+stockItemOutwardPNSDTO.getId();
			log.info("::: TransportService MasterList Controller calling  1 :::");
			stockItemOutwardPNSDTO = new StockItemOutwardPNSDTO();
			stockTransferItemsDeleteList = new ArrayList<StockTransferItems>(); 
			baseDTO = httpService.get(url);
			
			if (baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockItemOutwardPNSDTO = mapper.readValue(jsonResponse, StockItemOutwardPNSDTO.class);
				
				log.info("StockItemOutwardPNSDTO load Successfully "+baseDTO.getTotalRecords());
				withRequest = true;
				if(stockItemOutwardPNSDTO.getItemDistribution()==null) {
					type = StockItemOutWardType.WITH_OUT_REQUEST.toString();
				}else {
					type = StockItemOutWardType.WITH_REQUEST.toString();
					intendRequest = stockItemOutwardPNSDTO.getIntendRequest();
//					intendRequestList = commonDataService.loadAllIntendRequest();
					loadStockOutwardInfo();
					withRequest = false;
					itemDistributionList = stockItemOutwardPNSDTO.getDistributionList();
					entity = stockItemOutwardPNSDTO.getEntity();
				}
				 
				entityTypeMaster = stockItemOutwardPNSDTO.getEntityTypeMaster();
				onChangeEntityTypeMaster();
				stockTransfer.setToEntityMaster(stockItemOutwardPNSDTO.getEntity());
				onChangeShowroom();
				stockTransfer =stockItemOutwardPNSDTO.getStockTransfer();
				selectedTransportTypeMaster = stockItemOutwardPNSDTO.getStockTransfer().getTransportMaster().getTransportTypeMaster();
				selectedTransportMaster = stockItemOutwardPNSDTO.getStockTransfer().getTransportMaster();
				wayBillAvailable = "No";
				waybillRendered = false;
				if(stockTransfer.getWaybillAvailable()) {
					wayBillAvailable = "Yes";
					waybillRendered = true;
					wayBillNumber = stockTransfer.getWaybillNumber();
				}
				transportChargeAvailble = "No";
				transportChargeRendered = false;
				stockTransferItems = new StockTransferItems();
				if(stockTransfer.getTransportChargeAvailable()) {
					transportChargeAvailble = "Yes";
					transportChargeType = null;
					transportChargeRendered = true;
					if(stockItemOutwardPNSDTO.getStockTransfer().getTransportChargeType().toString().equals(TransportChargeType.Pay.toString())) {
						transportChargeType = "Cash"; 
					} else if(stockItemOutwardPNSDTO.getStockTransfer().getTransportChargeType().toString().equals(TransportChargeType.ToPay.toString())) {
						transportChargeType = "Cheque"; 
					}
				}
				
				if(pageAction.equals("EDIT")) {
					
					log.info("::: get TransportService Edit processing :");
					loadTransportTypeMasterList();
					transportTypeMasterListener();
					stockOutwardPNSCategory = false;
					return "/pages/printingAndStationary/createStockItemOutwardPNS.xhtml?faces-redirect=true";
				} else if(pageAction.equals("VIEW")) {
					log.info("::: get TransportService View processing :");
					stockOutwardPNSCategory = true;
					return "/pages/printingAndStationary/viewStockItemOutwardPNS.xhtml?faces-redirect=true";
				}
			}
			else{
				log.warn("TransportService StockItemOutwardPNSDTO Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportService StockItemOutwardPNSDTO ", ee);
		}
		return null;
	}
	
	
	public void checkGST() {
		stockItemOutwardPNSDTO = new StockItemOutwardPNSDTO();
	}
	 
}
