package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.StudentTraining;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("studentTrainingBean")
@Scope("session")
public class StudentTrainingBean implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final String STUDENT_TRAING_LIST_PAGE = "/pages/admin/training/listStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAING_ADD_PAGE = "/pages/admin/training/createStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAING_VIEW_PAGE = "/pages/admin/training/viewStudentTraining.xhtml?faces-redirect=true;";
	private final String STUDENT_TRAING_APPROVE_PAGE = "/pages/admin/training/approvalStudentTraining.xhtml?faces-redirect=true;";

	public static final String STUDENT_TRAINING_URL = AppUtil.getPortalServerURL() + "/student/training";

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	StudentTraining selectedStudentTraining = new StudentTraining();

	@Getter
	@Setter
	StudentTraining studentTraining = new StudentTraining();

	@Getter
	@Setter
	private String Url = null;

	@Getter
	@Setter
	LazyDataModel<StudentTraining> studentTraininglazyLoadList;

	List<StudentTraining> studentTrainingList = null;
	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	long employeePhotoSize;

	@Getter
	@Setter
	private UploadedFile file = null;

	@Getter
	@Setter
	private UploadedFile relievingLetterFile;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String originalFileName;

	@Getter
	@Setter
	private Date startTime, endTime;

	@PostConstruct
	public void init() {
		log.info("StudentTrainingBean Init is executed.....................");
		loadAppConfigValues();
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	/**
	 * @return
	 */

	@PostConstruct
	public String showListStudentTrainingPage() {
		log.info("showListStudentTrainingPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		approveButtonFlag = true;
		selectedStudentTraining = new StudentTraining();
		loadAppConfigValues();
		loadStudentTrainingListLazy();
		return STUDENT_TRAING_LIST_PAGE;
	}

	public void loadStudentTrainingListLazy() {

		log.info("<-----STUDENTTraining list lazy load starts------>");
		studentTraininglazyLoadList = new LazyDataModel<StudentTraining>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StudentTraining> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = STUDENT_TRAINING_URL + "/loadlazylist";
					BaseDTO baseDTO = httpService.post(url, paginationRequest);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<StudentTraining> studentTrainingList = mapper.readValue(jsonResponse,
							new TypeReference<List<StudentTraining>>() {
							});
					if (studentTrainingList == null) {
						log.info(" STUDENTTrainingList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" STUDENTTraining list search Successfully Completed");
					} else {
						log.error(" STUDENTTraining list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return studentTrainingList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading STUDENTTraining list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(StudentTraining res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StudentTraining getRowData(String rowKey) {
				List<StudentTraining> responseList = (List<StudentTraining>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StudentTraining res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedStudentTraining = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String studentTrainingListAction() {
		log.info("<====== StudentTrainingBean.studentTrainingListAction Starts====> action   :::" + action);
		try {
			originalFileName = null;
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== STUDENTTraining add page called.......====>");
				selectedStudentTraining = new StudentTraining();
				studentTraining = new StudentTraining();
				return STUDENT_TRAING_ADD_PAGE;
			}
			if (selectedStudentTraining == null) {
				errorMap.notify(ErrorDescription.SELECT_STUDENT_TRAINING.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStudenetTrainingDelete').show();");
			} else {
				log.info("<----Loading STUDENTTraining Edit page.....---->" + selectedStudentTraining.getId());
				studentTraining = new StudentTraining();
				BaseDTO response = httpService.get(STUDENT_TRAINING_URL + "/get/id/" + selectedStudentTraining.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					studentTraining = mapper.readValue(jsonResponse, new TypeReference<StudentTraining>() {
					});
					if (studentTraining.getUploadfilepath() != null) {
						String filePathValue[] = studentTraining.getUploadfilepath().split("uploaded/");
						log.info("0==>" + filePathValue[0]);
						log.info("1==>" + filePathValue[1]);
						originalFileName = filePathValue[1];
						studentTraining.setUploadfilepath(filePathValue[1]);
					} else {
						log.info("1==>" + originalFileName);
					}
					if (action.equalsIgnoreCase("EDIT")) {
						return STUDENT_TRAING_ADD_PAGE;
					}
					if (action.equalsIgnoreCase("VIEW")) {
						return STUDENT_TRAING_VIEW_PAGE;
					}
					if (action.equalsIgnoreCase("APPROVE")) {
						return STUDENT_TRAING_APPROVE_PAGE;
					}

				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== StudentTrainingBean.studentTrainingListAction Ends====>");
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in StudentTrainingBean.studentTrainingListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== studentTrainingBean.studentTrainingListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts StudentTrainingBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService.delete(STUDENT_TRAINING_URL + "/delete/" + selectedStudentTraining.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting STUDENTTraining....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends StudentTrainingBean.deleteConfirm ---->");
		return showListStudentTrainingPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts StudentTrainingBean.onRowSelect ========>" + event);
		selectedStudentTraining = ((StudentTraining) event.getObject());
		addButtonFlag = false;
		log.info("<===Starts StudentTrainingBean.onRowSelect ========>" + selectedStudentTraining.getStatus());
		if (selectedStudentTraining.getStatus().equals("APPROVED")
				|| selectedStudentTraining.getStatus().equals("REJECTED")) {
			editButtonFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
		} else if (selectedStudentTraining.getStatus().equals("INPROGRESS")) {
			approveButtonFlag = true;
			editButtonFlag = true;
		} else {
			editButtonFlag = true;
		}
		log.info("<===Ends StudentTrainingBean.onRowSelect ========>");
	}

	public String submit() {
		log.info("<=======Starts  StudentTrainingBean.submit ======>" + studentTraining);
		if (studentTraining.getScheduledStartTime() == null) {
			errorMap.notify(ErrorDescription.STUDTRAINING_SHEDULESTART_EMPTY.getErrorCode());
			return null;
		}
		if (studentTraining.getScheduledEndTime() == null) {
			errorMap.notify(ErrorDescription.STUDTRAINING_SHEDULESTART_EMPTY.getErrorCode());
			return null;
		}
		if (action.equalsIgnoreCase("ADD")) {
			studentTraining.setCreatedBy(loginBean.getUserDetailSession());
			studentTraining.setStatus("INPROGRESS");
			studentTraining.setCreatedDate(new Date());
		} else if (action.equalsIgnoreCase("EDIT")) {
			studentTraining.setModifiedBy(loginBean.getUserDetailSession());
			studentTraining.setModifiedDate(new Date());
		} else if (action.equalsIgnoreCase("APPROVE")) {
			studentTraining.setStatus("APPROVED");
			studentTraining.setModifiedDate(new Date());
			studentTraining.setApprovedDate(new Date());
			studentTraining.setModifiedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("REJECT")) {
			studentTraining.setStatus("REJECTED");
			studentTraining.setApprovedDate(new Date());
			studentTraining.setModifiedDate(new Date());
			studentTraining.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.put(STUDENT_TRAINING_URL + "/saveorupdate", studentTraining);
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_SAVE_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("EDIT"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_UPDATE_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("REJECT"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_REJECTED_SUCCESS.getErrorCode());
			else if (action.equalsIgnoreCase("APPROVE"))
				errorMap.notify(ErrorDescription.STUDENT_TRAINING_APPROVED_SUCCESS.getErrorCode());
			return showListStudentTrainingPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends StudentTrainingBean.submit ======>");
		return null;
	}

	public void getDayDifference() {
		try {

			Date startDate = studentTraining.getStartDate();
			Date endtDate = studentTraining.getEndDate();

			if (startDate != null || endtDate != null) {
				long difference = endtDate.getTime() - startDate.getTime();
				Long daydifference = (difference / (1000 * 60 * 60 * 24) + 1);
				studentTraining.setNoOfDays(daydifference);
			}
		} catch (Exception e) {
			log.error(" Exception In EmployeeMasterBean employeeAgeCalculation Method ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	public void uplaodTrainingDocuments(FileUploadEvent event) {
		log.info("uplaodCommunityCertificate Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.STUDTRAINING_DOCUMENTS_NOT_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			long size = relievingLetterFile.getSize();
			log.info("uploadFile size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				studentTraining.setUploadfilepath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.STUDTRAINING_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("STUDENT_TRAINING_DOCUMENT_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			studentTraining.setUploadfilepath(filePath);
			originalFileName = relievingLetterFile.getFileName();
			errorMap.notify(ErrorDescription.STUDTRAINING_DOCUMENT_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void validateMobileNo(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validate method");

		String mobileNo = (String) value;
		Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		Matcher matcher = pattern.matcher(mobileNo);
		if (!matcher.matches()) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Invalid contact number.");
			context.addMessage(comp.getClientId(context), message);
		}
	}

	public void confirmStatus() {
		if (studentTraining == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}
	/*
	 * private void showTime() { if (studentTraining.getScheduledStartTime() == null
	 * && studentTraining.getScheduledEndTime() == null) { Calendar calendar =
	 * Calendar.getInstance(); calendar.set(Calendar.AM_PM, Calendar.AM);
	 * calendar.set(Calendar.HOUR, 8); calendar.set(Calendar.MINUTE, 15); startTime
	 * = calendar.getTime(); log.info(":: Oral exam venue start time :: " +
	 * startTime); studentTraining.setScheduledStartTime(startTime);
	 * 
	 * calendar.set(Calendar.HOUR, 11); calendar.set(Calendar.MINUTE, 40); endTime =
	 * calendar.getTime(); log.info(":: Oral exam venue end time :: " + endTime);
	 * studentTraining.setScheduledEndTime(endTime); } }
	 */
}
