package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.dto.EmployeeAdminPromotionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeePromotion;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("adminPromotionBean")
public class AdminPromotionBean {

	ObjectMapper mapper;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String pagePromotion, entitytip;

	@Getter
	@Setter
	EmployeePromotionDetails selectedEmployeePromotionDetails = new EmployeePromotionDetails();

	@Getter
	@Setter
	EmployeePromotion selectedEmployeePromotion = new EmployeePromotion();

	@Getter
	@Setter
	EmployeePromotionDetails employeePromotionDetails = new EmployeePromotionDetails();

	@Getter
	@Setter
	LazyDataModel<EmployeePromotion> loadPromotionList;

	@Getter
	@Setter
	List<EmployeePromotionDetails> empPromotionDetailsList = new ArrayList<>();

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton, addLoan, editLoan;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	List<Designation> designationList;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	@Getter
	@Setter
	int totalPromotionSize = 0;

	@Getter
	@Setter
	EmployeeAdminPromotionDTO employeeAdminPromotionDto;

	@Getter
	@Setter
	EmployeeAdminPromotionDTO selectedEmployeeAdminPromotionDto;

	@Setter
	@Getter
	List<EntityMaster> entityMasterList;

	@Setter
	@Getter
	Department department;

	@Setter
	@Getter
	EmployeePromotion employeePromotion = new EmployeePromotion();

	@Setter
	@Getter
	List<EmployeeAdminPromotionDTO> dtoList = new ArrayList<>();

	@Setter
	@Getter
	Designation designation;

	@Setter
	@Getter
	List<Department> departmentList;

	@Setter
	@Getter
	List<EntityMaster> regionList = new ArrayList<>();

	@Setter
	@Getter
	List<EmployeeAdminPromotionDTO> promotionList;

	@Setter
	@Getter
	List<Integer> yearList;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	LazyDataModel<EmployeePromotionDetails> lazyEmployeePromotionDetails;

	@Getter
	@Setter
	int proTotalRecord = 0;

	public static final String PROMOTION_SERVER_URL = AppUtil.getPortalServerURL() + "/emppromotion";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@PostConstruct
	public void init() {
		log.info("adminPromotionBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadPromotionValue();
		// entityMasterList = commonDataService.loadEntityMaster();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	}

	public void loadPromotionValue() {
		try {
			departmentList = commonDataService.getDepartment();
			designationList = commonDataService.loadDesignation();
		} catch (Exception e) {
			log.error("Exception in loadPromotionValue  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String promotionPages() {
		try {
			if (pagePromotion.equalsIgnoreCase("ADD")) {
				employeeAdminPromotionDto = new EmployeeAdminPromotionDTO();
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				regionList = new ArrayList<>();
				promotionList = new ArrayList<>();
				yearList = getListofMonth();
				return "/pages/admin/promotion/createPromotionAdmin.xhtml?faces-redirect=true";
			} else if (pagePromotion.equalsIgnoreCase("Edit")) {

			} else if (pagePromotion.equalsIgnoreCase("LIST")) {
				selectedEmployeePromotion = new EmployeePromotion();
				loadLazyLoanandAdvanceDetails();
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				return "/pages/admin/promotion/listPromotionAdmin.xhtml?faces-redirect=true";
			} else if (pagePromotion.equalsIgnoreCase("View")) {
				if (selectedEmployeePromotion == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->" + selectedEmployeePromotion.getId());
				employeePromotion = getPromotionDetails(selectedEmployeePromotion.getId());
				return "/pages/admin/promotion/viewPromotion.xhtml?faces-redirect=true";
				// return "/pages/admin/promotion/ViewPromotionAdmin.xhtml?faces-redirect=true";
			} else if (pagePromotion.equalsIgnoreCase("PROMOTEEMP")) {
				if (selectedEmployeePromotion == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->" + selectedEmployeePromotion.getId());
				employeePromotion = getPromotionDetails(selectedEmployeePromotion.getId());
				return "/pages/admin/promotion/promotionList.xhtml?faces-redirect=true";
			} else if (pagePromotion.equalsIgnoreCase("DELETE")) {
				log.info(":: Interchange Delete Response :: ");
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				if (selectedEmployeePromotion == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedEmployeePromotion.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			} else if (pagePromotion.equalsIgnoreCase("FINALAPPROVAL")) {
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				lasyLoadApprovalEmp();
				return "/pages/admin/promotion/listPromotionApproval.xhtml?faces-redirect=true";
			}

		} catch (Exception e) {
			log.error("Exception in promotionPages :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	// List<EmployeeAdminPromotionDTO> dtoList
	private EmployeePromotion getPromotionDetails(Long promotionId) {
		log.info("<=getPromotionDetails call=>");
		try {
			if (selectedEmployeePromotion == null || selectedEmployeePromotion.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  Id : : " + selectedEmployeePromotion.getId());
			String getUrl = PROMOTION_SERVER_URL + "/get/id/:id";
			String url = getUrl.replace(":id", selectedEmployeePromotion.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeePromotion = mapper.readValue(jsonResponse, EmployeePromotion.class);

			// log.info("employeePromotion==>"+employeePromotion);

			/*
			 * if (empPromotionDetails != null) { employeePromotionDetails =
			 * empPromotionDetails; } else {
			 * log.error("promotion object failed to desearlize");
			 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); return null;
			 * }
			 */

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Promotion Retrived  SuccessFully ");
			} else {
				log.error(" Promotion Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeePromotion;
		} catch (Exception e) {
			log.error("ExceptionException Ocured while get Emp Promotion Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadLazyLoanandAdvanceDetails() {
		loadPromotionList = new LazyDataModel<EmployeePromotion>() {

			private static final long serialVersionUID = -9122090322809059767L;

			List<EmployeePromotion> dataList = new ArrayList<EmployeePromotion>();

			@Override
			public List<EmployeePromotion> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				EmployeePromotion request = new EmployeePromotion();

				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					/*
					 * UserMaster user = login.getUserDetailSession();
					 * 
					 * log.info("getId==>" + user.getId()); request.setUserId(user.getId());
					 */
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(PROMOTION_SERVER_URL + "/loadlazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					dataList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeePromotion>>() {
					});
					if (dataList == null) {
						log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalPromotionSize = baseDTO.getTotalRecords();
						log.info("totalPromotionSize===>" + totalPromotionSize);
					} else {
						log.error(":: Employee Promotion Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return dataList;
				} catch (Exception e) {
					log.error(":: Exception Promotion Ocured while Load in Promotion ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeePromotion res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeePromotion getRowData(String rowKey) {
				List<EmployeePromotion> responseList = (List<EmployeePromotion>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeePromotion res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeePromotion = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void onRowSelect(SelectEvent event) {
		selectedEmployeePromotion = ((EmployeePromotion) event.getObject());
		if (selectedEmployeePromotion.getStatus().equalsIgnoreCase("Approved")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedEmployeePromotion.getStatus().equalsIgnoreCase("Rejected")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = true;
		} else if (selectedEmployeePromotion.getStatus().equalsIgnoreCase("InProgress")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public String clear() {
		log.info("list Promotion Admin clear method called..");
		selectedEmployeePromotion = new EmployeePromotion();
		loadLazyLoanandAdvanceDetails();
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		return "/pages/admin/promotion/listPromotionAdmin.xhtml?faces-redirect=true";
	}

	public String deletePromotion() {
		try {

			if (selectedEmployeePromotionDetails == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedEmployeePromotionDetails.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			}
			log.info("Selected Promotion  Id : : " + selectedEmployeePromotionDetails.getPromotionId());
			String getUrl = PROMOTION_SERVER_URL + "/delete/:id";
			String url = getUrl.replace(":id", selectedEmployeePromotionDetails.getPromotionId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("Promotion Delete Response :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Promotion Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.RECORD_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

			loadLazyLoanandAdvanceDetails();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/admin/promotion/listPromotionAdmin.xhtml?faces-redirect=true";
	}

	public void changeEntity() {
		entitytip = "";
		for (EntityMaster entity : entityMasterList) {
			if (employeeAdminPromotionDto.getRegionList().contains(entity.getId())) {
				if (entitytip != "") {
					entitytip = entitytip + ", ";
				}
				entitytip = entitytip + entity.getName();
			}
		}
	}

	public void loadDesignationMasterList() {
		log.info("... Load Designation List called ....");
		try {
			log.info("dep id==>" + employeePromotion.getDepartment().getId());
			if (employeePromotion != null && employeePromotion.getDepartment().getId() != null) {
				mapper = new ObjectMapper();
				BaseDTO response = httpService.get(
						SERVER_URL + "/designation/getalldesignation/" + employeePromotion.getDepartment().getId());
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
				});
				log.info("designationList---->" + designationList.size());
			} else {
				designationList = new ArrayList<>();
				designation = null;
			}
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void generatePromotionDetails() {
		log.info("<==== Starts AdminPromotionBean.generatePromotionDetails=====>");
		try {
			/*
			 * if(employeePromotion.getHeadRegOffice() == null) {
			 * log.info("Region list required");
			 * errorMap.notify(ErrorDescription.EMP_PROMOTION_REGION_LIST_EMPTY.getCode());
			 * return; }
			 */

			if (employeeAdminPromotionDto.getRegionList() == null
					|| employeeAdminPromotionDto.getRegionList().size() == 0) {
				log.info("Region list required");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_REGION_LIST_EMPTY.getCode());
				return;
			}
			if (employeePromotion.getDepartment() == null) {
				log.info("Department required");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_DEPARTMENT_ID_MANDATORY.getCode());
				return;
			}
			if (employeePromotion.getDesignation() == null) {
				log.info("Designation required");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_DESIGNATION_ID_MANDATORY.getCode());
				return;
			}
			if (employeePromotion.getYear() == null) {
				log.info("Year required");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_YEAR_IS_MANDATORY.getCode());
				return;
			}
			if (employeePromotion.getMonth() == null) {
				log.info("Month required");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_MONTH_IS_MANDATORY.getCode());
				return;
			}
			String url = PROMOTION_SERVER_URL + "/generate";
			employeeAdminPromotionDto.setDepartmentId(employeePromotion.getDepartment().getId());
			employeeAdminPromotionDto.setDesignationId(employeePromotion.getDesignation().getId());
			employeeAdminPromotionDto.setPromotionPeriod(employeePromotion.getDesignation().getPromotionPeriod());
			employeeAdminPromotionDto.setYear(employeePromotion.getYear());
			employeeAdminPromotionDto.setMonth(employeePromotion.getMonth());
			log.info("employeeAdminPromotionDto==>" + employeeAdminPromotionDto);
			BaseDTO response = httpService.post(url, employeeAdminPromotionDto);
			
			log.info("generatePromotionDetails url==> "+url);
			
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				// empPromotionDetailsList = mapper.readValue(jsonResponse,new
				// TypeReference<List<EmployeePromotionDetails>>() {});
				promotionList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeAdminPromotionDTO>>() {
				});

				log.info("promotionList size==>" + promotionList.size());
				log.info("promotionList==>" + promotionList);

			} else if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in generatePromotionDetails....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<==== Ends AdminPromotionBean.generatePromotionDetails=====>");
	}

	public void viewPromotionDetails() {
		log.info("view promtoion details called 1==>" + selectedEmployeeAdminPromotionDto);
		log.info("employeeAdminPromotionDto==>" + employeeAdminPromotionDto);
		// log.info("month==>"+getMonth(employeeAdminPromotionDto.getMonth()));
		// selectedEmployeeAdminPromotionDto.setMonth(getMonth(employeeAdminPromotionDto.getMonth()));
		log.info("view promtoion details called 2==>" + selectedEmployeeAdminPromotionDto);
		RequestContext.getCurrentInstance().execute("PF('viewPromotion').show();");
		RequestContext.getCurrentInstance().update("viewPanel");
	}

	public List<Integer> getListofMonth() {
		List<Integer> yearListData = new ArrayList();
		try {
			Calendar beginCalendar = Calendar.getInstance();
			beginCalendar.set(Calendar.YEAR, 2016);
			int year = beginCalendar.get(Calendar.YEAR);
			for (int i = 0; i < 10; i++) {
				yearListData.add(year + i);
			}
			log.info("yearListData==>" + yearListData);
		} catch (Exception e) {
			log.error("getListofMonth........", e);
		}
		return yearListData;
	}

	private String getMonth(String month) {
		String monthString = null;
		switch (month) {
		case "01":
			monthString = "January";
			break;
		case "02":
			monthString = "February";
			break;
		case "03":
			monthString = "March";
			break;
		case "04":
			monthString = "April";
			break;
		case "05":
			monthString = "May";
			break;
		case "06":
			monthString = "June";
			break;
		case "07":
			monthString = "July";
			break;
		case "08":
			monthString = "August";
			break;
		case "09":
			monthString = "September";
			break;
		case "10":
			monthString = "October";
			break;
		case "11":
			monthString = "November";
			break;
		case "12":
			monthString = "December";
			break;
		default:
			monthString = "Invalid month";
			break;
		}
		return monthString;
	}

	public void reset() {
		selectedEmployeeAdminPromotionDto = null;
	}

	public String saveOrUpdatePromotion() {
		try {
			if (promotionList == null || promotionList.size() == 0) {
				log.info("Promotion details required........");
				errorMap.notify(ErrorDescription.EMP_PROMOTION_LIST_IS_EMPTY.getCode());
				return null;
			}

			log.info("saveOrUpdatePromotion promotionList==>" + promotionList);
			UserMaster user = loginBean.getUserDetailSession();
			employeePromotion.setCreatedBy(user);
			log.info("regionList==>" + employeeAdminPromotionDto.getRegionList());
			String entityName = "";
			/*
			 * if(employeeAdminPromotionDto.getRegionList() != null) { for(EntityMaster e :
			 * employeeAdminPromotionDto.getRegionList()) { if(entityName.isEmpty()) {
			 * entityName = e.getName(); }else { entityName = entityName+","+e.getName(); }
			 * } }
			 */
			if (entityMasterList != null) {
				for (EntityMaster entity : entityMasterList) {
					if (employeeAdminPromotionDto.getRegionList().contains(entity.getId())) {
						if (entityName != "") {
							entityName = entityName + ", ";
						}
						entityName = entityName + entity.getName();
					}
				}
			}
			log.info("entityName==>" + entityName);
			employeePromotion.setRegName(entityName);

			List<EmployeePromotionDetails> details = new ArrayList<>();
			if (promotionList != null) {
				for (EmployeeAdminPromotionDTO dto : promotionList) {
					EmployeePromotionDetails pro = new EmployeePromotionDetails();

					/*
					 * EmployeeMaster emp = new EmployeeMaster(); emp.setId(dto.getEmployeeId());
					 * pro.setEmployeeMaster(emp);
					 */

					pro.setEmpId(dto.getEmployeeId());

					Department dep = new Department();
					dep.setId(dto.getDepartmentId());
					pro.setDepartment(dep);

					Designation des = new Designation();
					des.setId(dto.getDesignationId());
					pro.setCurruntDesignation(des);
					pro.setPromotedDesignation(des);

					pro.setDateOfBirth(dto.getDateOfBirth());
					pro.setStatus("InProgress");
					pro.setApprovedStatus("InProgress");
					pro.setPromotionFinalStatus("InProgress");

					pro.setCreatedBy(user);
					details.add(pro);
				}
			}
			employeePromotion.setEmployeePromotionDetails(details);
			// employeePromotion.setEmployeePromotionDetails(promotionList.);

			String url = PROMOTION_SERVER_URL + "/savepromotion";

			BaseDTO response = httpService.post(url, employeePromotion);
			if (response != null && response.getStatusCode() == 0) {
				if (pagePromotion.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.EMP_PROMOTION_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getCode());
			} else if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(response.getStatusCode());
			}

		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return "/pages/admin/promotion/listPromotionAdmin.xhtml?faces-redirect=true";
	}

	public String savePromotEmp() {
		try {
			// log.info("promotionList=>"+promotionList);
			String url = PROMOTION_SERVER_URL + "/savepromotemployee";

			BaseDTO response = httpService.put(url, promotionList);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getCode());
				return "/pages/admin/promotion/listPromotionAdmin.xhtml?faces-redirect=true";
			} else if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return null;
	}

	public String clearApprovedEmp() {
		try {
			selectedEmployeePromotionDetails = new EmployeePromotionDetails();
			lasyLoadApprovalEmp();
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return "/pages/admin/promotion/listPromotionApproval.xhtml?faces-redirect=true";
	}

	public void lasyLoadApprovalEmp() {

		lazyEmployeePromotionDetails = new LazyDataModel<EmployeePromotionDetails>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<EmployeePromotionDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					EmployeePromotionDetails request = new EmployeePromotionDetails();
					/*
					 * UserMaster user = login.getUserDetailSession();
					 * 
					 * log.info("getId==>" + user.getId()); request.setUserId(user.getId());
					 */
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(PROMOTION_SERVER_URL + "/approval/lazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<EmployeePromotionDetails> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeePromotionDetails>>() {
							});
					if (data == null) {
						log.info(" :: Employee Interchange Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						proTotalRecord = baseDTO.getTotalRecords();
						log.info(":: Job Advertisement Search Successfully Completed ::");
					} else {
						log.error(":: Job Advertisement Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeePromotionDetails res) {
				return res != null ? res.getPromotionId() : null;
			}

			@Override
			public EmployeePromotionDetails getRowData(String rowKey) {
				List<EmployeePromotionDetails> responseList = (List<EmployeePromotionDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeePromotionDetails res : responseList) {
					if (res.getPromotionId().longValue() == value.longValue()) {
						selectedEmployeePromotionDetails = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String approveApprovedEmp() {
		try {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			employeeAdminPromotionDto = getApprovedList();
			return "/pages/admin/promotion/employeePromotionApproval.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error("Exception occured in approve Approved Emp method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());

		}
		return null;
	}

	public EmployeeAdminPromotionDTO getApprovedList() {
		try {
			if (selectedEmployeePromotionDetails == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("selectedEmployeePromotionDetails==>" + selectedEmployeePromotionDetails);

			log.info("Selected Employee  Id : : " + selectedEmployeePromotionDetails.getPromotionId());
			String url = PROMOTION_SERVER_URL + "/getpromotiondatils/id/"
					+ selectedEmployeePromotionDetails.getPromotionId();

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			// log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeAdminPromotionDTO dto = mapper.readValue(jsonResponse,
					new TypeReference<EmployeeAdminPromotionDTO>() {
					});

			if (dto != null) {
				employeeAdminPromotionDto = dto;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee promotion Retrived  SuccessFully ");
			} else {
				log.error(" Employee promotion Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return employeeAdminPromotionDto;
	}

	public String showApprovedEmp() {
		try {
			employeeAdminPromotionDto = getApprovedList();
			return "/pages/admin/promotion/employeePromotionApprovalView.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());

		}
		return null;
	}

	public String clearAppPromotion() {
		try {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			lasyLoadApprovalEmp();
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return "/pages/admin/promotion/listPromotionApproval.xhtml?faces-redirect=true";
	}

	public String adminRejectPromotion() {
		String returnPage = null;
		try {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			employeeAdminPromotionDto.setEmpStatus("Rejected");
			returnPage = updateAdminStatus();
			lasyLoadApprovalEmp();
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return returnPage;
	}

	public String adminApprovedPromotion() {
		String returnPage = null;
		try {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			employeeAdminPromotionDto.setEmpStatus("Approved");
			returnPage = updateAdminStatus();
			lasyLoadApprovalEmp();
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return returnPage;
	}

	public String updateAdminStatus() {
		try {
			log.info("employeeAdminPromotionDto==>" + employeeAdminPromotionDto);
			String url = PROMOTION_SERVER_URL + "/updateapprovedpromotemployee";

			BaseDTO response = httpService.put(url, employeeAdminPromotionDto);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getCode());
				return "/pages/admin/promotion/listPromotionApproval.xhtml?faces-redirect=true";
			} else if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return null;

	}

	public void updatePromotionPeriod() {

	}

}
