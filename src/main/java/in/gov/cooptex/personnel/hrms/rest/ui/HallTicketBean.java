package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.NominalDetailsStatus;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.ExamCentre;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.NominalDetails;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Log4j2
@Scope("session")
@Service("hallTicketBean")
public class HallTicketBean {
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();

	String url;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	List<NominalDetails> nominalDetailsList, selectedHallTicketList, hallTicketList, ticketList;

	@Setter
	@Getter
	List<Designation> designationList;

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	List<CasteMaster> casteList;

	@Setter
	@Getter
	List<Community> communityList;

	@Setter
	@Getter
	List<Designation> designations;

	@Setter
	@Getter
	List<ExamCentre> examCentreList;

	@Setter
	@Getter
	NominalDetails nominalDetails, selectedHallTicket, selectedNominal;

	@Getter
	@Setter
	String action = "";

	@Setter
	private DefaultStreamedContent downloadFile = null;

	@Getter
	@Setter
	LazyDataModel<NominalDetails> hallTicketLazyList;

	@Getter
	@Setter
	int totalRecords = 0;

	String centreAddress = null;

	private String hattticketPath, tamilNote, englishNote, coOpTexlogoPath, downloadPath,employmentExchangeEngNote;

	@Autowired
	NominalDetailsBean nominalDetailsBean;

	@PostConstruct
	public void init() {
		loadLazyHallTicketList();
		tamilNote = commonDataService.getAppKeyValue("HALLTICKET_NOTE_TAMIL");
		englishNote = commonDataService.getAppKeyValue("HALLTICKET_NOTE_ENGLISH");
		employmentExchangeEngNote = commonDataService.getAppKeyValue("EMPLOYMENT_EXCHANGE_NOTE_ENGLISH");
		loadData();
		loadHallTicketPath();
	}

	public void loadHallTicketPath() {
		try {
			coOpTexlogoPath = commonDataService.getAppKeyValue("CALL_LETTER_LOGO_PATH");
			downloadPath = commonDataService.getAppKeyValue("CALL_LETTER_DOWNLOAD_PATH");
			File file = new File(downloadPath);
			hattticketPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(hattticketPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Hall Ticket Path ::" + path);
		} catch (Exception e) {
			log.error("Exception Occured while loading hallticket Path ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadData() {
		designationList = commonDataService.loadDesignation();
		casteList = commonDataService.getAllCasteMaster();
		communityList = commonDataService.getAllCommunity();
		examCentreList = commonDataService.loadExamCentre();
	}

	public void downloadHallTicketPdf() {
		if (selectedHallTicketList == null || selectedHallTicketList.size() == 0) {
			log.error(" Please Select one Hall Ticket");
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return;
		}
		log.info("<< ===  Insdide Download Hall Ticket PDF === >>" + selectedHallTicketList);
		try {
			if (selectedHallTicketList.size() > 1) {
				log.info("Hall Ticket Path is :: " + hattticketPath);
				File fPath = new File(hattticketPath);
				//FileUtils.cleanDirectory(fPath);
				selectedHallTicketList.forEach(htList -> {
					createFile(htList, selectedHallTicketList.size());
				});
				ByteArrayInputStream bis = new ByteArrayInputStream(AppUtil.zip(hattticketPath));
				InputStream stream = bis;
				downloadFile = new DefaultStreamedContent(stream, "application/zip", "hallticket.zip",
						Charsets.UTF_8.name());
				//FileUtils.cleanDirectory(fPath);
			} else {
				NominalDetails hallTicket = selectedHallTicketList.get(0);
				createFile(hallTicket, selectedHallTicketList.size());
			}

		} catch (Exception e) {
			log.error("Exception Occured While Downloading Hall Ticket", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void createFile(NominalDetails hallTicket, Integer fileSize) {
		log.info("< == = == =  Inside Create File = == = = = ==  >>" + fileSize);
		try {
			JasperReport jasperReportMCQon;
			JasperDesign jasperDesignMCQon;
			JasperPrint report = null;
			Map<String, Object> map = new HashMap<String, Object>();
			List<NominalDetails> data = new ArrayList<NominalDetails>();
			data.add(null);
			jasperDesignMCQon = JRXmlLoader.load(getClass().getResourceAsStream("/hallticket.jrxml"));
			jasperReportMCQon = JasperCompileManager.compileReport(jasperDesignMCQon);
			Map<String, Object> params = new HashMap<String, Object>();
			map.put("APPL_POST", hallTicket.getAppliedPost().getName());
			map.put("REG_NO", hallTicket.getRegisterNumber());
			map.put("NAME", hallTicket.getCandidateName());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String examDate = sdf.format(hallTicket.getExaminationDate());
			String timeSlot = examDate + " / " + hallTicket.getTimeSlot();
			map.put("EXAM_SCHEDULE", timeSlot);
			map.put("FATHER_NAME", hallTicket.getFatherName());
			if (hallTicket.getExamCentre() != null && hallTicket.getExamCentre().getSubjectMaster() != null
					&& hallTicket.getExamCentre().getSubjectMaster().getName() != null) {
				map.put("SUBJECT", hallTicket.getExamCentre().getSubjectMaster().getName());
			} else {
				map.put("SUBJECT", "");
			}

			map.put("CENTRE_NAME", hallTicket.getExamCentre().getExamCentreName());
			centreAddress = "";
			if (hallTicket.getExamCentre().getAddressLineOne() != null
					&& !StringUtils.isEmpty(hallTicket.getExamCentre().getAddressLineOne().trim())) {
				centreAddress = centreAddress + hallTicket.getExamCentre().getAddressLineOne();
			}

			if (hallTicket.getExamCentre().getAddressLineTwo() != null
					&& !StringUtils.isEmpty(hallTicket.getExamCentre().getAddressLineTwo().trim())) {
				centreAddress = centreAddress + "," + hallTicket.getExamCentre().getAddressLineTwo();
			}

			if (hallTicket.getExamCentre().getAddressLineThree() != null
					&& !StringUtils.isEmpty(hallTicket.getExamCentre().getAddressLineThree().trim())) {
				centreAddress = centreAddress + "," + hallTicket.getExamCentre().getAddressLineThree();
			}

			if (hallTicket.getExamCentre().getPincode() != null
					|| !StringUtils.isEmpty(hallTicket.getExamCentre().getPincode())) {
				centreAddress = centreAddress + "," + hallTicket.getExamCentre().getPincode();
			}
			map.put("CENTRE_ADDRS", centreAddress);

			map.put("TAMIL_NOTE", tamilNote);
			if(hallTicket.getJobApplication()!=null && hallTicket.getJobApplication().getCandidateType().equalsIgnoreCase("EMPLOYMENTEXCHANGE")) {
				map.put("ENG_NOTE",employmentExchangeEngNote);
			}else {
			map.put("ENG_NOTE", englishNote);
			}
			map.put("CO_OP_TEX_LOGO", coOpTexlogoPath.trim());

			params.put("map", map);
			log.info(" Hall ticket Jasper report parameters are :: " + params);
			JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(data);
			report = JasperFillManager.fillReport(jasperReportMCQon, params, datasource);

			ByteArrayInputStream bais = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(report));
			if (fileSize > 1) {
				JasperExportManager.exportReportToPdfFile(report,
						hattticketPath + hallTicket.getRegisterNumber() + "_hallTicket.pdf");
			} else {
				downloadFile = new DefaultStreamedContent(bais, "pdf",
						hallTicket.getRegisterNumber() + "_hallTicket.pdf");
			}
		} catch (Exception e) {
			log.error(" Exception in Hall Ticket Generation", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public DefaultStreamedContent getDownloadFile() {
		return downloadFile;
	}

	public List<NominalDetails> loadHallTicketListByNotification(String notificationNo) {
		if (notificationNo == null || notificationNo.isEmpty()) {
			log.info(" Selected Notification Number is empty ");
			errorMap.notify(ErrorDescription.NOMINAL_DETAILS_NOTIFICATION_IS_EMPTY.getErrorCode());
			return null;
		}
		nominalDetailsList = new ArrayList<>();
		log.info("Selected Notification Number is  :- " + notificationNo);
		try {
			NominalDetails request = new NominalDetails();
			request.setNotificationNo(notificationNo);
			url = SERVER_URL + "/hallticket/gethallticketlistbynotificationno";
			log.info(" Get Hall Tickt By NotificationNumber URL :: " + url);
			log.info(" Get Hall Tickt By NotificationNumber Request :: " + request);
			BaseDTO baseDTO = httpService.post(url, request);
			// log.info("Get Hall Tickt By Id Response :: " + baseDTO);
			if (baseDTO == null) {
				log.error(" BaseDto Returned Null Values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getResponseContents() != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				nominalDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<NominalDetails>>() {
				});
			} else {
				log.error(" Nominal List By Desgnation no record found");
				errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Nominal Details By Desgnation for Hall ticket :: " + nominalDetailsList);
			} else {
				log.info("Error in Nominal Details By Desgnation with status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(" Exception Occured while Getting HallTicket Details ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return nominalDetailsList;
	}

	public String clear() {
		selectedHallTicket = new NominalDetails();
		return "/pages/personnelHR/listHallTicket.xhtml?faces-redirect=true";

	}

	public String loadHallTicketList() {
		log.info(" ::  Inside Load Hall Ticket List :: ");
		loadLazyHallTicketList();
		return "/pages/personnelHR/listHallTicket.xhtml?faces-redirect=true";
	}

	public String fetchHallTicketList(NominalDetails nominal) {
		if (nominal == null || nominal.getJobApplication() == null
				|| nominal.getJobApplication().getJobAdvertisement() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber() == null
				|| nominal.getJobApplication().getJobAdvertisement().getNotificationNumber().isEmpty()) {
			log.error(" Selected Hall Ticket Notification Number is empty");
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return null;
		}
		if (nominal.getStatus() != null && nominal.getStatus().equals(NominalDetailsStatus.INPROGRESS)) {
			log.info(" Nominal Details is not Approved ");
			errorMap.notify(ErrorDescription.NOMINAL_IS_NOT_APPROVED.getErrorCode());
			return null;
		}
		log.info(" ::  Inside LOad Hall Ticket List :: "
				+ nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		hallTicketList = loadHallTicketListByNotification(
				nominal.getJobApplication().getJobAdvertisement().getNotificationNumber());
		return "/pages/personnelHR/viewHallTicket.xhtml?faces-redirect=true";
	}

	public void loadLazyHallTicketList() {
		log.info("< --  Start Nominal Lazy Load -- > ");
		hallTicketLazyList = new LazyDataModel<NominalDetails>() {
			private static final long serialVersionUID = 1L;

			public List<NominalDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<NominalDetails> data = mapper.readValue(jsonResponse,
							new TypeReference<List<NominalDetails>>() {
							});
					if (data == null) {
						log.info(" Nominal Response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Hall ticket Search Successfully Completed");
					} else {
						log.error(" Hall Ticket Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("loadLazyHallTicketList exception ==>", e);
				}
				return null;
			}

			@Override
			public Object getRowKey(NominalDetails res) {
				return res != null ? res.getNominalId() : null;
			}

			@Override
			public NominalDetails getRowData(String rowKey) {
				List<NominalDetails> responseList = (List<NominalDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (NominalDetails res : responseList) {
					if (res.getNominalId().longValue() == value.longValue()) {
						selectedHallTicket = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	/**
	 * purpose will call server when search filtered
	 * 
	 * @param first
	 *            - page number
	 * @param pageSize
	 *            - how many records need to be fetched
	 * @param sortField
	 *            - which filed to be sorted
	 * @param sortOrder
	 *            - sort order that to be followed (ascending or descending)
	 * @param filters
	 *            - search filters
	 * @return BaseDTO with list of Nominal based filters and pages size
	 */
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			NominalDetails request = new NominalDetails();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);
				if (entry.getKey().contains("appliedPost") && entry.getKey().equals("appliedPost.name")) {
					log.info("recruitment for post : " + value);
					Designation des = new Designation();
					des.setName(value);
					request.setAppliedPost(des);
				}
				if (entry.getKey().contains("recuritmentYear")) {
					log.info("Requitment Year : " + value);
					request.setRecuritmentYear(Integer.parseInt(value));
				}

				if (entry.getKey().contains("createdDate")) {
					log.info("createdDate : " + AppUtil.serverDateFormat(value));
					request.setCreatedDate(AppUtil.serverDateFormat(value));
				}

				if (entry.getKey().contains("jobApplication.jobAdvertisement.notificationNumber")) {
					log.info("Notification No : " + value);
					JobAdvertisement adv = new JobAdvertisement();
					JobApplication ja = new JobApplication();
					adv.setNotificationNumber(value);
					ja.setJobAdvertisement(adv);
					request.setJobApplication(ja);
				}
			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/hallticket/getlazyhallticketlist", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Nominal List search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String back() {
		if (action.equalsIgnoreCase("HTBACK")) {
			log.info(" inside hall ticket list back ");
			return loadHallTicketList();
		} else if (action.equalsIgnoreCase("NLBACK")) {
			log.info(" inside nominal list back ");
			return nominalDetailsBean.loadNominalList();
		}
		return null;
	}

	public String viewClear() {
		selectedHallTicketList = null;
		downloadFile = null;
		return "/pages/personnelHR/viewHallTicket.xhtml?faces-redirect=true";

	}
}
