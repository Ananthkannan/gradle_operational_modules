/**
 * 
 */
package in.gov.cooptex.operation.fm.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import in.gov.cooptex.admin.vehicle.model.FuelCoupon;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.model.AppFeature;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.FileMovementConfig;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Log4j2
@Service("fileMovementConfigBean")
@Scope("session")
public class FileMovementConfigBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7555120159391217305L;

	private final String FILE_MOVEMENT_CONFIG_ADD_URL = "/pages/operation/filemovement/createFileMovementConfig.xhtml?faces-redirect=true;";
	
	
	private final String INPUT_FORM_LIST_URL = "/pages/operation/filemovement/listFileMovementConfig.xhtml?faces-redirect=true;";
	
	
	
	private String serverURL = null;
	
	
	@Autowired
	AppPreference appPreference;
	
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	private String action;
	
	@Getter
	@Setter
	boolean disableAddButton = false;
	
	@Getter
	@Setter
	FileMovementConfig fileMovementConfig;
	
	@Getter
	@Setter
	FileMovementConfig selectedFileMovementConfig;
	
	@Getter
	@Setter
	boolean  view = true;
	
	
	boolean forceLogin = false;

	// @Getter
	// @Setter
	// AppFeature appFeature;
	//
	// @Getter
	// @Setter
	// Integer forwardLevel;
	//
	// @Getter
	// @Setter
	// SectionMaster sectionMaster;
	//
	// @Getter
	// @Setter
	// Designation designation;

	@Autowired
	CommonDataService commonDataService;
	
	
	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Integer forwardLevelSize;
	
	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	List<AppFeature> activeFileMovementAppFeatureList = new ArrayList<AppFeature>();

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList = new ArrayList<SectionMaster>();

	@Getter
	@Setter
	List<Designation> designationList = new ArrayList<Designation>();

	@Getter
	@Setter
	List<FileMovementConfig> fileMovementConfigList = new ArrayList<FileMovementConfig>();
	
	@Getter
	@Setter
	List<FileMovementConfig> fileMovementConfigListForEdit = new ArrayList<FileMovementConfig>();
	
	@Getter
	@Setter
	LazyDataModel<FileMovementConfig> fileMovementConfigLazyList;

	@Getter
	@Setter
	List<Integer> forwardLevelNumberList = new ArrayList<Integer>();
	
	@Getter
	@Setter
	Set<Integer> selectedLevelSet = new LinkedHashSet<Integer>();
	
	
	
	@Getter
	@Setter
	boolean disableAddSelectAppFeatureDropDown = false;
	
	@Getter
	@Setter
	boolean disableForwardlevelDropDown = false;
	@Getter
	@Setter
	boolean disableSectionMasterButton =false;
	
	@Getter
	@Setter
	boolean disableDesignationButton = false;
	
	@Getter
	@Setter
	boolean disableDeleteCommandLinkButton = false;
	
	
	@Getter
	@Setter
	boolean disableSubmitButton = false;
	
	@Getter
	@Setter
	Long appFeatureId ;
	
	@Getter
	@Setter
	String maxValueStr;

	boolean editFileMovementConfig = false;
	/**
	 * 
	 */
	public FileMovementConfigBean() {
		log.info("Inside FileMovementConfigBean()");
	}
	
	public String  loadFileMovementListPage() {
		init();
		//clearListPage();
		return INPUT_FORM_LIST_URL;
	}

	@PostConstruct	
	public void init() {
		log.info("the post construct method of FileMovement is get Started");
		disableAddButton = false;
		log.info("Inside init()");
		fileMovementConfigList.clear();
		selectedLevelSet.clear();
		disableAddSelectAppFeatureDropDown = false;
		selectedFileMovementConfig = new FileMovementConfig();
		loadValues();
		loadValuesForCreate();
		loadLazyFileMovementConfigList();
		
		
		
	}

	
	
	
	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			fileMovementConfig = new FileMovementConfig();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}
	
	
	public String clearListPage() {
		
		if (selectedFileMovementConfig == null) {
			Util.addWarn("Please select one File Movement Config");
			return null;
		}else {
			log.info("<---- Start FileMovementConfigList . clear ----->");
			loadLazyFileMovementConfigList();
			selectedFileMovementConfig = new FileMovementConfig();
			log.info("<---- End FileMovementConfigList . clear ----->");
			disableAddButton = false;
			return INPUT_FORM_LIST_URL;
		}
		

	}

	private void loadValuesForCreate() {
		log.info("Inside loadValuesForCreate()");
		fileMovementConfig = new FileMovementConfig();
		activeFileMovementAppFeatureList = commonDataService.loadActiveFileMovementAppFeatureList();
		sectionMasterList = commonDataService.getActiveSectionMasters();
		designationList = commonDataService.loadActiveDesignations();
		
		log.info("AppconfigEnum"+AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE);
		if(AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE != null) {
			log.info("appconfigenum Enum value to string"+ AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE.toString() );
		}
		
		
		String maxValueStr=commonDataService.getAppKeyValue("FORWARD_LEVEL_MAX");

		Integer forwardLevelSize =Integer.valueOf(maxValueStr);
		
		log.info("forwardLevelSize >>>> " + forwardLevelSize);
		for (int i = 1; i <= forwardLevelSize; i++) {
			forwardLevelNumberList.add(i);
		}

	}

	public void addItem() {
		log.info("Inside addItem()");
		try {
			
			validateInput();
			FileMovementConfig item = new FileMovementConfig();
			item.setAppFeature(fileMovementConfig.getAppFeature());
			item.setForwardLevel(fileMovementConfig.getForwardLevel());
			item.setSectionMaster(fileMovementConfig.getSectionMaster());
			item.setDesignation(fileMovementConfig.getDesignation());
			
			 
			  
			forwardLevelNumberList.removeIf(n ->  n == fileMovementConfig.getForwardLevel());

			log.info("appFeature.id >>>> " + fileMovementConfig.getAppFeature().getId());
			log.info("appFeature.name >>>> " + fileMovementConfig.getAppFeature().getFeatureName());
			log.info("forwardLevel >>>> " + fileMovementConfig.getForwardLevel());
			log.info("sectionMaster.id >>>> " + fileMovementConfig.getSectionMaster().getId());
			log.info("designation.id >>>> " + fileMovementConfig.getDesignation().getId());

			disableAddSelectAppFeatureDropDown = true;
			
			fileMovementConfigList.add(item);
			selectedLevelSet.add(item.getForwardLevel());
		} catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
		}
	}

	private void showClientError(String errorMessage) {
		
		AppUtil.addError(errorMessage);
	}

	private void validateInput() throws UIException {
		if (fileMovementConfig == null) {
			throw new UIException("Invalid input");
		} else if (fileMovementConfig.getAppFeature() == null) {
			throw new UIException("Please select app feature");
		} else if (fileMovementConfig.getForwardLevel() == null) {
			throw new UIException("Please select forward level");
		} else if (fileMovementConfig.getDesignation() == null) {
			throw new UIException("Please select designation");
		} else if (selectedLevelSet.contains(fileMovementConfig.getForwardLevel())) {
			throw new UIException("Selected level already exists");
		} else if (!forwardLevelInOrder()) {
			throw new UIException("Selected level not in order");
		} else {

		}

	}

	private boolean forwardLevelInOrder() {
		if (!selectedLevelSet.isEmpty()) {
			log.info("selectedLevelSet"+selectedLevelSet);
			Integer lastItemNumber = 0;
			for (Integer value : selectedLevelSet) {
				lastItemNumber = value;
			}
			log.info("lastNumber is"+ lastItemNumber);
			int expectedNumber = lastItemNumber.intValue() + 1;
			log.info("expectedItemValue : " + expectedNumber);
			log.info("fileMovementConfig.getForwardLevel().intValue() : "
					+ (fileMovementConfig.getForwardLevel().intValue()));
			if (fileMovementConfig.getForwardLevel().intValue() != expectedNumber) {
				return false;
			}
		}

		return true;
	}

	public void removeItem(FileMovementConfig item) {
		log.info("Inside removeItem()");
		log.info("  before removel  selectedLevelSet is"+selectedLevelSet.toString());
		log.info(" current forwardLevelNumberList is"+forwardLevelNumberList.toString());
		
		
		   int a = forwardLevelNumberList.get(0);
		   int b =  item.getForwardLevel();
		   log.info("number value of a"+a);
		   log.info("number value of b"+b);
		   int c;
		   
	// This condition for not allowing user to delete the add item in-order 
	// a is first number in forwardLevelNumberList	   
	// b is forwardLevelNumber which is has to delete from list  c is expected number if c==b we allow user to delete the item from list	   
   try {
		   if(  a != 0) {   
			  c= a-1;
			   log.info("number value of c"+c);
			  if(c== b) {
				  
				  
				  fileMovementConfigList.remove(item);
					selectedLevelSet.remove(item.getForwardLevel());
					forwardLevelNumberList.add(item.getForwardLevel());
					Collections.sort(forwardLevelNumberList);
			
					log.info(" after removed  forwardLevelNumberList is"+forwardLevelNumberList.toString());
					log.info(" after removed  selectedLevelSet is"+selectedLevelSet.toString());
			  }
			  else {
				  log.info("deleting element is not in order");
				  throw new UIException("Selected level not in order");
			  }
			   
			   
		   }
   } catch (UIException ex) {
	  log.info("Inside UIException");
		showClientError(ex.getMessage());
		log.error("UIException at addItem() >>>> " + ex.getMessage());
	}catch (Exception ex) {
		 log.info("Inside Exception");
		log.error("Exception at addItem() >>>> ", ex);
	} 
   
}

	/**
	 * @return
	 */
	/*public String loadFileMovementCreatePage() {
		log.info("Inside loadFileMovementCreatePage()");
		loadValuesForCreate();

		return FILE_MOVEMENT_CONFIG_ADD_URL;
	}*/
	
	
	
	public String clear(List<FileMovementConfig> fileMovementConfiglist) {
		
		try {
			if (fileMovementConfiglist != null) {
				log.info("fileMovementConfiglist"+fileMovementConfiglist.toString());
				//Util.addWarn("Do you want to clear data in Data table");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFileMovementDataTableDelete').show();");
			} else {
				//loadValuesForCreate();
				
				fileMovementConfig = new FileMovementConfig();
				disableAddSelectAppFeatureDropDown = false;
				return FILE_MOVEMENT_CONFIG_ADD_URL;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		
		return null;
		
	}
	
	public String deleteFileMovemetDatatableData(List<FileMovementConfig> fileMovementConfiglist) {
		
		fileMovementConfiglist.clear();
		selectedLevelSet.clear();
		disableAddSelectAppFeatureDropDown = false;
		//loadValuesForCreate();
		fileMovementConfig = new FileMovementConfig();
		return FILE_MOVEMENT_CONFIG_ADD_URL;
		
		
		
	}
	
	
	public void loadLazyFileMovementConfigList() {
		disableAddButton = false;
		log.info("<===== Starts FuelCoupon Bean.loadLazyFuelCouponList======>");
		fileMovementConfigLazyList = new LazyDataModel<FileMovementConfig>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<FileMovementConfig> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL + "/filemovementconfig/getfileMovementConfigListBYLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get filemovementconfig list");
					log.info("FileMovementConfig Total records"+ response.getTotalRecords());
					
					log.info("the object returnend from FileMovementConfigControler"+response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						fileMovementConfigList = mapper.readValue(jsonResponse,new TypeReference<List<FileMovementConfig>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} 
					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"+ fileMovementConfigList.toString());
				log.info("Ends lazy load....");
				
				return fileMovementConfigList;
			}

			@Override
			public Object getRowKey(FileMovementConfig res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FileMovementConfig getRowData(String rowKey) {
				try {
				for (FileMovementConfig fileMovementConfig : fileMovementConfigList) {
					if (fileMovementConfig.getId().equals(Long.valueOf(rowKey))) {
						selectedFileMovementConfig = fileMovementConfig;
						return fileMovementConfig;
					}
				}				
				}
				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
			
		};
		log.info("<===== Ends selectedFileMovementConfig Bean.loadLazyselectedFileMovementConfigList ======>");
	}
	
	public String saveFileMovementConfig() {

	
		log.info("the filemovementConfing list from page"+fileMovementConfig.toString());
		
		log.info("editFileMovementConfig"+editFileMovementConfig);
		
		if(editFileMovementConfig) {
			try {
				log.info("Delete fileMovementConfig the id is [" +fileMovementConfig.getAppFeature()+ "]");
				
		log.info("fileMovementConfig for delete"+fileMovementConfig.toString());
				BaseDTO baseDTO = new BaseDTO();
				String url = serverURL +"/filemovementconfig/deleteFileMovementConfigListBYAppFeatureId";
				baseDTO = httpService.post(url, fileMovementConfig);
				
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 1019) {
						log.info("FileMovementConfigListBYAppFeatureId deleted successfully....!!!");
						errorMap.notify(baseDTO.getStatusCode());
						
					}
					if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
						log.info("Same user update invalidating session-------->");
						forceLogin = true;
						return forceLogin();

						

					} else {
						log.info("Error occured in Rest api ... ");
						// Util.addError(baseDTO.getErrorDescription());
						errorMap.notify(baseDTO.getStatusCode());
					}
				}
			} catch (Exception e) {
				log.error("Exception occured while deleting user ....", e);
			} 
			editFileMovementConfig = false;
		}
		
		log.info("fileMovementConfigList != null"+fileMovementConfigList != null);
		
		log.info("!(fileMovementConfigList.isEmpty())"+ !(fileMovementConfigList.isEmpty()));
		
		
		if( !(fileMovementConfigList.isEmpty())) {
			log.info("list is not empty");
			try {
				log.info("...... filemovementconfig submit begin ....");
				BaseDTO baseDTO = null;
				String url = serverURL + "/filemovementconfig/savefileMovementConfigList";
				baseDTO = httpService.post(url, fileMovementConfigList);
				
				log.info(" baseDTo return From Data Base"+baseDTO.toString());
				
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
						log.info("filemovementconfigList is saved successfully");
							
						//errorMap.notify(baseDTO.getStatusCode());
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.FILE_MOVEMENT_HIERARCHY_SAVE_SUCCESS).getErrorCode());
						
					}
				}else {
					log.info("base dto is empty from data base for file movement confing");
				}
			} catch (Exception e) {
				log.error("Error while creating CountryMaster" + e);
			}
		
			log.info("...... fileMovementConfiglist submit ended ....");
	
	}else {
		log.info("the list is empty");
		try {
			throw new UIException("Invalid input");
		} catch (UIException ex) {
			// TODO Auto-generated catch block
			showClientError(ex.getMessage());
			ex.printStackTrace();
		}
		
		return null;
	}
		
		
		
		
		return INPUT_FORM_LIST_URL;
	}
	
	
	
	public String addFileMovementConfig() {
		view = true;
		log.info("<---- Start addFileMovementConfig ---->");
		forwardLevelNumberList.clear();
		fileMovementConfigList.clear();
		selectedLevelSet.clear();
		disableAddSelectAppFeatureDropDown = false;
		disableAddButton = false;
		disableSubmitButton =  false;
		view = true;
		disableAddSelectAppFeatureDropDown = false;
		disableForwardlevelDropDown= false;
		disableSectionMasterButton= false;
		disableDesignationButton= false;
		disableDeleteCommandLinkButton = false;
		loadValues();
		loadValuesForCreate();
		return FILE_MOVEMENT_CONFIG_ADD_URL;

	}
	
	
	public String cancelFileMovementConfig() {
		
		return INPUT_FORM_LIST_URL;
	}
	
	
	
	public String viewFileMovementConfig() {
		log.info("<<<<< -------Start viewFileMovementConfig called ------->>>>>> ");
		view = false;
		disableAddButton = true;
		
		disableAddSelectAppFeatureDropDown = true;


		disableForwardlevelDropDown= true;


		disableSectionMasterButton= true;


		disableDesignationButton= true;


		disableDeleteCommandLinkButton = true;
		
		disableSubmitButton =  true;
		

		if (selectedFileMovementConfig == null) {
			Util.addWarn("Please select one File Movement Config");
			return null;
		}else {
			
				log.info("the fileMovementConfig from page"+fileMovementConfig.toString());
				
				try {
					log.info("...... viewFileMovementConfig begin ....");
					BaseDTO baseDTO = null;
					String url = serverURL +  "/filemovementconfig/getfileMovementConfigListByAppFeatureId";
					baseDTO = httpService.post(url, selectedFileMovementConfig);
					
					log.info(" baseDTo return From Data Base"+baseDTO.getResponseContents());
					fileMovementConfigList.clear();
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						fileMovementConfigList = mapper.readValue(jsonResponse,new TypeReference<List<FileMovementConfig>>() {
								});
						
					} 
					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					AppFeature appFeature = fileMovementConfigList.get(0).getAppFeature();
					fileMovementConfig.setAppFeature(appFeature);
				} catch (Exception e) {
					log.error("Error while creating CountryMaster" + e);
				}
			
				log.info("...... fileMovementConfiglist submit ended ....");
			
		}

		
		log.info("<<<<< -------End fileMovementConfig called ------->>>>>> ");
		
		return FILE_MOVEMENT_CONFIG_ADD_URL;

	}

	
	
	public String editFileMovementConfig() {
		log.info("<<<<< -------Start editFileMovementConfig called ------->>>>>> ");
		disableAddButton = false;
		view = true;
		disableAddSelectAppFeatureDropDown = true;
		disableForwardlevelDropDown= false;
		disableSectionMasterButton= false;
		disableDesignationButton= false;
		disableDeleteCommandLinkButton = false;
		disableSubmitButton =  false;
		
		//fileMovementConfig=new FileMovementConfig();
		
		if (selectedFileMovementConfig == null) {
			Util.addWarn("Please select one File Movement Config");
			return null;
		}else {
			
			try {
				
				log.info("...... viewFileMovementConfig begin ....");
				BaseDTO baseDTO = null;
				String url = serverURL + "/filemovementconfig/getfileMovementConfigListByAppFeatureId";
				baseDTO = httpService.post(url, selectedFileMovementConfig);
				log.info(" baseDTo return From Data Base"+baseDTO.getResponseContents());
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					fileMovementConfigList = mapper.readValue(jsonResponse,new TypeReference<List<FileMovementConfig>>() {
						
							});
					selectedLevelSet.clear();
					fileMovementConfigList.forEach(n ->selectedLevelSet.add(n.getForwardLevel()));
					log.info("selectedLevelSet before sort inside the edit function"+selectedLevelSet.toString());
				}  
				else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				
				
			}catch(Exception e){
				log.error("Error while creating CountryMaster" + e);
			}
			
			
			log.info("...... fileMovementConfiglist submit ended ....");
			
			
			log.info("...... GEt ForwardLevelNumber by Appfeature is started ....");
			
			AppFeature appFeature = fileMovementConfigList.get(0).getAppFeature();
			
			try {
				log.info("...... getting ForwardLevelNumber begin ....");
				BaseDTO baseDTO = null;
				String url = serverURL + "/filemovementconfig/getForwardLevelNumberListBasedOnAppFeature";
				baseDTO = httpService.post(url, appFeature);
				log.info(" baseDTo return From Data Base"+baseDTO.getResponseContents());
				List<Integer> ForwardLevelNumberList =  new ArrayList<Integer>();
				
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					ForwardLevelNumberList = mapper.readValue(jsonResponse,new TypeReference<List<Integer>>() {
							});
					
				}else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("the ForwardLevelNumberList from data base"+ForwardLevelNumberList.toString());
				
				
				loadValuesForCreate();
				fileMovementConfig.setAppFeature(appFeature);
				forwardLevelNumberList.clear();
				String maxValueStr=commonDataService.getAppKeyValue("FORWARD_LEVEL_MAX");
				Integer forwardLevelSize =Integer.valueOf(maxValueStr);
				log.info("forwardLevelSize >>>> " + forwardLevelSize);
				for (int i = 1; i <= forwardLevelSize; i++) {
					forwardLevelNumberList.add(i);
				}
				log.info("the ForwardLevelNumberList before removel function "+forwardLevelNumberList.toString());
				forwardLevelNumberList.removeAll(ForwardLevelNumberList);
				log.info("the ForwardLevelNumberList is to browser based on appfeature id"+forwardLevelNumberList.toString());
			}catch(Exception e) {
				
			}
			
			fileMovementConfig.setAppFeature(appFeature);
		}
		

		
		log.info("<<<<< -------End editx File Movement Config called ------->>>>>> ");
		
		
		log.info("the ForwardLevelNumberList is to browser based on appfeature id"+forwardLevelNumberList.toString());
		
		log.info("set appfeature to browser "+fileMovementConfig.getAppFeature()); 
		
		editFileMovementConfig = true;
		
		return FILE_MOVEMENT_CONFIG_ADD_URL;

	}
	
	
	public String deleteFileMovementConfig() {
		log.info("fileMovementConfig == null"+ fileMovementConfig == null);
		
		try {
			action = "Delete";
			if (selectedFileMovementConfig == null) {
				Util.addWarn("Please Select one File Movement Config");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFileMovementConfigListBYAppFeatureIdDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
		
	
		
	}
	
	
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	
	public String deleteFileMovementConfigListBYAppFeatureId() {
		disableSubmitButton =  false;
		try {
			log.info("Delete fileMovementConfig the id is [" +fileMovementConfig.getAppFeature()+ "]");
			
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/filemovementconfig/deleteFileMovementConfigListBYAppFeatureId";
			baseDTO = httpService.post(url, selectedFileMovementConfig);
			
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("FileMovementConfigListBYAppFeatureId deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;
					return forceLogin();

					

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}

		return INPUT_FORM_LIST_URL;
		
	}
	
	
	public FileMovementConfig onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		fileMovementConfig = ((FileMovementConfig) event.getObject());
		log.info("FileMovementConfig on row select"+fileMovementConfig.toString());
		
		appFeatureId = fileMovementConfig.getAppFeature().getId();
		disableAddButton = true;
		
		return fileMovementConfig;
	}
	
	
	public void filterForwardLevelNumberListBasedOnAppFeature( ) {
		
			log.info("filterForwardLevelNumberListBasedOnAppFeature is called");
			if(fileMovementConfig.getAppFeature()!= null) {
				AppFeature		appFeature= fileMovementConfig.getAppFeature() ;
				log.info("filterForwardLevelNumberListBasedOnAppFeature is called"+appFeature.toString());
				
				try {
					log.info("...... viewFileMovementConfig begin ....");
					BaseDTO baseDTO = null;
					String url = serverURL +  "/filemovementconfig/getForwardLevelNumberListBasedOnAppFeature";
					baseDTO = httpService.post(url, appFeature);
					
					log.info(" baseDTo return From Data Base"+baseDTO.getResponseContents());
					//fileMovementConfigList.clear();
					
					List<Integer> ForwardLevelNumberList =  new ArrayList<Integer>();
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						ForwardLevelNumberList = mapper.readValue(jsonResponse,new TypeReference<List<Integer>>() {
								});
						
					} 
					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					log.info("ForwardLevelNumberList is empty while change function"+ForwardLevelNumberList.size());
					if(ForwardLevelNumberList.size()== 0) {
						forwardLevelNumberList.clear();
						String maxValueStr=commonDataService.getAppKeyValue("FORWARD_LEVEL_MAX");
						Integer forwardLevelSize =Integer.valueOf(maxValueStr);
						log.info("forwardLevelSize >>>> " + forwardLevelSize);
						for (int i = 1; i <= forwardLevelSize; i++) {
							forwardLevelNumberList.add(i);
							
							}
						
					}else {
						forwardLevelNumberList.clear();
						String maxValueStr=commonDataService.getAppKeyValue("FORWARD_LEVEL_MAX");
						Integer forwardLevelSize =Integer.valueOf(maxValueStr);
						log.info("forwardLevelSize >>>> " + forwardLevelSize);
						for (int i = 1; i <= forwardLevelSize; i++) {
							forwardLevelNumberList.add(i);
							
							}
						forwardLevelNumberList.removeAll(ForwardLevelNumberList);
					}
					
					
					
					log.info("ForwardLevelNumberList To browser is"+forwardLevelNumberList.toString());
					
				} catch (Exception e) {
					log.info("ForwardLevelNumberList To browser is"+forwardLevelNumberList.toString());
					log.error("Error while creating CountryMaster" + e);
				}	

				
			}else {
				forwardLevelNumberList.clear();
				String maxValueStr=commonDataService.getAppKeyValue("FORWARD_LEVEL_MAX");
				Integer forwardLevelSize =Integer.valueOf(maxValueStr);
				log.info("forwardLevelSize >>>> " + forwardLevelSize);
				for (int i = 1; i <= forwardLevelSize; i++) {
					forwardLevelNumberList.add(i);
					
					}
			}
		
				
	

	
}
	
}
