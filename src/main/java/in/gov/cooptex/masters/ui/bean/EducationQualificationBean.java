package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("educationQualificationBean")
@Scope("session")
public class EducationQualificationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String EDUCATION_QUALIFICATION_LIST_PAGE = "/pages/masters/EducationQualification/listEducationQualification.xhtml?faces-redirect=true;";
	private final String EDUCATION_QUALIFICATION_ADD_PAGE = "/pages/masters/EducationQualification/createEducationQualification.xhtml?faces-redirect=true;";
	private final String EDUCATION_QUALIFICATION_VIEW_PAGE = "/pages/masters/EducationQualification/viewEducationQualification.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Getter @Setter
	QualificationMaster selectedQualification = new QualificationMaster();

	@Getter @Setter
	QualificationMaster qualification = new QualificationMaster();

	@Getter @Setter
	private RestTemplate restTemplate;

	@Getter @Setter
	private String Url = null;

	@Getter @Setter
	LazyDataModel<QualificationMaster> qualificationlazyLoadList;

	@Getter @Setter
	private Map<String, Object> filterMap;

	@Getter @Setter
	private SortOrder sortingOrder;

	@Getter @Setter
	private String sortingField;

	@Getter @Setter
	private Integer resultSize;

	@Getter @Setter 
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<QualificationMaster> qualificationList;
	
	public String showListQualificationPage() {
		log.info("showListQualificationPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		qualification = new QualificationMaster();
		selectedQualification = new QualificationMaster();
		loadQualificationListLazy();
		return EDUCATION_QUALIFICATION_LIST_PAGE;
	}
	
	public void loadQualificationListLazy() {

		log.info("<-----Qualification list lazy load starts------>");
		qualificationlazyLoadList = new LazyDataModel<QualificationMaster>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<QualificationMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters){
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					qualificationList = mapper.readValue(jsonResponse,
							new TypeReference<List<QualificationMaster>>() {
					});
					if (qualificationList == null) {
						log.info(" QualificationList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Qualification list search Successfully Completed");
					} else {
						log.error(" Qualification list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					log.info("<-----Qualification lists------>"+qualificationList.size());
					return qualificationList;

				}catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Qualification list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}



			@Override
			public Object getRowKey(QualificationMaster res) {
				return res != null ? res.getQualificationId() : null;
			}

			@Override
			public QualificationMaster getRowData(String rowKey) {
				List<QualificationMaster> responseList = (List<QualificationMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (QualificationMaster res : responseList) {
					if (res.getQualificationId().longValue() == value.longValue()) {
						selectedQualification = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			QualificationMaster request = new QualificationMaster();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("qualificationLevel")) {
					log.info("qualificationLevel : " + value);
					searchFilters.put("qualificationLevel", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("createdDate : " + value);
					searchFilters.put("createdDate",value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status",value);
				}	

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/qualification/getqualificationlistlazy", request);
			log.info("Search request response " + dataList);

		}catch(Exception e) {
			log.error("Exception Occured in Qualification load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}
	
	public String qualificationListAction() {
		log.info("<====== EducationQualificationBean.qualificationListAction Starts====> action   :::"+action);
		try {
			if(action.equalsIgnoreCase("ADD")) {
				log.info("<====== EducationQualificationBean add page called.......====>");
				selectedQualification = new QualificationMaster();
				qualification = new QualificationMaster();
				return EDUCATION_QUALIFICATION_ADD_PAGE;
			}
			if(action.equalsIgnoreCase("EDIT")) {
				log.info("<====== EducationQualificationBean add page called.......====>");
				if(selectedQualification==null ||  selectedQualification.getQualificationId() == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				qualification = getQualificationDetails(selectedQualification.getQualificationId());
				loadQualificationListLazy();
				log.info("seleted qualification::::::::"+qualification);
				return EDUCATION_QUALIFICATION_ADD_PAGE;
			}
			if(selectedQualification==null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if(action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmQualificationDelete').show();");
			} if(action.equalsIgnoreCase("VIEW")) {
				if(selectedQualification == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->"+selectedQualification.getQualificationId());
				
				selectedQualification = getQualificationDetails(selectedQualification.getQualificationId());
				log.info("selected Qualification Object:::"+selectedQualification);
				return "/pages/masters/EducationQualification/viewEducationQualification.xhtml?faces-redirect=true";
			}
			

		} catch (Exception e) {
			log.error("Exception Occured in EducationQualificationBean.qualificationListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EducationQualificationBean.qualificationListAction Ends====>");
		
		return null;
	}
	
	private QualificationMaster getQualificationDetails(Long qualificationId) {
		log.info("<====== EducationQualificationBean.getQualificationDetails starts====>"+qualificationId);
		try {
			if (selectedQualification == null || selectedQualification.getQualificationId() == null) {
				log.error(" No Qualification selected ");
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICATION_NO_SELECTED.getErrorCode());
			}
			log.info("Selected Qualification  Id : : " + selectedQualification.getQualificationId());
			String getUrl = SERVER_URL + "/qualification/view/id/:id";
			String url = getUrl.replace(":id", selectedQualification.getQualificationId().toString());

			log.info("Qualification Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			QualificationMaster qualificationDetail = mapper.readValue(jsonResponse, QualificationMaster.class);

			if (qualificationDetail != null) {
				qualification = qualificationDetail;
			} else {
				log.error("Qualification object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Qualification Retrived  SuccessFully ");
			} else {
				log.error(" Qualification Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return qualification;
		}catch(Exception e) {
			log.error("ExceptionException Ocured while get Qualification Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String submit() {
		log.info("<=======Starts  QualificationMasterBean.submit ======>");
		
		if(qualification.getQualificationLevel() == null || qualification.getQualificationLevel().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_NAME_REQUIRED.getErrorCode());
			return null;
		}
		
		if(qualification.getQualificationLocalLevel() == null || qualification.getQualificationLocalLevel().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_lNAME_REQUIRED.getErrorCode());
			return null;
		}
		
		if(qualification.getStatus() == null) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_STATUS_REQUIRED.getErrorCode());
			return null;
		}
		
		if(action.equalsIgnoreCase("ADD")) {
			qualification.setCreatedBy(loginBean.getUserDetailSession());
		}else if (action.equalsIgnoreCase("EDIT")) {
			qualification.setModifiedBy(loginBean.getUserDetailSession());
		}
		selectedQualification = new QualificationMaster();
		BaseDTO response= httpService.post(SERVER_URL + "/qualification/save",qualification);
//		errorMap.notify(response.getStatusCode());
		if(response.getStatusCode()==0) {
			if(action.equalsIgnoreCase("ADD")) {
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_SAVE_SUCCESS.getErrorCode());
			loadQualificationListLazy();}
			else 
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_UPDATE_SUCCESS.getErrorCode());
			loadQualificationListLazy();
			//return showListQualificationPage();
			return EDUCATION_QUALIFICATION_LIST_PAGE;
		}else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends QualificationMasterBean.submit ======>");
		return null;
	}
	
	public String deleteConfirm() {
		log.info("<--- Starts QualificationMasterBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/qualification/delete/" + selectedQualification.getQualificationId());
			if(response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_DELETE_SUCCESS.getErrorCode());
			loadQualificationListLazy();}
			else
				errorMap.notify(response.getStatusCode());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting Qualification....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends QualificationMasterBean.deleteConfirm ---->");
		return showListQualificationPage();
	}
	
	private String gotoViewPage() {

		if (selectedQualification == null || selectedQualification.getQualificationId() == null) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_ID_REQUIRED.getCode());
			return null;
		}

		qualification = getQualificationMasterById(selectedQualification.getQualificationId());

		return EDUCATION_QUALIFICATION_VIEW_PAGE;
	}

	public QualificationMaster getQualificationMasterById(Long id) {
		
		String URL = SERVER_URL + "/qualification/view/id/"+id;
		log.info("URL:::::::"+URL);
		QualificationMaster qualificationMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				qualificationMaster = mapper.readValue(jsonValue, QualificationMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return qualificationMaster;
	}
	

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		selectedQualification=new QualificationMaster();
		addButtonFlag = false;
		selectedQualification = ((QualificationMaster) event.getObject());
		log.info("<===Ends .onRowSelect ========>");
	}

}
