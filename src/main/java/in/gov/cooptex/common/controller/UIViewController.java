/**
 * 
 */
package in.gov.cooptex.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Controller
@Log4j2
@RequestMapping("/ui")
public class UIViewController {

	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String uiView() {
		log.info("uiView");
		return "jsp.ui.view";
	}

}
