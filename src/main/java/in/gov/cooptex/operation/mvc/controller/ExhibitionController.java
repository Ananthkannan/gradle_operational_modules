/**
 * 
 */
package in.gov.cooptex.operation.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.log4j.Log4j2;

/**
 * Controller that demonstrates tiles mapping, reguest parameters and path
 * variables.
 * 
 * @author GMR
 */
@Controller
@Log4j2
@RequestMapping("/exhibition")
public class ExhibitionController {

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public String exhibitionCreate() {
		log.info("Inside exhibitionCreate()");
		return "jsp.exhibition.create";
	}

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String exhibitionList() {
		log.info("Inside exhibitionList()");
		return "jsp.exhibition.list";
	}
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String exhibitionView() {
		log.info("Inside exhibitionView()");
		return "jsp.exhibition.view";
	}

}