package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.RegionSalesData;
import in.gov.cooptex.operation.dto.RegionWiseSalesData;
import in.gov.cooptex.operation.enums.RetailProductionPlanStage;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailProductionPlanBean")
@Scope("session")
public class RetailProductionPlanBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String HEAD_OFFICE_ADD_URL = "/pages/operation/retailproduction/retailSalesProductionPlanRequest.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_VIEW_URL = "/pages/operation/retailproduction/viewSalesProductionPlanRequest.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_LIST_URL = "/pages/operation/retailproduction/listRetailSalesProductionPlan.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_LIST_FINAL_URL = "/pages/operation/retailproduction/listRetailSalesProductionPlanFinal.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String buttonName;

	@Getter
	@Setter
	Integer index = null;

	@Getter
	@Setter
	List statusList;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	String planFromMonthYear;

	@Getter
	@Setter
	String planToMonthYear;

	@Getter
	@Setter
	Integer planFromMonth;

	@Getter
	@Setter
	Integer planFromYear;

	@Getter
	@Setter
	Integer planToMonth;

	@Getter
	@Setter
	Integer planToYear;

	@Getter
	@Setter
	String productCategoryNames;

	@Getter
	@Setter
	String regionNames;

	@Getter
	@Setter
	Set<String> planFromMonthYearList;

	@Getter
	@Setter
	Set<String> planToMonthYearList;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

	@Getter
	@Setter
	RetailProductionPlanRequest retailProductionPlanRequest;

	@Getter
	@Setter
	Map<Integer, String> monthMap = new LinkedHashMap<Integer, String>();

	@Getter
	@Setter
	List<EntityMaster> regionList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EntityMaster> selectedRegions = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	List<RegionWiseSalesData> regionWiseCategoryDatalist = new ArrayList<RegionWiseSalesData>();

	@Getter
	@Setter
	List<ProductCategory> selectedproductCategories = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	LazyDataModel<RetailProductionPlanResponse> retailProductionPlanResponseLazyList;

	@Getter
	@Setter
	List<RegionWiseSalesData> regionWiseSalesList;

	@Getter
	@Setter
	RegionWiseSalesData selectedregionWiseSales;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private String currentPlanStatus = "INITIATED";

	@Getter
	@Setter
	String comments;

	@Getter
	@Setter
	String dialogHeader;

	@Autowired
	RetailProductionPlanLogBean retailProductionPlanLogBean;

	@Getter
	@Setter
	List<RetailProductionPlanResponse> retailProductionPlanPdfList;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	List statusAndStageValues;

	@Setter
	@Getter
	StreamedContent file;

	@Getter
	@Setter
	Map filterMap;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	boolean addButtonFlag = false;
	
	@Getter
	@Setter
	boolean editButtonFlag = true;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	@Getter
	@Setter
	Double totalRetailSalesValue;

	@Getter
	@Setter
	Double totalBaseStockValue;

	@Getter
	@Setter
	Double totalEligibilityRP;

	@Getter
	@Setter
	Double totalClosingStockValue;

	@Getter
	@Setter
	Double netEligibilityRP;

	@Getter
	@Setter
	Double netEligibilityPP;

	@Getter
	@Setter
	String retailClosingStockMonthConfig;

	@Getter
	@Setter
	String retailProdPlanPeriodConfig;

	@Getter
	@Setter
	String previousSalesPeriodConfig;
	
	@Getter
	@Setter
	Date dueDateLimit;

	@Getter @Setter
	List<RegionWiseSalesData> allRegionWiseCategoryDatalist = new ArrayList<RegionWiseSalesData>();
	
	@Getter @Setter
	List<String> categoryList = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		retailClosingStockMonthConfig = commonDataService.getAppKeyValue("RETAIL_CLOSING_STOCK_MONTH");
		retailProdPlanPeriodConfig = commonDataService.getAppKeyValue("RETAIL_PROD_PLAN_PERIOD");
		previousSalesPeriodConfig = commonDataService.getAppKeyValue("PREVIOUS_SALES_PERIOD");
		getHeadOfficePlanList();
	}

	public RetailProductionPlanBean() {
		log.info("<#======Inside RetailProduction Plan Bean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public void preparePlanFromMonthYear() {
		log.info("RetailProductionPlanBean.preparePlanFromMonthYear Method Started");

		if (retailProdPlanPeriodConfig.trim().equals("ANY")) {
			planFromMonthYearList = AppUtil.calculateMonthYear(new Date(), -1, 3);
		} else {
			Date date = new GregorianCalendar(AppUtil.getYear(new Date()), 3, 1).getTime();
			planFromMonthYearList = AppUtil.calculateMonthYear(date, 0, 1);
			for (String monthYear : planFromMonthYearList) {
				planFromMonthYear = monthYear;
			}

			calculateSalesDuration("planFrom");

		}
		log.info("RetailProductionPlanBean.preparePlanFromMonthYear Method Completed : " + planFromMonthYearList);
	}

	public void preparePlanToMonthYear() {
		log.info("RetailProductionPlanBean.preparePlanToMonthYear Method Started");
		if (retailProdPlanPeriodConfig.trim().equals("ANY")) {
			planToMonthYearList = AppUtil.calculateMonthYear(new Date(), 0, 12);
		} else {
			Date date = new GregorianCalendar(AppUtil.getYear(AppUtil.addOrMinusYear(new Date(), 1)), 2,
					AppUtil.getDay(new Date())).getTime();
			planToMonthYearList = AppUtil.calculateMonthYear(date, 0, 1);
			for (String monthYear : planToMonthYearList) {
				planToMonthYear = monthYear;
			}
			calculateSalesDuration("planTo");
		}
		log.info("RetailProductionPlanBean.preparePlanToMonthYear Method Completed : " + planToMonthYearList);
	}

	public void changePlanToMonthYear(int month, int year) {
		log.info("RetailProductionPlanBean.changePlanToMonthYear Method Started");

		planToMonthYearList = new LinkedHashSet<>();
		Date date = new GregorianCalendar(year, month - 1, 1).getTime();
		planToMonthYearList = AppUtil.calculateMonthYear(date, 0, 12);
		log.info("RetailProductionPlanBean.changePlanToMonthYear Method Completed : " + planToMonthYearList);
	}

	@SuppressWarnings("unchecked")
	private void loadStatusValues() {
		log.info("RetailProductionPlanBean.loadStatusValues Method Started");
		Object[] statusArray = RetailProductionPlanStatus.class.getEnumConstants();
		Object[] stageArray = RetailProductionPlanStage.class.getEnumConstants();
		statusAndStageValues = new ArrayList<>();

		for (Object stage : stageArray) {
			for (Object status : statusArray) {

				if (!(stage + "-" + status).equalsIgnoreCase("SHOWROOM-APPROVED")
						&& !(stage + "-" + status).equalsIgnoreCase("SHOWROOM-REJECTED")) {
						statusAndStageValues.add(stage + "-" + status);
				}
			}
		}
		log.info("RetailProductionPlanBean.loadStatusValues Method Completed :" + statusAndStageValues);
	}

	public RetailProductionPlanRequest getRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				log.info("planFromSearch : " + value);
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("planTo")) {
				log.info("planToSearch : " + value);
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("stageOrStatus")) {
				log.info("selectedStatus : " + value);
				request.setStageOrStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("RetailProductionPlanBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		retailProductionPlanResponse = new RetailProductionPlanResponse();

		RetailProductionPlanRequest request = getRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("RetailProductionPlanBean.getSearchData Method Complted");
		return baseDTO;
	}

	public void loadLazyDraftProductionList() {
		addButtonFlag = false;
		editButtonFlag = true;
		log.info("RetailProductionPlanBean loadLazyDraftProductionList method started");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {

					filterMap = filters;
					sortingOrder = sortOrder;
					sortingField = sortField;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetailProductionPlanResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse res) {
				return res != null ? res.getPlanId() : null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RetailProductionPlanResponse res : responseList) {
					if (res.getPlanId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public String generateRegionWisePlan() {
		log.info("RetailProductionPlanBean generateRegionWisePlan method started");
		dataToggle = "collapse in";
		planFromMonth = AppUtil.getMonthNumber(planFromMonthYear.split("-")[0]);
		planFromYear = Integer.valueOf(planFromMonthYear.split("-")[1]);

		planToMonth = AppUtil.getMonthNumber(planToMonthYear.split("-")[0]);
		planToYear = Integer.valueOf(planToMonthYear.split("-")[1]);

		if (planFromMonth != null && planToMonth != 0 && planFromYear != null && planToYear != 0) {

			Integer months = (planToYear - planFromYear) * 12 + planToMonth - planFromMonth;

			if (months == null || months > 12 || months < 0) {
				regionWiseSalesList = null;
				if (months < 0) {
					errorMap.notify(8522);
					return null;
				}
				errorMap.notify(8523);
				return null;
			}

			Date planFrom = new GregorianCalendar(planFromYear, planFromMonth - 1, 01).getTime();
			retailProductionPlanRequest.setPlanFrom(planFrom);

			Date firstDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
			Date planTo = AppUtil.getLastDateOfMonth(firstDay);
			retailProductionPlanRequest.setPlanTo(planTo);
		}

		retailProductionPlanRequest.setRegionIds(getRegionIds());
		retailProductionPlanRequest.setProductCategoryIds(getProductCategoryIds());
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/salesdata";

		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, retailProductionPlanRequest);
		} catch (Exception exp) {
			log.error("Error in generateRegionWisePlan", exp);
			errorMap.notify(1);
			return null;
		}

		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					RegionSalesData regionSalesData = mapper.readValue(jsonValue, RegionSalesData.class);

					if (regionSalesData != null) {
						regionWiseSalesList = regionSalesData.getListOfRegionWiseSalesDto();
						retailProductionPlanRequest.setStockClosedDate(regionSalesData.getStockClosedDate());
					}

					if (regionWiseSalesList != null && regionWiseSalesList.size() > 0) {
						dataToggle = "collapse";
					}

				} else {
					regionWiseSalesList = new ArrayList<>();
					String msg = baseDTO.getErrorDescription();
					Util.addError(msg);
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}

			} catch (IOException e) {
				log.error(e);
			}
		}
		calculateTotalValue(regionWiseSalesList);
		return null;
	}

	public void regionWiseCategoryViewList(RetailProductionPlanRegion retailProductionPlanRegion) {

		RegionWiseSalesData selectedregionWiseSalesData = new RegionWiseSalesData();
		selectedregionWiseSalesData.setRegionId(retailProductionPlanRegion.getEntityMaster().getId());
		regionWiseCategoryList(selectedregionWiseSalesData);
		calculateTotalValue(regionWiseCategoryDatalist);
		retailProductionPlanRequest.setCategoryFind(null);
		selectedregionWiseSalesData = new RegionWiseSalesData();

	}

	public void regionWiseCategoryList(RegionWiseSalesData selectedregionWiseSalesData) {

		regionWiseCategoryDatalist = new ArrayList<>();

		log.info("RetailProductionPlanBean regionWiseCategoryList method started");

		planFromMonth = AppUtil.getMonthNumber(planFromMonthYear.split("-")[0]);
		planFromYear = Integer.valueOf(planFromMonthYear.split("-")[1]);

		planToMonth = AppUtil.getMonthNumber(planToMonthYear.split("-")[0]);
		planToYear = Integer.valueOf(planToMonthYear.split("-")[1]);

		if (planFromMonth != null && planToMonth != 0 && planFromYear != null && planToYear != 0) {

			Date planFrom = new GregorianCalendar(planFromYear, planFromMonth - 1, 01).getTime();
			retailProductionPlanRequest.setPlanFrom(planFrom);

			Date firstDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
			Date planTo = AppUtil.getLastDateOfMonth(firstDay);
			retailProductionPlanRequest.setPlanTo(planTo);
		}

		retailProductionPlanRequest.setRegionId(selectedregionWiseSalesData.getRegionId());
		retailProductionPlanRequest.setCategoryFind("For Category List");
		if(selectedproductCategories !=null) {
			retailProductionPlanRequest.setProductCategoryIds(getProductCategoryIds());
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getcategory";
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(" ========>>  RetailProductionPlanBean URL PART method started   ========>>");
			baseDTO = httpService.post(url, retailProductionPlanRequest);

			ObjectMapper mapper = new ObjectMapper();

			if (baseDTO.getResponseContent() != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				RegionSalesData regionSalesData = mapper.readValue(jsonValue, RegionSalesData.class);

				if (regionSalesData != null) {
					regionWiseCategoryDatalist = regionSalesData.getListOfRegionWiseSalesDto();
					retailProductionPlanRequest.setStockClosedDate(regionSalesData.getStockClosedDate());
					if(regionWiseCategoryDatalist != null && !regionWiseCategoryDatalist.isEmpty()) {
						retailProductionPlanRequest.setRegionCodeOrName((regionWiseCategoryDatalist.get(0).getRegionCode() + " / "
							+ regionWiseCategoryDatalist.get(0).getRegionName()));
						dataToggle = "collapse";
					}else {
						log.error("regionWiseCategoryDatalist is Empty");
					}
				}

				/*if (regionWiseCategoryDatalist != null && !regionWiseCategoryDatalist.isEmpty()) {
					dataToggle = "collapse";
				}*/
			
			} else {
				regionWiseSalesList = new ArrayList<>();
				String msg = baseDTO.getErrorDescription();
				Util.addError(msg);
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
			}

		} catch (Exception exp) {
			log.error("Error in generateRegionWisePlan", exp);
			errorMap.notify(1);

		}

	}

	public boolean validateDraftPlan() {
		log.info("RetailProductionPlanBean validateDraftPlan method started");
		boolean flag = true;
		if (regionWiseSalesList == null || regionWiseSalesList.size() == 0) {
			errorMap.notify(8519);
			flag = false;
		}

		for (RegionWiseSalesData regionWiseSalesData : regionWiseSalesList) {
			if (regionWiseSalesData != null && regionWiseSalesData.getDueDate() != null
					&& regionWiseSalesData.getDueDate().before(new Date())) {
				log.info(errorMap.getMessage(8529) + regionWiseSalesData.getRegionName());
				Util.addWarn(errorMap.getMessage(8529) + regionWiseSalesData.getRegionName());
				flag = false;
			}

		}
		return flag;
	}

	public void convertData() {
		log.info("RetailProductionPlanBean convertData method started");
		List<Long> regionsIds = new ArrayList<>();
		if (selectedRegions != null && !selectedRegions.isEmpty()) {
			for (EntityMaster entityMaster : selectedRegions) {
				regionsIds.add(entityMaster.getId());
			}
		}
		retailProductionPlanRequest.setRegionIds(regionsIds);

		List<Long> productCategoryIds = new ArrayList<>();
		if (selectedproductCategories != null && !selectedproductCategories.isEmpty()) {
			for (ProductCategory productCategory : selectedproductCategories) {
				productCategoryIds.add(productCategory.getId());
			}
		}

		retailProductionPlanRequest.setProductCategoryIds(productCategoryIds);
		calculateSalesDuration(null);
		retailProductionPlanRequest.setRegionWiseSalesDataList(regionWiseSalesList);
	}

	public String saveDraftPlan() {
		log.info("RetailProductionPlanBean saveDraftPlan method started");
		if (!validateDraftPlan()) {
			return null;
		}

		convertData();
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/create";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, retailProductionPlanRequest);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Draft Plan saved successfully");
					errorMap.notify(8524);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in saveDraftPlan", exp);
			// Error : JSF1095: The response was already committed by the time
			// we tried to set the outgoing cookie for the flash.
			// Any values stored to the flash will not be available on the next
			// request.
			// Note : Plan is saved successfully ,so that we are redirecting to
			// list page with success message
			log.info("Plan is saved successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8524);
			return getHeadOfficePlanList();
		}
		return getHeadOfficePlanList();
	}

	public String submitDraftPlan() {
		log.info("RetailProductionPlanBean submitDraftPlan method started");
		if (!validateDraftPlan()) {
			return null;
		}

		convertData();
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/submit";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, retailProductionPlanRequest);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Draft Plan Submitted successfully");
					errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			// Error : JSF1095: The response was already committed by the time
			// we tried to set the outgoing cookie for the flash.
			// Any values stored to the flash will not be available on the next
			// request.
			// Note : Plan is submitted successfully,so that we are redirecting
			// to list page with success message
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return getHeadOfficePlanList();
		}

		return getHeadOfficePlanList();
	}

	public String editPlan() {

		preparePlanFromMonthYear();
		preparePlanToMonthYear();

		dataToggle = "collapse in";
		log.info("RetailProductionPlanBean editPlan method started");
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		if (!(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.DRAFT)
				&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.INITIATED))
				&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.DRAFT)
						&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.REJECTED))) {
			errorMap.notify(8534);
			return null;
		}

		RetailProductionPlan retailProductionPlan = null;

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/get/"
				+ retailProductionPlanResponse.getPlanId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
		} catch (Exception e) {
			log.error("Error in editPlan", e);
			errorMap.notify(1);
			return null;
		}
		if (baseDTO != null) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailProductionPlan = mapper.readValue(jsonValue, RetailProductionPlan.class);
			} catch (IOException e) {
				log.error(e);
			}
		}

		if (retailProductionPlan == null) {
			log.info("#==--> RetailProductionPlan is Not Available <--==#");
			errorMap.notify(9612);
			return null;
		} else {
			setDefaultRequestData();
			convertRetailProductionPlanToRequest(retailProductionPlan);
		}

		return HEAD_OFFICE_ADD_URL;

	}

	public String viewPlan() {
		log.info("RetailProductionPlanBean viewPlan method started");
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}
		log.info("<<<<< RetailProductionPlanBean-viewPlan()-retailProductionPlanResponse >>>>> "
				+ retailProductionPlanResponse);
		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		// String url = serverURL + appPreference.getOperationApiUrl()+/retail/plan/get/"
		// + retailProductionPlanResponse.getPlanId();

		// Above url changed because api return whole object of retail production plan shown heap issue
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getplanbyid/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = null;

		try {
			allRegionWiseCategoryDatalist = new ArrayList<>();
			categoryList = new ArrayList<>();
			
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailProductionPlan = mapper.readValue(jsonValue, RetailProductionPlan.class);
				retailProductionPlanRequest = new RetailProductionPlanRequest();
			}

			if (retailProductionPlan == null) {
				log.info("#==--> RetailProductionPlan is Not Available <--==#");
				errorMap.notify(9612);
				return null;
			} else {
				convertRetailProductionPlanToRequest(retailProductionPlan);
			}
			Collections.sort(retailProductionPlan.getRetailProductionPlanRegionList(),
					new Comparator<RetailProductionPlanRegion>() {
						@Override
						public int compare(RetailProductionPlanRegion u1, RetailProductionPlanRegion u2) {
							return u1.getEntityMaster().getCode().compareTo(u2.getEntityMaster().getCode());
						}
					});
		} catch (Exception e) {
			log.error("Error in viewPlan", e);
			errorMap.notify(1);
			return null;
		}

		return HEAD_OFFICE_VIEW_URL;

	}

	public void convertRetailProductionPlanToRequest(RetailProductionPlan retailProductionPlan) {
		log.info("RetailProductionPlanBean convertRetailProductionPlanToRequest method started");
		if (retailProductionPlan != null) {
			retailProductionPlanRequest = new RetailProductionPlanRequest();
			retailProductionPlanRequest.setId(retailProductionPlan.getId());
			retailProductionPlanRequest.setBaseStockPercentage(retailProductionPlan.getBaseStockPercentage());
			retailProductionPlanRequest.setPlanName(retailProductionPlan.getPlanName());
			retailProductionPlanRequest.setSalesFrom(retailProductionPlan.getSalesFrom());
			retailProductionPlanRequest.setSalesTo(retailProductionPlan.getSalesTo());
			retailProductionPlanRequest.setStockClosedDate(retailProductionPlan.getStockClosedDate());
			retailProductionPlanRequest.setVersion(retailProductionPlan.getVersion());

			planFromMonth = AppUtil.getMonth(retailProductionPlan.getPlanFrom());
			planToMonth = AppUtil.getMonth(retailProductionPlan.getPlanTo());
			planFromYear = AppUtil.getYear(retailProductionPlan.getPlanFrom());
			planToYear = AppUtil.getYear(retailProductionPlan.getPlanTo());

			planFromMonthYear = AppUtil.getMonthString(planFromMonth) + "-" + planFromYear.toString();
			planToMonthYear = AppUtil.getMonthString(planToMonth) + "-" + planToYear.toString();

			selectedRegions = new ArrayList<>();
			selectedproductCategories = new ArrayList<>();

			regionWiseSalesList = new ArrayList<>();

			for (RetailProductionPlanRegion retailProductionPlanRegion : retailProductionPlan
					.getRetailProductionPlanRegionList()) {
				selectedRegions.add(retailProductionPlanRegion.getEntityMaster());
				RegionWiseSalesData regionWiseSalesData = new RegionWiseSalesData();

				// To check in the server side (new or old)
				regionWiseSalesData.setPlanRegionId(retailProductionPlanRegion.getId());

				regionWiseSalesData.setRegionId(retailProductionPlanRegion.getEntityMaster().getId());
				regionWiseSalesData.setRegionCode(retailProductionPlanRegion.getEntityMaster().getCode().toString());
				regionWiseSalesData.setRegionName(retailProductionPlanRegion.getEntityMaster().getName().toString());
				regionWiseSalesData.setNetRetailSalesInRP(retailProductionPlanRegion.getSelectedGrossSalesValue());
				regionWiseSalesData.setBaseStockValue(retailProductionPlanRegion.getBaseStockPercentage());
				regionWiseSalesData.setTotalEligibilityInRP(retailProductionPlanRegion.getTotalEligibilityValue());
				regionWiseSalesData.setStockValueAsOnDate(retailProductionPlanRegion.getClosingStockValue());
				regionWiseSalesData.setNetEligibilityInRP(retailProductionPlanRegion.getNetEligibilityValue());
				regionWiseSalesData.setTotalPurchaseValue(retailProductionPlanRegion.getTotalEligibilityPp());
				regionWiseSalesData.setDueDate(retailProductionPlanRegion.getDueDate());
				regionWiseSalesList.add(regionWiseSalesData);
			}
			
			int regionWiseSalesListSize = regionWiseSalesList != null ? regionWiseSalesList.size() : 0;
			if(regionWiseSalesListSize > 0) {
				regionWiseSalesList.sort(Comparator.comparing(RegionWiseSalesData::getRegionCode));
			}

			selectedRegions.sort(Comparator.comparing(EntityMaster::getCode));
			
			selectedproductCategories = retailProductionPlan.getProductCategoryList();
			calculateTotalValue(regionWiseSalesList);
			changeRegions();
			changeProductCategory();
			if (retailProductionPlan.getCurrentLog() != null) {
				currentPlanStatus = retailProductionPlan.getCurrentLog().getStatus().name();
			}
		}
	}

	public void calculateSalesDuration(String durationType) {
		log.info("RetailProductionPlanBean calculateSalesDuration method started");
		String date = "";
		try {
		if (retailProductionPlanRequest == null) {
			retailProductionPlanRequest = new RetailProductionPlanRequest();
		}

		if (durationType != null && durationType.equals("planFrom")) {
			planToMonthYear = null;
			planToYear = null;
			planToMonth = null;
			retailProductionPlanRequest.setPlanTo(null);
		}

		retailProductionPlanRequest.setSalesFrom(null);
		retailProductionPlanRequest.setSalesTo(null);

		if (planFromMonthYear != null) {
			planFromMonth = AppUtil.getMonthNumber(planFromMonthYear.split("-")[0]);
			planFromYear = Integer.valueOf(planFromMonthYear.split("-")[1]);
			changePlanToMonthYear(planFromMonth, planFromYear);
			
			String dueMonthDuration=commonDataService.getAppKeyValue("INITIAL_PRODUCTION_PLAN_MONTH_DURATION");
			if(dueMonthDuration!=null && !dueMonthDuration.trim().equalsIgnoreCase("")) {
				dueDateLimit = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dueDateLimit);
				cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+Integer.valueOf(dueMonthDuration)));
				dueDateLimit = cal.getTime();
				log.info("Due Date Limit:::"+dueDateLimit);
			}
			else {
				log.info("Check INITIAL_PRODUCTION_PLAN_MONTH_DURATION Key value in AppConfig");
			}
			
		}

		if (planToMonthYear != null) {
			planToMonth = AppUtil.getMonthNumber(planToMonthYear.split("-")[0]);
			planToYear = Integer.valueOf(planToMonthYear.split("-")[1]);
			log.info("---planToMonth value------"+planToMonth);
			/*SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
			if(planToMonth.longValue() == 2L) {
				date = "28";
			}else {
				if(planToMonth % 2 == 0) {
					date = "30";
				}else {
					date = "31";
				}
			}
			if(date !="") {
				dueDateLimit =simpleDateFormat.parse(date+"-"+planToMonth+"-"+planToYear);
			}*/
		}
		if (planFromYear != null && planFromMonth != null) {
			Date planFrom = new GregorianCalendar(planFromYear, planFromMonth - 1, 1).getTime();
			retailProductionPlanRequest.setPlanFrom(planFrom);
			Date firstDay = null;
			if (previousSalesPeriodConfig.trim().equals("FEB-JAN")) {
				firstDay = new GregorianCalendar(AppUtil.getYear(new Date()), 1, 1).getTime();
			} else {
				firstDay = new GregorianCalendar(planFromYear, planFromMonth - 1, 1).getTime();
			}
			Date salesFrom = AppUtil.addOrMinusYear(firstDay, -1);
			retailProductionPlanRequest.setSalesFrom(new Date(salesFrom.getTime()));
			
			

		}
		if (planToYear != null && planToMonth != null) {
			Date firstDay = null;
			Date salesTo = null;
			Date planToDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
			Date planTo = AppUtil.getLastDateOfMonth(planToDay);
			retailProductionPlanRequest.setPlanTo(planTo);

			if (previousSalesPeriodConfig.trim().equals("FEB-JAN")) {
				firstDay = new GregorianCalendar(AppUtil.getYear(new Date()), 0, 1).getTime();
				salesTo = AppUtil.getLastDateOfMonth(firstDay);
			} else {
				firstDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
				salesTo = AppUtil.getLastDateOfMonth(firstDay);
				salesTo = AppUtil.addOrMinusYear(salesTo, -1);
			}
			retailProductionPlanRequest.setSalesTo(new Date(salesTo.getTime()));
		}
		}catch (Exception e) {
			log.error("calculateSalesDuration exception-----"+e);
		}
	}

	public List<Long> getRegionIds() {
		log.info("RetailProductionPlanBean getRegionIds method started");
		List<Long> regionIds = new ArrayList<>();
		for (EntityMaster region : selectedRegions) {
			regionIds.add(region.getId());
		}
		return regionIds;
	}

	public List<Long> getProductCategoryIds() {
		log.info("RetailProductionPlanBean getProductCategoryIds method started");
		List<Long> productCategoryIds = new ArrayList<>();
		for (ProductCategory productCategory : selectedproductCategories) {
			productCategoryIds.add(productCategory.getId());
		}
		return productCategoryIds;
	}

	public void changeRegions() {
		log.info("RetailProductionPlanBean changeRegions method started");
		regionNames = "";
		for (EntityMaster region : selectedRegions) {
			if (regionNames != "") {
				regionNames = regionNames + ",<br/>";
			}
			regionNames = regionNames + region.getCode() + "/" + region.getName();
		}
		log.info("RetailProductionPlanBean changeRegions method completed : " + regionNames);
	}

	public void changeProductCategory() {
		log.info("RetailProductionPlanBean changeProductCategory method started");
		productCategoryNames = "";

		if (selectedproductCategories != null) {
			for (ProductCategory productCategory : selectedproductCategories) {
				if (productCategoryNames != "") {
					productCategoryNames = productCategoryNames + ",<br/>";
				}
				productCategoryNames = productCategoryNames + productCategory.getProductCatCode() + "/"
						+ productCategory.getProductCatName();
			}
		}
		log.info("RetailProductionPlanBean changeProductCategory method completed : " + productCategoryNames);

	}

	/*
	 * Load Retail Production Plan Form
	 */
	public String getHeadOfficePlanForm() {
		dataToggle = "collapse in";
		log.info("RetailProductionPlanBean getHeadOfficePlanForm method started");
		if (!checkPlanConfig()) {
			Util.addWarn("Retail Production Plan Configuration is not valid");
			return null;
		}
		setDefaultRequestData();
		preparePlanFromMonthYear();
		preparePlanToMonthYear();
		currentPlanStatus = "INITIATED";
		try {
			retailProductionPlanLogBean.setRetailProductionPlanResponse(null);
			retailProductionPlanLogBean.setRetailProductionPlanResponse(null);
		} catch (Exception e) {
			log.error("Exception at getHeadOfficePlanForm() ", e);
		}
		return HEAD_OFFICE_ADD_URL;
	}

	public boolean checkPlanConfig() {

		retailClosingStockMonthConfig = commonDataService.getAppKeyValue("RETAIL_CLOSING_STOCK_MONTH");
		retailProdPlanPeriodConfig = commonDataService.getAppKeyValue("RETAIL_PROD_PLAN_PERIOD");
		previousSalesPeriodConfig = commonDataService.getAppKeyValue("PREVIOUS_SALES_PERIOD");
		if (retailClosingStockMonthConfig.trim().equals("ACTUAL") && retailProdPlanPeriodConfig.trim().equals("ANY")
				&& previousSalesPeriodConfig.trim().equals("ACTUAL")

				|| retailClosingStockMonthConfig.trim().equals("FEB")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("FEB-JAN")

				|| retailClosingStockMonthConfig.trim().equals("ACTUAL")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("FEB-JAN")

				|| retailClosingStockMonthConfig.trim().equals("ACTUAL")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("ACTUAL")

		) {
			log.info("Retail Production Plan Configuration is valid");
			return true;
		} else {
			log.info("Retail Production Plan Configuration is not valid");
			return false;
		}
	}

	public void setDefaultRequestData() {
		log.info("RetailProductionPlanBean setDefaultRequestData method started");
		//regionList = commonDataService.loadRegionWiseEntityMaster();
		// productCategoryList = commonDataService.loadProductCategories();
		regionList = commonDataService.loadActiveRegionalOffices();
		productCategoryList = commonDataService.loadAllActiveProductCategories();
		List<ProductCategory> productCategories = new ArrayList<>();
		for(ProductCategory productCategory : productCategoryList) {
			if(productCategory.getProductCatCode().equalsIgnoreCase("A")
					|| productCategory.getProductCatCode().equalsIgnoreCase("AJ")
					|| productCategory.getProductCatCode().equalsIgnoreCase("B")
					|| productCategory.getProductCatCode().equalsIgnoreCase("C")
					|| productCategory.getProductCatCode().equalsIgnoreCase("D")) {
				productCategories.add(productCategory);
			}
		}
		productCategoryList.clear();
		productCategoryList.addAll(productCategories);
		log.info("<===== productCategoryList size in retailProductionPlanBean:setDefaultRequestData() ====>"
				+ productCategoryList.size());
		retailProductionPlanRequest = new RetailProductionPlanRequest();
		selectedproductCategories = new ArrayList<>();
		selectedRegions = new ArrayList<>();
		regionWiseSalesList = new ArrayList<>();
		planFromMonth = null;
		planToMonth = null;
		planFromYear = null;
		planToYear = null;
		planFromMonthYear = null;
		planToMonthYear = null;
		regionNames = null;
		productCategoryNames = null;

	}

	public String getHeadOfficePlanList() {
		log.info("RetailProductionPlanBean getHeadOfficePlanList method started");
		retailProductionPlanResponse = new RetailProductionPlanResponse();
		loadStatusValues();
		loadLazyDraftProductionList();
		return HEAD_OFFICE_LIST_URL;

	}

	public String getHeadOfficePlanFinalList() {
		log.info("RetailProductionPlanBean getHeadOfficePlanFinalList method started");
		retailProductionPlanResponse = new RetailProductionPlanResponse();
		loadStatusValues();
		loadLazyDraftProductionList();
		setDefaultRequestData();
		regionNames = null;
		productCategoryNames = null;
		return HEAD_OFFICE_LIST_FINAL_URL;

	}

	public String statusUpdate() {
		log.info("Status Updated is called");
		log.info(currentPlanStatus);
		log.info(retailProductionPlan.getId());
		log.info(comments);

		BaseDTO baseDTO = null;
		RetailProductionPlanRequest request = new RetailProductionPlanRequest(retailProductionPlan.getId(),
				currentPlanStatus, comments);

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/approve";
		      baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			
			if(comments == null || comments.equalsIgnoreCase("")) {
				errorMap.notify(MastersErrorCode.REJECTED_COMMENT_MANDATORY);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgComments1').show();");
				return null;
			}
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/reject";
		     baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return HEAD_OFFICE_LIST_URL;
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);

		if (value.equals("APPROVED")) {
			dialogHeader = "Approve Comments";
		} else {
			dialogHeader = "Reject Comments";
		}
		currentPlanStatus = value;
		comments = null;
	}

	public String delete() {
		log.info("RetailProductionPlanBean delete method started");
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}
		if (retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.DRAFT)
				&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.INITIATED)) {
			String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/delete/"
					+ retailProductionPlanResponse.getPlanId();
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.delete(url);
			} catch (Exception exp) {
				log.error("Error in delete draft plan", exp);
				errorMap.notify(1);
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(8535);
				return HEAD_OFFICE_LIST_URL;
			}
		} else {
			AppUtil.addError(errorMap.getMessage(8536) + retailProductionPlanResponse.getStage().name() + "-"
					+ retailProductionPlanResponse.getStatus().name());
			return null;
		}

		return null;

	}

	public void generatePDF() {
		log.info("Generate PDF started....");
		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/downloadpdf";
		try {
			RetailProductionPlanRequest request = getRequestObject(null, null, sortingField, sortingOrder, filterMap);
			log.info("Generate PDF [" + request + "]");
			InputStream inputStream = httpService.generatePDF(URL, request);
			if (inputStream == null) {
				log.info("Template : RETAIL_PRODUCTION_PLAN_DRAFT_PDF_FORMAT not found ");
				Util.addError(errorMap.getMessage(8544));
				return;
			}
			file = new DefaultStreamedContent(inputStream, "application/pdf", "RetailSalesProductionPlanList.pdf");
			log.info("Generate PDF finished successfully....");
		} catch (ParseException e) {
			log.error("ParseException ", e);
		}
	}

	public void processDelete() {
		log.info("RetailProductionPlanBean processDelete method started");
		if (retailProductionPlanResponse == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(8525);
			return;
		} else {
			if (!retailProductionPlanResponse.getStatus().toString().equals("INITIATED")) {
				log.info("Plan cannot be Deleted");
				Util.addWarn("Plan cannot be Deleted");
				return;
			}
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		retailProductionPlanResponse = ((RetailProductionPlanResponse) event.getObject());
		addButtonFlag = true;
		editButtonFlag = true;
		String stage = retailProductionPlanResponse.getStage().name();
		String status = retailProductionPlanResponse.getStatus().name();
		log.info("stage------------->"+stage);
		log.info("status------------->"+status);
				
		if (stage.equalsIgnoreCase(RetailProductionPlanStage.DRAFT.toString())
				&& status.equalsIgnoreCase(RetailProductionPlanStatus.INITIATED.toString())
				|| stage.equalsIgnoreCase(RetailProductionPlanStage.DRAFT.toString())
				&& status.equalsIgnoreCase(RetailProductionPlanStatus.REJECTED.toString())) 
		{
			log.info("inside if --------" + editButtonFlag);
			editButtonFlag = false;
		}
	}

	public void onPageLoad() {
		log.info("RetailProductionPlanBean onPageLoad method started");
		addButtonFlag = false;
	}

	public String authentication() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("Authentication called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("INITIAL_RETAIL_PRODUCTION_PLAN_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}

	public void calculateTotalValue(List<RegionWiseSalesData> regionWiseList) {
		totalRetailSalesValue = 0.0;
		totalBaseStockValue = 0.0;
		totalEligibilityRP = 0.0;
		totalClosingStockValue = 0.0;
		netEligibilityRP = 0.0;
		netEligibilityPP = 0.0;
		log.info("RetailProductionPlanBean.calculateTotalValue Method Started ");
		for (RegionWiseSalesData region : regionWiseList) {
			if (region.getNetRetailSalesInRP() != null) {
				totalRetailSalesValue = totalRetailSalesValue + region.getNetRetailSalesInRP();
			}
			if (region.getBaseStockValue() != null) {
				totalBaseStockValue = totalBaseStockValue + region.getBaseStockValue();
			}
			if (region.getTotalEligibilityInRP() != null) {
				totalEligibilityRP = totalEligibilityRP + region.getTotalEligibilityInRP();
			}
			if (region.getStockValueAsOnDate() != null) {
				totalClosingStockValue = totalClosingStockValue + region.getStockValueAsOnDate();
			}
			if (region.getNetEligibilityInRP() != null) {
				netEligibilityRP = netEligibilityRP + region.getNetEligibilityInRP();
			}
			if (region.getTotalPurchaseValue() != null) {
				netEligibilityPP = netEligibilityPP + region.getTotalPurchaseValue();
			}

		}

	}
	
	public void getAllRegionWiseCategoryViewList() {
		try {
			int allRegionWiseCategoryDatalistSize = allRegionWiseCategoryDatalist == null ? 0
					: allRegionWiseCategoryDatalist.size();
			if (allRegionWiseCategoryDatalistSize == 0) {
				int retailProductionRegionListSize = retailProductionPlan == null ? null
						: retailProductionPlan.getRetailProductionPlanRegionList() == null ? null
								: retailProductionPlan.getRetailProductionPlanRegionList().size();
				if (retailProductionRegionListSize > 0) {
					for (RetailProductionPlanRegion retailProductionPlanRegionObj : retailProductionPlan
							.getRetailProductionPlanRegionList()) {
						RegionWiseSalesData regionWiseSalesData = new RegionWiseSalesData();
						regionWiseSalesData.setRegionCode(retailProductionPlanRegionObj.getEntityMaster() == null ? ""
								: String.valueOf(retailProductionPlanRegionObj.getEntityMaster().getCode()));
						regionWiseSalesData.setRegionName(retailProductionPlanRegionObj.getEntityMaster() == null ? ""
								: String.valueOf(retailProductionPlanRegionObj.getEntityMaster().getName()));
						regionWiseSalesData.setRegionId(retailProductionPlanRegionObj.getEntityMaster() == null ? null
								: retailProductionPlanRegionObj.getEntityMaster().getId());
						regionWiseCategoryList(regionWiseSalesData);
						for (RegionWiseSalesData data : regionWiseCategoryDatalist) {
							if (!categoryList.contains(data.getCategoryCode())) {
								categoryList.add(data.getCategoryCode());
							}
						}
						allRegionWiseCategoryDatalist.addAll(regionWiseCategoryDatalist);
					}
					log.info("categoryList1 size" + categoryList);
				}
			}
		} catch (Exception e) {
			log.error("Exception at RetailProductionPlanBean. getAllRegionWiseCategoryViewList() ", e);
		}
	}

	public Double getEligibityPPValue(RetailProductionPlanRegion region, String categoryCode) {
		log.info("inside getEligibityPPValue()");
		Double eligibilityPPValue = 0.0;
		try {
			Integer entityCode = region.getEntityMaster() == null ? null : region.getEntityMaster().getCode();
			int allRegionWiseCategoryDatalistSize = allRegionWiseCategoryDatalist == null ? 0
					: allRegionWiseCategoryDatalist.size();
			if(allRegionWiseCategoryDatalistSize > 0) {
				eligibilityPPValue = allRegionWiseCategoryDatalist.stream()
						.filter(a -> a.getCategoryCode() != null && entityCode != null
								&& a.getCategoryCode().equals(categoryCode)
								&& a.getRegionCode().equals(String.valueOf(entityCode)))
						.mapToDouble(RegionWiseSalesData::getTotalPurchaseValue).sum();

			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return eligibilityPPValue;
	}
	
	public Double getCategoryTotalEligibityPPValue(String categoryCode) {
		Double totalEligibilityPPValue = 0.0;
		try {
			int allRegionWiseCategoryDatalistSize = allRegionWiseCategoryDatalist == null ? 0
					: allRegionWiseCategoryDatalist.size();
			if(allRegionWiseCategoryDatalistSize > 0) {
				totalEligibilityPPValue = allRegionWiseCategoryDatalist.stream()
						.filter(a -> a.getCategoryCode() != null && a.getCategoryCode().equals(categoryCode))
						.mapToDouble(RegionWiseSalesData::getTotalPurchaseValue).sum();
			}
		}catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return totalEligibilityPPValue;
	}
	
	public void clear() {
		retailProductionPlanRequest=new RetailProductionPlanRequest();
		selectedproductCategories = new ArrayList<ProductCategory>();
		selectedRegions = new ArrayList<EntityMaster>();
		regionNames="";
		productCategoryNames="";
		planFromMonthYear="";
		planToMonthYear="";
	}
}