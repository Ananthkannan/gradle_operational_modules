package in.gov.cooptex.operation.rest.ui;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("uomMasterBean")
@Scope("session")
@Log4j2
public class UomMasterBean {

	final String LIST_PAGE = "/pages/masters/listUomMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createUomMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String UOM_MASTER_URL = AppUtil.getPortalServerURL() + "/uommaster";

	final String VIEW_PAGE = "/pages/masters/viewUomMaster.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	UomMaster uomMaster, selectedUomMaster;

	@Getter
	@Setter
	LazyDataModel<UomMaster> lazyUomMasterList;

	List<UomMaster> uomMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	public String showUomMasterListPage() {
		log.info("<==== Starts UomMasterBean.showUomMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		uomMaster = new UomMaster();
		selectedUomMaster = new UomMaster();
		loadLazyUomMasterList();
		log.info("<==== Ends UomMasterBean.showUomMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyUomMasterList() {
		log.info("<===== Starts UomMasterBean.loadLazyUomMasterList ======>");

		lazyUomMasterList = new LazyDataModel<UomMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<UomMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = UOM_MASTER_URL + "/getalluommasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						uomMasterList = mapper.readValue(jsonResponse, new TypeReference<List<UomMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return uomMasterList;
			}

			@Override
			public Object getRowKey(UomMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public UomMaster getRowData(String rowKey) {
				for (UomMaster uomMaster : uomMasterList) {
					if (uomMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedUomMaster = uomMaster;
						return uomMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends UomMasterBean.loadLazyUomMasterList ======>");
	}

	public String showUomMasterListPageAction() {
		log.info("<===== Starts UomMasterBean.showUomMasterListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedUomMaster == null || selectedUomMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_UOM_MASTER.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create uomMaster called..");
				uomMaster = new UomMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit uomMaster called..");
				String url = UOM_MASTER_URL + "/getbyid/" + selectedUomMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					uomMaster = mapper.readValue(jsonResponse, new TypeReference<UomMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View uomMaster called..");

				String url = UOM_MASTER_URL + "/getbyid/" + selectedUomMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						uomMaster = mapper.readValue(jsonResponse, new TypeReference<UomMaster>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete uomMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUomMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showUomMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends UomMasterBean.showUomMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts UomMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(UOM_MASTER_URL + "/deletebyid/" + selectedUomMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.UOM_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUomMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete uomMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends UomMasterBean.deleteConfirm ===========>");
		return showUomMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts UomMasterBean.saveOrUpdate =======>");
		try {
			if (uomMaster.getCode() == null || uomMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.UOM_MASTER_CODE_EMPTY.getCode());
				return null;
			}
			if (uomMaster.getName() == null || uomMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.UOM_MASTER_NAME_EMPTY.getCode());
				return null;
			}
			if (uomMaster.getName().trim().length() < 3 || uomMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.UOM_MASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (uomMaster.getLocalName() == null || uomMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.UOM_MASTER_LNAME_EMPTY.getCode());
				return null;
			}
			if (uomMaster.getLocalName().trim().length() < 3 || uomMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.UOM_MASTER_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (uomMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.UOM_MASTER_STATUS_EMPTY.getCode());
				return null;
			}
			String url = UOM_MASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, uomMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("uomMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.UOM_MASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.UOM_MASTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends UomMasterBean.saveOrUpdate =======>");
		return showUomMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts UomMasterBean.onRowSelect ========>" + event);
		selectedUomMaster = ((UomMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends UomMasterBean.onRowSelect ========>");
	}

}
