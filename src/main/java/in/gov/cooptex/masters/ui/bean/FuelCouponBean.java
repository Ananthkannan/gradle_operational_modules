package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.vehicle.model.FuelCoupon;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.VehicleRegister;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("fuelCouponBean")
public class FuelCouponBean {

	private final String CREATE_FUELCOUPON_MASTER_PAGE = "/pages/masters/fuelcoupon/createFuelCoupon.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/fuelcoupon/listFuelCoupon.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/fuelcoupon/viewFuelCoupon.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	FuelCoupon fuelCoupon;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<FuelCoupon> fuelCouponList = new ArrayList<FuelCoupon>();

	@Getter
	@Setter
	private int fuelCouponListSize;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<FuelCoupon> lazyFuelCouponList;

	boolean forceLogin = false;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	FuelCoupon selectedFuelCoupon;
	@Getter
	@Setter
	List<VehicleRegister> vehicleRegisterList;

	public String showFuelCouponListPage() {
		log.info("<==== Starts FuelCouponBean.showFuelCouponListPage =====>");
		fuelCoupon = new FuelCoupon();
		mapper = new ObjectMapper();
		selectedFuelCoupon = new FuelCoupon();
		loadLazyFuelCouponList();
		log.info("<==== Ends FuelCouponBean.showFuelCouponListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	public String submit() {
		log.info("the submit method is called");
		if (fuelCoupon.getCouponBookNumber() == null) {
			log.info("error fuelcoupon book number  is empty");
			errorMap.notify(ErrorDescription.FUELCOUPON_BOOKNUMBER_NULL.getErrorCode());
			return null;
		}
		if (fuelCoupon.getCouponStartNumber() == null) {
			log.info("error fuelcoupon start number  is empty");
			errorMap.notify(ErrorDescription.FUELCOUPON__STARTNUMBER_NULL.getErrorCode());
			return null;
		}
		if (fuelCoupon.getCouponEndNumber() == null) {
			log.info("error fuelcoupon End number  is empty");
			errorMap.notify(ErrorDescription.FUELCOUPON__ENDNUMBER_NULL.getErrorCode());
			return null;
		} else {
			if (fuelCoupon.getCouponEndNumber() != null
					&& fuelCoupon.getCouponEndNumber() <= fuelCoupon.getCouponStartNumber()) {
				log.info("error fuelcoupon End number  is empty");
				errorMap.notify(ErrorDescription.FUELCOUPON__ENDNUMBER_VALIDATION.getErrorCode());
				return null;
			}
		}
		if (fuelCoupon.getValidFrom() == null) {
			log.info("error fuelcoupon valid from   is empty");
			errorMap.notify(ErrorDescription.FUELCOUPON_VALIDFROM_NULL.getErrorCode());
			return null;
		}
		if (fuelCoupon.getValidTo() == null) {
			log.info("error fuelcoupon valid to   is empty");
			errorMap.notify(ErrorDescription.FUELCOUPON__VALIDTO_NULL.getErrorCode());
			return null;
		}

		try {
			log.info("...... FuelCoupon submit begin ....");
			BaseDTO baseDTO = null;
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/fuelCoupon/createOrUpdate";
			baseDTO = httpService.post(url, fuelCoupon);
			log.info(" fuelCoupon id" + fuelCoupon.getId());
			log.info("return from fuelCoupon  controler" + baseDTO.toString());

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("salesType saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.FUELCOUPON_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.FUELCOUPON_UPDATE_SUCCESS.getCode());
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Error while creating CountryMaster" + e);
		}
		log.info("...... fuelCoupon submit ended ....");
		clear();
		return INPUT_FORM_LIST_URL;
	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		fuelCoupon = new FuelCoupon();
		disableAddButton = false;
		showFuelCouponListPage();
		clear();
		return INPUT_FORM_LIST_URL;

	}

	public String addFuelCoupon() {
		log.info("<---- Start FuelCoupon.addFuelCoupon ---->");
		try {
			action = "Create";
			fuelCoupon = new FuelCoupon();
			fuelCoupon.setActiveStatus(true);
			loadVehicleRegister();
			log.info("<---- End FuelCoupon.addFuelCoupon ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_FUELCOUPON_MASTER_PAGE;

	}

	public String deletetFuelCoupon() {
		try {
			action = "Delete";
			if (selectedFuelCoupon == null) {
				Util.addWarn("Please Select one Fuel Coupon");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFuelCouponDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;

	}

	public String clear() {
		log.info("<---- Start FuelCoupon . clear ----->");
		showFuelCouponListPage();
		selectedFuelCoupon = new FuelCoupon();
		log.info("<---- End FuelCoupon . clear ----->");
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;

	}
	
	public String deletetFuelCouponConfirm() {
		log.info("<===== Starts FuelCouponBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete( AppUtil.getPortalServerURL()+appPreference.getOperationApiUrl() + "/fuelCoupon/delete/"+selectedFuelCoupon.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FUELCOUPON_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFuelCouponDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete FuelCoupon ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends FuelCouponBean.deleteConfirm ===========>");
		return  showFuelCouponListPage();
	}
	

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String backPage() {
		showFuelCouponListPage();
		clear();
		return INPUT_FORM_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FuelCoupon.onRowSelect ========>" + event);
		selectedFuelCoupon = ((FuelCoupon) event.getObject());
		log.info("selectedFuelCoupon id" + selectedFuelCoupon.getId());
		disableAddButton = true;
		log.info("<===Ends FuelCoupon.onRowSelect ========>");
	}

	public void loadLazyFuelCouponList() {
		log.info("<===== Starts FuelCoupon Bean.loadLazyFuelCouponList======>");
		lazyFuelCouponList = new LazyDataModel<FuelCoupon>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<FuelCoupon> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
							+ "/fuelCoupon/getFuelCouponListByLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					
					if (response != null && response.getStatusCode() == 0) {
						 mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						fuelCouponList = mapper.readValue(jsonResponse, new TypeReference<List<FuelCoupon>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting load method" + fuelCouponList.toString());
				log.info("Ends lazy load....");

				return fuelCouponList;
			}

			@Override
			public Object getRowKey(FuelCoupon res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FuelCoupon getRowData(String rowKey) {
				try {
					for (FuelCoupon fuelCoupon : fuelCouponList) {
						if (fuelCoupon.getId().equals(Long.valueOf(rowKey))) {
							selectedFuelCoupon = fuelCoupon;
							return fuelCoupon;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends FuelCoupon Bean.loadLazyFuelCoupon List ======>");
	}

	public List<VehicleRegister> loadVehicleRegister() {
		log.info("<--- Inside loadVehicleRegister() --->");
		try {
			/*String requestPath = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/vehicleregister/getallvehicleregister";*/
			String requestPath = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
			+ "/vehicleregister/getallvehicleregisteridandname";
			log.info("requestPath==>" + requestPath);
			BaseDTO baseDTO = new BaseDTO();

			baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					vehicleRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
					});
					log.info("VehicleRegister with Size of : " + vehicleRegisterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("<--- Exception in loadVehicleRegister() --->", e);
		}
		return vehicleRegisterList;
	}

	public String viewFuelCoupon() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedFuelCoupon == null || selectedFuelCoupon.getId() == null) {
			Util.addWarn("Please Select one Fuel Coupon");
			return null;
		}
		log.info("ID-->" + selectedFuelCoupon.getId());

		selectedFuelCoupon = getFuelCouponDetails(selectedFuelCoupon.getId());

		log.info("selectedFuelCoupon Object:::" + selectedFuelCoupon);
		return INPUT_FORM_VIEW_URL;
	}

	public String editFuelCoupon() {
		log.info("<<<<< -------Start editFuelCoupon called ------->>>>>> ");

		try {
			if (action.equalsIgnoreCase("EDIT")) {
				if (selectedFuelCoupon == null || selectedFuelCoupon.getId() == null) {
					Util.addWarn("Please Select one Fuel Coupon");
					return null;
				} else {
					log.info("edit FuelCoupon called..");
					loadVehicleRegister();
					fuelCoupon = getFuelCouponDetails(selectedFuelCoupon.getId());
					return CREATE_FUELCOUPON_MASTER_PAGE;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in editFuelCoupon ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends FuelCoupon.editFuelCoupon ===========>" + action);
		return null;
	}

	private FuelCoupon getFuelCouponDetails(Long id) {
		log.info("<====== FuelCouponBean.getFuelCouponDetails starts====>" + id);
		try {
			if (selectedFuelCoupon == null || selectedFuelCoupon.getId() == null) {
				log.error(" No Fuel Coupon selected ");
				Util.addWarn("Please Select one Fuel Coupon");
			}
			log.info("Selected TapalFor  Id : : " + selectedFuelCoupon.getId());
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/fuelCoupon/get/"
					+ selectedFuelCoupon.getId();

			log.info("Fuel Coupon Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			FuelCoupon fuelCouponDetail = mapper.readValue(jsonResponse, FuelCoupon.class);

			if (fuelCouponDetail != null) {
				fuelCoupon = fuelCouponDetail;
			} else {
				log.error("FuelCoupon object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" FuelCoupon Retrived  SuccessFully ");
			} else {
				log.error(" FuelCoupon Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return fuelCoupon;
		} catch (Exception e) {
			log.error("Exception Ocured while get FuelCoupon Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

}
