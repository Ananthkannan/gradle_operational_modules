package in.gov.cooptex.operation.stock.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.TransportChargeType;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.TestingLabStockTransferDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.stock.dto.TestingLabStockInwardDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("testingLabStockInwardBean")
@Scope("session")
public class TestingLabStockInwardBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	StockTransfer stockTransfer;
	
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	StockTransfer stockTransferLazy;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	StockTransfer selectedStockTransfer;

	@Getter
	@Setter
	StockTransferItems selectedStockTransferItems;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDeleteList;

	@Getter
	@Setter
	boolean stockInwardTypeFlag;
	
	@Getter
	@Setter
	String editButtonChangeFlag = "create";

	@Getter
	@Setter
	String stockInwardType;

	@Getter
	@Setter
	TestingLabStockInwardDTO testingLabStockInwardDTO;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	Boolean addButtonFlag;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();

	@Getter
	@Setter
	ProductCategory selectedProductCategory = new ProductCategory();

	@Getter
	@Setter
	ProductGroupMaster selectedProductGroupMaster = new ProductGroupMaster();

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	Long uom;

	@Getter
	@Setter
	String hsnCode;
	
	@Getter
	@Setter
	UomMaster uomMaster = new UomMaster();
	
	

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportServiceMasterList;
	@Getter
	@Setter
	TransportMaster selectedTransportMaster;

	@Getter
	@Setter
	String wayBillAvailable;

	@Getter
	@Setter
	String transportChargeAvailble;

	@Getter
	@Setter
	String transportChargeType;

	// cash or toPay

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	SupplierMaster seletetedSupplierMaster;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	boolean entityFlag;

	@Getter
	@Setter
	boolean societyFlag;

	@Getter
	@Setter
	String entityType;

	@Getter
	@Setter
	String productType;

	@Getter
	@Setter
	private boolean renderedEdit,renderedView,renderedDelete = true;

	@Getter
	@Setter
	private boolean renderedCreate;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	LazyDataModel<StockTransfer> stockTransferLazyList;

	@Getter
	@Setter
	List<StockTransferStatus> stockTransferEnumStatus;

	@Getter
	@Setter
	boolean productSampleNumberRendered;
	
	@Getter
	@Setter
	String functionAction;
	
	Boolean dataAddFlag = false;
	
	@Getter
	@Setter
	TestingLabStockTransferDTO testingLabStockTransferDTO = new TestingLabStockTransferDTO();
	
	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	int productSampleNumberCount;
	
	@Getter
	@Setter
	CircleMaster selectedCircleMaster;
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_PAGE = "/pages/inspectionCenter/TestingLab/listTestingLabStockInward.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/inspectionCenter/TestingLab/viewTestingLabStockInward.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE = "/pages/inspectionCenter/TestingLab/createTestingLabStockInward.xhtml?faces-redirect=true;";
	private final String EDIT_PAGE = "/pages/inspectionCenter/TestingLab/editTestingLabStockInward.xhtml?faces-redirect=true;";

	@PostConstruct
	public void init() {
		stockInwardTypeFlag = true;
		entityFlag = true;
		societyFlag = true;
		renderedEdit = false;
		renderedView = true;
		renderedDelete = true;
		renderedCreate = true;
		productSampleNumberRendered = false;
		stockTransferEnumStatus = new ArrayList<StockTransferStatus>(Arrays.asList(StockTransferStatus.values()));
		loadLazyIntRequirementList();
	}

	public String saveTestingLabStockInward() {
		log.info("** receive  saveTestingLabStockInward ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/saveTestingLabStockTransferItems";
			log.info("::: saveTestingLabStockInward Controller calling:::");
			baseDTO = httpService.post(url, testingLabStockInwardDTO);
			log.info("::: get saveTestingLabStockInward Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("saveTestingLabStockInward Page Succes ");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_SUCCESS.getErrorCode());
				return LIST_PAGE;
			} else {
				log.warn("saveTestingLabStockInward Error *** :");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_CREATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in saveTestingLabStockInward load ", ee);
		}
		return null;
	}

	// Get list Of StockTransferItems for Selected Stock Inward Type
	public void loadStockTransferItemsListInward() {
		log.info("<--- receive loadStockTransferItemsListInward --->");
		BaseDTO baseDTO = null;
		stockTransferItemsList = new ArrayList<StockTransferItems>();
		StockTransferItems stockTransferItems = new StockTransferItems();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getStockTransferItemsListInward";
			log.info("::: loadStockTransferItemsListInward Controller calling:::");
			baseDTO = httpService.post(url, selectedStockTransfer);
			log.info("::: get loadStockTransferItemsListInward Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("loadStockTransferItemsListInward Page Succes ");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferItemsList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferItems>>() {
				});
				
			} else {
				log.warn("loadStockTransferItemsListInward Error *** :");
			}
		} catch (Exception ee) {
			log.error("loadStockTransferItemsListInward Page Error " + ee);
		}
	}

	public int sumofReceivedUnit(String type) {
		int count = 0;
		if (stockTransferItemsList != null && stockTransferItemsList.size() > 0)
			if (type.equalsIgnoreCase("receiveUnit")) {
				for (StockTransferItems items : stockTransferItemsList) {
					count += items.getOrderedQty();
				}
			} else if (type.equalsIgnoreCase("productNumber")) {
				for (StockTransferItems items : stockTransferItemsList) {
					count += items.getProductSampleNumber();
				}
			}
		return count;
	}

	// StockInwardType
	public String listenerOutwardType() {
		try {
		log.info("** receive listener stockOutwardDetails " + stockInwardType);
		reset();
		if (stockInwardType.equalsIgnoreCase("Stock Transfer")) {
			loadStockOutwardDetails();
			stockInwardTypeFlag = true;
		} else if (stockInwardType.equalsIgnoreCase("Manual Inward")) {
			stockInwardTypeFlag = false;
			stockTransferItemsList = new ArrayList<StockTransferItems>();
		} else {
			log.error("** Unknow stockOutwardDetails :" + stockInwardType);
		}
		}catch (Exception e) {
			log.error("listenerOutwardType exception-------------",e);
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Item List onRowSelect method started");
		log.info("Selected ID is :" + selectedStockTransfer.getId());
		if(selectedStockTransfer.getStatus().equals(StockTransferStatus.INITIATED)) {
			renderedEdit = true;
		}else {
			renderedEdit = false;
		}
		renderedView = true;
		renderedDelete = true;
		renderedCreate = false;
	}

	public void reset() {
		renderedEdit = false;
		renderedView = true;
		renderedDelete = true;
		renderedCreate = true;
		entityType = null;
		selectedStockTransfer = null;
		selectedProductCategory = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productType = null;
		uom = null;
		stockTransferItems.setOrderedQty(null);
		entityFlag = false;
		societyFlag = false;
		selectedTransportTypeMaster = null;
		selectedTransportMaster = null;
		transportServiceMasterList = new ArrayList<TransportMaster>();
		wayBillAvailable = "Select Waybill Type";
		stockTransfer.setWaybillAvailable(false);
		transportChargeAvailble = null;
		transportChargeType = null;
		stockTransfer.setTransportChargeAmount(null);
		stockTransferItemsList = new ArrayList<StockTransferItems>();
	}

	@Autowired
	LoginBean loginBean;
	
	// add Item ManualInward
	public void itemCollectiorAddNewInward() {
		log.info("itemCollectiorAddNew Inward received :");
		int entityCode = 0;
		Long sampleNumber = null;
		try {
			
			/*selectedProductCategoryGroup = new ProductCategoryGroup();
			selectedProductCategory = new ProductCategory();
			selectedProductGroupMaster = new ProductGroupMaster();
			selectedProductVarietyMaster = new ProductVarietyMaster();*/
			
						
			if(selectedProductVarietyMaster == null) {
				
				if(selectedProductCategoryGroup == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_CATEGORY_GROUP).getCode());
					return;
				}
				if(selectedProductCategory == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_CATEGORY).getCode());
					return;
				}
				if(selectedProductGroupMaster == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_GROUP).getCode());
					return;
				}
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_VARIETY).getCode());
				return;
			}
			if(uomMaster == null || uomMaster.getId() == null) {
				errorMap.notify(ErrorDescription.UOM_MASTER_NOT_FOUND.getErrorCode());
				return;
			}
			
			if(stockTransferItemsList != null || !stockTransferItemsList.isEmpty()) {
				for(StockTransferItems stockTransferItem : stockTransferItemsList) {
					if(stockTransferItem.getProductVarietyMaster().getId().equals(selectedProductVarietyMaster.getId())) {
					/*if(stockTransferItem.getProductVarietyMaster().getName().equals(selectedProductVarietyMaster.getName())) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_ALREDY_EXISTS).getCode());
						return;
					}
					if(stockTransferItem.getUomMaster().getName().equals(uomMaster.getName())) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.UOM_ALREDY_EXISTS).getCode());
						return;
					}*/
					if(stockTransferItem.getProductType().equals(stockTransferItems.getProductType())) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TYPE_ALREDY_EXISTS).getCode());
						return;
					}
					/*if(stockTransferItem.getOrderedQty() == stockTransferItems.getOrderedQty()) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.QUANTITY_ALREDY_EXISTS).getCode());
						return;
					}*/
					}
				}
			}
			if (stockTransferItems.getProductType() == null
					|| stockTransferItems.getProductType().equalsIgnoreCase("Select")) {
				log.info("Invalid Item Product Type quantity:");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_TYPE).getCode());
				
			}
			if(stockTransferItems.getOrderedQty() <= 0) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.QUANTITY_ZERO_NOT_ALLOWED).getCode());
				return; 
		 }
			 else if (stockTransferItems.getOrderedQty() == null || stockTransferItems.getOrderedQty() <= 0) {
					log.info("Invalid Item Datails quantity:");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.QUANTITY_SHOULD_NOT_EMPTY).getCode());
				}
				else {
				EmployeeMaster employeeMaster = loginBean.getEmployee();
				if(employeeMaster != null) {
					EntityMaster entityMaster = employeeMaster.getPersonalInfoEmployment().getWorkLocation();
					entityCode = entityMaster.getCode();
				}
				stockTransferItems.setProductVarietyMaster(selectedProductVarietyMaster);
				stockTransferItems.setUomMaster(uomMaster);

				stockTransferItems.setCreatedDate(new Date());
				if (productSampleNumberRendered == false) {
					sampleNumber = (long) getProductSampleNumber();
					stockTransferItems.setProductSampleNumber(Long.valueOf(entityCode+""+sampleNumber));
				}else {
					productSampleNumberCount += 1;
					sampleNumber = (long)productSampleNumberCount;
					stockTransferItems.setProductSampleNumber(Long.valueOf(entityCode+""+sampleNumber));
				}
				stockTransferItemsList.add(stockTransferItems);

				//errorMap.notify(ErrorDescription.TESTING_LAB_STOCK_INWARD_SAVE_SUCCESS.getErrorCode());
				log.info("Item Collection add Success :");
				
				
			}
			selectedProductCategoryGroup = new ProductCategoryGroup();
			selectedProductCategory = new ProductCategory();
			selectedProductGroupMaster = new ProductGroupMaster();
			stockTransferItems = new StockTransferItems();
			productVarietyMasterList = new ArrayList<>();
			uomMaster = new UomMaster();
		} catch (Exception e) {
			log.error("StockItemInward Collection Add Error :" , e);
			errorMap.notify(ErrorDescription.TESTING_LAB_STOCK_INWARD_SAVE_FAILED.getErrorCode());
		}

	}
	
	
	public void editItemCollectiorAddNewInward() {
		log.info(" :: Upadate - editItemCollectiorAddNewInward :: ");
		try {
			editButtonChangeFlag = "create";
			
			
			
			if (stockTransferItemsList != null && !stockTransferItemsList.isEmpty()) {
				for (StockTransferItems item : stockTransferItemsList) {
					log.info("row index==> "+item.getRowIndex());
					log.info("row stock transfer id==> "+stockTransferItems.getRowIndex());
					if (item.getRowIndex() != null
							&& item.getRowIndex() == (stockTransferItems.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						stockTransferItemsList.set(item.getRowIndex(), stockTransferItems);
						dataAddFlag = true;
					}else {
						dataAddFlag = false;
					}
				}
			
			} 
			if(dataAddFlag == false) {
				log.info(" :: Updated - Employee Disciplinary Action :: ");
				//-- to add new record
				itemCollectiorAddNewInward();
				//stockTransferItemsList.add(stockTransferItems);s
			}else {
				productVarietyMasterList = new ArrayList<>();
				uomMaster = new UomMaster();
				stockTransferItems = new StockTransferItems();
			}
			
			
		} catch (Exception e) {
			log.error("Exception editItemCollectiorAddNewInward ==> ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	

	public int getProductSampleNumber() {

		log.info("** receive getProductSampleNumber ");
		BaseDTO baseDTO = null;
		productSampleNumberRendered = true;
		productSampleNumberCount = 1;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getProductSampleNumber";
			baseDTO = httpService.get(url);
			log.info("::: get getProductSampleNumber Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("getProductSampleNumber Succes ");
				log.info("getProductSampleNumber Count:" + baseDTO.getTotalRecords());
				productSampleNumberCount = (baseDTO.getTotalRecords() + 1);
				return productSampleNumberCount;
			} else {
				log.warn("getProductSampleNumber Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in getProductSampleNumber load ", ee);
		}
		return 1;
	}

	public void listenerEntityMaster() {
		log.info("** receive listener listenerEntityMaster " + entityType);
		if (entityType.equalsIgnoreCase("Entity")) {
			entityFlag = true;
			societyFlag = false;
			loadEntityTypeMaster();
		} else if (entityType.equalsIgnoreCase("Society")) {
			//loadSupplierMasterList();
			circleMasterList = new ArrayList<>();
			circleMasterList = commonDataService.loadCircles();
			entityFlag = false;
			societyFlag = true;
		} else {
			log.error("** Unknow listenerEntityMaster :" + entityType);
		}
	}

	// #Load Stock Outward Number / Date
	public void loadStockOutwardDetails() {
		log.info("** receive loadStockOutwardDetails ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/testingLabStockInward/getStockOutward";
			log.info("::: loadStockOutwardDetails Controller calling:::");
			baseDTO = httpService.get(url);
			log.info("::: get loadStockOutwardDetails Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				log.info("loadStockOutwardDetails Page Succes ");

				stockTransferList = new ArrayList<StockTransfer>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
				});

			} else {
				log.warn("loadStockOutwardDetails Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadStockOutwardDetails load ", ee);
		}
	}

	public String listenerCreatedDate(Date date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			return (sdf.format(date));
		} catch (Exception e) {
			log.error("Date Convert error ");
			// TODO: handle exception
		}
		return null;
	}

	// product category Group ->1
	public void productCategoryGroupList() {
		log.info("Received productCategoryGroupList ");
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryGroupList";
			baseDTO = httpService.get(url);
			log.info("productCategoryGroupList :: url==> "+url);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
				if(productCategoryGroupList != null && !productCategoryGroupList.isEmpty()) {
					log.info("productCategoryGroupList size==> "+productCategoryGroupList.size());
				}
			} else {
				log.error("productCategoryGroupList has empty or null");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryGroupList load ", ee);
		}
	}

	// load Product Category Code / Name
	public void productCategoryListLoad() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		hsnCode = "";
		uom = null;
		try {
			if(selectedProductCategoryGroup == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_CATEGORY_GROUP).getCode());
				return;
			}
			log.info("Product category group==> "+selectedProductCategoryGroup.getId());
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getAllProductCategoryCodeName";
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllProductCategoryList/"
					+ selectedProductCategoryGroup.getId();
			log.info("productCategoryListLoad :: url==> "+url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
				
				if(productCategoryList != null && !productCategoryList.isEmpty()) {
					log.info("productCategoryList size==> "+productCategoryList.size());
				}
			} else {
				log.warn("productCategoryList has empty or null");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryListLoad ==> ", ee);
		}
	}

	// product category -> 3
	public void getAllProductGroupMasterList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		
		log.info("productCategory ID :" + selectedProductCategory.getId());
		try {
			if(selectedProductCategory == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_CATEGORY).getCode());
				return;
			}
			log.info("getProductCategoryGroup ID :" + selectedProductCategory.getProductCategoryGroup().getId());
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/"
					+ selectedProductCategory.getId();
			log.info("url==> "+url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
				
				if(productGroupMasterList != null && !productGroupMasterList.isEmpty()) {
					log.info("productGroupMasterList size==> "+productGroupMasterList.size());
				}
			} else {
				log.warn("productGroupMasterList empty or null");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in getAllProductGroupMasterList method==>  ", ee);
		}
	}

	// product category -> 4
	public void listenerProductVarietyMasterList() {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		
		try {
			if(selectedProductGroupMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_GROUP).getCode());
				return;
			}
			log.info("selectedProductGroupMaster ID :" + selectedProductGroupMaster.getId());
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getProductVarietyMasterList/" + selectedProductGroupMaster.getId();
			log.info("url==> "+url);
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				if(productVarietyMasterList != null && !productVarietyMasterList.isEmpty()) {
					log.info("productVarietyMasterList size==> "+productVarietyMasterList.size());
				}
			} else {
				log.warn("productVarietyMasterList Empty or null ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in listenerProductVarietyMasterList ==> ", ee);
		}
	}

	public void productVarietyUpdate() {
		try {
			log.info("***** Received productVarietyUpdate ******* ");
			if(selectedProductVarietyMaster != null && selectedProductVarietyMaster.getHsnCode() != null) {
				log.info("HSN code==> "+selectedProductVarietyMaster.getHsnCode());
				hsnCode = selectedProductVarietyMaster.getHsnCode();
			}else {
				log.error("HSN Code has error or empty");
			}
			if(selectedProductVarietyMaster != null && selectedProductVarietyMaster.getId() != null) {
				log.info("Product Id==> "+selectedProductVarietyMaster.getId());
				loadUOMByProductId(selectedProductVarietyMaster.getId());
			}else {
				log.error("product id has error or empty");
			}
			//uom = selectedProductVarietyMaster.getUomMaster().getId();
			log.info("Produect Variety Update Get Success :");
		} catch (Exception se) {
			log.error("Set productVarietyUpdate Get Error " + se);
		}
	}
	
	public void loadUOMByProductId(Long productId) {
		log.info("***** loadUOMByProductId start ******* ");
		BaseDTO baseDTO = null;
		try {
			if(productId == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_PRODUCT_VARIETY).getCode());
				return;
			}
			String url = AppUtil.getPortalServerURL() + "/uommaster/loaduombyproductid/"+productId;
			log.info("loadUOMByProductId url==> "+url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				uomMaster = mapper.readValue(jsonResponse, new TypeReference<UomMaster>() {
				});
				if(uomMaster != null) {
					log.info("uom name==> "+uomMaster.getName());
				}else {
					uomMaster = new UomMaster();
				}
				
			}else {
				uomMaster = new UomMaster();
			}
		}catch (Exception se) {
			log.error("Exception loadUOMByProductId :: Error " + se);
		}
	}

	public void test() {
		stockTransfer.getWaybillAvailable();
		stockTransfer.getWaybillNumber();
		stockTransfer.getTransportChargeAvailable();
		for (PurchaseOrder order : selectedStockTransfer.getPurchaseOrderList()) {
			log.info("Purhcase Order List is " + order.getId());
		}
	}

	public String gotoAddPage() {
		try {
			stockInwardTypeFlag = true;
			entityFlag = true;
			societyFlag = true;
			renderedEdit = false;
			renderedView = true;
			renderedDelete = true;
			renderedCreate = true;
			productSampleNumberRendered = false;
			log.info("Received Create ItemInward ");
			stockTransferItems = new StockTransferItems();
			stockTransfer = new StockTransfer();
			testingLabStockInwardDTO = new TestingLabStockInwardDTO();
			stockTransferItems = new StockTransferItems();
			selectedCircleMaster = new CircleMaster();
			supplierMasterList = new ArrayList<SupplierMaster>();
			seletetedSupplierMaster = new SupplierMaster();
			clearStockInward();
			//reset();

			loadTransportTypeMasterList();
			productCategoryGroupList();
			// FacesContext.getCurrentInstance().getExternalContext().redirect(CREATE_PAGE);
			return CREATE_PAGE;
		} catch (Exception e) {
			log.error("Redirect to add page Error :" + e);
		}
		return null;
	}

	public String gotoListPage() {
		try {
			log.info("Received Lsit TestingLab StockInward");
			renderedEdit = false;
			renderedCreate = true;
			renderedView = true;
			renderedDelete = true;
			stockTransferItems = new StockTransferItems();
			stockTransfer = new StockTransfer();
			selectedStockTransfer = new StockTransfer();
			testingLabStockInwardDTO = new TestingLabStockInwardDTO();
			stockTransferItems = new StockTransferItems();

			return LIST_PAGE;
			// FacesContext.getCurrentInstance().getExternalContext().redirect(LIST_PAGE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// loadTransportType List
	public void loadTransportTypeMasterList() {

		log.info(":::<- Load TransportTypeMasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportTypeMasterList";
			log.info("loadTransportTypeMasterList :: url==> "+url);
			baseDTO = httpService.get(url);
			log.info("::: get TransportTypeMasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportTypeMasterList = new ArrayList<TransportTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<TransportTypeMaster>>() {
						});
				if(transportTypeMasterList != null && !transportTypeMasterList.isEmpty()) {
					log.info("transportTypeMasterList size==> "+transportTypeMasterList.size());
				}
			} else {
				log.warn("transportTypeMasterList null or empty ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadTransportTypeMasterList==> ", ee);
		}
	}

	public void transportTypeMasterListener() {
		log.info("Received Listener of selectedTransportTypeMaster **************");
		try {
			transportServiceMasterList = new ArrayList<TransportMaster>();
			loadTransportServiceList(selectedTransportTypeMaster.getId());
		} catch (Exception se) {
			log.error("SET selectedTransportTypeMaster Get Error " + se);
		}
		log.info("Done selectedTransportTypeMaster **************");
	}
	// Transport Service Name

	public void loadTransportServiceList(Long id) {

		log.info(":::<- Load TransportService MasterList TypeStart ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getTransportServiceMasterList";
			log.info("::: TransportService MasterList Controller calling  1 :::");
			TransportMaster mas = new TransportMaster();
			mas.setId(id);
			baseDTO = httpService.post(url, mas);
			log.info("::: get TransportService MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				transportServiceMasterList = new ArrayList<TransportMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				transportServiceMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TransportMaster>>() {
				});
				log.info("TransportService MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("TransportService MasterList Page Convert Succes -->");
			} else {
				log.warn("TransportService MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in TransportService MasterList load ", ee);
		}
	}

	// Entity Type Master
	public void loadEntityTypeMaster() {

		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getActiveEntityTypeMasterList";
			baseDTO = httpService.get(url);
			log.info("::: get loadEntityTypeMaster MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				entityTypeMasterList = new ArrayList<EntityTypeMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});
				log.info("loadEntityTypeMaster MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("loadEntityTypeMaster MasterList Page Convert Succes -->");
			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	// Entity Master
	public void loadEntityMasterList() {

		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;
		try {
			if(selectedEntityTypeMaster ==  null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_TYPE_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getSelectedEntityList";
			baseDTO = httpService.post(url, selectedEntityTypeMaster);
			log.info("::: get loadEntityTypeMaster MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				entityMasterList = new ArrayList<EntityMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("loadEntityTypeMaster MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("loadEntityTypeMaster MasterList Page Convert Succes -->");
			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	// Supplier Master List
	public void loadSupplierMasterList() {

		log.info(":::<- loadSupplierMasterList MasterList ->::: ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getSupplierMasterList";
			baseDTO = httpService.get(url);
			log.info("::: get loadSupplierMasterList MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				supplierMasterList = new ArrayList<SupplierMaster>();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("loadSupplierMasterList MasterList load Successfully " + baseDTO.getTotalRecords());
				log.info("loadSupplierMasterList MasterList Page Convert Succes -->");
			} else {
				log.warn("loadSupplierMasterList MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadSupplierMasterList MasterList load ", ee);
		}
	}

	// view StockTransfer
	public String viewSelectedStockTransfer() {
		if (selectedStockTransfer == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		log.info("viewSelectedStockTransfer :: start==>");
		getStockTransferItemsByID();
		return VIEW_PAGE;
	}

	// edit stockTransfer
	/*public String editSelectedStockTransfer() {
		log.info("viewSelectedStockTransfer :: start==>");
		getStockTransferItemsByID();
		return EDIT_PAGE;
	}*/
	public String editSelectedStockTransfer() {
		try {
			
			log.info("Edit TestingLabStock Transfer Receieved");
			productCategoryGroupList();
			stockTransferItems = new StockTransferItems();
			stockTransfer = new StockTransfer();
			testingLabStockInwardDTO = new TestingLabStockInwardDTO();
			stockTransferItems = new StockTransferItems();

			selectedProductCategoryGroup = new ProductCategoryGroup();
			selectedProductCategory = new ProductCategory();
			selectedProductGroupMaster = new ProductGroupMaster();
			selectedProductVarietyMaster = new ProductVarietyMaster();
			stockTransferItemsDeleteList = new ArrayList<StockTransferItems>();

			log.info("Edit Stock Transfer done type master");
			getStockTransferItemsByID();
			
			/*if(stockTransfer.getSupplierMaster() != null && stockTransfer.getSupplierMaster().getId() != null) {
				societyFlag = true;
				entityFlag = false;
			}else {
				societyFlag = false;
				entityFlag = true;
			}*/
			
			if(testingLabStockTransferDTO != null && testingLabStockTransferDTO.getSupplierId() != null) {
				societyFlag = true;
				entityFlag = false;
			}else {
				societyFlag = false;
				entityFlag = true;
			}

			log.info("Edit Stock Transfer done STocktransfer   ****");
			
			if (testingLabStockTransferDTO != null && testingLabStockTransferDTO.getWayBillAvailable()) {
				wayBillAvailable = "Yes";
			}else {
				wayBillAvailable = "No";
			}
			
			log.info("Edit Stock Transfer Fetch level 1.1");
			if (testingLabStockTransferDTO.getTransferType() == StockTransferType.TESTING_INWARD_STOCKTRANSFER) {
				stockInwardType = "Stock Transfer";
				loadStockOutwardDetails();
				/*log.info("stockTransfer==> "+testingLabStockTransferDTO.gets);
				selectedStockTransfer = stockTransfer;*/
				
				stockInwardTypeFlag = true;
			} else if (testingLabStockTransferDTO.getTransferType() == StockTransferType.TESTING_INWARD_MANUAL) {
				stockInwardType = "Manual Inward";
				stockInwardTypeFlag = false;
			}

			/*selectedEntityTypeMaster = stockTransfer.getToEntityMaster().getEntityTypeMaster();
			selectedEntityMaster = stockTransfer.getToEntityMaster();
			log.info("Entity Type Master ->" + selectedEntityTypeMaster.getEntityName());
			log.info("Entity Master ->" + selectedEntityTypeMaster.getEntityName());*/
			
			
			
			EntityMaster fromEntityMaster = new EntityMaster();
		    fromEntityMaster.setId(testingLabStockTransferDTO.getFromEntityId());
			fromEntityMaster.setName(testingLabStockTransferDTO.getEntityName());
			selectedEntityMaster = fromEntityMaster;
			
			if(testingLabStockTransferDTO != null && testingLabStockTransferDTO.getSupplierName() != null) {
				entityType = "Society";
				SupplierMaster supplierMaster = new SupplierMaster();
				supplierMaster.setId(testingLabStockTransferDTO.getSupplierId());
				supplierMaster.setName(testingLabStockTransferDTO.getSupplierName()); 
				seletetedSupplierMaster = supplierMaster;
			}else {
				entityType = "Entity";
				EntityTypeMaster fromEntityTypeMaster = new EntityTypeMaster();
				fromEntityTypeMaster.setId(testingLabStockTransferDTO.getEntityTypeId());
				fromEntityTypeMaster.setEntityName(testingLabStockTransferDTO.getEntityTypeName());
				selectedEntityTypeMaster = fromEntityTypeMaster;
				if(selectedEntityTypeMaster.getId() != 0) {
					loadEntityMasterList();
				}
			}
			listenerEntityMaster();

			int rowIndex = 0;
			if (stockTransferItemsList.size() > 0) {

				for(StockTransferItems sItem : stockTransferItemsList) {
					log.info("rowIndex==>"+rowIndex);
					sItem.setRowIndex(rowIndex);
					rowIndex = rowIndex+1;
				}
				
			}
			

			log.info("Edit Stock Transfer Fetch Success");
			return EDIT_PAGE;
			
		} catch (Exception e) {
			log.error("editSelectedStockTransfer :: exception==> ",e);
		}
		return null;
	}

	// View Get Selected StockTransfer and StockTransferItems
	public void getStockTransferItemsByID() {

		if (selectedStockTransfer == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return;
		}
		log.info(":::<- getStockTransferItemsByID MasterList ->::: " + selectedStockTransfer.getId());
		BaseDTO baseDTO = null;
		stockTransfer = new StockTransfer();
		stockTransferItemsList = new ArrayList<StockTransferItems>();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getStockTransferByID";
			baseDTO = httpService.post(url, selectedStockTransfer);
			log.info("::: get getStockTransferItemsByID MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				TestingLabStockInwardDTO testingLabStockInwardDTO = new TestingLabStockInwardDTO();
				;

				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				testingLabStockInwardDTO = mapper.readValue(jsonResponse,
						new TypeReference<TestingLabStockInwardDTO>() {
						});

				/*jsonResponse = mapper.writeValueAsString(testingLabStockInwardDTO.getStockTransfer());
				stockTransfer = mapper.readValue(jsonResponse, new TypeReference<StockTransfer>() {
				});*/
				
				jsonResponse = mapper.writeValueAsString(testingLabStockInwardDTO.getTestingLabStockTransferDTO());
				testingLabStockTransferDTO = mapper.readValue(jsonResponse, new TypeReference<TestingLabStockTransferDTO>() {
				});

				jsonResponse = mapper.writeValueAsString(testingLabStockInwardDTO.getStockTransferItemsList());
				stockTransferItemsList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferItems>>() {
				});

				log.info("getStockTransferItemsByID Successfully ");

			} else {
				log.warn("getStockTransferItemsByID Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in getStockTransferItemsByID :", ee);
		}
	}

	public String goToPage(String pageName) {

		log.info("Goto Page [" + pageName + "]");

		if (pageName.equalsIgnoreCase("LIST")) {
			return showListPage();
		} else if (pageName.equalsIgnoreCase("ADD")) {
			return showAddPage();
		} else if (pageName.equalsIgnoreCase("EDIT")) {
			return showEditPage();
		} else if (pageName.equalsIgnoreCase("VIEW")) {
			return showViewPage();
		} else {
			return null;
		}
	}

	// Save StockTransfer
	
	public String saveStockTransfer() {
		log.info("<=saveStockTransfer :: start=>");
		try {
			if(stockInwardType == null || stockInwardType.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_INWARD_TYPE_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			if (stockInwardType.equals("Stock Transfer")) {
				if(selectedStockTransfer ==  null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_OUTWARD_NUMBER_SHOULD_NOT_EMPTY).getCode());
					return null;
				}
				
			}else if (stockInwardType.equals("Manual Inward")) {
				if(entityType == null || entityType.isEmpty()) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_TRANSFER_TYPE_SHOULD_NOT_EMPTY).getCode());
					return null;
				}
				if (entityType.equals("Entity")) {
					if(selectedEntityTypeMaster ==  null) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_TYPE_SHOULD_NOT_EMPTY).getCode());
						return null;
					}
					if(selectedEntityMaster ==  null) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_SHOULD_NOT_EMPTY).getCode());
						return null;
					}
					
				}else if (entityType.equals("Society")) {
					if(seletetedSupplierMaster == null) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_SHOULD_NOT_EMPTY).getCode());
						return null;
					}
				}
			}
			if(stockTransferItemsList == null || stockTransferItemsList.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
				return null;
			}
			
			if(functionAction.equals("SAVE")) {
				stockTransfer.setStatus(StockTransferStatus.INITIATED);
			}else {
				stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
			}

			log.info("<--- Save Start StockTransfer --->");

			if (stockInwardType.equals("Stock Transfer")) {
				stockTransfer.setFromEntityMaster(selectedStockTransfer.getToEntityMaster());
				stockTransfer.setTransferType(StockTransferType.TESTING_INWARD_STOCKTRANSFER);
				log.info("<--- Stock Transfer Type Save Processed --->");
			} else if (stockInwardType.equals("Manual Inward")) {
				log.info("<--- Manual Inward Save Processed --->");
				stockTransfer.setTransferType(StockTransferType.TESTING_INWARD_MANUAL);
				if (stockTransferItemsList == null || stockTransferItemsList.size() == 0) {
					errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TABLE_NULL.getErrorCode());
					return null;
				}
				if (entityType.equals("Entity")) {
					log.info("<--- Society Save Processed --->");
					stockTransfer.setFromEntityMaster(selectedEntityMaster);
				} else if (entityType.equals("Society")) {

					stockTransfer.setSupplierMaster(seletetedSupplierMaster);
					log.info("<--- Society Save Processed --->");
				}
			}

			try {
				RequestContext.getCurrentInstance().execute("PF('statusDialog').show()");
			} catch (Exception see) {
				log.error("Error to open dialog");
			}
			if (transportChargeType != null) {
				if (transportChargeType.equals("Cash")) {
					this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
				} else {
					this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
				}
			}

			/*selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);
			this.stockTransfer.setTransportMaster(selectedTransportMaster); // Type of Transport serviceType
*/
			testingLabStockInwardDTO = new TestingLabStockInwardDTO();
			this.stockTransfer.setDateReceived(new Date());
			

			testingLabStockInwardDTO.setStockTransfer(stockTransfer);
			testingLabStockInwardDTO.setStockTransferItemsList(stockTransferItemsList);

			BaseDTO baseDTO = null;
			
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/testingLabStockInward/saveTestingLabStockTransferItems";
				baseDTO = httpService.post(url, testingLabStockInwardDTO);
				log.info("::: get testingLabStockInward Response :");
				if (baseDTO.getStatusCode() == 0) {
					log.info("save testingLabStockInward Page Succes -->");
					errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_SUCCESS.getErrorCode());
					clearStockInward();
					loadLazyIntRequirementList();
					return LIST_PAGE;
				} else {
					log.warn("save testingLabStockInward Error *** :");
					errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_ERROR.getErrorCode());
				}
			
			
			
			
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in saveStockTransfer ==> ", ee);
		}
		return null;
	}
	
	public void clearStockInward() {
		stockInwardType = null;
		selectedStockTransfer = new StockTransfer();
		entityType = null;
		selectedEntityTypeMaster = new EntityTypeMaster();
		selectedEntityMaster = new EntityMaster();
		seletetedSupplierMaster = new SupplierMaster();
		selectedProductCategoryGroup = new ProductCategoryGroup();
		selectedProductCategory = new ProductCategory();
		selectedProductGroupMaster = new ProductGroupMaster();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		uomMaster = new UomMaster();
		stockTransferItems = new StockTransferItems();
		stockTransfer = new StockTransfer();
		stockTransferItemsList = new ArrayList<>();
		
	}
	
	public void clearEditStockItem() {
		selectedProductCategoryGroup = new ProductCategoryGroup();
		selectedProductCategory = new ProductCategory();
		selectedProductGroupMaster = new ProductGroupMaster();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		stockTransferItems = new StockTransferItems();
		uomMaster = new UomMaster();
	}
	
	
	public String saveStockTransfer_Old() {
		log.info("<--- Save StockTransfer Received   ---> " + wayBillAvailable + ":");

		if (entityType == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TYPE_NOT_FOUNT.getErrorCode());
			return null;
		}
		if (selectedTransportMaster == null) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_SERVICE_NOT_VALID.getErrorCode());
			return null;
		}
		if (this.wayBillAvailable == null || this.wayBillAvailable.equalsIgnoreCase("Select Waybill Type")) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			return null;
		}
		if (stockTransfer.getWaybillAvailable() == true && (stockTransfer.getWaybillNumber() == null
				|| stockTransfer.getWaybillNumber().trim().length() <= 0)) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_WAYBILL_INVALID.getErrorCode());
			return null;
		}
		if (transportChargeAvailble.equalsIgnoreCase("Yes")
				&& (transportChargeType == null || transportChargeType.equalsIgnoreCase("Select Type"))) {
			errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode());
			return null;
		}

		if (transportChargeAvailble.equalsIgnoreCase("Yes")) {
			if (stockTransfer.getTransportChargeAmount() == null || this.stockTransfer.getTransportChargeAmount() < 0) {
				errorMap.notify(
						ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TRANSPORT_CHARGE_AVAILABLE.getErrorCode());
				return null;
			}
		}
		/*
		 * if(transportChargeType.equalsIgnoreCase("Yes") &&
		 * (this.stockTransfer.getTransportChargeAmount() == 0)) {
		 * errorMap.notify(ErrorDescription.
		 * STOCK_ITEM_INWARD_PNS_SUBMIT_TRANS_CHARGE_NOT_VALID.getErrorCode()); return
		 * null; }
		 */
		if (stockTransferItemsList.size() == 0) {
			errorMap.notify(ErrorDescription.TESTING_INWARD_ITEMS_NULL.getErrorCode());
			return null;
		}

		log.info("<--- Save Start StockTransfer --->");

		if (stockInwardType.equals("Stock Transfer")) {
			stockTransfer.setFromEntityMaster(selectedStockTransfer.getToEntityMaster());
			stockTransfer.setTransferType(StockTransferType.TESTING_INWARD_STOCKTRANSFER);
			log.info("<--- Stock Transfer Type Save Processed --->");
		} else if (stockInwardType.equals("Manual Inward")) {
			log.info("<--- Manual Inward Save Processed --->");
			stockTransfer.setTransferType(StockTransferType.TESTING_INWARD_MANUAL);
			if (stockTransferItemsList == null || stockTransferItemsList.size() == 0) {
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_SUBMIT_TABLE_NULL.getErrorCode());
				return null;
			}
			if (entityType.equals("Entity")) {
				log.info("<--- Society Save Processed --->");
				stockTransfer.setFromEntityMaster(selectedEntityMaster);
			} else if (entityType.equals("Society")) {

				stockTransfer.setSupplierMaster(seletetedSupplierMaster);
				log.info("<--- Society Save Processed --->");
			}
		}

		try {
			RequestContext.getCurrentInstance().execute("PF('statusDialog').show()");
		} catch (Exception see) {
			log.error("Error to open dialog");
		}
		if (transportChargeType != null) {
			if (transportChargeType.equals("Cash")) {
				this.stockTransfer.setTransportChargeType(TransportChargeType.Pay);
			} else {
				this.stockTransfer.setTransportChargeType(TransportChargeType.ToPay);
			}
		}

		selectedTransportMaster.setTransportTypeMaster(selectedTransportTypeMaster);
		this.stockTransfer.setTransportMaster(selectedTransportMaster); // Type of Transport serviceType

		testingLabStockInwardDTO = new TestingLabStockInwardDTO();
		this.stockTransfer.setDateReceived(new Date());
		this.stockTransfer.setStatus(StockTransferStatus.SUBMITTED);

		testingLabStockInwardDTO.setStockTransfer(stockTransfer);
		testingLabStockInwardDTO.setStockTransferItemsList(stockTransferItemsList);

		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/saveTestingLabStockTransferItems";
			baseDTO = httpService.post(url, testingLabStockInwardDTO);
			log.info("::: get testingLabStockInward Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("save testingLabStockInward Page Succes -->");
				errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_SUCCESS.getErrorCode());
				loadLazyIntRequirementList();
				return LIST_PAGE;
			} else {
				log.warn("save testingLabStockInward Error *** :");
				errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.TESTING_INWARD_CREATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in save testingLabStockInward ", ee);
		}
		// try {
		// FacesContext.getCurrentInstance().getExternalContext().redirect(LIST_PAGE);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		return LIST_PAGE;
	}

	public String updateStockTransfer() {
		
		
		log.info("<--- Update StockTransfer Received   --->");

		
		
		if(stockInwardType == null || stockInwardType.isEmpty()) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_INWARD_TYPE_SHOULD_NOT_EMPTY).getCode());
			return null;
		}
		if (stockInwardType.equals("Stock Transfer")) {
			if(selectedStockTransfer ==  null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_OUTWARD_NUMBER_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			
		}else if (stockInwardType.equals("Manual Inward")) {
			if(entityType == null || entityType.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_TRANSFER_TYPE_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			if (entityType.equals("Entity")) {
				if(selectedEntityTypeMaster ==  null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_TYPE_SHOULD_NOT_EMPTY).getCode());
					return null;
				}
				if(selectedEntityMaster ==  null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_SHOULD_NOT_EMPTY).getCode());
					return null;
				}
				
			}else if (entityType.equals("Society")) {
				if(seletetedSupplierMaster == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_SHOULD_NOT_EMPTY).getCode());
					return null;
				}
			}
		}
		if(stockTransferItemsList == null || stockTransferItemsList.isEmpty()) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
			return null;
		}

		log.info("stockTransfer id==> "+testingLabStockTransferDTO.getId());
		log.info("to entity id==> "+testingLabStockTransferDTO.getToEntityId());
		log.info("to entity name==> "+testingLabStockTransferDTO.getToEntityName());
		log.info("getWayBillAvailable==> "+testingLabStockTransferDTO.getWayBillAvailable());
		log.info("getRefNumer==> "+testingLabStockTransferDTO.getRefNumer());
		log.info("getTransferType==> "+testingLabStockTransferDTO.getTransferType());
		
		stockTransfer.setId(testingLabStockTransferDTO.getId());
		stockTransfer.setFromEntityMaster(selectedEntityMaster); 
		EntityMaster toEntityMaster = new  EntityMaster();
		toEntityMaster.setId(testingLabStockTransferDTO.getToEntityId());
		toEntityMaster.setName(testingLabStockTransferDTO.getToEntityName());
		stockTransfer.setToEntityMaster(toEntityMaster);
		stockTransfer.setSupplierMaster(seletetedSupplierMaster); 
		stockTransfer.setWaybillAvailable(testingLabStockTransferDTO.getWayBillAvailable()); 
		stockTransfer.setReferenceNumber(testingLabStockTransferDTO.getRefNumer());
		stockTransfer.setTransferType(testingLabStockTransferDTO.getTransferType()); 
		
		testingLabStockInwardDTO = new TestingLabStockInwardDTO();
		this.stockTransfer.setModifiedDate(new Date());

		testingLabStockInwardDTO.setStockTransfer(stockTransfer);
		testingLabStockInwardDTO.setStockTransferItemsList(stockTransferItemsList);
		testingLabStockInwardDTO.setStockTransferItemsDeleteList(stockTransferItemsDeleteList);
		testingLabStockInwardDTO.setStockInwardType(stockInwardType);
		log.info("Size for delete is " + stockTransferItemsDeleteList.size());
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/updateTestingLabStockTransferItems";
			baseDTO = httpService.post(url, testingLabStockInwardDTO);
			log.info("::: get testingLabStockInward Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("save testingLabStockInward Page Succes -->");
				errorMap.notify(ErrorDescription.TESTING_INWARD_UPDATE_SUCCESS.getErrorCode());
				loadLazyIntRequirementList();
				clearStockInward();
				return LIST_PAGE;
			} else {
				log.warn("save testingLabStockInward Error *** :");
				errorMap.notify(ErrorDescription.TESTING_INWARD_UPDATE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.TESTING_INWARD_UPDATE_EXCEPTION.getErrorCode());
			log.error("Exception Occured in updateStockTransfer ", ee);
		}
		return null;
	}

	public void reinit(StockTransferItems items) {

		log.info("Received Collector remove " + items.getId());
		try {
			stockTransferItemsDeleteList.add(items);
			log.info("Received Collector remove Add Success");
		} catch (Exception e) {
			log.error("StockItems remove Add Error " + e);
			
		}

	}

	public String showAddPage() {
		stockTransferItems = new StockTransferItems();
		stockTransfer = new StockTransfer();
		testingLabStockInwardDTO = new TestingLabStockInwardDTO();
		loadTransportTypeMasterList();
		return CREATE_PAGE;
	}

	public String showEditPage() {
		return CREATE_PAGE;
	}

	public String showViewPage() {

		return VIEW_PAGE;
	}

	public void renderedWaybillNumber() {
		log.info("rendered WaybillNumber received ");
		if (wayBillAvailable.equalsIgnoreCase("YES")) {
			stockTransfer.setWaybillAvailable(true);
		} else {
			stockTransfer.setWaybillAvailable(false);
		}
	}

	public String showListPage() {

		log.info("<==== Starts TestingLabStockInward=====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		addButtonFlag = true;

		selectedStockTransfer = new StockTransfer();

		log.info("<==== Ends TestingLabStockInward=====>");

		return LIST_PAGE;
	}

	public String clearButton() {
		log.info("Clear Action Called");
		renderedEdit = false;
		renderedCreate = true;
		renderedView = true;
		renderedDelete = true;
		selectedStockTransfer = new StockTransfer();
		return LIST_PAGE;
	}

	public void clearButtonRendered() {
		log.info("Clear Action Called");
		renderedEdit = false;
		renderedCreate = true;
		renderedView = true;
		renderedDelete = true;
		selectedStockTransfer = new StockTransfer();
	}

	public void renderedTransportChargeRendered() {
		log.info("rendered transportChargeRendered received ");
		if (transportChargeAvailble.equalsIgnoreCase("YES")) {
			stockTransfer.setTransportChargeAvailable(true);
		} else {
			stockTransfer.setTransportChargeAvailable(false);
		}
	}

	// lazy search
	public void loadLazyIntRequirementList() {
		log.info("<--- loadLazyStockTransfer StockTransfer List ---> ");
		stockTransferLazyList = new LazyDataModel<StockTransfer>() {
			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<StockTransfer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransfer> data = new ArrayList<StockTransfer>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyStockTransfer RequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(StockTransfer res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransfer getRowData(String rowKey) {
				List<StockTransfer> responseList = (List<StockTransfer>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (StockTransfer res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							stockTransferLazy = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		stockTransferLazy = new StockTransfer();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		StockTransfer yarnRequirement = new StockTransfer();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/testingLabStockInward/searchData";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
	
	public void callDeleteTransfer() {
		if (selectedStockTransfer == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return;
		} 
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmUserDelete').show();");
		
	}

	public String deleteStockTransfer() {
		log.info("Delete StockTransfer Received :: start");
		try {
			if (selectedStockTransfer == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			BaseDTO baseDTO = null;
			log.info("Stock transfer id==> " + selectedStockTransfer.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/deleteStockTransfer";
			baseDTO = httpService.post(url, selectedStockTransfer);
			log.info("::: Retrieved Delete :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Delete StockTransfer Successfully Completed ******");
				errorMap.notify(ErrorDescription.STOCK_ITEM_INWARD_PNS_DELETE_SUCCESS.getErrorCode());
				
			} else {
				errorMap.notify(ErrorDescription.CANNOT_DELETE_REFERENCED_RECORD.getErrorCode());
				log.warn("Delete StockTransfer Error *** :");
			}
			clearStockInward();
			gotoListPage();
		} catch (Exception ee) {
			log.error("Exception Occured in Delete StockTransfer", ee);
		}
		return null;
	}

	/**
	 * 
	 */
	public TestingLabStockInwardBean() {
		super();
	}
	
	public void clearVarietyDetails() {
		stockInwardType = null;
		selectedStockTransfer = new StockTransfer();
		entityType = null;
		selectedEntityTypeMaster = new EntityTypeMaster();
		selectedEntityMaster = new EntityMaster();
		selectedEntityMaster = new EntityMaster();
		seletetedSupplierMaster = new SupplierMaster();
		stockTransferItemsList = new ArrayList<StockTransferItems>();
		stockTransferItems = null;
		stockTransferItems = new StockTransferItems();
		selectedProductCategoryGroup = new ProductCategoryGroup();
		selectedProductCategory = new ProductCategory();
		selectedProductGroupMaster = new ProductGroupMaster();
		selectedProductVarietyMaster = new ProductVarietyMaster();
		uom = null;
		uomMaster = new UomMaster();
		selectedCircleMaster = new CircleMaster();
		supplierMasterList = new ArrayList<SupplierMaster>();
		seletetedSupplierMaster = new SupplierMaster();
	}
	
	public void editTestingLab(StockTransferItems items) {
		log.info("items==> "+items.getId());
		try {
			stockTransferItems.setRowIndex(items.getRowIndex());
			log.info("item rowIndex==> "+items.getRowIndex());
			
			if(items.getId() != null) {
				log.info("items id==> "+items.getId());
				editButtonChangeFlag = "update";
				stockTransferItems.setId(items.getId());
			}
			if(items.getProductVarietyMaster() != null) {
				selectedProductVarietyMaster = items.getProductVarietyMaster();
				log.info("selectedProductVarietyMaster name==> "+selectedProductVarietyMaster.getName());
				stockTransferItems.setProductVarietyMaster(items.getProductVarietyMaster());
				selectedProductGroupMaster = commonDataService.getGroupByProduct(selectedProductVarietyMaster.getId());
				listenerProductVarietyMasterList();
			}
			if(items.getUomMaster() != null) {
				uomMaster = items.getUomMaster();
				stockTransferItems.setUomMaster(uomMaster); 
				log.info("uom master name==> "+uomMaster.getName());
			}else {
				log.error("UOM master not found");
			}
			if(items.getProductType() != null) {
				log.info("editTestingLab product type==> "+items.getProductType());
				stockTransferItems.setProductType(items.getProductType());
			}else {
				log.error("Product type not found");
			}
			if(items.getOrderedQty() != null) {
				log.info("editTestingLab qty==> "+items.getOrderedQty());
				stockTransferItems.setOrderedQty(items.getOrderedQty());
			}else {
				log.error("Qty type not found");
			}
			
			stockTransferItems.setProductSampleNumber(items.getProductSampleNumber());
		}catch(Exception e) {
			log.error("Exception : editTestingLab==> "+e);
		}
	}
	
	public List<SupplierMaster> supplierAutocomplete(String query) {
		List<SupplierMaster> supplierMasters = new ArrayList<>();
		try {
			log.info("supplierAutocomplete Autocomplete query==>" + query);
			if (supplierMasterList!=null && !supplierMasterList.isEmpty()) {
					if(query != null && !query.isEmpty()) {
						for(SupplierMaster master : supplierMasterList) {
							if(master.getCode().contains(query.toUpperCase())
									|| master.getName().contains(query.toUpperCase())) {
								supplierMasters.add(master);
							}
						}
					}
			} else {
				FacesContext facesContext = FacesContext.getCurrentInstance();
				facesContext.addMessage("circle", new FacesMessage("Please choose the circle"));
				return null;
			}
		}catch (Exception e) {
			log.error("supplierAutocomplete inside exception-----------",e);
		}
		return supplierMasters;
	}
	
	public void loadSupplierCodeNameByCircle() {
		try {
			log.info("-------loadSupplierCodeNameByCircle method starts----------");
			BaseDTO baseDTO = null;
			if (selectedCircleMaster !=null && selectedCircleMaster.getId() != null) {
				String url = SERVER_URL + "/supplier/master/getsuppliermasterbycirclemaster/"
						+ selectedCircleMaster.getId();
				log.info("::: supplierMaster Controller calling  :::");
				baseDTO = httpService.get(url);
				log.info("::: get supplierMaster Response :");
				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					supplierMasterList = new ArrayList<SupplierMaster>();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});
					log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
				} else {
					log.warn("supplierMaster Load Error ");
				}
			}

		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}
}
