package in.gov.cooptex.masters.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IncrementListDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.FeeType;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.TenderCategory;
import in.gov.cooptex.core.model.TenderType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.TenderApplyDTO;
import in.gov.cooptex.dto.pos.TenderListDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.FeeTypeName;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import in.gov.cooptex.operation.tender.model.Tender;
import in.gov.cooptex.operation.tender.model.TenderApplication;
import in.gov.cooptex.operation.tender.model.TenderApplicationFee;
import in.gov.cooptex.operation.tender.model.TenderApplicationFiles;
import in.gov.cooptex.operation.tender.model.TenderFileDetails;
import in.gov.cooptex.operation.tender.model.TenderItemDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("tenderApplyBean")
@Scope("session")
@Log4j2
public class TenderApplyBean {

	private final String VIEW_PAGE_TENDERAPPLY = "/pages/operation/tender/viewTenderApply.xhtml?faces-redirect=true";
	private final String ADD_PAGE_TENDERAPPLY = "/pages/operation/tender/createApplyingTender.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private UploadedFile file = null;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList = new ArrayList<>();
	@Getter
	@Setter
	List<BankMaster> bankMasterList = new ArrayList<>();
	@Getter
	@Setter
	List<BankBranchMaster> bankBranchMasterList = new ArrayList<BankBranchMaster>();

	@Getter
	@Setter
	BankMaster selectedBank;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster = new SupplierTypeMaster();

	@Getter
	@Setter
	List<TenderListDTO> tenderList = new ArrayList<>();

	@Getter
	@Setter
	Tender tender = new Tender();

	@Setter
	@Getter
	private StreamedContent files;

	@Getter
	@Setter
	LazyDataModel<TenderListDTO> lazyTenderList;

	@Getter
	@Setter
	TenderListDTO selectedTender = new TenderListDTO();

	@Getter
	@Setter
	Tender selectedTenderr;

	private String serverURL = AppUtil.getPortalServerURL();

	private final String VIEW_URL = "/tender/view";

	@Getter
	@Setter
	AddressMaster bidopeningAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster bidMeetingAddress = new AddressMaster();

	@Getter
	@Setter
	private int tenderListSize;

	@Getter
	@Setter
	String pageAction, supplierInfo;

	@Getter
	@Setter
	List<TenderType> tenderTypelist = new ArrayList<>();

	@Getter
	@Setter
	List<TenderCategory> tenderCategorylist = new ArrayList<>();

	@Getter
	@Setter
	List<TenderApplyDTO> tenderApplyDtoList = new ArrayList<>();

	@Getter
	@Setter
	List<PaymentMode> paymentModeList = new ArrayList<>();

	@Getter
	@Setter
	PaymentMode paymentMode = new PaymentMode();

	@Getter
	@Setter
	TenderApplication tenderApplication = new TenderApplication();

	@Getter
	@Setter
	TenderApplyDTO tenderApplyDto = new TenderApplyDTO();

	@Getter
	@Setter
	List<TenderApplicationFiles> tenderApplicationFileList = new ArrayList<>();

	@Getter
	@Setter
	List<TenderApplication> tenderApplicationList = new ArrayList<>();

	@Getter
	@Setter
	List<Tender> tendersList = new ArrayList<Tender>();

	@Getter
	@Setter
	TenderApplicationFiles tenderApplicationFiles = new TenderApplicationFiles();

	@Getter
	@Setter
	TenderApplicationFiles demoFiles = null;

	@Getter
	@Setter
	private UploadedFile feePrefile;

	@Getter
	@Setter
	private String feePrefileName;

	@Getter
	@Setter
	Double totalQuotationAmount = 0.0;

	@Getter
	@Setter
	Boolean applyButton = false;

	long allowedDocSize;

	@Getter
	@Setter
	boolean renderTenderCreate;

	@Getter
	@Setter
	List<EntityBankBranch> bankAccountNumberList = new ArrayList<EntityBankBranch>();

	@PostConstruct
	public void init() {
		log.info("TenderApplyBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadApplyTenter();
		paymentModeList = commonDataService.loadPaymentMode();
		if (paymentModeList != null && !paymentModeList.isEmpty()) {
			log.info("paymentModeList size==> " + paymentModeList.size());
		}
		// loadTenderDetailsById();
		loadAppConfigValues();
		selectedTenderr = new Tender();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			allowedDocSize = Long.valueOf(commonDataService.getAppKeyValue("EMP_CHECKLIST_DOC_SIZE_LIMIT_IN_MB"));
			log.info("loadAppConfigValues :: employeePhotoSize==> " + allowedDocSize);
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public void loadApplyTenter() {
		try {
			loadTenderType();
			loadTenderCategory();

		} catch (Exception e) {
			log.error("Exception in loadApplyTenter  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadAllActiveTenderName() {
		log.info("<=TenderApplyBean loadTenderName start=>");
		BaseDTO baseDTO = new BaseDTO();
		tendersList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + "/tenderapply/getalltender/gettenderdetails";
			log.info("loadAllActiveTenderName url==>" + url);
			baseDTO = httpService.get(url);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tendersList = mapper.readValue(jsonResponse, new TypeReference<List<Tender>>() {
				});

				if (tendersList != null && !tendersList.isEmpty()) {
					log.info("tendersList size==> " + tendersList.size());
				} else {
					log.error("tenderList list not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadAllActiveTenderName  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void loadTenderType() {
		log.info("<=TenderApplyBean loadTenderType start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/tenderType/getallactive";
			log.info("loadTenderType url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderTypelist = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
				});
				if (tenderTypelist != null && !tenderTypelist.isEmpty()) {
					log.info("tenderTypelist size==> " + tenderTypelist.size());
				} else {
					log.error("Tender type list not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadTenderType  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadBankBranchList() {
		log.info("<===== Starts TenderApplyBean.loadBankBranchList; ===========>");
		bankBranchMasterList = commonDataService.getBranchDetails(tenderApplyDto.getSelectedBank().getId());
		log.info("<===== End TenderApplyBean.loadBankBranchList; ===========>");
	}

	public void loadTenderCategory() {
		log.info("<=TenderApplyBean loadTenderCategory start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/tenderCategory/getallactive";
			log.info("loadTenderCategory url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderCategorylist = mapper.readValue(jsonResponse, new TypeReference<List<TenderCategory>>() {
				});
				if (tenderCategorylist != null && !tenderCategorylist.isEmpty()) {
					log.info("tenderCategorylist size==> " + tenderCategorylist.size());
				} else {
					log.error("Tender type list not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadTenderCategory  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String loadTenderListPage() {
		try {
			clearValue();
			getAllTenderList();
			bankMasterList = commonDataService.getBankDetails();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/operation/tender/listTenderApplyPage.xhtml?faces-redirect=true";

	}

	public String loadTenderDetailsById() {
		tenderApplyDtoList = new ArrayList<>();
		tender = new Tender();
		paymentModeList = commonDataService.loadPaymentMode();
		return showAddPage();
	}

	public String goToPage() {

		log.info("Goto Page [" + pageAction + "]");

		if (pageAction.equalsIgnoreCase("LIST")) {
			// return showListPage();
		} else if (pageAction.equalsIgnoreCase("ADD")) {
			loadAllActiveTenderName();
			selectedTenderr = new Tender();
//			paymentModeList = commonDataService.loadPaymentModes();
//			return showAddPage();
			return "/pages/operation/tender/createApplyingTender.xhtml?faces-redirect=true";
		} else if (pageAction.equalsIgnoreCase("EDIT")) {
			// return showEditPage();
		} else if (pageAction.equalsIgnoreCase("VIEW")) {
			// return showViewPage();
		} else if (pageAction.equalsIgnoreCase("DELETE")) {
			/*
			 * if (selectedBiometricDeviceRegister == null ||
			 * selectedBiometricDeviceRegister.getId() == null) {
			 * errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			 * return null; }else { RequestContext context =
			 * RequestContext.getCurrentInstance();
			 * context.execute("PF('confirmUserDelete').show();"); } return null;
			 */
		} else {
			return null;
		}
		return null;
	}

	public String showAddPage() {
		BaseDTO baseDTO = new BaseDTO();
		try {

			log.info("tender id==> " + selectedTenderr.getId());

			String url = AppUtil.getPortalServerURL() + "/tender/get/" + selectedTenderr.getId();
			log.info("showAddPage url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});
				loadTenderAmount(tender);
				// return
				// "/pages/operation/tender/createApplyingTender.xhtml?faces-redirect=true";

			} else {
				tender = new Tender();
			}

		} catch (Exception e) {
			log.error("Exception in showAddPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadTenderAmount(Tender tender) {
		try {
			TenderApplyDTO applyOne = new TenderApplyDTO();
			TenderApplyDTO applyTwo = new TenderApplyDTO();
			Double calExamFee = 0.0;

			applyOne.setTenderFeeType(FeeTypeName.TenderFee.toString());
			applyOne.setTenderActualFee(tender.getCostOfTender());
			applyOne.setTenderPaidFee(0D);

			Double tenderAmount = tender.getTotalAmount();
			Double emdPercentate = tender.getEmdPercentage();

			log.info("tenderAmount==>" + tenderAmount);
			log.info("emdPercentate==>" + emdPercentate);
			if (tenderAmount != null && emdPercentate != null) {
				calExamFee = (tenderAmount * emdPercentate) / 100;
			}
			log.info("calExamFee==>" + calExamFee);

			applyTwo.setTenderFeeType(FeeTypeName.EMDFee.toString());
			applyTwo.setTenderActualFee(calExamFee);

			applyTwo.setTenderPaidFee(0D);
			applyTwo.setTenderPaidFee(0D);
			if (tender.getTenderFeeExemptionAllowed() == true) {
				applyOne.setTenderExamFee(tender.getCostOfTender());
				applyTwo.setTenderExamFee(calExamFee);

				applyOne.setTenderFeeToPaid(0D);
				applyTwo.setTenderFeeToPaid(0D);

			} else {
				applyOne.setTenderExamFee(0D);
				applyTwo.setTenderExamFee(0D);
				applyOne.setTenderFeeToPaid(tender.getCostOfTender());
				applyTwo.setTenderFeeToPaid(calExamFee);
			}

			tenderApplyDtoList.add(applyOne);
			tenderApplyDtoList.add(applyTwo);
		} catch (Exception e) {
			log.error("Exception in loadTenderAmount  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public List<String> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete query==>" + query);
		log.info("supplierAutocomplete tender id==>" + tender.getId());
		List<String> results = new ArrayList<String>();
		supplierMasterList = loadSupplierMasterNoTender(query, tender.getId());
		if (supplierMasterList != null || !supplierMasterList.isEmpty()) {
			for (SupplierMaster supplier : supplierMasterList) {
				String supp = supplier.getId() + "/" + supplier.getName();
				results.add(supp);
			}
		}
		return results;
	}

	public List<SupplierMaster> loadSupplierMasterNoTender(String query, Long tenderId) {
		BaseDTO baseDTO = new BaseDTO();
		List<SupplierMaster> supplierMasterList;
		try {
			log.info(".......TenderApplyBean loadSupplierMasterNoTender..............");

			String url = AppUtil.getPortalServerURL() + "/supplier/master/loadsupplierautocompleteNoTender/" + query
					+ "/" + tenderId;
			log.info("loadSupplierMasterNoTender :: url==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
					log.info("supplierMasterList size==> " + supplierMasterList.size());
				} else {
					log.error("Supplier master list not found");
				}
			} else {
				supplierMasterList = new ArrayList<>();
			}
			return supplierMasterList;
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private void getAllTenderList() {
		log.info("<===== Starts TenderApplyBean.lazyTenderList ======>");
		lazyTenderList = new LazyDataModel<TenderListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TenderListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					// String url = AppUtil.getPortalServerURL() + "/tender/getall";
					String url = AppUtil.getPortalServerURL() + "/tenderapply/getall";
					log.info("getAllTenderList ::  url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderList = mapper.readValue(jsonResponse, new TypeReference<List<TenderListDTO>>() {
						});
						if (tenderList != null && !tenderList.isEmpty()) {
							log.info("tenderList size==> " + tenderList.size());
						} else {
							log.error("Tender list not found");
						}
						this.setRowCount(response.getTotalRecords());
						tenderListSize = response.getTotalRecords();
						log.info("tenderListSize==> " + tenderListSize);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in getAllTenderList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tenderList;
			}

			@Override
			public Object getRowKey(TenderListDTO res) {
				return res != null ? res.getTenderId() : null;
			}

			@Override
			public TenderListDTO getRowData(String rowKey) {
				try {
					for (TenderListDTO tenderObj : tenderList) {
						if (tenderObj.getTenderId().equals(Long.valueOf(rowKey))) {
							selectedTender = tenderObj;
							return tenderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in TenderApplyBean getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TenderBean.lazyTenderList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TenderApplyBean.onRowSelect ========>" + event);
		selectedTender = ((TenderListDTO) event.getObject());
		log.info("tender status=> " + selectedTender.getTenderStatus());
		if (selectedTender.getTenderStatus().equalsIgnoreCase("Tender Created")) {
			applyButton = true;
		} else {
			applyButton = false;
		}

		log.info("<===Ends TenderApplyBean.onRowSelect ========>");
	}

	public void showPaymentDetails(TenderApplyDTO tenApplyDto) {
		log.info("<===Starts TenderApplyBean.showPaymentDetails ========>" + tenApplyDto.getTenderFeeType());
		BeanUtils.copyProperties(tenApplyDto, tenderApplyDto);
		RequestContext context = RequestContext.getCurrentInstance();
//		tenderApplyDto=new TenderApplyDTO();
		context.execute("PF('addpayment').show();");
	}

	public String viewTender() {
		// action = "View";
		selectedTenderr = new Tender();
		log.info("<----Redirecting to user view page---->");
		if (selectedTender == null) {
			Util.addWarn("Please Select One Tender");
			return null;
		}
		selectedTenderr.setId(selectedTender.getTenderId());

		getTenderView(selectedTenderr);

		getBidPaymentDetails();

		if (selectedTender.getTenderStatus().equalsIgnoreCase("Tender Created")) {
			renderTenderCreate = true;
		} else {
			renderTenderCreate = false;
		}

		return VIEW_PAGE_TENDERAPPLY;

	}

	public void getBidPaymentDetails() {
		try {
			BaseDTO baseDTO = null;
			log.info(" ======= >>>>>>  Inside getBidPaymentDetails ======>>>>");
			String url = AppUtil.getPortalServerURL() + "/tenderapply/viewDetails";

			if (selectedTender.getTenderId() != null) {
				baseDTO = httpService.post(url, selectedTender.getTenderId());
			}

			if (baseDTO.getStatusCode() == 0) {

//				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//				tenderApplyDtoList = mapper.readValue(jsonResponse, new TypeReference<List<TenderApplyDTO>>() {
//				});
			}
		} catch (Exception e) {
			log.error("Exception Occured In getBidPaymentDetails", e);
		}

	}

	private void getTenderView(Tender selectedTender) {
		log.info("<<<< ----------Start tenderApplyBean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + VIEW_URL;
			baseDTO = httpService.post(url, selectedTender);
			selectedTenderr = new Tender();
			Double totalAmount = 0.0;

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedTenderr = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

				bidopeningAddress = selectedTenderr.getBidMeetingAddressId();
				log.info("bidopeningAddress id==> " + bidopeningAddress.getId());

				bidMeetingAddress = selectedTenderr.getBidMeetingAddressId();

				log.info("bidMeetingAddress id==> " + bidMeetingAddress.getId());

			}
			for (TenderItemDetails item : selectedTenderr.getTenderItemDetailsList()) {
				totalAmount += item.getItemAmount();
			}
			selectedTenderr.setTotalAmount(totalAmount);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedTender != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End TenderBean-getTenderView ------- >>>>");
	}

	public void downloadfile(TenderFileDetails tenderFileDetails) {
		InputStream input = null;
		try {
			log.info("filepathhhh" + tenderFileDetails.getFilePath());
			if (tenderFileDetails.getFilePath() != null) {
				File file = new File(tenderFileDetails.getFilePath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				files = (new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()),
						file.getName()));
				log.error(" File Downloaded >>>>>>>>>>------- " + files.getName());

			}
		} catch (Exception e) {
			log.error("Exception Occured IN DownloadFile", e);
		}

	}

	public void loadPaymentDetails() {
		log.info("<===Starts TenderApplyBean.loadPaymentDetails tenderApplyDto ========>" + tenderApplyDto);
		if (tenderApplyDto.getPaymentMode() == null) {
			errorMap.notify(ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED.getErrorCode());
			return;
		} else if (tenderApplyDto.getTenderPaidFee() == null || tenderApplyDto.getTenderPaidFee().equals(0D)) {
			errorMap.notify(ErrorDescription.EDLI_PAYMENT_AMOUNT_REQUIRED.getErrorCode());
			return;
		} else if (tenderApplyDto.getSelectedBank() == null) {
			errorMap.notify(ErrorDescription.BANKMASTER_BANK_NAME_CANNOT_BE_EMPTY.getErrorCode());
			return;
		} else {
//			tenderApplyDtoList
//			.removeIf((TenderApplyDTO ten) -> ten.getTenderFeeType().equals(tenderApplyDto.getTenderFeeType()));
//			tenderApplyDtoList.add(tenderApplyDto);
//			tenderApplyDto = new TenderApplyDTO();

			for (TenderApplyDTO tenderDTOobj : tenderApplyDtoList) {
				if (tenderApplyDto.getTenderFeeType().equalsIgnoreCase(tenderDTOobj.getTenderFeeType())) {
					tenderDTOobj.setActNo(tenderApplyDto.getActNo());
					tenderDTOobj.setChallanNumber(tenderApplyDto.getChallanNumber());
					tenderDTOobj.setDdNumber(tenderApplyDto.getDdNumber());
					tenderDTOobj.setExpDate(tenderApplyDto.getExpDate());
					tenderDTOobj.setIssuedDate(tenderApplyDto.getIssuedDate());
					tenderDTOobj.setPaymentMode(tenderApplyDto.getPaymentMode());
					tenderDTOobj.setSelectedBank(tenderApplyDto.getSelectedBank());
					tenderDTOobj.setSelectedBranchBankId(tenderApplyDto.getSelectedBranchBankId());
					tenderDTOobj.setTenderPaidFee(tenderApplyDto.getTenderPaidFee());
					tenderDTOobj.setPayableAt(tenderApplyDto.getPayableAt());

					if (tenderDTOobj.getTenderExamFee() > tenderApplyDto.getTenderPaidFee()) {
						tenderDTOobj.setTenderFeeToPaid(
								tenderDTOobj.getTenderExamFee() - tenderApplyDto.getTenderPaidFee());
					}
					break;
				}

			}

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addpayment').hide();");
		}
	}

	public void uploadFeePreFile(FileUploadEvent event) {
		log.info("TenderApplyBean.Upload button is pressed..");

		try {
			demoFiles = new TenderApplicationFiles();
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_DOCUMENTS_NOT_EMPTY).getCode());
				return;
			}
			feePrefile = event.getFile();
			long size = feePrefile.getSize();
			log.info("uploadFeePreFile size==>" + size);
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			allowedDocSize = Long.valueOf(commonDataService.getAppKeyValue("EMP_CHECKLIST_DOC_SIZE_LIMIT_IN_MB"));
			log.info("allowedDocSize ==> " + allowedDocSize);
			if (size > allowedDocSize) {
				tenderApplicationFiles.setFilePath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_FILE_SIZE).getCode());
				return;
			}
			log.info("uploadFeePreFile :: feePrefile==> " + feePrefile);
			String photoPathName = commonDataService.getAppKeyValue("TENDER_APPLY_PATH");
			log.info("uploadFeePreFile :: photoPathName==> " + photoPathName);
			String filePath = Util.fileUpload(photoPathName, feePrefile);
			log.info("uploadFeePreFile :: filePath==> " + filePath);
			tenderApplicationFiles.setFileName("Fee/Pre Qualification/Technical Qual/Finance Document");
			tenderApplicationFiles.setFilePath(filePath);
			BeanUtils.copyProperties(tenderApplicationFiles, demoFiles);
			tenderApplicationFileList.add(demoFiles);
			tenderApplicationFiles = new TenderApplicationFiles();
			feePrefileName = feePrefile.getFileName();
			errorMap.notify(ErrorDescription.TENDER_APPLY_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void deleteUpload(TenderApplicationFiles file) {
		tenderApplicationFileList.remove(file);
	}

	public String saveApplyTender() {
		try {
			log.info("<=call saveUpdateBiometric=>");
			if (supplierInfo == null || supplierInfo.isEmpty()) {
				/*
				 * AppUtil.addWarn("Select the supplier"); return null;
				 */
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_THE_SUPPLIER).getCode());
				return null;
			}

			if (tenderApplicationFileList == null || tenderApplicationFileList.isEmpty()) {
				// AppUtil.addWarn("Select the file details");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_THE_FILE_DETAILS).getCode());
				return null;
			}

			SupplierMaster supplierMaster = new SupplierMaster();
			if (supplierInfo != null) {
				String supplierArr[] = supplierInfo.split("/");
				log.info("supplierArr length==> " + supplierArr.length);
				if (supplierArr.length == 1) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_THE_SUPPLIER).getCode());
					return null;
				}
				supplierMaster.setId(Long.parseLong(supplierArr[0]));
				supplierMaster.setName(supplierArr[1]);

			}
			tenderApplication.setSupplierMaster(supplierMaster);
			tenderApplication.setActiveStatus(true);
			tenderApplication.setTender(tender);
			List<TenderApplicationFee> appList = new ArrayList<>();

			if (tenderApplyDtoList != null) {
				for (TenderApplyDTO tenderApplyDto : tenderApplyDtoList) {
					if (tenderApplyDto.getPaymentMode() == null || tenderApplyDto.getPaymentMode().getId() == null) {
						// AppUtil.addWarn("Select the payment type");
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.SELECT_THE_PAYMENT_TYPE).getCode());
						return null;
					}
					if (tenderApplyDto.getTenderPaidFee() == null || tenderApplyDto.getTenderPaidFee() == 0D) {
						// AppUtil.addWarn("Enter the amount");
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTER_THE_AMOUNT).getCode());
						return null;
					}

//					if (tenderApplyDto.getDdNumber() == null) {
//						// AppUtil.addWarn("Enter the DD number");
//						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTER_THE_DD_NUMBER).getCode());
//						return null;
//					}
//					if (tenderApplyDto.getExpDate() == null) {
//						// AppUtil.addWarn("Enter the exp date");
//						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTER_THE_EXP_DATE).getCode());
//						return null;
//					}
//					if (tenderApplyDto.getIssuedDate() == null) {
//						// AppUtil.addWarn("Enter the issued date");
//						errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTER_THE_ISSUED_DATE).getCode());
//						return null;
//					}
					TenderApplicationFee tenderApplicationFee = new TenderApplicationFee();
					tenderApplicationFee.setTenderApplication(tenderApplication);
					tenderApplicationFee.setPaymentMode(tenderApplyDto.getPaymentMode());
					tenderApplicationFee.setFeePaid(tenderApplyDto.getTenderFeeToPaid());
					FeeType feeType = new FeeType();
					feeType.setName(tenderApplyDto.getTenderFeeType());
					tenderApplicationFee.setFeeType(feeType);
					tenderApplicationFee.setActualFee(tenderApplyDto.getTenderActualFee());
					tenderApplicationFee.setFeeToBePaid(tenderApplyDto.getTenderFeeToPaid());
					tenderApplicationFee.setFeePaid(tenderApplyDto.getTenderPaidFee());
					tenderApplicationFee.setDemandDraftNo(tenderApplyDto.getDdNumber());
					tenderApplicationFee.setIssuedDate(tenderApplyDto.getIssuedDate());
					tenderApplicationFee.setExpiryDate(tenderApplyDto.getExpDate());
					tenderApplicationFee.setAmount(tenderApplyDto.getTenderPaidFee());
					tenderApplicationFee.setChallanNumber(tenderApplyDto.getChallanNumber());
					tenderApplicationFee.setAccountNumber(tenderApplyDto.getActNo());
					if (tenderApplyDto.getPayableAt() != null) {
						// tenderApplicationFee.(tenderApplyDto.getPayableAt());
					}
					appList.add(tenderApplicationFee);
				}
			}
			tenderApplication.setApplicationFeeList(appList);
			tenderApplication.setApplicationFilesList(tenderApplicationFileList);

			// --set Quotation
			List<QuotationItem> qutItemList = new ArrayList<>();
			Quotation quotation = new Quotation();
			quotation.setSupplier(supplierMaster);
			quotation.setActiveStatus(true);
			quotation.setTender(tender);

			if (tender != null || tender.getTenderItemDetailsList() != null) {
				for (TenderItemDetails tenderItem : tender.getTenderItemDetailsList()) {
					if (tenderItem.getQuotationAmount() != null && tenderItem.getQuotationAmount() != 0.0) {
						QuotationItem qutItem = new QuotationItem();
						qutItem.setProductVarietyMaster(tenderItem.getProductVarietyMaster());
						Long itQty = Math.round(tenderItem.getItemQuantity());
						qutItem.setUom(tenderItem.getProductVarietyMaster().getUomMaster());
						qutItem.setItemQuantity(itQty.doubleValue());
						qutItem.setItemAmount(tenderItem.getQuotationAmount());
						qutItemList.add(qutItem);
						quotation.setQuotationItemList(qutItemList);
					} else {
						// AppUtil.addWarn("Enter the quotation amount");
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.ENTER_THE_QUOTATION_AMOUNT).getCode());
						return null;
					}
				}
			}
			quotation.setQuotationItemList(qutItemList);
			tenderApplication.setQuotation(quotation);

			String url = AppUtil.getPortalServerURL() + "/tenderapply/create";
			log.info("saveApplyTender :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, tenderApplication);
			log.info("saveApplyTender :: baseDTO ==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.TENDER_APPLY_INSERTED_SUCCESSFULLY).getCode());
				clearValue();
				return "/pages/operation/tender/listTenderApplyPage.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving apply tender with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in saveApplyTender  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void clearValue() {
		tenderApplication = new TenderApplication();
		tenderApplyDto = new TenderApplyDTO();
		tenderApplyDtoList = new ArrayList<>();
		tenderApplicationFileList = new ArrayList<>();
		tender = new Tender();
		selectedTender = new TenderListDTO();
		supplierInfo = null;
		tenderList = new ArrayList<>();
		applyButton = false;

	}

	public String gotoList() {
		try {
			selectedTender = new TenderListDTO();
			tenderApplication = new TenderApplication();
			tenderApplyDto = new TenderApplyDTO();
			tenderApplyDtoList = new ArrayList<>();
			tenderApplicationFileList = new ArrayList<>();
			tender = new Tender();
			supplierInfo = null;
			tenderList = new ArrayList<>();

		} catch (Exception e) {
			log.error("Exception in gotoList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/operation/tender/listTenderApplyPage.xhtml?faces-redirect=true";
	}

	public void calTotalQuotationAmount() {
		log.info("<=call calTotalQuotationAmount=>");
		try {
			totalQuotationAmount = 0.0;
			Double calVal = 0.0;
			if (tender.getTenderItemDetailsList() != null) {
				for (TenderItemDetails tenderItemDetails : tender.getTenderItemDetailsList()) {
					if (tenderItemDetails.getItemAmount() > tenderItemDetails.getQuotationAmount()) {
						log.info("qut amount==>" + tenderItemDetails.getQuotationAmount());
						calVal = calVal + tenderItemDetails.getQuotationAmount();
						log.info("calVal 1==>" + calVal);
					} else {
						// AppUtil.addWarn("Quotation amount less than item amount");
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.QUOTATION_AMOUNT_LESS_THAN_ITEM_AMOUNT).getCode());
						return;
					}
				}
			}
			totalQuotationAmount = calVal;
			log.info("TotalQuotationAmount==>" + totalQuotationAmount);

		} catch (Exception e) {
			log.error("Exception in gotoList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadBankBranchAccountNumberList() {
		log.info("<===== Starts TenderApplyBean.loadBankBranchAccountNumberList; ===========>");
		bankAccountNumberList = commonDataService
				.getBranchAccountNumberDetails(tenderApplyDto.getSelectedBranchBankId());
		log.info("<===== End TenderApplyBean.loadBankBranchAccountNumberList; ===========>");
	}

}
