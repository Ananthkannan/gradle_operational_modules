package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.joda.time.Days;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.LeaveTravelConCessionStatus;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.accounts.model.HolidayMaster;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.LeaveTravelConcessionDto;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmpLtcHolidayDates;
import in.gov.cooptex.core.model.EmployeeFamilyDetails;
import in.gov.cooptex.core.model.EmployeeLeaveTravelConcession;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.FileMovementConfig;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.LeaveRequest;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.EmployeeLeaveTravelConcessionHolidayDatesDto;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeLtcBean")
public class EmployeeLTCBean {

	public static final String LTC_URL = AppUtil.getPortalServerURL() + "/employeeprofile/ltc";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String url;

	private final String LIST_URL = "/pages/leaveManagement/listLeaveTravelConcession.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean login;

	@Getter
	@Setter
	String pageAction, fileName;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	EmployeeLeaveTravelConcession employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();

	@Getter
	@Setter
	String action, blockYears, appliedFor = "Self", travelMode;

	@Getter
	@Setter
	List<String> blockYearsList = new ArrayList<String>();

	@Getter
	@Setter
	Boolean addMember, isPermission = false, holidayPermission, addButton, editButton, rejectCheckFlag, deleteButton;

	@Getter
	@Setter
	LeaveTypeMaster leaveTypeMaster = new LeaveTypeMaster();

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeFamilyDetails> employeeFamilyList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeFamilyDetails employeeFamily;

	@Getter
	@Setter
	List<EmployeeFamilyDetails> selectedFamilyDetails = new ArrayList<>();

	@Getter
	@Setter
	List<RelationshipMaster> relationshipMasterList = new ArrayList<>();

	@Getter
	@Setter
	RelationshipMaster selectedRelation;

	@Getter
	@Setter
	List<String> memberIds = new ArrayList<>();

	@Getter
	@Setter
	UploadedFile uploadedFile;

	long documentSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<String> fromdateToDateList = new ArrayList<>();

	@Getter
	@Setter
	LeaveRequest leaveDetail = null;

	String json;

	@Getter
	@Setter
	LazyDataModel<EmployeeLeaveTravelConcession> ltcDetailsList;

	@Getter
	@Setter
	EmployeeLeaveTravelConcession selectedLTCDetails;

	@Getter
	@Setter
	int totalSize = 0;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	EntityMaster selectedHoRo;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	EntityMaster slectedEntity;

	@Getter
	@Setter
	List<EntityTypeMaster> entitytypelistByHeadAndRegionalId = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> entitylistByHeadAndRegionalIdAndEntityTypeId = new ArrayList<>();

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department selectedDepartement;

	@Getter
	@Setter
	SectionMaster selectedSection;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterListBasedOnDepatementID = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterBasedOnEntityList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterBasedOnSectionList = new ArrayList<>();

	@Getter
	@Setter
	List<FinancialYear> blockYearListFromFinacialYear = new ArrayList<>();

	@Getter
	@Setter
	Boolean departementFlag = true;

	@Getter
	@Setter
	Boolean entityEmployeeFlag = false;

	@Getter
	@Setter
	EmployeeMaster employeeMasterBasedOnEntityId = new EmployeeMaster();

	@Getter
	@Setter
	EmployeeMaster employeeMasterBasedOnSectionId = new EmployeeMaster();

	@Getter
	@Setter
	FinancialYear selectedBlocKYear;

	@Getter
	@Setter
	List<EmpLtcHolidayDates> empLtcHolidayDatesList = new ArrayList<EmpLtcHolidayDates>();

	@Getter
	@Setter
	List<EmployeeLeaveTravelConcessionHolidayDatesDto> empLTCHolidaysDateList = new ArrayList<EmployeeLeaveTravelConcessionHolidayDatesDto>();

	@Getter
	@Setter
	EmployeeLeaveTravelConcessionHolidayDatesDto empLTCHolidaysDate;

	@Getter
	@Setter
	EmpLtcHolidayDates empLtcHolidayDates;

	@Getter
	@Setter
	List<HolidayMaster> holidayListInBetweenEmployeeLeaveDates = new ArrayList<HolidayMaster>();

	@Getter
	@Setter
	Boolean holidayFlag = true;

	@Getter
	@Setter
	List<UserMaster> leaveTravelConcessionForwardToList;

	@Getter
	@Setter
	LeaveTravelConcessionDto leaveTravelConcessionDto;

	@Getter
	@Setter
	List<LeaveTravelConcessionDto> listPageEmployeeLtclist = new ArrayList<LeaveTravelConcessionDto>();

	@Getter
	@Setter
	LazyDataModel<LeaveTravelConcessionDto> empLTClazyLoadReport;

	@Getter
	@Setter
	LeaveTravelConcessionDto selectedLtcDto = new LeaveTravelConcessionDto();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	List<LeaveTravelConcessionDto> ltcListForCurrentUser = new ArrayList<LeaveTravelConcessionDto>();

	@Getter
	@Setter
	EmployeeMaster loginEmployee = new EmployeeMaster();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Double eligibleDistance = 0D;

	@Getter
	@Setter
	List<EmpLtcHolidayDates> holidayList = new ArrayList<EmpLtcHolidayDates>();

	@Getter
	@Setter
	String dependantName;

	@Getter
	@Setter
	Long age;

	@Getter
	@Setter
	Boolean billButton;
	@Getter
	@Setter
	Boolean finalApproval;

	@PostConstruct
	public void init() {
		billButton = false;

		loadLazyEmployeeLeaveTravelConcessionList();
		log.info("Init is executed.....................");
		employeeMaster = new EmployeeMaster();
		loadLtcValues();
		loadAppConfigValues();
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		mapper = new ObjectMapper();
		selectedRelation = new RelationshipMaster();
		employeeFamily = new EmployeeFamilyDetails();
		empLTCHolidaysDate = new EmployeeLeaveTravelConcessionHolidayDatesDto();
		departementFlag = true;
		entityEmployeeFlag = false;
		addMember = false;
		employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
		empLtcHolidayDates = new EmpLtcHolidayDates();
		leaveTravelConcessionDto = new LeaveTravelConcessionDto();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		leaveTravelConcessionForwardToList = commonDataService
				.loadForwardToUsersByFeature(AppFeatureEnum.LEAVE_TRAVEL_CONCESSION.toString());
		log.info("leaveTravelConcessionForwardToList" + leaveTravelConcessionForwardToList);
		loadFamilyRelationshipFromRelationshipMaster();
		loadLeaveTypeMaster();
		loadBlockYearsFromFinancialYearTable();
		for (int i = 0; i < 1; i++) {
			String s = new String();
			fileList.add(s);
		}
		loginEmployee = loginBean.getEmployee();

	}

	private Double loadEligibleDistance() {
		log.info("====Start EmployeeLTCBean.loadEligibleDistance==== ");
		Double eligibleTravelDistance = 0D;
		try {
			EmployeeMaster employeeMaster = loginBean.getEmployee();
			String url = SERVER_URL + "/leavetravelconcession/ltc/getTravelledDistanceByEmpId/"
					+ employeeMaster.getId();
			log.info("===Url is===" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				Double travelledDistance = baseDTO.getSumTotal();
				eligibleTravelDistance = eligibleDistance - travelledDistance;
				if (eligibleTravelDistance < 0D) {
					eligibleTravelDistance = 0D;
				}
				employeeLeaveTravelConcession.setEligibleDistance(eligibleTravelDistance);
			}
		} catch (Exception e) {
			log.info("====Exception Occured in EmployeeLTCBean.loadEligibleDistance==== ");
			log.error("====Exception is==== " + e.getMessage());
		}
		log.info("====End EmployeeLTCBean.loadEligibleDistance==== ");
		return eligibleTravelDistance;
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			documentSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public void loadAllEntityTypesByRegionId() {

		departementFlag = true;
		entityEmployeeFlag = false;

		if (entitytypelistByHeadAndRegionalId != null) {
			entitytypelistByHeadAndRegionalId.clear();
			selectedEntityTypeMaster = new EntityTypeMaster();
		}
		if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
			entitylistByHeadAndRegionalIdAndEntityTypeId.clear();

		}
		if (departmentList != null) {
			departmentList.clear();

		}
		if (sectionMasterListBasedOnDepatementID != null) {
			sectionMasterListBasedOnDepatementID.clear();
		}
		if (employeeMasterBasedOnEntityList != null) {
			employeeMasterBasedOnEntityList.clear();
		}
		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
		}

		try {

			if (selectedHoRo != null) {
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ selectedHoRo.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					entitytypelistByHeadAndRegionalId = mapper.readValue(jsonValue,
							new TypeReference<List<EntityTypeMaster>>() {
							});

					log.info("entitytypelistByHeadAndRegionalId list size is "
							+ entitytypelistByHeadAndRegionalId.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void loadAllEntityByRegionIdandEntityTypeId() {

		if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
			entitylistByHeadAndRegionalIdAndEntityTypeId.clear();
			slectedEntity = new EntityMaster();

		}
		if (departmentList != null) {
			departmentList.clear();

		}
		if (sectionMasterListBasedOnDepatementID != null) {
			sectionMasterListBasedOnDepatementID.clear();
		}
		if (employeeMasterBasedOnEntityList != null) {
			employeeMasterBasedOnEntityList.clear();
		}
		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
		}

		try {

			if (selectedHoRo != null && selectedEntityTypeMaster != null) {
				Long regionId = selectedHoRo.getId();
				Long entityTypeId = selectedEntityTypeMaster.getId();
				String entityTypeName = selectedEntityTypeMaster.getEntityName();
				log.info("the selected entity name is" + " " + entityTypeName);

				if (entityTypeName.equalsIgnoreCase("Showroom")) {
					log.info("user selected the Showroom ");
					departementFlag = false;
					entityEmployeeFlag = true;
				} else {
					departementFlag = true;
					entityEmployeeFlag = false;
				}

				entitylistByHeadAndRegionalIdAndEntityTypeId = commonDataService
						.loadEntityByregionOrentityTypeId(regionId, entityTypeId);

				log.info("entitylistByHeadAndRegionalIdAndEntityTypeId List size is" + " "
						+ entitylistByHeadAndRegionalIdAndEntityTypeId.size());

				if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
					for (EntityMaster entity : entitylistByHeadAndRegionalIdAndEntityTypeId) {
						log.info("the WorkLocation Id" + " " + entity.getId());
					}
				}
			}

		} catch (Exception e) {
			log.info("The cause for Exception is" + " " + e.getCause());
		}

	}

	public void loadAllDepartementOREmployeBaseOnDepartementFlag() {

		if (departementFlag) {
			loadAllDepartement();
			employeeMasterBasedOnEntityId = new EmployeeMaster();
		} else {
			loadAllEmployeBasedOnEntityId();
			employeeMasterBasedOnSectionId = new EmployeeMaster();
		}

	}

	public void loadAllDepartement() {
		if (departmentList != null) {
			departmentList.clear();
			selectedDepartement = new Department();

		}
		if (sectionMasterListBasedOnDepatementID != null) {
			sectionMasterListBasedOnDepatementID.clear();
		}
		if (employeeMasterBasedOnEntityList != null) {
			employeeMasterBasedOnEntityList.clear();
		}
		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
		}

		try {
			departmentList = commonDataService.getDepartment();
			log.info("the list returned form data base size is" + " " + departmentList.size());
		} catch (Exception e) {
			log.info("the cause of the exception is " + " " + e.getCause());
		}
	}

	public void loadAllEmployeBasedOnEntityId() {

		try {
			log.info("selected entity id is" + " " + slectedEntity.getId());
			if (slectedEntity != null) {
				String url = SERVER_URL + "/employeeprofile/getEmployeListBaseOnEntityId/" + slectedEntity.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterBasedOnEntityList = mapper.readValue(jsonValue,
							new TypeReference<List<EmployeeMaster>>() {
							});

					log.info("employeeMasterBasedOnEntityList list size is " + employeeMasterBasedOnEntityList.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void loadAllSectionBasedOnDepartementId() {

		if (sectionMasterListBasedOnDepatementID != null) {
			sectionMasterListBasedOnDepatementID.clear();
			selectedSection = new SectionMaster();
		}
		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
		}

		log.info("inside loadAllSectionBasedOnDepartementId ");
		employeeMasterBasedOnSectionList.clear();

		try {

			if (selectedHoRo != null) {
				String url = SERVER_URL + "/employeeprofile/getSectionListBasedOnDepartementId/"
						+ selectedDepartement.getName();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					sectionMasterListBasedOnDepatementID = mapper.readValue(jsonValue,
							new TypeReference<List<SectionMaster>>() {
							});

					log.info("sectionMasterListBasedOnDepatementID list size is "
							+ sectionMasterListBasedOnDepatementID.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void loadAllEmploymasaterBasedOnSectionId() {

		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
			employeeMasterBasedOnSectionId = new EmployeeMaster();
		}

		try {
			log.info("the selected section is" + " " + selectedSection);
			if (selectedHoRo != null) {
				String url = SERVER_URL + "/employeeprofile/getEmployeListBaseOnSectionId/" + selectedSection.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterBasedOnSectionList = mapper.readValue(jsonValue,
							new TypeReference<List<EmployeeMaster>>() {
							});

					log.info(
							"employeeMasterBasedOnSectionList list size is " + employeeMasterBasedOnSectionList.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
			log.info("the cause of Error is" + " " + e.getMessage());
		}

	}

	public void loadBlockYearsFromFinancialYearTable() {

		try {
			if (blockYearListFromFinacialYear.size() > 0) {
				blockYearListFromFinacialYear.clear();
			}
			String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
			BaseDTO baseDTO = httpService.get(url);
			List<FinancialYear> blockYearList = new ArrayList<>();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				blockYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
				});

				log.info("blockYearList list size is " + blockYearList.size());

				if (blockYearList.size() > 0) {

					for (FinancialYear yearObject : blockYearList) {
						FinancialYear endYear = new FinancialYear();
						Long year = yearObject.getStartYear() + 4;
						log.info("the end year is" + " " + yearObject.getStartYear() + " -" + year);
						endYear.setId(yearObject.getId());
						endYear.setStartYear(yearObject.getStartYear());
						endYear.setEndYear(year);
						blockYearListFromFinacialYear.add(endYear);
					}
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void loadLeaveTypeMaster() {

		try {
			String url = SERVER_URL + "/leavetype/ltcleave";
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				leaveTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<LeaveTypeMaster>>() {
				});

				log.info("leaveTypeMasterList list size is " + leaveTypeMasterList.size());
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
			log.info("the cause of Error is" + " " + e.getMessage());
		}

	}

	public void showFamilyDetailsTable() {

	}

	/*
	 * public void lazyTTCDetails() { log.info("lazy load lazyTTCDetails called..");
	 * List = new LazyDataModel<EmployeeLeaveTravelConcession>() {
	 * 
	 * private static final long serialVersionUID = -9122090322809059767L;
	 * 
	 * List<EmployeeLeaveTravelConcession> dataList = new
	 * ArrayList<EmployeeLeaveTravelConcession>();
	 * 
	 * @Override public List<EmployeeLeaveTravelConcession> load(int first, int
	 * pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters)
	 * {
	 * 
	 * try { EmployeeLeaveTravelConcession request = new
	 * EmployeeLeaveTravelConcession(); request.setFirst(first / pageSize);
	 * request.setPagesize(pageSize); request.setSortField(sortField);
	 * request.setSortOrder(sortOrder.toString()); request.setFilters(filters);
	 * 
	 * log.
	 * info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}"
	 * , first, pageSize, sortField, sortOrder, filters); UserMaster user =
	 * login.getUserDetailSession();
	 * 
	 * log.info("getId==>" + user.getId()); request.setUserId(user.getId());
	 * 
	 * log.info("Search request data" + request);
	 * 
	 * BaseDTO baseDTO = httpService.post(LTC_URL + "/loadlazylist", request);
	 * 
	 * if (baseDTO == null) { log.error(" Base DTO returned Null Values ");
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); return null;
	 * }
	 * 
	 * json = mapper.writeValueAsString(baseDTO.getResponseContents()); dataList =
	 * mapper.readValue(json, new
	 * TypeReference<List<EmployeeLeaveTravelConcession>>() { }); if (dataList ==
	 * null) { log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); } if
	 * (baseDTO.getStatusCode() == 0) {
	 * 
	 * this.setRowCount(baseDTO.getTotalRecords()); totalSize =
	 * baseDTO.getTotalRecords(); log.info(":: LTC search Result size ::" +
	 * totalSize); } else {
	 * log.error(":: Employee LTC Search Failed With status code :: " +
	 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); } return
	 * dataList; } catch (Exception e) {
	 * log.error(":: Exception Exception Ocured while Loading Employee LTC data ::",
	 * e); errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); } return
	 * null; }
	 * 
	 * @Override public Object getRowKey(EmployeeLeaveTravelConcession res) { return
	 * res != null ? res.getId() : null; }
	 * 
	 * @Override public EmployeeLeaveTravelConcession getRowData(String rowKey) {
	 * List<EmployeeLeaveTravelConcession> responseList =
	 * (List<EmployeeLeaveTravelConcession>) getWrappedData(); Long value =
	 * Long.valueOf(rowKey); for (EmployeeLeaveTravelConcession res : responseList)
	 * { if (res.getId().longValue() == value.longValue()) { selectedLTCDetails =
	 * res; return res; } } return null; } };
	 * 
	 * }
	 */

	@Getter
	@Setter
	List<String> holidaysList;

	// @Getter @Setter
	// List<EmployeeFamilyDetails> employeeFamilyList;

	public String showPage() throws Exception {
		if (selectedLtcDto == null && !action.equalsIgnoreCase("add") && !action.equalsIgnoreCase("cancel")) {
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return null;
		}
		loadBlockYearsFromFinancialYearTable();
		loginEmployee = loginBean.getEmployee();
		log.info("showPage action ::::::" + action);
		selectedFamilyDetails = new ArrayList<>();
		if (action.equalsIgnoreCase("add")) {
			employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			selectedBlocKYear = new FinancialYear();
			memberIds = new ArrayList<>();
			leaveDetail = new LeaveRequest();
			employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			selectedFamilyDetails = new ArrayList<>();
			fromdateToDateList.clear();
			appliedFor = "Self";
			isPermission = false;
			String appValue = commonDataService.getAppKeyValue("LTC_DISTANCE");
			if (appValue == null) {
				throw new Exception("LTC_DISTANCE AppConfig Value is Empty");
			}
			eligibleDistance = Double.parseDouble(appValue);
			eligibleDistance = loadEligibleDistance();
			return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";
		}
		if (action.equalsIgnoreCase("edit")) {
			if (selectedLtcDto == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			employeeLeaveTravelConcession = getById(selectedLtcDto.getLeaveTravelId());
			travelMode = employeeLeaveTravelConcession.getTravelMode();
			selectedBlocKYear = new FinancialYear();
			String block = employeeLeaveTravelConcession.getBlockYears();
			if (block != null) {
				String[] blk = block.split("-");
				String year = blk[0] != null ? blk[0] : "";
				if (!year.equals(""))
					blocKYear = Long.parseLong(year);
			}
			selectBlockYearsFromFinancialYearTable();
			onLeaveTypeChange();

			return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";
		}
		if (action.equalsIgnoreCase("Delete")) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmAccountCategoryDelete').show();");
		}
		if (action.equalsIgnoreCase("")) {
			/*
			 * if (selectedLtcDto == null) {
			 * errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			 * return null; }
			 * 
			 * EmployeeLeaveTravelConcession employeeLeaveTravelConcession =
			 * getById(selectedLtcDto.getLeaveTravelId());
			 * 
			 * employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
			 * loadFamilyMembersView();
			 * log.info("employeeLeaveTravelConcession::::::::::::::::" +
			 * employeeLeaveTravelConcession); if (employeeLeaveTravelConcession != null &&
			 * employeeLeaveTravelConcession.getHolidayDates() != null) { String holidayDate
			 * = employeeLeaveTravelConcession.getHolidayDates(); holidaysList =
			 * Arrays.asList(holidayDate.split("\\s*,\\s*")); }
			 * 
			 * return
			 * "/pages/leaveManagement/viewLeaveTravelConcession.xhtml?faces-redirect=true";
			 */}
		if (action.equalsIgnoreCase("view") || action.equalsIgnoreCase("billsAdd")) {

			if (selectedLtcDto == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			;
			EmployeeLeaveTravelConcession employeeLeaveTravelConcession = getByAddId(selectedLtcDto.getLeaveTravelId());
			setPreviousApproval(leaveTravelConcessionDto.getForwardFor());
			employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
			log.info("getForwardTo::::::::::::::::" + leaveTravelConcessionDto.getForwardTo());
			loadFamilyMembersView();
			leaveTravelConcessionForwardToList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());

			StringBuilder sb = new StringBuilder();
			sb.append(employeeLeaveTravelConcession.getBlockYearFrom());
			sb.append("-");
			sb.append(employeeLeaveTravelConcession.getBlockYearTo());
			employeeLeaveTravelConcession.setBlockYears(sb.toString());

			if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getHolidayDates() != null) {
				String holidayDate = employeeLeaveTravelConcession.getHolidayDates();
				holidaysList = Arrays.asList(holidayDate.split("\\s*,\\s*"));
			}

			previousApproval = leaveTravelConcessionDto.getForwardFor();
			stage = leaveTravelConcessionDto.getStatus();
			finalApproval = leaveTravelConcessionDto.getForwardFor();

			if (leaveTravelConcessionDto.getForwardTo().equals(loginBean.getUserDetailSession().getId())) {
				log.info("====Approval Flag Condition Current User and ForwardTo User are Equal============");
				setApprovalFlag(true);
			} else {
				log.info("=======Approval Flag Condition Current User and ForwardTo User are Not Equal===============");
				setApprovalFlag(false);
			}

			if (action.equalsIgnoreCase("billsAdd")
					&& stage.equals(LeaveTravelConCessionStatus.FINALAPPROVED.toString())) {
				setApprovalFlag(true);
				setPreviousApproval(false);
				setBillsubmitFlag(true);
			} else {
				setBillsubmitFlag(false);
			}

			leaveTravelConcessionDto.setRemarks(null);
			userMaster = new UserMaster();
			if ("billsAdd".equalsIgnoreCase(action))
				return "/pages/personnelHR/addLTCBillsESS.xhtml?faces-redirect=true;";
			else
				return "/pages/leaveManagement/viewLeaveTravelConcession.xhtml?faces-redirect=true";

		}
		if (action.equalsIgnoreCase("cancel") || action.equalsIgnoreCase("list")) {
			loadBlockYears();
			addButton = false;
			billButton = false;
			editButton = false;
			selectedLTCDetails = new EmployeeLeaveTravelConcession();
			return "/pages/leaveManagement/listLeaveTravelConcession.xhtml?faces-redirect=true";
		}

		return null;
	}

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	public void loadFamilyMembersView() {
		try {/*
				 * log.info("loadFamilyMembersView called..." + employeeLeaveTravelConcession);
				 * if (employeeLeaveTravelConcession.getFamilyDetailsIds() != null) { String
				 * familyIds = employeeLeaveTravelConcession.getFamilyDetailsIds();
				 * 
				 * log.info("familyIdsList ids" + familyIds);
				 * 
				 * if (employeeMaster.getId() != null && familyIds.length() > 0) {
				 * log.info("final lst of Family IDs ::::" + familyIds);
				 * 
				 * url = AppUtil.getPortalServerURL() + "/employee/insurance/familymember/" +
				 * employeeMaster.getId() + "/" + familyIds;
				 * log.info("family member URL Request :::" + url); BaseDTO response =
				 * httpService.get(url); if (response.getStatusCode() == 0) { json =
				 * mapper.writeValueAsString(response.getResponseContents()); employeeFamilyList
				 * = mapper.readValue(json, new TypeReference<List<EmployeeFamilyDetails>>() {
				 * }); log.info("family member Response list :::" + employeeFamilyList);
				 * RequestContext.getCurrentInstance().execute("PF('viewMember').show();"); }
				 * else { errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); } } }
				 */

			log.info("loadFamilyMembersView called..." + employeeLeaveTravelConcession);
			if (employeeLeaveTravelConcession.getFamilyDetailsIds() != null) {
				String familyIds = employeeLeaveTravelConcession.getFamilyDetailsIds();
				log.info("familyIdsList ids" + familyIds);
				List<String> familyList = Stream.of(familyIds.split("/")).collect(Collectors.toList());
				employeeFamilyList = new ArrayList<>();
				if (familyList != null) {
					for (String obj : familyList) {
						log.info("final lst of Family IDs ::::" + obj);
						String[] family = obj.split("-");
						EmployeeFamilyDetails familyObj = new EmployeeFamilyDetails();
						String name = family[0] != null ? family[0] : "";
						Long famId = Long.parseLong((family[1] != null ? family[1] : ""));
						Long age = Long.parseLong((family[2] != null ? family[2] : ""));
						familyObj.setDependentName(name);
						for (RelationshipMaster relation : relationshipMasterList) {
							if (famId.equals(relation.getId())) {
								familyObj.setRelationshipMaster(relation);
								break;
							}
						}
						familyObj.setAge(age);
						employeeFamilyList.add(familyObj);
					}
					if (action.equalsIgnoreCase("view") || action.equalsIgnoreCase("billsAdd"))
						RequestContext.getCurrentInstance().execute("PF('viewMember').show();");
				}
			}

		} catch (Exception e) {
			log.info("error found in loading family members", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
	}

	public void loadFamilyMembers() {
		log.info("<=======Starts EmployeeLTCBean.loadFamilyMembers ======>");
		try {
			BaseDTO response = httpService.get(
					AppUtil.getPortalServerURL() + "/employee/insurance/getfamilydetails/" + employeeMaster.getId());
			if (response.getStatusCode() == 0) {
				json = mapper.writeValueAsString(response.getResponseContents());
				employeeFamilyList = mapper.readValue(json, new TypeReference<List<EmployeeFamilyDetails>>() {
				});
				RequestContext.getCurrentInstance().execute("PF('addMember').show();");
			} else if (employeeFamilyList == null || employeeFamilyList.isEmpty()) {
				errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in loadingFamilyMembers ::", e);
		}
		log.info("<=======Ends EmployeeLTCBean.loadFamilyMembers ======>" + employeeFamilyList.size());
	}

	public void loadLtcValues() {
		loadMonth();
		// loadLeaveTypeMaster();
		getByEmployeeId();
		loadLazyEmployeeLeaveTravelConcessionList();
	}

	@Getter
	@Setter
	LeaveTravelConcessionDto ltcDetails;
	@Setter
	@Getter
	private List<String> fileList = new ArrayList<>();

	public EmployeeLeaveTravelConcession getById(Long id) {

		employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
		BaseDTO baseDto = new BaseDTO();
		try {
			if (id != null) {
				url = LTC_URL + "/getLtc/id/" + id;
				log.info("LTC Requested URL :::::" + url);
				baseDto = httpService.get(url);
				if (baseDto != null) {
					json = mapper.writeValueAsString(baseDto.getResponseContent());
					employeeLeaveTravelConcession = mapper.readValue(json, EmployeeLeaveTravelConcession.class);
				}
				holidayList = employeeLeaveTravelConcession.getLtcHolidayList();
			}
		} catch (Exception e) {
			log.error("exception occured in getById", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return employeeLeaveTravelConcession;
	}

	/*
	 * public void onchangeAppliedFor() { log.info("onchangeAppliedFor called ..: "
	 * + appliedFor); addMember = false;
	 * 
	 * if (appliedFor.equalsIgnoreCase("Self")) { addMember = true;
	 * loadFamilyRelationshipFromRelationshipMaster(); } }
	 */

	public void onchangeAppliedForMehtod() {
		log.info("onchangeAppliedFor called ..: " + appliedFor);
		addMember = false;
		if (appliedFor.equalsIgnoreCase("Family")) {
			addMember = true;
			loadFamilyRelationshipFromRelationshipMaster();
		} else {
			if (employeeFamilyList != null) {
				employeeFamilyList.clear();
				employeeFamily = new EmployeeFamilyDetails();
				selectedRelation = new RelationshipMaster();
			}

		}
	}

	public void loadFamilyRelationshipFromRelationshipMaster() {
		log.info("addMember" + " " + addMember);

		try {

			String url = AppUtil.getPortalServerURL() + "/employeeprofile/getAllFamilyRelationShip";
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				relationshipMasterList = mapper.readValue(jsonValue, new TypeReference<List<RelationshipMaster>>() {
				});

				log.info("relationshipMasterList list size is " + relationshipMasterList.size());
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void FilterTheWeekEndsAndGovernmentHolidayFromEmployeeLeaveDates() {

		Calendar startCal = Calendar.getInstance();
		Date startDate = empLTCHolidaysDate.getFromDate();
		Date EndDate = empLTCHolidaysDate.getToDate();
		LocalDate dateStart = convertToLocalDateViaInstant(startDate);
		LocalDate dateEnd = convertToLocalDateViaInstant(EndDate);
		log.info("the dates in between");
		Log.info("the Start date is " + dateStart.toString());
		Log.info("the Start date is " + dateStart.getDayOfWeek().toString());
		Log.info("the End  date is " + EndDate.toString());

		String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(startDate);

		log.info("name of day is" + " " + dayOfWeek.toString());

		Long NumberOfDays = getDifferenceDays(startDate, EndDate);

		log.info("the number Days between the selected dates is" + " " + NumberOfDays);
		// startCal.setTime();

		int count = 0;
		for (LocalDate date = dateStart; date.isBefore(dateEnd); date = date.plusDays(1)) {
			log.info("the date" + date.getDayOfWeek());
			count = count + 1;
		}
		log.info("the number of day is " + " " + count);

	}

	public static long getDifferenceDays(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public void addFamilMember() {
		log.info("inside addFamilMember member ");
		try {
			String relationName = selectedRelation.getName();
			if (dependantName == null || dependantName.trim().isEmpty()) {
				Util.addWarn("Name is Required");
				return;
			}
			if (age == null) {
				Util.addWarn("Age is Required");
				return;
			}

			if (age == 0) {
				Util.addWarn(" Enter age is greater than zero");
				return;
			}
			if (relationName == null) {
				Util.addWarn("Relationship is Required");
				return;
			}
			employeeFamily.setDependentName(dependantName);
			employeeFamily.setAge(age);
			validateFamilMember();
			String listRelationName;

			log.info("first Condition" + " " + (!(relationName.equals("Daughter") || relationName.equals("Son"))));
			log.info("second Condition" + " " + (!(relationName.equals("Daughter") || relationName.equals("Son"))));

			if ((employeeFamilyList.size() > 0) && (!(relationName.equals("Daughter") || relationName.equals("Son")))) {
				log.info("relation name is " + " " + selectedRelation.getName());
				for (EmployeeFamilyDetails list : employeeFamilyList) {
					listRelationName = list.getRelationshipMaster().getName();

					if (listRelationName.equals(relationName)) {
						throw new UIException("The relation is already Exist");
					}
				}
				EmployeeFamilyDetails employeeFamileys = new EmployeeFamilyDetails();
				employeeFamileys.setId(employeeFamily.getId());
				employeeFamileys.setDependentName(employeeFamily.getDependentName());
				employeeFamileys.setAge(employeeFamily.getAge());
				employeeFamileys.setRelationshipMaster(selectedRelation);
				employeeFamilyList.add(employeeFamileys);

				log.info("size of employeeFamilyList is" + " " + employeeFamilyList.size());

			} else {
				EmployeeFamilyDetails employeeFamiley = new EmployeeFamilyDetails();
				employeeFamiley.setId(employeeFamily.getId());
				employeeFamiley.setDependentName(employeeFamily.getDependentName());
				employeeFamiley.setAge(employeeFamily.getAge());
				employeeFamiley.setRelationshipMaster(selectedRelation);
				employeeFamilyList.add(employeeFamiley);

				log.info("size of employeeFamilyList is" + " " + employeeFamilyList.size());
			}

		} catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
			if (ex.getMessage().contains("The Combination SectionAndDeSignation ByAppFeatureId is Aleready Exist")) {
				showClientError(ex.getMessage());
			}
		}
		dependantName = "";
		age = null;
		selectedRelation = new RelationshipMaster();

	}

	public void removeItem(EmployeeFamilyDetails item) {

		if (item != null) {
			employeeFamilyList.remove(item);
		}

	}

	public void addHoidaysInHolidaysTableOfLTc() {
		log.info("inside addHoidaysInHolidaysTableOfLTc ");

		try {

			if (empLtcHolidayDates.getHolidayDate() == null) {
				throw new UIException("please select the date");
			}

			Date selectedDate = empLtcHolidayDates.getHolidayDate();
			String NameOfSelectedtDate = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(selectedDate);

			Boolean selectedDateIsHoliday = CheckSelectedDateIsGovHoliday(empLtcHolidayDates.getHolidayDate());
			log.info("selected day is goverment holiday " + " " + selectedDateIsHoliday);
			if (!(selectedDateIsHoliday)) {
				if (NameOfSelectedtDate.equalsIgnoreCase("saturday")
						|| NameOfSelectedtDate.equalsIgnoreCase("sunday")) {
					if (empLtcHolidayDatesList != null) {
						for (EmpLtcHolidayDates listDate : empLtcHolidayDatesList) {
							log.info("the First Condition" + " " + selectedDate.equals(listDate.getHolidayDate()));

							if (selectedDate.equals(listDate.getHolidayDate())) {
								throw new UIException("The Date  is already Exist");
							}
						}
						EmpLtcHolidayDates empLtcHolidayobject = new EmpLtcHolidayDates();
						empLtcHolidayobject.setHolidayDate(selectedDate);
						empLtcHolidayDatesList.add(empLtcHolidayobject);

						log.info("size of empLtcHolidayDatesList is" + " " + empLtcHolidayDatesList.size());

					} else {
						EmpLtcHolidayDates empLtcHolidayobjects = new EmpLtcHolidayDates();
						empLtcHolidayobjects.setHolidayDate(selectedDate);
						empLtcHolidayDatesList.add(empLtcHolidayobjects);
						log.info("size of empLTCHolidaysDateList is" + " " + empLTCHolidaysDateList.size());
					}
				} else {
					throw new UIException("Selected Date is not Sunday or satarday");
				}
			} else {
				if (!(checkTheSelectedDateDupplicationInempLtcHolidayDatesList(selectedDate))) {

					EmpLtcHolidayDates empLtcHolidayobjects = new EmpLtcHolidayDates();
					empLtcHolidayobjects.setHolidayDate(selectedDate);
					empLtcHolidayDatesList.add(empLtcHolidayobjects);
					log.info("size of empLTCHolidaysDateList is" + " " + empLTCHolidaysDateList.size());
				} else {
					throw new UIException("The Date  is already Exist");
				}
			}

		} catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
			if (ex.getMessage().contains("The Combination SectionAndDeSignation ByAppFeatureId is Aleready Exist")) {
				showClientError(ex.getMessage());
			}
		}

	}

	public Boolean checkTheSelectedDateDupplicationInempLtcHolidayDatesList(Date selectedDate) throws UIException {

		Boolean duplication = false;

		if (empLtcHolidayDatesList != null) {
			for (EmpLtcHolidayDates listDate : empLtcHolidayDatesList) {
				log.info("the First Condition" + " " + selectedDate.equals(listDate.getHolidayDate()));

				if (selectedDate.equals(listDate.getHolidayDate())) {
					duplication = true;
					throw new UIException("The Date  is already Exist");

				}
			}

			log.info("duplication" + " " + duplication);

		}

		return duplication;

	}

	public Boolean CheckSelectedDateIsGovHoliday(Date selectedDate) {
		Boolean isHoliday = false;
		try {

			EmpLtcHolidayDates holidayobject = new EmpLtcHolidayDates();
			if (selectedDate != null) {
				holidayobject.setHolidayDate(selectedDate);
			}

			BaseDTO baseDTO = null;
			String url = AppUtil.getPortalServerURL() + "/employeeprofile/getHolidayListInEmployeeLeaveDates";
			baseDTO = httpService.post(url, holidayobject);

			if (baseDTO != null) {
				if (baseDTO.getTotalRecords() > 0) {
					isHoliday = true;
				}

				log.info("count  is " + baseDTO.getTotalRecords());
				log.info("condition  is " + isHoliday);

			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

		return isHoliday;
	}

	public void removeDataItem(EmpLtcHolidayDates item) {

		if (item != null) {
			empLtcHolidayDatesList.remove(item);
		}

	}

	private void showClientError(String errorMessage) {

		AppUtil.addError(errorMessage);
	}

	public void validateFamilMember() throws UIException {

		log.info("the family details" + employeeFamily);
		log.info("the relation ship is" + " " + selectedRelation);

		if (employeeFamily == null || selectedRelation == null) {
			throw new UIException("Invalid input");
		} else if (employeeFamily.getDependentName() == null) {
			throw new UIException("Please Entere The Name");
		} else if (selectedRelation == null) {
			throw new UIException("Please Select Reletio");
		} else if (employeeFamily.getAge() == null) {
			throw new UIException("Please Enter The Age");
		}
	}

	public void onchangeHolidayPermission() {
		log.info("onchangeAppliedFor called ..: " + isPermission);
		holidayPermission = false;
		if (!isPermission) {
			holidayPermission = true;
		}

		log.info("holidayPermission" + " " + holidayPermission);
		/*
		 * else { employeeLeaveTravelConcession.setHolidayPermission(isPermission); if
		 * (employeeLeaveTravelConcession.getFromDate() != null) {
		 * employeeLeaveTravelConcession.setFromDate(employeeLeaveTravelConcession.
		 * getFromDate()); } if (employeeLeaveTravelConcession.getToDate() != null) {
		 * employeeLeaveTravelConcession.setToDate(employeeLeaveTravelConcession.
		 * getToDate()); }
		 * 
		 * }
		 */
	}

	/*
	 * public void loadLeaveTypeMaster() { BaseDTO baseDto = new BaseDTO();
	 * leaveTypeMasterList = new ArrayList<LeaveTypeMaster>(); url =
	 * AppUtil.getPortalServerURL() + "/leave/ltcleave";
	 * log.info("LTC Request Url :::::  " + url); try { baseDto =
	 * httpService.get(url); if (baseDto != null && baseDto.getResponseContent() !=
	 * null) { mapper = new ObjectMapper(); json =
	 * mapper.writeValueAsString(baseDto.getResponseContent());
	 * 
	 * leaveTypeMasterList = mapper.readValue(json, new
	 * TypeReference<List<LeaveTypeMaster>>() { });
	 * log.info("Total size of LTC leave type  " + leaveTypeMasterList.size()); } }
	 * catch (Exception e) { log.error("Exception in loadLeaveTypeMaster "+" "+
	 * e.getCause()); } }
	 */
	public void loadBlockYears() {
		BaseDTO baseDto = new BaseDTO();
		blockYearsList = new ArrayList<>();
		url = LTC_URL + "/getall/blockyears";
		log.info("LTC loadBlockYears Request Url :::::  " + url);
		try {
			baseDto = httpService.get(url);
			if (baseDto != null) {
				mapper = new ObjectMapper();
				json = mapper.writeValueAsString(baseDto.getResponseContents());
				List<String> listObj = mapper.readValue(json, new TypeReference<List<String>>() {
				});
				log.info("block type list ::: " + listObj);
				listObj.forEach(list -> {
					log.info(" String :: " + list);
					blockYearsList.add(list);
				});
				log.info("Total size of LTC BlockYears   " + blockYearsList.size());
			}
		} catch (Exception e) {
			log.error("Exception in loadBlockYears ", e);
		}
	}

	public String deleteLTCDetail() {
		try {

			if (selectedLtcDto == null && !action.equalsIgnoreCase("add") && !action.equalsIgnoreCase("cancel")) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else if (selectedLtcDto.getStatus() != null && selectedLtcDto.getStatus().equalsIgnoreCase("APPROVED")) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else if (selectedLtcDto.getStatus() != null && selectedLtcDto.getStatus().equalsIgnoreCase("REJECTED")) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			} else if (selectedLtcDto.getStatus() != null
					&& selectedLtcDto.getStatus().equalsIgnoreCase("FINALAPPROVED")) {
				AppUtil.addWarn("Record already final approved.");
				return null;
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
		}
		return null;

	}

	public String deleteLTCDetailConfirm() {
		try {
			log.info("Selected LTC Details  Id : : " + selectedLtcDto.getLeaveTravelId());
			url = LTC_URL + "/ltcDelete/" + selectedLtcDto.getLeaveTravelId();
			log.info(" LTC Details Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info(" LTC Details Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("  LTC Details Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.RECORD_DELETED_SUCCESSFULLY.getErrorCode());
				selectedLTCDetails = new EmployeeLeaveTravelConcession();
				return "/pages/leaveManagement/listLeaveTravelConcession.xhtml?faces-redirect=true;";
			} else {
				log.error(" ExamCentre Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String validateLTCRequest() {

		if (blockYears == null || blockYears.isEmpty()) {
			return null;
		} else {
			employeeLeaveTravelConcession.setBlockYears(blockYears);
		}
		if (appliedFor != null) {
			employeeLeaveTravelConcession.setAvailedFor(appliedFor);
		}
		if (travelMode != null) {
			employeeLeaveTravelConcession.setTravelMode(travelMode);
		}
		if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getLeaveTypeMaster() == null) {
			log.info("leavetype required");
			return null;
		}

		if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getLeaveTypeMaster() != null
				&& leaveDetail != null
				&& leaveDetail.getLeaveBalance() < employeeLeaveTravelConcession.getHolidayNoOfDays()) {
			log.info("No Of days cannot be greater than leave balance");
			return null;
		}

		if (appliedFor.equalsIgnoreCase("Family") && selectedFamilyDetails == null
				|| selectedFamilyDetails.size() == 0) {
			log.info("Family rmember required..");
			return null;
		}

		if (isPermission && fromdateToDateList == null || fromdateToDateList.size() == 0) {
			log.info("HolidayDates required");
			return null;
		}
		return null;
	}

	public String createLTCRequest() {
		BaseDTO baseDto = new BaseDTO();

		log.info(":: Create LTCRequest start::");
		try {
			if (employeeLeaveTravelConcession.getEligibleDistance().equals(0D) || employeeLeaveTravelConcession
					.getEligibleDistance() < employeeLeaveTravelConcession.getTravelledDistance()) {
				Util.addWarn("You are not Eligible to Apply LTC,Insufficient KM Distance.");
				return null;
			}
			if (leaveDetail == null
					|| employeeLeaveTravelConcession.getHolidayNoOfDays() > leaveDetail.getLeaveBalance()) {
				Util.addWarn("You are not Eligible to Apply LTC,Insufficient Leave Balance");
				return null;
			}
			if (employeeMaster != null && employeeMaster.getUserId() != null) {
				employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
				employeeLeaveTravelConcession.setUserId(employeeMaster.getUserId());
				employeeLeaveTravelConcession.setAvailedFor(appliedFor);
			} else {
				return null;
			}

			url = LTC_URL + "/leaveTravelConcession";
			log.info("Create LTCRequest Mark URL :" + url);
			validateLTCRequest();
			employeeLeaveTravelConcession
					.setBlockYearFrom(Integer.valueOf(selectedBlocKYear.getStartYear().intValue()));
			employeeLeaveTravelConcession.setBlockYearTo(Integer.valueOf(selectedBlocKYear.getEndYear().intValue()));
			StringBuilder sb = new StringBuilder();
			sb.append(employeeLeaveTravelConcession.getBlockYearFrom());
			sb.append("-");
			sb.append(employeeLeaveTravelConcession.getBlockYearTo());
			employeeLeaveTravelConcession.setBlockYears(sb.toString());
			if (empLtcHolidayDatesList != null && !empLtcHolidayDatesList.isEmpty()) {
				log.info("empLtcHolidayDatesList" + empLtcHolidayDatesList);
				List<Date> holidaylist = new ArrayList<Date>();
				for (EmpLtcHolidayDates list : empLtcHolidayDatesList) {
					holidaylist.add(list.getHolidayDate());
				}
				employeeLeaveTravelConcession.setHolidayPermission(true);
				employeeLeaveTravelConcession.setHolidaylist(holidaylist);
				log.info("the holidays list we sending to data base is" + " " + holidaylist.size());
			} else {
				employeeLeaveTravelConcession.setHolidayPermission(false);
				employeeLeaveTravelConcession.setHolidaylist(new ArrayList<Date>());
			}
			employeeLeaveTravelConcession.setTravelMode(travelMode);

			if (!appliedFor.equals("Self")) {
				if (employeeFamilyList != null) {
					employeeLeaveTravelConcession.setLeaveAppliedFamily(true);
					employeeLeaveTravelConcession.setLeaveAppliedSelf(false);
					StringBuilder familyDatails = new StringBuilder();
					String name;
					String relationId;
					String age;
					String cobnination;
					for (EmployeeFamilyDetails family : employeeFamilyList) {
						name = family.getDependentName();
						relationId = family.getRelationshipMaster().getId().toString();
						age = family.getAge().toString();
						cobnination = name + "-" + relationId + "-" + age;

						log.info("cobnination" + cobnination);
						log.info("length is" + " " + familyDatails.length());
						log.info("length is" + " " + familyDatails);

						if (familyDatails.length() > 1) {
							familyDatails.append("/").append(cobnination);
						} else {
							familyDatails.append(cobnination);
						}
					}

					log.info("the familyDetails" + " " + familyDatails);
					employeeLeaveTravelConcession.setFamilyDetailsIds(familyDatails.toString());
				}
			} else {
				employeeLeaveTravelConcession.setLeaveAppliedSelf(true);
				employeeLeaveTravelConcession.setLeaveAppliedFamily(false);
			}
			baseDto = httpService.post(url, employeeLeaveTravelConcession);
			log.info("LTCRequest BaseDTO response:" + baseDto);
			if (baseDto == null) {
				log.info("::LTCRequest details response null. ::" + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (rejectCheckFlag != null) {
					if (appliedFor.equals("Self") && rejectCheckFlag) {
						employeeLeaveTravelConcession.setStatus("SUBMITTED");
					}
				}

				if (baseDto.getStatusCode() == 0) {
					log.info("::LTCRequest  created successfully. ::" + baseDto);
					AppUtil.addInfo("Leave Travel Concession Submitted Successfully");
					selectedLTCDetails = new EmployeeLeaveTravelConcession();
					return LIST_URL;
				} else if (baseDto != null
						&& baseDto.getStatusCode().equals(MastersErrorCode.REPORTING_MANAGER_NOT_AVAILABLE_TO_YOU)) {
					AppUtil.addWarn("Reporting Manager Not available for you");
					return null;
				} else if (baseDto.getStatusCode() == 2000) {
					AppUtil.addWarn("Reporting to is not available for you");
					return null;
				} else {
					log.info("::LTCRequest creation failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			// }
			/*
			 * else { log.error("validation failed while creating LTC Request.."); return
			 * null; }
			 */

		} catch (Exception e) {
			log.error(":: Exception in LTCRequest create :::::::::::", e);
			errorMap.notify(baseDto.getStatusCode());
		}
		return null;
	}

	public String loadLTCRequest() {
		try {
			if (pageAction.equalsIgnoreCase("ADD")) {

				return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";

			} else if (pageAction.equalsIgnoreCase("EDIT")) {

			} else if (pageAction.equalsIgnoreCase("VIEW")) {

			}

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Employee LTC Bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public EmployeeMaster getByEmployeeId() {
		try {
			UserMaster user = login.getUserDetailSession();
			mapper = new ObjectMapper();
			log.info("user ID ==>" + user.getId());
			if (user == null || user.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			url = AppUtil.getPortalServerURL() + "/employeeprofile/get/employee/" + user.getId();
			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null || baseDTO.getResponseContent() == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			json = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeMaster = mapper.readValue(json, EmployeeMaster.class);

			return employeeMaster;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public void onchangeBlockDate() {
		log.info("onchangeBlockDate is called : ");
		try {

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockYearFrom())) {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getBlockYearFrom());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			} else {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getBlockYearFrom());
				employeeLeaveTravelConcession.setBlockYearFrom(employeeLeaveTravelConcession.getBlockYearFrom());
			}

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockYearTo())) {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getBlockYearTo());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			} else {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getBlockYearTo());
				employeeLeaveTravelConcession.setBlockYearTo(employeeLeaveTravelConcession.getBlockYearTo());
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			String fromDate = dateFormat.format(employeeLeaveTravelConcession.getBlockYearFrom());
			String toDate = dateFormat.format(employeeLeaveTravelConcession.getBlockYearTo());

			log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

			Date firstDate = dateFormat.parse(fromDate);
			Date secondDate = dateFormat.parse(toDate);

			long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
			long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

			if (firstDate.compareTo(secondDate) == 0) {
				log.info("Number of days for leave is : " + 1);
				employeeLeaveTravelConcession.setBlockNumberOfDays(1.0);
				;
			} else {
				log.info("Number of days for leave is : " + diff);
				employeeLeaveTravelConcession.setBlockNumberOfDays((double) diff);
			}
		} catch (ParseException e) {
			log.error("found error in block date onchange", e);
		}

		// make todate null if fromdate is greater than todate
		if (employeeLeaveTravelConcession.getBlockYearFrom() != null
				&& employeeLeaveTravelConcession.getBlockYearTo() != null && employeeLeaveTravelConcession
						.getBlockYearFrom() > (employeeLeaveTravelConcession.getBlockYearTo())) {
			employeeLeaveTravelConcession.setBlockYearTo(null);
			log.info("setBlockYearTo set as null");
		}

	}

	/*
	 * public void getDayDifference() { try {
	 * 
	 * Date startDate = studentTraining.getStartDate(); Date endtDate =
	 * studentTraining.getEndDate();
	 * 
	 * if (startDate != null || endtDate != null) { long difference =
	 * endtDate.getTime() - startDate.getTime(); Long daydifference = (difference /
	 * (1000 * 60 * 60 * 24) + 1); studentTraining.setNoOfDays(daydifference); } }
	 * catch (Exception e) {
	 * log.error(" Exception In EmployeeMasterBean employeeAgeCalculation Method ",
	 * e); errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * 
	 * } }
	 */

	public void onchangeLeaveDate() {
		log.info("onchangeBlockDate is called : ");
		try {

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockFromDate())) {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getBlockFromDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			} else {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getBlockFromDate());
				// employeeLeaveTravelConcession.setBlockYearFrom(employeeLeaveTravelConcession.getBlockFromDate());
			}

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockToDate())) {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getBlockToDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			} else {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getBlockToDate());
				// employeeLeaveTravelConcession.setBlockYearTo(employeeLeaveTravelConcession.getBlockToDate());
			}

			/*
			 * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd",
			 * Locale.ENGLISH); String fromDate =
			 * dateFormat.format(employeeLeaveTravelConcession.getBlockFromDate()); String
			 * toDate = dateFormat.format(employeeLeaveTravelConcession.getBlockToDate());
			 * 
			 * log.info("Leave request from Date is : {} and to date is : {} ", fromDate,
			 * toDate);
			 * 
			 * Date firstDate = dateFormat.parse(fromDate); Date secondDate =
			 * dateFormat.parse(toDate);
			 */

			/*
			 * long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
			 * long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			 */

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			String fromDate = dateFormat.format(employeeLeaveTravelConcession.getBlockFromDate());
			String toDate = dateFormat.format(employeeLeaveTravelConcession.getBlockToDate());

			log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

			Date firstDate = dateFormat.parse(fromDate);
			Date secondDate = dateFormat.parse(toDate);

			long diffInMillies = (secondDate.getTime() - firstDate.getTime());
			// long diff=AppUtil.calculateWorkDays(firstDate,secondDate);
			long diff = AppUtil.calculateWorkDayswithCurrentDate(firstDate, secondDate);
			if (diffInMillies <= 0) {
				employeeLeaveTravelConcession.setBlockToDate(null);
			}
			if (firstDate.compareTo(secondDate) == 0) {
				log.info("Number of days for leave is : " + 1);
				employeeLeaveTravelConcession.setHolidayNoOfDays(1);
			} else {
				log.info("Number of days for leave is : " + diff);
				employeeLeaveTravelConcession.setHolidayNoOfDays((int) diff);
			}

//			if (employeeLeaveTravelConcession.getBlockFromDate() != null
//					&& employeeLeaveTravelConcession.getBlockToDate() != null) {
//				long difference = employeeLeaveTravelConcession.getBlockToDate().getTime()
//						- employeeLeaveTravelConcession.getBlockFromDate().getTime();
//				Long daydifference = (difference / (1000 * 60 * 60 * 24) + 1);
//				employeeLeaveTravelConcession.setHolidayNoOfDays(Integer.parseInt(daydifference.toString()));
//			}
			/*
			 * if (firstDate.compareTo(secondDate) == 0) {
			 * log.info("Number of days for leave is : " + 1);
			 * employeeLeaveTravelConcession.setHolidayNoOfDays(1); ; } else {
			 * log.info("Number of days for leave is : " + diff);
			 * employeeLeaveTravelConcession.setHolidayNoOfDays((int) diff); }
			 */
		} catch (Exception e) {
			log.error("found error in block date onchange", e);
		}

		if (employeeLeaveTravelConcession.getBlockToDate() != null
				&& employeeLeaveTravelConcession.getBlockFromDate() != null && employeeLeaveTravelConcession
						.getBlockFromDate().after(employeeLeaveTravelConcession.getBlockToDate())) {
			employeeLeaveTravelConcession.setBlockToDate(null);
			log.info("todate set as null");
		}

	}

	public void addHolidaysDate() {

		fromdateToDateList = new ArrayList<>();
		log.info("list of holidays date ::::::" + fromdateToDateList);

		if (isPermission) {

			if (employeeLeaveTravelConcession.getFromDate() != null
					&& employeeLeaveTravelConcession.getToDate() != null) {

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy ");

				String fromDate = dateFormat.format(employeeLeaveTravelConcession.getFromDate());

				String toDate = dateFormat.format(employeeLeaveTravelConcession.getToDate());

				String seperatedByDash = Joiner.on("To").join(fromDate, toDate);

				fromdateToDateList.add(seperatedByDash);
			}

			log.info("list of holidays date after add::::::" + fromdateToDateList);
		}

		employeeLeaveTravelConcession.setFromDate(null);
		employeeLeaveTravelConcession.setToDate(null);
	}

	public String clear() {
		log.info("clear methos called:::");
		addButton = false;
		editButton = false;
		billButton = false;
		deleteButton = false;
		selectedLTCDId = new LeaveTravelConcessionDto();
		selectedLTCDetails = new EmployeeLeaveTravelConcession();
		selectedLtcDto = new LeaveTravelConcessionDto();
		return "/pages/leaveManagement/listLeaveTravelConcession.xhtml?faces-redirect=true";
	}

	public void holidayDateChange() {
		if (employeeLeaveTravelConcession.getFromDate() != null && employeeLeaveTravelConcession.getToDate() != null
				&& employeeLeaveTravelConcession.getFromDate().after(employeeLeaveTravelConcession.getToDate())) {
			employeeLeaveTravelConcession.setToDate(null);
		}
	}

	public void loadMonth() {
		try {

			Calendar cal = Calendar.getInstance();

			// get the value of all the calendar date fields.
			log.info("Calendar's Year: " + cal.get(Calendar.YEAR));

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Employee LTC Bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onRowSelect(SelectEvent event) {

		addButton = true;
		selectedLtcDto = ((LeaveTravelConcessionDto) event.getObject());
		log.info("<=======Starts  EmployeeLTCB" + selectedLtcDto.getStatus());

		if (selectedLtcDto != null && selectedLtcDto.getStatus() != null
				&& selectedLtcDto.getStatus().equalsIgnoreCase("REJECTED")) {
			editButton = false;
			deleteButton = false;
		}
		if (selectedLtcDto != null && selectedLtcDto.getStatus() != null
				&& (selectedLtcDto.getStatus().equalsIgnoreCase("FINALAPPROVED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("APPROVED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("BILL_APPROVED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("BILL_SUBMITTED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("BILL_FINALAPPROVED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("BILL_REJECTED"))) {
			billButton = true;
			editButton = true;
			deleteButton = true;
		}
		if (selectedLtcDto != null && selectedLtcDto.getStatus() != null
				&& (selectedLtcDto.getStatus().equalsIgnoreCase("REJECTED")
						|| selectedLtcDto.getStatus().equalsIgnoreCase("BILL_REJECTED"))) {
			rejectCheckFlag = true;
		}
	}

	public String leaveTravelConcessionAction() {
		log.info("<=======Starts  EmployeeLTCBean.leaveTravelConcessionAction ======>");
		memberIds = new ArrayList<>();
		if (pageAction.equalsIgnoreCase("Create")) {
			employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			selectedFamilyDetails = new ArrayList<>();
			fromdateToDateList.clear();
			appliedFor = "Self";
			isPermission = false;
			return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";
		}
		log.info("<=======Ends  EmployeeLTCBean.leaveTravelConcessionAction ======>");
		return null;
	}

	/*
	 * public void loadFamilyMembers() {
	 * log.info("<=======Starts  EmployeeLTCBean.loadFamilyMembers ======>"); try {
	 * BaseDTO response = httpService.get(SERVER_URL +
	 * "/employee/insurance/getfamilydetails/" +employeeMaster.getId()); if
	 * (response.getStatusCode() == 0) { json =
	 * mapper.writeValueAsString(response.getResponseContents()); employeeFamilyList
	 * = mapper.readValue(json, new TypeReference<List<EmployeeFamilyDetails>>()
	 * {}); RequestContext.getCurrentInstance().execute("PF('addMember').show();");
	 * }else { errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); }
	 * }catch(Exception e) {
	 * log.error("Exception occured in loadingFamilyMembers ::",e); }
	 * log.info("<=======Ends  EmployeeLTCBean.loadFamilyMembers ======>"
	 * +employeeFamilyList.size()); }
	 */

	public void addOrRemoveFamilyMembers(boolean flag, Long id) {
		log.info("<=======Stars  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
		log.info("Flag is ::" + flag);
		if (flag) {
			memberIds.add(id.toString());
		} else {
			memberIds.remove(id.toString());
		}
		log.info("<=======Ends  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
	}

	public void canceLoadFamilyMembers() {
		memberIds = new ArrayList<>();
		RequestContext.getCurrentInstance().execute("PF('addMember').hide();");
	}

	public void addFamilyMembers() {
		RequestContext.getCurrentInstance().execute("PF('addMember').hide();");
		log.info("selectedFamilyDetails size ::" + selectedFamilyDetails);
		memberIds = new ArrayList<>();
		for (EmployeeFamilyDetails emp : selectedFamilyDetails) {
			memberIds.add(emp.getId().toString());
		}
		log.info("<=======Ends  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
	}

	public void fileUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");

				return;
			}
			uploadedFile = event.getFile();
			String fileType = uploadedFile.getFileName();

//			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
//					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")) {
//
//				AppUtil.addWarn("Please Upload The File Which is mention below");
//				log.info("<<====  error type in file upload");
//				return;
//			}

			if (fileType.contains(".pdf") || fileType.contains(".jpg") || fileType.contains(".png")
					|| fileType.contains(".jpeg") || fileType.contains(".docx") || fileType.contains(".doc")) {

			} else {
				AppUtil.addWarn("Please Upload The File Which is mention below");
				log.info("<<====  error type in file upload");
				return;
			}

			long size = uploadedFile.getSize();
			log.info("Size of document is" + " " + documentSize);
			log.info("priorityUpload size==>" + size);
			if ((size / (1024 * 1024)) > 2) {
				AppUtil.addWarn("File size is exced 2Mb ");
				log.info("<<====  Large size file uploaded");
				return;
			}

			RequestContext.getCurrentInstance().execute("PF('ticketupload').hide();");

			String PathName = commonDataService.getAppKeyValue("LEAVE_TRAVEL_CONCESSION");
			String filePath = Util.fileUpload(PathName, uploadedFile);
			employeeLeaveTravelConcession.setDocumentPath(filePath);

			fileName = uploadedFile.getFileName();

			AppUtil.addInfo("File upload SuccessFully");
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void onLeaveTypeChange() {
		EmployeeMaster employeeMaster = loginBean.getEmployee();
		log.info(" On leave type change ");
		log.info(" employee Id  " + " " + employeeMasterBasedOnSectionId.getId());
		log.info(" employee Id  " + employeeMaster.getId());
		log.info(" employee Id  " + employeeLeaveTravelConcession.getLeaveTypeMaster().getId());

		Long employeeMasterId = null;
		try {

			if (leaveTypeMaster != null) {
				String resource = SERVER_URL + "/leaverequest/leave/" + employeeMaster.getId() + "/"
						+ employeeLeaveTravelConcession.getLeaveTypeMaster().getId();
				BaseDTO baseDto = httpService.get(resource);
				json = mapper.writeValueAsString(baseDto.getResponseContent());
				leaveDetail = mapper.readValue(json, LeaveRequest.class);
				log.info(" Employee consumed leave is :" + leaveDetail);
				if (leaveDetail.getLeaveBalanceDouble() == null || leaveDetail.getLeaveBalanceDouble() < 0) {
					leaveDetail.setLeaveBalance(0D);
				} else {
					leaveDetail.setLeaveBalance(leaveDetail.getLeaveBalanceDouble());
				}
				log.info(" Employee consumed leave is :" + leaveDetail);
			}
			/*
			 * setFromDetails(); log.info(" employee Id  " + " " +
			 * employeeMasterBasedOnSectionId.getId()); log.info(" employee Id  " + " " +
			 * employeeMasterBasedOnEntityId.getId());
			 * if(employeeMasterBasedOnSectionId!=null) {
			 * employeeMasterId=employeeMasterBasedOnSectionId.getId(); }else
			 * if(employeeMasterBasedOnEntityId!=null) {
			 * employeeMasterId=employeeMasterBasedOnEntityId.getId(); } if (leaveTypeMaster
			 * != null && employeeMasterId!=null) { String resource = SERVER_URL +
			 * "/leaverequest/leave/" + employeeMasterId + "/" + leaveTypeMaster.getId();
			 * BaseDTO baseDto = httpService.get(resource); json =
			 * mapper.writeValueAsString(baseDto.getResponseContent()); leaveDetail =
			 * mapper.readValue(json, LeaveRequest.class);
			 * if(leaveDetail.getLeaveBalanceDouble() == null ||
			 * leaveDetail.getLeaveBalanceDouble() < 0) { leaveDetail.setLeaveBalance(0D);
			 * }else { leaveDetail.setLeaveBalance(leaveDetail.getLeaveBalanceDouble()); }
			 * log.info(" Employee consumed leave is :" + leaveDetail); }
			 * 
			 */ } catch (Exception e) {
			log.error(":: Exception while On leave type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void saveLTCForm() {

		try {

			log.info("Save process is started");

			if (slectedEntity != null) {
				leaveTravelConcessionDto.setEntityMaster(slectedEntity);
			} else {
				log.info("the entityMaster is null");
			}

			log.info("departementFlag" + departementFlag);

			if (departementFlag) {

				if (selectedSection.getId() != null) {
					leaveTravelConcessionDto.setSectionMaster(selectedSection);
				} else {
					log.info("sectionMaster is null");
				}

				if (employeeMasterBasedOnSectionId.getId() != null) {
					log.info("the selcete empMaster is" + " " + employeeMasterBasedOnSectionId);
					// employeeLeaveTravelConcession.setEmployeeMaster(employeeMasterBasedOnSectionId);
					leaveTravelConcessionDto.setEmployeeMaster(employeeMasterBasedOnSectionId);
				} else {
					log.info("employeeMasterBasedOnSectionId is null");
				}

			} else {
				log.info("departementFlag" + departementFlag);

				if (employeeMasterBasedOnEntityId.getId() != null) {
					log.info("the selcete empMaster is" + " " + employeeMasterBasedOnEntityId);
					// employeeLeaveTravelConcession.setEmployeeMaster(employeeMasterBasedOnEntityId);
					leaveTravelConcessionDto.setEmployeeMaster(employeeMasterBasedOnEntityId);
				}
			}

			if (employeeLeaveTravelConcession.getEligibleDistance() != null) {

			}

			if (selectedBlocKYear != null) {
				leaveTravelConcessionDto.setBlockFromYear(selectedBlocKYear.getStartYear());
				leaveTravelConcessionDto.setBlockToYear(selectedBlocKYear.getEndYear());
			}

			log.info("employeeLeaveTravelConcession From date " + " "
					+ employeeLeaveTravelConcession.getBlockFromDate());
			log.info("employeeLeaveTravelConcession to date " + " " + employeeLeaveTravelConcession.getBlockToDate());
			log.info("employeeLeaveTravelConcession Total number of day leave applied " + " "
					+ employeeLeaveTravelConcession.getNoOfDays());
			log.info("the travelled distance is" + " " + employeeLeaveTravelConcession.getTravelledDistance());

			if (addMember) {

				if (employeeFamilyList != null) {
					employeeLeaveTravelConcession.setLeaveAppliedFamily(true);
					StringBuilder familyDatails = new StringBuilder();
					String name;
					String relationId;
					String age;
					String cobnination;
					for (EmployeeFamilyDetails family : employeeFamilyList) {
						name = family.getDependentName();
						relationId = family.getRelationshipMaster().getId().toString();
						age = family.getAge().toString();
						cobnination = name + "-" + relationId + "-" + age;

						log.info("cobnination" + cobnination);
						log.info("length is" + " " + familyDatails.length());
						log.info("length is" + " " + familyDatails);

						if (familyDatails.length() > 1) {
							familyDatails.append("/").append(cobnination);
						} else {
							familyDatails.append(cobnination);
						}
					}

					log.info("the familyDetails" + " " + familyDatails);
					employeeLeaveTravelConcession.setFamilyDetailsIds(familyDatails.toString());
				}
			} else {
				employeeLeaveTravelConcession.setLeaveAppliedSelf(true);
			}

			if (travelMode != null) {
				log.info("the travel mode is" + travelMode);
				employeeLeaveTravelConcession.setTravelMode(travelMode);
			}

			if (leaveTypeMaster != null) {
				employeeLeaveTravelConcession.setLeaveTypeMaster(leaveTypeMaster);
			}

			if (employeeLeaveTravelConcession.getHolidayFromDate() != null) {
				log.info("the date is" + " " + employeeLeaveTravelConcession.getHolidayFromDate());
			} else {
				log.info("the holiday date from is null");
			}

			if (employeeLeaveTravelConcession.getHolidayToDate() != null) {
				log.info("the to date is" + employeeLeaveTravelConcession.getHolidayToDate());
			} else {
				log.info("to date is null");
			}

			if (employeeLeaveTravelConcession.getHolidayNoOfDays() != null) {
				log.info("number of holidays is" + " " + employeeLeaveTravelConcession.getHolidayNoOfDays());

			}

			if (employeeLeaveTravelConcession.getAdvanceAmount() != null) {
				log.info(" the cost of ticket" + " " + employeeLeaveTravelConcession.getAdvanceAmount());
			}
			log.info("isPermission" + isPermission);

			if (empLtcHolidayDatesList != null) {
				log.info("empLtcHolidayDatesList" + empLtcHolidayDatesList);
				List<Date> holidaylist = new ArrayList<Date>();
				for (EmpLtcHolidayDates list : empLtcHolidayDatesList) {
					holidaylist.add(list.getHolidayDate());
				}
				employeeLeaveTravelConcession.setHolidayPermission(true);
				leaveTravelConcessionDto.setHolidayList(holidaylist);

				// leaveTravelConcessionDto.getEmpLtcHolidayDatesList().addAll(empLtcHolidayDatesList);
				log.info("the holidays list we sending to data base is" + " " + holidaylist.size());
			} else {
				employeeLeaveTravelConcession.setHolidayPermission(false);
			}

			if (userMaster != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			}

			if (employeeLeaveTravelConcession.getDocumentPath() != null) {
				log.info("pathfile " + " " + employeeLeaveTravelConcession.getDocumentPath());
			}

			if (leaveTravelConcessionDto.getForwardTo() != null) {
				log.info("the forwoard to is" + leaveTravelConcessionDto.getForwardTo());
			}

			if (leaveTravelConcessionDto.getForwardFor() != null) {
				log.info("the forwoard for is" + leaveTravelConcessionDto.getForwardFor());
			}

			if (leaveTravelConcessionDto.getNote() != null) {
				log.info("the note is" + leaveTravelConcessionDto.getNote());
			}

			leaveTravelConcessionDto.setEmployeeLeaveTravelConcession(employeeLeaveTravelConcession);
			log.info("the leaveTravelConcessionDto we submiting to data base is" + " " + leaveTravelConcessionDto);

			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/employeeprofile/saveEmployeTravelConcession";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("FileMovementConfigListBYAppFeatureId deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.info("the cause of error is" + " " + e.getCause());
			log.info("the cause of error message is" + " " + e.getMessage());
		}

	}

	public String listPage() {

		loadLazyEmployeeLeaveTravelConcessionList();
		return LIST_URL;
	}

	public void loadLazyEmployeeLeaveTravelConcessionList() {
		// disableAddButton = false;
		log.info("<===== Lazy loading EMPLTC ======>");
		empLTClazyLoadReport = new LazyDataModel<LeaveTravelConcessionDto>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<LeaveTravelConcessionDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = SERVER_URL + "/leavetravelconcession/ltc/getTheListEmployeeLTCByLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get LeaveTravelConcessionDto list");
					log.info("LeaveTravelConcessionDto Total records" + response.getTotalRecords());

					log.info("the object returnend from LeaveTravelConcessionDto" + response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						listPageEmployeeLtclist = mapper.readValue(jsonResponse,
								new TypeReference<List<LeaveTravelConcessionDto>>() {
								});
						if (!listPageEmployeeLtclist.isEmpty()) {
							if (loginEmployee != null && loginEmployee.getEmpCode() != null) {
								ltcListForCurrentUser = listPageEmployeeLtclist.stream()
										.filter(i -> i.getEmpCodeName().equals(loginEmployee.getEmpCode()))
										.collect(Collectors.toList());
							}
						}
						Integer count = ltcListForCurrentUser != null ? ltcListForCurrentUser.size() : 0;
						this.setRowCount(count);
						if (!ltcListForCurrentUser.isEmpty()) {
							totalSize = ltcListForCurrentUser.size();
						}

					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyEmployeeLeaveTravelConcessionList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"
						+ listPageEmployeeLtclist.toString());
				log.info("Ends lazy load....");

				return ltcListForCurrentUser;
			}

			@Override
			public Object getRowKey(LeaveTravelConcessionDto res) {
				return res != null ? res.getLeaveTravelId() : null;
			}

			@Override
			public LeaveTravelConcessionDto getRowData(String rowKey) {
				try {
					for (LeaveTravelConcessionDto leaveTravelConcessionDto : ltcListForCurrentUser) {
						if (leaveTravelConcessionDto.getLeaveTravelId().equals(Long.valueOf(rowKey))) {
							selectedLtcDto = leaveTravelConcessionDto;
							return selectedLtcDto;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};

	}

	@Setter
	@Getter
	List<String> billsList;

	@Setter
	@Getter
	private StreamedContent file;

	@Setter
	@Getter
	List<String> ltcBillList;

	long letterSize;

	public void downloadIncomingFile(String downloadFile) {
		InputStream input = null;
		try {
			File files = new File(downloadFile);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	String downloadPath = "";

	public void downloadIncomingFileLTCBill(String downloadFile) {
		InputStream input = null;
		downloadPath = "";
		try {
			billsList.forEach(bills -> {
				if (bills != null && !bills.isEmpty()) {
					String[] parts = bills.split("/");
					String result = parts[parts.length - 1];
					if (result.equalsIgnoreCase(downloadFile)) {
						downloadPath = bills;
					}
				}
			});

			File files = new File(downloadPath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}
		return;
	}

	public void billsFileUpload(FileUploadEvent event) {
		log.info("<====== Incoming tapal Upload Event =====>" + event);
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = size / 1000;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("LTC_BILLS_FILE_SIZE"));
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String ltcPath = commonDataService.getAppKeyValue("LTC_BILLS_UPLOAD_PATH");

		log.info("ltcPath >>>> " + ltcPath);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStamp = Long.toString(timestamp.getTime());
		String fileDetail = Util.uploadDocument(ltcPath, uploadedFile, timeStamp);
		log.info("fileName with path...." + fileDetail);
		if (fileDetail.equalsIgnoreCase("Error")) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("Same file already exist on target path");
		} else
			employeeLeaveTravelConcession.setDocumentPath(fileDetail);

		String name = uploadedFile.getFileName();
		setLtcBillsDocument(ltcBillList.size() - 1, fileDetail);
		ltcBillList.set(ltcBillList.size() - 1, name);
		fileList.set(ltcBillList.size() - 1, name);
		log.info("file list size....." + ltcBillList.size());
		RequestContext.getCurrentInstance().execute("PF('pridocupload').hide();");
		errorMap.notify(
				ErrorDescription.getError(AdminErrorCode.LTC_BILSS_DOCUMENT_UPLOADED_SUCCESSFULLY).getErrorCode());
		log.info(" billsFileUpload  is uploaded successfully");
	}

	public void setLtcBillsDocument(int index, String val) {
		billsList.add(index, val);
	}

	public void addRow() {
		String s = new String();
		ltcBillList.add(s);
	}

	public void deleteRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		if (rowIndex != 0) {
			if (ltcBillList.size() > rowIndex)
				ltcBillList.remove(rowIndex);

			if (billsList.size() > rowIndex)
				billsList.remove(rowIndex);
		}
		if (rowIndex == 0) {
			if (ltcBillList.size() > 1)
				ltcBillList.remove(rowIndex);

			if (billsList.size() > 1)
				billsList.remove(rowIndex);
		}
	}

	@Getter
	@Setter
	Boolean billsubmitFlag = false;
	@Getter
	@Setter
	UserMaster userMaster = new UserMaster();
	@Getter
	@Setter
	String stage;

	@Getter
	@Setter
	LeaveTravelConcessionDto selectedLTCDId;

	public String saveBills() {
		log.info("<---- Start LTCBean bills save : ----->");
		final String LTC_URL_SAVE = AppUtil.getPortalServerURL() + "/leavetravelconcession/ltc";
		BaseDTO baseDTO = null;
		try {
			if (userMaster != null && userMaster.getId() != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			} else {
				Util.addWarn("Forward To should not be empty");
				return null;
			}
			leaveTravelConcessionDto.setFileList(getBillsList());
			leaveTravelConcessionDto.setLeaveTravelId(selectedLtcDto.getLeaveTravelId());

			String url = LTC_URL_SAVE + "/saveBills";
			log.info("LTC id----------" + employeeLeaveTravelConcession.getId());
			log.info("LTC DTO----------" + leaveTravelConcessionDto);
			if (leaveTravelConcessionDto.getFileList().size() == 1) {
				Util.addWarn("Add Leave Bills Documents ");
				return null;
			}
			baseDTO = httpService.post(url, leaveTravelConcessionDto);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_BILSS_DOCUMENT_SAVED).getErrorCode());
				log.info("Successfully Approved-----------------");
				clear();
				loadLazyEmployeeLeaveTravelConcessionList();
			}
		} catch (Exception e) {
			log.error("save bill exception ==> ", e);
		}
		return LIST_URL;
	}

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@PostConstruct
	public String showNotificationViewPage() {
		log.info("Leave RequestBean showNotificationViewPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedLTCDId = new LeaveTravelConcessionDto();
			action = "View";
			selectedLTCDId.setLeaveTravelId(Long.parseLong(id));
			systemNotificationId = Long.parseLong(notificationId);
			try {
				showPage();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		log.info("Leave RequestBean showNotificationViewPage() Ends");
		return null;
	}

	public EmployeeLeaveTravelConcession getByAddId(Long id) {
		// submitButton = false;
		ltcDetails = new LeaveTravelConcessionDto();
		BaseDTO baseDto = new BaseDTO();
		try {
			url = AppUtil.getPortalServerURL() + "/leavetravelconcession/ltc" + "/getLtcwithnotificationId/" + id + "/"
					+ (systemNotificationId != null ? systemNotificationId : 0);
			log.info("LTC Requested URL :::::" + url);
			baseDto = httpService.get(url);
			if (baseDto != null) {
				json = mapper.writeValueAsString(baseDto.getResponseContent());
				ltcDetails = mapper.readValue(json, LeaveTravelConcessionDto.class);
			}

			if (ltcDetails != null) {
				ltcBillList = new ArrayList<>();
				billsList = new ArrayList<>();
				log.info("employeeLeaveTravelConcession is.." + ltcDetails);
				employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
				employeeLeaveTravelConcession = ltcDetails.getEmployeeLeaveTravelConcession();
				ltcDetails.getEmployeeLeaveTravelConcessionLog();
				ltcDetails.getEmployeeLeaveTravelConcessionNote();
				leaveTravelConcessionDto = ltcDetails;
				leaveTravelConcessionDto.setLeaveTravelId(ltcDetails.getEmployeeLeaveTravelConcession().getId());
				leaveTravelConcessionDto.setStatus(ltcDetails.getEmployeeLeaveTravelConcessionLog().getStage());
				List<String> billsFile = ltcDetails.getFileList();
				List<String> billsFileList = new ArrayList<>();
				if (billsFile != null && billsFile.size() > 0) {
					log.info("inside file list loaded.....");
					setBillsList(billsFile);
					for (String uploadPath : billsFile) {
						if (uploadPath != null && !uploadPath.isEmpty()) {
							String filePathValue[] = uploadPath.split("uploaded/");
							if (filePathValue != null) {
								log.info("0==>" + filePathValue[0]);
								log.info("1==>" + filePathValue[1]);
								/*
								 * if(fileFinalList.length == 0) { fileFinalList[fileFinalList.length] =
								 * filePathValue[1]; }else { fileFinalList[fileSize] = filePathValue[1]; }
								 */
								billsFileList.add(filePathValue[1]);
							} else {
								log.error("Uploaded file not found");
							}
						}
					}

					setLtcBillList(billsFileList);
				} else {
					log.info("inside empty file load.....");
					String s = new String();
					ltcBillList.add(s);
					billsList.add(s);
					fileList.add(s);
				}

				if (employeeLeaveTravelConcession != null) {
					appliedFor = employeeLeaveTravelConcession.getAvailedFor();
					travelMode = employeeLeaveTravelConcession.getTravelMode();
					leaveTypeMaster = employeeLeaveTravelConcession.getLeaveTypeMaster();
					isPermission = employeeLeaveTravelConcession.getHolidayPermission();

					if (employeeLeaveTravelConcession.getEntityMaster() != null) {

						loadAllEntityTypesByRegionId();
						selectedEntityTypeMaster = employeeLeaveTravelConcession.getEntityMaster()
								.getEntityTypeMaster();
						log.info("selectedEntityTypeMaster........" + selectedEntityTypeMaster);
						loadAllEntityByRegionIdandEntityTypeId();
						log.info("selectedEntityTypeMaster........" + selectedEntityTypeMaster);
						selectedEntityTypeMaster = employeeLeaveTravelConcession.getEntityMaster()
								.getEntityTypeMaster();

						log.info("selected entity typeMaster and Ho/Fo is.." + selectedEntityTypeMaster + " and"
								+ selectedHoRo);

						slectedEntity = employeeLeaveTravelConcession.getEntityMaster();
						if (slectedEntity.getCode() != 10) {
							selectedHoRo = commonDataService
									.findRegionByShowroomId(employeeLeaveTravelConcession.getEntityMaster().getId());
						} else {
							selectedHoRo = slectedEntity;
						}
						log.info("HORO is...." + selectedHoRo.getName());
						log.info("region name is...." + slectedEntity.getName());
						log.info("region code is...." + slectedEntity.getCode());
					}
					/*
					 * if(employeeLeaveTravelConcession != null &&
					 * employeeLeaveTravelConcession.getEntityMaster() != null) { selectedHoRo =
					 * commonDataService
					 * .findRegionByShowroomId(employeeLeaveTravelConcession.getEntityMaster().getId
					 * ()); }
					 */
					if (employeeLeaveTravelConcession.getSectionMaster() != null) {
						selectedDepartement = employeeLeaveTravelConcession.getSectionMaster().getDepartment();
						loadAllDepartementOREmployeBaseOnDepartementFlag();
						setFromDetails();
						loadAllSectionBasedOnDepartementId();
						setFromDetails();
						loadAllEmploymasaterBasedOnSectionId();

						setFromDetails();
						loadBlockYearsFromFinancialYearTable();
						selectBlockYearsFromFinancialYearTable();
						log.info("selected entity typeMaster and Ho/Fo is.." + selectedEntityTypeMaster + " and"
								+ selectedHoRo);
						setFromDetails();
						loadFamilyMembersView();
						loadEmpHolidayList();
						onLeaveTypeChange();
					}
				}
			}
			systemNotificationBean.loadTotalMessages();
		} catch (Exception e) {
			log.error("exception occured in getById", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return employeeLeaveTravelConcession;
	}

	@Autowired
	SystemNotificationBean systemNotificationBean;

	public void loadEmpHolidayList() {
		log.info("<---- start getempHolidayList --->");
		try {
			String url = LTC_URL + "/getHolidayByLtc/" + selectedLTCDId.getLeaveTravelId();
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				empLtcHolidayDatesList = mapper.readValue(jsonValue, new TypeReference<List<EmpLtcHolidayDates>>() {
				});
				if (action.equalsIgnoreCase("billsAdd"))
					RequestContext.getCurrentInstance().execute("PF('viewHoliday').show();");

				log.info("empLtcHolidayDates list size is " + empLtcHolidayDatesList.size());
			}
		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<---- End getempHolidayList --->");
	}

	public void selectBlockYearsFromFinancialYearTable() {
		log.info("<<<<---end selectBlockYearsFromFinancialYearTable...");
		try {
			if (blocKYear != null) {
				String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
				BaseDTO baseDTO = httpService.get(url);
				List<FinancialYear> blockYearList = new ArrayList<>();
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					blockYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
					});

					log.info("blockYearList list size is " + blockYearList.size());

					if (blockYearList.size() > 0) {

						for (FinancialYear yearObject : blockYearList) {
							FinancialYear endYear = new FinancialYear();

							Long year = yearObject.getStartYear() + 4;
							log.info("the end year is" + " " + yearObject.getStartYear() + " -" + year);
							endYear.setId(yearObject.getId());
							endYear.setStartYear(yearObject.getStartYear());
							endYear.setEndYear(year);
							if (yearObject.getStartYear().equals(blocKYear)) {
								log.info("selectd block year:.." + endYear);
								selectedBlocKYear = endYear;
								break;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---end selectBlockYearsFromFinancialYearTable...");
	}

	@Getter
	@Setter
	Long blocKYear;

	public void setFromDetails() {
		if (employeeLeaveTravelConcession.getSectionMaster() != null) {
			selectedDepartement = employeeLeaveTravelConcession.getSectionMaster().getDepartment();
			selectedSection = employeeLeaveTravelConcession.getSectionMaster();
			log.info("Selected department and section is.." + selectedDepartement + " and " + selectedSection);
			employeeMasterBasedOnSectionId = ltcDetails.getEmployeeMaster();
			employeeMaster = ltcDetails.getEmployeeMaster();
			log.info("employee id is...." + employeeMasterBasedOnSectionId.getId());
			String block = employeeLeaveTravelConcession.getBlockYears();
			if (block != null) {
				String[] blk = block.split("-");
				String year = blk[0] != null ? blk[0] : "";
				if (!year.equals(""))
					blocKYear = Long.parseLong(year);
			}
			log.info("blockYear....." + blocKYear);
		}
	}

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		List<UserMaster> autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(SERVER_URL, query);
		return autoEmployeeMasterList;
	}

}
