/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyFieldVerificationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.weavers.model.SocietyEnrollment;
import in.gov.cooptex.weavers.model.SocietyFieldVerification;
import in.gov.cooptex.weavers.model.SocietyFieldVerificationEmp;
import in.gov.cooptex.weavers.model.SocietyRegRequestLog;
import in.gov.cooptex.weavers.model.SocietyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("societyCodeAllotmentBean")
@Scope("session")
@Log4j2
public class SocietyCodeAllotmentBean {
	@Autowired
	AppPreference appPreference;

	private final String CREATE_CODEALLOTMENT_PAGE = "/pages/weavers/societyEnrollment/createCodeAllotment.xhtml?faces-redirect=true;";
	private final String LIST_CODEALLOTMENT_PAGE = "/pages/weavers/societyEnrollment/listCodeAllotment.xhtml?faces-redirect=true;";
	private final String VIEW_CODEALLOTMENT_PAGE = "/pages/weavers/societyEnrollment/viewCodeAllotment.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;
	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	SocietyRegistrationRequest societyRegistrationRequest, selectedSocietyRegistration;
	@Getter
	@Setter
	List<SocietyRegistrationRequest> societyRegReqList = null;

	@Getter
	@Setter
	SocietyEnrollment societyEnrollment;
	@Getter
	@Setter
	List<SocietyEnrollment> societyEnrollmentList = null;

	@Getter
	@Setter
	SocietyFieldVerification societyFieldVerification;
	@Getter
	@Setter
	SocietyFieldVerificationDTO societyFieldVerificationDTO;

	@Getter
	@Setter
	SocietyFieldVerificationEmp committeeMembers;

	@Getter
	@Setter
	List<SocietyFieldVerificationEmp> committeeMembersList = null;

	@Getter
	@Setter
	SocietyFieldVerification loomVerification;

	@Getter
	@Setter
	List<SocietyFieldVerification> loomVerificationList = null;

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action = "";

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean contentFlag = false;

	String jsonResponse;

	public String showsocietyCodeAllotmentListPage() {
		log.info("<==== Starts societyAllotmentApprovalBean-showsocietyAllotmentApprovalListPage =====>");
		mapper = new ObjectMapper();
		
		societyRegistrationLogList = new ArrayList<>();
		selectedSocietyRegistrationLog = new SocietyRegRequestLog();
		 
		addButtonFlag = false;
		viewButtonFlag = true;
		action = "";
		lazySocietyRegistrationList();
		log.info("<==== Ends societyAllotmentApprovalBean-showsocietyAllotmentApprovalListPage =====>");
		return LIST_CODEALLOTMENT_PAGE;
	}

	public String societysCodeAllotmentListPageAction() {
		log.info("<========inside societyCodeAllotment.societysCodeAllotmentListPageAction=======>");
		societyFieldVerification = new SocietyFieldVerification();
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("create societysCodeAllotmentListPageAction called..");
				contentFlag = false;
				societyRegReqList = new ArrayList<>();
				selectedSocietyRegistration=new SocietyRegistrationRequest();
				societyRegReqList = loadSocietyRegReqbyBoardfinalApproval();
				log.info("=======societyregreq List size=============>" + societyRegReqList.size());
				return CREATE_CODEALLOTMENT_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				societyRegistrationRequest=new SocietyRegistrationRequest();
				societyRegistrationRequest.setId(selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
				societyRegReqValuebyId(selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
				loadAllrecords();
				return VIEW_CODEALLOTMENT_PAGE;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	// Loading Needed Resource Data
	public void societyRegReqValuebyId(long id) {
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societyregistrationrequest/get/"+id;
			log.info("after Url declared====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyRegistrationRequest = mapper.readValue(jsonResponse, new TypeReference<SocietyRegistrationRequest>() {
				});
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}
	
	public List<SocietyRegistrationRequest> loadSocietyRegReqbyBoardfinalApproval() {
		log.info("<=== Starts SocietyCodeAllotmentBean.loadSocietyRegReqbyBoardfinalApproval ===>");
		List<SocietyRegistrationRequest> societyRegistrationRequestList = new ArrayList<>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/loadSocietyRegReqbyBoardfinalApproval";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyRegistrationRequestList = mapper.readValue(jsonResponse,
						new TypeReference<List<SocietyRegistrationRequest>>() {
						});
			}
		} catch (Exception e) {
			log.error("Exception in SocietyCodeAllotmentBean.loadSocietyRegReqbyDepfinalApproval >>>>>>>>" + e);
		}
		log.info("<=== Ends SocietyCodeAllotmentBean.loadSocietyRegReqbyDepfinalApproval ===>");
		return societyRegistrationRequestList;
	}

	@Getter
	@Setter
	int total = 12, attached = 0, remaining = 0;

	public void loadAllrecords() {
		log.info("==============loadAllrecords===============");
		contentFlag = false;
		try {
			societyEnrollment = new SocietyEnrollment();
			societyFieldVerificationDTO = new SocietyFieldVerificationDTO();
			attached = 0;
			remaining = 0;
			if (societyRegistrationRequest.getId() != null) {
				societyEnrollment = commonDataService
						.getSocietyEnrollmentbySocRegReqId(societyRegistrationRequest.getId());
				societyFieldVerificationDTO = commonDataService
						.loadsocietyFieldVerificationbyReqId(societyRegistrationRequest.getId());
				if (societyFieldVerificationDTO != null
						&& societyFieldVerificationDTO.getSocietyFieldVerification() != null) {
					societyFieldVerification = societyFieldVerificationDTO.getSocietyFieldVerification();
					committeeMembersList = societyFieldVerificationDTO.getCommitteeMembersList();
					loomVerificationList = societyFieldVerificationDTO.getLoomVerificationList();
				} else {
					return;
				}
				if (societyEnrollment != null && societyEnrollment.getSocietyEnrollmentFilesList() != null)
					attached = societyEnrollment.getSocietyEnrollmentFilesList().size();
				remaining = total - attached;
				contentFlag = true;
				if (action.equalsIgnoreCase("View")) {
					
				}
			} else {
				return;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
		log.info("==============societyEnrollment===============" + societyEnrollment);
	}

	public String saveSupplierMaster() {
		log.info("inside statusUdate socreqreg------->" + societyRegistrationRequest.getId());
		try {
			Validate.objectNotNull(societyRegistrationRequest.getId(), ErrorDescription.CODE_ALLOTMENT_SOCIETY_REG_REQ_EMPTY);
			
			SupplierMaster supplierMaster = new SupplierMaster();
			supplierMaster.setName(societyRegistrationRequest.getName());
			supplierMaster.setRegistrationDate(societyRegistrationRequest.getRegisteredDate());
			supplierMaster.setAddressId(societyEnrollment.getSocietyAddress());
			supplierMaster.setLoomType(societyRegistrationRequest.getLoomTypeMaster().getName());
			supplierMaster.setCircleMaster(societyRegistrationRequest.getCircleMaster());
			
			String url = AppUtil.getPortalServerURL()+appPreference.getOperationApiUrl()
			+ "/societyregistrationrequest/saveSupplierfromSocietyCodeAllotment/"+societyRegistrationRequest.getId();
			BaseDTO response = httpService.post(url, supplierMaster);
			if (response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.CODE_ALLOTMENT_SUPPLIER_ADDED_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch(RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Exception occured while save .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}

		return showsocietyCodeAllotmentListPage();
	}
	@Getter
	@Setter
	LazyDataModel<SocietyRegRequestLog> lazySocietyRegistrationLogList;
	@Getter
	@Setter
	List<SocietyRegRequestLog> societyRegistrationLogList = null;
	@Getter
	@Setter
	SocietyRegRequestLog selectedSocietyRegistrationLog;
	
	public void lazySocietyRegistrationList() {
		log.info("<===== Starts SocietyBoardApprovalBean.lazySocietyRegistrationList List ======>");
		String lazyType="CODE_ALLOTTED";
		String lazylistUrl = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/societyregistrationrequest/getallSocietyRegforApprovallazy/"+lazyType;

		lazySocietyRegistrationLogList = new LazyDataModel<SocietyRegRequestLog>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1553357030678469897L;

			@Override
			public List<SocietyRegRequestLog> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for lazySocietyRegistrationList List=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						societyRegistrationLogList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyRegRequestLog>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazySocietyRegistrationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + societyRegistrationLogList.size());
				return societyRegistrationLogList;
			}

			@Override
			public Object getRowKey(SocietyRegRequestLog res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyRegRequestLog getRowData(String rowKey) {
				try {
					for (SocietyRegRequestLog societyRegReqLog : societyRegistrationLogList) {
						if (societyRegReqLog.getId().equals(Long.valueOf(rowKey))) {
							selectedSocietyRegistrationLog = societyRegReqLog;
							return societyRegReqLog;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.lazySocietyRegistrationList List ======>");
	}
	
	// =======================================================================================================
	public String subString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
			subStringAddress = subStringAddress.substring(0, 19);
			subStringAddress = subStringAddress + "........";
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	@Getter
	@Setter
	private String fileName = "";

	private StreamedContent file;

	public StreamedContent getFile() {
		return file;
	}

	public void getDownloadFile(String path) {
		try {

			byte[] downloadName = null;
			try {
				File file = new File(path);

				downloadName = new byte[(int) file.length()];

				log.info("downloadName==>" + downloadName);

				FileInputStream fis = new FileInputStream(file);
				fis.read(downloadName); // read file into bytes[]
				fis.close();
			} catch (Exception e) {
				log.error("convertImage error==>" + e);
			}
			if (path.contains("uploaded")) {
				String filePathValue[] = path.split("uploaded/");
				fileName = filePathValue[1];
			}
			log.info(" FIle Name :: " + fileName);
			String ext = FilenameUtils.getExtension(fileName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "image/jpg", fileName);
			} else if (ext.contains("pdf")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "pdf", fileName);
			} else if (ext.contains("doc")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			} else if (ext.contains("docx")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			}
			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded file", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts bean.onRowSelect ========>" + event);
		selectedSocietyRegistrationLog = ((SocietyRegRequestLog) event.getObject());
		addButtonFlag = true;
		viewButtonFlag = false;
		log.info("<===Ends bean.onRowSelect ========>"
				+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
	}

}
