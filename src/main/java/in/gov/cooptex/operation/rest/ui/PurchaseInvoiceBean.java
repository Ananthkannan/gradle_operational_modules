package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.PurchaseInvoiceItems;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.UserType;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.GeneralErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.PurchaseInvoiceRequest;
import in.gov.cooptex.operation.dto.PurchaseInvoiceResponse;
import in.gov.cooptex.operation.dto.PurchaseInvoiceSearchDTO;
import in.gov.cooptex.operation.enums.InvoiceStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("purchaseInvoiceBean")
@Scope("session")
public class PurchaseInvoiceBean implements Serializable{
	
	private static final long serialVersionUID = -6185008293943379273L;

	private static final String LIST_SOCIETY_INVOICE_PAGE = "/pages/weavers/listPurchaseInvoice.xhtml?faces-redirect=true;";
	
	private static final String CREATE_SOCIETY_INVOICE_PAGE = "/pages/weavers/createPurchaseInvoice.xhtml?faces-redirect=true;";
	
	private static final String VIEW_SOCIETY_INVOICE_PAGE = "/pages/weavers/viewPurchaseInvoice.xhtml?faces-redirect=true;";
	
	private static final String PREVIEW_SOCIETY_INVOICE_PAGE = "/pages/weavers/previewPurchaseInvoice.xhtml?faces-redirect=true;";
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Autowired
	AppPreference appPreference; 
	
	@Autowired
	HttpService httpService;
	
	@Getter @Setter
	PurchaseInvoiceResponse societyInvoiceResponse;
	
	@Getter @Setter
	PurchaseInvoiceResponse societyInvoiceResponseForGstSummary;
	
	@Getter @Setter
	LazyDataModel<PurchaseInvoiceResponse> societyInvoiceResponseList;
	
	@Getter @Setter
	StockTransfer stockTransfer; 
	
	@Getter @Setter
	Integer listCount;
	
	@Autowired
	ErrorMap errorMap;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	List<String> statusValuesList;
	
	@Getter @Setter
	String action=null;
	
	/*@Getter @Setter
	EntityMaster societyObject;*/
	
	@Autowired
	LanguageBean languageBean; 
	
	@Getter @Setter
	String societyAddressDetails;
	
	@Getter @Setter
	EntityMasterDTO entityMasterDTO = new EntityMasterDTO();
	
	@Getter @Setter
	String productwareHouseAddressDetails;
	
	@Getter @Setter
	Map<String, List<String>> dnpOfficeAndProductwarehouseMap = new LinkedHashMap<String, List<String>>();
	
	@Getter @Setter
	Map<String, List<String>> warehouseAndStockMovementIdsMap = new LinkedHashMap<String, List<String>>();
	
	@Getter @Setter
	Map<String, List<String>> govSchemeQStockMovementAndEntityMap = new LinkedHashMap<String, List<String>>();
	
	@Getter @Setter
	List<String> dnpOfficesList;
	
	@Getter @Setter
	List<String> productwarehousesList;

	@Getter @Setter
	List<String> stockMovementIdsList;
	
	@Getter @Setter
	List<Long> selectedStockMovementIdsList;
	
	@Getter @Setter
	Long selectedStockMovementId =null;
	
	@Getter @Setter
	String productwareHousekey=null;
	
	@Getter @Setter
	String dnpOfficeKey=null;
	
	@Getter @Setter
	EntityMaster productWarehouseObject; 
	
	@Getter @Setter
	List<PurchaseInvoiceResponse> invoiceResponseList;
	
	@Getter @Setter
	Double totalFooterAmount;
	
	@Getter
	@Setter
	SupplierMaster loginSupplierMaster=new SupplierMaster();
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;
	
	@Getter @Setter
	Double cgstTaxValue;
	
	@Getter @Setter
	Double sgstTaxValue;
	
	@Getter @Setter
	Double totalCgstSgstTaxValue;
	
	@Getter @Setter
	List<GstSummeryDTO> gstWiseList;
	
	@Getter
	@Setter
	Double totalTaxMaterialAmount;
	
	@Getter
	@Setter
	String invoiceNumber,totalTaxMaterialAmountInWords;
	
	@Getter
	@Setter
	PurchaseInvoice purchaseInvoice;
	
	@Getter
	@Setter
	List<PurchaseInvoiceItems> purchaseInvoiceItems;
	
	@Getter
	@Setter
	String invoiceDate;
	
	@Getter @Setter
	boolean addButtonFlag = false;
	
	@Getter @Setter
	String invoiceStatus;
	
	@Getter @Setter
	Long purchaseInvoiceNumberForApprove;
	
	@Getter @Setter
	String termsAndConditions;
	
	@Getter @Setter
	String supGstnumber;
	
	@Getter
	@Setter
	String approveDate;
	
	@Getter
	@Setter
	String qcDate;
	
	@Getter
	@Setter
	String parDate;
	
	public PurchaseInvoiceBean(){
	}
	
	public String getSocietyInvoiceList() {
		log.info("<--- Inside getSocietyInvoiceList() ---> ");
		loadLazySocietyInvoiceList();
		loadStatusValues();
		return LIST_SOCIETY_INVOICE_PAGE;
	}
	
	public void loadLazySocietyInvoiceList() {
		log.info("<--- loadLazySocietyInvoiceList ---> ");
		
		societyInvoiceResponseList = new LazyDataModel<PurchaseInvoiceResponse>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<PurchaseInvoiceResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<PurchaseInvoiceResponse> data = new ArrayList<PurchaseInvoiceResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseInvoiceResponse>>() {});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listCount = baseDTO.getTotalRecords();
						log.info("<--- List Count --->  "+listCount);
					}else{
						listCount=0;
						log.info("<--- No Records Found --->");
					}
				} catch (Exception e) {
					log.error("Error in loadLazySocietyInvoiceList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(PurchaseInvoiceResponse res) {
				return res != null ? res.getSocietyInvoiceId() : null;
			}

			@Override
			public PurchaseInvoiceResponse getRowData(String rowKey) {
				List<PurchaseInvoiceResponse> responseList = (List<PurchaseInvoiceResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (PurchaseInvoiceResponse res : responseList) {
					if (res.getSocietyInvoiceId().longValue() == value.longValue()) {
						societyInvoiceResponse = res;
						return res;
					}
				}
				return null;
			}

		};
	}
	
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		
		Integer inoviceNum=null;
		Double grantTotal=null;		

		societyInvoiceResponse = new PurchaseInvoiceResponse();

		PurchaseInvoiceSearchDTO request = new PurchaseInvoiceSearchDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();

			if (key.equals("invoiceNumberOrPrefix")) {
				log.info("Society Invoice Number Or Prefix : " + value);
				
				request.setInvoiceNumberOrPrefix((String)value);
			}
			if (key.equals("invoiceFromCodeOrName")) {
				log.info("Society InvoiceFrom CodeOrName : " + value);
				request.setInvoiceFromCodeOrName((String)value);
			} 
			if (key.equals("invoiceIssuedToProductwareHouseCodeOrName")) {
				log.info("Society Invoice InvoiceIssuedTo ProductwareHouse CodeOrName : " + value);
				request.setInvoiceIssuedToProductwareHouseCodeOrName((String)value);
			} 
			if (key.equals("invoiceIssuedDate")) {
				log.info("Society Invoice Issued Date : " + value);
				request.setInvoiceIssuedDate((Date)value);
			}
			if (key.equals("grantTotal")) {
				log.info("Society Invoice Grant Total : " + value);
				
				grantTotal = AppUtil.stringToDouble((String)value, null);
				
				if(grantTotal!=null){
					request.setGrandTotal(grantTotal);
				}else{
					request.setGrandTotal(null);
				}
				
			}
			if(key.equals("purchaseInvoiceStatus")){
				log.info("Society Invoice Status : " + value);
				request.setStatus((String)value);
			}
			
		}
		
		log.info("PurchaseInvoiceSearchDTO  "+request);
		
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/search";
			log.info("Search URL "+URL);
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
	
	public void loadSocietyDetailsByLoggedinUser() {
		UserType loginUserType = null;
		try {
			UserMaster userMaster = loginBean.getUserDetailSession();
			loginUserType = userMaster == null ? null : userMaster.getUserType();
			if(loginUserType != null) {
				if(loginUserType.equals(UserType.SUPPLIER)) {
					//loadUserProfile();
					loginSupplierMaster = (SupplierMaster) loginBean.getUserProfile();
					if (loginSupplierMaster == null) {
						errorMap.notify(ErrorDescription.SUPPLIER_MASTER_SHOULD_NOT_BE_EMPTY.getErrorCode());
						return;
					}
					
					String societyAddressArray[] = AppUtil.prepareAddress(loginSupplierMaster.getAddressId());
					
					if(societyAddressArray != null && languageBean.getLocaleCode().equals("en_IN")) {
						societyAddressDetails = societyAddressArray[0];
					}else {
						societyAddressDetails = societyAddressArray[1];
					}
					log.info("societyAddressDetails ==> " + societyAddressDetails);
				}else if(loginUserType.equals(UserType.EMPLOYEE)) {
					log.info("Login User Should not be a Supplier");
					if("VIEW".equalsIgnoreCase(action)) {
						loginSupplierMaster = new SupplierMaster();
						log.info("action-------------"+action);
						if(societyInvoiceResponse != null && societyInvoiceResponse.getInvoiceFromCode() != null) {
							log.info("societyInvoiceResponse obj------"+societyInvoiceResponse);
							loginSupplierMaster= commonDataService
									.getSupplierDetailsByCode(societyInvoiceResponse.getInvoiceFromCode(), SERVER_URL);
							log.info("supplierMaster obj-------------"+loginSupplierMaster);
							if(loginSupplierMaster != null && loginSupplierMaster.getAddressId() != null) {
								String societyAddressArray[] = AppUtil.prepareAddress(loginSupplierMaster.getAddressId());
								if(societyAddressArray != null && languageBean.getLocaleCode().equals("en_IN")) {
									societyAddressDetails = societyAddressArray[0];
								}else {
									societyAddressDetails = societyAddressArray[1];
								}
								log.info("societyAddressDetails ==> " + societyAddressDetails);
							}
						}
					}else {
					errorMap.notify(
							ErrorDescription.getError(GeneralErrorCode.SUPPLIER_USERTYPE_MANDATORY).getErrorCode());
					return;
					}
				}
			}else {
				log.info("Login UserType is Empty");
			}
		} catch (Exception exp) {
			log.error("Exception in loadSocietyDetailsByLoggedinUser ", exp);
		}
	}
	
	private void loadStatusValues() {
		Object[] statusArray = InvoiceStatus.class.getEnumConstants();

		statusValuesList = new ArrayList<>();

		for (Object status : statusArray) {
			statusValuesList.add(status.toString());
		}

		log.info("<--- statusValuesList ---> " + statusValuesList);
	}
	
	public String createSocietyInvoice(){
		log.info(" Inside createSocietyInvoice ");
		
		productwareHousekey = null;
		productwarehousesList=new ArrayList<>();
		
		selectedStockMovementId =null;
		stockMovementIdsList=new ArrayList<>();
		
		productWarehouseObject=null;		
		productwareHouseAddressDetails=null;
		
		invoiceResponseList=new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		gstWiseList=new ArrayList<>();
		
		loadSocietyDetailsByLoggedinUser();
		
		prepareDnpOfficesAndProductwareHousesAndStockMovements();
		
		return CREATE_SOCIETY_INVOICE_PAGE;
	}
	
	private void prepareDnpOfficesAndProductwareHousesAndStockMovements(){
		
		warehouseAndStockMovementIdsMap = new LinkedHashMap<>();
		
		productwarehousesList = new ArrayList<>();
		
		try{
			String prepareDnpOfficesAndProductwareHousesAndStockMovementsPath = appPreference.getPortalServerURL() + 
								appPreference.getOperationApiUrl() + "/societyinvoice/prepareDnpOfficesAndProductwareHousesAndStockMovements";
			log.info("Prepare DnpOffices, ProductwareHouses, StockMovements URL "+prepareDnpOfficesAndProductwareHousesAndStockMovementsPath);
			BaseDTO baseDTO = httpService.get(prepareDnpOfficesAndProductwareHousesAndStockMovementsPath);
			
			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			societyInvoiceResponse = mapper.readValue(jsonValue, PurchaseInvoiceResponse.class);
						
			if(societyInvoiceResponse==null){
				log.info("Prepare DnpOffices, ProductwareHouses, StockMovements Not Found");
				return;
			}
			
			warehouseAndStockMovementIdsMap = societyInvoiceResponse.getWarehouseAndStockMovementIdsMap();
			log.info("list returned from list is"+societyInvoiceResponse.getGovSchemeQcStockMovementAndEntityMap());
			govSchemeQStockMovementAndEntityMap = societyInvoiceResponse.getGovSchemeQcStockMovementAndEntityMap();
			productwarehousesList = new ArrayList<String>(warehouseAndStockMovementIdsMap.keySet());
			log.info("the productwarehousesList size "+productwarehousesList.size());
			productwarehousesList.addAll(govSchemeQStockMovementAndEntityMap.keySet());
			log.info("ProductwareHouses List from dataBase "+productwarehousesList);
			log.info("the productwarehousesList size "+productwarehousesList.size());
			
					
		}catch (Exception exp) {
			log.error("Exception in prepareDnpOfficesAndProductwareHousesAndStockMovements ", exp);
		}
		
	}
	
	public void onchangeShippingTo(){ 
		
		log.info("Inside onchangeShippingTo warehouseKey ");
		
		if(productwareHousekey!=null && productwareHousekey.length()>0){
			log.info("onchangeShippingTo warehouseKey "+productwareHousekey);
			
			prepareProductwareHouseDetails(productwareHousekey);
			
			//stockMovementIdsList=new ArrayList<>();
			if(warehouseAndStockMovementIdsMap.containsKey(productwareHousekey)){
				stockMovementIdsList =	warehouseAndStockMovementIdsMap.get(productwareHousekey);
			}else if(govSchemeQStockMovementAndEntityMap.containsKey(productwareHousekey)) {
				stockMovementIdsList =  govSchemeQStockMovementAndEntityMap.get(productwareHousekey);
				log.info("stockMovementIdsList"+stockMovementIdsList);
			} 
		}else{
			stockMovementIdsList=new ArrayList<>();
			productWarehouseObject=null;
			productwareHouseAddressDetails=null;
		}
		
	}
	
	public void prepareProductwareHouseDetails(String productwareHousekey){
		
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/getproductwarehousedet/"+
							productwareHousekey.split("/")[1].trim()+"/"+productwareHousekey.split("/")[2].trim();
			log.info("prepareProductwareHouseDetails URL "+URL);
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			productWarehouseObject = mapper.readValue(jsonValue, EntityMaster.class);
			
			if(productWarehouseObject == null) {
				log.info("ProductwareHouseDetails Not Found");
				AppUtil.addError("Product WareHouse Details Not Found"); 
				return;
			}
			
			productWarehouseObject.setName(productWarehouseObject.getCode()+" / "+productWarehouseObject.getName());
			
			String societyAddressArray[] = AppUtil.prepareAddress(productWarehouseObject.getAddressMaster());
			
			if(societyAddressArray != null && languageBean.getLocaleCode().equals("en_IN")) {
				productwareHouseAddressDetails = societyAddressArray[0];
			}else if(societyAddressArray != null) {
				productwareHouseAddressDetails = societyAddressArray[1];
			}
			log.info("Logged in productWarehouse Address Details  " + productwareHouseAddressDetails);
		} catch (Exception exp) {
			log.error("Exception in prepareProductwareHouse Address Details ", exp);
		}
	}
	
	public void afterStockMovementsSelect(){
		log.info("Inside afterStockMovementsSelect  "+selectedStockMovementId);
		
	}
	
	public String generateSocietyInvoiceProductDetails(){
		log.info("Inside generateSocietyInvoiceProductDetails "+ selectedStockMovementId);
		BaseDTO baseDTO=null;totalFooterAmount=0d;
		try {
			String requestPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + 
												"/societyinvoice/generateproductdetails/"+selectedStockMovementId;
			
			log.info("generateSocietyInvoiceProductDetails requestPath "+requestPath);
			
			baseDTO = httpService.get(requestPath);
			if(baseDTO!=null){
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				invoiceResponseList = mapper.readValue(jsonValue, new TypeReference<List<PurchaseInvoiceResponse>>(){});
				if(invoiceResponseList !=null && invoiceResponseList.size()>0) {
					
					boolean taxExemptedValue = societyInvoiceResponse.isTaxExempted();
					
					prepareGstSummary(invoiceResponseList);
					
					log.info(" invoiceResponseList "+invoiceResponseList);
					
					totalFooterAmount = invoiceResponseList.stream().filter(a -> a.getTotalAmount()!=null).mapToDouble(PurchaseInvoiceResponse::getTotalAmount).sum() - 
							invoiceResponseList.stream().filter(a -> a.getTaxAmount()!=null).mapToDouble(PurchaseInvoiceResponse::getTaxAmount).sum();
					log.info("<==== totalFooterAmount =====> "+totalFooterAmount);
					
					if(taxExemptedValue) {
						totalTaxMaterialAmount =  totalFooterAmount == null ? 0D : totalFooterAmount;
					}else {
						totalTaxMaterialAmount = (totalCgstSgstTaxValue == null ? 0D : totalCgstSgstTaxValue) + 
								(totalFooterAmount == null ? 0D : totalFooterAmount);
					}
					
					totalTaxMaterialAmountInWords = AmountToWordConverter.converter(String.valueOf(AppUtil.DECIMAL_FORMAT.format(totalTaxMaterialAmount)));
					totalTaxMaterialAmountInWords = totalTaxMaterialAmount == 0D ? "" : totalTaxMaterialAmountInWords;
					
				}else {
					setGstPercentageWiseDTOList(null);
					setGstWiseList(null);
					totalFooterAmount=0d;
					totalTaxMaterialAmount=0d;
					totalTaxMaterialAmountInWords = "";
					totalCgstSgstTaxValue=0d;
					sgstTaxValue=0d;
					cgstTaxValue=0d;
				}
			}
			
		} catch (Exception exp) {																														
			 log.error("Error in SocietyInvocieBean generateSocietyInvoiceProductDetails()  ", exp);
		}
		
		return CREATE_SOCIETY_INVOICE_PAGE;
	}
	
	public void prepareGstSummary(List<PurchaseInvoiceResponse> itemsList){
		//String requestPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/gstsummary";
		String requestPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/societyinvgstsummary";

		log.info("generateSocietyInvoiceProductDetails requestPath "+requestPath);
		
		try {
			BaseDTO baseDTO = httpService.post(requestPath, itemsList);
			if(baseDTO!=null){
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyInvoiceResponse = mapper.readValue(jsonValue, PurchaseInvoiceResponse.class);
				
				gstPercentageWiseDTOList = societyInvoiceResponse.getGstSummary();
				
				gstWiseList = societyInvoiceResponse.getGstWiseList();
				
				calculateTotalTaxSummary();
				
				//totalTaxMaterialAmount = invoiceResponseList.stream().filter(a -> a.getTotalAmount()!=null).mapToDouble(PurchaseInvoiceResponse::getTotalAmount).sum();
			}
			
		} catch (Exception e) {
			log.error("Error in prepareGstSummary() ",e);
		}
	}
	
	public void calculateTotalTaxSummary() {
		if (gstPercentageWiseDTOList.size() != 0) {
			cgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			sgstTaxValue  = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalCgstSgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
		}
	}
	
	public String preveiwSocietyInvoice(){
		log.info(" preveiwSocietyInvoice ::  start ");
		BaseDTO baseDTO= null;
		approveDate= null;invoiceDate=null;parDate=null;qcDate= null;
		try {
			if(invoiceResponseList == null || invoiceResponseList.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			if(gstPercentageWiseDTOList == null || gstPercentageWiseDTOList.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.GST_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			if(selectedStockMovementId == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STOCK_TRANSFER_ID_NOT_EMPTY).getCode());
				return null;
			}
			
			
			getSupplierGSTNumber();
			
			log.info("productWarehouseObject==> "+productWarehouseObject);
			log.info("Entity id==> "+productWarehouseObject.getId());
			if(purchaseInvoice.getInvoiceDate() != null) {
				invoiceDate = AppUtil.DATE_FORMAT.format(purchaseInvoice.getInvoiceDate());
				log.info("invoiceDate==> "+invoiceDate);
			}
			if(purchaseInvoice.getStatus().equals(InvoiceStatus.APPROVED.toString())) {
				if(purchaseInvoice.getModifiedDate() != null) {
					approveDate = AppUtil.DATE_FORMAT.format(purchaseInvoice.getModifiedDate());
					log.info("approveDate==> "+approveDate);
				}
			}
			
			log.info("selectedStockMovementId==> "+selectedStockMovementId);
			getQcDate(selectedStockMovementId);
			
			entityMasterDTO =  commonDataService.loadEntityByEntityId(productWarehouseObject.getId());
			
			stockTransfer = commonDataService.getStockTransferById(selectedStockMovementId);
			if(stockTransfer != null) {
				log.info("stockTransfer id==> "+stockTransfer.getId());
			}else {
				log.error("Stock transfer not found");
			}
			if("ADD".equalsIgnoreCase(action)){
				try {
					String prefixPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/generateinvoiceprefix";
					
					baseDTO = httpService.get(prefixPath);
					
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					invoiceNumber = mapper.readValue(jsonValue,  String.class);
					
					log.info("invoiceNumber "+invoiceNumber);
				
				} catch (Exception exp) {																														
					 log.error("Error in SocietyInvocieBean generateSocietyInvoiceProductDetails()  ", exp);
				}	
			} 
		}catch(Exception e) {
			log.error("preveiwSocietyInvoice :: Exception ==> ",e);
		}
		
		
		
		return PREVIEW_SOCIETY_INVOICE_PAGE;
	}
	
	private void getQcDate(Long stockMovementId) {
		log.info("getQcDate method starts---------");
		try {
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/getqcdate/"+stockMovementId;
			BaseDTO baseDTO = httpService.get(url);
			if(baseDTO != null && baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				PurchaseInvoiceResponse invoiceResponse = mapper.readValue(jsonValue,  PurchaseInvoiceResponse.class);
				if(invoiceResponse != null) {
					log.info("getQcDate response--------"+invoiceResponse);
					if(invoiceResponse.getQcDate() != null) {
						qcDate = AppUtil.DATE_FORMAT.format(invoiceResponse.getQcDate());
						log.info("qcDate==> "+qcDate);
					}
					if(invoiceResponse.getParDate() != null) {
						parDate = AppUtil.DATE_FORMAT.format(invoiceResponse.getParDate());
						log.info("parDate==> "+parDate);
					}
				}
			}
		}catch (Exception e) {
			log.error("getQcDate method exception-------",e);
		}
	}

	public String getSupplierGSTNumber() {
		log.info(" getSupplierGSTNumber :: start " );
		BaseDTO baseDTO= null;
		UserType loginUserType = null;
		try {
			UserMaster userMaster = loginBean.getUserDetailSession();
			loginUserType = userMaster == null ? null : userMaster.getUserType();
			if(loginUserType != null) {
				if(loginUserType.equals(UserType.SUPPLIER)) {
					loginSupplierMaster = (SupplierMaster) loginBean.getUserProfile();
					String url =   appPreference.getPortalServerURL()+ "/supplier/master/getsuppliergstNumber/"+loginSupplierMaster.getId();
					baseDTO = httpService.get(url);
					
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					supGstnumber = mapper.readValue(jsonValue,  String.class);
					 if(supGstnumber != null && !supGstnumber.isEmpty()) {
						 log.info("getSupplierGSTNumber :: supGstnumber==> "+supGstnumber);
					 }
				}else if(loginUserType.equals(UserType.EMPLOYEE)) {
					log.info("Login User Should not be a Supplier");
					if("VIEW".equalsIgnoreCase(action)) {
					if(loginSupplierMaster != null) {
						supGstnumber = loginSupplierMaster.getGstNumber();
					}
					}else {
						errorMap.notify(
								ErrorDescription.getError(GeneralErrorCode.SUPPLIER_USERTYPE_MANDATORY).getErrorCode());
						return null;
					}
				}
			}else {
				log.info("Login UserType is Empty");
			}
			
		}catch(Exception e) {
			log.error("getSupplierGSTNumber :: Exception ==> ",e);
		}
		return null;
	}
	
	
	public String submitSocietyInvoice(){
		log.info(" Inside submitSocietyInvoice " );
		BaseDTO baseDTO =null;
		
		PurchaseInvoiceRequest request = new PurchaseInvoiceRequest();
		
		request.setEntityId(productWarehouseObject.getId());
		request.setSupplierId(loginSupplierMaster.getId());
		request.setStockTransferId(selectedStockMovementId);
		request.setInvoiceNumber(invoiceNumber);
		
		request.setGstTaxDetailsList(gstPercentageWiseDTOList);
		
		request.setItemsList(invoiceResponseList);
		request.setTermsAndConditions(termsAndConditions);
				
		try {
			String createPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/create";
			
			baseDTO = httpService.post(createPath, request);
			
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {				 
				log.info("<--- Purchase Invoice Submitted Successfully");
				errorMap.notify(ErrorDescription.INFO_PURCHASE_INVOICE_SUBMITTED_SUCCESSFULLY.getCode());
				clearSocietyInvoice();
			 	getSocietyInvoiceList();
			}else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
			
		} catch (Exception exp) {																														
			 log.error("Error in SocietyInvocieBean generateSocietyInvoiceProductDetails()  ", exp);
		}
		
		return LIST_SOCIETY_INVOICE_PAGE;
	}
	
	public void clearSocietyInvoice(){
		termsAndConditions = null;
	}
	
 	public String viewSocietyInvoice(){
 		log.info(" Inside viewSocietyInvoice status " +societyInvoiceResponse.getStatus());
 		
 		log.info(" Inside viewSocietyInvoice " +societyInvoiceResponse.getSocietyInvoiceId());
 		
 		invoiceStatus = societyInvoiceResponse.getStatus();
 		
 		purchaseInvoiceNumberForApprove = societyInvoiceResponse.getSocietyInvoiceId();
 		
 		boolean taxExemptedValue = societyInvoiceResponse != null ? societyInvoiceResponse.isTaxExempted() : false;
 		
 		log.info("viewSocietyInvoice start==> ");
 		BaseDTO baseDTO =null;
 		try {
 			if (societyInvoiceResponse == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} 
 			log.info("getSocietyInvoiceId==> "+societyInvoiceResponse.getSocietyInvoiceId());
 			loadSocietyDetailsByLoggedinUser();
 			
			String getURL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + 
														"/societyinvoice/getdup/"+societyInvoiceResponse.getSocietyInvoiceId();
			log.info("getURL==> "+getURL);
			baseDTO = httpService.get(getURL);
			if(baseDTO!=null && baseDTO.getResponseContent()!=null){
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseInvoice = mapper.readValue(jsonValue,  PurchaseInvoice.class);
				
				invoiceNumber = purchaseInvoice.getInvoiceNumberPrefix()+"-"+purchaseInvoice.getInvoiceNumber();
				log.info("invoiceNumber==> "+invoiceNumber);
				
				String invocieIssedEntityKey = purchaseInvoice.getInvoiceIssuedToProductwareHouse().getId()+"/"+
												purchaseInvoice.getInvoiceIssuedToProductwareHouse().getCode()+"/"+
												purchaseInvoice.getInvoiceIssuedToProductwareHouse().getName();
				
				log.info("invocieIssedEntityKey==> "+invocieIssedEntityKey);
				
				prepareProductwareHouseDetails(invocieIssedEntityKey);
				
				selectedStockMovementId = purchaseInvoice.getStockTransferId();
				log.info("selectedStockMovementId==> "+selectedStockMovementId);
				
				//selectedStockMovementId = purchaseInvoice.getStockTransfer().getId();
				
				//log.info("selectedStockMovementId==> "+selectedStockMovementId);
				
				generateSocietyInvoiceProductDetails();
				
				productwareHousekey = purchaseInvoice.getInvoiceIssuedToProductwareHouse().getName();
				log.info("productwareHousekey==> "+productwareHousekey);
				
				
			}
			
			//getSocietyInvoiceList();
			
			societyInvoiceResponse.setTaxExempted(taxExemptedValue);
		
		} catch (Exception exp) {																														
			 log.error("Error in SocietyInvocieBean generateSocietyInvoiceProductDetails()  ", exp);
		}
		
		return VIEW_SOCIETY_INVOICE_PAGE;
	}
	
 	public String approvePurchaseInvoice(){
 		log.info(" Inside approvePurchaseInvoice "+purchaseInvoiceNumberForApprove);
 		BaseDTO baseDTO=null;
 		Long societyInvocieId = purchaseInvoiceNumberForApprove;
 		
 		try {
			String approvePath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/societyinvoice/approve/"+societyInvocieId;
			
			purchaseInvoice.setNetTotal(totalTaxMaterialAmount);
			purchaseInvoice.setMaterialValue(totalFooterAmount);
			purchaseInvoice.setCgstValue(cgstTaxValue);
			purchaseInvoice.setSgstValue(sgstTaxValue);
			baseDTO = httpService.post(approvePath,purchaseInvoice);
			
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {				 
				log.info("<--- Purchase Invocie Approved Successfully");
				errorMap.notify(ErrorDescription.INFO_PURCHASE_INVOICE_APPROVED_SUCCESSFULLY.getCode());
			 	getSocietyInvoiceList();
			}else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
			
		} catch (Exception exp) {																														
			 log.error("Error in SocietyInvocieBean approvePurchaseInvoice()  ", exp);
		}
 		
 		return LIST_SOCIETY_INVOICE_PAGE;
 	}
 	
	public String clear(){
		log.info("<--- Inside clear() --->");
		societyInvoiceResponse = new PurchaseInvoiceResponse();
		invoiceStatus=null;
		termsAndConditions = null;
		getSocietyInvoiceList();		
		return LIST_SOCIETY_INVOICE_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("PurchaseInvoiceResponse onRowSelect method started");
		societyInvoiceResponse = ((PurchaseInvoiceResponse) event.getObject());
		addButtonFlag=true;
    }
	
	public void onPageLoad() {
		log.info("PurchaseInvoiceResponse onPageLoad method started");
		addButtonFlag=false;
	 }
	
	public void clearSocietyInvoiceDetails() {
		invoiceResponseList=new ArrayList<PurchaseInvoiceResponse>();
		gstPercentageWiseDTOList=new ArrayList<HsnCodeTaxPercentageWiseDTO>();
		gstWiseList=new ArrayList<GstSummeryDTO>();
		productwareHousekey=null;
		selectedStockMovementId=null;
	}
	
	
}
