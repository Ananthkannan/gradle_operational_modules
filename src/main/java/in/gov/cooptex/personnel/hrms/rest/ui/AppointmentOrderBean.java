package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.apache.commons.io.Charsets;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.dto.AppointmentOrderPdfResponse;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.TemplateDetails;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.esr.dto.AppoinmentDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.AppointmentOrderDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Log4j2
@Service("appointmentOrderBean")
@Scope("session")
public class AppointmentOrderBean {

	ObjectMapper mapper;
	String jsonResponse;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Autowired
	CommonDataService commonDataService;
	@Autowired
	ConsolidatedMarksBean consolidatedMarksBean;
	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	List<Designation> designationList;
	@Getter
	@Setter
	List<Community> communityList;
	@Setter
	@Getter
	List<EntityMaster> regionList;
	@Getter
	@Setter
	List<PayScaleMaster> payScaleMasterList;
	@Getter
	@Setter
	EntityMaster region;
	@Getter
	@Setter
	PayScaleMaster payScale;
	@Getter
	@Setter
	String entityAddress;

	@Getter
	@Setter
	Double consolidatedPay;

	private String appointmentOrderPath, coOpTexlogoPath, downloadPath;

	@Getter
	@Setter
	List<AppointmentOrderDetail> appointmentOrderList = new ArrayList<AppointmentOrderDetail>();

	@Getter
	@Setter
	LazyDataModel<AppointmentOrderDetail> appointmentOrderLazyList;

	@Getter
	@Setter
	AppointmentOrderDetail selectedAppointment;

	@Getter
	@Setter
	AppointmentOrderDetail selectedEsrAppointment;

	@Getter
	@Setter
	List<AppointmentOrderDetail> selectedAppointmentOrderList = new ArrayList<AppointmentOrderDetail>();

	@Getter
	@Setter
	String notificationNumber;

	@Getter
	@Setter
	String recruitmentForPost;

	@Getter
	@Setter
	Long recruitmentYear;

	@Getter
	@Setter
	String remarks;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	String approveOrReject;

	private AppointmentOrderPdfResponse appointmentOrderPdfResponse;

	@Setter
	private DefaultStreamedContent downloadFile = null;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean esrButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;
	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean addnotesButtonFlag = false;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Autowired
	EmployeeMasterBean employeeMasterBean;

	@Getter
	@Setter
	Double securityDepositAmount = 0D;

	@Getter
	@Setter
	Boolean mobileAndEmailValidation = false;

	@Getter
	@Setter
	AppoinmentDTO appoinmentDTO = new AppoinmentDTO();

	@Getter
	@Setter
	JobApplication jobApplication = new JobApplication();

	/**
	 * @author B_Benny
	 * @return redirect to appointment order submit page
	 * 
	 */

	public DefaultStreamedContent getDownloadFile() {
		return downloadFile;
	}

	public String generateAppointmentOrder() {
		mapper = new ObjectMapper();
		try {
			if (consolidatedMarksBean.getSelectedConsolidatedMarks() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info(consolidatedMarksBean.getSelectedConsolidatedMarks().getId());
			BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/generateappointment",
					consolidatedMarksBean.getSelectedConsolidatedMarks());
			if (response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				appointmentOrderList = mapper.readValue(jsonResponse,
						new TypeReference<List<AppointmentOrderDetail>>() {
						});
				log.info("Appoinment order list size--->" + appointmentOrderList.size());
				return "/pages/personnelHR/appoinmentOrder/generateAppointmentOrder.xhtml?faces-redirect=true";
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception while generating appoinment order :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}

		return null;
	}

	public String saveAppointmentOrder() {
		log.info("<----Saving appoinment order---->");
		try {
			log.info("Appointment order size--->" + appointmentOrderList.size());

			appointmentOrderList.forEach(appointment -> {
				appointment.setRemarks(remarks);
				appointment.setCreatedBy(loginBean.getUserDetailSession());
			});
			AppointmentOrderDetail request = new AppointmentOrderDetail();
			request.setAppointmentOrderDetailList(appointmentOrderList);
			BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/create", request);
			errorMap.notify(response.getStatusCode());
			if (response.getStatusCode() != 2000) {
				remarks = null;
				return loadAppointmentOrderList();
			}

		} catch (Exception e) {
			log.error(" Exception while saving appoinment order :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
		return null;
	}

	public String orderGenerationCancel() {
		log.info("<------Generation of appointment order cancelled---->");
		appointmentOrderList = new ArrayList<AppointmentOrderDetail>();
		notificationNumber = null;
		recruitmentForPost = null;
		recruitmentYear = null;
		remarks = null;
		return consolidatedMarksBean.appointmentGenerationCancel();
	}

	public String loadAppointmentOrderList() {
		log.info("<-----Loading appointment order list page----->");
		mapper = new ObjectMapper();
		action = "LIST";
		appointmentOrderList = new ArrayList<AppointmentOrderDetail>();
		selectedAppointmentOrderList = new ArrayList<AppointmentOrderDetail>();
		selectedAppointment = new AppointmentOrderDetail();
		selectedEsrAppointment = new AppointmentOrderDetail();
		addButtonFlag = false;
		generateFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		addnotesButtonFlag = false;
		esrButtonFlag = false;
		loadMasterValues();
		loadAppointmentOrderLazy();
		return "/pages/personnelHR/appoinmentOrder/listAppointmentOrder.xhtml?faces-redirect=true";
	}

	public void loadAppointmentOrderLazy() {
		appointmentOrderLazyList = new LazyDataModel<AppointmentOrderDetail>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<AppointmentOrderDetail> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Response retrurn null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<AppointmentOrderDetail> appointmentOrderList = mapper.readValue(jsonResponse,
							new TypeReference<List<AppointmentOrderDetail>>() {
							});
					if (appointmentOrderList == null) {
						log.info(" Appointment order list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Appointment list search Successfully Completed");
					} else {
						log.error(" Appointment list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return appointmentOrderList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Appointmentlist :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(AppointmentOrderDetail res) {
				return res != null ? res.getNotificationNumber() : null;
			}

			@Override
			public AppointmentOrderDetail getRowData(String rowKey) {
				List<AppointmentOrderDetail> responseList = (List<AppointmentOrderDetail>) getWrappedData();
				for (AppointmentOrderDetail res : responseList) {
					if (res.getNotificationNumber().equals(rowKey)) {
						selectedAppointment = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			AppointmentOrderDetail request = new AppointmentOrderDetail();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("notificationNumber")) {
					log.info("Notification Number : " + value);
					searchFilters.put("notificationNumber", value);
				}
				if (filter.getKey().equals("recruitmentPost")) {
					log.info("recruitment for post : " + value);
					searchFilters.put("recruitmentPost", value);
				}
				if (filter.getKey().equals("recruitmentYear")) {
					log.info("Recruitment year : " + value);
					searchFilters.put("recruitmentYear", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("Written exam Date : " + value);
					searchFilters.put("createdDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}
			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/appointmentorder/getalllazy", request);

		} catch (Exception e) {
			log.error("Exception Occured in appointment load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public void loadMasterValues() {
		designationList = commonDataService.loadDesignation();
		communityList = commonDataService.getAllCommunity();
	}

	public Double getConsoildatedPayForDesignation(String designationName) {
		log.info("<<======= AppointmentOrderBean:getConsoildatedPayForDesignation() Starts =======>> ");
		Double consolidatedPay = 0D;
		try {
			String requestPath = SERVER_URL + "/designation/getdesignationbyname/" + designationName;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					Designation designation = mapper.readValue(jsonResponse, new TypeReference<Designation>() {
					});
					if (designation != null) {
						consolidatedPay = designation.getConsolidatedPay() == null ? 0.00
								: designation.getConsolidatedPay();
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in AppointmentOrderBean:getConsoildatedPayForDesignation() ", exp);
		}
		log.info("<<======= AppointmentOrderBean:getConsoildatedPayForDesignation() Ends =======>> ");
		return consolidatedPay;
	}

	public String appointmentOrderListAction() {
		try {
			if (selectedAppointment == null) {
				errorMap.notify(ErrorDescription.SELECT_APPOINTMENT_ORDER.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("DELETE")) {
				if (selectedAppointment.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_APPOINTMENT_ORDER_CANNOT_DELETE.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAppointmentDelete').show();");
			} else {
				log.info("<----Loading  page.....---->" + selectedAppointment.getNotificationNumber());
				notificationNumber = selectedAppointment.getNotificationNumber();
				recruitmentForPost = selectedAppointment.getRecruitmentPost();

				// Get Consolidated Pay For Designation(recruitmentForPost);
				consolidatedPay = getConsoildatedPayForDesignation(recruitmentForPost);

				recruitmentYear = selectedAppointment.getRecruitmentYear();
				remarks = selectedAppointment.getRemarks();
				appointmentOrderList = new ArrayList<AppointmentOrderDetail>();
				BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/getbynotificationnumber",
						selectedAppointment);
				if (response != null) {
					if (response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						appointmentOrderList = mapper.readValue(jsonResponse,
								new TypeReference<List<AppointmentOrderDetail>>() {
								});

						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						payScale = mapper.readValue(jsonResponse, new TypeReference<PayScaleMaster>() {
						});

						securityDepositAmount = response.getDepositAmount();
						if (action.equalsIgnoreCase("VIEW")) {
							return "/pages/personnelHR/appoinmentOrder/viewAppointmentOrder.xhtml?faces-redirect=true";
						} else if (action.equalsIgnoreCase("APPROVE") || action.equalsIgnoreCase("CREATEESR")) {
							selectedEsrAppointment = new AppointmentOrderDetail();
							return "/pages/personnelHR/appoinmentOrder/approvalAppointmentOrder.xhtml?faces-redirect=true";
						} else if (action.equalsIgnoreCase("ADDNOTES")) {
							// regionList = commonDataService.loadRegionalOffice();
							regionList = commonDataService.loadHeadAndRegionalOffice();
							payScaleMasterList = commonDataService.loadPayscale();
							return "/pages/personnelHR/appoinmentOrder/appointmentOrderAddNotes.xhtml?faces-redirect=true";
						}
					} else {
						errorMap.notify(response.getStatusCode());
						return null;
					}
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in apponitment order search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String appointmentOrderApproveReject() {
		try {
			log.info("<----Approve or Reject appointments with notification.....---->" + notificationNumber + " "
					+ approveOrReject);
			AppointmentOrderDetail appointmentOrderDetail = new AppointmentOrderDetail();
			appointmentOrderDetail.setNotificationNumber(notificationNumber);
			appointmentOrderDetail.setModifiedDate(new Date());
			appointmentOrderDetail.setModifiedBy(loginBean.getUserDetailSession());
			appointmentOrderDetail.setStatus(approveOrReject);
			BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/approve", appointmentOrderDetail);
			if (response.getStatusCode() == 0) {
				errorMap.notify(response.getStatusCode());
				return loadAppointmentOrderList();
			}
		} catch (Exception e) {
			log.error("Exception Occured in Appointment action ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void downloadAppointmentOrder() {
		try {
			log.info("<-----Downloading appointment order......" + selectedAppointmentOrderList.size());
			if (selectedAppointmentOrderList == null || selectedAppointmentOrderList.size() == 0) {
				errorMap.notify(ErrorDescription.SELECT_APPOINTMENT_ORDER.getErrorCode());
				return;
			}
			loadAppoinmetOrderPath();

			if (selectedAppointmentOrderList.size() > 1) {
				log.info("Call Letter Path is :: " + appointmentOrderPath);
				File fPath = new File(appointmentOrderPath);
				// FileUtils.cleanDirectory(fPath);
				selectedAppointmentOrderList.forEach(appontment -> {
					createFile(appontment, selectedAppointmentOrderList.size());
				});
				ByteArrayInputStream bis = new ByteArrayInputStream(AppUtil.zip(appointmentOrderPath));
				InputStream stream = bis;
				downloadFile = new DefaultStreamedContent(stream, "application/zip", "appointmentorder.zip",
						Charsets.UTF_8.name());
				// FileUtils.cleanDirectory(fPath);
			} else {
				log.info("selectedAppointmentOrderList size is greater than one else");
				AppointmentOrderDetail appointmentOrder = selectedAppointmentOrderList.get(0);
				createFile(appointmentOrder, selectedAppointmentOrderList.size());

			}

		} catch (Exception e) {
			log.error("Exception Occured in JobApplicationApproval search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String deleteConfirm() {
		log.info("<--- Appointment delete confirmed---->" + selectedAppointment.getNotificationNumber());
		try {
			BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/deletebynotificationnumber/",
					selectedAppointment);
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {

			log.error("Exception occured while deleting appointment order ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadAppointmentOrderList();
	}

	public void clearAppointmentSelection() {
		selectedAppointmentOrderList = new ArrayList<AppointmentOrderDetail>();
	}

	public void loadAppoinmetOrderPath() {
		try {
			coOpTexlogoPath = commonDataService.getAppKeyValue("CALL_LETTER_LOGO_PATH");
			downloadPath = commonDataService.getAppKeyValue("APPOINTMENT_ORDER_DOWNLOAD_PATH");
			File file = new File(downloadPath);
			appointmentOrderPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(appointmentOrderPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Hall Ticket Path ::" + path);
		} catch (Exception e) {
			log.error("Exception Occured while loading hallticket Path ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void createFile(AppointmentOrderDetail appoinment, Integer fileSize) {
		log.info("< == = == =  Inside Create File = == = = = ==  >>" + fileSize);
		try {

			BaseDTO response = httpService
					.get(SERVER_URL + "/appointmentorder/appointmentpdfdata/" + appoinment.getId());
			if (response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				appointmentOrderPdfResponse = mapper.readValue(jsonResponse,
						new TypeReference<AppointmentOrderPdfResponse>() {
						});

				JasperReport jasperReportMCQon;
				JasperDesign jasperDesignMCQon;
				JasperPrint report = null;
				Map<String, Object> map = new HashMap<String, Object>();
				jasperDesignMCQon = JRXmlLoader.load(getClass().getResourceAsStream("/appointment_order.jrxml"));
				jasperReportMCQon = JasperCompileManager.compileReport(jasperDesignMCQon);
				Map<String, Object> params = new HashMap<String, Object>();

				String desc = appointmentOrderPdfResponse.getTemplateDetails().getDescription().replace("<br>", "\n");
				String desctab = desc.replace("TAB_SPACE", "\t");
				List<TemplateDetails> termsAndConditionsList = new ArrayList<>();
				TemplateDetails tDetail = new TemplateDetails();
				tDetail.setDescription(desctab);
				termsAndConditionsList.add(tDetail);
				log.info("appointmentOrderPdfResponse.getOfficeAddress()"
						+ appointmentOrderPdfResponse.getOfficeAddress());
				map.put("CO_OP_TEX_LOGO", coOpTexlogoPath.trim());

				map.put("NAME", appointmentOrderPdfResponse.getCandidateName());
				map.put("REG_NO", appointmentOrderPdfResponse.getApplicationNumber());
				map.put("APPOINTMENT_DATE", appointmentOrderPdfResponse.getAppoinmentDate());
				map.put("FATHER_NAME", appointmentOrderPdfResponse.getFatherName());
				map.put("DOOR_NO", appointmentOrderPdfResponse.getDoorNumber());
				map.put("STREET_NAME", appointmentOrderPdfResponse.getStreetNumber());
				map.put("TALUK_NAME", appointmentOrderPdfResponse.getTalukName());
				map.put("DISTRICT_NAME", appointmentOrderPdfResponse.getDistrictName());
				map.put("PIN_CODE", appointmentOrderPdfResponse.getPinCode());
				map.put("REPORT_NAME", "APPOINTMENT_ORDER");
				map.put("SUB_CONTENT", "");
				map.put("OFFICE_ADDRESS", appointmentOrderPdfResponse.getOfficeAddress());
				map.put("REF_CONTENT_ADVERTISEMENT", "");
				map.put("REF_CONTENT_WRITTEN_TEST_DATE", "");
				map.put("REF_CONTENT_INTERVIEW_DATE", "");
				map.put("DISTRICT_NAME", appointmentOrderPdfResponse.getDistrictName());

				params.put("map", map);
				params.put("termsAndConditionsList", termsAndConditionsList);

				List<Object> data = new ArrayList<Object>();
				Object object = new Object();
				data.add(object);
				JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(data);
				report = JasperFillManager.fillReport(jasperReportMCQon, params, datasource);

				ByteArrayInputStream bais = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(report));

				if (fileSize > 1) {
					JasperExportManager.exportReportToPdfFile(report,
							appointmentOrderPath + appoinment.getRegistrationNumber() + "_appointmentOrder.pdf");
				} else {
					downloadFile = new DefaultStreamedContent(bais, "pdf",
							appoinment.getRegistrationNumber() + "_appointmentOrder.pdf");
				}
				selectedAppointmentOrderList = new ArrayList<AppointmentOrderDetail>();
			} else {
				errorMap.notify(response.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception in Call Letter Generation", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedAppointment = ((AppointmentOrderDetail) event.getObject());

		if (selectedAppointment.getStatus().equalsIgnoreCase("Approved")) {
			deleteFlag = true;
			approveButtonFlag = true;
			generateFlag = false;
			approveButtonFlag = true;
			addButtonFlag = true;
			esrButtonFlag = false;
			addnotesButtonFlag = false;

		} else if (selectedAppointment.getStatus().equalsIgnoreCase("Rejected")) {
			approveButtonFlag = true;
			esrButtonFlag = true;
			addnotesButtonFlag = true;
			generateFlag = true;
			deleteFlag = false;
			addButtonFlag = false;
		} else if (selectedAppointment.getStatus().equalsIgnoreCase("InProgress")) {
			generateFlag = true;
			esrButtonFlag = true;
			addnotesButtonFlag = true;
			approveButtonFlag = false;
			deleteFlag = false;
			addButtonFlag = false;
		}
	}

	public String loadAppointmentById() {
		log.info("Load the AppointmentOrderDetail based on selected Id()");
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data loadJobApprovalList");
		try {

			if (selectedEsrAppointment == null) {
				errorMap.notify(ErrorDescription.SELECT_APPOINTMENT_ORDER.getErrorCode());
				return null;
			}
			Long id = selectedEsrAppointment.getId();
			appoinmentDTO.setId(id);
			appoinmentDTO.setEmailMobileValidation(mobileAndEmailValidation);
			String url = SERVER_URL + "/appointmentorder/generate";
			log.info("appointmentOrderCreate ESR URL==>" + url);
			baseDTO = httpService.post(url, appoinmentDTO);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			if (baseDTO.getStatusCode() == 0) {
				employeeMaster = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
				});
				employeeMasterBean.setEmployeeMaster(employeeMaster);
				employeeMasterBean.setAction("ADD");
				if (employeeMaster.getEmployeeType().getName().equals("Permanent")) {
					employeeMasterBean.setIsPermanent(true);
				} else {
					employeeMasterBean.setIsPermanent(false);
				}
				return "/pages/personnelHR/creatEmployeeServicePrimary.xhtml?faces-redirect=true";
			} else {
				if (baseDTO.getStatusCode().equals(ErrorDescription.EMPLOYEE_CONTACT_NUMBER_EXISTS.getErrorCode()) || baseDTO.getStatusCode().equals(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getCode())) {
					mobileAndEmailValidation = true;
					log.info("====Mobile Number is already Exists====");
					errorMap.notify(baseDTO.getStatusCode());
					getJobApplicationById();
					return "/pages/personnelHR/editEmpPrimayInfo.xhtml?faces-redirect=true";
				} else if (baseDTO.getStatusCode().equals(ErrorDescription.EMPLOYEE_EMIAL_ID_EXISTS.getErrorCode()) || baseDTO.getStatusCode().equals(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getCode()) ) {
					mobileAndEmailValidation = true;
					log.info("====Email-Id already Exists====");
					errorMap.notify(baseDTO.getStatusCode());
					getJobApplicationById();
					return "/pages/personnelHR/editEmpPrimayInfo.xhtml?faces-redirect=true";
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					return "/pages/personnelHR/appoinmentOrder/approvalAppointmentOrder.xhtml?faces-redirect=true";
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in while getting AppointmentOrderDetail  search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void getJobApplicationById() {
		log.info("====START AppointmentOrderBean.getJobApplicationById====");
		try {
			String url = SERVER_URL + "/jobapplication/getbyappoinmentId/" + selectedEsrAppointment.getId();
			log.info("=====getJobApplicationById URL is====" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				jobApplication = mapper.readValue(jsonResponse, new TypeReference<JobApplication>() {
				});
				appoinmentDTO.setContactNumber(jobApplication.getMobileNumber());
				appoinmentDTO.setEmailId(jobApplication.getEmailId());
				log.info("====jobApplication is====" + jobApplication.toString());
			}
		} catch (Exception e) {
			log.info("====Exception occured in AppointmentOrderBean.getJobApplicationById====");
			log.error("====Exception is====" + e.getMessage());
		}
		log.info("====END AppointmentOrderBean.getJobApplicationById====");
	}

	public String clearSelectedEmployee() {
		selectedEsrAppointment = new AppointmentOrderDetail();
		return "/pages/personnelHR/appoinmentOrder/approvalAppointmentOrder.xhtml?faces-redirect=true";
	}

	public void loadEntityAddress() {
		StringBuilder address = new StringBuilder();
		if (region.getAddressMaster() != null) {
			if (region.getAddressMaster().getAddressLineOne() != null) {
				address.append(region.getAddressMaster().getAddressLineOne() + ", ");
			}
			if (region.getAddressMaster().getAddressLineTwo() != null) {
				address.append(region.getAddressMaster().getAddressLineTwo() + ", ");
			}
			if (region.getAddressMaster().getAddressLineThree() != null) {
				address.append(region.getAddressMaster().getAddressLineThree() + ", ");
			}
			if (region.getAddressMaster().getAreaMaster() != null
					&& region.getAddressMaster().getAreaMaster().getName() != null) {

				address.append(region.getAddressMaster().getAreaMaster().getName() + ", ");
			}
			if (region.getAddressMaster().getTalukMaster() != null
					&& region.getAddressMaster().getTalukMaster().getName() != null) {

				address.append(region.getAddressMaster().getTalukMaster().getName() + ", ");

			}
			if (region.getAddressMaster().getDistrictMaster() != null
					&& region.getAddressMaster().getDistrictMaster().getName() != null) {

				address.append(region.getAddressMaster().getDistrictMaster().getName() + ", ");

			}
			if (region.getAddressMaster().getStateMaster() != null
					&& region.getAddressMaster().getStateMaster().getName() != null) {

				address.append(region.getAddressMaster().getStateMaster().getName() + ", ");

			}
			if (region.getAddressMaster().getCityMaster() != null
					&& region.getAddressMaster().getCityMaster().getName() != null) {

				address.append(region.getAddressMaster().getCityMaster().getName() + " - ");

			}
			if (region.getAddressMaster().getPostalCode() != null) {
				address.append(region.getAddressMaster().getPostalCode() + ".");
			}
		}

		entityAddress = address.toString();
	}

	public void saveEntity() {
		try {
			log.info("Appointment order size--->" + selectedAppointmentOrderList.size());

			selectedAppointmentOrderList.forEach(appointment -> {
				appointment.setModifiedDate(new Date());
				appointment.setModifiedBy(loginBean.getUserDetailSession());
				appointment.setEntityAddress(entityAddress);
				appointment.setEntityMaster(region);
				appointment.setConsolidatePay(consolidatedPay);
				appointment.setPayScaleMaster(payScale);

			});
			AppointmentOrderDetail request = new AppointmentOrderDetail();
			request.setAppointmentOrderDetailList(appointmentOrderList);
			BaseDTO response = httpService.post(SERVER_URL + "/appointmentorder/create", request);
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.WRITTEN_EXAM_MARK_SUCCESSFULLY_ADDED.getErrorCode());
				// entityAddress = null;
				// region = null;
				// payScale = null;
				// consolidatedPay = null;
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('addNotes').hide();");
			}

		} catch (Exception e) {
			log.error(" Exception while saving appoinment order :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void cancelAddEntity() {
		log.info("dfdfsdfs");

	}

	public void addEntity() {
		entityAddress = null;
		region = null;
		consolidatedPay = null;
		if (selectedAppointmentOrderList == null || selectedAppointmentOrderList.size() == 0) {
			errorMap.notify(ErrorDescription.SELECT_APPOINTMENT_ORDER.getErrorCode());
			return;
		}
		region = selectedAppointmentOrderList.get(0).getEntityMaster() == null ? null
				: selectedAppointmentOrderList.get(0).getEntityMaster();
		if (region != null) {
			loadEntityAddress();
		} else {
			entityAddress = "";
		}
		consolidatedPay = selectedAppointmentOrderList.get(0).getConsolidatePay() == null ? consolidatedPay
				: selectedAppointmentOrderList.get(0).getConsolidatePay();
		// entityAddress = null;
		// region = null;
		// //payScale = null;
		// consolidatedPay = null;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addNotes').show();");
	}
}
