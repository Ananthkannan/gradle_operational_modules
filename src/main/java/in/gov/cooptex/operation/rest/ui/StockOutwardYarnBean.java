package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CurrentStockStatusRequest;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardYarnBean")
@Scope("session")
public class StockOutwardYarnBean {
	

	private final String STOCK_OUTWARD_LIST = "/pages/inspectionCenter/outward/listStockOutwardYarn.xhtml?faces-redirect=true;";

	private final String STOCK_OUTWARD_CREATE = "/pages/inspectionCenter/outward/createStockOutwardYarn.xhtml?faces-redirect=true;";

	private final String STOCK_OUTWARD_VIEW = "/pages/inspectionCenter/outward/viewStockOutwardYarn.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	StockTransferType stockTransferType;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnProductAdd = true;

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	List<EntityMaster> showroomList;


	@Getter
	@Setter
	int totalRecords;

	@Getter
	@Setter
	Boolean addFlag;
	
	@Getter
	@Setter
	Boolean viewFlag;
	
	@Getter
	@Setter
	Boolean editFlag;

	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}

	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		EntityMaster loginWorkLocationEntity = getLoginEmployeeWorkLocation();

		String loginEntityCode = loginWorkLocationEntity.getEntityTypeMaster().getEntityCode();
		if (loginEntityCode.equals(EntityType.YARN_INWARD)) {
			stockTransferType = StockTransferType.YARN_INWARD;
		}else if (loginEntityCode.equals(EntityType.YARN_OUTWARD)) {
			stockTransferType = StockTransferType.YARN_OUTWARD;
		}
		log.info("stockTransferType :"+stockTransferType);
		if (page.equals("ADD")) {
			return gotoAddPage();
		} else if (page.equals("EDIT")) {
			return gotoEditPage();
		} else if (page.equals("VIEW")) {
			return gotoViewPage();
		} else {
			return getListPage();
		}

	}

	/**********************************************************************************/

	public String getListPage() {

		log.info("Going to list page");

		disableAddButton = false;

		SERVER_URL = stockTransferUtility.loadServerUrl();

		statusValues = stockTransferUtility.loadStockTransferStatusList();

		stockTransferResponse = new StockTransferResponse();

		loadLazyList();

		return STOCK_OUTWARD_LIST;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();

		request.setStockTransferType(stockTransferType);
		if (getLoginEmployeeWorkLocation() != null && getLoginEmployeeWorkLocation().getId() != null) {
			request.setId(getLoginEmployeeWorkLocation().getId());
		}

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("senderCodeOrName")) {
				request.setSenderCodeOrName(value);
			}

			if (entry.getKey().equals("stockMovementTo")) {
				request.setStockMovementTo(value);
			}

			if (entry.getKey().equals("receiverCodeOrName")) {
				request.setReceiverCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dateTransferred")) {
				request.setDateTransferred(AppUtil.serverDateFormat(value));
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/list";

		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();

						for (StockTransferResponse response : data) {
							if (response.getSocietyCode() == null && response.getSocietyName() == null) {
								response.setSocietyCode(response.getDnpCode().toString());
								response.setSocietyName(response.getDnpName());
							}
						}
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
	}

	/*********************************************************************************************************************************/

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	String entityTypeCode;

	public String gotoAddPage() {
		disableBtnProductAdd = true;
		disableAddProduct = true;
		stockTransferItems = new StockTransferItems();
		stockTransfer = new StockTransfer();
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		entityTypeCode = null;
		stockTransferResponse = new StockTransferResponse();
		currentStockStatusRequest = new CurrentStockStatusRequest();
		/*
		 * Login Entity set as a From Entity
		 */

		stockTransfer.setFromEntityMaster(loginEmployee.getPersonalInfoEmployment().getWorkLocation());

		entityTypeMasterList = commonDataService.findAllEntityTypes();

		return STOCK_OUTWARD_CREATE;
	}

	public void onChangeEntityTypeMaster() {
		log.info("onChangeEntityTypeMaster is called [" + entityTypeCode + "]");

		if (entityTypeCode != null && entityTypeCode != "") {
			showroomList = commonDataService.findEntityByEntityType(entityTypeCode);

			removeCurrentLoggedInItem(showroomList, getLoginEmployeeWorkLocation().getId());

		} else {
			showroomList = null;
		}
	}

	public void removeCurrentLoggedInItem(List<EntityMaster> entityMasterList, Long idToRemove) {
		Iterator<EntityMaster> it = entityMasterList.iterator();
		while (it.hasNext()) {
			EntityMaster em = it.next();
			if (em.getId().longValue() == idToRemove.longValue()) {
				it.remove();
			}
		}
	}

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	CurrentStockStatusRequest currentStockStatusRequest;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	boolean disableAddProduct = false;

	public void onChangeShowroom() {
		log.info("onChangeShowroom is called ");

		if (stockTransfer.getToEntityMaster() != null) {
			log.info("Selected ISSR Showroom is [" + stockTransfer.getToEntityMaster() + "]");

			productCategoryList = commonDataService.loadProductCategories();
			currentStockStatusRequest = new CurrentStockStatusRequest();
			disableBtnProductAdd = false;
			populateTransportFormData();
		}
	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");
		currentStockStatusRequest.setProductGroupMaster(null);
		currentStockStatusRequest.setProductVarietyMaster(null);

		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductCategory() != null) {
			log.info("Selected Product Category " + currentStockStatusRequest.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(currentStockStatusRequest.getProductCategory());
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();

		}
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + currentStockStatusRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(currentStockStatusRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");
		stockTransferItems = new StockTransferItems();
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + currentStockStatusRequest.getProductVarietyMaster().getCode());
			StockTransferItems items = commonDataService.findInventoryItems(stockTransfer.getFromEntityMaster().getId(),
					currentStockStatusRequest.getProductVarietyMaster().getId());

			if (items != null) {
				stockTransferItems = items;
				disableAddProduct = false;
			} else {
				disableAddProduct = true;
			}
		}
	}

	public String validateCurrentDispatchQty() {

		log.info("stockTransferItems.getDispatchedQty() " + stockTransferItems.getDispatchedQty());
		log.info("stockTransferItems.getReceivedQty() " + stockTransferItems.getReceivedQty());
		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getReceivedQty() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				return null;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getReceivedQty()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				return null;
			}
		}

		return null;
	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	Set<String> setOfBundleNumbers = new HashSet<>();

	public String addProduct() {

		log.info("Product is being added [" + stockTransferItems + "]");

		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getReceivedQty() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				return null;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getReceivedQty()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				return null;
			}
		}

		stockTransferItems.setReceivedQty(0.0);
		stockTransfer.getStockTransferItemsList().add(stockTransferItems);

		if (stockTransferItems.getBundleNumber() != null) {
			setOfBundleNumbers.add(stockTransferItems.getBundleNumber());
			updateBundleNumbers(setOfBundleNumbers);
		}

		stockTransferItems = new StockTransferItems();

		currentStockStatusRequest.setProductVarietyMaster(null);

		return null;
	}

	public void removeProduct(StockTransferItems stockTransferItems) {

		log.info("Product is being added [" + stockTransferItems + "]");

		stockTransfer.getStockTransferItemsList().remove(stockTransferItems);

		setOfBundleNumbers = new HashSet<>();
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				if (items.getBundleNumber() != null) {
					setOfBundleNumbers.add(items.getBundleNumber());
				}
			}
		}
		updateBundleNumbers(setOfBundleNumbers);
	}

	public String gotoEditPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED.name())) {
			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());
			disableBtnProductAdd = false;

			if (stockTransfer.getTransportMaster() != null) {
				selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
			}

			if (selectedTransportTypeMaster != null) {
				transportMasterList = commonDataService
						.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
			}

			showroomList = commonDataService.findEntityByEntityType(EntityType.INSTITUTIONAL_SALES_SHOWROOM);
			populateTransportFormData();
			productCategoryList = commonDataService.loadProductCategories();
			currentStockStatusRequest = new CurrentStockStatusRequest();
			disableBtnProductAdd = false;
			stockTransferItems = new StockTransferItems();
			disableAddProduct = true;
		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

		return STOCK_OUTWARD_CREATE;
	}

	public String gotoViewPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());
		return STOCK_OUTWARD_VIEW;
	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {
			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferResponse.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	/***************************************************************************************************************************************/

	/*********************************************************************************/
	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<Object> payToPayList;

	public void populateTransportFormData() {
		transportTypeMasterList = commonDataService.getAllTransportTypes();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
	}

	public void onChangeTransportTypeMaster() {
		transportMasterList = null;
		stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void onChangeTransport() {
		log.info("onSelectTransport is called");
		if (stockTransfer.getTransportMaster() != null) {
			log.info("Selected Transport ID = [" + stockTransfer.getTransportMaster().getId() + "]");
		}
	}

	/*************************************************************************************************************************************/

	public String submit(String action) {

		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}

		stockTransfer.setDateTransferred(new Date());
		stockTransfer.setTransferType(stockTransferType);

		return create();
	}

	public String create() {
		log.info("create method is executing..");

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/inspection/center/outward/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return STOCK_OUTWARD_LIST;
	}

}
