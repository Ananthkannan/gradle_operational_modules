package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.FuelType;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("fuelTypeBean")
@Scope("session")
@Log4j2
public class FuelTypeBean {

	final String LIST_PAGE = "/pages/masters/fueltype/listFuelType.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/fueltype/createFuelType.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String FUEL_TYPE_URL = AppUtil.getPortalServerURL() + "/fueltype";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	FuelType fuelType, selectedFuelType;

	@Getter
	@Setter
	LazyDataModel<FuelType> lazyFuelTypeList;

	List<FuelType> fuelTypeList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showFuelTypeListPage() {
		log.info("<==== Starts FuelTypeBean.showFuelTypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		fuelType = new FuelType();
		selectedFuelType = new FuelType();
		loadLazyFuelTypeList();
		log.info("<==== Ends FuelTypeBean.showFuelTypeListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyFuelTypeList() {
		log.info("<===== Starts FuelTypeBean.loadLazyFuelTypeList ======>");

		lazyFuelTypeList = new LazyDataModel<FuelType>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<FuelType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = FUEL_TYPE_URL + "/getallfueltypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						fuelTypeList = mapper.readValue(jsonResponse, new TypeReference<List<FuelType>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return fuelTypeList;
			}

			@Override
			public Object getRowKey(FuelType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FuelType getRowData(String rowKey) {
				for (FuelType fuelType : fuelTypeList) {
					if (fuelType.getId().equals(Long.valueOf(rowKey))) {
						selectedFuelType = fuelType;
						return fuelType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends FuelTypeBean.loadLazyFuelTypeList ======>");
	}

	public String showFuelTypeListPageAction() {
		log.info("<===== Starts FuelTypeBean.showFuelTypeListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create") && (selectedFuelType == null || selectedFuelType.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_FUEL_TYPE.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create fuelType called..");
				fuelType = new FuelType();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit fuelType called..");
				String url = FUEL_TYPE_URL + "/getbyid/" + selectedFuelType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					fuelType = mapper.readValue(jsonResponse, new TypeReference<FuelType>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete fuelType called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFuelTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showFuelTypeListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends FuelTypeBean.showFuelTypeListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts FuelTypeBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(FUEL_TYPE_URL + "/deletebyid/" + selectedFuelType.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFuelTypeDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete fuelType ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends FuelTypeBean.deleteConfirm ===========>");
		return showFuelTypeListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts FuelTypeBean.saveOrUpdate =======>");
		try {
			if (fuelType.getCode() == null || fuelType.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_CODE_EMPTY.getCode());
				return null;
			}
			if (fuelType.getName() == null || fuelType.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_NAME_EMPTY.getCode());
				return null;
			}
			if (fuelType.getName().trim().length() < 3 || fuelType.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (fuelType.getLocalName() == null || fuelType.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_LNAME_EMPTY.getCode());
				return null;
			}
			if (fuelType.getLocalName().trim().length() < 3 || fuelType.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (fuelType.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.FUEL_TYPE_STATUS_EMPTY.getCode());
				return null;
			}
			String url = FUEL_TYPE_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, fuelType);
			if (response != null && response.getStatusCode() == 0) {
				log.info("fuelType saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.FUEL_TYPE_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.FUEL_TYPE_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends FuelTypeBean.saveOrUpdate =======>");
		return showFuelTypeListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FuelTypeBean.onRowSelect ========>" + event);
		selectedFuelType = ((FuelType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends FuelTypeBean.onRowSelect ========>");
	}

}
