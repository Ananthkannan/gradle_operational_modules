package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.SaleTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("salesTypeMasterBean")
@Scope("session")
@Log4j2
public class SalesTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listSalesType.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createSalesType.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/viewSalesType.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String SALES_TYPE_URL = AppUtil.getPortalServerURL()+"/salestype";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	SaleTypeMaster salesType = new SaleTypeMaster();
	
	@Getter @Setter
	SaleTypeMaster selectedSalesType = new SaleTypeMaster();

	@Getter @Setter
	LazyDataModel<SaleTypeMaster> lazySalesTypeList;

	List<SaleTypeMaster> salesTypeList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showSalesTypeListPage() {
		log.info("<==== Starts SalesTypeMasterBean.showSalesTypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag=true;
		salesType = new SaleTypeMaster();
		selectedSalesType = new SaleTypeMaster();
		loadLazySalesTypeList();
		log.info("<==== Ends SalesTypeMasterBean.showSalesTypeListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazySalesTypeList() {
		log.info("<===== Starts SalesTypeMasterBean.loadLazySalesTypeList ======>");

		lazySalesTypeList= new LazyDataModel<SaleTypeMaster>() {
		

			/**
			 * 
			 */
			private static final long serialVersionUID = 1850624068742224216L;

			@Override
			public List<SaleTypeMaster> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SALES_TYPE_URL+ "/getallsalestypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						salesTypeList= mapper.readValue(jsonResponse, new TypeReference<List<SaleTypeMaster>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return salesTypeList;
			}

			@Override
			public Object getRowKey(SaleTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SaleTypeMaster getRowData(String rowKey) {
				for (SaleTypeMaster saleType : salesTypeList) {
					if (saleType .getId().equals(Long.valueOf(rowKey))) {
						selectedSalesType = saleType ;
						return saleType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  SalesTypeMasterBean.loadLazySalesTypeList  ======>");
	}


	public String showSalesTypeListPageAction() {
		log.info("<===== Starts SalesTypeMasterBean.showSalesTypeListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedSalesType== null || selectedSalesType.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_SALESTYPE.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create SalesType called..");
				salesType = new SaleTypeMaster();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit SalesType called..");
				String url=SALES_TYPE_URL+"/getbyid/"+selectedSalesType.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					salesType = mapper.readValue(jsonResponse,new TypeReference<SaleTypeMaster>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else if(action.equalsIgnoreCase("View")){
				log.info("View community called..");

				String url=SALES_TYPE_URL+"/getbyid/"+selectedSalesType.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					salesType = mapper.readValue(jsonResponse,new TypeReference<SaleTypeMaster>(){
					});	
					return VIEW_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete community called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSalesTypeDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showSalesTypeListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends SalesTypeMasterBean.showSalesTypeListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts SalesTypeMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SALES_TYPE_URL+"/deletebyid/"+selectedSalesType.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SALESTYPE_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSalesTypeDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete salesType ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SalesTypeMasterBean.deleteConfirm ===========>");
		return  showSalesTypeListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts SalesTypeMasterBean.saveOrUpdate =======>");
		try {
			if(salesType.getCode() == null || salesType.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.SALESTYPE_CODE_EMPTY.getCode());
				return null;
			}
			if(salesType.getName() == null || salesType.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.SALESTYPE_NAME_EMPTY.getCode());
				return null;
			}
			if(salesType.getName().trim().length() < 3 || salesType.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.SALESTYPE_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(salesType.getLname()== null || salesType.getLname().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.SALESTYPE_LNAME_EMPTY.getCode());
				return null;
			}
			if(salesType.getLname().trim().length() < 3 || salesType.getLname()	.trim().length() > 75) {
				errorMap.notify(ErrorDescription.SALESTYPE_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(salesType.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.SALESTYPE_STATUS_EMPTY.getCode());
				return null;
			}
			String url = SALES_TYPE_URL+"/saveorupdate";
			BaseDTO response = httpService.post(url,salesType);
			if(response != null && response.getStatusCode() == 0) {
				log.info("salesType saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.SALESTYPE_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.SALESTYPE_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends SalesTypeMasterBean.saveOrUpdate =======>");
		return showSalesTypeListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SalesTypeMasterBean.onRowSelect ========>"+event);
		selectedSalesType= ((SaleTypeMaster) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends SalesTypeMasterBean.onRowSelect ========>");
    }
	
}
