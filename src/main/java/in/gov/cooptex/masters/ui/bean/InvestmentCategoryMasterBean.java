package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.InvestmentCategoryMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("investmentCategoryMasterBean")
public class InvestmentCategoryMasterBean {
	@Getter
	@Setter
	InvestmentCategoryMaster investmentCategoryMaster, selectedInvestmentCategoryMaster;

	@Getter
	@Setter
	LazyDataModel<InvestmentCategoryMaster> lazyInvestmentCategoryMasterList;

	List<InvestmentCategoryMaster> investmentCategoryMasterList = null;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	String action;

	@Autowired
	AppPreference appPreference;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;
	
	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	private final String CREATE_PAGE = "/pages/masters/createInvestmentCategory.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listInvestmentCategory.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE = "/pages/masters/viewInvestmentCategory.xhtml?faces-redirect=true;";

	public String showInvestmentCategoryMasterListPageAction() {
		log.info("<===== Starts investmentCategoryMaster.showInvestmentCategoryMasterListPageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedInvestmentCategoryMaster == null || selectedInvestmentCategoryMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create investmentCategoryMaster called..");
				investmentCategoryMaster = new InvestmentCategoryMaster();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit investmentCategoryMaster called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/investmentcategorymaster/get/"
						+ selectedInvestmentCategoryMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					investmentCategoryMaster = mapper.readValue(jsonResponse,
							new TypeReference<InvestmentCategoryMaster>() {
							});
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View InvestmentCategoryMaster called..");

				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/investmentcategorymaster/get/"
						+ selectedInvestmentCategoryMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						investmentCategoryMaster = mapper.readValue(jsonResponse,
								new TypeReference<InvestmentCategoryMaster>() {
								});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete InvestmentCategoryMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInvestmentCategoryMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showInvestmentCategoryMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends InvestmentCategoryMasterBean.showInvestmentCategoryMasterListPageAction ===========>"
				+ action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts InvestmentCategoryMasterBean.deleteConfirm ===========>"
				+ selectedInvestmentCategoryMaster.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/investmentcategorymaster/deletebyid/" + selectedInvestmentCategoryMaster.getId());

			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInvestmentCategoryMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete InvestmentCategoryMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends InvestmentCategoryMasterBean.deleteConfirm ===========>");
		return showInvestmentCategoryMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts InvestmentCategoryMaster.saveOrUpdate =======>" + investmentCategoryMaster.getLocName());
		try {
			if (investmentCategoryMaster.getCode() == null || investmentCategoryMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_CODE_EMPTY.getCode());
				return null;
			}
			if (investmentCategoryMaster.getName() == null || investmentCategoryMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_NAME_EMPTY.getCode());
				return null;
			}
			if (investmentCategoryMaster.getLocName() == null
					|| investmentCategoryMaster.getLocName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_LOC_NAME_EMPTY.getCode());
				return null;
			}

			if (investmentCategoryMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_STATUS_EMPTY.getCode());
				return null;
			}

			if (investmentCategoryMaster.getCode().trim().length() < 3
					|| investmentCategoryMaster.getCode().trim().length() > 75) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_CODE_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (investmentCategoryMaster.getName().trim().length() < 3
					|| investmentCategoryMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (investmentCategoryMaster.getLocName().trim().length() < 3
					|| investmentCategoryMaster.getLocName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_LOC_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			
			if(investmentCategoryMaster.getId()==null || investmentCategoryMaster.getId()==0) {
			String duplicateurl = SERVER_URL + appPreference.getOperationApiUrl() + "/investmentcategorymaster/checkDuplicateCode";
			BaseDTO checkDuplicate = httpService.post(duplicateurl, investmentCategoryMaster);
			if(checkDuplicate!=null  &&  checkDuplicate.getMessage().equalsIgnoreCase("YES")){
				AppUtil.addError("Investment category code already exists");
				return null;
			}
			}

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/investmentcategorymaster/saveorupdate";
			BaseDTO response = httpService.post(url, investmentCategoryMaster);

			if (response != null && response.getStatusCode() == 0) {
				log.info("investmentCategoryMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.INVESTMENTCATEGORYMASTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends investmentCategoryMaster.saveOrUpdate =======>");
		return showInvestmentCategoryMasterListPage();
	}

	public String showInvestmentCategoryMasterListPage() { 
		log.info("<==== Starts investmentCategoryMaster.showinvestmentCategoryMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		investmentCategoryMaster = new InvestmentCategoryMaster();
		selectedInvestmentCategoryMaster = new InvestmentCategoryMaster();
		loadLazyInvestmentCategoryMasterList();
		log.info("<==== Ends investmentCategoryMaster.showinvestmentCategoryMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyInvestmentCategoryMasterList() {
		log.info("<===== Starts investmentCategoryMasterBean.loadLazyInvestmentCategoryMasterList ======>");

		lazyInvestmentCategoryMasterList = new LazyDataModel<InvestmentCategoryMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<InvestmentCategoryMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/investmentcategorymaster/getallinvestmentcategorymasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						investmentCategoryMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<InvestmentCategoryMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("investmentCategoryMasterList size---------------->"
								+ lazyInvestmentCategoryMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return investmentCategoryMasterList;
			}

			@Override
			public Object getRowKey(InvestmentCategoryMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public InvestmentCategoryMaster getRowData(String rowKey) {
				for (InvestmentCategoryMaster investmentCategoryMaster : investmentCategoryMasterList) {
					if (investmentCategoryMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedInvestmentCategoryMaster = investmentCategoryMaster;
						return investmentCategoryMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  investmentCategoryMasterBean.loadLazyInvestmentCategoryMasterList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts InvestmentCategoryMasterBean.onRowSelect ========>" + event);
		selectedInvestmentCategoryMaster = ((InvestmentCategoryMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends InvestmentCategoryMaster.onRowSelect ========>" + selectedInvestmentCategoryMaster.getId());
	}

}
