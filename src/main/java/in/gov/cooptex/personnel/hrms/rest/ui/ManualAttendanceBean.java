package in.gov.cooptex.personnel.hrms.rest.ui;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.PostConstruct;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.InsuranceMasterDTO;
import in.gov.cooptex.core.dto.LogDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.AttendanceTypeMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ManualAttendance;
import in.gov.cooptex.core.model.ManualAttendanceDetails;
import in.gov.cooptex.core.model.ManualAttendanceLog;
import in.gov.cooptex.core.model.ManualAttendanceNote;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.operation.hrms.dto.EmployeeAdditionalChargeRequestDto;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("manualAttendanceBean")
public class ManualAttendanceBean extends CommonBean{

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	LazyDataModel<ManualAttendance> manualAttendanceList;

	@Getter
	@Setter
	ManualAttendance selectedManualAttendance;
	
	@Getter
	@Setter
	Boolean forwardFor,isIncrementEmployeeStatus=false;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;
	

	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	List<EntityMaster> hoentityList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> entityList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<>();

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<>();

	@Getter
	@Setter
	ManualAttendance manualAttendance = new ManualAttendance();

	@Getter
	@Setter
	Boolean isHeadOffice = false;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> selectedEmployeeList = new ArrayList<>();

	@Getter
	@Setter
	List<ManualAttendanceDetails> manualAttendanceDetails = new ArrayList<>();

	@Getter
	@Setter
	List<AttendanceTypeMaster> attendanceTypeList = new ArrayList<>();

	@Getter
	@Setter
	List<ManualAttendanceDetails> finalAttendanceDetails = new ArrayList<>();

	@Getter
	@Setter
	List<ManualAttendanceDetails> editedAttendanceDetails = new ArrayList<>();

	@Getter
	@Setter
	String focusProperty;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;
		
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	
	@Getter
	@Setter
	Date maxDate = null;
	
	@Getter
	@Setter
	Boolean viewFlag = false,attendanceFlag = false;
	
	@Getter @Setter
	List<LogDTO> logNoteEmployeeList = new ArrayList<>();
	
	@Getter
	@Setter
	EmployeeAdditionalChargeRequestDto viewResponseDTO = new EmployeeAdditionalChargeRequestDto();

	final String VIEW_PAGE = "/pages/personnelHR/manualAttendance/viewManualAttendace.xhtml?faces-redirect=true";

	final String APPROVING_PAGE = "/pages/personnelHR/manualAttendance/approvalManualAttendance.xhtml?faces-redirect=true";
	
	@Getter
	@Setter
	long inTime , outTime;
	
	@Getter
	@Setter
	UserMaster userMaster;
	
	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	public String loadManualAttendanceList() {
		log.info("ManualAttendanceBean.loadManualAttendanceList called");
		try {
			userMaster=new UserMaster();
			selectedManualAttendance = new ManualAttendance();
			editedAttendanceDetails = new ArrayList<>();
			manualAttendance = new ManualAttendance();
			selectedEmployeeList = new ArrayList<>();
			addButtonFlag = true;
			viewButtonFlag = false;
			deleteFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
//			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
//			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
			loadLazyManualAttendance();
			loadAllDeparmentList();
			//loadAllHOEntityList();
			loadHOorRO();
			loadAllEntityList();
			loadAllEntityTypeList();
			manualAttendance.setHeadOrRegionOffice(new EntityMaster());
			manualAttendance.setEntityTypeMaster(new EntityTypeMaster());
		} catch (Exception e) {
			log.error("Exception Occured while fetching ManualAttendanceList ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/manualAttendance/listManualAttendance.xhtml?faces-redirect=true";
	}

	public void loadLazyManualAttendance() {

		log.info("< --  Start Manual Attendance Lazy Load -- > ");
		manualAttendanceList = new LazyDataModel<ManualAttendance>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ManualAttendance> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<ManualAttendance> data = mapper.readValue(jsonResponse,
							new TypeReference<List<ManualAttendance>>() {
							});
					if (data == null) {
						log.info(" Manual Attendance search response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info("ManualAttendance :: totalRecords==> "+totalRecords);
					} else {
						log.error(" Manual Attendance search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Manual Attendance data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(ManualAttendance res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ManualAttendance getRowData(String rowKey) {
				List<ManualAttendance> responseList = (List<ManualAttendance>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (ManualAttendance res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedManualAttendance = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			ManualAttendance request = new ManualAttendance();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			Long userId = loginBean.getUserDetailSession().getId();
			searchFilters.put("loginUser", userId);
			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("headOrRegionOffice.name")) {
					log.info("Head office name : " + value);
					searchFilters.put("headOrRegionOffice", value);
				}

				if (entry.getKey().equals("entityMaster.name")) {
					log.info("entityManter name : " + value);

					searchFilters.put("entityMaster", value);
				}
				if (entry.getKey().equals("department.name")) {
					log.info("department name : " + value);

					searchFilters.put("department", value);
				}

				if (entry.getKey().equals("entityTypeMaster.entityName")) {
					log.info("entity type master : " + value);
					searchFilters.put("entityTypeMaster", value);

				}
				if (entry.getKey().equals("createdDate")) {
					log.info("created Date : " + value);

					searchFilters.put("createdDate", value);

				}
				if (entry.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/manualattendance/search", request);
			//log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Manual Attendance search generate data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void loadAllDeparmentList() {
		log.info("... Load Department List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/department/load");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			departmentList = mapper.readValue(jsonResponse, new TypeReference<List<Department>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Department....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void updateToDate() {
		focusProperty = "todate";
		if (manualAttendance.getFromDate() != null && manualAttendance.getToDate() != null
				&& (manualAttendance.getFromDate().after(manualAttendance.getToDate()))) {
			manualAttendance.setToDate(null);
		}
	}

	public void loadAllAttendanceTypeList() {
		log.info("... Load Attendance List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/attendancetype/getAll");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			attendanceTypeList = mapper.readValue(jsonResponse, new TypeReference<List<AttendanceTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Attendance....", e);
		}
	}

	public void loadAllHOEntityList() {
		log.info("... Load  HO/RO List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitymaster/getall/headandregionalOffice");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			hoentityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading HO/RO data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllEntityList() {
		log.info("... Load  Entity List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitymaster/getallactive");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			mapper = new ObjectMapper();
			/*
			 * EntityTypeMasterController.java - Method: getEntityType()
			 */
			BaseDTO response = httpService.get(SERVER_URL + "/entitytypemaster/getall/entitytype");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * 
	 */
	
	public void loadHOorRO() {
		log.info("... Load  loadHOorRO ....");
		try {
			Long userId = loginBean.getUserDetailSession().getId();
			String url = SERVER_URL + "/entitymaster/loadhoorro/"+ userId;
			log.info("loadHOorRO :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			hoentityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if(hoentityList != null && !hoentityList.isEmpty()) {
				log.info("hoentityList size==> "+hoentityList.size());
			}
			
		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		
	}
	
	
	public void loadAllEntityByRegionList() {
		log.info("... Load  Entity List by region called ....");
		try {
			log.info("<===== Head or RegionalOffice ======> " + manualAttendance.getHeadOrRegionOffice());
			if (manualAttendance != null && manualAttendance.getHeadOrRegionOffice() != null
					&& manualAttendance.getHeadOrRegionOffice().getEntityTypeMaster() != null
					&& manualAttendance.getHeadOrRegionOffice().getEntityTypeMaster().getEntityCode() != null
					&& manualAttendance.getHeadOrRegionOffice().getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
				isHeadOffice = true;
				return;
			}
			isHeadOffice = false;
			// log.info("loadAllEntityByRegionList :: entitycode==>
			// "+manualAttendance.getHeadOrRegionOffice().getEntityTypeMaster().getEntityCode());

			// if(manualAttendance.getEntityTypeMaster()!=null &&
			// manualAttendance.getHeadOrRegionOffice()!=null &&
			// manualAttendance.getHeadOrRegionOffice().getEntityTypeMaster().getEntityCode().equals("REGIONAL_OFFICE"))
			// {
			log.info("loadAllEntityByRegionList :: Head or region office id==> "
					+ manualAttendance.getHeadOrRegionOffice().getId());
			log.info(
					"loadAllEntityByRegionList :: Entity type id==> " + manualAttendance.getEntityTypeMaster().getId());

			/*
			 * EntityMasterController.java - Method: getEntityByHeadOrRegionalOffice
			 */
			String url = SERVER_URL + "/entitymaster/getall/getmanualAttenRegion/"
					+ manualAttendance.getHeadOrRegionOffice().getId() + "/"
					+ manualAttendance.getEntityTypeMaster().getId();
			log.info("loadAllEntityByRegionList :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			// }

		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * 
	 */
	public void loadAllEmployeeByWorkLocationAndDepartment() {
		log.info("... Load  Employee called ....");
		Long departmentId = 0L;
		try {
			selectedEmployeeList = new ArrayList<>();
			mapper = new ObjectMapper();
			String headOfficeId = null;
			if (isHeadOffice)
				headOfficeId = manualAttendance.getHeadOrRegionOffice() != null
						? manualAttendance.getHeadOrRegionOffice().getId().toString()
						: null;
			else
				headOfficeId = manualAttendance.getEntityMaster() != null
						? manualAttendance.getEntityMaster().getId().toString()
						: null;
			departmentId = manualAttendance.getDepartment() != null ? manualAttendance.getDepartment().getId() : null;
			/*
			 * EmployeeMasterController.java - Method: getAllByWOrkLocationAndDepartment
			 */
			log.info("headOfficeId==> "+headOfficeId);
			log.info("departmentId==> "+departmentId);
			BaseDTO response = httpService
					.get(SERVER_URL + "/employee/getallbyworklocation/" + headOfficeId + "/" + departmentId);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if(employeeList != null && !employeeList.isEmpty()) {
				log.info("employeeList size==> "+employeeList.size());
			}else {
				log.error("employeeList null or empty");
				selectedEmployeeList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception occured while loading Employee  data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public String beforeManualAttendanceApproval() {

		try {
			if (selectedManualAttendance == null) {
				//Util.addWarn("Please select one Manual Attendance");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLS_SELECT_ONE_MANUAL_ATTENDANCE).getCode());
				return null;
			} else {
				log.info("before Manual Attendance..." + selectedManualAttendance.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmManualAttendanceApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String confirmManualAttendance() {
		log.info("confirm approval....." + manualAttendance.getStatus());
		AtomicInteger approved = new AtomicInteger(0);
		AtomicInteger rejected = new AtomicInteger(0);
		AtomicInteger inProgress = new AtomicInteger(0);
		manualAttendance.getManualAttendanceDetails().forEach(c -> {
			if (finalAttendanceDetails.contains(c)) {
				if (manualAttendance.getStatus().equalsIgnoreCase("FINAL_APPROVED")) {
					c.setStatus(ApprovalStatus.FINAL_APPROVED.toString());
					approved.getAndIncrement();
				} else if (manualAttendance.getStatus().equalsIgnoreCase("REJECTED")) {
					c.setStatus(ApprovalStatus.REJECTED.toString());
					rejected.getAndIncrement();
				}

			}
			if (c.getStatus().equals(ApprovalStatus.INPROGRESS.toString()))
				inProgress.getAndIncrement();
		});

		if (inProgress.intValue() > 0) {
			manualAttendance.setStatus(ApprovalStatus.INPROGRESS.toString());
			manualAttendance.setFinalstage(ApprovalStatus.INPROGRESS.toString());
		}

		if (inProgress.intValue() == 0 && approved.intValue() > 0) {
			manualAttendance.setStatus(ApprovalStatus.FINAL_APPROVED.toString());
			manualAttendance.setFinalstage(ApprovalStatus.FINAL_APPROVED.toString());
		}

		if (inProgress.intValue() == 0 && approved.intValue() == 0) {
			manualAttendance.setStatus(ApprovalStatus.REJECTED.toString());
			manualAttendance.setFinalstage(ApprovalStatus.REJECTED.toString());
		}
		List<ManualAttendanceDetails> tempList = manualAttendance.getManualAttendanceDetails();
		tempList.addAll(editedAttendanceDetails);
		manualAttendance.setManualAttendanceDetails(tempList);

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.put(SERVER_URL + "/manualattendance/approve", manualAttendance);
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while approving manual attendance....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadManualAttendanceList();
	}

	public String deleteManualAttendance() {
		try {
			if (selectedManualAttendance == null) {
				//AppUtil.addWarn("Please select one Manual Attendance");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLS_SELECT_ONE_MANUAL_ATTENDANCE).getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmManualAttendanceDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting .... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	public String confirmDeleteManualAttendance() {
		log.info("... delete ManualAttendance called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.delete(SERVER_URL + "/manualattendance/delete/" + selectedManualAttendance.getId());
			if(response != null) {
				errorMap.notify(ErrorDescription.MANUAL_ATTENDANCE_DELETED_SUCCESSFULLY.getErrorCode());
				//errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadManualAttendanceList();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedManualAttendance = ((ManualAttendance) event.getObject());

		addButtonFlag = false;
		viewButtonFlag = true;
		editButtonFlag = false;
		Long loginUserId = loginBean.getUserDetailSession().getId();
		UserMaster createdBy = selectedManualAttendance.getCreatedBy();
		Long createdById = createdBy == null ? null : createdBy.getId();
		if(selectedManualAttendance != null && selectedManualAttendance.getStatus() != null) {
			attendanceFlag = true;
			if (selectedManualAttendance.getStatus().equalsIgnoreCase("FINAL_APPROVED")) {
				deleteFlag = false;
				approveButtonFlag = false;
				generateFlag = false;
	
			} else if (selectedManualAttendance.getStatus().equalsIgnoreCase("REJECTED")) {
				approveButtonFlag = false;
				generateFlag = true;
				deleteFlag = false;
				if(loginUserId != null && createdById != null && createdById.longValue() == loginUserId.longValue()) {
					editButtonFlag = true;
				}
			} else if (selectedManualAttendance.getStatus().equalsIgnoreCase("InProgress")) {
				generateFlag = true;
				approveButtonFlag = true;
				if(loginUserId != null && createdById != null && createdById.longValue() == loginUserId.longValue()) {
					editButtonFlag = true;
					deleteFlag = true;
				}
			}else if (selectedManualAttendance.getStatus().equalsIgnoreCase("APPROVED")) {
				deleteFlag = false;
				approveButtonFlag = false;
				generateFlag = false;
	
			} 
		}else {
			attendanceFlag = false;
		}
	}

	public String clear() {
		selectedManualAttendance = new ManualAttendance();
		addButtonFlag = true;
		editButtonFlag = false;
		viewButtonFlag = false;
		approveButtonFlag = false;
		deleteFlag = false;
		manualAttendance = new ManualAttendance();
		loadLazyManualAttendance();
		return "/pages/personnelHR/manualAttendance/listManualAttendance.xhtml?faces-redirect=true";
	}

	public String addManualAttendance() {
		log.info("Manual Attendance Add Called");
		manualAttendance = new ManualAttendance();
		entityList = new ArrayList<>();
		userMaster = new UserMaster();
		loadAllDeparmentList();
		//loadAllHOEntityList();
		loadHOorRO();
		loadAllEntityTypeList();
		loadAllAttendanceTypeList();
		isHeadOffice = false;
		generateFlag = false;
		selectedManualAttendance = new ManualAttendance();
		editedAttendanceDetails = new ArrayList<>();
		selectedEmployeeList = new ArrayList<>();
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
		return "/pages/personnelHR/manualAttendance/createManualAttendance.xhtml?faces-redirect=true";
	}
	

	public String editManualAttendance() {
		if (selectedManualAttendance == null) {
			//AppUtil.addWarn("Please Select one record");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			mapper = new ObjectMapper();
			// loadAllAttendanceTypeList();
			finalAttendanceDetails = new ArrayList<>();
			BaseDTO response = httpService
					.get(SERVER_URL + "/manualattendance/get/" + selectedManualAttendance.getId()+"/0");
			if (response != null || new Integer(response.getStatusCode()) != 90308) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				manualAttendance = mapper.readValue(jsonResponse, new TypeReference<ManualAttendance>() {
				});
				
				if(manualAttendance.getManualAttendanceNoteList() != null && !manualAttendance.getManualAttendanceNoteList().isEmpty()) {
					
					log.info("note size==> "+manualAttendance.getManualAttendanceNoteList().size());
					ManualAttendanceNote manualAttendanceNote = manualAttendance.getManualAttendanceNoteList().get(manualAttendance.getManualAttendanceNoteList().size()-1);
						log.info("manualAttendanceNote.getForwardTo().getId()==> "+manualAttendanceNote.getForwardTo().getId());
						manualAttendance.setForwardToId(manualAttendanceNote.getForwardTo().getId());
						userMaster = new UserMaster();
						userMaster = manualAttendanceNote.getForwardTo();
						log.info("manualAttendanceNote.getFinalApproval()==> "+manualAttendanceNote.getFinalApproval());
						manualAttendance.setFinalApprove(manualAttendanceNote.getFinalApproval()); 
						log.info("manualAttendanceNote.getNote()==> "+manualAttendanceNote.getNote());
						manualAttendance.setNote(manualAttendanceNote.getNote()); 
					
				}
				//forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
				/*List<ManualAttendanceDetails> tempList = new ArrayList<>();
				tempList = manualAttendance.getManualAttendanceDetails();
				tempList.forEach(matten -> {
					if (matten.getStatus().equals("FINAL_APPROVED") || matten.getStatus().equals("REJECTED")) {
						editedAttendanceDetails.add(matten);

					}
				}

				);*/
				manualAttendance.getManualAttendanceDetails().removeAll(editedAttendanceDetails);
				/*errorMap.notify(ErrorDescription.MANUAL_ATTENDANCE_FETCHED_SUCCESSFULLY.getErrorCode());
				return null;*/
			}else {
				if(response.getStatusCode() == 223445664) {

					errorMap.notify(
							ErrorDescription.getError(PersonnalErrorCode.MANUAL_ATTENDANCE_ALREADY_EXISTS).getCode());
					return null;
					
				}
			}

		} catch (Exception e) {
			log.error("editManualAttendance exception--------->", e);
		}
		return "/pages/personnelHR/manualAttendance/editManualAttendance.xhtml?faces-redirect=true";
	}

	public String updateManualAttendance() {
		UserMaster user = loginBean.getUserDetailSession();
		manualAttendance.setModifiedBy(user);


		if (finalAttendanceDetails == null || finalAttendanceDetails.isEmpty()) {
			//AppUtil.addWarn("Please select Manual Attendance Statement"); PLS_SELECT_MANUAL_ATTENDANCE_STATEMENT
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.PLS_SELECT_MANUAL_ATTENDANCE_STATEMENT).getCode());
			return null;
		}

		for (ManualAttendanceDetails manualAttendanceDetails : finalAttendanceDetails) {
			if ((manualAttendanceDetails.getInTime() == null || manualAttendanceDetails.getInTime().getHours() == 0)) {
				//AppUtil.addWarn("Please enter In Time");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_IN_TIME).getCode());
				return null;
			}
			if ((manualAttendanceDetails.getOutTime() == null
					|| manualAttendanceDetails.getOutTime().getHours() == 0)) {
				//AppUtil.addWarn("Please enter Out Time");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_OUT_TIME).getCode());
				return null;
			}
		}

		if (manualAttendance.getNote() == null || manualAttendance.getNote().isEmpty()) {
			//AppUtil.addWarn("Please enter the note");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_THE_NOTE).getCode());
			return null;
		}
		if(null == userMaster || null == userMaster.getId()) {
			AppUtil.addWarn("Please enter Forward To");
			return null;
		}
		if(null == manualAttendance.getFinalApprove()) {
			AppUtil.addWarn("Please enter Forward For");
			return null;
		}
		int attendanceMonth=AppUtil.getMonth(manualAttendance.getAttendancDate());
		int attendanceYear=AppUtil.getYear(manualAttendance.getAttendancDate());
		int currentMonth=AppUtil.getMonth(new Date());
		int currentYear=AppUtil.getYear(new Date());
		if(attendanceMonth!=currentMonth && attendanceYear!=currentYear) {
			AppUtil.addWarn("Previous Month Attendance Not Edited Current Month");
			return null;
		}
		manualAttendance.setForwardToId(userMaster.getId());
		manualAttendance.setManualAttendanceDetails(finalAttendanceDetails);
		editedAttendanceDetails = new ArrayList<>();
		mapper = new ObjectMapper();
		BaseDTO response = httpService.put(SERVER_URL + "/manualattendance/update", manualAttendance);
		try {
			if (response != null) {
				log.info("success.........");
				//errorMap.notify(response.getStatusCode());
				/*if (response.getStatusCode() != 90311) {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}*/ 
				if (response.getStatusCode().equals(ErrorDescription.MANUAL_ATTENDANCE_UPDATED_SUCCESSFULLY.getErrorCode())) {
					errorMap.notify(ErrorDescription.MANUAL_ATTENDANCE_UPDATED_SUCCESSFULLY.getErrorCode());
					return loadManualAttendanceList();
				}else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				}
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while saving manual attendance");
		}
		return null;
	}

	public String approveManualAttendance() {
		try {
			log.info("Approve roster...... called");
			if (selectedManualAttendance == null) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.PLS_SELECT_ONE_ROSTER).getCode());
				return null;
			}
			if (selectedManualAttendance.getStatus().equals("FINAL_APPROVED")) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.MANUAL_ATTENDANCE_ALREADY_APPROVED).getCode());
				return null;
			}
			if (selectedManualAttendance.getStatus().equals("REJECTED")) {
				//AppUtil.addWarn("Manual Attendance rejected once cannot be approved");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.REJECTED_CANNOT_BE_APPROVED).getCode());
				return null;
			}
			editedAttendanceDetails = new ArrayList<>();
			manualAttendance = selectedManualAttendance;

			finalAttendanceDetails = new ArrayList<>();
			log.info("Server url...." + SERVER_URL + "/manualattendance/get/" + selectedManualAttendance.getId());
			BaseDTO response = httpService
					.get(SERVER_URL + "/manualattendance/get/" + selectedManualAttendance.getId());

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			manualAttendance = mapper.readValue(jsonResponse, new TypeReference<ManualAttendance>() {
			});
			List<ManualAttendanceDetails> tempList = new ArrayList<>();
			tempList = manualAttendance.getManualAttendanceDetails();
			tempList.forEach(matten -> {
				if (matten.getStatus().equals("FINAL_APPROVED") || matten.getStatus().equals("REJECTED")) {
					editedAttendanceDetails.add(matten);

				}
			}

			);
			manualAttendance.getManualAttendanceDetails().removeAll(editedAttendanceDetails);
		} catch (Exception e) {
			log.error("Exception occured while approving Manual Attendance", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return "/pages/personnelHR/manualAttendance/approvalManualAttendance.xhtml?faces-redirect=true";
	}

	public String viewManualAttendance() {
		try {
			log.info("Approve roster...... called");
			if (selectedManualAttendance == null) {
				//AppUtil.addWarn("Please select one Manual Attendance");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLS_SELECT_ONE_MANUAL_ATTENDANCE).getCode());
				return null;
			}

			getViewNoteEmployeeDetails();
			manualAttendance = selectedManualAttendance;
			log.info("Server url...." + SERVER_URL + "/manualattendance/get/" + selectedManualAttendance.getId());
			BaseDTO response = httpService
					.get(SERVER_URL + "/manualattendance/get/" + selectedManualAttendance.getId()+"/"+systemNotificationId);

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
			
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			manualAttendance = mapper.readValue(jsonResponse, new TypeReference<ManualAttendance>() {
			});
			
			// Approve and Reject Comments
			getComments();
			
			if (selectedManualAttendance != null) {
				setPreviousApproval(selectedManualAttendance.getFinalApprove());
				if ("VIEW".equalsIgnoreCase(action)) {
					if (selectedManualAttendance.getForwardToId() != null) {
						if (loginBean.getUserMaster().getId().equals(selectedManualAttendance.getForwardToId())) {
							if (ApprovalStatus.REJECTED.equals(selectedManualAttendance.getFinalstage()) ||
									ApprovalStatus.REJECTED.name().equalsIgnoreCase(selectedManualAttendance.getStatus())) {
								approvalFlag = false;
							} else {
								approvalFlag = true;
							}
						} else {
							approvalFlag = false;
						}
					}
					setFinalApproveFlag(selectedManualAttendance.getFinalApprove());
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = selectedManualAttendance.getForwardToId();
					forwardFor = selectedManualAttendance.getFinalApprove();
					log.info("forwardFor=> "+forwardFor);
					log.info("loginUserId=> "+loginUserId);
					log.info("noteUserId=> "+noteUserId);
					if(loginUserId.equals(noteUserId)) {
						viewFlag = true;
					}else {
						viewFlag = false;
					}
					
					if(manualAttendance.getManualAttendanceLogList() != null && !manualAttendance.getManualAttendanceLogList().isEmpty()) {
						ManualAttendanceLog manualAttendanceLog = manualAttendance.getManualAttendanceLogList().get(manualAttendance.getManualAttendanceLogList().size()-1);
						if(manualAttendanceLog != null) {
							String stage = manualAttendanceLog.getStage();
							log.info("stage==> "+stage);
							if(stage.equals("FINAL_APPROVED")) {
								viewFlag = false;
							}
						}
						attendanceFlag = true;
					}else {
						attendanceFlag = false;
					}
					/*if (selectedManualAttendance.getFinalApprove()
							&& !selectedManualAttendance.getStatus()
									.equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())
							&& loginBean.getUserMaster().getId().equals(selectedManualAttendance.getForwardToId())) {

						buttonFlag = true;

						return APPROVING_PAGE;
					}*/
					selectedManualAttendance.setApproveremarks(null);

					return VIEW_PAGE;
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while approving Manual Attendance", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}
	
	public void getComments() {
		try {
			viewResponseDTO =  new EmployeeAdditionalChargeRequestDto();
			BaseDTO response = httpService
					.get(SERVER_URL + "/manualattendance/getapproverejectcomments/" + selectedManualAttendance.getId());
			if(response != null && response.getStatusCode()== ErrorDescription.SUCCESS_RESPONSE.getCode()) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeAdditionalChargeRequestDto>() {
						});
			}
		}catch (Exception e) {
			log.error("Exception occured while getComments", e);
		}
		
	}

	public void employeeSelection() {
		log.info("Employee Selection........");
	}

	public void GenerateEmployeeList() {
		log.info("Genrated.....");
		manualAttendance.setManualAttendanceDetails(new ArrayList<>());

		log.info("GenerateEmployeeList :: from date==> " + manualAttendance.getFromDate());
		log.info("GenerateEmployeeList :: to date==> " + manualAttendance.getToDate());

		//List<Date> listOfDates = getDatesBetween(manualAttendance.getFromDate(), manualAttendance.getToDate());
		Long id = 1l;
		Date startTime;
		manualAttendanceDetails = new ArrayList<>();
		Long empCount = Long.valueOf(commonDataService.getAppKeyValue("MANUAL_ATTENDANCE_EMP_COUNT"));
		log.info("empCount==> "+empCount);
		if(selectedEmployeeList.size() > empCount) {
			AppUtil.addError("Please select only " + String.valueOf(empCount) +" employee");
			/*errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.SELECT_FIVE_EMPLOYEE).getCode());*/
			return;
		}
		for (EmployeeMaster emp : selectedEmployeeList) {
				log.info("emp master------------>"+emp.getEmpNameWithPfNumber());
				ManualAttendanceDetails manual = new ManualAttendanceDetails();
				manual.setEmployeeName(emp.getFirstName());
				manual.setEmployeeNumber(emp.getEmpCode());
				if(StringUtils.isNotEmpty(emp.getEmpNameWithPfNumber())) {
					String pfNumber = StringUtils.substringBetween(emp.getEmpNameWithPfNumber(), "(", ")");
					manual.setPfNumber(pfNumber);
				}
				manual.setAttendanceDate(manualAttendance.getFromDate());
				manual.setEmployeeId(emp.getId());
				manual.setEmpAttId(id);
				Calendar calendar = Calendar.getInstance();
				calendar.setTime(new Date());
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				startTime = calendar.getTime();
				manual.setInTime(startTime);
				manual.setOutTime(startTime);
				calendar.set(Calendar.HOUR_OF_DAY, 0);
				calendar.set(Calendar.MINUTE, 0);
				startTime = calendar.getTime();

				manual.setMinHour(0l);
				manual.setMinMinute(0l);
				id = id + 1;
				manualAttendanceDetails.add(manual);
			
		}
		if (manualAttendanceDetails != null && manualAttendanceDetails.size() > 0) {
			generateFlag = true;
		}
		manualAttendance.setManualAttendanceDetails(manualAttendanceDetails);
	}

	public List<Date> getDatesBetween(Date startDate, Date endDate) {
		List<Date> result = new ArrayList<Date>();
		Calendar start = Calendar.getInstance();
		start.setTime(startDate);
		Calendar end = Calendar.getInstance();
		end.setTime(endDate);
		end.add(Calendar.DAY_OF_YEAR, 1); // Add 1 day to endDate to make sure endDate is included into the final list
		while (start.before(end)) {
			result.add(start.getTime());
			start.add(Calendar.DAY_OF_YEAR, 1);
		}
		return result;
	}

	public String saveManualAttendance() {
		List<Long> empId=new ArrayList<>();
		log.info("Start save Manual Attendance.....");
		if (manualAttendance.getFromDate() == null) {
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.MANUAL_ATTENDANCE_FROM_DATE_NOT_FOUND).getCode());
			return null;
		}
		/*if (manualAttendance.getToDate() == null) {
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.MANUAL_ATTENDANCE_TO_DATE_NOT_FOUND).getCode());
			return null;
		}*/
		if (finalAttendanceDetails == null || finalAttendanceDetails.isEmpty()) {
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.PLS_SELECT_MANUAL_ATTENDANCE_STATEMENT).getCode());
			return null;
		}
		
		
		
		for (ManualAttendanceDetails manualAttendanceDetails : finalAttendanceDetails) {
			if ((manualAttendanceDetails.getInTime() == null || manualAttendanceDetails.getInTime().getHours() == 0)) {
				//AppUtil.addWarn("Please enter In Time");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_IN_TIME).getCode());
						Calendar calendar = Calendar.getInstance();
				        calendar.setTime(new Date());
				        calendar.set(Calendar.MILLISECOND, 0);
				        calendar.set(Calendar.SECOND, 0);
				        calendar.set(Calendar.MINUTE, 0);
				        calendar.set(Calendar.HOUR, 0);
				        calendar.set(Calendar.HOUR_OF_DAY, 0);
				        manualAttendanceDetails.setOutTime(calendar.getTime());
				return null;
			}
			/*if ((manualAttendanceDetails.getOutTime() == null
					|| manualAttendanceDetails.getOutTime().getHours() == 0)) {
				//AppUtil.addWarn("Please enter Out Time");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_OUT_TIME).getCode());
				return null;
			}*/
			if (manualAttendanceDetails.getAttendanceType() == null) {
				//AppUtil.addWarn("Please enter Attendance type");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_ATTENDANCE_TYPE).getCode());
				return null;
			}
			if (manualAttendanceDetails.getShiftType() == null) {
				//AppUtil.addWarn("Please enter Shift type");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ENTER_SHIFT_TYPE).getCode());
				return null;
			}
			empId.add(manualAttendanceDetails.getEmployeeId());
		}
		
		
		//for(EmployeeMaster emp:selectedEmployeeList) {
			if(empId==null || empId.isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(PersonnalErrorCode.EMPLOYEE_DETAILS_DATA_IN_MANUAL_ATTENDANCE).getCode());
				return null;
			}
			
		//}

		if (manualAttendance.getNote() == null || manualAttendance.getNote().isEmpty()) {
			//AppUtil.addWarn("Please enter the note");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_THE_NOTE).getCode());
			return null;
		}
		
		if(null == userMaster ){
			AppUtil.addError("Please enter Forward For");
			return null;
		}

		log.info("saveManualAttendance :: from date==> " + manualAttendance.getFromDate());
		log.info("saveManualAttendance :: to date==> " + manualAttendance.getToDate());
		manualAttendance.setToDate(manualAttendance.getFromDate());
		manualAttendance.setAttendancDate(manualAttendance.getFromDate());
		UserMaster user = loginBean.getUserDetailSession();
		manualAttendance.setCreatedBy(user);
		manualAttendance.setForwardToId(userMaster.getId());
		log.info("Manul attendance...." + manualAttendance);
		manualAttendance.setStatus("InProgress");
		manualAttendance.setManualAttendanceDetails(finalAttendanceDetails);
		mapper = new ObjectMapper();
		BaseDTO response = httpService.post(SERVER_URL + "/manualattendance/add", manualAttendance);
		try {
			if (response != null) {
				log.info("Response recieved from RestAPI ..." + response);
				if (Long.valueOf(90307).equals(Long.valueOf(response.getStatusCode()))) {
					cancel();
					loadLazyManualAttendance();
					errorMap.notify(ErrorDescription.MANUAL_ATTENDANCE_ADDED_SUCCESSFULLY.getErrorCode());
					return "/pages/personnelHR/manualAttendance/listManualAttendance.xhtml?faces-redirect=true";
				} else if (response.getStatusCode().equals(
						ErrorDescription.getError(PersonnalErrorCode.MANUAL_ATTENDANCE_ALREADY_EXISTS).getCode())) {
					//AppUtil.addWarn(response.getMessage());
					 errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.MANUAL_ATTENDANCE_ALREADY_EXISTS).getErrorCode());
				}
			} else {
				log.info("Response from Rest API is null....");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while saving manual attendance", e);
		}
		return null;
	}

	public void onSelect(SelectEvent event) {
		ManualAttendanceDetails selectedManualAttendance = ((ManualAttendanceDetails) event.getObject());

		log.info("onSelect selected manual Attendance....." + selectedManualAttendance.getId());
	}

	public String cancel() {
		log.info("Start Cancel called.......");
		addButtonFlag = true;
		viewButtonFlag = false;
		deleteFlag = false;
		approveButtonFlag = false;
		editButtonFlag = false;
		loadLazyManualAttendance();
		selectedManualAttendance = new ManualAttendance();
		return "/pages/personnelHR/manualAttendance/listManualAttendance.xhtml?faces-redirect=true";
	}

	public void updateWorkDuration(AjaxBehaviorEvent event) {
		UIComponent component = event.getComponent();
		ManualAttendanceDetails manualDetails = (ManualAttendanceDetails) component.getAttributes().get("manAtte");

		log.info("updating work duration.......");
		
		if(manualDetails.getInTime() == null || manualDetails.getInTime().getHours() == 0) {
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_IN_TIME).getCode());
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.set(Calendar.MILLISECOND, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.HOUR, 0);
	        calendar.set(Calendar.HOUR_OF_DAY, 0);
	        manualDetails.setInTime(calendar.getTime());
			manualDetails.setOutTime(calendar.getTime());
			return;
		}
		
		DateTime in = new DateTime(manualDetails.getInTime());
		DateTime out = new DateTime(manualDetails.getOutTime());
		long hours = Hours.hoursBetween(in, out).getHours();
		long minutes = Minutes.minutesBetween(in, out).getMinutes();
		log.info("Hours...." + hours + "  minuites...." + minutes);
		minutes = minutes % 60;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, (int) hours);
		calendar.set(Calendar.MINUTE, (int) minutes);
		Date workDur = calendar.getTime();
		Date otDur;
		manualDetails.setTotalDuration(workDur);
		long othours = 0l;
		long otminutes = 0l;
		if (hours == 8 && minutes > 0) {
			othours = 0l;
			otminutes = minutes;
			calendar.set(Calendar.MINUTE, 0);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

		}
		if (hours > 8) {
			othours = hours - 8;
			otminutes = minutes;
			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 0);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

			calendar.set(Calendar.HOUR_OF_DAY, (int) othours);
			calendar.set(Calendar.MINUTE, (int) otminutes);
			otDur = calendar.getTime();
			manualDetails.setOverTime(otDur);
		}
		if (hours <= 8) {
			calendar.set(Calendar.HOUR_OF_DAY, (int) hours);
			calendar.set(Calendar.MINUTE, (int) minutes);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			otDur = calendar.getTime();
			manualDetails.setOverTime(otDur);
		}
		outTime = out.getMillis();
//		if(inTime != 0) {
//			if(inTime >= 12600000  && out.getMillis() <= 45000000) {
//				manualDetails.setShiftType("General");
//			}else{
//				manualDetails.setShiftType("night");
//			}
//			
//		}
	}

	public void updateMinhourMaxhour(AjaxBehaviorEvent event) {
		UIComponent component = event.getComponent();
		
		ManualAttendanceDetails manualDetails = (ManualAttendanceDetails) component.getAttributes().get("manAtte");
		DateTime in = new DateTime(manualDetails.getInTime());
		DateTime out = new DateTime(manualDetails.getOutTime());
		if(in.isAfter(out)) { 
			log.info("In time should less than Out time");
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.IN_TIME_OUT_TIME_VALIDATEION).getCode());
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.set(Calendar.MILLISECOND, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.HOUR, 0);
	        calendar.set(Calendar.HOUR_OF_DAY, 0);
			manualDetails.setInTime(calendar.getTime());
			manualDetails.setOutTime(calendar.getTime());
			manualDetails.setWorkDuration(null);
			manualDetails.setOverTime(null);
			manualDetails.setTotalDuration(null);
			return;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(manualDetails.getInTime());
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		manualDetails.setMinHour((long) hours);
		manualDetails.setMinMinute((long) minutes);
		if (manualDetails.getOverTime() != null) {
			updateWorkDuration(manualDetails);
		}
		inTime = in.getMillis();
//		if(outTime != 0) {
//			if(outTime <= 45000000 && in.getMillis() >= 12600000) {
//				manualDetails.setShiftType("General");
//			}else{
//				manualDetails.setShiftType("night");
//			}
//			
//		}
	}

	private void updateWorkDuration(ManualAttendanceDetails manualDetails) {
		log.info("updating work duration.......");
		DateTime in = new DateTime(manualDetails.getInTime());
		DateTime out = new DateTime(manualDetails.getOutTime());
		if(in.isAfter(out)) { 
			log.info("In time should less than Out time");
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.IN_TIME_OUT_TIME_VALIDATEION).getCode());
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.set(Calendar.MILLISECOND, 0);
	        calendar.set(Calendar.SECOND, 0);
	        calendar.set(Calendar.MINUTE, 0);
	        calendar.set(Calendar.HOUR, 0);
	        calendar.set(Calendar.HOUR_OF_DAY, 0);
			manualDetails.setInTime(calendar.getTime());
			manualDetails.setOutTime(calendar.getTime());
			manualDetails.setWorkDuration(null);
			manualDetails.setOverTime(null);
			manualDetails.setTotalDuration(null);
			return;
		}
		long hours = Hours.hoursBetween(in, out).getHours();
		long minutes = Minutes.minutesBetween(in, out).getMinutes();
		log.info("Hours...." + hours + "  minuites...." + minutes);
		minutes = minutes % 60;
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, (int) hours);
		calendar.set(Calendar.MINUTE, (int) minutes);
		Date workDur = calendar.getTime();
		Date otDur;
		manualDetails.setTotalDuration(workDur);
		long othours = 0l;
		long otminutes = 0l;
		if (hours == 8 && minutes > 0) {
			othours = 0l;
			otminutes = minutes;
			calendar.set(Calendar.MINUTE, 0);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

		}
		if (hours > 8) {
			othours = hours - 8;
			otminutes = minutes;
			calendar.set(Calendar.HOUR_OF_DAY, 8);
			calendar.set(Calendar.MINUTE, 0);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

			calendar.set(Calendar.HOUR_OF_DAY, (int) othours);
			calendar.set(Calendar.MINUTE, (int) otminutes);
			otDur = calendar.getTime();
			manualDetails.setOverTime(otDur);
		}
		if (hours < 8) {
			calendar.set(Calendar.HOUR_OF_DAY, (int) hours);
			calendar.set(Calendar.MINUTE, (int) minutes);
			workDur = calendar.getTime();
			manualDetails.setWorkDuration(workDur);

			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			otDur = calendar.getTime();
			manualDetails.setOverTime(otDur);
		}
	}

	public void updateTotalDuration(ManualAttendanceDetails manualDetails) {
		log.info("updating total duration.......");

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(manualDetails.getOverTime());
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		int minutes = calendar.get(Calendar.MINUTE);
		calendar.setTime(manualDetails.getWorkDuration());
		int whours = calendar.get(Calendar.HOUR_OF_DAY);
		int wminutes = calendar.get(Calendar.MINUTE);
		calendar.set(Calendar.HOUR_OF_DAY, hours + whours);
		calendar.set(Calendar.MINUTE, minutes + wminutes);
		Date totDur = calendar.getTime();
		manualDetails.setTotalDuration(totDur);
	}

	public String clearData() {
		selectedManualAttendance = new ManualAttendance();
		manualAttendance = new ManualAttendance();
		isHeadOffice = false;
		selectedEmployeeList = new ArrayList<>();
		return null;
	}

	public void validateDetails(SelectEvent event) {
		log.info("Validate details called");
		ManualAttendanceDetails selectedManualAttendanceDet = ((ManualAttendanceDetails) event.getObject());

		if (selectedManualAttendanceDet.getAttendanceType() == null) {
			//AppUtil.addWarn("Please enter Attendance type");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_ATTENDANCE_TYPE).getCode());
			finalAttendanceDetails.remove(selectedManualAttendanceDet);
		}
		if (selectedManualAttendanceDet.getAttendanceType().getName().equalsIgnoreCase("Manual")
				&& (selectedManualAttendanceDet.getInTime() == null
						|| selectedManualAttendanceDet.getInTime().getHours() == 0)) {
			//AppUtil.addWarn("Please enter In Time");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_IN_TIME).getCode());
			finalAttendanceDetails.remove(selectedManualAttendanceDet);

		} /*else if (selectedManualAttendanceDet.getAttendanceType().getName().equalsIgnoreCase("Manual")
				&& (selectedManualAttendanceDet.getOutTime() == null
						|| selectedManualAttendanceDet.getInTime().getHours() == 0)) {
			//AppUtil.addWarn("Please enter Out Time");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_OUT_TIME).getCode());
			finalAttendanceDetails.remove(selectedManualAttendanceDet);
		}*/ else if (selectedManualAttendanceDet.getShiftType() == null) {
			//AppUtil.addWarn("Please enter Shift type");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_SHIFT_TYPE).getCode());
		} else if (selectedManualAttendanceDet.getAttendanceType() == null) {
			//AppUtil.addWarn("Please enter Attendance type");
			errorMap.notify(
					ErrorDescription.getError(OperationErrorCode.ENTER_ATTENDANCE_TYPE).getCode());
		}
	}

	public String approveFlow() {
		BaseDTO baseDTO = null;
		try {
			if(userMaster != null && userMaster.getId() != null) {
				selectedManualAttendance.setForwardToId(userMaster.getId());
			}
			if (selectedManualAttendance.getForwardToId() == null) {
				errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDTO_REQUIRED.getErrorCode());
				return null;
			}
			
			if (selectedManualAttendance.getFinalApprove() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_TARGET_FINAL_APPROVE_REQUIRED.getErrorCode());
				return null;
			}
			
			if (selectedManualAttendance.getApproveremarks() == null || selectedManualAttendance.getApproveremarks().trim().length() == 0) {
				errorMap.notify(ErrorDescription.PLEASE_ENTER_APPROVE_COMMENTS.getErrorCode());
				return null;
			}
			log.info(" approveFLow StaggEligibilty id----------" + selectedManualAttendance.getId());
			baseDTO = new BaseDTO();
			if (previousApproval == false && selectedManualAttendance.getFinalApprove() == true) {
				selectedManualAttendance.setFinalstage(ApprovalStatus.APPROVED.toString());
			} else if (previousApproval == true && selectedManualAttendance.getFinalApprove() == true) {
				selectedManualAttendance.setFinalstage(ApprovalStatus.FINAL_APPROVED.toString());
			} else {
				selectedManualAttendance.setFinalstage(ApprovalStatus.APPROVED.toString());
			}
			log.info("approve remarks=========>" + selectedManualAttendance.getApproveremarks());
			String url = SERVER_URL + "/manualattendance/approveFlow";
			baseDTO = httpService.post(url, selectedManualAttendance);
			if (baseDTO.getStatusCode() == 0) {
				//Util.addWarn("Manual attendance approved Successfully");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.MANUAL_ATTENDANCE_APPROVED).getCode());
				log.info("Successfully FINAL_APPROVED-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadManualAttendanceList();
			}
		} catch (Exception e) {
			log.error("approveFLow method inside exception-------", e);
		}
		return null;
	}

	public String rejectFlow() {
		BaseDTO baseDTO = null;
		try {
			if (selectedManualAttendance.getApproveremarks() == null || selectedManualAttendance.getApproveremarks().trim().length() == 0) {
				errorMap.notify(ErrorDescription.PLEASE_ENTER_REJECT_COMMENTS.getErrorCode());
				return null;
			}
			log.info("approveFLow StaggEligibilty id----------" + selectedManualAttendance.getId());
			baseDTO = new BaseDTO();
			selectedManualAttendance.setFinalstage(ApprovalStatus.REJECTED.toString());
			String url = SERVER_URL + "/manualattendance/rejectFlow";
			baseDTO = httpService.post(url, selectedManualAttendance);
			if (baseDTO.getStatusCode() == 0) {
				//Util.addWarn("Manual attendance REJECTED Successfully");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.MANUAL_ATTENDANCE_REJECTED).getCode());
				log.info("Successfully REJECTED-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadManualAttendanceList();
			}
		} catch (Exception e) {
			log.error("rejectjv method inside exception-------", e);
		}
		return null;
	}
	
	public Date getMaxFromDate() {
				
		try {
			LocalDate localDate = LocalDate.now().with ( ChronoField.DAY_OF_MONTH , 1 );
	        maxDate = java.sql.Date.valueOf(localDate);
	        log.info("maxDate==> "+maxDate);
		}catch(Exception e) {
			log.error("maxFromDate error==> ",e);
		}
		return maxDate;
	}
	
	public void getViewNoteEmployeeDetails() {
		String url = "";
		Long manAttId = null;
		try {
			manAttId = selectedManualAttendance == null ? null : selectedManualAttendance.getId();
			url = SERVER_URL + "/manualattendance/viewNoteManualAttendance/"
					+ manAttId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				logNoteEmployeeList = mapper.readValue(jsonResponse, new TypeReference<List<LogDTO>>() {
				});
				
				if(logNoteEmployeeList != null && !logNoteEmployeeList.isEmpty()) {
					log.info("logNoteEmployeeList size==> "+logNoteEmployeeList.size());
				}
			}
		} catch (Exception e) {
			log.error("Exception getViewNoteEmployeeDetails", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}
	
	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts Manual attendance-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String attendanceId = "";
		String stage = "";
		String notificationId = "";
		String finalApprove = "";
		String forwardToId = "";
		String note = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			attendanceId = httpRequest.getParameter("attendanceId");
			stage = httpRequest.getParameter("stage");
			notificationId = httpRequest.getParameter("notificationId");
			finalApprove = httpRequest.getParameter("finalapprove");
			forwardToId = httpRequest.getParameter("forwardtoId");
			note =  httpRequest.getParameter("note");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (attendanceId != null) {
			log.info("insurance id----------------->"+attendanceId);
			selectedManualAttendance = new ManualAttendance();
			selectedManualAttendance.setId(Long.parseLong(attendanceId));
			selectedManualAttendance.setFinalstage(stage);
			if(!finalApprove.equals("null")) {
			selectedManualAttendance.setFinalApprove(Boolean.valueOf(finalApprove));
			}
			if(!forwardToId.equals("null")) {
			selectedManualAttendance.setForwardToId(Long.parseLong(forwardToId));
			}
			systemNotificationId = Long.parseLong(notificationId);
			selectedManualAttendance.setNote(note);
			action = "VIEW";
			viewManualAttendance();
		}
		return null;
	}

}

