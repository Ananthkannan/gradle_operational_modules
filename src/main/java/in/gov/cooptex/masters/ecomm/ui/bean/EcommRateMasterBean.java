package in.gov.cooptex.masters.ecomm.ui.bean;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EcommRateMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.EcommRateConfigDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("ecommRateMasterBean")
@Scope("session")
@Log4j2
public class EcommRateMasterBean {

	final String LIST_PAGE = "/pages/ecommerce/admin/listEcommRateMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/ecommerce/admin/createEcommRateMaster.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/ecommerce/admin/viewEcommRateMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<EcommRateMaster> ecommRateMasterList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	EcommRateMaster ecommRateMaster, selectedEcommRateMaster;

	@Getter
	@Setter
	boolean notEligibleFoSsave, disablemodifyTime, showCancelButton, addButtonFlag;

	@Getter
	@Setter
	private int ecommRateMasterListSize;

	@Getter
	@Setter
	LazyDataModel<EcommRateMaster> lazyEcommRateMasterList;

	@Getter
	@Setter
	int totalRecords = 0;

	public String showEcomRateMasterPageAction() {
		log.info("<==== Starts EcommRateMasterBean.showEcomRateMasterPageAction =====>");
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		addButtonFlag = false;
		ecommRateMaster = new EcommRateMaster();
		ecommRateMasterList = new ArrayList<>();
		loadLazyEcommRateList();
		log.info("<==== Ends EcommRateMasterBean.showEcomRateMasterPageAction =====>");
		return LIST_PAGE;
	}

	public void loadLazyEcommRateList() {
		log.info("<===== Starts EcommRateMasterBean.loadLazyEcommRateList ======>");
		lazyEcommRateMasterList = new LazyDataModel<EcommRateMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<EcommRateMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/ecommRateMaster/searchRateMaster";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						ecommRateMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<EcommRateMaster>>() {
								});
						if (null != ecommRateMasterList && ecommRateMasterList.size() > 0) {
							ecommRateMasterList.sort(Comparator.comparing(EcommRateMaster::getCategoryCode));
						}
						log.info("ecommRateMasterList " + ecommRateMasterList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("totalRecords " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyEcommRateList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return ecommRateMasterList;
			}

			@Override
			public Object getRowKey(EcommRateMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EcommRateMaster getRowData(String rowKey) {
				try {
					for (EcommRateMaster ecommRate : ecommRateMasterList) {
						if (ecommRate.getId().equals(Long.valueOf(rowKey))) {
							selectedEcommRateMaster = ecommRate;
							return ecommRate;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends EcommrateMasterBean.loadLazyEcommRateList ======>");
	}

	public void loadRateMasterList() {
		BaseDTO baseDTO = null;
		ecommRateMasterList = new ArrayList<>();
		try {

			String requestPath = SERVER_URL + "/ecommRateMaster/searchRateMaster";
			baseDTO = httpService.post(requestPath, ecommRateMaster);
			log.info("::: Retrieved loadRateMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ecommRateMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EcommRateMaster>>() {
				});
			}
		} catch (Exception ex) {
			log.error("Exception Occured in loadRateMasterList load ", ex);
		}

		if (null != ecommRateMasterList) {
			ecommRateMasterListSize = ecommRateMasterList.size();
		} else {
			ecommRateMasterListSize = 0;
		}
	}

	public String addRateConfigDetail() {
		action = "ADD";
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		addButtonFlag = false;
		ecommRateMaster = new EcommRateMaster();
		selectedEcommRateMaster = new EcommRateMaster();
		productCategoryList = new ArrayList<>();
		getCategoryMasterList();
		return ADD_PAGE;
	}

	private boolean validateIfalreadyExistPrice() {
		boolean isWithInRange = false;
		List<EcommRateMaster> tempEcommRateMasterList = new ArrayList<>();
		if (null != ecommRateMaster) {
			if (null != ecommRateMaster.getId()) {
				tempEcommRateMasterList = new ArrayList<>(ecommRateMasterList);
				tempEcommRateMasterList.removeIf(x -> x.getId().equals(ecommRateMaster.getId()));
			} else {
				tempEcommRateMasterList = new ArrayList<>(ecommRateMasterList);
			}
			if (null != ecommRateMasterList && ecommRateMasterList.size() > 0) {
				boolean isFromWithInRange = tempEcommRateMasterList.stream()
						.filter(x -> x.getCategoryCode().equalsIgnoreCase(ecommRateMaster.getCategoryCode()))
						.filter(x -> x.getPriceRangeFrom() <= ecommRateMaster.getPriceRangeFrom()
								&& x.getPriceRangeTo() >= ecommRateMaster.getPriceRangeFrom())
						.findAny().isPresent();
				boolean isFromWithInRge = tempEcommRateMasterList.stream()
						.filter(x -> x.getCategoryCode().equalsIgnoreCase(ecommRateMaster.getCategoryCode()))
						.filter(x -> x.getPriceRangeFrom() >= ecommRateMaster.getPriceRangeFrom()
								&& x.getPriceRangeTo() >= ecommRateMaster.getPriceRangeFrom())
						.findAny().isPresent();
				boolean isTOsWithInRange = tempEcommRateMasterList.stream()
						.filter(x -> x.getCategoryCode().equalsIgnoreCase(ecommRateMaster.getCategoryCode()))
						.filter(x -> x.getPriceRangeFrom() <= ecommRateMaster.getPriceRangeTo()
								&& x.getPriceRangeTo() >= ecommRateMaster.getPriceRangeTo())
						.findAny().isPresent();
				if (isFromWithInRange || isTOsWithInRange || isFromWithInRge) {
					isWithInRange = true;
				} else {
					isWithInRange = false;
				}
			}
		}
		return isWithInRange;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts onRowSelect() ========>" + event);
		addButtonFlag = true;
		log.info("<===Ends onRowSelect() ========>");
	}

	public void getCategoryMasterList() {
		log.info("===EcommRateMasterBean.getCategoryMasterList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getProductCategory";
			baseDTO = httpService.get(url);
			if (null != baseDTO && null != baseDTO.getResponseContents()) {
				ObjectMapper mapper = new ObjectMapper();
				log.debug("baseDTO.getResponseContents().size()" + baseDTO.getResponseContents().size());
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
				if (null != productCategoryList && productCategoryList.size() > 0) {
					productCategoryList.sort(Comparator.comparing(ProductCategory::getProductCatCode));
				}
			}
			log.info("<<==== EcommRateMasterBean.getCategoryMasterList List Size::: " + productCategoryList.size());
		} catch (Exception e) {
			log.error("<<==============  error in EcommRateMasterBean.getCategoryMasterLis t" + e);
		}
	}

	public String editEcommRateMaster() {
		action = "EDIT";
		if (selectedEcommRateMaster != null) {
			try {
				ecommRateMaster = new EcommRateMaster();
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommRateMaster/rateMaster/" + selectedEcommRateMaster.getId();
				baseDTO = httpService.get(requestPath);
				log.info("::: Retrieved ecommRateMaster :");
				if (baseDTO != null && baseDTO.getResponseContent() != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					EcommRateMaster data = mapper.readValue(jsonResponse, new TypeReference<EcommRateMaster>() {
					});

					if (data != null) {
						ecommRateMaster = data;
					}
					getCategoryMasterList();
					notEligibleFoSsave = false;
					disablemodifyTime = true;
					showCancelButton = false;
					selectedEcommRateMaster = new EcommRateMaster();
					return ADD_PAGE;
				}

			} catch (Exception ee) {
				log.error("Exception Occured in editEcommRateMaster load ", ee);
			}
		} else {
			Util.addWarn("Please Select One Ecommerce Rate Master");
			RequestContext.getCurrentInstance().update("growls");
		}
		return null;
	}

	public String viewRateMaster() {
		if (selectedEcommRateMaster != null) {
			try {
				ecommRateMaster = new EcommRateMaster();
				productCategoryList = new ArrayList<>();
				BaseDTO baseDTO = null;
				String requestPath = SERVER_URL + "/ecommRateMaster/rateMaster/" + selectedEcommRateMaster.getId();
				baseDTO = httpService.get(requestPath);
				log.info("::: Retrieved viewRateMaster :");
				if (baseDTO != null && baseDTO.getResponseContent() != null) {
					mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					EcommRateMaster data = mapper.readValue(jsonResponse, new TypeReference<EcommRateMaster>() {
					});
					if (data != null) {
						ecommRateMaster = data;
					}
					selectedEcommRateMaster = new EcommRateMaster();
					notEligibleFoSsave = false;
					disablemodifyTime = true;
					showCancelButton = false;
					return VIEW_PAGE;
				}

			} catch (Exception ee) {
				log.error("Exception Occured in productCategoryList load ", ee);
			}
		} else {
			Util.addWarn("Please Select One Ecommerce Rate Master");
			RequestContext.getCurrentInstance().update("growls");
		}
		return null;
	}

	public String cancel() {
		ecommRateMaster = new EcommRateMaster();
		selectedEcommRateMaster = new EcommRateMaster();
		productCategoryList = new ArrayList<>();
		notEligibleFoSsave = true;
		disablemodifyTime = false;
		showCancelButton = true;
		addButtonFlag = false;
		return LIST_PAGE;
	}

	public String saveOrUpdate() {
		log.info("<==== Starts EcommRateMasterBean.saveOrUpdate =======>");
		try {
			if (null != ecommRateMaster) {
				if (ecommRateMaster.getCategoryCode() == null) {
					AppUtil.addError("Category Code is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}

				if (ecommRateMaster.getPriceRangeFrom() == null || ecommRateMaster.getPriceRangeFrom() == 0) {
					AppUtil.addError("Price Range From is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}

				if (ecommRateMaster.getPriceRangeTo() == null || ecommRateMaster.getPriceRangeTo() == 0) {
					AppUtil.addError("Price Range To is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
				if (ecommRateMaster.getPriceRangeFrom() >= ecommRateMaster.getPriceRangeTo()) {
					AppUtil.addError("Price Range From should not be greater than Price Range To");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
				boolean isExist = validatePriceRange();
				if (isExist) {
					AppUtil.addError("Entered Price Range already exist");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
				String url = SERVER_URL + "/ecommRateMaster/saveRateMaster";
				BaseDTO response = httpService.post(url, ecommRateMaster);
				if (response != null && response.getStatusCode() == 0) {
					log.info("EcommRateMaster saved successfully..........");
					if ("ADD".equalsIgnoreCase(action))
						AppUtil.addInfo("Ecommerce Rate Master saved successfully");
					else
						AppUtil.addInfo("Ecommerce Rate Master updated successfully");
					return showEcomRateMasterPageAction();
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommRateMaster.saveOrUpdate =======>");
		return showEcomRateMasterPageAction();
	}

	private boolean validatePriceRange() {
		log.info("<==== Starts EcommRateMasterBean.validatePriceRange =======>");
		boolean isExist = false;
		try {
			String url = SERVER_URL + "/ecommRateMaster/validatePriceRange";
			BaseDTO response = httpService.post(url, ecommRateMaster);
			if (response != null && null != response.getMessage()) {
				log.info("EcommRateMaster validatePriceRange successfully..........");
				if("true".equalsIgnoreCase(response.getMessage())){
					isExist = true; 
				}else{
					isExist = false;
				}
				return isExist;
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return isExist;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while deleteRateMastere .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return isExist;
		}
		log.info("<==== Ends EcommRateMaster.deleteRateMaster =======>");
		return isExist;

	}

	public String deleteConfirm() {
		try {
			action = "Delete";
			if (selectedEcommRateMaster == null) {
				Util.addWarn("Please Select One Ecommerce Rate Master");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStateDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteRateMaster() {
		log.info("<==== Starts EcommRateMasterBean.deleteRateMaster =======>");
		try {
			String url = SERVER_URL + "/ecommRateMaster/deleteRateMaster";
			BaseDTO response = httpService.post(url, selectedEcommRateMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("EcommRateMaster deleted successfully..........");
				Util.addInfo("Ecommerce Rate Master deleted successfully");
				return showEcomRateMasterPageAction();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while deleteRateMastere .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommRateMaster.deleteRateMaster =======>");
		return showEcomRateMasterPageAction();
	}

	public void checkMandatoryValidation() {
		if (null == ecommRateMaster || null == ecommRateMaster.getCategoryCode()
				|| "".equalsIgnoreCase(ecommRateMaster.getCategoryCode())) {
			AppUtil.addWarn("Please select category code");
			return;
		}
		if (null == ecommRateMaster || null == ecommRateMaster.getPriceRangeFrom()) {
			AppUtil.addWarn("Please enter price range from value");
			return;
		}
		if (null == ecommRateMaster || null == ecommRateMaster.getPriceRangeTo()) {
			AppUtil.addWarn("Please enter price range to value");
			return;
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('clearConfirmDialog').show();");

	}

}