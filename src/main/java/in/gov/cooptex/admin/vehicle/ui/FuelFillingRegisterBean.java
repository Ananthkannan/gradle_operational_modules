/**
 * 
 */
package in.gov.cooptex.admin.vehicle.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.vehicle.dto.FuelFillingRecieptDTO;
import in.gov.cooptex.admin.vehicle.dto.FuelFillingRegisterListDTO;
import in.gov.cooptex.admin.vehicle.enums.FuelFillingRegisterStatus;
import in.gov.cooptex.admin.vehicle.model.FuelCoupon;
import in.gov.cooptex.admin.vehicle.model.FuelFillingReciept;
import in.gov.cooptex.admin.vehicle.model.FuelFillingRegister;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FuelType;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.VehicleModel;
import in.gov.cooptex.core.model.VehicleRegister;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.vehicle.dto.FuelFillingRegisterSaveDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */

@Log4j2
@Scope("session")
@Service("fuelFillingRegisterBean")
public class FuelFillingRegisterBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 950360973051886269L;
	private final String CREATE_FUEL_FILLING_PAGE = "/pages/admin/fuelDetails/createFuelFillingRegister.xhtml?faces-redirect=true;";
	private final String FUEL_FILLING_REGISTER_LIST_URL = "/pages/admin/fuelDetails/listFuelFillingRegister.xhtml?faces-redirect=true;";
	private final String FUEL_FILLING_REGISTER_VIEW_URL = "/pages/admin/fuelDetails/viewFuelFillingRegister.xhtml?faces-redirect=true;";
	private final String FUEL_FILLING_REGISTER_ADD_RECEIPT_URL = "/pages/admin/fuelDetails/addReceiptFuelFillingRegister.xhtml?faces-redirect=true;";
	private final String FUEL_FILLING_REGISTER_CANCEL = "/pages/admin/fuelDetails/cancellationFuelFillingRegister.xhtml?faces-redirect=true;";
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;

	@Setter
	@Getter
	String note;

	@Getter
	@Setter
	int totalRecords = 0;

	@Autowired
	CommonDataService commonDataService;

	String jsonResponse;

	@Setter
	@Getter
	String fileType;

	@Getter
	@Setter
	String fileName;

	long letterSize;

	@Getter
	@Setter
	UploadedFile uploadFile;

	@Setter
	@Getter
	private List<String> fileList;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean addReciptButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	FuelFillingRegister fuelFillingRegister;

	@Getter
	@Setter
	VehicleRegister vehicleRegister = new VehicleRegister();

	@Getter
	@Setter
	List<FuelFillingReciept> fuelFillingRecieptList = new ArrayList<FuelFillingReciept>();

	@Getter
	@Setter
	FuelFillingReciept fuelFillingReciept;

	@Getter
	@Setter
	PurchaseOrder purchaseOrder = new PurchaseOrder();
	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();
	@Getter
	@Setter
	List<EntityMaster> headandregionalofficelist = new ArrayList<>();

	@Getter
	@Setter
	FuelCoupon fuelCoupon = new FuelCoupon();

	@Getter
	@Setter
	FuelType fuelType = new FuelType();

	@Getter
	@Setter
	List<FuelType> fuelFillTypeList = new ArrayList<>();

	@Getter
	@Setter
	VehicleModel vehicleModel = new VehicleModel();

	@Getter
	@Setter
	List<VehicleRegister> vehicleRegisterList = new ArrayList<VehicleRegister>();

	@Getter
	@Setter
	List<VehicleRegister> fuelTypeList = new ArrayList<VehicleRegister>();

	@Getter
	@Setter
	List<FuelCoupon> fuelCouponList = new ArrayList<FuelCoupon>();

	@Getter
	@Setter
	LazyDataModel<FuelFillingRegisterListDTO> fuelFillingRegisterLazyList;

	@Getter
	@Setter
	List<FuelFillingRegisterListDTO> fuelFillingRegisterListDTO;

	@Getter
	@Setter
	FuelFillingRegisterListDTO selectedFuelFillingRegisterDTO;

	@Getter
	@Setter
	FuelFillingRegisterListDTO fuelFillingRegisterDTO;

	@Getter
	@Setter
	FuelFillingRecieptDTO fuelFillingRecieptDTO;

	@Getter
	@Setter
	List<FuelFillingRegister> fuelFillingRegisterList = new ArrayList<FuelFillingRegister>();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	FuelFillingRegister selectedfuelFillingRegister;

	@Getter
	@Setter
	Double fillingKillometer = 0.0, fuelQty = 0.0;

	@Getter
	@Setter
	FuelFillingRegisterSaveDTO fuelFillingRegisterSaveDTO;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean cancellationButtonFlag = false;

	@Getter
	@Setter
	private String buttonLable;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	Boolean finalApprovalFlag = false;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Getter
	@Setter
	Long fuelCapacity = null;

	@Getter
	@Setter
	Double kilometerfilled;
	ObjectMapper objectMapper = new ObjectMapper();

	public void init() {
		clear();
		getAllVehicle();
		getAllFuelType();
		getFuelType();
//		getAllFuelCoupon();
		fuelFillingRegisterPage();
	}

	public String createFuelFillingRegister() {
		init();
		getAllFuelCoupon();
		fuelFillingRegisterSaveDTO.setForwardTo(null);
		addButtonFlag = false;
		setFileName(null);
		fuelQty = null;
		fillingKillometer = 0.0;
		fuelFillingRegister.setDocumentPath("");
		vehicleRegister = new VehicleRegister();
		return CREATE_FUEL_FILLING_PAGE;
	}

	public String addFuelFillingRegisterReceipt() {
		init();
		fuelFillingRegister = new FuelFillingRegister();
		fuelFillingRegisterSaveDTO = new FuelFillingRegisterSaveDTO();
		clear();
		return FUEL_FILLING_REGISTER_ADD_RECEIPT_URL;
	}

	public String fuelFillingRegisterPage() {
		log.info("============== FuelFillingRegisterPage =============");
		try {
			clear();
			addButtonFlag = false;
			vehicleRegister = new VehicleRegister();
			fuelFillingRegister = new FuelFillingRegister();
			selectedfuelFillingRegister = new FuelFillingRegister();
			fuelFillingRegisterSaveDTO = new FuelFillingRegisterSaveDTO();
			fuelFillingReciept = new FuelFillingReciept();
			fuelFillingRecieptDTO = new FuelFillingRecieptDTO();
			selectedFuelFillingRegisterDTO = new FuelFillingRegisterListDTO();
			getAllVehicle();
			getAllFuelType();
			getFuelType();
			getAllFuelCoupon();

			addButtonFlag = false;
			editButtonFlag = true;
			addReciptButtonFlag = true;
			viewButtonFlag = true;
			deleteButtonFlag = true;
			cancellationButtonFlag = true;

			loadLazyList();
			headandregionalofficelist = new ArrayList<EntityMaster>();
			headandregionalofficelist = commonDataService.loadHeadAndRegionalOffice();
			forwardToUsersList = new ArrayList<UserMaster>();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.CIRCULAR.toString());
			createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();

		} catch (Exception e) {
			log.error("============== Exception FuelFillingRegisterPage()================" + e);
		}
		return FUEL_FILLING_REGISTER_LIST_URL;
	}

	public void clear() {
		vehicleRegister = new VehicleRegister();
		fuelFillingRegister = new FuelFillingRegister();
		vehicleRegisterList = new ArrayList<VehicleRegister>();
		fuelTypeList = new ArrayList<VehicleRegister>();
		fuelFillTypeList = new ArrayList<FuelType>();
		fuelCouponList = new ArrayList<FuelCoupon>();
		fuelFillingRegisterList = new ArrayList<FuelFillingRegister>();
		headandregionalofficelist = new ArrayList<EntityMaster>();
		fuelFillingRecieptList = new ArrayList<FuelFillingReciept>();
		fuelFillingRegisterSaveDTO = new FuelFillingRegisterSaveDTO();

	}

	public void getAllVehicle() {
		vehicleRegisterList = new ArrayList<VehicleRegister>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/fuelFillingRegister/getAllVehicle";
			log.info(" getAllVehicle -------->> getAllVehicle in fuel filling URL -------->> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End getAllVehicle in fuel filling  ------- >>>>");

	}

	public void getAllFuelType() {
		fuelTypeList = new ArrayList<VehicleRegister>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/fuelFillingRegister/getAllFuelType";
			log.info(" getAllFuelType -------->> getAllFuelType in fuel filling URL -------->> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				fuelTypeList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End fuelTypeList in fuel filling  ------- >>>>");

	}

	public void getAllFuelCoupon() {
		fuelCouponList = new ArrayList<FuelCoupon>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			/** vehicle based coupon tack ***/
			if (vehicleRegister != null && vehicleRegister.getId() != null) {
				String url = SERVER_URL + "/fuelFillingRegister/getAllFuelCoupon/" + vehicleRegister.getId();
				log.info(" getAllFuelCoupon -------->> getAllFuelCoupon in fuel filling URL -------->> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					fuelCouponList = mapper.readValue(jsonResponse, new TypeReference<List<FuelCoupon>>() {
					});
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End FuelCoupon in fuel filling  ------- >>>>");

	}

	public void onVehicleChange() {
		log.info("onVehicleChange vehicle");
		if (vehicleRegister.getId() != null) {
			getVehicleLsit(vehicleRegister.getId());
		}
		fuelFillingRegister.setFuelCoupon(null);
		fuelFillingRegister.setCouponNumber(null);
		fuelFillingRegister.setPricePerLiter(0.0);
		fuelQty = fuelFillingRegister.getFuelQty();
		fuelCapacity = vehicleRegister.getFuelCapacity();
		log.info("fuel capacity" + fuelCapacity);
		fuelFillingRegister.setFuelQty(0.0);
		fuelFillingRegister.setReason(null);
		try {
			String url = SERVER_URL + "/fuelFillingRegister/getlastfilledkilometer/" + vehicleRegister.getId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("valueeee" + baseDTO.getResponseContent());

					kilometerfilled = mapper.readValue(jsonValue, new TypeReference<Double>() {
					});

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in gettingfuellastkilometer() --->", exp);
		}

		// fillingKillometer = fuelFillingRegister.getFillingKillometer();
		fillingKillometer = kilometerfilled;
		fuelFillingRegister.setFillingKillometer(null);
		getAllFuelCoupon();
	}

	public void getVehicleLsit(Long vehicleId) {
		log.info("onVehicleChange--------" + vehicleId);

		String url = SERVER_URL + "/fuelFillingRegister/getFuelFillingRegister/" + vehicleId;
		BaseDTO baseDTO = new BaseDTO();
		FuelFillingRegister fuelFillingRegisters = new FuelFillingRegister();
		try {
			log.info("vehicleRegister:==> onVehicleChange Method Called---------");
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				fuelFillingRegisters = mapper.readValue(jsonResponse, new TypeReference<FuelFillingRegister>() {
				});
				if (fuelFillingRegisters == null) {
					log.info("onVehicleChange", fuelFillingRegister);
					fuelFillingRegister.setFuelQty(0.0);
					fuelFillingRegister.setFillingKillometer(0.0);
					fuelFillingRegister.setFillingDate(new Date());
					fuelFillingRegister.setDestination(null);
					// fuelFillingRegister.setFuelType();
					// errorMap.notify(ErrorDescription.FUEL_REG_VECHICLE_EMTPY.getErrorCode());
					// RequestContext.getCurrentInstance().update("growls");
				} else {
					fuelFillingRegisters.setId(fuelFillingRegister.getId());
					fuelFillingRegisters.setReferenceNumber(fuelFillingRegister.getReferenceNumber());
					fuelFillingRegister = fuelFillingRegisters;
				}

			} else {
				log.info("getVehicleLsit baseDTO is null");
			}
		} catch (Exception exp) {
			log.error("Exception in onVehicleChange method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	// check fuel coupon Expired or not
	public void onCouponChange() {
		log.info("fuelFillingRegister:==> onCouponChange Method Called---------");
		Boolean isExpired = null;
		String url;
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (fuelFillingRegister.getFuelCoupon() != null && fuelFillingRegister.getFuelCoupon().getId() != null) {
				log.info("onCouponChange--------" + fuelFillingRegister.getFuelCoupon().getId());
				url = SERVER_URL + "/fuelFillingRegister/getFuelCoupon/" + fuelFillingRegister.getFuelCoupon().getId();
				baseDTO = httpService.get(url);
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				isExpired = mapper.readValue(jsonResponse, new TypeReference<Boolean>() {
				});
				log.info("onCouponChange---------->", isExpired);

				if (isExpired) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.FUEL_COUPON_EXPIRED).getCode());
					fuelFillingRegister.setFuelCoupon(null);
					fuelFillingRegister.setCouponNumber(null);
				} else {
					log.info("-------fuel coupon not expired----------");
				}
			}
		} catch (Exception exp) {
			log.error("Exception in onCouponChange method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	// check for coupon number already used or not
	public void fuleCouponNumberChange() {
		log.info("fuelFillingRegister:==> fuleCouponNumberChange Method Called---------");
		String url;
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (fuelFillingRegister.getFuelCoupon() != null && fuelFillingRegister.getFuelCoupon().getId() != null
					&& fuelFillingRegister.getCouponNumber() != null) {
				log.info("fuleCouponNumberChange--------" + fuelFillingRegister.getCouponNumber());

				if (fuelFillingRegister.getCouponNumber() < fuelFillingRegister.getFuelCoupon()
						.getCouponStartNumber()) {
					log.info("---------inside if condition-----------");
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.FUEL_COUPON_NUMBER_NOT_BETWEEN).getErrorCode());
					fuelFillingRegister.setCouponNumber(null);
				} else if (fuelFillingRegister.getCouponNumber() > fuelFillingRegister.getFuelCoupon()
						.getCouponEndNumber()) {
					log.info("----------inside else if condition------------");
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.FUEL_COUPON_NUMBER_NOT_BETWEEN).getErrorCode());
					fuelFillingRegister.setCouponNumber(null);
				} else {
					log.info("------------inside else condition------------");
					url = SERVER_URL + "/fuelFillingRegister/checkfuelcouponnumber/"
							+ fuelFillingRegister.getFuelCoupon().getId() + "/" + fuelFillingRegister.getCouponNumber();
					baseDTO = httpService.get(url);

					if (baseDTO != null) {
						if (baseDTO.getStatusCode().equals(AdminErrorCode.FUEL_COUPON_NUMBER_ALERADY_USED)) {
							log.info("-----------coupon already used-----------");
							errorMap.notify(ErrorDescription.getError(AdminErrorCode.FUEL_COUPON_NUMBER_ALERADY_USED)
									.getErrorCode());
							fuelFillingRegister.setCouponNumber(null);
						} else {
							log.info("-----------coupon not used-----------");
						}
					}
				}
			}
		} catch (Exception exp) {
			log.error("Exception in onCouponChange method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void getFuelType() {
		log.info("getFuelType --------");

		String url = SERVER_URL + "/fuelFillingRegister/getFuelType";
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("fuelFillingRegister:==> getFuelType Method Called---------");
			baseDTO = httpService.get(url);
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			fuelFillTypeList = mapper.readValue(jsonResponse, new TypeReference<List<FuelType>>() {
			});
			log.info("fuelTypeList size", fuelFillTypeList.size());
		} catch (Exception exp) {
			log.error("Exception in getFuelFileType method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void fuelFillingFileUpload(FileUploadEvent event) {
		log.info(" <------fuelFillingFileUpload FileUpload Method Start--->");

		try {

			if (event == null || event.getFile() == null) {
				Util.addWarn("upload Doument should not be Null");
				return;
			} else {
				uploadFile = event.getFile();
				long size = uploadFile.getSize();
				log.info("uploadSignature size==>" + size);
				size = size / 1000;

				String docPathName = commonDataService.getAppKeyValue("FUEL_FILLING_REGISTER_EXCEL_FILE_PATH");

				String filePath = Util.fileUpload(docPathName, uploadFile);
				setFileName(uploadFile.getFileName());
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
				fuelFillingRegister.setDocumentPath(filePath);
			}
		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}

	}

	public void fuelRecieptFileUpload(FileUploadEvent event) {
		log.info(" <------fuelFillingFileUpload FileUpload Method Start--->");

		try {

			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadFile = event.getFile();
			long size = uploadFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;

			String docPathName = commonDataService.getAppKeyValue("FUEL_FILLING_RECIEPT_FILE_PATH");

			String filePath = Util.fileUpload(docPathName, uploadFile);
			setFileName(uploadFile.getFileName());
			fuelFillingReciept.setDocumentPath(filePath);
		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}

	}

	public String submit() {
		log.info("-------------- Fuel Filling Submit Called-----------------");

		try {
			
			 if(fuelFillingRegister.getPricePerLiter() ==null || fuelFillingRegister.getPricePerLiter()==0.0) {
				 Util.addWarn("Price per Liter Should Not be Zero");
					return null;
			 }

			if (fuelFillingRegisterSaveDTO.getNote() == null || fuelFillingRegisterSaveDTO.getNote().isEmpty()) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			if (fuelFillingRegister.getFillingKillometer() < getFillingKillometer()) {
				Util.addWarn("Current Meter Reading should Not Be Less Than Last filled Kilometer");
				return null;

			}
            if(fuelFillingRegister.getFuelQty() !=null || fuelFillingRegister.getFuelQty() !=0) {
			if (fuelFillingRegister.getFuelQty() > vehicleRegister.getFuelCapacity()) {
				//Util.addWarn("Fuel To be Filled is More Than Available Tank Capacity");
				Util.addWarn("Available Tank Capacity is "+  vehicleRegister.getFuelCapacity());
				return null;

			}
            }

			log.info("filenameeee" + getFileName());
			if (StringUtils.isEmpty(fileName) || getFileName() == null) {
				Util.addWarn("Upload Document Should Not Be Empty");
				return null;

			}

			fuelFillingRegister.setVehicleRegister(vehicleRegister);
			fuelFillingRegisterSaveDTO.setFuelFillingRegister(fuelFillingRegister);
			fuelFillingRegisterSaveDTO.setForwardTo(userMaster);
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(SERVER_URL + "/fuelFillingRegister/create", fuelFillingRegisterSaveDTO);
			log.info("FuelFillingRegister" + fuelFillingRegister);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info(" saved successfully");
					init();
					if (action.equalsIgnoreCase("add"))
						errorMap.notify(ErrorDescription.FUEL_FILLING_REG_INSERTED_SICCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.FUEL_FILLING_REG_UPDATED_SICCESSFULLY.getCode());
					return FUEL_FILLING_REGISTER_LIST_URL;
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating FuelFillingRegister" + e);
		}
		log.info("...... submit ended ....");

		return null;
	}

	public String fuelRecieptsubmit() {
		log.info("-------------- Fuel FillingReciept Submit Called-----------------");

		try {

			if (fuelFillingReciept.getFuelFilled() > fuelFillingRegister.getFuelQty()) {
				Util.addWarn("Fuel Filled Should Be Less Than Fuel To be Filled");
				return null;
			}

			if (fuelFillingRecieptDTO.getNote().equals("")) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			} 
			fuelFillingRecieptDTO.setFuelFillingRegister(fuelFillingRegister);
			fuelFillingRecieptDTO.setFuelFillingReciept(fuelFillingReciept);
			fuelFillingRecieptDTO.setForwardTo(userMaster);
			log.info("<<<==== fuelFillingRecieptDTO ..." + fuelFillingRecieptDTO.getForwardTo().getId());
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(SERVER_URL + "/fuelFillingRegister/recieptCreate", fuelFillingRecieptDTO);
			log.info("FuelFillingRegister" + fuelFillingRegister);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info(" saved successfully");
					init();
					if (fuelFillingReciept == null)
						errorMap.notify(ErrorDescription.FUEL_FILLING_RECIEPT_INSERTED_SICCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.FUEL_FILLING_RECIEPT_UPDATED_SICCESSFULLY.getCode());

					return FUEL_FILLING_REGISTER_LIST_URL;
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating FuelFillingRegister" + e);
		}
		log.info("...... submit ended ....");

		return null;
	}

	public void test() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> ");
		log.info("=========================================");
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + fuelFillingRecieptDTO.getNote());
		log.info("=========================================");
	}

	public void loadLazyList() {

		log.info("<===== Starts lazy List ======>");

		String FUEL_FILLING_REGISTER_LIST_URL = SERVER_URL + "/fuelFillingRegister/getAllFuelFillingRegister";
		log.info(" loadLazyList -------->> loadLazyList URL -------->> " + FUEL_FILLING_REGISTER_LIST_URL);

		fuelFillingRegisterLazyList = new LazyDataModel<FuelFillingRegisterListDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<FuelFillingRegisterListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					BaseDTO response = httpService.post(FUEL_FILLING_REGISTER_LIST_URL, paginationRequest);
					log.info("==========URL  List=============>" + FUEL_FILLING_REGISTER_LIST_URL);
					log.info("Pagination request :::" + paginationRequest);

					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						fuelFillingRegisterListDTO = mapper.readValue(jsonResponse,
								new TypeReference<List<FuelFillingRegisterListDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazy List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return fuelFillingRegisterListDTO;
			}

			@Override
			public Object getRowKey(FuelFillingRegisterListDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FuelFillingRegisterListDTO getRowData(String rowKey) {
				try {
					for (FuelFillingRegisterListDTO FuelFillingRegisterObj : fuelFillingRegisterListDTO) {
						if (FuelFillingRegisterObj.getId().equals(Long.valueOf(rowKey))) {
							selectedFuelFillingRegisterDTO = FuelFillingRegisterObj;
							return FuelFillingRegisterObj;
						}
					}
				}

				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};
		log.info("<===== Ends FuelFillingRegister.loadLazyFuelFillingRegister ======>");
	}

	public String viewFuelFillingRegister() {
		log.info("<<<<< -------Start FuelFillingRegisterPage AddRecipt called ------->>>>>> ");
		buttonLable = null;
		try {
			fuelFillingReciept = new FuelFillingReciept();

			String url = SERVER_URL + "/fuelFillingRegister/editFuelRecieptById/"
					+ selectedFuelFillingRegisterDTO.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				fuelFillingRecieptDTO = mapper.readValue(jsonResponse, new TypeReference<FuelFillingRecieptDTO>() {
				});
				// getAllFuelType();
				fuelQty = fuelFillingRecieptDTO.getFuelQty();
				fillingKillometer = fuelFillingRecieptDTO.getFillingKillometer();
				fuelFillingRegister = fuelFillingRecieptDTO.getFuelFillingRegister();
				log.info("fuelFillingRecieptDTO forwad for-----------" + fuelFillingRecieptDTO.getForwardFor());
				log.info("fuelFillingRecieptDTO forwad to-----------" + fuelFillingRecieptDTO.getForwardTo().getId());
				try {
					if (fuelFillingRecieptDTO.getForwardTo() != null) {
						if (loginBean.getUserMaster().getId().equals(fuelFillingRecieptDTO.getForwardTo().getId())) {
							if (FuelFillingRegisterStatus.REJECTED.equals(fuelFillingRecieptDTO.getStatus())
									|| FuelFillingRegisterStatus.RECEIPT_REJECTED
											.equals(fuelFillingRecieptDTO.getStatus())
									|| FuelFillingRegisterStatus.CANCELLATION_REJECTED
											.equals(fuelFillingRecieptDTO.getStatus())) {
								approvalFlag = false;
								buttonFlag = false;
							} else if (FuelFillingRegisterStatus.FINALAPPROVED.equals(fuelFillingRecieptDTO.getStatus())
									|| FuelFillingRegisterStatus.RECEIPT_FINALAPPROVED
											.equals(fuelFillingRecieptDTO.getStatus())
									|| FuelFillingRegisterStatus.CANCELLATION_FINALAPPROVED
											.equals(fuelFillingRecieptDTO.getStatus())) {
								buttonFlag = false;
								approvalFlag = false;
							} else {
								buttonFlag = true;
								approvalFlag = true;
							}
						} else {
							approvalFlag = false;
							buttonFlag = false;
						}

					}
					setPreviousApproval(fuelFillingRecieptDTO.getForwardFor());
					if (fuelFillingRecieptDTO.getFuelFillingReciept() != null) {
						fuelFillingReciept = fuelFillingRecieptDTO.getFuelFillingReciept();
					}
					if (fuelFillingReciept != null) {
						setFileName(fuelFillingReciept.getDocumentPath());
					}

					if (fuelFillingRecieptDTO.getForwardFor().equals(false)) {
						buttonLable = "Approve";
						finalApprovalFlag = false;
					} else if (fuelFillingRecieptDTO.getForwardFor().equals(true)) {
						buttonLable = "Final Approve";
						finalApprovalFlag = true;
					}

				} catch (Exception e) {
					log.info("exception in reciept:" + e);
				}
				File f = new File(fuelFillingRegister.getDocumentPath());
				fileName = f.getName();
				if ("View".equals(action)) {
					return FUEL_FILLING_REGISTER_VIEW_URL;
				} else if ("Cancel".equals(action)) {
					fuelFillingRegister.setReason(null);
					return FUEL_FILLING_REGISTER_CANCEL;
				}

				log.info("<<<<< -------End  EDIT FuelFillingRegister ------->>>>>> " + fuelFillingReciept);
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		return null;
	}

	public String editFuelFillingRegister() {

		log.info("<<<<< -------Start FuelFillingRegisterPage EDIT called ------->>>>>> ");

		try {
			if (selectedFuelFillingRegisterDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ONE.getErrorCode());
				Util.addWarn("Please select one field");
				return null;
			}
			String url = SERVER_URL + "/fuelFillingRegister/editFuelById/" + selectedFuelFillingRegisterDTO.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				fuelFillingRegisterSaveDTO = mapper.readValue(jsonResponse,
						new TypeReference<FuelFillingRegisterSaveDTO>() {
						});

				log.info("fuelFillingRegisterSaveDTO forwad to-----------"
						+ fuelFillingRegisterSaveDTO.getForwardTo().getId());
				if (fuelFillingRegisterSaveDTO != null) {
					fuelFillingRegister = fuelFillingRegisterSaveDTO.getFuelFillingRegister();
					vehicleRegister = fuelFillingRegisterSaveDTO.getFuelFillingRegister().getVehicleRegister();
					getAllFuelCoupon();

					log.info("" + vehicleRegister.getRegistrationNo());
					setFileName(fuelFillingRegister.getDocumentPath());
				}

				log.info("<<<<< -------End  EDIT FuelFillingRegister ------->>>>>> ");
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return CREATE_FUEL_FILLING_PAGE;
	}

	public String getReciptFuelFillingRegister() {

		log.info("<<<<< -------Start FuelFillingRegisterPage AddRecipt called ------->>>>>> ");
		getAllFuelType();
		getFuelType();
		fillingKillometer = 0.0;
		fuelQty = 0.0;
		try {
			fuelFillingReciept = new FuelFillingReciept();
			String url = SERVER_URL + "/fuelFillingRegister/editFuelRecieptById/"
					+ selectedFuelFillingRegisterDTO.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				fuelFillingRecieptDTO = mapper.readValue(jsonResponse, new TypeReference<FuelFillingRecieptDTO>() {
				});

				fuelFillingRecieptDTO.setForwardFor(null);
				fuelFillingRecieptDTO.setNote(null);
				fuelFillingRegister = fuelFillingRecieptDTO.getFuelFillingRegister();
				try {
					if (fuelFillingRecieptDTO.getFuelFillingReciept() != null) {
						fuelFillingReciept = fuelFillingRecieptDTO.getFuelFillingReciept();
					}
					if (fuelFillingReciept != null)
						setFileName(fuelFillingReciept.getDocumentPath());
				} catch (Exception e) {
					log.info("exception in reciept:" + e);

				}

				log.info("<<<<< -------End  EDIT FuelFillingRegister ------->>>>>> " + fuelFillingReciept);
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return FUEL_FILLING_REGISTER_ADD_RECEIPT_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		addButtonFlag = true;
		editButtonFlag = false;
		addReciptButtonFlag = false;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		cancellationButtonFlag = false;

		selectedFuelFillingRegisterDTO = ((FuelFillingRegisterListDTO) event.getObject());
		fuelFillingRegisterDTO = selectedFuelFillingRegisterDTO;
		try {
			log.info("SlectedfuelFillingRegister status is:" + selectedFuelFillingRegisterDTO.getStatus());
			log.info("selectedFuelFillingRegisterDTO id----------" + selectedFuelFillingRegisterDTO.getId());

			if (selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.REJECTED.toString())) {
				editButtonFlag = false;
			} else {
				editButtonFlag = true;
			}
			if (selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.FINALAPPROVED.toString())) {
				addReciptButtonFlag = false;
			} else {
				addReciptButtonFlag = true;
			}

			if (selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.FINALAPPROVED.toString())
					|| selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.SUBMITTED.toString())
					|| selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.APPROVED.toString())
					|| selectedFuelFillingRegisterDTO.getStatus()
							.equals(FuelFillingRegisterStatus.RECEIPT_APPROVED.toString())
					|| selectedFuelFillingRegisterDTO.getStatus()
							.equals(FuelFillingRegisterStatus.RECEIPT_FINALAPPROVED.toString())
					|| selectedFuelFillingRegisterDTO.getStatus()
							.equals(FuelFillingRegisterStatus.RECEIPT_SUBMITTED.toString())) {
				cancellationButtonFlag = true;
				deleteButtonFlag = true;
			}
			if (selectedFuelFillingRegisterDTO.getStatus().equals(FuelFillingRegisterStatus.SUBMITTED.toString())) {
				addReciptButtonFlag = true;
				editButtonFlag = false;
				cancellationButtonFlag = false;
			}
		} catch (Exception e) {
			log.info("<=== Ends onRowSelect exception ========>" + e);
		}
		log.info("<===Ends onRowSelect ========>");
	}

	public String deleteFuelRegister() {
		try {
			if (fuelFillingRegisterList == null) {
				errorMap.notify(ErrorDescription.SELECT_ONE.getErrorCode());
				Util.addWarn("Please select one field");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts FuelFillingRegisterPage.deleteConfirm ---->");
		try {
			BaseDTO response = httpService.delete(
					SERVER_URL + "/fuelFillingRegister/deleteFuelRegister/" + selectedFuelFillingRegisterDTO.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.MEETING_REPORT_DELETED_SUCCESSFULLY.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting FuelFillingRegisterPage....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends FuelFillingRegisterPage.deleteConfirm ---->");
		return fuelFillingRegisterPage();
	}

	public String approveFuelFilling() {
		// This is the View Page ForwardTo approved payment
		log.error("<---- Start TdsPaymentBean final approval: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("Reciept id----------" + fuelFillingReciept.getId());
			log.info("previousApproval ----------" + previousApproval);
			log.info("current approveal ----------" + fuelFillingRecieptDTO.getForwardFor());
			baseDTO = new BaseDTO();
			if (fuelFillingReciept != null && fuelFillingReciept.getId() != null) {
				if (previousApproval == false && fuelFillingRecieptDTO.getForwardFor() == true) {
					log.error("<---- Approve Status change on Approved  ----->");
					fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.RECEIPT_APPROVED);
					fuelFillingRecieptDTO.setForwardTo(userMaster);
				} else if (previousApproval == true && fuelFillingRecieptDTO.getForwardFor() == true) {
					log.error("<---- Approve Status change on Final Approved  ----->");
					fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.RECEIPT_FINALAPPROVED);
				} else {
					log.error("<---- Approve Status change on Approved  ----->");
					fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.RECEIPT_APPROVED);
					fuelFillingRecieptDTO.setForwardTo(userMaster);
				} 
			} else {
				if (fuelFillingRecieptDTO.getStatus().equals(FuelFillingRegisterStatus.CANCELLATION_SUBMITTED)
						|| fuelFillingRecieptDTO.getStatus().equals(FuelFillingRegisterStatus.CANCELLATION_APPROVED)) {
					if (previousApproval == false && fuelFillingRecieptDTO.getForwardFor() == true) {
						log.error("<---- Approve Status change on Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.CANCELLATION_APPROVED);
					} else if (previousApproval == true && fuelFillingRecieptDTO.getForwardFor() == true) {
						log.error("<---- Approve Status change on Final Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.CANCELLATION_FINALAPPROVED);
					} else {
						log.error("<---- Approve Status change on Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.CANCELLATION_APPROVED);
					}
				} else {
					if (previousApproval == false && fuelFillingRecieptDTO.getForwardFor() == true) {
						log.error("<---- Approve Status change on Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.APPROVED);
						fuelFillingRecieptDTO.setForwardTo(userMaster);
					} else if (previousApproval == true && fuelFillingRecieptDTO.getForwardFor() == true) {
						log.error("<---- Approve Status change on Final Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.FINALAPPROVED);
					} else {
						log.error("<---- Approve Status change on Approved  ----->");
						fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.APPROVED);
						fuelFillingRecieptDTO.setForwardTo(userMaster);
					}
				}
			}
 
			log.info("approve remarks=========>" + fuelFillingRecieptDTO.getRemarks());
			String url = SERVER_URL + "/fuelFillingRegister/approveFuelFilling"; 
//			fuelFillingRecieptDTO.setForwardTo(userMaster);
			baseDTO = httpService.post(url, fuelFillingRecieptDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FUEL_FILLING_REG_APPROVED_SUCCESSFULLY.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return fuelFillingRegisterPage();
			}
		} catch (Exception e) {
			log.error("approveTdsPayment method inside exception-------", e);
		}
		return null;
	}

	public String rejectFuelFilling() {
		// This is the View Page ForwardTo rejected payment
		log.error("<----Start FuelFillingRegisterBean.rejected: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("fuelFillingReciept selected id----------" + fuelFillingReciept.getId());
			baseDTO = new BaseDTO();
			if (fuelFillingReciept != null && fuelFillingReciept.getId() != null) {
				fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.RECEIPT_REJECTED);
			} else if (FuelFillingRegisterStatus.CANCELLATION_SUBMITTED.equals(fuelFillingRecieptDTO.getStatus())
					|| FuelFillingRegisterStatus.CANCELLATION_APPROVED.equals(fuelFillingRecieptDTO.getStatus())) {
				fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.CANCELLATION_REJECTED);
			} else {
				fuelFillingRecieptDTO.setStatus(FuelFillingRegisterStatus.REJECTED);
			}
			String url = SERVER_URL + "/fuelFillingRegister/rejectFuelFilling";
			baseDTO = httpService.post(url, fuelFillingRecieptDTO);
			if (baseDTO.getStatusCode() == 0) {

				if (fuelFillingRecieptDTO.getStatus().equals(FuelFillingRegisterStatus.RECEIPT_REJECTED)) {
					errorMap.notify(ErrorDescription.FUEL_FILLING_RECIEPT_REJECTED_SUCCESSFULLY.getErrorCode());
				} else if (fuelFillingRecieptDTO.getStatus().equals(FuelFillingRegisterStatus.REJECTED)) {
					errorMap.notify(ErrorDescription.FUEL_FILLING_REG_REJECTED_SUCCESSFULLY.getErrorCode());
				} else if (fuelFillingRecieptDTO.getStatus().equals(FuelFillingRegisterStatus.CANCELLATION_REJECTED)) {
					AppUtil.addWarn("Fuel Filling Cancellation Rejected Successfully");
				}
//				errorMap.notify(ErrorDescription.FUEL_FILLING_REG_REJECTED_SUCCESSFULLY.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return fuelFillingRegisterPage();
			}
		} catch (Exception e) {
			log.error("rejectFuelFilling method inside exception-------", e);
		}
		return null;
	}

	//

	// public void fuelFillingFileUpload(FileUploadEvent event) {
	// try
	// {
	// FuelFillingRegister fuelFillingRegisterFile = new FuelFillingRegister();
	// }
	// if (event == null || event.getFile() == null) {
	// log.error(" Upload Document is null ");
	// return;
	// }
	// file = event.getFile();
	// long size = file.getSize();
	// log.info("uploadSignature size==>" + size);
	// size = size / 1000;
	// letterSize=Long.valueOf(commonDataService.getAppKeyValue("FUEL_FILLING_REGISTER_UPLOAD_FILE_SIZE"));
	// if (size > letterSize) {
	// FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	// errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
	// return;
	// }
	// String docPathName =
	// commonDataService.getAppKeyValue("FUEL_FILLING_REGISTER_EXCEL_FILE_PATH");
	//
	// String filePath = Util.fileUpload(docPathName, file);
	// setFileName(file.getFileName());
	// fuelFillingRegisterFile.setFileType(filePath.substring(filePath.lastIndexOf(".")));
	// fuelFillingRegisterFile.setFilePath(filePath);
	// fuelFillingRegisterFile.setFileName(uploadedFile.getFileName());
	// tenderFileList.add(tenderFileDetails);
	// fuelFillingRegister.setDocumentPath(filePath);
	// for (int i = 0; i < fileList.size(); i++) {
	// String name = new String();
	// name = file.getFileName();
	// fileList.set(fileList.size() - 1, name);
	// }
	//
	// RequestContext context = RequestContext.getCurrentInstance();
	// context.execute("PF('addressDialog').hide();");
	// context.update("tenderForm");
	// errorMap.notify(ErrorDescription.INCOMINGTAPAL_FILEUPLOAD.getErrorCode());
	// log.info(" File upload is uploaded successfully");
	// }
	//
	// public void addRow() {
	// setFileName("");
	// for (int i = 0; i < 1; i++) {
	// String s = new String();
	// fileList.add(s);
	// }
	// }

	// public String uploadFile(FileUploadEvent event) {
	//
	// log.info("<------- fuelFillingRegister starts uploadFile method -------->");
	// String outFilePath =
	// commonDataService.getAppKeyValue("FUEL_FILLING_REGISTER_EXCEL_FILE_PATH");
	// try {
	// if (event == null || event.getFile() == null) {
	// log.error(" Document to be uploaded is null ");
	// return null;
	// }
	// file = event.getFile();
	// fileName = file.getFileName();
	// log.info("Content Type " + file.getContentType());
	// if (file == null) {
	// throw new RestException("File is empty");
	// }
	// try {
	// boolean fileCopied = copyFile(file.getInputstream(), fileName, outFilePath);
	// if (!fileCopied) {
	// throw new Exception("File not copied");
	// }
	// log.info("Filename " + fileName + " successfully");
	// BaseDTO baseDTO = new BaseDTO();
	//
	// FileDTO fileDTO = new FileDTO();
	// fileDTO.setName(fileName);
	// fileDTO.setPath(outFilePath);
	//
	// String url = SERVER_URL + "/fuelFillingRegister/upload";
	// log.info("<-Inside fuelFillingRegister starts uploadFile method to upload
	// File->" + url);
	// baseDTO = httpService.post(url, fileDTO);
	// if (baseDTO != null) {
	// if (baseDTO != null && baseDTO.getStatusCode() == 0) {
	// log.info("File uploaded successfully");
	// } else {
	// log.info("File upload unsuccessfull" + baseDTO);
	// return null;
	// }
	// }
	//
	// } catch (RestException exp) {
	// log.error("RestException in uploadFile method", exp);
	// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
	// return null;
	// } catch (Exception exp) {
	// log.error("Exception in uploadFile method", exp);
	// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
	// return null;
	// }
	//
	// log.info(" File Uploaded Successfully :: ");
	// } catch (Exception e) {
	// log.error("Exception occured While uploading file", e);
	// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	// return null;
	// }
	//
	// return null;
	// }
	//
	//
	// private boolean copyFile(InputStream inputStream, String name, String
	// outFilePath) {
	//
	// OutputStream outputStream = null;
	// boolean flag = false;
	// try {
	//
	// outputStream = new FileOutputStream(new File(outFilePath, name));
	//
	// int read = 0;
	// byte[] bytes = new byte[1024];
	//
	// while ((read = inputStream.read(bytes)) != -1) {
	// outputStream.write(bytes, 0, read);
	// }
	//
	// log.info("copyFile-Done!");
	// flag = true;
	// } catch (IOException ex) {
	// log.error("Exception at copyFile() ", ex);
	// flag = false;
	// } finally {
	// if (inputStream != null) {
	// try {
	// inputStream.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }
	// if (outputStream != null) {
	// try {
	// outputStream.close();
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	//
	// }
	// }
	// return flag;
	// }

	public String fuelFillingCancellation() {
		log.info("fuelFillingCancellation starts--------");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("fuelFillingCancellation register id--------" + fuelFillingRegister.getId());
			if (fuelFillingRegisterSaveDTO.getNote() == null || fuelFillingRegisterSaveDTO.getNote().isEmpty()) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			fuelFillingRegisterSaveDTO.setFuelFillingRegister(fuelFillingRegister);
			baseDTO = httpService.post(SERVER_URL + "/fuelFillingRegister/cancel", fuelFillingRegisterSaveDTO);
			if (baseDTO != null) {
				log.info("fuelFillingCancellation response------" + baseDTO);
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.FUEL_FILLING_CANCEL).getErrorCode());
					return FUEL_FILLING_REGISTER_LIST_URL;
				}
			}
		} catch (Exception e) {
			log.error("fuelFillingCancellation exception--------");
		}
		return null;
	}

	public void registerFileDownload() {
		InputStream input = null;
		try {
			if (fuelFillingRegister.getDocumentPath() != null) {
				File file = new File(fuelFillingRegister.getDocumentPath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				this.setFile(
						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void receiptFileDownload() {
		InputStream input = null;
		try {
			if (fuelFillingReciept.getDocumentPath() != null) {
				File file = new File(fuelFillingReciept.getDocumentPath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				this.setFile(
						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			File files = new File(fuelFillingRegister.getDocumentPath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	@PostConstruct
	public String showViewListPage() {
		log.info("FuelFillingRegisterBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String fuelFillingRegisterId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			fuelFillingRegisterId = httpRequest.getParameter("fuelFillingRegisterId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(fuelFillingRegisterId)) {
			init();
			selectedFuelFillingRegisterDTO = new FuelFillingRegisterListDTO();
			action = "VIEW";
			selectedFuelFillingRegisterDTO.setId(Long.parseLong(fuelFillingRegisterId));

			selectedNotificationId = Long.parseLong(notificationId);
			viewFuelFillingRegister();

		}
		log.info("FuelFillingRegisterBean showViewListPage() Ends");
		addButtonFlag = true;
		return FUEL_FILLING_REGISTER_VIEW_URL;
	}

}
