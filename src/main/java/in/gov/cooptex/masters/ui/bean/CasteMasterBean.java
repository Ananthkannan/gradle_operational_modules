package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("casteMasterBean")
@Scope("session")
public class CasteMasterBean {

	private final String CASTE_LIST_PAGE = "/pages/masters/caste/listCaste.xhtml?faces-redirect=true;";
	private final String CASTE_ADD_PAGE = "/pages/masters/caste/addCaste.xhtml?faces-redirect=true;";
	private final String CASTE_VIEW_PAGE = "/pages/masters/caste/viewCaste.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	CasteMaster selectedCasteMaster = new CasteMaster();

	@Getter
	@Setter
	CasteMaster casteMaster = new CasteMaster();

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;

	@Getter
	@Setter
	LazyDataModel<CasteMaster> casteMasterlazyLoadList;

	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<CasteMaster> casteMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<Community> communityList = new ArrayList<>();

	@Getter
	@Setter
	List<CasteMaster> casteMasterLazyList = new ArrayList<>();

	public String showListCasteMasterPage() {
		log.info("showListCasteMasterPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedCasteMaster = new CasteMaster();
		casteMasterList = commonDataService.getAllCasteMaster();
		communityList = commonDataService.getAllCommunity();
		loadCasteMasterListLazy();
		return CASTE_LIST_PAGE;
	}

	/*
	 * public void loadCasteMasterList() {
	 * log.info("<==== Starts CasteMasterBean.loadCasteMasterList========>"); try {
	 * BaseDTO response = httpService.get(SERVER_URL +
	 * "/castetypes/getallcastemaster/"); if (response.getStatusCode() == 0) {
	 * jsonResponse = mapper.writeValueAsString(response.getResponseContent());
	 * casteMasterList = mapper.readValue(jsonResponse, new
	 * TypeReference<List<CasteMaster>>() { });
	 * log.info("<====== CasteMasterBean.loadCasteMasterList====>"); } else {
	 * errorMap.notify(response.getStatusCode());
	 * log.info("<====== CasteMasterBean.loadCasteMasterList====>"); }
	 * }catch(Exception e) {
	 * log.error("Exception Ocured while Loading CasteMasterlist :: ", e); }
	 * log.info("<==== Ends CasteMasterBean.loadCasteMasterList========>"); }
	 */

	public void loadCasteMasterListLazy() {

		log.info("<-----CasteMaster list lazy load starts------>");
		casteMasterlazyLoadList = new LazyDataModel<CasteMaster>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<CasteMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					casteMasterLazyList = mapper.readValue(jsonResponse, new TypeReference<List<CasteMaster>>() {
					});
					if (casteMasterLazyList == null) {
						log.info(" CasteMasterList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" CasteMaster list search Successfully Completed");
					} else {
						log.error(" CasteMaster list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return casteMasterLazyList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading CasteMaster list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(CasteMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CasteMaster getRowData(String rowKey) {
				List<CasteMaster> responseList = (List<CasteMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (CasteMaster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedCasteMaster = res;
						return res;
					}
				}
				return null;
			}
		};
		log.info("<-----CasteMaster list lazy load ends------>");
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			CasteMaster request = new CasteMaster();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			/*
			 * Map<String, Object> searchFilters = new HashMap<>(); for (Map.Entry<String,
			 * Object> filter : filters.entrySet()) { String value =
			 * filter.getValue().toString(); log.info("Key : " + filter.getKey() +
			 * " Value : " + value);
			 * 
			 * if (filter.getKey().equals("name")) { log.info("casteName : " + value);
			 * searchFilters.put("casteName", value); } if (filter.getKey().equals("lname"))
			 * { log.info("casteName in tamil : " + value);
			 * searchFilters.put("casteNameTamil", value); } if
			 * (filter.getKey().equals("createdDate")) { log.info("createdDate : " + value);
			 * searchFilters.put("createdDate",value); } if
			 * (filter.getKey().equals("status")) { log.info("activeStatus : " + value);
			 * searchFilters.put("activeStatus",value); }
			 * 
			 * }
			 */
			request.setFilters(filters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/castetypes/getallcastemasterlistlazy", request);
			log.info("Search request response " + dataList);

		} catch (Exception e) {
			log.error("Exception Occured in CasteMaster load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String casteMasterListAction() {
		log.info("<====== CasteMasterBean.casteMasterListAction Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== casteMaster add page called.......====>");
				selectedCasteMaster = new CasteMaster();
				casteMaster = new CasteMaster();
				casteMaster.setCommunityMaster(new Community());
				return CASTE_ADD_PAGE;
			}
			if (selectedCasteMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_CASTE.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCasteMasterDelete').show();");
			} else {
				log.info("<----Loading CasteMaster Edit page.....---->" + selectedCasteMaster.getId());
				casteMaster = new CasteMaster();
				BaseDTO response = httpService
						.get(SERVER_URL + "/castetypes/getCastemasterbyid/" + selectedCasteMaster.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					casteMaster = mapper.readValue(jsonResponse, new TypeReference<CasteMaster>() {
					});
					log.info("<====== CasteMasterBean.casteMasterListAction Ends====>");
					return CASTE_ADD_PAGE;
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== CasteMasterBean.casteMasterListAction Ends====>");
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in CasteMasterBean.casteMasterListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== CasteMasterBean.casteMasterListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts CasteMasterBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/castetypes/deletecastemasterbyid/" + selectedCasteMaster.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.CASTE_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting casteMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends CasteMasterBean.deleteConfirm ---->");
		return showListCasteMasterPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts CasteMasterBean.onRowSelect ========>" + event);
		selectedCasteMaster = ((CasteMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends CasteMasterBean.onRowSelect ========>");
	}

	public String submit() {
		log.info("<=======Starts  CasteMasterBean.submit ======>" + casteMaster);

		if (casteMaster.getCommunityMaster() == null || casteMaster.getCommunityMaster().getId() == null) {
			errorMap.notify(ErrorDescription.CASTE_COMMUNITY_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (casteMaster.getName() == null || casteMaster.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.CASTE_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (casteMaster.getName().trim().length() < 3 || casteMaster.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.CASTE_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (casteMaster.getLname() == null || casteMaster.getLname().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.CASTE_LNAME_EMPTY.getErrorCode());
			return null;
		}
		if (casteMaster.getLname().trim().length() < 3 || casteMaster.getLname().trim().length() > 75) {
			errorMap.notify(ErrorDescription.CASTE_LNAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}

		if (action.equalsIgnoreCase("ADD")) {
			casteMaster.setCreatedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("EDIT")) {
			casteMaster.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL + "/castetypes/saveorupdate", casteMaster);
		// errorMap.notify(response.getStatusCode());
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.CASTE_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.CASTE_UPDATE_SUCCESS.getErrorCode());
			return showListCasteMasterPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends CasteMasterBean.submit ======>");
		return null;
	}

}
