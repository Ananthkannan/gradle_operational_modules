package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.RetailProcurementOrderDTO;
import in.gov.cooptex.operation.enums.RetailProcurementOrderStatus;
import in.gov.cooptex.operation.model.RetailProcurementOrder;
import in.gov.cooptex.operation.model.RetailProcurementOrderDetails;
import in.gov.cooptex.operation.model.RetailProcurementOrderLog;
import in.gov.cooptex.operation.production.dto.RetailProcurementOrderGeneratedResponseDTO;
import in.gov.cooptex.operation.production.dto.RetailProcurementOrderRequest;
import in.gov.cooptex.operation.production.dto.RetailProcurementOrderResponse;
import in.gov.cooptex.operation.production.dto.RetailProcurementOrderTotQuantityValueDTO;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailProcurementOrderBean")
@Scope("session")
public class RetailProcurementOrderBean extends CommonBean{

	private final String PROCUREMENT_ORDER_LIST_URL = "/pages/operation/retailproduction/procurement/listProcurementOrder.xhtml?faces-redirect=true;";

	private final String PROCUREMENT_ORDER_ADD_URL = "/pages/operation/retailproduction/procurement/createProcurementOrder.xhtml?faces-redirect=true;";

	private final String PROCUREMENT_ORDER_VIEW_URL = "/pages/operation/retailproduction/procurement/viewProcurementOrder.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	RetailProcurementOrder retailProcurementOrder;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	Boolean forwardToForFlag = false;

	@Getter
	@Setter
	Boolean approveRejectBtnFlag = true;

	@Setter
	@Getter
	public Long selectedForwardTo;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	RetailProcurementOrderRequest retailProcurementOrderRequest;

	@Getter
	@Setter
	List<RetailProductionPlan> retailProductionPlanList;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<String> procurementFromList;

	@Getter
	@Setter
	List<String> procurementToList;

	@Getter
	@Setter
	List<Integer> yearList = new ArrayList<>();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<RetailProcurementOrderDTO> viewLogResponseList = new ArrayList<>();

	@Getter
	@Setter
	Set<String> monthList = null;

	@Getter
	@Setter
	StringBuilder regionValues = null;

	@Getter
	@Setter
	StringBuilder productCategoryValues = null;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Setter
	@Getter
	List<EntityMaster> dandpOfficeList = new ArrayList<EntityMaster>();

	@Setter
	@Getter
	String action = null;

	@Getter
	@Setter
	List statusAndStageValues;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	LazyDataModel<RetailProcurementOrderResponse> retailProcurementOrderResponseList;

	@Getter
	@Setter
	RetailProcurementOrderResponse retailProcurementOrderResponse;

	@Getter
	@Setter
	RetailProcurementOrderTotQuantityValueDTO retailProcurementOrderTotQuantityValueDTO;

	@Getter
	@Setter
	List<RetailProcurementOrderTotQuantityValueDTO> retailProcurementOrderTotQuantityValueDTOList;

	@Setter
	@Getter
	List<ProductCategory> dandpProductCategoryList;

	@Getter
	@Setter
	Integer resultSize;

	@Getter
	@Setter
	Long totalQuantity;

	@Getter
	@Setter
	Long totalValue;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	String comments;

	@Getter
	@Setter
	String dialogHeader;

	@Getter
	@Setter
	String currentStatus;

	@Getter
	@Setter
	Double sumOfIssuedQty;
	@Getter
	@Setter
	Double sumOfIssuedValue;
	@Getter
	@Setter
	Double sumOfUnit;
	@Getter
	@Setter
	Double sumOfValues;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	public RetailProcurementOrderBean() {
		log.info("<--- Inside RetailProcurementOrderBean() --->");
		loadValues();
		totalQuantity = null;
		totalValue = null;
	}

	@PostConstruct
	public void init() {
		loadYear();
		loadStatusValues();
		totalQuantity = null;
		totalValue = null;
		log.info("<---Inside RetailProcurementOrderBean-init() ---> ");
	}

	public void loadForwardToUsers() {

		forwardToUsersList = commonDataService
				.loadForwardToUsersByFeature(AppFeatureEnum.RETAIL_PROCUREMENT_ORDER.toString());

	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = AppUtil.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("<--- Exception at loadValues() ---> " + e.toString());
			e.printStackTrace();
		}
	}

	private void loadStatusValues() {

		Object[] statusArray = RetailProcurementOrderStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public String showRetailProcurementOrderCreateForm() {
		log.info("<--- Inside showRetailProcurementOrderCreateForm() ");

		dataToggle = "collapse in";
		retailProcurementOrder = new RetailProcurementOrder();
		retailProcurementOrder.setRetailProcurementOrderDetails(new ArrayList<>());
		retailProcurementOrderRequest = new RetailProcurementOrderRequest();
		dandpOfficeList = new ArrayList<>();
		dandpProductCategoryList = new ArrayList<>();
		procurementFromList = new ArrayList<>();
		procurementToList = new ArrayList<>();
		totalQuantity = null;
		totalValue = null;
		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();
		sumOfIssuedQty = 0.0;
		sumOfIssuedValue = 0.0;
		sumOfUnit = 0.0;
		sumOfValues = 0.0;
		loadRetailProductionPlanList();
		loadForwardToUsers();
		loadEmployeeLoggedInUserDetails();
		return PROCUREMENT_ORDER_ADD_URL;
	}

	public void loadRetailProductionPlanList() {
		log.info("<--- Inside getProductionPlanList() --->");
		String requestPath = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/procurementorder/getretailproductionplanlist";
		BaseDTO baseDTO = httpService.get(requestPath);
		try {
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailProductionPlanList = (List<RetailProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<RetailProductionPlan>>() {
							});
					log.info("RetailProductionPlanList with Size of : " + retailProductionPlanList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("<--- Error in getProductionPlanList() --->" + e);
		}
	}

	public String onChangeProductionPlan() {
		log.info("onChangeProductionPlan ", retailProcurementOrder.getRetailProductionPlan());

		dataToggle = "collapse in";

		RetailProductionPlan retailProductionPlan = retailProcurementOrder.getRetailProductionPlan();

		RetailProcurementOrder existingRetailProcurementOrder = retailProcurementOrder;

		retailProcurementOrder = new RetailProcurementOrder();
		if (existingRetailProcurementOrder != null && existingRetailProcurementOrder.getId() != null) {
			retailProcurementOrder.setId(existingRetailProcurementOrder.getId());
			retailProcurementOrder.setFromMonth(existingRetailProcurementOrder.getFromMonth());
			retailProcurementOrder.setFromYear(existingRetailProcurementOrder.getFromYear());
			retailProcurementOrder.setToMonth(existingRetailProcurementOrder.getToMonth());
			retailProcurementOrder.setToYear(existingRetailProcurementOrder.getToYear());
			retailProcurementOrder.setCreatedBy(existingRetailProcurementOrder.getCreatedBy());
			retailProcurementOrder.setCreatedDate(existingRetailProcurementOrder.getCreatedDate());
		}
		retailProcurementOrder.setRetailProcurementOrderDetails(new ArrayList<>());

		dandpOfficeList = new ArrayList<>();
		procurementFromList = new ArrayList<>();
		procurementToList = new ArrayList<>();
		dandpProductCategoryList = new ArrayList<>();
		totalQuantity = null;
		totalValue = null;

		retailProcurementOrder.setRetailProductionPlan(retailProductionPlan);

		if (retailProcurementOrder.getRetailProductionPlan() == null) {
			regionValues = new StringBuilder();
			productCategoryValues = new StringBuilder();
			return null;
		}

		loadRegionandCategoryDetailsFromPlan(retailProcurementOrder.getRetailProductionPlan().getId());
		loadDAndpOfficeListUnderPlan(retailProcurementOrder.getRetailProductionPlan().getId());
		getMonthYearListBasedOnPlan();
		if (procurementFromList == null || procurementFromList.size() == 0 || procurementToList == null
				|| procurementToList.size() == 0) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_ALREDY_EXIST_FOR_PLAN.getCode());
			return null;
		}
		return null;
	}

	public void getMonthYearListBasedOnPlan() {

		if (retailProcurementOrderRequest == null) {
			retailProcurementOrderRequest = new RetailProcurementOrderRequest();
		}

		Long planId = retailProcurementOrder.getRetailProductionPlan().getId();

		RetailProductionPlanRequest response = null;
		String URL = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/procurementorder/getmonthyearlist/basedonplan/" + planId;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				response = mapper.readValue(jsonValue, RetailProductionPlanRequest.class);
			}
		} catch (Exception e) {
			log.error("Error in Get ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		if (response != null && response.getPlanFrom() != null && response.getPlanTo() != null
				&& retailProcurementOrder.getId() == null) {

			Date planTo = AppUtil.addOrMinusMonth(response.getPlanTo(), 1);

			if (planTo.equals(retailProcurementOrder.getRetailProductionPlan().getPlanTo())) {
				procurementFromList = new ArrayList<>();
				procurementToList = new ArrayList<>();
			} else {
				procurementFromList = loadProcurementFromAndToForSelectedPlan(AppUtil.getFirstDateOfMonth(planTo),
						retailProcurementOrder.getRetailProductionPlan().getPlanTo());
				procurementToList = loadProcurementFromAndToForSelectedPlan(AppUtil.getFirstDateOfMonth(planTo),
						retailProcurementOrder.getRetailProductionPlan().getPlanTo());
				retailProcurementOrderRequest.setProcurementFrom(procurementFromList.get(0));
			}

		} else {
			procurementFromList = loadProcurementFromAndToForSelectedPlan(
					retailProcurementOrder.getRetailProductionPlan().getPlanFrom(),
					retailProcurementOrder.getRetailProductionPlan().getPlanTo());
			procurementToList = loadProcurementFromAndToForSelectedPlan(
					retailProcurementOrder.getRetailProductionPlan().getPlanFrom(),
					retailProcurementOrder.getRetailProductionPlan().getPlanTo());
			if (retailProcurementOrder.getId() == null) {
				retailProcurementOrderRequest.setProcurementFrom(procurementFromList.get(0));
			}
		}
	}

	public void loadYear() {

		log.info("RetailProductionPlanBean.loadYear Method Started");
		// Getting current year and next year
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		yearList = new ArrayList<>();
		for (int count = 0; count < 2; count++) {
			yearList.add(currentYear + count);
		}
		log.info("RetailProductionPlanBean.loadYear Method Completed :  " + yearList);
	}

	private List<String> loadProcurementFromAndToForSelectedPlan(Date planFromDate, Date planToDate) {
		log.info("Inside datesBetween");
		List<String> dateInterval = new ArrayList<String>();
		Calendar c = Calendar.getInstance();
		c.setTime(planFromDate);
		while (c.getTimeInMillis() < planToDate.getTime()) {

			dateInterval.add(new SimpleDateFormat("MMM").format(c.getTime()).toString() + "-"
					+ Long.valueOf(new SimpleDateFormat("yyyy").format(c.getTime())).toString());
			c.add(Calendar.MONTH, 1);
		}
		log.info("Inside datesBetween list  " + dateInterval);
		return dateInterval;
	}

	private void loadValuesFromPlan(RetailProductionPlan retailProductionPlan) {
		regionValues = new StringBuilder();
		for (RetailProductionPlanRegion regionValue : retailProductionPlan.getRetailProductionPlanRegionList()) {
			if (regionValue != null && regionValue.getEntityMaster() != null
					&& regionValue.getEntityMaster().getCode() != null
					&& regionValue.getEntityMaster().getName() != null)
				regionValues.append(regionValue.getEntityMaster().getCode()).append("/")
						.append(regionValue.getEntityMaster().getName()).append(" ");
		}
		productCategoryValues = new StringBuilder();
		for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
			if (categoryValue != null && categoryValue.getProductCatCode() != null
					&& categoryValue.getProductCatName() != null)
				productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
						.append(categoryValue.getProductCatName()).append(" ");
		}

	}

	private void loadRegionandCategoryDetailsFromPlan(Long planId) {
		log.info("<--- Inside loadRegionandCategoryDetailsFromPlan() with plan ID---> " + planId);

		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();

		try {
			//String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/get/" + planId;
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getRetailProductionPlan/" + planId;
			log.info("<--- loadRegionandCategoryDetailsFromPlan() URL ---> " + URL);
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			retailProcurementOrder
					.setRetailProductionPlan(objectMapper.readValue(jsonResponse, RetailProductionPlan.class));
			loadValuesFromPlan(retailProcurementOrder.getRetailProductionPlan());
		} catch (Exception e) {
			log.error("Exception at getRegionandCategoryDetailsFromPlan >>> ", e.toString());
		}
	}

	/*
	 * used to load list of DANDP offices under selected plan
	 */

	private void loadDAndpOfficeListUnderPlan(Long id) {
		log.info("loadDAndpOfficeListUnderPlan" + id);
		dandpOfficeList = commonDataService.loadDAndpOfficeListUnderPlan(id);
		log.info("loadDAndpOfficeListUnderPlan" + dandpOfficeList.size());
	}

	public void onChangeDnpOffice() {
		log.info("onChangeDnpOffice " + retailProcurementOrder.getProducingRegion());

		dandpProductCategoryList = new ArrayList<>();
		totalQuantity = null;
		totalValue = null;
		retailProcurementOrder.setRetailProcurementOrderDetails(null);
		retailProcurementOrder.setProductCategory(null);

		if (retailProcurementOrder.getProducingRegion() != null) {
			loadCategoriesUnderDandPOffice(retailProcurementOrder.getProducingRegion().getId());
		}
	}

	private void loadCategoriesUnderDandPOffice(Long dandpId) {

		log.info("loadCategoriesUnderDandPOffice " + dandpId);

		String URL = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/procurementorder/loaddandpofficecategorylist/" + dandpId;

		try {

			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					dandpProductCategoryList = (List<ProductCategory>) mapper.readValue(jsonResponse,
							new TypeReference<List<ProductCategory>>() {
							});
					log.info("<--- productCategoryList size ---> " + dandpProductCategoryList.size());
				}
			}
		} catch (Exception e) {
			log.error("Error in loadDAndpOffices List --==>", e);
		}

	}

	public void onChangeProductCategory() {
		log.info("onChangeProductCategory ");
		retailProcurementOrder.setRetailProcurementOrderDetails(null);
		totalQuantity = null;
		totalValue = null;
		if (retailProcurementOrder.getProducingRegion() != null
				&& retailProcurementOrder.getProductCategory() != null) {
			log.info(retailProcurementOrder.getProductCategory().getId() + "  "
					+ retailProcurementOrder.getProducingRegion().getId());
			loadTotQuantityAndTotValue(retailProcurementOrder.getProducingRegion().getId(),
					retailProcurementOrder.getProductCategory().getId(),
					retailProcurementOrder.getRetailProductionPlan().getId());
		}
	}

	/*
	 * used to load Total Quantity and Total value under Selected DANDP and Product
	 * Category
	 * 
	 */
	private void loadTotQuantityAndTotValue(Long dandpId, Long productCategoryId, Long planId) {

		log.info("<--- Inside loadTotQuantityAndTotValue() with dandpId and productCategoryId" + "--->  " + dandpId
				+ "  " + productCategoryId);

		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/getQtyAndValue/"
				+ productCategoryId + "/" + dandpId + "/" + planId;

		BaseDTO baseDTO = httpService.get(URL);

		try {
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailProcurementOrderTotQuantityValueDTO = mapper.readValue(jsonResponse,
							new TypeReference<RetailProcurementOrderTotQuantityValueDTO>() {
							});
				}

			}

			if (retailProcurementOrderTotQuantityValueDTO == null) {

				log.info("value and qty is null");

				Util.addWarn("No values for Plan and category");

				return;

			}

			if (retailProcurementOrderTotQuantityValueDTO.getTotalQuantity() != null
					&& retailProcurementOrderTotQuantityValueDTO.getTotalValue() != null) {

				totalQuantity = retailProcurementOrderTotQuantityValueDTO.getTotalQuantity();

				totalValue = retailProcurementOrderTotQuantityValueDTO.getTotalValue();
			}
		} catch (Exception e) {
			log.error("Error in retailProcurementOrderTotQuantityValueDTO --==>", e);
		}

	}

	private void loadProcurementOrderLazyData() {

		log.info("loadProcurementOrderLazyData");
		addButtonFlag = false;

		retailProcurementOrderResponseList = new LazyDataModel<RetailProcurementOrderResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProcurementOrderResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				log.info("load is called.");
				List<RetailProcurementOrderResponse> data = new ArrayList<RetailProcurementOrderResponse>();
				try {
					BaseDTO baseDTO = getProcurementOrderSearchData(first / pageSize, pageSize, sortField, sortOrder,
							filters);

					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse,
							new TypeReference<List<RetailProcurementOrderResponse>>() {
							});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RetailProcurementOrderResponse response) {
				log.info("getRowKey is called.");
				return response != null ? response.getPlanId() : null;
			}

			@Override
			public RetailProcurementOrderResponse getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RetailProcurementOrderResponse> responseList = (List<RetailProcurementOrderResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RetailProcurementOrderResponse response : responseList) {
					if (response.getId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getProcurementOrderSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		retailProcurementOrderResponse = new RetailProcurementOrderResponse();
		BaseDTO baseDTO = new BaseDTO();

		RetailProcurementOrderRequest request = new RetailProcurementOrderRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			if (entry.getKey().equals("planCodeOrName")) {
				request.setPlanCodeOrName(value);
			}

			if (entry.getKey().equals("danpCodeOrName")) {
				request.setDanpCodeOrName(value);
			}

			if (entry.getKey().equals("productCategoryCodeOrName")) {
				request.setProductCategoryCodeOrName(value);
			}

			if (entry.getKey().equals("planNumber")) {
				request.setPlanNumber(value);
			}

			if (entry.getKey().equals("status")) {
				log.info("selectedStatus : " + value);
				request.setStatus(value);
			}
		}

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	public String onChangeDate() {
		if (retailProcurementOrder != null && retailProcurementOrder.getValidDate() != null
				&& retailProcurementOrder.getNotLaterThan() != null
				&& retailProcurementOrder.getValidDate().after(retailProcurementOrder.getNotLaterThan())) {
			log.info("Procurement To is required");
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_VALID_DATE_NOT_LATER_THAN.getCode());
			return null;
		}
		return null;
	}

	/*
	 * used to generate procurement order for given inputs
	 */

	public String generateProcurementOrder() {

		log.info("Inside generateProcurementOrder()---" + retailProcurementOrder);

		dataToggle = "collapse in";

		if (retailProcurementOrder != null && retailProcurementOrder.getValidDate() != null
				&& retailProcurementOrder.getNotLaterThan() != null
				&& retailProcurementOrder.getValidDate().after(retailProcurementOrder.getNotLaterThan())) {
			log.info("Procurement To is required");
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_VALID_DATE_NOT_LATER_THAN.getCode());
			return null;
		}

		if (procurementFromList == null || procurementFromList.size() == 0 || procurementToList == null
				|| procurementToList.size() == 0) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_ALREDY_EXIST_FOR_PLAN.getCode());
			return null;
		}

		retailProcurementOrderRequest.setProdPlanId(retailProcurementOrder.getRetailProductionPlan().getId());
		retailProcurementOrderRequest.setProducingRegionId(retailProcurementOrder.getProducingRegion().getId());
		retailProcurementOrderRequest.setProductCategoryId(retailProcurementOrder.getProductCategory().getId());

		log.info("Request Object after setting values--- " + retailProcurementOrderRequest);

		try {

			String URL = serverURL + appPreference.getOperationApiUrl()
					+ "/retail/procurementorder/generateprocurementorder";
			BaseDTO baseDTO = httpService.post(URL, retailProcurementOrderRequest);
			if (baseDTO != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					List<RetailProcurementOrderGeneratedResponseDTO> retailProcurementOrderGeneratedResponseDTOList = objectMapper
							.readValue(jsonResponse,
									new TypeReference<List<RetailProcurementOrderGeneratedResponseDTO>>() {
									});

					retailProcurementOrder.setRetailProcurementOrderDetails(new ArrayList<>());

					if (retailProcurementOrderGeneratedResponseDTOList != null
							&& !retailProcurementOrderGeneratedResponseDTOList.isEmpty()) {
						for (RetailProcurementOrderGeneratedResponseDTO retailProcurementOrderGeneratedResponseDTO : retailProcurementOrderGeneratedResponseDTOList) {

							RetailProcurementOrderDetails retailProcurementOrderDetails = new RetailProcurementOrderDetails();

							ProductVarietyMaster productVarietyMaster = new ProductVarietyMaster();
							productVarietyMaster.setId(retailProcurementOrderGeneratedResponseDTO.getProductId());
							productVarietyMaster.setCode(retailProcurementOrderGeneratedResponseDTO.getProductCode());
							productVarietyMaster.setName(retailProcurementOrderGeneratedResponseDTO.getProductName());
							retailProcurementOrderDetails.setProductVarietyMaster(productVarietyMaster);

							EntityMaster entityMaster = new EntityMaster();
							entityMaster.setId(retailProcurementOrderGeneratedResponseDTO.getIntendingRegionId());
							entityMaster.setCode(retailProcurementOrderGeneratedResponseDTO.getIntendingRegionCode());
							entityMaster.setName(retailProcurementOrderGeneratedResponseDTO.getIntendingRegionName());
							retailProcurementOrderDetails.setIntendingRegion(entityMaster);

							retailProcurementOrderDetails
									.setItemQuantity(retailProcurementOrderGeneratedResponseDTO.getTotalQuantity());
							retailProcurementOrderDetails
									.setItemValue(retailProcurementOrderGeneratedResponseDTO.getTotalValue());

							retailProcurementOrderDetails.setItemIssuedQuantity(
									retailProcurementOrderGeneratedResponseDTO.getIssuedQuantity());
							retailProcurementOrderDetails
									.setItemIssuedValue(retailProcurementOrderGeneratedResponseDTO.getIssuedValue());

							UomMaster uomMaster = new UomMaster();
							uomMaster.setId(retailProcurementOrderGeneratedResponseDTO.getUomId());
							uomMaster.setCode(retailProcurementOrderGeneratedResponseDTO.getUomCode());
							retailProcurementOrderDetails.setUomMaster(uomMaster);

							retailProcurementOrder.getRetailProcurementOrderDetails()
									.add(retailProcurementOrderDetails);
						}
						dataToggle = "collapse";
					}

				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		calculateTotalValues(retailProcurementOrder.getRetailProcurementOrderDetails());
		return null;
	}

	public String createRetailProcurementOrder(String requestType) {

		log.info("Inside submitRetailProcurementOrder()---");

		if (retailProcurementOrder != null && retailProcurementOrder.getValidDate() != null
				&& retailProcurementOrder.getNotLaterThan() != null
				&& retailProcurementOrder.getValidDate().after(retailProcurementOrder.getNotLaterThan())) {
			log.info("Procurement To is required");
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_VALID_DATE_NOT_LATER_THAN.getCode());
			return null;
		}

		if (procurementFromList == null || procurementFromList.size() == 0 || procurementToList == null
				|| procurementToList.size() == 0) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_ALREDY_EXIST_FOR_PLAN.getCode());
			return null;
		}

		if (retailProcurementOrder.getForwardTo() == null) {
			errorMap.notify(ErrorDescription.FORWARD_TO_EMPTY.getCode());
			return null;
		}

		RetailProcurementOrderLog currentLog = new RetailProcurementOrderLog();
		currentLog.setRetailProcurementOrder(retailProcurementOrder);

		if (requestType.equals(RetailProcurementOrderStatus.INITIATED.name())) {
			currentLog.setStatus(RetailProcurementOrderStatus.INITIATED);
		} else {
			currentLog.setStatus(RetailProcurementOrderStatus.SUBMITTED);
		}

		retailProcurementOrder.setCurrentLog(currentLog);
		retailProcurementOrder.setFromMonth(retailProcurementOrderRequest.getProcurementFrom().split("-")[0]);
		retailProcurementOrder.setFromYear(
				AppUtil.stringToLong(retailProcurementOrderRequest.getProcurementFrom().split("-")[1], null));
		retailProcurementOrder.setToMonth(retailProcurementOrderRequest.getProcurementTo().split("-")[0]);
		retailProcurementOrder
				.setToYear(AppUtil.stringToLong(retailProcurementOrderRequest.getProcurementTo().split("-")[1], null));

		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/create";
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(url, retailProcurementOrder);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Procuement Order saved successfully");
					if (currentLog.getStatus().equals(RetailProcurementOrderStatus.INITIATED)) {
						errorMap.notify(ErrorDescription.RETAIL_PROCUREMENT_ORDER_SAVED_SUCCESSFULLY.getCode());
					} else {
						errorMap.notify(ErrorDescription.RETAIL_PROCUREMENT_ORDER_SUBMITTED_SUCCESSFULLY.getCode());
					}
					return getProcurementOrderList();
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in submitRetailProcurementOrder", exp);
		}

		return null;
	}

	public String getProcurementOrderList() {
		log.info("GEt List  Called");
		retailProcurementOrderResponse = new RetailProcurementOrderResponse();
		loadProcurementOrderLazyData();
		return PROCUREMENT_ORDER_LIST_URL;
	}

	public String processDelete() {
		log.info("RetailProcurementOrder.processDelete method started");
		if (retailProcurementOrderResponse == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(24001);
			return null;
		} else {
			if (!retailProcurementOrderResponse.getStatus().equals(RetailProcurementOrderStatus.INITIATED.name())) {
				errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_CANNOT_DELETE.getCode());
				return null;
			}
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
		return null;
	}

	public String delete() {
		log.info("RetailProcurementOrder.delete method started");
		if (retailProcurementOrderResponse == null || retailProcurementOrderResponse.getId() == null) {
			errorMap.notify(24001);
			return null;
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/delete/"
				+ retailProcurementOrderResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete draft plan", exp);
			errorMap.notify(1);
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.INFO_RETAIL_PROCUREMNET_ORDER_DELETED_SUCCESSFULLY.getCode());
			return getProcurementOrderList();
		} else {
			errorMap.notify(baseDTO.getStatusCode());
			return getProcurementOrderList();
		}

	}

	public String clear() {

		log.info("<--- Inside clear() ---> " + action);
		retailProcurementOrder.setRetailProductionPlan(new RetailProductionPlan());
		retailProcurementOrder = new RetailProcurementOrder();
		retailProcurementOrderRequest = new RetailProcurementOrderRequest();
		totalQuantity = null;
		totalValue = null;
		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();
		return PROCUREMENT_ORDER_ADD_URL;

	}

	public String editProcurementOrder() {

		log.info("Edit ProcurementOrder started");

		dataToggle = "collapse in";

		if (retailProcurementOrderResponse == null) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (!retailProcurementOrderResponse.getStatus().equals(RetailProcurementOrderStatus.INITIATED.name())
				&& !retailProcurementOrderResponse.getStatus().equals(RetailProcurementOrderStatus.REJECTED.name())) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_CANNOT_EDIT_RECORD.getCode());
			return null;
		}

		log.info("Request " + retailProcurementOrderResponse);
        loadEmployeeLoggedInUserDetails();
		getById(retailProcurementOrderResponse.getId());

		if (retailProcurementOrder != null) {
			prepareEdit(retailProcurementOrder);
		}
		calculateTotalValues(retailProcurementOrder.getRetailProcurementOrderDetails());
		return PROCUREMENT_ORDER_ADD_URL;
	}

	public String viewProcurementOrder() {

		log.info("View Button is pressed");

		if (retailProcurementOrderResponse == null) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}

		log.info("Request " + retailProcurementOrderResponse);

		getById(retailProcurementOrderResponse.getId());

		if (retailProcurementOrder != null) {
			prepareView(retailProcurementOrder);
		}
		calculateTotalValues(retailProcurementOrder.getRetailProcurementOrderDetails());
		return PROCUREMENT_ORDER_VIEW_URL;
	}

	public void prepareView(RetailProcurementOrder retailProcurementOrder) {

		if (retailProcurementOrder.getRetailProductionPlan() != null) {

			loadValuesFromPlan(retailProcurementOrder.getRetailProductionPlan());

			retailProcurementOrderRequest = new RetailProcurementOrderRequest();
			String procurementFrom = retailProcurementOrder.getFromMonth() + "-" + retailProcurementOrder.getFromYear();
			String procurementTo = retailProcurementOrder.getToMonth() + "-" + retailProcurementOrder.getToYear();
			retailProcurementOrderRequest.setProcurementFrom(procurementFrom);
			retailProcurementOrderRequest.setProcurementTo(procurementTo);

		}

	}

	public void prepareEdit(RetailProcurementOrder retailProcurementOrder) {

		loadRetailProductionPlanList();

		if (retailProcurementOrder.getRetailProductionPlan() != null) {
			loadValuesFromPlan(retailProcurementOrder.getRetailProductionPlan());
			loadDAndpOfficeListUnderPlan(retailProcurementOrder.getRetailProductionPlan().getId());
			getMonthYearListBasedOnPlan();
			retailProcurementOrderRequest = new RetailProcurementOrderRequest();
			String procurementFrom = retailProcurementOrder.getFromMonth() + "-" + retailProcurementOrder.getFromYear();
			String procurementTo = retailProcurementOrder.getToMonth() + "-" + retailProcurementOrder.getToYear();
			retailProcurementOrderRequest.setProcurementFrom(procurementFrom);
			retailProcurementOrderRequest.setProcurementTo(procurementTo);

			if (retailProcurementOrder.getProducingRegion() != null) {
				loadCategoriesUnderDandPOffice(retailProcurementOrder.getProducingRegion().getId());

				if (retailProcurementOrder.getProductCategory() != null) {
					loadTotQuantityAndTotValue(retailProcurementOrder.getProducingRegion().getId(),
							retailProcurementOrder.getProductCategory().getId(),
							retailProcurementOrder.getRetailProductionPlan().getId());
				}

			}
		}
	}

	public void getById(Long id) {
		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/get/" + id;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailProcurementOrder = mapper.readValue(jsonValue, RetailProcurementOrder.class);

				ObjectMapper objectMapper = new ObjectMapper();
				objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
				viewLogResponseList = objectMapper.readValue(jsonResponse,
						new TypeReference<List<RetailProcurementOrderDTO>>() {
						});

				forwardToUsersList = commonDataService
						.loadForwardToUsersByFeature(AppFeatureEnum.RETAIL_PROCUREMENT_ORDER.toString());
				if (retailProcurementOrder != null) {
					setPreviousApproval(retailProcurementOrder.getForwardFor());

					if ("VIEW".equalsIgnoreCase(action)) {
						if (retailProcurementOrder.getForwardTo() != null) {
							if (loginBean.getUserMaster().getId().equals(retailProcurementOrder.getForwardTo().getId())) {
								if (retailProcurementOrderResponse.getStatus().equals("REJECTED") || retailProcurementOrderResponse.getStatus().equals("FINALIZED")) {
									forwardToForFlag = false;
									approveRejectBtnFlag = false;
								} else {
									forwardToForFlag = true;
									approveRejectBtnFlag = true;
								}
							} else {
								approveRejectBtnFlag = false;
								forwardToForFlag = false;
							}
						}
						setFinalApproveFlag(retailProcurementOrder.getForwardFor());
						if ("FINALIZED".equals(retailProcurementOrderResponse.getStatus())) {
							buttonFlag = true;
						}
						log.info("button flag-------------"+buttonFlag);
						// retailProcurementOrder.setRemarks(null);
					}
				}

			}
		} catch (Exception e) {
			log.error("Error in Get ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);

		if (value.equals("APPROVED")) {
			dialogHeader = "Approve Comments";
		} else if (value.equals("REJECTED")) {
			dialogHeader = "Reject Comments";
		} else {
			dialogHeader = "Finalize Comments";
		}

		comments = null;
		currentStatus = value;
	}

	public String statusUpdate() {
		log.info("Status Updated is called");
		log.info(retailProcurementOrder.getId());
		
		log.info("commments----------"+comments);
		log.info("currentStatus----------"+currentStatus);
		
		BaseDTO baseDTO = null;
		RetailProcurementOrderRequest request = new RetailProcurementOrderRequest();
		request.setId(retailProcurementOrder.getId());
		request.setStatus(currentStatus);
		request.setComments(comments);
		
		if(currentStatus.equals("APPROVED")) {
			log.info("user master------"+retailProcurementOrder.getForwardTo());
			request.setForwardFor(retailProcurementOrder.getForwardFor());
			request.setForwardTo(retailProcurementOrder.getForwardTo());
			request.setNote(retailProcurementOrder.getNote());
		}

		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/procurementorder/status/update";
		baseDTO = httpService.post(URL, request);
		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return PROCUREMENT_ORDER_LIST_URL;
	}

	public void calculateTotalValues(List<RetailProcurementOrderDetails> retailProcurementOrderDetails) {
		sumOfIssuedQty = 0.0;
		sumOfIssuedValue = 0.0;
		sumOfUnit = 0.0;
		sumOfValues = 0.0;

		if (retailProcurementOrderDetails == null) {
			return;
		}

		for (RetailProcurementOrderDetails details : retailProcurementOrderDetails) {
			sumOfIssuedQty = sumOfIssuedQty + details.getItemIssuedQuantity();
			sumOfIssuedValue = sumOfIssuedValue + details.getItemIssuedValue();
			sumOfUnit = sumOfUnit + details.getItemQuantity();
			sumOfValues = sumOfValues + details.getItemValue();
		}
	}

	public void onPageLoad() {
		log.info("onPageLoad method started");
		addButtonFlag = false;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		retailProcurementOrderResponse = ((RetailProcurementOrderResponse) event.getObject());
		addButtonFlag = true;
	}

}
