package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.dto.BankBranchDTO;
import in.gov.cooptex.core.accounts.dto.BankDetailsDTO;
import in.gov.cooptex.core.accounts.dto.PaymentRequestDTO;
import in.gov.cooptex.core.accounts.dto.PaymentRequestDetailsDTO;
import in.gov.cooptex.core.accounts.dto.PaymentResponseDTO;
import in.gov.cooptex.core.accounts.dto.PaymentStatusDetailsDTO;
import in.gov.cooptex.core.accounts.dto.VoucherCountDTO;
import in.gov.cooptex.core.accounts.dto.VoucherRequestDTO;
import in.gov.cooptex.core.accounts.dto.VoucherResponseDTO;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.accounts.model.VoucherDetails;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.PaymentMethod;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.ProductQRCodeItemDetailsDTO;
import in.gov.cooptex.operation.model.ProductQRCodeItem;
import in.gov.cooptex.operation.production.dto.BankPaymentReponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("paymentBean")
@Scope("session")
public class PaymentBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/accounts/paymentVoucher/listBankPayment.xhtml?faces-redirect=true;";

	private final String PAYMENT_LIST_PAGE_URL = "/pages/accounts/paymentVoucher/bankPaymentList.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/accounts/paymentVoucher/createBankPayment.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/accounts/paymentVoucher/viewBankPayment.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	LazyDataModel<BankPaymentReponse> bankPaymentResponseLazyList;

	@Getter
	@Setter
	LazyDataModel<PaymentResponseDTO> paymentList;

	@Getter
	@Setter
	BankPaymentReponse bankPaymentReponse;

	@Getter
	@Setter
	ProductQRCodeItemDetailsDTO singleProductItem;

	@Getter
	@Setter
	PaymentStatusDetailsDTO paymentStatusDetailsDTO;

	@Getter
	@Setter
	Double totalValueVoucher;

	@Getter
	@Setter
	Double totalValueVoucherSociety;

	@Getter
	@Setter
	Double totalVoucherAmount;

	@Getter
	@Setter
	Integer totalVoucherPaidCount;

	@Getter
	@Setter
	Double totalVoucherAmountPaid;

	@Getter
	@Setter
	Date chequeDate;

	@Getter
	@Setter
	List<VoucherDetails> voucherDetailsList;

	@Getter
	@Setter
	PaymentResponseDTO paymentResponseDTO;

	@Getter
	@Setter
	VoucherCountDTO voucherCountDTO;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String paidByName;

	@Getter
	@Setter
	List<Voucher> vouchersList;

	@Getter
	@Setter
	List<BankBranchDTO> bankList;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	EmployeeMaster employee;

	@Getter
	@Setter
	List<BankBranchMaster> branchList;

	@Getter
	@Setter
	List<EntityBankBranch> accountNumberList;

	@Getter
	@Setter
	List<PaymentMethod> paymentMethodList;

	@Getter
	@Setter
	PaymentMethod paymentMethod;

	@Getter
	@Setter
	List<PaymentMode> paymentModeList;

	@Getter
	@Setter
	String documentNumber;

	@Getter
	@Setter
	PaymentMode paymentMode;

	@Getter
	@Setter
	PaymentRequestDTO paymentRequest;

	@Getter
	@Setter
	LazyDataModel<VoucherResponseDTO> voucherList;

	@Getter
	@Setter
	List<VoucherResponseDTO> selectedVouchersList;

	@Getter
	@Setter
	List<VoucherResponseDTO> totalVoucherList;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	ProductQRCodeItem selectedProductQRCodeItem;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	boolean paidListFlag;

	@Getter
	@Setter
	String bankName;

	@Getter
	@Setter
	String branchName;

	@Getter
	@Setter
	String accountNumber;

	@Getter
	@Setter
	String paymentMethodName;

	@Getter
	@Setter
	BankBranchDTO bankMaster;

	@Getter
	@Setter
	BankBranchMaster branchValue;

	@Getter
	@Setter
	EntityBankBranch entityBankBranch;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	List<String> statusValuesList;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	Integer paymentListSize;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	StreamedContent downloadFile;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		loadUrl();
		loadStatusValues();
		loadVoucherList();
		addButtonFlag = false;
	}

	private void loadStatusValues() {
		Object[] statusArray = VoucherStatus.class.getEnumConstants();

		statusValuesList = new ArrayList<>();

		for (Object status : statusArray) {
			statusValuesList.add(status.toString());
		}

		log.info("<--- statusValuesList ---> " + statusValuesList);
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		VoucherRequestDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getvoucherlistbystatus";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public BaseDTO getSearchDataForPayment(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		paymentResponseDTO = new PaymentResponseDTO();
		PaymentRequestDTO request = getPaymentSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	private VoucherRequestDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		VoucherRequestDTO request = new VoucherRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		log.info("PaymentVoucherRequest  " + request);
		request.setFilters(filters);

		return request;
	}

	private PaymentRequestDTO getPaymentSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		PaymentRequestDTO request = new PaymentRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		log.info("PaymentRequestDTO  " + request);
		request.setFilters(filters);

		return request;
	}

	public String getListPage() {
		log.info("Going to list page");
		bankPaymentReponse = new BankPaymentReponse();
		selectedVouchersList = new ArrayList<>();
		totalValueVoucher = 0.00;
		totalValueVoucherSociety = 0.00;
		totalVoucherAmount = 0.00;
		totalVoucherAmountPaid = 0.00;
		paidListFlag = false;
		loadVoucherList();
		loadVoucherAmountDetails();
		loadEmployeeList();
		return LIST_PAGE_URL;
	}

	public void loadEmployeeList() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getemployeelist";

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			log.info("employeeList", employeeList);
		} catch (Exception exp) {
			log.error("Exception in loadEmployeeList method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void calculateVoucherAmount() {
		if (totalVoucherList != null && totalVoucherList.size() != 0) {

			totalVoucherAmount = totalVoucherList.stream()
					.collect(Collectors.summingDouble(VoucherResponseDTO::getVoucherAmount));
			totalVoucherAmountPaid = calculateTotalAmount(totalVoucherList, VoucherStatus.PAID);
			totalVoucherPaidCount = calculateTotalCount(totalVoucherList, VoucherStatus.PAID);
		}
	}

	private Double calculateTotalAmount(List<VoucherResponseDTO> items, VoucherStatus productStatus) {
		Double count = 0.00;
		for (VoucherResponseDTO item : items) {
			if (item.getVoucherStatus().equals(productStatus)) {
				count = count + item.getVoucherAmount();
			}
		}
		return count;
	}

	private Integer calculateTotalCount(List<VoucherResponseDTO> items, VoucherStatus productStatus) {
		Integer count = 0;
		for (VoucherResponseDTO item : items) {
			if (item.getVoucherStatus().equals(productStatus)) {
				count++;
			}
		}
		return count;
	}

	public void loadVoucherList() {

		voucherList = new LazyDataModel<VoucherResponseDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<VoucherResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<VoucherResponseDTO> data = new ArrayList<VoucherResponseDTO>();
				selectedVouchersList = new ArrayList<>();
				paidListFlag = false;
				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<VoucherResponseDTO>>() {
					});
					totalVoucherList = mapper.readValue(jsonResponse, new TypeReference<List<VoucherResponseDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(VoucherResponseDTO res) {
				return res != null ? res.getVoucherId() : null;
			}

			@Override
			public VoucherResponseDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<VoucherResponseDTO> responseList = (List<VoucherResponseDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (VoucherResponseDTO res : responseList) {
					if (res.getVoucherId().longValue() == value.longValue()) {
						selectedVouchersList.add(res);
						return res;
					}
				}
				return null;
			}

		};
	}

	public void loadAllBanks() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getbankdetails";
		BaseDTO baseDTO = null;
		BankDetailsDTO bankDetailsDTO = new BankDetailsDTO();
		accountNumberList = new ArrayList<>();
		paymentModeList = new ArrayList<>();

		try {
			baseDTO = httpService.post(url, bankDetailsDTO);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			bankList = mapper.readValue(jsonResponse, new TypeReference<List<BankBranchDTO>>() {
			});
			log.info("bankList", bankList);
		} catch (Exception exp) {
			log.error("Exception in loadAllBanks method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void loadVoucherAmountDetails() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getvoucheramountdetails";
		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			voucherCountDTO = mapper.readValue(jsonResponse, VoucherCountDTO.class);
			log.info("voucherCountDTO", voucherCountDTO);

			totalVoucherAmount = voucherCountDTO.getTotalVoucherAmount();
			if (totalVoucherAmount == null) {
				totalVoucherAmount = 0.00;
			}
			totalVoucherAmountPaid = voucherCountDTO.getTotalPaidVoucher();
			if (totalVoucherAmountPaid == null) {
				totalVoucherAmountPaid = 0.00;
			}
			totalVoucherPaidCount = voucherCountDTO.getTotalPaidCount();
		} catch (Exception exp) {
			log.error("Exception in loadAllBanks method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void onChangeBank() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getpaymentmethod";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			paymentMethodList = mapper.readValue(jsonResponse, new TypeReference<List<PaymentMethod>>() {
			});
			log.info("paymentMethodList", paymentMethodList);
		} catch (Exception exp) {
			log.error("Exception in loadPaymentMode method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String getPaymentListPage() {
		loadPaymentList();
		return PAYMENT_LIST_PAGE_URL;
	}

	public String getAddPage() {
		if (selectedVouchersList.size() == 0) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE_VOUCHER.getCode());
			return null;
		}

		for (VoucherResponseDTO voucherResponseDTO : selectedVouchersList) {
			if (voucherResponseDTO.getVoucherStatus().equals(VoucherStatus.PAID)) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_APPROVED_VOUCHER.getCode());
				return null;
			}
		}
		bankMaster = null;
		paymentMethod = new PaymentMethod();
		documentNumber = null;
		chequeDate = null;
		loadSelectedVouchersList();
		loadAllBanks();
		return ADD_PAGE_URL;
	}

	public String getViewPage() {

		if (paymentResponseDTO == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_PAYMENT.getCode());
			return null;
		}

		loadPaidVouchersList();
		loadAllBanks();
		return VIEW_PAGE_URL;
	}

	public void loadPaidVouchersList() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getpaidvoucherlist";
		BaseDTO baseDTO = null;

		PaymentRequestDTO paymentRequestDTO = new PaymentRequestDTO();
		paymentRequestDTO.setPaymentId(paymentResponseDTO.getPaymentId());

		try {
			baseDTO = httpService.post(url, paymentRequestDTO);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			paymentStatusDetailsDTO = mapper.readValue(jsonResponse, PaymentStatusDetailsDTO.class);

			vouchersList = paymentStatusDetailsDTO.getVouchersList();

			bankName = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getEntityBankBranch()
					.getBankBranchMaster().getBankMaster().getBankName();

			branchName = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getEntityBankBranch()
					.getBankBranchMaster().getBranchName();

			accountNumber = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getEntityBankBranch()
					.getAccountNumber();

			paymentMethodName = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getPaymentMethod()
					.getName();

			documentNumber = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0)
					.getBankReferenceNumber();

			chequeDate = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getDocumentDate();

			paidByName = paymentStatusDetailsDTO.getPayment().getPaymentDetailsList().get(0).getPaidBy().getUsername();

			log.info("vouchersList", vouchersList);
			loadSocietyDetailsList(vouchersList);
			calculateTotal();
			calculateTotalValueSociety();
		} catch (Exception exp) {
			log.error("Exception in getSelectedVouchersList method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void loadSelectedVouchersList() {

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/getallvouchers";
		BaseDTO baseDTO = null;

		List<Long> voucherIdList = new ArrayList<>();

		List<VoucherResponseDTO> uniqueList = selectedVouchersList.stream().distinct().collect(Collectors.toList());

		for (VoucherResponseDTO uniqueValue : uniqueList) {
			voucherIdList.add(uniqueValue.getVoucherId());
		}

		PaymentRequestDetailsDTO paymentRequestDetailsDTO = new PaymentRequestDetailsDTO();
		paymentRequestDetailsDTO.setVoucherIds(voucherIdList);

		try {
			baseDTO = httpService.post(url, paymentRequestDetailsDTO);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			vouchersList = mapper.readValue(jsonResponse, new TypeReference<List<Voucher>>() {
			});
			log.info("vouchersList", vouchersList);
			loadSocietyDetailsList(vouchersList);
			calculateTotal();
			calculateTotalValueSociety();
		} catch (Exception exp) {
			log.error("Exception in getSelectedVouchersList method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void calculateTotal() {
		totalValueVoucher = vouchersList.stream().collect(Collectors.summingDouble(Voucher::getNetAmount));
	}

	public void loadSocietyDetailsList(List<Voucher> vouchersList) {
		voucherDetailsList = new ArrayList<>();
		for (Voucher voucher : vouchersList) {
			voucherDetailsList.addAll(voucher.getVoucherDetailsList());
		}
	}

	public void calculateTotalValueSociety() {
		totalValueVoucherSociety = voucherDetailsList.stream()
				.collect(Collectors.summingDouble(VoucherDetails::getAmount));
	}

	public void loadPaymentList() {

		paymentList = new LazyDataModel<PaymentResponseDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<PaymentResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<PaymentResponseDTO> data = new ArrayList<PaymentResponseDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchDataForPayment(first / pageSize, pageSize, sortField, sortOrder,
							filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<PaymentResponseDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						paymentListSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(PaymentResponseDTO res) {
				return res != null ? res.getPaymentId() : null;
			}

			@Override
			public PaymentResponseDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<PaymentResponseDTO> responseList = (List<PaymentResponseDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (PaymentResponseDTO res : responseList) {
					if (res.getPaymentId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public String submitPayment() {

		log.info("submitPayment method started");

		log.info("Payment Mode ", paymentMode);

		paymentRequest = new PaymentRequestDTO();

		paymentRequest.setEntityBankBranchId(bankMaster.getBankBranchId());

		paymentRequest.setPaymentMethod(paymentMethod);

		paymentRequest.setEmpMaster(employee);

		paymentRequest.setVoucherEntityList(vouchersList);

		paymentRequest.setDocumentNumber(documentNumber);

		paymentRequest.setDocumentDate(chequeDate);

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/accounts/payment/create";
		BaseDTO baseDTO = null;
		try {

			baseDTO = httpService.post(url, paymentRequest);
			loadVoucherAmountDetails();

		} catch (Exception exp) {
			log.info("Exception in submitPayment method");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Payment Submitted successfully");
				errorMap.notify(ErrorDescription.PAYMENT_SUBMITTED_SUCCESSFULLY.getCode());
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} else {
			log.info("failiure in submitPayment method ");
		}
		return LIST_PAGE_URL;

	}

	public void onPageLoad() {
		paidListFlag = false;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		log.info("selectedVouchersList", selectedVouchersList);
		addButtonFlag = true;

		if (selectedVouchersList.size() > 0) {
			paidListFlag = true;
		} else {
			paidListFlag = false;
		}

	}

	public void onRowUnSelect() {
		log.info("onRowUnSelect method started");
		log.info("selectedVouchersList", selectedVouchersList);

		if (selectedVouchersList.size() == 0) {
			paidListFlag = false;
		}

	}

}
