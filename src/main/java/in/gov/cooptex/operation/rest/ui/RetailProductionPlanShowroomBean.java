package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.RoPlanDetailsDto;
import in.gov.cooptex.operation.dto.ShowroomPlanDetailsDto;
import in.gov.cooptex.operation.enums.RequestType;
import in.gov.cooptex.operation.enums.RetailProductionPlanStage;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.production.dto.ProductWiseProductionPlanDto;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailProductionPlanShowroomBean")
@Scope("session")
public class RetailProductionPlanShowroomBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String SHOWROOM_LIST_URL = "/pages/operation/retailproduction/listProductionPlanShowroom.xhtml?faces-redirect=true;";

	private final String SHOWROOM_OFFICE_EDIT_URL = "/pages/operation/retailproduction/initialProductionPlanShowroom.xhtml?faces-redirect=true;";

	private final String SHOWROOM_OFFICE_VIEW_URL = "/pages/operation/retailproduction/initialProductionPlanShowroomView.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	RetailProductionRegionalOfficeBean retailProductionRegionalOfficeBean;

	@Autowired
	RetailProductionPlanLogBean retailProductionPlanLogBean;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	ShowroomPlanDetailsDto showroomPlanDetailsDto;

	@Getter
	@Setter
	Integer resultSize;

	@Getter
	@Setter
	String serverURL;

	@Getter
	@Setter
	Date stockAsOnDate;

	@Getter
	@Setter
	List statusAndStageValues;

	@Getter
	@Setter
	RetailProductionPlanStage stage;

	@Getter
	@Setter
	boolean isValidate = true;

	@Getter
	@Setter
	private Boolean validateEligibilityKey;

	@Getter
	@Setter
	LazyDataModel<RetailProductionPlanResponse> retailProductionPlanResponseLazyList;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> productWiseProductionPlanDtoList;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

	@Getter
	@Setter
	Map<String, Double> totalDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMap;

	@Getter
	@Setter
	boolean validationFlag = true;

	String month, retailProductionPlanStage;

	Integer year;

	public RetailProductionPlanShowroomBean() {
		loadServerURL();
	}

	@PostConstruct
	public void init() {
		getShowroomPlanList();
	}

	private void loadServerURL() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public String getShowroomPlanList() {
		log.info("RetailProductionPlanShowroomBean.getShowroomPlanList Method Started");
		try {
			loadStatusValues();
			loadServerURL();
			loadLazyData();
		} catch (Exception e) {
			log.error("<<== Exception getShowroomPlanList...", e);
		}
		return SHOWROOM_LIST_URL;
	}

	private void loadLazyData() {
		log.info("RetailProductionPlanShowroomBean.loadLazyData Method Started");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				log.info("load is called.");
				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse,
							new TypeReference<List<RetailProductionPlanResponse>>() {
							});

					if (data != null) {
						if (filters.isEmpty()) {
							this.setRowCount(baseDTO.getTotalRecords());
							resultSize = baseDTO.getTotalRecords();
						} else {
							this.setRowCount(data.size());
							resultSize = data.size();
						}
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse response) {
				log.info("getRowKey is called.");
				return response != null ? response.getPlanId() : null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RetailProductionPlanResponse response : responseList) {
					if (response.getPlanId().longValue() == value.longValue()) {
						retailProductionPlanResponse = response;
						return response;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("RetailProductionPlanShowroomBean.getSearchData Method Started");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		retailProductionPlanResponse = new RetailProductionPlanResponse();
		BaseDTO baseDTO = new BaseDTO();

		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planTo")) {
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("receivedDate")) {
				request.setReceivedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dueDate")) {
				request.setDueDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("stageOrStatus")) {
				log.info("selectedStatus : " + value);
				request.setStageOrStatus(value);
			}
		}

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/showroom/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	public String viewPlan(String level, RoPlanDetailsDto roPlanDetailsDtoResponse) {
		log.info("RetailProductionPlanShowroomBean.viewPlan Method Started");
		if (level.equals(RetailProductionPlanStage.RO.name()) || level.equals(RetailProductionPlanStage.FINAL.name())) {
			if (roPlanDetailsDtoResponse == null || roPlanDetailsDtoResponse.getPlanId() == null) {
				errorMap.notify(ErrorDescription.RETAIL_PRODUCTION_PLAN_SELECT_ONE_PLAN.getCode());
				return null;
			}
			retailProductionPlanResponse = convertRoDtoToResponseDto(roPlanDetailsDtoResponse);

			// this code is to display work flow
			retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		} else if (level.equals(RetailProductionPlanStage.SHOWROOM.name())
				&& (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null)) {
			errorMap.notify(ErrorDescription.RETAIL_PRODUCTION_PLAN_SELECT_ONE_PLAN.getCode());
			return null;
		}

		stage = RetailProductionPlanStage.valueOf(level);

		if (getPlan(RequestType.VIEW)) {
			retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);
			return SHOWROOM_OFFICE_VIEW_URL;
		} else {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String editPlanByShowroom() {
		log.info("RetailProductionPlanShowroomBean.editPlanByShowroom Method Started");

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		Integer response = editPlan(RetailProductionPlanStage.SHOWROOM);
		if (response.equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
			stage = RetailProductionPlanStage.SHOWROOM;
			return SHOWROOM_OFFICE_EDIT_URL;
		} else {
			errorMap.notify(response);
			return null;
		}

	}

	public String editPlanByRO(RoPlanDetailsDto roPlanDetailsDtoResponse) {
		log.info("RetailProductionPlanShowroomBean.editPlanByRO Method Started " + roPlanDetailsDtoResponse);
		Integer response = 0;
		try {
			retailProductionPlanResponse = convertRoDtoToResponseDto(roPlanDetailsDtoResponse);

			// this code is to display work flow
			retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

			response = editPlan(RetailProductionPlanStage.RO);
		} catch (Exception e) {
			log.error("<<=== Exception editPlanByRO..", e);
		}
		if (response != null && response.equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
			stage = RetailProductionPlanStage.RO;
			return SHOWROOM_OFFICE_EDIT_URL;
		} else {
			errorMap.notify(response);
			return null;
		}
	}

	public String editPlanByFinal(RoPlanDetailsDto roPlanDetailsDtoResponse) {
		log.info("RetailProductionPlanShowroomBean.editPlanByFinal Method Started");

		retailProductionPlanResponse = convertRoDtoToResponseDto(roPlanDetailsDtoResponse);

		// this code is to display work flow
		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		Integer response = editPlan(RetailProductionPlanStage.FINAL);
		if (response.equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
			stage = RetailProductionPlanStage.FINAL;
			return SHOWROOM_OFFICE_EDIT_URL;
		} else {
			errorMap.notify(response);
			return null;
		}

	}

	public RetailProductionPlanResponse convertRoDtoToResponseDto(RoPlanDetailsDto roPlanDetailsDtoResponse) {
		log.info("RetailProductionPlanShowroomBean.convertRoDtoToResponseDto Method Started");
		try {
			retailProductionPlanResponse = new RetailProductionPlanResponse();
			retailProductionPlanResponse.setPlanId(roPlanDetailsDtoResponse.getPlanId());
			retailProductionPlanResponse.setShowroomId(roPlanDetailsDtoResponse.getShowroomId());
			retailProductionPlanResponse.setStage(roPlanDetailsDtoResponse.getStage());
			retailProductionPlanResponse.setStatus(roPlanDetailsDtoResponse.getStatus());
		} catch (Exception e) {
			log.error("<<=== Exception RetailProductionPlanResponse..", e);
		}
		return retailProductionPlanResponse;
	}

	public Integer editPlan(RetailProductionPlanStage stage) {
		log.info("RetailProductionPlanShowroomBean.editPlan Method Started");
		try {
			if (retailProductionPlanResponse == null) {
				return ErrorDescription.RETAIL_PRODUCTION_PLAN_SELECT_ONE_PLAN.getCode();
			}

			if (stage.equals(RetailProductionPlanStage.SHOWROOM)
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.PROCUREMENT)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.APPROVED))
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.SHOWROOM)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.INITIATED))) {
				if (retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.PROCUREMENT)
						&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.SUBMITTED)) {
					return ErrorDescription.RETAIL_PRODUCTION_PLAN_CANNOT_BE_MODIFIED.getCode();
				}
				if (retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.PROCUREMENT)) {
					return ErrorDescription.RETAIL_PRODUCTION_PLAN_MONTHLY_PROCUREMENT_NOT_APPROVED.getCode();
				}
				return ErrorDescription.RETAIL_PRODUCTION_PLAN_CANNOT_BE_MODIFIED.getCode();
			}

			else if (stage.equals(RetailProductionPlanStage.RO)
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.SHOWROOM)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.SUBMITTED))
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.RO)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.INITIATED))
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.RO)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.REJECTED))) {
				return ErrorDescription.RETAIL_PRODUCTION_PLAN_CANNOT_BE_MODIFIED.getCode();

			}

			else if (stage.equals(RetailProductionPlanStage.FINAL)
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.RO)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.APPROVED))
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.FINAL)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.INITIATED))
					&& !(retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.FINAL)
							&& retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.REJECTED))) {
				return ErrorDescription.RETAIL_PRODUCTION_PLAN_CANNOT_BE_MODIFIED.getCode();
			}

			if (getPlan(RequestType.UPDATE)) {
				return ErrorDescription.SUCCESS_RESPONSE.getCode();
			}

			retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);
		} catch (Exception e) {
			log.error("<<=== Exception editPlan..", e);
		}
		return ErrorDescription.FAILURE_RESPONSE.getCode();
	}

	public boolean getPlan(RequestType requestType) {
		log.info("RetailProductionPlanShowroomBean.getPlan Method Started");

		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			return false;
		}

		ShowroomPlanDetailsDto request = new ShowroomPlanDetailsDto();
		request.setPlanId(retailProductionPlanResponse.getPlanId());
		request.setShowroomId(retailProductionPlanResponse.getShowroomId());
		request.setRequestType(requestType);

		log.info("retailProductionPlanResponse plan id----------->" + request.getPlanId());
		log.info("retailProductionPlanResponse showroom id----------->" + request.getShowroomId());

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/showroom/getplandetails";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, request);
		} catch (Exception exp) {
			log.error("Error in saveDraftPlan", exp);
			return false;
		}

		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				showroomPlanDetailsDto = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				if (showroomPlanDetailsDto == null) {
					return false;
				}

				log.info("Stage  ==>" + showroomPlanDetailsDto.getStage());

				log.info("Status  ==>" + showroomPlanDetailsDto.getStatus());

				stockAsOnDate = new Date(showroomPlanDetailsDto.getSalesFrom().getTime());

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
						.getProductWiseProductionPlanMap().entrySet()) {
					productWiseProductionPlanDtoList = entry.getValue();
				}
				calculateTotalValue(null);
				log.info("showroomPlanDetailsDto == >>" + showroomPlanDetailsDto);

			} catch (IOException e) {
				log.error(e);
			}
		}
		return true;
	}

	public String stageLevelPlanUpdate(String apiType) {

		log.info("RetailProductionPlanShowroomBean.stageLevelPlanUpdate Method Started" + showroomPlanDetailsDto);

		if (showroomPlanDetailsDto.getStage() == null) {
			showroomPlanDetailsDto.setStage(RetailProductionPlanStage.SHOWROOM);
		}

		if (showroomPlanDetailsDto.getStatus() == null) {
			showroomPlanDetailsDto.setStatus(RetailProductionPlanStatus.INITIATED);
		}
		try {

			validateEligibilityKey = null;

			String retailPlanMarginPercentage = commonDataService
					.getAppKeyValue(AppConfigEnum.OPERATION_PLAN_MARGIN_PERCENTAGE.toString());

			/**
			 * RETAIL_PLAN_VALIDATE_ELIGIBILITY_FOR_RO
			 */
			String validateEligibilityKeyValue = commonDataService
					.getAppKeyValue("RETAIL_PLAN_VALIDATE_ELIGIBILITY_FOR_RO");
			validateEligibilityKeyValue = validateEligibilityKeyValue == null ? "false"
					: validateEligibilityKeyValue.trim();

			// String validateEligibilityKeyValue=

			validateEligibilityKey = "true".equals(validateEligibilityKeyValue);

			if (retailProductionPlanStage != null
					&& RetailProductionPlanStage.FINAL.toString().equalsIgnoreCase(retailProductionPlanStage)
					&& !validateTotalWithEligibilityValueOnsubmit(showroomPlanDetailsDto, retailProductionPlanStage,
							null)) {
				AppUtil.addWarn(errorMap.getMessage(8539) + month + " and year : " + year);
				return null;
			}

			// Validate before save or submit
			for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
					.getProductWiseProductionPlanMap().entrySet()) {
				for (ProductWiseProductionPlanDto dto : entry.getValue()) {
					String type = "quantity";
					log.info("entry.getKey() length==> " + entry.getKey().split("/").length);
					// --to check array index exception
					if (entry.getKey().split("/").length >= 3) {
						if (entry.getKey().split("/")[2].equals("A") || entry.getKey().split("/")[2].equals("AJ")) {
							type = "value";
						}
					}
					log.info("before finalvalue==> " + dto.getFinalValue());
					if (apiType.equals("save") && dto.getFinalValue() == null) {
						dto.setFinalValue(0.0);
					}
					log.info("after finalvalue==> " + dto.getFinalValue());
					if ((showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.PROCUREMENT)
							&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.APPROVED)
							|| showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.SHOWROOM)
									&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.INITIATED))
							&& changeShowroomQtyAndValue(entry.getKey(), dto, entry.getValue().indexOf(dto), type,
									apiType, retailPlanMarginPercentage) == null) {
						return null;
					}
					if ((showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.SHOWROOM)
							&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.SUBMITTED)
							|| showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.RO)
									&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.INITIATED))
							&& changeRoQtyAndValue(entry.getKey(), dto, entry.getValue().indexOf(dto), type,
									retailPlanMarginPercentage) == null) {
						return null;
					}
					if (!apiType.equals("save")) {
						if ((showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.RO)
								&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.APPROVED)
								|| showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.FINAL)
										&& showroomPlanDetailsDto.getStatus()
												.equals(RetailProductionPlanStatus.INITIATED))
								&& changeFinalQtyAndValue(entry.getKey(), dto, entry.getValue().indexOf(dto), type,
										retailPlanMarginPercentage) == null) {
							return null;
						}
					}
				}
			}

			if (validationFlag) {
				if (showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.SHOWROOM)
						&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.SUBMITTED)) {
					showroomPlanDetailsDto.setStage(RetailProductionPlanStage.RO);
				}

				else if (showroomPlanDetailsDto.getStage().equals(RetailProductionPlanStage.RO)
						&& showroomPlanDetailsDto.getStatus().equals(RetailProductionPlanStatus.APPROVED)) {
					showroomPlanDetailsDto.setStage(RetailProductionPlanStage.FINAL);
				}
				showroomPlanDetailsDto.setApiType(apiType);

				if (apiType.equals("save")) {
					showroomPlanDetailsDto.setStatus(RetailProductionPlanStatus.INITIATED);
				} else if (apiType.equals("submit")) {
					showroomPlanDetailsDto.setStatus(RetailProductionPlanStatus.SUBMITTED);
				}
			}

			String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/stagelevel/planupdate";
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.post(url, showroomPlanDetailsDto);
			} catch (Exception exp) {
				log.error("Error in stageLevelPlanUpdate", exp);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				return null;
			}
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Showroom Level Plan updated successfully");
					errorMap.notify(8528);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
			if (stage.equals(RetailProductionPlanStage.SHOWROOM)) {
				return getShowroomPlanList();
			}
			if (stage.equals(RetailProductionPlanStage.RO)) {
				return retailProductionRegionalOfficeBean.getShowRoomList();
			}
			if (stage.equals(RetailProductionPlanStage.FINAL)) {
				return retailProductionRegionalOfficeBean.getShowRoomList();
			}
		} catch (Exception e) {
			log.error("<<=== Exception stageLevelPlanUpdate..", e);
		}
		return SHOWROOM_LIST_URL;
	}

	public String changeShowroomQtyAndValue(String product, ProductWiseProductionPlanDto monthWise, int index,
			String type, String apiType, String retailPlanMarginPercentage) {
		log.info("apiType -------------->" + apiType);
		log.info("inside changeShowroomQtyAndValue and the product :" + product + " and showroomQty :"
				+ monthWise.getShowroomQty());

		validationFlag = true;
		try {

			for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
					.getProductWiseProductionPlanMap().entrySet()) {
				if (entry.getKey().equals(product)) {
					if (type.equals("quantity")) {
						entry.getValue().get(index).setShowroomQty(monthWise.getShowroomQty());
						log.info("showroom qty-------------" + entry.getValue().get(index).getShowroomQty());
						if (monthWise.getAveragePurchagePrice() != null && monthWise.getAveragePurchagePrice() != 0) {
							entry.getValue().get(index)
									.setShowroomValue(monthWise.getShowroomQty() * monthWise.getAveragePurchagePrice());
						} else {
							entry.getValue().get(index).setShowroomValue(monthWise.getShowroomQty());
						}
						log.info("showroom value-----------------" + entry.getValue().get(index).getShowroomValue());
					} else {
						entry.getValue().get(index).setRoValue(monthWise.getRoValue());
					}
				}
			}
			calculateTotalValue(RetailProductionPlanStage.SHOWROOM);
			if (!validateTotalWithEligibilityValue(monthWise, RetailProductionPlanStage.SHOWROOM,
					retailPlanMarginPercentage)) {
				/*
				 * if (apiType.equalsIgnoreCase("submit")) { AppUtil.addWarn(
				 * errorMap.getMessage(8537) + monthWise.getMonth() + " and year : " +
				 * monthWise.getYear()); validationFlag = false; return null; }
				 */
			}
		} catch (Exception e) {
			log.error("<<=== Exception changeShowroomQtyAndValue..", e);
		}
		return "success";

	}

	public String changeRoQtyAndValue(String product, ProductWiseProductionPlanDto monthWise, int index, String type,
			String retailPlanMarginPercentage) {
		log.info("inside changeRoQtyAndValue and the product :" + product + " and roQty :" + monthWise.getRoQty());
		validationFlag = true;
		try {

			for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
					.getProductWiseProductionPlanMap().entrySet()) {
				if (entry.getKey().equals(product)) {
					if (type.equals("quantity")) {
						entry.getValue().get(index).setRoQty(monthWise.getRoQty());
						if (monthWise.getAveragePurchagePrice() != null && monthWise.getAveragePurchagePrice() != 0) {
							entry.getValue().get(index)
									.setRoValue(monthWise.getRoQty() * monthWise.getAveragePurchagePrice());
						} else {
							entry.getValue().get(index).setRoValue(monthWise.getRoQty());
						}
					} else {
						entry.getValue().get(index).setRoValue(monthWise.getRoValue());
					}
				}
			}
			calculateTotalValue(RetailProductionPlanStage.RO);
			if (!validateTotalWithEligibilityValue(monthWise, RetailProductionPlanStage.RO,
					retailPlanMarginPercentage)) {
				AppUtil.addWarn(
						errorMap.getMessage(8538) + monthWise.getMonth() + " and year : " + monthWise.getYear());
				validationFlag = false;
				return null;
			}
		} catch (Exception e) {
			log.error("<<=== Exception changeRoQtyAndValue..", e);
		}
		return "success";
	}

	public String changeFinalQtyAndValue(String product, ProductWiseProductionPlanDto monthWise, int index, String type,
			String retailPlanMarginPercentage) {
		log.info("inside changeFinalQtyAndValue and the product :" + product + " and finalQty :"
				+ monthWise.getFinalQty());
		validationFlag = true;
		retailProductionPlanStage = RetailProductionPlanStage.FINAL.toString();
		try {
			if (monthWise.getFinalQty() == null) {
				AppUtil.addError("Final quantity should not be empty");
				return null;
			}
			if (monthWise.getFinalValue() == null) {
				AppUtil.addError("Final value should not be empty");
				return null;
			}
			log.info("changeFinalQtyAndValue :: FinalValue==> " + monthWise.getFinalValue());
			log.info("changeFinalQtyAndValue :: FinalQty==> " + monthWise.getFinalQty());

			for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
					.getProductWiseProductionPlanMap().entrySet()) {
				if (entry.getKey().equals(product)) {
					if (type.equals("quantity")) {
						entry.getValue().get(index).setFinalQty(monthWise.getFinalQty());
						if (monthWise.getAveragePurchagePrice() != null && monthWise.getAveragePurchagePrice() != 0) {
							entry.getValue().get(index)
									.setFinalValue(monthWise.getFinalQty() * monthWise.getAveragePurchagePrice());
						} else {
							entry.getValue().get(index).setFinalValue(monthWise.getFinalQty());
						}

					} else {
						if (monthWise.getFinalValue() == monthWise.getRoValue()) {
							entry.getValue().get(index).setFinalValue(monthWise.getRoValue());
						} else {
							entry.getValue().get(index).setFinalValue(monthWise.getFinalValue());
						}

					}
				}
			}
			calculateTotalValue(RetailProductionPlanStage.FINAL);
			if (!validateTotalWithEligibilityValue(monthWise, RetailProductionPlanStage.FINAL,
					retailPlanMarginPercentage)) {
				/*
				 * AppUtil.addWarn( errorMap.getMessage(8539) + monthWise.getMonth() +
				 * " and year : " + monthWise.getYear());
				 */
				validationFlag = false;

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
						.getProductWiseProductionPlanMap().entrySet()) {
					if (entry.getKey().equals(product)) {

						entry.getValue().get(index).setFinalValue(monthWise.getRoValue());

					}
				}

				return null;
			}
		} catch (Exception e) {
			log.error("<<=== Exception changeFinalQtyAndValue..", e);
		}
		return "success";

	}

	/**
	 * @param monthWise
	 * @param valueGivenFor
	 * @param retailPlanMarginPercentage
	 * @return
	 */
	public boolean validateTotalWithEligibilityValue(ProductWiseProductionPlanDto monthWise,
			RetailProductionPlanStage valueGivenFor, String retailPlanMarginPercentage) {

		log.info(
				"RetailProductionPlanShowroomBean.validateTotalWithEligibilityValue Method Started : " + valueGivenFor);
		isValidate = true;
		try {

			if (validateEligibilityKey == null) {
				String validateEligibilityKeyValue = commonDataService
						.getAppKeyValue("RETAIL_PLAN_VALIDATE_ELIGIBILITY_FOR_RO");
				validateEligibilityKeyValue = validateEligibilityKeyValue == null ? "false"
						: validateEligibilityKeyValue.trim();

				// String validateEligibilityKeyValue=

				validateEligibilityKey = "true".equals(validateEligibilityKeyValue);
			}

			Double eligibilityValue = showroomPlanDetailsDto.getTotalPurchaseValue();

			if (monthWise.getMonthWisePercentage() != null) {
				eligibilityValue = showroomPlanDetailsDto.getTotalPurchaseValue() * monthWise.getMonthWisePercentage();
			}

			/*
			 * String retailPlanMarginPercentage = commonDataService
			 * .getAppKeyValue(AppConfigEnum.OPERATION_PLAN_MARGIN_PERCENTAGE.toString());
			 */
			if (retailPlanMarginPercentage == null || retailPlanMarginPercentage.isEmpty()) {
				log.error("retailPlanMarginPercentage is empty. Taking default value as 2");
				retailPlanMarginPercentage = "2";
			}
			Double percentage = AppUtil.stringToDouble(retailPlanMarginPercentage, 2D);
			percentage = percentage == null ? 0D : percentage / 100;
			log.info("retailPlanMarginPercentage >>>>> " + percentage);

			// percentage will come from application.properties file
			// Double percentage =
			// (appPreference.getRetailPlanMarginPercentage().doubleValue()) / 100;

			Double eligibilityValuePlusOnePercent = eligibilityValue + eligibilityValue * percentage;
			Double eligibilityValueMinusOnePercent = eligibilityValue - eligibilityValue * percentage;
			log.info("validateTotalWithEligibilityValue eligibilityValuePlusOnePercent==> "
					+ eligibilityValuePlusOnePercent);
			log.info("validateTotalWithEligibilityValue eligibilityValueMinusOnePercent==> "
					+ eligibilityValueMinusOnePercent);
			Double totalValues = 0.0;
			if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.SHOWROOM)) {
				for (Entry<String, Double> entry : totalShowroomValueMap.entrySet()) {
					if (entry.getKey().equals(monthWise.getMonth())) {
						totalValues = entry.getValue();
						log.info("total value showroom-------------" + totalValues);
					}
				}
			} else if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.RO)) {
				for (Entry<String, Double> entry : totalRoValueMap.entrySet()) {
					if (entry.getKey().equals(monthWise.getMonth())) {
						totalValues = entry.getValue();
					}
				}
			} else if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.FINAL)) {
				for (Entry<String, Double> entry : totalFinalValueMap.entrySet()) {
					if (entry.getKey().equals(monthWise.getMonth())) {
						totalValues = entry.getValue();
					}
				}
			}
			log.info("total value-------------" + totalValues);
			log.info("eligilipility value plus one percentage---------" + eligibilityValuePlusOnePercent);
			log.info("eligilipility value minus one percentage---------" + eligibilityValueMinusOnePercent);

			if (validateEligibilityKey) {
				if (totalValues > eligibilityValuePlusOnePercent || totalValues < eligibilityValueMinusOnePercent) {
					isValidate = false;
					return false;
				}
			} else {
				isValidate = true;
				return true;
			}
		} catch (Exception e) {
			log.info("<<== Exception while validateTotalWithEligibilityValue...", e);
		}
		return true;
	}

	public void calculateTotalValue(RetailProductionPlanStage valueGivenFor) {

		log.info("RetailProductionPlanShowroomBean.calculateTotalValue Method Started : " + valueGivenFor);

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();
		try {
			for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
					.getProductWiseProductionPlanMap().entrySet()) {
				for (ProductWiseProductionPlanDto dto : entry.getValue()) {
					totalListFromMap.add(dto);
					showroomPlanDetailsDto.setUom(dto.getUom());

				}
			}

			if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.DRAFT)) {
				totalDraftValueMap = totalListFromMap.stream()
						.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
								Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
			}
			if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.SHOWROOM)) {
				totalShowroomValueMap = totalListFromMap.stream().filter(i -> i.getShowroomValue() != null)
						.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
								(Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue))));
			}
			if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.RO)) {
				totalRoValueMap = totalListFromMap.stream()
						.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
								Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
			}
			if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.FINAL)) {
				totalFinalValueMap = totalListFromMap.stream().filter(o -> o.getFinalValue() != null)
						.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
								Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));
			}
		}
		/*
		 * catch(NullPointerException e) {
		 * log.error("<<=== NullPointerException in calculateion...",e);
		 * AppUtil.addWarn(errorMap.getMessage(8537) ); validationFlag = false; return ;
		 * }
		 */
		catch (Exception e) {
			log.error("<<=== Exception in calculateion...", e);
			return;
		}

	}

	private void loadStatusValues() {
		log.info("RetailProductionPlanShowroomBean.loadStatusValues Method Started ");
		try {
			Object[] statusArray = RetailProductionPlanStatus.class.getEnumConstants();
			Object[] stageArray = RetailProductionPlanStage.class.getEnumConstants();

			statusAndStageValues = new ArrayList<>();

			for (Object stage : stageArray) {
				for (Object status : statusArray) {
					if (!stage.toString().equalsIgnoreCase("DRAFT")) {
						if (!(stage + "-" + status).equalsIgnoreCase("SHOWROOM-APPROVED")
								&& !(stage + "-" + status).equalsIgnoreCase("SHOWROOM-REJECTED")) {
							statusAndStageValues.add(stage + "-" + status);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("<<== Exception loadStatusValues ", e);
		}
		log.info("statusAndStageValues " + statusAndStageValues);
	}

	public String cancel() {
		log.info("RetailProductionPlanShowroomBean.cancel Method Started ");
		if (stage.equals(RetailProductionPlanStage.SHOWROOM)) {
			return getShowroomPlanList();
		} else if (stage.equals(RetailProductionPlanStage.RO) || stage.equals(RetailProductionPlanStage.FINAL)) {
			return retailProductionRegionalOfficeBean.getShowRoomList();
		}
		return null;

	}

	public boolean validateTotalWithEligibilityValueOnsubmit(ShowroomPlanDetailsDto showroomPlanDetailsDto,
			String valueGivenFor, String retailPlanMarginPercentage) {

		log.info("RetailProductionPlanShowroomBean.validateTotalWithEligibilityValueOnsubmit Method Started : "
				+ valueGivenFor);
		boolean isValidate = true;
		try {

			for (ProductWiseProductionPlanDto monthWise : productWiseProductionPlanDtoList) {

				Double eligibilityValue = showroomPlanDetailsDto.getTotalPurchaseValue();

				if (monthWise.getMonthWisePercentage() != null) {
					eligibilityValue = showroomPlanDetailsDto.getTotalPurchaseValue()
							* monthWise.getMonthWisePercentage();
				}

				if (retailPlanMarginPercentage == null || retailPlanMarginPercentage.isEmpty()) {
					log.error("retailPlanMarginPercentage is empty. Taking default value as 2");
					retailPlanMarginPercentage = "2";
				}
				Double percentage = AppUtil.stringToDouble(retailPlanMarginPercentage, 2D);
				percentage = percentage == null ? 0D : percentage / 100;
				log.info("retailPlanMarginPercentage >>>>> " + percentage);

				Double eligibilityValuePlusOnePercent = eligibilityValue + eligibilityValue * percentage;
				Double eligibilityValueMinusOnePercent = eligibilityValue - eligibilityValue * percentage;
				log.info("validateTotalWithEligibilityValue eligibilityValuePlusOnePercent==> "
						+ eligibilityValuePlusOnePercent);
				log.info("validateTotalWithEligibilityValue eligibilityValueMinusOnePercent==> "
						+ eligibilityValueMinusOnePercent);
				Double totalValues = 0.0;

				log.info("total value-------------" + totalValues);
				log.info("eligilipility value plus one percentage---------" + eligibilityValuePlusOnePercent);
				log.info("eligilipility value minus one percentage---------" + eligibilityValueMinusOnePercent);
				if (valueGivenFor == null || valueGivenFor.equals(RetailProductionPlanStage.FINAL.toString())) {
					for (Entry<String, Double> entry : totalFinalValueMap.entrySet()) {
						if (entry.getKey().equals(monthWise.getMonth())) {
							totalValues = entry.getValue();
						}
					}
				}
				
				if (validateEligibilityKey) {
					if (totalValues > eligibilityValuePlusOnePercent || totalValues < eligibilityValueMinusOnePercent) {
						isValidate = false;
						month = monthWise.getMonth();
						year = monthWise.getYear();
						break;
					}
				} else {
					isValidate = true;
				}
			}
		} catch (Exception e) {
			log.info("<<== Exception while validateTotalWithEligibilityValueOnsubmit...", e);
		}
		return isValidate;
	}

	public String formatTwoDecimalValue(String value) {
		Double valueD = StringUtils.isEmpty(value) ? 0D : Double.parseDouble(value);
		return AppUtil.formatIndianCommaSeparated(valueD);
	}

}