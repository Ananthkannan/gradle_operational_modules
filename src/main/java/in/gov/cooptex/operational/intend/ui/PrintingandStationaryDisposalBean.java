package in.gov.cooptex.operational.intend.ui;


import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.ItemConsumptionDTO;
import in.gov.cooptex.operation.intend.model.IntendConsolidation;
import in.gov.cooptex.operation.intend.model.IntendRequest;
import in.gov.cooptex.operation.model.ItemConsumption;
import in.gov.cooptex.operation.model.ItemConsumptionDetails;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("printingandStationaryDisposalBean")
@Scope("session")
public class PrintingandStationaryDisposalBean implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter	@Setter
	private EntityMaster selectedEntityMaster;
	
	@Getter	@Setter
	private List<EntityMaster> entityMasterList;
	
	@Getter	@Setter
	private SectionMaster selectedSectionMaster;
	
	@Getter	@Setter
	private List<SectionMaster> sectionMasterList;
	
	@Getter	@Setter
	private IntendConsolidation selectedIntendConsolidation;
	
	@Getter	@Setter
	private Long previousSelectedIntendConsolidationID;
	
	@Getter	@Setter
	private Long selectedIntendConsolidationID;
	
	@Getter	@Setter
	private List<IntendConsolidation> intendConsolidationList;
	
	@Getter	@Setter
	private IntendRequest selectedIntendRequest;
	
	@Getter	@Setter
	private List<IntendRequest> intendRequestList;
	
	
	@Getter	@Setter
	private List<ItemConsumptionDTO> itemConsumptionCollectorList;
	//payment end
	
	@Getter	@Setter
	private ItemConsumption itemConsumption;
	
	@Getter	@Setter
	private ItemConsumptionDetails itemConsumptionDetails;
	
	@Getter	@Setter
	private List<ItemConsumption> itemConsumptionList;
	
	@Getter	@Setter
	private ItemConsumption selectedItemConsumption;
	
	@Getter	@Setter
	private ItemConsumptionDetails selectedItemConsumptionDetails;

	@Getter	@Setter
	private List<ItemConsumptionDetails> itemConsumptionDetailsList;
	
	@Getter	@Setter
	ItemConsumptionDTO itemConsumptionDTO;
	
	@Getter	@Setter
	List<Department> departmentList = new ArrayList<>();
	
	@Getter	@Setter
	Department department = new Department();
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	private final String LIST_URL = "/pages/printingAndStationary/listPrintingandStationaryDisposal.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/printingAndStationary/viewPrintingandStationaryDisposal.xhtml?faces-redirect=true;";
	private final String ADD_URL = "/pages/printingAndStationary/createPrintingandStationaryDisposal.xhtml?faces-redirect=true;";
	private final String Edit_URL = "/pages/printingAndStationary/editPrintingandStationaryDisposal.xhtml?faces-redirect=true;";
	
	private final String INPUT_FORM_LIST_URL = "/pages/printingAndStationary/listPrintingandStationaryDisposal.xhtml?faces-redirect=true;";
	
	ObjectMapper mapper;
	
	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter	@Setter 
	private boolean renderedEdit;
	
	@Getter	@Setter 
	private boolean renderedCreate;
	
	
	@Getter	@Setter 
	boolean rowEditRendered;
	
	@Getter	@Setter 
	boolean currentDisposalQty;
	
	@Getter	@Setter
	LazyDataModel<ItemConsumptionDTO> itemConsumptionLazyList;
	
	@Getter	@Setter
	ItemConsumptionDTO itemConsumptionDTOLazy;
	
	@Getter	@Setter
	ItemConsumptionDTO selectedItemConsumptionDTO;
	
	@Getter	@Setter
	int size;
	
	@Getter	@Setter
	int rowIDNumber;
	
	@Getter	@Setter
	int rowConterNumber;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<EntityMaster> hoentityList = new ArrayList<>();
	
	@Getter
	@Setter
	EntityMaster headOrRegionOffice = new EntityMaster();
	
	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<>();
	
	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();
	
	@Getter
	@Setter
	Boolean isHeadOffice = false;
	
	@PostConstruct
	public void init() {
		selectedEntityMaster = new EntityMaster();
		selectedSectionMaster = new SectionMaster();
		entityMasterList=new ArrayList<EntityMaster>();
		sectionMasterList=new ArrayList<SectionMaster>();
		selectedIntendConsolidation = new IntendConsolidation();
		intendConsolidationList=new ArrayList<IntendConsolidation>();

		itemConsumptionCollectorList = new ArrayList<ItemConsumptionDTO>();

		rowEditRendered = true;
		currentDisposalQty  = false; 
		renderedEdit = false;
		renderedCreate = true;
		rowIDNumber = 0;
		previousSelectedIntendConsolidationID = 0l;
		rowConterNumber = 1;
		itemConsumptionDTO = new ItemConsumptionDTO();
		//loadEntityCodeName();
		loadLazyIntItemConsumptionList();
		loadHOorRO();
		loadAllEntityTypeList();
	}
	
	public void loadHOorRO() {
		log.info("... Load  loadHOorRO ....");
		try {
			Long userId = loginBean.getUserDetailSession().getId();
			//String url = SERVER_URL + "/entitymaster/loadhoorro/"+ userId;
			String url = SERVER_URL + "/entitymaster/getall/headandregionalOffice";
			log.info("loadHOorRO :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			hoentityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if(hoentityList != null && !hoentityList.isEmpty()) {
				log.info("hoentityList size==> "+hoentityList.size());
			}
			
		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		
	}
	
	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitytypemaster/getall/entitytype");
			String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
			if(entityTypeList != null && !entityTypeList.isEmpty()) {
				log.info("entityTypeList size==> "+entityTypeList.size());
			}else {
				log.error("entityTypeList is null or empty");
			}
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}
	
	public void loadAllEntityByRegionList() {
		log.info("... Load  Entity List by region called ....");
		try {
			
			if (headOrRegionOffice != null
					&& headOrRegionOffice.getEntityTypeMaster() != null
					&& headOrRegionOffice.getEntityTypeMaster().getEntityCode() != null
					&& headOrRegionOffice.getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
				entityTypeMaster = new EntityTypeMaster();
				entityMasterList = new ArrayList<>();
				selectedEntityMaster = new EntityMaster();
				isHeadOffice = true;
				//--load department on ho id
				loadDepartment();
				return;
			}else {
				isHeadOffice = false;
			}
			department=null;
			log.info("loadAllEntityByRegionList :: Head or region office id==> "
					+ headOrRegionOffice.getId());
			log.info("loadAllEntityByRegionList :: Entity type id==> " + entityTypeMaster.getId());

			String url = null;
			if(entityTypeMaster != null && entityTypeMaster.getId() != null) {
				url = SERVER_URL + "/entitymaster/getall/getmanualAttenRegion/"
					+ headOrRegionOffice.getId() + "/"
					+ entityTypeMaster.getId();
			}
			log.info("loadAllEntityByRegionList :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if(entityMasterList != null && !entityMasterList.isEmpty()) {
				log.info("entityMasterList size==> "+entityMasterList.size());
			}else {
				log.error("entityMasterList null or empty");
				entityMasterList = new ArrayList<>();
			}
			

		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			
		}
	}
	
	//Load Entity Master List 
	public void loadEntityCodeName() {
		log.info(":::<- loadEntityCodeName Called->:::");
		BaseDTO baseDTO = null;
		
		try {
			
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getEntityCodeName";
			baseDTO = httpService.get(url);
			log.info("::: loadEntityCodeName Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("loadEntityCodeName  Successfully "+baseDTO.getTotalRecords());
				
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
			else{
				log.warn("loadEntityCodeName  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityCodeName", ee);
		}
	}
	
	public void loadDepartment() {
		log.info(":::<- loadDepartment :: start ->:::");
		BaseDTO baseDTO = null;
		try {
			String url = null;
			if(isHeadOffice == true) {
				log.info(":::<- headOrRegionOffice id ->::: "+headOrRegionOffice.getId());
				url = SERVER_URL+"/department/loaddepartmentbyentity/"+headOrRegionOffice.getId();
			}else {
				log.info(":::<- selectedEntityMaster byID ->::: "+selectedEntityMaster.getId());
				url = SERVER_URL+"/department/loaddepartmentbyentity/"+selectedEntityMaster.getId();
			}
			log.info("url==> "+url);
			baseDTO = httpService.get(url);
			log.info("::: loadSectionCodeName Response :");
			if (baseDTO.getStatusCode()==0) {
							
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				departmentList = mapper.readValue(jsonResponse, new TypeReference<List<Department>>() {
				});
				if(departmentList != null && !departmentList.isEmpty()) {
					log.info("department size==> "+departmentList.size());
				}else {
					log.error("department null or empty ");
				}
			}
			else{
				log.warn("loadDepartment  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadSectionCodeName", ee);
		}
	}
	
	public void loadSectionByDepartment() {
		log.info(":::<- loadDepartment :: start ->:::");
		BaseDTO baseDTO = null;
		try {
			String url = null;
			
			log.info(":::<- selectedEntityMaster byID ->::: "+department.getId());
			url = SERVER_URL+"/sectionmaster/getsectionbydepartmentid/"+department.getId();
			
			log.info("loadSectionByDepartment :: url==> "+url);
			baseDTO = httpService.get(url);
			log.info("::: loadSectionCodeName Response :");
			if (baseDTO.getStatusCode()==0) {
							
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
				if(sectionMasterList != null && !sectionMasterList.isEmpty()) {
					log.info("sectionMasterList size==> "+sectionMasterList.size());
				}else {
					log.error("sectionMasterList null or empty ");
				}
			}
			else{
				log.warn("loadSectionByDepartment  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadSectionCodeName", ee);
		}
	
	}
	
	
	//Load Section Master List 
	/*public void loadSectionCodeName() {
		log.info(":::<- loadSectionCodeName received->:::");
		BaseDTO baseDTO = null;
		try {
			
			selectedSectionMaster = new SectionMaster();
			sectionMasterList=new ArrayList<SectionMaster>();
			String url = null;
			if(isHeadOffice == true) {
				log.info(":::<- headOrRegionOffice id ->::: "+headOrRegionOffice.getId());
				url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getSectionMasterCodeName/"+headOrRegionOffice.getId();
			}else {
				log.info(":::<- selectedEntityMaster byID ->::: "+selectedEntityMaster.getId());
				url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getSectionMasterCodeName/"+selectedEntityMaster.getId();
			}
			log.info("url==> "+url);
			baseDTO = httpService.get(url);
			log.info("::: loadSectionCodeName Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("loadSectionCodeName  Successfully "+baseDTO.getTotalRecords());
				
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
			}
			else{
				log.warn("loadSectionCodeName  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadSectionCodeName", ee);
		}
	}*/
	

	//Load Intend_Requst List 
	public void loadIntendRequest() {
		log.info(":::<- loadIntendConsolidation Called->:::");
		BaseDTO baseDTO = null;
		try {
			selectedIntendRequest = new IntendRequest();
			itemConsumptionCollectorList = new ArrayList<ItemConsumptionDTO>();
			rowIDNumber=0;
			rowConterNumber = 0;
			intendRequestList=new ArrayList<IntendRequest>();
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getIntendRequestCodeName/"+selectedSectionMaster.getId();
			baseDTO = httpService.get(url);
			log.info("::: loadIntendConsolidation Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("loadIntendConsolidation  Successfully "+baseDTO.getTotalRecords());
				
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				intendRequestList = mapper.readValue(jsonResponse, new TypeReference<List<IntendRequest>>() {
				});
			}
			else{
				log.warn("loadIntendConsolidation  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadIntendConsolidation", ee);
		}
	}
	
	//Load Intend Consolidation  
	public void loadIntendConsolidation() {
		log.info(":::<- loadIntendConsolidation Called->:::");
		BaseDTO baseDTO = null;
		try {
			selectedIntendConsolidation = new IntendConsolidation();
			intendConsolidationList = new ArrayList<IntendConsolidation>();
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getIntendConsolidation/"+selectedIntendRequest.getId();
			baseDTO = httpService.get(url);
			log.info("::: loadIntendConsolidation Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("loadIntendConsolidation  Successfully "+baseDTO.getTotalRecords());
				
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				intendConsolidationList = mapper.readValue(jsonResponse, new TypeReference<List<IntendConsolidation>>() {
				});
				
			}
			else{
				log.warn("loadIntendConsolidation  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadIntendConsolidation", ee);
		}
		
	}
	
	//Get count of Received Quantity from item_Distribution and item_Distribution_details table  
	public void loadCountOfReceivedQuantity() {
		log.info(":::<- loadCountOfReceivedQuantity Called->:::");
		BaseDTO baseDTO = new BaseDTO();
		try {
			itemConsumptionDTO.setReceivedQuantity(0);
			itemConsumptionDTO.setReceivedQuantityTemp(0);
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getSumOfReceivedQuantity/"+selectedIntendConsolidation.getIntendRequest().getId();
			baseDTO = httpService.get(url);
			log.info("::: loadCountOfReceivedQuantity Response :");
			if (baseDTO != null && baseDTO.getStatusCode()==0) {
				log.info("loadCountOfReceivedQuantity  Successfully ");

				itemConsumptionDTO.setReceivedQuantity(baseDTO.getTotalRecords());
				itemConsumptionDTO.setReceivedQuantityTemp(baseDTO.getTotalRecords());
				log.info("loadCountOfReceivedQuantity  Size is "+baseDTO.getTotalRecords());
			}
			else{
				log.warn("loadCountOfReceivedQuantity  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadCountOfReceivedQuantity", ee);
		}

		
	}
	
	//Get Already Disposal Quantity from itemConsumptionDetails  
	public void loadAlreadyDisposalQuantity() {
		log.info(":::<- loadAlreadyDisposalQuantity Called->:::");
		BaseDTO baseDTO = new BaseDTO();
		try {
			itemConsumptionDetails = new ItemConsumptionDetails();
			itemConsumption = new ItemConsumption();
			itemConsumption.setEntityMaster(selectedEntityMaster);
			itemConsumption.setSectionMaster(selectedSectionMaster);

			
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/getAlreadyDisposalQuantity";
			baseDTO = httpService.post(url,itemConsumption);
			log.info("::: loadAlreadyDisposalQuantity Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("loadAlreadyDisposalQuantity Successfully "+baseDTO.getTotalRecords());
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemConsumptionDetails = mapper.readValue(jsonResponse, new TypeReference<ItemConsumptionDetails>() {
				});

				if(itemConsumptionDetails.getDisposalQuantity() != null) {
					log.info(" Set ItemConsumptionDetails Done");
					itemConsumptionDTO.setAlreadyDisposalQuantity(itemConsumptionDetails.getDisposalQuantity());
				}else {
					log.info(" Set ItemConsumptionDetails skipe");
				}
			}
			else{
				log.warn("loadAlreadyDisposalQuantity  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadAlreadyDisposalQuantity", ee);
		}
		
	}
	
	 
	
	public String onEdit() {
		log.info("PrintingStationary OnEdit Received");
		rowEditRendered = false;
		return null;
	}
	
	public void onRowEdit(RowEditEvent event) {
		log.info("PrintingStationary OnRowEdit Received");
		rowEditRendered = true;
    }
     
    public void onRowCancel(RowEditEvent event) {
    	log.info("PrintingStationary OnRowCancel Received");
    	rowEditRendered = true;
    }
    
    public void itemConsumptionCollectorAdd() {
    	log.info("receive itemConsumptionCollector Add New");
    	try {
    		
    		currentDisposalQty = true;
	 		
			itemConsumptionDTO.setSelectedIntendConsolidationID(selectedIntendConsolidation.getId());
			
			itemConsumptionDTO.setItemTypeCodeName(selectedIntendConsolidation.getIntendRequest().getRequirementCode() + " / "+
					selectedIntendConsolidation.getIntendRequest().getRequirementName());
			
			addCollectorData();
				
			log.info("itemConsumptionCollector Add New Done");
		} catch (Exception e) {
			log.info("Error itemConsumptionCollectorAdd "+e);
		}
    }
    
    public void addCollectorData() {
    	log.info("ItemConsumptionCollector Add For New Table level 4");
    	try {
    		//itemConsumptionDTO.setReceivedQuantityTemp(10);
    	if(currentDisposalQty == false || itemConsumptionDTO.getCurrentDisposalQuantity()== null || 
    			itemConsumptionDTO.getCurrentDisposalQuantity()<=0) {
			log.warn("currentDisposalQty "+currentDisposalQty+",qty :"+itemConsumptionDTO.getCurrentDisposalQuantity());
			errorMap.notify(ErrorDescription.PRINTING_STATIONARY_CURRENT_DISPOSAL_QTY_NOT_VALID.getErrorCode());
		}else {
			log.info("Received Quantity value==> "+itemConsumptionDTO.getReceivedQuantityTemp());
			log.info("Current disposal Quantity==> "+itemConsumptionDTO.getCurrentDisposalQuantity());
	    	if(itemConsumptionDTO.getReceivedQuantityTemp() >= itemConsumptionDTO.getCurrentDisposalQuantity()) {
	    		
				if(itemConsumptionDTO.getAlreadyDisposalQuantity() != null) {
				
					currentDisposalQty = true;
					if(itemConsumptionDTO.getBalanceQuantityAvailable() == null) {
						itemConsumptionDTO.setBalanceQuantityAvailable( 
							itemConsumptionDTO.getReceivedQuantityTemp() - (itemConsumptionDTO.getAlreadyDisposalQuantity() 
								+ itemConsumptionDTO.getCurrentDisposalQuantity()) );
					}
					else {
						itemConsumptionDTO.setBalanceQuantityAvailable(itemConsumptionDTO.getReceivedQuantityTemp() - itemConsumptionDTO.getCurrentDisposalQuantity());
					}

					itemConsumptionDTO.setReceivedQuantityTemp(itemConsumptionDTO.getReceivedQuantityTemp() - itemConsumptionDTO.getCurrentDisposalQuantity());
					

					rowConterNumber++;
					
					
		    		itemConsumptionDTO.setRowCounter(rowConterNumber);
					itemConsumptionCollectorList.add(itemConsumptionDTO);
					//errorMap.notify(ErrorDescription.PRINTING_STATIONARY_LIST_ADD_SUCCESS.getErrorCode());
					log.info("itemConsumptionCollector Add New Successfully");
				}else {
					
					currentDisposalQty = true;
					itemConsumptionDTO.setBalanceQuantityAvailable( itemConsumptionDTO.getReceivedQuantityTemp() - itemConsumptionDTO.getCurrentDisposalQuantity() );

					itemConsumptionDTO.setReceivedQuantityTemp(itemConsumptionDTO.getReceivedQuantityTemp() - itemConsumptionDTO.getCurrentDisposalQuantity());

					rowConterNumber++;
					
		    		itemConsumptionDTO.setRowCounter(rowConterNumber);
					itemConsumptionCollectorList.add(itemConsumptionDTO);
					//errorMap.notify(ErrorDescription.PRINTING_STATIONARY_LIST_ADD_SUCCESS.getErrorCode());
					log.info("itemConsumptionCollector Add New Successfully");
				}
				

				for(ItemConsumptionDTO itemDTO: itemConsumptionCollectorList) {
					if(rowIDNumber == itemDTO.getRowID()) 
						itemDTO.setRowCounter(rowConterNumber);
				}
				itemConsumptionDTO.setRowCounter(rowIDNumber);
				double tempQunatipty = itemConsumptionDTO.getBalanceQuantityAvailable();
				int receivedQuanityTemp = itemConsumptionDTO.getReceivedQuantity();
				itemConsumptionDTO = new ItemConsumptionDTO();
				itemConsumptionDTO.setRowID(rowIDNumber);
				itemConsumptionDTO.setReceivedQuantity(receivedQuanityTemp);
				itemConsumptionDTO.setReceivedQuantityTemp(tempQunatipty);
				itemConsumptionDTO.setBalanceQuantityAvailable(tempQunatipty);

				itemConsumptionDTO.setAlreadyDisposalQuantity(itemConsumptionDetails.getDisposalQuantity());
				
				itemConsumptionDTO.setUomID(selectedIntendConsolidation.getProductVarietyMaster().getUomMaster().getId());
				itemConsumptionDTO.setRequestQuantity(selectedIntendConsolidation.getRequiredQuantity());
			}else {
				itemConsumptionDTO.setCurrentDisposalQuantity(null);
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_CURRENT_DISPOSAL_QTY_NOT_VALID.getErrorCode());
			}
		}
    	}catch(Exception see) {
    		log.error("Exception see==> ",see);
    		errorMap.notify(ErrorDescription.PRINTING_STATIONARY_LIST_ADD_FAILURE.getErrorCode());
    	}
    }
	public void paymentDetailsCollectorAdd() {
		log.info("receive PaymentDetailsCollector Add New");
		try { 

			double tempQunatipty = itemConsumptionDTO.getBalanceQuantityAvailable();

			int receivedQuanityTemp = itemConsumptionDTO.getReceivedQuantity();
			
			itemConsumptionDTO.setReceivedQuantity(receivedQuanityTemp);
			itemConsumptionDTO.setReceivedQuantityTemp(tempQunatipty);
			itemConsumptionDTO.setBalanceQuantityAvailable(tempQunatipty);
			itemConsumptionDTO.setAlreadyDisposalQuantity(itemConsumptionDetails.getDisposalQuantity());
			
			itemConsumptionDTO.setUomID(selectedIntendConsolidation.getProductVarietyMaster().getUomMaster().getId());
			itemConsumptionDTO.setRequestQuantity(selectedIntendConsolidation.getRequiredQuantity());

		} catch (Exception e) {
			log.error("receive PaymentDetailsCollector Add Error "+e);
		}
		
	}
	public void paymentDetailsUpdate() {
		try {
			log.info("receive PaymentDetails Update");
			itemConsumptionDTO = new ItemConsumptionDTO();
			boolean statusOfRowCounter = true;
			
			previousSelectedIntendConsolidationID = selectedIntendConsolidation.getId();
			loadCountOfReceivedQuantity();  //hide for second balance quantity 
			loadAlreadyDisposalQuantity();
			
			itemConsumptionDTO.setUomID(selectedIntendConsolidation.getProductVarietyMaster().getUomMaster().getId());
			itemConsumptionDTO.setRequestQuantity(selectedIntendConsolidation.getRequiredQuantity());
			

			for(ItemConsumptionDTO itemDTO: itemConsumptionCollectorList) {
				log.info("PreID "+previousSelectedIntendConsolidationID+",Cure :"+selectedIntendConsolidation.getId()+",ListID :"+itemDTO.getSelectedIntendConsolidationID());
 
				if(itemDTO.getSelectedIntendConsolidationID() == selectedIntendConsolidation.getId())
					{
					if(itemDTO.getRowID() == itemDTO.getRowCounter()) {
						statusOfRowCounter = false;
						rowIDNumber = itemDTO.getRowID();
						rowConterNumber = itemDTO.getRowCounter();
						itemConsumptionDTO.setBalanceQuantityAvailable(itemDTO.getBalanceQuantityAvailable());
						itemConsumptionDTO.setReceivedQuantityTemp(itemDTO.getBalanceQuantityAvailable());
						itemConsumptionDTO.setAlreadyDisposalQuantity(itemDTO.getAlreadyDisposalQuantity());
						log.info("Previous ID SElected Success et old Data Success BalanceQTY :"+itemDTO.getBalanceQuantityAvailable());
						break;
					}
					}
			}
			if(statusOfRowCounter) { 
				rowIDNumber++;
				rowConterNumber = 0;
			}
			itemConsumptionDTO.setRowID(rowIDNumber);
			 
		} catch (Exception e) {
			log.warn("PaymentDetails Set Error"+e);

		}
	}
	
	public void deleteColumnReceived(ItemConsumptionDTO itemDTO) {
		log.info("received delete Column Receivd");
		try {
			double currentDispQty = itemDTO.getCurrentDisposalQuantity();
			for(ItemConsumptionDTO itemDTO2: itemConsumptionCollectorList) {
				if(itemDTO.getRowID() == itemDTO2.getRowID() && itemDTO2.getRowID() == itemDTO2.getRowCounter()) {

					itemConsumptionDTO.setBalanceQuantityAvailable(itemConsumptionDTO.getBalanceQuantityAvailable() + currentDispQty);
					itemConsumptionDTO.setReceivedQuantityTemp(itemConsumptionDTO.getReceivedQuantityTemp() + currentDispQty);
					
					log.info("Delete ID Success set old Data Success BalanceQTY :"+itemDTO2.getBalanceQuantityAvailable());
					break;
				}
			}
		} catch (Exception e) {
			log.warn("Delete Column Error "+e);
			// TODO: handle exception
		}
	}
	
	public String getSumAlreadyDisposedQuantity() {
		double total = 0;
        for(ItemConsumptionDTO collector : getItemConsumptionCollectorList()) {
    		if(collector.getAlreadyDisposalQuantity()!=null)
    			total += collector.getAlreadyDisposalQuantity();
        }
	    return new DecimalFormat("###,###.###").format(total);
		
    }
	
	public String submitItemConsumption() {
		log.info(":::<- submitItemConsumption Called->:::");
		BaseDTO baseDTO = null;
		try {
			if(itemConsumptionCollectorList == null || itemConsumptionCollectorList.size() == 0) {
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_LIST_EMPTY.getErrorCode());
				return null;
			}
			itemConsumption = new ItemConsumption();
			itemConsumptionDetailsList = new ArrayList<ItemConsumptionDetails>();
			ItemConsumptionDTO itemDTO= new ItemConsumptionDTO();
			for(ItemConsumptionDTO bean : itemConsumptionCollectorList) {
				ItemConsumptionDetails item = new ItemConsumptionDetails();
				item.setProductVarietyMaster(selectedIntendConsolidation.getProductVarietyMaster());
				item.setDisposalQuantity(bean.getCurrentDisposalQuantity());
				itemConsumptionDetailsList.add(item);
			}
			itemConsumption.setEntityMaster(selectedEntityMaster);
			itemConsumption.setSectionMaster(selectedSectionMaster);
			itemConsumption.setVersion(0l);
			
			itemDTO.setItemConsumption(itemConsumption);
			itemDTO.setItemConsumptionDetailsList(itemConsumptionDetailsList);
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/saveItemConsumption";
			baseDTO = httpService.post(url,itemDTO);
			log.info("::: submitItemConsumption Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("submitItemConsumption Successfully "+baseDTO.getTotalRecords());
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_SAVE_SUCCESS.getErrorCode());
				return INPUT_FORM_LIST_URL;
			}
			else{
				log.warn("submitItemConsumption Save Error");
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_SAVE_FAILED.getErrorCode());
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.PRINTING_STATIONARY_SAVE_ERROR.getErrorCode());
			log.error("Exception Occured in submitItemConsumption", ee);
		}
		return null;
		
	}
	
	//lazy search
		public void loadLazyIntItemConsumptionList() {
			log.info("<--- loadLazyItemConsumptionTransfer RequirementList ---> ");
			itemConsumptionLazyList = new LazyDataModel<ItemConsumptionDTO>() {
				private static final long serialVersionUID = -1540942419672760421L;

				@Override
				public List<ItemConsumptionDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
						Map<String, Object> filters) {
					List<ItemConsumptionDTO> data = new ArrayList<ItemConsumptionDTO>();
					try {
						BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

						data = mapper.readValue(jsonResponse, new TypeReference<List<ItemConsumptionDTO>>() {
						});

						if (data != null) {
							this.setRowCount(baseDTO.getTotalRecords());
							log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
							size = baseDTO.getTotalRecords();
						}
					} catch (Exception e) {
						log.error("Error in loadLazyItemConsumption RequirementList()  ", e);
					}
					return data;
				}

				@Override
				public Object getRowKey(ItemConsumptionDTO res) {
					return res != null ? res.getId() : null;
				}

				@Override
				public ItemConsumptionDTO getRowData(String rowKey) {
					List<ItemConsumptionDTO> responseList = (List<ItemConsumptionDTO>) getWrappedData();
					try {
						Long value = Long.valueOf(rowKey);
						for (ItemConsumptionDTO res : responseList) {
							if (res.getId().longValue() == value.longValue()) {
								itemConsumptionDTOLazy = res;
								return res;
							}
						}
					}catch(Exception see) {}
					return null;
				}

			};
		}

		public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
				Map<String, Object> filters) throws ParseException {

			itemConsumptionDTOLazy = new ItemConsumptionDTO();

			BaseDTO baseDTO = new BaseDTO();
			log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
					+ "sortField:[" + sortField + "]");

			ItemConsumptionDTO yarnRequirement = new ItemConsumptionDTO();

			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			yarnRequirement.setPaginationDTO(paginationDTO);

			yarnRequirement.setFilters(filters);

			try {
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/itemconsumption/searchData";
				baseDTO = httpService.post(URL, yarnRequirement);
			} catch (Exception e) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.error("Exception in getSearchData() ", e);
			}

			return baseDTO;
		}
		
		public void onRowSelect(SelectEvent event) {
			log.info("Item List onRowSelect method started");
			renderedEdit = true;
			renderedCreate = false;
	    }
		public String addButtonAction() {
			try {
				log.info("Add ItemConsumption Received ");
				headOrRegionOffice = new EntityMaster();
				department = new Department();
				itemConsumptionDTO = new ItemConsumptionDTO();
				selectedEntityMaster = new EntityMaster();
				selectedSectionMaster = new SectionMaster();
				selectedIntendRequest = new IntendRequest();
				selectedIntendConsolidation = new IntendConsolidation();
				intendConsolidationList = new ArrayList<IntendConsolidation>();
				itemConsumptionCollectorList = new ArrayList<ItemConsumptionDTO>();
				sectionMasterList = new ArrayList<SectionMaster>();
				intendRequestList = new ArrayList<IntendRequest>();

				return ADD_URL;
			} catch (Exception e) {
				log.error("Add ItemConsumption Redired Error :"+e);
				
			}
			return null;
		}
		public void clearTableAction() {
				log.info("Clear Table Received ");
				itemConsumptionDTO = new ItemConsumptionDTO();
				
				selectedIntendRequest = new IntendRequest();
				selectedIntendConsolidation = new IntendConsolidation();
				intendConsolidationList = new ArrayList<IntendConsolidation>();
				itemConsumptionCollectorList = new ArrayList<ItemConsumptionDTO>();
		}
		public String clearButtonAction() {
			try {
				log.info("Clear Button Action Received");
				renderedEdit = false;
				renderedCreate = true;
				selectedItemConsumptionDTO = new ItemConsumptionDTO();

				return INPUT_FORM_LIST_URL;
			} catch (Exception e) {
				log.info("Clear Button Action redirect error "+e);
			}
			return null;
	    }
	//View Item Consumption  
	public String viewItemConsumptionDetails() {
		log.info(":::<- viewItemConsumptionDetailsList Called->:::"+selectedItemConsumptionDTO.getId());
		BaseDTO baseDTO = null;
		try {
			itemConsumptionDetailsList = new ArrayList<ItemConsumptionDetails>();
			itemConsumptionDetails = new ItemConsumptionDetails();
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/viewItemConsumptionDetailsList/"+selectedItemConsumptionDTO.getId();
			baseDTO = httpService.get(url);
			log.info("::: viewItemConsumptionDetailsList Response :");
			if (baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemConsumptionDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<ItemConsumptionDetails>>() {
				});

				itemConsumptionDetails = itemConsumptionDetailsList.get(0); 
				log.info("::: View ItemConsumptionDetails Fetch Success");
				

				return VIEW_URL;
			}
			else{
				log.warn("viewItemConsumptionDetailsList  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in viewItemConsumptionDetailsList", ee);
		}
		return null;
		
	}
	
	//Delete Item Consumption  
	public String deleteItemConsumption() {
		log.info(":::<- deleteItemConsumption Called->:::"+selectedItemConsumptionDTO.getId());
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/deleteItemConsumption/"+selectedItemConsumptionDTO.getId();
			baseDTO = httpService.get(url);
			log.info("::: deleteItemConsumption Response :");
			if (baseDTO.getStatusCode()==0) {
				renderedEdit = false;
				renderedCreate = true;
				selectedItemConsumptionDTO = new ItemConsumptionDTO();
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_DELETE_SUCCESS.getErrorCode());

				return INPUT_FORM_LIST_URL;
			}
			else{
				errorMap.notify(ErrorDescription.PRINTING_STATIONARY_DELETE_FAILED.getErrorCode());
				log.warn("deleteItemConsumption  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in deleteItemConsumption", ee);
		}
		return null;
		
	}
	
	public String acknowledgementByStaff() {
		log.info(":::<- acknowledgementByStaff Called->:::"+selectedItemConsumptionDTO.getId());
		try {
			BaseDTO baseDTO = null;
			ItemConsumptionDTO itemDTO= new ItemConsumptionDTO();
			itemDTO.setId(selectedItemConsumptionDTO.getId());
			itemDTO.setEmpId(loginBean.getEmployee().getId());
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/itemconsumption/Ackbystaff";
			baseDTO = httpService.post(url,itemDTO);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				AppUtil.addInfo("Acknowledgement Saved Successfully");
				return LIST_URL;
			}
		}
		catch(Exception ex) {
			log.error("Exception Occured in acknowledgementByStaff", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info(":::<-End acknowledgementByStaff Method->:::");
		return null;
	}
	
}
