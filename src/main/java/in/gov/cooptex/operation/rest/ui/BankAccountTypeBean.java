package in.gov.cooptex.operation.rest.ui;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.BankAccountType;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("bankAccountTypeBean")
@Scope("session")
@Log4j2
public class BankAccountTypeBean {

	final String LIST_PAGE = "/pages/masters/listBankAccountType.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createBankAccountType.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String BANK_ACCOUNT_TYPE_URL = AppUtil.getPortalServerURL() + "/bankAccountType";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	BankAccountType bankAccountType, selectedBankAccountType;

	@Getter
	@Setter
	LazyDataModel<BankAccountType> lazyBankAccountTypeList;

	List<BankAccountType> bankAccountTypeList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showBankAccountTypeListPage() {
		log.info("<==== Starts BankAccountTypeBean.showBankAccountTypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		bankAccountType = new BankAccountType();
		selectedBankAccountType = new BankAccountType();
		loadLazyBankAccountTypeList();
		log.info("<==== Ends BankAccountTypeBean.showBankAccountTypeListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyBankAccountTypeList() {
		log.info("<===== Starts BankAccountTypeBean.loadLazyBankAccountTypeList ======>");

		lazyBankAccountTypeList = new LazyDataModel<BankAccountType>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<BankAccountType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = BANK_ACCOUNT_TYPE_URL + "/getallbankaccounttypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						bankAccountTypeList = mapper.readValue(jsonResponse,
								new TypeReference<List<BankAccountType>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return bankAccountTypeList;
			}

			@Override
			public Object getRowKey(BankAccountType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BankAccountType getRowData(String rowKey) {
				for (BankAccountType bankAccountType : bankAccountTypeList) {
					if (bankAccountType.getId().equals(Long.valueOf(rowKey))) {
						selectedBankAccountType = bankAccountType;
						return bankAccountType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends BankAccountTypeBean.loadLazyBankAccountTypeList ======>");
	}

	public String showBankAccountTypeListPageAction() {
		log.info("<===== Starts BankAccountTypeBean.showBankAccountTypeListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedBankAccountType == null || selectedBankAccountType.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_BANK_ACCOUNT_TYPE.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create bankAccountType called..");
				bankAccountType = new BankAccountType();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit bankAccountType called..");
				String url = BANK_ACCOUNT_TYPE_URL + "/getbyid/" + selectedBankAccountType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					bankAccountType = mapper.readValue(jsonResponse, new TypeReference<BankAccountType>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete bankAccountType called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBankAccountTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showBankAccountTypeListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends BankAccountTypeBean.showBankAccountTypeListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts BankAccountTypeBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(BANK_ACCOUNT_TYPE_URL + "/deletebyid/" + selectedBankAccountType.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBankAccountTypeDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete bankAccountType ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends BankAccountTypeBean.deleteConfirm ===========>");
		return showBankAccountTypeListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts BankAccountTypeBean.saveOrUpdate =======>");
		try {
			if (bankAccountType.getCode() == null || bankAccountType.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_CODE_EMPTY.getCode());
				return null;
			}
			if (bankAccountType.getName() == null || bankAccountType.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_NAME_EMPTY.getCode());
				return null;
			}
			if (bankAccountType.getName().trim().length() < 3 || bankAccountType.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (bankAccountType.getLocalName() == null || bankAccountType.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_LNAME_EMPTY.getCode());
				return null;
			}
			if (bankAccountType.getLocalName().trim().length() < 3
					|| bankAccountType.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (bankAccountType.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_STATUS_EMPTY.getCode());
				return null;
			}
			String url = BANK_ACCOUNT_TYPE_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, bankAccountType);
			if (response != null && response.getStatusCode() == 0) {
				log.info("bankAccountType saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.BANK_ACCOUNT_TYPE_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends BankAccountTypeBean.saveOrUpdate =======>");
		return showBankAccountTypeListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts BankAccountTypeBean.onRowSelect ========>" + event);
		selectedBankAccountType = ((BankAccountType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends BankAccountTypeBean.onRowSelect ========>");
	}

}
