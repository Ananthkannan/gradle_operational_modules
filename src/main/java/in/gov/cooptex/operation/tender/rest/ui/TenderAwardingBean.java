package in.gov.cooptex.operation.tender.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.UnaryOperator;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.tenderawarding.dto.TenderAwardingDTO;
import in.gov.cooptex.core.tendernegotiation.dto.TenderNegotiationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.tender.model.Tender;
import in.gov.cooptex.operation.tender.model.TenderEvaluation;
import in.gov.cooptex.operation.tender.model.TenderFileDetails;
import in.gov.cooptex.operation.tender.model.TenderItemDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("tenderAwardingBean")
@Scope("session")
public class TenderAwardingBean {

	private final String TENDER_AWARDING_PAGE = "/pages/operation/tender/createTenderAwarding.xhtml?faces-redirect=true";

	private final String LIST_TENDER_AWARDING_PAGE = "/pages/operation/tender/listTenderAwarding.xhtml?faces-redirect=true";

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<TenderAwardingDTO> tenderList = new ArrayList<>();
	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList = new ArrayList<>();
	
	@Setter
	@Getter
	private StreamedContent files;

	@Autowired
	LoginBean loginBean;
	@Autowired
	@Getter
	@Setter
	private HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	String notes, viewNote;
	@Getter
	@Setter
	TenderAwardingDTO tenderAwardingDTO = new TenderAwardingDTO();

	@Getter
	@Setter
	List<TenderEvaluation> list = new ArrayList<>();

	@Getter
	@Setter
	private List<TenderEvaluation> tenderAwardingList = new ArrayList<>();

	@Getter
	@Setter
	LazyDataModel<TenderAwardingDTO> lazyTenderList;

	String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	Tender tender;

	@Getter
	@Setter
	AddressMaster bidopeningAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster bidMeetingAddress = new AddressMaster();
	
	@Getter
	@Setter
	Tender selectedTenderr;

	// private String serverURL = AppUtil.getPortalServerURL();

	private final String VIEW_URL = "/tender/view";

	@Getter
	@Setter
	TenderAwardingDTO selectedTender;

	@Getter
	@Setter
	private int tenderListSize;

	@Getter
	@Setter
	Boolean applyButton = false, renderTenderCreate;
	@Getter
	@Setter
	String pageAction;

	private final String LIST_URL = "/tenderAwarding/getall";
	private final String VIEW_PAGE_TENDERAPPLY = "/pages/operation/tender/viewTenderAwarding.xhtml?faces-redirect=true";

	private String serverURL = AppUtil.getPortalServerURL();

	public String showAwardingPage() {
		log.info("<===== Starts tenderAwardingBean.showAwardingPage ======>");
		loadTenderType();
		tenderAwardingDTO = new TenderAwardingDTO();
		tenderAwardingList = new ArrayList<>();
		list = new ArrayList<>();
		return TENDER_AWARDING_PAGE;
	}

	private int index;

	public void loadTenderType() {

		try {
			log.info("<============= loadTenderType=====================>");
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + "/tender/getTenderListByTenderAwardDetails";
			log.info("<<==  url:: " + url);
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				log.info(jsonResponse);
				tenderList = mapper.readValue(jsonResponse, new TypeReference<List<Tender>>() {
				});
				log.info("tenderList---------" + tenderList.size());
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void loadTenderName() {

		try {
			loadSupplierMaster();
			log.info("<============= loadTenderName=====================>");
			BaseDTO baseDTO = new BaseDTO();
			Tender tender = new Tender();
			tender.setId(tenderAwardingDTO.getId());
			log.info(tender.getId());
			String url = appPreference.getPortalServerURL() + "/tender/view";
			log.info("<<==  url:: " + url);
			baseDTO = httpService.post(url, tender);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				log.info(jsonResponse);
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

			}

			log.info(tenderAwardingDTO.getTenderName());

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void loadSupplierMaster() {
		log.info("<===== Starts tenderAwardingBean.loadSupplierMaster ======>");
		try {
			log.info("<============= loadSupplierMaster=====================>");
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + "/supplier/master/getSupplierByTenderNo/"
					+ tenderAwardingDTO.getId();
			log.info("<<==  url:: " + url);
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				log.info(jsonResponse);
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMasterList---------" + supplierMasterList.size());
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String search() {
		log.info("<===== Starts tenderAwardingBean.search ======>");
		try {

			tenderAwardingList = new ArrayList<>();

			BaseDTO baseDTO = null;
			String url = appPreference.getPortalServerURL() + "/tenderAwarding/getTenderDetails";
			baseDTO = httpService.post(url, tenderAwardingDTO);
			if (baseDTO != null) {
				log.info("inside");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tenderAwardingList = mapper.readValue(jsonResponse, new TypeReference<List<TenderEvaluation>>() {
				});
				log.info("reportData" + tenderAwardingList.size() + tenderAwardingList);

			}
		} catch (Exception e) {
			log.error("search", e);
		}
		return "";
	}

	public String award() {
		log.info("<===== Starts tenderAwardingBean.award ======>");
		try {
			list = new ArrayList<>();
			for (TenderEvaluation tenderEvaluation : tenderAwardingList) {

				if (tenderEvaluation.getSelectedData() == true) {
					TenderEvaluation tenderEvaluationList = new TenderEvaluation();
					tenderEvaluationList.setItemName(tenderEvaluation.getItemName());
					tenderEvaluationList.setSupplierName(tenderEvaluation.getSupplierName());
					if (tenderEvaluation.getNegotiatedAmount() >= 0) {
						tenderEvaluationList.setFinalizedAmount(tenderEvaluation.getNegotiatedAmount());
					}
					if (tenderEvaluation.getNegotiatedAmount() <= 0) {
						tenderEvaluationList.setFinalizedAmount(tenderEvaluation.getQuotedAmount());
					}

					tenderEvaluationList.setNotes(tenderEvaluation.getNotes());

					list.add(tenderEvaluationList);

				}

			}

		} catch (Exception e) {
			log.error("award", e);
		}
		return "";
	}

	/*
	 * public String callTenderAwarding() {
	 * log.info("<===== Starts tenderAwardingBean.callTenderAwarding ======>");
	 * return TENDER_AWARDING_PAGE; }
	 */

	public String createTenderAwarding() {
		log.info("<===== Starts tenderAwardingBean.createTenderAwarding ======>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			tenderAwardingDTO.setTenderAwardingTableList(tenderAwardingList);
			tenderAwardingDTO.setCreatedBy(loginBean.getUserDetailSession());
			String url = AppUtil.getPortalServerURL() + "/tenderAwarding/create";
			baseDTO = httpService.post(url, tenderAwardingDTO);
			log.info("saved");
			if (baseDTO.getResponseContent() != null) {
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.TENDER_AWARDING_CREATED_SUCCESSFULLY.getErrorCode());

					return loadTenderListPage();
				}
			}
		} catch (Exception e) {
			log.info("<--- Exception in createTenderAwarding() --->", e);
		}

		return LIST_TENDER_AWARDING_PAGE;
	}

	public void createNotes(TenderEvaluation tenderList, int index1) {
		notes = null;
		index = index1;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('commentdialog').show();");
		notes = tenderAwardingList.get(index).getNotes();

	}

	public void viewNotes(TenderEvaluation tenderList, int index1) {
		viewNote = null;
		index = index1;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('viewcommentdialog').show();");
		viewNote = list.get(index).getNotes();

	}

	public void saveNotes() {
		tenderAwardingList.get(index).setNotes(notes);
		log.info("..tenderAwardingBean.." + tenderAwardingList);

	}

	public String cancel() {

		tenderAwardingList = new ArrayList<>();
		list = new ArrayList<>();
		return LIST_TENDER_AWARDING_PAGE;
	}

	public String viewTender() {
		selectedTenderr = new Tender();
		log.info("<----Redirecting to user view page---->");
		if (selectedTender == null) {
			Util.addWarn("Please Select One Tender");
			return null;
		}
		selectedTenderr.setId(selectedTender.getId());

		getTenderView(selectedTenderr);
		
		if (selectedTender.getStatus().equalsIgnoreCase("Tender Negotiated")) {
			renderTenderCreate = true;
		} else {
			renderTenderCreate = false;
		}

		search();
		return VIEW_PAGE_TENDERAPPLY;

	}

	private void getTenderView(Tender selectedTender) {
		log.info("<<<< ----------Start tenderAwarding Bean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + VIEW_URL;
			baseDTO = httpService.post(url, selectedTender);
			selectedTenderr = new Tender();
			Double totalAmount = 0.0;

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedTenderr = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

				bidopeningAddress = selectedTenderr.getBidMeetingAddressId();
				log.info("bidopeningAddress id==> " + bidopeningAddress.getId());

				bidMeetingAddress = selectedTenderr.getBidMeetingAddressId();

				log.info("bidMeetingAddress id==> " + bidMeetingAddress.getId());

			}
			for (TenderItemDetails item : selectedTenderr.getTenderItemDetailsList()) {
				totalAmount += item.getItemAmount();
			}
			selectedTenderr.setTotalAmount(totalAmount);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedTender != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End tenderAwarding Bean-getTenderView ------- >>>>");
	}

	private void getAllTenderList() {
		log.info("<===== Starts tenderAwardingBean.lazyTenderList ======>");
		lazyTenderList = new LazyDataModel<TenderAwardingDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TenderAwardingDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + LIST_URL;
					log.info("getAllTenderList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderList = mapper.readValue(jsonResponse, new TypeReference<List<TenderAwardingDTO>>() {
						});

						this.setRowCount(response.getTotalRecords());
						tenderListSize = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("getAllTenderList :: Exception ==> ", e);
				}
				log.info("Ends lazy load...." + tenderList.size());
				return tenderList;
			}

			@Override
			public Object getRowKey(TenderAwardingDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TenderAwardingDTO getRowData(String rowKey) {
				try {
					for (TenderAwardingDTO tenderObj : tenderList) {
						if (tenderObj.getId().equals(Long.valueOf(rowKey))) {
							selectedTender = tenderObj;
							return tenderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends tenderAwardingBean.lazyTenderList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts tenderAwardingBean.onRowSelect ========>" + event);
		log.info("tender status=> " + selectedTender.getStatus());
		if (selectedTender.getStatus().equalsIgnoreCase("Tender Negotiated")) {
			applyButton = true;
		} else {
			applyButton = false;
		}
		log.info("<===Ends tenderAwardingBean.onRowSelect ========>");
	}

	public String showAddPage() {
		log.info("<=tenderAwardingBean showAddPage start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (selectedTender == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String url = AppUtil.getPortalServerURL() + "/tender/get/" + selectedTender.getId();
			log.info("tenderNegotiationBean showAddPage url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});
				tenderAwardingDTO.setTenderRefNumber(tender.getTenderReferenceNo());
				tenderAwardingDTO.setTenderName(tender.getTenderName());
				tenderAwardingDTO.setId(tender.getId());
				loadSupplierMaster();

				return TENDER_AWARDING_PAGE;

			} else {
				tender = new Tender();
			}

		} catch (Exception e) {
			log.error("Exception in showAddPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public void downloadfile(TenderFileDetails tenderFileDetails) {
		InputStream input = null;
		try {
			if (tenderFileDetails.getFilePath() != null) {
				File file = new File(tenderFileDetails.getFilePath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				files = (new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()),
						file.getName()));
				log.error(" File Downloaded >>>>>>>>>>------- " + files.getName());

			}
		} catch (Exception e) {
			log.error("Exception Occured IN DownloadFile", e);
		}

	}

	public String goToPage() {

		log.info("Goto Page [" + pageAction + "]");

		// return showListPage();
		if (pageAction.equalsIgnoreCase("ADD")) {

			return showAddPage();
			// return
			// "/pages/operation/tender/createApplyingTender.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String loadTenderListPage() {
		log.info("tenderAwardingBean loadTenderListPage.....");
		try {

			selectedTender = new TenderAwardingDTO();
			getAllTenderList();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_TENDER_AWARDING_PAGE;

	}

	public void clear() {

		tenderAwardingList = new ArrayList<>();
	}

}
