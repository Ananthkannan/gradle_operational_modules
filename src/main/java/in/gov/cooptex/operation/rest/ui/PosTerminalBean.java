/**
 * 
 */
package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AllowanceGradeCity;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.PosTerminal;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */

@Log4j2
@Service("posTerminalBean")
@Scope("session")
public class PosTerminalBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final String LIST_REGISTER_POS_TERMINAL_PAGE = "/pages/operation/posterminal/listPOSTerminal.xhtml?faces-redirect=true;";
	private final String CREATE_REGISTER_POS_TERMINAL_PAGE = "/pages/operation/posterminal/createPOSTerminal.xhtml?faces-redirect=true;";
	private final String VIEW_REGISTER_POS_TERMINAL_PAGE = "/pages/operation/posterminal/viewPOSTerminal.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> regionalOfficeList;

	@Getter
	@Setter
	List<EntityMaster> shrowroomList;

	@Getter
	@Setter
	PosTerminal posTerminal;

	@Getter
	@Setter
	LazyDataModel<PosTerminal> posTerminalLazyList;

	@Getter
	@Setter
	List<PosTerminal> posTerminalList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Autowired
	ErrorMap errorMap;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@PostConstruct
	private void init() {
		posTerminal = new PosTerminal();
		clear();
		loadRegionalOfficeList();
		loadLazyPosTerminalList();
	}

	public void clear() {
		posTerminal = new PosTerminal();
		regionalOfficeList = new ArrayList<EntityMaster>();

	}

	public String showRegisterPosTerminalListPage() {
		return LIST_REGISTER_POS_TERMINAL_PAGE;
	}

	public void loadRegionalOfficeList() {
		log.info("<- Inside posTerminalBean starts loadRegionalOfficeList() method ->");
		try {
			regionalOfficeList = commonDataService.loadActiveRegionalOffices();
			if (regionalOfficeList != null && regionalOfficeList.size() > 0) {
				log.info("<- Region List fetched successfully->" + regionalOfficeList.size());
			}

		} catch (Exception e) {
			log.error("Inside posTerminalBean exception occured in loadRegionalOfficeList method ", e);
		}
		log.info("<- Ends posTerminalBean.loadRegionalOfficeList ->");

	}
	
	public void loadActiveRegionalOfficeList() {
		log.info("<- Inside posTerminalBean starts loadActiveRegionalOfficeList() method ->");
		try {
			regionalOfficeList = commonDataService.loadActiveRegionalOffices();
			if (regionalOfficeList != null && regionalOfficeList.size() > 0) {
				log.info("<- Region List fetched successfully->" + regionalOfficeList.size());
			}

		} catch (Exception e) {
			log.error("Inside posTerminalBean exception occured in loadActiveRegionalOfficeList method ", e);
		}
		log.info("<- Ends posTerminalBean.loadRegionalOfficeList ->");

	}

	public void loadShrowroomList() {
		log.info("<- Inside posTerminalBean starts loadShrowroomList() method ->");
		try {
			log.info("Region id for selected region --->" + posTerminal.getRegion().getId());
			shrowroomList = commonDataService.loadShowroomListForRegion(posTerminal.getRegion().getId());

			if (shrowroomList != null && shrowroomList.size() > 0) {
				log.info("<- Shrowroom List fetched successfully->" + shrowroomList.size());
			}

		} catch (Exception e) {
			log.error("Inside posTerminalBean exception occured in loadShrowroomList method ", e);
		}
		log.info("<- Ends posTerminalBean.loadShrowroomList ->");

	}

	public String savePosTerminal() {
		log.info("<- Inside posTerminalBean starts savePosTerminal() method ->");
		BaseDTO response = new BaseDTO();

		try {
			String url = SERVER_URL + "/posTerminal/savePosTerminal";
			log.info(" Inside posTerminalBean starts savePosTerminal method" + url);
			response = httpService.post(url, posTerminal);
			if (response != null) {
				log.info("<- Inside posTerminalBean ===> save successfull ->");
			}
			log.info("<- Inside posTerminalBean ends savePosTerminal() method ->");

		} catch (Exception e) {
			log.error("Error at savePosTerminal " + e);
			response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_REGISTER_POS_TERMINAL_PAGE;
	}

	public void loadLazyPosTerminalList() {

		log.info("Inside posTerminalBean starts loadLazyPosTerminalList() method");
		String POS_TERMINAL_LIST_URL = SERVER_URL + "/posTerminal/getallPosTerminalListlazy";

		posTerminalLazyList = new LazyDataModel<PosTerminal>() {

			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<PosTerminal> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for POS Terminal List=============>" + POS_TERMINAL_LIST_URL);
					BaseDTO response = httpService.post(POS_TERMINAL_LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						posTerminalList = mapper.readValue(jsonResponse, new TypeReference<List<PosTerminal>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGradewiseCityConfigList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return posTerminalList;
			}

			@Override
			public Object getRowKey(PosTerminal res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PosTerminal getRowData(String rowKey) {
				try {
					for (PosTerminal posTerminalObj : posTerminalList) {
						if (posTerminalObj.getId().equals(Long.valueOf(rowKey))) {
							posTerminal = posTerminalObj;
							return posTerminalObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends posTerminalBean.loadLazyPosTerminalList ======>");
	}

}
