package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankAccountType;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.accounts.model.EntityBankBranch;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.ProfileMasterSearchRequestDTO;
import in.gov.cooptex.core.dto.ProfileMasterSearchResponseDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.BuildingType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityGroupMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("profileMasterBean")
public class ProfileMasterBean {

	private final String LIST_PAGE = "/pages/masters/listProfileMaster.xhtml?faces-redirect=true;";
	private final String ADD_PAGE = "/pages/masters/createProfileMaster.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/viewProfileMaster.xhtml?faces-redirect=true;";

	@Autowired
	AddressMasterBean addressMasterBean;

	@Autowired
	HttpService httpService;
 
	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action, name;

	@Getter
	@Setter
	private AddressMaster adressMaster;

	@Getter
	@Setter
	List<EntityMaster> regionEntityMasterList;

	@Getter
	@Setter
	EntityMasterDTO profileMaster;

	@Getter
	@Setter
	List<EntityType> listEntityType = new ArrayList<EntityType>();

	@Getter
	@Setter
	List<BuildingType> listBuildingType = new ArrayList<BuildingType>();

	@Getter
	@Setter
	List<EntityGroupMaster> listGroupMaster = new ArrayList<EntityGroupMaster>();

	@Getter
	@Setter
	EntityGroupMaster seletedEntityGroup = new EntityGroupMaster();

	@Getter
	@Setter
	List<BankMaster> listBankMaster = new ArrayList<BankMaster>();

	@Getter
	@Setter
	BankMaster selectedBank;

	@Getter
	@Setter
	List<BankBranchMaster> listBranchMaster = new ArrayList<BankBranchMaster>();
	
	
	@Getter
	@Setter
	List<BankBranchMaster> branchMasterList = new ArrayList<BankBranchMaster>();

	@Getter
	@Setter
	BankBranchMaster seletedBankBranchMaster;

	@Getter
	@Setter
	List<EntityBankBranch> bankBranchList = new ArrayList<EntityBankBranch>();

	@Getter
	@Setter
	List<EntityBankBranch> deletebankBranchList = new ArrayList<EntityBankBranch>();
	
	@Getter
	@Setter
	EntityBankBranch bankAccount;

	@Getter
	@Setter
	List<BankAccountType> accountTypeList = new ArrayList<BankAccountType>();

	@Getter
	@Setter
	List<GlAccount> listGlAccount = new ArrayList<GlAccount>();

	@Getter
	@Setter
	LazyDataModel<ProfileMasterSearchResponseDTO> profileLazyList;

	@Getter
	@Setter
	ProfileMasterSearchResponseDTO responseDto;

	@Getter
	@Setter
	private UploadedFile billcopyFile = null, agreementcopyFile = null;

	@Getter
	@Setter
	Integer totalRecords;

	@Getter
	@Setter
	private String address = "", billCopyFileName = "", agreementCopyFileName = "", pageHead = "", createdeName = "",
			modifiedName = "";

	@Getter
	@Setter
	Integer rentDate;

	@Getter
	@Setter
	private Boolean addButtonFlag = true;

	@Autowired
	CommonBean commonBean;

	@Getter
	@Setter
	private int[] rentDateList = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
			24, 25, 26, 27, 28, 29, 30, 31 };

	@Getter
	@Setter
	private Boolean monthRentAmountFlag = true, rentPayDateFlag = true, billCopyFlag = true, leaseAmountFlag = true,
			leaseFromFlag = true, leaseToFlag = true, leaseCopyFlag = true;

	private String serverURL;

	private BaseDTO baseDTO;

	private String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();
	
	@Getter
	@Setter
	BankMaster bankMaster;
	
	@Getter
	@Setter
	BankBranchMaster bankBranchMaster;
	
	@Getter
	@Setter
	BankAccountType bankAccountType;
	
	@Getter
	@Setter
	private String accountNumber;
	
	@Getter
	@Setter
	private GlAccount glAccount;
	

	private void loadData() {
		profileMaster = new EntityMasterDTO();
		try {
			serverURL = AppUtil.getPortalServerURL();
			regionEntityMasterList = commonDataService.findEntityByEntityType(EntityType.REGIONAL_OFFICE);
		} catch (Exception e) {

			log.error("<<====  Error In ProfileMasterBean Constructor:: ");
		}
	}

	public String showAddPage() {
		loadData();
		action = "ADD";
		pageHead = "Create";
		addressMasterBean.openAddressDialog();
		getEntityGroupList();
		loadBankMaster();
		loadBuildingType();
		loadAccountType();
		loadGlAccount();
		seletedEntityGroup = new EntityGroupMaster();
		bankAccount = new EntityBankBranch();
		bankBranchList.clear();
		profileMaster = new EntityMasterDTO();
		address = "";
		name = "";
		bankMaster = new BankMaster();
		bankBranchMaster = new BankBranchMaster();
		bankAccountType = new BankAccountType();
		glAccount = new GlAccount();
		accountNumber = null;
		return ADD_PAGE;
	}

	public String showEditPage() {
		if (responseDto == null || responseDto.getId() == null) {
			errorMap.notify(ErrorDescription.PROFILE_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}
		action = "EDIT";
		pageHead = "Edit";
		deletebankBranchList=new ArrayList<>();
		getEntityGroupList();
		loadBankMaster();
		loadAccountType();
		loadBuildingType();
		loadGlAccount();
		onSelectBuildingType();
		getProfileData(responseDto);
		name = profileMaster.getEntityTypeMaster().getGroupName();
		onSelectEntityGroup();
		address = addressMasterBean.prepareAddress(profileMaster.getAddressMaster());
		log.info("<<============  ADDRESS  :: " + address);
		bankAccount = new EntityBankBranch();
		addressMasterBean.setAddressMaster(profileMaster.getAddressMaster()!=null?profileMaster.getAddressMaster():new AddressMaster());
	//	addressMasterBean.openAddressDialogWithAction(action);
		addressMasterBean.openAddressDialog();
		if (profileMaster.getBillcopyFilePath() != null) {
			String[] billnames = profileMaster.getBillcopyFilePath().split("/");
			billCopyFileName = billnames[billnames.length - 1];
		}
		if (profileMaster.getAgreementcopyFilePath() != null) {
			String[] agreementNames = profileMaster.getAgreementcopyFilePath().split("/");
			agreementCopyFileName = agreementNames[agreementNames.length - 1];
		}
		return ADD_PAGE;
	}

	public String showViewPage() {
		if (responseDto == null || responseDto.getId() == null) {
			errorMap.notify(ErrorDescription.PROFILE_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}

		action = "VIEW";
		pageHead = "View";
		getEntityGroupList();
		loadBankMaster();
		loadAccountType();
		loadBuildingType();
		loadGlAccount();
		onSelectBuildingType();
		getProfileData(responseDto);
		name = profileMaster.getEntityTypeMaster().getGroupName();
		onSelectEntityGroup();
		address = addressMasterBean.prepareAddress(profileMaster.getAddressMaster());
		bankAccount = new EntityBankBranch();
		addressMasterBean.setAddressMaster(profileMaster.getAddressMaster());
		if (profileMaster.getBillcopyFilePath() != null) {
			String[] billnames = profileMaster.getBillcopyFilePath().split("/");
			billCopyFileName = billnames[billnames.length - 1];
		}
		if (profileMaster.getAgreementcopyFilePath() != null) {
			String[] agreementNames = profileMaster.getAgreementcopyFilePath().split("/");
			agreementCopyFileName = agreementNames[agreementNames.length - 1];
		}
		log.info("<<=== ProfileMaster getCreatedById ==== " + profileMaster.getCreatedById());
		EmployeeMaster employee = commonDataService.loadEmployeeByUser(profileMaster.getCreatedById());
		if(employee != null)
		{
			createdeName = employee.getFirstName() + " " + employee.getLastName();
		}
		if (profileMaster.getModifiedById() != null) {
			log.info("<<=== ProfileMaster getModifiedById==== " + profileMaster.getModifiedById());
			employee = commonDataService.loadEmployeeByUser(profileMaster.getModifiedById());
			modifiedName = employee.getFirstName() + " " + employee.getLastName();
		}
		// getEmployeeName(profileMaster.getCreatedById());
		return VIEW_PAGE;
	}

	public String showListPage() {
		action = "LIST";
		addButtonFlag = true;
		loadData();
		list();
		return LIST_PAGE;
	}

	public void getEntityGroupList() {
		log.info("<<======  getEntityGroupList ==##  STARTS");
		try {
			Set<String> set = new TreeSet<String>();
			String url = serverURL + "/entityGroupMaster/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listGroupMaster = mapper.readValue(jsonResponse, new TypeReference<List<EntityGroupMaster>>() {
			});

		} catch (Exception e) {
			log.error("<<==============  error in getEntityGroupList " + e);
		}
	}

	public void onSelectEntityGroup() {
		log.info("<<======  onSelectEntityGroup ==##  STARTS" + name);
		try {
			String url = serverURL + "/entitytypemaster/getByEntityGroup/" + name;
			log.info("<<=== URL:: " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listEntityType = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
			if (listEntityType != null)
				log.info("<<====  LiST SIZE:: " + listEntityType.size());
		} catch (Exception e) {
			log.error("<<==============  error in onSelectEntityGroup " + e);
		}
	}

	public void loadBankMaster() {
		log.info("<<======  loadBankMaster ==##  STARTS");
		try {
			/**
			 * path	/COOPTEX-CORE-SERVICES/src/main/java/in/gov/cooptex/common/controller/
			 * 
			 * BankMasterController.java
			 */
			String url = serverURL + "/bankmaster/getAllBanks";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBankMaster = mapper.readValue(jsonResponse, new TypeReference<List<BankMaster>>() {
			});
		} catch (Exception e) {
			log.error("<<==============  error in loadBankMaster " + e);
		}
	}

	/**
	 * 
	 */
	public void onBankChange() {
		log.info("<<======  loadBankMaster ==##  STARTS");
		try {
			//String url = serverURL + "/bankBranchMaster/getbybank/" + selectedBank.getId();
			/**
			 * path	/COOPTEX-CORE-SERVICES/src/main/java/in/gov/cooptex/core/accounts/controller/
			 * 
			 * BankBranchMasterController.java
			 */
			String url = serverURL + "/bankBranchMaster/getbankbranchbybankid/" + selectedBank.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBranchMaster = mapper.readValue(jsonResponse, new TypeReference<List<BankBranchMaster>>() {
			});
		} catch (Exception e) {
			log.info("<<===  Error in onBankChange :: " + e);
		}
		log.info("<<======  loadBankMaster ==##  ENDS");
	}
	
	public void bankChange() {
		log.info("<<======  loadBankMaster ==##  STARTS");
		try {
			branchMasterList = new ArrayList<>();
			//String url = serverURL + "/bankBranchMaster/getbybank/" + bankMaster.getId();
			/**
			 * path	/COOPTEX-CORE-SERVICES/src/main/java/in/gov/cooptex/core/accounts/controller/
			 * 
			 * BankBranchMasterController.java
			 */
			String url = serverURL + "/bankBranchMaster/getbankbranchbybankid/" + bankMaster.getId();
			
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			branchMasterList = mapper.readValue(jsonResponse, new TypeReference<List<BankBranchMaster>>() {
			});
		} catch (Exception e) {
			log.info("<<===  Error in onBankChange :: " + e);
		}
		log.info("<<======  loadBankMaster ==##  ENDS");
	}

	public void loadGlAccount() {
		log.info("<<=====  loadGlAccount --- ==##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listGlAccount = mapper.readValue(jsonResponse, new TypeReference<List<GlAccount>>() {
			});
		} catch (Exception e) {
			log.error("<<===  Error during loadGlAccount");
		}
		log.info("<<=====  loadGlAccount --- ==##ENDS");
	}

	public void loadAccountType() {
		log.info("<<====  load Account Type ==## STARTS");
		try {
			String url = serverURL + "/bankAccountType/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			accountTypeList = mapper.readValue(jsonResponse, new TypeReference<List<BankAccountType>>() {
			});
		} catch (Exception e) {
			log.error("<<====  Error in load account type");
		}
		log.info("<<=======  loadAccountType  ===##ENDS");
	}

	public void loadBuildingType() {
		log.info("<<======  loadBuildingType ==##  STARTS");
		try {
			String url = serverURL + "/buildingType/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBuildingType = mapper.readValue(jsonResponse, new TypeReference<List<BuildingType>>() {
			});
		} catch (Exception e) {
			log.error("<<==============  error in loadBuildingType " + e);
		}
	}

	public void onSelectBuildingType() {
		log.info("<<==  onSelectBuildingType ----  ===## STARTS");
		profileMaster.setLeaseAmount(null);
		profileMaster.setLeaseFromDate(null);
		profileMaster.setLeaseToDate(null);
		profileMaster.setRentalAmount(null);
		profileMaster.setRentalPayDate(null);
	//	profileMaster.setBillcopyFilePath(null);
	//	profileMaster.setAgreementcopyFilePath(null);
		billCopyFileName = null;
		agreementCopyFileName = null;
		if (profileMaster.getBuildingType() != null) {
			if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Rental")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				monthRentAmountFlag = false;
				rentPayDateFlag = false;
				billCopyFlag = false;
				leaseAmountFlag = true;
				leaseFromFlag = true;
				leaseToFlag = true;
				leaseCopyFlag = true;
			} else if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Own")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				monthRentAmountFlag = true;
				rentPayDateFlag = true;
				billCopyFlag = true;
				leaseAmountFlag = true;
				leaseFromFlag = true;
				leaseToFlag = true;
				leaseCopyFlag = true;
			} else if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Lease")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				monthRentAmountFlag = true;
				rentPayDateFlag = true;
				billCopyFlag = true;
				leaseAmountFlag = false;
				leaseFromFlag = false;
				leaseToFlag = false;
				leaseCopyFlag = false;
			}
		}
		log.info("<<===  onSelectBuildingType ===== ## ENDS");
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		// String addressArray[] =
		// AppUtil.prepareAddress(addressMasterBean.getAddressMaster());
		profileMaster.setAddressMaster(addressMasterBean.getAddressMaster());
		// adressMaster = circleMaster.getAddressMaster();
		address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("createProfileMasterForm");
	}

	public void addAccount(EntityBankBranch bankBranch) {
		log.info("<<====  addAccount === >>");
		log.info("<<===  bankBranch :: " + bankBranch.getBankBranchMaster().getBranchName());
		
		if(bankBranchList.stream().anyMatch(bb->bb.getAccountNumber().equalsIgnoreCase(bankBranch.getAccountNumber()))) {
			AppUtil.addWarn("Given Account Number Already Added");
		}else {
			bankBranchList.add(bankBranch);
			bankAccount = new EntityBankBranch();
			selectedBank = null;
			seletedBankBranchMaster = null;
		}
		log.info("<<====  addAccount === ##ENDS  >>");
	}

	public void deleteAccount(EntityBankBranch bankBranch) {
		log.info("<<==  deleteAccount ===>>");
		bankBranchList.remove(bankBranch);
		deletebankBranchList.add(bankBranch);
		log.info("<<=== deleteAccount -- ends ===>>");
	}

	public String handleFileUpload(FileUploadEvent event) {
		log.info("<<===  File Upload method called");
		try {
			billcopyFile = event.getFile();
			String fileType = billcopyFile.getFileName();
			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
				return null;
			}
			long size = billcopyFile.getSize();
			// size = size / (1024*1024);
			if ((size / (1024 * 1024)) > 2) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
				return null;
			}
			if (size > 0) {
				String billcopyPathName = commonDataService.getAppKeyValue("PROFILE_MASTER_BILLCOPY");
				String filePath = Util.fileUpload(billcopyPathName, billcopyFile);
				profileMaster.setBillcopyFilePath(filePath);
				if (!filePath.isEmpty()) {
					String[] names = filePath.split("/");
					billCopyFileName = names[names.length - 1];
				}
				errorMap.notify(ErrorDescription.PROFILE_BIIL_COPY_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Relieving Document is uploaded successfully");
			}

		} catch (Exception e) {
			log.info("<<==  Error in file upload " + e);
		}
		return null;
	}

	public String handleAgreementFile(FileUploadEvent event) {
		log.info("<<=== File Upload method called");
		try {
			agreementcopyFile = event.getFile();
			String fileType = agreementcopyFile.getFileName();
			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
				return null;
			}
			long size = agreementcopyFile.getSize();
			log.info("<<==  size::  " + size);
			// size = size / (1024*1024);
			if ((size / (1024 * 1024)) > 2) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
				return null;
			}
			if (size > 0) {
				String billcopyPathName = commonDataService.getAppKeyValue("PROFILE_MASTER_BILLCOPY");
				String filePath = Util.fileUpload(billcopyPathName, agreementcopyFile);
				profileMaster.setAgreementcopyFilePath(filePath);
				if (!filePath.isEmpty()) {
					String[] names = filePath.split("/");
					agreementCopyFileName = names[names.length - 1];
				}
				errorMap.notify(ErrorDescription.PROFILE_LEASE_AGREEMENT_COPY_SUCCESS.getErrorCode());
				log.info(" Relieving Document is uploaded successfully");
			}

		} catch (Exception e) {
			log.error("<<=== Error in upload agreement file");
		}
		return null;
	}

	public String create() {
		log.info("<<===  ProfileMasterBean -- create method ##STARTS=====>>");
		log.info("<<===   bankBranchList :: " + bankBranchList.size());
		profileMaster.setBankList(new ArrayList<>());
		if (profileMaster.getAreaSize() == null || profileMaster.getAreaSize() < 1) {
			errorMap.notify(ErrorDescription.PROFILE_OFFICE_AREA_REQUIRED.getErrorCode());
			return null;
		}
		if (profileMaster.getAddressMaster() == null || profileMaster.getAddressMaster().getStateMaster() == null) {
			errorMap.notify(ErrorDescription.ADDRESS_REQUIRED.getErrorCode());
			return null;
		}
		if (bankBranchList == null || bankBranchList.size() < 1) {
			errorMap.notify(ErrorDescription.PROFILE_BANK_REQUIRED.getErrorCode());
			return null;
		}

		Integer n = validateBuildingType(profileMaster);
		if (n != 0) {
			errorMap.notify(n);
			return null;
		}
		
		//For card payment bank details
		EntityBankBranch entityBankBranch = new EntityBankBranch();
		entityBankBranch.setBankBranchMaster(bankBranchMaster);
		entityBankBranch.setBankAccountType(bankAccountType);
		entityBankBranch.setAccountNumber(accountNumber);
		entityBankBranch.setGlAccount(glAccount);
		entityBankBranch.setPaymentFor("CARD");
//		bankBranchList.add(entityBankBranch);
		log.info("bankBranch Bean List size-------- "+bankBranchList.size());

		profileMaster.getBankList().addAll(bankBranchList);
		profileMaster.getBankList().add(entityBankBranch);
		profileMaster.setDeleteBankList(new ArrayList<>());
		profileMaster.getDeleteBankList().addAll(deletebankBranchList!=null?deletebankBranchList:null);
		log.info("bankBranch DTO List size-------- "+profileMaster.getBankList().size());
		String url = serverURL;
		if (action.equalsIgnoreCase("ADD"))
			url = url + "/profileMaster/create";
		if (action.equalsIgnoreCase("EDIT"))
			url = url + "/profileMaster/update";
		baseDTO = httpService.post(url, profileMaster);
		if (baseDTO.getStatusCode() != 0) {
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		} else {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.PROFILE_ADD_SUCCESSFULLY.getErrorCode());
			else
				errorMap.notify(ErrorDescription.PROFILE_UPDTAE_SUCCESSFULLY.getErrorCode());
		}
		log.info("<<===  ProfileMasterBean -- create method ##ENDS=====>>");
		return showListPage();
	}

	public void getProfileData(ProfileMasterSearchResponseDTO profileData) {
		log.info("<<======  getProfileData ---==##STARTS ");
		try {
			bankMaster = new BankMaster();
			bankBranchMaster = new BankBranchMaster();
			bankAccountType = new BankAccountType();
			glAccount = new GlAccount();
			accountNumber = null;
			bankBranchList = new ArrayList<>();
			String url = serverURL + "/profileMaster/get/" + profileData.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				// profileMaster = (EntityMasterDTO) baseDTO.getResponseContent();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				profileMaster = mapper.readValue(jsonResponse, new TypeReference<EntityMasterDTO>() {
				});
				
				if(profileMaster.getBankList()!=null && !profileMaster.getBankList().isEmpty()) {
					for(EntityBankBranch bankBranch:profileMaster.getBankList()) {
						if(bankBranch.getPaymentFor()!=null && bankBranch.getPaymentFor().equals("CARD")) {
							bankMaster = bankBranch.getBankBranchMaster().getBankMaster();
							bankBranchMaster = bankBranch.getBankBranchMaster();
							glAccount = bankBranch.getGlAccount();
							accountNumber = bankBranch.getAccountNumber();
							bankAccountType = bankBranch.getBankAccountType();
							bankChange();
						}else {
							bankBranchList.add(bankBranch);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("<<=== Error while load profile data :: " + e);
		}
		log.info("<<======  getProfileData ---==##ENDS ");
	}

	public void list() {
		profileLazyList = new LazyDataModel<ProfileMasterSearchResponseDTO>() {

			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<ProfileMasterSearchResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				List<ProfileMasterSearchResponseDTO> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<ProfileMasterSearchResponseDTO>>() {
								});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(ProfileMasterSearchResponseDTO res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public ProfileMasterSearchResponseDTO getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<ProfileMasterSearchResponseDTO> responseList = (List<ProfileMasterSearchResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (ProfileMasterSearchResponseDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};
		// return LIST_PAGE;
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		responseDto = new ProfileMasterSearchResponseDTO();

		BaseDTO baseDTO = new BaseDTO();

		ProfileMasterSearchRequestDTO request = new ProfileMasterSearchRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("groupName")) {
				request.setGroupName(value);
			} else if (key.equals("entityTypeCodeOrName")) {
				request.setEntityTypeCodeOrName(value);
			} else if (key.equals("entityCodeOrName")) {
				request.setEntityCodeOrName(value);
			} else if (key.equals("regionCodeOrName")) {
				request.setRegionCodeOrName(value);
			} else if (key.equals("gstNumber")) {
				request.setGstInNumber(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equalsIgnoreCase("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {

			String url = serverURL + "/profileMaster/search";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Profile Master onRowSelect method started");
		responseDto = ((ProfileMasterSearchResponseDTO) event.getObject());
		addButtonFlag = false;
	}

	public void bankClear() {
		log.info("<<====  bankClear ===>>");
		bankAccount = new EntityBankBranch();
		loadBankMaster();
		loadGlAccount();
		loadAccountType();
	}

	public String deleteProfile() {
		log.info("<<====   Delete Profile Master ====" + responseDto);
		if (responseDto == null || responseDto.getId() == null) {
			errorMap.notify(ErrorDescription.PROFILE_SELECT_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			String url = serverURL + "/profileMaster/delete/" + responseDto.getId();
			baseDTO = httpService.delete(url);
			if (baseDTO.getStatusCode() != 0) {
				errorMap.notify(ErrorDescription.PROFILE_CAN_NOT_DELETE.getErrorCode());
			} else {
				errorMap.notify(ErrorDescription.PROFILE_DELETE_SUCCESSFULLY.getErrorCode());
			}
		} catch (Exception e) {
			log.info("<<===  Error in delete profile master ===>>" + e);
		}

		return showListPage();
	}

	public Integer validateBuildingType(EntityMasterDTO dto) {
		log.info("<<===  validate building type=====>>");
		if (profileMaster.getBuildingType() != null) {
			if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Rental")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				if (profileMaster.getRentalAmount() == null) {
					// errorMap.notify(ErrorDescription.PROFILE_MONTHLY_RENT_RQUIRED.getErrorCode());
					return ErrorDescription.PROFILE_MONTHLY_RENT_RQUIRED.getErrorCode();
				}
				if (profileMaster.getRentalPayDate() == null) {
					// errorMap.notify(ErrorDescription.PROFILE_RENT_DATE_REQUIRED.getErrorCode());
					return ErrorDescription.PROFILE_RENT_DATE_REQUIRED.getErrorCode();
				}
				profileMaster.setLeaseAmount(null);
				profileMaster.setLeaseFromDate(null);
				profileMaster.setLeaseToDate(null);
				profileMaster.setAgreementcopyFilePath(null);
			} else if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Own")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				profileMaster.setLeaseAmount(null);
				profileMaster.setLeaseFromDate(null);
				profileMaster.setLeaseToDate(null);
				profileMaster.setRentalAmount(null);
				profileMaster.setRentalPayDate(null);
				profileMaster.setAgreementcopyFilePath(null);
			} else if (profileMaster.getBuildingType().getName().equalsIgnoreCase("Lease")) {
				log.info("<<==== " + profileMaster.getBuildingType().getName());
				if (profileMaster.getLeaseAmount() == null) {
					// errorMap.notify(ErrorDescription.PROFILE_LEASE_AMOUNT_REQUIRED.getErrorCode());
					return ErrorDescription.PROFILE_LEASE_AMOUNT_REQUIRED.getErrorCode();
				}
				if (profileMaster.getLeaseFromDate() == null) {
					// errorMap.notify(ErrorDescription.PROFILE_LEASE_FROM_DATE_REQUIRED.getErrorCode());
					return ErrorDescription.PROFILE_LEASE_FROM_DATE_REQUIRED.getErrorCode();
				}
				if (profileMaster.getLeaseToDate() == null) {
					// errorMap.notify(ErrorDescription.PROFILE_LEASE_TO_DATE_REQUIRED.getErrorCode());
					return ErrorDescription.PROFILE_LEASE_TO_DATE_REQUIRED.getErrorCode();
				}
				profileMaster.setRentalAmount(null);
				profileMaster.setRentalPayDate(null);
				profileMaster.setBillcopyFilePath(null);
			}
		}
		return 0;
	}

	public String getEmployeeName(Long userId) {
		log.info("<<===  getEmployeeName ====## STARTED");
		EmployeeMaster employee = commonDataService.loadEmployeeByUser(userId);
		log.info("<<====  getEmployeeName >>>>  === " + employee);
		createdeName = employee.getFirstName() + " " + employee.getLastName();
		return createdeName;
	}

}
