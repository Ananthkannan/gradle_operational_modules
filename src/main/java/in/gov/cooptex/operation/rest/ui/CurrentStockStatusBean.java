package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CurrentStockStatusRequest;
import in.gov.cooptex.core.dto.CurrentStockStatusResponse;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.MisPdfUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("currentStockStatusBean")
@Scope("session")
public class CurrentStockStatusBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/stock/currentStockStatus.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	CurrentStockStatusRequest currentStockStatusRequest;

	String SERVER_URL;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<CurrentStockStatusResponse> currentStockStatusList;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Setter
	@Getter
	private StreamedContent file, pdfFile;

	MisPdfUtil misPdfUtil = new MisPdfUtil();

	@Getter
	@Setter
	String downloadFileName = "";

	@Getter
	@Setter
	String downloadPath;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private String openingBalance = "";

	@Getter
	@Setter
	private String builedQty = "";

	@Getter
	@Setter
	private String dispatchQty = "";

	@Getter
	@Setter
	private String receivedQty = "";

	@Getter
	@Setter
	private String closingingBalance = "";

	@Getter
	@Setter
	private String stockClosingValue = "";

	@Getter
	@Setter
	private String returnQty = "";

	@Getter
	@Setter
	List<String> totalList = new ArrayList<String>();

	@Setter
	@Getter
	List<EntityMaster> showRoomEntityMasterList;

	@Setter
	@Getter
	Long showRoomEntityId;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String qrCodeGeneratePath;

	@Getter
	@Setter
	Boolean downloadBtnFlag = false, receiptDownloadBtnFlag = false;

	@Getter
	@Setter
	Boolean headOfficeEntityTypeFlag = false, regEntityTypeFlag = false, wareHouseEntityTypeFlag = false;

	@Getter
	@Setter
	private EntityTypeMaster loginEntityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	private EntityMaster loginEntityMaster = new EntityMaster();

	@Getter
	@Setter
	String receiptQrCodeGeneratePath;

	/**
	 * Size of a byte buffer to read/write file
	 */
	private static final int BUFFER_SIZE = 4096;
	
	@Getter
	@Setter
	private Boolean validEntityTypeToGenerateQRCode;
	@Getter
	@Setter
	private Boolean validEntityTypeToGenerateReciptQRCode;

	@PostConstruct
	public void init() {
		log.info("init is called.");
	}

	public String gotoPage() {

		SERVER_URL = stockTransferUtility.loadServerUrl();

		currentStockStatusRequest = new CurrentStockStatusRequest();
		currentStockStatusRequest.setStockDate(new Date());

		currentStockStatusList = new ArrayList<>();

		log.info(
				"work location id==> " + loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
		log.info("work location name==> "
				+ loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getName());
		log.info("work location name==> " + loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation()
				.getEntityTypeMaster().getEntityName());

		EntityTypeMaster entityTypeMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation()
				.getEntityTypeMaster();
		if (entityTypeMaster.getEntityCode().equals("HEAD_OFFICE")) {
			entityTypeMasterList = commonDataService.findAllEntityTypes();
			headOfficeEntityTypeFlag = true;
			wareHouseEntityTypeFlag = false;
			regEntityTypeFlag = false;
			validEntityTypeToGenerateQRCode=false;
			validEntityTypeToGenerateReciptQRCode=false;
		} else if (entityTypeMaster.getEntityCode().equals("REGIONAL_OFFICE")) {
			loginEntityTypeMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation()
					.getEntityTypeMaster();
			Long regId = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
			log.info("Region office :: entity regId==> " + regId);
			currentStockStatusRequest.setEntityTypeCode(loginEntityTypeMaster.getEntityCode());
			// onChangeEntityTypeMaster();
			loadEntityTypeByRegionId(regId);
			headOfficeEntityTypeFlag = false;
			wareHouseEntityTypeFlag = false;
			regEntityTypeFlag = true;
			validEntityTypeToGenerateQRCode=false;
			validEntityTypeToGenerateReciptQRCode=false;
		} else {
			/*
			 * String entityTypeCode =
			 * loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().
			 * getEntityTypeMaster().getEntityCode();
			 * log.info("entityTypeCode==> "+entityTypeCode);
			 */
			loginEntityTypeMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation()
					.getEntityTypeMaster();
			log.info("entityTypeCode==> " + loginEntityTypeMaster.getEntityCode());
			currentStockStatusRequest.setEntityTypeCode(loginEntityTypeMaster.getEntityCode());

			loginEntityMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();
			log.info("Warehouse :: entityId==> " + loginEntityMaster.getId());
			currentStockStatusRequest.setEntityId(loginEntityMaster.getId());
			entityMasterList=new ArrayList<EntityMaster>();
			entityTypeMasterList=new ArrayList<EntityTypeMaster>();
			entityTypeMasterList.add(loginEntityTypeMaster);
			entityMasterList.add(loginEntityMaster);
			headOfficeEntityTypeFlag = false;
			wareHouseEntityTypeFlag = true;
			regEntityTypeFlag = false;
			validEntityTypeToGenerateQRCode=true;
			validEntityTypeToGenerateReciptQRCode=true;
		}

		productCategoryList = commonDataService.loadProductCategories();
		qrCodeGeneratePath = null;
		downloadBtnFlag = false;
		openingBalance = null;
		receivedQty = null;
		dispatchQty = null;
		builedQty = null;
		returnQty = null;
		closingingBalance = null;
		stockClosingValue = null;
		return LIST_PAGE_URL;
	}

	public void loadEntityTypeByRegionId(Long regionId) {
		log.info("<<<< ----------Starts :: loadEntityTypeByRegionId------- >>>>");
		try {
			log.info("regionId ==> " + regionId);

			String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/" + regionId;
			log.info("loadEntityTypeByRegionId :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
				});
				if (entityTypeMasterList != null && !entityTypeMasterList.isEmpty()) {
					log.info("entityTypeMasterList size==> " + entityTypeMasterList.size());
				} else {
					log.error("entityTypeMasterList null or empty");
				}

			}

		} catch (Exception exp) {
			log.error("loadEntityTypeByRegionId :: Exception ==> ", exp);
		}

		log.info("<<<< ----------End filternig based on Head And Regional Office------- >>>>");
	}

	/*
	 * public void onEntityTypeChange(Long entityId) {
	 * log.info(":: Entity type change ::"); try { String url =
	 * AppUtil.getPortalServerURL() + "/entitymaster/getall/entity/" + entityId ;
	 * log.info("onEntityTypeChange :: url==> " + url); BaseDTO baseDto =
	 * httpService.get(url); ObjectMapper mapper = new ObjectMapper(); String
	 * jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
	 * entityMasterList = mapper.readValue(jsonResponse, new
	 * TypeReference<List<EntityMaster>>() { }); if(entityMasterList !=null) {
	 * log.info("onEntityTypeChange :: entityMasterList size==> " +
	 * entityMasterList.size()); }else {
	 * log.error("entityMasterList not found==> "); }
	 * 
	 * } catch (Exception e) { log.error(":: Exception while Entity type change ::",
	 * e); errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode()); } }
	 */

	public void onChangeEntityTypeMaster() {
		log.info("onChangeEntityTypeMaster is called [" + currentStockStatusRequest + "]");
		headOfficeEntityTypeFlag = false;
		wareHouseEntityTypeFlag = false;
		regEntityTypeFlag = true;
		/*
		 * if (currentStockStatusRequest != null &&
		 * !currentStockStatusRequest.getEntityTypeCode().equalsIgnoreCase("SHOW_ROOM"))
		 * {
		 */
		EntityTypeMaster entityTypeMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation()
				.getEntityTypeMaster();
		if (entityTypeMaster.getEntityCode().equals("HEAD_OFFICE")) {
			entityMasterList = commonDataService.findEntityByEntityType(currentStockStatusRequest.getEntityTypeCode());
		} else if (entityTypeMaster.getEntityCode().equals("REGIONAL_OFFICE")) {
			Long regId = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
			log.info("Region office :: entity regId==> " + regId);
			entityMasterList = commonDataService
					.findEntityByEntityTypeAndRegion(currentStockStatusRequest.getEntityTypeCode(), regId);
		}
		/*
		 * } else if (currentStockStatusRequest.getEntityTypeCode() != null &&
		 * currentStockStatusRequest.getEntityTypeCode().equalsIgnoreCase("SHOW_ROOM"))
		 * { showRoomEntityMasterList = commonDataService.loadAllRegionalOffices(); }
		 */ else {
			entityMasterList = null;
		}

	}

	public void onChangeEntityMaster() {
		log.info("onChangeEntityMaster is called [" + currentStockStatusRequest + "]");

		if (currentStockStatusRequest != null) {
			log.info("Selected Entity Master ID [" + currentStockStatusRequest.getEntityId() + "] ");
		}
	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductCategory() != null) {
			log.info("load productGroups with productCategory  "
					+ currentStockStatusRequest.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(currentStockStatusRequest.getProductCategory());
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductVarieties() {
		log.info("Inside processProductVarieties()>>>>>>>>>");
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductGroupMaster() != null) {
			log.info("load ProductVarieties with productGroupMaster  "
					+ currentStockStatusRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(currentStockStatusRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void onSearch() {
		log.info("onSearch is called [" + currentStockStatusRequest + "]");
		try {
			DecimalFormat df = new DecimalFormat("#,###,##0.00");
			currentStockStatusList = commonDataService.loadCurrentStockStatus(currentStockStatusRequest);
			int currentStockStatusListSize = currentStockStatusList == null ? 0 : currentStockStatusList.size();
			if (currentStockStatusListSize > 0) {
				Double ob = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(openbal -> openbal.getOpeningBalance()));
				openingBalance = df.format(ob);
				Double recqty = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(receivedqty -> receivedqty.getReceivedQty()));
				receivedQty = df.format(recqty);
				Double dq = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(dispatchQty -> dispatchQty.getDispatchedQty()));
				dispatchQty = df.format(dq);
				Double bq = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(builledQty -> builledQty.getBilledQty()));
				builedQty = df.format(bq);
				Double rq = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(returnQty -> returnQty.getReturnedQty()));
				returnQty = df.format(rq);
				Double cb = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(closebal -> closebal.getClosingBalance()));
				closingingBalance = df.format(cb);
				Double scv = currentStockStatusList.stream()
						.collect(Collectors.summingDouble(stock -> stock.getStockClosingValue()));
				stockClosingValue = df.format(scv);
				List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();
				for (CurrentStockStatusResponse currentStockStatusResponse : currentStockStatusList) {
					Map<String, Object> currentStockStatus = new LinkedHashMap<String, Object>();
					currentStockStatus.put("Product Category Code / Name",
							currentStockStatusResponse.getProductCategoryCode() + "/"
									+ currentStockStatusResponse.getProductCategoryName());
					currentStockStatus.put("Product Group Code / Name", currentStockStatusResponse.getProductGroupCode()
							+ "/" + currentStockStatusResponse.getProductGroupName());
					currentStockStatus.put("Product Variety Code / Name",
							currentStockStatusResponse.getProductVarietyCode() + "/"
									+ currentStockStatusResponse.getProductVarietyName());
					currentStockStatus.put("UOM", currentStockStatusResponse.getUomCode());
					currentStockStatus.put("Opening Stock", currentStockStatusResponse.getOpeningBalance());
					currentStockStatus.put("Stock Inward", currentStockStatusResponse.getReceivedQty());
					currentStockStatus.put("Stock Outward", currentStockStatusResponse.getDispatchedQty());
					currentStockStatus.put("Stock Sold", currentStockStatusResponse.getBilledQty());
					currentStockStatus.put("Stock Return", currentStockStatusResponse.getReturnedQty());
					currentStockStatus.put("Closing Stock", currentStockStatusResponse.getClosingBalance());
					currentStockStatus.put("Closing Stock Value", currentStockStatusResponse.getStockClosingValue());
					response.add(currentStockStatus);
				}

				totalList = new ArrayList<String>();
				totalList.add(openingBalance);
				totalList.add(receivedQty);
				totalList.add(dispatchQty);
				totalList.add(builedQty);
				totalList.add(returnQty);
				totalList.add(closingingBalance);
				totalList.add(stockClosingValue);
				generatePDF(response);
			}
		} catch (Exception e) {
			log.error("Inside Exception", e);
		}
	}

	public String onClear() {
		log.info("Clearing all search criteria");
		currentStockStatusList = new ArrayList<>();
		currentStockStatusRequest = new CurrentStockStatusRequest();
		currentStockStatusRequest.setStockDate(null);
		currentStockStatusRequest.setEntityId(null);
		showRoomEntityId = null;
		totalList = new ArrayList<String>();
		openingBalance = "";
		builedQty = "";
		dispatchQty = "";
		receivedQty = "";
		closingingBalance = "";
		stockClosingValue = "";
		returnQty = "";
		qrCodeGeneratePath = null;
		downloadBtnFlag = false;
		return LIST_PAGE_URL;
	}

	/***
	 * QRCodeGenreration For OpeningStock and download the qrcode generated file.
	 */
	public void generateQRCodeByStockStatus() {
		log.info(" Start of generateQRCodeByStockStatus");
		file = null;
		qrCodeGeneratePath = null;
		downloadBtnFlag = false;
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('statusDialog').show()");
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/status/generateQRCode";
			log.info("Server URL ::: " + URL);
			if (currentStockStatusRequest != null) {
				log.info("Entity ID :: " + currentStockStatusRequest.getEntityId());
				log.info("QRCode Generate Date :: " + currentStockStatusRequest.getStockDate());
				if (currentStockStatusRequest.getEntityId() == null) {
					AppUtil.addWarn("Entity master not available.");
				}
				if (currentStockStatusRequest.getStockDate() == null) {
					AppUtil.addWarn("Enter the stock date");
				}
			}

			BaseDTO response = httpService.post(URL, currentStockStatusRequest);
			if (response != null && response.getStatusCode() == 0) {
				log.info("QRCode successfully generated..");
				qrCodeGeneratePath = response.getDowloadUrlPath();
				log.info("downloadfile" + qrCodeGeneratePath);
				// downloadfile(qrCodeGeneratePath);
				AppUtil.addInfo("QRCode generated successfully");
				context.execute("PF('statusDialog').hide()");
				context.update("growls");
				downloadBtnFlag = true;
			} else {
				log.info("QRCode generate failed");
				AppUtil.addWarn("QRCode generation failed");
				context.execute("PF('statusDialog').hide()");
				downloadBtnFlag = false;
			}
		} catch (Exception e) {
			log.error("Exception at generateQRCodeByStockStatus", e);
			AppUtil.addWarn("QRCode generation failed");
			downloadBtnFlag = false;
		}
		log.info("End of generateQRCodeByStockStatus");
	}

	public void downloadfile() {
		InputStream input = null;
		log.info(" Start of downloadfile" + qrCodeGeneratePath);
		try {
			File file = new File(qrCodeGeneratePath);
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException------- " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception at downloadfile() " + e.getMessage());
		}
	}

	public void receiptDownloadfile() {
		InputStream input = null;
		log.info(" Start of downloadfile" + receiptQrCodeGeneratePath);
		try {
			File file = new File(receiptQrCodeGeneratePath);
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException------- ", e);
		} catch (Exception e) {
			log.error("Exception at downloadfile() ", e);
		}
	}

	public List<ProductVarietyMaster> productVarietyAutocomplete(String query) {
		log.info("Start productVarietyAutocomplete Autocomplete query==>" + query);
		try {
			currentStockStatusRequest.setCodeName(query);
			productList = new ArrayList<ProductVarietyMaster>();
			if (query != null) {
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/status/getvarity";
				log.info("Server URL ::: " + URL);
				BaseDTO response = httpService.post(URL, currentStockStatusRequest);
				if (response != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					productList = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyMaster>>() {
					});
				}
				log.info("list size" + productList.size());
			}

		} catch (Exception ex) {
			log.error("exception in productVarietyAutocomplete method------- " + ex);
		}
		log.info("End productVarietyAutocomplete Autocomplete");
		return productList;
	}

	private void generatePDF(List<Map<String, Object>> response) {
		log.info("===========START CurrentStockStatusBean.generatePDF =============");
		try {
			log.info("===========response is=============" + response.toString());
			String content = commonDataService.getTemplateDetailsforPortal(TemplateCode.REPORT_TEMPLATE.toString(),
					appPreference);
			String finalContent = VelocityStringUtil.getFinalContent("", content, null);
			String htmlData = generateHtml(currentStockStatusRequest, response, finalContent);
			downloadPath = commonDataService.getAppKeyValueforPortal(AppConfigEnum.REPORT_PDF_DOWNLOAD_PATH.toString());
			log.info("===========downloadPath is=============" + downloadPath.toString());
			downloadFileName = AppUtil.getReportFileName(ReportNames.CURRENT_STOCK_STATUS_REPORT_FILE, "pdf");
			misPdfUtil.createPDF(htmlData, downloadPath + "/" + downloadFileName, loginBean);
		} catch (Exception e) {
			log.info("===========Exception Occured in CurrentStockStatusBean.generatePDF =============");
			log.info("Exception is =============" + e.getMessage());
			log.error("Exception", e);
		}
		log.info("===========END CurrentStockStatusBean.generatePDF =============");
	}

	private String generateHtml(CurrentStockStatusRequest currentStockStatusRequest, List<Map<String, Object>> response,
			String finalContent) {
		log.info("===========START CurrentStockStatusBean.generateHtml =============");
		try {
			Long index = (long) 0;
			Long footerIndex = (long) 0;
			DateFormat dateFormat = AppUtil.DATE_FORMAT;
			StringBuffer finalTableData = new StringBuffer(110);
			StringBuffer finalHeaderData = new StringBuffer(110);
			StringBuffer finalFooterData = new StringBuffer(110);
			EntityMaster entityMaster = new EntityMaster();
			EntityTypeMaster entityTypeMaster = new EntityTypeMaster();
			for (EntityMaster entity : entityMasterList) {
				if (entity.getId().equals(currentStockStatusRequest.getEntityId())) {
					entityMaster = entity;
					break;
				}

			}
			for (EntityTypeMaster etm : entityTypeMasterList) {
				if (etm.getEntityCode().equalsIgnoreCase(currentStockStatusRequest.getEntityTypeCode())) {
					entityTypeMaster = etm;
					break;
				}

			}
			String toDate = "";
			if (currentStockStatusRequest.getStockDate() != null) {
				toDate = dateFormat.format(currentStockStatusRequest.getStockDate());
			}
			String categoryCodeName = "";
			String varityCodeName = "";
			String groupCodeName = "";
			if (currentStockStatusRequest.getProductCategory() != null) {
				categoryCodeName = currentStockStatusRequest.getProductCategory().getProductCatCode() + "/"
						+ currentStockStatusRequest.getProductCategory().getProductCatName();
			}
			if (currentStockStatusRequest.getProductVarietyMaster() != null) {
				varityCodeName = currentStockStatusRequest.getProductVarietyMaster().getCode() + "/"
						+ currentStockStatusRequest.getProductVarietyMaster().getName();
			}
			if (currentStockStatusRequest.getProductGroupMaster() != null) {
				groupCodeName = currentStockStatusRequest.getProductGroupMaster().getCode() + "/"
						+ currentStockStatusRequest.getProductGroupMaster().getName();
			}
			String filterTypeData = "<tr> " + "<td style='width: 10%'>Entity Type Code / Name</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>"
					+ entityTypeMaster.getEntityCode() + "/" + entityTypeMaster.getEntityName() + "</b></td> "
					+ "<td style='width: 10%'>Entity Code / Name</td> " + "<td align='right' style='width: 1%'>:</td> "
					+ "<td style='width: 24%'><b>" + entityMaster.getCode() + "/" + entityMaster.getName()
					+ "</b></td>";

			filterTypeData = filterTypeData + "<td style='width: 10%'>Product Category Code / Name</td>"
					+ "<td align='right' style='width: 1%'>:</td> \" + \"<td style='width: 24%'><b>" + categoryCodeName
					+ "</b></td>" + "</tr>";

			filterTypeData = filterTypeData + "<tr>" + "<td style='width: 10%'>Product Group Code / Name</td>"
					+ "<td align='right' style='width: 1%'>:</td> \" + \"<td style='width: 24%'><b>" + groupCodeName
					+ "</b></td>";

			filterTypeData = filterTypeData + "<td style='width: 10%'>Product Variety Code / Name</td>"
					+ "<td align='right' style='width: 1%'>:</td> \" + \"<td style='width: 24%'><b>" + varityCodeName
					+ "</b></td>";

			filterTypeData = filterTypeData + "<td style='width: 10%'>To Date</td>"
					+ "<td align='right' style='width: 1%'>:</td> \" + \"<td style='width: 24%'><b>" + toDate
					+ "</b></td>" + "</tr>";

			Set<String> headerNames = new HashSet<String>();
			for (Map<String, Object> map : response) {
				StringBuffer list = new StringBuffer(110);
				index++;
				headerNames = map.keySet();
				String beginTr = "<tr> " + "<td  class='bor-left'>" + index + "</td>";
				list.append(beginTr);
				Iterator<String> setItr = headerNames.iterator();
				while (setItr.hasNext()) {
					String keyString = setItr.next();
					String td = new String();
					if (isDecimal(map.get(keyString))) {
						td = "<td class='text-right'>" + getFormattedValue(map.get(keyString)) + "</td>";
					} else {
						td = "<td class='text-left'>" + (map.get(keyString)) + "</td>";
					}
					list.append(td);

				}
				String endTr = "</tr>";
				list.append(endTr);
				finalTableData.append(list);
			}
			String headerData = "<th style='width: 3%' class='text-center bor-left'>#</th>";
			finalHeaderData.append(headerData);
			Iterator<String> setItr = headerNames.iterator();
			while (setItr.hasNext()) {
				String keyString = setItr.next();
				headerData = "<th>" + keyString + "</th>";
				finalHeaderData.append(headerData);
			}
			Iterator<String> footerItr = headerNames.iterator();
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Opening Stock")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + footerIndex + "'>Total</td> ";
					tfoot.append(tr);
					for (String totalCount : totalList) {
						String td = new String();

						td = "<td class='text-center'>" + totalCount + "</td>";

						tfoot.append(td);
					}
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}

			}

			finalContent = finalContent.replace("$filterType", filterTypeData);
			finalContent = finalContent.replace("$columnData", finalHeaderData.toString());
			finalContent = finalContent.replace("$reportListData", finalTableData.toString());
			finalContent = finalContent.replace("$reportName", ReportNames.CURRENT_STOCK_STATUS_REPORT_FILE);
			finalContent = finalContent.replace("$footerData", finalFooterData.toString());
			log.info("===========END CurrentStockStatusBean.generateHtml =============");

		} catch (Exception e) {
			log.info("===========Exception Occured in CurrentStockStatusBean.generateHtml =============");
			log.info("===========Exception is =============" + e.getMessage());
		}

		return finalContent;
	}

	/**
	 * @param obj
	 * @return
	 */
	private String getFormattedValue(Object obj) {
		String value = null;
		try {

			// if (isDecimal(String.valueOf(obj))) {
			value = String.format("%.2f", (double) obj);
			// } else {
			// return (String) obj;
			// }

		} catch (Exception ex) {
			value = obj != null ? String.valueOf(obj) : null;
		}
		return value;
	}

	/**
	 * @param value
	 * @return
	 */
	private boolean isDecimal(Object value) {
		boolean isDecimal = true;
		try {
			Double.parseDouble(String.valueOf(value));
		} catch (Exception ex) {
			isDecimal = false;
		}
		return isDecimal;
	}

	public void downloadPdf() {
		InputStream input = null;
		try {
			File files = new File(downloadPath + "/" + downloadFileName);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			pdfFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
					files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void loadShowRoomByRegionId() {
		log.info("<=============Start loadShowRoomByRegionId Method=========>");
		try {
			log.info("ShowRoom Region Id=" + showRoomEntityId);
			if (showRoomEntityId != null) {
				entityMasterList = commonDataService.loadShowroomListForRegion(showRoomEntityId);
			}
		} catch (Exception ex) {
			log.error("Error in loadShowRoomByRegionId Method", ex);
		}
		log.info("<=============Start loadShowRoomByRegionId Method=========>");
	}

	/***
	 * QRCodeGeneration For Receipt Stock and download the qrcode generated file.
	 */
	public void generateQRCodeByReceiptStock() {
		log.info(" Start of generateQRCodeByReceiptStock");
		file = null;
		receiptQrCodeGeneratePath = null;
		receiptDownloadBtnFlag = false;
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('statusDialog').show()");
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/status/generateReceiptQRCode";
			log.info("Server URL ::: " + URL);
			if (currentStockStatusRequest != null) {
				log.info("Entity ID :: " + currentStockStatusRequest.getEntityId());
				log.info("QRCode Generate Date :: " + currentStockStatusRequest.getStockDate());
				if (currentStockStatusRequest.getEntityId() == null) {
					AppUtil.addWarn("Entity master not available.");
				}
				if (currentStockStatusRequest.getStockDate() == null) {
					AppUtil.addWarn("Enter the stock date");
				}
				currentStockStatusRequest.setTransferType(StockTransferType.RECEIPT_UPLOADED.toString());
			}

			BaseDTO response = httpService.post(URL, currentStockStatusRequest);
			if (response != null && response.getStatusCode() == 0) {
				log.info("QRCode successfully generated..");
				receiptQrCodeGeneratePath = response.getDowloadUrlPath();
				log.info("downloadfile" + qrCodeGeneratePath);
				// downloadfile(qrCodeGeneratePath);
				AppUtil.addInfo("Receipt QRCode generated successfully");
				context.execute("PF('statusDialog').hide()");
				context.update("growls");
				receiptDownloadBtnFlag = true;
			} else {
				log.info("Receipt QRCode generate failed");
				AppUtil.addWarn("Receipt QRCode generation failed");
				context.execute("PF('statusDialog').hide()");
				receiptDownloadBtnFlag = false;
			}
		} catch (Exception e) {
			log.error("Exception at generateQRCodeByReceiptStock", e);
			AppUtil.addWarn("QRCode generation failed");
			receiptDownloadBtnFlag = false;
		}
		log.info("End of generateQRCodeByReceiptStock");
	}

//========================================================================================================================
	
	@Getter
	@Setter
	List<CurrentStockStatusResponse> currentStockStatusViewList;
	
	public String viewStockStatusDetails(CurrentStockStatusResponse response) {
		log.info("Start the viewStockStatusDetails");
		log.info(" View StockCurrentStatus ::: ProductVarietyId -->>> " + response.getProductVarietyId());
		if (currentStockStatusRequest == null) {
			AppUtil.addError("Please select any param");
		}
		currentStockStatusRequest.setProductCategoryCodeName(
				response.getProductCategoryCode() + " / " + response.getProductCategoryName());
		currentStockStatusRequest
				.setProductGroupCodeName(response.getProductGroupCode() + " / " + response.getProductGroupName());
		currentStockStatusRequest
				.setProductVarietyCodeName(response.getProductVarietyCode() + " / " + response.getProductVarietyName());
		currentStockStatusRequest.setUnitRate(response.getUnitRate());
		currentStockStatusRequest.setProductVarietyMaster(new ProductVarietyMaster(response.getProductVarietyId(), "", ""));
		currentStockStatusViewList = viewStockStatusByProductId(currentStockStatusRequest);
		if (currentStockStatusViewList != null && currentStockStatusViewList.size() > 0) {
			log.info("Stock Status View List... SIZE -->>> " + currentStockStatusViewList.size());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('stockStatusByProduct').show();");
		} else {
			log.info("Stock Status List Is Empty");
		}
		log.info("End the viewStockStatusDetails");
		return null;
	}
	
	
	
	public List<CurrentStockStatusResponse> viewStockStatusByProductId(CurrentStockStatusRequest currentStockStatusRequest) {

		log.info("loadCurrentStockStatus [" + currentStockStatusRequest + "]");

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/stock/status/viewstockstatusbyproductid";

		List<CurrentStockStatusResponse> currentStockStatusList = new ArrayList<>();
		BaseDTO baseDto=new BaseDTO();
 		try {
 			baseDto = httpService.post(URL, currentStockStatusRequest);

			if (baseDto != null) {

				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());

				currentStockStatusList = mapper.readValue(jsonResponse,
						new TypeReference<List<CurrentStockStatusResponse>>() {
						});

				if (currentStockStatusList == null) {
					log.info("currentStockStatusList is empty");
				} else {
					log.info("currentStockStatusList " + currentStockStatusList.size());
				}
			} else {
				throw new RestException(ErrorDescription.FAILURE_RESPONSE);
			}

		} catch (Exception e) {
			log.error("Error in loadCurrentStockStatus ", e);
		}
		return currentStockStatusList;
	}
	
	
}
