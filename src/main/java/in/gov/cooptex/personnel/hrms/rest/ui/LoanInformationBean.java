package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.asset.model.ModernizationRequest;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.KntRegistrationUploadDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SalaryDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.LoanInformationDTO;
import in.gov.cooptex.dto.pos.LoanInformationScheduleDTO;
import in.gov.cooptex.dto.pos.ModernizationRequestDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.operation.enums.PayAspect;
import in.gov.cooptex.personnel.hrms.loans.model.EmpLoanDisbursement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("loanInformationBean")
public class LoanInformationBean {

	private final String LIST_PAGE = "/pages/personnelHR/listEmployeeLoanInformation.xhtml?faces-redirect=true;";
	
	private final String VIEW_PAGE = "/pages/personnelHR/viewEmployeeLoanInformation.xhtml?faces-redirect=true;";
	
	private final String LOAN_PRECLOSURE_PAGE = "/pages/personnelHR/requestForLoanPreclosure.xhtml?faces-redirect=true;";
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL()+"/emploandisbursement";
	
	@Autowired
	ErrorMap errorMap;
	
	ObjectMapper mapper;

	String url, jsonResponse;
	
	@Autowired
	HttpService httpService;
	
	@Getter
	@Setter
	LazyDataModel<LoanInformationDTO> lazyLoanInformationDTOList;
	
	@Getter
	@Setter
	List<LoanInformationDTO> loanInformationDTOList;
	
	@Getter
	@Setter
	LoanInformationDTO selectedLoanInformationDTO = new LoanInformationDTO();
	
	@Getter
	@Setter
	LoanInformationDTO loanInformationDTO = new LoanInformationDTO();
	
	@Getter
	@Setter
	List<LoanInformationScheduleDTO> loanInformationScheduleDTOList = new ArrayList<>();
	
	@Getter
	@Setter
	Integer totalRecords = 0;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	EmployeeMaster loginEmployee = new EmployeeMaster();
	
	@Getter @Setter
	private List<PaymentMode> paymentModeList = new ArrayList<>();
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();
	
	@Getter @Setter
	Boolean ispreclosedAllowed=false;
	
	@PostConstruct
	public void init() {
		log.info("loanInformationBean Init is executed.....................");
		mapper = new ObjectMapper();
		
	}
	
	/**
	 * Purpose : to call the employee loan information screen
	 * @author krishnakumar
	 * @return
	 */
	public String listLoanInformation() {
		log.info("<=call loanInformationBean :: viewLoanDetails =>");
		try {
			loadEmployeeLoanDisbursementList();
		}catch (Exception e) {
			log.info("Exception occured while viewLoanDetails........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_PAGE;
	}
	
	/**
	 * Purpose : to list the employee loan information
	 * @author krishnakumar
	 */
	public void loadEmployeeLoanDisbursementList() {
		log.info("<==== Starts LoanInformationBean.loadEmpLoanDisbursementList =====>");
		lazyLoanInformationDTOList = new LazyDataModel<LoanInformationDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<LoanInformationDTO> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL + "/loademployeeloaninformation";
					log.info("loadEmployeeLoanDisbursementList url==> "+url);
					EmployeeMaster employeMaster = loginBean.getEmployee();
					if(employeMaster != null) {
						paginationRequest.setEmpId(employeMaster.getId());
					}
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						loanInformationDTOList = mapper.readValue(jsonResponse, new TypeReference<List<LoanInformationDTO>>() {});
						for (LoanInformationDTO loanInformationDTO : loanInformationDTOList) {
							if(loanInformationDTO.getBalanceAmount()!=null && loanInformationDTO.getBalanceAmount().doubleValue()==0) {
								loanInformationDTO.setCompletedTenure(Long.valueOf(loanInformationDTO.getTotalTenure()));
								loanInformationDTO.setRemainingTenure(0L);
							}
						}
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadEmployeeLoanDisbursementList :: totalRecords==> "+totalRecords);
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return loanInformationDTOList;
			}

			@Override
			public Object getRowKey(LoanInformationDTO res) {
				return res != null ? res.getLoanId() : null;
			}

			@Override
			public LoanInformationDTO getRowData(String rowKey) {
				for (LoanInformationDTO empLoanDisbursement : loanInformationDTOList) {
					if (empLoanDisbursement.getLoanId().equals(Long.valueOf(rowKey))) {
						selectedLoanInformationDTO = empLoanDisbursement;
						return empLoanDisbursement;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends LoanInformationBean.loadEmpLoanDisbursementList =====>");
	}
	
	/**
	 * Purpose : to view employee loan details
	 * @author krishnakumar
	 * @return
	 */
	public String viewLoanInformation() {
		log.info("<=call loanInformationBean :: viewLoanDetails =>");
		if (selectedLoanInformationDTO == null || selectedLoanInformationDTO.getLoanId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		loadEmployeeLoanInformation();
		loadEmployeeLoanInformationDetails();
		return VIEW_PAGE;
	}
	
	/**
	 * purpose : to load employee loan information
	 * @author krishnakumar
	 */
	@SuppressWarnings("unchecked")
	public void loadEmployeeLoanInformation(){
		log.info("<= call loanInformationBean :: loadEmployeeLoanInformation =>");
		try {
			
			log.info("loadEmployeeLoanInformation :: Loan Id==> "+selectedLoanInformationDTO.getLoanId());
			
			String getUrl = SERVER_URL + "/loademployeeloaninformationbyloanid/:id";
			url = getUrl.replace(":id", selectedLoanInformationDTO.getLoanId().toString());
			
			log.info("loadEmployeeLoanInformation :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				
				List<LoanInformationDTO> loanInformationDTOList = (List<LoanInformationDTO>) mapper.readValue(jsonValue,
						new TypeReference<List<LoanInformationDTO>>() {});
				
				log.info("loadEmployeeLoanInformation :: loanInformationDTOList size ===> " + loanInformationDTOList.size());
				if(loanInformationDTOList != null && !loanInformationDTOList.isEmpty()) {
					loanInformationDTO = loanInformationDTOList.get(0);
					if(loanInformationDTO.getBalanceAmount().doubleValue()==0) {
						loanInformationDTO.setCompletedTenure(Long.valueOf(loanInformationDTO.getTotalTenure()));
						loanInformationDTO.setRemainingTenure(0L);
					}
					log.info("loadEmployeeLoanInformation :: loanInformationDTO ===> " + loanInformationDTO);
				}else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.NO_LOAN_BALANCE_FOUND).getCode());
					return;
				}
			} else {
				log.error("loadEmployeeLoanInformation :: failer status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		}catch (Exception e) {
			log.info("Exception occured while viewLoanInformation........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
	}
	
	/**
	 * purpose : to load employee loan payment details
	 * @author krishnakumar
	 */
	@SuppressWarnings("unchecked")
	public void loadEmployeeLoanInformationDetails() {
		log.info("<= call loanInformationBean :: loadEmployeeLoanInformationDetails =>");
		try {
			
			log.info("loadEmployeeLoanInformationDetails :: Loan Id==> "+selectedLoanInformationDTO.getLoanId());
						
			String getUrl = SERVER_URL + "/loademploaninfodetailsbyloanid/:id";
			url = getUrl.replace(":id", selectedLoanInformationDTO.getLoanId().toString());
			
			log.info("viewLoanInformation URL called : - " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				
				loanInformationScheduleDTOList = (List<LoanInformationScheduleDTO>) mapper.readValue(jsonValue,
						new TypeReference<List<LoanInformationScheduleDTO>>() {});
				log.info("loadEmployeeLoanInformation :: LoanInformationScheduleDTOList size ===> " + loanInformationScheduleDTOList.size());
			} else {
				log.error("loadEmployeeLoanInformation :: failer status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		}catch (Exception e) {
			log.info("Exception occured while viewLoanInformation........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
	}
	
	public String backLoanInformation() {
		log.info("<=call loanInformationBean :: backLoanInformation =>");
		try {
			selectedLoanInformationDTO = new LoanInformationDTO();
			loanInformationDTO = new LoanInformationDTO();
			loanInformationScheduleDTOList = new ArrayList<>();
			loadEmployeeLoanDisbursementList();
		}catch (Exception e) {
			log.info("Exception occured while viewLoanDetails........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
	}
	
	public String loanPreclosureInformation() {
		log.info("loanInformationBean. loanPreclosureInformation() ");
		Double preclosurePenaltyPercentage = 0D;
		Double penaltyAmount = 0D;
		try {
			if (selectedLoanInformationDTO == null || selectedLoanInformationDTO.getLoanId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if(selectedLoanInformationDTO.getBalanceAmount()!=null && selectedLoanInformationDTO.getBalanceAmount() <= 0D) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.NO_LOAN_BALANCE_FOUND).getCode());
				return null;
			}
			
			if(checkAlreadyLoanPreclosured()) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.EMP_LOAN_ALREADY_PRECLOSURED).getCode());
				return null;
			}
			
			if(checkAlreadyPayrollRunCurrentMonth(loginBean.getEmployee().getId(),selectedLoanInformationDTO.getLoanId())) {
				AppUtil.addWarn("This Month Payroll is already run.Please preclose your loan for next month");
				return null;
			}
			if(ispreclosedAllowed) {
				AppUtil.addWarn("Preclose option is not available for this loan");
				return null;
			}
			
			loadEmployeeLoanInformation();
			loadEmployeeLoanInformationDetails();
			loginEmployee = loginBean.getEmployee();
			employeeMaster=getEmployeeDetails(loanInformationDTO.getLoanId());
			paymentModeList = commonDataService.loadPaymentModes();
			if(loanInformationDTO!=null && loanInformationDTO.getBalanceAmount()!=null && loanInformationDTO.getBalanceAmount() > 0) {
				String appValue = commonDataService.getAppKeyValueWithserverurl(AppConfigKey.LOAN_PRECLOSURE_PENALTY_PERCENTAGE,
						AppUtil.getPortalServerURL());
				preclosurePenaltyPercentage = StringUtils.isEmpty(appValue) ? 0D : Double.parseDouble(appValue);
				if(preclosurePenaltyPercentage != 0D) {
					penaltyAmount = loanInformationDTO.getBalanceAmount() * (preclosurePenaltyPercentage / 100);
					loanInformationDTO.setPreclosurePenaltyAmount(penaltyAmount);
				}else {
					loanInformationDTO.setPreclosurePenaltyAmount(0D);
				}
				loanInformationDTO.setPreclosureBalAmount(loanInformationDTO.getBalanceAmount());
			}else {
				loanInformationDTO.setPreclosurePenaltyAmount(0D);
				loanInformationDTO.setPreclosureBalAmount(0D);
			}
		}catch (Exception e) {
			log.info("Exception occured at loanPreclosureInformation() ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LOAN_PRECLOSURE_PAGE;
	}
	
	private boolean checkAlreadyLoanPreclosured() {
		/*
		 * Controller : EmpLoanDisbursementController.java
		 */
		boolean preclosuredFlag = true;
		try{
			if(selectedLoanInformationDTO != null && selectedLoanInformationDTO.getLoanId() != null) {
				String getUrl = SERVER_URL + "/checkalreadyloanpreclosured/"+selectedLoanInformationDTO.getLoanId();
				
				log.info("submitLoanPreclosure URL : " + getUrl);
				BaseDTO baseDTO = httpService.get(getUrl);
				if (baseDTO != null && (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode())) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					preclosuredFlag = mapper.readValue(jsonValue, Boolean.class);
				}
			}
		}catch(Exception e) {
			log.info("Exception occured at checkAlreadyLoanPreclosured() ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return preclosuredFlag;
	}
	
	public String submitLoanPreclosure() {
		log.info("loanInformationBean. submitLoanPreclosure() ");
		try {
			
			if (StringUtils.isEmpty(loanInformationDTO.getPayBy())) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.EMP_LOAN_PAY_BY_REQUIRED).getCode());
				return null;
			}else {
				if ("OTHERS".equals(loanInformationDTO.getPayBy())) {
					if(loanInformationDTO.getPreclosurePaymentMode() == null) {
						errorMap.notify(ErrorDescription.VOUCHER_PAYMENT_MODE_IS_REQUIRED.getCode());
						return null;
					}
				}
			}
			
			loanInformationDTO.setEmpId(loginEmployee == null ? null : loginEmployee.getId());
			/*
			 * Controller : EmpLoanDisbursementController.java
			 */
			
			String getUrl = SERVER_URL + "/submitloanpreclosure";
			
			log.info("submitLoanPreclosure URL : " + getUrl);
			BaseDTO baseDTO = httpService.post(getUrl,loanInformationDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}else {
				if (baseDTO.getStatusCode() == 0) {
					log.error("submitLoanPreclosure :: success status code " + baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.EMP_LOAN_PRECLOSURED_SUBMIT_SUCCESS).getCode());
				} else {
					log.error("submitLoanPreclosure :: failer status code " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		}catch(Exception e) {
			log.error("Exception at loanInformationBean. submitLoanPreclosure() ", e);
		}
		return backLoanInformation();
	}
	
	public EmployeeMaster getEmployeeDetails(Long loanId) {
		log.info("LoanPaymentBean::getEmployeeDetails Method Start");
		EmployeeMaster empMaster = new EmployeeMaster();
		try {
			if (loanId != null) {
				String saveUrl = SERVER_URL + "/getemployeedetails/" + loanId;
				log.info("==== Save Url is  ====  " + saveUrl);
				BaseDTO baseDTO = httpService.get(saveUrl);
				if (baseDTO.getStatusCode() == 0) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					empMaster = mapper.readValue(jsonValue, new TypeReference<EmployeeMaster>() {
					});
				}
			}
		} catch (Exception e) {
			log.info("Exception ", e);
		}
		log.info("LoanPaymentBean::getEmployeeDetails Method Start");
		return empMaster;
	}
	
	private boolean checkAlreadyPayrollRunCurrentMonth(Long empId,Long loanId) {
		/*
		 * Controller : EmpLoanDisbursementController.java
		 */
		boolean preclosuredFlag = false;
		ispreclosedAllowed=false;
		try{
			if(selectedLoanInformationDTO != null && selectedLoanInformationDTO.getLoanId() != null) {
				String getUrl = SERVER_URL + "/checkalreadypayrollrun/"+empId+"/"+loanId;
				
				log.info("submitLoanPreclosure URL : " + getUrl);
				BaseDTO baseDTO = httpService.get(getUrl);
				if (baseDTO != null && baseDTO.getResponseContent()!=null && baseDTO.getStatusCode()==0) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					preclosuredFlag = mapper.readValue(jsonValue, Boolean.class);
				}else if(baseDTO != null && baseDTO.getStatusCode()==2000){
					ispreclosedAllowed=true;
				}
			}
		}catch(Exception e) {
			log.info("Exception occured at checkAlreadyLoanPreclosured() ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return preclosuredFlag;
	} 
}
