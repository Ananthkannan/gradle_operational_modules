package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AllowanceGrade;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.GradewisePayConfigEnum;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("gradewisePayConfigBean")
public class GradewisePayConfigBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	final String ADD_PAGE = "/pages/masters/createGradewisePayConfig.xhtml?faces-redirect=true";
	final String LIST_PAGE = "/pages/masters/listGradewisePayConfig.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/viewGradewisePayConfig.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;
	
	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Getter
	@Setter
	String pageAction;
	
	@Getter @Setter
	LazyDataModel<AllowanceGrade> allowanceGradeLazyList;
	
	@Getter @Setter
	List<AllowanceGrade> allowanceGradeList;
	
	@Getter @Setter
	AllowanceGrade selectedAllowanceGrade = new AllowanceGrade();
	
	@Getter @Setter
	AllowanceGrade allowanceGrade = new AllowanceGrade();
	
	@Getter @Setter
	Integer totalRecords=0;
	
	@Getter @Setter
	Boolean isAddButton,isEditButton,isDeleteButton;
	
	@Getter @Setter
	List<GradewisePayConfigEnum> allowanceTypeList = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		log.info("GradewisePayConfigBean Init is executed.....................");
		mapper = new ObjectMapper();
		isAddButton=true;
		isEditButton=true;
		isDeleteButton=true;
		loadAllowanceType();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	public void loadAllowanceType() {
		log.info("GradewisePayConfigBean :: loadAllowanceType start");
		GradewisePayConfigEnum allowance[] = GradewisePayConfigEnum.values();
		for(GradewisePayConfigEnum allowanceType : allowance) {
			allowanceTypeList.add(allowanceType);
		}
		log.info("loadAllowanceType :: allowanceTypeList==> "+allowanceTypeList);
	}
	
	public String loadPage() {
		log.info("GradewisePayConfigBean :: loadPage start");
		try {
			if (pageAction.equalsIgnoreCase("LIST")) {
				loadLazyGradeConfigList();
				return LIST_PAGE;
			}else if (pageAction.equalsIgnoreCase("ADD")) {
				return ADD_PAGE;
			}else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedAllowanceGrade == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} 
				return getGradeById();
			}else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedAllowanceGrade == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} 
				return getGradeById();
				
			}else if (pageAction.equalsIgnoreCase("DELETE")) {
				if (selectedAllowanceGrade == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} 
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			}
		}catch(Exception e) {
			log.error("GradewisePayConfigBean :: loadPage exception=> ",e);
		}
		return null;
	}
	
	public String getGradeById() {
		log.info("<==== Starts GradewisePayConfigBean.getGradeById =====>");
		try {
			Long gradeId = selectedAllowanceGrade.getId();
			log.info("getGradeById :: gradeId==> " + gradeId);
			String url = SERVER_URL + "/gradewisepayconfig/getgradebyid/" + gradeId;
			log.info("getGradeById :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				//--to get increment value
				allowanceGrade = mapper.readValue(jsonResponse, AllowanceGrade.class);
				if(allowanceGrade != null) {
					log.info("getGradeById :: code==> "+allowanceGrade.getCode());
					log.info("getGradeById :: name==> "+allowanceGrade.getName());
					log.info("getGradeById :: Allowance Type==> "+allowanceGrade.getAllowanceType());
					log.info("getGradeById :: status==> "+allowanceGrade.getActiveStatus());
				}
				if(pageAction.equalsIgnoreCase("EDIT")) {
					return ADD_PAGE;
				}else if(pageAction.equalsIgnoreCase("VIEW")) {
					return VIEW_PAGE;
				}
			}
			
		}catch(Exception e) {
			log.error("GradewisePayConfigBean :: getGradeById exception=> ",e);
		}
		return null;
	}
	
	public void loadLazyGradeConfigList() {
		log.info("<==== Starts GradewisePayConfigBean.loadLazyGradeConfigList =====>");
		allowanceGradeLazyList = new LazyDataModel<AllowanceGrade>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<AllowanceGrade> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL + "/gradewisepayconfig/loadgradeconfiglist";
					log.info("loadLazyIncrementList :: url==> "+url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						allowanceGradeList = mapper.readValue(jsonResponse, new TypeReference<List<AllowanceGrade>>() {});
						log.info("loadLazyGradeConfigList :: allowanceGradeList size==> "+allowanceGradeList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyIncrementList :: totalRecords==> "+totalRecords);
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return allowanceGradeList;
			}

			@Override
			public Object getRowKey(AllowanceGrade res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AllowanceGrade getRowData(String rowKey) {
				for (AllowanceGrade allowanceGrade : allowanceGradeList) {
					if (allowanceGrade.getId().equals(Long.valueOf(rowKey))) {
						selectedAllowanceGrade = allowanceGrade;
						return allowanceGrade;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedAllowanceGrade = ((AllowanceGrade) event.getObject());
		isAddButton = false;
		isDeleteButton = true;
	}
	
	public String deleteGrade() {
		log.info("<==== Starts GradewisePayConfigBean.deleteGrade =====>");
		try {
			
			log.info("Selected grade  Id :: " + selectedAllowanceGrade.getId());
			String getUrl = SERVER_URL + "/gradewisepayconfig/delete/id/:id";
			String url = getUrl.replace(":id", selectedAllowanceGrade.getId().toString());
			log.info("delete grade url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Grade Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.GRADE_DELETED_SUCCESSFULLY).getCode());
				clearGradeWisePay();
				loadLazyGradeConfigList();
				return LIST_PAGE;
			} else {
				log.error(" Grade Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Delete Grade :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String saveOrUpdateGradeWisePay() {
		log.info("<==== Starts GradewisePayConfigBean.saveOrUpdateGradeWisePay =====>");
		try {
			if(allowanceGrade.getCode() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CODE_NOT_FOUND).getCode());
				return null;
			}
			if(allowanceGrade.getName() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.NAME_NOT_FOUND).getCode());
				return null;
			}
			if(allowanceGrade.getAllowanceType() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ALLOWANCE_TYPE_NOT_FOUND).getCode());
				return null;
			}
			if(allowanceGrade.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.STATUS_NOT_FOUND).getCode());
				return null;
			}
			String url = SERVER_URL + "/gradewisepayconfig/creategradewisepay";
			log.info("saveEmployeeIncrement :: url==> "+url);
			BaseDTO baseDTO = httpService.put(url, allowanceGrade);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clearGradeWisePay();
				loadLazyGradeConfigList();
				if(pageAction.equals("ADD")) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.GRADE_INSERTED_SUCCESSFULLY).getCode());
				}else if(pageAction.equals("EDIT")) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.GRADE_UPDATED_SUCCESSFULLY).getCode());
				}
				return LIST_PAGE;
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
			
		}catch(Exception e) {
			log.error("GradewisePayConfigBean :: saveOrUpdateGradeWisePay exception=> ",e);
		}
		return null;
	}
	
	public void clearGradeWisePay() {
		selectedAllowanceGrade = new AllowanceGrade();
		isAddButton=true;
		isEditButton=true;
		isDeleteButton=true;
		allowanceGrade = new AllowanceGrade();
		
	}
	
	public String cancelGradeWisePay() {
		selectedAllowanceGrade = new AllowanceGrade();
		isAddButton=true;
		isEditButton=true;
		isDeleteButton=true;
		allowanceGrade = new AllowanceGrade();
		return LIST_PAGE;
	}
	
}
