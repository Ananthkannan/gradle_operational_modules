package in.gov.cooptex.personnel.admin.bean;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AddnlEarningDeductionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.JoiningChecklistDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.PayAspect;
import in.gov.cooptex.operation.hrms.dto.MonthDTO;
import in.gov.cooptex.personnel.hrms.dto.AllowanceDetailsDTO;
import in.gov.cooptex.personnel.payroll.model.EmpAddnlEarningDeduction;
import in.gov.cooptex.personnel.payroll.model.EmpAddnlEarningDeductionDetails;
import in.gov.cooptex.personnel.payroll.model.EmpAddnlEarningDeductionLog;
import in.gov.cooptex.personnel.payroll.model.EmpAddnlEarningDeductionNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("empAddnlEarningDeductionBean")
@Scope("session")
public class EmpAddnlEarningDeductionBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE = "/pages/personnelHR/payRoll/listEmployeeAdditionalEarningsDeduction.xhtml?faces-redirect=true;";

	private final String CREATE_EMP_ADDNL_EARNING_DEDUCTION_PAGE = "/pages/personnelHR/payRoll/createEmployeeAdditionalEarningsDeduction.xhtml?faces-redirect=true;";

	private final String VIEW_EMP_ADDNL_EARNING_DEDUCTION_PAGE = "/pages/personnelHR/payRoll/viewEmployeeAdditionalEarningsDeduction.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String REJECT = "REJECT";

	final String APPROVED = "APPROVED";

	@Autowired
	HttpService httpService;
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	int totalRecords = 0;
	@Autowired
	AppPreference appPreference;
	@Setter
	@Getter
	List<EntityMaster> headRegionOfficeList;
	@Autowired
	CommonDataService commonDataService;
	@Setter
	@Getter
	List<EntityTypeMaster> entityTypeList;
	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc = new EntityMaster();
	@Getter
	@Setter
	EntityMaster selectedEntity;
	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;
	@Getter
	@Setter
	EmpAddnlEarningDeduction empAddnlEarningDeduction = new EmpAddnlEarningDeduction();
	@Getter
	@Setter
	EmpAddnlEarningDeduction selectedEmpAddnlEarningDeduction = new EmpAddnlEarningDeduction();
	@Getter
	@Setter
	LazyDataModel<EmpAddnlEarningDeduction> lazyEmpAddnlEarningDeductionList;

	@Getter
	@Setter
	boolean visibility = false;
	@Getter
	@Setter
	List<Object> payAspect;
	@Getter
	@Setter
	public Double amount;
	@Getter
	@Setter
	public String note;

	@Getter
	@Setter
	PayAspect selectedPayAspect;

	@Getter
	@Setter
	List<EntityMaster> entitylist = new ArrayList<>();
	@Getter
	@Setter
	List<Department> departmentList;
	@Getter
	@Setter
	Department selectedDepartment = new Department();
	@Getter
	@Setter
	GlAccount selectedGlAccount;
	@Setter
	@Getter
	List<SectionMaster> sectionMasterList;
	@Setter
	@Getter
	List<EmployeeMaster> employeeMasterList;
	@Setter
	@Getter
	List<GlAccount> glAccountList;
	@Setter
	@Getter
	private List<EmployeeMaster> selectedEmployeeMasterList;
	@Getter
	@Setter
	SectionMaster selectedSection;

	@Getter
	@Setter
	BaseDTO baseDTO = new BaseDTO();

	ObjectMapper mapper;
	@Autowired
	ErrorMap errorMap;

	String jsonResponse;

	@Getter
	@Setter
	List<Integer> yearListData;

	@Getter
	@Setter
	Integer yearValue;

	@Getter
	@Setter
	List<MonthDTO> monthsList;

	@Getter
	@Setter
	Integer selectedMonth;

	@Getter
	@Setter
	String earnigType, addEarningRemarks;
	@Getter
	@Setter
	Boolean finalApproval;

	@Getter
	@Setter
	List<EmpAddnlEarningDeduction> empAddnlEarningDeductionList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster forwardTo = new UserMaster();
	@Getter
	@Setter
	String createNote;
	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	@Getter
	@Setter
	List<UserMaster> userMasters;
	@Getter
	@Setter
	EmpAddnlEarningDeductionNote empAddnlEarningDeductionNote;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	Boolean approvalFlag = false;
	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	Boolean viewFlag = false;

	@Getter
	@Setter
	Long noteId;

	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();

	@Getter
	@Setter
	Boolean forwardFor, isIncrementEmployeeStatus = false;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<EmpAddnlEarningDeductionLog> empAddnlEarningDeductionLogList = new ArrayList<>();

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	AddnlEarningDeductionDTO addnlEarningDeductionDTO = new AddnlEarningDeductionDTO();

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};

	@Getter
	@Setter
	private Boolean skipApprovalFlag = false;
	
	@Getter
	@Setter
	private Boolean inputDisabledFlag = false;

	public String addEmpAddnlEarningDeduction() {

		log.info("<--- Start EmpAddnlEarningDeductionBean.addEmpAddnlEarningDeduction ---->");
		try {
			action = "Add";
			skipApprovalFlag = false;
			setSelectedGlAccount(null);
			setSelectedPayAspect(null);
			setSelectedHeadRegionOfc(null);
			setSelectedEntityTypeMaster(null);
			setSelectedEntity(null);
			setSelectedDepartment(null);
			setSelectedSection(null);
			selectedEmployeeMasterList = new ArrayList<>();
			setYearValue(null);
			setSelectedMonth(null);
			setForwardTo(null);
			setFinalApproval(null);
			setCreateNote(null);
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			// entityTypeList = commonDataService.getEntityTypeByStatus();
			departmentList = commonDataService.loadDepartmentList();
			forwardToUsersList = commonDataService.loadForwardToUsers();
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			loadGlAccountByType();
			lPayAspect();
			yearListData = loadYearList();
			userMasters = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.EMP_ADDNL_EARNING_DEDUCTION.toString());
			monthsList = loadAllMonths();
			inputDisabledFlag = false;
			empAddnlEarningDeductionList = new ArrayList<>();
			log.info("<---- End  EmpAddnlEarningDeductionBean.addEmpAddnlEarningDeduction ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_EMP_ADDNL_EARNING_DEDUCTION_PAGE;

	}

	public String clearEmpAddnlEarningDeduction() {
		log.info("<---- statrs  EmpAddnlEarningDeductionBean.clearEmpAddnlEarningDeduction ---->");
		setSelectedGlAccount(null);
		setSelectedPayAspect(null);
		setSelectedHeadRegionOfc(null);
		setSelectedEntityTypeMaster(null);
		setSelectedEntity(null);
		setSelectedDepartment(null);
		setSelectedSection(null);
		selectedEmployeeMasterList = new ArrayList<>();
		setYearValue(null);
		setSelectedMonth(null);
		//showEmpAddnlEarningDeductionPage();
		skipApprovalFlag = false;
		inputDisabledFlag = false;
		setEmpAddnlEarningDeductionList(new ArrayList<>());
		log.info("<---- End  EmpAddnlEarningDeductionBean.clearEmpAddnlEarningDeduction ---->");
		return CREATE_EMP_ADDNL_EARNING_DEDUCTION_PAGE;

	}

	private void lPayAspect() {
		Object[] statusAspect = PayAspect.class.getEnumConstants();
		log.info("----> statusAspect" + statusAspect);
		payAspect = new ArrayList<Object>();
		for (Object payaspect : statusAspect) {
			payAspect.add(payaspect);
		}
	}

	public void headandregional() {
		log.info("<<<< ----------Starts filternig based on Head And Regional Office------- >>>>");
		log.info("entity master" + selectedHeadRegionOfc);

		try {

			/*
			 * if (selectedHeadRegionOfc != null &&
			 * selectedHeadRegionOfc.getEntityTypeMaster() != null &&
			 * selectedHeadRegionOfc.getEntityTypeMaster().getEntityCode().equalsIgnoreCase(
			 * "HEAD_OFFICE")) { visibility = true; setSelectedEntityTypeMaster(null);
			 * setSelectedEntity(null); } else { visibility = false; }
			 */

			if (selectedHeadRegionOfc != null) {
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ selectedHeadRegionOfc.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					entityTypeList = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
					});

					loadEmployeeBySectionId();
					log.info("entitytypelistByHeadAndRegionalId list size is " + entityTypeList.size());
				}
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}

		log.info("<<<< ----------End filternig based on Head And Regional Office------- >>>>");
	}

	public void onSelectEntityType() {

		log.info("<<<< ----------Starts loadMeretinEntityList ------- >>>>");
		log.info("entity master", selectedEntityTypeMaster);
		try {

			if (selectedHeadRegionOfc != null && selectedHeadRegionOfc.getId() != null
					&& selectedEntityTypeMaster != null && selectedEntityTypeMaster.getId() != null) {
				Long regionId = selectedHeadRegionOfc.getId();
				entitylist = commonDataService.loadEntityByregionOrentityTypeId(regionId,
						selectedEntityTypeMaster.getId());
				loadEmployeeBySectionId();
			} else {
				log.info("entity not found.");
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<<<< ----------End loadMeretinEntityList ------- >>>>");
	}

	public void loadSectionByDepartmentId() {
		log.info("<<<< ----------Starts filternig based on Department------- >>>>");
		if (selectedDepartment != null && selectedDepartment.getId() != null) {
			Long departmentId = selectedDepartment.getId();
			log.info("departmentId=============>" + departmentId);
			sectionMasterList = commonDataService.getSectionByDepartmentId(departmentId);
			loadEmployeeBySectionId();
			log.info("sectionMasterList===========>size<======>" + sectionMasterList.size());
		}
		log.info("<<<< ----------End filternig based onDepartment ------- >>>>");
	}

	public List<EmployeeMaster> loadEmployeeBySectionId() {
		try {
			Long horoId = selectedHeadRegionOfc != null && selectedHeadRegionOfc.getId() != null
					? selectedHeadRegionOfc.getId()
					: null;
			Long entityTypeId = selectedEntityTypeMaster != null && selectedEntityTypeMaster.getId() != null
					? selectedEntityTypeMaster.getId()
					: null;
			Long entityId = selectedEntity != null && selectedEntity.getId() != null ? selectedEntity.getId() : null;
			Long departmentId = selectedDepartment != null && selectedDepartment.getId() != null
					? selectedDepartment.getId()
					: null;
			Long sectionId = selectedSection != null && selectedSection.getId() != null ? selectedSection.getId()
					: null;
			log.info("HORO Id       ====  " + horoId);
			log.info("EntityType Id ====  " + entityTypeId);
			log.info("Entity Id     ====  " + entityId);
			log.info("Department Id ====  " + departmentId);
			log.info("Section Id    ====  " + sectionId);
			addnlEarningDeductionDTO.setHoroId(horoId);
			addnlEarningDeductionDTO.setEntityTypeId(entityTypeId);
			addnlEarningDeductionDTO.setEntityId(entityId);
			addnlEarningDeductionDTO.setDepartmentId(departmentId);
			addnlEarningDeductionDTO.setSectionId(sectionId);
			String url = AppUtil.getPortalServerURL() + "/employee/getactiveemployeelistbysearchcriteria";
			log.info("getactiveemployeelistbysearchcriteria url==> " + url);
			BaseDTO baseDTO = httpService.post(url, addnlEarningDeductionDTO);
			if (baseDTO == null) {
				log.error("Base dto returned null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			RequestContext.getCurrentInstance().update("inputpanelentityid");
			log.info("Employee Master List Size ===>" + employeeMasterList != null && !employeeMasterList.isEmpty()
					? employeeMasterList.size()
					: 0);
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
		return employeeMasterList;
	}

	public List<GlAccount> loadGlAccountByType() {
		log.info("loadGlAccountByType  started");
		glAccountList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empaddnlearningdeduction/getglaccountbytypeid";
			log.info("loadEmployeeList url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				glAccountList = mapper.readValue(jsonResponse, new TypeReference<List<GlAccount>>() {
				});
			} else {
				log.error("Unable to get Employee Information");
			}
		} catch (Exception e) {
			log.error("Exception Occured", e);
		}

		return glAccountList;
	}

	public List<Integer> loadYearList() {

		log.info(" loadYearList  called.....");
		List<Integer> yearListData = new ArrayList<>();

		try {
			Calendar beginCalendar = Calendar.getInstance();
			beginCalendar.set(Calendar.YEAR, beginCalendar.get(Calendar.YEAR));
			int year = beginCalendar.get(Calendar.YEAR) - 1;
			for (int i = 0; i < 3; i++) {
				yearListData.add(year + i);
			}
			log.info("yearListData==>" + yearListData);

		} catch (Exception e) {
			log.error("Exception in PolicyNotesBean  :: loadYearList ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return yearListData;
	}

	/*
	 * public List<String> loadAllMonths() { log.info("loadAllMonths called");
	 * List<String> monthsList = new ArrayList<String>(); String[] months = new
	 * DateFormatSymbols().getMonths(); for (int i = 0; i < months.length; i++) {
	 * String month = months[i]; log.info("month = " + month);
	 * monthsList.add(months[i]); } return monthsList; }
	 */

	public List<MonthDTO> loadAllMonths() {
		log.info("EmpAddnlEarningDeductionBean loadAllMonths.....");
		List<MonthDTO> monthsList = new ArrayList<>();
		try {

			String[] months = new DateFormatSymbols().getMonths();
			for (int i = 0; i < months.length - 1; i++) {
				String month = months[i];

				MonthDTO monthDTO = new MonthDTO();
				monthDTO.setMonthId(i + 1);
				monthDTO.setMonthName(month);

				System.out.println("month ===> " + month);
				monthsList.add(monthDTO);
			}
		} catch (Exception e) {
			log.error("Exception in EmpAddnlEarningDeductionBean  :: loadAllMonths ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return monthsList;
	}

	/*
	 * public void addEmployeeDateList() {
	 * log.info("EmpAddnlEarningDeductionBean addEmployeeList start==>"); try {
	 * if(selectedEmployeeMasterList != null &&
	 * !selectedEmployeeMasterList.isEmpty()) {
	 * List<EmpAddnlEarningDeductionDetails> earningList = new ArrayList<>(); for
	 * (EmployeeMaster employeeMaster : selectedEmployeeMasterList) {
	 * EmpAddnlEarningDeduction empAddnlEarningDeduction = new
	 * EmpAddnlEarningDeduction(); EmpAddnlEarningDeductionDetails
	 * empAddnlEarningDeductionDetails = new EmpAddnlEarningDeductionDetails();
	 * empAddnlEarningDeduction.setEntityMaster(selectedEntity);
	 * empAddnlEarningDeduction.setDepartmentMaster(getSelectedDepartment());
	 * empAddnlEarningDeduction.setSectionMaster(getSelectedSection());
	 * empAddnlEarningDeduction.setYear(getYearValue());
	 * empAddnlEarningDeduction.setActiveStatus(true);
	 * empAddnlEarningDeduction.setGlAccount(getSelectedGlAccount());
	 * 
	 * empAddnlEarningDeductionDetails.setEmpMaster(employeeMaster);
	 * empAddnlEarningDeductionDetails.setEmpAddnlEarningDeduction(
	 * empAddnlEarningDeduction);
	 * 
	 * earningList.add(empAddnlEarningDeductionDetails);
	 * empAddnlEarningDeduction.setEmpAddnlEarningDeductionDetailsList(earningList);
	 * empAddnlEarningDeductionList.add(empAddnlEarningDeduction); } }
	 * 
	 * 
	 * }catch (Exception e) { log.error("EmpAddnlEarningDeduction :: Exception ==> "
	 * + e);
	 * 
	 * } }
	 */

	private List<Long> checkExistAllowanceByEmpCount() {
		log.info("checkExistAllowanceByEmpCount() Starts ");
		List<Long> empList = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/empaddnlearningdeduction/checkexistcount";
			log.info("url==========>" + url);
			AllowanceDetailsDTO allowanceDetailsDTO = new AllowanceDetailsDTO();
			selectedEmployeeMasterList.forEach(o -> {
				if (o.getId() != null) {
					allowanceDetailsDTO.getEmpIdList().add(Long.valueOf(o.getId().toString()));
				}
			});
			allowanceDetailsDTO.setGlAccountId(selectedGlAccount == null ? null : selectedGlAccount.getId());
			allowanceDetailsDTO.setMonth(selectedMonth);
			allowanceDetailsDTO.setYear(yearValue);
			BaseDTO response = httpService.post(url, allowanceDetailsDTO);

			if (response != null && response.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
				empList = new ArrayList<>();
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(response.getResponseContent());
				empList = mapper.readValue(jsonValue, new TypeReference<List<Long>>() {
				});
				log.info("checkExistAllowanceByEmpCount ");
			}
		} catch (Exception e) {
			log.error("Exception at checkExistAllowanceByEmpCount() ", e);
		}
		return empList;
	}

	public void addEmployeeDateList() {
		List<Long> existEmpIdList = null;
		try {
			inputDisabledFlag = true;
			log.info("EmpAddnlEarningDeduction addEmployeeList new received :");
			if (getSelectedEmployeeMasterList() != null && getSelectedEmployeeMasterList().size() >= 0) {

				existEmpIdList = checkExistAllowanceByEmpCount();

				for (EmployeeMaster employeeMaster : selectedEmployeeMasterList) {
					log.info("EmployeeMaster=========>" + employeeMaster);
					EmpAddnlEarningDeduction empAddnlEarningDeduction = new EmpAddnlEarningDeduction();
					empAddnlEarningDeduction.setEmployeeMaster(employeeMaster);
					empAddnlEarningDeduction.setEarningType(getEarnigType());
					empAddnlEarningDeduction.setPayAspect(getSelectedPayAspect());
					if (selectedHeadRegionOfc != null) {
						log.info("selectedEntityTypeMaster==> " + selectedEntityTypeMaster);
						empAddnlEarningDeduction.setHeadRegionOfc(selectedHeadRegionOfc);
						empAddnlEarningDeduction.setEntityTypeMaster(selectedEntityTypeMaster);
						empAddnlEarningDeduction.setEntityMaster(selectedEntity);

					}
					empAddnlEarningDeduction.setDepartmentMaster(getSelectedDepartment());
					empAddnlEarningDeduction.setSectionMaster(getSelectedSection());
					empAddnlEarningDeduction.setYear(getYearValue());
					empAddnlEarningDeduction.setActiveStatus(true);
					empAddnlEarningDeduction.setGlAccount(getSelectedGlAccount());
					log.info("createNote====>" + getCreateNote());
					empAddnlEarningDeduction.setCreateNote(getCreateNote());
					Integer monthId = getSelectedMonth();
					log.info("monthId==> " + monthId);
					empAddnlEarningDeduction.setMonth(monthId);

					boolean alreadyExists = existEmpIdList.stream().anyMatch(t -> t.equals(employeeMaster.getId()));

					if (alreadyExists) {
						String errorMsg = new StringBuilder()
								.append("This allowance is already added for this employee (")
								.append(employeeMaster.getFirstName()).append(" and ")
								.append(employeeMaster.getPersonalInfoEmployment().getPfNumber()).append(") for ")
								.append(AppUtil.getMonthName(selectedMonth)).append(" ").append(yearValue).toString();
						AppUtil.addWarn(errorMsg);
						inputDisabledFlag = false;
						return;
					} else {
						boolean idExists = empAddnlEarningDeductionList.stream()
								.anyMatch(t -> t.getEmployeeMaster().getId().equals(employeeMaster.getId()));
						if (!idExists) {
							empAddnlEarningDeductionList.add(empAddnlEarningDeduction);
							inputDisabledFlag = true;
						}
					}
				}
				log.info("empAddnlEarningDeductionList===size=>" + empAddnlEarningDeductionList.size());
				log.info("empAddnlEarningDeductionList====>" + empAddnlEarningDeductionList);
			}

		} catch (Exception e) {
			inputDisabledFlag = false;
			log.error("EmpAddnlEarningDeduction Collection Add Error :", e);

		}
	}

	public String saveEmpAddnlEarningDeduction() {
		if (action.equalsIgnoreCase("Add")) {
			return saveAddnlEarningDeduction();
		} else if (action.equalsIgnoreCase("Edit")) {
			return updateAddnlEarningDeduction();
		}
		return null;
	}

	public String updateAddnlEarningDeduction() {
		log.info("<==== Starts EmpAddnlEarningDeductionBean.updateAddnlEarningDeduction =======>");
		try {
			if (empAddnlEarningDeductionList.size() == 0) {
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ITEMS_NULL.getErrorCode());
				return null;
			} else {
				for (EmpAddnlEarningDeduction empAddnlEarningDeduction : empAddnlEarningDeductionList) {
					if (empAddnlEarningDeduction.getDetailsId() == null) {
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_ID_NOT_FOUND).getCode());
						return null;
					}
					if (empAddnlEarningDeduction.getAmount() == null) {
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_AMOUNT_NOT_FOUND).getCode());
						return null;
					}
					// if (empAddnlEarningDeduction.getNote() == null) {
					// errorMap.notify(ErrorDescription
					// .getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_NOTE_NOT_FOUND).getCode());
					// return null;
					// }
				}
			}
			int empAddnlEarningDeductionListSize = empAddnlEarningDeductionList == null ? 0
					: empAddnlEarningDeductionList.size();
			log.info("empAddnlEarningDeductionList==> " + empAddnlEarningDeductionListSize);
			if (empAddnlEarningDeductionListSize > 0) {
				empAddnlEarningDeductionList.get(0).setSkipApprovalFlag(skipApprovalFlag);
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/empaddnlearningdeduction/update";
			log.info("url==========>" + url);
			BaseDTO response = httpService.put(url, empAddnlEarningDeductionList);

			if (response != null && response.getStatusCode() == 0) {
				log.info("EmpAddnlEarningDeduction updated successfully..........");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITIONAL_EARNINGS_UPDATE_SUCCESSFULLY)
						.getCode());
				clear();
			}
		} catch (Exception e) {
			log.error("updateAddnlEarningDeduction Collection Add Error :", e);
		}
		return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}

	public String saveAddnlEarningDeduction() {
		log.info("<==== Starts EmpAddnlEarningDeductionBean.saveEmpAddnlEarningDeduction =======>");
		try {

			if (empAddnlEarningDeductionList.size() == 0) {
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ITEMS_NULL.getErrorCode());
				return null;
			} else {
				for (EmpAddnlEarningDeduction empAddnlEarningDeduction : empAddnlEarningDeductionList) {
					if (empAddnlEarningDeduction.getAmount() == null) {
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_AMOUNT_NOT_FOUND).getCode());
						return null;
					}
					// if (empAddnlEarningDeduction.getNote() == null) {
					// errorMap.notify(ErrorDescription
					// .getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_NOTE_NOT_FOUND).getCode());
					// return null;
					// }
					if (!skipApprovalFlag) {
						if (forwardTo == null) {
							errorMap.notify(ErrorDescription
									.getError(OperationErrorCode.ADDITIONAL_EARNINGS_NOTE_NOT_FOUND).getCode());
							return null;
						}
						if (createNote == null) {
							errorMap.notify(ErrorDescription
									.getError(OperationErrorCode.ADDITIONAL_EARNINGS_NOTE_NOT_FOUND).getCode());
							return null;
						}
					}

					log.info("createNote====>" + getCreateNote());
					empAddnlEarningDeduction.setCreateNote(getCreateNote());
					empAddnlEarningDeduction.setForwardTo(getForwardTo());
					empAddnlEarningDeduction.setFinalApproval(getFinalApproval());
				}
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/empaddnlearningdeduction/create";
			log.info("url==========>" + url);

			int empAddnlEarningDeductionListSize = empAddnlEarningDeductionList == null ? 0
					: empAddnlEarningDeductionList.size();

			if (empAddnlEarningDeductionListSize > 0) {
				empAddnlEarningDeductionList.get(0).setSkipApprovalFlag(skipApprovalFlag);
			}

			BaseDTO response = httpService.post(url, empAddnlEarningDeductionList);

			if (response != null) {
				if (response.getStatusCode() == 0) {
					log.info("EmpAddnlEarningDeduction saved successfully..........");
					// AppUtil.addInfo("Employee Additional Earning Deduction Saved successfully");
					errorMap.notify(ErrorDescription
							.getError(MastersErrorCode.ADDITIONAL_EARNING_DETECTION_ADDED_SUCCESS).getCode());
					clear();
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			} /*
				 * else if (response != null && response.getStatusCode() != 0) {
				 * errorMap.notify(response.getStatusCode()); return null; }
				 */ /*
					 * else { errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); }
					 */

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EmpAddnlEarningDeductionBean.saveEmpAddnlEarningDeduction =======>");
		return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}

	public String deleteEmpAddnlEarningDeduction() {
		log.info(" deleteEmpAddnlEarningDeduction called");
		try {
			action = "Delete";
			if (selectedEmpAddnlEarningDeduction == null || selectedEmpAddnlEarningDeduction.getId() == null) {

				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEmpAddnlEarningDeductionDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteEmpAddnlEarningDeductionConfrom() {
		log.info("<===== Starts EmpAddnlEarningDeductionBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/empaddnlearningdeduction/delete/" + selectedEmpAddnlEarningDeduction.getId());
			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("Employee Additional Earning Deduction Deleted successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEmpAddnlEarningDeductionDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete EmpAddnlEarningDeduction ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends EmpAddnlEarningDeductionBean.deleteConfirm ===========>");
		return showEmpAddnlEarningDeductionPage();
	}

	public String clear() {
		log.info("<---- Start EmpAddnlEarningDeductionBean . clear ----->");
		showEmpAddnlEarningDeductionPage();
		log.info("<---- End EmpAddnlEarningDeductionBean.clear ----->");
		return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;

	}

	public String showEmpAddnlEarningDeductionPage() {
		log.info("<==== Starts EmpAddnlEarningDeductionBean.showEmpAddnlEarningDeductionPage =====>");
		mapper = new ObjectMapper();
		setSelectedEmpAddnlEarningDeduction(null);
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		departmentList = commonDataService.loadDepartmentList();
		sectionMasterList = commonDataService.getSectionMaster();
		loadLazyEmpAddnlEarningDeductionList();
		log.info("<==== ends EmpAddnlEarningDeductionBean.showEmpAddnlEarningDeductionPage =====>");
		return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;

	}

	public void loadLazyEmpAddnlEarningDeductionList() {
		log.info("<===== Starts EmpAddnlEarningDeductionBean.loadLazyEmpAddnlEarningDeductionList ======>");
		lazyEmpAddnlEarningDeductionList = new LazyDataModel<EmpAddnlEarningDeduction>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<EmpAddnlEarningDeduction> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/empaddnlearningdeduction/getempaddnlearningdeductionlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						empAddnlEarningDeductionList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpAddnlEarningDeduction>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyEmpAddnlEarningDeductionList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return empAddnlEarningDeductionList;
			}

			@Override
			public Object getRowKey(EmpAddnlEarningDeduction res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpAddnlEarningDeduction getRowData(String rowKey) {
				try {
					for (EmpAddnlEarningDeduction empAddnlEarningDeduction : empAddnlEarningDeductionList) {
						if (empAddnlEarningDeduction.getId().equals(Long.valueOf(rowKey))) {
							selectedEmpAddnlEarningDeduction = empAddnlEarningDeduction;
							return empAddnlEarningDeduction;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== ends EmpAddnlEarningDeductionBean.loadLazyEmpAddnlEarningDeductionList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmpAddnlEarningDeductionBean.onRowSelect ========>" + event);
		selectedEmpAddnlEarningDeduction = ((EmpAddnlEarningDeduction) event.getObject());
		log.info("selectedEmpAddnlEarningDeduction id" + selectedEmpAddnlEarningDeduction.getId());
		/*
		 * addButtonFlag = true; editButtonFlag = false; deleteButtonFlag = false;
		 */
		log.info("selectedDisposalAssetListDTO getStatus ==> " + selectedEmpAddnlEarningDeduction.getStage());
		if (selectedEmpAddnlEarningDeduction.getStage().equalsIgnoreCase("APPROVED")
				|| selectedEmpAddnlEarningDeduction.getStage().equalsIgnoreCase("FINAL-APPROVED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedEmpAddnlEarningDeduction.getStage().equalsIgnoreCase("REJECTED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = true;
			isDeleteButton = true;
		} else if (selectedEmpAddnlEarningDeduction.getStage().equalsIgnoreCase("SUBMITTED")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = false;
			isDeleteButton = true;
		} else {
			isAddButton = true;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
		log.info("<===Ends EmpAddnlEarningDeductionBean.onRowSelect ========>");
	}

	public String monthCovertToString(int monthNumber) {
		return Month.of(monthNumber).name();
	}

	public String viewEmpAddnlEarningDeduction() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedEmpAddnlEarningDeduction == null || selectedEmpAddnlEarningDeduction.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedEmpAddnlEarningDeduction.getId());
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		// selectedEmpAddnlEarningDeduction =
		// getEmpAddnlEarningDeductionDetails(selectedEmpAddnlEarningDeduction.getId());
		getEmpAddnlEarningDeductionDetails(selectedEmpAddnlEarningDeduction.getId());
		return VIEW_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}

	public String editEmpAddnlEarningDeduction() {
		log.info("<====== EmpAddnlEarningDeductionBean.editEmpAddnlEarningDeduction starts====>");
		EntityTypeMaster entityTypeMaster = null;
		EntityMaster horo = null;
		try {
			action = "Edit";
			selectedEmployeeMasterList = new ArrayList<>();
			empAddnlEarningDeductionList = new ArrayList<>();
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			departmentList = commonDataService.loadDepartmentList();
			// forwardToUsersList = commonDataService.loadForwardToUsers();
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			loadGlAccountByType();
			lPayAspect();
			yearListData = loadYearList();
			monthsList = loadAllMonths();
			userMasters = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.EMP_ADDNL_EARNING_DEDUCTION.toString());
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			if (selectedEmpAddnlEarningDeduction == null || selectedEmpAddnlEarningDeduction.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info(
					"editEmpAddnlEarningDeduction additional earning ID-->" + selectedEmpAddnlEarningDeduction.getId());
			log.info("Selected EmpAddnlEarningDeduction  Id : : " + selectedEmpAddnlEarningDeduction.getId());

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/empaddnlearningdeduction/get/ "
					+ selectedEmpAddnlEarningDeduction.getId();
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			if (baseDTO.getStatusCode() == 0) {
				EmpAddnlEarningDeduction empAddnlEarningDeduction = mapper.readValue(jsonResponse,
						EmpAddnlEarningDeduction.class);
				if (empAddnlEarningDeduction != null && empAddnlEarningDeduction.getEntityMaster() != null
						&& empAddnlEarningDeduction.getEntityMaster().getId() != null) {
					entityTypeMaster = commonDataService
							.getEntityType(empAddnlEarningDeduction.getEntityMaster().getId());
					selectedEntityTypeMaster = entityTypeMaster;
					horo = commonDataService.findRegionByShowroomId(empAddnlEarningDeduction.getEntityMaster().getId());
					selectedHeadRegionOfc = horo;
					headandregional();
					onSelectEntityType();
				}
				selectedGlAccount = empAddnlEarningDeduction.getGlAccount();
				selectedPayAspect = empAddnlEarningDeduction.getPayAspect();
				selectedEntity = empAddnlEarningDeduction.getEntityMaster();

				selectedDepartment = empAddnlEarningDeduction.getDepartmentMaster();
				loadSectionByDepartmentId();
				selectedSection = empAddnlEarningDeduction.getSectionMaster();
				selectedSection = empAddnlEarningDeduction.getSectionMaster();
				loadEmployeeBySectionId();
				yearValue = empAddnlEarningDeduction.getYear();
				selectedMonth = empAddnlEarningDeduction.getMonth();
				List<EmpAddnlEarningDeductionDetails> addnlEarningDeductionList = empAddnlEarningDeduction
						.getEmpAddnlEarningDeductionDetailsList();
				log.info("addnlEarningDeductionList size ==> " + addnlEarningDeductionList.size());
				if (addnlEarningDeductionList != null && !addnlEarningDeductionList.isEmpty()) {
					for (EmpAddnlEarningDeductionDetails addnlEarningDeductionDetails : addnlEarningDeductionList) {
						selectedEmployeeMasterList.add(addnlEarningDeductionDetails.getEmpMaster());
						EmpAddnlEarningDeduction addnlEarningDeduction = new EmpAddnlEarningDeduction();
						addnlEarningDeduction.setId(empAddnlEarningDeduction.getId());
						addnlEarningDeduction.setEmployeeMaster(addnlEarningDeductionDetails.getEmpMaster());
						addnlEarningDeduction.setEarningType(empAddnlEarningDeduction.getEarningType());
						addnlEarningDeduction.setPayAspect(empAddnlEarningDeduction.getPayAspect());
						if (horo != null) {
							addnlEarningDeduction.setHeadRegionOfc(horo);
						} else {
							log.error("horo is empty");
						}
						if (entityTypeMaster != null) {
							addnlEarningDeduction.setEntityTypeMaster(entityTypeMaster);
						} else {
							log.error("entity type is empty");
						}
						addnlEarningDeduction.setEntityMaster(empAddnlEarningDeduction.getEntityMaster());
						addnlEarningDeduction.setDepartmentMaster(empAddnlEarningDeduction.getDepartmentMaster());
						addnlEarningDeduction.setSectionMaster(empAddnlEarningDeduction.getSectionMaster());
						addnlEarningDeduction.setYear(empAddnlEarningDeduction.getYear());
						addnlEarningDeduction.setMonth(empAddnlEarningDeduction.getMonth());
						addnlEarningDeduction.setActiveStatus(true);
						addnlEarningDeduction.setGlAccount(empAddnlEarningDeduction.getGlAccount());
						addnlEarningDeduction.setAmount(addnlEarningDeductionDetails.getAmount());
						addnlEarningDeduction.setNote(addnlEarningDeductionDetails.getNote());
						addnlEarningDeduction.setDetailsId(addnlEarningDeductionDetails.getId());
						empAddnlEarningDeductionList.add(addnlEarningDeduction);
					}
				} else {
					log.error("EmpAddnlEarningDeductionDetails is empty");
				}
				log.info("empAddnlEarningDeductionList===size=>" + empAddnlEarningDeductionList.size());

				// -- to get empAddnlEarningDeduction note details
				if (empAddnlEarningDeduction.getEmpAddnlEarningDeductionNoteList() != null
						&& !empAddnlEarningDeduction.getEmpAddnlEarningDeductionNoteList().isEmpty()) {
					for (EmpAddnlEarningDeductionNote empAddnlEarningDeductionNote : empAddnlEarningDeduction
							.getEmpAddnlEarningDeductionNoteList()) {
						noteId = empAddnlEarningDeductionNote.getId();
						forwardTo = empAddnlEarningDeductionNote.getForwardTo();
						log.info("forwardTo id=> " + forwardTo.getId());
						forwardFor = empAddnlEarningDeductionNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						createNote = empAddnlEarningDeductionNote.getNote();
						log.info("createNote=> " + createNote);
					}

				} else {
					log.error("Increment note not found");
				}
			}
		} catch (Exception e) {
			log.error("editEmpAddnlEarningDeduction exception ==> ", e);
		}
		return CREATE_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}

	public void getEmpAddnlEarningDeductionDetails(Long id) {

		log.info("<====== EmpAddnlEarningDeductionBean.getEmpAddnlEarningDeductionDetails starts====>" + id);
		// empAddnlEarningDeduction = new EmpAddnlEarningDeduction();
		try {
			if (selectedEmpAddnlEarningDeduction == null || selectedEmpAddnlEarningDeduction.getId() == null) {
				log.error(" No EmpAddnlEarningDeduction selected ");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return;
			}
			log.info("Selected EmpAddnlEarningDeduction  Id : : " + selectedEmpAddnlEarningDeduction.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/empaddnlearningdeduction/get/ "
					+ selectedEmpAddnlEarningDeduction.getId();
			log.info("EmpAddnlEarningDeduction Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			if (baseDTO.getStatusCode() == 0) {
				selectedEmpAddnlEarningDeduction = mapper.readValue(jsonResponse, EmpAddnlEarningDeduction.class);
				log.info("selectedEmpAddnlEarningDeduction id=======>" + selectedEmpAddnlEarningDeduction.getId());
				log.info("Entity id=======>" + selectedEmpAddnlEarningDeduction.getEntityMaster().getId());
				if (selectedEmpAddnlEarningDeduction != null
						&& selectedEmpAddnlEarningDeduction.getEntityMaster() != null
						&& selectedEmpAddnlEarningDeduction.getEntityMaster().getId() != null) {
					EntityTypeMaster entityTypeMaster = commonDataService
							.getEntityType(selectedEmpAddnlEarningDeduction.getEntityMaster().getId());
					empAddnlEarningDeduction.setEntityTypeMaster(entityTypeMaster);

					EntityMaster entitymaster = commonDataService
							.findRegionByShowroomId(selectedEmpAddnlEarningDeduction.getEntityMaster().getId());
					empAddnlEarningDeduction.setHeadRegionOfc(entitymaster);
				}

				// -- to get empAddnlEarningDeduction note details
				if (selectedEmpAddnlEarningDeduction.getEmpAddnlEarningDeductionNoteList() != null
						&& !selectedEmpAddnlEarningDeduction.getEmpAddnlEarningDeductionNoteList().isEmpty()) {
					for (EmpAddnlEarningDeductionNote empAddnlEarningDeductionNote : selectedEmpAddnlEarningDeduction
							.getEmpAddnlEarningDeductionNoteList()) {
						noteId = empAddnlEarningDeductionNote.getId();
						forwardToUser = empAddnlEarningDeductionNote.getForwardTo();
						log.info("forwardToUser id=> " + forwardToUser.getId());
						forwardFor = empAddnlEarningDeductionNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						note = empAddnlEarningDeductionNote.getNote();
						log.info("note=> " + note);
					}
					// if(forwardToUser.getId() == )
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = forwardToUser.getId();

					log.info("loginUserId=> " + loginUserId);
					log.info("noteUserId=> " + noteUserId);
					if (loginUserId.equals(noteUserId)) {
						viewFlag = true;
					} else {
						viewFlag = false;
					}
				} else {
					log.error("Increment note not found");
				}
				// --to get increment log details
				if (selectedEmpAddnlEarningDeduction.getEmpAddnlEarningDeductionLogList() != null
						&& !selectedEmpAddnlEarningDeduction.getEmpAddnlEarningDeductionLogList().isEmpty()) {
					empAddnlEarningDeductionLogList = selectedEmpAddnlEarningDeduction
							.getEmpAddnlEarningDeductionLogList();
					log.info("empAddnlEarningDeductionLogList size==> " + empAddnlEarningDeductionLogList.size());
				} else {
					log.error("AddnlEarningDeduction log not found");
				}
				commentAndLogList = baseDTO.getCommentAndLogList();
				log.info(" EmpAddnlEarningDeduction Retrived  SuccessFully ");
			} else {
				log.error(" EmpAddnlEarningDeduction Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			// return empAddnlEarningDeduction;
		} catch (Exception e) {
			log.error("Exception Ocured while get EmpAddnlEarningDeduction Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		// return null;
	}

	/**
	 * purpose : to save approved or rejected status in log table
	 * 
	 * @param status
	 * @return
	 */
	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> " + status);
			log.info("addEarningRemarks==> " + addEarningRemarks);
			log.info("logList id==> " + empAddnlEarningDeductionLogList.get(0).getId());
			log.info("noteId==> " + noteId);
			log.info("forwardFor==> " + forwardFor);
			log.info("forwardTo id==> " + forwardTo.getId());
			AddnlEarningDeductionDTO addnlEarningDeductionDTO = new AddnlEarningDeductionDTO();
			addnlEarningDeductionDTO.setLogList(empAddnlEarningDeductionLogList);
			addnlEarningDeductionDTO.setForwardRemarks(addEarningRemarks);
			addnlEarningDeductionDTO.setAddnlEarningDeduction(selectedEmpAddnlEarningDeduction);
			addnlEarningDeductionDTO.setNoteId(noteId);
			addnlEarningDeductionDTO.setNotificationId(selectedNotificationId);

			addnlEarningDeductionDTO.setForwardUserId(forwardTo.getId());
			addnlEarningDeductionDTO.setForwardFor(finalApproval);
			if (status.equals(REJECT)) {
				addnlEarningDeductionDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals(APPROVED)) {
				addnlEarningDeductionDTO.setStatus(ApprovalStage.APPROVED);
			} else {
				addnlEarningDeductionDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/empaddnlearningdeduction/updateaddnlearninglog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, addnlEarningDeductionDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clearEmpAddnlEarningDeduction();
				// loadLazyIncrementList();
				if (status.equals(REJECT)) {
					errorMap.notify(ErrorDescription
							.getError(MastersErrorCode.ADDITIONAL_EARNING_DETECTION_REJECTED_SUCCESS).getCode());
				} else if (status.equals(APPROVED)) {
					errorMap.notify(ErrorDescription
							.getError(MastersErrorCode.ADDITIONAL_EARNING_DETECTION_APPROVED_SUCCESS).getCode());
				} else {
					errorMap.notify(ErrorDescription
							.getError(MastersErrorCode.ADDITIONAL_EARNING_DETECTION__FINAL_APPROVED_SUCCESS).getCode());
				}
				systemNotificationBean.loadTotalMessages();
				return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
			} else {
				log.info("Error while saving submitApprovedRejectedStatus :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("EmpAddnlEarningDeductionBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedEmpAddnlEarningDeduction = new EmpAddnlEarningDeduction();
			selectedEmpAddnlEarningDeduction.setId(Long.parseLong(id));
			selectedNotificationId = Long.parseLong(notificationId);
			viewEmpAddnlEarningDeduction();
		}
		log.info("joiningChecklistBean showViewListPage() Ends");
		return VIEW_EMP_ADDNL_EARNING_DEDUCTION_PAGE;
	}
}
