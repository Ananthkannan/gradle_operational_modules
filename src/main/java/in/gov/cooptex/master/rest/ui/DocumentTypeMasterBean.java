package in.gov.cooptex.master.rest.ui;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.DocumentTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("documentTypeMasterBean")
@Scope("session")
@Log4j2
public class DocumentTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listDocumentTypeMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createDocumentTypeMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String DOCUMENT_TYPE_MASTER_URL = AppUtil.getPortalServerURL() + "/documenttypemaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	DocumentTypeMaster documentTypeMaster, selectedDocumentTypeMaster;

	@Getter
	@Setter
	LazyDataModel<DocumentTypeMaster> lazyDocumentTypeMasterList;

	List<DocumentTypeMaster> documentTypeMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showDocumentTypeMasterListPage() {
		log.info("<==== Starts DocumentTypeMasterBean.showDocumentTypeMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		documentTypeMaster = new DocumentTypeMaster();
		selectedDocumentTypeMaster = new DocumentTypeMaster();
		loadLazyDocumentTypeMasterList();
		log.info("<==== Ends DocumentTypeMasterBean.showDocumentTypeMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyDocumentTypeMasterList() {
		log.info("<===== Starts DocumentTypeMasterBean.loadLazyDocumentTypeMasterList ======>");

		lazyDocumentTypeMasterList = new LazyDataModel<DocumentTypeMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<DocumentTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = DOCUMENT_TYPE_MASTER_URL + "/getalldocumenttypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						documentTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<DocumentTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return documentTypeMasterList;
			}

			@Override
			public Object getRowKey(DocumentTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public DocumentTypeMaster getRowData(String rowKey) {
				for (DocumentTypeMaster documentTypeMaster : documentTypeMasterList) {
					if (documentTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedDocumentTypeMaster = documentTypeMaster;
						return documentTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends DocumentTypeMasterBean.loadLazyDocumentTypeMasterList ======>");
	}

	public String showDocumentTypeMasterListPageAction() {
		log.info("<===== Starts DocumentTypeMasterBean.showDocumentTypeMasterListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedDocumentTypeMaster == null || selectedDocumentTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SELECT_DOCUMENT_TYPE_MASTER).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create documentTypeMaster called..");
				documentTypeMaster = new DocumentTypeMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit documentTypeMaster called..");
				String url = DOCUMENT_TYPE_MASTER_URL + "/getbyid/" + selectedDocumentTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					documentTypeMaster = mapper.readValue(jsonResponse, new TypeReference<DocumentTypeMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete documentTypeMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDocumentTypeMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showDocumentTypeMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends DocumentTypeMasterBean.showDocumentTypeMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts DocumentTypeMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(DOCUMENT_TYPE_MASTER_URL + "/deletebyid/" + selectedDocumentTypeMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDocumentTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete documentTypeMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends DocumentTypeMasterBean.deleteConfirm ===========>");
		return showDocumentTypeMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts DocumentTypeMasterBean.saveOrUpdate =======>");
		try {
			if (documentTypeMaster.getCode() == null || documentTypeMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_CODE_EMPTY).getCode());
				return null;
			}
			if (documentTypeMaster.getName() == null || documentTypeMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_NAME_EMPTY).getCode());
				return null;
			}
			if (documentTypeMaster.getName().trim().length() < 3 || documentTypeMaster.getName().trim().length() > 75) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (documentTypeMaster.getLocalName() == null || documentTypeMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_LNAME_EMPTY).getCode());
				return null;
			}
			if (documentTypeMaster.getLocalName().trim().length() < 3
					|| documentTypeMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_LNAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (documentTypeMaster.getActiveStatus() == null) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			String url = DOCUMENT_TYPE_MASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, documentTypeMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("documentTypeMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.DOCUMENT_TYPE_MASTER_UPDATE_SUCCESS).getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends DocumentTypeMasterBean.saveOrUpdate =======>");
		return showDocumentTypeMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts DocumentTypeMasterBean.onRowSelect ========>" + event);
		selectedDocumentTypeMaster = ((DocumentTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends DocumentTypeMasterBean.onRowSelect ========>");
	}

}
