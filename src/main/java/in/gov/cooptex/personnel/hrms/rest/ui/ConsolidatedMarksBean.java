package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.OralTestMarkSearchResponse;
import in.gov.cooptex.core.model.ConsolidatedMarks;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.rest.ui.master.OralTestMarkBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("consolidatedMarksBean")
public class ConsolidatedMarksBean {

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	LazyDataModel<ConsolidatedMarks> lazyApplicationModel;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	OralTestMarkBean oralTestMarkBean;

	@Getter
	@Setter
	String action, displayPopup;

	@Setter
	@Getter
	ConsolidatedMarks selectedConsolidatedMarks = new ConsolidatedMarks();

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	List<ConsolidatedMarks> consolidatedMarkList;

	@Setter
	@Getter
	ConsolidatedMarks consolidatedMarks = new ConsolidatedMarks();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	boolean disableButton;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String notficationNum, refPost;

	@Getter
	@Setter
	Long refYear, oralTestId;

	@Getter
	@Setter
	Boolean approveButtonFlag, deleteFlag, editButtonFlag, viewButtonFlag;

	@Getter
	@Setter
	Boolean generateButtonFlag = true;

	String url, jsonResponse;

	@Getter
	@Setter
	List<ConsolidatedMarks> consolidatedMarksList;

	public static final String CONSOLIDATE_SERVER_URL = AppUtil.getPortalServerURL() + "/consolidatedmarks";

	@PostConstruct
	public void init() {
		log.info("JobApplicationBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadSearchListOfValues();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public String showPage() {
		try {
			log.info("Show Employee Form Called with Action " + action);

			if (action.equalsIgnoreCase("APPROVAL")) {
				if (selectedConsolidatedMarks == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				}
				if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Rejected")) {
					errorMap.notify(ErrorDescription.JOB_APPLICATION_ALREADY_REJECTED.getErrorCode());
					return null;
				}
				notficationNum = selectedConsolidatedMarks.getOralTestMark().getNotificationNumber();
				refPost = selectedConsolidatedMarks.getWrittenExamMark().getRecruitmentPost();
				refYear = selectedConsolidatedMarks.getWrittenExamMark().getRecruitmentYear();
				oralTestId = selectedConsolidatedMarks.getOralTestMark().getId();
				Long conId = selectedConsolidatedMarks.getId();
				viewConsolidatedById(conId);
				log.info("notficationNum==>" + notficationNum);
				log.info("refPost==>" + refPost);
				log.info("refYear==>" + refYear);
				return "/pages/personnelHR/approveConslidatedMarkList.xhtml?faces-redirect=true";

			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info(":: consolidated mark Delete Response :: ");
				if (selectedConsolidatedMarks == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			} else if (action.equalsIgnoreCase("LISTPAGE")) {
				approveButtonFlag = true;
				editButtonFlag = true;
				viewButtonFlag = true;
				generateButtonFlag = true;
				mapper = new ObjectMapper();
				load();
				return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";

			}

		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String appointmentGenerationCancel() {
		approveButtonFlag = true;
		editButtonFlag = true;
		viewButtonFlag = true;
		generateButtonFlag = true;
		selectedConsolidatedMarks = new ConsolidatedMarks();
		load();
		return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
	}

	public void loadSearchListOfValues() {
		log.info("start loadSearchListOfValues ");
		try {
			designationList = commonDataService.loadDesignation();

		} catch (Exception exp) {
			log.error(" Exception Occured in while loading masters :: ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void load() {
		lazyApplicationModel = new LazyDataModel<ConsolidatedMarks>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<ConsolidatedMarks> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info(
							":: Pagination details First :{}, Page Size {}, Sort Field : {}, Sort Order : {}, Filter : {} ::",
							first, pageSize, sortField, sortOrder, filters);
					ConsolidatedMarks request = new ConsolidatedMarks();
					request.setFirst(first / pageSize);
					request.setPagesize(pageSize);
					request.setSortField(sortField);
					request.setSortOrder(sortOrder.toString());
					request.setFilters(filters);

					log.info("Search request data==>" + request);

					BaseDTO baseDTO = httpService.post(CONSOLIDATE_SERVER_URL + "/loadlazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					consolidatedMarksList = mapper.readValue(jsonResponse,
							new TypeReference<List<ConsolidatedMarks>>() {
							});
					if (consolidatedMarksList == null) {
						log.info(" :: Consolidated Marks Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: Consolidated Marks Search Successfully Completed ::");
					} else {
						log.error(":: Consolidated Marks Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}

				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return consolidatedMarksList;
			}

			@Override
			public Object getRowKey(ConsolidatedMarks res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ConsolidatedMarks getRowData(String rowKey) {
				for (ConsolidatedMarks res : consolidatedMarksList) {
					if (res.getId().equals(Long.valueOf(rowKey))) {
						selectedConsolidatedMarks = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void onRowSelect(SelectEvent event) {
		selectedConsolidatedMarks = ((ConsolidatedMarks) event.getObject());
		disableButton = true;
		if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Approved")) {
			deleteFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
			generateButtonFlag = true;
		} else if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Rejected")) {
			approveButtonFlag = false;
			deleteFlag = true;
			editButtonFlag = false;
			generateButtonFlag = false;
		} else if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("InProgress")) {
			editButtonFlag = true;
			approveButtonFlag = true;
			deleteFlag = true;
			viewButtonFlag = true;
			generateButtonFlag = false;
		}
	}

	/**
	 * @author krishnakumar
	 * @return
	 */
	public String viewConsolidatedMark() {
		try {
			if (selectedConsolidatedMarks == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}

			log.info("selectedConsolidatedMarks==>" + selectedConsolidatedMarks);
			log.info("selectedConsolidatedMarks id==>" + selectedConsolidatedMarks.getId());

			notficationNum = selectedConsolidatedMarks.getOralTestMark().getNotificationNumber();
			refPost = selectedConsolidatedMarks.getWrittenExamMark().getRecruitmentPost();
			refYear = selectedConsolidatedMarks.getWrittenExamMark().getRecruitmentYear();
			oralTestId = selectedConsolidatedMarks.getOralTestMark().getId();
			Long conId = selectedConsolidatedMarks.getId();
			viewConsolidatedById(conId);

			log.info("notficationNum==>" + notficationNum);
			log.info("refPost==>" + refPost);
			log.info("refYear==>" + refYear);

			return "/pages/personnelHR/viewConsolidateMarkList.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String showConsolidatePage() {
		log.info("<=showConsolidatePage start=>");
		try {
			OralTestMarkSearchResponse oralTestMarkSearchResponse = oralTestMarkBean.getSelectedOralTestMark();
			log.info("oralTestMarkSearchResponse==>" + oralTestMarkSearchResponse);
			if (oralTestMarkSearchResponse.getStatus().equalsIgnoreCase("APPROVED")) {
				notficationNum = oralTestMarkSearchResponse.getNotificationNo();
				refPost = oralTestMarkSearchResponse.getRecruitmentPost();
				refYear = oralTestMarkSearchResponse.getRecruitmentYear();
				getCanditateInfo(oralTestMarkSearchResponse.getId());
				return "/pages/personnelHR/generateConsolidateMarkList.xhtml?faces-redirect=true";
			} else {
				log.error("Approved The Oral Exam");
				errorMap.notify(ErrorDescription.ORAL_EXAM_ERROR.getErrorCode());
				return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private void viewConsolidatedById(Long id) {
		log.info("Inside viewConsolidatedById()");
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data loadJobApprovalList");
		try {
			String url = CONSOLIDATE_SERVER_URL + "/viewconsolidatedbyid/" + id;
			log.info("loadJobApplicationList==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			consolidatedMarks = mapper.readValue(jsonResponse, new TypeReference<ConsolidatedMarks>() {
			});

		} catch (Exception e) {
			log.error("Exception Occured in loadJobApplicationList search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	private void getCanditateInfo(Long oralTestMarkId) {
		log.info("Inside getCanditateInfo()");
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data loadJobApprovalList");
		try {
			String url = CONSOLIDATE_SERVER_URL + "/getexamdetails/" + oralTestMarkId;
			log.info("loadJobApplicationList==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			consolidatedMarkList = mapper.readValue(jsonResponse, new TypeReference<List<ConsolidatedMarks>>() {
			});
			totalRecords = consolidatedMarkList.size();
		} catch (Exception e) {
			log.error("Exception Occured in loadJobApplicationList search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * Purpose : to save the job details
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveConsolidatedMarks() {
		log.info("<---- : Save Consolidated Marks :------>" + consolidatedMarkList);
		try {

			url = CONSOLIDATE_SERVER_URL + "/create";
			BaseDTO baseDTO = httpService.post(url, consolidatedMarkList);
			log.info("Save Consolidated Marks Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			consolidatedMarkList = mapper.readValue(jsonResponse, new TypeReference<List<ConsolidatedMarks>>() {
			});
			log.info("ConsolidatedMarkList==>" + consolidatedMarkList);

			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.CONSOLIDATED_SAVED_SUCCUSSFULLY.getErrorCode());
				load();
				return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	public String backConsolidated() {
		try {
			approveButtonFlag = true;
			editButtonFlag = true;
			viewButtonFlag = true;
			load();
		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
	}

	public String deletedConsolidatedMark() {

		try {

			if (selectedConsolidatedMarks == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedConsolidatedMarks.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + selectedConsolidatedMarks.getId());
			String getUrl = CONSOLIDATE_SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedConsolidatedMarks.getId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Consolidated Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.CONSOLIDATED_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Consolidated Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			load();

		} catch (Exception e) {
			log.error(" Consolidated Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
	}

	public String clear() {
		log.info("job application clear method called..");
		selectedConsolidatedMarks = new ConsolidatedMarks();
		load();
		disableButton = false;
		deleteFlag = true;
		approveButtonFlag = true;
		editButtonFlag = true;
		viewButtonFlag = true;
		generateButtonFlag = true;
		return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
	}

	public String consolidatedChangeStatus() {
		try {

			log.info("id==>" + consolidatedMarks.getId());
			log.info("status==>" + consolidatedMarks.getStatus());
			log.info("remarks==>" + consolidatedMarks.getRemarks());
			url = CONSOLIDATE_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, consolidatedMarks);
			log.info("Save Consolidated Marks Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			selectedConsolidatedMarks = mapper.readValue(jsonResponse, new TypeReference<ConsolidatedMarks>() {
			});
			log.info("selectedConsolidatedMarks==>" + selectedConsolidatedMarks);

			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.CONSOLIDATED_UPDATED_SUCCUSSFULLY.getErrorCode());
				load();
				return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(" Exception Occured While consolidated Change Status  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listConsolidatedMarks.xhtml?faces-redirect=true";
	}

	public String callOralMark() {
		try {
			oralTestMarkBean.loadLazyEmployeeList();
		} catch (Exception e) {
			log.error(" Exception Occured While call Oral Mark  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true";
	}

	public String viewConDetails(ConsolidatedMarks consolidatedMark) {
		log.info("VIew.....");
		log.info("Inside getCanditateInfo()");
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data loadJobApprovalList");
		selectedConsolidatedMarks = consolidatedMark;
		try {
			long orid = consolidatedMark.getOralTestMark().getId();
			consolidatedMark.setOralMarkId(orid);
			log.info("orid==>" + orid);
			String url = CONSOLIDATE_SERVER_URL + "/getconexamdetails";
			log.info("viewConDetails==>" + url);
			baseDTO = httpService.post(url, consolidatedMark);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

			consolidatedMarkList = mapper.readValue(jsonResponse, new TypeReference<List<ConsolidatedMarks>>() {
			});
			if (displayPopup == null) {
				displayPopup = "TotalCandidates";
			}
			log.info("viewConDetails action==>" + action);
			if (displayPopup.equalsIgnoreCase("SelectedCandidates")) {
				consolidatedMarkList = consolidatedMarkList.stream()
						.filter(article -> article.getCandidateSelectedStatus() != null
								&& article.getCandidateSelectedStatus().equalsIgnoreCase("SELECTED"))
						.collect(Collectors.toList());
			}
			log.info("con size==>" + consolidatedMarkList.size());

		} catch (Exception e) {
			log.error(" Exception Occured While consolidated Change Status  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	public void callTotalSelectedCandidate() {

		log.info("<=callTotalSelectedCandidate=>");
		try {
			if (displayPopup != null && displayPopup.equalsIgnoreCase("SelectedCandidates")) {
				consolidatedMarkList = consolidatedMarkList.stream()
						.filter(article -> article.getCandidateSelectedStatus() != null
								&& article.getCandidateSelectedStatus().equalsIgnoreCase("SELECTED"))
						.collect(Collectors.toList());
				displayPopup = "SelectedCandidates";
			} else {
				displayPopup = "TotalCandidates";
			}
			log.info("con size state==>" + consolidatedMarkList.size());
		} catch (Exception e) {
			log.error(" Exception Occured While consolidated Change Status  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void approveOrRejectDialogue() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmStatus').show();");
	}

}
