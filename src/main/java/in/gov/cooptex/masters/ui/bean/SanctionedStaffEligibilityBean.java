package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.metamodel.EntityType;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.SanctionedStaffEligibility;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("sanctionedStaffEligibilityBean")
public class SanctionedStaffEligibilityBean {

	private final String CREATE_SANCTION_STRENGTH_PAGE = "/pages/masters/sanctionedStrength/createSanctionedStrength.xhtml?faces-redirect=true;";
	private final String SANCTION_STRENGTH_LIST_URL = "/pages/masters/sanctionedStrength/listSanctionedStrength.xhtml?faces-redirect=true;";
	private final String SANCTION_STRENGTH_VIEW_URL = "/pages/masters/sanctionedStrength/viewSanctionedStrength.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	LazyDataModel<SanctionedStaffEligibility> lazySanctionedStaffEligibilityModel;

	@Getter
	@Setter
	SanctionedStaffEligibility sanctionedStaffEligibility = new SanctionedStaffEligibility();

	@Getter
	@Setter
	SanctionedStaffEligibility selectedSanctionedStaffEligibility = new SanctionedStaffEligibility();

	@Getter
	@Setter
	List<SanctionedStaffEligibility> sanctionedStaffEligibilityList = new ArrayList<SanctionedStaffEligibility>();

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();

	@Getter
	@Setter
	List<Designation> designationList = new ArrayList<Designation>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<EntityTypeMaster>();

	@Getter
	@Setter
	List<FinancialYear> financialYearList = new ArrayList<FinancialYear>();

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	public String init() {
		log.info("Sanction Strength Bean Init is executed.....................");
		mapper = new ObjectMapper();
		loadLazySanctionStrengthList();
		entityTypeList = commonDataService.loadEntityTypes();
		departmentList = commonDataService.getDepartment();
		//designationList = commonDataService.loadActiveDesignations();
		addButtonFlag = true;
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		financialYearList = loadFinancialYear();
		return SANCTION_STRENGTH_LIST_URL;
	}

	public void loadDesignation() {
		try {
			if (sanctionedStaffEligibility.getDepartment() != null) {

				designationList = commonDataService.loadActiveDesignations();

				BaseDTO baseDTO = new BaseDTO();

				String url = SERVER_URL + "/designation/getalldesignation/"
						+ sanctionedStaffEligibility.getDepartment().getId();
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
					});
				}

			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void getEntity() {
		log.info("=====Sanction Strength Bean:=>getEntity Method Start=======");
		entityMasterList = commonDataService.loadEntityByEntityType(entityTypeMaster.getId());
		log.info("=====Sanction Strength Bean:=>getEntity Method End=======");
	}

	public List<FinancialYear> loadFinancialYear() {
		log.info("=====ReserveCreationsBean:=>getGlAccountGroup Method Start=======");
		BaseDTO baseDTO = new BaseDTO();
		List<FinancialYear> financialYearList = new ArrayList<FinancialYear>();
		try {
			String url = SERVER_URL + "/reserveCreations/getFinanceYear";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				financialYearList = mapper.readValue(jsonResponse, new TypeReference<List<FinancialYear>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
		return financialYearList;
	}

	public String saveSanctionStrength() {
		try {
			log.info("<=call saveEnquiryOfficer=>");
			log.info("<=sanctionedStaffEligibility=>" + sanctionedStaffEligibility);

			if (sanctionedStaffEligibility.getEntityType() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_REGION_REQUIRED.getErrorCode());
				return null;
			}
			if (sanctionedStaffEligibility.getDepartment() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_DEPARTMENT_REQUIRED.getErrorCode());
				return null;
			}
			if (sanctionedStaffEligibility.getDesignation() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_DESIGNATION_REQUIRED.getErrorCode());
				return null;
			}

			if (sanctionedStaffEligibility.getFinancialYear() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_FINYEAR_REQUIRED.getErrorCode());
				return null;
			}

			if (sanctionedStaffEligibility.getSactionedStrength() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_STRENGTH_REQUIRED.getErrorCode());
				return null;
			}

			if (sanctionedStaffEligibility.getStatus() == null) {
				errorMap.notify(ErrorDescription.SANCTION_STRENGTH_STATUS_REQUIRED.getErrorCode());
				return null;
			}

			String url = SERVER_URL + "/sanctionedstaff/create";
			BaseDTO baseDTO = httpService.post(url, sanctionedStaffEligibility);
			log.info("Save Enquiry Officer : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			SanctionedStaffEligibility sanctionedStaffEligibility = mapper.readValue(jsonResponse,
					SanctionedStaffEligibility.class);
			log.info("Enquiry Officer==>" + sanctionedStaffEligibility);
			if (baseDTO.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.SANCTIONED_STRENGTH_CREATED_SUCCESSFULLY).getCode());
				else
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.SANCTIONED_STRENGTH_UPDATED_SUCCESSFULLY).getCode());

				clear();
				loadLazySanctionStrengthList();
				return SANCTION_STRENGTH_LIST_URL;

			} else {
				log.info("Error while saving biometric with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in Sanction Strength  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadLazySanctionStrengthList() {

		log.info("<=sanctionedStaffEligibilityBean :: loadLazySanctionStrengthList=>");
		lazySanctionedStaffEligibilityModel = new LazyDataModel<SanctionedStaffEligibility>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<SanctionedStaffEligibility> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				SanctionedStaffEligibility request = new SanctionedStaffEligibility();
				try {
					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));
					log.info("Search request data" + request);
					String url = SERVER_URL + "/sanctionedstaff/lazyload/search";
					log.info("loadLazySanctionStrengthList url==>" + url);
					BaseDTO baseDTO = httpService.post(url, request);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					sanctionedStaffEligibilityList = mapper.readValue(jsonResponse,
							new TypeReference<List<SanctionedStaffEligibility>>() {
							});
					if (sanctionedStaffEligibilityList == null) {
						log.info(" :: loadLazySanctionStrengthList Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: totalRecords ::" + totalRecords);
					} else {
						log.error(":: loadLazySanctionStrengthList Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return sanctionedStaffEligibilityList;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading loadLazySanctionStrengthList ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(SanctionedStaffEligibility res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SanctionedStaffEligibility getRowData(String rowKey) {
				List<SanctionedStaffEligibility> responseList = (List<SanctionedStaffEligibility>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (SanctionedStaffEligibility res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedSanctionedStaffEligibility = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void onRowSelect() {
		addButtonFlag = false;

	}

	public void clear() {
		sanctionedStaffEligibility = new SanctionedStaffEligibility();
		selectedSanctionedStaffEligibility = new SanctionedStaffEligibility();
	}

	public String clearPage() {
		addButtonFlag = true;
		sanctionedStaffEligibility = new SanctionedStaffEligibility();
		selectedSanctionedStaffEligibility = new SanctionedStaffEligibility();
		loadLazySanctionStrengthList();
		entityMasterList = new ArrayList<EntityMaster>();
		return SANCTION_STRENGTH_LIST_URL;
	}

	@SuppressWarnings("unchecked")
	public String showSanctionStrengthData() {
		log.info("<===== Starts sanctionedStaffEligibilityBean.showSanctionStrengthData ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE") && (selectedSanctionedStaffEligibility == null
					|| selectedSanctionedStaffEligibility.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Enquiry Officer called..");
				return CREATE_SANCTION_STRENGTH_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit Sanction Strength Edit Method is  called..");
				sanctionedStaffEligibility = selectedSanctionedStaffEligibility;
				entityTypeMaster = selectedSanctionedStaffEligibility.getEntityType().getEntityTypeMaster();
				entityMasterList = commonDataService.loadEntityByEntityType(entityTypeMaster.getId());
				entityMaster = selectedSanctionedStaffEligibility.getEntityType();
				return CREATE_SANCTION_STRENGTH_PAGE;

			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete GlAccountCategory called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSanctionStrengthDelete').show();");

			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("<=EnquiryOfficerBean :: viewEnquiryOfficer=>");
				try {
					if (selectedSanctionedStaffEligibility == null) {
						errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
						return null;
					}
					sanctionedStaffEligibility = selectedSanctionedStaffEligibility;
					entityTypeMaster = selectedSanctionedStaffEligibility.getEntityType().getEntityTypeMaster();
					return SANCTION_STRENGTH_VIEW_URL;
				} catch (Exception e) {
					log.error(" Exception Occured While view Enquiry Officer :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showEnquriyOfficerData ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends sanctionedStaffEligibilityBean.showSanctionStrengthData ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		try {

			if (selectedSanctionedStaffEligibility == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("selected Enquiry Officer  Id : : " + selectedSanctionedStaffEligibility.getId());
			String getUrl = SERVER_URL + "/sanctionedstaff/delete/id/:id";
			String url = getUrl.replace(":id", selectedSanctionedStaffEligibility.getId().toString());
			log.info("deleteEnquiry Officer Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Roster Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SANCTIONED_STRENGTH_DELETED_SUCCESSFULLY).getCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			clear();
			init();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Sanction Strength  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return SANCTION_STRENGTH_LIST_URL;
	}

}
