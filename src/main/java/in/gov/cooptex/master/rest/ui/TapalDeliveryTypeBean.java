package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;

import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("tapalDeliveryTypeBean")
public class TapalDeliveryTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -176895761871211745L;

	private final String LIST_TAPALDELIVERYTYPE_MASTER_PAGE = "/pages/masters/tapaldeliverytype/listTapalDeliveryType.xhtml?faces-redirect=true;";
	private final String CREATE_TAPALDELIVERYTYPE_MASTER_PAGE = "/pages/masters/tapaldeliverytype/createTapalDeliveryType.xhtml?faces-redirect=true;";

	private final String VIEW_TAPALDELIVERYTYPE_MASTER_PAGE = "/pages/masters/tapaldeliverytype/viewTapalDeliveryType.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	Boolean addButtonFlag=true;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	TapalDeliveryType tapalDeliveryType,selectedTapalDeliveryType;
	
	

	@Getter
	@Setter
	TapalDeliveryType selectedtapalDeliveryType = new TapalDeliveryType();
	
	@Getter
	@Setter
	LazyDataModel<TapalDeliveryType> lazyTapalDeliveryTypeList;

	@Getter
	@Setter
	List<TapalDeliveryType> tapalDeliveryTypeList = new ArrayList<TapalDeliveryType>();
	
	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	private int tapalDeliveryTypeListSize;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	

	boolean forceLogin = false;

	@PostConstruct
	public void init() {

		log.info(" CountryMasterBean init --->" + serverURL);
		loadValues();
		addTapalDeliveryType();
		getAllTapalDeliveryTypeList();
		loadLazyTapalSenderTypeList();

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			tapalDeliveryType = new TapalDeliveryType();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String addTapalDeliveryType() {
		log.info("<---- Start CountryMasterBean.addCountry ---->");
		try {
			action = "ADD";
			tapalDeliveryType = new TapalDeliveryType();
			tapalDeliveryType.setActiveStatus(true);

			log.info("<---- End CountryMaster.addCountry ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_TAPALDELIVERYTYPE_MASTER_PAGE;

	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		tapalDeliveryType = new TapalDeliveryType();
		return LIST_TAPALDELIVERYTYPE_MASTER_PAGE;

	}

	public String submit() {
		log.info("...... TapalDeliveryType submit begin ....");
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + appPreference.getOperationApiUrl() + "/tapaldeliverytype/create",
					tapalDeliveryType);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("TapalDeliveryType saved successfully");
					if(action.equals("ADD")) {
						errorMap.notify(ErrorDescription.TAPAL_DELIVERY_TYPE_SAVE_SUCCESS.getErrorCode());
					}else if(action.equals("EDIT")) {
						errorMap.notify(ErrorDescription.TAPAL_DELIVERY_TYPE_UPDATE_SUCCESS.getErrorCode());
					}
					showList();
					return LIST_TAPALDELIVERYTYPE_MASTER_PAGE;
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating TapalDeliveryType" + e);
		}
		log.info("...... TapalDeliveryType submit ended ....");
		return null;
	}

	private void getAllTapalDeliveryTypeList() {
		log.info("<<<< ----------Start TapalDeliveryType-getAllTapalDeliveryTypeList() ------- >>>>");
		try {
			tapalDeliveryTypeList = new ArrayList<TapalDeliveryType>();
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/tapaldeliverytype/getActiveTapalDeliveryType";
			baseDTO = httpService.get(url);
			tapalDeliveryType = new TapalDeliveryType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalDeliveryTypeList = mapper.readValue(jsonResponse, new TypeReference<List<TapalDeliveryType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tapalDeliveryTypeList != null) {
			tapalDeliveryTypeListSize = tapalDeliveryTypeList.size();
		} else {
			tapalDeliveryTypeListSize = 0;
		}
		log.info("<<<< ----------End TapalDeliveryType . getAllTapalDeliveryTypeList ------- >>>>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TapalDeliveryType.onRowSelect ========>" + event);
		selectedtapalDeliveryType = ((TapalDeliveryType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends TapalDeliveryType.onRowSelect ========>");
	}
	


	public String editTapalDeliveryType() {
		log.info("<<<<< -------Start TapalDeliveryType called ------->>>>>> ");
		action = "EDIT";

		if (tapalDeliveryType == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}

		if (tapalDeliveryType.getActiveStatus().equals("ACTIVE")) {
			tapalDeliveryType.setActiveStatus(tapalDeliveryType.getActiveStatus());
		} else {
			tapalDeliveryType.setActiveStatus(tapalDeliveryType.getActiveStatus());
		}
		log.info("<<<<< -------End TapalDeliveryType called ------->>>>>> ");

		return CREATE_TAPALDELIVERYTYPE_MASTER_PAGE;

	}
	
	
	
	public String viewTapalDeliveryType() {
		try {
			action = "View";
			if (tapalDeliveryType == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			{
				return  VIEW_TAPALDELIVERYTYPE_MASTER_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	

	private void getTapalDeliveryTypeView() {
		log.info("<<<< ----------Start TapalDeliveryTypeBean . getTapalDeliveryTypeView() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			
			String url = serverURL + appPreference.getOperationApiUrl() + "/tapaldeliverytype/gettapalDeliveryType";
			baseDTO = httpService.post(url, tapalDeliveryType);
			tapalDeliveryType = new TapalDeliveryType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tapalDeliveryType = mapper.readValue(jsonResponse, new TypeReference<TapalDeliveryType>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tapalDeliveryType != null)
			log.info("TapalDeliveryTypeBean getTapalDeliveryTypeView" + tapalDeliveryType);
		else
			log.info("TapalDeliveryTypeBean getTapalDeliveryTypeView" + tapalDeliveryType);

		log.info("<<<< ----------End TapalDeliveryTypeBean . getTapalDeliveryTypeView ------- >>>>");
	}

	public String showList() {
		log.info("<---------- Loading TapalDeliveryTypeBean showList list page---------->");
		addButtonFlag = true;
		loadValues();
		getAllTapalDeliveryTypeList();
		loadLazyTapalSenderTypeList();
		return LIST_TAPALDELIVERYTYPE_MASTER_PAGE;
	}

	public String backPage() {
		showList();
		return LIST_TAPALDELIVERYTYPE_MASTER_PAGE;
	}

	public String deleteTapalDeliveryType() {
		try {
			action = "Delete";
			if (tapalDeliveryType == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTapalDeliverTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	public String clear() {
		log.info("<---- Start TapalDeliveryTypeBean . clear ----->");
		getAllTapalDeliveryTypeList();
		loadLazyTapalSenderTypeList();
		tapalDeliveryType = new TapalDeliveryType();
		addButtonFlag = true;
		log.info("<---- End TapalDeliveryTypeBean . clear ----->");

		return LIST_TAPALDELIVERYTYPE_MASTER_PAGE;

	}

	public String deleteTapalDeliveryTypeConfirm() {

		try {
			log.info("Delete confirmed for Tapal Delivery Type [" + tapalDeliveryType.getName() + "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/tapaldeliverytype/delete";
			baseDTO = httpService.post(url, tapalDeliveryType);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Tapal Delivery Type deleted successfully....!!!");
					errorMap.notify(ErrorDescription.TAPAL_DELIVERY_TYPE_DELETE_SUCCESS.getErrorCode());
					tapalDeliveryType = new TapalDeliveryType();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} 
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	
	public void loadLazyTapalSenderTypeList() {
		log.info("<===== Starts TapalDeliveryTypeBean.loadLazyTapalDeliveryTypeList ======>");
		lazyTapalDeliveryTypeList = new LazyDataModel<TapalDeliveryType>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TapalDeliveryType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl() +"/tapaldeliverytype/search";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						tapalDeliveryTypeList = mapper.readValue(jsonResponse,new TypeReference<List<TapalDeliveryType>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyTapalDeliveryTypeList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tapalDeliveryTypeList;
			}

			@Override
			public Object getRowKey(TapalDeliveryType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TapalDeliveryType getRowData(String rowKey) {
				try {
					for (TapalDeliveryType glAccountCategory : tapalDeliveryTypeList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedTapalDeliveryType = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TapalDeliveryTypeBean.loadLazyTapalDeliveryTypeList ======>");
	}

}
