package in.gov.cooptex.master.rest.ui;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.NatureOfDeathErrorCode;
import in.gov.cooptex.personnel.hrms.model.NatureOfDeath;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("natureOfDeathBean")
@Scope("session")
@Log4j2
public class NatureOfDeathBean {

	final String LIST_PAGE = "/pages/masters/listNatureOfDeath.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createNatureOfDeath.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String NATURE_OF_DEATH_URL = AppUtil.getPortalServerURL() + "/natureofdeath";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	NatureOfDeath natureOfDeath, selectedNatureOfDeath;

	@Getter
	@Setter
	LazyDataModel<NatureOfDeath> lazyNatureOfDeathList;

	List<NatureOfDeath> natureOfDeathList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showNatureOfDeathListPage() {
		log.info("<==== Starts NatureOfDeathBean.showNatureOfDeathListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		natureOfDeath = new NatureOfDeath();
		selectedNatureOfDeath = new NatureOfDeath();
		loadLazyNatureOfDeathList();
		log.info("<==== Ends NatureOfDeathBean.showNatureOfDeathListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyNatureOfDeathList() {
		log.info("<===== Starts NatureOfDeathBean.loadLazyNatureOfDeathList ======>");

		lazyNatureOfDeathList = new LazyDataModel<NatureOfDeath>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<NatureOfDeath> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = NATURE_OF_DEATH_URL + "/getallnatureofdeathlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						natureOfDeathList = mapper.readValue(jsonResponse, new TypeReference<List<NatureOfDeath>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return natureOfDeathList;
			}

			@Override
			public Object getRowKey(NatureOfDeath res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public NatureOfDeath getRowData(String rowKey) {
				for (NatureOfDeath natureOfDeath : natureOfDeathList) {
					if (natureOfDeath.getId().equals(Long.valueOf(rowKey))) {
						selectedNatureOfDeath = natureOfDeath;
						return natureOfDeath;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends NatureOfDeathBean.loadLazyNatureOfDeathList ======>");
	}

	public String showNatureOfDeathListPageAction() {
		log.info("<===== Starts NatureOfDeathBean.showNatureOfDeathListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedNatureOfDeath == null || selectedNatureOfDeath.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(NatureOfDeathErrorCode.SELECT_NATURE_OF_DEATH).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create natureOfDeath called..");
				natureOfDeath = new NatureOfDeath();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit natureOfDeath called..");
				String url = NATURE_OF_DEATH_URL + "/getbyid/" + selectedNatureOfDeath.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					natureOfDeath = mapper.readValue(jsonResponse, new TypeReference<NatureOfDeath>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete natureOfDeath called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmNatureOfDeathDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showNatureOfDeathListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends NatureOfDeathBean.showNatureOfDeathListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts NatureOfDeathBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(NATURE_OF_DEATH_URL + "/deletebyid/" + selectedNatureOfDeath.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_DELETE_SUCCESS)
						.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmNatureOfDeathDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete natureOfDeath ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends NatureOfDeathBean.deleteConfirm ===========>");
		return showNatureOfDeathListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts NatureOfDeathBean.saveOrUpdate =======>");
		try {
			if (natureOfDeath.getCode() == null || natureOfDeath.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_CODE_EMPTY).getCode());
				return null;
			}
			if (natureOfDeath.getName() == null || natureOfDeath.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_NAME_EMPTY).getCode());
				return null;
			}
			if (natureOfDeath.getName().trim().length() < 3 || natureOfDeath.getName().trim().length() > 75) {
				errorMap.notify(
						ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (natureOfDeath.getLocalName() == null || natureOfDeath.getLocalName().trim().isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_LNAME_EMPTY).getCode());
				return null;
			}
			if (natureOfDeath.getLocalName().trim().length() < 3 || natureOfDeath.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_LNAME_MIN_MAX_ERROR)
						.getCode());
				return null;
			}
			if (natureOfDeath.getActiveStatus() == null) {
				errorMap.notify(
						ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_STATUS_EMPTY).getCode());
				return null;
			}
			String url = NATURE_OF_DEATH_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, natureOfDeath);
			if (response != null && response.getStatusCode() == 0) {
				log.info("natureOfDeath saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(
							ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(
							ErrorDescription.getError(NatureOfDeathErrorCode.NATURE_OF_DEATH_UPDATE_SUCCESS).getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends NatureOfDeathBean.saveOrUpdate =======>");
		return showNatureOfDeathListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts NatureOfDeathBean.onRowSelect ========>" + event);
		selectedNatureOfDeath = ((NatureOfDeath) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends NatureOfDeathBean.onRowSelect ========>");
	}

}
