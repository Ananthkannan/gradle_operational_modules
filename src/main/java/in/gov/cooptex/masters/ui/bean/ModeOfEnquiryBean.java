package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.ModeOfEnquiry;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("modeOfEnquiryBean")
@Scope("session")
@Log4j2
public class ModeOfEnquiryBean {

	final String LIST_PAGE = "/pages/masters/listModeOfEnquiry.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createModeOfEnquiry.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/viewModeOfEnquiry.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String MODE_OF_ENQUIRY_URL = AppUtil.getPortalServerURL() + "/modeofenquiry";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	ModeOfEnquiry modeOfEnquiry, selectedModeOfEnquiry;

	@Getter
	@Setter
	LazyDataModel<ModeOfEnquiry> lazyModeOfEnquiryList;

	List<ModeOfEnquiry> modeOfEnquiryList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	

	public String showModeOfEnquiryListPage() {
		log.info("<==== Starts ModeOfEnquiryBean.showModeOfEnquiryListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		modeOfEnquiry = new ModeOfEnquiry();
		selectedModeOfEnquiry = new ModeOfEnquiry();
		loadLazyModeOfEnquiryList();
		log.info("<==== Ends ModeOfEnquiryBean.showModeOfEnquiryListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyModeOfEnquiryList() {
		log.info("<===== Starts ModeOfEnquiryBean.loadLazyModeOfEnquiryList ======>");

		lazyModeOfEnquiryList = new LazyDataModel<ModeOfEnquiry>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ModeOfEnquiry> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = MODE_OF_ENQUIRY_URL + "/getallmodeofenquirylistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						modeOfEnquiryList = mapper.readValue(jsonResponse, new TypeReference<List<ModeOfEnquiry>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return modeOfEnquiryList;
			}

			@Override
			public Object getRowKey(ModeOfEnquiry res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ModeOfEnquiry getRowData(String rowKey) {
				for (ModeOfEnquiry modeOfEnquiry : modeOfEnquiryList) {
					if (modeOfEnquiry.getId().equals(Long.valueOf(rowKey))) {
						selectedModeOfEnquiry = modeOfEnquiry;
						return modeOfEnquiry;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends ModeOfEnquiryBean.loadLazyModeOfEnquiryList ======>");
	}

	public String showModeOfEnquiryListPageAction() {
		log.info("<===== Starts ModeOfEnquiryBean.showModeOfEnquiryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedModeOfEnquiry == null || selectedModeOfEnquiry.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create modeOfEnquiry called..");
				modeOfEnquiry = new ModeOfEnquiry();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit") || action.equalsIgnoreCase("View")) {
				log.info("edit modeOfEnquiry called..");
				String url = MODE_OF_ENQUIRY_URL + "/getbyid/" + selectedModeOfEnquiry.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					modeOfEnquiry = mapper.readValue(jsonResponse, new TypeReference<ModeOfEnquiry>() {
					});
					if(action.equalsIgnoreCase("View")) {
						return VIEW_PAGE;
					}else {
						return ADD_PAGE;
					}
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			
			}
			else {
				log.info("delete modeOfEnquiry called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmModeOfEnquiryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showModeOfEnquiryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends ModeOfEnquiryBean.showModeOfEnquiryListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts ModeOfEnquiryBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(MODE_OF_ENQUIRY_URL + "/deletebyid/" + selectedModeOfEnquiry.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmModeOfEnquiryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete modeOfEnquiry ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ModeOfEnquiryBean.deleteConfirm ===========>");
		return showModeOfEnquiryListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts ModeOfEnquiryBean.saveOrUpdate =======>");
		try {
			if (modeOfEnquiry.getCode() == null || modeOfEnquiry.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_CODE_EMPTY.getCode());
				return null;
			}
			if (modeOfEnquiry.getName() == null || modeOfEnquiry.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_NAME_EMPTY.getCode());
				return null;
			}
			if (modeOfEnquiry.getName().trim().length() < 3 || modeOfEnquiry.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (modeOfEnquiry.getLocalName() == null || modeOfEnquiry.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_LNAME_EMPTY.getCode());
				return null;
			}
			if (modeOfEnquiry.getLocalName().trim().length() < 3 || modeOfEnquiry.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (modeOfEnquiry.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_STATUS_EMPTY.getCode());
				return null;
			}
			String url = MODE_OF_ENQUIRY_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, modeOfEnquiry);
			if (response != null && response.getStatusCode() == 0) {
				log.info("modeOfEnquiry saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.MODE_OF_ENQUIRY_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends ModeOfEnquiryBean.saveOrUpdate =======>");
		return showModeOfEnquiryListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ModeOfEnquiryBean.onRowSelect ========>" + event);
		selectedModeOfEnquiry = ((ModeOfEnquiry) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		log.info("<===Ends ModeOfEnquiryBean.onRowSelect ========>");
	}

}
