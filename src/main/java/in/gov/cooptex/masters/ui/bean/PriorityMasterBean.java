package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.PriorityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("priorityMasterBean")
@Scope("session")
@Log4j2
public class PriorityMasterBean {
	
	final String LIST_PAGE = "/pages/masters/priority/listPriority.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/priority/addPriority.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/priority/viewPriority.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String PRIORITY_URL = AppUtil.getPortalServerURL()+"/priority";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	PriorityMaster priorityMaster,selectedPriorityMaster;

	@Getter @Setter
	LazyDataModel<PriorityMaster> lazyPriorityMasterList;

	List<PriorityMaster> priorityMasterList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showPriorityListPage() {
		log.info("<==== Starts PriorityMasterBean.showPriorityListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag=true;
		priorityMaster = new PriorityMaster();
		selectedPriorityMaster= new PriorityMaster();
		loadLazyPriorityMasterList();
		log.info("<==== Ends PriorityMasterBean.showPriorityListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyPriorityMasterList() {
		log.info("<===== Starts PriorityMasterBean.loadLazyPriorityList ======>");

		lazyPriorityMasterList= new LazyDataModel<PriorityMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 3715469764921944412L;


			@Override
			public List<PriorityMaster> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					log.info("Pagination request :::"+paginationRequest.getFilters());
					
					String url = PRIORITY_URL+ "/getlazyPriorityMasterList";
					
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						priorityMasterList= mapper.readValue(jsonResponse, new TypeReference<List<PriorityMaster>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return priorityMasterList;
			}

			@Override
			public Object getRowKey(PriorityMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PriorityMaster getRowData(String rowKey) {
				for (PriorityMaster priorityMaster : priorityMasterList) {
					if (priorityMaster .getId().equals(Long.valueOf(rowKey))) {
						selectedPriorityMaster= priorityMaster ;
						return priorityMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  PriorityMasterBean.loadLazyPriorityList  ======>");
	}

public String showPriorityListPageAction() {
		log.info("<===== Starts PriorityMasterBean.showPriorityListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("Create") && (selectedPriorityMaster== null || selectedPriorityMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("Create")) {
				log.info("create Priority called..");
			priorityMaster= new PriorityMaster();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit Priority called..");
				String url=PRIORITY_URL+"/fetchbyid/"+selectedPriorityMaster.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					priorityMaster= mapper.readValue(jsonResponse,new TypeReference<PriorityMaster>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else if(action.equalsIgnoreCase("View")){
				log.info("View Priority called..");

				String url=PRIORITY_URL+"/fetchbyid/"+selectedPriorityMaster.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					priorityMaster= mapper.readValue(jsonResponse,new TypeReference<PriorityMaster>(){
					});	
					return VIEW_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete Priority called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPriorityDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showPriorityListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends PriorityMasterBean.showPriorityListPageAction ===========>"+action);
		return null;
	}



	public String deleteConfirm() {
		log.info("<===== Starts PriorityMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(PRIORITY_URL+"/deletebyid/"+selectedPriorityMaster.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.PRIORITY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPriorityDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete salesType ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends PriorityMasterBean.deleteConfirm ===========>");
		return  showPriorityListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts PriorityMasterBean.saveOrUpdate =======>");
		String url="";
		try {
			BaseDTO response=new BaseDTO() ;
			if(priorityMaster.getPriorityCode() == null || priorityMaster.getPriorityCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.PRIORITY_CODE_NOT_NULL.getCode());
				return null;
			}
			if(priorityMaster.getPriorityName() == null || priorityMaster.getPriorityName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.PRIORITY_NAME_NOT_NULL.getCode());
				return null;
			}
			if(priorityMaster.getStatus() == null) {
				errorMap.notify(ErrorDescription.PRIORITY_STATUS_EMPTY.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("Create")) {
			 url = PRIORITY_URL+"/create";
			 response = httpService.post(url,priorityMaster);
			}
			else {
			 url = PRIORITY_URL+"/update";
			 response = httpService.put(url,priorityMaster);
			}
			
			
			if(response != null && response.getStatusCode() == 0) {
				log.info("priorityMaster saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.PRIORITY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.PRIORITY_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends PriorityMasterBean.saveOrUpdate =======>");
		return showPriorityListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PriorityMasterBean.onRowSelect ========>"+event);
		selectedPriorityMaster= ((PriorityMaster) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends PriorityMasterBean.onRowSelect ========>");
    }
	
}
