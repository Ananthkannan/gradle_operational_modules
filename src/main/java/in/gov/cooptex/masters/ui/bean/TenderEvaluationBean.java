package in.gov.cooptex.masters.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.TenderCategory;
import in.gov.cooptex.core.model.TenderType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.TenderEvalDTO;
import in.gov.cooptex.dto.pos.TenderEvalGenDTO;
import in.gov.cooptex.dto.pos.TenderListDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.tender.model.Tender;
import in.gov.cooptex.operation.tender.model.TenderFileDetails;
import in.gov.cooptex.operation.tender.model.TenderItemDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("tenderEvaluationBean")
@Scope("session")
@Log4j2
public class TenderEvaluationBean {

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	LazyDataModel<TenderListDTO> lazyTenderListDTOList;

	private String serverURL = AppUtil.getPortalServerURL();

	private final String VIEW_URL = "/tender/view";

	@Getter
	@Setter
	AddressMaster bidopeningAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster bidMeetingAddress = new AddressMaster();

	@Getter
	@Setter
	TenderListDTO selectedTender = new TenderListDTO();

	@Getter
	@Setter
	private int tenderListSize;

	@Getter
	@Setter
	Tender selectedTenderr;

	@Getter
	@Setter
	List<TenderListDTO> tenderList = new ArrayList<>();

	@Getter
	@Setter
	List<TenderType> tenderTypelist = new ArrayList<>();

	@Getter
	@Setter
	TenderType tenderType = new TenderType();

	@Getter
	@Setter
	List<TenderCategory> tenderCategorylist = new ArrayList<>();

	@Getter
	@Setter
	Tender tender = new Tender();

	@Getter
	@Setter
	TenderEvalDTO tenderEvalDTO = new TenderEvalDTO();

	@Getter
	@Setter
	List<TenderEvalDTO> tenderEvalDTOList = new ArrayList<>();

	@Setter
	@Getter
	private StreamedContent files;
	
	@Getter
	@Setter
	Boolean listFlag;

	@Getter
	@Setter
	List<String> supplierList = new ArrayList<>();

	TenderEvalGenDTO tenderEvalGenDTO = new TenderEvalGenDTO();
	
	@Getter
	@Setter
	boolean renderTenderCreate;
	
	@Getter
    @Setter
	List<TenderEvalDTO> tenderFileDetailsList = new ArrayList<>();

	@Getter
	@Setter
	List<TenderEvalGenDTO> tenderEvalGenDTOList = new ArrayList<>();

	private final String VIEW_PAGE_TENDEREVALUATION = "/pages/operation/tender/viewTenderEvaluation.xhtml?faces-redirect=true";

	@PostConstruct
	public void init() {
		log.info("TenderEvaluationBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadTenderType();
		loadTenderCategory();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadTenderType() {
		log.info("<=TenderEvaluationBean loadTenderType start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/tenderType/getallactive";
			log.info("loadTenderType url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderTypelist = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
				});
				if (tenderTypelist != null && !tenderTypelist.isEmpty()) {
					log.info("tenderTypelist size==> " + tenderTypelist.size());
				} else {
					log.error("Tender type not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadTenderType  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadTenderCategory() {
		log.info("<=TenderEvaluationBean loadTenderCategory start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/tenderCategory/getallactive";
			log.info("loadTenderCategory url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderCategorylist = mapper.readValue(jsonResponse, new TypeReference<List<TenderCategory>>() {
				});
				if (tenderCategorylist != null && !tenderCategorylist.isEmpty()) {
					log.info("tenderCategorylist size==> " + tenderCategorylist.size());
				} else {
					log.error("Tender category not found");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadTenderCategory  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String loadTenderListPage() {
		log.info("TenderEvaluationBean loadTenderListPage.....");
		try {
			clearEval();
			getAllTenderList();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/operation/tender/listTenderEvalPage.xhtml?faces-redirect=true";

	}
	
	public void downloadfile(TenderFileDetails tenderFileDetails) {
		InputStream input = null;
		try {
			if (tenderFileDetails.getFilePath() != null) {
				File file = new File(tenderFileDetails.getFilePath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				files = (new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()),
						file.getName()));
				log.error(" File Downloaded >>>>>>>>>>------- " + files.getName());

			}
		} catch (Exception e) {
			log.error("Exception Occured IN DownloadFile", e);
		}

	}


	private void getAllTenderList() {
		log.info("<===== Starts TenderEvaluationBean.lazyTenderList ======>");
		lazyTenderListDTOList = new LazyDataModel<TenderListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TenderListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = AppUtil.getPortalServerURL() + "/tenderevaluation/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderList = mapper.readValue(jsonResponse, new TypeReference<List<TenderListDTO>>() {
						});
						if (tenderList != null && !tenderList.isEmpty()) {
							log.info("tenderList size==> " + tenderList.size());
						} else {
							log.error("Tender list not found");
						}
						this.setRowCount(response.getTotalRecords());
						tenderListSize = response.getTotalRecords();
						log.info("tenderListSize==> " + tenderListSize);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in getAllTenderList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tenderList;
			}

			@Override
			public Object getRowKey(TenderListDTO res) {
				return res != null ? res.getTenderId() : null;
			}

			@Override
			public TenderListDTO getRowData(String rowKey) {
				try {
					for (TenderListDTO tenderObj : tenderList) {
						if (tenderObj.getTenderId().equals(Long.valueOf(rowKey))) {
							selectedTender = tenderObj;
							return tenderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in TenderEvaluationBean getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TenderEvaluationBean.lazyTenderList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TenderEvaluationBean.onRowSelect ========>" + event);
		selectedTender = ((TenderListDTO) event.getObject());
		log.info("tender status=> " + selectedTender.getTenderStatus());
		if (selectedTender.getTenderStatus().equalsIgnoreCase("Tender Applied")) {
			listFlag = true;
		} else {
			listFlag = false;
		}
		log.info("<===Ends TenderEvaluationBean.onRowSelect ========>");
	}

	public String viewTender() {
		selectedTenderr = new Tender();
		log.info("<----Redirecting tenderEvaluationBean ---->");
		if (selectedTender == null) {
			Util.addWarn("Please Select One Tender");
			return null;
		}
		selectedTenderr.setId(selectedTender.getTenderId());

		getTenderView(selectedTenderr);

		return VIEW_PAGE_TENDEREVALUATION;

	}

	private void getTenderView(Tender selectedTender) {
		log.info("<<<< ----------Start tenderEvaluationBean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + VIEW_URL;
			baseDTO = httpService.post(url, selectedTender);
			selectedTenderr = new Tender();
			Double totalAmount = 0.0;

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedTenderr = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

				bidopeningAddress = selectedTenderr.getBidMeetingAddressId();
				log.info("bidopeningAddress id==> " + bidopeningAddress.getId());

				bidMeetingAddress = selectedTenderr.getBidMeetingAddressId();

				log.info("bidMeetingAddress id==> " + bidMeetingAddress.getId());

			}
			for (TenderItemDetails item : selectedTenderr.getTenderItemDetailsList()) {
				totalAmount += item.getItemAmount();
			}
			selectedTenderr.setTotalAmount(totalAmount);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedTender != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End tenderEvaluationBean-getTenderView ------- >>>>");
	}

	public String showAddPage() {
		log.info("<=TenderEvaluationBean showAddPage start=>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (selectedTender == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String url = AppUtil.getPortalServerURL() + "/tender/get/" + selectedTender.getTenderId();
			log.info("TenderEvaluationBean showAddPage url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});
				return "/pages/operation/tender/evalutionTender.xhtml?faces-redirect=true";

			} else {
				tender = new Tender();
			}

		} catch (Exception e) {
			log.error("Exception in showAddPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String gotoList() {
		try {
			selectedTender = new TenderListDTO();
			tender = new Tender();
			tenderList = new ArrayList<>();
			listFlag = false;

		} catch (Exception e) {
			log.error("Exception in gotoList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/operation/tender/listTenderEvalPage.xhtml?faces-redirect=true";
	}

	public void loadTenderEvalDetails() {
		log.info("<===== Start TenderEvaluationBean.loadTenderEvalDetails ======>");
		BaseDTO baseDto = new BaseDTO();
		tenderEvalDTOList = new ArrayList<>();
		supplierList = new ArrayList<>();
		TenderEvalDTO tenderEvalDto = new TenderEvalDTO();

		try {
			if (tender == null || tender.getId() == null) {
				// AppUtil.addWarn("Tender value not empty");
				errorMap.notify(ErrorDescription.TENDER_VALUE_NOT_EMPTY.getErrorCode());
				return;
			}
			if (tenderType == null || tenderType.getId() == null) {
				// AppUtil.addWarn("Tender type value not empty");
				errorMap.notify(ErrorDescription.TENDER_TYPE_VALUE_NOT_EMPTY.getErrorCode());
				return;
			}
			String url = AppUtil.getPortalServerURL() + "/tenderevaluation/getevaldetails/" + tender.getId() + "/"
					+ tenderType.getId();
			log.info("loadTenderEvalDetails url==>" + url);
			baseDto = httpService.get(url);
			if (baseDto != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
				tenderEvalDto = mapper.readValue(jsonResponse, new TypeReference<TenderEvalDTO>() {
				});
			}
			if (tenderEvalDto != null) {
				supplierList = tenderEvalDto.getTotalSupplier();
				tenderEvalDTOList = tenderEvalDto.getResponseDto();
			} else {
				clearEval();
			}
		} catch (Exception e) {
			log.info("<--- Exception in getSalesReport() --->", e);
		}
		log.info("<===== End TenderEvaluationBean.loadTenderEvalDetails ======>");
	}

	public String saveEvaluation() {
		log.info("<===== Start TenderEvaluationBean.saveEvaluation ======>");
		try {
			if (tenderType == null || tenderType.getId() == null) {
				// AppUtil.addWarn("Tender type value not empty");
				errorMap.notify(ErrorDescription.TENDER_TYPE_VALUE_NOT_EMPTY.getErrorCode());
				return null;
			}
			if (tenderEvalDTOList == null || tenderEvalDTOList.isEmpty()) {
				// AppUtil.addWarn("Supplier information not empty");
				errorMap.notify(ErrorDescription.SUPPLIER_INFO_NOT_EMPTY.getErrorCode());
				return null;
			}
			generateTenderEval();

			String url = AppUtil.getPortalServerURL() + "/tenderevaluation/create";
			BaseDTO baseDTO = httpService.put(url, tenderEvalGenDTOList);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TENDER_EVAL_INSERTED_SUCCESSFULLY.getErrorCode());
				clearEval();
				return "/pages/operation/tender/listTenderEvalPage.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving tender evaluation with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.info("<--- Exception in saveEvaluation() --->", e);
		}
		return null;
	}

	public void generateTenderEval() {
		log.info("<===== Start TenderEvaluationBean.generateTenderEval ======>");
		BaseDTO baseDto = new BaseDTO();
		try {
			if (tender == null || tender.getId() == null) {
				// AppUtil.addWarn("Tender value not empty");
				errorMap.notify(ErrorDescription.TENDER_VALUE_NOT_EMPTY.getErrorCode());
				return;
			}
			if (tenderType == null || tenderType.getId() == null) {
				// AppUtil.addWarn("Tender type value not empty");
				errorMap.notify(ErrorDescription.TENDER_TYPE_VALUE_NOT_EMPTY.getErrorCode());
				return;
			}
			String url = AppUtil.getPortalServerURL() + "/tenderevaluation/generatetendereval/" + tender.getId() + "/"
					+ tenderType.getId();
			log.info("TenderEvaluationBean generateTenderEval url==>" + url);
			baseDto = httpService.get(url);
			if (baseDto != null) {
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
				tenderEvalGenDTOList = mapper.readValue(jsonResponse, new TypeReference<List<TenderEvalGenDTO>>() {
				});
				log.info("size==>" + tenderEvalGenDTOList.size());

			}

		} catch (Exception e) {
			log.info("<--- Exception in getSalesReport() --->", e);
		}
		log.info("<===== End TenderEvaluationBean.generateTenderEval ======>");
	}

	public void clearEval() {
		tenderType = new TenderType();
		selectedTender = new TenderListDTO();
		tenderEvalDTOList = new ArrayList<>();
		supplierList = new ArrayList<>();
		tender = new Tender();
		tenderList = new ArrayList<>();
		listFlag = false;
	}

}
