package in.gov.cooptex.operation.rest.ui;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.SalesType;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.SalesInvoiceDto;
import in.gov.cooptex.operation.dto.SalesInvoiceItemDTO;
import in.gov.cooptex.operation.enums.QuotationStatus;
import in.gov.cooptex.operation.model.SalesInvoice;
import in.gov.cooptex.operation.model.SalesInvoiceItems;
import in.gov.cooptex.operation.model.SalesOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("contractSalesInvoiceBean")
@Scope("session")
public class ContractSalesInvoiceBean {
	
	
	private Double itemQuantityValue =0.0;
	
	private boolean checkInventoryItem = false;
	
	private int productVarityQuantityIndex;

	@Getter
	@Setter
	String action, totalAmountInWords = null, invoiceNumberPrefix = null;

	@Getter
	@Setter
	Date curDate = new Date();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	@Getter
	@Setter
	Long invoiceNumber = null;

	ObjectMapper objectMapper;

	private final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	private final String CREATE_SALES_INVOICE = "/pages/ISSR/createSalesInvoice.xhtml?faces-redirect=true";
	private final String LIST_SALES_INVOICE = "/pages/ISSR/listSalesInvoice.xhtml?faces-redirect=true";
	private final String PREVIEW_SALES_INVOICE = "/pages/ISSR/previewSalesInvoice.xhtml?faces-redirect=true";
	private final String VIEW_SALES_INVOICE = "/pages/ISSR/viewSalesInvoice.xhtml?faces-redirect=true";

	@Getter
	@Setter
	List<CustomerMaster> customerList;

	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeList;

	@Getter
	@Setter
	CustomerTypeMaster customerType;

	@Getter
	@Setter
	CustomerMaster customer;

	@Getter
	@Setter
	List<SalesOrder> salesOrderList;

	// @Getter
	// @Setter
	// List<SalesInvoiceItems> salesInvoiceItemsList, gstDetailsList;

	@Getter
	@Setter
	List<SalesInvoiceItemDTO> salesInvoiceItemsList, gstDetailsList, salesInvoiceItemsListUpdate;

	@Getter
	@Setter
	Double cgst = 0.0, sgst = 0.0, discount = 0.0, roundOff = 0.0, netTotal = 0.0, total = 0.0, sumTotalTaxAmount = 0.0,
			cgstPercent = 0.0, sgstPercent = 0.0, materialAmount = 0.0, orderUnitTotal = 0.0, billingUnitTotal = 0.0,
			itemtotal = 0.0;

	@Getter
	@Setter
	Date invoiceDate;

	@Getter
	@Setter
	String invoiceStartDate;

	@Getter
	@Setter
	boolean qtyGtOrderQtyAllowedFlag = true;

	@Getter
	@Setter
	SalesOrder salesOrder;

	@Getter
	@Setter
	UserMaster loginUser;

	@Autowired
	LoginBean loginBean;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	LazyDataModel<SalesInvoice> lazySalesInvoiceList;

	@Getter
	@Setter
	List<SalesInvoice> salesInvoiceList;

	@Getter
	@Setter
	SalesInvoiceDto salesInvoiceDto;

	@Getter
	@Setter
	SalesInvoice selectedSalesInvoice;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	EmployeeMaster empMaster, salesInvoiceTo;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	boolean addButtonFlag = false, editButtonFlag = true, deleteButtonFlag = true;

	@Getter
	@Setter
	Date salesInvoiceStartDate = null;

	@Getter
	@Setter
	String customerAddCountryName = null;

	@Getter
	final String countryName = "India";

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	public String showSalesInvioceListPage() {
		objectMapper = new ObjectMapper();
		loadLazySalesInvoiceList();
		selectedSalesInvoice = new SalesInvoice();

		// empMaster.getPersonalInfoEmployment().getWorkLocation().getAddressMaster();
		return LIST_SALES_INVOICE;
	}

	public String salesInvoiceAction() {
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedSalesInvoice == null || selectedSalesInvoice.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			loadCustomerTypeList();
			String qtyGtOrderQtyAllowedAppValue = commonDataService
					.getAppKeyValue(AppConfigEnum.QTY_GT_ORDEREDQTY_ALLOWED.toString());
			qtyGtOrderQtyAllowedFlag = "true".equals(qtyGtOrderQtyAllowedAppValue) ? true : false;
			if (action.equalsIgnoreCase("Create")) {
				empMaster = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
				customerList = new ArrayList<>();
				salesOrderList = new ArrayList<>();
				salesInvoiceItemsList = new ArrayList<>();
				gstDetailsList = new ArrayList<>();
				gstWiseList = new ArrayList<>();
				gstPercentageWiseDTOList = new ArrayList<>();
				customer = null;
				customerAddCountryName = null;
				salesOrder = null;
				customerType = null;
				cgst = 0.0;
				sgst = 0.0;
				discount = 0.0;
				roundOff = 0.0;
				netTotal = 0.0;
				itemtotal = 0.0;
				total = 0.0;
				materialAmount = 0.0;
				totalAmountInWords = null;
				invoiceDate = null;
				sumTotalTaxAmount = 0.0;
				loginUser = loginBean.getUserDetailSession();
				invoiceStartDate = commonDataService.getAppKeyValue(AppConfigEnum.SALES_INVOICE_START_DATE.toString());
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				salesInvoiceStartDate = simpleDateFormat.parse(invoiceStartDate);

				return CREATE_SALES_INVOICE;
			} else if (action.equalsIgnoreCase("Edit") || action.equalsIgnoreCase("View")) {
				empMaster = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesinvoice/getbyid/"
						+ selectedSalesInvoice.getId();
				BaseDTO response = httpService.get(url);
				jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
				salesInvoiceDto = objectMapper.readValue(jsonResponse, new TypeReference<SalesInvoiceDto>() {
				});
				invoiceNumber = salesInvoiceDto.getSalesInvoice().getInvoiceNumber();
				invoiceNumberPrefix = salesInvoiceDto.getSalesInvoice().getInvoiceNumberPrefix();
				invoiceDate = salesInvoiceDto.getSalesInvoice().getInvoiceDate();
				customer = salesInvoiceDto.getSalesInvoice().getCustomerMaster();

				if (customer.getAddressMaster() != null) {
					CountryMaster country = customer.getAddressMaster().getCountryMaster();
					if (country != null) {
						customerAddCountryName = country != null ? country.getName() : null;
					} else {
						errorMap.notify(ErrorDescription.COUNTRY_MASTER_SHOULD_NOT_BE_NULL.getCode());
						return null;
					}

				} else {
					errorMap.notify(ErrorDescription.CUST_ADDRESS_REQUIRED.getCode());
					return null;
				}

				customerType = salesInvoiceDto.getSalesInvoice().getCustomerMaster().getCustomerTypeMaster();
				salesOrder = salesInvoiceDto.getSalesInvoice().getSalesOrder();
				salesInvoiceItemsList = salesInvoiceDto.getSalesInvoiceItemDtoList();
				gstDetailsList = new ArrayList<>();
				gstWiseList = new ArrayList<>();
				gstPercentageWiseDTOList = new ArrayList<>();
				Map<String, List<SalesInvoiceItemDTO>> salesInvoiceGroupByHssnItems = salesInvoiceItemsList.stream()
						.collect(Collectors
								.groupingBy(s -> s.getSalesInvoiceItem().getProductVarietyMaster().getHsnCode()));
				sumTotalTaxAmount = 0.0;
				materialAmount = 0.0;
				cgst = 0.0;
				sgst = 0.0;
				discount = 0.0;
				roundOff = 0.0;
				netTotal = 0.0;
				itemtotal = 0.0;
				total = 0.0;
				orderUnitTotal = 0.0;
				billingUnitTotal = 0.0;
				log.info("salesInvoiceGroupByHssnItems :" + salesInvoiceGroupByHssnItems);
				for (Map.Entry<String, List<SalesInvoiceItemDTO>> entry : salesInvoiceGroupByHssnItems.entrySet()) {
					SalesInvoiceItemDTO dto = new SalesInvoiceItemDTO();
					SalesInvoiceItems si = new SalesInvoiceItems();
					dto.setUnit(entry.getValue().size());
					si.setProductVarietyMaster(entry.getValue().get(0).getSalesInvoiceItem().getProductVarietyMaster());
					dto.setCgstPercent(entry.getValue().get(0).getCgstPercent());
					dto.setSgstPercent(entry.getValue().get(0).getSgstPercent());
					dto.setCgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstAmount).sum());
					dto.setSgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstAmount).sum());
					dto.setTotalTaxAmount(dto.getCgstAmount() + dto.getSgstAmount());
					dto.setSalesInvoiceItem(si);
					gstDetailsList.add(dto);

					sgst += entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstAmount).sum();
					cgst += entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstAmount).sum();
					total += entry.getValue().stream().mapToDouble(
							s -> ((s.getSalesInvoiceItem().getItemAmount() + s.getSalesInvoiceItem().getTaxAmount())
									- s.getSalesInvoiceItem().getDiscountValue()))
							.sum();
					materialAmount += entry.getValue().stream()
							.mapToDouble(s -> (s.getSalesInvoiceItem().getItemAmount())).sum();
					discount += entry.getValue().stream().mapToDouble(s -> (s.getSalesInvoiceItem().getDiscountValue()))
							.sum();

					sumTotalTaxAmount += dto.getTotalTaxAmount();
					netTotal = ((materialAmount - discount) + sumTotalTaxAmount);
					orderUnitTotal += entry.getValue().stream().mapToDouble(s -> (s.getItemQuantity())).sum();
					billingUnitTotal += entry.getValue().stream()
							.mapToDouble(s -> (s.getSalesInvoiceItem().getItemQty())).sum();
				}

				/*
				 * for (Map.Entry<String, List<SalesInvoiceItems>> entry :
				 * salesInvoiceGroupByHssnItems.entrySet()) { SalesInvoiceItems si = new
				 * SalesInvoiceItems(); si.setUnit(entry.getValue().size());
				 * si.setProductVarietyMaster(entry.getValue().get(0).getProductVarietyMaster())
				 * ; si.setCgstPercent(entry.getValue().get(0).getCgstPercent());
				 * si.setSgstPercent(entry.getValue().get(0).getSgstPercent()); double
				 * itemAmount =
				 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getItemAmount).sum()
				 * ; si.setCgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItems::
				 * getCgstAmount).sum());
				 * si.setSgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItems::
				 * getSgstAmount).sum()); si.setTotalTaxAmount(si.getCgstAmount() +
				 * si.getSgstAmount()); gstDetailsList.add(si);
				 * 
				 * // splitup sgst +=
				 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getSgstAmount).sum()
				 * ; cgst +=
				 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getCgstAmount).sum()
				 * ; total += entry.getValue().stream() .mapToDouble(s -> ((s.getItemAmount() +
				 * s.getTaxAmount()) - s.getDiscountValue())).sum(); materialAmount +=
				 * entry.getValue().stream() .mapToDouble(item -> (item.getItemAmount() -
				 * item.getDiscountValue())).sum(); discount +=
				 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getDiscountValue).
				 * sum(); netTotal +=
				 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getItemTotal).sum();
				 * // total sumTotalTaxAmount += si.getTotalTaxAmount(); }
				 */
				for (SalesInvoiceItemDTO salesInvoiceItemDTO : salesInvoiceDto.getSalesInvoiceItemDtoList1()) {
					gstWiseList = salesInvoiceItemDTO.getGstWiseList();
					log.info("gstwiselistSize :" + gstWiseList.size());
					gstPercentageWiseDTOList = salesInvoiceItemDTO.getGstPercentageWiseDTOList();
					log.info("gstPercentageWiseDTOListSize :" + gstPercentageWiseDTOList.size());

				}
				double totalItemAmount = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getItemAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getItemAmount()));
				double totalunitrate = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getUnitRate() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getUnitRate()));
				materialAmount = totalItemAmount;

				discount = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getDiscountValue() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getDiscountValue()));

				cgst = gstPercentageWiseDTOList.stream().filter(o -> o.getCgstAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getCgstAmount()));
				sgst = gstPercentageWiseDTOList.stream().filter(o -> o.getSgstAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getSgstAmount()));
				sumTotalTaxAmount = cgst + sgst;
				netTotal = ((materialAmount - discount) + sumTotalTaxAmount);
				if (countryName.equals(customerAddCountryName)) {
					totalAmountInWords = AmountToWordConverter
							.converter(String.valueOf(AppUtil.DECIMAL_FORMAT.format(netTotal)));
				} else {
					totalAmountInWords = AmountToWordConverter
							.converter(String.valueOf(AppUtil.DECIMAL_FORMAT.format(netTotal - sumTotalTaxAmount)));
				}
				if (action.equalsIgnoreCase("Edit"))
					return CREATE_SALES_INVOICE;
				else
					return VIEW_SALES_INVOICE;
			} else if (action.equalsIgnoreCase("Delete")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSalesInvoiceDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in Exception....", e);
		}
		return null;
	}

	private void loadCustomerTypeList() {
		log.info("<===== Starts ContractSalesInvoiceBean.loadCustomerTypeList ======>");
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesorder/getcustomertypelist";
			BaseDTO response = httpService.get(url);
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			customerTypeList = objectMapper.readValue(jsonResponse, new TypeReference<List<CustomerTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured in loadCustomerTypeList....", e);
		}
		log.info("<===== Ends ContractSalesInvoiceBean.loadCustomerTypeList ======>");
	}

	public void updateCustomerList() {
		log.info("<===== Starts ContractSalesInvoiceBean.updateCustomerList ======>");
		if (customerType == null) {
			customerList = new ArrayList<>();
			customerType = new CustomerTypeMaster();
			salesOrder = new SalesOrder();
			salesOrderList = new ArrayList<>();
			return;
		}
		log.info("customerType Name ::" + customerType.getName());
		try {
			objectMapper = new ObjectMapper();
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/customerMaster/customerlistbysalesorder/"
					+ customerType.getId();
			BaseDTO response = httpService.get(url);
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			customerList = objectMapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured in updateCustomerList....", e);
		}
		log.info("<===== Ends ContractSalesInvoiceBean.updateCustomerList ======>");
	}

	public void updateSalesOrder() {
		log.info("<===== Starts ContractSalesInvoiceBean.updateSalesOrder ======>");
		if (customer == null) {
			salesOrderList = new ArrayList<>();
			return;
		}
		log.info("customer Name ::" + customer.getName());
		try {
			if (customer != null && customer.getId() != null) {
				customer = getCustomerDetails(customer.getId());
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesorder/getsalesorderList/"
					+ customer.getId();
			BaseDTO response = httpService.get(url);
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			salesOrderList = objectMapper.readValue(jsonResponse, new TypeReference<List<SalesOrder>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured in updateSalesOrder....", e);
		}
		log.info("<===== Ends ContractSalesInvoiceBean.updateSalesOrder ======>");
	}

	public void searchSalesInvoiceItems() {
		itemQuantityValue=0.0;
		productVarityQuantityIndex =-1;
		checkInventoryItem = false;
		log.info("<===== Starts ContractSalesInvoiceBean.searchSalesInvoiceItems ======>");
		if (customerType == null) {
			errorMap.notify(ErrorDescription.SALES_INVOICE_CUSTOMER_TYPE_IS_NULL.getCode());
			return;
		}
		if (customer == null) {
			errorMap.notify(ErrorDescription.SALES_INVOICE_CUSTOMER_NULL.getCode());
			return;
		}
		if (salesOrder == null) {
			errorMap.notify(ErrorDescription.SALES_INVOICE_SALES_ORDER_IS_NULL.getCode());
			return;
		}
		log.info("customer Name ::" + customer.getName());
		try {
			customer = getCustomerDetails(customer.getId());

			if (customer.getAddressMaster() != null) {
				CountryMaster country = customer.getAddressMaster().getCountryMaster();
				if (country != null) {
					customerAddCountryName = country != null ? country.getName() : null;
				} else {
					errorMap.notify(ErrorDescription.COUNTRY_MASTER_SHOULD_NOT_BE_NULL.getCode());
					return;
				}

			} else {
				errorMap.notify(ErrorDescription.CUST_ADDRESS_REQUIRED.getCode());
				return;
			}

			invoiceNumberPrefix = null;
			invoiceNumber = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesorder/loadsalesorderitems/"
					+ salesOrder.getId();
			BaseDTO response = httpService.get(url);
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			salesInvoiceItemsList = objectMapper.readValue(jsonResponse,
					new TypeReference<List<SalesInvoiceItemDTO>>() {
					});
			gstDetailsList = new ArrayList<>();
			gstWiseList = new ArrayList<>();
			gstPercentageWiseDTOList = new ArrayList<>();
			if (salesInvoiceItemsList != null) {
				Map<String, List<SalesInvoiceItemDTO>> salesInvoiceGroupByHssnItems = salesInvoiceItemsList.stream()
						.collect(Collectors
								.groupingBy(s -> s.getSalesInvoiceItem().getProductVarietyMaster().getHsnCode()));
				sumTotalTaxAmount = 0.0;
				materialAmount = 0.0;
				cgst = 0.0;
				sgst = 0.0;
				cgstPercent = 0.0;
				sgstPercent = 0.0;
				discount = 0.0;
				roundOff = 0.0;
				netTotal = 0.0;
				itemtotal = 0.0;
				total = 0.0;
				orderUnitTotal = 0.0;
				billingUnitTotal = 0.0;
				log.info("salesInvoiceGroupByHssnItems :" + salesInvoiceGroupByHssnItems);
				for (Map.Entry<String, List<SalesInvoiceItemDTO>> entry : salesInvoiceGroupByHssnItems.entrySet()) {
					SalesInvoiceItemDTO dto = new SalesInvoiceItemDTO();
					SalesInvoiceItems si = new SalesInvoiceItems();
					dto.setUnit(entry.getValue().size());
					si.setProductVarietyMaster(entry.getValue().get(0).getSalesInvoiceItem().getProductVarietyMaster());
					double roundedCgstAmount = Double.valueOf(AppUtil.DECIMAL_FORMAT
							.format(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstAmount).sum()));
					double roundedSsgtAmount = Double.valueOf(AppUtil.DECIMAL_FORMAT
							.format(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstAmount).sum()));

					log.info("roundedCgstPercent :"
							+ entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstPercent).sum());
					double roundedCgstPercent = Double.valueOf(AppUtil.DECIMAL_FORMAT
							.format(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstPercent).sum()));
					double roundedSgstPercent = Double.valueOf(AppUtil.DECIMAL_FORMAT
							.format(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstPercent).sum()));

					dto.setCgstPercent(entry.getValue().get(0).getCgstPercent());
					dto.setSgstPercent(entry.getValue().get(0).getSgstPercent());
					dto.setCgstAmount(roundedCgstAmount);
					dto.setSgstAmount(roundedSsgtAmount);
					dto.setTotalTaxAmount(dto.getCgstAmount() + dto.getSgstAmount());
					dto.setSalesInvoiceItem(si);
					gstDetailsList.add(dto);

					// splitup
					cgstPercent = roundedCgstPercent;
					sgstPercent = roundedSgstPercent;
					sgst += roundedSsgtAmount;
					cgst += roundedCgstAmount;
					total += entry.getValue().stream().mapToDouble(
							s -> ((s.getSalesInvoiceItem().getItemAmount() + s.getSalesInvoiceItem().getTaxAmount())
									- s.getSalesInvoiceItem().getDiscountValue()))
							.sum();
					materialAmount += entry.getValue().stream()
							.mapToDouble(s -> (s.getSalesInvoiceItem().getItemAmount())).sum();
					// discount += entry.getValue().stream().mapToDouble(SalesInvoiceItems::
					// getDiscountValue).sum();
					discount += entry.getValue().stream().mapToDouble(s -> (s.getSalesInvoiceItem().getDiscountValue()))
							.sum();
					// netTotal +=
					// entry.getValue().stream().mapToDouble(SalesInvoiceItems::getItemTotal).sum();

					// total
					sumTotalTaxAmount += dto.getTotalTaxAmount();
					netTotal = ((materialAmount - discount) + sumTotalTaxAmount);
					orderUnitTotal += entry.getValue().stream().mapToDouble(s -> (s.getItemQuantity())).sum();
					billingUnitTotal += entry.getValue().stream()
							.mapToDouble(s -> (s.getSalesInvoiceItem().getItemQty())).sum();
				}

				for (SalesInvoiceItemDTO salesInvoiceItemDTO : salesInvoiceItemsList) {
					gstWiseList = salesInvoiceItemDTO.getGstWiseList();
					log.info("gstwiselistSize :" + gstWiseList.size());
					gstPercentageWiseDTOList = salesInvoiceItemDTO.getGstPercentageWiseDTOList();
					log.info("gstPercentageWiseDTOListSize :" + gstPercentageWiseDTOList.size());

				}
				double totalItemAmount = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getItemAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getItemAmount()));
				double totalunitrate = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getUnitRate() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getUnitRate()));
				materialAmount = totalItemAmount;

				discount = salesInvoiceItemsList.stream()
						.filter(o -> o.getSalesInvoiceItem().getDiscountValue() != null)
						.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getDiscountValue()));

				netTotal = netTotal != null ? netTotal : 0D;

				cgst = gstPercentageWiseDTOList.stream().filter(o -> o.getCgstAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getCgstAmount()));
				sgst = gstPercentageWiseDTOList.stream().filter(o -> o.getSgstAmount() != null)
						.collect(Collectors.summingDouble(o -> o.getSgstAmount()));
				sumTotalTaxAmount = cgst + sgst;
				netTotal = ((materialAmount - discount) + sumTotalTaxAmount);
				sumTotalTaxAmount = sumTotalTaxAmount != null ? sumTotalTaxAmount : 0D;
				if (countryName.equals(customerAddCountryName)) {
					totalAmountInWords = AmountToWordConverter.converter(String.valueOf(netTotal));
				} else {
					totalAmountInWords = AmountToWordConverter.converter(String.valueOf(netTotal - sumTotalTaxAmount));
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in searchSalesInvoiceItems....", e);
		}
		log.info("<===== Ends ContractSalesInvoiceBean.searchSalesInvoiceItems ======>");
	}
	
	public void valueQty(SalesInvoiceItemDTO s,int index) {
		
		if(index != productVarityQuantityIndex)
		{
			checkInventoryItem = false;
		}
		
		if(!checkInventoryItem)
		{
		itemQuantityValue = s.getSalesInvoiceItem().getItemQty();
		productVarityQuantityIndex = index;
		}
		log.info("<===== Ends ContractSalesInvoiceBean.searchSalesInvoiceItems ======> " +s.getSalesInvoiceItem().getItemQty());
		log.info("<===== Ends ContractSalesInvoiceBean.searchSalesInvoiceItems ======> " +s.getSalesInvoiceItem().getItemQty());
		log.info("<===== Ends ContractSalesInvoiceBean.searchSalesInvoiceItems ======> " +s.getSalesInvoiceItem().getItemQty());
	}

	public void updateTable(SalesInvoiceItemDTO s,int index) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");
		decimalFormat.setRoundingMode(RoundingMode.DOWN);
		try {

		
			// check inventoryitems
			SalesInvoiceItemDTO checkInventory = new SalesInvoiceItemDTO();
			checkInventory = s;
			checkInventory.setSalesInvoiceItem(s.getSalesInvoiceItem());
			List<SalesInvoiceItemDTO> checkInventoryItemList = new ArrayList<SalesInvoiceItemDTO>();
			checkInventory.getSalesInvoiceItem()
					.setItemAmount(s.getSalesInvoiceItem().getItemQty() * s.getSalesInvoiceItem().getUnitRate());
			checkInventoryItemList.add(checkInventory);
			String requestUrl = SERVER_URL + appPreference.getOperationApiUrl() + "/salesinvoice/checkinventoryitems";
			BaseDTO apiResponce = httpService.post(requestUrl, checkInventoryItemList);
			if (apiResponce != null) {
				log.info("response" + apiResponce);
				if (apiResponce.getStatusCode()
						.equals(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode())) {
					salesInvoiceItemsList.get(index).getSalesInvoiceItem().setItemQty(itemQuantityValue);
					checkInventoryItem = true;
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode());
					return;
				} else if (apiResponce.getStatusCode().equals(ErrorDescription.FAILURE_RESPONSE.getErrorCode())) {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					return;
				} else if (apiResponce.getStatusCode().equals(100)) {
					salesInvoiceItemsList.get(index).getSalesInvoiceItem().setItemQty(itemQuantityValue);
					checkInventoryItem = true;
					AppUtil.addWarn("Billing Unit Is More Than Available Stock : " + apiResponce.getMessage());
					return;
				}
			}
			
			
			double itemQty1 = checkInventory.getSalesInvoiceItem().getItemQty();
			itemQty1 = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(itemQty1));
			if (itemQty1 > checkInventory.getItemQuantity().doubleValue()) {
				salesInvoiceItemsList.get(index).getSalesInvoiceItem().setItemQty(itemQuantityValue);
				checkInventoryItem = true;
				AppUtil.addError("Billing unit can not be greater than ordered unit for "
						+ s.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
						+ s.getSalesInvoiceItem().getProductVarietyMaster().getName());
				return;
			}
			
			double diff1 = checkInventory.getItemQuantity() - checkInventory.getAlreadyOrderUnit();
			if (itemQty1 > diff1) {
				salesInvoiceItemsList.get(index).getSalesInvoiceItem().setItemQty(itemQuantityValue);
				checkInventoryItem = true;
				AppUtil.addError("Billing unit can not be greater than ordered unit for "
						+ checkInventory.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
						+ checkInventory.getSalesInvoiceItem().getProductVarietyMaster().getName());
				return;
			}
			
			log.info("================================================== End =========== " +s.getSalesInvoiceItem().getItemQty());
			log.info("================================================== End =========== " +s.getSalesInvoiceItem().getItemQty());
			checkInventoryItem =false;
			
			if (action.equalsIgnoreCase("Create")) {
				if (s.getSalesInvoiceItem().getItemQty() == null || s.getSalesInvoiceItem().getItemQty() == 0) {
					s.getSalesInvoiceItem().setItemQty(s.getItemQuantity().doubleValue());
				}
				if (!qtyGtOrderQtyAllowedFlag) {
					if (s.getSalesInvoiceItem().getItemQty().doubleValue() > s.getItemQuantity().doubleValue()) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return;
					}
					double diff = s.getItemQuantity() - s.getAlreadyOrderUnit();
					diff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(diff));
					if (s.getSalesInvoiceItem().getItemQty() == null || s.getSalesInvoiceItem().getItemQty() == 0) {
						s.getSalesInvoiceItem().setItemQty(diff);
					}
					if (s.getSalesInvoiceItem().getItemQty() > diff) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return;
					}
				}
			} else {
				double diff = s.getItemQuantity() - s.getAlreadyOrderUnit();
				diff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(diff));
				if (s.getSalesInvoiceItem().getItemQty() == null || s.getSalesInvoiceItem().getItemQty() == 0) {
					s.getSalesInvoiceItem().setItemQty(diff);
				}
				if (!qtyGtOrderQtyAllowedFlag) {
					if (s.getSalesInvoiceItem().getItemQty() > diff) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return;
					}
				}
			}
			
			

			if (salesInvoiceItemsList != null) {
				List<SalesInvoiceItemDTO> salesInvoiceItemDTOList = new ArrayList<>();
				String calculateGstForFreeSampleFlag = commonDataService
						.getAppKeyValue(AppConfigKey.CALCULATE_GST_FOR_FREE_SAMPLE);
				for (SalesInvoiceItemDTO salesInvoiceItemDTO : salesInvoiceItemsList) {
					salesInvoiceItemDTO.getSalesInvoiceItem()
							.setItemAmount(salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty()
									* salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate());
					/*
					 * salesInvoiceItemDTO.getSalesInvoiceItem()
					 * .setDiscountValue(salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
					 * salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100);
					 * salesInvoiceItemDTO.getSalesInvoiceItem()
					 * .setTaxAmount((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount() -
					 * salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountValue())
					 * salesInvoiceItemDTO.getSalesInvoiceItem().getTaxPercent() / 100); Double
					 * itemamount = salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty()
					 * salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate(); itemamount =
					 * itemamount != null ?
					 * Double.valueOf(AppUtil.DECIMAL_FORMAT.format(itemamount)): 0D;
					 * salesInvoiceItemDTO.getSalesInvoiceItem().setItemAmount(itemamount);
					 */

					/*Double discountValue = (salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
							* salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100);*/
					
				

					Double discountValueUpd = 0D;
//					discountValueUpd = (salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() * (salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100));
//					discountValueUpd = discountValueUpd != null ? formatDouble(discountValueUpd) : 0D;
					
					discountValueUpd = (salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() * salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty());
					
					discountValueUpd = formatDouble(discountValueUpd * (salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100));
							
					/*Double discountValue = ((salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() - salesInvoiceItemDTO.getSalesInvoiceItem().getSupplierRate())
							* salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty());
					*/
					salesInvoiceItemDTO.getSalesInvoiceItem().setDiscountValue(discountValueUpd);

					Double taxamount = 0D;

					Double itemTaxAmount = salesInvoiceItemDTO.getSalesInvoiceItem().getTaxAmount();
					itemTaxAmount = itemTaxAmount != null ? itemTaxAmount : 0D;
					boolean freeSampleFlag = salesInvoiceItemDTO.getSalesInvoiceItem().getFreesamplecheckFlag() !=null ? 
							salesInvoiceItemDTO.getSalesInvoiceItem().getFreesamplecheckFlag() : false;

					if (freeSampleFlag) {
						if ("true".equals(calculateGstForFreeSampleFlag)) {
							taxamount = ((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount())
									* salesInvoiceItemDTO.getSalesInvoiceItem().getTaxPercent() / 100);
						} else {
							taxamount = 0D;
						}
					} else {
						taxamount = ((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
								- salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountValue())
								* salesInvoiceItemDTO.getSalesInvoiceItem().getTaxPercent() / 100);
					}

					taxamount = taxamount != null ? taxamount : 0D;

					// taxamount = taxamount != null ?
					// Double.parseDouble(decimalFormat.format(taxamount)): 0D;
					taxamount = taxamount != null ? Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxamount)) : 0D;

					salesInvoiceItemDTO.getSalesInvoiceItem().setTaxAmount(roundOffTaxValue(taxamount));

					salesInvoiceItemDTO.getSalesInvoiceItem()
							.setItemTotal((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
									+ salesInvoiceItemDTO.getSalesInvoiceItem().getTaxAmount())
									- salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountValue());
					// salesInvoiceItemDTO.setItemQuantity(salesInvoiceItemDTO.getItemQuantity());
					
					// removed by vinayak
					
					double itemQty = salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty();
					itemQty = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(itemQty));
					
					/*if (itemQty > salesInvoiceItemDTO.getItemQuantity().doubleValue()) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ s.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return;
					}*/
					
					double diff = salesInvoiceItemDTO.getItemQuantity() - salesInvoiceItemDTO.getAlreadyOrderUnit();
					diff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(diff));
					if (salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == null
							|| salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == 0) {
						salesInvoiceItemDTO.getSalesInvoiceItem().setItemQty(diff);
					}
					
					
					// removed by vinayak
					
					/*if (itemQty > diff) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return;
					}*/
					

					// salesInvoiceItemDTOList.add(s);
					salesInvoiceItemDTOList.add(salesInvoiceItemDTO);
				}
				
				// removed by vinayak
				
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesinvoice/checkinventoryitems";
				BaseDTO response = httpService.post(url, salesInvoiceItemDTOList);
				if (response != null) {
					log.info("response" + response);
					
					/*if (response.getStatusCode()
							.equals(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode())) {
						errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode());
						return;
					} else*/
						
						if (response.getStatusCode().equals(ErrorDescription.FAILURE_RESPONSE.getErrorCode())) {
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
						return;
					}
					
					/*else if (response.getStatusCode().equals(100)) {						
						AppUtil.addWarn("Billing Unit Is More Than Available Stock : " + response.getMessage());
//						searchSalesInvoiceItems();
						return;
					}*/
				}

				s.getSalesInvoiceItem()
						.setItemAmount(s.getSalesInvoiceItem().getItemQty() * s.getSalesInvoiceItem().getUnitRate());
				/*
				 * s.getSalesInvoiceItem().setDiscountValue(
				 * s.getSalesInvoiceItem().getItemAmount() *
				 * s.getSalesInvoiceItem().getDiscountPercent() / 100);
				 */
				// s.getSalesInvoiceItem().setTaxAmount(s.getSalesInvoiceItem().getItemAmount()
				// * s.getSalesInvoiceItem().getTaxPercent() / 100);

				/*Double discountValue = (s.getSalesInvoiceItem().getItemAmount()
						* s.getSalesInvoiceItem().getDiscountPercent() / 100);
				*/
				Double discountValueUpd = 0D;
				discountValueUpd = (s.getSalesInvoiceItem().getUnitRate() * (s.getSalesInvoiceItem().getDiscountPercent() / 100));
				discountValueUpd = discountValueUpd != null ? formatDouble(discountValueUpd) : 0D;
				
				/*Double discountValue = ((s.getSalesInvoiceItem().getUnitRate() - s.getSalesInvoiceItem().getSupplierRate())
						* s.getSalesInvoiceItem().getItemQty());
						* 
						* 
				*/
				
				Double totalItemValue = (s.getSalesInvoiceItem().getUnitRate()*s.getSalesInvoiceItem().getItemQty());
				
				Double discountValue = formatDouble(totalItemValue * (s.getSalesInvoiceItem().getDiscountPercent()/100));
				
				s.getSalesInvoiceItem().setDiscountValue(discountValue);

				Double taxamount = 0D;

				Double itemTaxAmount = s.getSalesInvoiceItem().getTaxAmount();
				itemTaxAmount = itemTaxAmount != null ? itemTaxAmount : 0D;

				boolean freeSampleFlag = s.getSalesInvoiceItem().getFreesamplecheckFlag() != null ?
						s.getSalesInvoiceItem().getFreesamplecheckFlag() : false;

				if (freeSampleFlag) {
					if ("true".equals(calculateGstForFreeSampleFlag)) {
						taxamount = ((s.getSalesInvoiceItem().getItemAmount()) * s.getSalesInvoiceItem().getTaxPercent()
								/ 100);
					} else {
						taxamount = 0D;
					}
				} else {
					taxamount = ((s.getSalesInvoiceItem().getItemAmount() - s.getSalesInvoiceItem().getDiscountValue())
							* s.getSalesInvoiceItem().getTaxPercent() / 100);
				}

				taxamount = taxamount != null ? taxamount : 0D;

				taxamount = taxamount != null ? Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxamount)) : 0D;

				s.getSalesInvoiceItem().setTaxAmount(roundOffTaxValue(taxamount));

				s.getSalesInvoiceItem()
						.setItemTotal((s.getSalesInvoiceItem().getItemAmount() + s.getSalesInvoiceItem().getTaxAmount())
								- s.getSalesInvoiceItem().getDiscountValue());
				s.setCgstAmount(s.getSalesInvoiceItem().getItemAmount() * s.getCgstPercent() / 100);
				s.setSgstAmount(s.getSalesInvoiceItem().getItemAmount() * s.getSgstPercent() / 100);

				gstDetailsList = new ArrayList<>();
				sumTotalTaxAmount = 0.0;
				materialAmount = 0.0;
				cgst = 0.0;
				sgst = 0.0;
				discount = 0.0;
				roundOff = 0.0;
				netTotal = 0.0;
				itemtotal = 0.0;
				total = 0.0;
				billingUnitTotal = 0.0;
				orderUnitTotal = 0.0;
				Map<String, List<SalesInvoiceItemDTO>> salesInvoiceGroupByHssnItems = salesInvoiceItemsList.stream()
						.collect(Collectors
								.groupingBy(obj -> obj.getSalesInvoiceItem().getProductVarietyMaster().getHsnCode()));
				log.info("salesInvoiceGroupByHssnItems :" + salesInvoiceGroupByHssnItems);
				for (Map.Entry<String, List<SalesInvoiceItemDTO>> entry : salesInvoiceGroupByHssnItems.entrySet()) {
					SalesInvoiceItemDTO dto = new SalesInvoiceItemDTO();
					SalesInvoiceItems si = new SalesInvoiceItems();
					dto.setUnit(entry.getValue().size());
					si.setProductVarietyMaster(entry.getValue().get(0).getSalesInvoiceItem().getProductVarietyMaster());
					dto.setCgstPercent(entry.getValue().get(0).getCgstPercent());
					dto.setSgstPercent(entry.getValue().get(0).getSgstPercent());
					dto.setCgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstAmount).sum());
					dto.setSgstAmount(entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstAmount).sum());
					dto.setTotalTaxAmount(dto.getCgstAmount() + dto.getSgstAmount());
					dto.setSalesInvoiceItem(si);
					sumTotalTaxAmount += dto.getTotalTaxAmount();
					gstDetailsList.add(dto);

					// splitup
					sgst += entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getSgstAmount).sum();
					cgst += entry.getValue().stream().mapToDouble(SalesInvoiceItemDTO::getCgstAmount).sum();
					total += entry.getValue().stream()
							.mapToDouble(obj -> ((obj.getSalesInvoiceItem().getItemAmount()
									+ obj.getSalesInvoiceItem().getTaxAmount())
									- obj.getSalesInvoiceItem().getDiscountValue()))
							.sum();
					materialAmount += entry.getValue().stream()
							.mapToDouble(obj -> (obj.getSalesInvoiceItem().getItemAmount())).sum();

					discount += entry.getValue().stream()
							.mapToDouble(obj -> (obj.getSalesInvoiceItem().getDiscountValue())).sum();

					billingUnitTotal += entry.getValue().stream()
							.mapToDouble(obj -> (obj.getSalesInvoiceItem().getItemQty())).sum();

					orderUnitTotal += entry.getValue().stream().mapToDouble(obj -> (obj.getItemQuantity())).sum();
					/*
					 * sgst +=
					 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getSgstAmount).sum()
					 * ; cgst +=
					 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getCgstAmount).sum()
					 * ; total += entry.getValue().stream() .mapToDouble(item ->
					 * ((item.getItemAmount() + item.getTaxAmount()) - item.getDiscountValue()))
					 * .sum(); materialAmount += entry.getValue().stream() .mapToDouble(item ->
					 * (item.getItemAmount() - item.getDiscountValue())).sum(); discount +=
					 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getDiscountValue).
					 * sum(); netTotal +=
					 * entry.getValue().stream().mapToDouble(SalesInvoiceItems::getItemTotal).sum();
					 */
				}
				String jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
				salesInvoiceItemsListUpdate = objectMapper.readValue(jsonResponse,
						new TypeReference<List<SalesInvoiceItemDTO>>() {
						});
				if (salesInvoiceItemsListUpdate != null) {
					for (SalesInvoiceItemDTO salesInvoiceItemdto : salesInvoiceItemsListUpdate) {
						gstWiseList = salesInvoiceItemdto.getGstWiseList();
						log.info("gstwiselistSize :" + gstWiseList.size());
						gstPercentageWiseDTOList = salesInvoiceItemdto.getGstPercentageWiseDTOList();
						log.info("gstPercentageWiseDTOListSize :" + gstPercentageWiseDTOList.size());

					}

					double totalItemAmount = salesInvoiceItemsList.stream()
							.filter(o -> o.getSalesInvoiceItem().getItemAmount() != null)
							.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getItemAmount()));
					materialAmount = totalItemAmount;

					discount = salesInvoiceItemsList.stream()
							.filter(o -> o.getSalesInvoiceItem().getDiscountValue() != null)
							.collect(Collectors.summingDouble(o -> o.getSalesInvoiceItem().getDiscountValue()));
					cgst = gstPercentageWiseDTOList.stream().filter(o -> o.getCgstAmount() != null)
							.collect(Collectors.summingDouble(o -> o.getCgstAmount()));
					sgst = gstPercentageWiseDTOList.stream().filter(o -> o.getSgstAmount() != null)
							.collect(Collectors.summingDouble(o -> o.getSgstAmount()));
					sumTotalTaxAmount = cgst + sgst;

					netTotal = ((materialAmount - discount) + sumTotalTaxAmount);
					sumTotalTaxAmount = sumTotalTaxAmount != null ? sumTotalTaxAmount : 0D;
				}
			}
			totalAmountInWords = AmountToWordConverter.converter(String.valueOf(netTotal));
		} catch (Exception e) {
			log.error("Exception in updateTable ..", e);
		}
	}

	private double roundOffTaxValue(double taxV) {
		Double taxRoundOff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxV));
		log.info("tax round off value----" + taxRoundOff);
		String str = String.valueOf(taxRoundOff);
		String[] convert = str.split("\\.");
		int length = convert[1].length();
		double taxValue = 0.0;
		if (length == 2) {
			int splitValue = Integer.valueOf(convert[1].substring(1));
			log.info("split up value----------" + splitValue);
			if (splitValue % 2 == 0) {
				log.info("splitValue is even-----");
				// taxValue = taxRoundOff;
				taxValue = taxV;
			} else {
				log.info("splitValue is odd-----");
				taxValue = taxRoundOff + 0.01;
				taxValue = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue));
			}
		} else {
			taxValue = taxRoundOff;
		}
		return taxValue;
	}

	private Double formatDouble(Double inputValue) {
		Double formattedValue = null;
		try {
			inputValue = inputValue == null ? 0D : inputValue;
			if (inputValue > 0D) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.00");
				formattedValue = decimalFormat.parse(decimalFormat.format(inputValue)).doubleValue();
			} else {
				formattedValue = inputValue;
			}
		} catch (Exception ex) {
			formattedValue = inputValue;
			log.error("Exception at formatDouble", ex);
		}
		return formattedValue;
	}
	
	public String gotoPreviewPage() {
		log.info("<=== Starts ContractSalesInvoiceBean.gotoPreviewPage =====>" + action);
		if (action.equalsIgnoreCase("Create")) {
			if (customerType == null) {
				errorMap.notify(ErrorDescription.SALES_INVOICE_CUSTOMER_TYPE_IS_NULL.getCode());
				return null;
			}
			if (customer == null) {
				errorMap.notify(ErrorDescription.SALES_INVOICE_CUSTOMER_NULL.getCode());
				return null;
			}
			if (salesOrder == null) {
				errorMap.notify(ErrorDescription.SALES_INVOICE_SALES_ORDER_IS_NULL.getCode());
				return null;
			}
			if (salesInvoiceItemsList == null || salesInvoiceItemsList.size() == 0) {
				errorMap.notify(ErrorDescription.SALES_INVOICE_ITEMS_LIST_EMPTY.getCode());
				return null;
			}
			for (SalesInvoiceItemDTO dto : salesInvoiceItemsList) {
				/*
				 * if (dto.getSalesInvoiceItem().getItemQty() == null ||
				 * dto.getSalesInvoiceItem().getItemQty() == 0) {
				 * errorMap.notify(ErrorDescription.SALES_INVOICE_BILLING_UNIT_REQUIRED.getCode(
				 * )); return null; }
				 */
				if (!qtyGtOrderQtyAllowedFlag) {
					if (dto.getSalesInvoiceItem().getItemQty().doubleValue() > dto.getItemQuantity().doubleValue()) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ dto.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ dto.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return null;
					}
					double diff = dto.getItemQuantity() - dto.getAlreadyOrderUnit();
					diff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(diff));
					if (dto.getSalesInvoiceItem().getItemQty() == null || dto.getSalesInvoiceItem().getItemQty() == 0) {
						dto.getSalesInvoiceItem().setItemQty(diff);
					}
					if (dto.getSalesInvoiceItem().getItemQty() > diff) {
						AppUtil.addError("Billing unit can not be greater than ordered unit for "
								+ dto.getSalesInvoiceItem().getProductVarietyMaster().getCode() + "/"
								+ dto.getSalesInvoiceItem().getProductVarietyMaster().getName());
						return null;
					}
				}

				if (salesInvoiceItemsList != null) {
					List<SalesInvoiceItemDTO> salesInvoiceItemDTOList = new ArrayList<>();
					for (SalesInvoiceItemDTO salesInvoiceItemDTO : salesInvoiceItemsList) {
						salesInvoiceItemDTO.getSalesInvoiceItem()
								.setItemAmount(salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty()
										* salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate());
						/*
						 * salesInvoiceItemDTO.getSalesInvoiceItem()
						 * .setDiscountValue(salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
						 * salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100);
						 * salesInvoiceItemDTO.getSalesInvoiceItem()
						 * .setTaxAmount((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount() -
						 * salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountValue())
						 * salesInvoiceItemDTO.getSalesInvoiceItem().getTaxPercent() / 100);
						 */

						/*Double discountValue = (salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
								* salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100);*/
						
						Double discountValueUpd = 0D;
//						discountValueUpd = (salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() * (salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100));
//						discountValueUpd = discountValueUpd != null ? formatDouble(discountValueUpd) : 0D;
						discountValueUpd = (salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() *salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty());
						discountValueUpd = formatDouble((discountValueUpd * (salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountPercent() / 100)));
						
														
						/*Double discountValue = ((salesInvoiceItemDTO.getSalesInvoiceItem().getUnitRate() - salesInvoiceItemDTO.getSalesInvoiceItem().getSupplierRate())
								* salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty());
						
						*/
						salesInvoiceItemDTO.getSalesInvoiceItem().setDiscountValue(discountValueUpd);

						Double taxamount = salesInvoiceItemDTO.getSalesInvoiceItem().getTaxAmount();
						taxamount = taxamount != null ? taxamount : 0D;

						taxamount = taxamount != null ? Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxamount)) : 0D;

						salesInvoiceItemDTO.getSalesInvoiceItem().setTaxAmount(roundOffTaxValue(taxamount));

						salesInvoiceItemDTO.getSalesInvoiceItem()
								.setItemTotal((salesInvoiceItemDTO.getSalesInvoiceItem().getItemAmount()
										+ salesInvoiceItemDTO.getSalesInvoiceItem().getTaxAmount())
										- salesInvoiceItemDTO.getSalesInvoiceItem().getDiscountValue());

						if (salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty().doubleValue() > salesInvoiceItemDTO
								.getItemQuantity().doubleValue()) {
							AppUtil.addError("Billing unit can not be greater than ordered unit for "
									+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getCode()
									+ "/"
									+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getName());
							return null;
						}
						double diff = salesInvoiceItemDTO.getItemQuantity() - salesInvoiceItemDTO.getAlreadyOrderUnit();
						diff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(diff));
						if (salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == null
								|| salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == 0) {
							salesInvoiceItemDTO.getSalesInvoiceItem().setItemQty(diff);
						}
						if (salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty().doubleValue() > diff) {
							AppUtil.addError("Billing unit can not be greater than ordered unit for "
									+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getCode()
									+ "/"
									+ salesInvoiceItemDTO.getSalesInvoiceItem().getProductVarietyMaster().getName());
							return null;
						}
						if (salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == null
								|| salesInvoiceItemDTO.getSalesInvoiceItem().getItemQty() == 0) {
							if (salesInvoiceItemDTO.getItemQuantity().doubleValue() != salesInvoiceItemDTO
									.getAlreadyOrderUnit().doubleValue()) {
								errorMap.notify(ErrorDescription.SALES_INVOICE_BILLING_UNIT_REQUIRED.getCode());
								return null;
							}
						}
						salesInvoiceItemDTOList.add(salesInvoiceItemDTO);
					}
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesinvoice/checkinventoryitems";
					BaseDTO response = httpService.post(url, salesInvoiceItemDTOList);
					if (response != null) {
						if (response.getStatusCode()
								.equals(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode())) {
							errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode());
							return null;
						} else if (response.getStatusCode().equals(ErrorDescription.FAILURE_RESPONSE.getErrorCode())) {
							errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode());
							return null;
						} else if (response.getStatusCode().equals(100)) {
							AppUtil.addWarn("Billing Unit Is More Than Available Stock : " + response.getMessage());
							return null;
						} else {
							String errorMsg = response.getErrorDescription();
							if (StringUtils.isNotEmpty(errorMsg)) {
								AppUtil.addError(errorMsg);
								return null;
							}
						}
					}
				}
			}

			log.info("<=== Ends ContractSalesInvoiceBean.gotoPreviewPage =====>");
			if (invoiceNumberPrefix == null && invoiceNumber == null) {
				try {
					SalesInvoice invoice = new SalesInvoice();
					invoice.setCustomerMaster(customer);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/salesinvoice/generatesalesinvoiceNumber";
					BaseDTO response = httpService.post(url, invoice);
					jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
					invoice = objectMapper.readValue(jsonResponse, new TypeReference<SalesInvoice>() {
					});
					if (invoice != null) {
						invoiceNumberPrefix = invoice.getInvoiceNumberPrefix();
						invoiceNumber = invoice.getInvoiceNumber();

					}
				} catch (Exception e) {
					log.error("Exception occured in updateSalesOrder....", e);
					return null;
				}
			}

			CustomerMaster customerMaster = getCustomerDetails(customer.getId());
			salesOrder.setCustomerMaster(customerMaster);
			// salesOrder.setOrderDate(salesOrder.getOrderDate());

			if (customerMaster.getAddressMaster() != null) {
				CountryMaster country = customerMaster.getAddressMaster().getCountryMaster();
				if (country != null) {
					customerAddCountryName = country != null ? country.getName() : null;
				} else {
					errorMap.notify(ErrorDescription.COUNTRY_MASTER_SHOULD_NOT_BE_NULL.getCode());
					return null;
				}

			} else {
				errorMap.notify(ErrorDescription.CUST_ADDRESS_REQUIRED.getCode());
				return null;
			}
		}

		return PREVIEW_SALES_INVOICE;
	}

	public String gotoCreatePage() {
		log.info("<=== ContractSalesInvoiceBean.gotoCreatePage called... =====>");
		if (action.equalsIgnoreCase("Create"))
			return CREATE_SALES_INVOICE;
		else
			return VIEW_SALES_INVOICE;
	}

	public String saveOrUpdate() {
		log.info("<=== Starts ContractSalesInvoiceBean.saveOrUpdate... =====>");
		try {
			BaseDTO response = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salesinvoice/saveorupdate";
			if (action.equalsIgnoreCase("Create")) {
				SalesInvoice request = new SalesInvoice();
				// request.setSalesInvoiceItemDtoList(salesInvoiceItemsList);
				request.setEntityMaster(empMaster.getPersonalInfoEmployment().getWorkLocation());
				request.setCreatedBy(loginUser);
				request.setInvoiceDate(invoiceDate);
				request.setCustomerMaster(customer);
				request.setSalesOrder(salesOrder);
				request.setMaterialValue(netTotal);
				request.setNetTotal(netTotal);
				request.setStatus(QuotationStatus.INITIATED.toString());
				request.setMaterialValue(materialAmount);
				request.setInvoiceNumberPrefix(invoiceNumberPrefix);
				request.setInvoiceNumber(invoiceNumber);
				request.setSaleType(SalesType.ISSR_SALES);
				SalesInvoiceDto invoiceDto = new SalesInvoiceDto();
				invoiceDto.setSalesInvoiceItemDtoList(salesInvoiceItemsList);
				invoiceDto.setSalesInvoice(request);
				response = httpService.post(url, invoiceDto);
			} else {
				// selectedSalesInvoice.setSalesInvoiceItemDtoList(salesInvoiceItemsList);
				selectedSalesInvoice.setModifiedBy(loginUser);
				selectedSalesInvoice.setCustomerMaster(customer);
				selectedSalesInvoice.setSalesOrder(salesOrder);
				SalesInvoiceDto invoiceDto = new SalesInvoiceDto();
				invoiceDto.setSalesInvoice(selectedSalesInvoice);
				invoiceDto.setSalesInvoiceItemDtoList(salesInvoiceItemsList);
				response = httpService.post(url, selectedSalesInvoice);
			}
			// SalesInvoice request = new SalesInvoice();
			// request.setSalesInvoiceItemsList(salesInvoiceItemsList);
			// request.setCreatedBy(loginUser);
			// request.setCustomerMaster(customer);
			// request.setSalesOrder(salesOrder);
			// String url = SERVER_URL
			// +appPreference.getOperationApiUrl()+"/salesinvoice/saveorupdate";
			// BaseDTO response = httpService.post(url, request);
			if (response != null) {
				if (response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.SALES_INVOICE_SAVED_SUCCESSFULLY.getCode());
				} else {
					String errorMsg = response.getErrorDescription();
					if (StringUtils.isNotEmpty(errorMsg)) {
						AppUtil.addError(errorMsg);
						return null;
					} else {
						errorMap.notify(response.getStatusCode());
						return null;
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in saveOrUpdate...." + e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<=== Ends ContractSalesInvoiceBean.saveOrUpdate ... =====>");
		return showSalesInvioceListPage();
	}

	public void loadLazySalesInvoiceList() {
		log.info("<===== Starts ContractSalesInvoiceBean.loadLazySalesInvoiceList ======>");
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('statusDialog').show()");
		lazySalesInvoiceList = new LazyDataModel<SalesInvoice>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<SalesInvoice> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/salesinvoice/getallsalesinvoicelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
						salesInvoiceList = objectMapper.readValue(jsonResponse,
								new TypeReference<List<SalesInvoice>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						context.execute("PF('statusDialog').hide()");
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					context.execute("PF('statusDialog').hide()");
				}
				log.info("Ends lazy load....");
				return salesInvoiceList;
			}

			@Override
			public Object getRowKey(SalesInvoice res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SalesInvoice getRowData(String rowKey) {
				for (SalesInvoice salesInvoice : salesInvoiceList) {
					if (salesInvoice.getId().equals(Long.valueOf(rowKey))) {
						selectedSalesInvoice = salesInvoice;
						return salesInvoice;
					}
				}
				return null;
			}
		};
		context.execute("PF('statusDialog').hide()");
		log.info("<===== Ends ContractSalesInvoiceBean.loadLazySalesInvoiceList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ContractSalesInvoiceBean.onRowSelect ========>" + event);
		selectedSalesInvoice = ((SalesInvoice) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends ContractSalesInvoiceBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts ContractSalesInvoiceBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/salesinvoice/deletebyid/" + selectedSalesInvoice.getId());
			if (response != null && response.getStatusCode() == 0) {
				// errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				AppUtil.addInfo("Sales Invoice Delete Successfuly");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSalesInvoiceDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				// errorMap.notify(response.getStatusCode());
				AppUtil.addInfo("Sales Invoice Delete Failed");
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured while delete SalesInvoice ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ContractSalesInvoiceBean.deleteConfirm ===========>");
		return showSalesInvioceListPage();
	}

	public List<CustomerMaster> loadCustomerByName(String query) {
		log.info("Loading all customer ............");

		// List<CustomerMaster> customerMasterList = null;

		try {
			/*
			 * CustomerMasterController.java - Method: getAllCustomersByType()
			 */
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/customerMaster/customerlistbyname/" + query;

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerList;
	}

	public CustomerMaster getCustomerDetails(Long custId) {
		log.info("=============>Start getCustomerBillingAndShippingAddress Method==========>" + custId);
		CustomerMaster customerMaster = new CustomerMaster();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/customerMaster/customerDetails/" + custId;
			baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerMaster = mapper.readValue(jsonResponse, new TypeReference<CustomerMaster>() {
				});

			}

		} catch (Exception ex) {
			log.error("Error in getCustomerBillingAndShippingAddress", ex);
		}
		log.info("=============>End getCustomerBillingAndShippingAddress Method==========>");
		return customerMaster;
	}
	
	
}
