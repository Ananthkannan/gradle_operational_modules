package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.SocietyClassMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("societyClassMasterBean")
@Scope("session")
@Log4j2
public class SocietyClassMasterBean {

	final String LIST_PAGE = "/pages/masters/listSocietyClass.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createSocietyClass.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/viewSocietyClass.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String SOCIETY_CLASS_URL = AppUtil.getPortalServerURL() + "/societyclass";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	SocietyClassMaster societyClass, selectedSocietyClass;

	@Getter
	@Setter
	LazyDataModel<SocietyClassMaster> lazySocietyClassList;

	List<SocietyClassMaster> societyClassList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showSocietyClassListPage() {
		log.info("<==== Starts SocietyClassMasterBean.showSocietyClassListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		societyClass = new SocietyClassMaster();
		selectedSocietyClass = new SocietyClassMaster();
		loadLazySocietyClassList();
		log.info("<==== Ends SocietyClassMasterBean.showSocietyClassListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazySocietyClassList() {
		log.info("<===== Starts SocietyClassMasterBean.loadLazySocietyClassList ======>");

		lazySocietyClassList = new LazyDataModel<SocietyClassMaster>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 8884297903748650093L;

			@Override
			public List<SocietyClassMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("Pagination request :::" + paginationRequest.getFilters());

					String url = SOCIETY_CLASS_URL + "/getallsocietyclasslistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						societyClassList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyClassMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return societyClassList;
			}

			@Override
			public Object getRowKey(SocietyClassMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyClassMaster getRowData(String rowKey) {
				for (SocietyClassMaster societyClass : societyClassList) {
					if (societyClass.getId().equals(Long.valueOf(rowKey))) {
						selectedSocietyClass = societyClass;
						return societyClass;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  SocietyClassMasterBean.loadLazySocietyClassList  ======>");
	}

	public String showSocietyClassListPageAction() {
		log.info("<===== Starts SocietyClassMasterBean.showSocietyClassListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedSocietyClass == null || selectedSocietyClass.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create SocietyClass called..");
				societyClass = new SocietyClassMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit SocietyClass called..");
				String url = SOCIETY_CLASS_URL + "/getbyid/" + selectedSocietyClass.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					societyClass = mapper.readValue(jsonResponse, new TypeReference<SocietyClassMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View SocietyClass called..");

				String url = SOCIETY_CLASS_URL + "/getbyid/" + selectedSocietyClass.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					societyClass = mapper.readValue(jsonResponse, new TypeReference<SocietyClassMaster>() {
					});
					return VIEW_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete SocietyClass called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyClassDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showSocietyClassListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends SocietyClassMasterBean.showSocietyClassListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts SocietyClassMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SOCIETY_CLASS_URL + "/deletebyid/" + selectedSocietyClass.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SOCIETYCLASS_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyClassDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete salesType ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyClassMasterBean.deleteConfirm ===========>");
		return showSocietyClassListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts SocietyClassMasterBean.saveOrUpdate =======>");
		try {

			if (societyClass.getTurnoverMin() != null && societyClass.getTurnoverMax() != null) {
				if (societyClass.getTurnoverMin() > societyClass.getTurnoverMax()) {
					log.error("MinTurnOver should be less than MaxTurnOver");
					Util.addWarn("Min Turn Over should be less than Max TurnOver");
					return null;
				}
			}
			if (societyClass.getCode() == null || societyClass.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.SOCIETYCLASS_CODE_EMPTY.getCode());
				return null;
			}
			if (societyClass.getName() == null || societyClass.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.SOCIETYCLASS_NAME_EMPTY.getCode());
				return null;
			}
			if (societyClass.getName().trim().length() < 3 || societyClass.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.SOCIETYCLASS_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (societyClass.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.SOCIETYCLASS_STATUS_EMPTY.getCode());
				return null;
			}
			String url = SOCIETY_CLASS_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, societyClass);
			if (response != null && response.getStatusCode() == 0) {
				log.info("societyClass saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.SOCIETYCLASS_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.SOCIETYCLASS_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends SocietyClassMasterBean.saveOrUpdate =======>");
		return showSocietyClassListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SocietyClassMasterBean.onRowSelect ========>" + event);
		selectedSocietyClass = ((SocietyClassMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends SocietyClassMasterBean.onRowSelect ========>");
	}

}
