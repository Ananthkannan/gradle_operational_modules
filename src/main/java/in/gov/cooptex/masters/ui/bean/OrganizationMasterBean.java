package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.OrganizationMaster;
import in.gov.cooptex.core.model.OrganizationTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("organizationMasterBean")
public class OrganizationMasterBean {
	@Getter
	@Setter
	String action;
	@Getter
	@Setter
	Boolean organizationAddressFlag = false;
	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	OrganizationMaster organizationMaster, selectedOrganizationMaster;
	final String SERVER_URL = AppUtil.getPortalServerURL();
	@Autowired
	AppPreference appPreference;
	@Autowired
	HttpService httpService;
	ObjectMapper mapper;
	String jsonResponse;
	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AddressMasterBean addressMasterBean = new AddressMasterBean();

	@Setter
	@Getter
	List<OrganizationMaster> organizationMasterList;
	@Getter
	@Setter
	LazyDataModel<OrganizationMaster> lazyOrganizationMasterList;

	@Getter
	@Setter
	List<StateMaster> stateList;
	@Getter
	@Setter
	int totalRecords = 0;

	@Setter
	@Getter
	List<OrganizationTypeMaster> organizationTypeMasterList;
	@Getter
	@Setter
	String address = "";
	@Getter
	@Setter
	List<OrganizationMaster> parentOrganizationMasterList;

	@Getter
	@Setter
	String selectedOrganizationNameCode;
	
	@Getter
	@Setter
	OrganizationMaster parentOrganization=null;

	private final String CREATE_PAGE = "/pages/masters/createOrganizationMaster.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listOrganizationMaster.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE = "/pages/masters/viewOrganizationMaster.xhtml?faces-redirect=true;";

	public String showOrganizationMasterListPageAction() {
		log.info("<===== Starts OrganizationMasterBean.showOrganizationMasterListPageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedOrganizationMaster == null || selectedOrganizationMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create OrganizationMaster called..");
				organizationMaster = new OrganizationMaster();
				addressMasterBean.setAddressMaster(new AddressMaster());
				organizationMaster.setOrganizationMaster(new OrganizationMaster());
				address = "";

				loadAllOrganizationTypes();
				parentOrganization=new OrganizationMaster();

				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit OrganizationMaster called..");
				loadAllOrganizationTypes();
				parentOrganization=new OrganizationMaster();
				addressMasterBean.setAddressMaster(new AddressMaster());

				// organizationMaster = selectedOrganizationMaster;

				String url = SERVER_URL + "/organisationmaster/get/" + selectedOrganizationMaster.getId();

				selectedOrganizationMaster = new OrganizationMaster();
				log.info("edit OrganizationMaster======.." + url);
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					log.info("edit OrganizationMaster======...." + response);
					ObjectMapper mapper = new ObjectMapper();
					// mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
					// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());

					organizationMaster = mapper.readValue(jsonResponse, new TypeReference<OrganizationMaster>() {
					});
					log.info("<<:::orgnizationMaster::::::>>" + organizationMaster);

					addressMasterBean.setAddressMaster(organizationMaster.getAddressMaster());
					address = addressMasterBean.prepareAddress(organizationMaster.getAddressMaster());
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}

				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View OrganizationMaster called..");
				String url = SERVER_URL + "/organisationmaster/get/" + selectedOrganizationMaster.getId();
				log.info("view OrganizationMaster======.." + url);
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					log.info("view OrganizationMaster======.." + response);
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					organizationMaster = mapper.readValue(jsonResponse, new TypeReference<OrganizationMaster>() {
					});
					return VIEW_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete OrganizationMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmOrganizationMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showOrganizationMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends OrganizationMasterBean.showOrganizationMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info(
				"<===== Starts OrganizationMasterBean.deleteConfirm ===========>" + selectedOrganizationMaster.getId());
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/organisationmaster/deletebyid/" + selectedOrganizationMaster.getId());

			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("OrganizationMaster Details Deleted Successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmOrganizationMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete OrganizationMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends OrganizationMasterBean.deleteConfirm ===========>");
		return showOrganizationMasterListPage();
	}

	public void saveAddress() {

		organizationMaster.setAddressMaster(addressMasterBean.getAddressMaster());

		address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("orgForm");
	}

	public String saveOrUpdate() {
		log.info("<==== Starts OrganizationMasterBean.saveOrUpdate =======>");
		try {

			if(parentOrganization!=null && parentOrganization.getId()==null)
			{
				organizationMaster.setOrganizationMaster(null);
			}else
			{
			   organizationMaster.setOrganizationMaster(parentOrganization);
			}
			String url = SERVER_URL + "/organisationmaster/saveorupdate";
			BaseDTO response = httpService.post(url, organizationMaster);

			if (response != null && response.getStatusCode() == 0) {
				log.info("OrganizationMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					AppUtil.addInfo("OrganizationMaster Details Added Successfully");
				else
					AppUtil.addInfo("OrganizationMaster Details Updated Successfully");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}	
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends OrganizationMasterBean.saveOrUpdate =======>");
		return showOrganizationMasterListPage();
	}

	public String showOrganizationMasterListPage() {
		log.info("<==== Starts OrganizationMasterBean.showOrganizationMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		organizationMaster = new OrganizationMaster();
		selectedOrganizationMaster = new OrganizationMaster();
		loadLazyOrganizationMasterList();
		log.info("<==== Ends OrganizationMasterBean.showOrganizationMasterListPage =====>");
		return LIST_PAGE;
	}

	public List<OrganizationTypeMaster> loadAllOrganizationTypes() {
		log.info("<======== OrganizationMasterBean.loadAllOrganizationTypes calling ======>");
		organizationTypeMasterList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + "/organizationtypemaster/getActiveOrganizationTypes";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				organizationTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<OrganizationTypeMaster>>() {
						});
			} else {
				organizationTypeMasterList = new ArrayList<>();
			}
			log.info("OrganizationMasterBean loadAllOrganizationTypes size" + organizationTypeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception OrganizationMasterBean.loadAllOrganizationTypes ==========>", e);
		}
		return organizationTypeMasterList;
	}

	public List<OrganizationMaster> loadOrganizationMasterList() {
		log.info("OrganizationMasterBean - loadOrganizationMasterList-->");
		parentOrganizationMasterList = new ArrayList<>();
		try {

			String url = AppUtil.getPortalServerURL() + "/organisationmaster/getallorganizationmaster";
			log.info(" OrganizationMaster GetAll Value URL ===>>> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
				// mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				parentOrganizationMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<OrganizationMaster>>() {
						});
			} else {
				parentOrganizationMasterList = new ArrayList<>();
			}
			log.info("OrganizationMasterBean loadAllOrganizationMaster size" + parentOrganizationMasterList.size());
		} catch (Exception e) {
			log.info("inside loadOrganizationMasterList method--", e);
		}
		return parentOrganizationMasterList;
	}
	
	public List<OrganizationMaster> getOrganizationAutoComplecityData(String query) {
		log.info("START OrganizationMasterBean - getOrganizationAutoComplecityData-->");
		try
		{
			String url = AppUtil.getPortalServerURL() + "/organisationmaster/getOrganizationListAutoComplete/"+query;
			log.info(" OrganizationMaster GetAll Value URL ===>>> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);
				// mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				parentOrganizationMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<OrganizationMaster>>() {
						});
			}
		}catch (Exception e) {
			log.info("Exception Occured in OrganizationMasterBean - getOrganizationAutoComplecityData-->  "+e);
		}
		log.info("END OrganizationMasterBean - getOrganizationAutoComplecityData-->");
		return parentOrganizationMasterList;
		
	}

	public void loadLazyOrganizationMasterList() {
		log.info("<===== Starts OrganizationMasterBean.loadLazyOrganizationMasterList ======>");

		lazyOrganizationMasterList = new LazyDataModel<OrganizationMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<OrganizationMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);

					String url = SERVER_URL + "/organisationmaster/getallorganizationmasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();

						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						organizationMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<OrganizationMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("OrganizationMasterBean size---------------->"
								+ lazyOrganizationMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return organizationMasterList;
			}

			@Override
			public Object getRowKey(OrganizationMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OrganizationMaster getRowData(String rowKey) {
				for (OrganizationMaster organizationMaster : organizationMasterList) {
					if (organizationMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedOrganizationMaster = organizationMaster;
						return organizationMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  OrganizationMasterBean.loadLazyOrganizationMasterList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts OrganizationMasterBean.onRowSelect ========>" + event);
		selectedOrganizationMaster = ((OrganizationMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends OrganizationMasterBean.onRowSelect ========>" + selectedOrganizationMaster.getId());
	}
}
