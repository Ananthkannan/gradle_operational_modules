/**
 * 
 */
package in.gov.cooptex.operation.rest.ui;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("salesTargetReportBean")
public class SalesTargetReportBean {
	
	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	Map<Long, String> monthList;
	
	@Getter
	@Setter
	List<String> monthNameList;
	
	@Getter
	@Setter
	String fromMonth;
	
	
	@PostConstruct
	public void init() {
		log.info(" Sales Target Report Init method loaded ");
		 monthNameList = AppUtil.getAllMonthNames();
		 for(Map.Entry<Long, String> mList : monthList.entrySet()) {
			log.info(" month Key ===>>> " + mList.getKey() +   " Month Name ====>>> " + mList.getValue());
		 }
		
	}
	
	
	
	

}
