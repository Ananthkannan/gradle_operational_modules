package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class CountryMasterBean {

	private final String CREATE_COUNTRY_MASTER_PAGE = "/pages/masters/createCountryMaster.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listCountryMaster.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewCountryMaster.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	CountryMaster countryMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<CountryMaster> countryMasterList = new ArrayList<CountryMaster>();

	@Getter
	@Setter
	private int countryMasterListSize;
	
	@Getter
	@Setter
	Boolean addButton=true;
	
	@Getter
	@Setter
	int totalRecords = 0;

	boolean forceLogin = false;

	@PostConstruct
	public void init() {
		log.info(" CountryMasterBean init --->" + serverURL);
		
		loadValues();
		addCountry();
		getAllCountriesList();
		getCountryView(countryMaster);
	}

	public String showList() {
		log.info("<---------- Loading CountryMasterBean showList list page---------->");
		loadValues();
		getAllCountriesList();
		addButton=true;
		return INPUT_FORM_LIST_URL;
	}

	public String addCountry() {
		log.info("<---- Start CountryMasterBean.addCountry ---->");
		try {
			action = "ADD";
			countryMaster = new CountryMaster();
			countryMaster.setStatus(true);

			log.info("<---- End CountryMaster.addCountry ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_COUNTRY_MASTER_PAGE;

	}

	private void getAllCountriesList() {
		log.info("<<<< ----------Start countryMasterBean . loadAllUserList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/country/getall";
			baseDTO = httpService.get(url);
			countryMaster = new CountryMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				countryMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CountryMaster>>() {
				});
				totalRecords =countryMasterList.size();
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (countryMasterList != null) {
			countryMasterListSize = countryMasterList.size();
		} else {
			countryMasterListSize = 0;
		}
		log.info("<<<< ----------End CountryMasterBean . getAllCountriesList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			countryMaster = new CountryMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		countryMaster = new CountryMaster();
		addButton=true;
		return INPUT_FORM_LIST_URL;

	}
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PayScaleBean.onRowSelect ========>" + event);
		addButton=false;
		log.info("<===Ends PayScaleBean.onRowSelect ========>");
	}

	private boolean checkDuplicateCountry() {
		boolean valid=true;
			
			if(countryMaster.getName()!=null  && countryMaster.getName().length()>0){
				if(countryMaster.getName().length()>=5 ) {
					
				}else {
					AppUtil.addError("Country Name minimumm 5 characters");
					return false;
				}
			}
			
			if(countryMaster.getLcountryName()!=null  && countryMaster.getLcountryName().length()>0){
				if(countryMaster.getLcountryName().length()>=5 ) {
					
				}else {
					AppUtil.addError("Country Name (In Tamil) minimumm 5 characters");
					return false;
				}
			}

			
		
		if(countryMaster.getId()!=null && countryMaster.getId()>0){
			return valid;
		}
		
		BaseDTO baseDTO = null;
		baseDTO = httpService.post(serverURL + "/country/checkDuplicateCountryCode", countryMaster);
		if(baseDTO.getStatusCode()==1){
			AppUtil.addError("Country Code alredy exists ");
			return false;
		}
		
		BaseDTO baseDTO1 = null;
		baseDTO1 = httpService.post(serverURL + "/country/checkDuplicateCountryName", countryMaster);
		if(baseDTO1.getStatusCode()==1){
			AppUtil.addError("Country Name alredy exists ");
			return false;
		}
		
		BaseDTO baseDTO2 = null;
		baseDTO2 = httpService.post(serverURL + "/country/checkDuplicateCountryLocatioName", countryMaster);
		if(baseDTO2.getStatusCode()==1){
			AppUtil.addError("Country Name (In Tamil) alredy exists ");
			return false;
		}
		
		return valid;
	}
	
	public String submit() {
		log.info("...... CountryMasterBean submit begin ....");
		try {
			if(checkDuplicateCountry()) {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + "/country/create", countryMaster);
			if (baseDTO != null) {
				if (baseDTO.getMessage()!=null && baseDTO.getMessage().equalsIgnoreCase("SUCCESS")) {
					log.info("CountryMasterBean saved successfully");
					showList();
					if(action.equals("ADD"))
					{
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUNTRY_MASTER_CREATED_SUCCESSFULLY).getCode());
					}else
					{
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUNTRY_MASTER_UPDATED_SUCCESSFULLY).getCode());
					}
					//errorMap.notify(baseDTO.getStatusCode());
					return INPUT_FORM_LIST_URL;
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		}
		} catch (Exception e) {
			log.error("Error while creating CountryMaster" + e);
		}
		log.info("...... CountryMasterBean submit ended ....");
		return null;
	}

	public String editCountry() {
		log.info("<<<<< -------Start editCountry called ------->>>>>> ");
		action = "EDIT";

		if (countryMaster == null) {
			Util.addWarn("Please select one country");
			return null;
		}

		if (countryMaster.getStatus().equals("ACTIVE")) {
			countryMaster.setStatus(countryMaster.getStatus());
		} else {
			countryMaster.setStatus(countryMaster.getStatus());
		}
		log.info("<<<<< -------End editCountry called ------->>>>>> ");

		return CREATE_COUNTRY_MASTER_PAGE;

	}

	public String deleteCountry() {
		try {
			action = "Delete";
			if (countryMaster == null) {
				Util.addWarn("Please Select One country");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCountryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteCountryConfirm() {

		try {
			log.info("Delete confirmed for country [" + countryMaster.getName() + "]");
			CountryMaster countryMasterDelete = new CountryMaster();
			countryMasterDelete.setId(countryMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/country/delete";
			baseDTO = httpService.post(url, countryMaster);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("Country deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					countryMaster = new CountryMaster();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUNTRY_MASTER_DELETED_SUCCESSFULLY).getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start countryMasterBean . clear ----->");
		getAllCountriesList();
		countryMaster = new CountryMaster();
		log.info("<---- End CountryMasterBean . clear ----->");
		addButton=true;
		return INPUT_FORM_LIST_URL;

	}

	public String backPage() {

		showList();
		addButton=true;
		return INPUT_FORM_LIST_URL;
	}

	public String viewCountry() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (countryMaster == null) {
			Util.addWarn("Please Select One Country");
			return null;
		}
		return INPUT_FORM_VIEW_URL;

	}

	private void getCountryView(CountryMaster countryMaster) {
		log.info("<<<< ----------Start countryMasterBean . getCountryView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/country/getcountry";
			baseDTO = httpService.post(url, countryMaster);
			countryMaster = new CountryMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				countryMaster = mapper.readValue(jsonResponse, new TypeReference<CountryMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (countryMaster != null)
			log.info("countrymaster getCountryView" + countryMaster);
		else
			log.info("countrymaster getCountryView" + countryMaster);

		log.info("<<<< ----------End CountryMasterBean . getCountryView ------- >>>>");
	}

}
