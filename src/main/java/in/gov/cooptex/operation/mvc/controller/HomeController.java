/**
 * 
 */
package in.gov.cooptex.operation.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.extern.log4j.Log4j2;

/**
 * Controller that demonstrates tiles mapping, reguest parameters and path
 * variables.
 * 
 * @author GMR
 */
@Controller
@Log4j2
@RequestMapping("/home")
public class HomeController {

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String login() {
		log.info("Welcome");
		return "jsp.welcome.home";
	}

}