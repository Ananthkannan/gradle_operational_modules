package in.gov.cooptex.accounts.rest.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.dto.VoucherInvoiceDetailsResponseDTO;
import in.gov.cooptex.core.accounts.dto.VoucherRequestDTO;
import in.gov.cooptex.core.accounts.dto.VoucherResponseDTO;
import in.gov.cooptex.core.accounts.dto.VoucherSupplierDetailsResponseDTO;
import in.gov.cooptex.core.accounts.enums.VoucherPaymentType;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.accounts.model.Voucher;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeeTypeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.core.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("voucherBean")
@Scope("session")
@Log4j2
public class VoucherBean {
	
	@Getter @Setter
	List<String>  paymentVoucherTypeList;	
	
	@Getter @Setter
	String paymentVoucherType;
	
	private static final String LIST_PAYMENT_VOUCHER = "/pages/accounts/paymentVoucher/listPaymentVoucher.xhtml?faces-redirect=true;";
	
	private static final String CREATE_PAYMENT_VOUCHER = "/pages/accounts/paymentVoucher/createPaymentVoucher.xhtml?faces-redirect=true;";
	
	private static final String PREVIEW_PAYMENT_VOUCHER = "/pages/accounts/paymentVoucher/previewPaymentVoucher.xhtml?faces-redirect=true;";
	
	private static final String VIEW_PAYMENT_VOUCHER = "/pages/accounts/paymentVoucher/viewPaymentVoucher.xhtml?faces-redirect=true;";
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	List<SupplierTypeMaster> supplierTypeMasterList;
	
	@Getter @Setter
	SupplierTypeMaster supplierTypeMaster;
	
	@Getter @Setter
	List<SupplierMaster> vendorsList;
	
	@Getter @Setter
	SupplierMaster supplierMaster;
	
	@Getter @Setter
	List<EmployeeTypeMaster> employeeTypeMasterList;
	
	@Getter @Setter
	EmployeeTypeMaster employeeTypeMaster;
	
	@Getter @Setter
	VoucherResponseDTO voucherResponseDTO;
	
	@Getter @Setter
	Voucher voucher;
	
	@Getter @Setter
	LazyDataModel<VoucherResponseDTO> voucherResponseDTOList;
	
	@Autowired
	AppPreference appPreference;
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap; 
	
	@Getter @Setter
	List<String> statusValuesList;
	
	@Getter @Setter
	boolean addButtonFlag=false;
	
	@Getter	@Setter
	Integer size;
	
	@Getter	@Setter
	boolean enableFlag;
	
	@Getter	@Setter
	String societyCodeName;
	
	@Getter	@Setter
	Date fromDate;
	
	@Getter	@Setter
	Date toDate;
	
	@Getter	@Setter
	Date updatedTodate;
	
	@Getter	@Setter
	List<VoucherSupplierDetailsResponseDTO> voucherSupplierDetailsList;
	
	@Getter	@Setter
	List<VoucherInvoiceDetailsResponseDTO> voucherInvoiceDetailsList;
	
	@Getter	@Setter
	List<VoucherInvoiceDetailsResponseDTO> voucherInvoiceAdjustmentDetailsList;
	
	@Getter	@Setter
	Double amountWithoutTax,amountWithoutTax1;
	
	@Getter	@Setter
	Double tax,tax1;
	
	@Getter	@Setter
	Double totalPayableAmount,totalPayableAmount1;
	
	@Getter	@Setter
	Double totalAdjustmentAmount,totalAdjustmentAmount1,totalAdjustmentAmount3;
	
	@Getter	@Setter
	Double totalAmountPaid,totalAmountPaid1;
	
	@Getter	@Setter
	Double balance,balance1;
	
	@Getter	@Setter
	Double totalAmountToBePaid,totalAmountToBePaid1;
	
	@Getter	@Setter
	Double percentage;
	
	@Getter	@Setter
	List<PaymentMode> paymentModeList;
	
	@Getter	@Setter
	PaymentMode paymentModeDet; 
	
	@Getter	@Setter
	String narration;
	
	@Getter	@Setter
	String action;
	
	@Getter @Setter
	String voucherNumber;
	
	@Getter @Setter
	String voucherCreatedDate;
	
	@Getter @Setter
	EntityMaster entityMaster; 
	
	@Getter @Setter
	String vendorTypeName;
	
	@Getter @Setter
	String paymentMode;
	
	@Getter @Setter
	String approveComments;
	
	@Getter @Setter
	String rejectComments;
	
	@Getter
	@Setter
	private String currentPlanStatus;
	
	@Autowired
	LanguageBean languageBean; 
	
	@Getter @Setter
	String productwareHouseAddressDetails;
	
	@Autowired
	LoginBean loginBean; 
	
	@Getter @Setter
	String voucherCreatedBy;
	
	@Getter @Setter
	EmployeeMaster empDet;
	
	@Getter @Setter
	String amountInWords;
	
	@Getter @Setter
	String voucherApprovedBy;
	
	@Getter @Setter
	String headOfficeName;
	
	public VoucherBean() {
		log.info("<--- VoucherBean VoucherBean() --->");
	}
	
	public String getVoucherList(){
		log.info("<--- VoucherBean getVoucherList() --->");
		
		loadLazyPaymentVoucherList();		
		
		loadStatusValues();
		
		return LIST_PAYMENT_VOUCHER;
	}
	
	private void getUserEntityDetails(){
		
		String getUserEntityDetailsPath = appPreference.getPortalServerURL()+"/accounts/voucher/getuserentitydetails";
		log.info(" getUserEntityDetailsPath : "+getUserEntityDetailsPath);
		try {
			BaseDTO baseDTO = httpService.get(getUserEntityDetailsPath);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			voucherResponseDTO = mapper.readValue(jsonResponse, VoucherResponseDTO.class);
			
			entityMaster=voucherResponseDTO.getEntityMaster();
			
			empDet = voucherResponseDTO.getEmployeeMaster();
			
			headOfficeName = voucherResponseDTO.getHeadOfficeName();
			 
			String societyAddressArray[] = AppUtil.prepareAddress(entityMaster.getAddressMaster());
			
			if(languageBean.getLocaleCode().equals("en_IN")) {
				productwareHouseAddressDetails = societyAddressArray[0];
			}else {
				productwareHouseAddressDetails = societyAddressArray[1];
			}
			log.info("Logged in Head Office Address Details  " + productwareHouseAddressDetails);
			
		} catch (Exception e) {
			log.error("Exception in create ", e);
		}
		
	}
	
	private void loadStatusValues() {
		Object[] statusArray = VoucherStatus.class.getEnumConstants();

		statusValuesList = new ArrayList<>();

		for (Object status : statusArray) {
			statusValuesList.add(status.toString());
		}

		log.info("<--- statusValuesList ---> " + statusValuesList);
	}
	
	
	public void loadLazyPaymentVoucherList() {
		log.info("<--- loadLazyPaymentVoucherList ---> ");
		
		voucherResponseDTOList = new LazyDataModel<VoucherResponseDTO>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<VoucherResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<VoucherResponseDTO> data = new ArrayList<VoucherResponseDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					
					log.info("<--- jsonResponse1  ---> "+jsonResponse);
					
					data = mapper.readValue(jsonResponse, new TypeReference<List<VoucherResponseDTO>>() {});
					
					log.info("<--- data  ---> "+data);

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  "+baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					} 
				} catch (Exception e) {
					log.error("Error in loadLazyPaymentVoucherList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(VoucherResponseDTO res) {
				return res != null ? res.getVoucherId() : null;
			}

			@Override
			public VoucherResponseDTO getRowData(String rowKey) {
				List<VoucherResponseDTO> responseList = (List<VoucherResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (VoucherResponseDTO res : responseList) {
					if (res.getVoucherId().longValue() == value.longValue()) {
						voucherResponseDTO = res;
						return res;
					}
				}
				return null;
			}

		};
	}
	
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		voucherResponseDTO = new VoucherResponseDTO();
		
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		
		VoucherRequestDTO voucherRequest = new VoucherRequestDTO();
		
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		voucherRequest.setPaginationDTO(paginationDTO);

		log.info("voucherRequestDTO  "+voucherRequest);
		voucherRequest.setFilters(filters);
		
		try {
			String voucherSearchUrl = appPreference.getPortalServerURL() + "/accounts/voucher/search";
			log.info("Voucher Search Url " + voucherSearchUrl);
			baseDTO = httpService.post(voucherSearchUrl, voucherRequest);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
	
	public String gotoCreateVoucherForm(){
		log.info("<--- VoucherBean createVoucher() --->");
		
		getUserEntityDetails();
		
		paymentVoucherType=null;
		
		Object[] voucherPaymentTypeArray = VoucherPaymentType.class.getEnumConstants();
		
		paymentVoucherTypeList = new ArrayList<>();
		 
		for (Object voucherPaymentType : voucherPaymentTypeArray) {
			paymentVoucherTypeList.add(voucherPaymentType.toString());
		}
		
		log.info("<--- VoucherBean createVoucher() paymentVoucherTypeList ---> "+paymentVoucherTypeList);
		
		supplierTypeMasterList = commonDataService.loadSupplierTypeMaster();
		
		log.info("<--- VoucherBean createVoucher() supplierTypeMasterList ---> "+supplierTypeMasterList);
		
		employeeTypeMasterList = commonDataService.loadEmployeeTypeMaster();
		
		log.info("<--- VoucherBean createVoucher() employeeTypeMasterList ---> "+employeeTypeMasterList);
		
		supplierTypeMaster = new SupplierTypeMaster();
		
		vendorsList = new ArrayList<>();
		supplierMaster=new SupplierMaster();
		
		voucherSupplierDetailsList=new ArrayList<>();
		
		amountWithoutTax=null;
		amountWithoutTax1=null;	
		
		tax=null;
		tax1=null;		
		
		totalPayableAmount=null;
		totalPayableAmount1=null;		

		totalAdjustmentAmount=null;
		totalAdjustmentAmount1=null;
		totalAdjustmentAmount3=null;
		
		totalAmountPaid=null;
		totalAmountPaid1=null;
		
		balance=null;
		balance1=null;
		
		totalAmountToBePaid=null;
		totalAmountToBePaid1=null;
		
		paymentModeList = commonDataService.loadPaymentModes();
		
		percentage =null;
		narration=null;
				
		return CREATE_PAYMENT_VOUCHER;
	}
	
	public void onchangePaymentVoucherType(){
		log.info("<--- VoucherBean onchangePaymentVoucherType() ---> " +paymentVoucherType);
		
		if(StringUtils.isEmpty(paymentVoucherType)){
			enableFlag=false;
			return;
		}
		
		enableFlag=true;
				
	}
	
	public void onchangeVendorType(){
		log.info("<--- VoucherBean onchangePaymentVoucherType() ---> " +supplierTypeMaster.getId());
		
		if(supplierTypeMaster==null){
			vendorsList = new ArrayList<>();
			return;
		}
		
		Long vendortypeId = supplierTypeMaster.getId();	
		log.info(" vendortypeId : "+vendortypeId);
		String vedorsListPath = appPreference.getPortalServerURL()+"/accounts/voucher/getvendorslist/"+vendortypeId;
		log.info(" vedorsListPath : "+vedorsListPath);
		try {
			BaseDTO baseDTO = httpService.get(vedorsListPath);
			ObjectMapper mapper = new ObjectMapper();
			if(baseDTO !=null){
				vendorsList = mapper.readValue(mapper.writeValueAsString(
						baseDTO.getResponseContent()),new TypeReference<List<SupplierMaster>>() {});
			}
		} catch (Exception e) {
			log.error("Exception in onchangeVendorType ", e);
		}
	}
	
	public void onchangeVendor(){
		log.info("<--- VoucherBean onchangeVendor() ---> " +supplierMaster);
						
	}
	
	public void	updateTodate(){
		log.info("<--- VoucherBean updateTodate() ---> "+fromDate);
		updatedTodate = fromDate;
	}
	
	public void search(){
		log.info("<--- VoucherBean search() ---> ");
		
		prepareSupplierDetails();
		
	//	return CREATE_PAYMENT_VOUCHER;
	}
	
	private void prepareSupplierDetails(){
		/*log.info("<--- Payment For ---> "+paymentVoucherType);
		log.info("<--- Vendor Type ---> "+supplierTypeMaster.getId()+"/"+supplierTypeMaster.getSupplierType().name());
		log.info("<--- Vendor Code / Name ---> "+supplierMaster.getId()+"/"+supplierMaster.getCode()+"/"+supplierMaster.getName());
		log.info("<--- From Date ---> "+fromDate);
		log.info("<--- To Date ---> "+toDate);*/
		
		VoucherRequestDTO voucherRequestDTO = new VoucherRequestDTO();
		voucherRequestDTO.setSupplierId(supplierMaster.getId());
		voucherRequestDTO.setFromDate(fromDate);
		voucherRequestDTO.setToDate(toDate);
		if("VIEW".equalsIgnoreCase(action)){
			voucherRequestDTO.setVoucherId(voucher.getId());
			voucherRequestDTO.setActionFlag(action);
		}
				
		String supplierDetailsPath = appPreference.getPortalServerURL()+"/accounts/voucher/getsupplierdetails";
		log.info(" supplierDetailsPath : "+supplierDetailsPath);
		try {
			BaseDTO baseDTO = httpService.post(supplierDetailsPath,voucherRequestDTO);
			ObjectMapper mapper = new ObjectMapper();
			if(baseDTO !=null){
				voucherSupplierDetailsList = mapper.readValue(mapper.writeValueAsString(
						baseDTO.getResponseContent()),new TypeReference<List<VoucherSupplierDetailsResponseDTO>>() {});
				
				if(voucherSupplierDetailsList!=null && voucherSupplierDetailsList.size() >0){
					prepareFooterTotal(voucherSupplierDetailsList);
				}else{
					log.info("No Invocies available for the selected Invoice Dates");
					AppUtil.addWarn("No Invocies available for the selected Invoice Dates");
					return;
				}
				
				/*amountWithoutTax=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getAmountWithoutTax).sum(); 
				tax=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTax).sum();
				totalPayableAmount=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalPayableAmount).sum();
				totalAdjustmentAmount=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAdjustmentAmount).sum();
				totalAmountPaid=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAmountPaid).sum();
				balance=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getBalance).sum();
				totalAmountToBePaid=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAmountToBePaid).sum();*/	
			}
		} catch (Exception e) {
			log.error("Exception in onchangeVendorType ", e);
		}
	}
	
	public void prepareFooterTotal(List<VoucherSupplierDetailsResponseDTO> voucherSupplierDetailsList){
		amountWithoutTax=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getAmountWithoutTax).sum(); 
		tax=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTax).sum();
		totalPayableAmount=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalPayableAmount).sum();
		totalAdjustmentAmount=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAdjustmentAmount).sum();
		totalAmountPaid=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAmountPaid).sum();
		balance=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getBalance).sum();
		totalAmountToBePaid=voucherSupplierDetailsList.stream().mapToDouble(VoucherSupplierDetailsResponseDTO::getTotalAmountToBePaid).sum();
	}
	
	public void calculateTotalAmountToBePaid(){
		log.info("<--- calculateTotalAmountToBePaid ---> "+percentage);
		for(VoucherSupplierDetailsResponseDTO supRes : voucherSupplierDetailsList){
			
			Double totalAmountToBePaid = supRes.getBalance();			
			
			totalAmountToBePaid=totalAmountToBePaid * (percentage / 100);
			
			supRes.setTotalAmountToBePaid(totalAmountToBePaid);
			
		}
		
		prepareFooterTotal(voucherSupplierDetailsList);
		
	}
	
	public void gotoInvoiceDetails(VoucherSupplierDetailsResponseDTO supplierDetails){
		log.info("<--- gotoInvoiceDetails ---> "+supplierDetails);
		
		if(supplierDetails==null){
			log.info("<--- supplierDetails==null ---> "+supplierDetails);
			return;
		}
		
		VoucherRequestDTO voucherRequestDTO = new VoucherRequestDTO();
		voucherRequestDTO.setSupplierId(supplierDetails.getSupplierId());
		voucherRequestDTO.setFromDate(fromDate);
		voucherRequestDTO.setToDate(toDate);
		
		if("VIEW".equalsIgnoreCase(action)){
			voucherRequestDTO.setVoucherId(voucher.getId());
			voucherRequestDTO.setActionFlag(action);
		}
		
		societyCodeName = supplierDetails.getSupplierCode()+" / "+supplierDetails.getSupplierName();
		
		String invoiceDetailsPath = appPreference.getPortalServerURL()+"/accounts/voucher/getinvoicedetails";
		log.info(" invoiceDetailsPath : "+invoiceDetailsPath);
		try {
			BaseDTO baseDTO = httpService.post(invoiceDetailsPath,voucherRequestDTO);
			ObjectMapper mapper = new ObjectMapper();
			if(baseDTO !=null){
				voucherResponseDTO = mapper.readValue(mapper.writeValueAsString(
						baseDTO.getResponseContent()),VoucherResponseDTO.class);
				
				voucherInvoiceDetailsList=voucherResponseDTO.getVoucherInvoiceDetailsList();
				Double availableAmount = totalAmountToBePaid;
				for(VoucherInvoiceDetailsResponseDTO invoiceDet : voucherInvoiceDetailsList){
					if(availableAmount==0){
						invoiceDet.setTotalAmountToBePaid(0D);
					}
					
					if(availableAmount>0){
						availableAmount = availableAmount - invoiceDet.getTotalAmountToBePaid();
					}
					/*if(availableAmount > 0 && availableAmount!=invoiceDet.getTotalAmountToBePaid()){
						availableAmount = availableAmount - invoiceDet.getTotalAmountToBePaid();
					}*/
					 			
					if(availableAmount < 0){
						invoiceDet.setTotalAmountToBePaid(invoiceDet.getTotalAmountToBePaid()+availableAmount);
						availableAmount=0D;
						//isAmountAvialble =false;
					}
					/*if(availableAmount<0 && !isAmountAvialble){
						invoiceDet.setTotalAmountToBePaid(0D);
					}*/
				}
				
				voucherInvoiceAdjustmentDetailsList=voucherResponseDTO.getVoucherInvoiceAdjustmentDetailsList();
				
				amountWithoutTax1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getAmountWithoutTax).sum(); 
				tax1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getTax).sum();
				totalPayableAmount1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getTotalPayableAmount).sum();
				totalAdjustmentAmount1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getTotalAdjustmentAmount).sum();
				totalAmountPaid1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getTotalAmountPaid).sum();
				balance1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getBalance).sum();
				totalAmountToBePaid1=voucherInvoiceDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getTotalAmountToBePaid).sum();	
				
				totalAdjustmentAmount3=voucherInvoiceAdjustmentDetailsList.stream().mapToDouble(VoucherInvoiceDetailsResponseDTO::getAdjustmentAmount).sum();
			}
		} catch (Exception e) {
			log.error("Exception in onchangeVendorType ", e);
		}
		
	}
	
	
	
	public String gotoPreviewPaymentVoucher(){
		log.info("fromDatefromDatefromDatefromDate"+fromDate);
		log.info("toDatetoDatetoDatetoDatetoDate"+toDate);
		
		
		BaseDTO baseDTO= null;
			
			try {
				
				if("ADD".equalsIgnoreCase(action)){
				
					voucherCreatedDate = AppUtil.getFormattedCurrentDate();
					
					String prefixPath = appPreference.getPortalServerURL() + "/accounts/voucher/generatevoucherprefix";
					
					baseDTO = httpService.get(prefixPath);
					
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					voucherNumber = mapper.readValue(jsonValue,  String.class);
					
					log.info("voucherNumber :: "+voucherNumber);
				
			} else{
				 voucherNumber = voucher.getReferenceNumberPrefix()+"-"+voucher.getReferenceNumber();
				 SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
				 voucherCreatedDate = formatter.format(voucher.getCreatedDate());
				
			}
				amountInWords = AmountToWordConverter.converter(String.valueOf(totalAmountToBePaid));
				
				VoucherSupplierDetailsResponseDTO getInvoiceDet = new VoucherSupplierDetailsResponseDTO();
				getInvoiceDet.setSupplierId(supplierMaster.getId());
				
				gotoInvoiceDetails(getInvoiceDet);
				
				
			} catch (Exception exp) {																														
				 log.error("Error in VoucherBean gotoPreviewPaymentVoucher()  ", exp);
			}	
		
		return PREVIEW_PAYMENT_VOUCHER;		
	}
	
	
	public String submitVoucher(){
		log.info("<--- create ---> ");	
		
		VoucherRequestDTO voucherRequestDTO = new VoucherRequestDTO();
		voucherRequestDTO.setPaymentFor(paymentVoucherType);
		voucherRequestDTO.setVendorTypeId(supplierTypeMaster.getId());
		voucherRequestDTO.setFromDate(fromDate);
		voucherRequestDTO.setToDate(toDate);
		voucherRequestDTO.setPaymentModeId(paymentModeDet.getId());
		voucherRequestDTO.setVoucherSupplierDetailsList(voucherSupplierDetailsList);
		voucherRequestDTO.setVoucherInvoiceDetailsList(voucherInvoiceDetailsList);
		voucherRequestDTO.setNarration(narration);
		voucherRequestDTO.setSupplierId(supplierMaster.getId());
		voucherRequestDTO.setVoucherPrefix(voucherNumber.split("-")[0].trim()+"-"+voucherNumber.split("-")[1].trim());
		voucherRequestDTO.setVoucherNum(Integer.valueOf(voucherNumber.split("-")[2]));
		
		String createPath = appPreference.getPortalServerURL()+"/accounts/voucher/create";
		log.info(" createPath : "+createPath);
		try {
			BaseDTO baseDTO = httpService.post(createPath,voucherRequestDTO);

			if(baseDTO !=null && baseDTO.getStatusCode()==0){
				log.info("Voucher Submitted Successfully");
				errorMap.notify(ErrorDescription.VOUCHER_SUBMITTED_SUCCESSFULLY.getCode());
				getVoucherList();
			}else{
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
			}
		} catch (Exception e) {
			log.error("Exception in create ", e);
		}
		
		return LIST_PAYMENT_VOUCHER;
	}
	
	public void clear(){
		log.info("<--- VoucherBean clear() ---> ");
		paymentVoucherType=null;
		
		supplierTypeMasterList = commonDataService.loadSupplierTypeMaster();
		vendorsList=new ArrayList<>();
		
		supplierTypeMaster=new SupplierTypeMaster();
		supplierMaster=new SupplierMaster();
		
		fromDate = null;
		toDate=null;
		
		voucherSupplierDetailsList = new ArrayList<>();
		voucherInvoiceDetailsList=new ArrayList<>();
		
		percentage =null;
		narration=null;
		
		amountWithoutTax=null;
		amountWithoutTax1=null;	
		
		tax=null;
		tax1=null;		
		
		totalPayableAmount=null;
		totalPayableAmount1=null;		

		totalAdjustmentAmount=null;
		totalAdjustmentAmount1=null;
		totalAdjustmentAmount3=null;
		
		totalAmountPaid=null;
		totalAmountPaid1=null;
		
		balance=null;
		balance1=null;
		
		totalAmountToBePaid=null;
		totalAmountToBePaid1=null;
		
		paymentModeList = commonDataService.loadPaymentModes();
	}
	
	public String cancel(){
		log.info("<--- VoucherBean cancel() ---> ");
		
		getVoucherList();
		
		paymentVoucherType=null;
		
		Object[] voucherPaymentTypeArray = VoucherPaymentType.class.getEnumConstants();
		
		paymentVoucherTypeList = new ArrayList<>();
		 
		for (Object voucherPaymentType : voucherPaymentTypeArray) {
			paymentVoucherTypeList.add(voucherPaymentType.toString());
		}
		
		supplierTypeMasterList = commonDataService.loadSupplierTypeMaster();
		vendorsList=new ArrayList<>();
		
		supplierTypeMaster=new SupplierTypeMaster();
		supplierMaster=new SupplierMaster();
		
		fromDate = null;
		toDate=null;
		
		voucherSupplierDetailsList = new ArrayList<>();
		voucherInvoiceDetailsList=new ArrayList<>();
		
		return LIST_PAYMENT_VOUCHER;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("VoucherBean onRowSelect method started");
		voucherResponseDTO = ((VoucherResponseDTO) event.getObject());
		addButtonFlag=true;
    }
	
	public void onPageLoad() {
		log.info("VoucherBean onPageLoad method started");
		addButtonFlag=false;
	 }
	
	@Getter @Setter
	Boolean showButtom=true;
	
	public String gotoVoucherViewPage(){
		log.info("VoucherBean getoVoucherViewPage "+voucherResponseDTO);
		voucher = new Voucher();
		showButtom=true;
		
		if("APPROVED".equalsIgnoreCase(voucherResponseDTO.getVoucherStatus().toString())){
			showButtom=false;
		} 
		
		String createPath = appPreference.getPortalServerURL()+"/accounts/voucher/get/"+voucherResponseDTO.getVoucherId();
		log.info(" createPath : "+createPath);
		try {
			BaseDTO baseDTO = httpService.get(createPath);
			
			ObjectMapper mapper = new ObjectMapper();
			voucherResponseDTO = mapper.readValue(mapper.writeValueAsString(baseDTO.getResponseContent()),VoucherResponseDTO.class);
			
			//voucher = voucherResponseDTO.getVoucher();			
			vendorTypeName = voucherResponseDTO.getVendorTypeName();			
			supplierMaster = voucherResponseDTO.getSupplierMaster();			
			fromDate=voucherResponseDTO.getFromDate();
			toDate=voucherResponseDTO.getToDate();
			
			voucher.setId(voucherResponseDTO.getVoucherId());
			voucher.setFromDate(fromDate);
			voucher.setToDate(toDate);
			voucher.setReferenceNumberPrefix(voucherResponseDTO.getVoucherNumberPrfix());
			voucher.setReferenceNumber(voucherResponseDTO.getVoucherNumber());
			voucher.setCreatedDate(voucherResponseDTO.getVoucherCreatedDate());
			voucher.setPaymentFor(voucherResponseDTO.getPaymentFor());
			voucher.setNarration(voucherResponseDTO.getNarration());
			
			paymentModeDet = voucherResponseDTO.getPayMode();
			narration = voucher.getNarration();
			
			empDet = voucherResponseDTO.getEmployeeMaster();
			
			/*voucherApprovedBy = voucherResponseDTO.getVoucherApprovedBy();*/
			prepareSupplierDetails();
			
			getUserEntityDetails();
			 
		} catch (Exception e) {
			log.error("Exception in getoVoucherViewPage ", e);
		}
		
		return VIEW_PAYMENT_VOUCHER;
	}
	
	public void changeCurrentStatus(String value) {
		log.info("<---changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;
		
		approveComments=null;
		rejectComments=null;
	}
	
	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current D & P Procurement Plan Status ---> "+currentPlanStatus);
		log.info("<--- D & P Id ---> "+voucher.getId());
		log.info("<--- Comments1 ---> "+approveComments);
		log.info("<--- Comments2---> "+rejectComments);

		BaseDTO baseDTO = null;
		
		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			VoucherRequestDTO request = new VoucherRequestDTO(voucher.getId(),
					currentPlanStatus, approveComments);
			String URL = appPreference.getPortalServerURL() + "/accounts/voucher/approve";
			baseDTO = httpService.post(URL, request);
			log.info(" VoucherService Approved Successfully. ");
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			VoucherRequestDTO request = new VoucherRequestDTO(voucher.getId(),
					currentPlanStatus, approveComments);
			String URL = appPreference.getPortalServerURL() + "/accounts/voucher/reject";
			baseDTO = httpService.post(URL, request);
			log.info(" VoucherService Approved Successfully. ");
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		approveComments=null;
		rejectComments=null;
		
		return LIST_PAYMENT_VOUCHER;
	}
	
	
	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (voucherResponseDTO == null) {
			log.info("<---if(voucherResponseDTO == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			Util.addWarn("Please Select Any One Voucher");
			return;
		} else {
			log.info("<---Voucher Id--->"+voucherResponseDTO.getVoucherId());
			if (!voucherResponseDTO.getVoucherStatus().toString().equals("SUBMITTED")) {
				log.info("Voucher Cannot be Deleted.");
				
				String status = voucherResponseDTO.getVoucherStatus().toString().equalsIgnoreCase("APPROVED") ? "Approved" : "";
				
				Util.addWarn(status +" Voucher Cannot be Deleted");
				
				return;
			}
		RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called with Voucher Id---> " + voucherResponseDTO.getVoucherId());
		String url = appPreference.getPortalServerURL() + "/accounts/voucher/delete/"+ voucherResponseDTO.getVoucherId();
		
		log.info("<---Inside confirmDelete url ---> " + url);
		
		BaseDTO baseDTO = httpService.delete(url);

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("<---Voucher has been Deleted Successfully ---> ");
				errorMap.notify(ErrorDescription.VOUCHER_DELETED_SUCCESSFULLY.getCode());
				getVoucherList();
			} else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
		} else{
			log.info("<--- Internal Server Error ---> ");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_PAYMENT_VOUCHER;
	}
	
	
}
