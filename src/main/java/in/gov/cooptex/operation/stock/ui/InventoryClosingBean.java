package in.gov.cooptex.operation.stock.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.dto.InventoryClosingDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("inventoryClosingBean")
public class InventoryClosingBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = -8784384497044703791L;

	private static final String INVENTORY_CLOSING_PAGE = "/pages/stock/inventoryClosing.xhtml?faces-redirect=true;";

	private static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private List<InventoryClosingDTO> qrCodeWiseDetailsList = new ArrayList<>();

	@Getter
	@Setter
	InventoryClosingDTO inventoryClosingDTO = new InventoryClosingDTO();

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	private boolean searchBtnDisableFlag = true;

	@Getter
	@Setter
	private boolean fetchObDataBtnDisableFlag = true;

	private static final String stockDateStr = "2019-04-01";

	@Getter
	@Setter
	private boolean successPanelFlag = false;

	@Getter
	@Setter
	private boolean stockAlreadyVerifiedPanelFlag = false;

	@Getter
	@Setter
	private boolean stockVerifiedSuccessMsgFlag = false;

	@Getter
	@Setter
	LazyDataModel<InventoryClosingDTO> lazyStockDetailsList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	List<InventoryClosingDTO> inventoryClosingDataList;

	@Getter
	@Setter
	private boolean lazyListDisplayFlag = true;

	@Getter
	@Setter
	private String entityTypeCode = null;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private Date inventoryClosingDate;

	@Getter
	@Setter
	private boolean toDateFlag = true;

	@Getter
	@Setter
	private Date nextVerifiedDate;

	@Getter
	@Setter
	List<InventoryClosingDTO> inventoryClosingTotalDataList;

	@Getter
	@Setter
	private boolean totalListFlag = false;

	public String redirectSVInProgressPage() {
		if (loginBean.getStockVerificationIsInProgess()) {
			loginBean.setMessage("Inventory closing");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}

	public String loadInventoryClosingPage() {
		log.info("InventoryClosingBean. loadInventoryClosingPage() Starts ");
		inventoryClosingDTO.setLoginEntityId(null);
		entityTypeCode = null;
		inventoryClosingDate = null;
		nextVerifiedDate = null;
		try {

			redirectSVInProgressPage();

			inventoryClosingTotalDataList = new ArrayList<>();
			inventoryClosingDTO.setStockFromDate(null);
			inventoryClosingDTO.setStockToDate(null);
			inventoryClosingDTO.setOpeningBalanceDataList(new ArrayList<>());
			inventoryClosingDTO.setCategoryWiseOpeningBalanceDataList(new ArrayList<>());
			loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
			if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
					|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
					|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
					|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
							.getEntityCode() == null) {
				errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
				return null;
			} else {
				successPanelFlag = true;
				stockVerifiedSuccessMsgFlag = false;
				stockAlreadyVerifiedPanelFlag = false;
				lazyListDisplayFlag = false;
				totalListFlag = false;
				Long entityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
				inventoryClosingDTO.setLoginEntityId(entityId);
				log.info("InventoryClosingBean. loadInventoryClosingPage() - loginEntityId:  "+entityId);
				
				checkStockLastVerifiedDate();
				entityTypeCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode();
				log.info("InventoryClosingBean. loadInventoryClosingPage() - entityTypeCode:  "+entityTypeCode);
				
				toDateFlag = true;
			}
		} catch (Exception e) {
			log.error("Exception at InventoryClosingBean. loadInventoryClosingPage() ", e);
		}
		log.info("InventoryClosingBean. loadInventoryClosingPage() Ends ");
		return INVENTORY_CLOSING_PAGE;
	}

	@Getter
	@Setter
	LazyDataModel<InventoryClosingDTO> stockTransferLazyList;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	InventoryClosingDTO stockTransferLazy;

	// lazy search
	public void loadLazyStockDetailsList() {
		log.info("<--- loadLazyStockTransfer loadLazyStockDetailsList ---> ");
		stockTransferLazyList = new LazyDataModel<InventoryClosingDTO>() {
			// StockTransfer ssss =new StockTransfer();
			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<InventoryClosingDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<InventoryClosingDTO> data = new ArrayList<InventoryClosingDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

					data = mapper.readValue(jsonResponse, new TypeReference<List<InventoryClosingDTO>>() {
					});

					String jsonResponses = mapper.writeValueAsString(baseDTO.getResponseContent());

					inventoryClosingDTO = mapper.readValue(jsonResponses, InventoryClosingDTO.class);

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyStockTransfer RequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(InventoryClosingDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public InventoryClosingDTO getRowData(String rowKey) {
				List<InventoryClosingDTO> responseList = (List<InventoryClosingDTO>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (InventoryClosingDTO res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							stockTransferLazy = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		stockTransferLazy = new InventoryClosingDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString(), filters);
		inventoryClosingDTO.setPaginationDTO(paginationDTO);

		String URL = SERVER_URL + "/stockUpload/searchInventoryClosingData";
		baseDTO = httpService.post(URL, inventoryClosingDTO);

		return baseDTO;
	}

	private void checkStockLastVerifiedDate() {
		Date lastVerifiedDate = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date stockDate = dateFormat.parse(stockDateStr);
			
			String URL = SERVER_URL + "/stockUpload/getlastverifieddate";
			BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					if (baseDTO.getResponseContent() != null) {
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						lastVerifiedDate = mapper.readValue(jsonResponse, Date.class);
						Calendar start = Calendar.getInstance();
						start.setTime(lastVerifiedDate);
						start.add(Calendar.DAY_OF_MONTH, 1);
						nextVerifiedDate = start.getTime();
						inventoryClosingDTO.setStockFromDate(nextVerifiedDate);
					} else {
						inventoryClosingDTO.setStockFromDate(stockDate);
					}

					String obCutOffValue = baseDTO.getGeneralContent();
					log.info("InventoryClosingBean. checkStockLastVerifiedDate() - obCutOffValue: "+obCutOffValue);
					if (StringUtils.isNotEmpty(obCutOffValue)) {
						Date obCutOffDate = dateFormat.parse(obCutOffValue);
						if(obCutOffDate == null) {
							inventoryClosingDTO.setOpeningBalanceDate(stockDate);
						}
						inventoryClosingDTO.setOpeningBalanceDate(obCutOffDate);
					} else {
						inventoryClosingDTO.setOpeningBalanceDate(stockDate);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception at checkEnableDefaultStockDate() ", e);
		}
	}

	private void getFooterValues(List<InventoryClosingDTO> openingBalanceDataList) {
		// Total Opening Stock
		Double totOpeningStockQty = openingBalanceDataList.stream().filter(a -> a.getOpeningStock() != null)
				.collect(Collectors.summingDouble(x -> x.getOpeningStock()));
		inventoryClosingDTO.setTotalOpeningStock(AppUtil.formatIndianCommaSeparated(totOpeningStockQty));
		Double totOpeningStockValue = openingBalanceDataList.stream().filter(a -> a.getOpeningStockValue() != null)
				.collect(Collectors.summingDouble(x -> x.getOpeningStockValue()));
		inventoryClosingDTO.setTotalOpeningStockValue(AppUtil.formatIndianCommaSeparated(totOpeningStockValue));

		// Stock Inward
		Double totStockInwardQty = openingBalanceDataList.stream().filter(a -> a.getStockInwardQty() != null)
				.collect(Collectors.summingDouble(x -> x.getStockInwardQty()));
		inventoryClosingDTO.setTotalStockInwardQty(AppUtil.formatIndianCommaSeparated(totStockInwardQty));
		Double totStockInwardValue = openingBalanceDataList.stream().filter(a -> a.getStockInwardValue() != null)
				.collect(Collectors.summingDouble(x -> x.getStockInwardValue()));
		inventoryClosingDTO.setTotalStockInwardValue(AppUtil.formatIndianCommaSeparated(totStockInwardValue));

		// Stock Outward
		Double totStockOutwardQty = openingBalanceDataList.stream().filter(a -> a.getStockOutwardQty() != null)
				.collect(Collectors.summingDouble(x -> x.getStockOutwardQty()));
		inventoryClosingDTO.setTotalStockOutwardQty(AppUtil.formatIndianCommaSeparated(totStockOutwardQty));
		Double totStockOutwardValue = openingBalanceDataList.stream().filter(a -> a.getStockOutwardValue() != null)
				.collect(Collectors.summingDouble(x -> x.getStockOutwardValue()));
		inventoryClosingDTO.setTotalStockOutwardValue(AppUtil.formatIndianCommaSeparated(totStockOutwardValue));

		// Stock Sold
		Double totStockSoldQty = openingBalanceDataList.stream().filter(a -> a.getStockSoldQty() != null)
				.collect(Collectors.summingDouble(x -> x.getStockSoldQty()));
		inventoryClosingDTO.setTotalStockSoldQty(AppUtil.formatIndianCommaSeparated(totStockSoldQty));
		Double totStockSoldValue = openingBalanceDataList.stream().filter(a -> a.getStockSoldValue() != null)
				.collect(Collectors.summingDouble(x -> x.getStockSoldValue()));
		inventoryClosingDTO.setTotalStockSoldValue(AppUtil.formatIndianCommaSeparated(totStockSoldValue));

		// Stock Return
		Double totStockReturnQty = openingBalanceDataList.stream().filter(a -> a.getStockReturnQty() != null)
				.collect(Collectors.summingDouble(x -> x.getStockReturnQty()));
		inventoryClosingDTO.setTotalStockReturnQty(AppUtil.formatIndianCommaSeparated(totStockReturnQty));
		Double totStockReturnValue = openingBalanceDataList.stream().filter(a -> a.getStockReturnQty() != null)
				.collect(Collectors.summingDouble(x -> x.getStockReturnValue()));
		inventoryClosingDTO.setTotalStockReturnValue(AppUtil.formatIndianCommaSeparated(totStockReturnValue));

		// Closing Stock
		Double totClosingStockQty = openingBalanceDataList.stream().filter(a -> a.getClosingStockQty() != null)
				.collect(Collectors.summingDouble(x -> x.getClosingStockQty()));
		inventoryClosingDTO.setTotalClosingStockQty(AppUtil.formatIndianCommaSeparated(totClosingStockQty));
		Double totClosingStockValue = openingBalanceDataList.stream().filter(a -> a.getClosingStockValue() != null)
				.collect(Collectors.summingDouble(x -> x.getClosingStockValue()));
		inventoryClosingDTO.setTotalClosingStockValue(AppUtil.formatIndianCommaSeparated(totClosingStockValue));

		// Map<String, List<InventoryClosingDTO>> categoryWiseOpeningDataMap =
		// openingBalanceDataList
		// .stream().filter(x -> StringUtils.isNotEmpty(x.getCategoryCode()))
		// .collect(Collectors.groupingBy(x -> x.getCategoryCode()));
		// int categoryWiseOpeningDataMapSize = categoryWiseOpeningDataMap != null
		// ? categoryWiseOpeningDataMap.size()
		// : 0;
		// log.info("categoryWiseOpeningDataMap Size : " +
		// categoryWiseOpeningDataMapSize);
		// if (categoryWiseOpeningDataMapSize > 0) {
		// Map<String, List<InventoryClosingDTO>> sortedCategoryWiseMap = new
		// TreeMap<String, List<InventoryClosingDTO>>(
		// categoryWiseOpeningDataMap);
		// getCategoryWiseOpeningStockList(sortedCategoryWiseMap);
		// }
	}

	public String fetchOpeningBalanceData() {
		log.info("InventoryClosingBean. fetchOpeningBalanceData() Starts ");
		try {
			inventoryClosingDTO.setOpeningBalanceDataList(new ArrayList<>());
			inventoryClosingDTO.setCategoryWiseOpeningBalanceDataList(new ArrayList<>());
			String URL = SERVER_URL + "/stockUpload/fetchOpeningBalanceData";
			BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<InventoryClosingDTO> openingBalanceDataList = mapper.readValue(jsonResponse,
							new TypeReference<List<InventoryClosingDTO>>() {
							});
					int openingBalanceDataListSize = openingBalanceDataList != null ? openingBalanceDataList.size() : 0;
					log.info("openingBalanceDataListSize : " + openingBalanceDataListSize);
					if (openingBalanceDataListSize > 0) {
						getFooterValues(openingBalanceDataList);
					}
					inventoryClosingDTO.setOpeningBalanceDataList(openingBalanceDataList);
				}
			}
		} catch (Exception e) {
			log.error("Exception at InventoryClosingBean. fetchOpeningBalanceData() ", e);
		}
		log.info("InventoryClosingBean. fetchOpeningBalanceData() Ends ");
		return null;
	}

	private void getCategoryWiseOpeningStockList(Map<String, List<InventoryClosingDTO>> categoryWiseOpeningDataMap) {
		try {
			for (Map.Entry<String, List<InventoryClosingDTO>> entry : categoryWiseOpeningDataMap.entrySet()) {
				String categoryCode = entry.getKey();
				if (StringUtils.isNotEmpty(categoryCode)) {
					List<InventoryClosingDTO> categoryWiseStockList = entry.getValue();
					int categoryWiseStockListSize = categoryWiseStockList != null ? categoryWiseStockList.size() : 0;
					if (categoryWiseStockListSize > 0) {
						InventoryClosingDTO inventoryClosingDTOObj = new InventoryClosingDTO();
						inventoryClosingDTOObj.setCategoryCode(categoryCode);

						// Opening Stock
						Double openingStockQty = categoryWiseStockList.stream().filter(a -> a.getOpeningStock() != null)
								.collect(Collectors.summingDouble(x -> x.getOpeningStock()));
						inventoryClosingDTOObj.setOpeningStock(openingStockQty);
						Double openingStockValue = categoryWiseStockList.stream()
								.filter(a -> a.getOpeningStockValue() != null)
								.collect(Collectors.summingDouble(x -> x.getOpeningStockValue()));
						inventoryClosingDTOObj.setOpeningStockValue(openingStockValue);

						// Stock Inward
						Double stockInwardQty = categoryWiseStockList.stream()
								.filter(a -> a.getStockInwardQty() != null)
								.collect(Collectors.summingDouble(x -> x.getStockInwardQty()));
						inventoryClosingDTOObj.setStockInwardQty(stockInwardQty);
						Double stockInwardValue = categoryWiseStockList.stream()
								.filter(a -> a.getStockInwardValue() != null)
								.collect(Collectors.summingDouble(x -> x.getStockInwardValue()));
						inventoryClosingDTOObj.setStockInwardValue(stockInwardValue);

						// Stock Outward
						Double stockOutwardQty = categoryWiseStockList.stream()
								.filter(a -> a.getStockOutwardQty() != null)
								.collect(Collectors.summingDouble(x -> x.getStockOutwardQty()));
						inventoryClosingDTOObj.setStockOutwardQty(stockOutwardQty);
						Double stockOutwardValue = categoryWiseStockList.stream()
								.filter(a -> a.getStockOutwardValue() != null)
								.collect(Collectors.summingDouble(x -> x.getStockOutwardValue()));
						inventoryClosingDTOObj.setStockOutwardValue(stockOutwardValue);

						// Stock Sold
						Double stockSoldQty = categoryWiseStockList.stream().filter(a -> a.getStockSoldQty() != null)
								.collect(Collectors.summingDouble(x -> x.getStockSoldQty()));
						inventoryClosingDTOObj.setStockSoldQty(stockSoldQty);
						Double stockSoldValue = categoryWiseStockList.stream()
								.filter(a -> a.getStockSoldValue() != null)
								.collect(Collectors.summingDouble(x -> x.getStockSoldValue()));
						inventoryClosingDTOObj.setStockSoldValue(stockSoldValue);

						// Stock Return
						Double stockReturnQty = categoryWiseStockList.stream()
								.filter(a -> a.getStockReturnQty() != null)
								.collect(Collectors.summingDouble(x -> x.getStockReturnQty()));
						inventoryClosingDTOObj.setStockReturnQty(stockReturnQty);
						Double stockReturnValue = categoryWiseStockList.stream()
								.filter(a -> a.getStockReturnValue() != null)
								.collect(Collectors.summingDouble(x -> x.getStockReturnValue()));
						inventoryClosingDTOObj.setStockReturnValue(stockReturnValue);

						// Closing Stock
						Double closingStockQty = categoryWiseStockList.stream()
								.filter(a -> a.getClosingStockQty() != null)
								.collect(Collectors.summingDouble(x -> x.getClosingStockQty()));
						inventoryClosingDTOObj.setClosingStockQty(closingStockQty);
						Double closingStockValue = categoryWiseStockList.stream()
								.filter(a -> a.getClosingStockValue() != null)
								.collect(Collectors.summingDouble(x -> x.getClosingStockValue()));
						inventoryClosingDTOObj.setClosingStockValue(closingStockValue);
						inventoryClosingDTO.getCategoryWiseOpeningBalanceDataList().add(inventoryClosingDTOObj);
					}
				}
			}
			List<InventoryClosingDTO> categoryWiseOpeningDataList = inventoryClosingDTO
					.getCategoryWiseOpeningBalanceDataList();
			int categoryWiseOpeningDataListSize = categoryWiseOpeningDataList != null
					? categoryWiseOpeningDataList.size()
					: 0;
			if (categoryWiseOpeningDataListSize > 0) {
				// Opening Stock
				Double categoryTotOpeningStockQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getOpeningStock() != null)
						.collect(Collectors.summingDouble(x -> x.getOpeningStock()));
				inventoryClosingDTO
						.setCategoryTotalOpeningStock(AppUtil.formatIndianCommaSeparated(categoryTotOpeningStockQty));
				Double categoryTotOpeningStockValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getOpeningStockValue() != null)
						.collect(Collectors.summingDouble(x -> x.getOpeningStockValue()));
				inventoryClosingDTO.setCategoryTotalOpeningStockValue(
						AppUtil.formatIndianCommaSeparated(categoryTotOpeningStockValue));

				// Stock Inward
				Double categoryTotStockInwardQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockInwardQty() != null)
						.collect(Collectors.summingDouble(x -> x.getStockInwardQty()));
				inventoryClosingDTO
						.setCategoryTotalStockInwardQty(AppUtil.formatIndianCommaSeparated(categoryTotStockInwardQty));
				Double categoryTotStockInwardValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockInwardValue() != null)
						.collect(Collectors.summingDouble(x -> x.getStockInwardValue()));
				inventoryClosingDTO.setCategoryTotalStockInwardValue(
						AppUtil.formatIndianCommaSeparated(categoryTotStockInwardValue));

				// Stock Outward
				Double categoryTotStockOutwardQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockOutwardQty() != null)
						.collect(Collectors.summingDouble(x -> x.getStockOutwardQty()));
				inventoryClosingDTO.setCategoryTotalStockOutwardQty(
						AppUtil.formatIndianCommaSeparated(categoryTotStockOutwardQty));
				Double categoryTotStockOutwardValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockOutwardValue() != null)
						.collect(Collectors.summingDouble(x -> x.getStockOutwardValue()));
				inventoryClosingDTO.setCategoryTotalStockOutwardValue(
						AppUtil.formatIndianCommaSeparated(categoryTotStockOutwardValue));

				// Stock Sold
				Double categoryTotStockSoldQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockSoldQty() != null)
						.collect(Collectors.summingDouble(x -> x.getStockSoldQty()));
				inventoryClosingDTO
						.setCategoryTotalStockSoldQty(AppUtil.formatIndianCommaSeparated(categoryTotStockSoldQty));
				Double categoryTotStockSoldValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockSoldValue() != null)
						.collect(Collectors.summingDouble(x -> x.getStockSoldValue()));
				inventoryClosingDTO
						.setCategoryTotalStockSoldValue(AppUtil.formatIndianCommaSeparated(categoryTotStockSoldValue));

				// Stock Return
				Double categoryTotStockReturnQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockReturnQty() != null)
						.collect(Collectors.summingDouble(x -> x.getStockReturnQty()));
				inventoryClosingDTO
						.setCategoryTotalStockReturnQty(AppUtil.formatIndianCommaSeparated(categoryTotStockReturnQty));
				Double categoryTotStockReturnValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getStockReturnValue() != null)
						.collect(Collectors.summingDouble(x -> x.getStockReturnValue()));
				inventoryClosingDTO.setCategoryTotalStockReturnValue(
						AppUtil.formatIndianCommaSeparated(categoryTotStockReturnValue));

				// Closing Stock
				Double categoryTotClosingStockQty = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getClosingStockQty() != null)
						.collect(Collectors.summingDouble(x -> x.getClosingStockQty()));
				inventoryClosingDTO.setCategoryTotalClosingStockQty(
						AppUtil.formatIndianCommaSeparated(categoryTotClosingStockQty));
				Double categoryTotClosingStockValue = categoryWiseOpeningDataList.stream()
						.filter(a -> a.getClosingStockValue() != null)
						.collect(Collectors.summingDouble(x -> x.getClosingStockValue()));
				inventoryClosingDTO.setCategoryTotalClosingStockValue(
						AppUtil.formatIndianCommaSeparated(categoryTotClosingStockValue));
			}
		} catch (Exception e) {
			log.error("Exception at getCategoryWiseOpeningStockList() ", e);
		}
	}

	public String clear() {
		inventoryClosingDTO.setOpeningBalanceDataList(new ArrayList<>());
		inventoryClosingDTO.setCategoryWiseOpeningBalanceDataList(new ArrayList<>());
		inventoryClosingDTO.setTotalOpeningStock(null);
		inventoryClosingDTO.setTotalOpeningStockValue(null);
		inventoryClosingDTO.setTotalStockInwardQty(null);
		inventoryClosingDTO.setTotalStockInwardValue(null);
		inventoryClosingDTO.setTotalStockOutwardQty(null);
		inventoryClosingDTO.setTotalStockOutwardValue(null);
		inventoryClosingDTO.setTotalStockSoldQty(null);
		inventoryClosingDTO.setTotalStockSoldValue(null);
		inventoryClosingDTO.setTotalStockReturnQty(null);
		inventoryClosingDTO.setTotalStockReturnValue(null);
		inventoryClosingDTO.setTotalClosingStockQty(null);
		inventoryClosingDTO.setTotalClosingStockValue(null);

		inventoryClosingDTO.setCategoryTotalOpeningStock(null);
		inventoryClosingDTO.setCategoryTotalOpeningStockValue(null);
		inventoryClosingDTO.setCategoryTotalStockInwardQty(null);
		inventoryClosingDTO.setCategoryTotalStockInwardValue(null);
		inventoryClosingDTO.setCategoryTotalStockOutwardQty(null);
		inventoryClosingDTO.setCategoryTotalStockOutwardValue(null);
		inventoryClosingDTO.setCategoryTotalStockSoldQty(null);
		inventoryClosingDTO.setCategoryTotalStockSoldValue(null);
		inventoryClosingDTO.setCategoryTotalStockReturnQty(null);
		inventoryClosingDTO.setCategoryTotalStockReturnValue(null);
		inventoryClosingDTO.setCategoryTotalClosingStockQty(null);
		inventoryClosingDTO.setCategoryTotalClosingStockValue(null);
		inventoryClosingDTO.setStockFromDate(null);
		inventoryClosingDTO.setStockToDate(null);
		inventoryClosingDate = null;
		toDateFlag = true;
		totalListFlag = false;
		inventoryClosingTotalDataList = null;
		checkStockLastVerifiedDate();

		stockAlreadyVerifiedPanelFlag = false;
		stockVerifiedSuccessMsgFlag = false;
		lazyListDisplayFlag = false;
		return INVENTORY_CLOSING_PAGE;
	}

	public String closeInventory() {
		log.info("InventoryClosingBean. closeInventory() Starts");
		try {

			if (inventoryClosingDTO == null || inventoryClosingDTO.getStockDate() == null) {
				log.info("Date is empty.");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.DATE_EMPTY).getErrorCode());
				return null;
			}

			/**
			 * COOPTEX-CORE-SERVICES/src/main/java/in/gov/cooptex/common/controller
			 * 
			 * StockUploadController.java
			 */
			String URL = SERVER_URL + "/stockUpload/verifyStock";
			BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					successPanelFlag = true;
					stockVerifiedSuccessMsgFlag = true;
					stockAlreadyVerifiedPanelFlag = false;
					lazyListDisplayFlag = true;
					loadLazyStockDetailsList();
					checkStockLastVerifiedDate();
				} else if (baseDTO.getStatusCode().intValue() == ErrorDescription.NO_RESULT_FOR_QUERY.getErrorCode()
						.intValue()) {
					successPanelFlag = false;
					stockVerifiedSuccessMsgFlag = false;
					stockAlreadyVerifiedPanelFlag = true;
					lazyListDisplayFlag = false;
					Date stockDate = inventoryClosingDTO.getStockDate();
					String previousStockDateStr = null;
					clear();
					if (stockDate != null) {
						final Calendar cal = Calendar.getInstance();
						cal.setTime(stockDate);
						cal.add(Calendar.DATE, -1);
						previousStockDateStr = AppUtil.DATE_FORMAT.format(cal.getTime());
					}
					Util.addWarn("No records found for previous day. Please verify stock for previous day : "
							+ previousStockDateStr);
					return null;
				} else if (baseDTO.getStatusCode().intValue() == ErrorDescription
						.getError(OperationErrorCode.INVENTORY_CLOSING_NOT_ALLOWED_ERROR_MSG).getErrorCode()) {
					successPanelFlag = true;
					lazyListDisplayFlag = false;
					errorMap.notify(ErrorDescription
							.getError(OperationErrorCode.INVENTORY_CLOSING_NOT_ALLOWED_ERROR_MSG).getErrorCode());
					return null;
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			successPanelFlag = false;
			stockVerifiedSuccessMsgFlag = false;
			lazyListDisplayFlag = false;
			stockAlreadyVerifiedPanelFlag = true;
			log.error("Exception at InventoryClosingBean. closeInventory() ", e);
		}
		log.info("InventoryClosingBean. closeInventory() Ends");
		return null;
	}

	public void stockDateListener() {
		log.info("InventoryClosingBean. stockDateListener() Starts");
		boolean existFlag = false;
		stockAlreadyVerifiedPanelFlag = false;
		successPanelFlag = false;
		stockVerifiedSuccessMsgFlag = false;
		try {
			if (inventoryClosingDTO == null || inventoryClosingDTO.getStockDate() == null) {
				log.info("Date is empty.");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.DATE_EMPTY).getErrorCode());
				return;
			}
			String URL = SERVER_URL + "/stockUpload/checkobdata";
			inventoryClosingDTO.setClosingStockDate(inventoryClosingDTO.getStockDate());
			BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					if (baseDTO.getResponseContent() != null) {
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						existFlag = mapper.readValue(jsonResponse, Boolean.class);
					}
				}
			} else {
				existFlag = false;
			}
			if (existFlag) {
				stockAlreadyVerifiedPanelFlag = true;
				lazyListDisplayFlag = true;
				loadLazyStockDetailsList();
				successPanelFlag = false;
				stockVerifiedSuccessMsgFlag = false;
			} else {
				stockVerifiedSuccessMsgFlag = false;
				successPanelFlag = true;
				stockAlreadyVerifiedPanelFlag = false;
				lazyStockDetailsList = null;
				lazyListDisplayFlag = false;
			}
		} catch (Exception e) {
			log.error("Exception at InventoryClosingBean. stockDateListener() ", e);
		}
		log.info("InventoryClosingBean. stockDateListener() Ends");
	}

	public void getInventoryItemsData() {
		log.info("InventoryClosingBean. getInventoryItemsData() Starts");
		try {
			inventoryClosingDTO.setOpeningBalanceDataList(new ArrayList<>());
			String URL = SERVER_URL + "/stockUpload/searchInventoryClosingData";
			BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<InventoryClosingDTO> openingBalanceDataList = mapper.readValue(jsonResponse,
							new TypeReference<List<InventoryClosingDTO>>() {
							});
					inventoryClosingDTO.setOpeningBalanceDataList(openingBalanceDataList);

					int openingBalanceDataListSize = openingBalanceDataList != null ? openingBalanceDataList.size() : 0;
					log.info("openingBalanceDataListSize : " + openingBalanceDataListSize);
					if (openingBalanceDataListSize > 0) {
						getFooterValues(openingBalanceDataList);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception at getInventoryItemsData() ", e);
		}
		log.info("InventoryClosingBean. getInventoryItemsData() Ends");
	}

	public void changeFromDate() {
		log.info("changeFromDate method starts----");
		String inventoryClosingDays = null;
		Date date = new Date();
		Calendar start = Calendar.getInstance();
		try {
			if (inventoryClosingDTO.getStockFromDate() != null) {
				inventoryClosingDTO.setStockToDate(null);
				Long entityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
				inventoryClosingDays = commonDataService
						.getInventoryClosingDaysAppKeyValue(AppConfigEnum.INVENTORY_CLOSING_DAYS.toString(), entityId);
				if (StringUtils.isEmpty(inventoryClosingDays)) {
					inventoryClosingDays = "1";
				}
				start.setTime(inventoryClosingDTO.getStockFromDate());
				start.add(Calendar.DAY_OF_MONTH, Integer.valueOf(inventoryClosingDays));

				if (inventoryClosingDTO.getStockFromDate().compareTo(date) == 0) {
					inventoryClosingDate = date;
				} else {
					inventoryClosingDate = start.getTime();
				}

				if (inventoryClosingDate.after(date)) {
					setInventoryClosingDate(date);
				}
				toDateFlag = false;
				totalListFlag = false;
			}
		} catch (Exception e) {
			log.error("changeFromDate exception---", e);
		}
		log.info("changeFromDate method ends----");
	}

	public void inventoryClosing() {
		log.info("inventoryClosing method starts---------");
		try {
			if (inventoryClosingDTO.getStockFromDate() != null && inventoryClosingDTO.getStockToDate() != null) {
				inventoryClosingTotalDataList = new ArrayList<>();
				String URL = SERVER_URL + "/stockUpload/inventoryclosing";
				BaseDTO baseDTO = httpService.post(URL, inventoryClosingDTO);
				if (baseDTO != null) {
					log.info("status code------>" + baseDTO.getStatusCode());
					if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						inventoryClosingTotalDataList = mapper.readValue(jsonResponse,
								new TypeReference<List<InventoryClosingDTO>>() {
								});
						log.info("total list size-------------->" + inventoryClosingTotalDataList.size());
						successPanelFlag = true;
						stockVerifiedSuccessMsgFlag = true;
						stockAlreadyVerifiedPanelFlag = false;
						totalListFlag = true;
						// checkStockLastVerifiedDate();
					} else if (baseDTO.getStatusCode().equals(ErrorDescription
							.getError(OperationErrorCode.INVENTORY_CLOSING_NOT_ALLOWED_ERROR_MSG).getErrorCode())) {
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.INVENTORY_CLOSING_NOT_ALLOWED_ERROR_MSG).getErrorCode());
					} else {
						String errorMsg = baseDTO.getMessage();
						if (StringUtils.isNotEmpty(errorMsg)) {
							AppUtil.addWarn(errorMsg);
							stockVerifiedSuccessMsgFlag = false;
							return;
						} else {
							errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("inventoryClosing method exception----", e);
		}
		log.info("inventoryClosing method ends---------");
	}
}
