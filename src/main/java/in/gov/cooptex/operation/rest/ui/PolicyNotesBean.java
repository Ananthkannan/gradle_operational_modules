package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.policynote.model.PolicyNote;
import in.gov.cooptex.admin.policynote.model.PolicyNoteIntimation;
import in.gov.cooptex.admin.policynote.model.PolicyNoteLog;
import in.gov.cooptex.admin.policynote.model.PolicyNoteNote;
import in.gov.cooptex.admin.tapal.model.IncomingTapal;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.InsuranceMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PolicyNoteDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("policyNotesBean")
@Scope("session")
public class PolicyNotesBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7282406643494576666L;
	private final String LIST_POLICY_NOTES = "/pages/admin/PolicyNotes/listPolicyNotes.xhtml?faces-redirect=true";
	private final String CREATE_POLICY_NOTES = "/pages/admin/PolicyNotes/createPolicyNotes.xhtml?faces-redirect=true";
	private final String VIEW_POLICY_NOTES = "/pages/admin/PolicyNotes/viewPolicyNotes.xhtml?faces-redirect=true";
	private final String PROCESS_POLICY_NOTES = "/pages/admin/PolicyNotes/policyNotesProcess.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	String url, jsonResponse;

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<>();
	
	@Getter
	@Setter
	List<Department> selectDepartmentList = new ArrayList<>();

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	Date dateSelect;

	@Getter
	@Setter
	List<String> conYearList = new ArrayList<>();

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	PolicyNoteIntimation policyNoteIntimation;

	@Getter
	@Setter
	private String period;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	long letterSize;

	@Setter
	@Getter
	String fileName, processfileName,fileName1;

	@Getter
	@Setter
	LazyDataModel<PolicyNoteIntimation> lazyPolicyNoteIntimationList;

	@Setter
	@Getter
	private StreamedContent file;

	@Setter
	@Getter
	private StreamedContent processFile;

	@Setter
	@Getter
	private Boolean referenceFlag = false;

	@Setter
	@Getter
	PolicyNoteDTO policyNoteDTO;

	@Getter
	@Setter
	private String stage;

	@Getter
	@Setter
	private Boolean processFlag = true;

	@Getter
	@Setter
	PolicyNote policyNote;

	@Getter
	@Setter
	PolicyNoteLog policyNoteLog;

	@Getter
	@Setter
	PolicyNoteNote policyNoteNote;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	private Boolean approvalFlag = false;

	@Getter
	@Setter
	private Boolean previousApproval = false;
	@Getter
	@Setter
	List<SectionMaster> sectionMasterList;

	@Getter
	@Setter
	private List<PolicyNoteLog> policyNoteLogList;
	@Getter
	@Setter
	List<IncomingTapal> incomingTapalList;

	@Getter
	@Setter
	List<PolicyNoteDTO> viewLogResponseList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@Getter
	@Setter
	private String note;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	public String loadPolicyNotes() {
		try {
			mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			conYearList = commonDataService.loadFinancialYear();
			selectDepartmentList = new ArrayList<>();
			departmentList = commonDataService.loadDepartmentList();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.POLICY_NOTES.toString());
			policyNoteIntimation = new PolicyNoteIntimation();
			policyNoteDTO = new PolicyNoteDTO();
			policyNote = new PolicyNote();
			policyNoteLog = new PolicyNoteLog();
			policyNoteNote = new PolicyNoteNote();
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = false;
			processFlag = true;
			viewButtonFlag = true;
			note = "";
			
			loadPolicyNoteList();
		} catch (Exception e) {
			log.error("Exception in PolicyNotesBean  :: loadPolicyNotes ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_POLICY_NOTES;
	}

	private void loadPolicyNoteList() {
		lazyPolicyNoteIntimationList = new LazyDataModel<PolicyNoteIntimation>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 9092821910693795627L;

			public List<PolicyNoteIntimation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<PolicyNoteIntimation> policyNoteIntimationList = null;
				try {
					policyNoteIntimationList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/loadLazyPolicyNote";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						policyNoteIntimationList = mapper.readValue(jsonResponse,
								new TypeReference<List<PolicyNoteIntimation>>() {
								});
						log.info("list----------" + policyNoteIntimationList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = policyNoteIntimationList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyPolicyNote List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return policyNoteIntimationList;
			}

			@Override
			public Object getRowKey(PolicyNoteIntimation res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PolicyNoteIntimation getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<PolicyNoteIntimation> responseList = (List<PolicyNoteIntimation>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (PolicyNoteIntimation policyNoteIntimationObj : responseList) {
					if (policyNoteIntimationObj.getId().longValue() == value.longValue()) {
						policyNoteIntimation = policyNoteIntimationObj;
						return policyNoteIntimationObj;
					}
				}
				return null;
			}
		};

	}

	public String createPolicyNotes() {
		referenceFlag = false;
		fileName = "";
		processfileName = "";
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		department = new Department();
		// incomingTapalList=commonDataService.getIncomeTapalReferenceNo();

		String appValue = commonDataService.getAppKeyValueWithserverurl("ENABLE_PAST_DATE", SERVER_URL);
		if (appValue.equalsIgnoreCase("true")) {
			dateSelect = null;
		} else {
			dateSelect = new Date();
		}
		getIncomeTapalReferenceNo();
		policyNoteIntimation = new PolicyNoteIntimation();

		return CREATE_POLICY_NOTES;
	}

	public void getIncomeTapalReferenceNo() {
		incomingTapalList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/policynoteintimation/getincomingtapalrefno";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				incomingTapalList = mapper.readValue(jsonResponse, new TypeReference<List<IncomingTapal>>() {
				});
			}

		} catch (Exception e) {
			log.info("Error in getIncomeTapalReferenceNo()>>>>>>>>" + e);
		}
	}

//			public void loadSectionByDepartmentId() {
//				log.info("loadSectionByDepartmentId starts");
//				log.info("department=====id====>" + department.getId());
//				Long dId = departmentList.get(0).getId();
//				//sectionMasterList = commonDataService.getSectionByDepartmentId(department.getId());
//				sectionMasterList = commonDataService.getSectionByDepartmentId(dId);
//				log.info("loadSectionByDepartmentId ends");
//			}
			
			public void loadSectionByDepartmentId() {
				log.info("loadSectionByDepartmentId starts");
				log.info("department=====id====>" + department.getId());
				sectionMasterList = commonDataService.getSectionByDepartmentId(department.getId());
				log.info("loadSectionByDepartmentId ends");
			}
			
			public void onSelectdepartment() {
				log.info("<<<< ----------Starts loadMeretinSectionList ------- >>>>");
				sectionMasterList = new ArrayList<>();
				//meetingsectionlist = commonDataService.getActiveSectionMastersByDepartment(department);
				sectionMasterList=getActiveSectionMastersByDepartment();
				log.info("Section list........." + sectionMasterList);
				log.info("<<<< ----------End sectionMasterList ------- >>>>");
			}
			public List<SectionMaster> getActiveSectionMastersByDepartment() {
				log.info("getActiveSectionMastersByDepartment Start===>");
				List<SectionMaster> sectionMasterList=new ArrayList<SectionMaster>();
				try {
					BaseDTO relationShipBaseDTO = new BaseDTO();
					String url = AppUtil.getPortalServerURL() + "/sectionmaster/getAllActiveByDepartmentList/";
					log.info("getActiveSectionMasters url===> " + url);
					relationShipBaseDTO = httpService.post(url,selectDepartmentList);
					if (relationShipBaseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContents());
						sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
						});
					}
					log.info("sectionMasterList.size ===> " + sectionMasterList != null ? sectionMasterList.size() : 0);
					log.info("getActiveSectionMastersByDepartment end===>");

				} catch (Exception e) {
					log.error("getActiveSectionMastersByDepartment error==>", e);
				}
				return sectionMasterList;
			}

	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		policyNoteIntimation = null;
		processFlag = true;
		viewButtonFlag = true;
		loadPolicyNoteList();
		return LIST_POLICY_NOTES;
	}

	public String savePolicyNotes() {
		log.info("savePolicyNotes is called...");
		BaseDTO baseDTO = null;
		try {
			if (saveValidation(true)) {
				baseDTO = new BaseDTO();
				policyNoteIntimation.setDepartment(department);
				url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/create";
				baseDTO = httpService.post(url, policyNoteIntimation);
				log.info("Status Code - " + ErrorDescription.POLICY_NOT_ALREADY_EXISTS.getErrorCode());
				if (baseDTO.getStatusCode() == 0) {
					if ("ADD".equalsIgnoreCase(pageAction)) {
						errorMap.notify(ErrorDescription.POLICY_NOTE_SAVE.getErrorCode());
					} else if ("EDIT".equalsIgnoreCase(pageAction)) {
						errorMap.notify(ErrorDescription.POLICY_NOTE_UPDATE.getErrorCode());
					}
					return loadPolicyNotes();
				} /*
					 * else if (baseDTO.getStatusCode() ==
					 * ErrorDescription.POLICY_NOT_ALREADY_EXISTS.getErrorCode()) {
					 * errorMap.notify(ErrorDescription.POLICY_NOT_ALREADY_EXISTS.getErrorCode());
					 * return null; }
					 */ else {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("inside savePolicyNotes method exception--", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private boolean saveValidation(boolean valid) {
		valid = true;
		if (StringUtils.isEmpty(policyNoteIntimation.getGovReferenceNo())) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_REFEENCENO.getErrorCode());
			valid = false;
		}
		
		if (StringUtils.isEmpty(policyNoteIntimation.getPeriod())) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PEROID.getErrorCode());
			valid = false;
		} else if (policyNoteIntimation.getDueDate() == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_DUEDATE.getErrorCode());
			valid = false;
		} else if (department == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_DEPARTMENT.getErrorCode());
			valid = false;
		} else if (StringUtils.isEmpty(policyNoteIntimation.getPolicyDescription())) {
			valid = false;
			errorMap.notify(ErrorDescription.POLICY_NOTE_DESCRIPTION.getErrorCode());
		} else if (fileName.isEmpty()) {
			valid = false;
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_UPLOADFILE_EMPTY.getErrorCode());
		}
		return valid;
	}

	public void onRowSelect(SelectEvent event) {
		viewButtonFlag = false;
		deleteButtonFlag = true;
		addButtonFlag = true;
		policyNoteIntimation = (PolicyNoteIntimation) event.getObject();
		log.info("stage----------" + policyNoteIntimation.getStage());
		setStage(policyNoteIntimation.getStage());
		if (stage.equalsIgnoreCase("INITIATED")) {
			processFlag = false;
			editButtonFlag = false;
			deleteButtonFlag = false;
		} else if (stage.equalsIgnoreCase("SUBMITTED") || stage.equalsIgnoreCase("APPROVED")) {
			processFlag = true;
			editButtonFlag = true;
		} else if (stage.equalsIgnoreCase("FINAL APPROVED")) {
			processFlag = true;
			editButtonFlag = true;
		} else if (stage.equalsIgnoreCase("REJECTED")) {
			processFlag = true;
			editButtonFlag = false;
		}
	}

	/**
	 * @param event
	 */
	public void fileUpload(FileUploadEvent event) {
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();
		log.info("Uploaded File Size -" + size);
		String type = FilenameUtils.getExtension(uploadedFile.getFileName());
		log.info(" File Type :: " + type);
		boolean validFileType = AppUtil.isValidFileType(type,
				new String[] { "png", "jpg", "JPG", "doc", "jpeg", "docx", "pdf", "gif", "xlsx", "xls" });
		if (!validFileType) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
			return;
		}
		size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
		long letterSize = Long.valueOf(commonDataService.getAppKeyValue("POLICY_NOTE_UPLOAD_FILE_SIZE"));
		letterSize = letterSize / 1000;
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String docPathName = commonDataService.getAppKeyValue("POLICY_NOTE_UPLOAD_PATH");
		String filePath = Util.fileUpload(docPathName, uploadedFile);
		// docPathName +=event.getFile().getFileName();
		log.info("filePath ------------" + filePath);
		setFileName(uploadedFile.getFileName());
		policyNoteIntimation.setFilePath(filePath);
		errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE.getErrorCode());
		log.info(" policynote  is uploaded successfully");
	}

	public void clearRemarks() {
		log.info("clearRemarks calling ....");
		policyNoteLog.setRemarks("");
	}

	public String viewPolicyNotes() {
		try {
			log.info("policyidddd" + policyNoteIntimation.getId());
			approvalFlag = false;
			//fileName = "";
			policyNoteDTO = new PolicyNoteDTO();
			BaseDTO baseDTO = new BaseDTO();
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/get/"
					+ policyNoteIntimation.getId() + "/" + (systemNotificationId != null ? systemNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				policyNoteDTO = mapper.readValue(jsonResponse, new TypeReference<PolicyNoteDTO>() {
				});
			}
			setPolicyNoteIntimation(policyNoteDTO.getPolicyNoteIntimation());
			if (policyNoteIntimation != null) {
				department = new Department();
				sectionMaster = new SectionMaster();
				setDepartment(policyNoteIntimation.getDepartment());
				setSectionMaster(policyNoteIntimation.getSectionMaster());
			}
			if (!"INITIATED".equalsIgnoreCase(stage)) {
				setPolicyNote(policyNoteDTO.getPolicyNote());
				setPolicyNoteLog(policyNoteDTO.getPolicyNoteLog());
				setPolicyNoteNote(policyNoteDTO.getPolicyNoteNote());
				setUserMaster(policyNoteDTO.getUserMaster());
				setPreviousApproval(policyNoteNote.getFinalApproval());
				note = policyNoteNote.getNote();
			}
			if ("VIEW".equalsIgnoreCase(pageAction)) {
				if (policyNoteIntimation.getFilePath() != null) {
					File file = new File(policyNoteIntimation.getFilePath());
					fileName1 = file.getName();
				}
				if (policyNote != null && policyNote.getFileName() != null && policyNote.getFilePath() != null) {
					log.info("policy note process file name------------>" + policyNote.getFileName());
					log.info("policy note process file path------------>" + policyNote.getFilePath());
					File file = new File(policyNote.getFilePath());
					fileName = file.getName();
				}
				if ("INITIATED".equalsIgnoreCase(stage)) {
					approvalFlag = false;
				} else {
					if (policyNoteDTO.getUserMaster() != null) {
						if (policyNoteDTO.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId()
								.longValue()) {
							approvalFlag = true;
						}
					}
				}
				/*
				 * if (policyNote != null) {
				 * setFinalApproveFlag(policyNoteNote.getFinalApproval()); }
				 */
				String employeeDatajsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<PolicyNoteDTO>>() {
						});
				if (viewLogResponseList != null) {
					log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());
				}

				policyNoteLog.setRemarks("");
				log.info("remarks -----> " + policyNoteLog.getRemarks());
				if (systemNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}
				return VIEW_POLICY_NOTES;
			} else if ("EDIT".equalsIgnoreCase(pageAction)) {
				// incomingTapalList=commonDataService.getIncomeTapalReferenceNo();
				getIncomeTapalReferenceNo();

				String appValue = commonDataService.getAppKeyValueWithserverurl("ENABLE_PAST_DATE", SERVER_URL);
				if (appValue.equalsIgnoreCase("true")) {
					dateSelect = null;
				} else {
					dateSelect = new Date();
				}
				referenceFlag = true;
				if (policyNoteIntimation.getFilePath() != null) {
					File file = new File(policyNoteIntimation.getFilePath());
					fileName = file.getName();
				}
				if ("INITIATED".equalsIgnoreCase(stage)) {
					return CREATE_POLICY_NOTES;
				} else {
					employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
					return PROCESS_POLICY_NOTES;
				}
			} else if ("PROCESS".equalsIgnoreCase(pageAction)) {
				log.info("inside  action------------" + pageAction);
				if (policyNoteIntimation.getFilePath() != null) {
					log.info("filepath---------------" + policyNoteIntimation.getFilePath());
					File file = new File(policyNoteIntimation.getFilePath());
					fileName = file.getName();
				}
				userMaster = null;
				employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
				return PROCESS_POLICY_NOTES;
			}
		} catch (Exception e) {
			log.error("exception in view method-----", e);
		}
		return null;
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			log.info("policy note intimation file download--------" + policyNoteIntimation.getFilePath() + "-");
			if (policyNoteIntimation.getFilePath() != null) {
				log.info("policy note intimation file path--------" + policyNoteIntimation.getFilePath());
				File file = new File(policyNoteIntimation.getFilePath());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				this.setFile(
						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
			}
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException in filedownload method------- " + e);
		} catch (Exception e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void deletePolicyDelete() {
		String url;
		try {
			log.info("id-----------" + policyNoteIntimation.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/policynoteintimation/deleteById/" + policyNoteIntimation.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_DELETE.getErrorCode());
				loadPolicyNoteList();
				editButtonFlag = true;
				deleteButtonFlag = true;
				addButtonFlag = false;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("inside deleteIncomeingTapal method--", e);
		}
	}

	public void processFileUpload(FileUploadEvent event) {
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = size / 1000;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("TAPAL_UPLOAD_FILE_SIZE"));
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String docPathName = commonDataService.getAppKeyValue("POLICY_NOTE_PROCESS_UPLOAD_PATH");

		Util.fileUpload(docPathName, uploadedFile);
		setProcessfileName(uploadedFile.getFileName());
		policyNote.setFileName(processfileName);
		policyNote.setFilePath(docPathName);
		errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_UPLOAD.getErrorCode());
		log.info(" policynote  is uploaded successfully");
	}

	public String savePolicyNoteProcess() {
		BaseDTO baseDTO = null;
		String url = "";
		try {
			if (saveProcessValidation(true)) {
				policyNoteDTO.setUserMaster(userMaster);
				policyNoteDTO.setPolicyNoteIntimation(policyNoteIntimation);
				policyNoteDTO.setPolicyNote(policyNote);
				policyNoteDTO.setPolicyNoteNote(policyNoteNote);
				policyNoteDTO.setPolicyNoteLog(policyNoteLog);
				baseDTO = new BaseDTO();
				url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/savePolicyNoteProcess";
				baseDTO = httpService.post(url, policyNoteDTO);
				if (baseDTO.getStatusCode() == 0) {

					if (pageAction.equals("PROCESS")) {
						errorMap.notify(
								ErrorDescription.getError(MastersErrorCode.POLICY_NOTE_PROCESS_SUBMIT).getCode());
					} else if (pageAction.equals("ADD")) {
						errorMap.notify(
								ErrorDescription.getError(MastersErrorCode.POLICY_NOTE_PROCESS_INITIATED).getCode());
					}
					policyNoteIntimation = null;
					return loadPolicyNotes();
				} else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("inside savePolicyNoteProcess exception-----", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return null;

	}

	private boolean saveProcessValidation(boolean valid) {
		valid = true;
		if (StringUtils.isEmpty(policyNote.getPolicyDescription())) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_COMMENTS.getErrorCode());
			valid = false;
		}
		if (policyNoteNote.getFinalApproval() == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDFOR.getErrorCode());
			valid = false;
		}
		if (userMaster == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDTO.getErrorCode());
			valid = false;
		}
		if (StringUtils.isEmpty(policyNoteNote.getNote())) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_CREATENOTE.getErrorCode());
			valid = false;
		}
		if (StringUtils.isEmpty(policyNote.getFilePath())) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_UPLOADFILE_EMPTY.getErrorCode());
			valid = false;
		}
		return valid;
	}

	public String processApprove() {
		BaseDTO baseDTO = null;
		String url = "";
		try {
			if (previousApproval == false && policyNoteNote.getFinalApproval() == true) {
				policyNoteLog.setStage("APPROVED");
			} else if (previousApproval == true && policyNoteNote.getFinalApproval() == true) {
				policyNoteLog.setStage("FINAL APPROVED");
			} else {
				policyNoteLog.setStage("APPROVED");
			}
			policyNoteDTO.setUserMaster(userMaster);
			policyNoteDTO.setPolicyNoteIntimation(policyNoteIntimation);
			policyNoteDTO.setPolicyNote(policyNote);
			policyNoteNote.setNote(note);
			policyNoteDTO.setPolicyNoteNote(policyNoteNote);
			policyNoteDTO.setPolicyNoteLog(policyNoteLog);
			log.info("policy note id---------" + policyNoteDTO.getPolicyNote().getId());
			baseDTO = new BaseDTO();
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/approveProcess";
			baseDTO = httpService.post(url, policyNoteDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_APPROVE.getErrorCode());
				policyNoteIntimation = null;
				return loadPolicyNotes();
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			log.error("inside processApprove exception-----", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return "";
	}

	public String processReject() {
		BaseDTO baseDTO = null;
		String url = "";
		try {
			policyNoteDTO.setPolicyNote(policyNote);
			policyNoteDTO.setPolicyNoteLog(policyNoteLog);
			baseDTO = new BaseDTO();
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/rejectProcess";
			baseDTO = httpService.post(url, policyNoteDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_REJECTED.getErrorCode());
				policyNoteIntimation = null;
				return loadPolicyNotes();
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			log.error("inside rejectApprove exception-----", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return "";
	}

	public void getCreatedByNotes() {
		BaseDTO baseDTO = null;
		String url = "";
		try {
			baseDTO = new BaseDTO();
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/policynoteintimation/getCreatedByNotes/"
					+ policyNote.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				policyNoteLogList = mapper.readValue(jsonResponse, new TypeReference<List<PolicyNoteLog>>() {
				});
				log.info("list size------" + policyNoteLogList.size());
			}
		} catch (Exception e) {
			log.error("inside getCreatedByNotes exception-----", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

//	public void processFileDownload() {
//		InputStream input = null;
//		try {
//			log.info("<-----processFileDownload starts-------->" + policyNote.getFilePath() +"-"+ policyNote.getFileName() );
//			if(policyNote.getFilePath()!=null) {
//				log.info("policy note intimation file path--------"+ policyNote.getFileName());
//				File file = new File(policyNote.getFilePath()+"/"+"uploaded/");
//				input = new FileInputStream(file);
//				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//				this.setFile(
//						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
//			}
//		} catch (FileNotFoundException e) {
//			log.error("FileNotFoundException in filedownload method------- " + e);
//		} catch (Exception e) {
//			log.error("exception in filedownload method------- " + e);
//		}
//	}

	public void processFileDownload() {
		log.info("<-----processFileDownload starts-------->" + policyNote.getFilePath() + "-"
				+ policyNote.getFileName());
		InputStream input = null;
		try {
			if (policyNote.getFileName() != null && policyNote.getFilePath() != null) {
				StringBuilder builder = new StringBuilder();
				builder.append(policyNote.getFilePath()).append("/uploaded/").append(policyNote.getFileName());
				log.info("processFileDownload filePath------------->" + builder.toString());
				File file = new File(builder.toString());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				this.setProcessFile(
						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
			}
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException in processFileDownload method------- " + e);
		} catch (Exception e) {
			log.error("exception in processFileDownload method------- " + e);
		}
		log.info("<-----processFileDownload ends-------->");
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts PolicyNoteBean-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String policyId = "";
		String stage = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			policyId = httpRequest.getParameter("policynoteId");
			stage = httpRequest.getParameter("stage");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (policyId != null) {
			log.info("policyId--------------->" + policyId);
			Long fileId = Long.parseLong(policyId);
			policyNoteIntimation = new PolicyNoteIntimation();
			policyNoteIntimation.setId(fileId);
			policyNoteIntimation.setStage(stage);
			setStage(stage);
			systemNotificationId = Long.parseLong(notificationId);
			pageAction = "VIEW";
			viewPolicyNotes();
		}
		return null;
	}
}
