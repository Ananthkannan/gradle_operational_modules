package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.asset.model.AssetDisposalRequest;
import in.gov.cooptex.asset.model.AssetDisposalRequestDetails;
import in.gov.cooptex.asset.model.AssetDisposalRequestLog;
import in.gov.cooptex.asset.model.AssetDisposalRequestNote;
import in.gov.cooptex.asset.model.AssetRegister;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AssetDisposalForwardDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UpdateAssetDisposalDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.DisposalAssetDetailsDTO;
import in.gov.cooptex.dto.pos.DisposalAssetListDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.finance.dto.ExhibitionClaimRequestDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("assetDisposalBean")
public class AssetDisposalBean extends CommonBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/assetManagement/createAssetDisposal.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/assetManagement/viewAssetDisposal.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/assetManagement/listAssetDisposal.xhtml?faces-redirect=true";

	final String REJECT = "REJECT";

	final String APPROVED = "APPROVED";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Setter
	@Getter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	EntityMaster headRegionOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityTypeMaster entityType = new EntityTypeMaster();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterList = new ArrayList<>();

	@Getter
	@Setter
	AssetCategoryMaster assetCategoryMaster = new AssetCategoryMaster();

	@Getter
	@Setter
	List<AssetCategoryMaster> subAssetCategoryList = new ArrayList<>();

	@Getter
	@Setter
	AssetCategoryMaster subAssetCategoryMaster = new AssetCategoryMaster();

	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	List<DisposalAssetDetailsDTO> disposalAssetDetailsDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<DisposalAssetDetailsDTO> selectedDisposalAssetList = new ArrayList<>();

	@Getter
	@Setter
	LazyDataModel<DisposalAssetListDTO> disposalAssetListDTOLazyList;

	@Getter
	@Setter
	List<DisposalAssetListDTO> disposalAssetListDTOList = new ArrayList<>();

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	DisposalAssetListDTO selectedDisposalAssetListDTO = new DisposalAssetListDTO();

	@Getter
	@Setter
	DisposalAssetListDTO viewResponseDTO = new DisposalAssetListDTO();

	@Getter
	@Setter
	AssetDisposalRequest assetDisposal;

	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	Long noteId;

	@Getter
	@Setter
	Boolean forwardFor, isDisposalStatus = false, viewFlag = false;

	@Getter
	@Setter
	String stage;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<AssetDisposalRequestLog> assetDisposalRequestLogList = new ArrayList<>();

	@Getter
	@Setter
	String disposalRemarks;

	@Getter
	@Setter
	Boolean visibility = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@PostConstruct
	public void init() {
		log.info("assetDisposalBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadAssetDisposalInformation();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadAssetDisposalInformation() {
		log.info("<=AssetDisposalBean :: loadAssetDisposalInformation=>");
		try {
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("ASSET_DISPOSAL");
			entityTypeList = new ArrayList<>();
			entityTypeList = commonDataService.getAllEntityType();
			loadAssetCategory();

		} catch (Exception e) {
			log.error("Exception in loadAssetDisposalInformation  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadAssetCategory() {
		log.info("<=AssetDisposalBean :: loadAssetCategory=>");
		try {
			url = SERVER_URL + "/assetcategorymaster/loadallprimaryassetcategory";
			log.info("loadIncrementCycle :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					assetCategoryMasterList = mapper.readValue(jsonValue,
							new TypeReference<List<AssetCategoryMaster>>() {
							});
					log.info("AssetDisposalBean :: assetCategoryMasterList==> " + assetCategoryMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadAssetCategory  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadAssetSubCategory() {
		log.info("<=AssetDisposalBean :: loadAssetCategory=>");
		try {
			if (assetCategoryMaster == null && assetCategoryMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_CATEGORY_ID_REQUIRED).getCode());
				return;
			}
			Long categoryId = assetCategoryMaster.getId();
			log.info("loadAssetSubCategory :: categoryId==> " + categoryId);
			url = SERVER_URL + "/assetcategorymaster/loadassetsubcategory/:categoryId";
			url = url.replace(":categoryId", categoryId.toString());
			log.info("loadAssetSubCategory :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					subAssetCategoryList = mapper.readValue(jsonValue, new TypeReference<List<AssetCategoryMaster>>() {
					});
					log.info("AssetDisposalBean :: subAssetCategoryList==> " + subAssetCategoryList.size());
				} else {
					subAssetCategoryList = new ArrayList<>();
					if (baseDTO.getStatusCode() == ErrorDescription
							.getError(OperationErrorCode.ASSET_SUB_CATEGORY_LIST_EMPTY).getCode()) {
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.ASSET_SUB_CATEGORY_LIST_EMPTY).getCode());
						return;
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadAssetCategory  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onEntityChg() {

		if (headRegionOffice != null && entityType != null) {
			entityMasterList = commonDataService.loadEntityByregionOrentityTypeId(headRegionOffice.getId(),
					entityType.getId());
		}

	}

	public void updateEntityList() {
		log.info("<=Starts AssetRegister.updateEntityList =>");
		try {

			if (headRegionOffice.getName().equalsIgnoreCase("HEAD OFFICE")) {
				visibility = true;

			} else {
				visibility = false;

			}
			if (headRegionOffice != null && headRegionOffice.getId() != null && entityType != null
					&& entityType.getId() != null) {
				Long regionId = headRegionOffice.getId();
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityType.getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends AssetRegister.updateEntityList =>");
	}

	public String callAssetDisposalPage() {
		log.info("<=AssetDisposalBean :: callAssetDisposalPage=>");
		try {

			if (pageAction.equals("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				loadLazyAssetDisposal();
				return LIST_PAGE;
			} else if (pageAction.equals("ADD")) {

				headRegionOffice = new EntityMaster();
				assetCategoryMaster = new AssetCategoryMaster();
				subAssetCategoryMaster = new AssetCategoryMaster();
				disposalAssetDetailsDTOList = new ArrayList<>();
				entityType = new EntityTypeMaster();
				entityTypeList = new ArrayList<>();
				entityTypeList = commonDataService.getAllEntityType();
				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();
				updateEntityList();
				onEntityChg();
				return ADD_PAGE;
			} else if (pageAction.equals("EDIT")) {
				if (selectedDisposalAssetListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return editAssetDisposal();
			} else if (pageAction.equals("VIEW")) {
				if (selectedDisposalAssetListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewAssetDisposal();
			} else if (pageAction.equals("DELETE")) {
				if (selectedDisposalAssetListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
				// return LIST_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in callAssetDisposalPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editAssetDisposal() {
		log.info("<=AssetDisposalBean :: editAssetDisposal=>");
		try {
			log.info("Selected disposal asset Id :: " + selectedDisposalAssetListDTO.getDisposalAssetId());

			Long disposalAssetId = selectedDisposalAssetListDTO.getDisposalAssetId();

			Long notificationId = selectedDisposalAssetListDTO.getNotificationId();

			String url = SERVER_URL + "/assetdisposal/getassetdisposalbyid/" + disposalAssetId + "/"
					+ (notificationId != null ? notificationId : 0);

			/*
			 * String url = SERVER_URL + "/assetdisposal/getassetdisposalbyid/:id"; url =
			 * url.replace(":id",
			 * selectedDisposalAssetListDTO.getDisposalAssetId().toString());
			 * log.info("viewAssetDisposal :: url==> " + url);
			 */
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				assetDisposal = mapper.readValue(jsonResponse, AssetDisposalRequest.class);
				if (assetDisposal != null || assetDisposal.getId() != null) {
					headRegionOffice = assetDisposal.getEntityMaster();
					log.info("headRegionOffice id==> " + headRegionOffice.getId());
					assetCategoryMaster = assetDisposal.getAssetCategory();
					log.info("assetCategoryMaster id==> " + assetCategoryMaster.getId());
					loadAssetSubCategory();
					subAssetCategoryMaster = assetDisposal.getAssetSubCategory();
					log.info("subAssetCategoryMaster id==> " + subAssetCategoryMaster.getId());
					loadAssetDisposalDetails(assetDisposal.getAssetDisposalRequestDetailsList());

					// -- to get asset disposal note details
					if (assetDisposal.getAssetDisposalRequestNoteList() != null
							&& !assetDisposal.getAssetDisposalRequestNoteList().isEmpty()) {
						for (AssetDisposalRequestNote assetDisposalRequestNote : assetDisposal
								.getAssetDisposalRequestNoteList()) {
							noteId = assetDisposalRequestNote.getId();
							forwardToUser = assetDisposalRequestNote.getForwardTo();
							log.info("forwardToUser id=> " + forwardToUser.getId());
							forwardFor = assetDisposalRequestNote.getFinalApproval();
							log.info("forwardFor=> " + forwardFor);
							note = assetDisposalRequestNote.getNote();
							log.info("note=> " + note);
						}

						for (AssetDisposalRequestLog assetDisposalRequestLog : assetDisposal
								.getAssetDisposalRequestLogList()) {
							stage = assetDisposalRequestLog.getStage();

						}

						if (stage.equalsIgnoreCase("APPROVED") && forwardFor == true) {
							finalApproveFlag = true;

						} else {
							finalApproveFlag = false;
						}

						Long loginUserId = loginBean.getUserMaster().getId();
						Long noteUserId = forwardToUser.getId();

						log.info("loginUserId=> " + loginUserId);
						log.info("noteUserId=> " + noteUserId);
						if (loginUserId.equals(noteUserId)) {
							viewFlag = true;
						} else {
							viewFlag = false;
						}
					} else {
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.CONFIRMATION_POSTPONE_NOTE_NOT_FOUND).getCode());
						return null;
					}
					return ADD_PAGE;
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_EMPTY).getCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception in callAssetDisposalPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void getComments() {
		try {
			viewResponseDTO = new DisposalAssetListDTO();
			BaseDTO response = httpService
					.get(SERVER_URL + "/getapproverejectcomments/" + selectedDisposalAssetListDTO.getDisposalAssetId());
			if (response != null && response.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<DisposalAssetListDTO>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception occured while getComments", e);
		}

	}

	public String viewAssetDisposal() {
		log.info("<=AssetDisposalBean :: viewAssetDisposal=>");
		try {
			log.info("Selected disposal asset Id :: " + selectedDisposalAssetListDTO.getDisposalAssetId());

			Long disposalAssetId = selectedDisposalAssetListDTO.getDisposalAssetId();

			Long notificationId = selectedDisposalAssetListDTO.getNotificationId();

			String url = SERVER_URL + "/assetdisposal/getassetdisposalbyid/" + disposalAssetId + "/"
					+ (notificationId != null ? notificationId : 0);
			/*
			 * url = url.replace(":id",
			 * selectedDisposalAssetListDTO.getDisposalAssetId().toString()); url =
			 * url.replace(":notificationId",
			 * selectedDisposalAssetListDTO.getNotificationId().toString());
			 */
			log.info("viewAssetDisposal :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				assetDisposal = mapper.readValue(jsonResponse, AssetDisposalRequest.class);
				if (assetDisposal != null || assetDisposal.getId() != null) {
					headRegionOffice = assetDisposal.getEntityMaster();
					log.info("headRegionOffice id==> " + headRegionOffice.getId());
					assetCategoryMaster = assetDisposal.getAssetCategory();
					log.info("assetCategoryMaster id==> " + assetCategoryMaster.getId());
					subAssetCategoryMaster = assetDisposal.getAssetSubCategory();
					log.info("subAssetCategoryMaster id==> " + subAssetCategoryMaster.getId());

					loadAssetDisposalDetails(assetDisposal.getAssetDisposalRequestDetailsList());

					commentAndLogList = baseDTO.getCommentAndLogList();

					// -- to get asset disposal note details
					if (assetDisposal.getAssetDisposalRequestNoteList() != null
							&& !assetDisposal.getAssetDisposalRequestNoteList().isEmpty()) {
						for (AssetDisposalRequestNote assetDisposalRequestNote : assetDisposal
								.getAssetDisposalRequestNoteList()) {
							noteId = assetDisposalRequestNote.getId();
							forwardToUser = assetDisposalRequestNote.getForwardTo();
							log.info("forwardToUser id=> " + forwardToUser.getId());
							forwardFor = assetDisposalRequestNote.getFinalApproval();
							log.info("forwardFor=> " + forwardFor);
							note = assetDisposalRequestNote.getNote();
							log.info("note=> " + note);
						}

						if (notificationId != null) {
							systemNotificationBean.loadTotalMessages();
						}

						for (AssetDisposalRequestLog assetDisposalRequestLog : assetDisposal
								.getAssetDisposalRequestLogList()) {
							stage = assetDisposalRequestLog.getStage();

						}

						if (stage.equalsIgnoreCase("APPROVED") && forwardFor == true) {
							finalApproveFlag = true;

						} else {
							finalApproveFlag = false;
						}

						Long loginUserId = loginBean.getUserMaster().getId();
						Long noteUserId = forwardToUser.getId();

						log.info("loginUserId=> " + loginUserId);
						log.info("noteUserId=> " + noteUserId);
						if (loginUserId.equals(noteUserId)) {
							viewFlag = true;
						} else {
							viewFlag = false;
						}
					} else {
						log.error("Asset disposal note not found");
					}
					// --to get asset disposal log details
					if (assetDisposal.getAssetDisposalRequestLogList() != null
							&& !assetDisposal.getAssetDisposalRequestLogList().isEmpty()) {
						assetDisposalRequestLogList = assetDisposal.getAssetDisposalRequestLogList();
					} else {
						log.error("Asset disposal log not found");
					}
					return VIEW_PAGE;
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_EMPTY).getCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception in callAssetDisposalPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadAssetDisposalDetails(List<AssetDisposalRequestDetails> AssetDisposalRequestDetailsList) {
		log.info("<=AssetDisposalBean :: loadAssetDisposalDetails=>");
		try {
			// List<DisposalAssetDetailsDTO> disposalAssetDetailsDTOList = new
			// ArrayList<>();
			if (AssetDisposalRequestDetailsList != null && !AssetDisposalRequestDetailsList.isEmpty()) {
				for (AssetDisposalRequestDetails disposalRequestDetails : AssetDisposalRequestDetailsList) {
					DisposalAssetDetailsDTO disposalAssetDetailsDTO = new DisposalAssetDetailsDTO();

					log.info("Asset register id==> " + disposalRequestDetails.getAssetRegister().getId());
					log.info("Asset name==> " + disposalRequestDetails.getAssetRegister().getAssetName());
					log.info("Quantity==> " + disposalRequestDetails.getAssetRegister().getQuantity());
					log.info("PurchasedDate==> " + disposalRequestDetails.getAssetRegister().getPurchasedDate());
					log.info("PurchaseValue==> " + disposalRequestDetails.getAssetRegister().getCostOfAsset());
					log.info("Asset Age==> " + disposalRequestDetails.getAssetRegister().getAssetAge());
					log.info("Current Value==> " + disposalRequestDetails.getCurrentValue());
					log.info("Disposal Quantity==> " + disposalRequestDetails.getDisposalQuantity());
					log.info("Reason==> " + disposalRequestDetails.getReason());
					log.info("Asset disposal details id ==> " + disposalRequestDetails.getId());

					disposalAssetDetailsDTO.setAssetDisposalDetailId(disposalRequestDetails.getId());
					disposalAssetDetailsDTO.setAssetRegisterId(disposalRequestDetails.getAssetRegister().getId());
					disposalAssetDetailsDTO.setAssetName(disposalRequestDetails.getAssetRegister().getAssetName());
					disposalAssetDetailsDTO.setQuantity(disposalRequestDetails.getAssetRegister().getQuantity());
					disposalAssetDetailsDTO
							.setPurchasedDate(disposalRequestDetails.getAssetRegister().getPurchasedDate());
					disposalAssetDetailsDTO
							.setPurchaseValue(disposalRequestDetails.getAssetRegister().getCostOfAsset());
					disposalAssetDetailsDTO.setAssetAge(disposalRequestDetails.getAssetRegister().getAssetAge());
					disposalAssetDetailsDTO.setCurrentValue(disposalRequestDetails.getCurrentValue());
					disposalAssetDetailsDTO.setDisposalQty(disposalRequestDetails.getDisposalQuantity());
					disposalAssetDetailsDTO.setReason(disposalRequestDetails.getReason());

					disposalAssetDetailsDTOList.add(disposalAssetDetailsDTO);
				}
			} else {
				AppUtil.addError("Asset disposal request details not found");
				return;
			}
		} catch (Exception e) {
			log.error(" Exception Occured While load AssetDisposal Details  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String deleteAssetDisposal() {
		log.info("<=AssetDisposalBean :: deleteAssetDisposal=>");
		try {
			if (selectedDisposalAssetListDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected disposal asset Id :: " + selectedDisposalAssetListDTO.getDisposalAssetId());
			String url = SERVER_URL + "/assetdisposal/delete/id/:id";
			url = url.replace(":id", selectedDisposalAssetListDTO.getDisposalAssetId().toString());
			log.info("deleteAssetDisposal url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Increment Deleted SuccessFully ");
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_DELETED_SUCCESSFULLY).getCode());
				clear();
				loadLazyAssetDisposal();
				return LIST_PAGE;
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Delete Asset Disposal  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void clear() {
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		selectedDisposalAssetListDTO = new DisposalAssetListDTO();
		selectedDisposalAssetList = new ArrayList<>();
		assetCategoryMaster = new AssetCategoryMaster();
		subAssetCategoryMaster = new AssetCategoryMaster();
		forwardToUser = new UserMaster();
		forwardFor = null;
		entityMaster = new EntityMaster();
		entityType = new EntityTypeMaster();
		disposalAssetDetailsDTOList = new ArrayList<>();
		isDisposalStatus = false;
		headRegionOffice = new EntityMaster();
	}

	public String cancelAssetDisposal() {
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		selectedDisposalAssetList = new ArrayList<>();
		selectedDisposalAssetListDTO = new DisposalAssetListDTO();
		assetCategoryMaster = new AssetCategoryMaster();
		subAssetCategoryMaster = new AssetCategoryMaster();
		forwardToUser = new UserMaster();
		headRegionOffice = new EntityMaster();
		forwardFor = null;
		disposalAssetDetailsDTOList = new ArrayList<>();
		isDisposalStatus = false;
		return LIST_PAGE;
	}

	public void loadLazyAssetDisposal() {
		log.info("<==== Starts AssetDisposalBean.loadLazyAssetDisposal =====>");
		disposalAssetListDTOLazyList = new LazyDataModel<DisposalAssetListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<DisposalAssetListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/assetdisposal/loadassetdisposal";
					log.info("loadLazyIncrementList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						disposalAssetListDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<DisposalAssetListDTO>>() {
								});
						log.info("loadLazyAssetDisposal :: disposalAssetListDTOList size==> "
								+ disposalAssetListDTOList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyIncrementList :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return disposalAssetListDTOList;
			}

			@Override
			public Object getRowKey(DisposalAssetListDTO res) {
				return res != null ? res.getDisposalAssetId() : null;
			}

			@Override
			public DisposalAssetListDTO getRowData(String rowKey) {
				for (DisposalAssetListDTO disposalAssetListDTO : disposalAssetListDTOList) {
					if (disposalAssetListDTO.getDisposalAssetId().equals(Long.valueOf(rowKey))) {
						selectedDisposalAssetListDTO = disposalAssetListDTO;
						return disposalAssetListDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends AssetDisposalBean.loadLazyAssetDisposal =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedDisposalAssetListDTO = ((DisposalAssetListDTO) event.getObject());
		log.info("selectedDisposalAssetListDTO getStatus ==> " + selectedDisposalAssetListDTO.getStatus());
		if (selectedDisposalAssetListDTO.getStatus().equalsIgnoreCase("APPROVED")
				|| selectedDisposalAssetListDTO.getStatus().equalsIgnoreCase("FINAL-APPROVED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedDisposalAssetListDTO.getStatus().equalsIgnoreCase("REJECTED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = true;
			isDeleteButton = true;
		} else if (selectedDisposalAssetListDTO.getStatus().equalsIgnoreCase("SUBMITTED")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = false;
			isDeleteButton = true;
		}
	}

	public void loadDisposalAssetList() {
		log.info("<=AssetDisposalBean :: loadDisposalAssetList=>");
		try {
			if (headRegionOffice == null || headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return;
			}
			if (assetCategoryMaster == null || assetCategoryMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_CATEGORY_ID_REQUIRED).getCode());
				return;
			}
			if (subAssetCategoryMaster == null || subAssetCategoryMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_SUB_CATEGORY_ID_REQUIRED).getCode());
				return;
			}
			log.info("loadDisposalAssetList :: head region office id==> " + headRegionOffice.getId());
			log.info("loadDisposalAssetList :: category id==> " + assetCategoryMaster.getId());
			log.info("loadDisposalAssetList :: subcategory id==> " + subAssetCategoryMaster.getId());
			url = SERVER_URL + "/assetdisposal/loaddisposalassetlist/:horoId/:categoryId/:subcategoryId";
			if (headRegionOffice.getName().equalsIgnoreCase("HEAD OFFICE")) {
				url = url.replace(":horoId", headRegionOffice.getId().toString());
			} else {
				url = url.replace(":horoId", entityMaster.getId().toString());
			}

			url = url.replace(":categoryId", assetCategoryMaster.getId().toString());
			url = url.replace(":subcategoryId", subAssetCategoryMaster.getId().toString());

			log.info("loadAssetSubCategory :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					disposalAssetDetailsDTOList = mapper.readValue(jsonValue,
							new TypeReference<List<DisposalAssetDetailsDTO>>() {
							});
					log.info("AssetDisposalBean :: disposalAssetDetailsDTOList==> "
							+ disposalAssetDetailsDTOList.size());

				} else {
					disposalAssetDetailsDTOList = new ArrayList<>();

					AppUtil.addWarn("No Record Found");
					/*
					 * if (baseDTO.getStatusCode() == ErrorDescription
					 * .getError(OperationErrorCode.ASSET_SUB_CATEGORY_LIST_EMPTY).getCode()) {
					 * errorMap.notify(
					 * ErrorDescription.getError(OperationErrorCode.ASSET_SUB_CATEGORY_LIST_EMPTY).
					 * getCode()); return; }
					 */
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadDisposalAssetList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void setDisposalList(DisposalAssetDetailsDTO disposalAssetDetails) {
		log.info("<=AssetDisposalBean :: setDisposalList=>");
		try {
			log.info("isDisposalStatus==> " + isDisposalStatus);
			if (isDisposalStatus == true) {
				log.info("disposalAssetDetails=> " + disposalAssetDetails);
				selectedDisposalAssetList.add(disposalAssetDetails);
				log.info("selectedDisposalAssetList size==> " + selectedDisposalAssetList.size());
			}
		} catch (Exception e) {
			log.error("Exception in setDisposalList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String saveOrUpdateAssetDisposal() {
		log.info("<=AssetDisposalBean :: callAssetDisposal=>");
		try {
			if (headRegionOffice == null || headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return null;
			}
			if (assetCategoryMaster == null || assetCategoryMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_CATEGORY_ID_REQUIRED).getCode());
				return null;
			}
			if (subAssetCategoryMaster == null || subAssetCategoryMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_SUB_CATEGORY_ID_REQUIRED).getCode());
				return null;
			}
			if (forwardToUser == null || forwardToUser.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
				return null;
			}
			if (forwardFor == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_FOR_NOT_FOUND).getCode());
				return null;
			}
			if (note == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.DISPOSAL_NOTE_EMPTY).getCode());
				return null;
			}

			for (DisposalAssetDetailsDTO data : disposalAssetDetailsDTOList) {
				if (data.getCurrentValue() == null) {
					AppUtil.addError("Current Value is Required");
					return null;
				}
				if (data.getDisposalQty() == null) {
					AppUtil.addError("Disposal Quantity is Required");
					return null;
				}
				if (selectedDisposalAssetList.isEmpty()) {
					AppUtil.addError("Select Atleast One Record");
					return null;
				}
			}
			if (pageAction.equals("ADD")) {
				return saveAssetDisposal();
			} else {
				return updateAssetDisposal();
			}
		} catch (Exception e) {
			log.error("Exception in setDisposalList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String updateAssetDisposal() {
		log.info("<=AssetDisposalBean :: updateAssetDisposal=>");
		try {
			UpdateAssetDisposalDTO updateAssetDisposalDTO = new UpdateAssetDisposalDTO();
			updateAssetDisposalDTO.setAssetDisposalRequest(assetDisposal);
			updateAssetDisposalDTO.setDisposalAssetDetailsDTOList(disposalAssetDetailsDTOList);

			List<AssetDisposalRequestNote> AssetDisposalRequestList = new ArrayList<>();
			AssetDisposalRequestNote assetDisposalRequestNote = new AssetDisposalRequestNote();
			log.info("updateAssetDisposal: note==> " + note);
			assetDisposalRequestNote.setId(noteId);
			assetDisposalRequestNote.setNote(note);
			assetDisposalRequestNote.setAssetDisposalRequest(assetDisposal);
			log.info("saveConfirmationPostpone: forwardToUser id==> " + forwardToUser.getId());
			assetDisposalRequestNote.setForwardTo(forwardToUser);
			log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
			assetDisposalRequestNote.setFinalApproval(forwardFor);
			AssetDisposalRequestList.add(assetDisposalRequestNote);

			// updateAssetDisposalDTO.setAssetDisposalRequestNoteList(AssetDisposalRequestList);

			List<AssetDisposalRequestLog> empManualPostponedLogList = new ArrayList<>();
			AssetDisposalRequestLog assetDisposalRequestLog = new AssetDisposalRequestLog();
			assetDisposalRequestLog.setAssetDisposalRequest(assetDisposal);
			assetDisposalRequestLog.setStage(ApprovalStage.SUBMITTED);
			empManualPostponedLogList.add(assetDisposalRequestLog);
			updateAssetDisposalDTO.setAssetDisposalRequestLogList(empManualPostponedLogList);

			String url = SERVER_URL + "/assetdisposal/updateassetdisposal";
			log.info("updateAssetDisposal :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, updateAssetDisposalDTO);
			log.info("updateAssetDisposal :: baseDTO ==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clear();
				loadLazyAssetDisposal();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_UPDATED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in setDisposalList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String saveAssetDisposal() {
		log.info("<=AssetDisposalBean :: saveAssetDisposal=>");
		try {

			AssetDisposalRequest assetDisposalRequest = new AssetDisposalRequest();
			assetDisposalRequest.setEntityMaster(headRegionOffice);
			assetDisposalRequest.setAssetCategory(assetCategoryMaster);
			assetDisposalRequest.setAssetSubCategory(subAssetCategoryMaster);
			List<AssetDisposalRequestDetails> disposalAssetDetailsList = new ArrayList<>();
			if (selectedDisposalAssetList != null && !selectedDisposalAssetList.isEmpty()) {
				log.info("selectedDisposalAssetList size==> " + selectedDisposalAssetList.size());
				for (DisposalAssetDetailsDTO disposalAssetDetailsDTO : selectedDisposalAssetList) {
					log.info("Asset register id==> " + disposalAssetDetailsDTO.getAssetRegisterId());
					log.info("Current value==> " + disposalAssetDetailsDTO.getCurrentValue());
					log.info("Disposal qty==> " + disposalAssetDetailsDTO.getDisposalQty());
					log.info("Reason==> " + disposalAssetDetailsDTO.getReason());

					if (disposalAssetDetailsDTO.getDisposalQty() == 0) {
						AppUtil.addWarn("Disposal Quantity Should Not be Zero");
						return null;

					}

					if (disposalAssetDetailsDTO.getCurrentValue() == 0) {
						AppUtil.addWarn("Current Value Should Not be Zero");
						return null;

					}

					AssetDisposalRequestDetails assetDisposalRequestDetails = new AssetDisposalRequestDetails();
					AssetRegister assetRegister = new AssetRegister();
					assetDisposalRequestDetails.setAssetDisposalRequest(assetDisposalRequest);
					assetRegister.setId(disposalAssetDetailsDTO.getAssetRegisterId());
					assetDisposalRequestDetails.setAssetRegister(assetRegister);
					assetDisposalRequestDetails.setCurrentValue(disposalAssetDetailsDTO.getCurrentValue());
					assetDisposalRequestDetails.setCurrentAmount(disposalAssetDetailsDTO.getCurrentValue());

					log.info("Asset quantity==> " + disposalAssetDetailsDTO.getQuantity());
					if (disposalAssetDetailsDTO.getQuantity() >= disposalAssetDetailsDTO.getDisposalQty()) {
						assetDisposalRequestDetails.setDisposalQuantity(disposalAssetDetailsDTO.getDisposalQty());
					} else {
						// AppUtil.addError("Disposal quantity should less than asset quantity");
						errorMap.notify(ErrorDescription
								.getError(OperationErrorCode.DISPOSAL_QUANTITY_SHOULD_LESS_THAN_ASSET_QUANTITY)
								.getCode());
						return null;
					}
					assetDisposalRequestDetails.setReason(disposalAssetDetailsDTO.getReason());
					disposalAssetDetailsList.add(assetDisposalRequestDetails);
				}
				assetDisposalRequest.setAssetDisposalRequestDetailsList(disposalAssetDetailsList);
			} else {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.DISPOSAL_ASSET_LIST_NOT_FOUND).getCode());
				// errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			List<AssetDisposalRequestNote> assetDisposalRequestNoteList = new ArrayList<>();
			AssetDisposalRequestNote assetDisposalRequestNote = new AssetDisposalRequestNote();
			log.info("saveEmployeeIncrement: note==> " + note);
			assetDisposalRequestNote.setNote(note);
			assetDisposalRequestNote.setAssetDisposalRequest(assetDisposalRequest);
			log.info("saveEmployeeIncrement: forwardToUser id==> " + forwardToUser.getId());
			assetDisposalRequestNote.setForwardTo(forwardToUser);
			log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
			assetDisposalRequestNote.setFinalApproval(forwardFor);
			assetDisposalRequestNoteList.add(assetDisposalRequestNote);
			assetDisposalRequest.setAssetDisposalRequestNoteList(assetDisposalRequestNoteList);

			List<AssetDisposalRequestLog> assetDisposalRequestLogList = new ArrayList<>();
			AssetDisposalRequestLog assetDisposalRequestLog = new AssetDisposalRequestLog();
			assetDisposalRequestLog.setAssetDisposalRequest(assetDisposalRequest);
			assetDisposalRequestLog.setStage(ApprovalStage.SUBMITTED);
			assetDisposalRequestLogList.add(assetDisposalRequestLog);
			assetDisposalRequest.setAssetDisposalRequestLogList(assetDisposalRequestLogList);

			String url = SERVER_URL + "/assetdisposal/createassetdisposal";
			log.info("saveEmployeeIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.post(url, assetDisposalRequest);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clear();
				loadLazyAssetDisposal();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_INSERTED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in saveAssetDisposal  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose : to save approved or rejected status in log table
	 * 
	 * @param status
	 * @return
	 */
	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> " + status);
			log.info("disposalRemarks==> " + disposalRemarks);
			log.info("logList id==> " + assetDisposalRequestLogList.get(0).getId());
			log.info("noteId==> " + noteId);
			log.info("forwardFor==> " + forwardFor);
			AssetDisposalForwardDTO assetDisposalForwardDTO = new AssetDisposalForwardDTO();
			assetDisposalForwardDTO.setLogList(assetDisposalRequestLogList);
			assetDisposalForwardDTO.setForwardRemarks(disposalRemarks);
			assetDisposalForwardDTO.setAssetDisposalId(assetDisposal.getId());
			assetDisposalForwardDTO.setNoteId(noteId);
			log.info("forward user id==> " + forwardToUser.getId());
			assetDisposalForwardDTO.setForwardUserId(forwardToUser.getId());
			assetDisposalForwardDTO.setForwardFor(forwardFor);
			if (status.equals(REJECT)) {
				assetDisposalForwardDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals(APPROVED)) {
				assetDisposalForwardDTO.setStatus(ApprovalStage.APPROVED);
			} else {
				assetDisposalForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + "/assetdisposal/updateassetdisposallog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, assetDisposalForwardDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clear();
				loadLazyAssetDisposal();
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ASSET_DISPOSAL_LOG_INSERTED_SUCCESSFULLY)
						.getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving submitApprovedRejectedStatus :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_PAGE;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String assetDisposalId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			assetDisposalId = httpRequest.getParameter("assetDisposalId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		selectedDisposalAssetListDTO = new DisposalAssetListDTO();
		if (!StringUtils.isEmpty(notificationId)) {
			selectedDisposalAssetListDTO.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(assetDisposalId)) {
			selectedDisposalAssetListDTO.setDisposalAssetId(Long.parseLong(assetDisposalId));
			pageAction = "VIEW";
			viewAssetDisposal();
		}
		return VIEW_PAGE;
	}
}
