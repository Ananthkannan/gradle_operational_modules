package in.gov.cooptex.operation.rest.util;

import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class FormValidation {

	// Mobile Number Validation
	public static String validateMobileNumber(String mobileNumber) {
		if (mobileNumber.length() > 0) {
			if (mobileNumber.length() != 10) {
				return null;
			}
			if (mobileNumber.length() == 10) {
				String regex = "[7-9][0-9]{9}";
				Pattern pattern = Pattern.compile(regex);
				Matcher matcher = pattern.matcher(mobileNumber);
				if (!matcher.matches()) {
					return null;
				}
			}
		}
		return mobileNumber;
	}

	// Fax Number Validation
	public static String validateFaxNumber(String faxNumber) {

		if (faxNumber.length() > 0 && faxNumber.length() == 12) {

			Pattern faxPattern = Pattern.compile("\\d{3}-\\d{8}");
			Matcher faxMatcher = faxPattern.matcher(faxNumber);
			if (!faxMatcher.matches()) {
				return null;
			}

		}
		return faxNumber;
	}

	// Email Id Validation
	public static String validateEmailId(String emailId) {
		if (emailId != null && emailId.length() > 0) {

			String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
			Pattern pattern = Pattern.compile(ePattern);
			Matcher matcher = pattern.matcher(emailId);
			if (!matcher.matches()) {
				return null;
			}

		}
		return emailId;
	}

	public static boolean validateName(String name) {
		int length = name.trim().length();
		if (length < 3 || length > 50) {
			return false;
		}
		return true;
	}

	public static boolean validateLength(int min, int max, String value) {
		log.info(":: Request Min Length is : {}",min);
		log.info(":: Request Max Length is : {}",max);
		log.info(":: Request Value is : {}",value);
		int length = value.trim().length();
		if (length < min || length > max) {
			return false;
		}
		return true;
	}

	public static boolean validateYear(int reqYear) {
		log.info(":: Request year is : {}",reqYear);
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		if (reqYear > year || reqYear < 1947) {
			return false;
		}
		return true;
	}
	
	public static boolean validateTwoDates(Date first,Date second) {
		log.info(" :: First Date is : {}",first);
		log.info(" :: Second Date id : {}",second);
        //compare both dates
        if(first.after(second)){
        	return false;
        }
		return true;
	}

}
