package in.gov.cooptex.masters.ecomm.ui.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.SocietyProductAppraisal;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("ecommStockAcknowledgementBean")
@Scope("session")
@Log4j2
public class EcommStockAcknowledgementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/ecommerce/admin/listEcommStockAcknowledgement.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/ecommerce/admin/createEcommStockAcknowledgement.xhtml?faces-redirect=true;";

	private final String PREVIEW_SOCIETY_STOCK_ACK_URL = "/pages/ecommerce/admin/previewEcommStockAcknowledgement.xhtml?faces-redirect=true;";
	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	String pageaction;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	EntityMaster selectedFromEntity;

	@Getter
	@Setter
	List<EntityMaster> fromEntityList;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer stockSize;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	String purchaseOrderNumber;

	@Getter
	@Setter
	Integer invoiceNumber;

	@Getter
	@Setter
	Date invoiceDate;

	// Stock Inward From
	@Getter
	@Setter
	List<SupplierMaster> stockInwardFromList;

	@Getter
	@Setter
	List<StockTransfer> stockMovementNumberList;

	@Getter
	@Setter
	SupplierMaster selectedStockInwardFrom;

	@Getter
	@Setter
	StockTransfer selectedStockTransfer;

	@Getter
	@Setter
	SocietyProductAppraisal societyProductAppraisal = new SocietyProductAppraisal();

	@Getter
	@Setter
	StockTransfer selectedStockMovementObj;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	List<StockTransfer> stockTransferMovementList;

	@Getter
	@Setter
	Boolean disableBtnSearch;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsMovementList;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDlgDisplay;

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	@Autowired
	LoginBean loginBean;

	@PostConstruct
	private void init() {
		log.info("init is called.");
		stockAcknowledgementBeanListPage();
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferResponse = new StockTransferResponse();
		selectedFromEntity = new EntityMaster();
		fromEntityList = new ArrayList<>();
		stockAcknowledgementBeanListPage();
		addButtonFlag = false;
		editButtonFlag = true;

	}

	public String stockAcknowledgementBeanListPage() {
		log.info("stockAcknowledgementBeanListPage is called.");

		addButtonFlag = false;
		editButtonFlag = true;
		pageaction = "";
		stockTransferResponse = new StockTransferResponse();
		statusValues = stockTransferUtility.loadStockTransferStatusList();

		List<Object> status = getStatus();
		if (!status.isEmpty()) {
			statusValues = status;
		}
		loadLazyList();
		return LIST_PAGE_URL;
	}

	private List<Object> getStatus() {
		List<Object> status = new ArrayList<>();
		status.add("INITIATED");
		status.add("SUBMITTED");
		status.add("COMPLETED");
		status.add("ACKNOWLEDGE");
		return status;
	}

	// List Page Method
	private void loadLazyList() {
		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();
				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO != null && baseDTO.getResponseContent() != null) {
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
						});
						if (data != null && data.size() > 0) {
							this.setRowCount(baseDTO.getTotalRecords());
							stockSize = baseDTO.getTotalRecords();
						}
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}
		};
	}

	private BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		String URL = SERVER_URL + "/ecommStockAcknowledgementController/lazystockacksearchlist";
		baseDTO = httpService.post(URL, request);
		log.info("SocietyStockMovementBean.getSearchData Method Complted");

		return baseDTO;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("fromEntityCodeOrName")) {
				request.setFromEntityCodeOrName(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		addButtonFlag = true;
		if (stockTransferResponse != null && stockTransferResponse.getStatus() != null
				&& stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
			editButtonFlag = false;
		} else if (stockTransferResponse != null && stockTransferResponse.getStatus() != null
				&& stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
			editButtonFlag = true;
		}
	}

	/**
	 * @return
	 */
	public String redirectSVInProgressPage() {
		try {
			Boolean stockVerification = loginBean.getStockVerificationIsInProgess();
			if (null != stockVerification && stockVerification.booleanValue()) {
				loginBean.setMessage("Stock acknowledgement");
				loginBean.redirectSVInProgressPgae();
			}
		} catch (Exception ex) {
			log.error("Exception at redirectSVInProgressPage()", ex);
		}
		return null;
	}

	public String addStockAcknowledgement() {
		log.info("addStockAcknowledgement method start");
		redirectSVInProgressPage();
		clear();
		// loadStockInwardFromList();
		loadFromEntity();
		log.info("addStockAcknowledgement method End");
		return ADD_PAGE_URL;
	}

	private void loadFromEntity() {

		log.info("loadFromEntity method start");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + "/ecommStockAcknowledgementController/loadfromentitylist";

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.info("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				fromEntityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});

			} else {
				log.warn("loadFromEntity not success error in service side *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadFromEntity " + ee);
		}

	}

	private void clear() {
		log.info("clear method start");
		stockMovementNumberList = new ArrayList<StockTransfer>();
		stockInwardFromList = new ArrayList<SupplierMaster>();
		selectedStockInwardFrom = new SupplierMaster();
		stockTransferMovementList = new ArrayList<StockTransfer>();
		selectedStockMovementObj = new StockTransfer();
		selectedStockTransfer = new StockTransfer();
		stockTransfer = new StockTransfer();
		selectedFromEntity = new EntityMaster();
	}

	public void loadStockInwardFromList(EntityMaster selectedFromEntity) {
		log.info("loadActiveProductwarehouseList method start");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + "/ecommStockAcknowledgementController/loadstockinwardfromlist/"
					+ selectedFromEntity.getId();

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				stockMovementNumberList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
				});

			} else {
				log.warn("loadActiveProductwarehouseList not success error in service side *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadActiveProductwarehouseList ", ee);
		}

	}

	public void onChangeFromEntity() {
		log.info("onChangeFromEntity " + selectedStockInwardFrom);
		try {
			stockMovementNumberList = new ArrayList<>();
			selectedStockTransfer = new StockTransfer();
			loadStockInwardFromList(selectedFromEntity);

		} catch (Exception e) {
			log.error("onChangeStockTransferFrom exception----", e);
		}

	}

	public void onSelectStockTransfer() {
		if (selectedStockMovementObj != null) {
			disableBtnSearch = false;
		} else {
			disableBtnSearch = true;
		}
	}

	public String searchSocietyStockMovement() {
		log.info("start searchSocietyStockMovement.....");
		stockTransfer = new StockTransfer();
		try {

			if (selectedStockTransfer == null || selectedStockTransfer.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(selectedStockTransfer.getId());

			if (stockTransfer != null) {

				// stockTransferResponse = new StockTransferResponse();

				stockTransfer.setStockTransfer(new StockTransfer());
				stockTransfer.getStockTransfer().setId(stockTransfer.getId());
				stockTransfer.setId(null);
				stockTransfer.setReferenceNumber(null);
				stockTransfer.setDateReceived(new Date());
				stockTransfer.setIsReceived(true);
				stockTransfer.setStatus(StockTransferStatus.INITIATED);
				stockTransfer.setTransferType(StockTransferType.ESHOPPING_INWARD);

				/*
				 * if (stockTransfer.getId() == null) {
				 * stockTransfer.setStockTransferItemsList(new ArrayList<>()); }
				 */
				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {

						/*
						 * stockTransferItemsObj
						 * .setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster()
						 * );
						 */

						if (stockTransferItemslocalObj.getUomMaster() == null) {
							Util.addWarn("UOM is Empty");
							// stockTransferItemsObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							log.info("UOM is not Empty" + stockTransferItemslocalObj.getUomMaster().getCode());
						}
						stockTransferItemslocalObj.setItemTotal(stockTransferItemslocalObj.getUnitRate()
								* stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						stockTransferItemslocalObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
						stockTransferItemslocalObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItems.setDescription(description);
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						log.info("selectedFromEntity " + selectedFromEntity != null
								? selectedFromEntity.getIsLocalState()
								: "NULL");
						if (selectedFromEntity != null && null != selectedFromEntity.getIsLocalState() && !selectedFromEntity.getIsLocalState()) {
							if (stockTransferItemslocalObj != null && stockTransferItemslocalObj.getTaxPercent() != null
									&& stockTransferItemslocalObj.getTaxPercent() > 0) {
								double tax = stockTransferItemslocalObj.getTaxPercent();
								double taxV = (total * tax) / 100;
								log.info("tax value----" + taxV);
								stockTransferItemslocalObj.setTaxValue(taxV);
								double taxValue = roundOffTaxValue(taxV);
								stockTransferItemslocalObj.setTaxValue(taxValue);
								stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

								stockTransferItems = new StockTransferItems();
								selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
								stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
								societyProductAppraisal = new SocietyProductAppraisal();
								loadProductVerietyTaxPercentage();
								stockTransferItemslocalObj
										.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
												? stockTransferItems.getCgstTaxPercent()
												: 0);
								stockTransferItemslocalObj
										.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
												? stockTransferItems.getSgstTaxPercent()
												: 0);
								stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
										+ stockTransferItemslocalObj.getTaxValue());
							}
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "";
	}

	private double roundOffTaxValue(double taxV) {
		Double taxRoundOff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxV));
		log.info("tax round off value----" + taxRoundOff);
		String str = String.valueOf(taxRoundOff);
		String[] convert = str.split("\\.");
		int length = convert[1].length();
		double taxValue = 0.0;
		if (length == 2) {
			int splitValue = Integer.valueOf(convert[1].substring(1));
			log.info("split up value----------" + splitValue);
			if (splitValue % 2 == 0) {
				log.info("splitValue is even-----");
				taxValue = taxRoundOff;
			} else {
				log.info("splitValue is odd-----");
				taxValue = taxRoundOff + 0.01;
				taxValue = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue));
			}
		} else {
			taxValue = taxRoundOff;
		}
		return taxValue;
	}

	public void loadProductVerietyTaxPercentage() {
		log.info("loadProductVerietyTaxPercentage :: start");
		try {

			if (societyProductAppraisal != null) {
				if (societyProductAppraisal.getProductCost() != null && stockTransferItems.getUnitRate() != null) {
					if (!societyProductAppraisal.getProductCost().equals(stockTransferItems.getUnitRate())) {
						stockTransferItems.setUnitRate(null);
						AppUtil.addError(" Product Cost And Unit Rate In Item Receive Details Should be Same");
						return;
					}
				}
			}

			Double totalTax = 0.0;
			if (selectedProductVarietyMaster.getId() == null) {
				// AppUtil.addError("Please select Product Variety Master");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
				return;
			}
			if (stockTransferItems.getUnitRate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.UNIT_RATE_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			Long ProductId = selectedProductVarietyMaster.getId();
			Double unitRate = stockTransferItems.getUnitRate();

			log.info("loadProductVerietyTaxPercentage :: ProductId==> " + ProductId);
			log.info("loadProductVerietyTaxPercentage :: unitRate==> " + unitRate);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/gettaxlistbyproductidunitrate/" + ProductId + "/" + unitRate;
			log.info("loadProductVerietyTaxPercentage :: url==> " + url);
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				List<ProductVarietyTax> proTaxList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyTax>>() {
						});
				if (proTaxList != null && !proTaxList.isEmpty()) {
					log.info("proTaxList size==> " + proTaxList.size());
					for (ProductVarietyTax proTax : proTaxList) {
						log.info("tax==> " + proTax.getTaxPercent());
						if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("SGST")) {
							stockTransferItems.setSgstTaxPercent(proTax.getTaxPercent());
						} else if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("CGST")) {
							stockTransferItems.setCgstTaxPercent(proTax.getTaxPercent());
						}

						totalTax = totalTax + proTax.getTaxPercent();

					}
				} else {
					totalTax = 0.0;
					log.error("proTaxList is null or empty");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
				}
				log.info("totalTax==> " + totalTax);
				stockTransferItems.setTaxPercent(totalTax);
			} else {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
			}

		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public void viewItemDetailsDlg(StockTransferItems items) {
		stockTransferItemsDlgDisplay = items;
	}

	public Double getCgstValue() {
		Double cgstValue = 0.0;
		try {
			if (stockTransfer.getStockTransferItemsList() != null
					&& stockTransfer.getStockTransferItemsList().size() > 0) {
				for (StockTransferItems stockTransferItemsObj : stockTransfer.getStockTransferItemsList()) {
					if (stockTransferItemsObj.getCgstTaxPercent() != null
							&& stockTransferItemsObj.getCgstTaxPercent() > 0) {
						Double cgstValueTemp = ((stockTransferItemsObj.getItemTotal() != null
								? stockTransferItemsObj.getItemTotal()
								: 0.0) * stockTransferItemsObj.getCgstTaxPercent()) / 100;
						log.info("cgst value----" + cgstValueTemp);
						cgstValue = cgstValue + roundOffTaxValue(cgstValueTemp);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return cgstValue;
	}

	public Double getSgstValue() {
		Double sgstValue = 0.0;
		try {
			if (stockTransfer.getStockTransferItemsList() != null
					&& stockTransfer.getStockTransferItemsList().size() > 0) {
				for (StockTransferItems stockTransferItemsObj : stockTransfer.getStockTransferItemsList()) {
					if (stockTransferItemsObj.getSgstTaxPercent() != null
							&& stockTransferItemsObj.getSgstTaxPercent() > 0) {
						Double sgstValueTemp = ((stockTransferItemsObj.getItemTotal() != null
								? stockTransferItemsObj.getItemTotal()
								: 0.0) * stockTransferItemsObj.getSgstTaxPercent()) / 100;
						log.info("sgst value----" + sgstValueTemp);
						sgstValue = sgstValue + roundOffTaxValue(sgstValueTemp);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return sgstValue;
	}

	public String saveStockTransfer() {
		log.info("saveStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		if (validateStockTransfer()) {
			// clearBundleDetails();
			return createstockackinward();
		} else {
			return null;
		}
	}

	public String submitStockTransfer() {
		log.info("submitStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		log.info("stocktransfer item list---------" + stockTransfer.getStockTransferItemsList().size());
		if (validateStockTransfer()) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			return null;
		} else {
			return null;
		}
	}

	private boolean validateStockTransfer() {

		if (stockTransfer.getStockTransferItemsList() == null || stockTransfer.getStockTransferItemsList().isEmpty()) {
			errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_ITEMS_REQUIRED.getCode());
			return false;
		}

		return true;
	}

	public String createstockackinward() {
		log.info("create method is executing..");

		String URL = SERVER_URL + "/ecommStockAcknowledgementController/createstockackinward"; 
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					AppUtil.addInfo("Stock Acknowledged Successfully");
					RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public String getStockAcknowledgementDetailBystockID() {
		log.info("getStockAcknowledgementDetailBystockID method start");
		clear();

		log.info("getStockAcknowledgementDetailBystockID method End");
		return ADD_PAGE_URL;
	}

	public String validateCurrentReceivedQty(StockTransferItems stockTransferItemsObj) {
		log.info("DispatchedQty() " + stockTransferItemsObj.getDispatchedQty());
		log.info("ReceivedQty() " + stockTransferItemsObj.getReceivedQty());
		if (stockTransferItemsObj != null && stockTransferItemsObj.getDispatchedQty() != null
				&& stockTransferItemsObj.getReceivedQty() != null) {
			if (stockTransferItemsObj.getReceivedQty() <= 0) {
				stockTransferItemsObj.setReceivedQty(0D);
			} else if (stockTransferItemsObj.getDispatchedQty() < stockTransferItemsObj.getReceivedQty()) {
				errorMap.notify(OperationErrorCode.SOCIETY_STOCK_ACK_RECEIVED_MORE_THAN_AVAILABLILITY);
				stockTransferItemsObj.setReceivedQty(null);
			}

			stockTransferItemsObj.setItemTotal(stockTransferItemsObj.getUnitRate()
					* (stockTransferItemsObj.getReceivedQty() != null ? stockTransferItemsObj.getReceivedQty() : 0));
			stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemTotal());
			Double total = stockTransferItemsObj.getItemTotal();
			if (stockTransferItemsObj.getDiscountPercent() != null && stockTransferItemsObj.getDiscountPercent() > 0
					&& total != null) {
				double discountV = (total * stockTransferItemsObj.getDiscountPercent()) / 100;
				stockTransferItemsObj.setDiscountValue(discountV);
				stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemNetTotal()
						- (stockTransferItemsObj.getDiscountValue() != null ? stockTransferItemsObj.getDiscountValue()
								: 0));
			}
			if(selectedFromEntity != null && null != selectedFromEntity.getIsLocalState() && !selectedFromEntity.getIsLocalState()){
			if (stockTransferItemsObj.getTaxPercent() != null && stockTransferItemsObj.getTaxPercent() > 0
					&& total != null) {
				double tax = stockTransferItemsObj.getTaxPercent();
				double taxV = (total * tax) / 100;
				log.info("tax value----" + taxV);
				stockTransferItemsObj.setTaxValue(taxV);
				double taxValue = roundOffTaxValue(taxV);
				stockTransferItemsObj.setTaxValue(taxValue);
				stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemNetTotal()
						+ (stockTransferItemsObj.getTaxValue() != null ? stockTransferItemsObj.getTaxValue() : 0));

				if ((stockTransferItemsObj.getCgstTaxPercent() == null
						&& stockTransferItemsObj.getSgstTaxPercent() == null)
						|| (stockTransferItemsObj.getCgstTaxPercent() == 0.0
								&& stockTransferItemsObj.getSgstTaxPercent() == 0.0)) {
					stockTransferItems = new StockTransferItems();
					selectedProductVarietyMaster = (stockTransferItemsObj.getProductVarietyMaster());
					stockTransferItems.setUnitRate(stockTransferItemsObj.getUnitRate());
					societyProductAppraisal = new SocietyProductAppraisal();
					loadProductVerietyTaxPercentage();
					stockTransferItemsObj.setCgstTaxPercent(
							stockTransferItems.getCgstTaxPercent() != null ? stockTransferItems.getCgstTaxPercent()
									: 0);
					stockTransferItemsObj.setSgstTaxPercent(
							stockTransferItems.getSgstTaxPercent() != null ? stockTransferItems.getSgstTaxPercent()
									: 0);
					}
				}
			}
		}
		return null;
	}

	public String viewSocietyStockAckInward() {
		log.info("start viewSocietyStockMovement:");
		stockTransfer = new StockTransfer();
		try {

			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

			if (stockTransfer != null) {
				stockTransferItemsMovementList = new ArrayList<>();
				stockTransferResponse = new StockTransferResponse();
				invoiceNumber = (stockTransfer.getPurchaseInvoiceNumber() != null
						? Integer.parseInt(stockTransfer.getPurchaseInvoiceNumber())
						: null);
				invoiceDate = stockTransfer.getInvoiceDate();
				selectedTransportTypeMaster = stockTransfer.getTransportMaster() != null
						? stockTransfer.getTransportMaster().getTransportTypeMaster()
						: null;

				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {

						// StockTransferItems stockTransferItemsObj = new StockTransferItems();
						stockTransferItemslocalObj
								.setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster());

						if (stockTransferItemslocalObj.getUomMaster() != null) {
							stockTransferItemslocalObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							Util.addWarn("UOM is Empty");
						}
						stockTransferItemslocalObj.setItemTotal((stockTransferItemslocalObj.getUnitRate() != null
								? stockTransferItemslocalObj.getUnitRate()
								: 0)
								* (stockTransferItemslocalObj.getReceivedQty() != null
										? stockTransferItemslocalObj.getReceivedQty()
										: 0));
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getReceivedQty());
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {
							double tax = stockTransferItemslocalObj.getTaxPercent();
							double taxV = (total * tax) / 100;
							log.info("tax value----" + taxV);
							stockTransferItemslocalObj.setTaxValue(taxV);
							double taxValue = roundOffTaxValue(taxV);
							stockTransferItemslocalObj.setTaxValue(taxValue);
							stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							
							if (stockTransfer.getFromEntityMaster() != null
									&& !stockTransfer.getFromEntityMaster().getIsLocalState()) {
								loadProductVerietyTaxPercentage();
							}

							stockTransferItemslocalObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									+ stockTransferItemslocalObj.getTaxValue());
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return PREVIEW_SOCIETY_STOCK_ACK_URL;
	}

	public String deleteConfirmation() {
		log.info("starts deleteConfirmation");
		try {
			pageaction = "DELETE";
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlg1').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("ends deleteConfirmation");
		return null;
	}

	public String deleteSocietyStockInward() {
		log.info("starts deleteSocietyStockInward");
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/delete/"
				+ stockTransferResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(URL);
			if (baseDTO != null && baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				addButtonFlag = false;
				log.info("ends deleteSocietyStockOutward");
				return stockAcknowledgementBeanListPage();
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}
			RequestContext context = RequestContext.getCurrentInstance();
			// context.execute("PF('statusDialog').hide();");
			context.execute("PF('dlg1').hide();");
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		log.info("ends deleteSocietyStockInward");
		return null;
	}

	public String editSocietyStockAckInward() {
		log.info("start viewSocietyStockMovement:");
		stockTransfer = new StockTransfer();
		try {

			redirectSVInProgressPage();

			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			if (stockTransferResponse == null || stockTransferResponse.getStatus() == null
					|| stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

			if (stockTransfer != null) {
				stockTransferItemsMovementList = new ArrayList<>();
				stockTransferResponse = new StockTransferResponse();
				invoiceNumber = (stockTransfer.getPurchaseInvoiceNumber() != null
						? Integer.parseInt(stockTransfer.getPurchaseInvoiceNumber())
						: null);
				invoiceDate = stockTransfer.getInvoiceDate();
				selectedTransportTypeMaster = stockTransfer.getTransportMaster() != null
						? stockTransfer.getTransportMaster().getTransportTypeMaster()
						: null;

				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {

						// StockTransferItems stockTransferItemsObj = new StockTransferItems();
						stockTransferItemslocalObj
								.setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster());

						if (stockTransferItemslocalObj.getUomMaster() != null) {
							stockTransferItemslocalObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							Util.addWarn("UOM is Empty");
						}
						stockTransferItemslocalObj.setItemTotal((stockTransferItemslocalObj.getUnitRate() != null
								? stockTransferItemslocalObj.getUnitRate()
								: 0)
								* (stockTransferItemslocalObj.getReceivedQty() != null
										? stockTransferItemslocalObj.getReceivedQty()
										: 0));
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getReceivedQty());
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {
							double tax = stockTransferItemslocalObj.getTaxPercent();
							double taxV = (total * tax) / 100;
							log.info("tax value----" + taxV);
							stockTransferItemslocalObj.setTaxValue(taxV);
							double taxValue = roundOffTaxValue(taxV);
							stockTransferItemslocalObj.setTaxValue(taxValue);
							stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							loadProductVerietyTaxPercentage();
							stockTransferItemslocalObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									+ stockTransferItemslocalObj.getTaxValue());
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return ADD_PAGE_URL;
	}

	public String dateFormateChanged(Date date) {
		String returnDate = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			if (date != null) {
				returnDate = simpleDateFormat.format(date);
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return returnDate;
	}
}
