package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionerType;
import in.gov.cooptex.core.accounts.dto.GlAccountDTO;
import in.gov.cooptex.core.accounts.dto.GlAccountRequest;
import in.gov.cooptex.core.accounts.dto.GlAccountResponse;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.accounts.model.GlAccountGroup;
import in.gov.cooptex.core.accounts.model.GlAccountType;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("glAccountHeadMasterBean")
@Scope("session")
public class GlAccountHeadMasterBean implements Serializable {
	private static final long serialVersionUID = 5456465L;
	private static final String ADD_PAGE = "/pages/masters/createAccountHeadMaster.xhtml?faces-redirect=true;";
	private static final String LIST_PAGE = "/pages/masters/listAccountHeadMaster.xhtml?faces-redirect=true;";
	private static final String VIEW_PAGE = "/pages/masters/viewAccountHeadMaster.xhtml?faces-redirect=true;";

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	private GlAccountCategory glAccountCategory;

	@Getter
	@Setter
	private List<GlAccountCategory> listGlAccountCategory;

	@Getter
	@Setter
	private GlAccountGroup glAccountGroup;

	@Getter
	@Setter
	private List<GlAccountGroup> listGlAccountGroup;

	@Getter
	@Setter
	private String action, serverURL;

	@Getter
	@Setter
	private BaseDTO baseDTO;

	@Getter
	@Setter
	LazyDataModel<GlAccount> glAccountHeadLazyList;

	@Getter
	@Setter
	private GlAccountDTO glAccount;

	@Getter
	@Setter
	LazyDataModel<GlAccountResponse> accountResponseLazyList;

	@Getter
	@Setter
	GlAccountResponse glAccountResponse;

	@Getter
	@Setter
	GlAccountRequest glAccountRequest;

	@Getter
	@Setter
	Integer totalRecords;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Autowired
	AppPreference appPreference;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	private List<GlAccountType> glAccountTypeList;
	
	@Getter
	@Setter
	private GlAccountType glAccountType;

	GlAccountHeadMasterBean() {
		glAccountCategory = new GlAccountCategory();
		glAccountGroup = new GlAccountGroup();
		glAccount = new GlAccountDTO();
		glAccountType = new GlAccountType();
		baseDTO = new BaseDTO();
		loadValues();
	}

	public String getAccountHeadMasterList() {
		log.info("<---GlAccountHeadMasterBean Inside getvarityMasterList() ---> ");
		addButtonFlag = true;
		loadAccountLazyList();
		log.info("<--- Inside getAccountHeadMasterList()  completed ---> ");
		return LIST_PAGE;
	}

	private void loadValues() {
		log.info("---loadValues----");
		try {
			serverURL = AppUtil.getPortalServerURL();
			log.info("<<====== serverURL:: " + serverURL);
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public void loadaccountCategory() {
		log.info("<<==== loadaccountCategory ==>>");
		try {
			String url = serverURL + "/accounts/get/allcategory";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listGlAccountCategory = mapper.readValue(jsonResponse, new TypeReference<List<GlAccountCategory>>() {
			});
		} catch (Exception e) {
			log.error("<<=====  Load account category error====>>" + e);
		}
	}

	public void onchangeAccountCategory() {
		log.info("<<====  onchangeAccountCategory =====>> " + glAccountCategory.getId());
		try {
			// glAccountGroup.setGlAccountCategory(glAccountCategory);
			String url = serverURL + "/accounts/group/getByCategory/" + glAccountCategory.getId();
			// log.info("<<==== glAccountGroup
			// "+glAccountGroup.getGlAccountCategory().getId());
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listGlAccountGroup = mapper.readValue(jsonResponse, new TypeReference<List<GlAccountGroup>>() {
			});
		} catch (Exception e) {
			log.error("<<=====  onchangeAccountCategory error====>>" + e);
		}
	}

	public String showAddForm() {
		glAccountCategory = new GlAccountCategory();
		glAccount = new GlAccountDTO();
		loadValues();
		loadaccountCategory();
		loadAccountTypes();
		action = "Add";
		return ADD_PAGE;
	}

	public String showEditForm() {
		if (glAccountResponse == null || glAccountResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		action = "Edit";
		loadValues();
		loadAccountTypes();
		loadaccountCategory();
		log.info("<<====== glAccountResponse " + glAccountResponse);
		getAccount();
		log.info("<<===== glAccount:::: " + glAccount);
//		log.info("<<====== glAccount.getGroup " + glAccount.getGlAccountGroup());
		if (glAccount.getGlAccountGroup() != null) {
			glAccountGroup = glAccount.getGlAccountGroup();
			getAccountategoryByGroup();
		}
		onchangeAccountCategory();
		glAccountGroup = glAccount.getGlAccountGroup();

		return ADD_PAGE;
	}

	public String showViewForm() {
		if (glAccountResponse == null || glAccountResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		action = "View";
		log.info("<<====== glAccountResponse " + glAccountResponse);
		getAccount();
		if (glAccount.getGlAccountGroup()!=null) {
			glAccountGroup = glAccount.getGlAccountGroup();
		}
		if (glAccount.getGlAccountGroup() != null) {
			getAccountategoryByGroup();	
		}
		return VIEW_PAGE;
	}

	public String submitGlAccountHead() {
		log.info("<<====  submitGlAccountHead STARTS ====>>");
		if (glAccountCategory == null || glAccountCategory.getId() == null) {
			log.error("<<==== glAccountCategory NULL ====>>");
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_SELECT_CATEGORY.getErrorCode());
			return null;
		}
		if (glAccount.getGlAccountGroup() == null || glAccount.getGlAccountGroup().getId() == null) {
			log.error("<<==== getGlAccountGroup NULL ====>>");
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_SELECT_GROUP.getErrorCode());
			return null;
		}
		if (glAccount.getCode() == null || glAccount.getCode().trim().isEmpty()) {
			log.error("<<==== getCode NULL ====>>");
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_CODE_EMPTY.getErrorCode());
			return null;
		}
		if (glAccount.getName() == null || glAccount.getName().trim().isEmpty()) {
			log.error("<<==== getName NULL ====>>");
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (glAccount.getLName() == null || glAccount.getLName().trim().isEmpty()) {
			log.error("<<==== getLName NULL ====>>");
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_LOCAL_NAME_EMPTY.getErrorCode());
			return null;
		}
		glAccount.setGlAccountTypeId(glAccountType != null ? glAccountType.getId() : null);
		glAccount.setGroupId(glAccount.getGlAccountGroup().getId());
		if (action.equalsIgnoreCase("ADD")) {
			glAccount.setActiveStatus(true);
			log.info("<<====  Add value:: " + glAccount);
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/create";
			baseDTO = httpService.post(url, glAccount);
		}
		if (action.equalsIgnoreCase("EDIT")) {
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/update";
			baseDTO = httpService.post(url, glAccount);
		}
		log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("ADD")) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_CREATE_SUCCESS.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_UPDATE_SUCCESS.getErrorCode());
				}
				log.info("Product Variety Master has been Added");
			} else {
				log.error("Status code:" + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}

		log.info("<<====  submitGlAccountHead ENDS ====>>");
		return getAccountHeadMasterList();
	}

	public void getAccount() {
		log.info("<<====== getAccount " + glAccountResponse.getId());
		try {
			glAccount.setId(glAccountResponse.getId());
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/getAccount";
			baseDTO = httpService.post(url, glAccount);

			ObjectMapper objectMapper = new ObjectMapper();
			if (baseDTO != null) {
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				glAccount = objectMapper.readValue(jsonResponse, GlAccountDTO.class);
			}
		} catch (Exception e) {
			log.error("<<==== exception occored to get Account::: " + e);
		}
	}

	public void getAccountategoryByGroup() {
		log.info("<<====== getAccountategory " + glAccount.getGlAccountGroup().getId());
		try {
			String url = serverURL + "/accounts/group/getCategoryByGroup";
			baseDTO = httpService.post(url, glAccount.getGlAccountGroup());
			ObjectMapper objectMapper = new ObjectMapper();
			if (baseDTO != null) {
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				glAccountCategory = objectMapper.readValue(jsonResponse, GlAccountCategory.class);
			}
		} catch (Exception e) {
			log.error("<<==== error in getAccountategoryByGroup " + e);
		}
	}

	public void loadAccountLazyList() {

		log.info("Lazy load == Account Master  ==>>");
		accountResponseLazyList = new LazyDataModel<GlAccountResponse>() {

			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<GlAccountResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<GlAccountResponse> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse, new TypeReference<List<GlAccountResponse>>() {
						});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(GlAccountResponse res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public GlAccountResponse getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<GlAccountResponse> responseList = (List<GlAccountResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (GlAccountResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		glAccountResponse = new GlAccountResponse();

		BaseDTO baseDTO = new BaseDTO();

		GlAccountRequest request = new GlAccountRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("categeoryCodeOrName")) {
				request.setCategoryCodeOrName(value);
			} else if (key.equals("groupCodeOrName")) {
				request.setGroupCodeOrName(value);
			} else if (key.equals("codeOrName")) {
				request.setCodeOrName(value);
			} else if (key.equals("localName")) {
				request.setLocalName(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equalsIgnoreCase("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {
			// String url = serverURL + appPreference.getOperationApiUrl() +
			// "/productvariety/searchvaritys";
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/searchGlAccounts";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Product Varity Master onRowSelect method started");
		glAccountResponse = ((GlAccountResponse) event.getObject());
		addButtonFlag = false;
	}

	public String deleteGlAccountHead() {
		try {
			action = "Delete";
			if (glAccountResponse == null) {
				Util.addWarn("Select Atleast One Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountHeadDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteGlAccountHeadConfirm() {

		try {
			log.info("Delete Gl Account Head started the id is [" + glAccountResponse.getId() + "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/delete";
			baseDTO = httpService.post(url, glAccountResponse);
			getAccountHeadMasterList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Gl Account Head deleted successfully....!!!");
					errorMap.notify(ErrorDescription
							.getError(OperationErrorCode.GL_ACCOUNT_HEAD_DELETED_SUCCESSFULLY)
							.getErrorCode());
					return "";
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	
	public void loadAccountTypes() {
		log.info("<<==== GlAccountHeadMasterBean : loadAccountTypes() ====>>");
		glAccountTypeList = new ArrayList<>();
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/getGlAccountTypes";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			glAccountTypeList = mapper.readValue(jsonResponse, new TypeReference<List<GlAccountType>>() {
			});
		} catch (Exception e) {
			log.error("<<=====  GlAccountHeadMasterBean : loadAccountTypes() error====>>" , e);
		}
	}

}
