package in.gov.cooptex.operation.tender.rest.ui;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.ApproveEmployeeLoanAndAdvanceDto;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.finance.TaBillDTO;
import in.gov.cooptex.operation.dto.ItemConsumptionDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.ProductDesignTargetDTO;
import in.gov.cooptex.operation.production.model.ProductDesignTarget;
import in.gov.cooptex.operation.production.model.ProductDesignTargetDetails;
import in.gov.cooptex.operation.production.model.ProductDesignTargetLog;
import in.gov.cooptex.operation.production.model.ProductDesignTargetNote;
import in.gov.cooptex.personnel.hrms.model.EmpLoanAdvanceSuretyDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("productDesignTargetBean")
@Scope("session")
public class ProductDesignTargetBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1481356617541249476L;

	@Getter
	@Setter
	ProductDesignTarget productDesignTarget;

	@Getter
	@Setter
	ProductDesignTarget selectedFromMonthYear;

	@Getter
	@Setter
	ProductDesignTarget selectedToMonthYear;

	@Getter
	@Setter
	ProductDesignTargetDetails productDesignTargetDetails;

	@Getter
	@Setter
	List<ProductDesignTargetDetails> productDesignTargetDetailsList;

	@Getter
	@Setter
	ProductDesignTargetLog productDesignTargetLog;

	@Getter
	@Setter
	ProductDesignTargetNote productDesignTargetNote;

	@Getter
	@Setter
	List<ProductDesignTarget> fromMonthYearList;

	@Getter
	@Setter
	List<ProductDesignTarget> toMonthYearList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	LazyDataModel<ProductDesignTarget> productDesignTargetLazyList;

	@Getter
	@Setter
	List<ProductDesignTarget> productDesignTargetLogList = null;

	@Getter
	@Setter
	ProductDesignTarget productDesignTargetLazy;

	@Getter
	@Setter
	ProductDesignTarget selectedProductDesignTarget;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	ObjectMapper mapper;

	@Getter
	@Setter
	private Long targetId;

	@Getter
	@Setter
	private Long notificationId;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	String remarks;

	@Getter
	@Setter
	Boolean viewButtonFlag;

	@Getter
	@Setter
	Boolean addButtonFlag;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetNoteList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetDTOList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetDTOGroupList;

	@Getter
	@Setter
	ProductDesignTargetDTO productDesignTargetDTO;

	@Getter
	@Setter
	Double targetQuantity;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	Boolean finalApproval;

	@Getter
	@Setter
	String errorMSGForDelete;

	@Getter
	@Setter
	Boolean finalApprovalUser;

	@Getter
	@Setter
	Boolean finalApprovalTemp;

	@Getter
	@Setter
	Boolean finalApprovalActionButton;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	Date currentDate;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String userName;

	@Getter
	@Setter
	String designationName;

	String jsonResponse;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String note = "";

	@Getter
	@Setter
	String productVarietyCodeName;

	@Getter
	@Setter
	List<EntityMaster> hoentityList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster headOrRegionOffice;

	@Getter
	@Setter
	List<UomMaster> uomMasterList = new ArrayList<>();

	@Getter
	@Setter
	UomMaster uomMaster = new UomMaster();
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	private final String LIST_PAGE_URL = "/pages/operation/listProductDevelopmentAndDesign.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE_URL = "/pages/operation/createProductDevelopmentAndDesign.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/operation/viewProductDevelopmentAndDesign.xhtml?faces-redirect=true;";
	private final String EDIT_URL = "/pages/operation/editProductDevelopmentAndDesign.xhtml?faces-redirect=true;";

	public void loadMontAndYears() {
		log.info("<=ProductDesignTargetBean :: loadMontAndYears Start=>");
		try {
			editButtonFlag = false;
			viewButtonFlag = false;
			addButtonFlag = true;
			currentDate = new Date();
			lazySocietyRegistrationList();
		} catch (Exception se) {
			log.error("Error to load month andyear " + se);
		}
	}

	public String loadProductDevelopmentDesignPage() {
		log.info("<=ProductDesignTargetBean :: loadProductDevelopmentDesignPage Start=>");
		try {
			loadMontAndYears();
			loadProductCategoryGroup();
		} catch (Exception e) {
			log.error("loadProductDevelopmentDesignPage :: Exception==> ", e);
		}
		return LIST_PAGE_URL;

	}

	public void loadToMontAndYears() {
		log.info("receive load To month and years");
		try {
			log.info("selectedFromToYear " + selectedFromMonthYear.getFromMonth() + ","
					+ selectedFromMonthYear.getFromYear());
			toMonthYearList = new ArrayList<ProductDesignTarget>();
			Calendar fromCalendar = Calendar.getInstance();
			Calendar toCalendar = Calendar.getInstance();
			String selectedMonthYear = selectedFromMonthYear.getFromMonth() + "-" + selectedFromMonthYear.getFromYear();
			log.info("receive To Month Year " + selectedMonthYear);
			SimpleDateFormat selectedMonth = new SimpleDateFormat("MMM-yyyy");
			Date dd = selectedMonth.parse(selectedMonthYear);
			log.info("Convert SelectedMonth success");
			fromCalendar.setTime(dd);
			toCalendar.setTime(dd);
			toCalendar.add(Calendar.YEAR, 1);
			SimpleDateFormat month = new SimpleDateFormat("MMM");
			SimpleDateFormat year = new SimpleDateFormat("yyyy");
			while (toCalendar.getTime().after(fromCalendar.getTime())) {
				ProductDesignTarget target = new ProductDesignTarget();

				target.setToMonth(month.format(fromCalendar.getTime()));
				target.setToYear(Long.valueOf(year.format(fromCalendar.getTime())));
				log.info("Mont add " + month.format(fromCalendar.getTime()) + " - "
						+ year.format(fromCalendar.getTime()));
				fromCalendar.add(Calendar.MONTH, 1);
				toMonthYearList.add(target);
			}
		} catch (Exception se) {
			log.error("Error to load month andyear " + se);
		}
	}

	public void clearProductDesign() {
		log.info(":::<- ProductDesignTargetBean :: clearProductDesign start ->::: ");
		productDesignTargetDTOList = new ArrayList<>();
		productDesignTargetDTOGroupList = new ArrayList<>();
		targetQuantity = null;
		productVarietyCodeName = null;
		productGroupMaster = new ProductGroupMaster();
		productCategory = new ProductCategory();
		productCategoryGroup = new ProductCategoryGroup();
		entityMaster = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		selectedToMonthYear = new ProductDesignTarget();
		selectedFromMonthYear = new ProductDesignTarget();
		productDesignTarget = new ProductDesignTarget();
		headOrRegionOffice = new EntityMaster();
	}

	// Entity Wise Product Details
	// Entity Type Master ->1
	public void loadEntityTypeMaster() {
		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");

		try {
			log.info("selectedFromToYear " + selectedToMonthYear.getToMonth() + "," + selectedToMonthYear.getToYear());
			entityTypeMasterList = new ArrayList<EntityTypeMaster>();
			entityMasterList = new ArrayList<EntityMaster>();
			/*
			 * productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
			 * productCategoryList = new ArrayList<ProductCategory>();
			 * productGroupMasterList = new ArrayList<ProductGroupMaster>();
			 * productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			 */
			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getActiveEntityTypeMasterList";
			log.info("loadEntityTypeMaster :: url ==> " + url);
			baseDTO = httpService.get(url);
			log.info("::: get loadEntityTypeMaster MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});
				if (entityTypeMasterList != null && !entityTypeMasterList.isEmpty()) {
					log.info("entityTypeMasterList size==> " + entityTypeMasterList.size());
				}
			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	// Entity Master ->
	public void loadEntityMasterList() {

		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			if (entityTypeMaster != null && !entityTypeMaster.getEntityCode().equals("SHOW_ROOM")) {
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/testingLabStockInward/getSelectedEntityList";
				baseDTO = httpService.post(url, entityTypeMaster);
				log.info("::: get loadEntityTypeMaster MasterList Response :");
				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});

				} else {
					log.warn("loadEntityTypeMaster MasterList Error ");
				}
			} else if (entityTypeMaster.getEntityCode().equals("SHOW_ROOM")) {
				entityMasterList = null;
				loadAllHOEntityList();
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	public void loadAllEntityByRegionList() {
		log.info("... Load  Entity List by region called ....");
		try {

			if (headOrRegionOffice != null && headOrRegionOffice.getId() != null)

				log.info("loadAllEntityByRegionList :: region id==> " + headOrRegionOffice.getId());

			String url = SERVER_URL + "/entitymaster/getall/getmanualAttenRegion/" + headOrRegionOffice.getId() + "/"
					+ entityTypeMaster.getId();
			log.info("loadAllEntityByRegionList :: url==> " + url);
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});

		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllHOEntityList() {
		log.info("... Load  HO/RO List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitymaster/getall/headandregionalOffice");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			hoentityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading HO/RO data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	// product category Group ->1
	public void loadProductCategoryGroup() {
		log.info("Received productCategoryGroupList ");
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductCategoryGroupList";
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
			} else {
				log.warn("productCategoryGroupList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryGroupList load ", ee);
		}
	}

	// load Product Category Code / Name
	public void productCategoryListLoad() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getAllProductCategoryCodeName";
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllProductCategoryList/"
					+ productCategoryGroup.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productCategoryList :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("productCategoryList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
			} else {
				log.warn("productCategoryList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	// product category -> 3
	public void getAllProductGroupMasterList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		log.info("Received productGroupMasterList ID :" + productCategory.getId());
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/" + productCategory.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productGroupMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("productGroupMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
			} else {
				log.warn("productGroupMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productGroupMasterList load ", ee);
		}
	}

	// product category -> 4productVarietyMasterList
	public void listenerProductVarietyMasterList() {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		log.info("Received productVarietyMasterList ID :" + productGroupMaster.getId());
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProVarietyMasterList/" + productGroupMaster.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("productVarietyMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
	}

	// save data
	public String saveProductTargetDesign() {
		log.info("Received saveProductTargetDesign ");
		try {
			if (forwardTo == null) {
				errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDTO_REQUIRED.getErrorCode());
				return null;
			}
			if (productDesignTargetDTOList == null || productDesignTargetDTOList.size() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_TABLE_EMPTY.getErrorCode());
				return null;
			}
			if (finalApproval == null) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_TARGET_FINAL_APPROVE_REQUIRED.getErrorCode());
				return null;
			}

			if (productDesignTargetNote.getNote() == null || productDesignTargetNote.getNote().trim().length() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}
			BaseDTO baseDTO = null;
			productDesignTargetDTO = new ProductDesignTargetDTO();
			productDesignTargetDetails = new ProductDesignTargetDetails();

			productDesignTargetLog = new ProductDesignTargetLog();

			productDesignTarget.setFromMonth(selectedFromMonthYear.getFromMonth());
			productDesignTarget.setFromYear(selectedFromMonthYear.getFromYear());
			productDesignTarget.setToMonth(selectedToMonthYear.getToMonth());
			productDesignTarget.setToYear(selectedToMonthYear.getToYear());
			productDesignTarget.setActiveStatus(true);

			log.info("selectedFromMonthYear " + selectedFromMonthYear.getFromMonth() + ","
					+ selectedFromMonthYear.getFromYear());
			log.info("selectedFromToYear " + selectedToMonthYear.getToMonth() + "," + selectedToMonthYear.getToYear());
			productCategory.setProductCategoryGroup(productCategoryGroup);
			productGroupMaster.setProductCategory(productCategory);
			productVarietyMaster.setProductGroupMaster(productGroupMaster);
			entityMaster.setEntityTypeMaster(entityTypeMaster);

			productDesignTargetDetails.setEntityMaster(entityMaster);
			productDesignTargetDetails.setProductVarietyMaster(productVarietyMaster);

			productDesignTargetNote.setUserMaster(forwardTo);
			productDesignTargetNote.setFinalApproval(finalApproval);

			productDesignTargetDTO.setProductDesignTarget(productDesignTarget);
			productDesignTargetDTO.setProductDesignTargetDetails(productDesignTargetDetails);
			productDesignTargetDTO.setProductDesignTargetDTOList(productDesignTargetDTOList);
			productDesignTargetDTO.setProductDesignTargetNote(productDesignTargetNote);
			productDesignTargetDTO.setProductDesignTargetLog(productDesignTargetLog);

			/*
			 * ObjectMapper obj = new ObjectMapper(); String json =
			 * obj.writeValueAsString(productDesignTargetDTO);
			 */

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/saveProducttDesignTarget";
			baseDTO = httpService.post(url, productDesignTargetDTO);
			log.info("::: Retrieved saveProductTargetDesign :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("SaveProductTargetDesign Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_SAVE_SUCCESS.getErrorCode());
				return LIST_PAGE_URL;
			} else {
				log.warn("SaveProductTargetDesign Error *** :");
				if (baseDTO.getStatusCode() == 5013) {
					errorMap.notify(ErrorDescription.ERROR_PRODUCT_VARIETY_CODE.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription.PRODUCT_DESIGN_SAVE_FAILURE.getErrorCode());
				}
			}
		} catch (DataIntegrityViolationException e) {
			errorMap.notify(ErrorDescription.ERROR_PRODUCT_VARIETY_CODE.getErrorCode());
		} catch (Exception ee) {
			String dataIntegretyException = ee.getMessage();
			if (dataIntegretyException.equals("500 null")) {
				AppUtil.addWarn("Product Code Already Exist ");
				log.error("Exception Occured in SaveProductTargetDesign While Saving Product Code");
			} else {
				log.error("Exception Occured in SaveProductTargetDesign", ee);
			}
		}
		return null;
	}

	// Approved
	public String approvedDesignTarget() {
		try {
			BaseDTO baseDTO = null;
			if (finalApproval == null) {
				log.info("Invalid FinalApproval");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_FINAL_APPROVE.getErrorCode());
				return null;
			}
			if (forwardTo == null) {
				log.info("Invalid forwardTo");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_FORWARD_ALREADY_EXIST.getErrorCode());
				return null;
			}
			if (forwardTo.getId() == loginBean.getUserMaster().getId()
					&& (finalApproval == null || finalApproval == false)) {
				log.info("Approved ItSelf Rejected -> Invalid ForwardTo User");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_FORWARD_ALREADY_EXIST.getErrorCode());
				return null;
			}
			if (remarks == null || remarks.length() == 0) {
				log.info("Note Not Valid");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}
			if (remarks != null) {
				productDesignTargetNote.setNote(remarks);
			}

			productDesignTargetNote.setUserMaster(forwardTo);
			productDesignTargetNote.setFinalApproval(finalApproval);

			log.info("productDesignTargetNote :" + productDesignTargetNote.getNote());
			log.info("productDesignTargetNote USer :" + productDesignTargetNote.getUserMaster().getUsername());

			productDesignTargetLog.setStage("SUBMITTED");
			if (finalApprovalUser == true && finalApproval == true) {
				productDesignTargetLog.setStage("FINAL_APPROVED");
			} else if (finalApprovalUser == false && finalApproval == true) {
				productDesignTargetLog.setStage("APPROVED");
			}

			productDesignTargetDTO.setProductDesignTargetNote(productDesignTargetNote);
			productDesignTargetDTO.setProductDesignTargetLog(productDesignTargetLog);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/updateProducttDesignTarget";
			baseDTO = httpService.post(url, productDesignTargetDTO);
			log.info("::: Retrieved updateProductTargetDesign :");
			if (baseDTO.getStatusCode() == 0) {
				
				log.info("Update ProductTargetDesign Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_APPROVE_SUCCESS.getErrorCode());
				editButtonFlag = false;
				viewButtonFlag = false;
				addButtonFlag = true;
				selectedProductDesignTarget = null;
				return LIST_PAGE_URL;
			} else {
				log.warn("SaveProductTargetDesign Error *** :");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_APPROVE_FAILED.getErrorCode());
			}
		} catch (Exception ee) {
			log.error("Exception Occured in approvedDesignTarget", ee);
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_APPROVE_FAILED.getErrorCode());
		}
		return null;
	}

	// Rejected
	public String rejectedDesignTarget() {
		try {
			BaseDTO baseDTO = null;
			if (finalApproval == null) {
				log.info("Invalid FinalApproval");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_FINAL_APPROVE.getErrorCode());
				return null;
			}
			if (forwardTo == null) {
				log.info("Invalid forwardTo");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_FORWARD_ALREADY_EXIST.getErrorCode());
				return null;
			}
			if (forwardTo.getId() == loginBean.getUserMaster().getId()
					&& (finalApproval == null || finalApproval == false)) {
				log.info("Approved ItSelf Rejected -> Invalid ForwardTo User");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_FORWARD_ALREADY_EXIST.getErrorCode());
				return null;
			}
			if (finalApprovalUser == false && finalApproval == true) {
				log.info("Approved Rejected Before asigned ->");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_FINAL_APPROVE.getErrorCode());
				return null;
			}

			if (remarks == null || remarks.length() == 0) {
				log.info("Note Not Valid");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}

			if (note == null || note.trim().length() == 0) {
				log.info("Note Not Valid");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}

			if (remarks != null) {
				productDesignTargetNote.setNote(remarks);
			}
			log.info("productDesignTargetNote :" + productDesignTargetNote.getNote());
			log.info("productDesignTargetNote USer :" + productDesignTargetNote.getUserMaster().getUsername());

			productDesignTargetNote.setUserMaster(forwardTo);
			productDesignTargetNote.setFinalApproval(finalApproval);
			productDesignTargetLog.setStage("REJECTED");
			//productDesignTargetNote.setNote(note);

			productDesignTargetDTO.setProductDesignTargetNote(productDesignTargetNote);
			productDesignTargetDTO.setProductDesignTargetLog(productDesignTargetLog);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/updateProducttDesignTarget";
			baseDTO = httpService.post(url, productDesignTargetDTO);
			log.info("::: Retrieved updateProductTargetDesign :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Update ProductTargetDesign Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_REJECTED_SUCCESS.getErrorCode());
				editButtonFlag = false;
				viewButtonFlag = false;
				addButtonFlag = true;
				selectedProductDesignTarget = null;
				return LIST_PAGE_URL;
			} else {
				log.warn("SaveProductTargetDesign Error *** :");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_REJECTED_FAILED.getErrorCode());
			}
		} catch (Exception ee) {
			log.error("Exception Occured in approvedDesignTarget", ee);
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_REJECTED_FAILED.getErrorCode());
		}
		return null;
	}

	public String updateDesignTarget() {
		log.info("::: receive updateProductTargetDesign :");
		try {
			BaseDTO baseDTO = null;

			productDesignTargetNote.setUserMaster(forwardTo);
			productDesignTargetNote.setFinalApproval(finalApproval);

			log.info("productDesignTargetNote :" + productDesignTargetNote.getNote());
			log.info("productDesignTargetNote USer :" + productDesignTargetNote.getUserMaster().getUsername());

			productDesignTargetDTO.setProductDesignTargetNote(productDesignTargetNote);
			productDesignTargetDTO.setProductDesignTargetLog(productDesignTargetLog);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/updateProducttDesignTarget";
			baseDTO = httpService.post(url, productDesignTargetDTO);
			log.info("::: Retrieved updateProductTargetDesign :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Update ProductTargetDesign Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_SAVE_SUCCESS.getErrorCode());
				return LIST_PAGE_URL;
			} else {
				log.warn("SaveProductTargetDesign Error *** :");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_SAVE_FAILURE.getErrorCode());
			}
		} catch (Exception ee) {
			log.error("Exception Occured in updateDesignTarget", ee);
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_SAVE_FAILURE.getErrorCode());
		}
		return null;
	}

	public void resetData() {
		log.info("Reset Save Received ");
		forwardTo = null;
		productDesignTarget = new ProductDesignTarget();
		selectedFromMonthYear = null;
		selectedToMonthYear = null;
		selectedFromMonthYear = null;
		entityTypeMaster = null;
		entityMaster = null;
		productCategoryGroup = null;
		productCategory = null;
		productGroupMaster = null;
		productVarietyMaster = null;
		productDesignTargetDTOList = new ArrayList<>();
		uomMaster = null;
		targetQuantity = null;

		finalApproval = null;
		productDesignTargetNote = new ProductDesignTargetNote();
		headOrRegionOffice = null;
		log.info("Reset Save Done");
	}

	public String addProductDesign() {
		log.info("ProductDesign Add Received ");
		try {

			/*
			 * if (productVarietyCodeName == null || productVarietyCodeName.isEmpty()) {
			 * errorMap.notify(
			 * ErrorDescription.getError(OperationErrorCode.PRODCUT_VARIETY_SHOULD_NOT_EMPTY
			 * ).getCode()); return null; }
			 */
			if (targetQuantity == null || targetQuantity == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_TARGET_QUANTITY_REQUIRED.getErrorCode());
				return null;
			}
			/*
			 * if (!productVarietyCodeName.contains("/")) { AppUtil.
			 * addError("Please enter product variety Code/Name (eg:product CODE/product NAME)"
			 * ); return null; }
			 */
			if (uomMaster == null) {
				AppUtil.addError("Uom should not be empty");
				return null;
			}
			ProductDesignTargetDTO productDTO = new ProductDesignTargetDTO();
			productDesignTargetDetails = new ProductDesignTargetDetails();
			productCategory.setProductCategoryGroup(productCategoryGroup);

			productGroupMaster.setProductCategory(productCategory);

			// productVarietyMaster.setProductGroupMaster(productGroupMaster);

			entityMaster.setEntityTypeMaster(entityTypeMaster);

			productDTO.setEntityTypeMaster(entityTypeMaster);
			productDTO.setEntityMaster(entityMaster);
			productDTO.setProductCategoryGroup(productCategoryGroup);
			productDTO.setProductCategory(productCategory);
			productDTO.setProductGroupMaster(productGroupMaster);
			productDTO.setUomMaster(uomMaster);
			productDTO.setProductVarietyMaster(productVarietyMaster);
			// productDTO.setProductVarietyMaster(productVarietyMaster);
			productDTO.setTargetQuantity(targetQuantity);

			productDesignTargetDTOList.add(productDTO);
			// errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ITEM_ADD_SUCCESS.getErrorCode());
			groupingProductDevelopmentAndDesign();

			log.info("ProductDesign Add Done");

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ITEM_ADD_FAILURE.getErrorCode());
			log.error("ProductDesign Add Error " + e);
		}
		return null;

	}

	public Double getSumTargetQuantity() {
		log.info("received getSumTargetQuantity");
		try {
			double quantity = 0.0;
			for (ProductDesignTargetDTO productDTO : productDesignTargetDTOList) {
				quantity += productDTO.getTargetQuantity();
			}

			return quantity;
		} catch (Exception e) {
			log.error("getSumTargetQuantity Add Error " + e);
		}
		return 0.0;
	}

	public void groupingProductDevelopmentAndDesign() {
		try {
			productDesignTargetDTOGroupList = new ArrayList<ProductDesignTargetDTO>();
			java.util.Map<EntityMaster, java.util.Map<ProductCategory, Double>> designationGroupByAvg = productDesignTargetDTOList
					.stream()
					.collect(Collectors.groupingBy(ProductDesignTargetDTO::getEntityMaster,
							Collectors.groupingBy(ProductDesignTargetDTO::getProductCategory,
									Collectors.summingDouble(ProductDesignTargetDTO::getTargetQuantity))));
			designationGroupByAvg.forEach((designation, values) -> {
				values.forEach((gender, avg) -> {
					ProductDesignTargetDTO dto = new ProductDesignTargetDTO();
					dto.setEntityMaster(designation);
					log.info("MAP LIST :" + gender.getProductCatCode() + "," + gender.getProductCatName() + ":" + avg);
					dto.setProductCategory(gender);
					dto.setTargetQuantity(avg);
					productDesignTargetDTOGroupList.add(dto);
				});
				log.info("After grouping Function");
			});
			log.info("Done grouping");
		} catch (Exception se) {
			log.warn("grouping error " + se);
		}

	}

	public void noteAction() {
		log.info("Create Note Action received ");
		if (productDesignTargetNote.getNote() == null || productDesignTargetNote.getNote().trim().length() == 0) {
			log.warn("Product Design Target Note Valid");
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_CREATE_NOTE_EMPTY.getErrorCode());
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('notedialog').hide();");
			// errorMap.notify(ErrorDescription.PRODUCT_DESIGN_CREATE_NOTE_SUCCESS.getErrorCode());
		}
		log.info("value is " + productDesignTargetNote.getNote());

	}

	public String showListPage() {
		try {
			editButtonFlag = false;
			viewButtonFlag = false;
			addButtonFlag = true;
			selectedProductDesignTarget = null;
			return LIST_PAGE_URL;
		} catch (Exception se) {
			log.error("ProductDesign Target error showListPage()" + se);
		}
		return null;
	}

	public String createTargetDesign() {
		resetData();

		try {
			fromMonthYearList = new ArrayList<ProductDesignTarget>();
			toMonthYearList = new ArrayList<ProductDesignTarget>();
			uomMasterList = new ArrayList<UomMaster>();
			productDesignTargetDTOList = new ArrayList<ProductDesignTargetDTO>();
			productDesignTarget = new ProductDesignTarget();
			productDesignTargetNote = new ProductDesignTargetNote();
			productDesignTargetDetailsList = new ArrayList<ProductDesignTargetDetails>();
			productDesignTargetDTOGroupList = new ArrayList<ProductDesignTargetDTO>();
			productVarietyMasterList = new ArrayList<>();
			editButtonFlag = false;
			addButtonFlag = true;
			uomMaster = new UomMaster();
			currentDate = new Date();
			productVarietyCodeName = null;
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(" ");
			uomMasterList = commonDataService.loadUOMMaster();
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			Calendar fromCalendar = Calendar.getInstance();
			Calendar toCalendar = Calendar.getInstance();
			fromCalendar.setTime(new Date());
			toCalendar.add(Calendar.YEAR, 1);
			SimpleDateFormat month = new SimpleDateFormat("MMM");
			SimpleDateFormat year = new SimpleDateFormat("yyyy");
			while (toCalendar.getTime().after(fromCalendar.getTime())) {
				ProductDesignTarget target = new ProductDesignTarget();
				target.setFromMonth(month.format(fromCalendar.getTime()));
				target.setFromYear(Long.valueOf(year.format(fromCalendar.getTime())));

				log.info("Mont add " + month.format(fromCalendar.getTime()) + " - "
						+ year.format(fromCalendar.getTime()));
				fromCalendar.add(Calendar.MONTH, 1);
				fromMonthYearList.add(target);
			}
		} catch (Exception se) {
			log.error("Error to load month andyear " + se);
		}
		try {
			log.info("load user details");
			userName = loginBean.getUsername();
			designationName = loginBean.getEmployee().getPersonalInfoEmployment().getCurrentDesignation().getName();
			log.info("load user details Success");
		} catch (Exception e) {
			log.error("load user details error " + e);

		}
		return CREATE_PAGE_URL;
	}

	public String editTargetDesign() {
		viewProductDetailsTargetDetailsList();
		return EDIT_URL;
	}

	public String viewTargetDesign() {
		viewProductDetailsTargetDetailsList();
		return VIEW_URL;
	}

	public String clearTargetDesign() {
		editButtonFlag = false;
		viewButtonFlag = false;
		addButtonFlag = true;
		selectedProductDesignTarget = null;
		return LIST_PAGE_URL;
	}

	public void onRowSelect(SelectEvent event) {
		editButtonFlag = false;
		finalApprovalTemp = false;
		finalApprovalActionButton = false;
		try {
			ProductDesignTarget target = (ProductDesignTarget) event.getObject();
			log.info("Received onRowSelect " + target.getId());
			if (target != null) {

				log.info("Processed 2: " + loginBean.getUserMaster().getId() + "," + target.getForwardTO());
				addButtonFlag = false;
				viewButtonFlag = true;
				if (loginBean.getUserMaster().getId().equals(target.getForwardTO())
						&& (target.getStage().equalsIgnoreCase("SUBMITTED")
								|| target.getStage().equalsIgnoreCase("APPROVED"))) {
					log.info("Edit enabled");
					editButtonFlag = true;
					finalApprovalTemp = true;
					finalApprovalActionButton = true;
				}
			}
			if (finalApprovalUser == true) {
				finalApprovalTemp = false;
			}

		} catch (Exception see) {
			log.error("Product Target Item selection Exception " + see);
		}
	}

	// view Product_Design_Target DetailsList
	public void viewProductDetailsTargetDetailsList() {
		log.info("Received productGroupMasterList ");
		forwardToUsersList = new ArrayList<>();
		forwardToUsersList = commonDataService.loadForwardToUsers();
		// forwardToUsersList =
		// commonDataService.loadForwardToUsersByFeature("ProductDesign");

		productDesignTargetDTO = new ProductDesignTargetDTO();
		productDesignTargetDTOList = new ArrayList<ProductDesignTargetDTO>();
		productDesignTargetNote = new ProductDesignTargetNote();
		productDesignTargetLog = new ProductDesignTargetLog();
		forwardTo = new UserMaster();
		finalApproval = false;
		finalApprovalUser = false;
		log.info("Received productDesignTargetDetailsList ID :" + selectedProductDesignTarget.getId());
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/viewProductDetailsTargetDetailsList";
			baseDTO = httpService.post(url, selectedProductDesignTarget);
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productDesignTargetDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignTargetDTO>>() {
						});

				if (productDesignTargetDTOList.size() > 0) {
					jsonResponse = mapper.writeValueAsString(productDesignTargetDTOList.get(0));
					productDesignTargetDTO = mapper.readValue(jsonResponse,
							new TypeReference<ProductDesignTargetDTO>() {
							});
				}
				
				systemNotificationBean.loadTotalMessages();

				forwardTo = productDesignTargetDTO.getUserMaster();

				jsonResponse = mapper.writeValueAsString(productDesignTargetDTO.getProductDesignTarget());
				productDesignTarget = mapper.readValue(jsonResponse, new TypeReference<ProductDesignTarget>() {
				});

				log.info("DesignTarget set Success");

				jsonResponse = mapper.writeValueAsString(productDesignTargetDTO.getProductDesignTargetLog());
				productDesignTargetLog = mapper.readValue(jsonResponse, new TypeReference<ProductDesignTargetLog>() {
				});

				log.info("DesignTarget LOG set Success");
				productDesignTargetNote = productDesignTargetDTO.getProductDesignTargetNote();

				finalApproval = productDesignTargetNote.getFinalApproval();
				finalApprovalUser = productDesignTargetNote.getFinalApproval();

				note = productDesignTargetNote.getNote();

				log.info("DesignTarget NOTE ID " + productDesignTargetNote.getId());
				productDesignTargetLog.setProductDesignTarget(productDesignTarget);
				productDesignTargetNote.setProductDesignTarget(productDesignTarget);
				productDesignTargetNote.setProductDesignTargetNote(productDesignTargetNote); // set parent

				if (loginBean.getUserMaster().getId().equals(productDesignTargetNote.getUserMaster().getId())
						&& (productDesignTargetLog.getStage().equalsIgnoreCase("SUBMITTED"))) {
					editButtonFlag = true;
					finalApprovalTemp = true;
					finalApprovalActionButton = true;
				} else if (loginBean.getUserMaster().getId().equals(productDesignTargetNote.getUserMaster().getId())
						&& (productDesignTargetLog.getStage().equalsIgnoreCase("APPROVED"))) {

					editButtonFlag = true;
					finalApprovalTemp = false;
					finalApprovalActionButton = true;
				} else {
					editButtonFlag = false;
					finalApprovalTemp = false;
					finalApprovalActionButton = false;
				}
				groupingProductDevelopmentAndDesign();
				loadproductDesignTargetNoteList();

			} else {
				log.warn("productDesignTargetDetails Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productView TargetDesign load:", ee);
		}
	}

	// lazy new view
	public void lazySocietyRegistrationList() {
		log.info("<===== Starts SocietyBoardApprovalBean.lazySocietyRegistrationList List ======>");
		String lazyType = "CODE_ALLOTTED";
		String lazylistUrl = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/productdesigntarget/getallProductDesignTargetlazy/" + lazyType;
		mapper = new ObjectMapper();
		productDesignTargetLogList = new ArrayList<>();
		productDesignTargetLazyList = new LazyDataModel<ProductDesignTarget>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1553357030678469897L;

			@Override
			public List<ProductDesignTarget> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for lazySocietyRegistrationList List=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						productDesignTargetLogList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductDesignTarget>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = productDesignTargetLogList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazySocietyRegistrationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + productDesignTargetLogList.size());
				return productDesignTargetLogList;
			}

			@Override
			public Object getRowKey(ProductDesignTarget res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductDesignTarget getRowData(String rowKey) {
				try {
					for (ProductDesignTarget societyRegReqLog : productDesignTargetLogList) {
						if (societyRegReqLog.getId().equals(Long.valueOf(rowKey))) {
							productDesignTargetLazy = societyRegReqLog;
							return societyRegReqLog;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.lazySocietyRegistrationList List ======>");
	}

	// Delete Design Target Stage Check before Delete
	public String deleteStageVerificationDesignTarget() {
		log.info(":::<- Delete ProductDesignTarget Called->:::" + selectedProductDesignTarget.getId());
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesigntarget/deleteTargetDesignStageVerification/" + selectedProductDesignTarget.getId();
			baseDTO = httpService.get(url);

			log.info("::: Delete ProductDesignTargetResponse :");
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.warn("Delete Verification Success delete Start");
				deleteDesignTarget();
			} else if (baseDTO != null && baseDTO.getStatusCode() == 66677) {
				log.warn("DeleteDesignTarget  Error code 66677");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDeleteSecondStage').show();");
			} else {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_DELETE_FAILED.getErrorCode());
				log.warn("deleteDesignTarget  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in deleteItemConsumption", ee);
		}
		return null;
	}

	// Delete Stage Confirm
	public String deleteDesignTarget() {
		log.info(":::<- Delete ProductDesignTarget Called->:::" + selectedProductDesignTarget.getId());
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/productdesigntarget/deleteTargetDesign/"
					+ selectedProductDesignTarget.getId();
			baseDTO = httpService.get(url);

			log.info("::: delete ProductDesignTargetResponse :");
			if (baseDTO.getStatusCode() == 0) {
				editButtonFlag = false;
				viewButtonFlag = false;
				addButtonFlag = true;
				selectedProductDesignTarget = null;
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_DELETE_SUCCESS.getErrorCode());
				return LIST_PAGE_URL;
			} else {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_DELETE_FAILED.getErrorCode());
				log.warn("deleteDesignTarget  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in deleteItemConsumption", ee);
		}
		return null;
	}

	public void loadproductDesignTargetNoteList() {
		log.info("Received productDesignTargetNoteList ");
		productDesignTargetNoteList = new ArrayList<ProductDesignTargetDTO>();

		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/productdesigntarget/getCreatedByNoteLog/"
					+ selectedProductDesignTarget.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productDesignTargetNoteList :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("productDesignTargetNoteList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productDesignTargetNoteList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignTargetDTO>>() {
						});
				log.info("Size of productDesignTargetNoteList: " + productDesignTargetNoteList.size());
			} else {
				log.warn("productDesignTargetNoteList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productDesignTargetNoteList load ", ee);
		}
	}

	// Entity Master AutoComplete
	public List<EntityMaster> entityAutoComplete(String entityName) {
		List<EntityMaster> entityList = new ArrayList<>();
		if (entityName != null && entityMasterList != null) {
			log.info("entityAutoComplete entityName-------------" + entityName);
			for (EntityMaster entity : entityMasterList) {
				if (entity.getName().contains(entityName.toUpperCase())
						|| String.valueOf(entity.getCode()).contains(entityName)) {
					entity.setLname(entity.getCode() + "/" + entity.getName());
					entityList.add(entity);
				}
			}
		}
		return entityList;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String targetId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			targetId = httpRequest.getParameter("targetId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		selectedProductDesignTarget = new ProductDesignTarget();
		if (!StringUtils.isEmpty(notificationId)) {
			selectedProductDesignTarget.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(targetId)) {
			selectedProductDesignTarget.setId(Long.parseLong(targetId));
			// action = "VIEW";
			viewProductDetailsTargetDetailsList();
		}
		loadMontAndYears();
		return VIEW_URL;
	}

	public String showNotification() {
		log.info("id------------------------->" + getTargetId());
		try {
			if (!Objects.isNull(targetId)) {
				selectedProductDesignTarget = new ProductDesignTarget();
				selectedProductDesignTarget.setId(targetId);
				// selectedEmployeeLoanAndAdvance.setLoanType(loanType);
				Long systemNotificationId = (notificationId != null ? Long.valueOf(notificationId) : 0l);

				selectedProductDesignTarget.setNotificationId(systemNotificationId);

				viewProductDetailsTargetDetailsList();

			}
		} catch (Exception e) {
			log.error("notification method error---", e);
		}
		return null;
	}
}
