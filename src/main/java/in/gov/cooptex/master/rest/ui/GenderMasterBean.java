package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("genderMasterBean")
public class GenderMasterBean {

	private final String CREATE_PAGE = "/pages/masters/createGenderMaster.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/masters/listGenderMaster.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/viewGenderMaster.xhtml?faces-redirect=true";

	private final String LIST_URL = "/gender/getallactive";
	private final String CREATE_URL = "/gender/create";
	private final String DELETE_URL = "/gender";
	private final String LAZY_LIST_URL = "/gender/search";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	GenderMaster genderMaster;

	@Autowired
	AppPreference appPreference;
	
	@Getter
	@Setter
	LazyDataModel<GenderMaster> lazyGenderMasterList;
	
	@Getter
	@Setter
	GenderMaster selectedGenderMaster;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	List<GenderMaster> genderMasterList = new ArrayList<GenderMaster>();

	@Getter
	@Setter
	private int genderMasterListSize;

	boolean forceLogin = false;

	@PostConstruct
	public void init() {
		log.info(" GenderMasterBean init --->" + serverURL);
		loadValues();
		addGender();
		getAllGenderList();
		loadLazyGenderMasterList();
		getGenderView(genderMaster);
	}

	public String showList() {
		log.info("<---------- Loading GenderMasterBean showList list page---------->");
		loadValues();
		getAllGenderList();
		loadLazyGenderMasterList();
		addButtonFlag = true;
		return LIST_PAGE;
	}

	public String addGender() {
		log.info("<---- Start GenderMasterBean.addGender ---->");
		try {
			action = "ADD";
			genderMaster = new GenderMaster();
			genderMaster.setActiveStatus(true);

			log.info("<---- End GenderMaster.addGender ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	private void getAllGenderList() {
		log.info("<<<< ----------Start genderMasterBean-getAllGenderList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.get(url);
			genderMaster = new GenderMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				genderMasterList = mapper.readValue(jsonResponse, new TypeReference<List<GenderMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (genderMasterList != null) {
			genderMasterListSize = genderMasterList.size();
		} else {
			genderMasterListSize = 0;
		}
		log.info("<<<< ----------End GenderMasterBean . getAllGenderList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			genderMaster = new GenderMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to GenderMaster List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... GenderMasterBean submit begin .... "+action+" " + genderMaster);
		try {
			
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + CREATE_URL, genderMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("GenderMasterBean saved successfully");
					if(action.equalsIgnoreCase("ADD"))
				    errorMap.notify(ErrorDescription.GENDER_MASTER_CREATED_SUCCESSFULLY.getCode());
					else
				    errorMap.notify(ErrorDescription.GENDER_MASTER_UPDATED_SUCCESSFULLY.getCode());
					showList();
					//errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					 return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating GenderMaster" + e);
		}
		log.info("...... GenderMasterBean submit ended ....");
		return LIST_PAGE;
	}

	public String editGender() {
		log.info("<<<<< -------Start editGender called ------->>>>>> ");
		action = "EDIT";

		if (genderMaster == null) {
			Util.addWarn("Please select one gender");
			return null;
		}

		if (genderMaster.getActiveStatus().equals("ACTIVE")) {
			genderMaster.setActiveStatus(genderMaster.getActiveStatus());
		} else {
			genderMaster.setActiveStatus(genderMaster.getActiveStatus());
		}
		log.info("<<<<< -------End editGender called ------->>>>>> ");

		return CREATE_PAGE;

	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts GenderMasterBean.onRowSelect ========>" + event);
		addButtonFlag = false;
		log.info("<===Ends GenderMasterBean.onRowSelect ========>");
	}

	public String deleteGender() {
		try {
			action = "Delete";
			if (genderMaster == null) {
				Util.addWarn("Please Select One gender");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmGenderDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteGenderConfirm() {

		try {
			log.info("Delete confirmed for gender [" + genderMaster.getName() + "]"+genderMaster.getId());
			GenderMaster genderMasterDelete = new GenderMaster();
			genderMasterDelete.setId(genderMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + DELETE_URL;
			baseDTO = httpService.delete(url+"/delete/"+genderMaster.getId());

			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() ==0) {
					log.info("Gender deleted successfully....!!!");
					//errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.GENDER_MASTER_DELETED_SUCCESSFULLY.getCode());
					genderMaster = new GenderMaster();
				}
				else
				{
					errorMap.notify(baseDTO.getStatusCode());
				}
				
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start genderMasterBean . clear ----->");
		addButtonFlag = true;
		getAllGenderList();
		genderMaster = new GenderMaster();
		log.info("<---- End GenderMasterBean . clear ----->");

		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewGender() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (genderMaster == null) {
			Util.addWarn("Please Select One Gender");
			return null;
		}
		return VIEW_PAGE;

	}

	private void getGenderView(GenderMaster genderMaster) {
		log.info("<<<< ----------Start genderMasterBean . getGenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.post(url, genderMaster);
			genderMaster = new GenderMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				genderMaster = mapper.readValue(jsonResponse, new TypeReference<GenderMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (genderMaster != null) {
			log.info("gendermaster getGenderView" + genderMaster);
		} else {
			log.info("gendermaster getGenderView" + genderMaster);
		}

		log.info("<<<< ----------End GenderMasterBean . getGenderView ------- >>>>");
	}
	
	public void loadLazyGenderMasterList() {
		log.info("<===== Starts GenderMasterBean.loadLazyGenderMasterList ======>");
		lazyGenderMasterList = new LazyDataModel<GenderMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<GenderMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL +LAZY_LIST_URL;
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						genderMasterList = mapper.readValue(jsonResponse,new TypeReference<List<GenderMaster>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGlAccountCategoryList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return genderMasterList;
			}

			@Override
			public Object getRowKey(GenderMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public GenderMaster getRowData(String rowKey) {
				try {
					for (GenderMaster glAccountCategory : genderMasterList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedGenderMaster = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends GenderMasterBean.loadLazyGlAccountCategoryList ======>");
	}


}
