package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ItemQRCodeDetailsDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.enums.StockTransferScanTypeEnum;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockInwardDWHBean")
@Scope("session")
public class StockInwardDWHBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/distributionWarehouse/inward/listStockInwardDWH.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/distributionWarehouse/inward/createStockInwardDWH.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/distributionWarehouse/inward/viewStockInwardDWH.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnSearch = true;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	LazyDataModel<StockTransferRequest> stockTransferRequestLazyList;

	@Getter
	@Setter
	StockTransferRequest stockTransferRequest;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> pwhList;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	public String getListPage() {
		log.info("Going to list page");

		// SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		stockTransferRequest = new StockTransferRequest();
		disableAddButton = false;
		loadLazyList();

		return LIST_PAGE_URL;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();

		request.setStockTransferType(StockTransferType.DISTRIB_INWARD);

		if (getLoginEmployeeWorkLocation() != null && getLoginEmployeeWorkLocation().getId() != null) {
			request.setId(getLoginEmployeeWorkLocation().getId());
		}

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("senderCodeOrName")) {
				request.setSenderCodeOrName(value);
			}

			if (entry.getKey().equals("receiverCodeOrName")) {
				request.setReceiverCodeOrName(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/list";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferRequestLazyList = new LazyDataModel<StockTransferRequest>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferRequest> data = new ArrayList<StockTransferRequest>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferRequest>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferRequest getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferRequest> responseList = (List<StockTransferRequest>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferRequest = ((StockTransferRequest) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferRequest = new StockTransferRequest();
		disableAddButton = false;
	}

	/***************************************************************************************************/

	/*
	 * This method is called when Edit Button is pressed
	 */

	public String gotoEditPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED.toString())) {

			stockTransfer = getStockTransferById(stockTransferRequest.getId());

			if (stockTransfer.getStockTransfer() != null) {
				stockTransfer.getStockTransfer().setCreatedByName(stockTransfer.getStockTransfer().getReferenceNumber()
						+ " / " + AppUtil.DATE_FORMAT.format(stockTransfer.getStockTransfer().getDateTransferred()));
				prepareReceiveProduct();
			}

			bundleNumbersAdded = new HashSet<>();

			stockTransferItemsDisplayList = new ArrayList<>();
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				// addItemToDisplayList(items);
				addItemToDisplayProductVarietyList(items);
			}

			disableBtnSearch = false;
			displayRemoveButtonItemDetails = true;

			return ADD_PAGE_URL;
		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

	}

	public String gotoViewPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = getStockTransferById(stockTransferRequest.getId());

		bundleNumbersAdded = new HashSet<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
			// addItemToDisplayList(items);
			addItemToDisplayProductVarietyList(items);
		}

		displayRemoveButtonItemDetails = false;

		return VIEW_PAGE_URL;

	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {

		// Step 1: Check the object is selected from the list
		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {

			// Step 2: Find the Object is removable
			if (stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferRequest.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	/*********************************************************************************************************************************/

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null
				|| !loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.DISTRIBUTION_WAREHOUSE)) {
			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_DISTRIBUTION_WAREHOUSE.getCode());

			return null;
		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}

	public String redirectSVInProgressPage() {
		Boolean stockVerification = loginBean.getStockVerificationIsInProgess();
		if (null != stockVerification && stockVerification.booleanValue()) {
			loginBean.setMessage("Inward");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}

	public String gotoPage(String page) {
		log.info("Button is pressed....", page);

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null
				|| !loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.DISTRIBUTION_WAREHOUSE)) {
			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_DISTRIBUTION_WAREHOUSE.getCode());

			return null;
		} else {

			if ("ADD".equals(page) || "EDIT".equals(page)) {
				redirectSVInProgressPage();
			}

			if (page.equals("ADD")) {
				return gotoAddPage();
			} else if (page.equals("EDIT")) {
				return gotoEditPage();
			} else if (page.equals("VIEW")) {
				return gotoViewPage();
			} else {
				return null;
			}
		}
	}

	public String gotoAddPage() {

		log.info("Add Button is pressed....");

		disableBtnSearch = true;

		stockTransfer = new StockTransfer();

		stockTransferItems = new StockTransferItems();

		stockTransfer.setStockTransfer(null);

		stockTransfer.setSupplierMaster(null);

		stockTransferList = null;

		stockTransferItemsDisplayListTemp = new ArrayList<>();

		stockTransferItemsDisplayList = new ArrayList<>();

		transferredBundleNumbers = null;

		transferredBundleWeight = null;

		transferredTotalBundles = null;

		bundleNumbersAdded = new HashSet<String>();

		displayRemoveButtonItemDetails = true;

		pwhList = commonDataService
				.getPWHListByDWH(loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId());

		return ADD_PAGE_URL;
	}

	public void onChangeProductWarehouse() {

		log.info("onChangeProductWarehouse");

		stockTransferList = new ArrayList<>();

		stockTransfer.setStockTransfer(null);

		if (stockTransfer.getFromEntityMaster() != null) {

			stockTransferList = commonDataService.getStockInwardListByDWHAndPWH(
					loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId(),
					stockTransfer.getFromEntityMaster().getId(), stockTransfer.getScanType());

			if (stockTransferList != null) {
				for (StockTransfer st : stockTransferList) {
					if (st.getDateTransferred() != null) {
						st.setCreatedByName(
								st.getReferenceNumber() + " / " + AppUtil.DATE_FORMAT.format(st.getDateTransferred()));
					} else {
						log.info("Transfer Date" + st.getDateTransferred());
					}
				}
			}

		}

	}

	public void onSelectStockTransfer() {
		if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
			if (stockTransfer.getFromEntityMaster() != null && stockTransfer.getStockTransfer() != null) {
				disableBtnSearch = false;
			} else {
				disableBtnSearch = true;
			}
		} else if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.BULK.toString())) {
			disableBtnSearch = true;
		}
	}

	/***************************************************************************************************************************/

	@Getter
	@Setter
	String transferredBundleNumbers;

	@Getter
	@Setter
	Integer transferredTotalBundles;

	@Getter
	@Setter
	Double transferredBundleWeight;

	public void searchStock() {
		log.info("Seach Stock Button is pressed.....");
		try {
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
				if (stockTransfer.getFromEntityMaster() != null && stockTransfer.getStockTransfer() != null) {
					log.info("Selected From Entity Master ID : [" + stockTransfer.getFromEntityMaster().getId()
							+ "] Stock Transfer ID : [" + stockTransfer.getStockTransfer().getId() + "]");
					stockTransfer.setStockTransfer(getStockTransferById(stockTransfer.getStockTransfer().getId()));

					transferredBundleNumbers = stockTransfer.getStockTransfer().getBundleNumbers();
					transferredBundleWeight = stockTransfer.getStockTransfer().getBundleWeight();
					transferredTotalBundles = stockTransfer.getStockTransfer().getTotalBundles();

					prepareStockTransfer(stockTransfer.getStockTransfer());
				} else {
					log.info("Fields are required.");
				}
			} else if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.BULK.toString())) {
				disableBtnSearch = true;
				stockTransferItemsDisplayList = new ArrayList<>();

				StockTransfer transfer = getStockTransferById(stockTransfer.getStockTransfer().getId());
				transferredBundleNumbers = transfer.getBundleNumbers();
				transferredBundleWeight = transfer.getBundleWeight();
				transferredTotalBundles = transfer.getTotalBundles();

				prepareStockTransfer(transfer);

				stockTransfer.setStockTransferItemsList(new ArrayList<>(transfer.getStockTransferItemsList()));
				stockTransfer.getStockTransferItemsList().stream().forEach(items -> {
					log.info("transfer dispatched qty -------" + items.getDispatchedQty());
					if (items.getDispatchedQty() != null) {
						items.setReceivedQty(items.getDispatchedQty());
					}
				});

				Map<Long, List<StockTransferItems>> stockTransferItemsMap = transfer.getStockTransferItemsList()
						.stream().collect(Collectors.groupingBy(x -> x.getProductVarietyMaster().getId()));

				if (stockTransferItemsMap != null && !stockTransferItemsMap.isEmpty()) {
					log.info("stockTransferItemsMap size-------->" + stockTransferItemsMap.size());
					for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItemsMap.entrySet()) {
						StockTransferItems stockTransferItems = new StockTransferItems();
						stockTransferItems.setProductVarietyMaster(entry.getValue().get(0).getProductVarietyMaster());
						stockTransferItems.setUomMaster(entry.getValue().get(0).getUomMaster());
						stockTransferItems.setMonthAge(entry.getValue().get(0).getMonthAge());
						stockTransferItems.setYearAge(entry.getValue().get(0).getYearAge());
						Double dispatchedQty = 0.0;
						Double itemNetTotal = 0.0;
						for (StockTransferItems items : entry.getValue()) {
							if (items.getProductVarietyMaster().getId()
									.equals(stockTransferItems.getProductVarietyMaster().getId())) {
								dispatchedQty += items.getDispatchedQty();
								itemNetTotal += items.getDispatchedQty() * items.getUnitRate();
								StockTransferItemsBundles bundles = new StockTransferItemsBundles();
								bundles.setStockTransferItems(items);
								bundles.setAtNumber(stockTransferItems.getAtNumber());
								bundles.setQuantity(AppUtil.ifNullRetunZero(items.getDispatchedQty()));
								bundles.setItemNetTotal(
										AppUtil.ifNullRetunZero(items.getDispatchedQty() * items.getUnitRate()));
								bundles.setBundleNumber(items.getBundleNumber());
								bundles.setAtNumber(items.getAtNumber());
								bundles.setQrCode(items.getQrCode());
								stockTransferItems.getStockTransferItemsBundlesList().add(bundles);
							}
						}
						stockTransferItems.setDispatchedQty(dispatchedQty);
						stockTransferItems.setReceivedQty(dispatchedQty);
						stockTransferItems.setItemNetTotal(itemNetTotal);
						stockTransferItemsDisplayList.add(stockTransferItems);
					}
				} else {
					log.info("stockTransferItemsMap is empty");
				}
			}
		} catch (Exception e) {
			log.error("Exception-------------", e);
		}
	}

	public void prepareStockTransfer(StockTransfer selectedStockTransfer) {

		// stockTransfer.setFromEntityMaster(selectedStockTransfer.getFromEntityMaster());
		stockTransfer.setToEntityMaster(selectedStockTransfer.getToEntityMaster());

		stockTransfer.setTransportMaster(selectedStockTransfer.getTransportMaster());
		stockTransfer.setWaybillAvailable(selectedStockTransfer.getWaybillAvailable());
		stockTransfer.setWaybillNumber(selectedStockTransfer.getWaybillNumber());
		stockTransfer.setTransportChargeAvailable(selectedStockTransfer.getTransportChargeAvailable());
		stockTransfer.setTransportChargeType(selectedStockTransfer.getTransportChargeType());
		stockTransfer.setTransportChargeAmount(selectedStockTransfer.getTransportChargeAmount());

		stockTransfer.setDateTransferred(selectedStockTransfer.getDateTransferred());
		stockTransfer.setDateReceived(new Date());
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		stockTransfer.setTransferType(StockTransferType.DISTRIB_INWARD);

		if (stockTransfer.getId() == null) {
			stockTransfer.setStockTransferItemsList(new ArrayList<>());
		}
		stockTransferItemsDisplayList = new ArrayList<>();
		// Preparing Receive Product Form
		prepareReceiveProduct();

	}

	public void prepareReceiveProduct() {
		log.info("gotoBundleDetails is called ", stockTransfer.getStockTransfer());

		bundleNumberSet = new HashSet<>();
		stockTransferItems = new StockTransferItems();

		if (stockTransfer.getStockTransfer() != null
				&& stockTransfer.getStockTransfer().getStockTransferItemsList() != null) {
			for (StockTransferItems sti : stockTransfer.getStockTransfer().getStockTransferItemsList()) {
				if (sti.getBundleNumber() != null) {
					bundleNumberSet.add(sti.getBundleNumber());
				}
			}
		}
		stockTransferItemsListTemp = new ArrayList<>(stockTransfer.getStockTransfer().getStockTransferItemsList());
		log.info("Number of Bundle Numbers " + bundleNumberSet);
	}

	/*************************************************************************************************************************************/

	public void clearBundleDetails() {
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList().stream().forEach(items -> {
				items.getStockTransferItemsBundlesList().clear();
			});
		}
	}

	private boolean validateStockTransfer() {

		if (stockTransfer.getStockTransferItemsList() == null || stockTransfer.getStockTransferItemsList().isEmpty()) {
			errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_ITEMS_REQUIRED.getCode());
			return false;
		}

		return true;
	}

	public String saveStockTransfer() {
		log.info("saveStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		if (validateStockTransfer()) {
			clearBundleDetails();
			return create();
		} else {
			return null;
		}
	}

	public String submitStockTransfer() {
		log.info("submitStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		if (validateStockTransfer()) {
			clearBundleDetails();
			return create();
		} else {
			return null;
		}
	}

	/*************************************************************************************************************************************/

	@Getter
	@Setter
	Set<String> bundleNumberSet = new HashSet<>();

	@Getter
	@Setter
	StockTransferItems stockTransferItems = new StockTransferItems();

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsListTemp;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayListTemp;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDisplay;

	@Getter
	@Setter
	Set<String> bundleNumbersAdded;

	@Getter
	@Setter
	boolean displayRemoveButtonItemDetails = false;

	public StockTransferItems searchStockTransferItemsByQrCode(StockTransferItems stockTransferItems) {

		if (stockTransferItemsListTemp != null && !stockTransferItemsListTemp.isEmpty()) {
			for (StockTransferItems items : stockTransferItemsListTemp) {
				if (items.getQrCode() != null && items.getQrCode().equals(stockTransferItems.getQrCode())) {
					StockTransferItems newItem = new StockTransferItems(items);
					newItem.setBundleNumber(stockTransferItems.getBundleNumber());
					return newItem;
				}
			}
		}

		return null;
	}

	public void viewItemDetails(StockTransferItems items) {
		stockTransferItemsDisplay = items;
	}

	/*
	 * Scan QR Code For StockInward
	 */
	public String scanOrAddReceiveProduct() {
		log.info("<<<--- Start the scanOrAddReceiveProduct() --->>> ");
		log.info(" scanOrAddReceiveProduct :: QR Code :: " + stockTransferItems.getQrCode());

		ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();

		try {
			if (stockTransferItems.getQrCode() != null && stockTransferItems.getQrCode().length() > 0) {

				if (AppUtil.isJSONValid(stockTransferItems.getQrCode())) {

					itemQRCodeDetailsDTO = new Gson().fromJson(stockTransferItems.getQrCode(),
							ItemQRCodeDetailsDTO.class);
				} else {
					itemQRCodeDetailsDTO = getItemQrcodeDetails(stockTransferItems.getQrCode());
				}

				log.info("itemQRCodeDetailsDTO " + itemQRCodeDetailsDTO);
				if (itemQRCodeDetailsDTO != null && itemQRCodeDetailsDTO.getQRCode() != null) {
					stockTransferItems.setQrCode(itemQRCodeDetailsDTO.getQRCode());
				}

				if (stockTransferUtility.isQrCodeDuplicated(itemQRCodeDetailsDTO.getQRCode(),
						stockTransfer.getStockTransferItemsList())) {
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_QR_CODE_DUPLICATED.getCode());
					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);
					return null;
				}

				StockTransferItems items = searchStockTransferItemsByQrCode(stockTransferItems);
				if (items != null) {
					items.setReceivedQty(AppUtil.ifNullRetunZero(items.getDispatchedQty()));
					items.setDispatchedQty(items.getDispatchedQty() - items.getDispatchedQty());
					// addItemToDisplayList(items)
					addItemToDisplayProductVarietyList(items);
					stockTransfer.getStockTransferItemsList().add(items);
					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);
				} else {
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_QR_CODE_INVALID.getCode());
					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);
					return null;
				}
				// }
				// else {
				// errorMap.notify(ErrorDescription.STOCK_TRANSFER_QR_CODE_INVALID.getCode());
				// String bundleNumber = stockTransferItems.getBundleNumber();
				// stockTransferItems = new StockTransferItems();
				// stockTransferItems.setBundleNumber(bundleNumber);
				// return null;
				// }
			}
		} catch (Exception e) {
			log.error("Exception occurred at scanOrAddReceiveProduct() ", e);
		}
		log.info("<<<--- End the scanOrAddReceiveProduct() --->>> ");
		return null;
	}

	public ItemQRCodeDetailsDTO getItemQrcodeDetails(String qrCode) {
		log.info("Start getItemQrcodeDetails Method======>");
		ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = null;
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stock/transfer/product/warehouse/getqrcodedetails/" + qrCode;
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Item Qr code details get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemQRCodeDetailsDTO = mapper.readValue(jsonResponse, ItemQRCodeDetailsDTO.class);

			}

		} catch (Exception ex) {
			log.error("Error in getItemQrcodeDetails Method", ex);
		}
		log.info("Start getItemQrcodeDetails Method======>");
		return itemQRCodeDetailsDTO;
	}

	public void removeReceiveProduct(StockTransferItemsBundles bundles) {
		log.info("removeReceiveProduct ", bundles);
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList()
					.removeIf(item -> item.getAtNumber().equals(bundles.getAtNumber()));
			removeItemFromDisplayList(bundles);
		}
	}

	public void removeItemFromDisplayList(StockTransferItemsBundles bundles) {
		if (bundleNumbersAdded != null && !bundleNumbersAdded.isEmpty()) {
			bundleNumbersAdded.removeIf(bundleNumber -> bundleNumber.equals(bundles.getBundleNumber()));
			updateBundleNumbers(bundleNumbersAdded);
		}

		if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {
			StockTransferItems itemToRemove = null;
			boolean removeItemFlag = false;
			for (StockTransferItems items : stockTransferItemsDisplayList) {
				boolean isBundleRemoved = false;
				if (items.getProductVarietyMaster().getId()
						.equals(bundles.getStockTransferItems().getProductVarietyMaster().getId())) {

					if (items.getStockTransferItemsBundlesList() != null
							&& !items.getStockTransferItemsBundlesList().isEmpty()
							&& items.getStockTransferItemsBundlesList()
									.removeIf(b -> b.getAtNumber().equals(bundles.getAtNumber()))) {
						isBundleRemoved = true;
					}
					if (isBundleRemoved) {
						items.setReceivedQty(items.getReceivedQty() - 1);
						items.setItemNetTotal(items.getItemNetTotal() - bundles.getItemNetTotal());
						if (items.getReceivedQty() == 0) {
							removeItemFlag = true;
							itemToRemove = items;
						}
					}
				}
			}
			if (removeItemFlag) {
				stockTransferItemsDisplayList.remove(itemToRemove);
			}
		}
	}

	private void addItemToDisplayProductVarietyList(StockTransferItems items) {
		log.info("<<<--- Inside the addItemToDisplayProductVarietyList()  --->>> ");
		StockTransferItems stockItems = new StockTransferItems(items);
		ProductVarietyMaster productVarietyMaster = stockItems.getProductVarietyMaster();
		/*
		 * Bundle Number Details
		 */
		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setStockTransferItems(stockItems);
		bundles.setAtNumber(stockItems.getAtNumber());
		bundles.setQrCode(stockItems.getQrCode());
		bundles.setQuantity(AppUtil.ifNullRetunZero(stockItems.getReceivedQty()));
		bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stockItems.getUnitRate()));
		if (stockItems.getBundleNumber() != null) {
			bundles.setBundleNumber(stockItems.getBundleNumber());
			bundleNumbersAdded.add(stockItems.getBundleNumber());
			updateBundleNumbers(bundleNumbersAdded);
		}
		int count = 0;
		if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {

			for (StockTransferItems stockTransferItems : stockTransferItemsDisplayList) {
				/*
				 * Check already exists product variety and add the received and dispatched
				 * quantity
				 */
				if (stockTransferItems.getProductVarietyMaster() != null
						&& stockTransferItems.getProductVarietyMaster().getId().equals(productVarietyMaster.getId())) {
					stockTransferItems.setReceivedQty(
							stockTransferItems.getReceivedQty() != null ? stockTransferItems.getReceivedQty() : 0.00);
					stockItems.setReceivedQty(stockItems.getReceivedQty() != null ? stockItems.getReceivedQty() : 0.00);
					// Sum the current and exists received quantity
					stockTransferItems
							.setReceivedQty(stockTransferItems.getReceivedQty() + stockItems.getReceivedQty());
					// sum the current and exists item net total
					stockItems.setItemNetTotal(stockItems.getUnitRate() != null ? stockItems.getUnitRate() : 0.00);
					stockTransferItems
							.setItemNetTotal(stockTransferItems.getItemNetTotal() + stockItems.getItemNetTotal());
					if (stockTransferItems.getStockTransferItemsBundlesList() != null
							&& !stockTransferItems.getStockTransferItemsBundlesList().isEmpty()) {
						stockTransferItems.getStockTransferItemsBundlesList().add(bundles);
					} else {
						log.info("<<< ---- stockTransferItems.getStockTransferItemsBundlesList() is empty --->>>> ");
					}
					++count;
					break;
				}
			}
			/*
			 * Newly added Product variety
			 */
			if (count <= 0) {
				stockItems.setDispatchedQty(stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems,
						stockTransferItemsListTemp));
				stockItems.getStockTransferItemsBundlesList().add(bundles);
				stockItems.setItemNetTotal(stockItems.getUnitRate());
				stockTransferItemsDisplayList.add(stockItems);
			}

		} else {
			stockItems.setDispatchedQty(
					stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems, stockTransferItemsListTemp));
			stockItems.getStockTransferItemsBundlesList().add(bundles);
			stockItems.setItemNetTotal(stockItems.getUnitRate());
			stockTransferItemsDisplayList.add(stockItems);

		}
		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
		log.info("<<<--- End the addItemToDisplayProductVarietyList()  --->>> ");
	}

	public void addItemToDisplayList(StockTransferItems items) {

		StockTransferItems stockItems = new StockTransferItems(items);

		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setStockTransferItems(stockItems);
		bundles.setAtNumber(stockItems.getAtNumber());
		bundles.setQuantity(AppUtil.ifNullRetunZero(stockItems.getReceivedQty()));
		bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stockItems.getItemNetTotal()));
		if (stockItems.getBundleNumber() != null) {
			bundles.setBundleNumber(stockItems.getBundleNumber());
			bundleNumbersAdded.add(stockItems.getBundleNumber());
			updateBundleNumbers(bundleNumbersAdded);
		}

		stockTransferItemsDisplayList = new ArrayList<>();

		if (!stockTransferItemsDisplayList.isEmpty()) {
			stockItems.getStockTransferItemsBundlesList().add(bundles);
			stockItems.setDispatchedQty(
					stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems, stockTransferItemsListTemp));
			stockItems.setReceivedQty(
					stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems, stockTransferItemsListTemp));
			stockTransferItemsDisplayList.add(stockItems);
			// stockTransferItemsDisplayList = new ArrayList<>();
			// for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
			//
			// boolean checkItemPresent =
			// checkListForRepeatedProductVariety(stockTransferItemsDisplayListTemp,
			// stockItems);
			// if(checkItemPresent == true) {
			// sti.setReceivedQty(sti.getReceivedQty() +
			// AppUtil.ifNullRetunZero(stockItems.getReceivedQty()));
			// sti.setItemNetTotal(sti.getItemNetTotal() + stockItems.getItemNetTotal());
			// sti.getStockTransferItemsBundlesList().add(bundles);
			// break;
			// }else {
			// stockItems.getStockTransferItemsBundlesList().add(bundles);
			// stockTransferItemsDisplayList.add(stockItems);
			// break;
			// }
			//
			//
			//
			//// if (sti.getProductVarietyMaster().getId().longValue() ==
			// stockItems.getProductVarietyMaster().getId()
			//// .longValue()) {
			//// sti.setReceivedQty(sti.getReceivedQty() +
			// AppUtil.ifNullRetunZero(stockItems.getReceivedQty()));
			//// sti.setItemNetTotal(sti.getItemNetTotal() +
			// AppUtil.ifNullRetunZero(bundles.getItemNetTotal()));
			//// sti.getStockTransferItemsBundlesList().add(bundles);
			//// }
			// //stockTransferItemsDisplayList.add(sti);
			// }
		} else {
			stockItems.getStockTransferItemsBundlesList().add(bundles);
			// stockItems.setReceivedQty(AppUtil.ifNullRetunZero(stockItems.getReceivedQty()));
			stockItems.setDispatchedQty(
					stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems, stockTransferItemsListTemp));
			stockTransferItemsDisplayList.add(stockItems);
		}

		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null && !setOfBundleNumbers.isEmpty()) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	public void onSelectBundleNumber() {
		log.info("Stock Transfer " + stockTransfer);
	}

	public void onSelectProductVarietyMaster() {
		log.info("Stock Transfer " + stockTransfer);
	}

	/********************************************************************************************************************************************/

	public StockTransfer getStockTransferById(Long id) {
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/get/" + id;

		StockTransfer stockTransfer = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransfer = mapper.readValue(jsonValue, StockTransfer.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return stockTransfer;
	}

	public String confirmCreateInward() {
		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/distribution/warehouse/inward/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					if (stockTransfer.getStatus() != null) {
						if (stockTransfer.getStatus().equals(StockTransferStatus.INITIATED)) {
							log.info("Stock Transfer saved successfully");
							errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
						} else if (stockTransfer.getStatus().equals(StockTransferStatus.SUBMITTED)) {
							log.info("Stock Transfer saved successfully");
							AppUtil.addInfo("Stock transfer submitted successfully");
						}
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				return null;
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_PAGE_URL;
	}

	public String create() {
		log.info("create method is executing..");

		if (!CollectionUtils.isEmpty(stockTransferItemsDisplayList)) {
			Double totalDispatchedQty = stockTransferItemsDisplayList.stream().filter(o -> o.getDispatchedQty() != null)
					.collect(Collectors.summingDouble(StockTransferItems::getDispatchedQty));
			totalDispatchedQty = totalDispatchedQty != null ? totalDispatchedQty.doubleValue() : 0D;

			Double totalReceivedQty = stockTransferItemsDisplayList.stream().filter(o -> o.getReceivedQty() != null)
					.collect(Collectors.summingDouble(StockTransferItems::getReceivedQty));
			totalReceivedQty = totalReceivedQty != null ? totalReceivedQty.doubleValue() : 0D;
			if (StockTransferStatus.INITIATED.equals(stockTransfer.getStatus())) {
				if (totalReceivedQty < totalDispatchedQty) {
					RequestContext.getCurrentInstance().execute("PF('confirmDlg').show();");
					return null;
				}
			} else if (StockTransferStatus.SUBMITTED.equals(stockTransfer.getStatus())) {
				if (totalReceivedQty < totalDispatchedQty) {
					AppUtil.addError("Total Dispatched quantity and total received quantity should be same.");
					return null;
				}
			}

			if (totalReceivedQty > totalDispatchedQty) {
				AppUtil.addError("Dispatched quantity and received quantity are mismatched.");
				return null;
			}
		}

		confirmCreateInward();
		return LIST_PAGE_URL;
	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("DWH_STOCK_INWARD_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}

	private boolean checkListForRepeatedProductVariety(List<StockTransferItems> stockTransferItemsDisplayListTemp,
			StockTransferItems stiItems) {
		boolean check = false;
		for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
			if (sti.getProductVarietyMaster().getId() == stiItems.getProductVarietyMaster().getId()) {
				check = true;
				break;
			} else {
				check = false;
			}
		}
		return check;
	}

}
