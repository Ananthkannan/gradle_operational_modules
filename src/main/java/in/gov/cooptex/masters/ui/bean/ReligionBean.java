package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("religionBean")
@Scope("session")
public class ReligionBean implements Serializable {/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private final String RELIGION_LIST_PAGE = "/pages/masters/religion/listReligion.xhtml?faces-redirect=true;";
	private final String RELIGION_ADD_PAGE = "/pages/masters/religion/createReligion.xhtml?faces-redirect=true;";
	private final String RELIGION_VIEW_PAGE = "/pages/masters/religion/viewReligion.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter @Setter
	Religion selectedReligion = new Religion();

	@Getter @Setter
	Religion religion = new Religion();

	@Getter @Setter
	private RestTemplate restTemplate;

	@Getter @Setter
	private String Url = null;

	/*@Getter @Setter
	LazyDataModel<Region> religionlazyLoadList;*/

	@Getter @Setter
	private Map<String, Object> filterMap;

	@Getter @Setter
	private SortOrder sortingOrder;

	@Getter @Setter
	private String sortingField;

	@Getter @Setter
	private Integer resultSize;

	@Getter @Setter 
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<Religion> religionList = new ArrayList<Religion>();
	
	@Getter
	@Setter
	LazyDataModel<Religion> lazyReligionList;
	
	/**
	 * @return
	 */
	public String showReligionListPage() {
		log.info("showReligionListPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedReligion = new Religion();
		//loadReligionList();
		loadLazyReligionList();
		return RELIGION_LIST_PAGE;
	}
	
	
	
	public void loadLazyReligionList() {
		log.info("<===== Starts ReligionBean.loadLazyReligionList ======>");
		lazyReligionList = new LazyDataModel<Religion>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<Religion> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/religion/getreligionlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						religionList = mapper.readValue(jsonResponse, new TypeReference<List<Religion>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return religionList;
			}

			@Override
			public Object getRowKey(Religion res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Religion getRowData(String rowKey) {
				try {
					for (Religion religion : religionList) {
						if (religion.getId().equals(Long.valueOf(rowKey))) {
							selectedReligion = religion;
							return religion;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ReligionBean.loadLazyReligionList ======>");
	}

	
	public void loadReligionList() {
		log.info("<-----Region list load starts------>");
		try {
			BaseDTO baseDTO = httpService.get(SERVER_URL + "/religion/getAll");
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			religionList = mapper.readValue(jsonResponse,
					new TypeReference<List<Religion>>() {
			});
			if (religionList == null) {
				log.info(" RegionList is null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		}catch(Exception e) {
			log.error("Exception occuredin loading religion ",e);
		}
		log.info("<-----Region list load endss------>");
	}


	public String religionListAction() {
		log.info("<====== RegionBean.regionListAction Starts====> action   :::"+action);
		try {
			if(action.equalsIgnoreCase("ADD")) {
				log.info("<====== region add page called.......====>");
				selectedReligion = new Religion();
				religion = new Religion();
				return RELIGION_ADD_PAGE;
			}
			if(selectedReligion==null) {
				errorMap.notify(ErrorDescription.SELECT_RELIGION.getErrorCode());
				return null;
			}
			if(action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmReligionDelete').show();");
			}else {
				log.info("<----Loading Region Edit page.....---->" + selectedReligion.getId());
				religion = new Religion();
				BaseDTO response = httpService
						.get(SERVER_URL + "/religion/getby/"+selectedReligion.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					religion = mapper.readValue(jsonResponse, new TypeReference<Religion>() {
					});
					log.info("<====== RegionBean.regionListAction Ends====>");
					if(action.equalsIgnoreCase("EDIT")) {
						return RELIGION_ADD_PAGE;
					}else if(action.equalsIgnoreCase("VIEW")) {
						return RELIGION_VIEW_PAGE;
					}
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== RegionBean.regionListAction Ends====>");
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured in RegionBean.regionListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== RegionBean.regionListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts RegionBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/religion/deleteby/"+ selectedReligion.getId());
			if(response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.RELIGION_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting religion....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends RegionBean.deleteConfirm ---->");
		return showReligionListPage();
	}

	
	/*public void onRowSelect(SelectEvent event) {
		log.info("<===Starts RegionBean.onRowSelect ========>"+event);
		selectedReligion = ((Religion) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends RegionBean.onRowSelect ========>");
    }*/
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts BankMasterBean.onRowSelect ========>" + event);
		selectedReligion = ((Religion) event.getObject());
		log.info("selectedBankMaster id" + selectedReligion.getId());
		addButtonFlag = false;
		log.info("<===Ends BankMasterBean.onRowSelect ========>");
	}


	
	public String submit() {
		log.info("<=======Starts  RegionBean.submit ======>"+religion);
		if(religion.getName() == null || religion.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.RELIGION_NAME_EMPTY.getErrorCode());
			return null;
		}
		if(religion.getName().trim().length()<3  || religion.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.RELIGION_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(religion.getLocalName() == null || religion.getLocalName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.RELIGION_LNAME_EMPTY.getErrorCode());
			return null;
		}
		if(religion.getLocalName().trim().length()<3  || religion.getLocalName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.RELIGION_LNAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(religion.getActiveStatus() == null) {
			errorMap.notify(ErrorDescription.RELIGION_STATUS_EMPTY.getErrorCode());
			return null;
		}
		if(action.equalsIgnoreCase("ADD")) {
			religion.setCreatedBy(loginBean.getUserDetailSession());
		}else if (action.equalsIgnoreCase("EDIT")) {
			religion.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response= httpService.post(SERVER_URL + "/religion/saveorupdate",religion);
//		errorMap.notify(response.getStatusCode());
		if(response.getStatusCode()==0) {
			if(action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.RELIGION_SAVE_SUCCESS.getErrorCode());
			else 
				errorMap.notify(ErrorDescription.RELIGION_UPDATE_SUCCESS.getErrorCode());
			return showReligionListPage();
		}else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends RegionBean.submit ======>");
		return null;
	}
	
}
