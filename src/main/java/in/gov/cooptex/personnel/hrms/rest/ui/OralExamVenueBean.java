package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.operation.model.OralExamVenue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("oralExamVenueBean")
public class OralExamVenueBean {

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	private String page;

	@Getter
	@Setter
	private OralExamVenue oralExamVenue, selectedOralExamVenue;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private LazyDataModel<OralExamVenue> lazyOralExamVenueDataModel;

	@Getter
	@Setter
	private List<OralExamVenue> oralExamVenueList;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<DistrictMaster> districtList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<JobAdvertisement> jobNotificationList;

	@Getter
	@Setter
	private Date startTime, endTime;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean add, approve, delete, edit, view;

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/oralexamvenue";

	public String showPage() {
		log.info(":: Show oral test venue page. {}.xhtml :: ", page);
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		if (page.equals("add")) {
			clear();
			showTime();
			return "/pages/personnelHR/createOralExamVenue.xhtml?faces-redirect=true";
		} else if (page.equals("edit")) {
			if (selectedOralExamVenue == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			oralExamVenue = oralTestVenueById();
			loadDropDown();
			onStateChange();
			showTime();
			return "/pages/personnelHR/createOralExamVenue.xhtml?faces-redirect=true";
		} else if (page.equals("view")) {
			if (selectedOralExamVenue == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			oralExamVenue = oralTestVenueById();
			loadLazyDataModel();
			selectedOralExamVenue = new OralExamVenue();
			return "/pages/personnelHR/viewOralExamVenue.xhtml?faces-redirect=true";
		} else if (page.equals("approve")) {
			if (selectedOralExamVenue == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			oralExamVenue = oralTestVenueById();
			loadLazyDataModel();
			selectedOralExamVenue = new OralExamVenue();
			return "/pages/personnelHR/approveOralExamVenue.xhtml?faces-redirect=true";
		} else if (page.equalsIgnoreCase("delete")) {
			log.info(":: Job Advertisement Delete Response :: " + selectedOralExamVenue);
			if (selectedOralExamVenue == null) {
				errorMap.notify(ErrorDescription.ORAL_EXAM_VENUE_NOT_FOUND.getErrorCode());
				return null;
			} else if (selectedOralExamVenue.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
			return "";
		} else {
			loadLazyDataModel();
			selectedOralExamVenue = new OralExamVenue();
			add = true;
			delete = true;
			approve = true;
			edit = true;
			view = true;
			return "/pages/personnelHR/listOralExamVenue.xhtml?faces-redirect=true";
		}
	}

	public void loadDropDown() {
		jobNotificationList = commonDataService.loadJobNotificationNo();
		entityMasterList = commonDataService.loadRegionalOffice();
		stateList = commonDataService.loadSates();
	}

	private void showTime() {
		if (oralExamVenue.getOralExamStartTime() == null && oralExamVenue.getOralExamEndTime() == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.AM_PM, Calendar.AM);
			calendar.set(Calendar.HOUR, 8);
			calendar.set(Calendar.MINUTE, 15);
			startTime = calendar.getTime();
			log.info(":: Oral exam venue start time :: " + startTime);
			// oralTestVenue = new OralTestVenue();
			oralExamVenue.setOralExamStartTime(startTime);

			calendar.set(Calendar.HOUR, 11);
			calendar.set(Calendar.MINUTE, 40);
			endTime = calendar.getTime();
			log.info(":: Oral exam venue end time :: " + endTime);
			oralExamVenue.setOralExamEndTime(endTime);
		}
	}

	private OralExamVenue oralTestVenueById() {
		log.info(":: Get oral test venue details by id ::");
		OralExamVenue venueDetail = null;
		try {
			log.info(":: Selected oral test venue Id ::" + selectedOralExamVenue.getId());
			String url = SERVER_URL + "/load/" + selectedOralExamVenue.getId();
			log.info(":: Oral test venue Id URL ::" + url);
			BaseDTO baseDto = httpService.get(url);
			log.info(":: Oral test venue base dto response object ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Oral test venue base dto response object null::");
				return null;
			}
			if (baseDto.getResponseContent() != null) {
				venueDetail = new OralExamVenue();
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
				venueDetail = mapper.readValue(jsonResponse, OralExamVenue.class);
				if (venueDetail == null) {
					errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
					return null;
				}
			}

		} catch (Exception e) {
			log.error(":: Exception in oral test venue by id ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return venueDetail;
	}

	public String saveoralExamVenue() {
		log.info(":: Oral Test Venue Save ::");
		try {
			log.info("#= Oral Test Venue Save =#" + oralExamVenue);
			if (oralExamVenue == null) {
				log.info(":: Oral Test Venue Save request object null ::");
				return null;
			}
			String url;
			BaseDTO baseDto;
			if (oralExamVenue.getId() != null) {
				url = SERVER_URL + "/update";
				log.info(":: Oral Test Venue update URL ::" + url);
				baseDto = httpService.put(url, oralExamVenue);
				log.info(":: Oral Test Venue update BaseDTO Response ::" + baseDto);
			} else {
				url = SERVER_URL + "/create";
				log.info(":: Oral Test Venue Save URL ::" + url);
				baseDto = httpService.post(url, oralExamVenue);
				log.info(":: Oral Test Venue Save BaseDTO Response ::" + baseDto);
			}
			if (baseDto == null) {
				log.info(":: Oral Test Venue BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			log.info(":: Oral Test Venue Save Response Status code::" + baseDto.getStatusCode());
			if (baseDto.getStatusCode() == 32004 || baseDto.getStatusCode() == 32005) {
				log.info(":: Oral test venue saved successfully ::");
				errorMap.notify(baseDto.getStatusCode());
			} else {
				log.info(":: Oral test venue failed to save ::");
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(":: Exception in Oral Test Venue Save ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		loadLazyDataModel();
		selectedOralExamVenue = new OralExamVenue();
		add = true;
		return "/pages/personnelHR/listOralExamVenue.xhtml?faces-redirect=true";
	}

	public String deleteRecord() {
		log.info(":: Oral Test Venue delete record by id ::");
		try {
			if (selectedOralExamVenue == null) {
				log.info(":: Oral Test Venue id not found ::");
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			log.info(":: Oral Test Venue selected record id ::" + selectedOralExamVenue.getId());
			String url = SERVER_URL + "/delete/" + selectedOralExamVenue.getId();
			log.info(":: Oral Test Venue delete by id URL ::" + url);
			BaseDTO baseDto = httpService.delete(url);
			log.info(":: Oral Test Venue delete by id BaseDTO Response ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Oral Test Venue delete by id BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			if (baseDto.getStatusCode() == 32006) {
				log.info(":: Oral test venue record deleted successfully ::");
				errorMap.notify(baseDto.getStatusCode());
			} else {
				log.info(":: Oral test venue failed to delete ::");
				return null;
			}
			loadLazyDataModel();
		} catch (Exception e) {
			log.error(":: Exception in Oral Test Venue delete by id ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		add = true;
		return "/pages/personnelHR/listOralExamVenue.xhtml?faces-redirect=true";
	}

	public void clear() {
		log.info(":: Clear oral test venue details. :: ");
		oralExamVenue = new OralExamVenue();
		selectedOralExamVenue = new OralExamVenue();
		loadDropDown();
		loadLazyDataModel();
	}

	public void onStateChange() {
		log.info("> Oral test venue onStateChange() :: ");
		try {
			if (oralExamVenue.getStateMaster() == null) {
				log.info(":: State object is null :: ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			log.info("State Id ::" + oralExamVenue.getStateMaster().getId());
			String url = AppUtil.getPortalServerURL() + "/districtMaster/getalldistrictbystate/"
					+ oralExamVenue.getStateMaster().getId();
			log.info("State Id ::" + url);
			BaseDTO baseDto = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			districtList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
			});
			if (districtList.isEmpty()) {
				log.info(":: District not found. :: " + districtList.size());
			} else {
				log.info(":: District found. :: " + districtList.size());
			}
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading districts::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("< Oral test venue onStateChange() :: ");
	}

	public void loadLazyDataModel() {
		log.info(":: Load lazy oral test venue details. :: ");

		lazyOralExamVenueDataModel = new LazyDataModel<OralExamVenue>() {

			private static final long serialVersionUID = 1819149031170182827L;

			@Override
			public List<OralExamVenue> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					log.info(":: Oral test venue search request data ::" + paginationRequest);
					String url = SERVER_URL + "/loadlazydata";
					log.info(":: Oral test venue search request url ::" + url);

					BaseDTO lazyBaseDto = httpService.post(url, paginationRequest);

					if (lazyBaseDto == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(lazyBaseDto.getResponseContents());
					oralExamVenueList = mapper.readValue(jsonResponse, new TypeReference<List<OralExamVenue>>() {
					});

					if (oralExamVenueList == null) {
						log.info(" :: Oral test venue Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (lazyBaseDto.getStatusCode() == 0) {
						this.setRowCount(lazyBaseDto.getTotalRecords());
						totalRecords = lazyBaseDto.getTotalRecords();
						log.info(":: Oral test venue Search Successfully Completed ::");
					} else {
						log.error(
								":: Oral test venue Search Failed With status code :: " + lazyBaseDto.getStatusCode());
						errorMap.notify(lazyBaseDto.getStatusCode());
					}
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading oral test venue ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info(":: Oral test venue Search list Size ::" + oralExamVenueList);
				return oralExamVenueList;
			}

			@Override
			public Object getRowKey(OralExamVenue res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OralExamVenue getRowData(String rowKey) {
				for (OralExamVenue oralVenue : oralExamVenueList) {
					if (oralVenue.getId().equals(Long.valueOf(rowKey))) {
						selectedOralExamVenue = oralVenue;
						return oralVenue;
					}
				}
				return null;
			}
		};
	}

	public void confirmStatus() {
		if (oralExamVenue == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public String approveReject() {
		try {
			if (oralExamVenue == null) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info(":: Oral Test Venue approve record id ::" + oralExamVenue.getId());
			String url = SERVER_URL + "/approve";
			log.info(":: Oral Test Venue approve by id URL ::" + url);
			BaseDTO baseDto = httpService.put(url, oralExamVenue);
			log.info(":: Oral Test Venue approve by id BaseDTO Response ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Oral Test Venue approve by id BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			if (baseDto.getStatusCode() == 32018 || baseDto.getStatusCode() == 32019) {
				log.info(":: Oral test venue record approved successfully ::");
				errorMap.notify(baseDto.getStatusCode());
			} else {
				log.info(":: Oral test venue failed to approve ::");
				errorMap.notify(baseDto.getStatusCode());
				return null;
			}
			loadLazyDataModel();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while approve oral test venue ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		add = true;
		return "/pages/personnelHR/listOralExamVenue.xhtml?faces-redirect=true";
	}

	public void onRowSelect(SelectEvent event) {
		selectedOralExamVenue = ((OralExamVenue) event.getObject());

		if (selectedOralExamVenue.getStatus().equalsIgnoreCase("Approved")) {
			add = false;
			delete = false;
			approve = false;
			edit = false;
		} else if (selectedOralExamVenue.getStatus().equalsIgnoreCase("Rejected")) {
			add = false;
			approve = false;
			delete = true;
			edit = false;
		} else if (selectedOralExamVenue.getStatus().equalsIgnoreCase("InProgress")) {
			add = false;
			edit = true;
			approve = true;
			delete = true;
			view = true;
		}
	}

	public void validateMobileNo(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validate method");

		String mobileNo = (String) value;
		Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		Matcher matcher = pattern.matcher(mobileNo);
		if (!matcher.matches()) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Inavlid primary contact number.");
			context.addMessage(comp.getClientId(context), message);
		}

	}

	public void validateMobileNum(ValueChangeEvent event) {
		String mobileNo = (String) ((UIOutput) event.getSource()).getValue();
		// String number = "782223456";
		Pattern pattern = Pattern.compile("^[789]\\d{9}$");
		Matcher matcher = pattern.matcher(mobileNo);
		if (matcher.matches()) {
			log.info("Phone Number Valid");
		} else {
			log.info("Phone Number invalid");
			FacesMessage message = new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Mobile number is not valid.");
			message.setDetail("Mobile number not valid.");
			FacesContext.getCurrentInstance().addMessage("oralVenueForm:oralVenuePanel", message);
			((UIInput) event.getComponent()).setLocalValueSet(false);
		}

	}
}
