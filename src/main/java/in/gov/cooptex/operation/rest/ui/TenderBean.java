package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TenderDTO;
import in.gov.cooptex.core.dto.TenderItemDetailsDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.ContractForm;
import in.gov.cooptex.core.model.CurrencyMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.TenderCategory;
import in.gov.cooptex.core.model.TenderType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.operation.dto.ProductGroupMasterDTO;
import in.gov.cooptex.operation.dto.ProductVarietyMasterDTO;
import in.gov.cooptex.operation.tender.model.Tender;
import in.gov.cooptex.operation.tender.model.TenderFileDetails;
import in.gov.cooptex.operation.tender.model.TenderItemDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("tenderBean")
public class TenderBean {

	private final String CREATE_PAGE = "/pages/operation/tender/createTender.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/operation/tender/listTender.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/operation/tender/viewTender.xhtml?faces-redirect=true";

	private final String LIST_URL = "/tender/getall";
	private final String CREATE_URL = "/tender/create";
	private final String DELETE_URL = "/tender/delete";
	private final String VIEW_URL = "/tender/view";

	private final String TENDERTYPE_LIST_URL = "/tenderType/getallactive";
	private final String TENDERCATEGORY_LIST_URL = "/tenderCategory/getallactive";
	private final String CONTRACTFORM_LIST_URL = "/contractForm/getallactive";
	private final String ACCOUNTHEAD_LIST_URL = "/glAccountHead/getAll";
	private final String PAYMENTMODE_LIST_URL = "/paymentmode/load";
	private final String PRODUCT_LIST_URL = "/category/getAll";
	private final String PRODUCTGROUP_LIST_URL = "/group/pgroup/getallProductGroup/";
	private final String CURRENCYMASTER_LIST_URL = "/currencyMaster/getallactive";
	private final String PRODUCTVARITY_LIST_URL = "/productvariety/getallProductVarity/";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	private String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	TenderDTO tenderDto;

	@Getter
	@Setter
	Tender tender, selectedTender;

	@Autowired
	private AddressMasterBean addressMasterBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private AddressMaster adressMaster;

	@Getter
	@Setter
	ProductCategoryDTO productCategoryDTO = new ProductCategoryDTO();

	@Getter
	@Setter
	ProductGroupMasterDTO productGroupMasterDTO = new ProductGroupMasterDTO();

	@Getter
	@Setter
	ProductVarietyMasterDTO productVarietyMasterDTO = new ProductVarietyMasterDTO();

	@Getter
	@Setter
	TenderItemDetailsDTO tenderItemDetailsDTO = new TenderItemDetailsDTO();

	@Getter
	@Setter
	TenderFileDetails tenderFileDetails = new TenderFileDetails();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<Tender> tenderList = new ArrayList<Tender>();

	@Getter
	@Setter
	List<TenderType> tenderTypelist = new ArrayList<TenderType>();

	@Getter
	@Setter
	List<TenderCategory> tenderCategorylist = new ArrayList<TenderCategory>();

	@Getter
	@Setter
	List<ContractForm> contracFormlist = new ArrayList<ContractForm>();

	@Getter
	@Setter
	List<GlAccount> accountHeadList = new ArrayList<GlAccount>();

	@Getter
	@Setter
	List<PaymentMode> paymentModeList = new ArrayList<PaymentMode>();

	@Getter
	@Setter
	List<Long> noOfCoverAndnoOfBidOpenersList = new ArrayList<Long>();

	@Getter
	@Setter
	List<ProductCategoryDTO> productCategoryDTOList = new ArrayList<ProductCategoryDTO>();

	@Getter
	@Setter
	List<ProductGroupMasterDTO> productGroupMasterDTOList = new ArrayList<ProductGroupMasterDTO>();

	@Getter
	@Setter
	List<CurrencyMaster> currencyMasterList = new ArrayList<CurrencyMaster>();

	@Getter
	@Setter
	List<ProductVarietyMasterDTO> productVarietyMasterDTOList = new ArrayList<ProductVarietyMasterDTO>();

	@Getter
	@Setter
	List<TenderItemDetailsDTO> tenderItemListDTO = new ArrayList<TenderItemDetailsDTO>();

	@Getter
	@Setter
	List<TenderFileDetails> tenderFileList = new ArrayList<TenderFileDetails>();

	@Getter
	@Setter
	LazyDataModel<Tender> lazyTenderList;

	@Getter
	@Setter
	private String address = "";

	@Getter
	@Setter
	private String preBidMeetingAddress = "";

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Setter
	@Getter
	String fileName;

	@Getter
	@Setter
	TenderDTO temptenderDto;

	@Getter
	@Setter
	private int tenderListSize;

	@Getter
	@Setter
	private int tenderTypeListSize;

	@Getter
	@Setter
	private String isAddress;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Double totalAmount;

	@Getter
	@Setter
	AddressMaster bidopeningAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster bidMeetingAddress = new AddressMaster();

	boolean forceLogin = false;

	@Getter
	@Setter
	Double itemTotalAmount;

	@PostConstruct
	public void init() {
		log.info(" tenderBean init --->" + serverURL);

		// addTender();
		// getTenderView(selectedTender);

		// babu
		getAllTenderDroupDownList();
		loadTenderListPage();

	}

	public String loadTenderListPage() {
		try {
			getAllTenderList();
		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;

	}

	public String showList() {
		log.info("<---------- Loading tenderBean showList list page---------->");
		selectedTender = new Tender();
		addButtonFlag = true;
		return LIST_PAGE;
	}

	public String addTender() {
		log.info("<---- Start tenderBean.addTender ---->");
		try {
//			if (temptenderDto == null) {
			tenderDto = new TenderDTO();
			address = "";
			preBidMeetingAddress = "";
			fileName = "";
			tenderItemListDTO = new ArrayList<TenderItemDetailsDTO>();
			tenderFileList = new ArrayList<TenderFileDetails>();
			action = "ADD";
//			}
			log.info("<---- End Tender.addTender ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	private void getAllTenderList() {
		log.info("<===== Starts TenderBean.lazyTenderList ======>");
		lazyTenderList = new LazyDataModel<Tender>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<Tender> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + LIST_URL;
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						tenderList = mapper.readValue(jsonResponse, new TypeReference<List<Tender>>() {
						});
						this.setRowCount(response.getTotalRecords());
						tenderListSize = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in loadLazyBloodGroupList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tenderList;
			}

			@Override
			public Object getRowKey(Tender res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Tender getRowData(String rowKey) {
				try {
					for (Tender tenderObj : tenderList) {
						if (tenderObj.getId().equals(Long.valueOf(rowKey))) {
							selectedTender = tenderObj;
							return tenderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TenderBean.lazyTenderList ======>");
	}

	public String cancel() {
		log.info("<----Redirecting to Tender List page---->");
		return showList();
	}

	public String submit() {
		log.info("<...... TenderBean submit begin ....>#Start");

		tenderDto.setTenderItemDetailsDTOList(tenderItemListDTO);
		tenderDto.setTenderFileDetailsList(tenderFileList);
		try {
			BaseDTO baseDTO = null;
			if (action.equalsIgnoreCase("ADD")) {
				baseDTO = httpService.post(serverURL + CREATE_URL, tenderDto);
			} else if (action.equalsIgnoreCase("EDIT")) {
				baseDTO = httpService.post(serverURL + "/tender/update", tenderDto);
			}
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("TenderBean saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.TENDER_CREATED_SUCCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.TENDER_UPDATED_SUCCESSFULLY.getCode());
					showList();
					// errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating Tender" + e);
		}
		log.info("...... TenderBean submit ended ....");
		return LIST_PAGE;
	}

	public String editTender() {
		log.info("<<<<< -------Start editTender called ------->>>>>> ");
		action = "EDIT";

		if (selectedTender == null) {
			Util.addWarn("Please select one Tender");
			return null;
		} else if (selectedTender.getActiveStatus().equals("ACTIVE")) {
			selectedTender.setActiveStatus(selectedTender.getActiveStatus());
		} else {
			selectedTender.setActiveStatus(selectedTender.getActiveStatus());
		}

		if (selectedTender != null) {
			address = "";
			preBidMeetingAddress = "";
			tenderItemListDTO = new ArrayList<TenderItemDetailsDTO>();
			tenderFileList = new ArrayList<TenderFileDetails>();
			// getAllTenderDroupDownList();
			getTenderEdit(selectedTender);
		}
		log.info("<<<<< -------End editTender called ------->>>>>> ");

		return CREATE_PAGE;

	}

	public String deleteTender() {
		try {
			action = "Delete";
			if (selectedTender == null) {
				Util.addWarn("Please select one Tender");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteTenderConfirm() {

		try {
			log.info("Delete confirmed for tender [" + selectedTender.getTenderName() + "]");

			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + DELETE_URL;
			baseDTO = httpService.post(url, selectedTender);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 33810) {
					log.info("Tender deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					// tenderDto = new TenderDTO();
				} else {
					log.info("Error occured in Rest api ... ");
					errorMap.notify(ErrorDescription.TENDER_DELETED_SUCCESSFULLY.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		addButtonFlag = true;
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start TenderBean . clear ----->");
		getAllTenderList();
		tenderDto = new TenderDTO();
		log.info("<---- End TenderBean . clear ----->");

		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewTender() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (selectedTender == null) {
			Util.addWarn("Please Select One Tender");
			return null;
		}
		getTenderView(selectedTender);
		return VIEW_PAGE;

	}

	private void getTenderView(Tender tender) {
		log.info("<<<< ----------Start tenderBean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + VIEW_URL;
			baseDTO = httpService.post(url, tender);
			selectedTender = new Tender();
			Double totalAmount = 0.0;

			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				selectedTender = mapper.readValue(jsonResponse, new TypeReference<Tender>() {
				});

				bidopeningAddress = selectedTender.getBidopeningAddressId();
				log.info("bidopeningAddress id==> " + bidopeningAddress.getId());

				bidMeetingAddress = selectedTender.getBidMeetingAddressId();

				log.info("bidMeetingAddress id==> " + bidMeetingAddress.getId());

				if (selectedTender.getBidopeningAddressId() != null) {
					address = addressMasterBean.prepareAddress(selectedTender.getBidopeningAddressId());
				}
				if (selectedTender.getBidMeetingAddressId() != null) {
					preBidMeetingAddress = addressMasterBean.prepareAddress(selectedTender.getBidMeetingAddressId());
				}

			}
			for (TenderItemDetails item : selectedTender.getTenderItemDetailsList()) {
				totalAmount += item.getItemAmount();
			}
			selectedTender.setTotalAmount(totalAmount);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (selectedTender != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End TenderBean-getTenderView ------- >>>>");
	}

	private void getAllTenderDroupDownList() {
		log.info("<<<< ----------Start tenderBean-getAllTenderTypeList() ------- >>>>");

		try {
			getAllNoofCoverAndBidOpeners();

			BaseDTO baseDTO = new BaseDTO();
			BaseDTO tenderCategorybaseDTO = new BaseDTO();
			BaseDTO accountHeadbaseDTO = new BaseDTO();
			BaseDTO paymentModebaseDTO = new BaseDTO();
			BaseDTO productbaseDTO = new BaseDTO();
			BaseDTO contractFormbaseDTO = new BaseDTO();

			String tenderTypeUrl = serverURL + TENDERTYPE_LIST_URL;
			String tenderCategoryurl = serverURL + TENDERCATEGORY_LIST_URL;
			String accountHeadurl = serverURL + appPreference.getOperationApiUrl() + "/glAccountHead/getAll";
			String paymentModeurl = serverURL + PAYMENTMODE_LIST_URL;
			String productCategoryurl = serverURL + PRODUCT_LIST_URL;
			String contractFormurl = serverURL + CONTRACTFORM_LIST_URL;

			log.info("getAllTenderDroupDownList :: tenderTypeUrl==> " + tenderTypeUrl);
			baseDTO = httpService.get(tenderTypeUrl);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderTypelist = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
				});
				/*
				 * if(tenderTypelist != null && !tenderTypelist.isEmpty()) { log. }else {
				 * 
				 * }
				 */
			}

			log.info("getAllTenderDroupDownList :: tenderCategoryurl==> " + tenderCategoryurl);
			tenderCategorybaseDTO = httpService.get(tenderCategoryurl);
			if (tenderCategorybaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(tenderCategorybaseDTO.getResponseContents());
				tenderCategorylist = mapper.readValue(jsonResponse, new TypeReference<List<TenderCategory>>() {
				});
			}

			log.info("getAllTenderDroupDownList :: accountHeadurl==> " + accountHeadurl);
			accountHeadbaseDTO = httpService.get(accountHeadurl);
			if (accountHeadbaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(accountHeadbaseDTO.getResponseContents());
				accountHeadList = mapper.readValue(jsonResponse, new TypeReference<List<GlAccount>>() {
				});
			}

			log.info("getAllTenderDroupDownList :: paymentModeurl==> " + paymentModeurl);
			paymentModebaseDTO = httpService.get(paymentModeurl);
			if (paymentModebaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(paymentModebaseDTO.getResponseContents());
				paymentModeList = mapper.readValue(jsonResponse, new TypeReference<List<PaymentMode>>() {
				});
			}
			/*
			 * if (accountHeadbaseDTO != null) { ObjectMapper mapper = new ObjectMapper();
			 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			 * String jsonResponse =
			 * mapper.writeValueAsString(accountHeadbaseDTO.getResponseContents());
			 * accountHeadList = mapper.readValue(jsonResponse, new
			 * TypeReference<List<GlAccount>>() { }); }
			 */

			log.info("getAllTenderDroupDownList :: productCategoryurl==> " + productCategoryurl);
			productbaseDTO = httpService.get(productCategoryurl);
			if (productbaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(productbaseDTO.getResponseContents());
				productCategoryDTOList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryDTO>>() {
				});

			}

			log.info("getAllTenderDroupDownList :: contractFormurl==> " + contractFormurl);
			contractFormbaseDTO = httpService.get(contractFormurl);
			if (contractFormbaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(contractFormbaseDTO.getResponseContents());
				contracFormlist = mapper.readValue(jsonResponse, new TypeReference<List<ContractForm>>() {
				});
			}

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderTypelist != null) {
			tenderTypeListSize = tenderTypelist.size();
		} else {
			tenderTypeListSize = 0;
		}
		log.info("<<<< ----------End TenderBean . getAllTenderTypeList ------- >>>>");
	}

	public void loadCurrencyMasterList() {
		BaseDTO currencyMasterbaseDTO = new BaseDTO();

		try {
			String currencyMasterurl = serverURL + CURRENCYMASTER_LIST_URL;

			log.info("getAllTenderDroupDownList :: currencyMasterurl==> " + currencyMasterurl);
			currencyMasterbaseDTO = httpService.get(currencyMasterurl);
			if (currencyMasterbaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(currencyMasterbaseDTO.getResponseContents());
				currencyMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CurrencyMaster>>() {
				});
			}

			for (CurrencyMaster currencyMaster : currencyMasterList) {
				if ("false".equalsIgnoreCase(tenderDto.getMulticurrencyAllowed().toString())) {
					if (currencyMaster.getCode().equalsIgnoreCase("INR")) {
						currencyMasterList.clear();
						currencyMasterList.add(currencyMaster);
					}

				}

			}

		} catch (Exception e) {

		}

	}

	private void getAllContractFormList() {
		log.info("<<<< ----------Start tenderBean-getAllTenderTypeList() ------- >>>>");

		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + CONTRACTFORM_LIST_URL;
			baseDTO = httpService.get(url);
			tenderDto = new TenderDTO();

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderTypelist = mapper.readValue(jsonResponse, new TypeReference<List<TenderType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderTypelist != null) {
			tenderTypeListSize = tenderTypelist.size();
		} else {
			tenderTypeListSize = 0;
		}
		log.info("<<<< ----------End TenderBean . getAllTenderTypeList ------- >>>>" + tenderTypelist.get(0).getCode());
	}

	private void getAllNoofCoverAndBidOpeners() {
		log.info("<<<< ----------Start tenderBean-getAllNoofCoverAndBidOpeners() ------- >>>>");
		try {
			for (Long j = 1L; j < 10; j++) {
				noOfCoverAndnoOfBidOpenersList.add(j);
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------End TenderBean . getAllNoofCoverAndBidOpeners() ------- >>>>"
				+ noOfCoverAndnoOfBidOpenersList.get(0));
	}

	public void getProductGroupList() {
		log.info("<<======  getProductGroupList ==##  STARTS");
		try {
			BaseDTO baseDTO = new BaseDTO();
			productGroupMasterDTOList = new ArrayList<ProductGroupMasterDTO>();
			String url = serverURL + PRODUCTGROUP_LIST_URL + productCategoryDTO.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productGroupMasterDTOList = mapper.readValue(jsonResponse,
					new TypeReference<List<ProductGroupMasterDTO>>() {
					});
		} catch (Exception e) {
			log.info("<<===  Error in getProductGroupList :: " + e);
		}
		log.info("<<======  getProductGroupList ==##  ENDS");
	}

	public void getItemList() {
		log.info("<<======  getItemList ==##  STARTS");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + PRODUCTVARITY_LIST_URL + productGroupMasterDTO.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productVarietyMasterDTOList = mapper.readValue(jsonResponse,
					new TypeReference<List<ProductVarietyMasterDTO>>() {
					});
		} catch (Exception e) {
			log.info("<<===  Error in getItemList :: " + e);
		}
		log.info("<<======  getItemList ==##  ENDS");
	}

	public void addTenderItemDetails() {
		log.info("<<======  addTenderItemDetails ==##  STARTS");
		try {
			itemTotalAmount = 0.0;

			Double totalAmount = 0.0;
			productGroupMasterDTO.setProductCategoryDTO(productCategoryDTO);
			productVarietyMasterDTO.setProductGroupMasterDTO(productGroupMasterDTO);
			tenderItemDetailsDTO.setProductVarityMasterDTO(productVarietyMasterDTO);
			tenderItemListDTO.add(tenderItemDetailsDTO);
			for (TenderItemDetailsDTO tenderItem : tenderItemListDTO) {
				totalAmount += tenderItem.getItemAmount() * tenderItem.getItemQuantity();
			}
			tenderDto.setTotalAmount(totalAmount);
			productCategoryDTO = new ProductCategoryDTO();
			productGroupMasterDTO = new ProductGroupMasterDTO();
			productVarietyMasterDTO = new ProductVarietyMasterDTO();
			tenderItemDetailsDTO = new TenderItemDetailsDTO();
			itemTotalAmount = 0.0;
		} catch (Exception e) {
			log.info("<<===  Error in addTenderItemDetails :: " + e);
		}
		log.info("<<======  addTenderItemDetails ==##  ENDS"
				+ tenderItemListDTO.get(0).getProductVarityMasterDTO().getName());
	}

	public void saveAddress() {
		log.info("<<======  saveAddress ==##  STARTS");
		try {
			if (isAddress.equalsIgnoreCase("OK")) {
				log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
				address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
				tenderDto.setBidopeningAddressId(addressMasterBean.getAddressMaster());
				addressMasterBean.setAddressMaster(new AddressMaster());
			} else {
				log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
				preBidMeetingAddress = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
				tenderDto.setBidMeetingAddressId(addressMasterBean.getAddressMaster());
			}

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('addressDialog').hide();");
			context.update("tenderForm");
		} catch (Exception e) {
			log.info("<<===  Error in addTenderItemDetails :: " + e);
		}
		log.info("<<======  saveAddress ==##  END");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TenderBean.onRowSelect ========>" + event);
		selectedTender = ((Tender) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends TenderBean.onRowSelect ========>");
	}

	public void fileUpload(FileUploadEvent event) {
		log.info(" <------Tender FileUpload Method Start--->");

		try {
			tenderFileDetails = new TenderFileDetails();
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadedFile = event.getFile();
			long size = uploadedFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > 1000) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String docPathName = commonDataService.getAppKeyValue("TENDER_FILE_UPLOAD_PATH");

			String filePath = Util.fileUpload(docPathName, uploadedFile);
			setFileName(uploadedFile.getFileName());
			tenderFileDetails.setFileType(filePath.substring(filePath.lastIndexOf(".")));
			tenderFileDetails.setFilePath(filePath);
			tenderFileDetails.setFileName(uploadedFile.getFileName());
			tenderFileList.add(tenderFileDetails);
			errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());

		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}
		// errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE.getErrorCode());
		log.info(" Tender File  is uploaded successfully" + tenderFileList.get(0).getFileName());
	}

	public void uploadTenderItemDetails(FileUploadEvent event) {
		log.info(" <------Tender uploadTenderItemDetails Method Start--->");
//		
//
//		try {
//			tenderFileDetails = new TenderFileDetails();
//			if (event == null || event.getFile() == null) {
//				log.error(" Upload Document is null ");
//				return;
//			}
//			uploadedFile = event.getFile();
//			long size = uploadedFile.getSize();
//			log.info("uploadSignature size==>" + size);
//			
//			size = size / 1000;
////			FileInputStream excelFile = new FileInputStream(new File("E:\\file\\Excel\\"+uploadedFile.getFileName()));
//			FileInputStream excelFile = new FileInputStream(new File("D:\\file\\Excel\\new.xlsx"));
//			
//			  Workbook workbook = new XSSFWorkbook(excelFile);
//			  
//			  Sheet datatypeSheet = workbook.getSheetAt(0);
//			  
//			  Iterator<Row> iterator = datatypeSheet.iterator();
//			  
//			  
//			   while (iterator.hasNext()) {
//
//	                Row currentRow = iterator.next();
//	                Iterator<Cell> cellIterator = currentRow.iterator();
//
//	                while (cellIterator.hasNext()) {
//
//	                    Cell currentCell = cellIterator.next();
//	                    //getCellTypeEnum shown as deprecated for version 3.15
//	                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
//	                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
//	                        System.out.print(currentCell.getStringCellValue() + "--");
//	                    } else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
//	                        System.out.print(currentCell.getNumericCellValue() + "--");
//	                    }
//	                    
//
//	                }
//	                System.out.println();
//
//	            }
//
//		} catch (Exception exp) {
//			log.info("<<===  Error in fileUpload :: " + exp);
//		}
//		// errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE.getErrorCode());
//		log.info(" Tender File  is uploaded successfully" + tenderFileList.get(0).getFileName());
	}

	private void getTenderEdit(Tender tender) {
		log.info("<<<< ----------Start tenderBean . getTenderView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/tender/getupdatedata";
			baseDTO = httpService.post(url, tender);
			tenderDto = new TenderDTO();
			if (baseDTO != null) {

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tenderDto = mapper.readValue(jsonResponse, new TypeReference<TenderDTO>() {
				});
			}
			if (tenderDto.getBidopeningAddressId() != null) {
				address = addressMasterBean.prepareAddress(tenderDto.getBidopeningAddressId());
			}
			if (tenderDto.getBidMeetingAddressId() != null) {
				preBidMeetingAddress = addressMasterBean.prepareAddress(tenderDto.getBidMeetingAddressId());
			}

			tenderItemListDTO.addAll(tenderDto.getTenderItemDetailsDTOList());

			for (TenderFileDetails tenderFile : tenderDto.getTenderFileDetailsList()) {
				TenderFileDetails tenderFileDetails = new TenderFileDetails();
				setFileName(tenderFile.getFileName());
				tenderFileDetails.setId(tenderFile.getId());
				tenderFileDetails.setFilePath(tenderFile.getFilePath());
				tenderFileDetails.setFileName(tenderFile.getFileName());
				tenderFileDetails.setTender(tenderFile.getTender());
				tenderFileList.add(tenderFileDetails);
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderDto != null) {
			log.info("tender getTenderView");
		} else {
			log.info("tender getTenderView is null");
		}

		log.info("<<<< ----------End TenderBean-getTenderView ------- >>>>");
	}

	public void deleteFile(TenderFileDetails file) {
		log.info("<===Ends TenderBean.deleteFile Start ========>");
		try {

			if (tenderFileList.contains(file)) {
				tenderFileList.remove(file);
			}
			setFileName(tenderFileList.get(0).getFileName());

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<===Ends TenderBean.deleteFile ========>");
	}

	public void deleteItemDetails(TenderItemDetailsDTO tenderItemDetailsDto) {
		log.info("<===Ends TenderBean.deleteItemDetails Start ========>");
		try {
			Double totalAmount = 0.0;
			if (tenderItemListDTO.contains(tenderItemDetailsDto)) {
				tenderItemListDTO.remove(tenderItemDetailsDto);
			}
			for (TenderItemDetailsDTO itemdto : tenderItemListDTO) {
				totalAmount += itemdto.getItemAmount() * itemdto.getItemQuantity();
			}
			tenderDto.setTotalAmount(totalAmount);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<===Ends TenderBean.deleteItemDetails  ========>");
	}

	public void downloadfile(TenderFileDetails fileObj) {
		InputStream input = null;
		try {
			File files = new File(fileObj.getFilePath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void saveValues() {
		log.info("<===Ends TenderBean.saveValues Start ========>");
		try {
			if (tenderDto != null) {
				temptenderDto = tenderDto;
			}
		} catch (Exception exp) {
			log.error("exception in saveValues method------- " + exp);
		}
		log.info("<===Ends TenderBean.saveValues End ========>");
	}

	public void calculateItemAmount() {
		log.info("<===Start TenderBean.calculateItemAmount Start ========>");
		try {
			if (tenderItemDetailsDTO != null) {
				itemTotalAmount = tenderItemDetailsDTO.getItemQuantity() * tenderItemDetailsDTO.getItemAmount();
			}

		} catch (Exception ex) {
			log.error("exception in calculateItemAmount method------- " + ex);
		}
		log.info("<===Ends TenderBean.calculateItemAmount Start ========>");
	}

}
