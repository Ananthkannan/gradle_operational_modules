package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import lombok.Getter;
import lombok.Setter;

@Service("retailProductionPlanLogBean")
@Scope("session")
public class RetailProductionPlanLogBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5112758956475029956L;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

}