package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.admin.petition.model.PetitionerType;
import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.core.accounts.model.InvestmentTypeMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.KNTPlanDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.CountryMasterBean;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class PetitionerTypeBean {
	
	private final String CREATE_PETITIONERTYPE_MASTER_PAGE = "/pages/masters/petitionerType/createPetitionerType.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/petitionerType/listPetitionerType.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/petitionerType/viewPetitionerType.xhtml?faces-redirect=true;";
	private String serverURL = null;
	
	
	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	PetitionerType petitionerType ;

	@Autowired
	AppPreference appPreference;
	
	ObjectMapper mapper;

	String jsonResponse;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	LazyDataModel<PetitionerType> lazyPetitionerTypeList;

	@Getter
	@Setter
	List<PetitionerType> petitionerTypeList = new ArrayList<PetitionerType>();

	@Getter
	@Setter
	private int petitionerTypeListSize;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter
	@Setter
	boolean disableAddButton = false;
	

	boolean forceLogin = false;
	
	
	
	@PostConstruct
	public void init() {
		log.info(" CountryMasterBean init --->" + serverURL);
		petitionerType = new PetitionerType();
		mapper=new ObjectMapper();
		loadValues();
		loadLazypetitionerTypeList();
		
	}
	
	
	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			petitionerType = new PetitionerType();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}
	
	
	public String showList() {
		log.info("<---------- Loading CountryMasterBean showList list page---------->");
		loadValues();
		loadLazypetitionerTypeList();
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;
	}
	
	
	private void getAllPetitionerTypeList() {
		//log.info("<<<< ----------Start countryMasterBean . loadAllUserList ------- >>>>");
		
		
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/api/v1/operation/petitionertype/getall";
			baseDTO = httpService.get(url);
			petitionerType = new PetitionerType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				petitionerTypeList = mapper.readValue(jsonResponse, new TypeReference<List<PetitionerType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (petitionerTypeList != null) {
			petitionerTypeListSize = petitionerTypeList.size();
		} else {
			petitionerTypeListSize = 0;
		}
		//log.info("<<<< ----------End CountryMasterBean . getAllCountriesList ------- >>>>");
	}
	
	
	public String submit() {
		
		log.info("the submit method is called");
		if (petitionerType.getCode() == null ) {
			errorMap.notify(ErrorDescription.COMMUNITY_CODE_EMPTY .getErrorCode());
			return null;
		}
		if (petitionerType.getName() == null || petitionerType.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.CASTE_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (petitionerType.getName().trim().length() < 3 || petitionerType.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.CASTE_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (petitionerType.getLocalName() == null || petitionerType.getLocalName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.CASTE_LNAME_EMPTY.getErrorCode());
			return null;
		}
		
		
		try {
			log.info("...... petitionerType submit begin ....");
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + "/api/v1/operation/petitionertype/create", petitionerType);
			log.info(" petitioner type id"+petitionerType.getId());
			log.info("return from petitioner type  controler"+ baseDTO.toString());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() != 0) {
					log.info("CountryMasterBean saved successfully");
					//showList();
					errorMap.notify(baseDTO.getStatusCode());
				} else if (baseDTO.getStatusCode() == 50238 || baseDTO.getStatusCode() == 50239|| baseDTO.getStatusCode() == 50236){
					log.error(" processing exception occured while converting .... "+ baseDTO.getStatusCode());
					/*log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());*/
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			//log.error("Error while creating CountryMaster" + e);
		}
		log.info("...... CountryMasterBean submit ended ....");
		return INPUT_FORM_LIST_URL;
	}
	
	public String cancel() {
		System.out.println("the cancel method is called");
		log.info("<----Redirecting to user List page---->");
		petitionerType = new PetitionerType();
		disableAddButton = false;
		loadLazypetitionerTypeList();
		return INPUT_FORM_LIST_URL;

	}
	
	public String addPetitionerType() {
		//log.info("<---- Start CountryMasterBean.addCountry ---->");
		try {
			action = "ADD";
			petitionerType = new PetitionerType();
			//petitionerType.setActiveStatus(true);

			log.info("<---- End CountryMaster.addCountry ---->");
		} catch (Exception e) {
			//log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		
		disableAddButton = false;
		return CREATE_PETITIONERTYPE_MASTER_PAGE;

	}
	
	
	public String editPetitionerType() {
		log.info("<<<<< -------Start editCountry called ------->>>>>> ");
		action = "EDIT";

		if (petitionerType == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}

		if (petitionerType.getActiveStatus().equals("ACTIVE")) {
			petitionerType.setActiveStatus(petitionerType.getActiveStatus());
		} else {
			petitionerType.setActiveStatus(petitionerType.getActiveStatus());
		}
		getrecordById();
		log.info("<<<<< -------End editCountry called ------->>>>>> ");
		disableAddButton = false;
		return CREATE_PETITIONERTYPE_MASTER_PAGE;

	}
	
	
	public String viewPetitionerType() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (petitionerType == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}
		getrecordById();
		disableAddButton = false;
		return INPUT_FORM_VIEW_URL;

	}
	
	
	public String deletePetitionerType() {
		try {
			action = "Delete";
			if (petitionerType == null) {
				Util.addWarn("Select Atleast One Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPetitionerTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}
	
	
	public String clear() {
		log.info("<---- Start countryMasterBean . clear ----->");
		loadLazypetitionerTypeList();
		petitionerType = new PetitionerType();
		log.info("<---- End CountryMasterBean . clear ----->");
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;

	}
	
	
	public String deletePetitionerTypeConfirm() {

		try {
			log.info("Delete petitioner type started the id is [" +petitionerType.getId()  + "]");
			PetitionerType petitionerTypeDelete = new PetitionerType();
			petitionerTypeDelete.setId(petitionerType.getId());
			//countryMasterDelete.setId(countryMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/api/v1/operation/petitionertype/delete";
			baseDTO = httpService.post(url, petitionerType);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("Country deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					petitionerType = new PetitionerType();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	
	public String backPage() {

		showList();
		return INPUT_FORM_LIST_URL;
	}
	
	
	/*public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Petitioner type onRowSelect ========>" + event);
		petitionerType = ((PetitionerType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends Asset Category Bean.onRowSelect ========>");
	}*/

	public void loadLazypetitionerTypeList() {
		log.info("<===== Starts petitionertypeBean.loadLazypetitionerTypeList ======>");

		lazyPetitionerTypeList = new LazyDataModel<PetitionerType>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<PetitionerType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/petitionertype/getallpetitionertypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						petitionerTypeList = mapper.readValue(jsonResponse, new TypeReference<List<PetitionerType>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("petitionerTypeBean size---------------->" + lazyPetitionerTypeList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return petitionerTypeList;
			}

			@Override
			public Object getRowKey(PetitionerType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PetitionerType getRowData(String rowKey) {
				for (PetitionerType petitionerTypes : petitionerTypeList) {
					if (petitionerTypes.getId().equals(Long.valueOf(rowKey))) {
						petitionerType = petitionerTypes;
						return petitionerType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  petitionerTypeBean.loadLazySalesTypeList  ======>");
	}

//	public void onRowSelect(SelectEvent event) {
//		log.info("<===Starts PetitionerTypeBean.onRowSelect ========>" + event);
//		selectedPetitionerType = ((PetitionerType) event.getObject());
//		addButtonFlag = false;
//		editButtonFlag = true;
//		deleteButtonFlag = true;
//		log.info("<===Ends PetitionerTypeBean.onRowSelect ========>" + selectedPetitionerType.getId());
//	}
//	
	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		petitionerType = ((PetitionerType) event.getObject());
		disableAddButton = true;
	}

	public void getrecordById() {
		log.info("Inside getrecordById() ");
		BaseDTO baseDTO = null;
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/petitionertype/get/"
					+ petitionerType.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				petitionerType = mapper.readValue(jsonResponse, new TypeReference<PetitionerType>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While getrecordById() :: ", e);
		}
		
		
	}
	
	
	

}
