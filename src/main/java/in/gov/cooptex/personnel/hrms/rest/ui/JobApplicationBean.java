package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ExamCentre;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.JobApplication;
import in.gov.cooptex.core.model.JobApplicationAdditionalInfo;
import in.gov.cooptex.core.model.JobApplicationApproval;
import in.gov.cooptex.core.model.JobApplicationEduQualif;
import in.gov.cooptex.core.model.JobApplicationOrther;
import in.gov.cooptex.core.model.JobApplicationPaymentDetails;
import in.gov.cooptex.core.model.JobApplicationPrimaryKey;
import in.gov.cooptex.core.model.JobApplicationWorkExp;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.model.MotherTongueMaster;
import in.gov.cooptex.core.model.NonPriorityMaster;
import in.gov.cooptex.core.model.PriorityMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.model.SalutationMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.VillageMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("jobApplicationBean")
public class JobApplicationBean {

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private UploadedFile file = null;

	@Getter
	@Setter
	private Date dob;

	@Getter
	@Setter
	private Boolean uploadDisable = false, uploadGovDisable = false, phyChallengedDisable = false, isPriority = false,
			isDesWidow = false, isExService = false, isDisAble = false;

	@Getter
	@Setter
	long age;

	@Getter
	@Setter
	List<GenderMaster> genderList;

	@Getter
	@Setter
	List<MaritalStatus> maritalStatusList;

	@Getter
	@Setter
	List<MotherTongueMaster> motherTongueMasterList;

	@Getter
	@Setter
	DistrictMaster districtMaster;

	@Getter
	@Setter
	List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();

	@Getter
	@Setter
	List<DistrictMaster> totalDistrictMasterList = new ArrayList<DistrictMaster>();

	@Getter
	@Setter
	List<TalukMaster> talukMasterList = new ArrayList<TalukMaster>();

	@Getter
	@Setter
	List<TalukMaster> totalTalukMasterList = new ArrayList<TalukMaster>();

	@Getter
	@Setter
	List<VillageMaster> villageMasterList = new ArrayList<VillageMaster>();

	@Getter
	@Setter
	List<SalutationMaster> salutationList;

	/*
	 * @Getter
	 * 
	 * @Setter EmployeeContactDetails employeeContactDetails = new
	 * EmployeeContactDetails();
	 */

	@Getter
	@Setter
	List<QualificationMaster> qualificationList;

	@Getter
	@Setter
	JobApplication jobApplication = new JobApplication();

	@Getter
	@Setter
	JobApplicationApproval jobApplicationApproval = new JobApplicationApproval();

	@Setter
	@Getter
	JobApplication selectedJobApplication = new JobApplication();

	@Getter
	@Setter
	List<JobApplication> jobApplicationList = new ArrayList<>();

	@Getter
	@Setter
	List<JobAdvertisement> jobAdvertisementList = new ArrayList<>();

	@Getter
	@Setter
	JobApplicationAdditionalInfo jobApplicationAdditionalInfo = new JobApplicationAdditionalInfo();

	@Getter
	@Setter
	JobApplicationEduQualif jobApplicationEduQualif = new JobApplicationEduQualif();

	@Getter
	@Setter
	List<JobApplicationEduQualif> eduQua = new ArrayList<>();

	@Getter
	@Setter
	JobApplicationWorkExp jobApplicationWorkExp = new JobApplicationWorkExp();

	@Getter
	@Setter
	List<JobApplicationWorkExp> workExpList = new ArrayList<>();

	@Getter
	@Setter
	JobApplicationPaymentDetails jobAppPayment = new JobApplicationPaymentDetails();

	@Getter
	@Setter
	JobApplicationOrther jobApplicationOther = new JobApplicationOrther();

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	String signatureFileName, communityCertFileName, pstmFileName, cerFileName, govFileName, paymentFileName,
			expFileName, demandDraftFileName, phyFileName, priFileName, exServiceFileName, disWidowFileName, sigName,
			fileName;

	@Setter
	@Getter
	List<EntityMaster> preExamCentreList;

	@Setter
	@Getter
	ExamCentre examCentre = new ExamCentre();

	@Getter
	@Setter
	private UploadedFile relievingLetterFile, pstmfile, cerfile, govfile, expfile, ddFile, phyChallengedFile, priFile,
			exServiceFile, disWidowFile;

	@Getter
	@Setter
	byte[] photograph;

	@Getter
	@Setter
	byte[] communityCertificatedownloadValue, sigFileNameValue, eduDocFileNameValue, pstmDocFileNameValue,
			nonCerDocFileNameValue, workExpFileNameValue, disWidowFileNameValue, exServiceFileNameValue,
			priFileNamedownloadValue, phyFileNamedownloadValue, jobAppPaymentValue;

	@Getter
	@Setter
	String jobButton, action;

	@Setter
	@Getter
	List<PriorityMaster> priorityMasterList;

	@Setter
	@Getter
	List<NonPriorityMaster> nonPriorityMasterList;

	@Setter
	@Getter
	Designation designation;

	@Setter
	@Getter
	StateMaster state;

	@Getter
	@Setter
	List<GenderMaster> genderMasterList;

	@Getter
	@Setter
	List<Community> communityList;

	@Getter
	@Setter
	String viewFlag;

	@Getter
	@Setter
	boolean disableButton = false;

	@Getter
	@Setter
	int totalRecords = 0;

	public static final String JOB_SERVER_URL = AppUtil.getPortalServerURL() + "/jobapplication";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String url, jsonResponse;

	long employeePhotoSize;

	@Getter
	@Setter
	List<Religion> religionList;

	@Getter
	@Setter
	List<CasteMaster> casteMasterList;

	@Getter
	@Setter
	LazyDataModel<JobApplication> lazyApplicationModel;

	@Getter
	@Setter
	LazyDataModel<JobApplication> lazyAdvAndApplicationModel;

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	List<StateMaster> permanentStateMasterList = new ArrayList<StateMaster>();

	@Getter
	@Setter
	StringBuilder presenetAddress, permanentAddress;
	@Getter
	@Setter
	Date onlineStartDate;
	@Getter
	@Setter
	Date onlineEndDate;

	@Getter
	@Setter
	String presenet, permanent;
	
	@Getter
	@Setter
	Boolean navigationFlag = true;

	@PostConstruct
	public void init() {
		log.info("JobApplicationBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadJobDetailsValues();
		// loadJobApplicationList();
		loadLazyList();
		loadAppConfigValues() ;
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	/**
	 * Purpose : to load the start up value
	 * 
	 * @author krishnakumar
	 */
	public void loadJobDetailsValues() {
		try {
			salutationList = commonDataService.getAllSalutations();
			designationList = commonDataService.loadDesignation();
			stateMasterList = commonDataService.getStateList();
			genderList = commonDataService.getAllGenderType();
			maritalStatusList = commonDataService.getAllMaritalStatus();
			motherTongueMasterList = commonDataService.getMotherTongueList();
			qualificationList = commonDataService.loadQualifications();
			preExamCentreList = commonDataService.getPreExamMasterList();
			totalDistrictMasterList = commonDataService.getDistrictMasterList();
			totalTalukMasterList = commonDataService.getTalukMasterList();
			priorityMasterList = commonDataService.getPriorityMasterList();
			nonPriorityMasterList = commonDataService.getNonPriorityMasterList();
			genderMasterList = commonDataService.getAllGenderType();
			communityList = commonDataService.getAllCommunity();
			religionList = commonDataService.getAllReligion();
			casteMasterList = commonDataService.getAllCasteMaster();
			// randomApplicationNumber();
			getNotficationNumber();
		} catch (Exception e) {
			log.error("Exception in loadJobDetailsValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void getNotficationNumber() {
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data getNotficationNumber");
		try {
			String url = SERVER_URL + "/jobadvertisement/getnotification";
			log.info("getNotficationNumber==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			jobAdvertisementList = mapper.readValue(jsonResponse, new TypeReference<List<JobAdvertisement>>() {
			});
			log.info("jobAdvertisementList size==> " + jobAdvertisementList.size());
		} catch (Exception e) {
			log.error("Exception Occured in JobApplicationApproval search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void changeGender() {
		log.info("jobApplication Change Gender Called----->" + jobApplication);
		if (jobApplication != null && jobApplication.getSalutation() != null
				&& jobApplication.getSalutation().getSalutationName().equalsIgnoreCase("Mr") && genderList.size() > 0) {
			for (GenderMaster gender : genderList) {
				if (gender.getName().equalsIgnoreCase("Male")) {
					jobApplication.setGender(gender);
				}
			}

		} else if (jobApplication != null && jobApplication.getSalutation() != null
				&& (jobApplication.getSalutation().getSalutationName().equalsIgnoreCase("Mrs")
						|| jobApplication.getSalutation().getSalutationName().equalsIgnoreCase("Miss"))
				&& genderList.size() > 0) {
			for (GenderMaster gender : genderList) {
				if (gender.getName().equalsIgnoreCase("Female")) {
					jobApplication.setGender(gender);
				}
			}
		}
	}

	public String loadJobApplicationList() {
		BaseDTO baseDTO = new BaseDTO();
		log.info("Search request data loadJobApprovalList");
		try {
			String url = JOB_SERVER_URL + "/getall";
			log.info("loadJobApplicationList==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			jobApplicationList = mapper.readValue(jsonResponse, new TypeReference<List<JobApplication>>() {
			});
			totalRecords = jobApplicationList.size();
		} catch (Exception e) {
			log.error("Exception Occured in loadJobApplicationList search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return "/pages/personnelHR/listScrutinization.xhtml?faces-redirect=true";
	}

	public void getDes() {
		try {
			if (jobApplication == null) {
				log.error(" Employee Application is Empty ");
				errorMap.notify(ErrorDescription.JOB_APPLICATION_EMPTY.getErrorCode());
			}
			designation = jobApplication.getJobAdvertisement().getRosterReservation().getStaffEligibility()
					.getRecruitmentForPost();
			state = jobApplication.getJobAdvertisement().getRosterReservation().getStaffEligibility().getState();

			log.info("designation==>" + designation);
			log.info("designation==>" + designation.getName());

			log.info("state==>" + state);
			log.info("state==>" + state.getName());

		} catch (Exception e) {
			log.error("Exception Occured in getDes ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * Purpose : to save the job details
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveJobPrimary() {
		log.info("<---- : Save job Details :------>" + jobApplication);
		try {

			if(jobApplicationAdditionalInfo==null) {
				jobApplicationAdditionalInfo = new JobApplicationAdditionalInfo();
			}
			log.info("<---- : Save job Details getSignatureDocPath :------>" + jobApplication.getSignatureDocPath());
			if (jobApplication == null) {
				log.error(" Employee application is empty ");
				errorMap.notify(ErrorDescription.JOB_APPLICATION_EMPTY.getErrorCode());
				return null;
			}
			if (jobApplication.getPhotographPath() == null) {
				log.error("Update faild due to photo is not available");
				errorMap.notify(ErrorDescription.CANDIDATE_PHOTO_NOT_NULL.getErrorCode());
				return null;
			}
			if (jobApplication.getSignatureDocPath() == null) {
				log.error("Update faild due to photo is not available");
				errorMap.notify(ErrorDescription.CANDIDATE_SIGN_NOT_NULL.getErrorCode());
				return null;
			}
			if (!EmployeeValidator.validateEmail(jobApplication.getEmailId())) {
				log.error(" Employee Invalid Email Id ");
				errorMap.notify(ErrorDescription.JOB_PRIMARY_EMAIL_INVALID.getErrorCode());
				return null;
			}
			log.info("jobApplication.getPlaceOfBirth().getName()========>"+jobApplication.getPlaceOfBirth().getName());
			if(jobApplication.getPlaceOfBirth().getName().equals("OTHERS")) {
				jobApplication.setAppliedPost(designation);
				jobApplication.setRegistrationDate(new Date());
				jobApplication.setAppliedState(state);
			}else {
				jobApplication.setOtherPlaceOfBirth(null);
				jobApplication.setAppliedPost(designation);
				jobApplication.setRegistrationDate(new Date());
				jobApplication.setAppliedState(state);
			}
			
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			log.info("jobApp==>" + jobApp);
			if (jobApp != null) {
				jobApplication = jobApp;
			}
			if (baseDTO.getStatusCode() == 0) {
				navigationFlag = false;
				errorMap.notify(ErrorDescription.JOB_PRIMARY_SAVED.getErrorCode());
				return "/pages/personnelHR/createJobApplicationAdditionalInfo.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Purpose : to save the job additional details
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveJobAddition() {
		log.info("<---- : Save job additional Details :------>" + jobApplication);
		log.info("<---- : Save job additional Details :------>" + jobApplicationAdditionalInfo);
		try {
			if (jobApplicationAdditionalInfo.getCommunityCertDoc() == null) {
				log.error("Update faild due to photo is not available");
				errorMap.notify(ErrorDescription.COMMUNITY_CERTIFICATE_NOT_EMPTY.getErrorCode());
				return null;
			}
			jobApplication.setAdditionalInfo(jobApplicationAdditionalInfo);
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			if (jobApp != null) {
				jobApplication = jobApp;
				jobApplicationAdditionalInfo = jobApp.getAdditionalInfo();
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.JOB_ADDITIONAL_INFO_SAVED.getErrorCode());
				return "/pages/personnelHR/createJobApplicationEducation.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	public void clearJobPrimary() {
		jobApplication = new JobApplication();
		// randomApplicationNumber();
		RequestContext.getCurrentInstance().update("jobAppForm:jobAppPanel");
	}

	public StreamedContent getUploadedImage() {
		try {
			if (photograph == null || photograph == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				return new DefaultStreamedContent(
						externalContext.getResourceAsStream("/assets/images/profile-photo.png"), "image/png");
			}
			return new DefaultStreamedContent(new ByteArrayInputStream(photograph), "image/png");
		} catch (Exception e) {
			log.error(" Error in while getting uploaded employee Image", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public String handleFileUpload(FileUploadEvent event) {
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			file = event.getFile();
			log.info(" Emplyee Photo Uploaded file name :: " + file.getFileName());
			log.info("Employee Photo Uploaded file Content Type : " + file.getContentType());
			String type = file.getFileName();
			if (!type.contains("png") && !type.contains("jpg") && !type.contains("gif") && !type.contains("jpeg")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_FORMAT.getErrorCode());
				log.info(ErrorCodeDescription.EMPLOYEE_PHOTO_FORMAT.getErrorDescription());
				return null;
			}
			long size = file.getSize();
			log.info("size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplication.setPhotographPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return null;
			}
			if (size > 0) {
				String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
				log.info("photoPathName==>" + photoPathName);
				String photoPath = photoPathName + "/uploaded/" + file.getFileName();
				Path path = Paths.get(photoPathName + "/uploaded/");
				log.info(" Photo Upload Path  : " + photoPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(path)) {
					Files.createDirectories(path);
				}
				double kilobytes = outputFile.length() / 1024;
				log.info("File kilobytes :" + kilobytes);
				if (kilobytes > 100) {
					errorMap.notify(ErrorDescription.PHOTO_SIZE_INVALID.getErrorCode());
					AppUtil.addWarn("photo shoud be less than 100kb.");
					return null;
				}
				inputStream = file.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) file.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				jobApplication.setPhotographPath(photoPath);
				convertImage();
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Upload Photo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * Purpose : to read the image and convert byte array
	 * 
	 * @author krishnakumar
	 * 
	 */
	public void convertImage() {
		try {
			File file = new File(jobApplication.getPhotographPath());
			photograph = new byte[(int) file.length()];
			FileInputStream fis = new FileInputStream(file);
			fis.read(photograph); // read file into bytes[]
			fis.close();
		} catch (Exception e) {
			log.error("convertImage error==>" + e);
		}
	}

	/**
	 * purpose : to uploaded the image
	 * 
	 * @author krishnakumar
	 * @param event
	 * @return
	 */
	public void uploadSignature(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			String type = FilenameUtils.getExtension(relievingLetterFile.getFileName());
			log.info(" File Type :: " + type);
			if (!type.equalsIgnoreCase("png") && !type.equalsIgnoreCase("jpg") && !type.equalsIgnoreCase("doc") && !type.equalsIgnoreCase("jpeg") && !type.equalsIgnoreCase("gif")
					&& !type.equalsIgnoreCase("docx") && !type.equalsIgnoreCase("pdf") && !type.equalsIgnoreCase("xlsx") && !type.equalsIgnoreCase("xls")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());		 
				return ;
			}
			long size = relievingLetterFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplication.setSignatureDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			log.info("uploadSignature filePath===>" + filePath);
			jobApplication.setSignatureDocPath(filePath);
			signatureFileName = relievingLetterFile.getFileName();
			sigName = relievingLetterFile.getFileName();
			errorMap.notify(ErrorDescription.CAN_SIG_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	/**
	 * purpose : to uploaded the image
	 * 
	 * @author krishnakumar
	 * @param event
	 * @return
	 */
	public void uplaodCommunityCertificate(FileUploadEvent event) {
		log.info("uplaodCommunityCertificate Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.COMMUNITY_CERTIFICATE_NOT_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			long size = relievingLetterFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationAdditionalInfo.setCommunityCertDoc(null);
				jobApplicationAdditionalInfo.setCommunityCertDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			jobApplicationAdditionalInfo.setCommunityCertDoc(relievingLetterFile.getContents());
			jobApplicationAdditionalInfo.setCommunityCertDocPath(filePath);
			communityCertFileName = relievingLetterFile.getFileName();

			errorMap.notify(ErrorDescription.COMMUNITY_CERTIFICATE_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	/**
	 * Purpose : calculate age passing date
	 * 
	 * @author krishnakumar
	 */
	public void employeeAgeCalculation() {
		try {
			log.info("employeeAgeCalculation Method Called :: " + jobApplication.getDateOfBirth());
			Date birthDay = jobApplication.getDateOfBirth();
			age = AppUtil.getAge(birthDay);
			jobApplication.setAge(age);
			log.error(" Employee Family Age :: " + age);

		} catch (Exception e) {
			log.error(" Exception In EmployeeMasterBean employeeAgeCalculation Method ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());

		}
	}

	public void onStateChangePermanent(Long stateId) {
		log.info("Employee Contact Creation inside OnStateChange() Start for Contact Details :: " + stateId);
		try {
			if (stateId == null) {
				log.error("State id is empty");
				errorMap.notify(ErrorDescription.STATE_ID_EMPTY.getErrorCode());
			}
			url = SERVER_URL + "/districtMaster/getalldistrictbystate/" + stateId;
			log.info(" On State Change URL Called :: " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info(" On State Change Response Recieved  :: " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			districtMasterList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
			});

			if (districtMasterList == null) {
				log.error("districtMaster failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			log.info(" District Master List Based on State Id :: " + districtMasterList);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on State Change Permanent :: ", e);
		}
	}

	public void onDistrictChangePermanent(Long desId) {
		log.info("<==== Employee Contact Creation inside onTalukChangePermanent() Start ======>" + desId);

		try {
			if (desId == null) {
				log.error("District id is empty");
				errorMap.notify(ErrorDescription.DISTRICT_ID_IS_EMPTY.getErrorCode());
			}
			if (desId != null) {
				url = SERVER_URL + "/taluk/getbydistrict/" + desId;
				log.info(" On District Change URL Called :: " + url);
				BaseDTO baseDTO = httpService.get(url);
				log.info(" On District Change Response :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				talukMasterList = mapper.readValue(jsonResponse, new TypeReference<List<TalukMaster>>() {
				});

				if (talukMasterList == null) {
					log.error("Taluk Master failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on District Change Permanent :: ", e);
		}
	}

	public void onTalukChangePermanent(Long talukId) {
		log.info("<==== Employee Contact Creation inside onTalukChangePermanent() Start ======>" + talukId);
		try {
			if (talukId == null) {
				log.error("Taluk id is empty");
				errorMap.notify(ErrorDescription.TALUK_ID_IS_EMPTY.getErrorCode());
			}

			url = SERVER_URL + "/village/getallvillagebytaluk/" + talukId;
			log.info(" On Taluk Change URL Called :: " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info(" On Taluk Change Response :: " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

			villageMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VillageMaster>>() {
			});

			if (villageMasterList == null) {
				log.error("Village Master failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Village Masters Rerived Successfully " + villageMasterList);
			} else {
				log.info(" :: Village Masters Failed To Retrive with Status Code :: ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error(" Exception Occured on Taluk Change Permanent :: ", e);
		}

	}

	/**
	 * Purpose : to calculate past year
	 * 
	 * @author krishnakumar
	 * @param
	 * @return Date
	 */
	public Date getPastYearDate() {
		Date date = getCurrentDate();
		return date;
	}

	public Date getCurrentDate() {
		Date currentDate = new Date();
		currentDate.setYear(currentDate.getYear() - 18);
		return currentDate;
	}

	/**
	 * Purpose : to clear the job application additional information
	 * 
	 * @author krishnakumar
	 */
	public void clearAdditional() {
		jobApplicationAdditionalInfo = new JobApplicationAdditionalInfo();
		RequestContext.getCurrentInstance().update("jobAddForm:jobAddPanel");
	}

	public void copyAddress() {
		log.info("Inside ContactDetails copyAddress for employee Contact"
				+ jobApplicationAdditionalInfo.isPermanentAndPresent());
		try {

			log.info("copyAddress jobApplicationAdditionalInfo==>" + jobApplicationAdditionalInfo);
			if (jobApplicationAdditionalInfo.isPermanentAndPresent()) {

				if (jobApplicationAdditionalInfo == null) {
					log.error(" Employee Contact Details is Null ");
					errorMap.notify(ErrorDescription.EMP_CONTACT_DETAILS_IS_EMPTY.getErrorCode());
				}

				log.info("copyAddress jobApplicationAdd==>" + jobApplicationAdditionalInfo);
				log.info("copyAddress getPermAddressLine1==>" + jobApplicationAdditionalInfo.getPermAddressLine1());
				log.info("copyAddress getPresentAddressLine1==>"
						+ jobApplicationAdditionalInfo.getPresentAddressLine1());
				jobApplicationAdditionalInfo.setPermAddressLine1(jobApplicationAdditionalInfo.getPresentAddressLine1());
				jobApplicationAdditionalInfo.setPermAddressLine2(jobApplicationAdditionalInfo.getPresentAddressLine2());
				jobApplicationAdditionalInfo.setPermAddressLine3(jobApplicationAdditionalInfo.getPresentAddressLine3());
				jobApplicationAdditionalInfo.setPermPincode(jobApplicationAdditionalInfo.getPresentPincode());
				jobApplicationAdditionalInfo.setPermState(jobApplicationAdditionalInfo.getPresentState());
				jobApplicationAdditionalInfo.setPermDistrict(jobApplicationAdditionalInfo.getPresentDistrict());
				jobApplicationAdditionalInfo.setPermTaluk(jobApplicationAdditionalInfo.getPresentTaluk());
				jobApplicationAdditionalInfo.setPermVillage(jobApplicationAdditionalInfo.getPresentVillage());

			} else {
				jobApplicationAdditionalInfo.setPermAddressLine1(null);
				jobApplicationAdditionalInfo.setPermAddressLine2(null);
				jobApplicationAdditionalInfo.setPermAddressLine3(null);
				jobApplicationAdditionalInfo.setPermPincode(null);
				jobApplicationAdditionalInfo.setPermState(null);
				jobApplicationAdditionalInfo.setPermDistrict(null);
				jobApplicationAdditionalInfo.setPermTaluk(null);
				jobApplicationAdditionalInfo.setPermVillage(null);
			}
		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * purpose : to add education qulification value
	 * 
	 * @author krishnakumar
	 * 
	 */
	public void addEduQulif() {
		log.info("addEduQulif Start==>" + jobApplicationEduQualif);
		log.info("addEduQulif Start==>" + jobApplicationEduQualif.getEducationalCertificateDocPath());
		if (jobApplicationEduQualif.getEducationalCertificateDocPath() == null) {
			log.error("Update faild due to education doc is not available");
			errorMap.notify(ErrorDescription.EDU_UPLOAD_DOC.getErrorCode());
			return;
		}

		if (jobApplicationEduQualif.getPercentageCgpa() > 100) {
			log.error("Educational qualification percentage not greater than 100");
			errorMap.notify(ErrorDescription.EDU_QUA_PER.getErrorCode());
			return;
		}
		// eduQua.add(jobApplicationEduQualif);
		if (jobApplicationEduQualif != null) {
			// for(JobApplicationEduQualif eduQualif : jobApplicationEduQualif) {
			String filePathValue[] = jobApplicationEduQualif.getEducationalCertificateDocPath().split("uploaded/");
			log.info("Edu 0==>" + filePathValue[0]);
			log.info("Edu 1==>" + filePathValue[1]);
			jobApplicationEduQualif.setPstmCertDocName(filePathValue[1]);
			eduQua.add(jobApplicationEduQualif);
			// }
		}
		jobButton = "ADDJOB";
		jobApplicationEduQualif = new JobApplicationEduQualif();
		log.info("addEduQulif End==>");
	}

	public void updateEduQulif() {
		log.info(" :: Upadate - EduQulif Action :: ");
		try {
			log.info("updateEduQulif Start==>" + jobApplicationEduQualif);
			if (jobApplicationEduQualif == null) {
				log.error(" :: Update - Employee jobApplicationEduQualif Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (jobApplicationEduQualif.getPercentageCgpa() > 100) {
				log.error("Educational qualification percentage not greater than 100");
				errorMap.notify(ErrorDescription.EDU_QUA_PER.getErrorCode());
				jobApplicationEduQualif.setPercentageCgpa(null);
				return;
			}
			if (jobApplicationEduQualif.getEducationalCertificateDocPath() == null) {
				log.error("Update faild due to education doc is not available");
				errorMap.notify(ErrorDescription.EDU_UPLOAD_DOC.getErrorCode());
				return;
			}
			if (eduQua.size() > 0) {
				for (JobApplicationEduQualif deatails : eduQua) {

					// for(JobApplicationEduQualif eduQualif : jobApplicationEduQualif) {
					String filePathValue[] = deatails.getEducationalCertificateDocPath().split("uploaded/");
					log.info("Edu 0==>" + filePathValue[0]);
					log.info("Edu 1==>" + filePathValue[1]);
					jobApplicationEduQualif.setPstmCertDocName(filePathValue[1]);

					// }

					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(jobApplicationEduQualif.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						eduQua.set(deatails.getRowIndex(), jobApplicationEduQualif);
					}
				}
			} else {
				log.info(" :: Updated - Employee Disciplinary Action :: ");
				eduQua.add(jobApplicationEduQualif);
			}
			jobApplicationEduQualif = new JobApplicationEduQualif();
			jobButton = "ADDJOB";
			RequestContext.getCurrentInstance().update("jobEduForm:jobEduPanel");
		} catch (Exception e) {
			log.error("Exception occured while Update updateEduQulif Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void addWorkExp() {
		log.info("addWorkExp Start==>" + jobApplicationWorkExp);
		if (jobApplicationWorkExp.getWorkFrom().after(jobApplicationWorkExp.getWorkTo())) {
			Util.addWarn("Work TO date Should greater than Work From date");
			return;
		}
		if (jobApplicationWorkExp.getWorkExpDocPath() == null) {
			Util.addWarn("Work experience document is mandatory");
			return;
		}
		workExpList.add(jobApplicationWorkExp);
		jobButton = "ADDJOB";
		jobApplicationWorkExp = new JobApplicationWorkExp();
		log.info("addEduQulif End==>");
	}

	public void updateWorkExp() {
		log.info(" :: Upadate - updateWorkExp Action :: ");
		try {
			if (jobApplicationWorkExp.getWorkFrom().after(jobApplicationWorkExp.getWorkTo())) {
				Util.addWarn("Work TO date Should greater than Work From date");
				return;
			}
			if (jobApplicationWorkExp == null) {
				log.error(" :: Update - Employee updateWorkExp Action Object null ::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (workExpList.size() > 0) {
				for (JobApplicationWorkExp deatails : workExpList) {
					if (deatails.getRowIndex() != null
							&& deatails.getRowIndex().equals(jobApplicationEduQualif.getRowIndex())) {
						log.info(" :: Updated - Employee Disciplinary Action :: ");
						workExpList.set(deatails.getRowIndex(), jobApplicationWorkExp);
					}
				}
			} else {
				log.info(" :: Updated - Employee updateWorkExp Action :: ");
				workExpList.add(jobApplicationWorkExp);
			}
			jobApplicationWorkExp = new JobApplicationWorkExp();
			jobButton = "ADDJOB";
			RequestContext.getCurrentInstance().update("jobWorkForm:jobWorkPanel");
		} catch (Exception e) {
			log.error("Exception occured while Update updateWorkExp Action", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearEdu() {
		log.info(":: Clear - Education");
		jobApplicationEduQualif = new JobApplicationEduQualif();
		RequestContext.getCurrentInstance().update("jobEduForm:jobEduPanel");
	}

	public void clearWorkExp() {
		log.info(":: Clear - work experiance");
		jobApplicationWorkExp = new JobApplicationWorkExp();
		RequestContext.getCurrentInstance().update("jobWorkForm:jobWorkPanel");
	}

	public void uploadPstmFile(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.PSTM_DOC_EMPTY.getErrorCode());
				return;
			}
			pstmfile = event.getFile();
			long size = pstmfile.getSize();
			log.info("uploadPstmFile size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationEduQualif.setPstmCertDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, pstmfile);
			jobApplicationEduQualif.setPstmCertDocPath(filePath);
			pstmFileName = pstmfile.getFileName();
			errorMap.notify(ErrorDescription.PSTM_DOC_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void uploadGovFile(FileUploadEvent event) {
		log.info("uploadGovFile button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.GOV_DOC_EMPTY.getErrorCode());
				return;
			}
			govfile = event.getFile();
			long size = govfile.getSize();
			log.info("uploadGovFile size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationWorkExp.setNocCertDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, govfile);
			jobApplicationWorkExp.setNocCertDocPath(filePath);
			if (govFileName == null || govFileName == "")
				govFileName = govfile.getFileName();
			else
				govFileName = govFileName + "," + govfile.getFileName();
			errorMap.notify(ErrorDescription.GOV_DOC_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void uploadExperenceFile(FileUploadEvent event) {
		log.info("uploadExperenceFile button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.EXP_CER_DOC_EMPTY.getErrorCode());
				return;
			}
			expfile = event.getFile();
			long size = expfile.getSize();
			log.info("uploadExperenceFile size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationWorkExp.setWorkExpDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;

			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, expfile);
			jobApplicationWorkExp.setWorkExpDocPath(filePath);
			if (expFileName == null || expFileName == "")
				expFileName = expfile.getFileName();
			else
				expFileName = expFileName + "," + expfile.getFileName();
			errorMap.notify(ErrorDescription.EXP_CER_UPLOAD_SUCCESS.getErrorCode());
			log.info("experience certificate is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void disableEnableGovEmp() {
		if (jobApplicationWorkExp.getIsGovEmployee() == true) {
			uploadGovDisable = true;
		} else {
			uploadGovDisable = false;
		}
	}

	public String disableEnablePSTM() {
		if (jobApplicationEduQualif.getIsTamilMedium() == true) {
			uploadDisable = true;
			log.info("# Udate Status :" + uploadDisable);
			RequestContext.getCurrentInstance().update("jobEduForm:certificatePanel");
		} else {
			uploadDisable = false;
			log.info("# Udate Status :" + uploadDisable);
			RequestContext.getCurrentInstance().update("jobEduForm:certificatePanel");
		}
		return "/pages/personnelHR/createJobApplicationEducation.xhtml?faces-redirect=true;";
	}

	public void updatePanel() {
		log.info("# Update panel calling ....");
		log.info("# Udate Status :" + uploadDisable);
		RequestContext.getCurrentInstance().update("jobEduForm:certificatePanel");
	}

	public void uploadEducationCertificates(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CER_DOC_EMPTY.getErrorCode());
				return;
			}
			cerfile = event.getFile();
			long size = cerfile.getSize();
			log.info("uploadEducationCertificates size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationEduQualif.setEducationalCertificateDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, cerfile);
			// jobApplicationEduQualif.setPstmCertDocPath(filePath);
			cerFileName = cerfile.getFileName();
			jobApplicationEduQualif.setEducationalCertificateDocPath(filePath);
			errorMap.notify(ErrorDescription.CER_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void editEdu(JobApplicationEduQualif jobApplicationEdu) {
		log.info(":: Edit - Employee Eduction qulification Action :: " + jobApplicationEdu);
		try {
			if (jobApplicationEdu == null) {
				log.error(":: Edit - Employee Eduction qulification Action Object null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			jobApplicationEduQualif = jobApplicationEdu;
			cerFileName = jobApplicationEdu.getPstmCertDocName();
			jobButton = "UPDATEJOB";
			log.info(" Selected Employee Eduction qulification details Action :: " + jobApplicationEduQualif);
			RequestContext.getCurrentInstance().update("jobEduForm:jobEduPanel");
		} catch (Exception e) {
			log.error("Exception While Editing incremental details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void editWorkExp(JobApplicationWorkExp workExp) {
		log.info(":: Edit - Employee work Exp :: " + workExp);
		try {
			if (workExp == null) {
				log.error(":: Edit - Employee work Exp Object null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			jobApplicationWorkExp = workExp;
			jobButton = "UPDATEJOB";
			log.info(" Selected Employee Work Exp Action :: " + jobApplicationWorkExp);
			RequestContext.getCurrentInstance().update("jobWorkForm:jobWorkPanel");
		} catch (Exception e) {
			log.error("Exception While Editing incremental details Action Details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void deleteWorkExp(JobApplicationWorkExp workExp) {
		log.info(" Delete Single Record  for deleteWorkExp :: " + workExp);
		try {
			if (workExp.getId() != null) {

				url = JOB_SERVER_URL + "/deleteworkexp/id/" + workExp.getId();
				log.info("Delete Single Record  for Employee Family URL ::  " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info("Delete Single Record  for Employee Family Response  :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				if (baseDTO.getStatusCode() == 0) {
					workExpList.remove(workExp);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					RequestContext.getCurrentInstance().update("educationForm:familyPanel");
					log.info("Employee Family Details Saved Succuessfully s");
				} else {
					log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
				}

			} else {
				workExpList.remove(workExp);
				log.info(" Employee Family Row Deleted ");
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}

		} catch (Exception e) {
			log.error(" Exception in Delete Employee Education Record : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void deleteEdu(JobApplicationEduQualif jobApplicationEdu) {
		log.info(" Delete Single Record  for Employee Education :: " + jobApplicationEdu);
		try {
			if (jobApplicationEdu.getId() != null) {

				url = JOB_SERVER_URL + "/deleteedu/id/" + jobApplicationEdu.getId();
				log.info("Delete Single Record  for Employee Family URL ::  " + url);
				BaseDTO baseDTO = httpService.delete(url);
				log.info("Delete Single Record  for Employee Family Response  :: " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				if (baseDTO.getStatusCode() == 0) {
					eduQua.remove(jobApplicationEdu);
					errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
					RequestContext.getCurrentInstance().update("educationForm:familyPanel");
					log.info("Job application edu qulification delete successfully");
				} else {
					log.info("Error while saving Family Details with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
				}

			} else {
				eduQua.remove(jobApplicationEdu);
				log.info(" Employee Family Row Deleted ");
				errorMap.notify(ErrorDescription.EMP_RECORD_DELETED.getErrorCode());
			}

		} catch (Exception e) {
			log.error(" Exception in Delete Employee Education Record : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * Purpose : to save the job Education details
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveJobEduQul() {
		log.info("<---- : Save job Education Details :: jobApplication :------>" + jobApplication);
		log.info("<---- : Save job Education Details :------>" + jobApplicationAdditionalInfo);
		try {

			jobApplication.setJobEduList(eduQua);
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application Education details Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			if (jobApp != null) {
				jobApplication = jobApp;
				jobApplicationAdditionalInfo = jobApp.getAdditionalInfo();
				eduQua = new ArrayList<>();
				if (jobApp.getJobEduList() != null) {
					for (JobApplicationEduQualif eduQualif : jobApp.getJobEduList()) {
						String filePathValue[] = eduQualif.getEducationalCertificateDocPath().split("uploaded/");
						log.info("Edu 0==>" + filePathValue[0]);
						log.info("Edu 1==>" + filePathValue[1]);
						eduQualif.setPstmCertDocName(filePathValue[1]);
						eduQua.add(eduQualif);
					}
				}
				// eduQua = jobApp.getJobEduList();
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee Family Details Saved Succuessfully ");
				errorMap.notify(ErrorDescription.EDU_QUI_SAVED.getErrorCode());
				return "/pages/personnelHR/createJobApplicationWorkExp.xhtml?faces-redirect=true";

			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * purpose : to uploaded the DD
	 * 
	 * @author krishnakumar
	 * @param event
	 * @return
	 */
	public void uploadDD(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.DD_NUM_EMPTY.getErrorCode());
				return;
			}
			ddFile = event.getFile();
			long size = ddFile.getSize();
			log.info("uploadDD size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobAppPayment.setDemandDraftDocPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			log.info("ddFile==>" + ddFile);
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, ddFile);
			jobAppPayment.setDemandDraftDocPath(filePath);
			demandDraftFileName = ddFile.getFileName();
			errorMap.notify(ErrorDescription.DD_NUN_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	/**
	 * Purpose : to save the job work experience
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveWorkExp() {
		log.info("<---- : Save job WorkExp Details :: jobApplication :------>" + jobApplication);
		log.info("<---- : Save job WorkExp Details :---workExpList--->" + workExpList);
		try {

			jobApplication.setJobWorkExp(workExpList);
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application work experience Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			if (jobApp != null) {
				jobApplication = jobApp;
				jobApplicationAdditionalInfo = jobApp.getAdditionalInfo();
				eduQua = jobApp.getJobEduList();
				workExpList = jobApp.getJobWorkExp();
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.WORK_EXP_SAVED.getErrorCode());
				return "/pages/personnelHR/createJobApplicationOtherInfo.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Purpose : to save the job application payment
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveJobAppPayment() {
		log.info("<---- : Save job WorkExp Details :: jobApplication :------>" + jobApplication);
		log.info("<---- : Save job WorkExp Details :---JobApplicationPaymentDetails--->" + jobAppPayment);
		try {
			if (jobAppPayment.getDemandDraftDocPath() == null) {
				Util.addWarn("DD uploaded documents is mandatory");
				return null;
			}
			jobApplication.setPaymentDetails(jobAppPayment);
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application payment Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			if (jobApp != null) {
				jobApplication = jobApp;
				jobApplicationAdditionalInfo = jobApp.getAdditionalInfo();
				eduQua = jobApp.getJobEduList();
				workExpList = jobApp.getJobWorkExp();
				jobApplicationOther = jobApp.getJobApplicationOrther();
				jobAppPayment = jobApp.getPaymentDetails();
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.JOB_PAYMENT_SAVED_SUCCESS.getErrorCode());
				return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Purpose : to clear the job application payment
	 * 
	 * @author krishnakumar
	 */
	public void clearJobPayment() {
		jobAppPayment = new JobApplicationPaymentDetails();
		RequestContext.getCurrentInstance().update("jobPaymentForm:jobPaymentPanel");
	}

	public void disableEnablephyChallenged() {
		if (jobApplicationOther.getIsPhysicallyChallenged() == true) {
			phyChallengedDisable = true;
		} else {
			phyChallengedDisable = false;
		}
	}

	public void onChangePriority() {
		if (jobApplicationOther.getIsPriority() != null && jobApplicationOther.getIsPriority() == true) {
			isPriority = true;
		} else {
			isPriority = null;
		}
		RequestContext.getCurrentInstance().update("otherForm:otherPanel");
	}

	public void disableDestitureWin() {
		if (jobApplicationOther.getIsDestituteWidow() != null && jobApplicationOther.getIsDestituteWidow() == true) {
			isDesWidow = true;
		} else {
			isDesWidow = false;
		}
	}

	public void disableExServiceman() {
		if (jobApplicationOther.getIsExServiceman() != null && jobApplicationOther.getIsExServiceman() == true) {
			isExService = true;
		} else {
			isExService = false;
		}
	}

	/**
	 * Purpose : to save the job application payment
	 * 
	 * @author krishnakumar
	 * @param
	 * @return String
	 */
	public String saveJobOther() {
		log.info("<---- : Save job WorkExp Details :: jobApplication :------>" + jobApplication);
		log.info("<---- : Save job WorkExp Details :---JobApplicationOrther--->" + jobApplicationOther);
		try {
			if (jobApplicationOther.getIsPriority() == true) {
				if (jobApplicationOther.getPriorityMaster() == null) {
					log.error("Priority master is empty ");
					errorMap.notify(ErrorDescription.PRI_MASTER_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getPriCertificateNumber() == null
						|| jobApplicationOther.getPriCertificateNumber().isEmpty()) {
					log.error("Priority certificate number is empty");
					errorMap.notify(ErrorDescription.PRI_CRI_NUMNER_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getPriDateOfIssue() == null) {
					log.error("Priority date of issue is empty");
					errorMap.notify(ErrorDescription.PRI_DATE_OF_ISSUE_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getPriIssuingAuthority() == null
						|| jobApplicationOther.getPriIssuingAuthority().isEmpty()) {
					log.error("Priority issuing authority is empty");
					errorMap.notify(ErrorDescription.PRI_ISSUEING_AUT_EMPTY.getErrorCode());
					return null;
				}
			}
			if (jobApplicationOther.getIsPhysicallyChallenged() == true) {
				if (jobApplicationOther.getDifferentillyAbled() == null
						|| jobApplicationOther.getDifferentillyAbled().isEmpty()) {
					log.error("Differentilly abled is empty");
					errorMap.notify(ErrorDescription.DIF_ABLED_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getPercentageDisablity() == null
						|| jobApplicationOther.getPercentageDisablity().isNaN()) {
					log.error("Percentage disablity is empty");
					errorMap.notify(ErrorDescription.PERCENTAGE_DISABL_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getPercentageDisablity() > 100) {
					log.error("Percentage disablity not greater than 100");
					errorMap.notify(ErrorDescription.PRE_DIS_PERCENTAGE.getErrorCode());
					return null;
				} else if (jobApplicationOther.getDifAbledCertificateNumber() == null
						|| jobApplicationOther.getDifAbledCertificateNumber().isEmpty()) {
					log.error("Difabled certificate number is empty");
					errorMap.notify(ErrorDescription.DIFABLED_CER_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getDifAbledDateOfIssue() == null) {
					log.error("Difabled date of issue is empty");
					errorMap.notify(ErrorDescription.DIFABLED_DATE_OF_ISSUE_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getDifAbledIssuingAuthority() == null
						|| jobApplicationOther.getDifAbledIssuingAuthority().isEmpty()) {
					log.error("Difabled issueing authority is empty");
					errorMap.notify(ErrorDescription.DIFABLED_ISSUEING_AUTOR_EMPTY.getErrorCode());
					return null;
				}

			}
			if (jobApplicationOther.getIsDestituteWidow() == true) {
				if (jobApplicationOther.getDesCertificateNumber() == null
						|| jobApplicationOther.getDesCertificateNumber().isEmpty()) {
					log.error("Destitute widow certificate number is empty");
					errorMap.notify(ErrorDescription.DES_CER_NUM_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getDesDateOfIssue() == null) {
					log.error("Destitute date of issue is empty");
					errorMap.notify(ErrorDescription.DES_DATE_OF_ISSUE_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getDesIssuingAuthority() == null
						|| jobApplicationOther.getDesIssuingAuthority().isEmpty()) {
					log.error("Destitute issuing authority is empty");
					errorMap.notify(ErrorDescription.DES_ISSUEING_AUT_EMPTY.getErrorCode());
					return null;
				}
			}
			if (jobApplicationOther.getIsExServiceman() == true) {
				if (jobApplicationOther.getIsExCertificateNumber() == null
						|| jobApplicationOther.getIsExCertificateNumber().isEmpty()) {
					log.error("Ex-service man certificate number is empty");
					errorMap.notify(ErrorDescription.EX_SERVICE_CER_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getIsExDateOfIssue() == null) {
					log.error("Ex-service man date of issue is empty");
					errorMap.notify(ErrorDescription.EX_SERVICE_DATE_OF_ISS_EMPTY.getErrorCode());
					return null;
				} else if (jobApplicationOther.getIsExIssuingAuthority() == null
						|| jobApplicationOther.getIsExIssuingAuthority().isEmpty()) {
					log.error("Ex-service man issuing authority is empty");
					errorMap.notify(ErrorDescription.EX_SERVICE_ISS_AUTHORITY_EMPTY.getErrorCode());
					return null;
				}
			}
			jobApplication.setJobApplicationOrther(jobApplicationOther);
			url = JOB_SERVER_URL + "/update";
			BaseDTO baseDTO = httpService.put(url, jobApplication);
			log.info("Save Job Application Other Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);
			if (jobApp != null) {
				jobApplication = jobApp;
				jobApplicationAdditionalInfo = jobApp.getAdditionalInfo();
				eduQua = jobApp.getJobEduList();
				workExpList = jobApp.getJobWorkExp();
				jobApplicationOther = jobApp.getJobApplicationOrther();
				// jobAppPayment = jobApp.getPaymentDetails();
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.JOB_OTHER_SAVED_SUCCESS.getErrorCode());
				return "/pages/personnelHR/createJobApplicationPayment.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Purpose : to clear the job application Others
	 * 
	 * @author krishnakumar
	 */
	public void clearOther() {
		jobApplicationOther = new JobApplicationOrther();
		RequestContext.getCurrentInstance().update("otherForm:otherPanel");
	}

	/**
	 * purpose : to uploaded the image
	 * 
	 * @author krishnakumar
	 * @param event
	 * @return
	 */
	public void phyChallengedFileUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.PHYSICALLY_CHALLENGED_EMPTY.getErrorCode());
				return;
			}
			phyChallengedFile = event.getFile();
			long size = phyChallengedFile.getSize();
			log.info("phyChallengedFileUpload size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationOther.setPhysicallyChallengedPersonPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, phyChallengedFile);
			jobApplicationOther.setPhysicallyChallengedPersonPath(filePath);
			phyFileName = phyChallengedFile.getFileName();
			if (fileName == null || fileName == "")
				fileName = phyFileName;
			else
				fileName = fileName + "," + phyFileName;
			errorMap.notify(ErrorDescription.PHYSICALLY_CHALLENGED_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void priorityUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.PRIORITY_EMPTY.getErrorCode());
				return;
			}
			priFile = event.getFile();
			long size = priFile.getSize();
			log.info("priorityUpload size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationOther.setPriorityNonPriorityPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, priFile);
			jobApplicationOther.setPriorityNonPriorityPath(filePath);

			priFileName = priFile.getFileName();
			if (fileName == null || fileName == "")
				fileName = priFileName;
			else
				fileName = fileName + "," + priFileName;
			errorMap.notify(ErrorDescription.PRIORITY_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void exServicemanFileUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.EX_SERVICEMAN_EMPTY.getErrorCode());
				return;
			}
			exServiceFile = event.getFile();
			long size = exServiceFile.getSize();
			log.info("exServicemanFileUpload size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationOther.setExServicemanPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, exServiceFile);
			jobApplicationOther.setExServicemanPath(filePath);
			exServiceFileName = exServiceFile.getFileName();
			if (fileName == null || fileName == "")
				fileName = exServiceFileName;
			else
				fileName = fileName + "," + exServiceFileName;
			errorMap.notify(ErrorDescription.EX_SERVICE_MAN_UPLOAD_SUCCESSFULLY.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.EX_SERVICEMAN_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void disWidowFileUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.DIS_WIDOW_EMPTY.getErrorCode());
				return;
			}
			disWidowFile = event.getFile();
			long size = disWidowFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				jobApplicationOther.setDestituteWidowPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, disWidowFile);
			// jobApplicationOther.setPriorityNonPriorityPath(filePath);
			jobApplicationOther.setDestituteWidowPath(filePath);
			disWidowFileName = disWidowFile.getFileName();
			if (fileName == null || fileName == "")
				fileName = disWidowFileName;
			else
				fileName = fileName + "," + disWidowFileName;
			errorMap.notify(ErrorDescription.DIS_WIDOW_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String showJobApplication() {
		try {
			log.info("Show Employee Form Called with Action " + action);
			viewFlag = "ADDJOBAPPLICATION";
			if (action.equalsIgnoreCase("ADD")) {
				action = "ADD";
				jobApplication = new JobApplication();
				jobApplicationList = new ArrayList<>();
				jobApplicationAdditionalInfo = new JobApplicationAdditionalInfo();
				jobApplicationEduQualif = new JobApplicationEduQualif();
				eduQua = new ArrayList<>();
				jobApplicationWorkExp = new JobApplicationWorkExp();
				workExpList = new ArrayList<>();
				jobApplicationOther = new JobApplicationOrther();
				selectedJobApplication = new JobApplication();
				jobAppPayment = new JobApplicationPaymentDetails();
				demandDraftFileName = null;
				signatureFileName = null;
				communityCertFileName = null;
				pstmFileName = null;
				cerFileName = null;
				govFileName = null;
				expFileName = null;
				demandDraftFileName = null;
				phyFileName = null;
				priFileName = null;
				exServiceFileName = null;
				disWidowFileName = null;
				sigName = null;
				photograph = null;
				getNotficationNumber();
				navigationFlag = true;
				return "/pages/personnelHR/createJobApplicationPrimary.xhtml?faces-redirect=true";
			} else if (action.equalsIgnoreCase("EDIT")) {
				action = "EDIT";
				if (selectedJobApplication == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				if (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
					return null;
				}
				/*if (selectedJobApplication.getStatus().equalsIgnoreCase("Rejected")) {
					errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
					return null;
				}*/
				log.info("Selected jobapplication  Id : : " + selectedJobApplication.getId());
				jobApplication = getJobApplicationById();
				designation = jobApplication.getAppliedPost();
				state = jobApplication.getAppliedState();
				jobApplicationAdditionalInfo = jobApplication.getAdditionalInfo();
				eduQua = new ArrayList<>();
				if (jobApplication.getJobEduList() != null) {
					for (JobApplicationEduQualif eduQualif : jobApplication.getJobEduList()) {
						String filePathValue[] = eduQualif.getEducationalCertificateDocPath().split("uploaded/");
						log.info("Edu 0==>" + filePathValue[0]);
						log.info("Edu 1==>" + filePathValue[1]);
						eduQualif.setPstmCertDocName(filePathValue[1]);
						eduQua.add(eduQualif);
					}
				}
				// eduQua = jobApplication.getJobEduList();
				workExpList = jobApplication.getJobWorkExp();
				fileName = "";
				if (jobApplication.getJobApplicationOrther() != null) {
					jobApplicationOther = jobApplication.getJobApplicationOrther();
					log.info("getPhysicallyChallengedPersonPath==>"
							+ jobApplicationOther.getPhysicallyChallengedPersonPath());
					if (jobApplicationOther.getPhysicallyChallengedPersonPath() != null) {
						String phyFileNameArr[] = jobApplicationOther.getPhysicallyChallengedPersonPath()
								.split("uploaded/");
						if (phyFileNameArr != null) {
							phyFileName = phyFileNameArr[1];
							if (fileName == "")
								fileName = phyFileName;
						}
					}
					if (jobApplicationOther.getPriorityNonPriorityPath() != null) {
						String priFileNameArr[] = jobApplicationOther.getPriorityNonPriorityPath().split("uploaded/");
						if (priFileNameArr != null) {
							priFileName = priFileNameArr[1];
							if (fileName == "")
								fileName = priFileName;
							else
								fileName = fileName + "," + priFileName;
						}
					}
					if (jobApplicationOther.getDestituteWidowPath() != null) {
						String disWidowFileNameArr[] = jobApplicationOther.getDestituteWidowPath().split("uploaded/");
						if (disWidowFileNameArr != null) {
							disWidowFileName = disWidowFileNameArr[1];
							if (fileName == "")
								fileName = disWidowFileName;
							else
								fileName = fileName + "," + disWidowFileName;
						}
					}
					if (jobApplicationOther.getExServicemanPath() != null) {
						String exServiceFileNameArr[] = jobApplicationOther.getExServicemanPath().split("uploaded/");
						if (exServiceFileNameArr != null) {
							exServiceFileName = exServiceFileNameArr[1];
							if (fileName == "")
								fileName = exServiceFileName;
							else
								fileName = fileName + "," + exServiceFileName;
						}
					}
				} else {
					jobApplicationOther = new JobApplicationOrther();
				}
				log.info("jobID==>" + jobApplication.getPaymentDetails());
				if (jobApplication.getPaymentDetails() != null) {
					jobAppPayment = jobApplication.getPaymentDetails();

					if (jobAppPayment.getDemandDraftDocPath() != null) {
						String filePathValue[] = jobAppPayment.getDemandDraftDocPath().split("uploaded/");
						log.info("0==>" + filePathValue[0]);
						log.info("1==>" + filePathValue[1]);
						paymentFileName = filePathValue[1];
					} else {
						log.info("1==>" + paymentFileName);
						paymentFileName = "ppp";
					}
				} else {
					jobAppPayment = new JobApplicationPaymentDetails();
				}
				getUploadedImage();
				convertImage();
				if (jobApplication.getSignatureDocPath() != null) {
					signatureFileName = jobApplication.getSignatureDocPath();
					if (signatureFileName != null) {
						String filePathValue[] = signatureFileName.split("uploaded/");
						log.info("0==>" + filePathValue[0]);
						log.info("1==>" + filePathValue[1]);
						sigName = filePathValue[1];
					}
				}
				if (jobApplication.getAdditionalInfo() != null) {
					communityCertFileName = jobApplication.getAdditionalInfo().getCommunityCertDocPath();
					if (communityCertFileName != null) {
						String comCerPathValue[] = communityCertFileName.split("uploaded/");
						communityCertFileName = comCerPathValue[1];
					}
					log.info("communityCertFileName==>" + communityCertFileName);
				}
				if (jobApplication.getJobApplicationOrther() != null) {
					phyFileName = jobApplication.getJobApplicationOrther().getPhysicallyChallengedPersonPath();
					if (phyFileName != null) {
						String phyFileNameArr[] = phyFileName.split("uploaded/");
						phyFileName = phyFileNameArr[1];
					}
					priFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
					if (priFileName != null) {
						String priFileNameArr[] = priFileName.split("uploaded/");
						priFileName = priFileNameArr[1];
					}
					exServiceFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
					if (exServiceFileName != null) {
						String exServiceFileNameArr[] = exServiceFileName.split("uploaded/");
						exServiceFileName = exServiceFileNameArr[1];
					}
					disWidowFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
					if (disWidowFileName != null) {
						String disWidowFileNameArr[] = disWidowFileName.split("uploaded/");
						disWidowFileName = disWidowFileNameArr[1];
					}
				}
				if (jobApplication.getJobWorkExp() != null) {
					govFileName = "";
					expFileName = "";
					for (JobApplicationWorkExp workExp : jobApplication.getJobWorkExp()) {
						if (workExp != null && workExp.getWorkExpDocPath() != null) {
							String filePathValue[] = workExp.getWorkExpDocPath().split("uploaded/");
							log.info("work 0==>" + filePathValue[0]);
							log.info("work 1==>" + filePathValue[1]);
							if (expFileName == "" && filePathValue[1] != null)
								expFileName = filePathValue[1];
							else
								expFileName = expFileName + "," + filePathValue[1];
						}
						if (workExp != null && workExp.getNocCertDocPath() != null) {
							String filePathNoc[] = workExp.getNocCertDocPath().split("uploaded/");
							log.info("noc 0==>" + filePathNoc[0]);
							log.info("noc 1==>" + filePathNoc[1]);
							if (govFileName == "" && filePathNoc[1] != null)
								govFileName = filePathNoc[1];
							else
								govFileName = govFileName + "," + filePathNoc[1];
						}
					}
				}
				districtMasterList = commonDataService.getDistrictMasterList();
				talukMasterList = commonDataService.getTalukMasterList();
				villageMasterList = commonDataService.gellAllVillageMasterList();
				getNotficationNumber();
				navigationFlag = true;
				return "/pages/personnelHR/createJobApplicationPrimary.xhtml?faces-redirect=true";
			} else if (action.equalsIgnoreCase("APPROVAL")) {
				if (selectedJobApplication == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				if (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.JOB_APPLICATION_APPROVAL_ALREADY_IS_EXISTS.getErrorCode());
					return null;
				}
				if (selectedJobApplication.getStatus().equalsIgnoreCase("Rejected")) {
					errorMap.notify(ErrorDescription.JOB_APPLICATION_ALREADY_REJECTED.getErrorCode());
					return null;
				}
				jobApplicationApproval = new JobApplicationApproval();
				// jobApplication = selectedJobApplication;
				loadJobApplicationDetaiils();
				return "/pages/personnelHR/approvalJobApplication.xhtml?faces-redirect=true";
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info(":: Job Application Delete Response :: ");
				if (selectedJobApplication == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			} else if (action.equalsIgnoreCase("LISTPAGE")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				// loadLazyAdvAndAppList();
				return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
				// return
				// "/pages/personnelHR/listJobApplicationListing.xhtml?faces-redirect=true";
			}

		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * @author krishnakumar
	 * @return
	 */
	public String viewJobApplication() {
		try {
			if (selectedJobApplication == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			loadJobApplicationDetaiils();
			return "/pages/personnelHR/viewApprovalJobApplication.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String approvalJobApplication() {
		try {

			log.info("Selected jobapplication  Id : : " + selectedJobApplication);

			if (selectedJobApplication == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			/*
			 * jobApplication = getJobApplicationById(); jobApplicationAdditionalInfo =
			 * jobApplication.getAdditionalInfo(); eduQua = jobApplication.getJobEduList();
			 * workExpList = jobApplication.getJobWorkExp(); jobApplicationOther =
			 * jobApplication.getJobApplicationOrther(); jobAppPayment =
			 * jobApplication.getPaymentDetails();
			 * 
			 * getUploadedImage(); convertImage();
			 * 
			 * signatureFileName = jobApplication.getSignatureDocPath();
			 * if(jobApplication.getAdditionalInfo() != null) { communityCertFileName =
			 * jobApplication.getAdditionalInfo().getCommunityCertDocPath(); }
			 * if(jobApplication.getJobApplicationOrther() != null) { phyFileName =
			 * jobApplication.getJobApplicationOrther().getPhysicallyChallengedPersonPath();
			 * priFileName =
			 * jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
			 * exServiceFileName =
			 * jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
			 * disWidowFileName =
			 * jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath(); }
			 */

			jobApplication = getJobApplicationById();
			designation = jobApplication.getAppliedPost();
			state = jobApplication.getAppliedState();
			jobApplicationAdditionalInfo = jobApplication.getAdditionalInfo();
			eduQua = jobApplication.getJobEduList();
			workExpList = jobApplication.getJobWorkExp();
			if (jobApplication.getJobApplicationOrther() != null) {
				jobApplicationOther = jobApplication.getJobApplicationOrther();
			} else {
				jobApplicationOther = new JobApplicationOrther();
			}
			log.info("jobID==>" + jobApplication.getPaymentDetails());
			if (jobApplication.getPaymentDetails() != null) {
				jobAppPayment = jobApplication.getPaymentDetails();
			} else {
				jobAppPayment = new JobApplicationPaymentDetails();
			}
			getUploadedImage();
			convertImage();
			if (jobApplication.getSignatureDocPath() != null) {
				signatureFileName = jobApplication.getSignatureDocPath();
				if (signatureFileName != null) {
					String filePathValue[] = signatureFileName.split("uploaded/");
					log.info("0==>" + filePathValue[0]);
					log.info("1==>" + filePathValue[1]);
					sigName = filePathValue[1];
				}
			}
			if (jobApplication.getAdditionalInfo() != null) {
				communityCertFileName = jobApplication.getAdditionalInfo().getCommunityCertDocPath();
				if (communityCertFileName != null) {
					String comCerPathValue[] = communityCertFileName.split("uploaded/");
					communityCertFileName = comCerPathValue[1];
				}
				log.info("communityCertFileName==>" + communityCertFileName);
			}
			if (jobApplication.getJobApplicationOrther() != null) {
				phyFileName = jobApplication.getJobApplicationOrther().getPhysicallyChallengedPersonPath();
				if (phyFileName != null) {
					String phyFileNameArr[] = phyFileName.split("uploaded/");
					phyFileName = phyFileNameArr[1];
				}
				priFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
				if (priFileName != null) {
					String priFileNameArr[] = priFileName.split("uploaded/");
					priFileName = priFileNameArr[1];
				}
				exServiceFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
				if (exServiceFileName != null) {
					String exServiceFileNameArr[] = exServiceFileName.split("uploaded/");
					exServiceFileName = exServiceFileNameArr[1];
				}
				disWidowFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();
				if (disWidowFileName != null) {
					String disWidowFileNameArr[] = disWidowFileName.split("uploaded/");
					disWidowFileName = disWidowFileNameArr[1];
				}
			}

			return "/pages/personnelHR/approvalJobApplication.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public JobApplication getJobApplicationById() {
		try {
			if (selectedJobApplication == null || selectedJobApplication.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  Id : : " + selectedJobApplication.getId());
			String getUrl = JOB_SERVER_URL + "/get/id/:id";
			url = getUrl.replace(":id", selectedJobApplication.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			// log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			JobApplication jobApp = mapper.readValue(jsonResponse, JobApplication.class);

			if (jobApp != null) {
				jobApplication = jobApp;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return jobApplication;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public String cancelViewPage() {
		log.info("Cancel page :::::::");
		try {
			selectedJobApplication = new JobApplication();
			jobApplicationApproval = new JobApplicationApproval();
			photograph = null;
			pstmFileName = null;
			cerFileName = null;
			disableButton = false;
			uploadGovDisable = false;
			uploadDisable = false;
			uploadGovDisable = false;
			/*
			 * phyFileName = null; priFileName = null; disWidowFileName = null;
			 * exServiceFileName = null;
			 */
			loadJobApplicationList();
			onChangePriority();
			disableEnablephyChallenged();
			disableExServiceman();
			disableDestitureWin();
			isAddButton = true;
		} catch (Exception e) {
			log.error(" Error Occured While cancel View Page ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		isAddButton = true;
		return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
	}

	public String approveRejectStatus() {
		BaseDTO baseDto = new BaseDTO();
		log.info("job Approval Process " + jobApplication);
		try {
			log.info("job Approval Process " + jobApplication);
			if (jobApplication == null) {
				log.error(" Error Occured While Job Application Approve ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			JobApplicationApproval jobApproval = new JobApplicationApproval();
			JobApplicationPrimaryKey key = new JobApplicationPrimaryKey();
			key.setJobAppId(jobApplication.getId());
			jobApproval.setPrimaryKey(key);
			jobApproval.setJobApplication(jobApplication);
			jobApproval.setJobApplicationStatus(jobApplication.getStatus());
			String url = SERVER_URL + "/jobapplapproval/process";
			log.info(" :: Job Application Approve URL :: " + url);
			baseDto = httpService.post(url, jobApproval);
			log.info(" :: Job Application Approve Response :: " + baseDto);
			if (baseDto == null) {
				log.error(" Error Occured While Job Application Approve ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			errorMap.notify(baseDto.getStatusCode());
		} catch (Exception e) {
			log.info("exception", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		jobApplication = new JobApplication();
		selectedJobApplication = new JobApplication();
		disableButton = false;
		return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
	}

	public String clear() {
		log.info("job application clear method called..");
		selectedJobApplication = new JobApplication();
		loadLazyList();
		disableButton = false;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
	}

	public String deleteJobApplication() {
		try {

			if (selectedJobApplication == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + selectedJobApplication.getId());
			String getUrl = JOB_SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedJobApplication.getId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				isAddButton = true;
				errorMap.notify(ErrorDescription.EMPLOYEE_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			// loadJobApplicationList();
			loadLazyList();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true";
	}

	public String cancelPage() {
		log.info("Cancel Page ==>");
		try {
			log.info(":: Cancel Page");
			selectedJobApplication = new JobApplication();
			loadJobApplicationList();
			pstmFileName = null;
			cerFileName = null;
			disableButton = false;
			uploadGovDisable = false;
			uploadDisable = false;
			phyChallengedDisable = false;
			isPriority = false;
			isDesWidow = false;
			isExService = false;
			isDisAble = false;
			fileName = "";
		} catch (Exception e) {
			log.error(" Exception Occured While cancel Page  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listJobApplication.xhtml?faces-redirect=true;";
	}

	public void onRowSelect(SelectEvent event) {
		selectedJobApplication = ((JobApplication) event.getObject());
		disableButton = true;
		if (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedJobApplication.getStatus().equalsIgnoreCase("Rejected")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = true;
			isDeleteButton = true;
		} else if (selectedJobApplication.getStatus().equalsIgnoreCase("InProgress")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public void confirmStatus() {
		if (jobApplication == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public void onChangeDifferenctiallyPriority() {
		log.info("jobApplicationOther == >>" + jobApplicationOther);
		try {
			PriorityMaster priorityMaster = jobApplicationOther.getPriorityMaster();
			if (priorityMaster.getPriorityName().equals("Diffentially-Abled")) {
				isDisAble = true;
			} else {
				isDisAble = false;
			}
			log.info("pri name===>" + priorityMaster.getPriorityName());
			RequestContext.getCurrentInstance().update("otherForm:otherPanel");
		} catch (Exception e) {
			log.error(" Exception onChangeDifferenctiallyPriority  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/*
	 * public void loadLazyAdvAndAppList() {
	 * 
	 * lazyAdvAndApplicationModel = new LazyDataModel<JobApplication>() {
	 * 
	 * private static final long serialVersionUID = 8370582987826925988L;
	 * 
	 * @Override public List<JobApplication> load(int first, int pageSize, String
	 * sortField, SortOrder sortOrder, Map<String, Object> filters) { try {
	 * JobApplication request = new JobApplication();
	 * request.setFirst(first/pageSize); request.setPagesize(pageSize);
	 * request.setSortField(sortField); request.setSortOrder(sortOrder.toString());
	 * request.setFilters(filters);
	 * 
	 * BaseDTO baseDTO = httpService.post(JOB_SERVER_URL + "/loadadvandapplazylist",
	 * request);
	 * 
	 * if (baseDTO == null) { log.error(" Base DTO returned Null Values ");
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); return null;
	 * } jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * List<JobApplication> data = mapper.readValue(jsonResponse, new
	 * TypeReference<List<JobApplication>>() {}); log.info("data====>"+data.size());
	 * if (data == null) {
	 * log.info(" :: Job Advertisement Failed to Deserialize ::");
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); } if
	 * (baseDTO.getStatusCode() == 0) { this.setRowCount(baseDTO.getTotalRecords());
	 * totalRecords = baseDTO.getTotalRecords();
	 * log.info(":: Job Advertisement Search Successfully Completed ::"); } else {
	 * log.error(":: Job Advertisement Search Failed With status code :: " +
	 * baseDTO.getStatusCode()); errorMap.notify(baseDTO.getStatusCode()); } return
	 * data; } catch (Exception e) { log.
	 * error(":: Exception Exception Ocured while Loading Job Advertisement data ::"
	 * , e); errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); }
	 * return null; }
	 * 
	 * @Override public Object getRowKey(JobApplication res) { return res != null ?
	 * res.getId() : null; }
	 * 
	 * @Override public JobApplication getRowData(String rowKey) {
	 * List<JobApplication> responseList = (List<JobApplication>) getWrappedData();
	 * Long value = Long.valueOf(rowKey); for (JobApplication res : responseList) {
	 * if (res.getId().longValue() == value.longValue()) { selectedJobApplication =
	 * res; return res; } } return null; } }; }
	 */

	public void loadLazyList() {
		lazyApplicationModel = new LazyDataModel<JobApplication>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<JobApplication> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<JobApplication> data = mapper.readValue(jsonResponse,
							new TypeReference<List<JobApplication>>() {
							});
					if (data == null) {
						log.info(" :: Job Advertisement Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: Job Advertisement Search Successfully Completed ::");
					} else {
						log.error(":: Job Advertisement Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(JobApplication res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public JobApplication getRowData(String rowKey) {
				List<JobApplication> responseList = (List<JobApplication>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (JobApplication res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedJobApplication = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first, pageSize,
					sortField, sortOrder, filters);
			JobApplication request = new JobApplication();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			request.setFilters(filters);
			log.info("Search request data" + request);
			baseDTO = httpService.post(JOB_SERVER_URL + "/loadlazylist", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in employee search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void bindAddress() {
		presenetAddress = new StringBuilder();		
		if(jobApplication.getAdditionalInfo()!=null) {
		presenetAddress.append(jobApplication.getAdditionalInfo().getPresentAddressLine1()).append(", \n");
		presenetAddress.append(jobApplication.getAdditionalInfo().getPresentAddressLine2()).append(", \n");
		presenetAddress.append(jobApplication.getAdditionalInfo().getPresentAddressLine3()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPresentDistrict()!=null && jobApplication.getAdditionalInfo().getPresentDistrict().getName()!=null)
			presenetAddress.append(jobApplication.getAdditionalInfo().getPresentDistrict().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPresentTaluk()!=null && jobApplication.getAdditionalInfo().getPresentTaluk().getName()!=null)
			presenetAddress.append(jobApplication.getAdditionalInfo().getPresentTaluk().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPresentVillage()!=null && jobApplication.getAdditionalInfo().getPresentVillage().getName()!=null)
			presenetAddress.append(jobApplication.getAdditionalInfo().getPresentVillage().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPresentState()!=null && jobApplication.getAdditionalInfo().getPresentState().getName()!=null)
			presenetAddress.append(jobApplication.getAdditionalInfo().getPresentState().getName()).append(" - ");
		presenetAddress.append(jobApplication.getAdditionalInfo().getPresentPincode()).append(".");
		log.info("Job application Present address :" + presenetAddress);
		presenet = presenetAddress.toString();

		permanentAddress = new StringBuilder();
		permanentAddress.append(jobApplication.getAdditionalInfo().getPermAddressLine1()).append(", \n");
		permanentAddress.append(jobApplication.getAdditionalInfo().getPermAddressLine2()).append(", \n");
		permanentAddress.append(jobApplication.getAdditionalInfo().getPermAddressLine3()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPermDistrict()!=null && jobApplication.getAdditionalInfo().getPermDistrict().getName()!=null)
			permanentAddress.append(jobApplication.getAdditionalInfo().getPermDistrict().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPermTaluk()!=null && jobApplication.getAdditionalInfo().getPermTaluk().getName()!=null)
			permanentAddress.append(jobApplication.getAdditionalInfo().getPermTaluk().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPermVillage()!=null && jobApplication.getAdditionalInfo().getPermVillage().getName()!=null)
			permanentAddress.append(jobApplication.getAdditionalInfo().getPermVillage().getName()).append(", \n");
		if(jobApplication.getAdditionalInfo().getPermState()!=null && jobApplication.getAdditionalInfo().getPermState().getName()!=null)
			permanentAddress.append(jobApplication.getAdditionalInfo().getPermState().getName()).append(" - ");
		
		permanentAddress.append(jobApplication.getAdditionalInfo().getPermPincode()).append(".");
		log.info("Job application permanent address :" + permanentAddress);
		permanent = permanentAddress.toString();
		}
	}

	public String loadJobApplicationDetaiils() {
		try {
			if (selectedJobApplication == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			jobApplication = getJobApplicationById();
			bindAddress();
			designation = jobApplication.getAppliedPost();
			state = jobApplication.getAppliedState();
			jobApplicationAdditionalInfo = jobApplication.getAdditionalInfo();
			// eduQua = jobApplication.getJobEduList();
			workExpList = jobApplication.getJobWorkExp();
			if (jobApplication.getJobApplicationOrther() != null) {
				jobApplicationOther = jobApplication.getJobApplicationOrther();
			} else {
				jobApplicationOther = new JobApplicationOrther();
			}
			log.info("jobID==>" + jobApplication.getPaymentDetails());
			if (jobApplication.getPaymentDetails() != null) {
				jobAppPayment = jobApplication.getPaymentDetails();
				jobAppPaymentValue = downloadImage(jobAppPayment.getDemandDraftDocPath());
				if (jobAppPayment.getDemandDraftDocPath() != null) {
					String filePathValue[] = jobAppPayment.getDemandDraftDocPath().split("uploaded/");
					log.info("0==>" + filePathValue[0]);
					log.info("1==>" + filePathValue[1]);
					paymentFileName = filePathValue[1];
				} else {
					log.info("1==>" + paymentFileName);

				}
			} else {
				jobAppPayment = new JobApplicationPaymentDetails();
			}
			getUploadedImage();
			convertImage();
			if (jobApplication.getSignatureDocPath() != null) {
				signatureFileName = jobApplication.getSignatureDocPath();

				sigFileNameValue = downloadImage(signatureFileName);
				log.info("sigFileNameValue==>" + sigFileNameValue);

				if (signatureFileName != null) {
					String filePathValue[] = signatureFileName.split("uploaded/");
					log.info("0==>" + filePathValue[0]);
					log.info("1==>" + filePathValue[1]);
					sigName = filePathValue[1];
				}
			}
		//	eduQua = new ArrayList<>();
			if (jobApplication.getJobEduList() != null) {
				for (JobApplicationEduQualif eduQualif : jobApplication.getJobEduList()) {
					if (eduQualif != null && eduQualif.getPstmCertDocPath() != null) {

						pstmDocFileNameValue = downloadImage(eduQualif.getPstmCertDocPath());
						log.info("pstmDocFileNameValue==>" + pstmDocFileNameValue);

						String eduPathValue[] = eduQualif.getPstmCertDocPath().split("uploaded/");
						log.info("Edu 0==>" + eduPathValue[0]);
						log.info("Edu 1==>" + eduPathValue[1]);
						eduQualif.setPstmCertDocName(eduPathValue[1]);
						//eduQua.add(eduQualif);
					}
					if (eduQualif != null && eduQualif.getEducationalCertificateDocPath() != null) {
						eduDocFileNameValue = downloadImage(eduQualif.getEducationalCertificateDocPath());
						log.info("eduDocFileNameValue==>" + eduDocFileNameValue);

						String filePathValue[] = eduQualif.getEducationalCertificateDocPath().split("uploaded/");
						log.info("Edu 0==>" + filePathValue[0]);
						log.info("Edu 1==>" + filePathValue[1]);
						eduQualif.setEducationalCertificateDocPath(filePathValue[1]);
					}					
				}
			}
			if (jobApplication.getJobWorkExp() != null) {
				for (JobApplicationWorkExp workExp : jobApplication.getJobWorkExp()) {

					if (workExp != null && workExp.getWorkExpDocPath() != null) {
						workExpFileNameValue = downloadImage(workExp.getWorkExpDocPath());
						log.info("disWidowFileNameValue==>" + workExpFileNameValue);

						String filePathValue[] = workExp.getWorkExpDocPath().split("uploaded/");
						log.info("work 0==>" + filePathValue[0]);
						log.info("work 1==>" + filePathValue[1]);
						workExp.setWorkExpDocPath(filePathValue[1]);
						if (expFileName == "")
							expFileName = filePathValue[1];
						else
							expFileName = expFileName + "," + filePathValue[1];
					}

					if (workExp != null && workExp.getNocCertDocPath() != null) {
						nonCerDocFileNameValue = downloadImage(workExp.getNocCertDocPath());
						log.info("nonCerDocFileNameValue==>" + nonCerDocFileNameValue);
						String filePathNoc[] = workExp.getNocCertDocPath().split("uploaded/");
						log.info("noc 0==>" + filePathNoc[0]);
						log.info("noc 1==>" + filePathNoc[1]);
						workExp.setNocCertDocPath(filePathNoc[1]);
						if (govFileName == "")
							govFileName = filePathNoc[1];
						else
							govFileName = govFileName + "," + filePathNoc[1];
					}
				}
			}
			//jobApplication.setJobEduList(eduQua);
			if (jobApplication.getAdditionalInfo() != null) {
				communityCertFileName = jobApplication.getAdditionalInfo().getCommunityCertDocPath();
				log.info("communityCertFileName==>" + communityCertFileName);

				communityCertificatedownloadValue = downloadImage(communityCertFileName);
				log.info("downloadImageValue==>" + communityCertificatedownloadValue);

				if (communityCertFileName != null) {
					String comCerPathValue[] = communityCertFileName.split("uploaded/");
					communityCertFileName = comCerPathValue[1];
				}
				log.info("communityCertFileName==>" + communityCertFileName);
			}
			if (jobApplication.getJobApplicationOrther() != null) {
				phyFileName = jobApplication.getJobApplicationOrther().getPhysicallyChallengedPersonPath();

				phyFileNamedownloadValue = downloadImage(phyFileName);
				log.info("phyFileNamedownloadValue==>" + phyFileNamedownloadValue);

				if (phyFileName != null) {
					String phyFileNameArr[] = phyFileName.split("uploaded/");
					phyFileName = phyFileNameArr[1];
					if (fileName == null || fileName == "")
						fileName = phyFileName;
				}
				priFileName = jobApplication.getJobApplicationOrther().getPriorityNonPriorityPath();

				priFileNamedownloadValue = downloadImage(priFileName);
				log.info("priFileNamedownloadValue==>" + priFileNamedownloadValue);

				if (priFileName != null) {
					String priFileNameArr[] = priFileName.split("uploaded/");
					priFileName = priFileNameArr[1];
					if (fileName == null || fileName == "")
						fileName = priFileName;
					else
						fileName = fileName + "," + priFileName;
				}
				exServiceFileName = jobApplication.getJobApplicationOrther().getExServicemanPath();
				exServiceFileNameValue = downloadImage(exServiceFileName);
				log.info("priFileNamedownloadValue==>" + exServiceFileNameValue);

				if (exServiceFileName != null) {
					String exServiceFileNameArr[] = exServiceFileName.split("uploaded/");
					exServiceFileName = exServiceFileNameArr[1];
					if (fileName == null || fileName == "")
						fileName = exServiceFileName;
					else
						fileName = fileName + "," + exServiceFileName;
				}
				disWidowFileName = jobApplication.getJobApplicationOrther().getDestituteWidowPath();
				disWidowFileNameValue = downloadImage(disWidowFileName);
				log.info("disWidowFileNameValue==>" + disWidowFileNameValue);

				if (disWidowFileName != null) {
					String disWidowFileNameArr[] = disWidowFileName.split("uploaded/");
					disWidowFileName = disWidowFileNameArr[1];
					if (fileName == null || fileName == "")
						fileName = disWidowFileName;
					else
						fileName = fileName + "," + disWidowFileName;
				}
			}

			jobAppPayment = jobApplication.getPaymentDetails();
			districtMasterList = commonDataService.getDistrictMasterList();
			talukMasterList = commonDataService.getTalukMasterList();
			villageMasterList = commonDataService.gellAllVillageMasterList();
			return "/pages/personnelHR/viewApprovalJobApplication.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void cancelEntryPage() {
		log.info("Cancel page :::::::");
		try {
			selectedJobApplication = new JobApplication();
			jobApplicationApproval = new JobApplicationApproval();
			photograph = null;
			pstmFileName = null;
			cerFileName = null;
			disableButton = false;
			uploadGovDisable = false;
			uploadDisable = false;
			uploadGovDisable = false;

			loadJobApplicationList();
			onChangePriority();
			disableEnablephyChallenged();
			disableExServiceman();
			disableDestitureWin();
		} catch (Exception e) {
			log.error(" Error Occured While cancel View Page ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String showJobApplicationScreen(String screenName) {
		log.info("showJobApplicationScreen called...");
		try {
			cancelEntryPage();
			if (screenName.equalsIgnoreCase("JOBPRI")) {
				return "/pages/personnelHR/createJobApplicationPrimary.xhtml?faces-redirect=true";
			} else if (screenName.equalsIgnoreCase("JOBADD")) {
				return "/pages/personnelHR/createJobApplicationAdditionalInfo.xhtml?faces-redirect=true";
			}
			if (screenName.equalsIgnoreCase("JOBEDU")) {
				return "/pages/personnelHR/createJobApplicationEducation.xhtml?faces-redirect=true";
			}
			if (screenName.equalsIgnoreCase("JOBWORK")) {
				return "/pages/personnelHR/createJobApplicationWorkExp.xhtml?faces-redirect=true";
			}
			if (screenName.equalsIgnoreCase("JOBOTHER")) {
				return "/pages/personnelHR/createJobApplicationOtherInfo.xhtml?faces-redirect=true";
			}
			if (screenName.equalsIgnoreCase("JOBPAY")) {
				validateDDDate();
				return "/pages/personnelHR/createJobApplicationPayment.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			log.error(" Error Occured showJobApplicationScreen form ", e);
		}
		return null;
	}
	
	public void validateDDDate(){
		log.info("validateDDDate called");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if(jobApplication.getId()!=null && jobApplication.getJobAdvertisement().getId()!=null) {
			log.info("JobAdvertisement id====>"+jobApplication.getId());
			String URL =JOB_SERVER_URL + "/getvalidatedddatebyjobadvertisementid/"
					+ jobApplication.getJobAdvertisement().getId();
			log.info("URL----------------->"+URL);

			baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			JobAdvertisement validateDDDateObject = mapper.readValue(jsonResponse, new TypeReference<JobAdvertisement>() {
				
			});
			log.info("validateDDDateObject=====>"+validateDDDateObject.getOnlineStartDate());
			log.info("validateDDDateObject=====>"+validateDDDateObject.getOnlineEndDate());
			setOnlineStartDate((Date) validateDDDateObject.getOnlineStartDate());
			setOnlineEndDate((Date) validateDDDateObject.getOnlineEndDate());
			}
		}catch (Exception e) {
			log.error("Exception ",e);
		}
			
		
		
	}

	/*
	 * public StreamedContent getDownloadFile() { InputStream stream =
	 * FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream(
	 * "/home/user1/kk/Tex/Cooptext_Workspace_Ano/jobapplication/SS/uploaded/index.jpeg"
	 * ); downloadFile = new DefaultStreamedContent(stream, "image/jpg",
	 * "downloaded_optimus.jpg");
	 * 
	 * log.info("downloadFile==>"+downloadFile); return downloadFile; }
	 */

	public StreamedContent getDownloadFile(byte[] value) {
		try {
			if (value == null || value == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				return new DefaultStreamedContent(
						externalContext.getResourceAsStream("/assets/images/profile-photo.png"), "image/png");
			}
			return new DefaultStreamedContent(new ByteArrayInputStream(value), "image/png", "downloadimage");
		} catch (Exception e) {
			log.error(" Error in while getting uploaded employee Image", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public byte[] downloadImage(String imgPath) {
		byte[] downloadName = null;
		try {
			File file = new File(imgPath);
			downloadName = new byte[(int) file.length()];

			log.info("downloadName==>" + downloadName);

			FileInputStream fis = new FileInputStream(file);
			fis.read(downloadName); // read file into bytes[]
			fis.close();
		} catch (Exception e) {
			log.error("convertImage error==>" + e);
		}
		return downloadName;
	}
	
	public String authentication() throws IOException {
		log.info("Authentication called........");
		log.info("ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			getNotficationNumber();
		}
		return null;
	}

}
