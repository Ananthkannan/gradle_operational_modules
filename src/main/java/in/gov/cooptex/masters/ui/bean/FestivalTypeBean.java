package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.FestivalTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("festivalTypeBean")
public class FestivalTypeBean {

	private final String CREATE_PAGE = "/pages/masters/createFestivalMaster.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listFestivalMaster.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/viewFestivalMaster.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	FestivalTypeMaster festivalTypeMaster, selectedFestivalTypeMaster;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Setter
	@Getter
	List<FestivalTypeMaster> festivalTypeMasterList;

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;
	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<FestivalTypeMaster> lazyFestivalTypeMasterList;

	public String showFestivalTypeListPageAction() {
		log.info("<===== Starts FestivalTypeBean.showFestivalTypeListPageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("ADD")&&!action.equalsIgnoreCase("LIST")
					&& (selectedFestivalTypeMaster == null || selectedFestivalTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create FestivalType called..");
				festivalTypeMaster = new FestivalTypeMaster();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				loadAllFestivalTypeTypes();
				log.info("edit FestivalType called..");
				String url = SERVER_URL + "/festivaltypemaster/getById/"
						+ selectedFestivalTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					festivalTypeMaster = mapper.readValue(jsonResponse, new TypeReference<FestivalTypeMaster>() {
					});
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("View petitionType called..");

				String url = SERVER_URL+ "/festivaltypemaster/getById/"
						+ selectedFestivalTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						festivalTypeMaster = mapper.readValue(jsonResponse, new TypeReference<FestivalTypeMaster>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else if (action.equalsIgnoreCase("DELETE"))  {
				log.info("delete festivaltypemaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFestivalTypeMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showFestivalTypeListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends FestivalTypeBean.showFestivalTypeListPageAction ===========>" + action);
		return LIST_PAGE;
	}

	public String deleteConfirm() {
		log.info("<===== Starts FestivalTypeBean.deleteConfirm ===========>" + selectedFestivalTypeMaster.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL 
					+ "/festivaltypemaster/deleteById/" + selectedFestivalTypeMaster.getId());

			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("FestivalType Details Deleted Successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFestivalTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete festivaltypemaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends FestivalTypeBean.deleteConfirm ===========>");
		return showfestivalTypeMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts FestivalTypeBean.saveOrUpdate =======>");
		try {

			String url = SERVER_URL + "/festivaltypemaster/saveorupdate";
			BaseDTO response = httpService.post(url, festivalTypeMaster);

			if (response != null && response.getStatusCode() == 0) {
				log.info("FestivalTypeMaster saved successfully..........");
				if (action.equalsIgnoreCase("ADD"))
					AppUtil.addInfo("Festival Details Added Successfully");
				else
					AppUtil.addInfo("Festival Details Updated Successfully");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends FestivalTypeBean.saveOrUpdate =======>");
		return showfestivalTypeMasterListPage();
	}

	public String showfestivalTypeMasterListPage() {
		log.info("<==== Starts FestivalTypeBean.showfestivalTypeMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		festivalTypeMaster = new FestivalTypeMaster();
		selectedFestivalTypeMaster = new FestivalTypeMaster();
		loadLazyFestivalTypeMasterList();
		log.info("<==== Ends FestivalTypeBean.showfestivalTypeMasterListPage =====>");
		return LIST_PAGE;
	}

	public List<FestivalTypeMaster> loadAllFestivalTypeTypes() {
		log.info("<======== FestivalTypeBean.loadAllFestivalTypeTypes calling ======>");
		festivalTypeMasterList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + "/festivaltypemaster/getActiveFestivalTypeTypes";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				festivalTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<FestivalTypeMaster>>() {
				});
			} else {
				festivalTypeMasterList = new ArrayList<>();
			}
			log.info("suppliermasterService organizationtypemasterList size" + festivalTypeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception suppliermasterService.loadAllOrganizationTypeMaster() ==========>", e);
		}
		return festivalTypeMasterList;
	}

	public void loadLazyFestivalTypeMasterList() {
		log.info("<===== Starts FestivalTypeBean.loadLazyFestivalTypeMasterList ======>");

		lazyFestivalTypeMasterList = new LazyDataModel<FestivalTypeMaster>() {
			/**
			 * festivaltypemaster
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<FestivalTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL+ "/festivaltypemaster/loadfestivaltype";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						festivalTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<FestivalTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("FestivalTypeBean size---------------->" + lazyFestivalTypeMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return festivalTypeMasterList;
			}

			@Override
			public Object getRowKey(FestivalTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FestivalTypeMaster getRowData(String rowKey) {
				for (FestivalTypeMaster festivalTypeMaster : festivalTypeMasterList) {
					if (festivalTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedFestivalTypeMaster = festivalTypeMaster;
						return festivalTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  FestivalTypeBean.loadLazyFestivalTypeMasterList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FestivalTypeBean.onRowSelect ========>" + event);
		selectedFestivalTypeMaster = ((FestivalTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends FestivalTypeBean.onRowSelect ========>" + selectedFestivalTypeMaster.getId());
	}
}
