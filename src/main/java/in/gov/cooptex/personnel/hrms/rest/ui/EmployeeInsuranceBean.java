package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.type.descriptor.java.CalendarDateTypeDescriptor;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.filemovement.model.FileMovement;
import in.gov.cooptex.admin.filemovement.model.FileMovementLog;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.LeaveTravelConCessionStatus;
import in.gov.cooptex.core.accounts.model.EmpInsuranceDetails;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.InsuranceMasterDTO;
import in.gov.cooptex.core.dto.LeaveTravelConcessionDto;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmpFmlyMemberInsuranceDetails;
import in.gov.cooptex.core.model.EmpInsuranceDetailsLog;
import in.gov.cooptex.core.model.EmpInsuranceDetailsNote;
import in.gov.cooptex.core.model.EmpPersonalInsuranceDetails;
import in.gov.cooptex.core.model.EmployeeConfirmationDetails;
import in.gov.cooptex.core.model.EmployeeFamilyDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.Insurance;
import in.gov.cooptex.core.model.Insurance.InsuranceStatus;
import in.gov.cooptex.core.model.InsuranceMaster;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.enums.CommonForwardStatus;
import in.gov.cooptex.operation.exceptions.UIException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@ManagedBean("employeeInsuranceBean")
public class EmployeeInsuranceBean extends CommonBean {

	private final String INSURANCE_LIST_PAGE = "/pages/personnelHR/insurance/listInsurance.xhtml?faces-redirect=true;";
	private final String INSURANCE_ADD_PAGE = "/pages/personnelHR/insurance/createInsurance.xhtml?faces-redirect=true;";
	private final String INSURANCE_VIEW_PAGE = "/pages/personnelHR/insurance/viewInsurance.xhtml?faces-redirect=true;";
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	public static final String INSURANCE_URL = SERVER_URL + "/employee/insurance";
	Double sanctionedAmt = 0D;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Insurance insurance;

	@Getter
	@Setter
	Insurance selectedInsurance;

	@Getter
	@Setter
	List<Insurance> insuranceList;

	@Getter
	@Setter
	List<InsuranceMaster> insuranceMasterList;

	@Getter
	@Setter
	InsuranceMaster selectedInsuranceMaster;

	@Getter
	@Setter
	Long selectedInsuranceId;

	@Getter
	@Setter
	List<EmployeeFamilyDetails> employeeFamilyList;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	UserMaster userMaster;

	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<EntityMaster> headAndRegionList;

	@Getter
	@Setter
	EntityMaster selectedHoRo;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypelist = new ArrayList<>();

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	List<EntityMaster> entityList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department selectedDepartement;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeMaster selectedEmployee;

	@Getter
	@Setter
	List<UserMaster> forwardToList;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	InsuranceMasterDTO insuranceMasterDTO;

	@Getter
	@Setter
	List<InsuranceMasterDTO> insuranceMasterDTOList;

	@Getter
	@Setter
	LazyDataModel<InsuranceMasterDTO> empInsuranceLazyList;

	@Getter
	@Setter
	EmpInsuranceDetails empInsuranceDetails = new EmpInsuranceDetails();

	@Getter
	@Setter
	EmployeeConfirmationDetails employeePayDetail;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean disabledMonth = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	private Boolean exitInsuranceFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	Boolean familyFlag = true;

	@Getter
	@Setter
	Boolean insuranceFlag = true;

	@Getter
	@Setter
	Boolean otherInsuranceFlag = true;

	@Getter
	@Setter
	String selectedPaymentCycle;

	@Getter
	@Setter
	private Boolean viewAdd, viewApprove, viewEdit, viewDelete, forwardFor;

	@Getter
	@Setter
	LazyDataModel<Insurance> lazyInsuranceModel;

	@Getter
	@Setter
	List<RelationshipMaster> relationshipMasterList = new ArrayList<>();

	@Getter
	@Setter
	RelationshipMaster selectedRelation;

	@Getter
	@Setter
	Map<String, Integer> startMonthList;

	@Getter
	@Setter
	Map<String, Integer> maturityMonthList;

	@Getter
	@Setter
	List<Integer> startYearList;

	@Getter
	@Setter
	List<Integer> maturityYearList;

	@Getter
	@Setter
	EmpPersonalInsuranceDetails empPersonalInsuranceDetails;

	@Getter
	@Setter
	List<EmpPersonalInsuranceDetails> empPersonalInsuranceDetailsList;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	EmpPersonalInsuranceDetails selectedEmpPersonalInsurance;

	@Getter
	@Setter
	List<EmpFmlyMemberInsuranceDetails> empFmlyMemberDetails = new ArrayList<>();

	@Getter
	@Setter
	Double insurancePremium;

	@Getter
	@Setter
	Double sanctionedAmount;

	@Getter
	@Setter
	Double insuredAmount;

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@Getter
	@Setter
	private Integer maturityMonth;

	@Getter
	@Setter
	private Integer maturityYear;

	@Getter
	@Setter
	private InsuranceMasterDTO selectedInsuranceMasterDTO = new InsuranceMasterDTO();

	private static final String GROUP_MEDICAL = "Group Medical";

	// List page call function
	public String showInsuranceListPage() {
		log.info("<====Starts InsuranceBean.showInsuranceListPage()  ====>");
		clear();
		loadLazyEmpInsuranceList();
		selectedInsurance = new Insurance();
		headAndRegionList = commonDataService.loadHeadAndRegionalOffice();
		forwardToList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
		insuranceMasterList = commonDataService.getAllInsurance();
		startMonthList = Util.getMonthIdsByName();
		maturityMonthList = Util.getMonthIdsByName();
		startYearList = loadYearList();
		maturityYearList = getPreviousCurrentYearList();
		empPersonalInsuranceDetails = new EmpPersonalInsuranceDetails();
		empPersonalInsuranceDetailsList = new ArrayList<>();
		log.info("<<--- insuranceMasterList size..." + insuranceMasterList.size());
		return INSURANCE_LIST_PAGE;
	}

	// Load lazy insurance list
	public void loadLazyEmpInsuranceList() {
		log.info("<===== Starts InsuranceDetailsBean.loadLazyEmpInsuranceList ======>");
		empInsuranceLazyList = new LazyDataModel<InsuranceMasterDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<InsuranceMasterDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					mapper = new ObjectMapper();
					log.info("Pagination request :::" + paginationRequest);
					String url = INSURANCE_URL + "/lazyInsuranceDetailsList";
					log.info("url==>" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						log.info("get TourProgram list" + response.getTotalRecords());
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						insuranceMasterDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<InsuranceMasterDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());

						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in getTDSList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return insuranceMasterDTOList;
			}

			@Override
			public Object getRowKey(InsuranceMasterDTO res) {
				log.info("Get Lazy load Insurance getRowKey:" + res.getInsuranceId());
				return res != null ? res.getInsuranceId() : null;
			}

			@Override
			public InsuranceMasterDTO getRowData(String rowKey) {
				try {
					for (InsuranceMasterDTO obj : insuranceMasterDTOList) {
						if (obj.getInsuranceId().equals(Long.valueOf(rowKey))) {
							log.info("Get Lazy load Insurance getRowKey:" + obj.getInsuranceId());
							insuranceMasterDTO = obj;
							return obj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in Insurance getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};
		log.info("<===== Ends InsuranceDetailsBean.loadLazyEmpInsuranceList ======>" + insuranceMasterDTOList);
	}

	// Load HORO based Entity Type List
	public void loadAllEntityTypesByRegionId() {
		try {
			if (selectedHoRo != null && !selectedHoRo.getName().equalsIgnoreCase("HEAD OFFICE")) {
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ selectedHoRo.getId();
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					entityTypelist = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
					});
					log.info("entitytypelistByHeadAndRegionalId list size is " + entityTypelist.size());
				}
			} else {
				entityTypelist.add(selectedHoRo.getEntityTypeMaster());
				selectedEntityTypeMaster = selectedHoRo.getEntityTypeMaster();
				entityList.add(selectedHoRo);
				selectedEntityMaster = selectedHoRo;
				loadAllDepartement();
			}
		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
	}

	// Load HORO and Entity Type Based Entity List
	public void loadAllEntityByRegionandEntityTypeId() {
		try {
			if (selectedHoRo != null && selectedEntityTypeMaster != null) {
				Long regionId = selectedHoRo.getId();
				Long entityTypeId = selectedEntityTypeMaster.getId();
				String entityTypeName = selectedEntityTypeMaster.getEntityName();
				log.info("the selected entity name is...." + " " + entityTypeName);
				log.info("the selected region id...." + " " + regionId);
				log.info("the selected entityType id...." + " " + entityTypeId);
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
				log.info("entitylistByHeadAndRegionalIdAndEntityTypeId List size is" + " " + entityList.size());
			}
		} catch (Exception e) {
			log.info("The cause for Exception is" + " " + e.getCause());
		}
	}

	// Load All Department
	public void loadAllDepartement() {
		try {
			departmentList = commonDataService.getDepartment();
			log.info("the list returned form data base size is" + " " + departmentList.size());
		} catch (Exception e) {
			log.info("the cause of the exception is " + " " + e.getCause());
		}
	}

	// Load Employee Based Location and Department
	public void loadAllEmployeeByLocationAndDepartment() {
		log.info("<----Start loadAllEmployee");
		try {
			if (selectedEntityMaster != null && selectedDepartement != null) {
				String url = AppUtil.getPortalServerURL() + "/employee/insurance/getEmployeeByLocationAndDepartmentId/"
						+ selectedEntityMaster.getId() + "/" + selectedDepartement.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info("loadAllEmployee list size is " + employeeList.size());
				}
			}
		} catch (Exception e) {
			log.info("loadAllEmployee the cause of Error is" + " " + e.getCause());
		}
		log.info("<----End loadAllEmployee");
	}

	// Load All Relation Ship for drop down from Relation Ship Master
	/*
	 * public void loadFamilyRelationshipFromRelationshipMaster() {
	 * log.info("<===== start family relation ship master =========>"); try { String
	 * url = AppUtil.getPortalServerURL()
	 * +"/employeeprofile/getAllFamilyRelationShip"; BaseDTO baseDTO =
	 * httpService.get(url);
	 * 
	 * if (baseDTO != null) { ObjectMapper mapper = new ObjectMapper(); String
	 * jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * relationshipMasterList = mapper.readValue(jsonValue, new
	 * TypeReference<List<RelationshipMaster>>() { });
	 * log.info("relationshipMasterList list size is "+relationshipMasterList);
	 * //RequestContext context = RequestContext.getCurrentInstance();
	 * //context.execute("PF('addMember').show();"); } }catch(Exception e) {
	 * log.info("the cause of Error is"+" "+ e.getCause() ); }
	 * log.info("<===== end family relation ship master =========>"); }
	 */

	// Add Family Members Based on the Relation ship ID
	/*
	 * public void getandaddFamily() {
	 * log.info("<===== start family relation ship master =========>");
	 * List<EmployeeFamilyDetails> empFamilyList=new ArrayList<>(); try {
	 * if(employeeFamilyList!=null && employeeFamilyList.size() >
	 * selectedInsuranceMaster.getMaxPerson()) {
	 * log.info("Employee limit was exceed -----------------");
	 * errorMap.notify(ErrorDescription.getError(AdminErrorCode.
	 * FAMILY_LIST_WAS_EXCEED_THE_LIMIT).getErrorCode());
	 * RequestContext.getCurrentInstance().update("growls"); } else {
	 * if(selectedEmployee!=null && selectedRelation!=null) { String url =
	 * INSURANCE_URL +"/getFamilyByEmpAndRelationId/"+selectedEmployee.getId()+"/"+
	 * selectedRelation.getId(); BaseDTO baseDTO = httpService.get(url);
	 * 
	 * if (baseDTO != null) { ObjectMapper mapper = new ObjectMapper(); String
	 * jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * empFamilyList = mapper.readValue(jsonValue, new
	 * TypeReference<List<EmployeeFamilyDetails>>() { });
	 * log.info("relationshipMasterList list size is "+empFamilyList);
	 * if(empFamilyList!=null && empFamilyList.size() >0) {
	 * for(EmployeeFamilyDetails obj:empFamilyList) { EmployeeFamilyDetails
	 * family=new EmployeeFamilyDetails(); family=obj;
	 * if(employeeFamilyList.contains(family)) {
	 * errorMap.notify(ErrorDescription.getError(AdminErrorCode.
	 * THIS_RELATION_MEMBER_ALREADY_EXIST).getErrorCode());
	 * RequestContext.getCurrentInstance().update("growls"); } else
	 * employeeFamilyList.add(family); }
	 * log.info("employeeFamilyList list size is "+employeeFamilyList); }else {
	 * errorMap.notify(ErrorDescription.getError(AdminErrorCode.
	 * THIS_RELATION_MEMBER_NOT_AVAILABLE).getErrorCode());
	 * RequestContext.getCurrentInstance().update("growls"); } RequestContext
	 * context = RequestContext.getCurrentInstance();
	 * context.execute("PF('addMember').hide();"); } } } }catch(Exception e) {
	 * log.error("the cause of Error is" , e); }
	 * 
	 * log.info("<===== end family relation ship master =========>"); }
	 */

	// Load All Insurance From Insurance Master
	public void loadInsuranceMasterList() {
		if (selectedEmployee != null)
			insuranceFlag = false;
		/*
		 * if(selectedInsuranceMaster!=null && selectedInsuranceMaster.getName()!=null)
		 * { addFamilyMembers(); }
		 */
		selectedInsuranceId = null;
		selectedInsurance = null;
		insuranceMasterList = commonDataService.getAllInsurance();
	}

	// Get Amount and load employeeFamily from SelectedInsurance Master ID
	public void getInsuranceDetails() {
		log.info("<<-- Start getInsuranceDetails() -->");
		Date date = new Date();
		SimpleDateFormat monthFormat = new SimpleDateFormat("M");
		SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
		try {
			if (selectedInsuranceId != null && selectedInsuranceId < 0) {
				familyFlag = false;
				otherInsuranceFlag = true;
				log.info("selectedInsuranceId..." + selectedInsuranceId);
				getEmpPersonalInsurance();
				selectedInsuranceMaster = new InsuranceMaster();
				empInsuranceDetails.setTotalMembers(1);
				employeeFamilyList = new ArrayList<>();
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('addInsurance').show();");
			} else {
				if (insuranceMasterList != null) {
					for (InsuranceMaster obj : insuranceMasterList) {
						if (selectedInsuranceId != null && selectedInsuranceId.equals(obj.getId())) {
							otherInsuranceFlag = false;
							if ("Group Medical".equals(obj.getName())) {
								otherInsuranceFlag = true;
								empInsuranceDetails.setStartMonth(monthFormat.format(date));
								empInsuranceDetails.setStartYear(Integer.valueOf(yearFormat.format(date)));
							}
							log.info("selected insurance master........" + obj);
							log.info("selected insurance master start date........"
									+ selectedInsuranceMaster.getStartDate());
							selectedInsuranceMaster = obj;
							empInsuranceDetails.setInsuranceMaster(selectedInsuranceMaster);
							selectedPaymentCycle = obj.getType();
							empInsuranceDetails.setInsurancePremiumAmount(obj.getInsurancePremium());
							empInsuranceDetails.setSanctionedAmount(obj.getSanctionedAmount());
							empInsuranceDetails.setInsuranceAmount(obj.getInsuranceAmount());
							if (obj.getGroupInsurance() != null && obj.getGroupInsurance()) {
								log.info("<<-- inside the group insurance add family");
								addFamilyMembers();
								if (employeeFamilyList != null && !employeeFamilyList.isEmpty()) {
									familyFlag = true;
								}
							} else {
								log.info("<<-- inside the group family insurance false");
								familyFlag = false;
								empInsuranceDetails.setTotalMembers(1);
								empInsuranceDetails.setTotalPremium(obj.getInsurancePremium());
								// empInsuranceDetails.setTotalPremium(obj.getInsurancePremium());
								// getEmployeeBasicPay();
								Double totalPrimiumAmt = obj.getInsurancePremium() != null
										&& obj.getInsurancePremium() > 0 ? obj.getInsurancePremium() : 0D;
								Double sanctionedAmount = obj.getSanctionedAmount() != null
										&& obj.getSanctionedAmount() > 0 ? obj.getSanctionedAmount() : 0D;
								// Double salary=employeePayDetail.getBasicPay();
								// log.info("Employee basic pay amt............"+salary);

								Double empPayamt = totalPrimiumAmt - sanctionedAmount;

								if (empPayamt >= 0) {
									Double amt = empPayamt;
									log.info("Employee RecoveryAmount amt............" + amt);
									empInsuranceDetails.setRecoveryAmount(amt);
								}
								if (empPayamt <= 0) {
									Double amt = Math.abs(empPayamt);
									log.info("Employee PayableAmount amt............" + amt);
									empInsuranceDetails.setPayableAmount(amt);
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception while getInsuranceDetails()........ ", e);
		}
		log.info("<<-- End getInsuranceDetails() -->");
	}

	// Get Amount and load employeeFamily from SelectedInsurance Master
	public Double getInsuranceAmount(int count) {
		log.info("<====Starts InsuranceBean.getInsuranceAmount()  ====>" + count);
		Double amount = 0D;
		try {
			Long insuranceId = selectedInsuranceMaster.getId();
			BaseDTO response = new BaseDTO();
			if (insuranceId != null) {
				log.info("<<--: get insurance details id :" + insuranceId);
				response = httpService
						.get(SERVER_URL + "/employee/insurance/empFmlyMemberInsuranceDetails/" + insuranceId);
			}
			if (response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(response.getResponseContents());
				empFmlyMemberDetails = mapper.readValue(jsonValue,
						new TypeReference<List<EmpFmlyMemberInsuranceDetails>>() {
						});
				log.info("<<---- empPersonalInsuranceDetailsList..." + empFmlyMemberDetails.size());
				for (EmpFmlyMemberInsuranceDetails obj : empFmlyMemberDetails) {
					Integer cnt = (Integer) count;
					if (cnt.equals(obj.getMemberCount())) {
						amount = obj.getMemberPerAmount();
					}
				}
			}
		} catch (Exception e) {
			log.error("<====Exception while get getInsuranceAmount()", e);
		}
		log.info("<==== End InsuranceBean.getInsuranceAmount()  ====>");
		return amount;
	}

	// Clear the Form
	public void clear() {
		addButtonFlag = false;
		editButtonFlag = true;
		viewButtonFlag = true;
		deleteButtonFlag = true;
		finalApproveFlag = false;
		buttonFlag = false;
		familyFlag = true;
		insuranceFlag = true;
		otherInsuranceFlag = false;
		insurancePremium = 0D;
		sanctionedAmount = 0D;
		insuredAmount = 0D;

		empInsuranceDetails = new EmpInsuranceDetails();
		selectedInsuranceMaster = new InsuranceMaster();
		selectedEmpPersonalInsurance = new EmpPersonalInsuranceDetails();
		selectedInsuranceId = 0L;
		insuranceMasterDTO = new InsuranceMasterDTO();

		insurancePremium = 0D;
		selectedPaymentCycle = new String();
		selectedEmpPersonalInsurance = new EmpPersonalInsuranceDetails();
		insurance = new Insurance();
		employeeFamilyList = new ArrayList<>();
		empInsuranceDetails = new EmpInsuranceDetails();
		insuranceMasterDTO = new InsuranceMasterDTO();
		selectedHoRo = new EntityMaster();
		entityTypelist = new ArrayList<>();
		selectedEntityTypeMaster = new EntityTypeMaster();
		entityList = new ArrayList<>();
		selectedEntityMaster = new EntityMaster();
		departmentList = new ArrayList<>();
		selectedDepartement = new Department();
		employeeList = new ArrayList<>();
		insuranceMasterDTO = new InsuranceMasterDTO();
		selectedEmployee = new EmployeeMaster();
		selectedInsuranceMaster = new InsuranceMaster();
		selectedInsuranceId = 0L;
		empPersonalInsuranceDetails = new EmpPersonalInsuranceDetails();
		empPersonalInsuranceDetailsList = new ArrayList<>();
		forwardTo = new UserMaster();
	}

	// Edit Page Load the Data
	public void editLoad() {
		if (insuranceMasterDTO != null) {

			if (insuranceMasterDTO.getEmpPersonalInfo() != null) {
				Long entityId = insuranceMasterDTO.getEmpPersonalInfo().getWorkLocation().getId();
				selectedHoRo = commonDataService.findRegionByShowroomId(entityId);
				log.info("selected selectedHoRo ...." + selectedHoRo);

				loadAllEntityTypesByRegionId();
				selectedEntityTypeMaster = insuranceMasterDTO.getEmpPersonalInfo().getWorkLocation()
						.getEntityTypeMaster();
				log.info("selected selectedEntityTypeMaster ...." + selectedEntityTypeMaster);

				loadAllEntityByRegionandEntityTypeId();
				selectedEntityMaster = insuranceMasterDTO.getEmpPersonalInfo().getWorkLocation();
				log.info("selected selectedEntityMaster ...." + selectedEntityMaster);

				loadAllDepartement();
				selectedDepartement = insuranceMasterDTO.getEmpPersonalInfo().getCurrentDepartment();
				log.info("selected selectedDepartement ...." + selectedDepartement);

				loadAllEmployeeByLocationAndDepartment();
				selectedEmployee = insuranceMasterDTO.getEmpMaster();
				log.info("selected employee ...." + selectedEmployee);

				insuranceMasterList = commonDataService.getAllInsurance();
				selectedInsuranceMaster = insuranceMasterDTO.getInsuranceMaster();

				log.info("selected empselectedInsuranceMaster ...." + selectedInsuranceMaster);

				empInsuranceDetails = insuranceMasterDTO.getEmpInsuranceDetails();
				log.info("<<<----- empInsuranceDetails........." + empInsuranceDetails);
				if (selectedInsuranceMaster != null) {
					empInsuranceDetails.setInsuranceMaster(selectedInsuranceMaster);
					employeeFamilyList = insuranceMasterDTO.getEmployeeFamilyList();
					if(CollectionUtils.isNotEmpty(employeeFamilyList)) {
						for(EmployeeFamilyDetails family : employeeFamilyList) {
							if(family != null && family.getDateOfBirth() != null) {
								Integer ageInt = AppUtil.getAge(family.getDateOfBirth());
								ageInt = ageInt != null ? ageInt : 0;
								family.setAge(ageInt.longValue());
							}
						}
					}
							
					log.info("employee family list...." + employeeFamilyList.size());
				}
				EmpInsuranceDetailsNote empInsuranceNote = insuranceMasterDTO.getEmpInsuranceNote();
				EmpInsuranceDetailsLog empInsuranceLog = insuranceMasterDTO.getEmpInsuranceLog();
				insuranceMasterDTO.setForwardFor(empInsuranceNote.getFinalApproval());
				insuranceMasterDTO.setForwardTo(empInsuranceNote.getForwardTo().getId());
				insuranceMasterDTO.setNote(empInsuranceNote.getNote());
				insuranceMasterDTO.setInsuranceId(getEmpInsuranceDetails().getInsurance_id());

				if (empInsuranceDetails.getInsuranceMaster() != null
						&& empInsuranceDetails.getInsuranceMaster().getGroupInsurance() != null
						&& empInsuranceDetails.getInsuranceMaster().getGroupInsurance())
					familyFlag = true;
				else
					familyFlag = false;
				if (empInsuranceDetails.getEmpPersonalInsuranceDetails() != null
						&& empInsuranceDetails.getEmpPersonalInsuranceDetails().getId() != null) {
					log.info("Emp personal insurance details....");
					selectedInsuranceId = -1L;
					selectedEmpPersonalInsurance = insuranceMasterDTO.getEmpPersonalInsuranceDetails();
					familyFlag = false;
					otherInsuranceFlag = true;
				}

				if ("Group Medical".equals(selectedInsuranceMaster.getName())) {
					otherInsuranceFlag = false;
				}

				log.info("insuranceMaster............." + insuranceMasterDTO);
				log.info(
						"selected entity typeMaster and Ho/Fo is.." + selectedEntityTypeMaster + " and" + selectedHoRo);
			}

			InsuranceMaster insuranceMaster = insuranceMasterDTO.getInsuranceMaster();
			if (insuranceMaster != null) {
				selectedInsuranceId = insuranceMaster.getId();
			}
		}
	}

	// Other insurance selected after load the amount value
	public void loadEmpPersonalInsurance() {
		log.info("<====Starts InsuranceBean.insuranceListPageAction()  ====>" + empPersonalInsuranceDetails);
		try {
			if (selectedEmpPersonalInsurance != null) {
				log.info("<==== inside the seleted personal insurance  ====>");
				selectedPaymentCycle = selectedEmpPersonalInsurance.getRecoveryPeriod();
				empInsuranceDetails.setInsurancePremiumAmount(selectedEmpPersonalInsurance.getPremiumAmount());
				empInsuranceDetails.setTotalPremium(selectedEmpPersonalInsurance.getPremiumAmount());
				empInsuranceDetails.setSanctionedAmount(0D);
			}
			log.info("<====Starts InsuranceBean.insuranceListPageAction()  ====>");
		} catch (Exception e) {
			log.error("<==== Exception while loadEmpPersonalInsurance()..", e);
		}
		log.info("<====End InsuranceBean.insuranceListPageAction()  ====>");
	}

	// List page to redirected by Page Action
	public String insurancePageAction() {
		log.info("<====Starts InsuranceBean.insuranceListPageAction()  ====>" + action);
		try {
			insuranceMasterList = commonDataService.getAllInsurance();
			if (action.equalsIgnoreCase("ADD")) {
				insurance = new Insurance();
				employeeFamilyList = new ArrayList<>();
				clear();
				otherInsuranceFlag = false;
				familyFlag = false;
			} else if (action.equalsIgnoreCase("DELETE")) {
				if (Objects.isNull(selectedInsurance)) {
					log.info(": Selected employee insurance is : " + selectedInsurance);
					errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmDelete').show();");
					return null;
				}
			} else {
				if (Objects.isNull(insuranceMasterDTO)) {
					log.info(": Selected insurance is : " + insuranceMasterDTO);
					errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
					return null;
				}
				Long id = insuranceMasterDTO.getInsuranceId();
				String url = SERVER_URL + "/employee/insurance/getDetails/" + id + "/"
						+ (systemNotificationId != null ? systemNotificationId : 0);
				log.info(":: Selected employee insurance URL is ::" + url);
				BaseDTO response = httpService.get(url);
				if (response.getStatusCode() == 0) {
					log.info(":: Selected employee insurance success response ::");
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(response.getResponseContent());
					insuranceMasterDTO = mapper.readValue(jsonValue, new TypeReference<InsuranceMasterDTO>() {
					});
					log.info("insuranceMasterDTO details......" + insuranceMasterDTO);
					editLoad();

					setPreviousApproval(insuranceMasterDTO.getForwardFor());
					// String
					// status=insuranceMasterDTO.getEmpInsuranceLog()!=null?insuranceMasterDTO.getEmpInsuranceLog().getStage():"";
					if (insuranceMasterDTO.getForwardTo() != null) {
						if (loginBean.getUserMaster().getId().equals(insuranceMasterDTO.getForwardTo())) {
							log.info("insuranceMasterDTO status..." + insuranceMasterDTO.getStatus());
							if (CommonForwardStatus.REJECTED.toString().equalsIgnoreCase(insuranceMasterDTO.getStatus())
									|| CommonForwardStatus.FINAL_APPROVED.toString()
											.equalsIgnoreCase(insuranceMasterDTO.getStatus())) {
								approvalFlag = false;
								buttonFlag = true;
							} else {
								approvalFlag = true;
								buttonFlag = false;
							}
						} else {
							approvalFlag = false;
							buttonFlag = true;
						}
					}
					if (action.equalsIgnoreCase("VIEW")) {
						systemNotificationBean.loadTotalMessages();
						return INSURANCE_VIEW_PAGE;
					} else if (action.equalsIgnoreCase("EDIT")) {
						return INSURANCE_ADD_PAGE;
					}
				} else {
					log.info(":: Selected employee insurance failuer response ::");
					errorMap.notify(response.getStatusCode());
					return INSURANCE_VIEW_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in insuranceListPageAction :::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<====Ends InsuranceBean.insuranceListPageAction()  ====>");
		return INSURANCE_ADD_PAGE;
	}

	// Get Employee basic Pay from employee ID
	public void getEmployeeBasicPay() {
		log.info("<----Start getEmployeeBasicPay");
		try {
			if (selectedEmployee != null) {
				String url = AppUtil.getPortalServerURL() + "/employee/insurance/getEmployeeBasicPay/"
						+ selectedEmployee.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeePayDetail = mapper.readValue(jsonValue, new TypeReference<EmployeeConfirmationDetails>() {
					});
					log.info("loadAllEmployee list size is " + employeePayDetail);
				}

				if ((baseDTO == null || baseDTO.getResponseContent() == null)) {
					log.info("Employee Basic Pay is empty -----------------");
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.EMP_BASIC_PAY_IS_EMPTY).getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
				}
			}
		} catch (Exception e) {
			log.info("getEmployeeBasicPay the cause of Error is" + e);
		}
		log.info("<----End getEmployeeBasicPay");
	}

	// Load family members based insurance master dropDwon change
	public void addFamilyMembers() {
		log.info("<====Starts InsuranceBean.loadFamilyMemberDetails()  ====>");
		try {

			Long employeeId = 0L;
			if (!Objects.isNull(selectedEmployee) && !Objects.isNull(selectedEmployee.getId())) {
				employeeId = selectedEmployee.getId();
				log.info("employeeId............" + employeeId);
			}

			BaseDTO response = httpService.get(INSURANCE_URL + "/getEmployeefamilydetails/" + employeeId);
			if (response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(response.getResponseContents());
				employeeFamilyList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeFamilyDetails>>() {
				});
				if (employeeFamilyList == null || (employeeFamilyList != null && employeeFamilyList.size() <= 0)) {
					log.info("Employee limit was exceed -----------------");
					errorMap.notify(ErrorDescription.EMP_FAMILY_LIST_EMPTY.getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
				}
				if (employeeFamilyList != null && !employeeFamilyList.isEmpty()
						&& employeeFamilyList.size() > selectedInsuranceMaster.getMaxPerson()) {
					log.info("Employee limit was exceed -----------------");
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.FAMILY_LIST_WAS_EXCEED_THE_LIMIT).getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
				}
			} else {
				employeeFamilyList = new ArrayList<>();
				errorMap.notify(response.getStatusCode());
				RequestContext.getCurrentInstance().update("growls");
				log.info("<====== InsuranceBean.loadFamilyMemberDetails Ends====>");
			}
			insuranceAmountCalcualtion();
			log.info("<====== InsuranceBean.loadFamilyMemberDetails Ends====> size is ::" + employeeFamilyList.size());

		} catch (Exception e) {
			log.error(":: Exception occured while add Family Member Details ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<====Ends InsuranceBean.loadFamilyMemberDetails()  ====>");
	}

	// Insurance Amount Calculation based the Family members total
	public void insuranceAmountCalcualtion() {
		int familySize = (employeeFamilyList != null && employeeFamilyList.size() > 0) ? employeeFamilyList.size() + 1
				: 1;
		log.info("<<---- familySize...." + familySize);
		Double totalPrimiumAmt = getInsuranceAmount(familySize);
		log.info("<<---- totalPrimiumAmt...." + totalPrimiumAmt);
		// empInsuranceDetails.setSanctionedAmount(sanctionedAmt);
		sanctionedAmt = empInsuranceDetails.getSanctionedAmount();
		empInsuranceDetails.setTotalPremium(totalPrimiumAmt);
		empInsuranceDetails.setTotalMembers(familySize);
		// getEmployeeBasicPay();
		log.info("Employee sanctionedAmt amt............" + sanctionedAmt);
		Double empPayamt = 0D;
		try {
			/*
			 * Double salary=0.00; if(employeePayDetail!=null) {
			 * salary=employeePayDetail.getBasicPay(); }
			 * log.info("Employee basic pay amt............"+salary);
			 */

			if (totalPrimiumAmt != null && sanctionedAmt != null) {
				empPayamt = totalPrimiumAmt - sanctionedAmt;
			}
		} catch (Exception e) {
			log.error("<==== Exception while get Employee basic pay get and caluclate..", e);
		}

		if (empPayamt >= 0) {
			Double amt = empPayamt;
			log.info("Employee RecoveryAmount amt............" + amt);
			empInsuranceDetails.setRecoveryAmount(amt);
			empInsuranceDetails.setPayableAmount(0.00);
		}
		if (empPayamt <= 0) {
			Double amt = Math.abs(empPayamt);
			log.info("Employee PayableAmount amt............" + amt);
			empInsuranceDetails.setPayableAmount(amt);
			empInsuranceDetails.setRecoveryAmount(0.00);
		}

		log.info("empInsuranceDetails......" + empInsuranceDetails);
	}

	// Save employee given other insurance details, and get other insurance drop
	// down Based on the employee added , employee id
	public void addAndGetEmpInsurance() {
		log.info("<====Starts InsuranceBean.submitEmpInsurance()  ====>");
		try {
			EmployeeMaster employee = selectedEmployee;
			empPersonalInsuranceDetails.setEmployeeMaster(employee);
			log.info("<<--- empPersonalInsurance Details is....." + empPersonalInsuranceDetails);
			BaseDTO response = new BaseDTO();
			if (empPersonalInsuranceDetails != null) {
				log.info(": Get empPersonal Insurance details get based on employee :");
				response = httpService.post(SERVER_URL + "/employee/insurance/saveEmpPersonalInsurance",
						empPersonalInsuranceDetails);
			}
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INSURANCE_CREATED_SUCCESS.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");

				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(response.getResponseContents());
				empPersonalInsuranceDetailsList = mapper.readValue(jsonValue,
						new TypeReference<List<EmpPersonalInsuranceDetails>>() {
						});
				log.info("<<---- empPersonalInsuranceDetailsList..." + empPersonalInsuranceDetailsList.size());
				log.info(": Employee insurance details saved successfully :");
			} else {
				errorMap.notify(ErrorDescription.INSURANCE_REJECTED_SUCCESS.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
			}
		} catch (Exception e) {
			log.error("<====Exception while get submitEmpInsurance()", e);
		}
		otherInsuranceFlag = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addInsurance').hide();");
		log.info("<==== End InsuranceBean.submitEmpInsurance()  ====>");
	}

	// Load the other insurance details drop down Based on the employee added ,
	// employee id
	public void getEmpPersonalInsurance() {
		log.info("<====Starts InsuranceBean.getEmpPersonalInsurance()  ====>");
		try {
			Long empId = selectedEmployee.getId();
			BaseDTO response = new BaseDTO();
			if (empId != null) {
				log.info("<<--: get insurance details :" + empId);
				response = httpService.get(SERVER_URL + "/employee/insurance/getEmpInsuranceDetails/" + empId);
			}
			if (response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(response.getResponseContents());
				empPersonalInsuranceDetailsList = mapper.readValue(jsonValue,
						new TypeReference<List<EmpPersonalInsuranceDetails>>() {
						});
				log.info("<<---- empPersonalInsuranceDetailsList..." + empPersonalInsuranceDetailsList.size());
				log.info(": Employee getEmpPersonalInsurance() details saved successfully :");
				// errorMap.notify(ErrorDescription.INSURANCE_CREATED_SUCCESS.getErrorCode());
			}
		} catch (Exception e) {
			log.error("<====Exception while get getEmpPersonalInsurance()", e);
		}
		otherInsuranceFlag = true;
		log.info("<==== End InsuranceBean.getEmpPersonalInsurance()  ====>");
	}

	// Final Form Submit the all details
	public String submitInsuranceRequest() {
		log.info("<====Starts InsuranceBean.submitInsuranceRequest()  ====>");
		try {
			if (empInsuranceDetails != null && empInsuranceDetails.getInsurancePremiumAmount() == null) {
				AppUtil.addWarn("Premium Amount is Empty");
				return null;
			}
			/*
			 * if(empInsuranceDetails!=null && empInsuranceDetails.getRecoveryAmount()==null
			 * || empInsuranceDetails.getRecoveryAmount()==0) {
			 * AppUtil.addWarn("Recovery Amount is Empty"); return null; }
			 */
			if (empInsuranceDetails != null && empInsuranceDetails.getInsuranceAmount() == null) {
				AppUtil.addWarn("Insured amount should not be Empty");
				return null;
			}
			if (insuranceMasterDTO.getNote() == null || insuranceMasterDTO.getNote().equals("")) {
				log.info("note is empty........");
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (forwardTo != null) {
				insuranceMasterDTO.setForwardTo(forwardTo.getId());
			}
			empInsuranceDetails.setEmpMaster(selectedEmployee);
			empInsuranceDetails.setInsuranceMaster(selectedInsuranceMaster);
			if (selectedInsuranceId < 0 && selectedEmpPersonalInsurance != null) {
				log.info("<<<--- selected insurance is Other insurance........");
				empInsuranceDetails.setTotalMembers(1);
				empInsuranceDetails.setTotalPremium(empInsuranceDetails.getInsurancePremiumAmount());
				if (employeeFamilyList != null && !employeeFamilyList.isEmpty()
						&& selectedInsuranceMaster.getMaxPerson() != null
						&& employeeFamilyList.size() > selectedInsuranceMaster.getMaxPerson()) {
					log.info("Employee limit was exceed -----------------");
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.FAMILY_LIST_WAS_EXCEED_THE_LIMIT).getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
				}
			}

			insuranceMasterDTO.setEmpMaster(selectedEmployee);
			insuranceMasterDTO.setInsuranceMaster(selectedInsuranceMaster);
			insuranceMasterDTO.setEmpInsuranceDetails(empInsuranceDetails);
			insuranceMasterDTO.setEmployeeFamilyList(employeeFamilyList);
			insuranceMasterDTO.setEmpPersonalInsuranceDetails(selectedEmpPersonalInsurance);

			BaseDTO response = new BaseDTO();
			if (insuranceMasterDTO != null) {
				log.info(": Save employee insurance details :");
				response = httpService.post(SERVER_URL + "/employee/insurance/saveInsuranceDetails",
						insuranceMasterDTO);

			}
			if (response.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("ADD")) {
					log.info(": Employee insurance details saved successfully :");
					errorMap.notify(ErrorDescription.INSURANCE_CREATED_SUCCESS.getErrorCode());
				} else {
					log.info(": Employee insurance details updated successfully :");
					errorMap.notify(ErrorDescription.INSURANCE_UPDATED_SUCCESS.getErrorCode());
				}
				showInsuranceListPage();
				return INSURANCE_LIST_PAGE;
			} else if (response.getStatusCode().equals(ErrorDescription.APPFEATURE_ALREADY_EXISTS.getErrorCode())) {
				AppUtil.addWarn("Insurance already added for this insurance type");
			} else {
				log.info(": Employee insurance details failed to {} :", action.toLowerCase());
				errorMap.notify(response.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured in InsuranceBean.submitInsuranceRequest ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<====Ends InsuranceBean.submitInsuranceRequest()  ====>");
		return null;
	}

	// Remove Family members from the familList and reduce the Total Premium amounts
	// based total family count
	public void removeFamilyMembers(EmployeeFamilyDetails empployeeFamily) {
		log.info("<====Starts InsuranceBean.removeFamilyMembers()  ====>" + employeeFamilyList.size());
		if (employeeFamilyList != null) {

			log.info(":: Add employee insurance details ::");
			int listsize = employeeFamilyList.size() + 1;

			empInsuranceDetails.setTotalMembers(listsize - 1);
			Double totalamt = getInsuranceAmount(listsize - 1);
			empInsuranceDetails.setTotalPremium(totalamt);
			employeeFamilyList.remove(empployeeFamily);
			insuranceAmountCalcualtion();

			log.info("After Insurance details :" + empInsuranceDetails);
			log.info(":: Selected employee total amount is ::" + totalamt);
		}
		log.info("<====Ends InsuranceBean.removeFamilyMembers()  ====>" + employeeFamilyList.size());
	}

	/**
	 * Employee if married only added children added only two, and spouse add.
	 * Employee if not married added only parents. Selected insurance master id
	 * based maxPerson, if not meet the Max person count. After added the employee
	 * Other relationship.
	 */
	// Check the added family list, if condition based
	public void checkTheFamilyAddedList(String commingRelation) {
		log.error("<---- Start checkTheFamilyAddedList() ----->");
		if (commingRelation != null && employeeFamilyList != null && employeeFamilyList.size() > 0) {
			log.error("<---- Inside the condition loop ----->");
			int daughter = 0;
			int son = 0;
			String listRelationName;
			String relationName = commingRelation;

			for (EmployeeFamilyDetails list : employeeFamilyList) {
				listRelationName = list.getRelationshipMaster().getName().toString();
				if ((!relationName.equalsIgnoreCase("Daughter") && !relationName.equalsIgnoreCase("Son"))) {
					log.info("inside fo no parent ......");
					if (listRelationName.equalsIgnoreCase(relationName)) {
						errorMap.notify(ErrorDescription.getError(AdminErrorCode.THIS_RELATION_MEMBER_ALREADY_EXIST)
								.getErrorCode());
						RequestContext.getCurrentInstance().update("growls");
					}
				} else {
					log.info("inside children......");
					log.info("daughter and son counts....." + daughter + " / " + son);
					if (daughter == 1 && son == 1) {
						log.info("Relation limit was exceed -----------------");
						errorMap.notify(ErrorDescription.getError(AdminErrorCode.FAMILY_LIST_WAS_EXCEED_THE_LIMIT)
								.getErrorCode());
						RequestContext.getCurrentInstance().update("growls");
					} else if (daughter > 1 || son > 1) {
						log.info("Relation limit was exceed -----------------");
						errorMap.notify(ErrorDescription.getError(AdminErrorCode.FAMILY_LIST_WAS_EXCEED_THE_LIMIT)
								.getErrorCode());
						RequestContext.getCurrentInstance().update("growls");
					}
					if ((relationName.equalsIgnoreCase("Daughter"))) {
						daughter = daughter + 1;
					}
					if (relationName.equalsIgnoreCase("Son")) {
						son = son + 1;
					}
				}
			}
		}
		log.error("<---- End checkTheFamilyAddedList() ----->");
	}

	// Delete the one record form the listing page
	public void deleteEmployeeInsurance() {
		log.info(": Selected employee insurance id is : " + selectedInsurance.getId());
		try {
			String URL = AppUtil.getPortalServerURL() + "/employee/insurance/deleteInsurance/"
					+ insuranceMasterDTO.getInsuranceId();
			log.info("Delete employee insurance by id URL is : " + URL);

			BaseDTO baseDTO = httpService.delete(URL);

			if (Objects.isNull(baseDTO)) {
				log.info("Delete employee insurance by id response is : " + baseDTO);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info("Employee insurance by id deleted successfully : " + baseDTO);
				errorMap.notify(ErrorDescription.INSURANCE_DELETED_SUCCESS.getErrorCode());
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				return;
			}
			insuranceMasterDTO = new InsuranceMasterDTO();
			showInsuranceListPage();
		} catch (Exception e) {
			log.info("Exception in selected employee insurance by id : ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	// On click or select the radio button from the list page, call and select the
	// object
	public void onRowSelect() {
		log.error("<----Start employee insurance onselect: ----->");
		addButtonFlag = true;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		exitInsuranceFlag = true;
		selectedInsuranceMasterDTO = new InsuranceMasterDTO();
		if (StringUtils.isNotEmpty(insuranceMasterDTO.getStatus())) {
			if (insuranceMasterDTO.getStatus().equalsIgnoreCase(CommonForwardStatus.REJECTED.toString())) {
				editButtonFlag = false;
				deleteButtonFlag = false;
			} else {
				editButtonFlag = true;
			}

			if (CommonForwardStatus.FINAL_APPROVED.toString().equals(insuranceMasterDTO.getStatus())
					&& !GROUP_MEDICAL.equals(insuranceMasterDTO.getInsuranceType())) {
				exitInsuranceFlag = false;
				selectedInsuranceMasterDTO.setInsuranceId(insuranceMasterDTO.getInsuranceId());
			} else {
				exitInsuranceFlag = true;
			}
		} else {
			editButtonFlag = false;
			exitInsuranceFlag = true;
		}
		log.info("<=== Ends employee insurance.onRowSelect ========>");
	}

	// Forward For approved functions
	public String approveInsurance() {
		log.error("<----Start Insurance Bean approval: ----->");
		BaseDTO baseDTO = null;
		try {
			if (previousApproval != true && insuranceMasterDTO.getForwardTo() == null) {
				errorMap.notify(ErrorDescription.FORWARD_TO_EMPTY.getErrorCode());
				log.info("Forward To is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (insuranceMasterDTO.getForwardFor() == null) {
				errorMap.notify(ErrorDescription.FORWARD_FOR_ISEMPTY.getErrorCode());
				log.info("Forward for is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (insuranceMasterDTO.getRemarks() == null) {
				errorMap.notify(ErrorDescription.LOG_REMARKS_EMPTY.getErrorCode());
				log.info("Forward remarks is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if (forwardTo != null && forwardTo.getId() != null) {
				insuranceMasterDTO.setForwardTo(forwardTo.getId());
			}
			log.info("tour program id----------" + insuranceMasterDTO.getInsuranceId());
			log.info("previousApproval ----------" + insuranceMasterDTO.getForwardFor());
			log.info("forward to ----------" + insuranceMasterDTO.getForwardTo());
			baseDTO = new BaseDTO();
			if (previousApproval == false && insuranceMasterDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Approved  ----->");
				insuranceMasterDTO.setStatus(CommonForwardStatus.APPROVED.toString());
			} else if (previousApproval == true && insuranceMasterDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Final Approved  ----->");
				insuranceMasterDTO.setStatus(CommonForwardStatus.FINAL_APPROVED.toString());
			} else {
				log.error("<---- Approve Status change on Approved  ----->");
				insuranceMasterDTO.setStatus(CommonForwardStatus.APPROVED.toString());
			}
			log.info("approve remarks=========>" + insuranceMasterDTO.getRemarks());
			String url = INSURANCE_URL + "/approveInsurance";
			baseDTO = httpService.post(url, insuranceMasterDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INSURANCE_APPROVED_SUCCESS.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showInsuranceListPage();
			}
		} catch (Exception e) {
			log.error("approveInsurance method inside exception-------", e);
		}
		return null;
	}

	// Forward For rejected functions
	public String rejectInsurance() {
		log.error("<----Start Insurance Bean.rejected: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("Insurance selected id----------" + insuranceMasterDTO.getId());
			if (forwardTo != null) {
				insuranceMasterDTO.setForwardTo(forwardTo.getId());
			}
			baseDTO = new BaseDTO();
			insuranceMasterDTO.setStatus(CommonForwardStatus.REJECTED.toString());
			String url = INSURANCE_URL + "/rejectInsurance";
			baseDTO = httpService.post(url, insuranceMasterDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INSURANCE_REJECTED_SUCCESS.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showInsuranceListPage();
			}
		} catch (Exception e) {
			log.error("rejectInsurance method inside exception-------", e);
		}
		return null;
	}

	// Load Maturity Year List after 30 years from stared year
	public void loadMaturityYear() {
		log.info("<<<--- Start load maturity Year List -->>>");
		maturityYearList = loadYearList();
	}

	// Load the year list for start year and matured year
	public List<Integer> loadYearList() {
		log.info("<<<--- Start load Year List -->>>");
		List<Integer> yearList = new ArrayList<>();
		try {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, -1); // to get previous year add -1

			// Calendar cal = Calendar.getInstance();
			// Date today = cal.getTime();
			// cal.add(Calendar.YEAR, 1); // to get previous year add -1
			// Date nextYear = cal.getTime();

			int currentYear = cal.get(Calendar.YEAR);
			if (empInsuranceDetails != null && empInsuranceDetails.getStartYear() != null) {
				log.info("<<<--- Start selected year -->>>" + empInsuranceDetails.getStartYear());
				currentYear = empInsuranceDetails.getStartYear();
			}
			for (int i = 0; i < 30; i++) {
				int year = currentYear + i;
				yearList.add((Integer) (year));
			}
			log.info("<<-- yearList size()...." + yearList.size());
		} catch (Exception e) {
			log.error("<<<--- Exception while load Years List", e);
		}
		log.info("<<<--- End load Year List -->>>");
		return yearList;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts EmployeeInsurance-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String insuranceId = "";
		String stage = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			insuranceId = httpRequest.getParameter("insuranceId");
			stage = httpRequest.getParameter("stage");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (insuranceId != null) {
			log.info("insurance id----------------->" + insuranceId);
			Long fileId = Long.parseLong(insuranceId);
			insuranceMasterDTO = new InsuranceMasterDTO();
			insuranceMasterDTO.setInsuranceId(fileId);
			insuranceMasterDTO.setStatus(stage);
			systemNotificationId = Long.parseLong(notificationId);
			action = "VIEW";
			insurancePageAction();
		}
		return null;
	}

	private List<Integer> getPreviousCurrentYearList() {
		Calendar now = Calendar.getInstance();
		int currentYear = now.get(Calendar.YEAR);

		List<Integer> yearList = Arrays.asList(currentYear, currentYear - 1);
		return yearList;
	}

	private void flagClear() {
		addButtonFlag = false;
		editButtonFlag = true;
		viewButtonFlag = true;
		deleteButtonFlag = true;
		finalApproveFlag = false;
		buttonFlag = false;
		familyFlag = true;
		insuranceFlag = true;
		otherInsuranceFlag = false;
	}

	public String closeInsurance() {
		log.info("EmployeeInsuranceBean. closeInsurance() - START");
		try {
			String url = INSURANCE_URL + "/exitInsurance";
			Long insuranceId = selectedInsuranceMasterDTO != null ? selectedInsuranceMasterDTO.getInsuranceId() : null;
			InsuranceMasterDTO insuranceMasterDTOObj = new InsuranceMasterDTO();
			insuranceMasterDTOObj.setMaturityMonth(maturityMonth);
			insuranceMasterDTOObj.setMaturityYear(maturityYear);
			insuranceMasterDTOObj.setInsuranceId(insuranceId);

			BaseDTO baseDTO = httpService.post(url, insuranceMasterDTOObj);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					RequestContext.getCurrentInstance().execute("PF('exitInsurance').hide();");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.EXIT_INSURANCE_SAVE_SUCCESS).getErrorCode());
					loadLazyEmpInsuranceList();
					flagClear();
				} else {
					String errorMessage = baseDTO.getErrorDescription();
					if (org.apache.commons.lang.StringUtils.isNotEmpty(errorMessage)) {
						AppUtil.addWarn(errorMessage);
						return null;
					} else {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception at closeInsurance()", e);
		}
		log.info("EmployeeInsuranceBean. closeInsurance() - END");
		return null;
	}

}
