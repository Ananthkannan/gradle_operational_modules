package in.gov.cooptex.operation.rest.ui;

import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.UserType;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardBean")
@Scope("session")
public class StockOutwardBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/weaversModule/listStockOutward.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/weaversModule/createStockOutward.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/weaversModule/viewStockOutward.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Setter
	@Getter
	StreamedContent pdfFile;

	@Setter
	@Getter
	String bundleNumberFrom;

	@Setter
	@Getter
	String bundleNumberTo;

	@Setter
	@Getter
	StockTransfer stockTransfer;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	List<Object> payToPayList;

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<PurchaseOrder> purchaseOrderList;

	@Getter
	@Setter
	Set<EntityMaster> dnpOfficeSet;

	@Getter
	@Setter
	Set<EntityMaster> wareHouseSet;

	@Getter
	@Setter
	String stockToName;

	@Getter
	@Setter
	String stockToGST;

	@Getter
	@Setter
	String[] stockToAddress;

	@Getter
	@Setter
	SupplierMaster loginSupplierMaster;

	@Getter
	@Setter
	String stockFromName;

	@Getter
	@Setter
	String stockFromGST;

	@Getter
	@Setter
	String[] stockFromAddress;

	@Getter
	@Setter
	Double totalOrderedQty = 0.0;
	@Getter
	@Setter
	Double totalDispatchedQty = 0.0;
	@Getter
	@Setter
	Double totalCurrentDispatchedQty = 0.0;

	@Getter
	@Setter
	Double totalItemNetTotal = 0.0;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	StockTransferItemsBundles bundles = new StockTransferItemsBundles();

	@Getter
	@Setter
	List<StockTransferItemsBundles> stockTransferItemsBundleListTemp;

	@Getter
	@Setter
	Double totalBundleQuantity = 0.0;

	@Getter
	@Setter
	String action;

	@PostConstruct
	public void init() {
		log.info("init is called.");
		getStockOutwardPage();
	}

	public String getStockOutwardPage() {
		log.info("getStockOutwardPage is called.");
		// SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		stockTransferResponse = new StockTransferResponse();
		addButtonFlag = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	public String clearSearch() {
		log.info("clear Search is called");
		return getStockOutwardPage();
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("dnpCodeOrName")) {
				request.setDnpCodeOrName(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		addButtonFlag = false;

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/search";
		baseDTO = httpService.post(URL, request);
		log.info("StockOutwardBean.getSearchData Method Complted");

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();

				try {

					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	/*******************************************************************************************
	 * 
	 * Add Page Code Starts
	 * 
	 ******************************************************************************************/

	public String gotoAddPage() {

		stockTransfer = new StockTransfer();
		stockTransfer.setPurchaseOrderList(new ArrayList<>());
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		stockTransfer.setTransportMaster(null);
		bundles = new StockTransferItemsBundles();
		selectedTransportTypeMaster = null;
		transportTypeMasterList = null;
		transportMasterList = null;
		purchaseOrderList = null;
		stockTransferItemsBundleListTemp = null;

		wareHouseSet = null;

		stockFromName = null;
		stockFromGST = null;
		stockFromAddress = null;

		stockToName = null;
		stockToGST = null;
		stockToAddress = null;

		loginSupplierMaster = getsupplierByuser();

		if (loginSupplierMaster != null) {
			yesOrNoList = stockTransferUtility.loadYesNoList();
			payToPayList = stockTransferUtility.loadTransportChargeType();
			transportTypeMasterList = commonDataService.getAllTransportTypes();
			dnpOfficeSet = commonDataService.findEntityList(loginSupplierMaster.getId());
		}

		return ADD_PAGE_URL;
	}

	private SupplierMaster getsupplierByuser() {
		SupplierMaster supplierMaster = null;
		try {
			String url = AppUtil.getPortalServerURL() + "/supplier/master/getSupplierByUserId/"
					+ loginBean.getUserMaster().getId();
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			supplierMaster = mapper.readValue(jsonResponse, new TypeReference<SupplierMaster>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return supplierMaster;
	}

	public void loadTransportByTranportType() {
		transportMasterList = null;
		// stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void onSelectTransport() {
		log.info("onSelectTransport is called");
		if (stockTransfer.getTransportMaster() != null) {
			log.info("Selected Transport ID = [" + stockTransfer.getTransportMaster().getId() + "]");
		}
	}

	public void prepareFromDetails() {
		stockFromName = loginSupplierMaster.getCode() + " / " + loginSupplierMaster.getName();
		stockFromGST = loginSupplierMaster.getGstNumber();

//		if (loginSupplierMaster.getAddressMaster() != null) {
//			stockFromAddress = AppUtil.prepareAddress(loginSupplierMaster.getAddressMaster());
//		}
		if (loginSupplierMaster.getAddressId() != null) {
			stockFromAddress = AppUtil.prepareAddress(loginSupplierMaster.getAddressId());
		}
	}

	public void prepareToDetails() {
		EntityMaster warehouse = stockTransfer.getToEntityMaster();
		stockToName = warehouse.getCode() + " / " + warehouse.getName();
		stockToGST = warehouse.getGstNumber();
		if (stockTransfer.getToEntityMaster().getAddressMaster() != null) {
			stockToAddress = AppUtil.prepareAddress(stockTransfer.getToEntityMaster().getAddressMaster());
		}
	}

	public void afterDNPSelect() {

		wareHouseSet = null;
		stockTransfer.setToEntityMaster(null);
		purchaseOrderList = null;
		stockTransfer.setPurchaseOrderList(new ArrayList<>());
		stockTransfer.setPurchaseOrderListIds(null);
		stockTransfer.setStockTransferItemsList(new ArrayList<>());

		if (stockTransfer.getFromEntityMaster() != null && loginSupplierMaster != null) {

			wareHouseSet = commonDataService.findDeliveryEntityList(stockTransfer.getFromEntityMaster().getId(),
					loginSupplierMaster.getId());

			// Currently Logged in SupplierMaster Address taken as
			// StockFromAddress
			prepareFromDetails();

		} else {
			wareHouseSet = null;

			stockFromName = null;
			stockFromGST = null;
			stockFromAddress = null;

			stockToName = null;
			stockToGST = null;
			stockToAddress = null;

		}
	}

	public void afterShippingToSelect() {

		purchaseOrderList = null;
		stockTransfer.setPurchaseOrderList(new ArrayList<>());
		stockTransfer.setPurchaseOrderListIds(null);
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		// To Address is taken from Warehouse
		if (stockTransfer.getFromEntityMaster() != null && stockTransfer.getToEntityMaster() != null) {

			prepareToDetails();

			purchaseOrderList = commonDataService.findPurchaseOrderList(stockTransfer.getToEntityMaster().getId(),
					stockTransfer.getFromEntityMaster().getId(), loginSupplierMaster.getId());

		} else {
			stockToName = null;
			stockToGST = null;
			stockToAddress = null;
			purchaseOrderList = null;
		}
	}

	public void afterPurchaseOrderSelect() {
		log.info("afterPurchaseOrderSelect is executed");
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
	}

	private boolean validateAndPrepareObject() {

		if (stockTransfer.getFromEntityMaster() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_DNP_REQUIRED.getCode());
			return false;
		}

		if (stockTransfer.getToEntityMaster() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SHIPPING_TO_REQUIRED.getCode());
			return false;
		}

		if (loginSupplierMaster == null) {
			log.info("Login Supplier master is mandatory");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SUPPLIER_REQUIRED.getCode());
			return false;
		} else {
			stockTransfer.setSupplierMaster(loginSupplierMaster);
		}

		if (stockTransfer.getPurchaseOrderList() == null || stockTransfer.getPurchaseOrderList().isEmpty()) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_PO_REQUIRED.getCode());
			return false;
		}

		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {

			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				if (items.getCurrentDispatchedQty() != null) {

					Double maxValue = items.getOrderedQty() - items.getDispatchedQty();

					if (items.getCurrentDispatchedQty() < 0) {
						log.info("Current dispatch value is negative");
						Util.addWarn(
								errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_DISPATCH_NEGATIVE.getCode()));
						return false;
					}

					if (items.getCurrentDispatchedQty() > maxValue) {
						log.info("Current dispatch value is greater then maxValue");
						Util.addWarn(errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_DISPATCH_MORE.getCode())
								+ maxValue);
						return false;
					}
				}
			}
		}

		return true;
	}

	public String submit(String action) {

		if (validateAndPrepareObject()) {

			log.info("Submitting [" + action + "]");
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/create";
			BaseDTO baseDTO = null;
			try {

				stockTransfer.setPurchaseOrderListIds(new ArrayList<>());

				// Selected Purchase Orders
				if (stockTransfer != null && stockTransfer.getPurchaseOrderList() != null) {
					for (PurchaseOrder po : stockTransfer.getPurchaseOrderList()) {
						stockTransfer.getPurchaseOrderListIds().add(po.getId());
					}
				}

				if (action != null) {
					if (action.equals("save")) {
						stockTransfer.setStatus(StockTransferStatus.INITIATED);
					} else {
						stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
					}
				}

				stockTransfer.setTransferType(StockTransferType.SOCIETY_OUTWARD);

				// From Entity is not required for society stock outward
				// Stock Transfer happens from Supplier to product warehouse

				stockTransfer.setFromEntityMaster(null);

				baseDTO = httpService.post(url, stockTransfer);

				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("Stock Transfer saved successfully");
						if (stockTransfer.getStatus().equals(StockTransferStatus.INITIATED)) {
							errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
						} else if (stockTransfer.getStatus().equals(StockTransferStatus.SUBMITTED)) {
							AppUtil.addInfo("Stock Transfer Submitted Successfully");
						}
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			} catch (Exception exp) {
				log.error("Exception ", exp);
			}

			return LIST_PAGE_URL;

		} else {
			log.info("Validation Failed.");
			return null;
		}

	}

	public void generateData() {

		log.info("Generate Button is called...");

		if (stockTransfer.getPurchaseOrderList() != null && !stockTransfer.getPurchaseOrderList().isEmpty()) {
			ArrayList<Long> idList = new ArrayList<Long>();
			for (PurchaseOrder purchaseOrder : stockTransfer.getPurchaseOrderList()) {
				idList.add(purchaseOrder.getId());
			}
			Long[] idListObj = new Long[idList.size()];

			StockTransferRequest request = new StockTransferRequest();
			addButtonFlag = false;
			request.setIdList(idList.toArray(idListObj));
			stockTransfer.setStockTransferItemsList(commonDataService.loadStockTransferItems(request));

			calculateTotalValues();
		}
	}

	public String calculateTotalValues() {

		totalOrderedQty = 0.0;
		totalDispatchedQty = 0.0;
		totalCurrentDispatchedQty = 0.0;
		totalItemNetTotal = 0.0;
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {

				items.setItemNetTotal(0.0);

				if (items.getCurrentDispatchedQty() != null) {

					Double itemNetTotal = (items.getUnitRate() == null ? 0.0 : items.getUnitRate())
							* (items.getCurrentDispatchedQty() == null ? 0.0 : items.getCurrentDispatchedQty());
					Double itemPercentage = AppUtil.ifNullRetunZero(items.getTaxPercent()) / 100;
					Double itemPercentageValue = itemNetTotal * itemPercentage;

					Double itemTotalWithTax = itemNetTotal + itemPercentageValue;

					log.info("itemTotalWithTax==>" + itemTotalWithTax);

					items.setItemNetTotal(itemTotalWithTax);
					totalItemNetTotal += items.getItemNetTotal() != null ? items.getItemNetTotal() : 0.0;

					Double maxValue = items.getOrderedQty() - items.getDispatchedQty();

					if (items.getCurrentDispatchedQty() < 0) {
						log.info("Current dispatch value is negative");

						Util.addWarn(
								errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_DISPATCH_NEGATIVE.getCode()));
						return null;
					}

					if (items.getCurrentDispatchedQty() > maxValue) {
						log.info("error");
						Util.addWarn(errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_DISPATCH_MORE.getCode())
								+ maxValue);
						return null;
					}
				}

				totalOrderedQty += items.getOrderedQty() != null ? items.getOrderedQty() : 0.0;
				totalDispatchedQty += items.getDispatchedQty() != null ? items.getDispatchedQty() : 0.0;
				totalCurrentDispatchedQty += items.getCurrentDispatchedQty() != null ? items.getCurrentDispatchedQty()
						: 0.0;
			}
		}
		return null;

	}

	public String editStockOutward() {
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (!stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

		selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
		loadTransportByTranportType();

		stockTransfer.setTransportMaster(stockTransfer.getTransportMaster());

		if (stockTransfer != null) {

			if (stockTransfer.getToEntityMaster() != null) {
				EntityMaster dnpOffice = commonDataService
						.findDNPOfficeByProductWarehouse(stockTransfer.getToEntityMaster().getId());
				stockTransfer.setFromEntityMaster(dnpOffice);
			}

			// loginSupplierMaster = (SupplierMaster) loginBean.getUserProfile();

			loginSupplierMaster = getsupplierByuser();

			if (loginSupplierMaster == null) {
				log.info("Login Supplier master is mandatory");
				errorMap.notify(ErrorDescription.STOCK_TRANS_SUPPLIER_REQUIRED.getCode());
				return null;
			} else {
				stockTransfer.setSupplierMaster(loginSupplierMaster);
			}

			stockTransferResponse = new StockTransferResponse();
			addButtonFlag = false;

			yesOrNoList = stockTransferUtility.loadYesNoList();
			payToPayList = stockTransferUtility.loadTransportChargeType();

			transportTypeMasterList = commonDataService.getAllTransportTypes();
			// Getting All D&POffice
			dnpOfficeSet = commonDataService.findEntityList(loginSupplierMaster.getId());
			if (stockTransfer.getFromEntityMaster() != null && loginSupplierMaster != null
					&& stockTransfer.getToEntityMaster() != null) {
				wareHouseSet = commonDataService.findDeliveryEntityList(stockTransfer.getFromEntityMaster().getId(),
						loginSupplierMaster.getId());
				/*
				 * purchaseOrderList =
				 * commonDataService.findPurchaseOrderList(stockTransfer.getToEntityMaster().
				 * getId(), stockTransfer.getFromEntityMaster().getId(),
				 * loginSupplierMaster.getId());
				 */

				purchaseOrderList = new ArrayList<>();

				Set<PurchaseOrder> purchaseOrderSet = new HashSet<>();
				stockTransfer.getStockTransferItemsList().stream().filter(a -> a.getPurchaseOrder() != null)
						.forEach(o -> {
							purchaseOrderSet.add(o.getPurchaseOrder());
						});

				purchaseOrderList.addAll(purchaseOrderSet);
				stockTransfer.setPurchaseOrderList(purchaseOrderList);
				generateData();
				prepareFromDetails();

				prepareToDetails();
				calculateTotalValues();

			}
		}
		return ADD_PAGE_URL;
	}

	public String processDelete() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {
			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
		return null;
	}

	public String removeStockOutward() {
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferResponse.getId();

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				addButtonFlag = false;
				return getStockOutwardPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public String viewStockOutward() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

		if (stockTransfer != null) {

			if (stockTransfer != null && stockTransfer.getToEntityMaster() != null) {
				EntityMaster dnpOffice = commonDataService
						.findDNPOfficeByProductWarehouse(stockTransfer.getToEntityMaster().getId());
				stockTransfer.setFromEntityMaster(dnpOffice);
			}

			stockTransferResponse = new StockTransferResponse();
			if (stockTransfer != null) {

				if (stockTransfer.getToEntityMaster() != null) {
					EntityMaster dnpOffice = commonDataService
							.findDNPOfficeByProductWarehouse(stockTransfer.getToEntityMaster().getId());
					stockTransfer.setFromEntityMaster(dnpOffice);
				}

				// loginSupplierMaster = (SupplierMaster) loginBean.getUserProfile();

				loginSupplierMaster = getsupplierByuser();

				if (loginSupplierMaster == null) {
					log.info("Login Supplier master is mandatory");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SUPPLIER_REQUIRED.getCode());
					return null;
				} else {
					stockTransfer.setSupplierMaster(loginSupplierMaster);
				}

				stockTransferResponse = new StockTransferResponse();
				addButtonFlag = false;

				yesOrNoList = stockTransferUtility.loadYesNoList();
				payToPayList = stockTransferUtility.loadTransportChargeType();

				transportTypeMasterList = commonDataService.getAllTransportTypes();
				// Getting All D&POffice
				dnpOfficeSet = commonDataService.findEntityList(loginSupplierMaster.getId());
				if (stockTransfer.getFromEntityMaster() != null && loginSupplierMaster != null
						&& stockTransfer.getToEntityMaster() != null) {
					wareHouseSet = commonDataService.findDeliveryEntityList(stockTransfer.getFromEntityMaster().getId(),
							loginSupplierMaster.getId());
					/*
					 * purchaseOrderList =
					 * commonDataService.findPurchaseOrderList(stockTransfer.getToEntityMaster().
					 * getId(), stockTransfer.getFromEntityMaster().getId(),
					 * loginSupplierMaster.getId());
					 */

					purchaseOrderList = new ArrayList<>();

					Set<PurchaseOrder> purchaseOrderSet = new HashSet<>();
					stockTransfer.getStockTransferItemsList().stream().filter(a -> a.getPurchaseOrder() != null)
							.forEach(o -> {
								purchaseOrderSet.add(o.getPurchaseOrder());
							});

					purchaseOrderList.addAll(purchaseOrderSet);
					stockTransfer.setPurchaseOrderList(purchaseOrderList);
					generateData();
					prepareFromDetails();

					prepareToDetails();
					calculateTotalValues();

				}
			}
			addButtonFlag = false;
			if (loginBean.getUserDetailSession().getUserType().equals(UserType.SUPPLIER)) {
				loginSupplierMaster = (SupplierMaster) loginBean.getUserProfile();
			}
			if (loginSupplierMaster != null) {
				prepareFromDetails();
			}

			if (stockTransfer.getToEntityMaster() != null) {
				prepareToDetails();
			}

			if (stockTransfer.getTransportMaster() != null) {
				selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
			}

			calculateTotalValues();
		}

		return VIEW_PAGE_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		addButtonFlag = true;
	}

	/**
	 * This method is generating the PDF based on the search result.
	 */

	public void generatePDF() {

		log.info("Generate PDF started....");

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/pdf/download";

		try {

			StockTransferRequest request = getSearchRequestObject(null, null, sortingField, sortingOrder, filtersMap);

			log.info("Generate PDF [" + request + "]");

			InputStream inputStream = httpService.generatePDF(URL, request);

			pdfFile = new DefaultStreamedContent(inputStream, "application/pdf", "StockOutwardList.pdf");

			log.info("Generate PDF finished successfully....");

		} catch (ParseException e) {
			log.error("ParseException ", e);
		}

	}

	public String gotoBundleDetails(StockTransferItems items) {

		log.info("gotoBundleDetails " + items);

		this.stockTransferItems = items;
		bundles = new StockTransferItemsBundles();
		totalBundleQuantity = 0.0;

		if (items.getStockTransferItemsBundlesList() == null) {
			stockTransferItemsBundleListTemp = new ArrayList<>();
		} else {
			stockTransferItemsBundleListTemp = new ArrayList<>(items.getStockTransferItemsBundlesList());
			if (stockTransferItemsBundleListTemp != null && !stockTransferItemsBundleListTemp.isEmpty()) {
				for (StockTransferItemsBundles bundleQuantity : stockTransferItemsBundleListTemp) {
					totalBundleQuantity += bundleQuantity.getQuantity();
				}
			}
		}

		return null;
	}

	public String addBundles(StockTransferItems items) {
		log.info("addBundles");

		Double sumOfQuantity = 0.0;
		totalBundleQuantity = 0.0;

		if (bundles.getQuantity() != null) {
			if (bundles.getQuantity() > 0) {
				sumOfQuantity += bundles.getQuantity();
				totalBundleQuantity = sumOfQuantity;
			} else {
				Util.addWarn(errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_QUANTITY_NEGATIVE.getCode()));
				return null;
			}
		}

		if (stockTransferItemsBundleListTemp != null && !stockTransferItemsBundleListTemp.isEmpty()) {
			for (StockTransferItemsBundles bundleItem : stockTransferItemsBundleListTemp) {
				sumOfQuantity += bundleItem.getQuantity();
				totalBundleQuantity = sumOfQuantity;
			}
		}

		if (items.getCurrentDispatchedQty() != null && items.getCurrentDispatchedQty() >= sumOfQuantity) {
			stockTransferItemsBundleListTemp.add(bundles);
			bundles = new StockTransferItemsBundles();
		} else {
			Util.addWarn(errorMap.getMessage(ErrorDescription.STOCK_TRANS_CURRENT_QUANTITY_EXCEEDS.getCode()));
		}

		return null;
	}

	public void submitBundles(StockTransferItems items) {
		log.info("Submit Bundles");
		items.setStockTransferItemsBundlesList(stockTransferItemsBundleListTemp);
		stockTransferItemsBundleListTemp = null;

		// Updating the total Number of bundles

		Integer totalBundles = 0;
		String bundleNumbers = "";
		totalBundleQuantity = 0.0;
		if (stockTransfer.getStockTransferItemsList() != null) {
			for (StockTransferItems stockTransferItems : stockTransfer.getStockTransferItemsList()) {
				if (stockTransferItems != null && stockTransferItems.getStockTransferItemsBundlesList() != null) {
					totalBundles += stockTransferItems.getStockTransferItemsBundlesList().size();

					for (StockTransferItemsBundles itemsBundles : stockTransferItems
							.getStockTransferItemsBundlesList()) {
						bundleNumbers += itemsBundles.getBundleNumber();
						totalBundleQuantity += itemsBundles.getQuantity();

						if (bundleNumbers != "") {
							bundleNumbers += ",";
						}
					}
				}
			}
		}

		bundleNumbers = bundleNumbers.substring(0, bundleNumbers.length() - 1);

		log.info("Total Bundles " + totalBundles);
		log.info("Bundle Numbers " + bundleNumbers);

		stockTransfer.setTotalBundles(totalBundles);
		stockTransfer.setBundleNumbers(bundleNumbers);

	}

}
