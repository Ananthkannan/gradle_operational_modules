package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.PageURL;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.KntRegistrationUploadData;
import in.gov.cooptex.core.dto.OpeningStockDataPOSDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ValidateOb;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.OpeningStockUploadDataDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("validateOBFileBean")
public class ValidateOBFileBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2319855122600609469L;

	private final String CREATE_VALIDATE_OB_PAGE = "/pages/operation/validateOB.xhtml?faces-redirect=true;";

	private final String LIST_VALIDATE_OB_PAGE = "/pages/operation/yarn/listValidateOB.xhtml?faces-redirect=true;";

	private final String VIEW_VALIDATE_OB_PAGE = "/pages/operation/yarn/viewValidateOB.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String VALIDATEOB_URL = AppUtil.getPortalServerURL() + "/validateOB";

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	ValidateOb validateOb, selectedValidateOb;

	@Getter
	@Setter
	LazyDataModel<ValidateOb> lazyValidateObList;

	List<ValidateOb> glAccountCategoryList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String fileName = "";

	@Getter
	@Setter
	String fileType;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Setter
	@Getter
	private StreamedContent file;

	@Setter
	@Getter
	List<String> errorMessages = new ArrayList<>();

	@Setter
	@Getter
	List<OpeningStockUploadDataDTO> openingStockDataPOSDTOList = new ArrayList<>();

	@Setter
	@Getter
	List<KntRegistrationUploadData> kntRegistrationList = new ArrayList<>();
	
	@Setter
	@Getter
	OpeningStockDataPOSDTO openingStockDataDTO = new OpeningStockDataPOSDTO();
	
	@Setter
	@Getter
	Boolean successResponse=false;
	
	@Autowired
	LoginBean loginBean;

	public String showPageAction() {
		log.info("<===== Starts ValidateObBean.showAccountCategoryListPageAction ===========>" + action);
		try {
			
			if(loginBean.getStockVerificationIsInProgess()) {
				loginBean.setMessage("Validate ob");
				return PageURL.STOCK_VERIFICATION_INPROGRESS_PAGE;
			}
			
			if (!action.equalsIgnoreCase("CREATE") && !action.equalsIgnoreCase("LIST")
					&& (selectedValidateOb == null || selectedValidateOb.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create ValidateOb called..");
				fileType = new String();
				entityMaster = new EntityMaster();
				errorMessages = new ArrayList<>();
				successResponse=false;
				openingStockDataPOSDTOList = new ArrayList<>();
				kntRegistrationList = new ArrayList<>();
				fileName = "";
				validateOb = new ValidateOb();
				return CREATE_VALIDATE_OB_PAGE;
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("<===== Starts ValidateObBean.getById ======>");
				try {
					String url = VALIDATEOB_URL + "/getById/" + selectedValidateOb.getId();
					log.info("===Save Validate Ob Url is=== " + url);
					BaseDTO baseDTO = httpService.get(url);
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						String response = mapper.writeValueAsString(baseDTO.getResponseContent());
						validateOb = mapper.readValue(response, new TypeReference<ValidateOb>() {
						});
						File f = new File(validateOb.getFilePath());
						fileName=f.getName();
						log.info("=== File Name ====  " + fileName);
					}
				} catch (Exception e) {
					log.info("<===== Exception Occured in ValidateObBean.getById ======>");
					log.info("<===== Exception is ======>" + e.getMessage());
				}
				log.info("<===== End ValidateObBean.getById ======>");
				return VIEW_VALIDATE_OB_PAGE;
			} else if (action.equalsIgnoreCase("LIST")) {
				addButtonFlag = true;
				selectedValidateOb = new ValidateOb();
				loadLazyValidateObList();
				return LIST_VALIDATE_OB_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends ValidateObBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}

	public void fileUpload(FileUploadEvent event) {
		log.info("FileMovement:==> fileUpload Method Called---------");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadedFile = event.getFile();
			String type = FilenameUtils.getExtension(uploadedFile.getFileName());
			log.info(" File Type :: " + type);
			long size = uploadedFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			if (size > 5) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_RECOMMANDATION_LETTER_SIZE.getErrorCode());
				return;
			}
			log.info("file size---------------" + size);
			String docPathName = commonDataService.getAppKeyValue("VALIDATE_OB_UPLOAD_PATH");
			String filePath = fileUpload(docPathName, uploadedFile);
			fileName = uploadedFile.getFileName();
			validateOb.setFilePath(filePath);
		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}
		log.info("FileMovement:==> fileUpload Method End---------");
	}

	public static String fileUpload(String path, UploadedFile relievingLetterFile) {
		String photoPath = null;
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			// String candiPhotoPath =
			// getInstance().getPropValues("jobapplication.photo.upload.path");
			long size = relievingLetterFile.getSize();
			log.info("size==>" + size);
			if (size > 0) {
				log.info("path==>" + path);
				photoPath = path + "/VALIDATE_OB_UPLOAD/" + relievingLetterFile.getFileName();
				log.info("photoPath==>" + photoPath);
				Path pathName = Paths.get(path + "/VALIDATE_OB_UPLOAD/");
				File outputFile = new File(photoPath);
				if (Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				inputStream = relievingLetterFile.getInputstream();
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) relievingLetterFile.getSize()];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath==>" + photoPath);
			}
		} catch (Exception e) {
			log.error("fileUpload error==>", e);
		}
		return photoPath;
	}

	public void loadLazyValidateObList() {
		log.info("<===== Starts ValidateObBean.loadLazyValidateObList ======>");
		lazyValidateObList = new LazyDataModel<ValidateOb>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ValidateOb> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = VALIDATEOB_URL + "/getValidateObLazyData";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						glAccountCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ValidateOb>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyValidateObList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return glAccountCategoryList;
			}

			@Override
			public Object getRowKey(ValidateOb res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ValidateOb getRowData(String rowKey) {
				try {
					for (ValidateOb validateOb : glAccountCategoryList) {
						if (validateOb.getId().equals(Long.valueOf(rowKey))) {
							selectedValidateOb = validateOb;
							return validateOb;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ValidateObBean.loadLazyValidateObList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ValidateObBean.onRowSelect ========>" + event);
		selectedValidateOb = ((ValidateOb) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends ValidateObBean.onRowSelect ========>");
	}

	public String validateOB() {
		log.info("<===== Starts ValidateObBean.validateOB ======>");
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			if (validateOb != null && validateOb.getFilePath() == null) {
				fc.addMessage("fileName", new FacesMessage("OB File is Required"));
			}
			if (validateOb != null && validateOb.getFileType() == null) {
				fc.addMessage("status", new FacesMessage("Filetype is Required"));
			}

			if (entityMaster != null && entityMaster.getCode() == null) {
				fc.addMessage("showroomCode", new FacesMessage("ShowroomCode is Required"));
			}

			if (validateOb != null && validateOb.getObDate() == null) {
				fc.addMessage("from", new FacesMessage("OB Date is Required"));
			}
			String ext = FilenameUtils.getExtension(validateOb.getFilePath());
			log.info("=== File Extension is ====   "+ext);
			if(validateOb.getFileType().equals("KNT File") &&  !ext.equals("KNT"))
			{
				openingStockDataPOSDTOList = new ArrayList<>();
				kntRegistrationList = new ArrayList<>();
				Util.addWarn("Please Upload Valid File");
				return null;
			}
			if(validateOb.getFileType().equals("Sales OB File") &&  !ext.equals("IQR"))
			{
				openingStockDataPOSDTOList = new ArrayList<>();
				kntRegistrationList = new ArrayList<>();
				Util.addWarn("Please Upload Valid File");
				return null;
			}
			String url = VALIDATEOB_URL + "/validateObFile";
			log.info("===Validate OB Url is=== " + url);
			validateOb.setEntityMaster(entityMaster);
			validateOb.setFileName(fileName);
			BaseDTO baseDTO = httpService.post(url, validateOb);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMessages = new ArrayList<>();
				successResponse=true;
				String response = mapper.writeValueAsString(baseDTO.getResponseContents());
				if (validateOb.getFileType().equals("KNT File")) {
					kntRegistrationList = mapper.readValue(response, new TypeReference<List<KntRegistrationUploadData>>() {
					});
					openingStockDataPOSDTOList = new ArrayList<>();
				} else {
					kntRegistrationList = new ArrayList<>();
					openingStockDataPOSDTOList = mapper.readValue(response,
						new TypeReference<List<OpeningStockUploadDataDTO>>() {
					});
					
					String responses = mapper.writeValueAsString(baseDTO.getResponseContent());
					openingStockDataDTO = mapper.readValue(responses,OpeningStockDataPOSDTO.class);
				}
				RequestContext.getCurrentInstance().update("successMessages");
			} else {
				openingStockDataPOSDTOList = new ArrayList<>();
				kntRegistrationList = new ArrayList<>();
				if (baseDTO.getStatusCode()
						.equals(ErrorDescription.getError(MastersErrorCode.INVALID_SHOWROOM_CODE).getCode())) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.INVALID_SHOWROOM_CODE).getCode());
					RequestContext.getCurrentInstance().update("growls");
				} else {
						String response = mapper.writeValueAsString(baseDTO.getResponseContents());
						errorMessages = mapper.readValue(response, new TypeReference<List<String>>() {
						});
						RequestContext.getCurrentInstance().update("errorMessages");
				}
			}
		} catch (Exception e) {
			log.info("<===== Exception Occured in ValidateObBean.validateOB ======>",e);
			log.info("<===== Exception is ======>" + e.getMessage());
		}
		log.info("<===== End ValidateObBean.validateOB ======>");
		return null;
	}

	public String saveValidateOb() {
		log.info("<===== Starts ValidateObBean.saveValidateOb ======>");
		try {
			String url = VALIDATEOB_URL + "/saveValidateOb";
			log.info("===Save Validate Ob Url is=== " + url);
			validateOb.setOpeningStockDataDTO(openingStockDataDTO);
			BaseDTO baseDTO = httpService.post(url, validateOb);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.VALIDATE_OB_ADDED_SUCCESSFULLY).getCode());
				RequestContext.getCurrentInstance().update("growls");
				action = "LIST";
				return showPageAction();
			}
		} catch (Exception e) {
			log.info("<===== Exception Occured in ValidateObBean.saveValidateOb ======>");
			log.info("<===== Exception is ======>" + e.getMessage());
		}
		log.info("<===== End ValidateObBean.saveValidateOb ======>");
		return null;
	}

	public String clearAndCancelValidateOb() {
		selectedValidateOb = new ValidateOb();
		action = "LIST";
		return showPageAction();
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			File files = new File(validateOb.getFilePath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

}
