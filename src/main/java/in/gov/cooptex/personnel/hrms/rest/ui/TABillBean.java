package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.TaBill;
import in.gov.cooptex.core.accounts.model.TaBillDetails;
import in.gov.cooptex.core.accounts.model.TaBillLog;
import in.gov.cooptex.core.accounts.model.TaBillNote;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.finance.TaBillDTO;
import in.gov.cooptex.finance.dto.AdditionalBudgetDTO;
import in.gov.cooptex.operation.model.TourProgram;
import in.gov.cooptex.personnel.hrms.TABillClaimTypeEnum;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("taBillBean")
@Scope("session")
@Log4j2
public class TABillBean extends CommonBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 5744200647145888060L;

	private static final String LIST_PAGE = "/pages/personnelHR/listTaBill.xhtml?faces-redirect=true";
	private static final String ADD_PAGE = "/pages/personnelHR/createTaBill.xhtml?faces-redirect=true";
	private static final String VIEW_PAGE = "/pages/personnelHR/viewTaBill.xhtml?faces-redirect=true";

	@Autowired
	AppPreference appPreference;

	private static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;
	
	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<TourProgram> tourProgramList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeList = new ArrayList<>();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean claim = false;

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private TaBillDTO viewResponseDTO = new TaBillDTO();

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	private Boolean approveeditFlag = true;

	@Getter
	@Setter
	int forwardfor;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false, railRender = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	TaBill tabill = new TaBill();

	@Getter
	@Setter
	TaBillDetails taBillDetails = new TaBillDetails();

	@Getter
	@Setter
	List<TaBillDetails> taBillDetailsList = new ArrayList<TaBillDetails>();

	@Getter
	@Setter
	EmployeeMaster empMaster = new EmployeeMaster();

	@Getter
	@Setter
	EmployeePersonalInfoEmployment personalInfoEmployment = new EmployeePersonalInfoEmployment();

	@Getter
	@Setter
	TaBillDTO taBillDTO = new TaBillDTO();

	@Getter
	@Setter
	TaBillLog tabilllog = new TaBillLog();

	@Getter
	@Setter
	Double basicpay, amount, nokm, addrailfare, addamount, nod, allowamount;

	@Getter
	@Setter
	LazyDataModel<TaBillDTO> lazyTravAlloBillList;

	@Getter
	@Setter
	TaBillDTO selectedTaBill = new TaBillDTO();

	@Getter
	@Setter
	TaBillNote tabillnote = new TaBillNote();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<TaBillDTO> taBillList = new ArrayList<TaBillDTO>();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	EmployeeMaster loggedInEmployeeMaster;

	@Getter
	@Setter
	List<Map<String, Object>> taBillNoteEmployeeList = new ArrayList<>();

	@PostConstruct
	public void init() {
		amount = 0D;
		nokm = 0D;
		addrailfare = 0D;
		addamount = 0d;
		nod = 0D;
		allowamount = 0D;
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		loadTourProgramPlanCode();
		tabill = new TaBill();
		taBillDetailsList = new ArrayList<TaBillDetails>();
		taBillDetails = new TaBillDetails();
		forwardToUsersList = new ArrayList<>();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(SERVER_URL, AppFeatureEnum.TA_BILL.name());
		lazyTaBillListResponse();
		showViewListPage();
	}

	public void taBillTypeChange() {
		log.info("<<============== TaBillBean. taBillTypeChange() Starts ================>> ");
		try {
			empMaster = null;
			personalInfoEmployment = null;
			basicpay = 0D;
			if (tabill.getTaBillType().equals(TABillClaimTypeEnum.OTHER_CLAIM.toString())) {
				claim = true;
			} else {
				claim = false;
			}
			if (tabill.getTaBillType().equals(TABillClaimTypeEnum.RETIREMENT.toString())
					|| tabill.getTaBillType().equals(TABillClaimTypeEnum.TRANSFER.toString())) {
				loadEmployeeList();
			}

		} catch (Exception e) {
			log.info("Exception at TaBillBean. taBillTypeChange()", e);
		}
		log.info("<<============== TaBillBean. taBillTypeChange() Ends ================>> ");
	}

	public void loadTourProgramPlanCode() {
		log.info("<<============== TaBillBean. loadTourProgramPlanCode() Starts ================>> ");
		try {
			tourProgramList = new ArrayList<>();
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/getalltourprogramplancode";
			BaseDTO response = httpService.get(URL);
			if (response != null) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				tourProgramList = mapper.readValue(jsonResponse, new TypeReference<List<TourProgram>>() {
				});
				int tourProgramListSize = tourProgramList == null ? 0 : tourProgramList.size();
				log.info("tourProgramListSize - " + tourProgramListSize);
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean. loadTourProgramPlanCode() ", e);
		}
		log.info("<<============== TaBillBean. loadTourProgramPlanCode() Starts ================>> ");
	}

	public void loadEmployeeList() {
		log.info("<<============== TaBillBean. loadEmployeeList() Starts ================>> ");
		try {
			employeeList = new ArrayList<>();
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/getemployeelist/"
					+ tabill.getTaBillType();
			BaseDTO response = httpService.get(URL);
			if (response != null) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				int employeeListSize = employeeList == null ? 0 : employeeList.size();
				log.info("employeeListSize - " + employeeListSize);
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean. loadEmployeeList() ", e);
		}
		log.info("<<============== TaBillBean. loadEmployeeList() Starts ================>> ");
	}

	public void generate() {
		log.info("<<============== TaBillBean. generate() Starts ================>> ");
		Long empId = null;
		try {
			if (action.equals("ADD")) {
				empId = tabill.getTourProgram().getEmployeeId();
			} else {
				empId = tabill.getEmpMaster() == null ? null : tabill.getEmpMaster().getId();
			}
			if (empId != null) {
				empMaster = new EmployeeMaster();
				personalInfoEmployment = new EmployeePersonalInfoEmployment();
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/getempinfo/" + empId;
				BaseDTO response = httpService.get(url);
				if (response != null) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					empMaster = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
					});
					tabill.getTourProgram().setEmployeeId(empMaster == null ? null : empMaster.getId());
					;
					personalInfoEmployment = empMaster.getPersonalInfoEmployment();
					basicpay = response.getSumTotal();
				}
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean. generate() ", e);
		}
		log.info("<<============== TaBillBean. generate() Ends ================>> ");
	}

	public void addTabillDetails() {
		log.info("<<============== TaBillBean. addTabillDetails() Starts ================>> ");
		double total = 0D;
		amount = 0D;
		nokm = 0D;
		addrailfare = 0D;
		addamount = 0d;
		nod = 0D;
		allowamount = 0D;
		try {
			setJourneyFromAndToDate();

			if (taBillDetails.getDateJourneyTo().before(taBillDetails.getDateJourneyFrom())) {
				Util.addWarn("To date should not be allowed for before from date");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			if (taBillDTO.getToTime().before(taBillDTO.getFromTime())) {
				Util.addWarn("To time should not be allowed for before from time");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}

			if (taBillDetails.getAdditionalAmount() != null) {
				taBillDetails.setTotalTravelAmount(taBillDetails.getAdditionalAmount() + taBillDetails.getAmount());
			} else {
				taBillDetails.setTotalTravelAmount(taBillDetails.getAmount());
			}

			taBillDetailsList.add(taBillDetails);
			taBillDetails = new TaBillDetails();
			taBillDTO.setFromTime(null);
			taBillDTO.setToTime(null);
			for (TaBillDetails taBillDetails : taBillDetailsList) {
				taBillDetails.setTotalTravelAmount(0D);
				if (taBillDetails.getAdditionalAmount() != null) {
					total += taBillDetails.getAdditionalAmount() + taBillDetails.getAmount();
				}

				amount += taBillDetails.getAmount();
				nokm += taBillDetails.getNumberOfKilometers();
				if (taBillDetails.getAdditionalRailFare() != null) {
					addrailfare += taBillDetails.getAdditionalRailFare();
					taBillDetails.setTotalTravelAmount(
							taBillDetails.getAdditionalRailFare() + taBillDetails.getTotalTravelAmount());
				}

				if (taBillDetails.getAdditionalAmount() != null) {
					addamount += taBillDetails.getAdditionalAmount();
					taBillDetails.setTotalTravelAmount(
							taBillDetails.getAdditionalAmount() + taBillDetails.getTotalTravelAmount());
				}
				nod += taBillDetails.getNumberOfDays();
				if (taBillDetails.getAllowanceAmount() != null) {
					allowamount += taBillDetails.getAllowanceAmount();
					taBillDetails.setTotalTravelAmount(
							taBillDetails.getAllowanceAmount() + taBillDetails.getTotalTravelAmount());
				}
				total = amount + addrailfare + addamount + allowamount;
			}
			tabill.setClaimTravelAmount(total);
			tabill.setClaimContgAmount(allowamount);
		} catch (Exception e) {
			log.error("Exception at TaBillBean. addTabillDetails() ", e);
		}
		log.info("<<============== TaBillBean. addTabillDetails() Ends ================>> ");
	}

	public void setJourneyFromAndToDate() {
		log.info("<<============== TaBillBean. setJourneyFromAndToDate() Starts ================>> ");
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		if (taBillDTO.getFromTime() != null && taBillDTO.getToTime() != null) {
			String fromTime = sdf.format(taBillDTO.getFromTime().getTime());
			String toTime = sdf.format(taBillDTO.getToTime().getTime());
			int frmm = Integer.parseInt(fromTime.split(":")[0]);
			int frmsc = Integer.parseInt(fromTime.split(":")[1]);
			int tomm = Integer.parseInt(toTime.split(":")[0]);
			int tosc = Integer.parseInt(toTime.split(":")[1]);

			Calendar calfrm = Calendar.getInstance();
			calfrm.setTime(taBillDetails.getDateJourneyFrom());
			calfrm.set(Calendar.MINUTE, frmm);
			calfrm.set(Calendar.SECOND, frmsc);

			Calendar calTo = Calendar.getInstance();
			calTo.setTime(taBillDetails.getDateJourneyTo());
			calTo.set(Calendar.MINUTE, tomm);
			calTo.set(Calendar.SECOND, tosc);

			taBillDetails.setDateJourneyFrom(calfrm.getTime());
			taBillDetails.setDateJourneyTo(calTo.getTime());
		}
		log.info("<<============== TaBillBean. setJourneyFromAndToDate() Ends ================>> ");
	}

	public String submit() {
		log.info("<<============== TaBillBean. submit() Ends ================>> ");
		try {
			if (taBillDetailsList != null && !taBillDetailsList.isEmpty() && taBillDetailsList.size() > 0) {
				taBillDTO = new TaBillDTO();
				tabill.setEmpMaster(empMaster);
				taBillDTO.setTabill(tabill);
				taBillDTO.setTaBillDetailsList(taBillDetailsList);
				if (StringUtils.isEmpty(tabillnote.getNote())) {
					Util.addWarn("Note is empty");
					return null;
				}
				taBillDTO.setTabillnote(tabillnote);
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/create";
				BaseDTO response = httpService.post(url, taBillDTO);
				if (response != null && response.getStatusCode() == 0) {
					if (action.equals("ADD")) {
						errorMap.notify(
								ErrorDescription.getError(PersonnalErrorCode.TA_BILL_SAVE_SUCCESS).getErrorCode());
					} else if (action.equals("EDIT")) {
						errorMap.notify(
								ErrorDescription.getError(PersonnalErrorCode.TA_BILL_UPDATE_SUCCESS).getErrorCode());
					}
					RequestContext.getCurrentInstance().update("growls");
					return showListPage();
				} else {
					errorMap.notify(response.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean. submit() ", e);
		}
		log.info("<<============== TaBillBean. submit() Ends ================>> ");
		return null;
	}

	public void lazyTaBillListResponse() {
		log.info("<<============== TaBillBean. lazyTaBillListResponse() Starts ================>> ");
		lazyTravAlloBillList = new LazyDataModel<TaBillDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TaBillDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						taBillList = mapper.readValue(jsonResponse, new TypeReference<List<TaBillDTO>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in TaBillBean.lazyTaBillListResponse ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return taBillList;
			}

			@Override
			public Object getRowKey(TaBillDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TaBillDTO getRowData(String rowKey) {
				try {
					for (TaBillDTO benefit : taBillList) {
						if (benefit.getId().equals(Long.valueOf(rowKey))) {
							selectedTaBill = benefit;
							return benefit;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyTaBillListResponse. getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<========= TaBillBean.lazyTaBillListResponse() Ends =========>");
	}

	public String showListPage() {
		log.info("<========= TaBillBean.showListPage() Starts =========>");
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		lazyTaBillListResponse();
		selectedTaBill = new TaBillDTO();
		log.info("<========= TaBillBean.showListPage() Ends =========>");
		return LIST_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<========= TaBillBean.onRowSelect() Starts =========>");
		addButtonFlag = true;
		viewButtonFlag = false;
		selectedTaBill = (TaBillDTO) event.getObject();
		if (ApprovalStage.REJECTED.equals(selectedTaBill.getStatus())) {
			editButtonFlag = false;
		} else {
			editButtonFlag = true;
		}
		log.info("<========= TaBillBean.onRowSelect() Ends =========>");
	}

	public void updateTAType() {
		if (taBillDetails.getModeOfTravel().equalsIgnoreCase("Train")) {
			railRender = true;
		} else {
			railRender = false;
		}
	}

	public String addPageAction() {
		log.info("<========= TaBillBean.addPageAction() Starts =========>");
		try {
			if (action.equalsIgnoreCase("ADD")) {
				amount = 0D;
				nokm = 0D;
				addrailfare = 0D;
				addamount = 0d;
				nod = 0D;
				allowamount = 0D;
				tabill = new TaBill();
				tabilllog = new TaBillLog();
				tabillnote = new TaBillNote();
				taBillDetailsList = new ArrayList<TaBillDetails>();
				forwardToUsersList = new ArrayList<>();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(SERVER_URL,
						AppFeatureEnum.TA_BILL.name());
				loadEmployeeLoggedInUserDetails();
				empMaster = null;
				return ADD_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<========= TaBillBean.addPageAction() Ends =========>");
		return null;
	}

	public String clearButton() {
		log.info("<========= TaBillBean.clearButton() Starts =========>");
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		selectedTaBill = new TaBillDTO();
		taBillDTO = new TaBillDTO();
		return LIST_PAGE;
	}

	public String viewtaBill() {
		log.info("<========= TaBillBean.viewtaBill() Starts =========>");

		if (selectedTaBill == null || selectedTaBill.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		getViewtaBill();

		if (selectedTaBill != null) {
			setPreviousApproval(selectedTaBill.getTabillnote().getFinalApproval());
			if ("VIEW".equalsIgnoreCase(action)) {
				if (selectedTaBill.getTabillnote().getForwardTo() != null) {
					if (loginBean.getUserMaster().getId()
							.equals(selectedTaBill.getTabillnote().getForwardTo().getId())) {
						approveeditFlag = false;
						if (AmountMovementType.REJECTED.equals(selectedTaBill.getStatus())) {
							approvalFlag = false;
						} else {
							approvalFlag = true;
						}
					} else {
						approvalFlag = false;
					}
				}
				setFinalApproveFlag(selectedTaBill.getTabillnote().getFinalApproval());
				log.info(AmountMovementType.FINAL_APPROVED + "selectedTaBill.getStatus()" + selectedTaBill.getStatus());
				if (ApprovalStage.FINAL_APPROVED.toString().equalsIgnoreCase(selectedTaBill.getStatus())) {
					buttonFlag = true;
				}
				return VIEW_PAGE;
			}
		}
		return null;
	}

	public void getViewtaBill() {
		log.info("<========= TaBillBean.getViewtaBill() Starts =========>");
		viewResponseDTO = new TaBillDTO();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/getbyid/";
			Long notificationId = selectedTaBill.getNotificationId();
			BaseDTO baseDTO = httpService.post(url, selectedTaBill);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<TaBillDTO>() {
				});
				tabill = viewResponseDTO.getTabill();
				taBillDetailsList = viewResponseDTO.getTaBillDetailsList();
				tabillnote = viewResponseDTO.getTabillnote();
				selectedTaBill.setTabilllog(viewResponseDTO.getTabilllog());
				selectedTaBill.setTabillnote(tabillnote);
				if (notificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}
				generate();
				// View Note Employee Details
				getViewNoteEmployeeDetails();
				Double total = 0D;
				amount = 0D;
				nokm = 0D;
				addrailfare = 0D;
				addamount = 0d;
				nod = 0D;
				allowamount = 0D;
				for (TaBillDetails taBillDetails : taBillDetailsList) {
					total += (taBillDetails.getAdditionalAmount() != null ? taBillDetails.getAdditionalAmount() : 0D)
							+ (taBillDetails.getAmount() != null ? taBillDetails.getAmount() : 0D);
					amount += taBillDetails.getAmount();
					nokm += taBillDetails.getNumberOfKilometers();
					addrailfare += (taBillDetails.getAdditionalRailFare() != null ? taBillDetails.getAdditionalAmount()
							: 0D);
					addamount += (taBillDetails.getAdditionalAmount() != null ? taBillDetails.getAdditionalAmount()
							: 0D);
					nod += taBillDetails.getNumberOfDays();
					allowamount += taBillDetails.getAllowanceAmount();
				}
			}
		} catch (Exception e) {
			log.error("Exception at getViewtaBill() ", e);
		}
		log.info("<========= TaBillBean.getViewtaBill() Ends =========>");
	}

	public String edittaBill() {
		loadEditTABill();
		return ADD_PAGE;
	}

	public void loadEditTABill() {
		log.info("<========= TaBillBean.loadEditTABill() Starts =========>");
		viewResponseDTO = new TaBillDTO();
		try {
			if (selectedTaBill == null || selectedTaBill.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return;
			}
			tabill.setTaBillType(selectedTaBill.getTaBillType());
			taBillTypeChange();
			getViewtaBill();
		} catch (Exception e) {
			log.error("Exception at TaBillBean.loadEditTABill() ", e);
		}
		log.info("<========= TaBillBean.loadEditTABill() Ends =========>");
	}

	public String approveTaBill() {
		log.info("<========= TaBillBean.approveTaBill() Starts =========>");
		
		try {
			if (previousApproval == false && selectedTaBill.getTabillnote().getFinalApproval() == true) {
				selectedTaBill.getTabilllog().setStage(ApprovalStage.APPROVED);
			} else if (previousApproval == true && selectedTaBill.getTabillnote().getFinalApproval() == true) {
				selectedTaBill.getTabilllog().setStage(ApprovalStage.FINAL_APPROVED);
			} else {
				selectedTaBill.getTabilllog().setStage(ApprovalStage.APPROVED);
			}

			selectedTaBill.setTabill(tabill);

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/approvetabill";

			BaseDTO baseDTO = httpService.post(url, selectedTaBill);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TA_BILL_APPROVED.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean.approveTaBill() ", e);
		}
		log.info("<========= TaBillBean.approveTaBill() Ends =========>");
		return null;
	}

	public String rejectTaBill() {
		log.info("<========= TaBillBean.rejectTaBill() Starts =========>");
		try {
			log.info("amount transfer id----------" + selectedTaBill.getId());
			selectedTaBill.getTabilllog().setStage(ApprovalStage.REJECTED);
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/rejecttabill";
			BaseDTO baseDTO = httpService.post(url, selectedTaBill);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TA_BILL_REJECTED.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean.rejectTaBill()", e);
		}
		log.info("<========= TaBillBean.rejectTaBill() Ends =========>");
		return null;
	}

	public void clear() {
		tabill.setTaBillType(null);
		tabill.setTourProgram(null);
		tabill.setClaimName("");
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("<========= TaBillBean.loadEmployeeLoggedInUserDetails() Starts =========>");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + "/employee/findempdetailsbyloggedinuser/" + loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				loggedInEmployeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean.loadEmployeeLoggedInUserDetails()", e);
		}
		log.info("<========= TaBillBean.loadEmployeeLoggedInUserDetails() Ends =========>");
	}

	public void getViewNoteEmployeeDetails() {
		log.info("<========= TaBillBean. getViewNoteEmployeeDetails() Starts =========>");
		Long taBillId = null;
		try {
			taBillId = selectedTaBill == null ? null : selectedTaBill.getId();
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tabill/viewnoteemployeedetails/"
					+ taBillId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				taBillNoteEmployeeList = mapper.readValue(jsonResponse, new TypeReference<List<Map<String, Object>>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception at TaBillBean. getViewNoteEmployeeDetails()", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<========= TaBillBean.getViewNoteEmployeeDetails() Ends =========>");
	}

	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String taBillId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			taBillId = httpRequest.getParameter("taBillId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		selectedTaBill = new TaBillDTO();
		if (!StringUtils.isEmpty(notificationId)) {
			selectedTaBill.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(taBillId)) {
			selectedTaBill.setId(Long.parseLong(taBillId));
			action = "VIEW";
			viewtaBill();
		}
		return VIEW_PAGE;
	}

}