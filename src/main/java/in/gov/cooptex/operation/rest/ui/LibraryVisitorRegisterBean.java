package in.gov.cooptex.operation.rest.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;


import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetMaster;
import in.gov.cooptex.asset.model.AssetRegister;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.operation.library.model.LibraryVisitorRegister;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("libraryVisitorRegisterBean")
@Scope("session")
public class LibraryVisitorRegisterBean {

	private final String LIST_TEXTILE_LIBRARY = "/pages/operation/textileLibrary/listTextileLibraryRegister.xhtml?faces-redirect=true";
	private final String CREATE_LIBRARY_REGISTER = "/pages/operation/textileLibrary/createTextileLibraryRegister.xhtml?faces-redirect=true";
	private final String VIEW_LIBRARY_REGISTER = "/pages/operation/textileLibrary/viewTextileLibraryRegister.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	String url, jsonResponse;

	@Getter @Setter
	private String pageAction;
	
	@Getter @Setter
	EmployeeMaster employeeMaster;
	
	@Getter @Setter
	AssetRegister assetRegister;
	
	@Getter @Setter
	private List<AssetRegister> assertRegisterList;
	
	@Getter @Setter
	private List<EmployeeMaster> employeeNameList;
	
	@Getter @Setter
	LibraryVisitorRegister libraryVisitorRegister = new LibraryVisitorRegister();
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	int totalRecords = 0;
	
	@Getter@Setter
	Boolean addButtonFlag = true;

	@Getter @Setter
	Boolean editButtonFlag = true;

	@Getter @Setter
	Boolean deleteButtonFlag = true;
	
	@Getter @Setter
	LazyDataModel<LibraryVisitorRegister> lazyLibraryVisitorRegisterList;
	
	@Getter @Setter
	int inTime ;
	
	public String showListPage() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		libraryVisitorRegister=new LibraryVisitorRegister();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		loadLibraryRegisterList();
		return LIST_TEXTILE_LIBRARY;
	}
	
	public void loadLibraryRegisterList() {
		lazyLibraryVisitorRegisterList=new LazyDataModel<LibraryVisitorRegister>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 7021954582412933073L;
			public List<LibraryVisitorRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<LibraryVisitorRegister> libraryVisitorRegisterList = null;
				try {
					libraryVisitorRegisterList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url = SERVER_URL + appPreference.getOperationApiUrl() + "/libraryvisitorregister/loadLazyLibraryRegister";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						libraryVisitorRegisterList = mapper.readValue(jsonResponse,
								new TypeReference<List<LibraryVisitorRegister>>() {
								});
						log.info("list----------" + libraryVisitorRegisterList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyPolicyNote List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return libraryVisitorRegisterList;
			}

			@Override
			public Object getRowKey(LibraryVisitorRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public LibraryVisitorRegister getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<LibraryVisitorRegister> responseList = (List<LibraryVisitorRegister>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (LibraryVisitorRegister libraryVisitorRegisterObj : responseList) {
					if (libraryVisitorRegisterObj.getId().longValue() == value.longValue()) {
						libraryVisitorRegister = libraryVisitorRegisterObj;
						return libraryVisitorRegisterObj;
					}
				}
				return null;
			}
		};
	}
	
	public String createLibrary() {
		employeeNameList=commonDataService.getAllEmployee();
		loadAssetList();
		libraryVisitorRegister=new LibraryVisitorRegister();
		employeeMaster=new EmployeeMaster();
		assetRegister=new AssetRegister();
		return CREATE_LIBRARY_REGISTER;
	}
	
	public void loadAssetList() {
		BaseDTO baseDTO=null;
		try {
			baseDTO=new BaseDTO();
			url=SERVER_URL+appPreference.getOperationApiUrl()+"/libraryvisitorregister/getassetbylisttextilelib";
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				assertRegisterList = mapper.readValue(jsonResponse,new TypeReference<List<AssetRegister>>() {
				});
				log.info("assert list size----------"+assertRegisterList.size()); 
			}
		}catch (Exception e) {
			log.info("exception in createLibrary method------",e);
		}
	}
	
	public void employeeChange() {
		libraryVisitorRegister.setMobileNumber(employeeMaster.getContactNumber());
	}
	
	public String librarySave() {
		BaseDTO baseDTO=null;
		try {
			if(saveValidation(true)) {
				baseDTO=new BaseDTO();
				if(employeeMaster.getId()==null) {
					libraryVisitorRegister.setEmpMaster(null);
				}else {
					libraryVisitorRegister.setEmpMaster(employeeMaster);
				}
				if(assetRegister != null && assetRegister.getId()==null) {
					libraryVisitorRegister.setAssetRegister(null);
				}else {
					libraryVisitorRegister.setAssetRegister(assetRegister);
				}
				EntityMaster entityMaster=loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();
				libraryVisitorRegister.setActiveStatus(true);
				libraryVisitorRegister.setEntityMaster(entityMaster);
				url=SERVER_URL+appPreference.getOperationApiUrl()+"/libraryvisitorregister/create";
				baseDTO=httpService.post(url, libraryVisitorRegister);
				if(baseDTO.getStatusCode()==0) {
					if("ADD".equalsIgnoreCase(pageAction)) {
						errorMap.notify(ErrorDescription.LIBRARY_REGISTER_SAVE.getErrorCode());
					}else if("EDIT".equalsIgnoreCase(pageAction)) {
						errorMap.notify(ErrorDescription.LIBRARY_REGISTER_UPDATE.getErrorCode());
					}
					return showListPage();
				}else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					return null;
				}
			}
		}catch (Exception e) {
			log.info("exception in createLibrary method------",e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return null;
	}
	
	public void updateInTime() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(libraryVisitorRegister.getInTime());
		inTime = cal.get(Calendar.HOUR);
	}
	
	private boolean saveValidation(boolean valid) {
		valid=true;
		try {
			if(libraryVisitorRegister.getVisitorType().equalsIgnoreCase("Co-opetex Employee")) {
				Validate.notNull(employeeMaster.getId(), ErrorDescription.LIBRARY_REGISTER_EMPLOYEE);
				Validate.notNull(libraryVisitorRegister.getLendingAllowed(), ErrorDescription.LIBRARY_REGISTER_BOOKLEND);
				if(libraryVisitorRegister.getLendingAllowed()) {
					Validate.dateNotNull(libraryVisitorRegister.getReturnDueDate(),ErrorDescription.LIBRARY_REGISTER_DUDDATE);
					Validate.dateNotNull(libraryVisitorRegister.getActualDateReturned(),ErrorDescription.LIBRARY_REGISTER_RETURNDATE);
				}else {
					log.info("libraryVisitorRegister LendingAllowed"+libraryVisitorRegister.getLendingAllowed());
				}
				
			}
			else if(libraryVisitorRegister.getVisitorType().equalsIgnoreCase("General Public")) {
				Validate.notNullOrEmpty(libraryVisitorRegister.getName(),ErrorDescription.LIBRARY_REGISTER_NAME);
				Validate.notNullOrEmpty(libraryVisitorRegister.getMobileNumber(), ErrorDescription.LIBRARY_REGISTER_CONDUCTNO);
			}
			Validate.notNullOrEmpty(libraryVisitorRegister.getVisitorType(), ErrorDescription.LIBRARY_REGISTER_VISITER_TYPE);
			Validate.notNullOrEmpty(libraryVisitorRegister.getPurposeOfVisit(), ErrorDescription.LIBRARY_REGISTER_PURPOSEOFVISIT);
			Validate.dateNotNull(libraryVisitorRegister.getDateOfVisit(), ErrorDescription.LIBRARY_REGISTER_DATE);
			Validate.dateNotNull(libraryVisitorRegister.getInTime(),ErrorDescription.LIBRARY_REGISTER_INTIME);
			Validate.dateNotNull(libraryVisitorRegister.getOutTime(),ErrorDescription.LIBRARY_REGISTER_OUTTIME);
			
			Validate.phoneValidation(libraryVisitorRegister.getMobileNumber(),ErrorDescription.getError(OperationErrorCode.PHONE_VALIDATION));
			
			
			
			Date inTime = libraryVisitorRegister.getInTime();
			Date outTime = libraryVisitorRegister.getOutTime();
			if(inTime.after(outTime)) {
				log.info("In time should less than Out time");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.IN_TIME_OUT_TIME_VALIDATEION).getCode());
				valid=false;
			}
			
			/*String phoneNo = libraryVisitorRegister.getMobileNumber();
			Boolean phoneValidate = Validate.phoneValidation(phoneNo);
			log.info("phoneValidate==> "+phoneValidate);
			if(phoneValidate == false) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PHONE_VALIDATION).getCode());
				return false;
			}*/
			
			
		}catch (RestException e) {
			valid=false;
			errorMap.notify(e.getStatusCode());
		}
		return valid;
	}

	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		libraryVisitorRegister = null;
		loadLibraryRegisterList();
		return LIST_TEXTILE_LIBRARY;
	}
	
	public void onRowSelect(SelectEvent event) {
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		libraryVisitorRegister = (LibraryVisitorRegister) event.getObject();
	}
	
	public String viewLibraryRegister() {
		BaseDTO baseDTO=null;
		try {
			baseDTO=new BaseDTO();
			url=SERVER_URL+appPreference.getOperationApiUrl()+"/libraryvisitorregister/get/"+libraryVisitorRegister.getId();
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				libraryVisitorRegister = mapper.readValue(jsonResponse,new TypeReference<LibraryVisitorRegister>() {
				});
				if(libraryVisitorRegister.getEmpMaster()!=null) {
					setEmployeeMaster(libraryVisitorRegister.getEmpMaster());
				}else {
					employeeMaster=new EmployeeMaster();
				}
				if(libraryVisitorRegister.getAssetRegister()!=null) {
					setAssetRegister(libraryVisitorRegister.getAssetRegister());
				}else{
					assetRegister=new AssetRegister();
				}
				if("VIEW".equalsIgnoreCase(pageAction)) {
					return VIEW_LIBRARY_REGISTER;
				}else if("EDIT".equalsIgnoreCase(pageAction)) {
					return CREATE_LIBRARY_REGISTER;
				}
			}
		}catch (Exception e) {
			log.info("exception in viewLibraryRegister method------",e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return null;
	}
	
	public void libraryDelete() {
		BaseDTO baseDTO=new BaseDTO();
		try {
			url=SERVER_URL+appPreference.getOperationApiUrl()+"/libraryvisitorregister/deleteById/"+libraryVisitorRegister.getId();
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				errorMap.notify(ErrorDescription.LIBRARY_REGISTER_DELETE.getErrorCode());
				editButtonFlag = true;
				deleteButtonFlag = true;
				addButtonFlag = false;
				loadLibraryRegisterList();
			}else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
		}catch (Exception e) {
			log.info("exception in libraryDelete method------",e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}
}


