/**
 * 
 */
package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.ExhibitionAddress;
import in.gov.cooptex.operation.model.ExhibitionMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Praveen
 *
 */
@Log4j2
@Scope("session")
@Service("exhibitionMasterBean")
public class ExhibitionMasterBean {

	private final String CREATE_EXHIBITION_MASTER_PAGE = "/pages/masters/exhibition/createExhibition.xhtml?faces-redirect=true;";
	private final String LIST_EXHIBITION_MASTER_PAGE = "/pages/masters/exhibition/listExhibition.xhtml?faces-redirect=true;";
	private final String VIEW_EXHIBITION_MASTER_PAGE = "/pages/masters/exhibition/viewExhibition.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	ExhibitionAddress exhibitionAddress;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	ExhibitionMaster exhibitionMaster, selectedExhibitionMaster;

	@Getter
	@Setter
	EntityMaster exhibitionEntity;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	ExhibitionAddressBean exhibitionAddressBean;

	@Getter
	@Setter
	List<EntityMaster> entityMasterAllList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<ExhibitionMaster> exhibitionMasterList = new ArrayList<ExhibitionMaster>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean statusButtonFlag = false;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<ExhibitionMaster> activeExhibitionMasterList = null;

	@Getter
	@Setter
	ExhibitionMaster selectedExhibitionMasterList;

	@Getter
	@Setter
	String address = "";

	@Getter
	@Setter
	private int exhibitionMasterListSize;

	@Getter
	@Setter
	private List<StateMaster> selectedStateMasterList;

	@Getter
	@Setter
	LazyDataModel<ExhibitionMaster> lazyExhibitionMasterList;

	@Getter
	@Setter
	Boolean entitytypeflag = true;
	@Getter
	@Setter
	Boolean entityflag = true;
	@Getter
	@Setter
	Boolean exhibitionInchargeflag = true;

	@Getter
	@Setter
	EntityMaster regionHeadOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList = new ArrayList<>();

	@Getter
	@Setter
	EntityTypeMaster entityType;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<>();

	/*** show exhibition master here ***/
	public void createExhibition() {
		log.info("<---- Start ExhibitionMasterBean.createExhibition ---->");
		try {
			exhibitionMaster = new ExhibitionMaster();
			regionHeadOffice = new EntityMaster();
			entityType = new EntityTypeMaster();

			setEntityMasterAllList(null);
			entitytypeflag = true;
			entityflag = true;
			/* getEntityMasterList(); */
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			entityTypeList = commonDataService.loadEntityTypes();

			log.info("::::::::::headRegionOfficeList:::::::::::::::" + headRegionOfficeList.size());

		} catch (Exception e) {
			log.error("<<<<======Error occured while Exhibition add page===>>>>>>", e);
		}
	}

	/*** Exhibition master all list here ***/
	public String showAllExhibitionMasterList() {
		log.info(" inside the showAllExhibitionMasterList");
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		try {
			exhibitionMaster = new ExhibitionMaster();
			selectedExhibitionMaster = new ExhibitionMaster();
			// getAllExhibitionMasterList();
			loadLazyTapalSenderTypeList();
		} catch (Exception e) {
			log.error("Exception at showAllExhibitionMasterList  " + e);
		}

		return LIST_EXHIBITION_MASTER_PAGE;

	}

	/*** Exhibition master all list action Here ***/
	public String exhibitionMasterListPageAction() {
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("called add exhibition master page");
				exhibitionMaster = new ExhibitionMaster();
				selectedExhibitionMaster = new ExhibitionMaster();
				address = "";
				createExhibition();
				return CREATE_EXHIBITION_MASTER_PAGE;

			} else if (action.equalsIgnoreCase("View")) {
				log.info("called view exhibition master page");
				exhibitionMasterValuebyId(selectedExhibitionMaster.getId());
				return VIEW_EXHIBITION_MASTER_PAGE;

			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("called Edit exhibition master page");
				exhibitionMasterValuebyId(selectedExhibitionMaster.getId());
				log.info("region---->" + regionHeadOffice);
				entityType = exhibitionMaster.getExhibitionEntity().getEntityTypeMaster();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				entityTypeList = commonDataService.loadEntityTypes();
				updateEntityList();
				onEntityTypeChange();
				onEntityChange();
				return CREATE_EXHIBITION_MASTER_PAGE;

			} else if (action.equalsIgnoreCase("Delete")) {
				log.info("called Delete exhibition master page ==>>> Exhibition ID ==>> "
						+ selectedExhibitionMaster.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmExhibitionMasterDelete').show();");
			} else if (action.equalsIgnoreCase("Clear")) {
				log.info("called Clear exhibition master page");
				showAllExhibitionMasterList();
				return LIST_EXHIBITION_MASTER_PAGE;
			}
		} catch (Exception e) {
			log.error(" Exception at exhibitionMasterListPageAction " + e);
		}

		return null;
	}

	/*** GET All Exhibition Master List Here ***/
	private void getAllExhibitionMasterList() {
		log.info("<<<< ----------Start ExhibitionMaster-getAllExhibitionMasterList() ------- >>>>");
		try {
			exhibitionMasterList = new ArrayList<ExhibitionMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/exhibition/getall";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				exhibitionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ExhibitionMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (exhibitionMasterList != null) {
			exhibitionMasterListSize = exhibitionMasterList.size();
		} else {
			exhibitionMasterListSize = 0;
		}
		log.info("<<<< ----------End ExhibitionMaster . getAllExhibitionMasterList ------- >>>>");

	}

	/*** Get the entity master list here ***/
	public void getEntityMasterList() {
		try {
			entityMasterAllList = new ArrayList<EntityMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/exhibition/getAllEntityList";
			log.info(" exhibition master ==>>> getEntityMasterList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterAllList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End EntityMaster . getEntityMasterList ------- >>>>");
	}

	/**
	 * Get active employee master list here
	 */
	public void getAllEmployeeMasterList() {

		employeeMasterList = new ArrayList<EmployeeMaster>();
		log.info("<===  getAllEmployeeMasterList Called====>");
		try {
			// employeeMasterList = commonDataService.getAllEmployee();
			employeeMasterList = commonDataService.getActiveEmployeeNameOrderList();
			log.info("<===  getAllEmployeeMasterList END ====>");
		} catch (Exception e) {
			log.error(" Exception occured at get employee master list" + e);
		}
	}

	/*** Exhibition Master Grid OnRow Select Here ***/
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ExhibitionMaster.onRowSelect ========>" + event);
		selectedExhibitionMaster = ((ExhibitionMaster) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends ExhibitionMaster.onRowSelect ========>");
	}

	/*** Save and update exhibition master here ***/
	public String createExhibitionMaster() {
		log.info("<==== Starts .createExhibitionMaster =======>");
		try {
			if (!exhibitionMaster.getContactNumber().equals("")) {
				if (exhibitionMaster.getContactNumber().length() > 10
						|| exhibitionMaster.getContactNumber().length() < 8) {
					errorMap.notify(ErrorDescription.EXHIBITION_CONTACT_NUMBER_VALIDATION.getCode());
					return null;
				}
			}
			if (exhibitionMaster.getExhibitionAddress() == null) {
				errorMap.notify(ErrorDescription.EXHIBITION_ADDRESS_EMPTY.getCode());
				// Util.addWarn("Ex");
				return null;
			}

			if (exhibitionMaster.getEmailId().equals("")) {
				AppUtil.addWarn("Please enter email id");
				// Util.addWarn("Ex");
				return null;
			}

			if (!EmployeeValidator.validateEmail(exhibitionMaster.getEmailId())) {
				log.error("  Invalid Email Id ");
				AppUtil.addWarn("Please enter valid email pattern");
				// errorMap.notify(ErrorDescription.INAVALID_SUPPORT_EMAIL.getErrorCode());
				return null;
			}
			if (exhibitionMaster.getContactNumber().startsWith("0")
					|| exhibitionMaster.getContactNumber().startsWith("1")
					|| exhibitionMaster.getContactNumber().startsWith("2")
					|| exhibitionMaster.getContactNumber().startsWith("3")
					|| exhibitionMaster.getContactNumber().startsWith("4")
					|| exhibitionMaster.getContactNumber().startsWith("5")) {
				AppUtil.addWarn("Please Give Valid Contact Number");
				return null;
			}

			if (exhibitionMaster.getLexhibitionName() == null || exhibitionMaster.getLexhibitionName().length() == 0) {
				AppUtil.addError("Name (in Tamil) is required");
				return null;
			}

			String url = SERVER_URL + "/exhibition/saveOrUpdateExhibitionMaster";
			log.info(" Exhibition master ====> createExhibitionMaster ==>> URL " + url);
			BaseDTO response = httpService.post(url, exhibitionMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("Exhibition Master saved successfully..........");
				log.info(" createExhibitionMaster ::::: ACTION URL ===>>>  " + action);
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.EXHIBITION_INSERTED_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.EXHIBITION_UPDATE_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends ExhibitionMaster.createExhibitionMaster  =======>");

		return showAllExhibitionMasterList();
	}

	/*** Edit and View Exhibition master here ***/
	public void exhibitionMasterValuebyId(Long id) {
		try {
			log.info("called exhibitionMasterValuebyId Start..");
			String url = SERVER_URL + "/exhibition/getExhibitionById/" + id;
			log.info("exhibitionMasterValuebyId URL ====>>>  " + url);
			BaseDTO baseDTO = httpService.get(url);
			selectedExhibitionMaster = new ExhibitionMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				exhibitionMaster = mapper.readValue(jsonResponse, new TypeReference<ExhibitionMaster>() {
				});
				String json = mapper.writeValueAsString(baseDTO.getResponseContents());
				List<EntityMaster> entityMasterList = mapper.readValue(json, new TypeReference<List<EntityMaster>>() {
				});
				log.info("=============selectedExhibitionMaster====================="
						+ selectedExhibitionMaster.getExhibitionName());
				if (entityMasterList != null && !entityMasterList.isEmpty()) {
					regionHeadOffice = entityMasterList.get(0);
				}

				log.info(" Exhibition master ===>>> exhibition address ===>>> "
						+ selectedExhibitionMaster.getExhibitionAddress());
				address = exhibitionAddressBean.prepareExhibitionAddress(exhibitionMaster.getExhibitionAddress());
				// log.info(" Exhibition Address ===>>> ", address);
				if (action.equalsIgnoreCase("Edit")) {
					exhibitionAddressBean.setExhibitionAddress(exhibitionMaster.getExhibitionAddress());
				}
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	/*** Delete Exhibition Master Confirm ***/
	public String deleteConfirmExhibitionMaster() {
		log.info("<===== Starts ExhibitionMaster.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/exhibition/deletebyid/" + selectedExhibitionMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EXHIBITION_DELETE_SUCCESSFULLY.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmExhibitionMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete exhibition master ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ExhibitionMasterBean.deleteConfirm ===========>");
		return showAllExhibitionMasterList();
	}

	public void saveExbitionAddress() {
		log.info(" Inside saveExbitionAddress " + exhibitionAddressBean.getExhibitionAddress());
		// String addressArray[] =
		// AppUtil.prepareAddress(addressMasterBean.getAddressMaster());
		exhibitionMaster.setExhibitionAddress(exhibitionAddressBean.getExhibitionAddress());
		// adressMaster = circleMaster.getAddressMaster();
		address = exhibitionAddressBean.prepareExhibitionAddress(exhibitionAddressBean.getExhibitionAddress());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("createExhibitionMasterForm");
	}

	public void loadLazyTapalSenderTypeList() {
		log.info("<===== Starts ExhibitionMasterBean.loadLazyExhibitionMasterList ======>");
		lazyExhibitionMasterList = new LazyDataModel<ExhibitionMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ExhibitionMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/exhibition/search";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						exhibitionMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ExhibitionMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyExhibitionMasterList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return exhibitionMasterList;
			}

			@Override
			public Object getRowKey(ExhibitionMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ExhibitionMaster getRowData(String rowKey) {
				try {
					for (ExhibitionMaster glAccountCategory : exhibitionMasterList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedExhibitionMaster = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ExhibitionMasterBean.loadLazyExhibitionMasterList ======>");
	}

	public void fromDateChange() {
		log.info(":::::::::::Inside::::::toDateChange:::::::::" + exhibitionMaster.getFromDate());
		try {
			if (exhibitionMaster.getToDate() != null) {
				if (exhibitionMaster.getFromDate().compareTo(exhibitionMaster.getToDate()) < 0) {
					log.info("inside condition from Date is Less then To Date");
				} else {
					log.info("inside condition from Date is Greater then To Date");
					exhibitionMaster.setToDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in getRowData ...", e);
		}
	}

	public void regDateChange() {
		log.info(":::::::::::Inside::::::toDateChange:::::::::" + exhibitionMaster.getFromDate());
		try {
			if (exhibitionMaster.getRegistrationDate() != null) {
				if (exhibitionMaster.getFromDate().compareTo(exhibitionMaster.getRegistrationDate()) > 0) {
					log.info("inside condition from Date is Less then getRegistrationDate");
				} else {
					log.info("inside condition from Date is Greater then To Date");
					exhibitionMaster.setFromDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in getRowData ...", e);
		}
	}

	public void updateEntityList() {
		log.info("<=Starts ExhibitionMasterBean.updateEntityList =>");
		try {
			employeeMasterList = new ArrayList<>();
			if (regionHeadOffice != null && regionHeadOffice.getEntityTypeMaster() != null
					&& regionHeadOffice.getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE)) {
				entitytypeflag = true;
				entityflag = true;
				setEntityType(null);
				setExhibitionEntity(null);
				employeeMasterList = commonDataService.getActiveEmployeeMasterByEntityId(regionHeadOffice.getId());
				log.info(":::::updateEntityList:::::employeeMasterList::::::::::::::" + employeeMasterList.size());
			} else {
				entitytypeflag = false;
				entityflag = false;
			}
			if (regionHeadOffice != null && getEntityType() != null
					&& !regionHeadOffice.getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE)) {
				entityMasterAllList = commonDataService.loadEntityByregionOrentityTypeId(regionHeadOffice.getId(),
						getEntityType().getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends ExhibitionMasterBean.updateEntityList =>");
	}

	public void onEntityTypeChange() {
		log.info("::::::::onEntityTypeChange method Starts::::::::::::::");
		try {
			employeeMasterList = new ArrayList<>();
			entityMasterAllList = new ArrayList<>();
			if (regionHeadOffice != null && entityType != null) {
				entityMasterAllList = commonDataService.loadEntityByregionOrentityTypeId(getRegionHeadOffice().getId(),
						getEntityType().getId());
				log.info("entityMasterAllList size--------------->" + entityMasterAllList.size());
			}
		} catch (Exception e) {
			log.error("::::Exception in onEntityTypeChange:::::", e);
		}
		log.info("::::::::onEntityTypeChange method Ends::::::::::::::");
	}

	public void onEntityChange() {
		int employeeMasterListSize = 0;
		log.info("::::::::onEntityChange method Starts::::::::::::::" + exhibitionMaster.getExhibitionEntity().getId());
		try {
			employeeMasterList = new ArrayList<>();
			if (exhibitionMaster.getExhibitionEntity() != null) {
				employeeMasterList = commonDataService
						.getActiveEmployeeMasterByEntityId(exhibitionMaster.getExhibitionEntity().getId());
				employeeMasterListSize = employeeMasterList != null ? employeeMasterList.size() : 0;
				log.info("onEntityChange - employeeMasterList- Size: " + employeeMasterListSize);
			}
		} catch (Exception e) {
			log.error("::::Exception in onEntityChange:::::", e);
		}
		log.info("::::::::onEntityChange method Ends::::::::::::::");
	}

}
