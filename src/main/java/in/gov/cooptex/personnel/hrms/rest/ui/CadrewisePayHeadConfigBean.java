package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CadreMaster;
import in.gov.cooptex.core.model.PayHeadCadreConfig;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.PayAspect;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("cradeWisePayHeadConfigBean")
public class CadrewisePayHeadConfigBean {

	private final String CRADE_WISE_PAY_CONFIG_CREATE_PAGE_URL = "/pages/personnelHR/payRoll/createCadrewisePayHeadConfig.xhtml?faces-redirect=true";
	private final String CRADE_WISE_PAY_CONFIG_VIEW_PAGE_URL = "/pages/personnelHR/payRoll/viewCadrewisePayHeadConfig.xhtml?faces-redirect=true";
	private final String CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL = "/pages/personnelHR/payRoll/listCadrewisePayHeadConfig.xhtml?faces-redirect=true";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	ObjectMapper mapper;

	@Getter
	@Setter
	List<PayHeadCadreConfig> payHeadCadreConfigList = new ArrayList<PayHeadCadreConfig>();

	@Getter
	@Setter
	PayHeadCadreConfig payHeadCadreConfig;

	@Getter
	@Setter
	CadreMaster cadreMaster = new CadreMaster();

	@Setter
	@Getter
	List<PayHeadCadreConfig> cadreList = new ArrayList<>();

	@Getter
	@Setter
	GlAccount percentageHead = new GlAccount();

	@Getter
	@Setter
	GlAccount glAccount = new GlAccount();

	@Setter
	@Getter
	List<GlAccount> glAccountList = new ArrayList<GlAccount>();

	@Setter
	@Getter
	List<PayHeadCadreConfig> payHeadList = new ArrayList<PayHeadCadreConfig>();

	@Getter
	@Setter
	PayHeadCadreConfig selectedCadrewisePayHeadConfig;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	private String fileNames;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	LazyDataModel<PayHeadCadreConfig> payHeadCadreConfigLazyList;

	@Getter
	@Setter
	List<CadreMaster> cadreMasterList = new ArrayList<>();

	@Autowired
	CommonDataService commonDataService;

	String jsonResponse;

	@Getter
	@Setter
	boolean disableaddButton = false;

	@Getter
	@Setter
	List<Object> payAspect;

	@Getter
	@Setter
	List<Integer> numberOrderList = new ArrayList<>();

	@Getter
	@Setter
	Boolean percentageFlag = true;
	
	@Getter
	@Setter
	boolean addButtonFlag = false;

	@PostConstruct
	public void init() {
		loadCadreWiseInfo();

	}

	public void loadCadreWiseInfo() {
		payHeadCadreConfig = new PayHeadCadreConfig();
		payHeadCadreConfigList = new ArrayList<PayHeadCadreConfig>();
		loadValues();
		getAllGLAccountType();
		cadreMasterList = commonDataService.loadCadreMaster();
		// getAllCadreType();
		getAllPayHeadType();
	}

	public String cadrewisePayHeadConfig() {
		action="LIST";
		loadCadreWiseInfo();
		return CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL;

	}

	public String createCadrewisePayHeadConfig() {
		action = "ADD";
		loadCadreWiseInfo();
		loadDisplayOrderNumber();
		return CRADE_WISE_PAY_CONFIG_CREATE_PAGE_URL;
	}

	public void loadDisplayOrderNumber() {
		log.info("<=CadrewisePayHeadConfigBean :: loadDisplayOrderNumber=>");
		for (int order = 1; order <= 20; order++) {
			log.info("loadDisplayOrderNumber :: order==>" + order);
			numberOrderList.add(order);
		}
	}
	
	
	public String checkPercentageVal() {				
		log.info("<=CadrewisePayHeadConfigBean :: checkPercentageVal=>");
		 
			if (payHeadCadreConfig.getPercentage() >= 1000) {
               errorMap.notify(ErrorDescription.getError(OperationErrorCode.PERCENTAGE_SHOULD_LESS_THAN_THOUSAND)
				.getErrorCode());
               return null;		 
			}
			return null; 
	}
	
	 

	public void updatePercentageFlag() {
		log.info("<=CadrewisePayHeadConfigBean :: updatePercentageFlag=>");
		try {
			if (payHeadCadreConfig.getPercentageHead() == null) {
				percentageFlag = true;
			} else {
				percentageFlag = false;
			}
		} catch (Exception e) {
			log.error("Exception in updatePercentageFlag  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	private void loadValues() {
		try {
			cadrewisePayHeadConfigListPage();
			lPayAspect();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + SERVER_URL);
	}

	private void lPayAspect() {
		Object[] statusAspect = PayAspect.class.getEnumConstants();
		log.info("----> statusAspect" + statusAspect);
		payAspect = new ArrayList<Object>();
		for (Object payaspect : statusAspect) {
			payAspect.add(payaspect);
		}
	}

	public void getAllGLAccountType() {
		glAccountList = new ArrayList<GlAccount>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/cradeWisePayHeadConfigBean/getAllGLAccountType";
			log.info(" getAllGLAccountType ==>>> getAllGLAccountType URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				glAccountList = mapper.readValue(jsonResponse, new TypeReference<List<GlAccount>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End getAllGLAccountType ------- >>>>");
	}

	public void getAllPayHeadType() {
		payHeadList = new ArrayList<PayHeadCadreConfig>();
		List<PayHeadCadreConfig> headList = new ArrayList<PayHeadCadreConfig>();
		List<GlAccount> glAccountList=new ArrayList<>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/cradeWisePayHeadConfigBean/getAllPayHeadType";
			log.info(" getAllPayHeadType ==>>> getAllPayHeadType URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				headList = mapper.readValue(jsonResponse, new TypeReference<List<PayHeadCadreConfig>>() {
				});
				
				if(headList!=null && headList.size()>0) {
					for(PayHeadCadreConfig list:headList) {
						if(!glAccountList.contains(list.getHead())) {
							payHeadList.add(list);
						}
						glAccountList.add(list.getHead());
					}
					log.info("payHeadList size..."+payHeadList.size());
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End getAllGLAccountType ------- >>>>");
	}

	public void getAllCadreType() {
		cadreList = new ArrayList<PayHeadCadreConfig>();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/cradeWisePayHeadConfigBean/getAllCadreType";
			log.info(" cadre ==>>> getAllCadreType URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				cadreList = mapper.readValue(jsonResponse, new TypeReference<List<PayHeadCadreConfig>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End cadreList ------- >>>>");
	}

	public String submit() {
		log.info("...... cradeWisePayHeadConfigBean submit begin ....");
		try {
			if (payHeadCadreConfig.getCadre() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CADRE_REQUIRED).getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getHead() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HEAD_REQUIRED).getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getPercentage() != null && payHeadCadreConfig.getPercentage() >= 1000) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PERCENTAGE_SHOULD_LESS_THAN_THOUSAND)
						.getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getPayAspect() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PAY_ASPECT_IS_REQUIRED).getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getIsProbationPresent() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PROBATION_RERIOD_REQUIRED).getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getApplicableStatus() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_THE_APPLICABLE).getErrorCode());
				return null;
			}

			if (payHeadCadreConfig.getDisplayOrder() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_THE_DISPLAY_ORDER).getErrorCode());
				return null;
			}

			BaseDTO baseDTO = null;
			baseDTO = httpService.post(
					SERVER_URL + appPreference.getOperationApiUrl() + "/cradeWisePayHeadConfigBean/create",
					payHeadCadreConfig);
			log.info("payHeadCadreConfig" + payHeadCadreConfig);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info(" saved successfully");
					loadCadreWiseInfo();
					percentageFlag = true;
					errorMap.notify(223345057);
					return CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL;
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating cradeWisePayHeadConfig" + e);
		}
		log.info("...... cradeWisePayHeadConfig submit ended ....");

		return null;

	}

	public String clearPage() {
		log.info("============== clearPage =============");
		payHeadCadreConfig = new PayHeadCadreConfig();
		percentageFlag = true;
		addButtonFlag = false;
		loadLazyList();
		selectedCadrewisePayHeadConfig = new PayHeadCadreConfig();
		return CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL;
	}

	public String cadrewisePayHeadConfigListPage() {
		log.info("============== CadrewisePayHeadConfigListPage =============");
		try {
			payHeadCadreConfig = new PayHeadCadreConfig();
			glAccountList = new ArrayList<GlAccount>();
			cadreList = new ArrayList<PayHeadCadreConfig>();
			payHeadList = new ArrayList<PayHeadCadreConfig>();
			selectedCadrewisePayHeadConfig = new PayHeadCadreConfig();
			loadLazyList();

		} catch (Exception e) {
			log.error("============== Exception CadrewisePayHeadConfigListPage()================" + e);
		}
		return CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL;
	}

	public void loadLazyList() {

		log.info("<===== Starts lazy List ======>");
		String CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/cradeWisePayHeadConfigBean/getAllCadre";

		payHeadCadreConfigLazyList = new LazyDataModel<PayHeadCadreConfig>() {

			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<PayHeadCadreConfig> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					BaseDTO response = httpService.post(CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL, paginationRequest);
					log.info("==========URL  List=============>" + CRADE_WISE_PAY_CONFIG_LIST_PAGE_URL);
					log.info("Pagination request :::" + paginationRequest);

					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						payHeadCadreConfigList = mapper.readValue(jsonResponse,
								new TypeReference<List<PayHeadCadreConfig>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazy List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return payHeadCadreConfigList;
			}

			@Override
			public Object getRowKey(PayHeadCadreConfig res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PayHeadCadreConfig getRowData(String rowKey) {
				try {
					for (PayHeadCadreConfig payHeadCadreConfigObj : payHeadCadreConfigList) {
						if (payHeadCadreConfigObj.getId().equals(Long.valueOf(rowKey))) {
							payHeadCadreConfig = payHeadCadreConfigObj;
							return payHeadCadreConfigObj;
						}
					}
				}

				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};
		log.info("<===== Ends GradewiseCityConfigBean.loadLazyGradewiseCityConfigList ======>");
	}

	public String viewCadrewisePayHeadConfig() {
		log.info("<<<< ----------Start viewCadrewisePayHeadConfigList() ------- >>>>");

		try {
			if (selectedCadrewisePayHeadConfig == null) {
				errorMap.notify(ErrorDescription.SELECT_ONE.getErrorCode());
				Util.addError("Please select the Status");
				Util.addWarn("Please select one field");
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/cradeWisePayHeadConfigBean/editCadreById/"
					+ selectedCadrewisePayHeadConfig.getId();
			log.info(" selectedCadrewisePayHeadConfig ===>> view...URL.. " + url);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				payHeadCadreConfig = mapper.readValue(jsonResponse, new TypeReference<PayHeadCadreConfig>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		return CRADE_WISE_PAY_CONFIG_VIEW_PAGE_URL;
	}

	public String editCadrewisePayHeadConfig() {

		log.info("<<<<< -------Start CadrewisePayHeadConfig EDIT called ------->>>>>> ");

		try {
			action = "EDIT";
			if (selectedCadrewisePayHeadConfig == null) {
				errorMap.notify(ErrorDescription.SELECT_ONE.getErrorCode());
				Util.addWarn("Please select one field");
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/cradeWisePayHeadConfigBean/editCadreById/"
					+ selectedCadrewisePayHeadConfig.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				payHeadCadreConfig = mapper.readValue(jsonResponse, new TypeReference<PayHeadCadreConfig>() {
				});
				log.info("<<<<< -------End  EDIT  ------->>>>>> ");
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return CRADE_WISE_PAY_CONFIG_CREATE_PAGE_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		selectedCadrewisePayHeadConfig = ((PayHeadCadreConfig) event.getObject());
		payHeadCadreConfig = selectedCadrewisePayHeadConfig;
		disableaddButton = true;
		addButtonFlag=true;
		log.info("<===Ends .onRowSelect ========>");
	}

	public String deleteCadre() {
		try {
			if (payHeadCadreConfigList == null) {
				errorMap.notify(ErrorDescription.SELECT_ONE.getErrorCode());
				Util.addWarn("Please select one field");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts payHeadCadreConfigList.deleteConfirm ---->");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/cradeWisePayHeadConfigBean/deleteCadre/" + selectedCadrewisePayHeadConfig.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.CRADEWISE_PAYHEADCONFIG_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting CadrewisePayHeadConfig....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends CadrewisePayHeadConfig.deleteConfirm ---->");
		return cadrewisePayHeadConfigListPage();
	}

}
