package in.gov.cooptex.masters.ecomm.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import in.gov.cooptex.common.ui.service.CommonDataService;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.OrderTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;

import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.SalesOrder;
import in.gov.cooptex.operation.model.SalesOrderItems;
import in.gov.cooptex.operation.model.SalesOrderSampleImage;
import in.gov.cooptex.operation.model.SupplierMaster;

import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("madeToOrderBean")
@Scope("session")
public class MadeToOrderBean  {
	
	private final String CREATE_PAGE = "/pages/ecommerce/admin/createMadeToOrder.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/ecommerce/admin/listMadeToOrder.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	
	@Autowired
	AppPreference appPreference;
	
	ObjectMapper mapper=new ObjectMapper();
	
	String jsonResponse;
	
	@Getter @Setter
	List<SalesOrder> salesOrderList=new ArrayList<SalesOrder>();
	
	@Getter @Setter
	SalesOrder salesOrder=new SalesOrder();
	
	@Getter @Setter
	SalesOrder selectedSalesOrder=new SalesOrder();
	
	@Getter @Setter
	List<SalesOrder> madeToOrderList = new ArrayList<SalesOrder>();
	
	@Getter @Setter
	SalesOrderItems salesOrderItem=new SalesOrderItems();
	
	@Getter @Setter
	Long saleOrderId=0l;
	
	@Getter @Setter
	Double quantity=0D;
	
	@Getter @Setter
	String description;
	
	@Getter @Setter
	private String action;
	
	@Getter @Setter
	Boolean orderAccept;
	
	@Getter
	@Setter
	LazyDataModel<SalesOrder> lazySalesOrderList;
	
	@Getter @Setter
	private int madeToOrderListSize;
	
	@Getter @Setter
	List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();
	
	@Getter @Setter
	List<SupplierMaster> supplierMasterList=new ArrayList<SupplierMaster>();
	
	@Getter @Setter
	SupplierMaster supplierMaster=new SupplierMaster();
	
	@Getter @Setter
	Long circleId=0l;
	
	@Getter @Setter
	private SalesOrderSampleImage salesOrderSampleImage=new SalesOrderSampleImage();
	
	@Getter @Setter
	List<SalesOrderSampleImage> salesOrderSampleImageList=new ArrayList<SalesOrderSampleImage>();
	
	@Setter @Getter
	private StreamedContent file;
	
	@Getter @Setter
	List<Integer> imageSize=new ArrayList<Integer>();
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	Boolean addButtonFlag = true;

	@Getter @Setter
	Boolean editButtonFlag = true;

	@Getter @Setter
	Boolean viewButtonFlag = true;
	
	@Getter @Setter
	private String zipfileName;
	
	@Getter @Setter
	OrderTypeMaster orderTypeMaster=new OrderTypeMaster();
	
public String loadData() {
	log.info("MadeToOrderBean=====>loadData Method Start");
	getSalesOrderByOrderTypeId();
	getAllCircleMaster();
	addButtonFlag = true;
	editButtonFlag = true;
	viewButtonFlag = false;
	log.info("MadeToOrderBean=====>loadData Method End");
	return CREATE_PAGE;
	}
	
public String loadMadeToOrderList() {
	log.info("MadeToOrderBean=====>loadMadeToOrderList Method Start");
	getAllMadeToOrderList();
	log.info("MadeToOrderBean=====>loadMadeToOrderList Method End");
	return LIST_PAGE;
}
	
public void getSalesOrderByOrderTypeId() {
	log.info("MadeToOrderBean=====>getSalesOrderByOrderTypeId Method Start");
	BaseDTO baseDTO=new BaseDTO();
	action="ADD";
	try {
		orderTypeMaster=getMadeToOrderTypeId();
		String url=SERVER_URL+appPreference.getOperationApiUrl()+"/salesorder/getsalesorderslist/"+orderTypeMaster.getId();
		baseDTO=httpService.get(url);
		if(baseDTO!=null && baseDTO.getStatusCode() == 0) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			salesOrderList = mapper.readValue(jsonResponse, new TypeReference<List<SalesOrder>>() {
			});	
			log.info("salesOrderList---------"+salesOrderList.size());
		}else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean=====>Error In getSalesOrderByOrderTypeId Method",exp);
		errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	}
	log.info("MadeToOrderBean=====>getSalesOrderByOrderTypeId Method End");
	
}
public void getSalesOrderById() {
	log.info("MadeToOrderBean=====>getSalesOrderById Method Start");
	BaseDTO baseDTO=new BaseDTO();
	imageSize=new ArrayList<Integer>();
	salesOrderSampleImageList=new ArrayList<SalesOrderSampleImage>();
	try {
		String url=SERVER_URL+appPreference.getOperationApiUrl()+"/salesorder/get/"+saleOrderId;
		baseDTO=httpService.get(url);
		if(baseDTO!=null && baseDTO.getStatusCode() == 0) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			salesOrder = mapper.readValue(jsonResponse, new TypeReference<SalesOrder>() {
			});
			if (salesOrder != null) {
				for (SalesOrderSampleImage salesOrderSampleImageObj : salesOrder.getSalesOrderSampleImageList()) {
					salesOrderSampleImage=new SalesOrderSampleImage();
					File imageFile = new File(salesOrderSampleImageObj.getFilePath()+"/"+salesOrderSampleImageObj.getFileName()+"."+salesOrderSampleImageObj.getFileType());
					salesOrderSampleImage.setStreamcontent(new DefaultStreamedContent(new FileInputStream(imageFile), "image/png"));
					salesOrderSampleImage.setFilePath(salesOrderSampleImageObj.getFilePath());
					salesOrderSampleImage.setFileType(salesOrderSampleImageObj.getFileType());
					salesOrderSampleImage.setFileName(salesOrderSampleImageObj.getFileName());
					//salesOrderSampleImageList.add(salesOrderSampleImage);
				}
				log.info("MadeToOrderBean=====>image Size=",salesOrderSampleImageList.size());
				imageSize.add(salesOrderSampleImageList.size());
				
				salesOrderItem = salesOrder.getSalesOrderItemsList().get(0);
				quantity=salesOrder.getSalesOrderItemsList().get(0).getItemQty();
				description=salesOrder.getSalesOrderItemsList().get(0).getItemNote();
				zipfileName=salesOrder.getSoNumberPrefix();
			}
			
			
		}else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
			
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean=====>Error In getSalesOrderById Method",exp);
		errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	}
	log.info("MadeToOrderBean=====>getSalesOrderById Method End");
	
}
public String submit() {
	log.info("MadeToOrderBean=====>acceptMadeToOrder Method Start"+salesOrder);
	BaseDTO baseDTO=new BaseDTO();
	List<SalesOrderItems> salesOrderItemsList=new ArrayList<SalesOrderItems>();
	try {
		salesOrder.setSupplier(supplierMaster);
		SalesOrderItems salesOrderItemObj=salesOrder.getSalesOrderItemsList().get(0);
		salesOrderItemObj.setItemTotal(salesOrderItem.getItemTotal());
		salesOrderItemsList.add(salesOrderItemObj);
		salesOrder.setSalesOrderItemsList(salesOrderItemsList);
		if(orderAccept) {
			salesOrder.setOrderStatus("ORDER_ACCEPTED");
		}else {
			salesOrder.setOrderStatus("ORDER_REJECTED");
		}

		String url=SERVER_URL+"/madeToOrder/accept";
		baseDTO=httpService.post(url, salesOrder);
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("MadeToOrder accepted successfully");
				if(action.equalsIgnoreCase("ADD"))
				    errorMap.notify(ErrorDescription.MADETOORDER_CREATED_SUCCESS.getCode());
				return LIST_PAGE;
				
			} else {
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
						+ baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				 return null;
			}
		}
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean=====>Error In acceptMadeToOrder Method",exp);
		errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	}
	return LIST_PAGE;
}

private void getAllMadeToOrderList() {
	log.info("<===== Starts MadeToOrderBean.lazyTenderList ======>");
	lazySalesOrderList = new LazyDataModel<SalesOrder>() {
		private static final long serialVersionUID = 8422543223567350599L;

		@Override
		public List<SalesOrder> load(int first, int pageSize, String sortField, SortOrder sortOrder,
				Map<String, Object> filters) {
			try {
				PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				log.info("Pagination request :::" + paginationRequest);
				String url = SERVER_URL+"/madeToOrder/getall";
				BaseDTO response = httpService.post(url, paginationRequest);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					madeToOrderList = mapper.readValue(jsonResponse,
							new TypeReference<List<SalesOrder>>() {
							});
					this.setRowCount(response.getTotalRecords());
					madeToOrderListSize = response.getTotalRecords();
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			} catch (Exception e) /*
									 * showBloodGroupListPageAction method used to action based view pages and
									 * render viewing
									 */ {
				log.error("Exception occured in loadLazySalesOrderList ...", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			log.info("Ends lazy load....");
			return madeToOrderList;
		}

		@Override
		public Object getRowKey(SalesOrder res) {
			return res != null ? res.getId() : null;
		}

		@Override
		public SalesOrder getRowData(String rowKey) {
			try {
				for (SalesOrder salesOrderObj : madeToOrderList) {
					if (salesOrderObj.getId().equals(Long.valueOf(rowKey))) {
						selectedSalesOrder = salesOrderObj;
						return salesOrderObj;
					}
				}
			} catch (Exception e) {
				log.error("Exception occured in getRowData ...", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			return null;
		}

	};
	log.info("<===== Ends MadeToOrderBean.loadLazySalesOrderList ======>");
}

public void getAllCircleMaster() {
	log.info("MadeToOrderBean:===>getAllCircleMaster Method Start");
	BaseDTO baseDTO=new BaseDTO();
   try {
		String url=SERVER_URL+"/circleMaster/loadallcircles/";
		baseDTO=httpService.get(url);
		if(baseDTO!=null) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			circleMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CircleMaster>>() {
			});	
			log.info("circleMasterList Size=---------"+circleMasterList.size());
			}else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean:===> Error In getAllCircleMaster Method",exp);
		errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	}
	log.info("MadeToOrderBean:===>getAllCircleMaster Method End");
}

public void getAllSupplierByCircleId() {
	log.info("MadeToOrderBean:===>getAllCircleMaster Method Start");
	BaseDTO baseDTO=new BaseDTO();
   try {
		String url=appPreference.getPortalServerURL()+"/supplier/master/getallsupplymaster/"+circleId;
		baseDTO=httpService.get(url);
		if(baseDTO!=null) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
			});	
			log.info("supplierMasterList Size=---------"+supplierMasterList.size());
			}else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean:===> Error In getAllCircleMaster Method",exp);
		errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	}
	log.info("MadeToOrderBean:===>getAllCircleMaster Method End");
}
public String cancel() {
	saleOrderId=0l;
	description="";
	salesOrder=new SalesOrder();
	selectedSalesOrder=new SalesOrder();
	salesOrderSampleImageList=new ArrayList<SalesOrderSampleImage>();
	salesOrderItem=new SalesOrderItems();
	imageSize=new ArrayList<Integer>();
	quantity=0D;
	circleId=0l;
	orderAccept=null;
	return LIST_PAGE;
}

public void downloadfile() {
	log.info("MadeToOrderBean:===>downloadfile Method Start");
	try {
		String zipPath = commonDataService.getAppKeyValue("MADE_TO_ORDER_ZIP_FILE_PATH");
		zipPath+="/"+zipfileName+".zip";
		FileOutputStream fos = new FileOutputStream(zipPath);
		ZipOutputStream zos = new ZipOutputStream(fos);
		for (SalesOrderSampleImage salesOrderSampleImageObj : salesOrderSampleImageList) {
		String path=salesOrderSampleImageObj.getFilePath()+"/"+salesOrderSampleImageObj.getFileName()+"."+salesOrderSampleImageObj.getFileType();
		addToZipFile(path,zos);
		downloadZipFile(zipPath);

		}
	
		zos.close();
		fos.close();
	} catch (FileNotFoundException e) {
		log.error("exception in downloadfile method------- " + e);
	}
	catch (Exception exp) {
		log.error("exception in downloadfile method------- " + exp);
	}
	log.info("MadeToOrderBean:===>downloadfile Method End");
}

public static void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {
	log.info("MadeToOrderBean:===>addToZipFile Method Start");
    try {
    	
    log.info("MadeToOrderBean:===>Writing "+fileName+" to zip file");	
	File file = new File(fileName);
	FileInputStream fis = new FileInputStream(file);
	ZipEntry zipEntry = new ZipEntry(fileName);
	zos.putNextEntry(zipEntry);

	byte[] bytes = new byte[1024];
	int length;
	while ((length = fis.read(bytes)) >= 0) {
		zos.write(bytes, 0, length);
	}

	zos.closeEntry();
	fis.close();
    }
    catch(Exception exp) {
    	log.info("MadeToOrderBean:===>Error In addToZipFile Method",exp);
    }
	log.info("MadeToOrderBean:===>addToZipFile Method End");
}

public void downloadZipFile(String zipFilePath) {
	log.info("MadeToOrderBean:===>downloadZipFile Method Start");
     InputStream input = null;
    try {
         File files = new File(zipFilePath);
		input = new FileInputStream(files);
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		file=(new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
       

    } catch (IOException e) {
    	log.info("MadeToOrderBean:===>Error In downloadZipFile Method",e);
    }
    log.info("MadeToOrderBean:===>downloadZipFile Method end");
}

public void onRowSelect(SelectEvent event) {
	log.info("<===Starts MadeToOrderBean.onRowSelect ========>" + event);
	selectedSalesOrder = ((SalesOrder) event.getObject());
	addButtonFlag = false;
	editButtonFlag = true;
	viewButtonFlag = true;
	
	log.info("<===Ends MadeToOrderBean.onRowSelect ========>");
}

public String showViewPage() {
	log.info("MadeToOrderBean:===>showViewPage Method Start");
	action = "VIEW";
	
	log.info("<----Redirecting to user view page---->");
	if (selectedSalesOrder.getId() == null) {
		Util.addWarn("Please Select One MadeTOOrder");
		return null;
	}
	viewMadeToOrder(selectedSalesOrder);
	loadData();
	viewButtonFlag = true;
	log.info("MadeToOrderBean:===>showViewPage Method End");
	return CREATE_PAGE;
}

public String showEditPage() {
	log.info("MadeToOrderBean:===>showEditPage Method Start");
	action = "EDIT";
	viewButtonFlag = false;
	log.info("<----Redirecting to user view page---->");
	if (selectedSalesOrder.getId() == null) {
		Util.addWarn("Please Select One MadeTOOrder");
		return null;
	}
	viewMadeToOrder(selectedSalesOrder);
	loadData();
	log.info("MadeToOrderBean:===>showEditPage Method End");
	return CREATE_PAGE;
}
public void viewMadeToOrder(SalesOrder salesOrderArg) {
	log.info("MadeToOrderBean:===>viewMadeToOrder Method Start");
	BaseDTO baseDTO=new BaseDTO();
	try {
		String url=SERVER_URL+appPreference.getOperationApiUrl()+"/salesorder/get/"+salesOrderArg.getId();
		baseDTO=httpService.get(url);
		if(baseDTO!=null && baseDTO.getStatusCode() == 0) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			salesOrder = mapper.readValue(jsonResponse, new TypeReference<SalesOrder>() {
			});	
		}
		if (salesOrder !=  null) {
			salesOrderItem = salesOrder.getSalesOrderItemsList().get(0);
			saleOrderId=salesOrder.getId();
			quantity=salesOrder.getSalesOrderItemsList().get(0).getItemQty();
			description=salesOrder.getSalesOrderItemsList().get(0).getItemNote();
			zipfileName=salesOrder.getSoNumberPrefix();
			if(salesOrder.getOrderStatus().equalsIgnoreCase("ORDER_ACCEPTED")) {
			orderAccept=true;
			}else if(salesOrder.getOrderStatus().equalsIgnoreCase("ORDER_REJECTED")) {
				orderAccept=false;
			}
			circleId=salesOrder.getSupplier().getCircleMaster().getId();
			supplierMaster=salesOrder.getSupplier();
			salesOrderItem=salesOrder.getSalesOrderItemsList().get(0);
			for (SalesOrderSampleImage salesOrderSampleImageObj : salesOrder.getSalesOrderSampleImageList()) {
				salesOrderSampleImage=new SalesOrderSampleImage();
				File imageFile = new File(salesOrderSampleImageObj.getFilePath()+"/"+salesOrderSampleImageObj.getFileName()+"."+salesOrderSampleImageObj.getFileType());
				salesOrderSampleImage.setStreamcontent(new DefaultStreamedContent(new FileInputStream(imageFile), "image/png"));
				salesOrderSampleImage.setFilePath(salesOrderSampleImageObj.getFilePath());
				salesOrderSampleImage.setFileType(salesOrderSampleImageObj.getFileType());
				salesOrderSampleImage.setFileName(salesOrderSampleImageObj.getFileName());
				salesOrderSampleImageList.add(salesOrderSampleImage);
			}
			imageSize=new ArrayList<Integer>();
			imageSize.add(salesOrderSampleImageList.size());
		}
		
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean:==> Error In viewMadeToOrder",exp);
	}
	log.info("MadeToOrderBean:===>viewMadeToOrder Method End");
}
public String clear() {
	log.info("MadeToOrderBean:===>clear Method Start");
	salesOrder=new SalesOrder();
	selectedSalesOrder=new SalesOrder();
	log.info("MadeToOrderBean:===>clear Method End");
	return LIST_PAGE;
}

public OrderTypeMaster getMadeToOrderTypeId() {
	log.info("MadeToOrderBean:getMadeToOrderTypeId Method Start===>");
	OrderTypeMaster orderTypeMaster=new OrderTypeMaster();
	BaseDTO baseDTO=new BaseDTO();
	try {
		String url=SERVER_URL+"/madeToOrder/getMadetoOrderTypeId";
		baseDTO=httpService.get(url);
		if(baseDTO!=null) {
			mapper=new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			orderTypeMaster = mapper.readValue(jsonResponse, new TypeReference<OrderTypeMaster>() {
			});	
		}
	}
	catch(Exception exp) {
		log.info("MadeToOrderBean:Error In getMadeToOrderTypeId Method ===>",exp);
	}
	log.info("MadeToOrderBean:getMadeToOrderTypeId Method End===>");
	return orderTypeMaster;
}
}
