package in.gov.cooptex.personnel.admin.bean;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.disciplinary.dto.DisciplinaryActionComplaintDetailsRequestDTO;
import in.gov.cooptex.admin.disciplinary.dto.DisciplinaryActionComplaintDetailsResponseDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintRegisterListResponseDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintRegisterManagerCommentsSaveDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintRegisterSaveDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintRegisterUpdateDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintReviewCommentsSaveDTO;
import in.gov.cooptex.admin.disciplinary.dto.EmpDisiplinaryActionViewDto;
import in.gov.cooptex.admin.disciplinary.dto.SuspensionDetailsResponseDto;
import in.gov.cooptex.admin.disciplinary.model.ComplaintManagerReview;
import in.gov.cooptex.admin.disciplinary.model.EmpComplaintRegister;
import in.gov.cooptex.admin.disciplinary.model.EmpComplaintRegisterLog;
import in.gov.cooptex.admin.disciplinary.model.EmpComplaintRegisterNote;
import in.gov.cooptex.admin.disciplinary.model.EnquiryOfficer;
import in.gov.cooptex.admin.model.ComplaintTypeMaster;
import in.gov.cooptex.admin.model.EmployeeSuspensionDetailsNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PenaltyDetailsDTO;
import in.gov.cooptex.core.dto.SalaryDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.DisciplinaryActionComplaintStatus;
import in.gov.cooptex.core.enums.PunishmentType;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeeSuspensionDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.PunishmentTypeMaster;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.hrms.dto.DeathRegistrationDTO;
import in.gov.cooptex.personnel.payroll.model.PenaltyRecoverySchedule;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("disciplinaryActionBean")
@Scope("session")
public class DisciplinaryActionBean extends CommonBean implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private final String DISCIPLINARY_ACTION_LIST_PAGE = "/pages/admin/raisecomplaint/listRaiseComplaint.xhtml?faces-redirect=true;";

	private final String CREATE_RAISE_COMPLAINT_PAGE = "/pages/admin/raisecomplaint/createRaiseComplaint.xhtml?faces-redirect=true;";
	private final String REVIEW_REGION_MANAGER = "/pages/admin/raisecomplaint/reviewRegionalManager.xhtml?faces-redirect=true;";

	private final String REVIEW_PRELIMINARY_ENQUIRY = "/pages/admin/raisecomplaint/preliminaryEnquiry.xhtml?faces-redirect=true;";
	private final String MANAGER_REVIEW_ONE = "/pages/admin/raisecomplaint/reviewManager-One.xhtml?faces-redirect=true;";
	private final String EXPLANATION_FROM_EMPLOYEE = "/pages/admin/raisecomplaint/explanationFromEmployee.xhtml?faces-redirect=true;";
	private final String ASSIGN_ENQUIRY_OFFICER = "/pages/admin/raisecomplaint/assignEnquiryOfficer.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/admin/raisecomplaint/viewRaiseComplaint.xhtml?faces-redirect=true;";

	private final String DOMESTIC_ENQUIRY_REPORT = "/pages/admin/raisecomplaint/domesticEnquiryReport.xhtml?faces-redirect=true;";
	private final String REVIEW_MANAGER = "/pages/admin/raisecomplaint/reviewManager.xhtml?faces-redirect=true;";

	private final String VIEWS_FROM_EMPLOYEE = "/pages/admin/raisecomplaint/explanationFromEmployee-two.xhtml?faces-redirect=true;";
	private final String SHOWCASE_NOTICE_ISSUE = "/pages/admin/raisecomplaint/showCaseNoticeIssue.xhtml?faces-redirect=true;";
	private final String EMPLOYEE_EXPLANATION_AGAINST_NOTICE = "/pages/admin/raisecomplaint/explanationAgainstNotice.xhtml?faces-redirect=true;";
	private final String FINAL_DECISION = "/pages/admin/raisecomplaint/complaintPunished.xhtml?faces-redirect=true;";

	private final String CREATE_SUSPENSION_EXTENSION = "/pages/admin/raisecomplaint/createSuspensionExtension.xhtml?faces-redirect=true;";

	private final String SUSPENSION_DETAILS_LIST_PAGE = "/pages/admin/raisecomplaint/listSuspensionDetails.xhtml?faces-redirect=true;";
	private final String SUSPENSION_DETAILS_VIEW_PAGE = "/pages/admin/raisecomplaint/viewSuspensionDetails.xhtml?faces-redirect=true;";

	private final String REVIEW_MANAGER_TWO = "/pages/admin/raisecomplaint/reviewManager-Two.xhtml?faces-redirect=true;";

	// private final String
	// VIEW_PAGE="/pages/admin/raisecomplaint/reviewRegionalManager.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;

	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	EmpComplaintRegister empComplaintRegister = new EmpComplaintRegister();

	@Getter
	@Setter
	EmpComplaintRegisterSaveDTO empComplaintRegisterSaveDTO = new EmpComplaintRegisterSaveDTO();

	@Getter
	@Setter
	EmpComplaintRegisterUpdateDTO empComplaintRegisterUpdateDTO = new EmpComplaintRegisterUpdateDTO();

	@Getter
	@Setter
	EmpComplaintReviewCommentsSaveDTO empComplaintReviewCommentsSaveDTO = new EmpComplaintReviewCommentsSaveDTO();

	@Getter
	@Setter
	List<EmpComplaintRegisterUpdateDTO> viewNoteEmployeeDetails = new ArrayList<>();
	
	@Getter
	@Setter
	List<EmpComplaintRegisterUpdateDTO> viewNoteSuspensionEmployeeDetails = new ArrayList<>();

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	String noteText, managerNoteText, managerRemarks, managerRemarksReject, explanation;

	@Getter
	@Setter
	Boolean isFinalApproval;

	@Getter
	@Setter
	Boolean managerShow = true;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Getter
	@Setter

	Long actionTakenByUserId;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String complainDetail;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Date complainRaiseDate, managerReviewDecisionEffectiveDate, managerReviewDecisionFromDate,
			managerReviewDecisionToDate;

	@Getter
	@Setter
	Date selectedFromDate, selectedToDate, selectedEffectiveDate;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<Religion> religionList = new ArrayList<>();

	@Setter
	@Getter
	String priliminaryFileName, priliminaryFilePath, priliminaryRemarks, explanationFilePath, explanationFileName,
			explanationRemarks, managerReviewRemarks, enquiryOfficerRemarks, enquiryOfficerUploadedfileName,
			enquiryOfficerUploadedfilePath, managerReviewFinalRemarks, managerReviewDecisionPunishmentType,
			employeeExplanationViews, employeeExplanationViewsUploadedfileName,
			employeeExplanationViewsUploadedfilePath, managerReviewDecisionRemarks, managerReviewShowCauseNotice;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();
	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<>();

	@Getter
	@Setter
	Department selectedDepartment;

	@Getter
	@Setter
	SectionMaster selectedSectionMaster;

	@Getter
	@Setter
	EmployeeMaster selectedEmployeeMaster;

	@Getter
	@Setter
	ComplaintTypeMaster selectedComplaintTypeMaster;

	@Setter
	@Getter
	String fileType;

	@Setter
	@Getter
	String filePath;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<>();

	@Getter
	@Setter
	Long departmentId, sectionId, employeeId, complaintTypeId, forwardToUserId, forwardToManagerUserId;

	@Getter
	@Setter
	String employeeCode;

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;
	@Getter
	@Setter
	List<ComplaintTypeMaster> complaintTypeMasterList;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Getter
	@Setter
	String docFileName;

	@Getter
	@Setter
	Boolean editable = true;

	@Getter
	@Setter
	private UploadedFile file = null;

	StreamedContent downloadFile;

	@Setter
	@Getter
	List<EntityMaster> headRegionOfficeList;

	@Setter
	@Getter
	List statusValues;

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	Long managerID, noteId;

	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc;

	@Getter
	@Setter
	LazyDataModel<EmpComplaintRegisterListResponseDTO> lazyEmpComplaintRegisterList;

	@Setter
	@Getter
	EmpComplaintRegisterListResponseDTO empComplaintRegisterListResponseDTO;

	@Setter
	@Getter
	DisciplinaryActionComplaintDetailsResponseDTO viewDisciplinaryActionComplaintDetailsResponseDTO;

	@Setter
	@Getter
	DisciplinaryActionComplaintDetailsRequestDTO disciplinaryActionComplaintDetailsRequestDTO = new DisciplinaryActionComplaintDetailsRequestDTO();

	@Getter
	@Setter
	private String file1Path = "", file2Path = "", fileName = "";

	@Getter
	@Setter
	EmpComplaintRegister empComplaintRegisterData;

	@Getter
	@Setter
	List<EmpComplaintRegisterLog> empComplaintRegisterLogList;

	@Getter
	@Setter
	List<EmpComplaintRegisterNote> empComplaintRegisterNoteList;

	@Getter
	@Setter
	List<ComplaintManagerReview> complaintManagerReviewLists;

	List<EmpComplaintRegisterListResponseDTO> complaintRegisterList = null;

	@Getter
	@Setter
	String selectedMenuDecsEnq;
	@Getter
	@Setter
	boolean decisionFlag;
	@Getter
	@Setter
	boolean assignEnquiryFlag;

	@Getter
	@Setter
	EmpComplaintRegisterListResponseDTO complaint, selectedComplaintId, selectedComplaintRegisterListRespDTO;

	@Getter
	@Setter
	List<PunishmentTypeMaster> punishmentTypeMasterList;

	@Getter
	@Setter
	PunishmentTypeMaster punishmentTypeMaster;

	@Getter
	@Setter
	Long selectedPunishmentTypeMasterId;

	@Getter
	@Setter
	List<EnquiryOfficer> enquiryOfficerList;

	@Getter
	@Setter
	Long selectedEnquiryOfficerId;

	@Getter
	@Setter
	String selectedEnquiryType;

	@Getter
	@Setter
	Double fineAmount;

	@Getter
	@Setter
	Long dueNumber;

	@Getter
	@Setter
	SalaryDTO salaryDTO = null;

	@Getter
	@Setter
	List<PenaltyDetailsDTO> penaltyDetailsDTOList;

	@Getter
	@Setter
	EmployeeMaster employee = new EmployeeMaster();

	@Getter
	@Setter
	ComplaintManagerReview complaintManagerReview = new ComplaintManagerReview();

	@Getter
	@Setter
	EmployeeSuspensionDetails employeeSuspensionDetails = new EmployeeSuspensionDetails();

	@Getter
	@Setter
	Boolean extendSuspension = false;

	@Getter
	@Setter
	EmpDisiplinaryActionViewDto empDisiplinaryActionViewDto = new EmpDisiplinaryActionViewDto();

	@Getter
	@Setter
	Boolean finalApproval;

	@Getter
	@Setter
	Long userId;

	@Getter
	@Setter
	String note = "";

	@Getter
	@Setter
	SuspensionDetailsResponseDto selectedSuspensionDetailsResponseDto, suspensionDetailsResponseDto;

	@Getter
	@Setter
	LazyDataModel<SuspensionDetailsResponseDto> lazySuspensionDetailsResponseDtoList;

	List<SuspensionDetailsResponseDto> suspensionDetailsResponseDtoList = null;

	@Getter
	@Setter
	Boolean suspensionDetailsViewFlag = true;

	@Getter
	@Setter
	Boolean buttonShowsCondition = false;

	@Getter
	@Setter
	Boolean approveButtonAction = true;

	@Getter
	@Setter
	String remarks;

	@Getter
	@Setter
	EmpComplaintRegisterLog empComplaintRegisterLog = new EmpComplaintRegisterLog();

	@Getter
	@Setter
	EmpComplaintRegisterNote empComplaintRegisterNote = new EmpComplaintRegisterNote();

	@Getter
	@Setter
	List<PunishmentTypeMaster> managerPunishmentTypeMasterList = new ArrayList<PunishmentTypeMaster>();

	@Getter
	@Setter
	boolean isAuthorization;

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	/**
	 * @return
	 */
	public String showDisciplinaryListPage() {
		log.info("showDisciplinaryListPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = false;
		deleteButtonFlag = false;
		extendSuspension = false;
		addButtonFlag = true;
		viewButtonFlag = false;
		fineAmount = null;
		salaryDTO = new SalaryDTO();
		selectedComplaintRegisterListRespDTO = new EmpComplaintRegisterListResponseDTO();
		lazyComplaintRaiseList();
		loadStatus();
		userMasters = commonDataService.loadForwardToUsersByFeature("DISCIPLINARY_ACTION");
		actionTakenByUserId = loginBean.getUserDetailSession().getId();
		onCancelRaiseComplaintCancel();
		// clearDisciplinaryListData();
		return DISCIPLINARY_ACTION_LIST_PAGE;
	}

	/**
	 * Load actions on List page
	 */
	public String disciplinaryListAction() {
		log.info("<====== disciplinaryActionBean.disciplinaryListAction Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== create disciplinary action page called.......====>");
				editable = true;
				onCancelRaiseComplaintCancel();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				departmentList = commonDataService.getDepartment();
				if (employeeMaster == null) {
					employeeMaster = new EmployeeMaster();
					employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
				}
				getActiveComplaintTypeMaster();
				// onSectionSelection();
				userMaster = new UserMaster();
				userMaster = null;
				return CREATE_RAISE_COMPLAINT_PAGE;
			}
			if (action.equalsIgnoreCase("View")) {
				editable = true;
				complainDetail = "";
				managerReviewShowCauseNotice = null;
				userMaster = new UserMaster();
				if (employeeMaster == null) {
					employeeMaster = new EmployeeMaster();
					employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
				}
				selectedMenuDecsEnq = "";
				return viewDisplinaryComplaintInfo();

			}
			if (action.equalsIgnoreCase("Edit")) {
				editable = false;
				// entityTypeMasterList = commonDataService.getEntityTypeByStatus();
				loadAllEntityTypeList();
				departmentList = commonDataService.getDepartment();

				getActiveComplaintTypeMaster();
				return viewDisplinaryComplaintInfo();

			}

		} catch (Exception e) {
			log.error("Exception Occured in disciplinaryActionBean.disciplinaryListAction:", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== disciplinaryActionBean.disciplinaryListAction Ends====>");
		return null;
	}

	public void updateExplanation() {
		log.info("===updateExplanation===", explanation);
	}

	public void headandRegional() {

		log.info("<====== disciplinaryActionBean.headandRegional Starts====>entity master",
				selectedHeadRegionOfc.getId());

		try {

			if (selectedHeadRegionOfc.getId() != null && selectedHeadRegionOfc.getEntityTypeMaster() != null
					&& selectedHeadRegionOfc.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
				visibility = true;
				selectedEntityMaster = new EntityMaster();
				selectedEntityMaster.setId(selectedHeadRegionOfc.getId());
				employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(selectedHeadRegionOfc,
						null, null, null, null);

			} else {
				visibility = false;
				// entityTypeMasterList = commonDataService.getEntityTypeByStatus();
				loadAllEntityTypeList();
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<======= disciplinaryActionBean.headandRegiona Ends =========>");

	}

	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			mapper = new ObjectMapper();
			/*
			 * EntityTypeMasterController.java - Method: getEntityType()
			 */
			BaseDTO response = httpService.get(SERVER_URL + "/entitytypemaster/getall/entitytype");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * Load Entity type dropdown
	 */

	public void onEntityTypeSelection() {

		log.info("<====== disciplinaryActionBean.onEntityTypeSelection Starts====> - selectedEntityTypeMaster"
				+ selectedEntityTypeMaster);
		selectedEntityMaster = new EntityMaster();
		// entityMasterList =
		// commonDataService.getEntityMaster(selectedEntityTypeMaster);
		entityMasterList = commonDataService.loadActiveEntityByregionOrentityTypeId(selectedHeadRegionOfc.getId(),
				selectedEntityTypeMaster.getId());
		employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(selectedEntityMaster,
				selectedEntityTypeMaster, null, null, null);
		log.info("<====== disciplinaryActionBean.onEntityTypeSelection Ends====> ");
	}

	/**
	 * Load SectionMaster based on department
	 */
	public void onDepartmentSelection() {
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Starts====> - Selected Dept. Id: "
				+ selectedDepartment.getId());
		employeeMasterList = new ArrayList<EmployeeMaster>();
		selectedEmployeeMaster = new EmployeeMaster();
		sectionMasterList = commonDataService.getSectionByDepartmentId(selectedDepartment.getId());
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Ends====> - SectionMasterListSize: "
				+ (sectionMasterList == null ? 0 : sectionMasterList.size()));
		employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(selectedEntityMaster,
				selectedEntityTypeMaster, selectedEntityMaster, selectedDepartment, null);
	}

	public void onSectionSelection() {
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Starts====> - Selected Dept. Id:"
				+ selectedDepartment.getId() + "- Selected section master. Id:" + selectedSectionMaster.getId());
		// getEmployeeNameAndIdByDepartmentAndSection(selectedDepartment.getId(),
		// selectedSectionMaster.getId());
		selectedEmployeeMaster = null;
		employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(selectedEntityMaster,
				selectedEntityTypeMaster, selectedEntityMaster, selectedDepartment, selectedSectionMaster);
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Ends====> ");
	}

	// Load employee list
	public List<EmployeeMaster> getEmployeeNameAndIdByDepartmentAndSection(Long departmentId, Long sectionId) {
		log.info("Inside disciplinaryActionbean.getEmployeeNameAndIdByDepartmentAndSection()===> -departmentId "
				+ departmentId + "sectionId" + sectionId);
		selectedEmployeeMaster = new EmployeeMaster();
		employeeMasterList = new ArrayList<EmployeeMaster>();
		String Url = AppUtil.getPortalServerURL() + "/employeepersonalinfoemployment/getbydepartmentandsection/"
				+ departmentId + "/" + sectionId;
		try {

			BaseDTO response = httpService.get(Url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				if (employeeMasterList != null)
					log.info("employeeMasterList size ::::::::::::::::" + employeeMasterList.size());

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in getEmployeeNameAndIdByDepartmentAndSection ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends disciplinarActionbean.getEmployeeNameAndIdByDepartmentAndSection ======>");

		return employeeMasterList;
	}

	public void onSectionSelectionDisciplinary() {
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Starts====> - Selected Dept. Id:"
				+ selectedDepartment.getId() + "- Selected section master. Id:" + selectedSectionMaster.getId());
		getEmployeeNameAndIdByDepartmentAndSectionforDisciplinary(selectedDepartment.getId(),
				selectedSectionMaster.getId());
		log.info("<====== disciplinaryActionBean.onDepartmentSelection Ends====> ");
	}

	// Load employee list
	public List<EmployeeMaster> getEmployeeNameAndIdByDepartmentAndSectionforDisciplinary(Long departmentId,
			Long sectionId) {
		log.info("Inside disciplinaryActionbean.getEmployeeNameAndIdByDepartmentAndSection()===> -departmentId "
				+ departmentId + "sectionId" + sectionId);
		selectedEmployeeMaster = new EmployeeMaster();
		String Url = null;
		employeeMasterList = new ArrayList<EmployeeMaster>();
		if (empComplaintRegisterData != null) {
			if (empComplaintRegisterData.getEmpMaster() != null) {
				if (empComplaintRegisterData.getEmpMaster().getId() != null) {
					Url = AppUtil.getPortalServerURL()
							+ "/employeepersonalinfoemployment/getbydepartmentandsectionfordisciplinary/" + departmentId
							+ "/" + sectionId + "/" + empComplaintRegisterData.getEmpMaster().getId();
				}
			}
		}
		try {
			BaseDTO response = httpService.get(Url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				if (employeeMasterList != null)
					log.info("employeeMasterList size ::::::::::::::::" + employeeMasterList.size());

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in getEmployeeNameAndIdByDepartmentAndSection ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends disciplinarActionbean.getEmployeeNameAndIdByDepartmentAndSection ======>");

		return employeeMasterList;
	}

	// Load employee list
	public void getActiveComplaintTypeMaster() {
		log.info("Inside disciplinaryActionbean.getActiveComplaintTypeMaster()");
		complaintTypeMasterList = new ArrayList<ComplaintTypeMaster>();
		String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/complainttype/getActiveComplaintTypeMaster";
		try {

			BaseDTO response = httpService.get(Url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				complaintTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ComplaintTypeMaster>>() {
						});
				if (complaintTypeMasterList != null)
					log.info("complaintTypeMasterList size ::::::::::::::::" + complaintTypeMasterList.size());

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in getActiveComplaintTypeMaster ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends disciplinaryActionbean.getActiveComplaintTypeMaster ======>");

		// return complaintTypeMasterList;
	}

	/**
	 * Reset all the values
	 */

	public String onCancelRaiseComplaintCancel() {
		log.info(
				"===================disciplinaryActionbean.onCancelRaiseComplaintCancel started===============================================");
		if (editable == true) {
			selectedEntityTypeMaster = null;
			selectedHeadRegionOfc = null;
			selectedEntityMaster = null;
			selectedDepartment = null;
			selectedSectionMaster = null;
			selectedEmployeeMaster = null;
			selectedComplaintTypeMaster = null;
		}
		complainRaiseDate = null;
		complainDetail = "";

		forwardToUserId = null;
		fileType = null;
		filePath = null;
		isFinalApproval = null;
		noteText = "";
		log.info("===================disciplinaryActionbean.on"
				+ "celRaiseComplaintCancel Ends===============================================");
		return DISCIPLINARY_ACTION_LIST_PAGE;

	}

	public String onCancelManagerReviewCancel() {
		log.info(
				"===================disciplinaryActionbean.onCancelManagerReviewCancel started===============================================");
		punishmentTypeMaster = null;
		complainDetail = "";
		selectedEntityTypeMaster = null;
		selectedHeadRegionOfc = null;
		selectedEntityMaster = null;
		selectedDepartment = null;
		selectedSectionMaster = null;
		selectedEmployeeMaster = null;
		selectedComplaintTypeMaster = null;
		forwardToUserId = null;
		selectedFromDate = null;
		selectedToDate = null;
		selectedEffectiveDate = null;
		selectedMenuDecsEnq = null;
		selectedPunishmentTypeMasterId = null;
		selectedEnquiryType = null;
		selectedEnquiryOfficerId = null;
		fineAmount = null;
		dueNumber = null;
		log.info("===================disciplinaryActionbean.on"
				+ "onCancelManagerReviewCancel Ends===============================================");

		return showDisciplinaryListPage();

	}

	public void onChangeManagerReviewCancel() {
		log.info(
				"===================disciplinaryActionbean.onChangeManagerReviewCancel started===============================================");
		punishmentTypeMaster = null;
		selectedEntityTypeMaster = null;
		selectedHeadRegionOfc = null;
		selectedEntityMaster = null;
		selectedDepartment = null;
		selectedSectionMaster = null;
		selectedEmployeeMaster = null;
		selectedComplaintTypeMaster = null;
		forwardToUserId = null;
		selectedFromDate = null;
		selectedToDate = null;
		selectedEffectiveDate = null;
		selectedPunishmentTypeMasterId = null;
		selectedEnquiryType = null;
		selectedEnquiryOfficerId = null;
		fineAmount = null;
		dueNumber = null;
		log.info("===================disciplinaryActionbean.on"
				+ "onChangeManagerReviewCancel Ends===============================================");

	}

	public void onChangeManagerReviewDeciscionCancel() {
		log.info(
				"===================disciplinaryActionbean.onChangeManagerReviewDeciscionCancel started==============================================="
						+ punishmentTypeMaster);

		selectedEntityTypeMaster = null;
		selectedHeadRegionOfc = null;
		selectedEntityMaster = null;
		selectedDepartment = null;
		selectedSectionMaster = null;
		selectedEmployeeMaster = null;
		selectedComplaintTypeMaster = null;
		forwardToUserId = null;
		selectedFromDate = null;
		selectedToDate = null;
		selectedEffectiveDate = null;
		selectedEnquiryOfficerId = null;
		fineAmount = null;
		dueNumber = null;
		log.info("===================disciplinaryActionbean.on"
				+ "onChangeManagerReviewDeciscionCancel Ends===============================================");

	}

	/**
	 * Reset create note value
	 */
	public void onCancelRaiseComplaintNote() {
		log.info(
				"===================disciplinaryActionbean.onCancelRaiseComplaintNote started===============================================");
		log.info("note value" + noteText);
		noteText = "";

		log.info(
				"===================disciplinaryActionbean.onCancelRaiseComplaintNote Ends===============================================");

	}

	/*
	 * SocietyAdjustmentBean.lazySocietyAdjustmentList method used to load list
	 * using lazy loading
	 */
	public void lazyComplaintRaiseList() {
		// List<EmpComplaintRegisterListResponseDTO> complaintRegisterList = new
		// ArrayList<>();
		log.info("<===== Starts disciplinaryActionbean.lazyComplaintRaiseList ======>");
		lazyEmpComplaintRegisterList = new LazyDataModel<EmpComplaintRegisterListResponseDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EmpComplaintRegisterListResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					EmployeeMaster employeMaster = loginBean.getEmployee();
					if (employeMaster != null && employeMaster.getId() != null) {
						log.info("======Login Employee Id=====" + employeMaster.getId());
						paginationRequest.setEmpId(employeMaster.getId());
					}
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/api/v1/operation/empcomplaintregister/getallcomplaintlist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						complaintRegisterList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpComplaintRegisterListResponseDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						for (EmpComplaintRegisterListResponseDTO p : complaintRegisterList) {
							log.info("p.getId   ::::::::::" + p.getEntity());
						}
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in lazyComplaintRaiseList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends disciplinaryActionbean.lazyComplaintRaiseList....");
				return complaintRegisterList;
			}

			@Override
			public Object getRowKey(EmpComplaintRegisterListResponseDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpComplaintRegisterListResponseDTO getRowData(String rowKey) {
				try {
					for (EmpComplaintRegisterListResponseDTO complaint : complaintRegisterList) {
						if (complaint.getId().equals(Long.valueOf(rowKey))) {
							selectedComplaintId = complaint;
							return complaint;
						}

					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends disciplinaryActionbean.lazySocietyAdjustmentList ======>");
	}

	/**
	 * Set statusValues
	 */
	private void loadStatus() {
		log.info("<===== Starts disciplinaryActionbean.loadStatus ======>");
		statusValues = new ArrayList<>();
		Object[] statusArray = DisciplinaryActionComplaintStatus.class.getEnumConstants();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
		log.info("<--- statusValues ---> " + statusValues);
		log.info("<===== Ends disciplinaryActionbean.loadStatus ======>");
	}

	/**
	 * Set Max Date to Today Date
	 */
	public Date getToday() {
		return new Date();
	}

	/**
	 * Creating the disciplinary complaint
	 */
	public String onCreateRaiseComplaintSubmit() {
		log.info(
				"===================disciplinaryActionbean.onCreateRaiseComplaintSubmit started===============================================");
		if (editable == true) {
			empComplaintRegisterSaveDTO.setActiveStatus(true);
			empComplaintRegisterSaveDTO.setComplaintDescription(complainDetail);
			empComplaintRegisterSaveDTO.setComplaintRaisedDate(complainRaiseDate);
			empComplaintRegisterSaveDTO.setComplaintTypeMasterId(selectedComplaintTypeMaster.getId());
			empComplaintRegisterSaveDTO.setDepartmentId(selectedDepartment.getId());
			empComplaintRegisterSaveDTO.setEmployeeId(selectedEmployeeMaster.getId());
			empComplaintRegisterSaveDTO.setEntityId(selectedEntityMaster.getId());
			empComplaintRegisterSaveDTO.setSectionId(selectedSectionMaster.getId());
			empComplaintRegisterSaveDTO.setUserId(actionTakenByUserId);
			empComplaintRegisterSaveDTO.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.INITIATED));
			empComplaintRegisterSaveDTO.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.INITIATED));
			// empComplaintRegisterSaveDTO.setLogRemarks("log remarks hardcoded");
			empComplaintRegisterSaveDTO.setNote(noteText);
			if (userMaster != null && userMaster.getId() != null) {
				empComplaintRegisterSaveDTO.setForwardToUserId(userMaster.getId());
			}

			empComplaintRegisterSaveDTO.setFinalApproval(finalApproval);

			log.info("Employee complaint register " + empComplaintRegisterSaveDTO);

			try {

				BaseDTO response = new BaseDTO();

				String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/empcomplaintregister/create";

				response = httpService.post(Url, empComplaintRegisterSaveDTO);
				if (response != null && response.getStatusCode() == 0)
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.COMPLAINT_RAISE_SUCCESS).getCode());
				else
					errorMap.notify(response.getStatusCode());

			} catch (Exception e) {
				log.info("Exception occured while submitting........", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} else {
			empComplaintRegisterUpdateDTO.setComplaintDescription(complainDetail);
			empComplaintRegisterUpdateDTO.setComplaintRaisedDate(complainRaiseDate);
			empComplaintRegisterUpdateDTO.setUserId(actionTakenByUserId);
			empComplaintRegisterUpdateDTO
					.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.COMPLAINT_EDITED));
			empComplaintRegisterUpdateDTO
					.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.COMPLAINT_EDITED));
			empComplaintRegisterUpdateDTO.setNote(noteText);
			if (userMaster != null && userMaster.getId() != null) {
				empComplaintRegisterUpdateDTO.setForwardToUserId(userMaster.getId());
			}
			// empComplaintRegisterUpdateDTO.setForwardToUserId(forwardToUserId);

			empComplaintRegisterUpdateDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
			empComplaintRegisterUpdateDTO.setEmpComplaintRegisterNoteId(noteId);
			empComplaintRegisterUpdateDTO.setFinalApproval(finalApproval);
			log.info("Employee complaint register " + empComplaintRegisterUpdateDTO);

			try {

				BaseDTO response = new BaseDTO();

				String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/empcomplaintregister/update";

				response = httpService.post(Url, empComplaintRegisterUpdateDTO);
				if (response != null && response.getStatusCode() == 0)
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.COMPLAINT_RAISE_UPDATE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());

			} catch (Exception e) {
				log.info("Exception occured while submitting........", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		}
		log.info("<===== Ends disciplinaryActionbean.onCreateRaiseComplaintSubmit ======>");
		onCancelRaiseComplaintCancel();
		return showDisciplinaryListPage();

	}
	// public String onUpdateRaiseComplaintSubmit() {
	// log.info(
	// "===================disciplinaryActionbean.onUpdateRaiseComplaintSubmit
	// started===============================================");
	//
	//
	// log.info("<===== Ends disciplinaryActionbean.onUpdateRaiseComplaintSubmit
	// ======>");
	// onCancelRaiseComplaintCancel();
	// return DISCIPLINARY_ACTION_LIST_PAGE;
	//
	// }

	/**
	 * Uploading the file
	 */

	public String handleFileUpload(FileUploadEvent event) {
		fileType = null;
		filePath = null;
		log.info("<<=== disciplinaryActionbean. File Upload method called" + empComplaintRegisterSaveDTO.getFileName());
		try {
			// empComplaintRegisterSaveDTO = (empComplaintRegisterSaveDTO)
			// event.getComponent().getAttributes().get("object");
			UploadedFile file = event.getFile();
			fileType = file.getFileName();
			long size = file.getSize();
			Long disciplinaryActionUploadFileMaxSizeInMb = Long
					.valueOf(commonDataService.getAppKeyValue("DISCIPLINARY_ACTION_UPLOAD_FILE_MAX_SIZE_IN_MB"));

			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")
					&& !fileType.contains(".txt") && !fileType.contains(".xls") && !fileType.contains(".xlsx")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
				fileType = null;
				filePath = null;
				empComplaintRegisterSaveDTO.setFileName(fileType);
				empComplaintRegisterSaveDTO.setFilePath(filePath);

				empComplaintReviewCommentsSaveDTO.setFileName(fileType);
				empComplaintReviewCommentsSaveDTO.setFilePath(filePath);
			} else if ((size / (1024 * 1024)) > disciplinaryActionUploadFileMaxSizeInMb) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
				fileType = null;
				filePath = null;
				empComplaintRegisterSaveDTO.setFileName(fileType);
				empComplaintRegisterSaveDTO.setFilePath(filePath);
				empComplaintReviewCommentsSaveDTO.setFileName(fileType);
				empComplaintReviewCommentsSaveDTO.setFilePath(filePath);
			} else {
				String createRaiseComplaintPathName = commonDataService
						.getAppKeyValue("DISCIPLINARY_ACTION_FILES_UPLOAD_PATH");
				filePath = Util.fileUpload(createRaiseComplaintPathName, file);
				empComplaintRegisterSaveDTO.setFileName(fileType);
				empComplaintRegisterSaveDTO.setFilePath(filePath);
				empComplaintReviewCommentsSaveDTO.setFileName(fileType);
				empComplaintReviewCommentsSaveDTO.setFilePath(filePath);
				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Relieving Document is uploaded successfully");
			}
			return null;

		} catch (Exception e) {
			log.info("<<==  Error in file upload " + e);
		}

		log.info("<====== disciplinaryActionBean.handleFileUpload Ends====> ");
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("disciplinaryActionBean onRowSelect method started");
		editable = true;
		log.info("disciplinaryActionBean onRowSelect method started", selectedComplaintRegisterListRespDTO);

		if ((selectedComplaintRegisterListRespDTO.getStatus()
				.equals(String.valueOf(DisciplinaryActionComplaintStatus.INITIATED))
				|| selectedComplaintRegisterListRespDTO.getStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_REJECTED))
				|| selectedComplaintRegisterListRespDTO.getStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.COMPLAINT_EDITED)))
				&& selectedComplaintRegisterListRespDTO.getCreatedByUserId().equals(actionTakenByUserId)) {
			// managerShow = false;
			deleteButtonFlag = true;
			editButtonFlag = true;
			// viewButtonFlag = true;
			viewButtonFlag = false;
		} else if (selectedComplaintRegisterListRespDTO.getStatus()
				.equals(String.valueOf(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED))) {
			UserMaster currentUser = loginBean.getUserMaster();
			Long userId = currentUser != null ? currentUser.getId() : null;
			if (selectedComplaintRegisterListRespDTO.getCreatedByUserId() != null && userId != null
					&& userId.longValue() == selectedComplaintRegisterListRespDTO.getCreatedByUserId().longValue()) {
				suspensionDetails();
				if ("Suspension".equals(empDisiplinaryActionViewDto.getPunishmentType())) {
					extendSuspension = true;
				} else {
					extendSuspension = false;
				}
			} else {
				extendSuspension = false;
			}
			fineAmount = null;
			loadEmployeePenalty(selectedComplaintRegisterListRespDTO.getId());
			fineAmount = fineAmount != null ? fineAmount.doubleValue() : 0D;
			if (fineAmount > 0D) {
				viewButtonFlag = true;
			} else {
				viewButtonFlag = false;
			}
		} else {
			managerShow = true;
			editButtonFlag = false;
			// viewButtonFlag = true;
			viewButtonFlag = false;
			deleteButtonFlag = false;
		}

		addButtonFlag = false;

		// empComplaintRegisterListResponseDTO = ((EmpComplaintRegisterListResponseDTO)
		// event.getObject());
		// addButtonFlag = true;
	}

	public void updateToDate() {
		if (selectedFromDate != null && selectedToDate != null && selectedToDate.before(selectedFromDate)) {
			selectedToDate = null;
		}

	}

	public String viewDisplinaryComplaintInfo() {
		log.info("<===== Starts disciplinaryActionBean.viewDisplinaryComplaintInfo ======>");
		try {
			note = null;
			managerPunishmentTypeMasterList = new ArrayList<>();
			disciplinaryActionComplaintDetailsRequestDTO
					.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
			disciplinaryActionComplaintDetailsRequestDTO
					.setNotificationId(selectedComplaintRegisterListRespDTO.getNotificationId());
			disciplinaryActionComplaintDetailsRequestDTO.setUserId(actionTakenByUserId);
			clearViewData();
			String url = SERVER_URL + "/api/v1/operation/empcomplaintregister/viewcomplaintdetails/";
			BaseDTO response = httpService.post(url, disciplinaryActionComplaintDetailsRequestDTO);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				viewDisciplinaryActionComplaintDetailsResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<DisciplinaryActionComplaintDetailsResponseDTO>() {
						});

				if (response.getTotalListOfData() != null) {
					String jsonResponses = mapper.writeValueAsString(response.getTotalListOfData());
					viewNoteEmployeeDetails = mapper.readValue(jsonResponses,
							new TypeReference<List<EmpComplaintRegisterUpdateDTO>>() {
							});
				}

				if (viewDisciplinaryActionComplaintDetailsResponseDTO != null) {
					log.info("viewDisplinaryComplaintInfo ::::::::::::::::"
							+ viewDisciplinaryActionComplaintDetailsResponseDTO);
					empComplaintRegisterData = viewDisciplinaryActionComplaintDetailsResponseDTO
							.getEmpComplaintRegister();
					empComplaintRegisterLogList = viewDisciplinaryActionComplaintDetailsResponseDTO
							.getEmpComplaintRegisterLogList();
					complaintManagerReviewLists = viewDisciplinaryActionComplaintDetailsResponseDTO
							.getComplaintManagerReviewList();
					empComplaintRegisterNoteList = viewDisciplinaryActionComplaintDetailsResponseDTO
							.getEmpComplaintRegisterNoteList();
					isAuthorization = viewDisciplinaryActionComplaintDetailsResponseDTO
							.isAuthorizedUserForNextOperation();

					if (!empComplaintRegisterNoteList.isEmpty()) {
						empComplaintRegisterNote = empComplaintRegisterNoteList
								.get(empComplaintRegisterNoteList.size() - 1);
						note = empComplaintRegisterNoteList.get(0).getNote();
					}
					for (EmpComplaintRegisterLog empComplaintRegisterLog : empComplaintRegisterLogList) {
						if (empComplaintRegisterLog.getStage().equals(
								DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_COMPLETED.toString())) {
							managerID = empComplaintRegisterLog.getCreatedBy().getId();
						}
						if (empComplaintRegisterLog.getStage().equals(
								DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_4_COMPLETED.toString())) {
							managerID = empComplaintRegisterLog.getCreatedBy().getId();
						}
						if (empComplaintRegisterLog.getStage()
								.equals(DisciplinaryActionComplaintStatus.PRELIMINARY_ENQUIRY_COMPLETED.toString())) {

							priliminaryFileName = empComplaintRegisterLog.getFileName();
							priliminaryFilePath = empComplaintRegisterLog.getFilePath();
							priliminaryRemarks = empComplaintRegisterLog.getRemarks();
						}
						if (empComplaintRegisterLog.getStage()
								.equals(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_5_COMPLETED
										.toString())) {

							explanationFileName = empComplaintRegisterLog.getFileName();
							explanationFilePath = empComplaintRegisterLog.getFilePath();
							explanationRemarks = empComplaintRegisterLog.getRemarks();

						}
						if (empComplaintRegisterLog.getStage()
								.equals(DisciplinaryActionComplaintStatus.ENQUIRE_OFFICER_ASSIGNED.toString())) {

							managerReviewRemarks = empComplaintRegisterLog.getRemarks();

						}
						if (empComplaintRegisterLog.getStage().equals(
								DisciplinaryActionComplaintStatus.ENQUIRY_OFFICER_ENQUIRY_COMPLETED.toString())) {
							if (StringUtils.isNotEmpty(empComplaintRegisterLog.getRemarks())) {
								enquiryOfficerRemarks = empComplaintRegisterLog.getRemarks().replaceAll("\\<.*?\\>",
										"");
							}
							enquiryOfficerUploadedfileName = empComplaintRegisterLog.getFileName();
							enquiryOfficerUploadedfilePath = empComplaintRegisterLog.getFilePath();
						}
						if (empComplaintRegisterLog.getStage().equals(
								DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_8_COMPLETED.toString())) {

							managerReviewFinalRemarks = empComplaintRegisterLog.getRemarks();

						}
						if (empComplaintRegisterLog.getStage()
								.equals(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_9_COMPLETED
										.toString())) {

							employeeExplanationViews = empComplaintRegisterLog.getRemarks();
							employeeExplanationViewsUploadedfileName = empComplaintRegisterLog.getFileName();
							employeeExplanationViewsUploadedfilePath = empComplaintRegisterLog.getFilePath();
						}
						if (empComplaintRegisterLog.getStage().equals(
								DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_10_COMPLETED.toString())) {
							managerReviewShowCauseNotice = empComplaintRegisterLog.getRemarks();
						}

						if (empComplaintRegisterLog.getStage()
								.equals(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED.toString())) {

							managerReviewDecisionRemarks = empComplaintRegisterLog.getRemarks();

						}
					}
					for (ComplaintManagerReview complaintManagerReview : complaintManagerReviewLists) {
						if (complaintManagerReview.getEmpComplaintRegister().getComplaintStatus()
								.equals(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED.toString())) {

							if (complaintManagerReview.getPunishmentTypeMaster() != null) {
								managerReviewDecisionEffectiveDate = complaintManagerReview.getEffectiveDate();
								managerReviewDecisionFromDate = complaintManagerReview.getFromDate();
								managerReviewDecisionToDate = complaintManagerReview.getToDate();
								managerReviewDecisionPunishmentType = complaintManagerReview.getPunishmentTypeMaster()
										.getName();
							}
						}
					}
				}

				selectedComplaintRegisterListRespDTO.setNotificationId(null);
				systemNotificationBean.loadTotalMessages();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
			log.info("<===== Ends disciplinaryActionBean.viewDisplinaryComplaintInfo ======>");
			if (editable == false) {
				selectedEntityTypeMaster = empComplaintRegisterData.getEntity().getEntityTypeMaster();
				selectedEntityMaster = empComplaintRegisterData.getEntity();
				complainRaiseDate = empComplaintRegisterData.getComplaintRaisedDate();
				selectedDepartment = empComplaintRegisterData.getDepartmentMaster();
				complainDetail = empComplaintRegisterData.getComplaintDescription();
				fileType = empComplaintRegisterData.getFileName();
				filePath = empComplaintRegisterData.getFilePath();
				empComplaintRegisterUpdateDTO.setFileName(fileType);
				empComplaintRegisterUpdateDTO.setFilePath(filePath);
				onDepartmentSelection();
				selectedSectionMaster = empComplaintRegisterData.getSectionMaster();
				onSectionSelection();
				selectedEmployeeMaster = empComplaintRegisterData.getEmpMaster();
				selectedComplaintTypeMaster = empComplaintRegisterData.getComplaintTypeMaster();

				// entityMasterList =
				// commonDataService.loadEntityByregionOrentityTypeId(selectedEntityMaster.getEntityMasterRegion().getId(),
				// selectedEntityTypeMaster.getId());
				for (EmpComplaintRegisterNote empComplaintRegisterNote : empComplaintRegisterNoteList) {
					if (empComplaintRegisterNote.getCreatedBy().getId().equals(actionTakenByUserId)) {
						noteText = empComplaintRegisterNote.getNote();
						forwardToUserId = empComplaintRegisterNote.getForwardTo().getId();
						noteId = empComplaintRegisterNote.getId();
						finalApproval = empComplaintRegisterNote.getFinalApproval();
					}
				}
				return CREATE_RAISE_COMPLAINT_PAGE;
			} else if (isAuthorization == true) {
				if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.INITIATED))
						|| empComplaintRegisterData.getComplaintStatus()
								.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_APPROVED))
						|| empComplaintRegisterData.getComplaintStatus()
								.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_FINAL_APPROVED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					Iterator<PunishmentTypeMaster> punishmentTypeMasterItr = punishmentTypeMasterList.iterator();
					while (punishmentTypeMasterItr.hasNext()) {
						PunishmentTypeMaster punishmentTypeMaster = punishmentTypeMasterItr.next();

						if (punishmentTypeMaster.getName().equalsIgnoreCase(PunishmentType.DROP)
								|| punishmentTypeMaster.getName().equalsIgnoreCase(PunishmentType.WARNING)
								|| punishmentTypeMaster.getName().equalsIgnoreCase(PunishmentType.SEVERE_WARNING)) {
							managerPunishmentTypeMasterList.add(punishmentTypeMaster);
						}
					}
					return REVIEW_REGION_MANAGER;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_COMPLETED))) {
					return REVIEW_PRELIMINARY_ENQUIRY;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.PRELIMINARY_ENQUIRY_COMPLETED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
					departmentList = commonDataService.getDepartment();
					enquiryOfficerList = commonDataService.getEnquiryOfficerList();
					selectedMenuDecsEnq = null;
					return MANAGER_REVIEW_ONE;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_4_COMPLETED))) {
					return EXPLANATION_FROM_EMPLOYEE;
				} else if (empComplaintRegisterData.getComplaintStatus().equals(String
						.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_5_COMPLETED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
					departmentList = commonDataService.getDepartment();
					enquiryOfficerList = commonDataService.getEnquiryOfficerList();
					return ASSIGN_ENQUIRY_OFFICER;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRE_OFFICER_ASSIGNED))) {
					return DOMESTIC_ENQUIRY_REPORT;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRY_OFFICER_ENQUIRY_COMPLETED))) {
					return REVIEW_MANAGER;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED))) {
					return VIEW_PAGE;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_8_COMPLETED))) {
					return VIEWS_FROM_EMPLOYEE;
				} else if (empComplaintRegisterData.getComplaintStatus().equals(String
						.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_9_COMPLETED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					return REVIEW_MANAGER_TWO;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_10_COMPLETED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
					departmentList = commonDataService.getDepartment();
					enquiryOfficerList = commonDataService.getEnquiryOfficerList();
					return SHOWCASE_NOTICE_ISSUE;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.SHOW_CAUSE_NOTICE_ISSUED))) {
					return EMPLOYEE_EXPLANATION_AGAINST_NOTICE;
				} else if (empComplaintRegisterData.getComplaintStatus().equals(String.valueOf(
						DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_12_COMPLETED_AGAINST_NOTICE))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					punishmentTypeMasterList = setPunishmentTypeMasterValues(punishmentTypeMasterList);
					punishmentTypeMaster = new PunishmentTypeMaster();
					selectedEffectiveDate = null;
					return FINAL_DECISION;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.COMPLAINT_EDITED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					return REVIEW_REGION_MANAGER;
				} else if (empComplaintRegisterData.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_REJECTED))) {
					punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
					return REVIEW_REGION_MANAGER;
				}
			} else {
				punishmentTypeMasterList = commonDataService.loadPunishmentTypeMaster();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				departmentList = commonDataService.getDepartment();
				enquiryOfficerList = commonDataService.getEnquiryOfficerList();
				return VIEW_PAGE;
			}

		} catch (Exception e) {
			log.error("Exception occured in viewDisplinaryComplaintInfo ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		// if(empComplaintRegisterData.getCreatedBy().getId().equals(actionTakenByUserId)){
		//
		// }

		log.info("<===== Ends disciplinaryActionBean.viewDisplinaryComplaintInfo ======>");
		return null;
	}

	private void clearViewData() {
		managerID = null;
		priliminaryFileName = null;
		priliminaryFilePath = null;
		priliminaryRemarks = null;
		explanationFileName = null;
		explanationFilePath = null;
		explanationRemarks = null;
		managerReviewRemarks = null;
		enquiryOfficerRemarks = null;
		enquiryOfficerUploadedfileName = null;
		enquiryOfficerUploadedfilePath = null;
		managerReviewFinalRemarks = null;
		employeeExplanationViews = null;
		employeeExplanationViewsUploadedfileName = null;
		employeeExplanationViewsUploadedfilePath = null;
		managerReviewShowCauseNotice = null;
		managerReviewDecisionRemarks = null;
		managerReviewDecisionEffectiveDate = null;
		managerReviewDecisionFromDate = null;
		managerReviewDecisionToDate = null;
		managerReviewDecisionPunishmentType = null;
	}

	private List<PunishmentTypeMaster> setPunishmentTypeMasterValues(
			List<PunishmentTypeMaster> punishmentTypeMasterList2) {
		List<PunishmentTypeMaster> punishmentTypeMasterLists = new ArrayList<>();
		for (PunishmentTypeMaster punTypeMaster : punishmentTypeMasterList) {
			if (punTypeMaster.getName().equalsIgnoreCase("Removal from service")
					|| punTypeMaster.getName().equalsIgnoreCase("Termination from service")
					|| punTypeMaster.getName().equalsIgnoreCase("Drop")) {
				punishmentTypeMasterLists.add(punTypeMaster);
			}
		}
		return punishmentTypeMasterLists;

	}

	// public void clearDisciplinaryListData() {
	// log.info("===================disciplinaryActionbean.clearDisciplinaryListData
	// started===============================================");
	//
	// selectedComplaintRegisterListRespDTO = null;
	// editButtonFlag = false;
	// deleteButtonFlag = false;
	// addButtonFlag = true;
	// viewButtonFlag = false;
	// log.info("<===== Ends disciplinaryActionBean.clearDisciplinaryListData
	// ======>");
	// }

	public void getDownloadFile(String path) {
		log.info(
				"===================disciplinaryActionbean.getDownloadFile started===============================================");

		try {

			byte[] downloadName = null;
			try {
				File file = new File(path);

				downloadName = new byte[(int) file.length()];

				log.info("downloadName==>" + downloadName);

				FileInputStream fis = new FileInputStream(file);
				fis.read(downloadName); // read file into bytes[]
				fis.close();
			} catch (Exception e) {
				log.error("convertImage error==>" + e);
			}
			/*
			 * log.info(" Value :: " + value); if (value == null || value == null) {
			 * errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode()); return;
			 * }
			 */
			if (path.contains("uploaded")) {
				String filePathValue[] = path.split("uploaded/");
				fileName = filePathValue[1];
			}
			log.info(" FIle Name :: " + fileName);
			String ext = FilenameUtils.getExtension(fileName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
				downloadFile = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "image/jpg",
						fileName);
				// file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName),
				// "image/jpg", fileName);
			} else if (ext.contains("pdf")) {
				downloadFile = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "pdf", fileName);
			} else if (ext.contains("doc")) {
				downloadFile = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			} else if (ext.contains("docx")) {
				downloadFile = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "docx", fileName);
			}
			log.info(
					"===================disciplinaryActionbean.getDownloadFile ends===============================================");

			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded file", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info(
					"===================disciplinaryActionbean.getDownloadFile ends===============================================");

			return;
		}

	}

	public String getDateFormat(Date d) {
		log.info("===================disciplinaryActionbean.getDateFormat started====================");
		String dateStr = null;
		if (d != null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
			dateStr = simpleDateFormat.format(d);
		}
		return dateStr;
	}

	public StreamedContent getDownloadFile() {
		return downloadFile;
	}

	public void updateForwardToList() {
		log.info("empComplaintReviewCommentsSaveDTO", empComplaintReviewCommentsSaveDTO);
	}

	/**
	 * Creating the onManagerReviewSubmit
	 */
	public String onManagerReviewSubmit() {
		log.info(
				"===================disciplinaryActionbean.onManagerReviewSubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		if (empComplaintReviewCommentsSaveDTO.getFinalApproval() != null) {
			empComplaintReviewCommentsSaveDTO
					.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_APPROVED));
		} else if (selectedMenuDecsEnq.equalsIgnoreCase("type_assign")) {
			empComplaintReviewCommentsSaveDTO
					.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_COMPLETED));
			empComplaintReviewCommentsSaveDTO.setPunishmentTypeId(null);
			if (userMaster != null && userMaster.getId() != null) {
				empComplaintReviewCommentsSaveDTO.setForwardToUserId(userMaster.getId());
			}
		} else {
			empComplaintReviewCommentsSaveDTO
					.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED));
		}
		empComplaintReviewCommentsSaveDTO
				.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_COMPLETED));
		// empComplaintRegisterSaveDTO.setLogRemarks("log remarks hardcoded");
		empComplaintReviewCommentsSaveDTO.setNote(managerNoteText);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		if (StringUtils.isNotEmpty(managerRemarks)) {
			empComplaintReviewCommentsSaveDTO.setLogRemarks(managerRemarks);
		}
		if (StringUtils.isNotEmpty(complainDetail)) {
			empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
		}
		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();

		log.info("<===== Ends disciplinaryActionbean.onManagerReviewSubmit ======>");
		return showDisciplinaryListPage();

	}

	public String onPriliminaryEnquirySubmit() {
		log.info(
				"===================disciplinaryActionbean.onPriliminaryEnquirySubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO
				.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.PRELIMINARY_ENQUIRY_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setLogRemarks(explanation);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO
				.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.PRELIMINARY_ENQUIRY_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(null);

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onPriliminaryEnquirySubmit ======>");
		return showDisciplinaryListPage();
	}

	public String onEmployeeExplanationSubmit() {
		log.info(
				"===================disciplinaryActionbean.onEmployeeExplanationSubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO.setLogStage(
				String.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_5_COMPLETED));
		// empComplaintReviewCommentsSaveDTO.setLogRemarks(explanation);
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(managerID);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO.setComplaintStatus(
				String.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_5_COMPLETED));

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onEmployeeExplanationSubmit ======>");
		return showDisciplinaryListPage();
	}

	public String onManagerReviewSubmitReject() {
		log.info(
				"===================disciplinaryActionbean.onManagerReviewSubmitReject started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO
				.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_REJECTED));
		empComplaintReviewCommentsSaveDTO.setNote(managerNoteText);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO.setLogRemarks(managerRemarksReject);
		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		empComplaintReviewCommentsSaveDTO
				.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_REJECTED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(selectedComplaintRegisterListRespDTO.getCreatedByUserId());
		// empComplaintReviewCommentsSaveDTO.setForwardToUserId(viewDisciplinaryActionComplaintDetailsResponseDTO.getLatestForwardToUserId());
		onManagerReviewSubmitApprove();

		log.info("<===== Ends disciplinaryActionbean.onManagerReviewSubmitReject ======>");

		return showDisciplinaryListPage();

	}

	public String onDomesticEnquirySubmit() {
		log.info(
				"===================disciplinaryActionbean.onDomesticEnquirySubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO
				.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRY_OFFICER_ENQUIRY_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO.setComplaintStatus(
				String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRY_OFFICER_ENQUIRY_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(managerID);

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onDomesticEnquirySubmit ======>");
		return showDisciplinaryListPage();
	}

	public String onFinalManagerReviewSubmit() {
		log.info(
				"===================disciplinaryActionbean.onFinalManagerReviewSubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO
				.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_8_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO
				.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_8_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(empComplaintRegisterData.getEmpMaster().getUserId());

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onFinalManagerReviewSubmit ======>");
		return showDisciplinaryListPage();
	}

	public String onEmployeeExplanationViewsSubmit() {
		log.info(
				"===================disciplinaryActionbean.onEmployeeExplanationViewsSubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO.setLogStage(
				String.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_9_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO.setComplaintStatus(
				String.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_9_COMPLETED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(managerID);

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onEmployeeExplanationViewsSubmit ======>");
		return showDisciplinaryListPage();
	}

	public String onEmpExplanationAgainstNotice() {
		log.info(
				"===================disciplinaryActionbean.onEmpExplanationAgainstNotice started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO.setLogStage(String.valueOf(
				DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_12_COMPLETED_AGAINST_NOTICE));
		empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO.setComplaintStatus(String.valueOf(
				DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_12_COMPLETED_AGAINST_NOTICE));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(managerID);

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onEmpExplanationAgainstNotice ======>");
		return showDisciplinaryListPage();
	}

	public String onShowCauseNoticeSubmit() {
		log.info(
				"===================disciplinaryActionbean.onShowCauseNoticeSubmit started===============================================");

		empComplaintReviewCommentsSaveDTO.setUserId(actionTakenByUserId);
		empComplaintReviewCommentsSaveDTO
				.setLogStage(String.valueOf(DisciplinaryActionComplaintStatus.SHOW_CAUSE_NOTICE_ISSUED));
		empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
		empComplaintReviewCommentsSaveDTO
				.setComplaintStatus(String.valueOf(DisciplinaryActionComplaintStatus.SHOW_CAUSE_NOTICE_ISSUED));
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(empComplaintRegisterData.getEmpMaster().getUserId());

		log.info("Employee complaint register " + empComplaintReviewCommentsSaveDTO);
		onManagerReviewSubmitApprove();
		log.info("<===== Ends disciplinaryActionbean.onShowCauseNoticeSubmit ======>");
		return showDisciplinaryListPage();
	}

	public void onCancelMangerReviewNote() {
		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewNote started===============================================");

		managerNoteText = null;

		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewNote Ends===============================================");

	}

	public String onCancelMangerReviewPage() {
		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewPage started===============================================");

		managerNoteText = null;
		actionTakenByUserId = null;
		managerRemarksReject = null;
		managerRemarks = null;
		empComplaintReviewCommentsSaveDTO.setForwardToUserId(null);
		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewPage Ends===============================================");

		return showDisciplinaryListPage();

	}

	public void onCancelMangerReviewSubmit() {
		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewNote started ==============================================");

		managerRemarksReject = null;
		managerRemarks = null;

		log.info(
				"===================disciplinaryActionbean.onCancelMangerReviewNote hoi ===============================================");

	}

	public void onManagerReviewSubmitApprove() {
		log.info(
				"===================disciplinaryActionbean.onManagerReviewSubmitApprove started===============================================");

		try {

			BaseDTO response = new BaseDTO();

			if (userMaster != null && userMaster.getId() != null) {
				empComplaintReviewCommentsSaveDTO.setForwardToUserId(userMaster.getId());
			}

			String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/createcomplaintreviewcomments";

			response = httpService.post(Url, empComplaintReviewCommentsSaveDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MANAGE_REVIEW_APPROVED).getCode());
				}

				else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_2_REJECTED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MANAGE_REVIEW_REJECTED).getCode());
				}

				else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.PRELIMINARY_ENQUIRY_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.PRELIMINARY_ENQUIRY_SUCCESS).getCode());
				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus().equals(String
						.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_5_COMPLETED))) {
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.EXPLANATION_FROM_EMPLOYEE_SUCCESS).getCode());
				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_4_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MANAGER_REVIEW_SUCCESS).getCode());
				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRE_OFFICER_ASSIGNED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.ASSIGN_ENQUIRY_OFFICER_SUCCESS).getCode());
				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.ENQUIRY_OFFICER_ENQUIRY_COMPLETED))) {
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.DOMESTIC_ENQUIRY_REPORT_SUCCESS).getCode());

				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_8_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MANAGER_REVIEW_SUCCESS).getCode());
				}

				else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus().equals(String
						.valueOf(DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_9_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.EMPLOYEE_VIEWS_SUCCESS).getCode());
				}

				else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.SHOW_CAUSE_NOTICE_ISSUED))) {
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.SHOW_CAUSE_NOTICE_ISSUE_SUCCESS).getCode());
				}

				else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus().equals(String.valueOf(
						DisciplinaryActionComplaintStatus.EXPLANATION_FROM_EMPLOYEE_STAGE_12_COMPLETED_AGAINST_NOTICE))) {
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.EXPLANATION_AGAINST_NOTICE_SUCCESS).getCode());

				} else if (empComplaintReviewCommentsSaveDTO.getComplaintStatus()
						.equals(String.valueOf(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED))) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.DECISION_SUCCESS).getCode());
				} else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
				}
			}

			else if (response == null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}

		} catch (Exception e) {
			log.info("Exception occured while submitting........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("openApproveDialog called....");
		RequestContext.getCurrentInstance().execute("PF('rejectcmd').hide();");
		RequestContext.getCurrentInstance().execute("PF('approvecmd').hide()");
		log.info("<===== Ends disciplinaryActionbean.onManagerReviewSubmitApprove ======>");

	}

	public void openRejectDialog() {
		log.info(
				"===================disciplinaryActionbean.openRejectDialog started===============================================");

		log.info("openApproveDialog called....");
		RequestContext.getCurrentInstance().execute("PF('rejectcmd').show()");
		log.info(
				"===================disciplinaryActionbean.openRejectDialog ends===============================================");

	}

	public void openApproveDialog() {
		log.info(
				"===================disciplinaryActionbean.openApproveDialog started===============================================");

		log.info("openApproveDialog called....");
		RequestContext.getCurrentInstance().execute("PF('approvecmd').show()");
		log.info(
				"===================disciplinaryActionbean.openApproveDialog Ends===============================================");

	}

	public String onCancelPriliminaryAction() {
		log.info(
				"===================disciplinaryActionbean.onCancelPriliminaryAction started===============================================");

		fileType = null;
		filePath = null;
		explanation = null;
		forwardToUserId = null;
		log.info(
				"===================disciplinaryActionbean.onCancelPriliminaryAction ends===============================================");

		return showDisciplinaryListPage();

	}

	public String onCreateManagerReviewSecStage(String pageType) {
		log.info("onCreateManagerReviewSecStage starts----------" + pageType);
		try {
			EmpComplaintReviewCommentsSaveDTO empComplaintReviewCommentsSaveDTO = new EmpComplaintReviewCommentsSaveDTO();

			if (pageType == null) {
				pageType = "";
			}

			if ("DECISION".equals(pageType)) {
				if (loginBean.getUserMaster() != null) {
					empComplaintReviewCommentsSaveDTO.setForwardToUserId(loginBean.getUserMaster().getId());
				}
			} else {
				if (forwardToUserId != null) {
					empComplaintReviewCommentsSaveDTO.setForwardToUserId(forwardToUserId);

				} else {
					if ("type_punish".equals(selectedMenuDecsEnq)) {
						empComplaintReviewCommentsSaveDTO
								.setForwardToUserId(userMaster != null ? userMaster.getId() : null);
					}
				}
				/*
				 * * else {
				 * empComplaintReviewCommentsSaveDTO.setForwardToUserId(selectedEmployeeMaster.
				 * getUserId()); }
				 */
			}

			empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
			empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());

			EmpComplaintRegisterManagerCommentsSaveDTO empComplaintRegisterManagerCommentsSaveDTO = new EmpComplaintRegisterManagerCommentsSaveDTO();
			empComplaintRegisterManagerCommentsSaveDTO
					.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
			empComplaintRegisterManagerCommentsSaveDTO.setDecisionOrEnquiry(selectedMenuDecsEnq);
			if (punishmentTypeMaster != null) {
				empComplaintRegisterManagerCommentsSaveDTO.setPunishmentTypeMasterId(punishmentTypeMaster.getId());
			}
			empComplaintRegisterManagerCommentsSaveDTO.setFromDate(selectedFromDate);
			empComplaintRegisterManagerCommentsSaveDTO.setToDate(selectedToDate);
			empComplaintRegisterManagerCommentsSaveDTO.setEffectiveDate(selectedEffectiveDate);
			log.info("effective date------------" + selectedEffectiveDate);
			empComplaintRegisterManagerCommentsSaveDTO.setEnquiryType(selectedEnquiryType);
			empComplaintRegisterManagerCommentsSaveDTO.setEnquiryOfficerId(selectedEnquiryOfficerId);
			empComplaintRegisterManagerCommentsSaveDTO.setFineAmount(fineAmount);

			// empComplaintRegisterManagerCommentsSaveDTO.setNoOfInstallment(dueNumber);
			if (StringUtils.isNotEmpty(selectedMenuDecsEnq)) {
				if (selectedMenuDecsEnq.equals("type_assign")) {
					if (selectedEnquiryType != null) {
						if (selectedEnquiryType.equals("internal")) {
							if (selectedDepartment != null && selectedDepartment.getId() != null) {
								empComplaintRegisterManagerCommentsSaveDTO.setDepartmentId(selectedDepartment.getId());
							}
							empComplaintRegisterManagerCommentsSaveDTO.setEmpMasterId(selectedEmployeeMaster.getId());
							empComplaintRegisterManagerCommentsSaveDTO.setEntityMasterId(selectedEntityMaster.getId());
							if (selectedSectionMaster != null && selectedSectionMaster.getId() != null) {
								empComplaintRegisterManagerCommentsSaveDTO
										.setSectionMasterId(selectedSectionMaster.getId());
							}
						}
					}
				}
			} else {
				empComplaintRegisterManagerCommentsSaveDTO.setDecisionOrEnquiry("type_punish");
			}
			empComplaintRegisterManagerCommentsSaveDTO
					.setEmpComplaintReviewCommentsSaveDTO(empComplaintReviewCommentsSaveDTO);

			if (StringUtils.isNotEmpty(pageType)) {
				if ("MANAGER_REVIEW_TWO".equals(pageType)) {
					empComplaintRegisterManagerCommentsSaveDTO
							.setStage(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_10_COMPLETED.toString());
				} else if ("MANAGER_REVIEW".equals(pageType)) {
					empComplaintRegisterManagerCommentsSaveDTO
							.setStage(DisciplinaryActionComplaintStatus.MANAGER_REVIEW_STAGE_4_COMPLETED.toString());
				} else if ("DECISION".equals(pageType)) {
					empComplaintRegisterManagerCommentsSaveDTO
							.setStage(DisciplinaryActionComplaintStatus.DECISION_MAKING_COMPLETED.toString());
				}
			}

			BaseDTO response = new BaseDTO();
			String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/createmanagerreviewcomments";

			response = httpService.post(Url, empComplaintRegisterManagerCommentsSaveDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (pageType.equalsIgnoreCase("DECISION")) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.DECISION_SUCCESS).getCode());
				} else if (pageType.equalsIgnoreCase("MANAGER_REVIEW")
						|| pageType.equalsIgnoreCase("MANAGER_REVIEW_TWO")) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MANAGER_REVIEW_SUCCESS).getCode());
				} else {
					errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getCode());
				}
				if (response.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					ComplaintManagerReview complaintManagerReview = mapper.readValue(jsonResponse,
							ComplaintManagerReview.class);
					// -- to save the penalty recovery schedule
					if (complaintManagerReview != null && complaintManagerReview.getPunishmentTypeMaster() != null) {
						if (complaintManagerReview.getPunishmentTypeMaster().getName().equalsIgnoreCase("Fine")) {
							savePenaltyRecoverySchedule(complaintManagerReview);
						}
					}

				}
				return showDisciplinaryListPage();
			} else {
				errorMap.notify(response.getStatusCode());
			}

		} catch (Exception e) {
			log.info("Exception occured while submitting........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		return null;
	}

	public String onCreateAssignEnquiryOfficer() {

		try {
			EmpComplaintReviewCommentsSaveDTO empComplaintReviewCommentsSaveDTO = new EmpComplaintReviewCommentsSaveDTO();

			if (selectedEnquiryType != null) {
				if (selectedEnquiryType.equals("external")) {
					if (userMaster != null && userMaster.getId() != null) {
						empComplaintReviewCommentsSaveDTO.setForwardToUserId(userMaster.getId());
					} else {
						AppUtil.addWarn("Forward for is required");
						return null;
					}
				} else if (selectedEnquiryType.equals("internal")) {
					log.info("selectedEmployeeMaster --------------" + selectedEmployeeMaster.toString());
					if (selectedEmployeeMaster != null && selectedEmployeeMaster.getUserId() != null) {
						empComplaintReviewCommentsSaveDTO.setForwardToUserId(selectedEmployeeMaster.getUserId());
					} else {
						log.info("selected employee userid is emptty-----");
						AppUtil.addWarn("Selected employee login details not found");
						return null;
					}
				}
			}

			empComplaintReviewCommentsSaveDTO.setLogRemarks(complainDetail);
			empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());

			EmpComplaintRegisterManagerCommentsSaveDTO empComplaintRegisterManagerCommentsSaveDTO = new EmpComplaintRegisterManagerCommentsSaveDTO();
			empComplaintRegisterManagerCommentsSaveDTO
					.setUserId(empComplaintReviewCommentsSaveDTO.getForwardToUserId());
			empComplaintRegisterManagerCommentsSaveDTO
					.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
			empComplaintRegisterManagerCommentsSaveDTO.setEnquiryType(selectedEnquiryType);
			empComplaintRegisterManagerCommentsSaveDTO.setEnquiryOfficerId(selectedEnquiryOfficerId);
			if (selectedEnquiryType.equals("internal")) {
				if (selectedDepartment != null && selectedDepartment.getId() != null) {
					empComplaintRegisterManagerCommentsSaveDTO.setDepartmentId(selectedDepartment.getId());
				}
				empComplaintRegisterManagerCommentsSaveDTO.setEmpMasterId(selectedEmployeeMaster.getId());
				empComplaintRegisterManagerCommentsSaveDTO.setEntityMasterId(selectedEntityMaster.getId());
				if (selectedSectionMaster != null && selectedSectionMaster.getId() != null) {
					empComplaintRegisterManagerCommentsSaveDTO.setSectionMasterId(selectedSectionMaster.getId());
				}
			}
			empComplaintRegisterManagerCommentsSaveDTO
					.setEmpComplaintReviewCommentsSaveDTO(empComplaintReviewCommentsSaveDTO);

			BaseDTO response = new BaseDTO();

			String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/createassignenquiryofficer";

			response = httpService.post(Url, empComplaintRegisterManagerCommentsSaveDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (response.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.ASSIGN_ENQUIRY_OFFICER_SUCCESS).getCode());
				}
			} else {
				errorMap.notify(response.getStatusCode());
			}

		} catch (Exception e) {
			log.info("Exception occured while submitting........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		return showDisciplinaryListPage();
	}

	/**
	 * purpose : to save the penalty recovery schedule
	 * 
	 * @author krishnakumar
	 * @param complaintManagerReview
	 */
	public void savePenaltyRecoverySchedule(ComplaintManagerReview complaintManagerReview) {
		log.info("======disciplinaryActionbean.savePenaltyRecoverySchedule started =========");
		try {
			List<PenaltyRecoverySchedule> scheduleList = new ArrayList<>();
			Map<String, Object> noOfDue = calculateDue();
			if (noOfDue != null) {
				log.info("noOfDue==> " + noOfDue);
				Long due = (Long) noOfDue.get("noOfDue");
				log.info("totalDue==> " + due);
				Double calculateTotal = 0.0;
				Calendar yearCalendar = Calendar.getInstance();
				Integer year = 0;
				String month = null;
				for (int numDue = 1; numDue <= due; numDue++) {

					PenaltyRecoverySchedule penaltyRecoverySchedule = new PenaltyRecoverySchedule();

					if (month != null && month.equalsIgnoreCase("December")) {
						yearCalendar.add(Calendar.YEAR, 1);
					}
					// --to get current year
					year = yearCalendar.get(Calendar.YEAR);
					log.info("DisciplinaryActionBean :: year=> " + year);
					penaltyRecoverySchedule.setYear(year.longValue());

					// --to get next month
					Calendar currentMonth = Calendar.getInstance();
					SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
					currentMonth.add(Calendar.MONTH, numDue);
					month = dateFormat.format(currentMonth.getTime());
					log.info("DisciplinaryActionBean :: month==> " + month);
					penaltyRecoverySchedule.setMonth(month);

					penaltyRecoverySchedule.setComplaintManagerReview(complaintManagerReview);
					if (numDue != due) {
						calculateTotal = calculateTotal + (Double) noOfDue.get("percentageSalary");
						log.info("calculateTotal==>" + calculateTotal);
						penaltyRecoverySchedule.setRecovedAmount((Double) noOfDue.get("percentageSalary"));
					} else {
						Double balance = fineAmount - calculateTotal;
						log.info("calculateTotal==> " + calculateTotal);
						log.info("finalAmount==> " + noOfDue.get("percentageSalary"));
						log.info("balance==> " + balance);
						penaltyRecoverySchedule.setRecovedAmount(balance);
					}
					scheduleList.add(penaltyRecoverySchedule);
				}
				log.info("scheduleList==>" + scheduleList);

				String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/penaltyrecovery/create";
				log.info("savePenaltyRecoverySchedule url==>" + url);

				BaseDTO baseDTO = httpService.post(url, scheduleList);

				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
			}

		} catch (Exception e) {
			log.info("Exception occured while savePenaltyRecoverySchedule........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
	}

	/**
	 * purpose : to calculate 70% salary, total due and net salary
	 * 
	 * @author krishnakumar
	 * @return
	 */
	@SuppressWarnings("unused")
	public Map<String, Object> calculateDue() {
		log.info("======disciplinaryActionbean.calDue started =========");
		Long noOfDue = null;
		Map<String, Object> dueMap = new HashMap<>();
		try {
			Integer penaltyPercentage = Integer.parseInt(commonDataService.getAppKeyValue("PENALTY_PERCENTAGE"));

			if (penaltyPercentage == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PENALTY_PERCENTAGE_NOT_FOUND).getCode());
				return null;
			}
			Double netEmpSalary = getEmployeePreviousMonthSalary();
			if (netEmpSalary == null || netEmpSalary == 0.0) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_NET_SALARY_NOT_FOUND).getCode());
				return null;
			}
			Double percentAgeSalary = (netEmpSalary * penaltyPercentage) / 100;
			log.info("fineAmount==> " + fineAmount);
			log.info("netEmpSalary==> " + netEmpSalary);
			log.info("percentAgeSalary==> " + percentAgeSalary);
			Double due = fineAmount / percentAgeSalary;
			log.info("due==> " + due);
			noOfDue = Math.round(due);
			log.info("noOfDue==> " + noOfDue);
			if (noOfDue == 0) {
				noOfDue = 1L;
			}
			dueMap.put("fineAmount", fineAmount);
			dueMap.put("netSalary", netEmpSalary);
			dueMap.put("percentageSalary", percentAgeSalary);
			dueMap.put("noOfDue", noOfDue);

		} catch (Exception e) {
			log.info("Exception occured while calculateDue........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return dueMap;
	}

	/**
	 * purpose : to calculate the employee net salary as on previous month
	 * 
	 * @author krishnakumar
	 * @param null
	 * @return null
	 * 
	 * 
	 *         1. Take current month
	 * 
	 *         2. Take employee id and take previous month salary, if not found,
	 *         salary not found exception will be thrown
	 * 
	 *         3. This is done assuming at least one previous month salary will be
	 *         available. *
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Double getEmployeePreviousMonthSalary() {
		log.info("======disciplinaryActionbean.getEmployeePreviousMonthSalary started =========");

		Double netSalary = null;
		try {
			// --to get next month
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
			calendar.add(Calendar.MONTH, -1);
			String previousMonth = dateFormat.format(calendar.getTime());

			log.info("getEmployeePreviousMonthSalary :: previousMonth ==> " + previousMonth);

			Long complaintRegId = selectedComplaintRegisterListRespDTO.getId();
			log.info("getEmployeePreviousMonthSalary :: complaintRegId==> " + complaintRegId);
			EmployeeMaster empMaster = getEmployeeByComplaintId(complaintRegId);
			if (empMaster == null || empMaster.getId() == null) {
				errorMap.notify(ErrorDescription.ERROR_EMPLOYEE_MASTER_DOES_NOT_EXISTS.getErrorCode());
				return null;
			}
			log.info("getEmployeePreviousMonthSalary :: empId==> " + empMaster.getId());

			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/getempsalary/" + previousMonth + "/" + empMaster.getId();

			log.info("getEmployeePreviousMonthSalary :: url==>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info("status code==>" + baseDTO.getStatusCode());
			if (baseDTO.getStatusCode() == 0) {

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				List<SalaryDTO> salaryDetailList = (List<SalaryDTO>) mapper.readValue(jsonValue,
						new TypeReference<List<SalaryDTO>>() {
						});
				salaryDTO = salaryDetailList.get(0);
				log.info("salaryDTO ===>" + salaryDTO);
				netSalary = salaryDTO.getNetSalary();
			}

		} catch (Exception e) {
			log.info("Exception occured while getEmployeePreviousMonthSalary........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return netSalary;
	}

	/**
	 * purpose : to get employee by complaint id
	 * 
	 * @author krishnakumar
	 * @param complaintRegId
	 * @return Employee Master
	 */
	public EmployeeMaster getEmployeeByComplaintId(Long complaintRegId) {
		log.info("======disciplinaryActionbean.getEmployeePreviousMonthSalary started =========");
		EmployeeMaster empMaster = null;
		try {
			if (complaintRegId == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.COMPLAINT_ID_NOT_FOUND).getCode());
				return null;
			}
			log.info("getEmployeeByComplaintId :: complaintRegId==> " + complaintRegId);
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/getempbycomplaintid/" + complaintRegId;

			log.info("getEmployeeByComplaintId :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info("status code==>" + baseDTO.getStatusCode());
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empMaster = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
				});
				log.info("empMaster ===>" + empMaster.getId());
			}

		} catch (Exception e) {
			log.info("Exception occured while getEmployeeByComplaintId........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return empMaster;
	}

	public String onCancelDomesticEnquiryReport() {
		log.info(
				"===================disciplinaryActionbean.onCancelDomesticEnquiryReport started ==============================================");

		complainDetail = null;
		fileType = null;
		log.info(
				"===================disciplinaryActionbean.onCancelDomesticEnquiryReport Ends ===============================================");

		return showDisciplinaryListPage();

	}

	public void checking() {
		log.info(
				"===================disciplinaryActionbean.checking started ==============================================",
				managerRemarks + managerRemarksReject);

	}

	public String onDeleteDisciplinaryList() {

		log.info(
				"===================disciplinaryActionbean.onDeleteDisciplinaryList started ==============================================");

		try {

			BaseDTO response = new BaseDTO();

			String Url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/delete/" + selectedComplaintRegisterListRespDTO.getId();

			response = httpService.get(Url);
			if (response != null) {
				if (response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.DISCIPLINARY_ACTION_DELETE_SUCCESS)
							.getErrorCode());
				} else if (response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.info("Exception occured while submitting........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info(
				"===================disciplinaryActionbean.onDeleteDisciplinaryList Ends ===============================================");
		return showDisciplinaryListPage();
	}

	/**
	 * Purpose : to View the Penalty Schedule
	 * 
	 * @author krishnakumar
	 * @return
	 */
	public String viewPenaltySchedule() {
		log.info("<=disciplinaryActionbean :: viewPenaltySchedule=>");
		try {
			if (selectedComplaintRegisterListRespDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("emp_complaint_reg id==> " + selectedComplaintRegisterListRespDTO.getId());
			// --to get the employee net Salary
			getEmployeePreviousMonthSalary();
			// --to get the emp panlty amount
			loadEmployeePenalty(selectedComplaintRegisterListRespDTO.getId());
			// --to get employee fine amount details
			getEmployeeFineAmountDetails(selectedComplaintRegisterListRespDTO.getId());
		} catch (Exception e) {
			log.error("Exception Occured in disciplinaryActionbean viewPenaltySchedule ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/admin/raisecomplaint/viewPanaltySchedule.xhtml?faces-redirect=true";
	}

	/**
	 * Purpose : to get employee penalty amount
	 * 
	 * @author krishnakumar
	 * @return
	 */

	public void loadEmployeePenalty(Long empComplaintId) {
		log.info("<=disciplinaryActionbean :: loadEmployeePenalty=>");
		try {
			log.info("loadEmployeePenalty :: empComplaintId==> " + empComplaintId);
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/getemppenlty/" + empComplaintId;

			log.info("loadEmployeePenalty :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			log.info("status code==>" + baseDTO.getStatusCode());
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				fineAmount = mapper.readValue(jsonResponse, new TypeReference<Double>() {
				});
				log.info("loadEmployeePenalty fineAmount ===> " + fineAmount);
			}

		} catch (Exception e) {
			log.info("Exception occured while loadEmployeePenalty........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

	}

	/**
	 * Purpose : to get employee fine amount details
	 * 
	 * @author krishnakumar
	 * @param empComplaintId
	 */
	public void getEmployeeFineAmountDetails(Long empComplaintId) {
		log.info("<=disciplinaryActionbean :: getEmployeeFineAmountDetails=>");
		try {
			log.info("loadEmployeePenalty :: empComplaintId==>" + empComplaintId);

			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/penaltyrecovery/getempfinedetails/" + empComplaintId;
			log.info("getEmployeeFineAmountDetails :: url==>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			Integer penaltySchedule = 0;
			log.info("status code==>" + baseDTO.getStatusCode());
			Long penaltyPercentage = Long.parseLong(commonDataService.getAppKeyValue("PENALTY_PERCENTAGE"));
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				penaltyDetailsDTOList = mapper.readValue(jsonResponse, new TypeReference<List<PenaltyDetailsDTO>>() {
				});
				Double totalRecovedAmount = 0.0;
				for (PenaltyDetailsDTO penaltyDetailsDTO : penaltyDetailsDTOList) {
					Double fineAmount = penaltyDetailsDTO.getFineAmount();
					Double recovedAmount = penaltyDetailsDTO.getRecovedAmount();
					totalRecovedAmount = totalRecovedAmount + recovedAmount;
					Double balanceAmount = fineAmount - totalRecovedAmount;

					log.info("getEmployeeFineAmountDetails fineAmount==> " + fineAmount);
					log.info("getEmployeeFineAmountDetails recovedAmount==> " + recovedAmount);
					log.info("getEmployeeFineAmountDetails totalRecovedAmount==> " + totalRecovedAmount);
					log.info("getEmployeeFineAmountDetails balanceAmount==> " + balanceAmount);
					penaltySchedule = penaltySchedule + 1;
					penaltyDetailsDTO.setPenaltySchedule(penaltySchedule);

					penaltyDetailsDTO.setDeductions(penaltyPercentage);
					penaltyDetailsDTO.setBalanceAmount(balanceAmount);
				}
				log.info("getEmployeeFineAmountDetails penaltyDetailsDTOList ===> " + penaltyDetailsDTOList);
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.info("Exception occured while getEmployeeByComplaintId........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
	}

	public String createExtendSuspension() {
		log.info("=====START DisciplinaryActionBean.createExtendSuspension=====");
		try {
			userId = null;
			note = null;
			employeeSuspensionDetails = new EmployeeSuspensionDetails();
			userMasters = commonDataService.loadForwardToUsersByFeature("DISCIPLINARY_ACTION");
			String url = AppUtil.getPortalServerURL() + "/employee/getemployeedetailsbyid/"
					+ selectedComplaintRegisterListRespDTO.getEmployeeId();
			log.info("Get Employee By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employee = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
				});
				log.info("=====Employee Master is=====" + employee);
				log.info("loadEmployeePenalty :: empComplaintId==> " + selectedComplaintRegisterListRespDTO.getId());

			}
			suspensionDetails();
			if (employeeMaster == null) {
				employeeMaster = new EmployeeMaster();
				employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			}
		} catch (Exception e) {
			log.info("=====Exception Occured in DisciplinaryActionBean.createExtendSuspension=====");
			log.info("=====Exception is=====" + e);
		}
		log.info("=====END DisciplinaryActionBean.createExtendSuspension=====");
		return CREATE_SUSPENSION_EXTENSION;
	}

	private void suspensionDetails() {
		try {
			String complaintUrl = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/getcomplaintsdetails/" + selectedComplaintRegisterListRespDTO.getId();
			log.info("load Employee Complaint Data :: url==> " + complaintUrl);
			BaseDTO complaintBaseDTO = httpService.get(complaintUrl);
			if (complaintBaseDTO != null && complaintBaseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(complaintBaseDTO.getResponseContent());
				empDisiplinaryActionViewDto = mapper.readValue(jsonResponse,
						new TypeReference<EmpDisiplinaryActionViewDto>() {
						});
				log.info("=====empComplaintReviewCommentsSaveDTO is=====" + empDisiplinaryActionViewDto.toString());
			}
		} catch (Exception e) {
			log.info("=====Exception Occured in DisciplinaryActionBean.createExtendSuspension=====");
			log.info("=====Exception is=====" + e);
		}

	}

	public String saveEmployeeSuspensionDetails() {
		log.info("=====START DisciplinaryActionBean.saveEmployeeSuspensionDetails=====");
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/empcomplaintregister/saveemployeesuspension";
			log.info("load Employee Complaint Data :: url==> " + url);
			EmployeeMaster employeeMaster = new EmployeeMaster();
			employeeMaster.setId(employee.getId());
			employeeMaster.setPersonalInfoEmployment(employee.getPersonalInfoEmployment());
			empComplaintReviewCommentsSaveDTO.setEmpComplaintRegisterId(selectedComplaintRegisterListRespDTO.getId());
			employeeSuspensionDetails.setSuspensionEffectiveDate(employeeSuspensionDetails.getExtensionFrom());
			employeeSuspensionDetails.setSuspensionReferenceDate(new Date());
			employeeSuspensionDetails.setFromDate(employeeSuspensionDetails.getExtensionFrom());
			empComplaintReviewCommentsSaveDTO.setEmployeeSuspensionDetails(employeeSuspensionDetails);
			empComplaintReviewCommentsSaveDTO.setEmployeeMaster(employeeMaster);
			EmployeeSuspensionDetailsNote employeeSuspensionDetailsNote = new EmployeeSuspensionDetailsNote();
			employeeSuspensionDetailsNote.setUserMaster(userMaster);
			employeeSuspensionDetailsNote.setFinalApproval(finalApproval);
			employeeSuspensionDetailsNote.setNote(note);
			empComplaintReviewCommentsSaveDTO.setEmployeeSuspensionDetailsNote(employeeSuspensionDetailsNote);
			BaseDTO baseDTO = httpService.post(url, empComplaintReviewCommentsSaveDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				AppUtil.addInfo("Employee Suspension extended saved successfully");
				selectedComplaintRegisterListRespDTO = new EmpComplaintRegisterListResponseDTO();
				return DISCIPLINARY_ACTION_LIST_PAGE;
			}
		} catch (Exception e) {
			log.info("=====Exception Occured in DisciplinaryActionBean.saveEmployeeSuspensionDetails=====");
			log.info("=====Exception is=====" + e.getMessage());
		}
		log.info("=====END DisciplinaryActionBean.saveEmployeeSuspensionDetails=====");
		return null;
	}

	public String showSuspensionDetails() {
		log.info("=====START DisciplinaryActionBean.showSuspensionDetails=====");
		try {
			selectedSuspensionDetailsResponseDto = null;
			loadLazySuspensionDetailsyList();
		} catch (Exception e) {
			log.info("=====Exception Occured in DisciplinaryActionBean.showSuspensionDetails=====");
			log.info("=====Exception is=====" + e.getMessage());
		}
		log.info("=====END DisciplinaryActionBean.showSuspensionDetails=====");
		return SUSPENSION_DETAILS_LIST_PAGE;
	}

	public void loadLazySuspensionDetailsyList() {
		log.info("<===== Starts DisciplinaryActionBean.loadLazySuspensionDetailsResponseDtoList ======>");
		lazySuspensionDetailsResponseDtoList = new LazyDataModel<SuspensionDetailsResponseDto>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<SuspensionDetailsResponseDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
							+ "/empcomplaintregister/getallsuspensiondetailslist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						suspensionDetailsResponseDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<SuspensionDetailsResponseDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazySuspensionDetailsResponseDtoList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return suspensionDetailsResponseDtoList;
			}

			@Override
			public Object getRowKey(SuspensionDetailsResponseDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SuspensionDetailsResponseDto getRowData(String rowKey) {
				try {
					for (SuspensionDetailsResponseDto suspensionDetailsResponseDto : suspensionDetailsResponseDtoList) {
						if (suspensionDetailsResponseDto.getId().equals(Long.valueOf(rowKey))) {
							selectedSuspensionDetailsResponseDto = suspensionDetailsResponseDto;
							return suspensionDetailsResponseDto;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends DisciplinaryActionBean.loadLazySuspensionDetailsResponseDtoList ======>");
	}

	public String viewStatus() {
		log.info("<===Starts DisciplinaryActionBean.viewStatus ========>");
		try {

			if (selectedSuspensionDetailsResponseDto == null) {
				log.info("==============Selected Retirement Dto value is Empty===========");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			userMasters = commonDataService.loadForwardToUsersByFeature("DISCIPLINARY_ACTION");
			String url = SERVER_URL + "/suspensiondetails/getsuspensiondetails/"
					+ selectedSuspensionDetailsResponseDto.getId();
			log.info("<=EmpRetirementRequestBean :: viewStatus :: url=>" + url);
			viewNoteSuspensionEmployeeDetails = new ArrayList<>();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if(baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					suspensionDetailsResponseDto = mapper.readValue(jsonResponse,
							new TypeReference<SuspensionDetailsResponseDto>() {
							});
					log.info("<===DisciplinaryActionBean.viewStatus suspensionDetailsResponseDto is ========>"
							+ employeeSuspensionDetails.toString());
					if (baseDTO.getTotalListOfData() != null) {
						String jsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
						viewNoteSuspensionEmployeeDetails = mapper.readValue(jsonResponses,
								new TypeReference<List<EmpComplaintRegisterUpdateDTO>>() {
								});
					}
				}
			}
			if (suspensionDetailsResponseDto.getEmployeeSuspensionDetailsNote() != null
					&& loginBean.getUserDetailSession().getId().equals(
							suspensionDetailsResponseDto.getEmployeeSuspensionDetailsNote().getUserMaster().getId())) {
				buttonShowsCondition = true;
				approveButtonAction = true;
			}
			if (suspensionDetailsResponseDto.getEmployeeSuspensionDetailsNote() != null
					&& suspensionDetailsResponseDto.getEmployeeSuspensionDetailsNote().getFinalApproval() != null
					&& suspensionDetailsResponseDto.getEmployeeSuspensionDetailsNote().getFinalApproval()) {
				approveButtonAction = false;
			}
			if (suspensionDetailsResponseDto.getEmployeeSuspensionDetailsLog() != null && suspensionDetailsResponseDto
					.getEmployeeSuspensionDetailsLog().getStage().equals("FINAL_APPROVED")) {
				buttonShowsCondition = false;
			}
			suspensionDetailsResponseDto.setNotificationId(null);
			systemNotificationBean.loadTotalMessages();
		} catch (Exception e) {
			log.error(" Exception Occured While EmpRetirementRequestBean.viewStatus :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return SUSPENSION_DETAILS_VIEW_PAGE;
	}

	public String approveReject() {
		log.info("<===Starts DisciplinaryActionBean.approveReject ========>");
		suspensionDetailsResponseDto.setUserId(userMaster != null ? userMaster.getId() : null);
		if (suspensionDetailsResponseDto.getApproveReject().equals("APPROVE")) {
			if (suspensionDetailsResponseDto.getUserId() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (suspensionDetailsResponseDto.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
		}
		if (suspensionDetailsResponseDto.getApproveReject().equals("REJECT")) {
			if (suspensionDetailsResponseDto.getNote() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}
		}
		String url = SERVER_URL + "/suspensiondetails/approvereject";
		log.info("approveReject Url is==================>>>>>>>>>>>>" + url);
		suspensionDetailsResponseDto.setId(selectedSuspensionDetailsResponseDto.getId());
		// suspensionDetailsResponseDto.setEmpPromotionRequestDetailsList(empPromotionRequestDetailsList);
		BaseDTO response = httpService.post(url, suspensionDetailsResponseDto);
		if (response != null && response.getStatusCode() == 0) {
			if (suspensionDetailsResponseDto.getApproveReject().equals("REJECT")) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.EXTEND_SUSPENSION_REJECTED_SUCCESS).getErrorCode());
			} else {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.EXTEND_SUSPENSION_APPROVED_SUCCESS).getErrorCode());
			}
			clearSuspensionDetailPage();
			showSuspensionDetails();
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===End DisciplinaryActionBean.approveReject ========>");
		return SUSPENSION_DETAILS_LIST_PAGE;
	}

	public String clearSuspensionDetailPage() {
		log.info("<===End DisciplinaryActionBean.clearSuspensionDetailPage ========>");
		suspensionDetailsResponseDto = new SuspensionDetailsResponseDto();
		selectedSuspensionDetailsResponseDto = new SuspensionDetailsResponseDto();
		log.info("<===End DisciplinaryActionBean.clearSuspensionDetailPage ========>");
		return SUSPENSION_DETAILS_LIST_PAGE;
	}

	public String updateExtendSuspension() {
		log.info("<===Start DisciplinaryActionBean.updateExtendSuspension ========>");
		try {

		} catch (Exception e) {
			log.info("<===Exception Occured in DisciplinaryActionBean.updateExtendSuspension ========>");
			log.info("<===Exception is ========>" + e.getMessage());
		}
		log.info("<===End DisciplinaryActionBean.updateExtendSuspension ========>");
		return CREATE_RAISE_COMPLAINT_PAGE;

	}

	public String showEditPage() {
		log.info("<===Start DisciplinaryActionBean.showEditPage ========>");
		try {

		} catch (Exception e) {
			log.info("<===Exception Occured in DisciplinaryActionBean.showEditPage ========>");
			log.info("<===Exception is ========>" + e.getMessage());
		}
		log.info("<===End DisciplinaryActionBean.showEditPage ========>");
		return CREATE_SUSPENSION_EXTENSION;
	}

	public List<EmployeeMaster> autoCompleteEmployee(String query) {
		log.info("DisciplinaryActionBean.autoCompleteEmployee() - START");
		List<EmployeeMaster> employeeMasterList = null;
		try {
			query = StringUtils.isNotEmpty(query) ? query.trim() : "";
			Long entityId = selectedEntityMaster != null ? selectedEntityMaster.getId() : null;
			Long deptId = selectedDepartment != null ? selectedDepartment.getId() : null;
			Long sectionId = selectedSectionMaster != null ? selectedSectionMaster.getId() : null;
			log.info("DisciplinaryActionBean.autoCompleteEmployee() - entityId: " + entityId);
			log.info("DisciplinaryActionBean.autoCompleteEmployee() - deptId: " + deptId);
			log.info("DisciplinaryActionBean.autoCompleteEmployee() - sectionId: " + sectionId);
			if (entityId == null) {
				AppUtil.addWarn("Please select entity.");
				return null;
			}
			if (deptId == null) {
				AppUtil.addWarn("Please select department.");
				return null;
			}
			if (sectionId == null) {
				AppUtil.addWarn("Please select section.");
				return null;
			}
			if (entityId != null && deptId != null && sectionId != null) {
				String url = SERVER_URL + "/employee/getempbyworklocationdeptsecquery/" + entityId + "/" + deptId + "/"
						+ sectionId + "/" + query;
				log.info("DisciplinaryActionBean.autoCompleteEmployee() - url: " + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
					int employeeMasterListSize = employeeMasterList != null ? employeeMasterList.size() : 0;
					log.info("DisciplinaryActionBean.autoCompleteEmployee() - employeeMasterListSize: "
							+ employeeMasterListSize);
				}
			}
		} catch (Exception e) {
			log.error("Exception at autoCompleteEmployee()", e);
		}

		log.info("DisciplinaryActionBean.autoCompleteEmployee() - END");
		return employeeMasterList;
	}

	public void loadEmployees() {
		employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(selectedEntityMaster,
				selectedEntityTypeMaster, selectedEntityMaster, null, null);
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("DisciplinaryActionBean.showViewListPage() - START");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String requestId = "";
		String notificationId = "";
		String suspensionId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			requestId = httpRequest.getParameter("requestId");
			notificationId = httpRequest.getParameter("notificationId");
			suspensionId = httpRequest.getParameter("suspensionId");
			log.info("DisciplinaryActionBean.showViewListPage() - " + notificationId);

		}
		if (StringUtils.isNotEmpty(requestId)) {
			selectedComplaintRegisterListRespDTO = new EmpComplaintRegisterListResponseDTO();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedComplaintRegisterListRespDTO.setNotificationId(Long.parseLong(notificationId));
			}

			selectedComplaintRegisterListRespDTO.setId(Long.parseLong(requestId));
			action = "VIEW";
			loadStatus();
			userMasters = commonDataService.loadForwardToUsersByFeature("DISCIPLINARY_ACTION");
			actionTakenByUserId = loginBean.getUserDetailSession().getId();
			if (employeeMaster == null) {
				employeeMaster = new EmployeeMaster();
				employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			}
			viewDisplinaryComplaintInfo();

		}
		if (StringUtils.isNotEmpty(suspensionId)) {
			suspensionDetailsResponseDto = new SuspensionDetailsResponseDto();
			selectedSuspensionDetailsResponseDto = new SuspensionDetailsResponseDto();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedSuspensionDetailsResponseDto.setNotificationId(Long.parseLong(notificationId));
			}
			selectedSuspensionDetailsResponseDto.setId(Long.parseLong(suspensionId));
			viewStatus();
		}
		return null;
	}
}
