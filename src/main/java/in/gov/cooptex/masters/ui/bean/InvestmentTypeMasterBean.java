	package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionSource;
import in.gov.cooptex.core.accounts.model.InvestmentCategoryMaster;
import in.gov.cooptex.core.accounts.model.InvestmentTypeMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.finance.JournalVoucherDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class InvestmentTypeMasterBean {

	private final String CREATE_PAGE = "/pages/masters/createInvestmentType.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listInvestmentType.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/viewInvestmentType.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	InvestmentTypeMaster investmentTypeMaster;
	

	@Getter
	@Setter
	LazyDataModel<InvestmentTypeMaster> lazyInvestmentTypeMasterList;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<InvestmentTypeMaster> investmentTypeMasterList = new ArrayList<InvestmentTypeMaster>();

	@Getter
	@Setter
	private int investmentTypeMasterListSize;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	boolean disableAddButton = false;

	boolean forceLogin = false;
	
	ObjectMapper mapper;
	
	String jsonResponse;
	
	@Getter
	@Setter
	int totalRecords = 0;

	@PostConstruct
	public void init() {
		log.info(" InvestmentTypeMasterBean init --->" + serverURL);
		investmentTypeMaster = new InvestmentTypeMaster();
		mapper=new ObjectMapper();
		loadValues();
		addButtonFlag = true;
		getAllInvestmentTypeMasterList();
		loadLazyInvestmentTypeMasterList();

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			investmentTypeMaster = new InvestmentTypeMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String showList() {
		log.info("<---------- Loading InvestmentTypeMasterBean showList list page---------->");
		loadValues();
		getAllInvestmentTypeMasterList();
		
		disableAddButton = false;
		return LIST_PAGE;
	}
	
	
	public void loadLazyInvestmentTypeMasterList() {
		log.info("<===== Starts investmentTypeMasterBean.loadLazyInvestmentTypeMasterList ======>");

		lazyInvestmentTypeMasterList = new LazyDataModel<InvestmentTypeMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<InvestmentTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/investmenttypemaster/getallinvestmenttypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						investmentTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<InvestmentTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("investmentTypeMasterList size---------------->"
								+ lazyInvestmentTypeMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return investmentTypeMasterList;
			}

			@Override
			public Object getRowKey(InvestmentTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public InvestmentTypeMaster getRowData(String rowKey) {
				for (InvestmentTypeMaster investmentTypeMasters : investmentTypeMasterList) {
					if (investmentTypeMasters.getId().equals(Long.valueOf(rowKey))) {
						investmentTypeMaster = investmentTypeMasters;
						return investmentTypeMasters;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  investmentTypeMasterBean.loadLazyInvestmentTypeMasterList  ======>");
	}

	private void getAllInvestmentTypeMasterList() {

		log.info("<---------- Start InvestmentTypeMasterBean loadAllUserList---------->");

		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/investmenttypemaster/getall";
			baseDTO = httpService.get(url);
			investmentTypeMaster = new InvestmentTypeMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				investmentTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<InvestmentTypeMaster>>() {
						});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (investmentTypeMasterList != null) {
			investmentTypeMasterListSize = investmentTypeMasterList.size();
		} else {
			investmentTypeMasterListSize = 0;
		}
		log.info("<---------- End InvestmentTypeMasterBean loadAllUserList---------->");
	}

	public String submit() {

		log.info("InvestmentTypeMasterBean Submit method called");
		if (investmentTypeMaster.getCode() == null) {
			errorMap.notify(ErrorDescription.INVESTMENTTYPEMASTER_CODE_EMPTY.getErrorCode());
			return null;
		}
		if (investmentTypeMaster.getName() == null || investmentTypeMaster.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.INVESTMENTTYPEMASTER_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (investmentTypeMaster.getName().trim().length() < 3 || investmentTypeMaster.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.INVESTMENTTYPEMASTER_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (investmentTypeMaster.getLocalName() == null || investmentTypeMaster.getLocalName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.INVESTMENTTYPEMASTER_LNAME_EMPTY.getErrorCode());
			return null;
		}
		log.info("...... investmentTypeMaster submit begin ....");
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + appPreference.getOperationApiUrl() + "/investmenttypemaster/create",
					investmentTypeMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("InvestmentTypeMasterBean saved successfully");
					showList();
					//errorMap.notify(baseDTO.getStatusCode());
					if (action.equalsIgnoreCase("EDIT")) {
						errorMap.notify(
								ErrorDescription.getError(MastersErrorCode.INVESTMENT_TYPE_UPDATED_SUCCESSFULLY).getCode());
					}else {
						errorMap.notify(
								ErrorDescription.getError(MastersErrorCode.INVESTMENT_TYPE_INSERTED_SUCCESSFULLY).getCode());
					}
				} else if (baseDTO.getStatusCode() == 900117 || baseDTO.getStatusCode() == 900118
						|| baseDTO.getStatusCode() == 900119) {
					log.error(" processing exception occured while converting .... " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}

			}
		} catch (Exception e) {
			log.error("Error while creating InvestmentTypeMaster" + e);
		}
		log.info("...... InvestmentTypeMaster submit ended ....");
		return LIST_PAGE;
	}

	public String cancel() {
		System.out.println("the cancel method is called");
		log.info("<----Redirecting to user List page---->");
		investmentTypeMaster = new InvestmentTypeMaster();
		disableAddButton = false;
		addButtonFlag=true;
		getAllInvestmentTypeMasterList();
		return LIST_PAGE;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		investmentTypeMaster = ((InvestmentTypeMaster) event.getObject());
		addButtonFlag=false;
		disableAddButton = true;
		log.info("<===Ends InvestmentTypeMasterBean.onRowSelect ========>");
	}

	public String addInvestmentTypeMaster() {
		log.info("<---- Start InvestmentTypeMasterBean.addInvestmentTypeMaster ---->");
		try {
			action = "ADD";
			investmentTypeMaster = new InvestmentTypeMaster();
			/* investmentTypeMaster.setActiveStatus(true); */

			log.info("<---- End InvestmentTypeMasterBean.addInvestmentTypeMaster ---->");
		} catch (Exception e) {

			log.error("<---- Error occured while redirectin to user add page ---->");
		}
		disableAddButton = false;
		return CREATE_PAGE;

	}

	public String editInvestmentTypeMaster() {
		log.info("<<<<< -------Start editInvestmentTypeMaster called ------->>>>>> ");
		action = "EDIT";

		if (investmentTypeMaster == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		getrecordById();
		if (investmentTypeMaster.getActiveStatus().equals("ACTIVE")) {
			investmentTypeMaster.setActiveStatus(investmentTypeMaster.getActiveStatus());
		} else {
			investmentTypeMaster.setActiveStatus(investmentTypeMaster.getActiveStatus());
		}
		log.info("<<<<< -------End editInvestmentTypeMaster called ------->>>>>> ");

		return CREATE_PAGE;

	}

	public String viewInvestmentTypeMaster() {
		action = "VIEW";

		log.info("<----Redirecting to user view page---->");
		if (investmentTypeMaster == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		getrecordById();
		disableAddButton = false;
		return VIEW_PAGE;

	}

	public String deleteInvestmentTypeMaster() {

		try {
			log.info("<----deleteInvestmentTypeMaster begins---->");
			action = "DELETE";
			if (investmentTypeMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (investmentTypeMaster.getActiveStatus() == true) {
				log.info("<<<<< -------Active Investment Type Cannot be deleted ------->>>>>> ");
				Util.addWarn("Active Investment Type Cannot be deleted");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInvestmentTypeMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String deleteInvestmentTypeMasterConfirm() {

		try {
			log.info("Delete confirmed for InvestmentTypeMaster [" + investmentTypeMaster.getId() + "]");
			InvestmentTypeMaster investmentTypeMasterDelete = new InvestmentTypeMaster();
			investmentTypeMasterDelete.setId(investmentTypeMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/investmenttypemaster/delete";
			baseDTO = httpService.post(url, investmentTypeMaster);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("InvestmentTypeMaster deleted successfully....!!!");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.INVESTMENT_TYPE_DELETED_SUCCESSFULLY).getCode());
					//errorMap.notify(baseDTO.getStatusCode());
					investmentTypeMaster = new InvestmentTypeMaster();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					//errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.INVESTMENT_TYPE_DELETED_SUCCESSFULLY).getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting InvestmentTypeMaster ....", e);
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start InvestmentTypeMasterBean . clear ----->");
		getAllInvestmentTypeMasterList();
		investmentTypeMaster = new InvestmentTypeMaster();
		log.info("<---- End InvestmentTypeMasterBean . clear ----->");
		addButtonFlag=true;
		disableAddButton = false;
		return LIST_PAGE;

	}

	public String backPage() {
		log.info("<---- Back to the InvestmentTypeMaster list page ----->");
		showList();
		return LIST_PAGE;
	}
	
	public void getrecordById() {
		log.info("Inside getrecordById() ");
		BaseDTO baseDTO = null;
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/investmenttypemaster/get/"
					+ investmentTypeMaster.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				investmentTypeMaster = mapper.readValue(jsonResponse, new TypeReference<InvestmentTypeMaster>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While getrecordById() :: ", e);
		}
		
		
	}
	
	

}
