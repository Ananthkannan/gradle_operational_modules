package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import in.gov.cooptex.operation.dto.ProcurementOrderResponseDto;
import in.gov.cooptex.operation.dto.ProcurementQuantityDTO;
import in.gov.cooptex.operation.enums.ProcurementOrderStatus;
import in.gov.cooptex.operation.model.AdditionalProductionPlan;
import in.gov.cooptex.operation.model.AdditionalProductionPlanLog;
import in.gov.cooptex.operation.model.ContractExportPlan;
import in.gov.cooptex.operation.model.ContractExportPlanLog;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.ProcurementOrder;
import in.gov.cooptex.operation.model.ProcurementOrderDetails;
import in.gov.cooptex.operation.model.ProcurementOrderLog;
import in.gov.cooptex.operation.model.ProcurementOrderNote;
import in.gov.cooptex.operation.production.model.GovtSchemePlanLog;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
public class ProcurementOrderBean extends CommonBean {

	private final String LIST_PAGE_URL = "/pages/operation/procurement/listProcurementOrder.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/procurement/createProcurementOrder.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/procurement/viewProcurementOrder.xhtml?faces-redirect=true;";

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<GovtSchemePlan> govtProductionPlanList;

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlan;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	String govtProductionPlan;

	@Getter
	@Setter
	EntityMaster dpOfficeEntity;

	@Getter
	@Setter
	EntityMaster regionalEntity;

	@Getter
	@Setter
	EntityMaster issrEntity;

	@Getter
	@Setter
	List<EntityMaster> dpOfficeEntityList;

	@Getter
	@Setter
	List<EntityMaster> regionalEntityList;

	@Getter
	@Setter
	List<EntityMaster> issrEntityList;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	private Boolean editFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProcurementOrderNote planNote = new ProcurementOrderNote();

	@Getter
	@Setter
	private Double quantity;

	@Getter
	@Setter
	private Double totalQuantity = 0.00, totalValue = 0.00;

	@Getter
	@Setter
	private Double totalProcPlanQty, alreadyIssuedQty;

	@Getter
	@Setter
	private Date validityDate, notLaterThanDate;

	@Getter
	@Setter
	private int procrmntListSize = 0;

	/*
	 * @Setter @Getter UomMaster uomMaster;
	 */

	@Setter
	@Getter
	List<UomMaster> uomMasterList;

	@Setter
	@Getter
	ProcurementOrder procurementOrder;

	@Setter
	@Getter
	List<ProcurementOrder> procurementOrderList;

	@Setter
	@Getter
	ProcurementOrderDetails procurementOrderDetails;

	@Setter
	@Getter
	ProcurementOrderResponseDto procurementOrderResponseDto;

	ObjectMapper mapper;

	@Getter
	@Setter
	LazyDataModel<ProcurementOrderResponseDto> procurementOrderResponseDtoList;

	@Getter
	@Setter
	ContractExportPlan contractExportPlan = new ContractExportPlan();

	@Getter
	@Setter
	List<ContractExportPlan> contractExportPlanList = new ArrayList<>();

	@Getter
	@Setter
	List<AdditionalProductionPlan> additionalProductionPlanList = new ArrayList<>();

	@Getter
	@Setter
	AdditionalProductionPlan additionalProductionPlan = new AdditionalProductionPlan();

	@Getter
	@Setter
	String action = null, govTypeName = "gov";

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	boolean addButtonFlag = true;

	@Getter
	@Setter
	List<ProcurementOrderResponseDto> viewLogResponseList = new ArrayList<>();

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	ProcurementOrderNote lastNote = new ProcurementOrderNote();

	@Getter
	@Setter
	Date minDate;

	@Getter
	@Setter
	Date maxDate;

	public void init() {
		log.info("Inside init()>>>>>>>>>");
		govtProductionPlan = null;
		govtSchemePlan = null;
		editFlag = false;
		procrmntListSize = 0;
		addButtonFlag = true;
		loadUrl();
		// loadStatusValues();
		// getDPOfficeCodeName();
		loadForwardToUsers();
		loadEmployeeLoggedInUserDetails();
		loadRegionOfficeEntityList();
	}

	public String getListPage() {
		editFlag = false;
		addButtonFlag = true;
		return LIST_PAGE_URL;
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		// BaseDTO baseDTO = null;
		try {
			forwardToUsersList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.PROCUREMENT_ORDER.toString());
			log.info("forwardToUsersList size" + forwardToUsersList.size());
			/*
			 * String url = appPreference.getPortalServerURL() +
			 * "/user/getallforwardtousers"; baseDTO = httpService.get(url); if (baseDTO !=
			 * null) { ObjectMapper mapper = new ObjectMapper(); String jsonResponse =
			 * mapper.writeValueAsString(baseDTO.getResponseContent()); forwardToUsersList =
			 * mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() { }); }
			 */
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadGovtProductionPlanApprovedPlanList() {
		log.info("loadGovtProductionPlanApprovedPlanList govtProductionPlan==>" + govtProductionPlan);
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			govTypeName = "gov";
			loadGovtSchemePlanApprovedPlanList();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Contract")) {
			govTypeName = "contract";
			loadContractPlanList(govtProductionPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			govTypeName = "export";
			loadContractPlanList(govtProductionPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			govTypeName = "additional";
			loadAdditionalPlanList();
		} else {
			govtProductionPlanList = new ArrayList<GovtSchemePlan>();
		}
	}

	@SuppressWarnings("unchecked")
	public void loadContractPlanList(String planType) {
		log.info("<--- Inside loadContactExportPlanApprovedPlanList() ---> ");
		try {

			uomMasterList = new ArrayList<UomMaster>();
			String url = SERVER_URL + "/govt/societyPlan/getConAndExportListNotInProcurementOrder/" + planType;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					contractExportPlanList = (List<ContractExportPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<ContractExportPlan>>() {
							});
					if (contractExportPlanList != null) {
						log.info("contractExportPlanList with Size of : " + contractExportPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadContactExportPlanApprovedPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					contractExportPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadAdditionalPlanList() {
		log.info("<--- Inside loadAdditionalList() ---> ");
		try {

			uomMasterList = new ArrayList<UomMaster>();
			String url = SERVER_URL + "/govt/societyPlan/getadditionalplanlistwithoutprocument";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					additionalProductionPlanList = (List<AdditionalProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<AdditionalProductionPlan>>() {
							});
					if (additionalProductionPlanList != null) {
						log.info("AdditionalProductionPlanList with Size of : " + additionalProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadAdditionalList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					additionalProductionPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadGovtSchemePlanApprovedPlanList() {
		log.info("<--- Inside loadRetailProductionPlanFinalApprovedList() ---> ");
		String requestPath = SERVER_URL + "/govt/societyPlan/getgovtproductionplanlistnotinprocurrement";

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					govtProductionPlanList = (List<GovtSchemePlan>) mapper.readValue(jsonValue,
							new TypeReference<List<GovtSchemePlan>>() {
							});

					if (govtProductionPlanList != null) {
						log.info("govtProductionPlanList with Size of : " + govtProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" govtProductionPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadRetailProductionPlanFinalApprovedList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadRetailProductionPlanFinalApprovedList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadRegionOfficeEntityList() {
		log.info("<--- Inside loadRegionOfficeEntityList() ---> ");
		String requestPath = SERVER_URL + "/procurement/getRegionOfficeEntityList";

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					regionalEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (regionalEntityList != null) {
						log.info("regionalEntityList with Size of : " + regionalEntityList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" regionalEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadRegionOfficeEntityList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadRegionOfficeEntityList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadISSREntityList() {
		log.info("<--- Inside loadISSREntityList() ---> ");
		if (regionalEntity == null || regionalEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_INDENTING_REGION.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/getISSREntityList/" + regionalEntity.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					issrEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (issrEntityList != null) {
						log.info("issrEntityList with Size of : " + issrEntityList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" issrEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadISSREntityList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadISSREntityList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getDPOfficeList() {
		log.info("<--- Inside getDPOfficeList() ---> ");
		if (govtSchemePlan == null || govtSchemePlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_GOV_SCHEME_PLAN.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/getDPOfficeList/" + govtSchemePlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					dpOfficeEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (dpOfficeEntityList != null) {
						log.info("dpOfficeEntityList with Size of : " + dpOfficeEntityList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dpOfficeEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getDPOfficeList() ---> ");
			}
			minDate = govtSchemePlan.getPeriodFrom();
			maxDate = govtSchemePlan.getPeriodTo();
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getDPOfficeList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getDPOfficeListByConExport() {
		log.info("<--- Inside getDPOfficeListByConExport() ---> ");
		if (contractExportPlan == null || contractExportPlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_CON_EXPORT_PLAN.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/getDPOfficeListByConExport/" + contractExportPlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					dpOfficeEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (dpOfficeEntityList != null) {
						log.info("dpOfficeEntityList with Size of : " + dpOfficeEntityList.size());
						contractExportPlanList.stream().forEach(contractExportplanObj -> {
							if (contractExportplanObj.getId() == contractExportPlan.getId()) {
								contractExportPlan.setApprovedBy(contractExportplanObj.getApprovedBy());
								contractExportPlan.setApprovedDate(contractExportplanObj.getApprovedDate());
							}
						});

					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dpOfficeEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getDPOfficeList() ---> ");
			}
			minDate = contractExportPlan.getFromDate();
			maxDate = contractExportPlan.getToDate();
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getDPOfficeList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getDPOfficeListByAdd() {
		log.info("<--- Inside getDPOfficeListByAdd() ---> ");
		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ADD_SCHEME_PLAN.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/getDPOfficeListByAdd/" + additionalProductionPlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					dpOfficeEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (dpOfficeEntityList != null) {
						log.info("dpOfficeEntityList with Size of : " + dpOfficeEntityList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dpOfficeEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getDPOfficeList() ---> ");
			}
			minDate = additionalProductionPlan.getPlanFrom();
			maxDate = additionalProductionPlan.getPlanTo();
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getDPOfficeList() --->", e);
		}
	}

	public void loadProductList() {
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			loadProductListGov();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Contract")) {
			loadProductListConExport();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			loadProductListConExport();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			loadProductListAdditional();
		}
	}

	@SuppressWarnings("unchecked")
	public void loadProductListAdditional() {
		log.info("<--- Inside loadProductListAdditional() ---> ");
		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ADD_SCHEME_PLAN.getCode());
			return;
		}
		if (dpOfficeEntity == null || dpOfficeEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_DP_OFFICE.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/loadProductListAdditional/" + dpOfficeEntity.getId() + "/"
				+ additionalProductionPlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productVarietyMasterList = (List<ProductVarietyMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductVarietyMaster>>() {
							});

					if (productVarietyMasterList != null) {
						log.info("productVarietyMasterList with Size of : " + productVarietyMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productVarietyMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadProductList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadProductList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadProductListConExport() {
		log.info("<--- Inside loadProductListConExport() ---> ");
		if (contractExportPlan == null || contractExportPlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_CON_EXPORT_PLAN.getCode());
			return;
		}
		if (dpOfficeEntity == null || dpOfficeEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_DP_OFFICE.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/loadProductListConExport/" + dpOfficeEntity.getId() + "/"
				+ contractExportPlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productVarietyMasterList = (List<ProductVarietyMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductVarietyMaster>>() {
							});

					if (productVarietyMasterList != null) {
						log.info("productVarietyMasterList with Size of : " + productVarietyMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productVarietyMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadProductList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadProductList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadProductListGov() {
		log.info("<--- Inside loadProductList() ---> ");
		if (govtSchemePlan == null || govtSchemePlan.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_GOV_SCHEME_PLAN.getCode());
			return;
		}
		if (dpOfficeEntity == null || dpOfficeEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_DP_OFFICE.getCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/loadProductList/" + dpOfficeEntity.getId() + "/"
				+ govtSchemePlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productVarietyMasterList = (List<ProductVarietyMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductVarietyMaster>>() {
							});

					if (productVarietyMasterList != null) {
						log.info("productVarietyMasterList with Size of : " + productVarietyMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productVarietyMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadProductList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadProductList() --->", e);
		}
	}

	public void test() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");
	}

	private static String createRegistrationNumber() {
		Random rand = new Random();
		int num = rand.nextInt(9000000) + 1000000;
		log.info("::::::::" + num);
		return String.valueOf(num);
	}

	public void addProcurementDetails() {
		if (procurementOrder == null) {
			procurementOrder = new ProcurementOrder();
			if (govtProductionPlan == null || govtProductionPlan.equals("")) {
				errorMap.notify(ErrorDescription.SELECT_PLAN_TYPE.getErrorCode());
				return;
			}

			/*
			 * if(govtSchemePlan == null || govtSchemePlan.getId() == null) {
			 * AppUtil.addWarn("Please select GovtSchemePlan"); return; }
			 */
			if (quantity <= 0) {
				AppUtil.addWarn("Please Enter the Current Issuing Quantity");
				return;
			}
			if (govtProductionPlan != null && !govtProductionPlan.equals("")
					&& govtProductionPlan.equals("Govt_Scheme")) {
				if (govtSchemePlan == null || govtSchemePlan.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_PLAN_CODE.getErrorCode());
					return;
				}
				procurementOrder.setGovtSchemePlan(govtSchemePlan);

			} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
					&& govtProductionPlan.equals("Contract")) {
				if (contractExportPlan == null || contractExportPlan.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_PLAN_CODE.getErrorCode());
					return;
				}
				procurementOrder.setContractExportPlan(contractExportPlan);
			} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
					&& govtProductionPlan.equals("Export")) {
				if (contractExportPlan == null || contractExportPlan.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_PLAN_CODE.getErrorCode());
					return;
				}
				procurementOrder.setContractExportPlan(contractExportPlan);
			} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
					&& govtProductionPlan.equals("Additional_Production_Plan")) {
				if (contractExportPlan == null || additionalProductionPlan.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_PLAN_CODE.getErrorCode());
					return;
				}
				procurementOrder.setAdditionalProductionPlan(additionalProductionPlan);
			}

			if (dpOfficeEntity == null || dpOfficeEntity.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_DP_OFFICE.getErrorCode());
				return;
			}

			procurementOrder.setEntityMaster(dpOfficeEntity);
			procurementOrder.setSevenDigitNumber(createRegistrationNumber());
			procurementOrder.setProcurementOrderDetailsList(new ArrayList<ProcurementOrderDetails>());
		}
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return;
		}
		if (regionalEntity == null || regionalEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_INDENTING_REGIONAL_ENTITY.getErrorCode());
			return;
		}
		if (issrEntity == null || issrEntity.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ISSR_ENTITY.getErrorCode());
			return;
		}
		if (quantity == null || quantity.equals("")) {
			errorMap.notify(ErrorDescription.ENTER_CURRENT_ISSUED_QUANTITY.getErrorCode());
			return;
		}
		if (validityDate == null || validityDate.equals("")) {
			errorMap.notify(ErrorDescription.SELECT_VALIDITY_DATE.getErrorCode());
			return;
		}
		if (notLaterThanDate == null || notLaterThanDate.equals("")) {
			errorMap.notify(ErrorDescription.ENTER_NOT_LATER_THAN_DATE.getErrorCode());
			return;
		}
		log.info("pro list==>" + procurementOrder.getProcurementOrderDetailsList());
		if (procurementOrder.getProcurementOrderDetailsList() != null) {
			for (ProcurementOrderDetails details : procurementOrder.getProcurementOrderDetailsList()) {

				if (details.getProductVarietyMaster().getId() != null && productVarietyMaster.getId() != null
						&& details.getIssrEntityMaster() != null && issrEntity.getId() != null) {
					log.info("veriety master id==>" + details.getProductVarietyMaster().getId());
					log.info("productVarietyMaster id==>" + productVarietyMaster.getId());
					log.info("getIssrEntityMaster id==>" + details.getIssrEntityMaster().getId());
					log.info("issrEntity id==>" + issrEntity.getId());

					if (details.getProductVarietyMaster().getId().equals(productVarietyMaster.getId())
							&& details.getIssrEntityMaster().getId().equals(issrEntity.getId())) {
						errorMap.notify(ErrorDescription.COMBINATION_OF_PRODUCT_ISSR.getErrorCode());
						return;
					}
				}
			}
		}
		if ((totalProcPlanQty - alreadyIssuedQty) < quantity) {
			errorMap.notify(ErrorDescription.GREATER_THAN_OF_TOTAL_QTY_ISSUED_QTY.getErrorCode());
			quantity = null;
			return;
		}

		log.info("regionalEntity id==>" + regionalEntity.getId() + "<=name=>" + regionalEntity.getName());
		log.info("issrEntity id==>" + issrEntity.getId() + "<=name=>" + issrEntity.getName());

		procurementOrderDetails = new ProcurementOrderDetails();
		procurementOrderDetails.setProductVarietyMaster(productVarietyMaster);
		procurementOrderDetails.setUomMaster(getUOM());
		procurementOrderDetails.setRegionEntityMaster(regionalEntity);
		procurementOrderDetails.setIssrEntityMaster(issrEntity);
		procurementOrderDetails.setItemQuantity(quantity);
		procurementOrderDetails.setItemValue(getProductUnitRate());
		procurementOrderDetails.setValidityDate(validityDate);
		procurementOrderDetails.setNotLesserDate(notLaterThanDate);

		totalQuantity = totalQuantity + quantity;
		totalValue = totalValue + procurementOrderDetails.getItemValue();

		procurementOrder.getProcurementOrderDetailsList().add(procurementOrderDetails);
		procrmntListSize = procurementOrder.getProcurementOrderDetailsList().size();
		// procurementOrderDetails = null;
		productVarietyMaster = null;
		regionalEntity = null;
		issrEntity = null;
		quantity = null;
		validityDate = null;
		notLaterThanDate = null;
		totalProcPlanQty = null;
		alreadyIssuedQty = null;
	}

	public void deleteProcurementDtls(ProcurementOrderDetails prDetails) {
		log.info("<--- Inside deleteProcurementDtls() ---> ");
		for (ProcurementOrderDetails details : procurementOrder.getProcurementOrderDetailsList()) {
			if (details.getProductVarietyMaster().getId().equals(prDetails.getProductVarietyMaster().getId())
					&& details.getIssrEntityMaster().getId().equals(prDetails.getIssrEntityMaster().getId())) {
				totalQuantity = totalQuantity - details.getItemQuantity();
				totalValue = totalValue - details.getItemValue();

				procurementOrder.getProcurementOrderDetailsList().remove(details);
				procrmntListSize = procurementOrder.getProcurementOrderDetailsList().size();
				break;
			}
		}
	}

	@SuppressWarnings("unchecked")
	public UomMaster getUOM() {
		log.info("<--- Inside getUOM() ---> ");
		UomMaster uomMaster = null;
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return null;
		}
		String requestPath = SERVER_URL + "/govt/societyPlan/getUOM/" + productVarietyMaster.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					uomMasterList = (List<UomMaster>) mapper.readValue(jsonValue, new TypeReference<List<UomMaster>>() {
					});
					if (uomMasterList != null && !uomMasterList.isEmpty()) {
						for (int j = 0; j < uomMasterList.size(); j++) {
							if (uomMasterList.get(j) != null && uomMasterList.get(j).getId() != null) {
								uomMaster = uomMasterList.get(j);
							}
						}
					}
					if (uomMaster != null) {
						log.info("uomMaster : " + uomMaster);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" uomMaster is Empty ");
						return null;
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
					return null;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getUOM() ---> ");
				return null;
			}
			return uomMaster;
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getUOM() --->", e);
			return null;
		}
	}

	// @SuppressWarnings("unchecked")
	public Double getProductUnitRate() {
		log.info("<--- Inside getProductUnitRate() ---> ");
		Double unitRate = 0d;
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return null;
		}
		String requestPath = SERVER_URL + "/procurement/getProductUnitRate/" + productVarietyMaster.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					unitRate = (Double) mapper.readValue(jsonValue, new TypeReference<Double>() {
					});
					if (unitRate != null) {
						log.info("unitRate : " + unitRate);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" unitRate is Empty ");
						unitRate = 0d;
						return unitRate;
					}
				} else {
					// errorMap.notify(baseDTO.getStatusCode());
					unitRate = 0d;
					log.info(" Error Message: " + baseDTO.getErrorDescription());
					return unitRate;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getProductUnitRate() ---> ");
				return null;
			}
			return unitRate;
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getProductUnitRate() --->", e);
			return null;
		}
	}

	public void getQuantites() {
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			getQuantitesByGov();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Contract")) {
			getQuantitesByConExport();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			getQuantitesByConExport();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			getQuantitesByAdditional();
		}
	}

	public void getQuantitesByAdditional() {
		log.info("<--- Started getQuantites() ---> ");
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return;
		}
		try {
			log.info("productVarietyMaster id==>" + productVarietyMaster.getId());
			log.info("contractExportPlan id==>" + contractExportPlan.getId());
			String requestPath = SERVER_URL + "/procurement/getQuantitesByAdditional/" + productVarietyMaster.getId()
					+ "/" + additionalProductionPlan.getId();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					ProcurementQuantityDTO dto = (ProcurementQuantityDTO) mapper.readValue(jsonValue,
							new TypeReference<ProcurementQuantityDTO>() {
							});

					if (dto != null) {
						totalProcPlanQty = dto.getTotalProcPlanQty();
						alreadyIssuedQty = dto.getAlreadyIssuedQty();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dto is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getQuantites() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getQuantites() --->", e);
		}
	}

	public void getQuantitesByConExport() {
		log.info("<--- Started getQuantites() ---> ");
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return;
		}
		try {
			log.info("productVarietyMaster id==>" + productVarietyMaster.getId());
			log.info("contractExportPlan id==>" + contractExportPlan.getId());
			String requestPath = SERVER_URL + "/procurement/getQuantitesByConExport/" + productVarietyMaster.getId()
					+ "/" + contractExportPlan.getId();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					ProcurementQuantityDTO dto = (ProcurementQuantityDTO) mapper.readValue(jsonValue,
							new TypeReference<ProcurementQuantityDTO>() {
							});

					if (dto != null) {
						totalProcPlanQty = dto.getTotalProcPlanQty();
						alreadyIssuedQty = dto.getAlreadyIssuedQty();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dto is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getQuantites() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getQuantites() --->", e);
		}
	}

	// @SuppressWarnings("unchecked")
	public void getQuantitesByGov() {
		log.info("<--- Started getQuantites() ---> ");
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getErrorCode());
			return;
		}
		String requestPath = SERVER_URL + "/procurement/getQuantites/" + productVarietyMaster.getId() + "/"
				+ govtSchemePlan.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					ProcurementQuantityDTO dto = (ProcurementQuantityDTO) mapper.readValue(jsonValue,
							new TypeReference<ProcurementQuantityDTO>() {
							});

					if (dto != null) {
						totalProcPlanQty = dto.getTotalProcPlanQty();
						alreadyIssuedQty = dto.getAlreadyIssuedQty();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" dto is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getQuantites() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getQuantites() --->", e);
		}
	}

	public String saveProcurementOrder(String status) {
		log.info("from saveProcurementOrder...........");
		if (procurementOrder == null || procurementOrder.getProcurementOrderDetailsList() == null
				|| procurementOrder.getProcurementOrderDetailsList().isEmpty()) {
			errorMap.notify(ErrorDescription.ENTER_VALID_DATA.getCode());
			return null;
		}
		log.info("planNote : " + planNote);
		/*
		 * if(planNote == null || planNote.getNote() == null ||
		 * planNote.getNote().equals("") || planNote.getUserMaster() == null ||
		 * planNote.getUserMaster().getId() == null || planNote.getFinalApproval() ==
		 * null || planNote.getFinalApproval().equals("")) {
		 * errorMap.notify(ErrorDescription.PROPER_PLAN_NOTE_DETAILS.getCode()); return
		 * null; }
		 */

		if (planNote.getNote() == null || planNote.getNote().equals("")) {
			errorMap.notify(ErrorDescription.DEPARTMENT_APPROVAL_REG_NOTE_EMPTY.getCode());
			return null;
		}

		procurementOrder.setStage(status);

		procurementOrder.setProcurementOrderNoteList(new ArrayList<ProcurementOrderNote>());
		procurementOrder.getProcurementOrderNoteList().add(planNote);

		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Contract")) {
			procurementOrder.setContractExportPlan(contractExportPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			procurementOrder.setContractExportPlan(contractExportPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			procurementOrder.setAdditionalProductionPlan(additionalProductionPlan);
		}

		String url = SERVER_URL + "/procurement/submitProcurementOrder";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, procurementOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("ProcurementOrder Submitted successfully");
					// AppUtil.addInfo("Procurement Order Submitted successfully");
					if (!status.equalsIgnoreCase("INITIATED")) {
						errorMap.notify(ErrorDescription.PROCUREMENT_SUBMITED_SUCCESSFULLY.getErrorCode());
					} else {
						AppUtil.addInfo("Procurement order Initiated successfully");
					}
					// showFdsList();
					// errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(ErrorDescription.PROCUREMENT_NOT_SUBMITED_SUCCESSFULLY.getErrorCode());
					// AppUtil.addError("Procurement Order not able to Submitted");
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			// errorMap.notify(8545);
			return null;
		}
	}

	public String showProcurementList() {
		log.info("Inside FreeDistributionSystemBean showList()>>>>>>>>>");
		loadProcurementnList();
		// prepareSchemePeriod();
		loadStatusValues();
		addButtonFlag = true;
		return LIST_PAGE_URL;
	}

	private void loadStatusValues() {
		Object[] statusArray = ProcurementOrderStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status.toString().replace("_", "-"));
		}
		log.info("<--- statusValues ---> " + statusValues);
	}

	@SuppressWarnings("unchecked")
	public void loadProcurementnList() {
		log.info("<--- loadProcurementnList ---> ");
		planNote = new ProcurementOrderNote();
		procurementOrderResponseDtoList = new LazyDataModel<ProcurementOrderResponseDto>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<ProcurementOrderResponseDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<ProcurementOrderResponseDto> data = new ArrayList<ProcurementOrderResponseDto>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<ProcurementOrderResponseDto>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadProcurementnList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(ProcurementOrderResponseDto res) {
				return res != null ? res.getProcurementOrderId() : null;
			}

			@Override
			public ProcurementOrderResponseDto getRowData(String rowKey) {
				List<ProcurementOrderResponseDto> responseList = (List<ProcurementOrderResponseDto>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (ProcurementOrderResponseDto res : responseList) {
					if (res.getProcurementOrderId().longValue() == value.longValue()) {
						procurementOrderResponseDto = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		procurementOrderResponseDto = new ProcurementOrderResponseDto();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		ProcurementOrder procurOrder = new ProcurementOrder();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		procurOrder.setPaginationDTO(paginationDTO);

		// log.info("procurOrder " + procurOrder);
		procurOrder.setFilters(filters);

		try {
			String procurOrderSearchUrl = SERVER_URL + "/procurement/search";
			log.info("procurOrderSearchUrl " + procurOrderSearchUrl);
			baseDTO = httpService.post(procurOrderSearchUrl, procurOrder);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		procurementOrderResponseDto = ((ProcurementOrderResponseDto) event.getObject());
		addButtonFlag = false;
	}

	public String gotoViewPage() {
		log.info("Inside gotoViewPage()");
		enableFlag = false;
		employeeMaster = new EmployeeMaster();
		BaseDTO baseDTO = null;
		if (procurementOrderResponseDto == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getErrorCode());
			return null;
		}

		try {
			String URL = SERVER_URL + "/procurement/get";
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.post(URL,procurementOrderResponseDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				procurementOrder = objectMapper.readValue(jsonResponse, ProcurementOrder.class);
				log.info("===================== ");

				if (procurementOrder != null && procurementOrder.getGovtSchemePlan() != null) {
					govTypeName = "gov";
					govtProductionPlan = "Govt Scheme";

					for (GovtSchemePlanLog schemeLog : procurementOrder.getGovtSchemePlan()
							.getGovtSchemePlanLogList()) {
						if (schemeLog != null && schemeLog.getCreatedBy() != null
								&& schemeLog.getStage().equals("FINAL_APPROVED")) {
							procurementOrder.getGovtSchemePlan().setApprovedDate(schemeLog.getCreatedDate());
							procurementOrder.getGovtSchemePlan().setApprovedBy(schemeLog.getCreatedBy());
							break;
						}
					}

				} else if (procurementOrder != null && procurementOrder.getContractExportPlan() != null
						&& procurementOrder.getContractExportPlan().getPlanType().name().equals("Contract")) {
					govTypeName = "contract";
					govtProductionPlan = "Contract";
					for (ContractExportPlanLog conLog : procurementOrder.getContractExportPlan()
							.getContractExportPlanLogList()) {
						if (conLog != null && conLog.getCreatedBy() != null
								&& conLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getContractExportPlan().setApprovedDate(conLog.getCreatedDate());
							procurementOrder.getContractExportPlan().setApprovedBy(conLog.getCreatedBy());
							break;
						}
					}

				} else if (procurementOrder != null && procurementOrder.getContractExportPlan() != null
						&& procurementOrder.getContractExportPlan().getPlanType().name().equals("Export")) {
					govTypeName = "export";
					govtProductionPlan = "Export";
					for (ContractExportPlanLog conLog : procurementOrder.getContractExportPlan()
							.getContractExportPlanLogList()) {
						if (conLog != null && conLog.getCreatedBy() != null
								&& conLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getContractExportPlan().setApprovedDate(conLog.getCreatedDate());
							procurementOrder.getContractExportPlan().setApprovedBy(conLog.getCreatedBy());
							break;
						}
					}

				} else if (procurementOrder != null && procurementOrder.getAdditionalProductionPlan() != null) {
					govtProductionPlan = "Additional Production Plan";
					govTypeName = "additional";
					for (AdditionalProductionPlanLog addLog : procurementOrder.getAdditionalProductionPlan()
							.getAdditionalProductionPlanLogList()) {
						if (addLog != null && addLog.getCreatedBy() != null
								&& addLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getAdditionalProductionPlan().setApprovedDate(addLog.getCreatedDate());
							procurementOrder.getAdditionalProductionPlan().setApprovedBy(addLog.getCreatedBy());
							break;
						}
					}
					additionalProductionPlan = procurementOrder.getAdditionalProductionPlan();
					additionalProductionPlanList = new ArrayList<>();
					additionalProductionPlanList.add(additionalProductionPlan);
				}

				// loadEmployeeLoggedInUserDetails();

				totalQuantity = 0.00;
				totalValue = 0.00;
				for (ProcurementOrderDetails schemeDetails : procurementOrder.getProcurementOrderDetailsList()) {
					if (schemeDetails != null) {
						totalQuantity = totalQuantity + schemeDetails.getItemQuantity();
						totalValue = totalValue + schemeDetails.getItemValue();

						productVarietyMaster = schemeDetails.getProductVarietyMaster();
						regionalEntity = schemeDetails.getRegionEntityMaster();
						issrEntity = schemeDetails.getIssrEntityMaster();
						quantity = schemeDetails.getItemQuantity();
						validityDate = schemeDetails.getValidityDate();
						notLaterThanDate = schemeDetails.getNotLesserDate();
						schemeDetails.setUomMaster(getUOM());
						schemeDetails.setRegionEntityMaster(regionalEntity);
						schemeDetails.setIssrEntityMaster(issrEntity);
					}
				}
				planNote = new ProcurementOrderNote();

				// getEmployeeDetailsByUserId();
				loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();
				log.info("Last note:::::::::::::::" + planNote);
				String employeeDatajsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = objectMapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<ProcurementOrderResponseDto>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size()
						+ viewLogResponseList);
				lastNote = procurementOrder.getProcurementOrderNoteList()
						.get(procurementOrder.getProcurementOrderNoteList().size() - 1);
				if (lastNote.getUserMaster() != null)
					if (lastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
						if (!procurementOrderResponseDto.getProcurementStatus()
								.equals(ProcurementOrderStatus.INITIATED.toString())) {
							enableFlag = true;
						}
					}
				/*
				 * if(procurementOrder.getProcurementOrderLogList() != null &&
				 * !procurementOrder.getProcurementOrderLogList().isEmpty()) { EmployeeMaster
				 * empDetails = null; for(ProcurementOrderLog planLog :
				 * procurementOrder.getProcurementOrderLogList()) { String url =
				 * appPreference.getPortalServerURL() +
				 * "/employee/findempdetailsbyloggedinuser/"+planLog.getCreatedBy().getId();
				 * 
				 * BaseDTO dto = httpService.get(url); if (dto != null && dto.getStatusCode() ==
				 * 0) { ObjectMapper maper = new ObjectMapper(); String jsonResp =
				 * maper.writeValueAsString(dto.getResponseContent()); empDetails =
				 * maper.readValue(jsonResp, EmployeeMaster.class); } else {
				 * errorMap.notify(ErrorDescription.EMP_NOT_FOUND_THIS_USER.getErrorCode());
				 * return null; }
				 * 
				 * EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
				 * empDetails.getPersonalInfoEmployment().getDesignation().getName(),
				 * planLog.getCreatedDate(), planLog.getRemarks());
				 * if(planLog.getStage().equals(ProcurementOrderStatus.INITIATED.toString()) ||
				 * planLog.getStage().equals(ProcurementOrderStatus.SUBMITTED.toString())) {
				 * eventLog.setTitle("Created By"); } else
				 * if(planLog.getStage().equals(ProcurementOrderStatus.APPROVED.toString())){
				 * eventLog.setTitle("Approved By"); approvedEventLogList.add(eventLog); } else
				 * if(planLog.getStage().equals(ProcurementOrderStatus.REJECTED.toString())){
				 * eventLog.setTitle("Rejected By"); rejectedEventLogList.add(eventLog); }
				 * 
				 * eventLogList.add(eventLog); } }
				 */
				/*
				 * 
				 * Collections.sort(eventLogList, new Comparator<EventLog>() { public int
				 * compare(EventLog m1, EventLog m2) { return
				 * m1.getTitle().compareTo(m2.getTitle()); } });
				 */
				return VIEW_PAGE_URL;
				// return null;

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return null;
	}

	public void getEmployeeDetailsByUserId() {
		log.info("Inside getEmployeeDetailsByUserId()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ planNote.getCreatedBy().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				errorMap.notify(ErrorDescription.EMP_NOT_FOUND_THIS_USER.getErrorCode());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public String approveRejectGovtSocietyPlan(String status) {
		// GovtSocietyDTO govtSocietyDTO = new GovtSocietyDTO();

		log.info("planNote : " + planNote);
		log.info("getUserMaster : " + planNote.getUserMaster());
		if (planNote == null || planNote.getNote() == null || planNote.getNote().equals("")
				|| planNote.getUserMaster() == null || planNote.getUserMaster().getId() == null
				|| planNote.getFinalApproval() == null || planNote.getFinalApproval().equals("")) {
			errorMap.notify(ErrorDescription.PROPER_PLAN_NOTE_DETAILS.getCode());
			return null;
		}

		procurementOrder.setStage(status);
		procurementOrder.setCreatedBy(loginBean.getUserDetailSession());
		ProcurementOrderLog procLog = new ProcurementOrderLog();

		procurementOrder.getProcurementOrderNoteList().clear();
		// govtSocietyPlan.getGovtSocietyPlanLogList().clear();

		planNote.setCreatedBy(loginBean.getUserDetailSession());
		planNote.setCreatedDate(new Date());
		procurementOrder.getProcurementOrderNoteList().add(planNote);
		procLog.setStage(status);
		procLog.setProcurementOrder(procurementOrder);
		procLog.setCreatedBy(loginBean.getUserDetailSession());
		procurementOrder.getProcurementOrderLogList().add(procLog);
		// govtSocietyDTO.setGovtSocietyPlan(govtSocietyPlan);

		String url = SERVER_URL + "/procurement/approveRejectProcurementOrder";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, procurementOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Govt Society Plan Submitted successfully");
					errorMap.notify(ErrorDescription.PROCUREMENT_SUBMITED_SUCCESSFULLY.getErrorCode());
					showProcurementList();
					// errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.PROCUREMENT_NOT_SUBMITED_SUCCESSFULLY.getErrorCode());
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info(
					"Procurement Order is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public void callDelete() {
		log.info(":: callDelete Delete Response :: ");
		if (procurementOrderResponseDto == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return;
		} else if (procurementOrderResponseDto.getProcurementStatus()
				.equals(ProcurementOrderStatus.SUBMITTED.toString())
				|| procurementOrderResponseDto.getProcurementStatus().equals(ProcurementOrderStatus.APPROVED.toString())
				|| procurementOrderResponseDto.getProcurementStatus()
						.equals(ProcurementOrderStatus.FINAL_APPROVED.toString())) {
			errorMap.notify(ErrorDescription.PROCUREMENT_NOT_ABLE_DELETED.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");
		}
	}

	public String deleteProcurementOrder() {
		if (procurementOrderResponseDto == null) {
			log.info("<---Please Select any one Procurement Order--->");
			errorMap.notify(ErrorDescription.SELECT_ANY_ONE_PROCUREMENT.getErrorCode());
			return null;
		}
		String url = SERVER_URL + "/procurement/deleteProcurementOrder";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, procurementOrderResponseDto);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("ProcurementOrder Deleted successfully");
					errorMap.notify(ErrorDescription.PROCUREMENT_DELETED_SUCCESSFULLY.getErrorCode());
					showProcurementList();
					// errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(ErrorDescription.PROCUREMENT_NOT_ABLE_DELETED.getErrorCode());
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in ", exp);
			log.info(
					"ProcurementOrder is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String getAddPage() {

		minDate = null;
		maxDate = null;
		govtProductionPlan = null;
		procurementOrder = null;

		govtSchemePlan = null;
		govtProductionPlanList = new ArrayList<GovtSchemePlan>();

		eventLogList = new ArrayList<>();
		approvedEventLogList = new ArrayList<>();
		rejectedEventLogList = new ArrayList<>();

		editFlag = false;
		procrmntListSize = 0;

		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMasterList = new ArrayList<UomMaster>();
		// govtSchemePlanItems = null;
		// quantity = 0.00;
		quantity = null;
		totalQuantity = 0.00;
		totalValue = 0.00;
		planNote = new ProcurementOrderNote();
		// getDPOfficeCodeName();

		dpOfficeEntity = null;
		regionalEntity = null;
		issrEntity = null;
		dpOfficeEntityList = new ArrayList<EntityMaster>();
		regionalEntityList = new ArrayList<EntityMaster>();
		;
		issrEntityList = new ArrayList<EntityMaster>();

		totalProcPlanQty = 0.00;
		alreadyIssuedQty = 0.00;
		procurementOrderDetails = null;

		loadRegionOfficeEntityList();

		loadEmployeeLoggedInUserDetails();

		return ADD_PAGE_URL;
	}

	public String gotoEditPage() {
		log.info("Inside gotoEditPage()");

		employeeMaster = new EmployeeMaster();
		BaseDTO baseDTO = null;

		if (procurementOrderResponseDto == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getErrorCode());
			return null;
		}

		if (procurementOrderResponseDto.getProcurementStatus().equals(ProcurementOrderStatus.SUBMITTED.toString())
				|| procurementOrderResponseDto.getProcurementStatus().equals(ProcurementOrderStatus.APPROVED.toString())
				|| procurementOrderResponseDto.getProcurementStatus()
						.equals(ProcurementOrderStatus.FINAL_APPROVED.toString())) {
			errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_ONLY_CAN_EDITABLE.getErrorCode());
			return null;
		}

		try {
			String URL = SERVER_URL + "/procurement/get/" + procurementOrderResponseDto.getProcurementOrderId();
			log.info("<--- gotoEditPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				procurementOrder = objectMapper.readValue(jsonResponse, ProcurementOrder.class);
				log.info("===================== ");

				editFlag = true;
				if (procurementOrder != null && procurementOrder.getGovtSchemePlan() != null) {
					govTypeName = "gov";
					for (GovtSchemePlanLog schemeLog : procurementOrder.getGovtSchemePlan()
							.getGovtSchemePlanLogList()) {
						if (schemeLog != null && schemeLog.getCreatedBy() != null
								&& schemeLog.getStage().equals("FINAL_APPROVED")) {
							procurementOrder.getGovtSchemePlan().setApprovedDate(schemeLog.getCreatedDate());
							procurementOrder.getGovtSchemePlan().setApprovedBy(schemeLog.getCreatedBy());
							break;
						}
					}

					govtProductionPlan = "Govt_Scheme";
					govtSchemePlan = procurementOrder.getGovtSchemePlan();
					govtProductionPlanList = new ArrayList<GovtSchemePlan>();
					govtProductionPlanList.add(govtSchemePlan);
				} else if (procurementOrder != null && procurementOrder.getContractExportPlan() != null
						&& procurementOrder.getContractExportPlan().getPlanType().name().equals("Contract")) {
					govTypeName = "contract";
					govtProductionPlan = "Contract";

					for (ContractExportPlanLog conLog : procurementOrder.getContractExportPlan()
							.getContractExportPlanLogList()) {
						if (conLog != null && conLog.getCreatedBy() != null
								&& conLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getContractExportPlan().setApprovedDate(conLog.getCreatedDate());
							procurementOrder.getContractExportPlan().setApprovedBy(conLog.getCreatedBy());
							break;
						}
					}
					contractExportPlan = procurementOrder.getContractExportPlan();
					contractExportPlanList = new ArrayList<>();
					contractExportPlanList.add(contractExportPlan);

				} else if (procurementOrder != null && procurementOrder.getContractExportPlan() != null
						&& procurementOrder.getContractExportPlan().getPlanType().name().equals("Export")) {
					govTypeName = "export";
					govtProductionPlan = "Export";
					for (ContractExportPlanLog conLog : procurementOrder.getContractExportPlan()
							.getContractExportPlanLogList()) {
						if (conLog != null && conLog.getCreatedBy() != null
								&& conLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getContractExportPlan().setApprovedDate(conLog.getCreatedDate());
							procurementOrder.getContractExportPlan().setApprovedBy(conLog.getCreatedBy());
							break;
						}
					}
					contractExportPlan = procurementOrder.getContractExportPlan();
					contractExportPlanList = new ArrayList<>();
					contractExportPlanList.add(contractExportPlan);

				} else if (procurementOrder != null && procurementOrder.getAdditionalProductionPlan() != null) {
					govtProductionPlan = "Additional_Production_Plan";
					govTypeName = "additional";
					for (AdditionalProductionPlanLog addLog : procurementOrder.getAdditionalProductionPlan()
							.getAdditionalProductionPlanLogList()) {
						if (addLog != null && addLog.getCreatedBy() != null
								&& addLog.getStage().name().equals("FINAL_APPROVED")) {
							procurementOrder.getAdditionalProductionPlan().setApprovedDate(addLog.getCreatedDate());
							procurementOrder.getAdditionalProductionPlan().setApprovedBy(addLog.getCreatedBy());
							break;
						}
					}
					additionalProductionPlan = procurementOrder.getAdditionalProductionPlan();
					additionalProductionPlanList = new ArrayList<>();
					additionalProductionPlanList.add(additionalProductionPlan);
				}

				if (procurementOrder.getProcurementOrderDetailsList() != null) {
					for (ProcurementOrderDetails proOrderDetails : procurementOrder.getProcurementOrderDetailsList()) {
						productVarietyMaster = proOrderDetails.getProductVarietyMaster();
						regionalEntity = proOrderDetails.getRegionEntityMaster();
						issrEntity = proOrderDetails.getIssrEntityMaster();
						quantity = proOrderDetails.getItemQuantity();
						validityDate = proOrderDetails.getValidityDate();
						notLaterThanDate = proOrderDetails.getNotLesserDate();
						proOrderDetails.setUomMaster(getUOM());
						proOrderDetails.setRegionEntityMaster(regionalEntity);
						proOrderDetails.setIssrEntityMaster(issrEntity);
					}
				}
				// getQuantitesByAdditional();
				loadISSREntityList();

				dpOfficeEntity = procurementOrder.getEntityMaster();
				dpOfficeEntityList = new ArrayList<EntityMaster>();
				dpOfficeEntityList.add(dpOfficeEntity);

				loadProductList();

				totalQuantity = 0.00;
				totalValue = 0.00;
				for (ProcurementOrderDetails schemeDetails : procurementOrder.getProcurementOrderDetailsList()) {
					if (schemeDetails != null) {
						totalQuantity = totalQuantity + schemeDetails.getItemQuantity();
						totalValue = totalValue + schemeDetails.getItemValue();
					}
				}
				planNote = procurementOrder.getProcurementOrderNoteList().get(0);

				getEmployeeDetailsByUserId();
				loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();

				if (procurementOrder.getProcurementOrderLogList() != null
						&& !procurementOrder.getProcurementOrderLogList().isEmpty()) {
					EmployeeMaster empDetails = null;
					for (ProcurementOrderLog planLog : procurementOrder.getProcurementOrderLogList()) {
						String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
								+ planLog.getCreatedBy().getId();

						BaseDTO dto = httpService.get(url);
						if (dto != null && dto.getStatusCode() == 0) {
							ObjectMapper maper = new ObjectMapper();
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);
						} else {
							AppUtil.addError(" Employee Details Not Found for the User ");
							return null;
						}

						EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
								empDetails.getPersonalInfoEmployment().getDesignation().getName(),
								planLog.getCreatedDate(), planLog.getRemarks());
						if (planLog.getStage().equals(ProcurementOrderStatus.INITIATED.toString())
								|| planLog.getStage().equals(ProcurementOrderStatus.SUBMITTED.toString())) {
							eventLog.setTitle("Created By");
						} else if (planLog.getStage().equals(ProcurementOrderStatus.APPROVED.toString())) {
							eventLog.setTitle("Approved By");
							approvedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(ProcurementOrderStatus.REJECTED.toString())) {
							eventLog.setTitle("Rejected By");
							rejectedEventLogList.add(eventLog);
						}
						/*
						 * else
						 * if(planLog.getStage().equals(ProcurementOrderStatus.SUBMITTED.toString())){
						 * eventLog.setTitle("Submited By"); approvedEventLogList.add(eventLog); }
						 */
						eventLogList.add(eventLog);
					}
				}

				// log.info("eventLogList", eventLogList);

				Collections.sort(eventLogList, new Comparator<EventLog>() {
					public int compare(EventLog m1, EventLog m2) {
						return m1.getTitle().compareTo(m2.getTitle());
					}
				});

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoEditPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return ADD_PAGE_URL;
	}

	public String cancelProcurementOrder() {
		log.info("<=cancelProcurementOrder bean call=>");
		govtProductionPlanList = new ArrayList<>();
		govtSchemePlan = new GovtSchemePlan();
		contractExportPlan = new ContractExportPlan();
		contractExportPlanList = new ArrayList<>();
		additionalProductionPlanList = new ArrayList<>();
		additionalProductionPlan = new AdditionalProductionPlan();
		dpOfficeEntity = new EntityMaster();
		dpOfficeEntity = new EntityMaster();
		regionalEntity = new EntityMaster();
		issrEntity = new EntityMaster();
		dpOfficeEntityList = new ArrayList<>();
		regionalEntityList = new ArrayList<>();
		issrEntityList = new ArrayList<>();
		procurementOrder = new ProcurementOrder();
		loadProcurementnList();
		addButtonFlag = true;
		return LIST_PAGE_URL;
	}

	public String submitApprovedStatus(String status) {
		log.info("<=========== Starts ProcurementOrderBean submitApprovedStatus =========>" + status);
		log.info("<===== Procurement Order Id ========>" + procurementOrderResponseDto.getProcurementOrderId());
		if (status.equalsIgnoreCase(CreditSalesDemandStatus.FINAL_APPROVED.toString())) {
			procurementOrderResponseDto.setProcurementStatus(CreditSalesDemandStatus.FINAL_APPROVED.toString());
		} else {
			procurementOrderResponseDto.setProcurementStatus(CreditSalesDemandStatus.APPROVED.toString());
		}
		procurementOrderResponseDto.setForwardTo(planNote.getUserMaster());
		procurementOrderResponseDto.setFinalApproval(planNote.getFinalApproval());
		log.info("submited info", procurementOrderResponseDto);
		try {
			String url = SERVER_URL + "/procurement/approveProcurementOrder";
			BaseDTO baseDTO = httpService.post(url, procurementOrderResponseDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("<========== Procurement Order Approved Successfully ==========>");
				errorMap.notify(ErrorDescription.CIRCULAR_APPROVE_SUCCESS.getCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=========== Ends ProcurementOrderBean submitApprovedStatus =========>");
		return getListPage();
	}

	public String submitRejectedStatus() {
		log.info("<=========== Starts ProcurementOrderBean submitRejectedStatus =========>");
		log.info("<===== Procurement Order Id ========>" + procurementOrderResponseDto.getProcurementOrderId());
		procurementOrderResponseDto.setProcurementStatus(CreditSalesDemandStatus.REJECTED.toString());
		procurementOrderResponseDto.setForwardTo(planNote.getUserMaster());
		procurementOrderResponseDto.setFinalApproval(planNote.getFinalApproval());
		log.info("submited info", procurementOrderResponseDto);
		try {
			String url = SERVER_URL + "/procurement/rejectProcurementOrder";
			BaseDTO baseDTO = httpService.post(url, procurementOrderResponseDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("<========== Procurement Order Rejected Successfully ==========>");
				errorMap.notify(ErrorDescription.CIRCULAR_REJECTED_SUCCESS.getCode());

			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=========== Ends ProcurementOrderBean submitRejectedStatus =========>");
		return getListPage();
	}

	public void clearDandPOfficeDetails() {
		quantity = 0.0;
		dpOfficeEntity = new EntityMaster();
		productVarietyMaster = new ProductVarietyMaster();
		totalProcPlanQty = 0.0;
		alreadyIssuedQty = 0.0;
		regionalEntity = new EntityMaster();
		issrEntity = new EntityMaster();
		validityDate = null;
		notLaterThanDate = null;
	}

	public void showApprovePopup() {
		RequestContext.getCurrentInstance().execute("PF('dlgComments').show();");
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String procurementOrderId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			procurementOrderId = httpRequest.getParameter("procurementOrderId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		procurementOrderResponseDto = new ProcurementOrderResponseDto();
		if (!StringUtils.isEmpty(notificationId)) {
			init();
			procurementOrderResponseDto.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(procurementOrderId)) {
			procurementOrderResponseDto.setProcurementOrderId(Long.parseLong(procurementOrderId));
			action = "VIEW";
			gotoViewPage();
		}
		return VIEW_PAGE_URL;
	}

}
