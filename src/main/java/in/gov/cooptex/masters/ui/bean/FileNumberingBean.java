package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.util.StringUtil;
import org.apache.xmlbeans.impl.xb.xsdschema.Public;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.FileName;
import in.gov.cooptex.admin.model.FileNumber;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("fileNumberBean")
@Scope("session")
@Log4j2
public class FileNumberingBean {

	final String LIST_PAGE = "/pages/admin/tapal/listFileNumbering.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/admin/tapal/createFileNumbering.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/admin/tapal/viewFileNumbering.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;
	
	
	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	FileNumber fileNumber,selectedFileNumber;
	
	@Getter
	@Setter
	private String fileName;
	
	@Getter @Setter
	List<Department> departmentList = null;
	
	@Getter @Setter
	List<FileName> fileNameList = null;
	
	@Getter @Setter
	Department department;
	
	@Getter @Setter
	SectionMaster sectionMaster;
	
	@Getter @Setter
	List<SectionMaster> sectionMasterList = null;
 
	@Getter @Setter
	LazyDataModel<FileNumber> lazyFileNumberList;

	List<FileNumber> fileNumberList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showFileNumberListPage() {
		log.info("<==== Starts FileNumberBean.showFileNumberListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag=true;
		fileNumber = new FileNumber();
		selectedFileNumber= new FileNumber();
		loadLazyFileNumberList();
		log.info("<==== Ends FileNumberBean.showFileNumberListPage =====>");
		return LIST_PAGE;
	}

	@Autowired
	CommonDataService commonDataService;
	
	public String showFileNumberListPageAction() {
		log.info("<===== Starts FileNumberBean.showSalesTypeListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedFileNumber== null || selectedFileNumber.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_FILENUMBER.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("Create")) {
				log.info("create FileNumber called..");
				fileNumber.setFileName(null);
				setFileName("");
				fileNumber = new FileNumber();
				department=new Department();
				departmentList = new ArrayList<>();
				sectionMasterList = new ArrayList<>();
				fileNameList= new ArrayList<>();
				departmentList=commonDataService.loadDepartmentList();
				fileNameList=loadFileNameList();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit FileNumber called..");
				String url=SERVER_URL+appPreference.getOperationApiUrl()+"/filenumbering/getbyid/"+selectedFileNumber.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					fileNumber = mapper.readValue(jsonResponse,new TypeReference<FileNumber>(){
					});	
					fileNumber.getFileName();
					log.info("cheking"+ selectedFileNumber.getFile());
					setFileName(selectedFileNumber.getFile());
					departmentList=commonDataService.loadDepartmentList();
					fileNameList=loadFileNameList();
					loadSection();
					
					
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else if(action.equalsIgnoreCase("View")){
				log.info("View FileNumber called..");
				
				String url=SERVER_URL+appPreference.getOperationApiUrl()+"/filenumbering/getbyid/"+selectedFileNumber.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					fileNumber = mapper.readValue(jsonResponse,new TypeReference<FileNumber>(){
					});	
					return VIEW_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
			}else{
				log.info("delete FileNumber called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFileNumberDelete').show()");
			}
		}catch(Exception e) {
			log.error("Exception occured in showFileNumberListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends FileNumberBean.showFileNumberListPageAction ===========>"+action);
		return null;
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts FileNumberBean.saveOrUpdate =======>");
		try {
//			if(fileNumber.getFileName() == null) {
//				errorMap.notify(ErrorDescription.FILENUMBERING_FILE_NAME_EMPTY.getCode());
//				return null;
//			}
			log.info("filenameee"+fileName);
			if(fileName==null||StringUtils.isEmpty(fileName))
			{
				errorMap.notify(ErrorDescription.FILENUMBERING_FILE_NAME_EMPTY.getCode());
				return null;
			}
			
			if(fileNumber.getDepartmentMaster() == null) {
				errorMap.notify(ErrorDescription.FILENUMBERING_DEPARTMENT_EMPTY.getCode());
				return null;
			}
			if(fileNumber.getSectionMaster()== null ) {
				errorMap.notify(ErrorDescription.FILENUMBERING_SECTION_EMPTY.getCode());
				return null;
			}
			fileNumber.setFileName(new FileName());
			fileNumber.getFileName().setName(fileName);
			fileNumber.setFile(fileName);
			
			String url = SERVER_URL+appPreference.getOperationApiUrl()+"/filenumbering/saveOrupdate";
			BaseDTO response = httpService.post(url,fileNumber);
			if(response != null && response.getStatusCode() == 0) {
				log.info("FileNumber saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.FILENUMBERING_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.FILENUMBERING_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends FileNumberBean.saveOrUpdate =======>");
		return showFileNumberListPage();
	}
	public void  loadSection() {
		log.info("FileNumberBean - LoadSection-->"+fileNumber.getDepartmentMaster().getId());
		try {
			String url;
			//log.info("department--------"+department.getName());
			BaseDTO baseDTO=new BaseDTO();
			url=appPreference.getPortalServerURL()+appPreference.getOperationApiUrl()+"/incomingtapal/getSectionByDesignation/"+fileNumber.getDepartmentMaster().getName();
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				mapper=new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});	
				log.info("sectionList---------"+sectionMasterList.size());
			}else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		}catch (Exception e) {
			log.info("inside departmentChange method--",e);
		}
	}
	
	public List<FileName>  loadFileNameList() {
		log.info("FileNumberBean - loadFileNameList-->");
		List<FileName> fileNameListvalue=new ArrayList<>();
		try {
			String url;
			BaseDTO baseDTO=new BaseDTO();
			url=appPreference.getPortalServerURL()+appPreference.getOperationApiUrl()+"/filenumbering/getAllFileNameList";
			baseDTO=httpService.get(url);
			if(baseDTO.getStatusCode()==0) {
				mapper=new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				fileNameListvalue = mapper.readValue(jsonResponse, new TypeReference<List<FileName>>() {
				});	
				log.info("loadFileNameList---------"+fileNameListvalue.size());
			}else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		}catch (Exception e) {
			log.info("inside loadFileNameList method--",e);
		}
		return fileNameListvalue;
	}
	
	public void  loadLazyFileNumberList() {
		log.info("<===== Starts FileNumberBean.loadLazySalesTypeList ======>");

		lazyFileNumberList= new LazyDataModel<FileNumber>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6613861142448851255L;

			@Override
			public List<FileNumber> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL+appPreference.getOperationApiUrl()+"/filenumbering/getallFileNumberlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						fileNumberList= mapper.readValue(jsonResponse, new TypeReference<List<FileNumber>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("FileNumberList size---------------->"+fileNumberList.size());
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return fileNumberList;
			}

			@Override
			public Object getRowKey(FileNumber res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FileNumber getRowData(String rowKey) {
				for (FileNumber FileNumber : fileNumberList) {
					if (FileNumber .getId().equals(Long.valueOf(rowKey))) {
						selectedFileNumber= FileNumber ;
						return FileNumber;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  FileNumberBean.loadLazySalesTypeList  ======>");
	}




	public String deleteConfirm() {
		log.info("<===== Starts FileNumberBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL+appPreference.getOperationApiUrl()+"/filenumbering/deletebyid/"+selectedFileNumber.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.FILENUMBERING_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFileNumberDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete FileNumber ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends FileNumberBean.deleteConfirm ===========>");
		return  showFileNumberListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FileNumberBean.onRowSelect ========>"+event);
		selectedFileNumber= ((FileNumber) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends FileNumberBean.onRowSelect ========>");
    }
	
} 
