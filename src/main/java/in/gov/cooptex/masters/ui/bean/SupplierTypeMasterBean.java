package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("supplierTypeMasterBean")
@Scope("session")
@Log4j2
public class SupplierTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listSupplierTypeMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createSupplierTypeMaster.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/viewSupplierTypeMaster.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String SUPPLIER_TYPE_MASTER_URL = AppUtil.getPortalServerURL() + "/suppliertypemaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster, selectedSupplierTypeMaster;

	@Getter
	@Setter
	LazyDataModel<SupplierTypeMaster> lazySupplierTypeMasterList;

	List<SupplierTypeMaster> supplierTypeMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showSupplierTypeMasterListPage() {
		log.info("<==== Starts SupplierTypeMasterBean.showSupplierTypeMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		supplierTypeMaster = new SupplierTypeMaster();
		selectedSupplierTypeMaster = new SupplierTypeMaster();
		loadLazySupplierTypeMasterList();
		log.info("<==== Ends SupplierTypeMasterBean.showSupplierTypeMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazySupplierTypeMasterList() {
		log.info("<===== Starts SupplierTypeMasterBean.loadLazySupplierTypeMasterList ======>");

		lazySupplierTypeMasterList = new LazyDataModel<SupplierTypeMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<SupplierTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SUPPLIER_TYPE_MASTER_URL + "/getallsuppliertypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						supplierTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<SupplierTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return supplierTypeMasterList;
			}

			@Override
			public Object getRowKey(SupplierTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SupplierTypeMaster getRowData(String rowKey) {
				for (SupplierTypeMaster supplierTypeMaster : supplierTypeMasterList) {
					if (supplierTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedSupplierTypeMaster = supplierTypeMaster;
						return supplierTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends SupplierTypeMasterBean.loadLazySupplierTypeMasterList ======>");
	}

	public String showSupplierTypeMasterListPageAction() {
		log.info("<===== Starts SupplierTypeMasterBean.showSupplierTypeMasterListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedSupplierTypeMaster == null || selectedSupplierTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECT_SUPPLIER_TYPE_MASTER).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create supplierTypeMaster called..");
				supplierTypeMaster = new SupplierTypeMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit supplierTypeMaster called..");
				String url = SUPPLIER_TYPE_MASTER_URL + "/getbyid/" + selectedSupplierTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					supplierTypeMaster = mapper.readValue(jsonResponse, new TypeReference<SupplierTypeMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View supplierTypeMaster called..");

				String url = SUPPLIER_TYPE_MASTER_URL + "/getbyid/" + selectedSupplierTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						supplierTypeMaster = mapper.readValue(jsonResponse, new TypeReference<SupplierTypeMaster>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			}

			else {
				log.info("delete supplierTypeMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSupplierTypeMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showSupplierTypeMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends SupplierTypeMasterBean.showSupplierTypeMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts SupplierTypeMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SUPPLIER_TYPE_MASTER_URL + "/deletebyid/" + selectedSupplierTypeMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_DELETE_SUCCESS)
						.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSupplierTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete supplierTypeMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SupplierTypeMasterBean.deleteConfirm ===========>");
		return showSupplierTypeMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts SupplierTypeMasterBean.saveOrUpdate =======>");
		try {
			if (supplierTypeMaster.getCode() == null || supplierTypeMaster.getCode().trim().isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_CODE_EMPTY).getCode());
				return null;
			}
			/*
			 * if (supplierTypeMaster.getSupplierType() == null) {
			 * errorMap.notify(ErrorDescription.SUPPLIER_TYPE_MASTER_TYPE_EMPTY.getCode());
			 * return null; }
			 */
			if (supplierTypeMaster.getName() == null || supplierTypeMaster.getName().trim().isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_NAME_EMPTY).getCode());
				return null;
			}
			if (supplierTypeMaster.getName().trim().length() < 3 || supplierTypeMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_NAME_MIN_MAX_ERROR)
						.getCode());
				return null;
			}
			if (supplierTypeMaster.getLocalName() == null || supplierTypeMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_LNAME_EMPTY).getCode());
				return null;
			}
			if (supplierTypeMaster.getLocalName().trim().length() < 3
					|| supplierTypeMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_LNAME_MIN_MAX_ERROR)
						.getCode());
				return null;
			}
			if (supplierTypeMaster.getActiveStatus() == null) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			String url = SUPPLIER_TYPE_MASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, supplierTypeMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("supplierTypeMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.SUPPLIER_TYPE_MASTER_UPDATE_SUCCESS)
							.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends SupplierTypeMasterBean.saveOrUpdate =======>");
		return showSupplierTypeMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SupplierTypeMasterBean.onRowSelect ========>" + event);
		selectedSupplierTypeMaster = ((SupplierTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends SupplierTypeMasterBean.onRowSelect ========>");
	}

}
