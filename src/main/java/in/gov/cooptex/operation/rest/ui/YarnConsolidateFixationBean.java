package in.gov.cooptex.operation.rest.ui;

import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.operation.dto.YarnConsolidateRequirementListDTO;
import lombok.extern.log4j.Log4j2;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;
import java.util.Calendar;
import java.util.TimeZone;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.YarnConsolidateRequirementDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.model.YarnConsolidateRequirement;
import in.gov.cooptex.operation.model.YarnConsolidateRequirementDetails;
import in.gov.cooptex.operation.production.dto.ProductDesignTargetDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("yarnConsolidateFixationBean")
@Scope("session")
public class YarnConsolidateFixationBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8321969408081921576L;

	@Getter	@Setter
	String month;
	
	@Getter	@Setter
	Long year;

    ObjectMapper mapper;

    String jsonResponse;
    @Autowired
    ErrorMap errorMap;

    @Getter
    @Setter
    int totalRecords = 0;


    @Getter	@Setter
	List<YarnConsolidateRequirement> fromMonthYearList;
	
	@Getter	@Setter
	List<YarnConsolidateRequirement> toMonthYearList;
	
	@Getter	@Setter
	YarnConsolidateRequirement selectedFromMonthYear;
	
	@Getter	@Setter
	YarnConsolidateRequirement selectedToMonthYear;
	
	//Yarn Unit Wise Requirements
	@Getter	@Setter
	Long tenS;
	
	@Getter	@Setter
	Long twentyS;
	
	@Getter	@Setter
	Long twentyTwoS;
	
	@Getter	@Setter
	Long twentyEightS;
	
	@Getter	@Setter
	Long thiryS;
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> yarnUnitWiseRequirementsList;
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> yarnUnitWiseRequirementsGroupList;
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> yarnUnitWiseRequirementsGroupListSecond;
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> headerMenusList;
	
	@Getter	@Setter
	YarnConsolidateRequirementDTO yarnConsolidateRequirementDTO;
//	@Getter	@Setter
//	List<YarnConsolidateRequirementDetails> yarnUnitWiseRequirementsList;
	
	//Purhcase Order Details
	@Getter	@Setter
	List<YarnConsolidateRequirementDetails> purchasePriceDetailsList;
	
	
	//Supplier Details
	
	@Getter	@Setter
	EntityMaster entityMaster; //yarn Category code name

	@Getter	@Setter
	ProductVarietyMaster productVarietyMaster; //yarn type code name
	
//	@Getter	@Setter
//	List<ProductCategory> productCategoryList; // yarnTypeList
	
	@Getter	@Setter
	List<SupplierMaster> supplierMasterList;
	
	@Getter	@Setter
	SupplierMaster supplierMaster;
	
	@Getter	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;
	
	@Getter	@Setter
	SupplierTypeMaster supplierTypeMaster;
	
	
	@Getter	@Setter
	List<YarnConsolidateFixationBean> yarnSupplierList;
	
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> supplierDetailsList;
	
	//Commitee Details Add
//	@Getter	@Setter
//	String commiteeMemberName;
//	
//	@Getter	@Setter
//	String officeName;
//	
//	@Getter	@Setter
//	String designation;
	
	@Getter	@Setter
	List<YarnConsolidateRequirementDTO> commiteeDetailsList;
	
	//forwardTo and forwardFor
	@Getter @Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Getter @Setter
	UserMaster forwardTo;

	@Getter
    @Setter
    String errormessage="";
	
	@Getter @Setter
	Boolean forwardFor;
	
	//End 
	@Autowired
	CommonDataService commonDataService;
	
	@Autowired
	AppPreference appPreference;
	
	@Autowired
	HttpService httpService;
	

	
	@Getter	@Setter
	YarnConsolidateFixationBean yarnPriceFixationBean;
	
	@Getter	@Setter
	String note;
	
	@Getter	@Setter
	Date currentDate;
	
	@Getter	@Setter
	String userName;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	String designationName;
	
	@Getter	@Setter
	Boolean editButtonFlag = true;
	
	@Getter	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter	@Setter
	Boolean addButtonFlag = true;

	@Getter @Setter
    Boolean conditionFlag = false;

    @Getter
    @Setter
    YarnConsolidateRequirementListDTO selecteYarnPriceFixation;


    @Getter
    @Setter
    LazyDataModel<YarnConsolidateRequirementListDTO> lazyYarnPriceFixationList;

    @Getter
    @Setter
    List<YarnConsolidateRequirementDTO> viewYarnPriceFixationdetails = new ArrayList<>();

    @Getter
    @Setter
    YarnConsolidateRequirementDTO viewyarnotherdetails;

    @Getter
    @Setter
    String action;


    @Getter
    @Setter
    List<YarnConsolidateRequirementListDTO> yarnPriceFixationList = null;
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	private final String LIST_PAGE_URL = "/pages/operation/yarn/listYarnPriceFixation.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE_URL = "/pages/operation/yarn/createYarnPriceFixation.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/operation/yarn/viewYarnPriceFixation.xhtml?faces-redirect=true;";
	private final String EDIT_URL = "/pages/operation/yarn/editYarnPriceFixation.xhtml?faces-redirect=true;";
	
//	@PostConstruct
//	public void init() {
//		log.info(":::YarnPriceFixation() loadYarnCategoryCode received postConstruct ");
//
//	}
	public String createYarnPriceFixation() {
		log.info(":::YarnPriceFixation() received createYarnPriceFixation Page");
		loadFromMonthYear();
		loadSupplierType();
		loadForwardToUser();
		currentDate = new Date();
		yarnConsolidateRequirementDTO = new YarnConsolidateRequirementDTO();
		supplierDetailsList = new ArrayList<>();
		commiteeDetailsList = new ArrayList<>();
		addButtonFlag = true;
		editButtonFlag = false;
		viewButtonFlag = false;
		try {
			log.info("load user details");
			userName = loginBean.getUsername();
			designationName = loginBean.getEmployee().getPersonalInfoEmployment().getDesignation().getName();
			log.info("load user details Success");
		} catch (Exception e) {
			log.error("load user details error "+e);
			
		}
		return CREATE_PAGE_URL;
	}
	
	public String showYarnView() {
		try {
			log.info("received list view yarn :: ");
//			addButtonFlag = false;
//			editButtonFlag = true;
//			viewButtonFlag = true;
			return LIST_PAGE_URL;
		} catch (Exception e) {
			log.error("Error to redirect list page "+e);
			// TODO: handle exception
		}
		return null;
	}

    public String showYarnPriceFixationList() {


        addButtonFlag = true;


        loadYarnPriceFixationList();
        return LIST_PAGE_URL;


    }


    public String viewYarnPriceFixation(){
        long yarnpricefixationid = selecteYarnPriceFixation.getId();
        log.info("<============selected id==============>"+yarnpricefixationid);




        try {


            viewyarnotherdetails = new YarnConsolidateRequirementDTO();

                log.info("view meeting list called..");

                String url = SERVER_URL+ appPreference.getOperationApiUrl()+ "/yarnconsolidaterequirement/getconsolidaterequirementdetailsbyid/" + yarnpricefixationid;

                BaseDTO baseDTO = httpService.get(url);
                if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                    ObjectMapper mapper= new ObjectMapper();
                    jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

                    viewYarnPriceFixationdetails = mapper.readValue(jsonResponse,
                            new TypeReference<List<YarnConsolidateRequirementDTO>>() {
                            });

                    jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
                    viewyarnotherdetails = mapper.readValue(jsonResponse,
                            new TypeReference<YarnConsolidateRequirementDTO>(){

                            } );


                    headerMenusList = new ArrayList<>();
                    java.util.Map<EntityMaster, java.util.Map<ProductVarietyMaster, Double>> designationGroupByAvg = viewYarnPriceFixationdetails.stream()
                            .collect(Collectors
                                    .groupingBy(YarnConsolidateRequirementDTO::getEntityMaster, Collectors
                                            .groupingBy(YarnConsolidateRequirementDTO::getProductVarietyMaster, Collectors
                                                    .summingDouble(YarnConsolidateRequirementDTO::getRequiredQuantity))));
                    designationGroupByAvg.forEach((designation, values) -> {
                        //   log.info("Entity And Product :" + designation.getCode()+","+designation.getName());

                        values.forEach((gender, avg) -> {
                            YarnConsolidateRequirementDTO dto = new YarnConsolidateRequirementDTO();
                            dto.setEntityMaster(designation);
//											       log.info("MAP LIST :"+gender.getProductCatCode()+","+gender.getProductCatName() + ":" + avg);
//											       dto.setProductCategory(gender);
//											       dto.setTargetQuantity(avg);
                            dto.setEntityMaster(designation);
                            dto.setProductVarietyMaster(gender);
                            dto.setRequiredQuantity(avg);
                            log.info("E_ID :"+designation.getId()+",P_ID"+gender.getId()+",P_V :"+avg);
                            headerMenusList.add(dto);
                        });
//											   log.info("After grouping Function");
                        //   log.info("");
                    });
                    log.info("Done header group set");
                    log.info("Header row list :"+headerMenusList.size());
                    log.info("Header row :"+headerMenusList);

                    java.util.Map<EntityMaster, Double> designationGroupByAvg2 = viewYarnPriceFixationdetails.stream()
                            .collect(Collectors
                                    .groupingBy(YarnConsolidateRequirementDTO::getEntityMaster, Collectors
                                            .summingDouble(YarnConsolidateRequirementDTO::getRequiredQuantity)));
                    designationGroupByAvg2.forEach((designation, values) -> {
                        YarnConsolidateRequirementDTO dto = new YarnConsolidateRequirementDTO();
                        dto.setEntityMaster(designation);
                        dto.setRequiredQuantity(values);
                        yarnUnitWiseRequirementsGroupList.add(dto);

                    });






                return VIEW_URL;
            }
        } catch (Exception e) {
            errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
        }

        log.info("<<<< ----------Ends viewDetails------ >>>>"+viewYarnPriceFixationdetails);

        return VIEW_URL;

    }

    public String editYarnPriceFixation(){
	    return EDIT_URL;
    }


    public String cancelYarn() {
		addButtonFlag = true;

        selecteYarnPriceFixation = new YarnConsolidateRequirementListDTO();
		return LIST_PAGE_URL;
	}
	
	//load from and date 
	public void loadFromMonthYear() {
		try {
			fromMonthYearList = new ArrayList<YarnConsolidateRequirement>();
			toMonthYearList = new ArrayList<YarnConsolidateRequirement>();
			
			Calendar fromCalendar = Calendar.getInstance();
			Calendar toCalendar = Calendar.getInstance();
			fromCalendar.setTime(new Date());
			toCalendar.add(Calendar.YEAR, 1);
			SimpleDateFormat month = new SimpleDateFormat("MMM");
			SimpleDateFormat year = new SimpleDateFormat("yyyy");
			while(toCalendar.getTime().after(fromCalendar.getTime())) {
				YarnConsolidateRequirement target = new YarnConsolidateRequirement();
				target.setFromMonth(month.format(fromCalendar.getTime()));
				target.setFromYear(Long.valueOf(year.format(fromCalendar.getTime())));
				fromCalendar.add(Calendar.MONTH, 1);
				fromMonthYearList.add(target);
			}
			}catch(Exception se) {
				log.error("YarnPriceFixation Bean () Error to load fromMonthYear ",se);
			}
	}
	public void loadToMonthYear() {
		try {
			toMonthYearList = new ArrayList<YarnConsolidateRequirement>();
			Calendar fromCalendar = Calendar.getInstance();
			Calendar toCalendar = Calendar.getInstance();
			String selectedMonthYear = selectedFromMonthYear.getFromMonth()+"-"+selectedFromMonthYear.getFromYear();
			SimpleDateFormat selectedMonth = new SimpleDateFormat("MMM-yyyy");
			Date dd=selectedMonth.parse(selectedMonthYear);
			
			fromCalendar.setTime(dd);
			toCalendar.setTime(dd);
			toCalendar.add(Calendar.YEAR, 1);
			SimpleDateFormat month = new SimpleDateFormat("MMM");
			SimpleDateFormat year = new SimpleDateFormat("yyyy");
			while(toCalendar.getTime().after(fromCalendar.getTime())) {
				YarnConsolidateRequirement target = new YarnConsolidateRequirement();
				target.setToMonth(month.format(fromCalendar.getTime()));
				target.setToYear(Long.valueOf(year.format(fromCalendar.getTime())));
				fromCalendar.add(Calendar.MONTH, 1);
				toMonthYearList.add(target);
			}
			}catch(Exception se) {
				log.error("YarnPriceFixation Bean () Error to load toMonthYear ",se);
			}
	}
	public void loadYarnU() {
		
	}
	public void loadForwardToUser() {
		forwardToUsersList=commonDataService.loadForwardToUsers();
	}

	public void loadYarnUnitWiseRequirements() {

		log.info("<==============frommonth================>"+selectedFromMonthYear);
		log.info("<==============frommonth================>"+selectedToMonthYear);





		try {

            String s = selectedFromMonthYear.getFromYear() + "-" + selectedFromMonthYear.getFromMonth() + "-" + 01;
            String tod = selectedToMonthYear.getToYear() + "-" + selectedToMonthYear.getToMonth() + "-" + 01;
            SimpleDateFormat smp = new SimpleDateFormat("yyyy-MMM-dd");
            Date dt = smp.parse(s);
            Date tdt = smp.parse(tod);
            LocalDate localDate = dt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int month = localDate.getMonthValue();
            LocalDate localDatet = tdt.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            int tomonth = localDatet.getMonthValue();
            int reqMonth=month+1;
            if (reqMonth == tomonth && selectedFromMonthYear.getFromYear().equals(selectedToMonthYear.getToYear())) {
                conditionFlag = false;


            log.info("form Month ==== " + month + "To Month ==" + tomonth);
            log.info("tryuiopiuy" + dt);
            log.info("From Date ");
            yarnUnitWiseRequirementsList = new ArrayList<>();
            yarnUnitWiseRequirementsGroupList = new ArrayList<>();
            yarnUnitWiseRequirementsGroupListSecond = new ArrayList<>();
            String url = SERVER_URL + appPreference.getOperationApiUrl() + "/yarnconsolidaterequirement/getYarnUnitWiseRequirementsList/"
                    + selectedFromMonthYear.getFromMonth() + "/" + selectedFromMonthYear.getFromYear() + "/"
                    + selectedToMonthYear.getToMonth() + "/" + selectedToMonthYear.getToYear();
            BaseDTO baseDTO = httpService.get(url);
            log.info(":::YarnPriceFixation() YarnConsolidateRequirementDetails Response ");
            if (baseDTO.getStatusCode() == 0) {
                ObjectMapper mapper = new ObjectMapper();
                String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
                yarnUnitWiseRequirementsList = mapper.readValue(jsonResponse, new TypeReference<List<YarnConsolidateRequirementDTO>>() {
                });
                log.info("yarnUnitWiseRequirementsList get Success " + yarnUnitWiseRequirementsList.size());

                headerMenusList = new ArrayList<>();
//				java.util.Map<ProductVarietyMaster, Double> designationGroupByAvg = yarnUnitWiseRequirementsList.stream()
//					                        .collect(Collectors
//					                                .groupingBy(YarnConsolidateRequirementDTO::getProductVarietyMaster, Collectors
//					                                                .summingDouble(YarnConsolidateRequirementDTO::getRequiredQuantity)));
//					designationGroupByAvg.forEach((designation, values) -> {
//						YarnConsolidateRequirementDTO dto = new YarnConsolidateRequirementDTO();
//						dto.setProductVarietyMaster(designation);
//						dto.setRequiredQuantity(values);
//						headerMenusList.add(dto);
//
//					});

                java.util.Map<EntityMaster, java.util.Map<ProductVarietyMaster, Double>> designationGroupByAvg = yarnUnitWiseRequirementsList.stream()
                        .collect(Collectors
                                .groupingBy(YarnConsolidateRequirementDTO::getEntityMaster, Collectors
                                        .groupingBy(YarnConsolidateRequirementDTO::getProductVarietyMaster, Collectors
                                                .summingDouble(YarnConsolidateRequirementDTO::getRequiredQuantity))));
                designationGroupByAvg.forEach((designation, values) -> {
                    //   log.info("Entity And Product :" + designation.getCode()+","+designation.getName());

                    values.forEach((gender, avg) -> {
                        YarnConsolidateRequirementDTO dto = new YarnConsolidateRequirementDTO();
                        dto.setEntityMaster(designation);
//											       log.info("MAP LIST :"+gender.getProductCatCode()+","+gender.getProductCatName() + ":" + avg);
//											       dto.setProductCategory(gender);
//											       dto.setTargetQuantity(avg);
                        dto.setEntityMaster(designation);
                        dto.setProductVarietyMaster(gender);
                        dto.setRequiredQuantity(avg);
                        log.info("E_ID :" + designation.getId() + ",P_ID" + gender.getId() + ",P_V :" + avg);
                        headerMenusList.add(dto);
                    });
//											   log.info("After grouping Function");
                    //   log.info("");
                });
                log.info("Done header group set");
                log.info("Header row list :" + headerMenusList.size());
                log.info("Header row :" + headerMenusList);

                java.util.Map<EntityMaster, Double> designationGroupByAvg2 = yarnUnitWiseRequirementsList.stream()
                        .collect(Collectors
                                .groupingBy(YarnConsolidateRequirementDTO::getEntityMaster, Collectors
                                        .summingDouble(YarnConsolidateRequirementDTO::getRequiredQuantity)));
                designationGroupByAvg2.forEach((designation, values) -> {
                    YarnConsolidateRequirementDTO dto = new YarnConsolidateRequirementDTO();
                    dto.setEntityMaster(designation);
                    dto.setRequiredQuantity(values);
                    yarnUnitWiseRequirementsGroupList.add(dto);

                });

                log.info("Done Entity group set");
//					  for(YarnConsolidateRequirementDTO dto :yarnUnitWiseRequirementsGroupList) {
//				        	for(YarnConsolidateRequirementDTO dto2 :yarnUnitWiseRequirementsList) {
//				        		if(dto.getEntityMaster().getId().equals(dto2.getEntityMaster().getId())) {
//				        			for(YarnConsolidateRequirementDTO dto3 : headerMenusList) {
//
////				        				dto2.setProductVarietyMaster(dto.getProductVarietyMaster());
////					        			dto2.setEntityMaster(dto.getEntityMaster());
////					        			dto2.setId(dto.getId());
////					        			dto2.setRequiredQuantity(dto.getRequiredQuantity());
//					        			log.info("Add Variety "+dto2.getEntityMaster().getId()+","+dto2.getProductVarietyMaster().getId()+","+dto2.getRequiredQuantity());
//					        			yarnUnitWiseRequirementsGroupListSecond.add(dto2);
//
//				        			}
//				        			break;
//				        		}
//				        	}
//					  }

                log.info("Header row list :" + headerMenusList.size());
                log.info("Header row :" + headerMenusList);
//					log.info("yarnUnitWiseRequirementsGroupList row list :"+yarnUnitWiseRequirementsGroupList.size());
//					log.info("yarnUnitWiseRequirementsGroupList row :"+yarnUnitWiseRequirementsGroupList);
            } else {
                log.warn("YarnConsolidateRequirementDetails MasterList Error ");
            }
        } else{



                conditionFlag = true;
                log.info("please select the 1 month diffrence///");
                errormessage ="please select the 1 month diffrence";

                yarnUnitWiseRequirementsList = new ArrayList<>();
            }
		} catch (Exception ee) {
			log.info(":::YarnPriceFixation() YarnConsolidateRequirementDetails Exception Occured ",ee);
		}

	}
	//add supplier details
	public String addSupplierDetails() {
		try {
			log.info("YarnPriceFixation Bean () received addNew Supplier Details");
			if(entityMaster == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_CATEGORY.getErrorCode());
				return null;
			}
			else if(productVarietyMaster == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_TYPE.getErrorCode());
				return null;
			}
			else if(yarnConsolidateRequirementDTO.getRequiredQuantity() == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_TOTAL_REQ_QTY.getErrorCode());
				return null;
			}
			else if(yarnConsolidateRequirementDTO.getAllotedQuanity() == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_ALLOTED_QTY.getErrorCode());
				return null;
			}
			else if(yarnConsolidateRequirementDTO.getBalanceQuantitySupplied() == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_BAL_QTY.getErrorCode());
				return null;
			}
			else if(supplierMaster == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_SUPPLIER.getErrorCode());
				return null;
			}
//			yarnConsolidateRequirementDTO = new YarnConsolidateRequirementDTO();
//			yarnConsolidateRequirementDTO.setProductGroupMaster(productGroupMaster);
			yarnConsolidateRequirementDTO.setEntityMaster(entityMaster);
			yarnConsolidateRequirementDTO.setProductVarietyMaster(productVarietyMaster);
//			yarnConsolidateRequirementDTO.setProductCategory(productCategory);
//			yarnConsolidateRequirementDTO.setTotalRequirementsQuantity(totalRequirementsQuantity);
//			yarnConsolidateRequirementDTO.setAllotedQuanity(allotedQuanity);
//			yarnConsolidateRequirementDTO.setBalanceQuantitySupplied(balanceQuantitySupplied);
			supplierMaster.setSupplierTypeMaster(supplierTypeMaster);
			yarnConsolidateRequirementDTO.setSupplierMaster(supplierMaster);
			supplierDetailsList.add(yarnConsolidateRequirementDTO);
			log.info("entity :"+entityMaster.getCode()+""+entityMaster.getName()+",Pvar :"+productVarietyMaster.getCode()+" "+productVarietyMaster.getName()+
					",TQty:"+yarnConsolidateRequirementDTO.getRequiredQuantity()+",All :"+
					yarnConsolidateRequirementDTO.getAllotedQuanity()+",Bal :"+yarnConsolidateRequirementDTO.getBalanceQuantitySupplied());
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_COMMITTEE_ADD_SUCCESS.getErrorCode());
			log.info("YarnPriceFixation Bean () addNew Supplier Details Done ");
//			clearSupplierDetails();
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_COMMITTEE_ADD_FAILED.getErrorCode());
			log.error("YarnPriceFixation Bean () addNew Supplier Details Error ",e);
		}
		return null;
	}
	
	public void clearSupplierDetails() {
		yarnConsolidateRequirementDTO = new YarnConsolidateRequirementDTO();
		supplierMaster =null;
		entityMaster = null;
		supplierTypeMaster = null;
		supplierDetailsList = null;
		productVarietyMaster = null;
	}
	//add Commitee Details List
	public String addCommiteeDetails() {
		try {
			log.info("YarnPriceFixation Bean () received addNew Commitee Details");
			if(yarnConsolidateRequirementDTO.getCommiteeMemberName() == null || yarnConsolidateRequirementDTO.getCommiteeMemberName().trim().length() ==0) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_INVALID_COMMITTEE_MEMBER.getErrorCode());
				return null;
			}
			if(yarnConsolidateRequirementDTO.getOfficeName() == null || yarnConsolidateRequirementDTO.getOfficeName().trim().length() ==0) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_INVALID_COMMITTEE_OFFICE.getErrorCode());
				return null;
			}
			if(yarnConsolidateRequirementDTO.getDesignation() == null || yarnConsolidateRequirementDTO.getDesignation().trim().length() ==0) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_INVALID_COMMITTEE_DESIGNATION.getErrorCode());
				return null;
			}
			
//			yarnPriceFixationBean = new YarnConsolidateFixationBean();
//			yarnPriceFixationBean.setCommiteeMemberName(commiteeMemberName);
//			yarnPriceFixationBean.setOfficeName(officeName);
//			yarnPriceFixationBean.setDesignation(designation);
			commiteeDetailsList.add(yarnConsolidateRequirementDTO);
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_COMMITTEE_ADD_SUCCESS.getErrorCode());
			log.info("YarnPriceFixation Bean () addNew Commitee Details Done ");
			clearCommiteeDetails();
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_COMMITTEE_ADD_FAILED.getErrorCode());
			log.error("YarnPriceFixation Bean () addNew Commitee Details Error ",e);
		}
		return null;
	}
	
	public String calculateBalQnty() {
		try {
			log.info("YarnPriceFixation Bean () calculateBalQnty received");
			if(yarnConsolidateRequirementDTO.getRequiredQuantity() == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_TOTAL_REQ_QTY.getErrorCode());
				return null;
			}
					
			if(yarnConsolidateRequirementDTO.getAllotedQuanity() == null) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_ALLOTED_QTY.getErrorCode());
				return null;
			}
				Long a=yarnConsolidateRequirementDTO.getAllotedQuanity();
double d =  yarnConsolidateRequirementDTO.getRequiredQuantity();
			Long v= (new Double(d)).longValue();
			log.info("alloteed value"+ v +""+a);
//			Long balQty = Long.parseLong(yarnConsolidateRequirementDTO.getRequiredQuantity().toString()) - yarnConsolidateRequirementDTO.getAllotedQuanity();
            Long balQty= v - a;
			if(balQty<0) {
				errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_INVALID_BAL_QTY.getErrorCode());
				yarnConsolidateRequirementDTO.setBalanceQuantitySupplied(null);
				return null;
			}else {
				yarnConsolidateRequirementDTO.setBalanceQuantitySupplied(balQty);
			}
		} catch (Exception e) {
			log.error("YarnPriceFixation Bean () calculateBalQnty Error "+e);
			// TODO: handle exception
		}
		return null;
	}
	
	//save yarn
	public String submitYarnFixation() {
		log.info("YarnPriceFixation Bean () submit yarn Save received ");
		if(forwardTo == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDTO.getErrorCode());
			return null;
		}
		if(forwardFor == null) {
			errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDFOR.getErrorCode());
			return null;
		}
		if(note == null || note.trim().length() == 0) {
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_CREATE_NOTE_EMPTY.getErrorCode());
			return null;
		}
		if(commiteeDetailsList == null || commiteeDetailsList.size() ==0) {
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_COMMITTEE_TABLE_NULL.getErrorCode());
			return null;
		}
		if(supplierDetailsList == null || supplierDetailsList.size() ==0) {
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SUPPLIER_TABLE_NULL.getErrorCode());
			return null;
		}
		try {

			for(YarnConsolidateRequirementDTO dto : yarnUnitWiseRequirementsList) {
				if(dto.getPurchasePrice() == null) {
					errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_PURCHASE_TABLE_PRICE_NULL.getErrorCode());
					return null;
				}
				if(dto.getValidityDate() ==null) {
					errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_PURCHASE_TABLE_VALIDITY_DATE_NULL.getErrorCode());
					return null;
				}
				else {
					log.info("U_Unit :"+dto.getEntityMaster().getCode()+" / "+dto.getEntityMaster().getName()+""
							+ ",Y_Count :"+dto.getProductVarietyMaster().getCode() +" / "+dto.getProductVarietyMaster().getName()+""
									+ ",Tota : "+dto.getTotalRequirementsQuantity()+",P_Date :"+
										dto.getPurchasePrice()+",V_Date :"+dto.getValidityDate());
				}
			}

			
			log.info("commiteeDetailsList "+commiteeDetailsList.size());
			log.info("supplierDetailsList "+supplierDetailsList.size());
			
			yarnConsolidateRequirementDTO.setCommiteeDetailsList(commiteeDetailsList);
			yarnConsolidateRequirementDTO.setSupplierDetailsList(supplierDetailsList);
			yarnConsolidateRequirementDTO.setPurchasePriceDetailsList(yarnUnitWiseRequirementsList); //purchasePriceDetailsList
			yarnConsolidateRequirementDTO.setForwardTo(forwardTo);
			yarnConsolidateRequirementDTO.setForwardFor(forwardFor);
			yarnConsolidateRequirementDTO.setFromMonthYear(selectedFromMonthYear);
			yarnConsolidateRequirementDTO.setToMonthYear(selectedToMonthYear);
			yarnConsolidateRequirementDTO.setNote(note);
			yarnConsolidateRequirementDTO.setYarnConsolidateRequirement(yarnUnitWiseRequirementsList.get(0).getYarnConsolidateRequirement());

            double d =  yarnConsolidateRequirementDTO.getRequiredQuantity();
            Long v= (new Double(d)).longValue();
			yarnConsolidateRequirementDTO.setTotalRequirementsQuantity(v);
			BaseDTO baseDTO = null;
			try {
				String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/yarnconsolidaterequirement/createYarnConsolidate";
				log.info("::: Supplier Controller calling  1 :::");
				baseDTO = httpService.post(url,yarnConsolidateRequirementDTO);
				log.info("::: get Supplier Response :");
				if (baseDTO.getStatusCode()==0) {
					log.info("Supplier load Successfully ");
// 					ObjectMapper mapper = new ObjectMapper();
//				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//					supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
//					});
					log.info("StockItemInwardList Page Convert Error -->");

				}
				else{
					log.warn("Supplier Error ");
				}
			} catch (Exception ee) {
				log.error("Exception Occured in Supplier load ", ee);
			}
			log.info("YarnPriceFixation Bean (), save success");
//			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SAVE_FAILED.getErrorCode());
//			log.warn("YarnPriceFixation Bean (), save failed");
		} catch (Exception e) {
			log.error("YarnPriceFixation Bean (), Error save new",e);
			errorMap.notify(ErrorDescription.YARN_PRICE_FIXATION_SAVE_ERROR.getErrorCode());
		}
        yarnConsolidateRequirementDTO =  new YarnConsolidateRequirementDTO();
		return LIST_PAGE_URL ;
	}
	public void clearCommiteeDetails() {
		log.info("YarnPriceFixation Bean () received clear New Commitee Details");
		yarnConsolidateRequirementDTO = new YarnConsolidateRequirementDTO();
	}
	//supplier Details collector 
	//Load Supplier Type List 
	public void loadSupplierType() {
		log.info(":::<- Load Supplier TypeStart ->:::");
		BaseDTO baseDTO = null;
		try {
			supplierTypeMasterList=new ArrayList<SupplierTypeMaster>();
			supplierMasterList=new ArrayList<SupplierMaster>();
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllSupplierType";
			log.info("::: Supplier Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get Supplier Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("Supplier load Successfully "+baseDTO.getTotalRecords());
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			}
			else{
				log.warn("Supplier Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Supplier load ", ee);
		}
		
	}
	
	public void noteAction() {
		log.info("noteAction received -->");
		if(note == null || note.trim().length() == 0) {
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_CREATE_NOTE_EMPTY.getErrorCode());
		}else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('notedialog').hide();");
		}
	}
	
	//Load Supplier Code / Name
	public void loadSupplierCodeName() {
		try {
			log.info(":::<- Load loadSupplierCodeName Called ->::: "+supplierTypeMaster.getCode());
			BaseDTO baseDTO = null;
			supplierMaster = new SupplierMaster();
//			supplierMaster.setId(supplierTypeMaster.getId());
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/yarnconsolidaterequirement/getSelectedSupplierMasterDetails/"+supplierTypeMaster.getId();
			log.info("::: supplierMaster Controller calling  :::");
			baseDTO = httpService.get(url);
			log.info("::: get supplierMaster Response :");
			if (baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMaster load Successfully "+baseDTO.getTotalRecords());
			}
			else{
				log.warn("supplierMaster Load Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public void onSlectingyarnTypeCode(){
	    log.info("<==============slected type ============>"+productVarietyMaster.getName());


        for (YarnConsolidateRequirementDTO obj: yarnUnitWiseRequirementsList ){
            log.info("<=========gwtrwe============>"+obj.getProductVarietyMasterID()+" "+productVarietyMaster.getId());
            if (obj.getProductVarietyMasterID()== productVarietyMaster.getId()) {
                yarnConsolidateRequirementDTO.setRequiredQuantity(obj.getRequiredQuantity());
log.info("<=========gwtrwe============>"+obj.getRequiredQuantity());

            }

        }

    }
    public void loadYarnPriceFixationList(){
        log.info("<<<< ----------Starts lazyLoadMeetingList------- >>>>");

        lazyYarnPriceFixationList = new LazyDataModel<YarnConsolidateRequirementListDTO>() {
            private static final long serialVersionUID = 8422543223567350599L;

            @Override
            public List<YarnConsolidateRequirementListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
                                             Map<String, Object> filters) {
                try {
                    PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
                            sortOrder.toString(), filters);
                    log.info("Pagination request :::" + paginationRequest);
                    String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/yarnconsolidaterequirement/searchyarnconsolidate";


                    BaseDTO response = httpService.post(url, paginationRequest);
                    if (response != null && response.getStatusCode() == 0) {
                        ObjectMapper mapper= new ObjectMapper();
                        jsonResponse = mapper.writeValueAsString(response.getResponseContents());
                        yarnPriceFixationList = mapper.readValue(jsonResponse,
                                new TypeReference<List<YarnConsolidateRequirementListDTO>>() {
                                });
                        this.setRowCount(response.getTotalRecords());
                        totalRecords = response.getTotalRecords();
                    } else {
                        yarnPriceFixationList = new ArrayList<>();
                        errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
                    }
                } catch (Exception e) /*
                 * showBloodGroupListPageAction method used to action based view pages and
                 * render viewing
                 */ {
                    log.error("Exception occured in meetingList ...", e);
                    errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
                }
                log.info("Ends lazy load....");
                return yarnPriceFixationList;
            }

            @Override
            public Object getRowKey(YarnConsolidateRequirementListDTO res) {
                return res != null ? res.getId() : null;
            }

            @Override
            public YarnConsolidateRequirementListDTO getRowData(String rowKey) {
                try {
                    for (YarnConsolidateRequirementListDTO invoice : yarnPriceFixationList) {
                        if (invoice.getId().equals(Long.valueOf(rowKey))) {
                            selecteYarnPriceFixation = invoice;
                            return invoice;
                        }
                    }
                } catch (Exception e) {
                    log.error("Exception occured in getRowData ...", e);
                    errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
                }
                return null;
            }

        };
        log.info("<<<< ----------Ends lazyLoadMeetingList------- >>>>"+yarnPriceFixationList);

    }

    public void onRowSelect(SelectEvent event){

        log.info("<<<< ----------Starts onRowSelect------ >>>>");

        addButtonFlag=false;
        viewButtonFlag=true;
        editButtonFlag=true;
        selecteYarnPriceFixation = ((YarnConsolidateRequirementListDTO) event.getObject());

        log.info("<<<< ----------Ends onRowSelect------ >>>>");

    }
 public String yarnFromDateFormat(Date d){
     log.info("<<<< ----------Starts MeetingSEndTime formate changing------ >>>>");

     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-yyyy");

     log.info("<<<< ----------Ends MeetingSEndTime formate changing------ >>>>");
     return simpleDateFormat.format(d);


 }

 public String yarnToDateFormat(Date d){
     log.info("<<<< ----------Starts MeetingSEndTime formate changing------ >>>>");

     SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-yyyy");

     log.info("<<<< ----------Ends MeetingSEndTime formate changing------ >>>>");
     return simpleDateFormat.format(d);


 }

 public String yarnCreatedDateFormat(Date d){

         log.info("<<<< ----------Starts MeetingSEndTime formate changing------ >>>>");

         SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

         log.info("<<<< ----------Ends MeetingSEndTime formate changing------ >>>>");
         return simpleDateFormat.format(d);

 }

}
