package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.employeeresign.dto.EmployeeResignationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.mis.hrms.dto.EmpGeneralInfoDto;
import in.gov.cooptex.operation.hrms.model.EmployeeResignation;
import in.gov.cooptex.personnel.hrms.model.EmpResignationLog;
import in.gov.cooptex.personnel.hrms.model.EmpResignationNote;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("resignationBean")
public class ResignationBean extends CommonBean {

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String pageResignation;

	@Getter
	@Setter
	EmpGeneralInfoDto empGeneralInfoDto = new EmpGeneralInfoDto();

	@Getter
	@Setter
	LazyDataModel<EmployeeResignation> lazyEmployeeResignation;

	@Getter
	@Setter
	EmployeeResignation selectedEmployeeResignation = new EmployeeResignation();

	@Getter
	@Setter
	EmployeeResignation employeeResignation = new EmployeeResignation();

	String jsonResponse;

	public static final String RESIGNATION_SERVER_URL = AppUtil.getPortalServerURL() + "/resignation";

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton, isApproved, isRejected, finalApproval;

	@Getter
	@Setter
	int resTotalRecord = 0;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EmployeeMaster employee = new EmployeeMaster();

	@Getter
	@Setter
	String resignationNote, fileName, note, remarks;

	@Getter
	@Setter
	EmpResignationNote empResignationNote = new EmpResignationNote();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	private UploadedFile resignationDocument;

	@Getter
	@Setter
	EmpResignationLog empResignationLog = new EmpResignationLog();

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	Date relivingDate;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Double depositeDetectionAmount = 0.0;

	@PostConstruct
	public void init() {
		log.info("ResignationBean Init is executed.....................");
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public String resignationPages() {
		try {
			if (pageResignation.equalsIgnoreCase("ADD")) {

				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.RESIGNATION.name());

				return "/pages/personnelHR/createResignationDetails.xhtml?faces-redirect=true";
			} else if (pageResignation.equalsIgnoreCase("EDIT")) {
				employeeResignation = getResignationDetails(selectedEmployeeResignation.getId());
				employee = employeeResignation.getEmployee();
				return "/pages/personnelHR/createResignationDetails.xhtml?faces-redirect=true";
			} else if (pageResignation.equalsIgnoreCase("VIEW")) {
				calculateDepositeDetectionAmount();
				if (selectedEmployeeResignation == null) {
					errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
					return null;
				}
				employeeResignation = getResignationDetails(selectedEmployeeResignation.getId());
				getEmployeeResignationNoteDetails(employeeResignation.getId());
				employee = employeeResignation.getEmployee();
				// forwardToUsersList =
				// commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.RESIGNATION.name());
				if (empResignationNote == null) {
					empResignationNote = new EmpResignationNote();
				}
				if (employee != null && employee.getId() != null) {
					getEmpGeneralInfo(employee.getId());
				}

				return "/pages/personnelHR/viewResignation.xhtml?faces-redirect=true";
			} else if (pageResignation.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				lasyLoadResignation();
				return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
			} else if (pageResignation.equalsIgnoreCase("DELETE")) {
				log.info(":: Resignation Bean Delete Response :: ");
				if (selectedEmployeeResignation == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedEmployeeResignation.getResignationStatus().name().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			}
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	private void getEmpGeneralInfo(Long employeeId) {
		log.info("<===Starts EmpRetirementRequestBean.getEmpGeneralInfo ========>");
		try {
			empGeneralInfoDto = new EmpGeneralInfoDto();
			String url = AppUtil.getPortalServerURL() + "/employee/retirementrequest/employeegeneralinfo/" + employeeId;
			log.info("<=EmpRetirementRequestBean :: getEmpGeneralInfo :: url=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empGeneralInfoDto = mapper.readValue(jsonResponse, new TypeReference<EmpGeneralInfoDto>() {
				});
				empGeneralInfoDto.setEncashmentAmount(0D);
				empGeneralInfoDto.setUnEarnedAmount(0D);
				empGeneralInfoDto.setAmountDueToTotalAmount(empGeneralInfoDto.getGratuityAmount()
						+ empGeneralInfoDto.getOtherAmount() + empGeneralInfoDto.getEncashmentAmount()
						+ empGeneralInfoDto.getUnEarnedAmount() + empGeneralInfoDto.getRetirementBenefits()
						+ empGeneralInfoDto.getEmployeeSecurityDeposit() + empGeneralInfoDto.getBalanceGratuity());
				empGeneralInfoDto.setTotalAmountToBeSettled(
						empGeneralInfoDto.getAmountDueToTotalAmount() - empGeneralInfoDto.getAmountDueByTotalAmount());
				Float approxmateAmount = (float) Math.ceil(empGeneralInfoDto.getTotalAmountToBeSettled());
				empGeneralInfoDto.setApproximateAmountSettled(approxmateAmount);
			}
		} catch (Exception e) {
			log.info("<===Exception Occured in EmpRetirementRequestBean.getEmpGeneralInfo ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.getEmpGeneralInfo ========>");
	}

	public void clear() {
		employee = new EmployeeMaster();
		employeeResignation = new EmployeeResignation();
	}

	public String cancel() {
		try {
			selectedEmployeeResignation = new EmployeeResignation();
			isAddButton = true;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			lasyLoadResignation();
			return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private EmployeeResignation getResignationDetails(Long selectedId) {
		log.info("<====== resignation.getResignationDetails starts====>" + selectedId);
		try {
			if (selectedEmployeeResignation == null || selectedEmployeeResignation.getId() == null) {
				log.error(" No Loan selected ");
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_SELECT.getErrorCode());
			}
			log.info("Selected resignation  Id : : " + selectedEmployeeResignation.getId());
			String getUrl = RESIGNATION_SERVER_URL + "/get/:id";
			String url = getUrl.replace(":id", selectedEmployeeResignation.getId().toString());

			log.info("resignation Viw By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeResignation resignationDetail = mapper.readValue(jsonResponse, EmployeeResignation.class);

			if (resignationDetail != null) {
				employeeResignation = resignationDetail;
			} else {
				log.error("Loan object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" resignation Retrived  SuccessFully ");
			} else {
				log.error(" resignation Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeResignation;
		} catch (Exception e) {
			log.error("ExceptionException Ocured while get resignation Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public EmployeeMaster searchEmplyee() {
		try {
			log.info("<=searchEmplyee call=>");

			if (employee == null || employee.getEmpCode() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  code : : " + employee.getEmpCode());
			String getUrl = RESIGNATION_SERVER_URL + "/get/employee/:id";
			String url = getUrl.replace(":id", employee.getEmpCode().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employee = mapper.readValue(jsonResponse, EmployeeMaster.class);

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employee;
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		selectedEmployeeResignation = ((EmployeeResignation) event.getObject());
		if (selectedEmployeeResignation.getStatus() != null
				&& selectedEmployeeResignation.getStatus().equalsIgnoreCase("APPROVED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedEmployeeResignation.getStatus() != null
				&& selectedEmployeeResignation.getStatus().equalsIgnoreCase("REJECTED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = true;
		} else if (selectedEmployeeResignation.getStatus() != null
				&& selectedEmployeeResignation.getStatus().equalsIgnoreCase("SUBMITTED")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public void lasyLoadResignation() {

		lazyEmployeeResignation = new LazyDataModel<EmployeeResignation>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<EmployeeResignation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					EmployeeResignation request = new EmployeeResignation();

					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(RESIGNATION_SERVER_URL + "/lazyload", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<EmployeeResignation> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeeResignation>>() {
							});
					if (data == null) {
						log.info(" :: Employee Resignation Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						resTotalRecord = baseDTO.getTotalRecords();
						log.info(":: admin Resignation Search Successfully Completed ::");
					} else {
						log.error(":: admin Resignation Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeResignation res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeResignation getRowData(String rowKey) {
				List<EmployeeResignation> responseList = (List<EmployeeResignation>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeResignation res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeResignation = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String saveAdminResignation() {
		try {
			log.info("<---- : Save job Details employeeResignation :------>" + employeeResignation);

			if (employeeResignation.getResigntionComments() == null) {
				log.error("Please enter comments");
				errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_COMMENTS_REQUIRED.getErrorCode());
				return null;
			}

			employeeResignation.setEmployeeMaster(employee);
			employeeResignation.setDepartment(employee.getPersonalInfoEmployment().getDepartment());
			employeeResignation.setDesignation(employee.getPersonalInfoEmployment().getDesignation());
			employeeResignation.setEntityType(employee.getPersonalInfoEmployment().getEntityType());
			employeeResignation.setHeadAndRegionOffice(employee.getPersonalInfoEmployment().getHeadAndRegionOffice());
			employeeResignation.setWorkLocation(employee.getPersonalInfoEmployment().getWorkLocation());
			employeeResignation.setResignationStatus(ApprovalStatus.INPROGRESS);

			employeeResignation.setEmpResignationNote(empResignationNote);

			String url = RESIGNATION_SERVER_URL + "/createResignation";
			BaseDTO baseDTO = httpService.put(url, employeeResignation);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_SAVED_SUCCESSFULLY.getErrorCode());
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				lasyLoadResignation();
				return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving Job Application with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(" Error Occured while loading employee form ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts Resignation Bean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(RESIGNATION_SERVER_URL + "/delete/" + selectedEmployeeResignation.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_DELETED_SUCCESSFULLY.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting Resignation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends EmployeeResignationBean.deleteConfirm ---->");
		return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
	}

	public String clearList() {
		log.info("Resignation Bean clear method called...");
		selectedEmployeeResignation = new EmployeeResignation();
		lasyLoadResignation();
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
	}

	public void addNote() {
		log.info("<---setResignationNote method Start ---->");
		try {
			empResignationNote.setNote(resignationNote);
		} catch (Exception ex) {
			log.info("<---Error in setResignationNote method---->", ex);
		}
		log.info("<---setResignationNote method End---->");
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			resignationDocument = event.getFile();
			long size = resignationDocument.getSize();
			log.info("Employee Resignation document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				employeeResignation.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_RESIGNATION_VRS_UPLOAD_PATH");

			fileName = resignationDocument.getFileName();
			String docPath = Util.fileUpload(uploadPath, resignationDocument);
			employeeResignation.setDocumentPath(docPath);
			errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_TRANSFER_DOCUMENT_UPLOADED.getErrorCode());

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String approveEmpResignationDetails() {
		log.error(" approveEmpResignationDetails method Start------- ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			Boolean finalApprovalFlag = null;
			if (finalApproval == null) {
				finalApprovalFlag = empResignationNote.getFinalApproval();
			} else {
				finalApprovalFlag = finalApproval;
			}

			empResignationNote = new EmpResignationNote();
			empResignationNote.setNote(resignationNote);
			empResignationNote.setFinalApproval(finalApprovalFlag);
			empResignationNote.setForwardTo(forwardTo);
			empResignationLog.setRemarks(remarks);
			if (empResignationNote.getFinalApproval() == true) {
				empResignationLog.setStage(ApprovalStage.FINAL_APPROVED);
			} else {
				empResignationLog.setStage(ApprovalStage.APPROVED);
			}
			employeeResignation.setEmpResignationNote(empResignationNote);
			employeeResignation.setEmpResignationLog(empResignationLog);
			employeeResignation.getEmployee().getPersonalInfoEmployment().setRelievingDate(relivingDate);
			String url = AppUtil.getPortalServerURL() + "/resignation/emprequest/approve";
			baseDTO = httpService.post(url, employeeResignation);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.EMPLOYEE_RESIGNATION_APPROVED_SUCCESSFULLY)
						.getErrorCode());
				return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
			}

		} catch (Exception exp) {
			log.error("Error in approveEmpResignationDetails method------- ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.error(" approveEmpResignationDetails method End------- ");
		return null;
	}

	public String rejectEmpResignationDetails() {
		log.error(" rejectEmpResignationDetails method Start------- ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			empResignationLog.setRemarks(remarks);
			empResignationLog.setStage(ApprovalStage.REJECTED);
			employeeResignation.setEmpResignationLog(empResignationLog);

			String url = AppUtil.getPortalServerURL() + "/resignation/emprequest/reject";
			baseDTO = httpService.post(url, employeeResignation);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_REJECTED_SUCCESSFULLY.getErrorCode());
				return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
			}

		} catch (Exception exp) {
			log.error("Error in rejectEmpResignationDetails method------- ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.error(" rejectEmpResignationDetails method End------- ");
		return null;
	}

	public void getEmployeeResignationNoteDetails(Long empResignationId) {
		log.info("getEmployeeResignationNoteDetails Method Start====>");
		BaseDTO baseDTO = new BaseDTO();

		try {
			String url = AppUtil.getPortalServerURL() + "/resignation/getnote/" + empResignationId;
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empResignationNote = mapper.readValue(jsonResponse, new TypeReference<EmpResignationNote>() {
				});
			}
			if (empResignationNote == null) {
				isApproved = true;
				isRejected = true;
			} else if (empResignationNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
				isApproved = true;
				isRejected = true;
			} else {
				isApproved = false;
				isRejected = false;
			}
		} catch (Exception ex) {
			log.info("Error in getEmployeeResignationNoteDetails Method====>", ex);
		}
		log.info("getEmployeeResignationNoteDetails Method End====>");
	}

	public void calculateDepositeDetectionAmount() {
		log.info("Resignation Bean:calculateDepositeDetectionAmount Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			EmployeeResignationDTO employeeResignationDTO = new EmployeeResignationDTO();
			employeeResignationDTO.setId(selectedEmployeeResignation.getId());
			// CR-On the date before 01-01-2017 the 6 months gross salary is recovered from
			// the employee in case they have not completed the 3 years of service.
			// CR-On the date After 01-01-2017 the 3 months gross salary is recovered from
			// the employee in case they have not completed the 3 years of service.
			// working period should be less than 3 years to calculate gross salary and if
			// joing is before 2017 it is calculate for 6 month else 3 month should be
			// calculated
			String date = commonDataService.getAppKeyValue("RESIGNATION_DEPOSITE_DETECTION_DATE");
			employeeResignationDTO.setDate(date);
			String url = AppUtil.getPortalServerURL() + "/resignation/getdepositedetectionamt";
			baseDTO = httpService.post(url, employeeResignationDTO);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getDepositAmount());
				depositeDetectionAmount = mapper.readValue(jsonResponse, new TypeReference<Double>() {
				});
			}
			log.info("Deposite detection amount" + depositeDetectionAmount);
		} catch (Exception ex) {
			log.info("Error in calculateDepositeDetectionAmount", ex);
		}
		log.info("Resignation Bean:calculateDepositeDetectionAmount Method End===>");
	}

	public String saveDepositeDetectionAmount() {
		log.error(" saveDepositeDetectionAmount method Start------- ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			empResignationNote = new EmpResignationNote();
			empResignationNote.setNote(resignationNote);
			empResignationNote.setFinalApproval(finalApproval);
			empResignationNote.setForwardTo(forwardTo);
			empResignationLog.setRemarks(remarks);
			empResignationLog.setStage(ApprovalStatus.INPROGRESS.toString());
			employeeResignation.setDepositeDetectionAmount(depositeDetectionAmount);
			employeeResignation.setEmpResignationNote(empResignationNote);
			employeeResignation.setEmpResignationLog(empResignationLog);
			String url = AppUtil.getPortalServerURL() + "/resignation/saveDepositeDetectionamt";
			baseDTO = httpService.post(url, employeeResignation);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				AppUtil.addInfo("Resignation Submitted Successfully");
				return "/pages/personnelHR/listResignation.xhtml?faces-redirect=true";
			}

		} catch (Exception exp) {
			log.error("Error in saveDepositeDetectionAmount method------- ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.error(" saveDepositeDetectionAmount method End------- ");
		return null;
	}
}