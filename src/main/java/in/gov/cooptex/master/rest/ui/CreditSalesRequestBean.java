package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.CreditSalesRequest;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("creditSalesRequestBean")
public class CreditSalesRequestBean {

	private final String CREATE_PAGE = "/pages/masters/creditSalesRequest/createCreditSalesRequest.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/masters/creditSalesRequest/listCreditSalesRequest.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/creditSalesRequest/viewCreditSalesRequest.xhtml?faces-redirect=true";

	private final String LIST_URL = "/creditsalesrequest/getAll";
	private final String CREATE_URL = "/creditsalesrequest/create";
	private final String DELETE_URL = "/creditsalesrequest/delete";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	CreditSalesRequest creditSalesRequest;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<CreditSalesRequest> creditSalesRequestList = new ArrayList<CreditSalesRequest>();

	@Getter
	@Setter
	private int creditSalesRequestListSize;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@PostConstruct
	public void init() {
		log.info(" CreditSalesRequestBean init --->" + serverURL);
		addButtonFlag = true;
		loadValues();
		addCreditSalesRequest();
		getAllCreditSalesRequestList();
		getCreditSalesRequestView(creditSalesRequest);
	}

	public String showList() {
		log.info("<---------- Loading CreditSalesRequestBean showList list page---------->");
		loadValues();
		addButtonFlag = true;
		getAllCreditSalesRequestList();
		return LIST_PAGE;
	}

	public String addCreditSalesRequest() {
		log.info("<---- Start CreditSalesRequestBean.addCreditSalesRequest ---->");
		try {
			action = "ADD";
			creditSalesRequest = new CreditSalesRequest();
			creditSalesRequest.setActiveStatus(true);

			log.info("<---- End CreditSalesRequest.addCreditSalesRequest ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	private void getAllCreditSalesRequestList() {
		log.info("<<<< ----------Start creditSalesRequestBean-getAllCreditSalesRequestList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.get(url);
			creditSalesRequest = new CreditSalesRequest();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				creditSalesRequestList = mapper.readValue(jsonResponse, new TypeReference<List<CreditSalesRequest>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (creditSalesRequestList != null) {
			creditSalesRequestListSize = creditSalesRequestList.size();
		} else {
			creditSalesRequestListSize = 0;
		}
		log.info("<<<< ----------End CreditSalesRequestBean . getAllCreditSalesRequestList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			creditSalesRequest = new CreditSalesRequest();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to CreditSalesRequest List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... CreditSalesRequestBean submit begin .... " + action + " " + creditSalesRequest);
		try {

			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + CREATE_URL, creditSalesRequest);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("CreditSalesRequestBean saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.CREDIT_SALES_REQ_CREATED_SUCCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.CREDIT_SALES_REQ_UPDATED_SUCCESSFULLY.getCode());
					showList();
					// errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating CreditSalesRequest" + e);
		}
		log.info("...... CreditSalesRequestBean submit ended ....");
		return LIST_PAGE;
	}

	public String editCreditSalesRequest() {
		log.info("<<<<< -------Start editCreditSalesRequest called ------->>>>>> ");
		action = "EDIT";

		if (creditSalesRequest == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}

		if (creditSalesRequest.getActiveStatus()) {
			creditSalesRequest.setActiveStatus(creditSalesRequest.getActiveStatus());
		} else {
			creditSalesRequest.setActiveStatus(creditSalesRequest.getActiveStatus());
		}
		log.info("<<<<< -------End editCreditSalesRequest called ------->>>>>> ");

		return CREATE_PAGE;

	}

	public String deleteCreditSalesRequest() {
		try {
			action = "Delete";
			if (creditSalesRequest == null) {
				Util.addWarn("Select Atleast One Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCreditSalesRequestDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteCreditSalesRequestConfirm() {

		try {
			log.info("Delete confirmed for creditsalesrequest [" + creditSalesRequest.getName() + "]"
					+ creditSalesRequest.getId());
			CreditSalesRequest creditSalesRequestDelete = new CreditSalesRequest();
			creditSalesRequestDelete.setId(creditSalesRequest.getId());
			BaseDTO baseDTO = new BaseDTO();
			// String url = serverURL + DELETE_URL;
			String url = AppUtil.appendStrings(serverURL, DELETE_URL, "/", String.valueOf(creditSalesRequest.getId()));
			baseDTO = httpService.delete(url);

			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("CreditSalesRequest deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					creditSalesRequest = new CreditSalesRequest();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					// errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.CREDIT_SALES_REQ_DELETED_SUCCESSFULLY.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start creditSalesRequestBean . clear ----->");
		getAllCreditSalesRequestList();
		creditSalesRequest = new CreditSalesRequest();
		log.info("<---- End CreditSalesRequestBean . clear ----->");
		addButtonFlag = true;
		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewCreditSalesRequest() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (creditSalesRequest == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}
		return VIEW_PAGE;

	}

	private void getCreditSalesRequestView(CreditSalesRequest creditSalesRequest) {
		log.info("<<<< ----------Start creditSalesRequestBean . getCreditSalesRequestView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.post(url, creditSalesRequest);
			creditSalesRequest = new CreditSalesRequest();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				creditSalesRequest = mapper.readValue(jsonResponse, new TypeReference<CreditSalesRequest>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (creditSalesRequest != null) {
			log.info("CreditSalesRequest getCreditSalesRequestView" + creditSalesRequest);
		} else {
			log.info("CreditSalesRequest getCreditSalesRequestView" + creditSalesRequest);
		}

		log.info("<<<< ----------End CreditSalesRequestBean . getCreditSalesRequestView ------- >>>>");
	}
	
	
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		addButtonFlag = false;
		creditSalesRequest = ((CreditSalesRequest) event.getObject());
		log.info("<===Ends .onRowSelect ========>");
	}

	
	public void fromDateChange() {
		log.info(":::::::::::Inside::::::fromDateChange:::::::::"+creditSalesRequest.getFromDate());
		try {
			if (creditSalesRequest.getToDate() != null) {
				if (creditSalesRequest.getFromDate().compareTo(creditSalesRequest.getToDate())<0) {
				log.info("inside condition from Date is Less then To Date");
				}
				else {
				log.info("inside condition from Date is Greater then To Date");
				creditSalesRequest.setToDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in getRowData ...", e);
		}
	}
	
	
}
