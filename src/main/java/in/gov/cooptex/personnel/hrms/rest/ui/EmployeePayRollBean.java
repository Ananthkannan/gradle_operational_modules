package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
/*mport com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;*/
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayRollDetails;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeePersonalInfoGeneral;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.PayAspect;
import in.gov.cooptex.personnel.hrms.dto.EmployeePayrollRequestDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeePayRollBean")
@Scope("session")
public class EmployeePayRollBean {
	@Autowired
	LoginBean loginbean;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<Long> yearList = new ArrayList<>();

	@Getter
	@Setter
	Map<Integer, String> monthList = new HashMap<>();

	@Getter
	@Setter
	Integer month;

	@Getter
	@Setter
	Long payYear;

	@Getter
	@Setter
	List<EmployeePayRollDetails> empPayrollDetails = new ArrayList<>();

	@Getter
	@Setter
	String secondaryPassword;

	@Getter
	@Setter
	EmployeeMaster employee = new EmployeeMaster();

	@Getter
	@Setter
	String empWrkLoc;

	@Getter
	@Setter
	Double grossPay;

	@Getter
	@Setter
	Double deduction;

	@Getter
	@Setter
	Double netPay;

	@Getter
	@Setter
	String amtInWords;

	@Getter
	@Setter
	private DefaultStreamedContent downloadFile = null;

	@Getter
	@Setter
	String filePath;

	@Getter
	@Setter
	Integer workedDays;

	@Getter
	@Setter
	List<EmployeePayRollDetails> allowanceList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeePayRollDetails> otherAllowanceList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeePayRollDetails> deductionList = new ArrayList<>();

	@Getter
	@Setter
	String monthName;

	@Getter
	@Setter
	EmployeePayrollRequestDTO empPayRollRequest = new EmployeePayrollRequestDTO();

	@Getter
	@Setter
	List<EmployeePayRollDetails> commonAllowanceDeductionList = new ArrayList<>();

	/* @Value("${default.landing.page}") */
	private String defaultLandingPage;

	@Getter
	@Setter
	String logoPath;

	@Getter
	@Setter
	String inrPath;

	@Getter
	@Setter
	Double roundOff;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList = new ArrayList<>();

	@Getter
	@Setter
	String pfNumber;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	Boolean pfNumberDisableFlag = false;

	public String loadEmployeePayroll() {
		log.info("Load Employee Basic master info");
		loadYearByEmployee();
		loadEmployeesList();
		monthList = loadMonths();
		empPayrollDetails = new ArrayList<>();
		grossPay = 0.0;
		deduction = 0.0;
		netPay = 0.0;
		amtInWords = "";
		secondaryPassword = null;

		employee = new EmployeeMaster();
		commonAllowanceDeductionList = new ArrayList<>();
		empWrkLoc = null;
		workedDays = 0;
		payYear = null;
		month = null;
		empPayRollRequest = new EmployeePayrollRequestDTO();
		if (!pfNumberDisableFlag) {
			pfNumber = null;
		}

		filePath = commonDataService.getAppKeyValue("DOWNLOAD_PAY_SLIP_PDF_PATH");
		logoPath = commonDataService.getAppKeyValue("EMP_PAYSLIP_LOGO_PATH");
		inrPath = commonDataService.getAppKeyValue("EMP_PAYSLIP_INR_PATH");
		return "/pages/personnelHR/payRoll/paySlipGeneration.xhtml?faces-redirect=true";
	}

	// Load Employee List (Based on Role Name : Payroll Admin)
	public void loadEmployeesList() {
		int count = 0;
		EmployeeMaster empMaster = null;
		EmployeePersonalInfoEmployment personalInfoEmployment = null;
		try {
			String url = SERVER_URL + "/role/getpayrollrolecountbyuser";

			log.info("URL " + url);
			BaseDTO response = httpService.get(url);
			if (response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				count = mapper.readValue(jsonResponse, Integer.class);
			}
			if (count > 0) {
				employeeList = commonDataService.getActiveEmployeeNameOrderList();
				int employeeListSize = employeeList == null ? 0 : employeeList.size();
				log.info("employeeListSize - " + employeeListSize);
				pfNumberDisableFlag = false;
			} else {
				empMaster = loginbean.getEmployee();
				if (empMaster != null) {
					personalInfoEmployment = empMaster.getPersonalInfoEmployment();
					if (personalInfoEmployment != null) {
						setPfNumber(personalInfoEmployment.getPfNumber() + " / " + empMaster.getFirstName());
					}
				}
				pfNumberDisableFlag = true;
			}
		} catch (Exception e) {
			log.error("Exception at loadEmployeesList()", e);
		}
	}

	// Load Employee Auto Complete List
	public List<String> employeeAutoComplete(String input) {
		log.info("<<========= EmployeePayRollBean. employeeAutoComplete() Starts ===========>> ");
		List<String> searchList = new ArrayList<>();
		try {
			int employeeListSize = employeeList == null ? 0 : employeeList.size();
			if (employeeListSize > 0) {
				log.info("Employee Pf Number Or Name" + input);
				for (EmployeeMaster employeeMaster : employeeList) {
					if (employeeMaster.getPersonalInfoEmployment().getPfNumber().contains(input)) {
						searchList.add(
								new StringBuilder().append(employeeMaster.getPersonalInfoEmployment().getPfNumber())
										.append(" / ").append(employeeMaster.getFirstName()).toString());
					}
				}
			} else {
				log.info("Employee List Size is Empty");
			}
		} catch (Exception e) {
			log.error("Exception at EmployeePayRollBean. employeeAutoComplete() ", e);
		}
		log.info("<<========= EmployeePayRollBean. employeeAutoComplete()  Ends ===========>> ");
		return searchList;
	}

	public String cancel() {
		defaultLandingPage = commonDataService.getAppKeyValue(AppConfigKey.DEFAULT_LANDING_PAGE);
		;
		empPayrollDetails = new ArrayList<>();
		employee = new EmployeeMaster();
		commonAllowanceDeductionList = new ArrayList<>();
		amtInWords = "";
		monthName = null;
		empWrkLoc = null;
		workedDays = 0;
		payYear = null;
		month = null;
		secondaryPassword = null;
		grossPay = 0.0;
		deduction = 0.0;
		netPay = 0.0;
		log.info("defaultLandingPage:::" + defaultLandingPage);
		return "/" + defaultLandingPage;
	}

	public void clear() {
		empPayrollDetails = new ArrayList<>();
		employee = new EmployeeMaster();
		commonAllowanceDeductionList = new ArrayList<>();
		amtInWords = "";
		monthName = null;
		empWrkLoc = null;
		workedDays = 0;
		payYear = null;
		month = null;
		secondaryPassword = null;
		grossPay = 0.0;
		deduction = 0.0;
		empPayRollRequest = new EmployeePayrollRequestDTO();
		netPay = 0.0;
		if (!pfNumberDisableFlag) {
			pfNumber = null;
		}
	}

	public void loadYearByEmployee() {
		EmployeeMaster master = commonDataService.loadEmployeeByUser(loginbean.getUserDetailSession().getId());
		log.info("Server url...." + SERVER_URL + "/payroll/getyearbyemp/" + master.getId());
		BaseDTO response = httpService.get(SERVER_URL + "/payroll/getyearbyemp/" + master.getId());
		if (response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
			return;
		}
		try {
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			yearList = mapper.readValue(jsonResponse, new TypeReference<List<Long>>() {
			});
			log.info("yearList..." + yearList);
		} catch (JsonProcessingException jpEx) {
			log.info("Exception while parsing ...", jpEx);
		} catch (IOException e) {
			log.info("IOException occured ... ", e);
		}

	}

	public Map<Integer, String> loadMonths() {
		log.info("Server url...." + SERVER_URL + "/payroll/getmonths/");
		BaseDTO response = httpService.get(SERVER_URL + "/payroll/getmonths");
		if (response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
			return null;
		}
		try {
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			monthList = mapper.readValue(jsonResponse, new TypeReference<Map<Integer, String>>() {
			});
			log.info("month list");
		} catch (JsonProcessingException jpEx) {
			log.info("Exception while parsing ...", jpEx);
		} catch (IOException e) {
			log.info("IOException occured ... ", e);
		}
		return monthList;
	}

	public EmployeePayrollRequestDTO getEmployeeDetailsByPfNumber(EmployeePayrollRequestDTO request) {
		log.info("<<========= EmployeePayrollBean. getEmployeeDetailsByPfNumber() Starts ===========>> ");
		try {
			if (StringUtils.isEmpty(pfNumber)) {
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
				return null;
			}
			int employeeListSize = employeeList == null ? 0 : employeeList.size();
			if (employeeListSize > 0) {
				employeeList.stream().forEach(employee -> {
					if (employee.getPersonalInfoEmployment().getPfNumber().equals(pfNumber.split(" / ")[0])) {
						request.setEmpId(employee.getId());
					}
				});
			}
		} catch (Exception e) {
			log.error("Exception at EmployeePayrollBean. getEmployeeDetailsByPfNumber() ", e);
		}
		log.info("<<========= EmployeePayrollBean. getEmployeeDetailsByPfNumber()  Ends ===========>> ");
		return request;
	}

	public List<EmployeePayRollDetails> formEmpPayrollList(List<EmployeePayRollDetails> empPayrollDetailsList) {
		try {
			allowanceList = new ArrayList<>();
			Map<Object, List<EmployeePayRollDetails>> empPayrollList = empPayrollDetailsList.stream()
					.filter(w -> w.getPayAspect() != null).collect(Collectors.groupingBy(w -> w.getPayAspect()));
			int empPayrollListSize = empPayrollList == null ? 0 : empPayrollList.size();
			log.info("Employee Payroll List Size : " + empPayrollListSize);

			int deductionSize = 0;
			int allowanceSize = 0;

			int loopCount = 0;
			if (empPayrollList != null) {
				if (empPayrollList.get(PayAspect.Earnings) != null) {
					allowanceList = empPayrollList.get(PayAspect.Earnings);
				}
				if (empPayrollList.get(PayAspect.Allowance) != null) {
					EmployeePayRollDetails payrollDetails = new EmployeePayRollDetails();
					payrollDetails.setOtherAllowance(PayAspect.Allowance.toString());
					payrollDetails.setAllowanceName(null);
					payrollDetails.setAllowanceAmount(null);
					allowanceList.add(payrollDetails);
					allowanceList.addAll(empPayrollList.get(PayAspect.Allowance));
				}
				allowanceSize = allowanceList == null ? 0 : allowanceList.size();
				log.info("deduction list size.." + allowanceSize);
				if (empPayrollList.get(PayAspect.Deduction) != null) {
					deductionList = empPayrollList.get(PayAspect.Deduction);
				}
				deductionSize = deductionList == null ? 0 : deductionList.size();
				log.info("deduction list size.." + deductionSize);

				if (allowanceSize >= deductionSize) {
					loopCount = allowanceSize;
				} else if (deductionSize > allowanceSize) {
					loopCount = deductionSize;
				}

			}
			commonAllowanceDeductionList = new ArrayList<>();

			Boolean glblOtherAllwnceFlag = false;

			for (int i = 0; i < loopCount; i++) {
				EmployeePayRollDetails payAllowance = new EmployeePayRollDetails();
				if (allowanceSize > 0) {
					if (i < allowanceList.size()) {
						EmployeePayRollDetails payrollAlwnce = allowanceList.get(i);
						payAllowance.setEmployee(payrollAlwnce.getEmployee());
						if (PayAspect.Allowance.toString().equals(payrollAlwnce.getOtherAllowance())) {
							payAllowance.setAllowanceName(null);
							payAllowance.setEarningId(null);
							payAllowance.setPayAspect(null);
							payAllowance.setOtherAllowance(PayAspect.Allowance.toString());
						} else {
							payAllowance.setAllowanceAmount(payrollAlwnce.getAmount());
							payAllowance.setAllowanceName(
									payrollAlwnce.getHead() == null ? null : payrollAlwnce.getHead().getName());
							payAllowance.setEarningId(payrollAlwnce.getId());
							payAllowance.setPayAspect(payrollAlwnce.getPayAspect());
						}
					} else {
						payAllowance.setAllowanceAmount(null);
						payAllowance.setAllowanceName(null);
						payAllowance.setEarningId(null);
					}
				}
				if (deductionSize > 0) {
					if (i < deductionList.size()) {
						EmployeePayRollDetails payDedu = deductionList.get(i);
						payAllowance.setDeductionAmount(payDedu.getAmount());
						payAllowance.setDeductionId(payDedu.getId());
						if (payDedu.getNote() != null)
							payAllowance.setDeductionName(payDedu.getHead() == null ? ""
									: payDedu.getHead().getName() + " - " + payDedu.getNote());
						else
							payAllowance.setDeductionName(payDedu.getHead() == null ? "" : payDedu.getHead().getName());
					} else {
						payAllowance.setDeductionName(null);
						payAllowance.setDeductionAmount(null);
						payAllowance.setDeductionId(null);
						payAllowance.setNote(null);
					}
				}
				commonAllowanceDeductionList.add(payAllowance);
			}

			/*
			 * if (allowanceSize <= deductionSize) { int allowanceIndex = 0; for (int i = 0;
			 * i < deductionSize; i++) {
			 * 
			 * EmployeePayRollDetails payAllowance = new EmployeePayRollDetails(); try {
			 * EmployeePayRollDetails payrollAlwnce = allowanceList.get(allowanceIndex);
			 * 
			 * payAllowance.setEmployee(payrollAlwnce.getEmployee());
			 * payAllowance.setAllowanceAmount(payrollAlwnce.getAmount());
			 * payAllowance.setAllowanceName( payrollAlwnce.getHead() == null ? null :
			 * payrollAlwnce.getHead().getName());
			 * payAllowance.setEarningId(payrollAlwnce.getId());
			 * payAllowance.setPayAspect(payrollAlwnce.getPayAspect());
			 * EmployeePayRollDetails payDedu = deductionList.get(i);
			 * payAllowance.setDeductionAmount(payDedu.getAmount());
			 * payAllowance.setDeductionId(payDedu.getId()); if (payDedu.getNote() != null)
			 * payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName() + " - " + payDedu.getNote()); else
			 * payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName()); if (!glblOtherAllwnceFlag) { if
			 * (PayAspect.Allowance.toString().equals(payrollAlwnce.getPayAspect().toString(
			 * ))) { glblOtherAllwnceFlag = true;
			 * payDedu.setOtherAllowance(PayAspect.Allowance.toString());
			 * payDedu.setHead(payrollAlwnce.getHead());
			 * payDedu.setDeductionId(payDedu.getId());
			 * payDedu.setDeductionAmount(payAllowance.getDeductionAmount());
			 * payDedu.setDeductionName(payAllowance.getDeductionName());
			 * payDedu.setPayAspect(payrollAlwnce.getPayAspect());
			 * commonAllowanceDeductionList.add(payDedu); i = i + 1; payDedu =
			 * deductionList.get(i); payAllowance.setDeductionAmount(payDedu.getAmount());
			 * if (payDedu.getNote() != null)
			 * payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName() + " - " + payDedu.getNote()); else
			 * payAllowance.setDeductionName( payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName()); payAllowance.setDeductionId(payDedu.getId()); }
			 * 
			 * commonAllowanceDeductionList.add(payAllowance);
			 * 
			 * } else { commonAllowanceDeductionList.add(payAllowance); } allowanceIndex++;
			 * } catch (Exception e) { log.info("Deduction Size greater"); payAllowance =
			 * new EmployeePayRollDetails(); EmployeePayRollDetails payDedu =
			 * deductionList.get(i); payAllowance.setDeductionAmount(payDedu.getAmount());
			 * payAllowance.setDeductionId(payDedu.getId());
			 * payAllowance.setPayAspect(payDedu.getPayAspect()); if (payDedu.getNote() !=
			 * null) payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName() + " - " + payDedu.getNote()); else
			 * payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName()); commonAllowanceDeductionList.add(payAllowance);
			 * 
			 * } } } else { int allowanceIndex = 0; for (int i = 0; i < allowanceSize; i++)
			 * { EmployeePayRollDetails payAllowance = new EmployeePayRollDetails(); try {
			 * EmployeePayRollDetails payrollAlwnce = null; if (allowanceList != null) {
			 * payrollAlwnce = allowanceList.get(allowanceIndex);
			 * payAllowance.setPayAspect(payrollAlwnce.getPayAspect());
			 * payAllowance.setEarningId(payrollAlwnce.getId()); if
			 * (payrollAlwnce.getEmployee() != null) {
			 * payAllowance.setEmployee(payrollAlwnce.getEmployee()); } if
			 * (payrollAlwnce.getAmount() != null) {
			 * payAllowance.setAllowanceAmount(payrollAlwnce.getAmount()); } if
			 * (payrollAlwnce.getHead().getName() != null) { payAllowance.setAllowanceName(
			 * payrollAlwnce.getHead() == null ? "" : payrollAlwnce.getHead().getName()); }
			 * } if (deductionList != null) { if (i <= deductionList.size()) {
			 * EmployeePayRollDetails payDedu = deductionList.get(i);
			 * payAllowance.setPayAspect(payDedu.getPayAspect());
			 * payAllowance.setDeductionId(payDedu.getId());
			 * payAllowance.setHead(payDedu.getHead()); if (payDedu.getAmount() != null) {
			 * payAllowance.setDeductionAmount(payDedu.getAmount()); } if (payDedu.getNote()
			 * != null) { payAllowance.setDeductionName(payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName() + " - " + payDedu.getNote()); } else {
			 * payAllowance.setDeductionName( payDedu.getHead() == null ? "" :
			 * payDedu.getHead().getName()); } if (!glblOtherAllwnceFlag) { if
			 * (PayAspect.Allowance.toString()
			 * .equals(payrollAlwnce.getPayAspect().toString())) { glblOtherAllwnceFlag =
			 * true; payDedu.setOtherAllowance(PayAspect.Allowance.toString());
			 * payDedu.setHead(payrollAlwnce.getHead());
			 * payDedu.setDeductionId(payAllowance.getDeductionId());
			 * payDedu.setDeductionAmount(payAllowance.getDeductionAmount());
			 * payDedu.setDeductionName(payAllowance.getDeductionName());
			 * payDedu.setPayAspect(payrollAlwnce.getPayAspect());
			 * commonAllowanceDeductionList.add(payDedu);
			 * payAllowance.setDeductionAmount(null); payAllowance.setDeductionName(null);
			 * payAllowance.setDeductionId(null); //
			 * commonAllowanceDeductionList.add(payAllowance); } } }else { if
			 * (PayAspect.Allowance.toString()
			 * .equals(payrollAlwnce.getPayAspect().toString())) { glblOtherAllwnceFlag =
			 * true; EmployeePayRollDetails otherAllowance = new EmployeePayRollDetails();
			 * otherAllowance.setOtherAllowance(PayAspect.Allowance.toString());
			 * commonAllowanceDeductionList.add(otherAllowance); } } }
			 * 
			 * commonAllowanceDeductionList.add(payAllowance); //
			 * log.info("commonAllowanceDeductionList size - " + //
			 * commonAllowanceDeductionList.size()); } catch (Exception e) {
			 * log.info("Allowance Size greater"); EmployeePayRollDetails payrollAlwnce =
			 * allowanceList.get(allowanceIndex); if (!glblOtherAllwnceFlag) { if
			 * (PayAspect.Allowance.toString().equals(payrollAlwnce.getPayAspect().toString(
			 * ))) { glblOtherAllwnceFlag = true; EmployeePayRollDetails otherAllowance =
			 * new EmployeePayRollDetails(); otherAllowance.setAllowanceAmount(null);
			 * otherAllowance.setAllowanceName(null); otherAllowance.setPayAspect(null);
			 * otherAllowance.setOtherAllowance(PayAspect.Allowance.toString());
			 * commonAllowanceDeductionList.add(otherAllowance); EmployeePayRollDetails
			 * payrollExeAlwnce = new EmployeePayRollDetails(); String name =
			 * payrollAlwnce.getHead() == null ? "" : payrollAlwnce.getHead().getName();
			 * Double amount = payrollAlwnce.getAmount();
			 * payrollExeAlwnce.setAllowanceName(name);
			 * payrollExeAlwnce.setAllowanceAmount(amount);
			 * payrollExeAlwnce.setHead(payrollAlwnce.getHead());
			 * payrollExeAlwnce.setEarningId(payrollAlwnce.getId());
			 * payrollExeAlwnce.setPayAspect(payrollAlwnce.getPayAspect());
			 * commonAllowanceDeductionList.add(payrollExeAlwnce); }else {
			 * EmployeePayRollDetails payAllowances = new EmployeePayRollDetails();
			 * payAllowances.setAllowanceAmount(payrollAlwnce.getAmount()); String name =
			 * payrollAlwnce.getHead() == null ? "" : payrollAlwnce.getHead().getName();
			 * payAllowances.setAllowanceName(name);
			 * payAllowances.setPayAspect(payrollAlwnce.getPayAspect());
			 * payAllowances.setEarningId(payrollAlwnce.getId());
			 * commonAllowanceDeductionList.add(payAllowances); } } else {
			 * EmployeePayRollDetails payAllowances = new EmployeePayRollDetails();
			 * payAllowances.setAllowanceAmount(payrollAlwnce.getAmount()); String name =
			 * payrollAlwnce.getHead() == null ? "" : payrollAlwnce.getHead().getName();
			 * payAllowances.setAllowanceName(name);
			 * payAllowances.setPayAspect(payrollAlwnce.getPayAspect());
			 * payAllowances.setEarningId(payrollAlwnce.getId());
			 * commonAllowanceDeductionList.add(payAllowances); } } allowanceIndex++; } }
			 */
		} catch (Exception e) {
			log.error("Exception at formEmpPayrollList() ", e);
		}
		return commonAllowanceDeductionList;
	}
	
	

	public EmployeePayrollRequestDTO convertPayslip(List<EmployeePayRollDetails> empPayrollDetails, Boolean isUnPaidEmp) {
		EmployeePayrollRequestDTO empPayRollRequest = new EmployeePayrollRequestDTO();
		EntityMaster entityMaster = new EntityMaster();
		if (empPayrollDetails != null && empPayrollDetails.size() > 0) {
			grossPay = empPayrollDetails.stream()
					.filter(o -> o.getPayAspect() != null && o.getPayAspect().equals(PayAspect.Earnings))
					.collect(Collectors.summingDouble(o -> o.getAmount()));

			grossPay = grossPay + empPayrollDetails.stream()
					.filter(o -> o.getPayAspect() != null && o.getPayAspect().equals(PayAspect.Allowance))
					.collect(Collectors.summingDouble(o -> o.getAmount()));

			Stream<EmployeePayRollDetails> deductionFilter = empPayrollDetails.stream()
					.filter(o -> o.getPayAspect() != null && o.getPayAspect().equals(PayAspect.Deduction));
			deduction = deductionFilter.collect(Collectors.summingDouble(o -> o.getAmount()));

			if(isUnPaidEmp) {
				netPay = 0D;
			} else {
				netPay = grossPay - deduction;
			}

			employee = empPayrollDetails.get(0).getEmployee();

			commonAllowanceDeductionList = formEmpPayrollList(empPayrollDetails);

			int commonAllowanceDeductionListSize = commonAllowanceDeductionList == null ? 0
					: commonAllowanceDeductionList.size();
			log.info("commonAllowanceDeductionList size" + commonAllowanceDeductionListSize);

			if (employee.getPersonalInfoEmployment() != null) {
				if (employee.getPersonalInfoEmployment().getWorkLocation() != null) {
					if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster() != null) {
						empWrkLoc = employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAddressLineOne();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAddressLineTwo() != null)
							empWrkLoc = empWrkLoc + "," + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getAddressLineTwo();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAddressLineThree() != null)
							empWrkLoc = empWrkLoc + "," + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getAddressLineThree();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAreaMaster() != null)
							empWrkLoc = empWrkLoc + "," + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getAreaMaster().getName();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getCityMaster() != null)
							empWrkLoc = empWrkLoc + "," + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getCityMaster().getName();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getStateMaster() != null)
							empWrkLoc = empWrkLoc + "," + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getStateMaster().getName();
						if (employee.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getPostalCode() != null)
							empWrkLoc = empWrkLoc + "-" + employee.getPersonalInfoEmployment().getWorkLocation()
									.getAddressMaster().getPostalCode();

					}
				}
			} else {
				errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_INFO_EMPTY.getErrorCode());
				return null;
			}
			try {
				log.info("<====== Net Pay ======> " + netPay);
				// amtInWords =
				// AmountToWordConverter.converter(String.valueOf(AppUtil.ifNullRetunZero(netPay)));
				// empPayRollRequest.setAmountInWords(amtInWords);
				empPayRollRequest.setGrossPay(grossPay.toString());
				empPayRollRequest.setDeduction(deduction.toString());
				empPayRollRequest.setNetPay(formatDecimalNumber(netPay));
				roundOff = Double.valueOf(Math.round(Double.valueOf(netPay)));
				if (roundOff >= 0) {
					empPayRollRequest.setRoundOff(roundOff);
					amtInWords = AmountToWordConverter.converter(String.valueOf(AppUtil.ifNullRetunZero(roundOff)));
				} else {
					amtInWords = "";
				}
				empPayRollRequest.setAmountInWords(amtInWords);
				empPayRollRequest.setDaysWorked(workedDays.toString());
				empPayRollRequest.setAllowanceTable(formAllowanceTable());
				empPayRollRequest.setDeductionTable(formDeductionTable());
				empPayRollRequest.setEmpName(employee.getFirstName() + " " + employee.getLastName());
				empPayRollRequest.setEmpNumber(employee.getEmpCode());
				empPayRollRequest.setEmpId(employee.getId());

				/*
				 * if (employee.getPersonalInfoAccountDetails() != null)
				 * empPayRollRequest.setPfNo(employee.getPersonalInfoAccountDetails().
				 * getPfNumber()); else empPayRollRequest.setPfNo("");
				 */
				EmployeePersonalInfoEmployment personalInfoEmployment = employee.getPersonalInfoEmployment();
				if (personalInfoEmployment != null) {
					if (personalInfoEmployment.getPfNumber() != null) {
						empPayRollRequest.setPfNo(personalInfoEmployment.getPfNumber());
						log.info("pf Number - " + empPayRollRequest.getPfNo());
					} else {
						empPayRollRequest.setPfNo("NA");
					}
					empPayRollRequest.setDepartmentName(personalInfoEmployment.getCurrentDepartment() == null ? "NA"
							: personalInfoEmployment.getCurrentDepartment().getName());
					empPayRollRequest.setDesignation(personalInfoEmployment.getCurrentDesignation() == null ? "NA"
							: personalInfoEmployment.getCurrentDesignation().getName());
					empPayRollRequest.setSectionName(personalInfoEmployment.getCurrentSection() == null ? "NA"
							: personalInfoEmployment.getCurrentSection().getName());

					if (personalInfoEmployment.getWorkLocation().getEntityTypeMaster().getEntityCode()
							.equalsIgnoreCase("HEAD_OFFICE"))

					{
						empPayRollRequest.setRegionName(personalInfoEmployment.getWorkLocation() == null ? "NA"
								: personalInfoEmployment.getWorkLocation().getName());

						empPayRollRequest.setRegionCode(personalInfoEmployment.getWorkLocation() == null ? "NA"
								: personalInfoEmployment.getWorkLocation().getCode().toString());
						empPayRollRequest.setShowroomName("NA");
						empPayRollRequest.setShowroomCode("NA");
					} else {

						entityMaster = commonDataService
								.findRegionByShowroomId(personalInfoEmployment.getWorkLocation().getId());

						empPayRollRequest.setRegionName(entityMaster == null ? "NA" : entityMaster.getName());

						empPayRollRequest
								.setRegionCode(entityMaster == null ? "NA" : entityMaster.getCode().toString());

						empPayRollRequest.setShowroomName(personalInfoEmployment.getWorkLocation() == null ? "NA"
								: personalInfoEmployment.getWorkLocation().getName());

						empPayRollRequest.setShowroomCode(personalInfoEmployment.getWorkLocation() == null ? "NA"
								: personalInfoEmployment.getWorkLocation().getCode().toString());
					}
					if (personalInfoEmployment.getDateOfJoining() != null) {
						SimpleDateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
						String doj = format.format(personalInfoEmployment.getDateOfJoining());
						empPayRollRequest.setDateOfJoining(doj == null ? "NA" : doj);
					}
					empPayRollRequest.setUanNo(personalInfoEmployment.getUniversalAccountNumber() == null ? "NA"
							: personalInfoEmployment.getUniversalAccountNumber());

				}

				EmployeePersonalInfoGeneral personalInfoGeneral = employee.getPersonaInfoGeneral();
				if (personalInfoGeneral != null) {
					empPayRollRequest.setFatherName(personalInfoGeneral.getFatherName());
					empPayRollRequest.setPanNumber(personalInfoGeneral.getPanNumber());
				} else {
					empPayRollRequest.setFatherName("");
					empPayRollRequest.setPanNumber("");
				}
				empPayRollRequest.setEntityAddress(formAddress(employee));
				empPayRollRequest.setMonthName(monthList.get(month));
				empPayRollRequest.setYear(payYear);
				empPayRollRequest.setEmployeePayrollDetailsList(commonAllowanceDeductionList);
			} catch (Exception e) {
				log.info("Exception Occured while converting amount to word....", e);
			}
		}
		return empPayRollRequest;
	}

	public void getPayslipForEmployee() throws ParseException {
		log.info("Start Employeepay roll get payslip....");
		log.info("selected year..." + payYear);
		log.info("month ....." + month);
		employee = new EmployeeMaster();
		EmployeePayrollRequestDTO request = new EmployeePayrollRequestDTO();
		Long userId = loginbean.getUserDetailSession().getId();
		request.setLoggedInUserId(userId);
		if (pfNumberDisableFlag) {
			log.info("getPayslipForEmployee userid==>" + loginbean.getUserDetailSession().getId());
			EmployeeMaster master = commonDataService.loadEmployeeByUser(loginbean.getUserDetailSession().getId());
			request.setEmpId(master.getId());
		} else {
			request = getEmployeeDetailsByPfNumber(request);
		}
		request.setMonth(month);
		request.setYear(payYear);
		request.setPassword(secondaryPassword);
		monthName = monthList.get(month);
		
		Long empId = request.getEmpId();
		log.info("Get pay slip URL::::" + SERVER_URL + "/payroll/getpayslip" + "/" + request.getEmpId() + "/" + month
				+ "/" + payYear + "/" + secondaryPassword);
		BaseDTO response = httpService.post(SERVER_URL + "/payroll/getpayslip", request);
		if (response != null) {
			if (response.getStatusCode() == 0) {
				try {
					empWrkLoc = new String();
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					empPayrollDetails = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeePayRollDetails>>() {
							});
					// jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					// workedDays = mapper.readValue(jsonResponse, new TypeReference<Integer>() {
					// });
					Date startDate = AppUtil.DATE_FORMAT_MM_YYYY.parse(month + "-" + payYear);
					Date fromDate = AppUtil.REPORT_DATE_FORMAT.parse(AppUtil.REPORT_DATE_FORMAT.format(startDate));
					Calendar calendar = Calendar.getInstance();
					calendar.setTime(fromDate);
					calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
					Date toDate = AppUtil.REPORT_DATE_FORMAT
							.parse(AppUtil.REPORT_DATE_FORMAT.format(calendar.getTime()));
					workedDays = AppUtil.getDay(toDate);

					if (response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
						log.error("Error occured..." + response.getStatusCode());
						return;
					}
					List<Long> unpaidEmpList = loadUnpaidEmployeeList(userId, month, payYear);
					boolean isUnPaidEmp = false;
					if (CollectionUtils.isNotEmpty(unpaidEmpList)) {
						isUnPaidEmp = unpaidEmpList.stream().anyMatch(t -> t.equals(empId));
					}
					
					empPayRollRequest = convertPayslip(empPayrollDetails, isUnPaidEmp);
				} catch (JsonProcessingException jpEx) {
					log.info("Exception while parsing ...", jpEx);
				} catch (IOException e) {
					log.info("IOException occured ... ", e);
				}

			} else {
				errorMap.notify(response.getStatusCode());
				return;
			}

		}
	}

	/**
	 * 
	 * @param userId
	 * @param monthNumber
	 * @param yearNumber
	 * @return
	 */
	private List<Long> loadUnpaidEmployeeList(Long userId, Integer monthNumber, Long yearNumber) {
		log.info("EmployeePayrollRegisterBean. loadUnpaidEmployeeList() - START");
		List<Long> unpaidEmployeeList = null;
		try {
			EmployeePayrollRequestDTO request = new EmployeePayrollRequestDTO();
			request.setLoggedInUserId(userId);
			request.setMonth(monthNumber);
			request.setYear(yearNumber);
			BaseDTO response = httpService.post(SERVER_URL + "/payroll/getunpaidemplist", request);
			if (response != null) {
				if (response.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponses = mapper.writeValueAsString(response.getResponseContents());
					unpaidEmployeeList = mapper.readValue(jsonResponses, new TypeReference<List<Long>>() {
					});
					int unpaidEmployeeListSize = unpaidEmployeeList == null ? 0 : unpaidEmployeeList.size();
					log.info("unpaidEmployeeListSize: " + unpaidEmployeeListSize);
				}
			}
		} catch (Exception e) {
			log.error("Exception at loadUnpaidEmployeeList()", e);
		}
		log.info("EmployeePayrollRegisterBean. loadUnpaidEmployeeList() - END");
		return unpaidEmployeeList;
	}
	
	public String formAddress(EmployeeMaster emp) {
		if (emp.getPersonalInfoEmployment() != null) {
			if (emp.getPersonalInfoEmployment().getWorkLocation() != null) {
				if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster() != null) {
					String address = emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
							.getAddressLineOne();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
							.getAddressLineTwo() != null)
						address = address + "," + emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAddressLineTwo();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
							.getAddressLineThree() != null)
						address = address + "," + emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAddressLineThree();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster().getAreaMaster() != null)
						address = address + "," + emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getAreaMaster().getName();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster().getCityMaster() != null)
						address = address + "," + emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getCityMaster().getName();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster().getStateMaster() != null)
						address = address + "," + emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster()
								.getStateMaster().getName();
					if (emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster().getPostalCode() != null)
						address = address + "-"
								+ emp.getPersonalInfoEmployment().getWorkLocation().getAddressMaster().getPostalCode();

					return address;
				}
			}
		}
		return "";
	}

	public void generatePaySlipForEmployee(EmployeePayrollRequestDTO empPayRollRequest) {
		String htmlContent = commonDataService.getTemplateDetails("EMPLOYEE_PAY_SLIP_TEMPLATE");
		try {
			log.info("File path..." + filePath);
			String content = replaceParams(htmlContent, empPayRollRequest);
			log.info("Html content :::::");
			content = StringUtils.replace(content, "&", "&amp;");
			createSinglePDF(content, filePath + "Employee_Payslip.html");

			File file = new File(filePath);

			byte[] array = Files.readAllBytes(new File(filePath + "Employee_Payslip.html").toPath());
			if (!file.exists()) {
				String errorMessage = "Sorry. The file you are looking for does not exist";
				log.info("File Not Found...", errorMessage);
			}
			ByteArrayInputStream bais = new ByteArrayInputStream(array);
			downloadFile = new DefaultStreamedContent(bais, "pdf",
					employee.getEmpCode() + "_" + employee.getFirstName() + "_" + month + "_" + payYear + ".pdf");
			// FileUtils.cleanDirectory(file);
		} catch (IOException e) {
			log.error("Exception occured while generating payslip for Employee", e);
		} catch (Exception e) {
			log.info("Exception occured while accessing a file...", e);
		}
	}

	public String replaceParams(String content, EmployeePayrollRequestDTO employee) {
		log.info("Start Replace Contenet with values from employee...");
		content = content.replace("$empNumber", employee.getEmpNumber());
		content = content.replace("$empName", employee.getEmpName());
		content = content.replace("$panNumber", employee.getPanNumber());
		content = content.replace("$fatherName", employee.getFatherName());
		content = content.replace("$dateOfJoining", employee.getDateOfJoining());

		content = content.replace("$regionCode", employee.getRegionCode() == null ? "" : employee.getRegionCode());

		content = content.replace("$regionName", employee.getRegionName() == null ? "" : employee.getRegionName());

		if (employee.getShowroomCode() != null && employee.getShowroomName() != null) {
			content = content.replace("$showroomCode",
					employee.getShowroomCode() == null ? "" : employee.getShowroomCode());

			content = content.replace("$showroomName",
					employee.getShowroomName() == null ? "" : employee.getShowroomName());
		}
		content = content.replace("$empDesignation", employee.getDesignation());
		content = content.replace("$departmentName", employee.getDepartmentName());
		content = content.replace("$sectioName", employee.getSectionName());
		content = content.replace("$daysWorked", employee.getDaysWorked());
		content = content.replace("$allowanceDeduTbl", employee.getAllowanceTable());
		log.info("Gross pay:::" + employee.getGrossPay());
		log.info("deduction pay:::" + employee.getDeduction());
		log.info("net pay:::" + employee.getNetPay());
		content = content.replace("$grosspay", formatDecimalNumber(Double.parseDouble(employee.getGrossPay())));
		content = content.replace("$deduction", formatDecimalNumber(Double.parseDouble(employee.getDeduction())));
		content = content.replace("$netPay", formatDecimalNumber(Double.parseDouble(employee.getNetPay())));
		Double roundOff = (double) Math.round(Double.parseDouble(employee.getNetPay()));
		content = content.replace("$roundOff", formatRoundOffDecimalNumber(roundOff));
		content = content.replace("$amtInwrds", employee.getAmountInWords());
		content = content.replace("$address", employee.getEntityAddress());
		content = content.replace("$month", employee.getMonthName());
		content = content.replace("$year", employee.getYear().toString());
		content = content.replace("$pfNumber", employee.getPfNo());
		content = content.replace("$logoimage", logoPath);
		content = content.replace("$inrImage", inrPath);
		content = content.replace("$uanNumber", employee.getUanNo());
		return content;

	}

	public String formatDecimalNumber(Double number) {
		if (number == null)
			return "0.00";
		DecimalFormat decim = new DecimalFormat("0.00");
		String price = decim.format(number);
		return price;
	}

	public String formatRoundOffDecimalNumber(Double number) {
		if (number == null)
			return "0";
		DecimalFormat decim = new DecimalFormat("0");
		String price = decim.format(number);
		return price;
	}

	public String formAllowanceTable() {
		log.info("Start Form Allowance Deduction table");
		String allowanceTable = "";
		if (commonAllowanceDeductionList != null && commonAllowanceDeductionList.size() > 0) {
			for (EmployeePayRollDetails payRoll : commonAllowanceDeductionList) {
				if (!PayAspect.Allowance.toString().equals(payRoll.getOtherAllowance())) {
					/*
					 * allowanceTable=allowanceTable+
					 * "												<tr><td>"+payRoll.
					 * getAllowanceName()+"</td>\r\n" +
					 * "												<td align=\"right\">"
					 * +formatDecimalNumber(payRoll.getAllowanceAmount())+"</td>\r\n" +
					 * "												<td>"+payRoll.
					 * getDeductionName()+"</td>\r\n" +
					 * "												<td align=\"right\">"
					 * +formatDecimalNumber(payRoll.getDeductionAmount())+"</td>\r\n" +
					 * "											</tr>";
					 */
					allowanceTable = allowanceTable + "<tr>";
					if (payRoll.getAllowanceName() != null) {
						allowanceTable = allowanceTable + "												<td>"
								+ payRoll.getAllowanceName() + "</td>\r\n"
								+ "												<td align=\"right\">"
								+ formatDecimalNumber(payRoll.getAllowanceAmount()) + "</td>\r\n";

					} else {
						allowanceTable = allowanceTable + "												<td></td>\r\n"
								+ "												<td align=\"right\"></td>\r\n";

					}
					if (payRoll.getDeductionName() != null) {
						allowanceTable = allowanceTable + "												<td>"
								+ payRoll.getDeductionName() + "</td>\r\n"
								+ "												<td align=\"right\">"
								+ formatDecimalNumber(payRoll.getDeductionAmount()) + "</td>\r\n";

					} else {
						allowanceTable = allowanceTable + "												<td></td>\r\n"
								+ "												<td align=\"right\"></td>\r\n";

					}
					allowanceTable = allowanceTable + "	</tr>";
				} else if (PayAspect.Allowance.toString().equals(payRoll.getOtherAllowance())) {
					allowanceTable = allowanceTable
							+ "												<tr><td colspan=\"2\" class=\"border-hed\">Other Allowance</td>";
					if (payRoll.getDeductionName() != null) {
						allowanceTable = allowanceTable + "												<td>"
								+ payRoll.getDeductionName() + "</td>\r\n"
								+ "												<td align=\"right\">"
								+ formatDecimalNumber(payRoll.getDeductionAmount()) + "</td>\r\n";
					} else {
						allowanceTable = allowanceTable + "												<td></td>\r\n"
								+ "												<td align=\"right\"></td>\r\n";

					}
					allowanceTable = allowanceTable + "											</tr>";
				}

			}
		}
		return allowanceTable;
	}

	public String formDeductionTable() {
		log.info("Start Form Allowance Deduction table");
		String deductionTable = "";
		if (deductionList != null && deductionList.size() > 0) {
			deductionTable = "<table class=\"\" style='width:200%; font-size:12px'>\r\n" + "\r\n" + "\r\n" + "\r\n"
					+ "									<thead class=\"headcolor\">\r\n"
					+ "										<tr>\r\n" + "\r\n"
					+ "											<th align=\"center\" class=\"\">Deductions </th>\r\n"
					+ "											<th align=\"center\">Amount</th>\r\n"
					+ "											\r\n"
					+ "											\r\n" + "\r\n" + "\r\n" + "\r\n" + "\r\n"
					+ "										</tr>\r\n"
					+ "									</thead><tbody>";
			for (EmployeePayRollDetails payroll : deductionList) {
				deductionTable = deductionTable + "<tr>\r\n" + "											<td>"
						+ payroll.getHead().getName() + "</td>\r\n"
						+ "											<td align=\"right\">" + payroll.getAmount()
						+ "</td>\r\n" + "											\r\n"
						+ "										</tr>";
			}
			deductionTable = deductionTable + "</tbody>\r\n" + "									<tfoot>\r\n"
					+ "										<tr>\r\n"
					+ "											\r\n"
					+ "											<td  align=\"right\"><b>Total Deduction</b></td>\r\n"
					+ "											<td  align=\"right\"><b>" + deduction + "</b></td>\r\n"
					+ "\r\n" + "										</tr>\r\n"
					+ "										<tr>\r\n" + "\r\n"
					+ "											\r\n"
					+ "											<td  align=\"right\"><strong>Net payable Amount</strong></td>\r\n"
					+ "\r\n" + "\r\n" + "											<td  align=\"right\"><b>" + netPay
					+ "</b></td>\r\n" + "\r\n" + "										</tr>\r\n"
					+ "										\r\n" + "										\r\n"
					+ "									</tfoot>\r\n" + "\r\n"
					+ "								</table>";
		}
		log.info("End Form Allowance Deduction table");
		return deductionTable;
	}

	public DefaultStreamedContent downloadEmpPayrollPdf() {
		log.info("get Download File...." + downloadFile);
		try {
			if (empPayrollDetails != null && empPayrollDetails.size() > 0) {
				generatePaySlipForEmployee(empPayRollRequest);
				return downloadFile;
			} else {
				AppUtil.addWarn("Payment Not Processed For This Month");
				return null;
			}
		} catch (Exception e) {
			log.error("Exception at EmployeePayRollBean. getDownloadFile()", e);
		}
		return null;
	}

	public void createPDF(String htmlInputContent, String pdfOutputFilePath) throws Exception {

		if (htmlInputContent == null || htmlInputContent.isEmpty()) {
			throw new Exception("HTML InputContent is empty");
		}

		if (pdfOutputFilePath == null || pdfOutputFilePath.isEmpty()) {
			throw new Exception("PDF OutputFilePath is empty");
		}

		// step 1, landscape.
		Document document = new Document(PageSize.A4.rotate());
		// step 2
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfOutputFilePath));
		writer.setInitialLeading(2);
		// step 3
		document.open();

		InputStream htmlInputStream = new ByteArrayInputStream(htmlInputContent.getBytes());

		/*
		 * CSSResolver cssResolver =
		 * XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
		 * 
		 * 
		 * 
		 * HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		 * htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		 * //htmlContext.setImageProvider(new Base64ImageProvider());
		 * 
		 * PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer); HtmlPipeline
		 * html = new HtmlPipeline(htmlContext, pdf); CssResolverPipeline css = new
		 * CssResolverPipeline(cssResolver, html);
		 * 
		 * // XML Worker XMLWorker worker = new XMLWorker(css, true); XMLParser p = new
		 * XMLParser(worker); p.parse(new
		 * ByteArrayInputStream(htmlInputContent.getBytes()));
		 * 
		 * 
		 */

		// step 4
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlInputStream);
		// step 5
		document.close();
	}

	private void createSinglePDF(String htmlInputContent, String pdfOutputFilePath) throws Exception {

		if (htmlInputContent == null || htmlInputContent.isEmpty()) {
			throw new Exception("HTML InputContent is empty");
		}

		if (pdfOutputFilePath == null || pdfOutputFilePath.isEmpty()) {
			throw new Exception("PDF OutputFilePath is empty");
		}

		// step 1, landscape.
		Document document = new Document(PageSize.A4.rotate());
		// step 2
		PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfOutputFilePath));
		writer.setInitialLeading(2);
		// step 3
		document.open();

		InputStream htmlInputStream = new ByteArrayInputStream(htmlInputContent.getBytes());

		/*
		 * CSSResolver cssResolver =
		 * XMLWorkerHelper.getInstance().getDefaultCssResolver(true);
		 * 
		 * 
		 * 
		 * HtmlPipelineContext htmlContext = new HtmlPipelineContext(null);
		 * htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
		 * //htmlContext.setImageProvider(new Base64ImageProvider());
		 * 
		 * PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer); HtmlPipeline
		 * html = new HtmlPipeline(htmlContext, pdf); CssResolverPipeline css = new
		 * CssResolverPipeline(cssResolver, html);
		 * 
		 * // XML Worker XMLWorker worker = new XMLWorker(css, true); XMLParser p = new
		 * XMLParser(worker); p.parse(new
		 * ByteArrayInputStream(htmlInputContent.getBytes()));
		 * 
		 * 
		 */

		// step 4
		XMLWorkerHelper.getInstance().parseXHtml(writer, document, htmlInputStream);
		// step 5
		document.close();
	}
}
