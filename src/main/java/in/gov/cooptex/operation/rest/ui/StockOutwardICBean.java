package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CurrentStockStatusRequest;
import in.gov.cooptex.core.dto.DeliveryChallanDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.StockTransferItemsDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.ApplicationConstants;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.SupplierType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardICBean")
@Scope("session")
public class StockOutwardICBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String STOCK_OUTWARD_LIST = "/pages/inspectionCenter/outward/listStockOutwardInspection.xhtml?faces-redirect=true;";

	private final String STOCK_OUTWARD_CREATE = "/pages/inspectionCenter/outward/createStockOutwardInspection.xhtml?faces-redirect=true;";

	private final String STOCK_OUTWARD_VIEW = "/pages/inspectionCenter/outward/viewStockOutwardInspection.xhtml?faces-redirect=true;";

	private final String DELIVERY_CHALLAN_ACKNOWLEDGE = "/pages/ISSR/outward/deliveryChellanAcknowledgement.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	StockTransferType stockTransferType;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<DeliveryChallanDTO> productCodeSummaryList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> transferItemsList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> hsnSummaryList;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnProductAdd = true;

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	List<EntityMaster> showroomList;

	@Getter
	@Setter
	List<Double> unitRateList;

	@Getter
	@Setter
	Double unitRate;

	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}

	public String redirectSVInProgressPage() {
		if(loginBean.getStockVerificationIsInProgess()) {
			loginBean.setMessage("Outward");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}
	
	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		EntityMaster loginWorkLocationEntity = getLoginEmployeeWorkLocation();

		String loginEntityCode = loginWorkLocationEntity.getEntityTypeMaster().getEntityCode();

		if (loginEntityCode.equals(EntityType.INSPECTION_CENTER)) {
			stockTransferType = StockTransferType.INSPECTION_OUTWARD;
		} else if (loginEntityCode.equals(EntityType.INSTITUTIONAL_SALES_SHOWROOM)) {
			stockTransferType = StockTransferType.ISSR_OUTWARD;
		} else if (loginEntityCode.equals(EntityType.EXPORT_WAREHOUSE)) {
			stockTransferType = StockTransferType.EXPORT_WAREHOUSE_OUTWARD;
		} else if (loginEntityCode.equals(EntityType.PRINTING_WARE_HOUSE)) {
			stockTransferType = StockTransferType.PRINTING_WAREHOUSE_OUTWARD;
		} 
		
//		else if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)) {
//			stockTransferType = StockTransferType.PRODUCT_WAREHOUSE;
//		}
		if ("ADD".equals(page) || "EDIT".equals(page)) {
			redirectSVInProgressPage();
		}
		
		if (page.equals("ADD")) {
			return gotoAddPage();
		} else if (page.equals("EDIT")) {
			return gotoEditPage();
		} else if (page.equals("VIEW")) {
			return gotoViewPage();
		} else {
			return getListPage();
		}

	}

	/**********************************************************************************/

	public String getListPage() {

		log.info("Going to list page");

		disableAddButton = false;

		SERVER_URL = stockTransferUtility.loadServerUrl();

		statusValues = stockTransferUtility.loadStockTransferStatusList();

		stockTransferResponse = new StockTransferResponse();

		loadLazyList();

		return STOCK_OUTWARD_LIST;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();

		request.setStockTransferType(stockTransferType);
		if (getLoginEmployeeWorkLocation() != null && getLoginEmployeeWorkLocation().getId() != null) {
			request.setId(getLoginEmployeeWorkLocation().getId());
		}

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("senderCodeOrName")) {
				request.setSenderCodeOrName(value);
			}

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("stockMovementTo")) {
				request.setStockMovementTo(value);
			}

			if (entry.getKey().equals("receiverCodeOrName")) {
				request.setReceiverCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dateTransferred")) {
				request.setDateTransferred(AppUtil.serverDateFormat(value));
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/list";

		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();

						for (StockTransferResponse response : data) {
							if (response.getSocietyCode() == null && response.getSocietyName() == null) {
								response.setSocietyCode(response.getDnpCode().toString());
								response.setSocietyName(response.getDnpName());
							}
						}
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
	}

	/*********************************************************************************************************************************/

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	String entityTypeCode;

	public String gotoAddPage() {
		disableBtnProductAdd = true;
		disableAddProduct = true;
		stockTransferItems = new StockTransferItems();
		stockTransfer = new StockTransfer();
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		entityTypeCode = null;
		stockTransferResponse = new StockTransferResponse();
		currentStockStatusRequest = new CurrentStockStatusRequest();
		selectedSupplierTypeMaster = new SupplierTypeMaster();
		selectedCircleMaster = new CircleMaster();
		selectedSupplierMaster = new SupplierMaster();
		selectedTransportTypeMaster = new TransportTypeMaster();
		gstNumber = "";
		outwardFrom = "";

		/*
		 * Login Entity set as a From Entity
		 */

		stockTransfer.setFromEntityMaster(loginEmployee.getPersonalInfoEmployment().getWorkLocation());

		entityTypeMasterList = commonDataService.findAllEntityTypes();
		unitRate = null;
		unitRateList = new ArrayList<>();

		return STOCK_OUTWARD_CREATE;
	}

	public void onChangeEntityTypeMaster() {
		log.info("onChangeEntityTypeMaster is called [" + entityTypeCode + "]");

		if (entityTypeCode != null && entityTypeCode != "") {
			showroomList = commonDataService.findEntityByEntityType(entityTypeCode);

			removeCurrentLoggedInItem(showroomList, getLoginEmployeeWorkLocation().getId());

		} else {
			showroomList = null;
		}
	}

	public void removeCurrentLoggedInItem(List<EntityMaster> entityMasterList, Long idToRemove) {
		Iterator<EntityMaster> it = entityMasterList.iterator();
		while (it.hasNext()) {
			EntityMaster em = it.next();
			if (em.getId().longValue() == idToRemove.longValue()) {
				it.remove();
			}
		}
	}

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	CurrentStockStatusRequest currentStockStatusRequest;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	boolean disableAddProduct = false;

	public void onChangeShowroom() {
		log.info("onChangeShowroom is called ");

		if (stockTransfer.getToEntityMaster() != null) {
			log.info("Selected ISSR Showroom is [" + stockTransfer.getToEntityMaster() + "]");

			productCategoryList = commonDataService.loadProductCategories();
			currentStockStatusRequest = new CurrentStockStatusRequest();
			disableBtnProductAdd = false;
			populateTransportFormData();
		}
	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");
		currentStockStatusRequest.setProductGroupMaster(null);
		currentStockStatusRequest.setProductVarietyMaster(null);

		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductCategory() != null) {
			log.info("Selected Product Category " + currentStockStatusRequest.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(currentStockStatusRequest.getProductCategory());
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();

		}
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + currentStockStatusRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(currentStockStatusRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");
		stockTransferItems = new StockTransferItems();
		if (currentStockStatusRequest != null && currentStockStatusRequest.getProductVarietyMaster() != null
				&& unitRate != null) {
			log.info("Selected Product Variety " + currentStockStatusRequest.getProductVarietyMaster().getCode());
			StockTransferItems items = findInventoryItems(stockTransfer.getFromEntityMaster().getId(),
					currentStockStatusRequest.getProductVarietyMaster().getId(), unitRate);

			if (items != null) {
				stockTransferItems = items;
				disableAddProduct = false;
				RequestContext.getCurrentInstance().update("avaliableQtyMeter");
			} else {
				disableAddProduct = true;
			}
		}
	}

	public StockTransferItems findInventoryItems(Long entityId, Long productId, Double unitPrice) {
		log.info("findItemByQRCode method is executing..");

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/stock/transfer/inspection/center/getstockdetails";

		StockTransferItems stockTransferItems = null;

		try {
			StockTransferItemsDTO itemsDTO = new StockTransferItemsDTO();
			itemsDTO.setProductId(productId);
			itemsDTO.setUnitRate(unitPrice);
			itemsDTO.setEntityId(entityId);

			BaseDTO baseDTO = httpService.post(URL, itemsDTO);

			if (baseDTO != null) {
				log.info("status code------------>" + baseDTO.getStatusCode());
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer Items get successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					stockTransferItems = mapper.readValue(jsonResponse, StockTransferItems.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				throw new RestException(ErrorDescription.FAILURE_RESPONSE);
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return stockTransferItems;
	}

	public String validateCurrentDispatchQty() {

		log.info("stockTransferItems.getDispatchedQty() " + stockTransferItems.getDispatchedQty());
		log.info("stockTransferItems.getReceivedQty() " + stockTransferItems.getReceivedQty());
		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getReceivedQty() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				stockTransferItems.setDispatchedQty(null);
				return null;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getReceivedQty()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				stockTransferItems.setDispatchedQty(null);
				return null;
			}
		}

		return null;
	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	Set<String> setOfBundleNumbers = new HashSet<>();

	public String addProduct() {

		log.info("Product is being added [" + stockTransferItems + "]");

		if (stockTransferItems.getDispatchedQty() != null && stockTransferItems.getReceivedQty() != null) {
			if (stockTransferItems.getDispatchedQty() <= 0) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_ZERO.getCode());
				return null;
			}
			if (stockTransferItems.getDispatchedQty() > stockTransferItems.getReceivedQty()) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_DISPATCH_MORE_THAN_AVAILABLILITY.getCode());
				return null;
			}
		}

		stockTransferItems.setUnitRate(unitRate);
		stockTransferItems.setItemNetTotal(unitRate * stockTransferItems.getDispatchedQty());
		stockTransferItems.setReceivedQty(0.0);
		stockTransfer.getStockTransferItemsList().add(stockTransferItems);

		if (stockTransferItems.getBundleNumber() != null) {
			setOfBundleNumbers.add(stockTransferItems.getBundleNumber());
			updateBundleNumbers(setOfBundleNumbers);
		}

		stockTransferItems = new StockTransferItems();

		currentStockStatusRequest.setProductVarietyMaster(null);

		unitRate = null;

		return null;
	}

	public void removeProduct(StockTransferItems stockTransferItems) {

		log.info("Product is being added [" + stockTransferItems + "]");

		stockTransfer.getStockTransferItemsList().remove(stockTransferItems);

		setOfBundleNumbers = new HashSet<>();
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				if (items.getBundleNumber() != null) {
					setOfBundleNumbers.add(items.getBundleNumber());
				}
			}
		}
		updateBundleNumbers(setOfBundleNumbers);
	}

	public String gotoEditPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());
			disableBtnProductAdd = false;

			if (stockTransfer.getTransportMaster() != null) {
				selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
			}

			if (selectedTransportTypeMaster != null) {
				transportMasterList = commonDataService
						.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
			}

			if (stockTransferResponse.getToEntityId() == stockTransferResponse.getFromEntityId()) {
				outwardFrom = ApplicationConstants.EXTERNEL;
				if (stockTransfer.getSupplierMaster() != null) {
					selectedSupplierMaster = stockTransfer.getSupplierMaster();
					SupplierMaster supplierMaster = commonDataService.getSupplierDetailsByCode(
							stockTransfer.getSupplierMaster().getCode(), AppUtil.getPortalServerURL());
					if (supplierMaster != null) {
						selectedSupplierTypeMaster = supplierMaster.getSupplierTypeMaster();
						loadSupplierCodeName();
						selectedCircleMaster = supplierMaster.getCircleMaster();
						selectCircle();
						selectedSupplierMaster = supplierMaster;
						gstNumber = supplierMaster.getGstNumber();
					}
				}
				loadSupplierType();
			} else {
				outwardFrom = ApplicationConstants.INTERNEL;
				if (stockTransfer.getToEntityMaster() != null) {
					entityTypeCode = stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode();
					onChangeEntityTypeMaster();
					entityTypeMasterList = commonDataService.findAllEntityTypes();
				}
			}
			// outwardFromChange();
			// showroomList =
			// commonDataService.findEntityByEntityType(EntityType.INSTITUTIONAL_SALES_SHOWROOM);
			populateTransportFormData();
			productCategoryList = commonDataService.loadProductCategories();
			currentStockStatusRequest = new CurrentStockStatusRequest();
			disableBtnProductAdd = false;
			stockTransferItems = new StockTransferItems();
			disableAddProduct = true;

		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

		return STOCK_OUTWARD_CREATE;
	}

	public String gotoViewPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());
		return STOCK_OUTWARD_VIEW;
	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {
			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferResponse.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	/***************************************************************************************************************************************/

	/*********************************************************************************/
	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<Object> payToPayList;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplierTypeMaster;

	final String society = SupplierType.SOCIETY;

	@Getter
	@Setter
	CircleMaster selectedCircleMaster;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	SupplierMaster selectedSupplierMaster;

	@Getter
	@Setter
	Boolean circleflag = false;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	private String outwardFrom;

	@Getter
	@Setter
	EntityMaster entityMaster;

	private String serverURL = null;

	@Getter
	@Setter
	String gstNumber;

	public void populateTransportFormData() {
		transportTypeMasterList = commonDataService.getAllTransportTypes();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
	}

	public void onChangeTransportTypeMaster() {
		transportMasterList = null;
		stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void onChangeTransport() {
		log.info("onSelectTransport is called");
		if (stockTransfer.getTransportMaster() != null) {
			log.info("Selected Transport ID = [" + stockTransfer.getTransportMaster().getId() + "]");
		}
	}

	/*************************************************************************************************************************************/

	public String submit(String action) {

		if (StringUtils.isNotEmpty(outwardFrom)) {
			if (outwardFrom.equals("Externel")) {
				if (selectedSupplierTypeMaster == null || selectedSupplierTypeMaster.getId() == null) {
					errorMap.notify(
							ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_TYPE_MASTER.getErrorCode());
					return null;
				}
				if (circleflag) {
					if (selectedCircleMaster == null || selectedCircleMaster.getId() == null) {
						errorMap.notify(
								ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_CIRCLE_MASTER.getErrorCode());
						return null;
					}
				}
				if (selectedSupplierMaster == null || selectedSupplierMaster.getId() == null) {
					errorMap.notify(ErrorDescription.STOCK_TRANS_SUPPLIER_REQUIRED.getErrorCode());
					return null;
				}
				stockTransfer.setToEntityMaster(stockTransfer.getFromEntityMaster());
				stockTransfer.setSupplierMaster(selectedSupplierMaster);
			} else if (outwardFrom.equals("Internel")) {

				if (entityTypeCode == null || entityTypeCode == "") {
					AppUtil.addWarn("Please select Entity Type Code / Name");
					return null;
				}

				if (stockTransfer.getToEntityMaster() == null || stockTransfer.getToEntityMaster().getId() == null) {
					AppUtil.addWarn("Please select Stock Outward to");
					return null;
				}
			}
		} else {
			AppUtil.addWarn("Please select Entity Type");
			return null;
		}

		if (stockTransfer.getStockTransferItemsList() == null
				|| stockTransfer.getStockTransferItemsList().size() <= 0) {
			AppUtil.addWarn("Please Add Product for Outward");
			return null;
		}

		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}

		stockTransfer.setDateTransferred(new Date());
		stockTransfer.setTransferType(stockTransferType);
		stockTransfer.setEntityType(outwardFrom);

		return create();
	}

	public String create() {
		log.info("create method is executing..");

		/**
		 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller
		 * 
		 * StockTransferICController.java
		 */
		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/inspection/center/outward/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					if (StringUtils.isNotEmpty(msg)) {
						AppUtil.addWarn(msg);
						return null;
					} else {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return STOCK_OUTWARD_LIST;
	}

	// Load Supplier Type List
	public void loadSupplierType() {
		log.info(":::<- Load Supplier TypeStart ->:::");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllSupplierType";
			log.info("::: Supplier Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get Supplier Response :");
			if (baseDTO.getStatusCode() == 0) {
				supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
				log.info("Supplier load Successfully " + baseDTO.getTotalRecords());

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
				log.info("StockItemoutwardList Page Convert Error -->");
			} else {
				log.warn("Supplier Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Supplier load ", ee);
		}

	}

	// Load Supplier Code / Name
	public void loadSupplierCodeName() {
		try {
			gstNumber = null;
			if (!society.equals(selectedSupplierTypeMaster.getCode())) {
				circleflag = false;
				selectedCircleMaster = new CircleMaster();
				log.info(":::<- Load loadSupplierCodeName Called ->::: " + selectedSupplierTypeMaster.getCode());
				BaseDTO baseDTO = null;
				selectedSupplierMaster = new SupplierMaster();
				// selectedSupplierMaster.setId(selectedSupplierTypeMaster.getId());

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/stockItemInwardPNS/getSelectedSupplierMasterDetailswithGst/"
						+ selectedSupplierTypeMaster.getId();
				log.info("::: supplierMaster Controller calling  :::");
				baseDTO = httpService.get(url);
				log.info("::: get supplierMaster Response :");
				if (baseDTO.getStatusCode() == 0) {

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});

					log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
				} else {
					log.warn("supplierMaster Load Error ");
				}

				// supplierMasterList=commonDataService.loadSupplierBySupplierTypeId(SERVER_URL,
				// selectedSupplierTypeMaster.getId());
			} else {
				supplierMasterList = null;
				circleflag = true;
				loadAllActiveCircleMaster();
				// circleMasterList = commonDataService.loadCircles();
			}
		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public void loadAllActiveCircleMaster() {

		try {
			selectedSupplierMaster = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllCircle";
			log.info("::: StockItemInwardPNS Controller calling  :::");
			BaseDTO baseDTO = httpService.get(url);
			log.info("::: get loadAllActiveCircleMaster Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				circleMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CircleMaster>>() {
				});
				log.info("circleMasterList load Successfully " + baseDTO.getTotalRecords());
			} else {
				log.warn("circleMasterList Load Error ");
			}
		} catch (Exception e) {
			log.error(" ==== >>>> Exception Occured In Circle MAster circleMasterList ==== >> ");
		}

	}

	public void outwardFromChange() {
		try {
			entityMaster = new EntityMaster();
			selectedSupplierMaster = new SupplierMaster();
			selectedSupplierTypeMaster = new SupplierTypeMaster();
			selectedCircleMaster = new CircleMaster();
			gstNumber = "";
			entityTypeMasterList = new ArrayList<>();
			if (outwardFrom.equals("Internel")) {
				entityTypeMasterList = commonDataService.findAllEntityTypes();
			} else if (outwardFrom.equals("Externel")) {
				loadSupplierType();

			} else

			{
				log.error("Select Entity Type");
			}
		} catch (Exception e) {
			log.error("outwardFromChange method exception-----", e);
		}
	}

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete Autocomplete query==>" + query);
		List<SupplierMaster> results = new ArrayList<SupplierMaster>();
		Long circleId = null;

		if (selectedSupplierMaster != null) {
			String supplierCode = query;
			if (selectedCircleMaster != null) {
				circleId = selectedCircleMaster.getId();
			}
			if (circleId == null) {
				if (supplierMasterList != null) {
					for (SupplierMaster master : supplierMasterList) {
						if (master.getName().contains(supplierCode.toUpperCase())
								|| master.getCode().contains(supplierCode)) {
							results.add(master);
						}
					}
				}
			} else {
				supplierMasterList = new ArrayList<>();
				supplierMasterList = commonDataService.loadSupplierAutoCompleteCodeAndCircle(serverURL, supplierCode,
						circleId);
				results.addAll(supplierMasterList);
			}
		} else {
			AppUtil.addWarn("Select product group master");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

		return results;
	}

	public void selectAutoSearch() {
		log.info("<<=== inside selectAutoSearch");
		if (supplierMasterList.contains(selectedSupplierMaster)) {
			log.info("<<=== selected SupplierMaster", selectedSupplierMaster);
			gstNumber = selectedSupplierMaster.getGstNumber();
		}
		productCategoryList = commonDataService.loadProductCategories();
		currentStockStatusRequest = new CurrentStockStatusRequest();
		disableBtnProductAdd = false;
		populateTransportFormData();

	}

	public void selectCircle() {
		log.info("<<=== inside selectCircle");
		gstNumber = "";
		selectedSupplierMaster = new SupplierMaster();

	}

	public void loadUnitRate() {
		log.info("loadUnitRate method starts--------");
		Long workLocationId = null;
		Long productId = null;
		try {
			unitRateList = new ArrayList<>();
			if (loginBean.getEmployee() != null) {
				workLocationId = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
			}
			if (currentStockStatusRequest.getProductVarietyMaster() != null
					&& currentStockStatusRequest.getProductVarietyMaster().getId() != null) {
				productId = currentStockStatusRequest.getProductVarietyMaster().getId();
			}

			if (workLocationId != null && productId != null) {
				String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/stock/transfer/inspection/center/getunitratelist/" + workLocationId + "/" + productId;
				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("Stock Transfer Items get successfully");
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						unitRateList = mapper.readValue(jsonResponse, new TypeReference<List<Double>>() {
						});
						log.info("loadUnitRate response success--list size------" + unitRateList.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						errorMap.notify(baseDTO.getStatusCode());
					}
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
		}
		log.info("loadUnitRate method ends--------");
	}

	public List<ProductVarietyMaster> productVarietyAutoComplete(String varietyCodeOrName) {
		log.info("productVarietyAutoComplete method starts----- ");
		List<ProductVarietyMaster> productList = new ArrayList<ProductVarietyMaster>();
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductVarietyMasterList/"
					+ currentStockStatusRequest.getProductGroupMaster().getId() + "/" + varietyCodeOrName;
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productList = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyMaster>>() {
				});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
		log.info("productVarietyAutoComplete method ends----- ");
		return productList;
	}

	@Getter
	@Setter
	Double deliverychallanTotalAmount = 0.0;

	public String createDeliveryChallan() {
		log.info("<==== Starts createDeliveryChallan ====>");
		try {
			deliverychallanTotalAmount = 0.0;
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				return null;
			}

			if (!stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
				// errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				AppUtil.addWarn("Delivery challan only for submitted records");
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

			transferItemsList = new ArrayList<>();

			if (stockTransfer != null) {

				Map<Long, List<StockTransferItems>> stockTransferItems = stockTransfer.getStockTransferItemsList()
						.stream().collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));

				if (stockTransferItems != null) {

					for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItems.entrySet()) {

						Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));

						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount
								.entrySet()) {

							DeliveryChallanDTO dto = new DeliveryChallanDTO();

							dto.setId(entrynew.getValue().get(0).getId());
							dto.setHsnCode(entrynew.getValue().get(0).getProductVarietyMaster().getHsnCode());
							dto.setItem(entrynew.getValue().get(0).getProductVarietyMaster().getCode());
							dto.setProductName(entrynew.getValue().get(0).getProductVarietyMaster().getName());
							dto.setUnitRate(entrynew.getValue().get(0).getUnitRate());
							Double Qty = entrynew.getValue().stream().mapToDouble(t -> t.getDispatchedQty()).sum();
							dto.setQuantity(Qty);
							// dto.setQuantity(entrynew.getValue().size());

							Double rate = entrynew.getValue().get(0).getUnitRate() * dto.getQuantity();
							dto.setProductRate(rate);

							DecimalFormat decim = new DecimalFormat("0.00");

							String price = decim.format(dto.getUnitRate());
							dto.setUnitPrice(price.split("\\.")[0]);
							dto.setUnitPaisa(price.split("\\.")[1]);

							String p = decim.format(rate);
							dto.setTotalPriceValue(rate);
							dto.setTotalPrice(p.split("\\.")[0]);
							dto.setTotalPaisa(p.split("\\.")[1]);
							deliverychallanTotalAmount = deliverychallanTotalAmount + (rate != null ? rate : 0.0);
							transferItemsList.add(dto);
						}
					}
					/*
					 * for(DeliveryChallanDTO deliveryChallanDTO: transferItemsList) {
					 * DeliveryChallanDTO dto=new DeliveryChallanDTO(); dto=deliveryChallanDTO;
					 * transferItemsList.add(dto); }
					 */

					log.info("transferItemsList size is ::::::" + transferItemsList.size());

					Map<String, List<DeliveryChallanDTO>> hsnCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(r -> r.getHsnCode()));
					hsnSummaryList = new ArrayList<>();
					productCodeSummaryList = new ArrayList<>();
					if (hsnCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : hsnCodeWiseGroupingDTO.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setHsnCode(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							hsnSummaryList.add(deliveryChallanDTO);
						}
					}

					Map<String, List<DeliveryChallanDTO>> productCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(c -> c.getItem()));
					if (productCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : productCodeWiseGroupingDTO
								.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setItem(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							productCodeSummaryList.add(deliveryChallanDTO);
						}
					}
					transferItemsList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					hsnSummaryList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					productCodeSummaryList.sort((s1, s2) -> s1.getItem().compareTo(s2.getItem()));
					/*
					 * abstractList = new ArrayList<>();
					 * 
					 * if (transferItemsList.size() <= 10) { List<DeliveryChallanDTO> tempList =
					 * transferItemsList.subList(0, transferItemsList.size()); DeliveryChallanDTO
					 * abs = new DeliveryChallanDTO(); abs.setQuantity(tempList.stream().mapToInt(t
					 * -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs); } else { for(int i = 1; i
					 * < transferItemsList.size(); i++) {
					 * 
					 * if(i==1) { List<DeliveryChallanDTO> tempList = transferItemsList.subList(0,
					 * 10); DeliveryChallanDTO abs = new DeliveryChallanDTO();
					 * abs.setQuantity(tempList.stream().mapToInt(t -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs); i=9; }else { if
					 * (transferItemsList.size() > 30) { List<DeliveryChallanDTO> tempList =
					 * transferItemsList.subList(i, i+20); DeliveryChallanDTO abs = new
					 * DeliveryChallanDTO(); abs.setQuantity(tempList.stream().mapToInt(t ->
					 * t.getQuantity()).sum()); abs.setProductRate(tempList.stream().mapToDouble(t
					 * -> t.getProductRate()).sum()); abstractList.add(abs); i=i+20;
					 * 
					 * }else { List<DeliveryChallanDTO> tempList = transferItemsList.subList(i,
					 * transferItemsList.size()); DeliveryChallanDTO abs = new DeliveryChallanDTO();
					 * abs.setQuantity(tempList.stream().mapToInt(t -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs);
					 * i=transferItemsList.size(); } }
					 * 
					 * } }
					 */

				}

				// log.info("abstractList size is ::::::" + abstractList.size());

				return DELIVERY_CHALLAN_ACKNOWLEDGE;
			} else {
				log.info("Object is null");
			}
			log.info("<==== Ends createDeliveryChallan ====>");
			return null;
		} catch (Exception e) {
			// log.info("abstractList size is ::::::" + abstractList.size());
			log.error("Error in createDeliveryChallan Method", e);
		}
		return null;
	}

	public String getValue(Double p) {
		DecimalFormat decim = new DecimalFormat("0.00");

		String price = decim.format(p);

		return price;
	}

}
