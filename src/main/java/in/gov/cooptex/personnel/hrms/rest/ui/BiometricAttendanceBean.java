package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.hrms.dto.BiometricAttendanceSynchDTO;
import in.gov.cooptex.personnel.hrms.attendance.model.BiometricAttendanceSynch;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("biometricAttendanceBean")
public class BiometricAttendanceBean {

	final String VIEW_BIOMETRIC_PAGE="/pages/personnelHR/manualAttendance/viewBiometric.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();  

	@Autowired
	AppPreference appPreference; 

	@Autowired
	CommonDataService commonDataService; 

	@Getter @Setter
	List<EntityMaster> headRegionOfficeList;
	
	@Autowired
	LoginBean loginBean;

	@Getter @Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter @Setter
	List<EntityMaster> entityList;
	
	@Getter @Setter
	Date currentDate = new Date();

	@Getter @Setter
	List<Department> departmentList;

	@Getter @Setter
	List<Designation> designationList;

	@Getter @Setter
	List<EmployeeMaster> employeeList;

	@Getter @Setter
	List<BiometricAttendanceSynch> biometricAttendanceList;

	@Getter @Setter
	BiometricAttendanceSynch selectedBiometricAttendance;

	@Getter @Setter
	LazyDataModel<BiometricAttendanceSynch> biometricAttendanceLazyList;

	@Getter @Setter
	BiometricAttendanceSynchDTO biometricAttendanceDto;
	
	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper objectMapper = new ObjectMapper();

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	BiometricAttendanceSynch searchBiometricAttendance = new BiometricAttendanceSynch();

	public String goToViewBiometricPage() {
		log.info("<=== Starts BiometricBean.goToViewBiometricPage===>");
		biometricAttendanceList = new ArrayList<>();
		searchBiometricAttendance = new BiometricAttendanceSynch();
		visibility = false;
		//headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
		loadHOorRO();
		//entityTypeList = commonDataService.getEntityTypeByStatus();
		//		entityList = commonDataService.loadEntityMaster();
		loadAllEntityTypeList();
		departmentList = commonDataService.getDepartment();

		designationList = commonDataService.loadDesignation();
		
		biometricAttendanceDto = new BiometricAttendanceSynchDTO();
		//		loadLazyBiometricAttendaceList();
		log.info("<=== Ends BiometricBean.goToViewBiometricPage===>");
		return VIEW_BIOMETRIC_PAGE;
	}
	
	
	public void loadHOorRO() {
		log.info("... Load  loadHOorRO ....");
		try {
			Long userId = loginBean.getUserDetailSession().getId();
			String url = SERVER_URL + "/entitymaster/loadhoorro/"+ userId; 
			log.info("loadHOorRO :: url==> " + url);
			objectMapper = new ObjectMapper();
			BaseDTO response = httpService.get(url);
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			headRegionOfficeList = objectMapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if(headRegionOfficeList != null && !headRegionOfficeList.isEmpty()) {
				log.info("headRegionOfficeList size==> "+headRegionOfficeList.size());
			}
			
		} catch (Exception e) {
			log.error("Exception occured while loading Entity by region data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		
	}

	/*public void updateEntityList() {
		log.info("<=Starts BiometricBean.updateEntityList =>");
		if(searchBiometricAttendance.getEntityType() != null && searchBiometricAttendance.getEntityType().getId() != null &&
				searchBiometricAttendance.getRegionOffice() != null && searchBiometricAttendance.getRegionOffice().getId() != null) {
			entityList = commonDataService.loadEntityByregionOrentityTypeId(searchBiometricAttendance.getRegionOffice().getId(), searchBiometricAttendance.getEntityType().getId());
		}else {
			return;
		}

		log.info("<=Ends BiometricBean.updateEntityList =>");
	}*/

	public void updateEntityList() {
		log.info("<=Starts BiometricBean.updateEntityList =>");
		try {
			biometricAttendanceDto.setEmpId(null);
			if (biometricAttendanceDto.getRegionHeadOffice() != null
					&& biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster() != null && biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
				biometricAttendanceDto.setEntityId(biometricAttendanceDto.getRegionHeadOffice().getId());
				visibility =true;
				biometricAttendanceDto.setEntityType(null);
			} else {
				visibility =false;
			}
			if (biometricAttendanceDto.getRegionHeadOffice() != null
					&& biometricAttendanceDto.getRegionHeadOffice().getId() != null
					&& biometricAttendanceDto.getEntityType() != null 
					&& biometricAttendanceDto.getEntityType().getId() != null) {
				Long regionId = biometricAttendanceDto.getRegionHeadOffice().getId();
				//entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, biometricAttendanceDto.getEntityType().getId());
				onEntityTypeChange();
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends BiometricBean.updateEntityList =>");
	}
	
	public void onEntityTypeChange() {
		log.info(":: Entity type change ::");
		try {
				Long regionId = biometricAttendanceDto.getRegionHeadOffice().getId();
				String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/activeEntity/" + regionId + "/"
						+ biometricAttendanceDto.getEntityType().getId();
				log.info("URL request for entity :: " + resource);
				BaseDTO baseDto = httpService.get(resource);
				jsonResponse = objectMapper.writeValueAsString(baseDto.getResponseContents());
				entityList = objectMapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("Entity location list size is" + entityList.size());
			
		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}


	public String clear() {
		log.info("<=== Starts BiometricBean.clear===>");
		searchBiometricAttendance = new BiometricAttendanceSynch();
		biometricAttendanceDto = new BiometricAttendanceSynchDTO();
		biometricAttendanceList = new ArrayList<>();
		visibility = false;
		log.info("<=== Ends BiometricBean.clear===>");
		return VIEW_BIOMETRIC_PAGE;
	}
	
	public void loadAllEntityTypeList() {
		log.info("... Load  Entity Type Master called ....");
		try {
			/*
			 * EntityTypeMasterController.java - Method: getEntityType()
			 */
			BaseDTO response = httpService.get(AppUtil.getPortalServerURL() + "/entitytypemaster/getall/entitytype");
			jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
			entityTypeList = objectMapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Entity Type Master data....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void updateDesginationList() {
		log.info("<==== Starts BiometricBean.updateDesignationList ======>");
		if(biometricAttendanceDto == null || biometricAttendanceDto.getDepartmentId() == null)
			return;

		log.info("<==== Department id..."+biometricAttendanceDto.getDepartmentId());
		try {
			String url=SERVER_URL+"/designation/getalldesignation/"+biometricAttendanceDto.getDepartmentId();
			BaseDTO response = httpService.get(url);
			if(response != null && response.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
				designationList = objectMapper.readValue(jsonResponse,new TypeReference<List<Designation>>(){
				});	
			}
		}catch(Exception e) {
			log.info("Exception occured in updateDesginationList...........",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return;
		}
		log.info("<==== Starts BiometricBean.updateDesignationList ======>");
	}


	public void updateEmployeeList() {
		log.info("<==== Starts BiometricBean.updateEmployeeList ======>");

		if(biometricAttendanceDto == null || biometricAttendanceDto.getDesignationId() == null || 
				biometricAttendanceDto.getRegionHeadOffice() == null || (biometricAttendanceDto.getRegionHeadOffice() != null
				&& biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster() != null 
				&& !biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")
				&& biometricAttendanceDto.getEntityType() == null )) {
			return;
		}	

		log.info("<==== Designation id..."+biometricAttendanceDto.getDesignationId());

		try {
			String url=SERVER_URL+"/employee/getemployeeListByDesignation";
			BaseDTO response = httpService.post(url,biometricAttendanceDto);
			if(response != null && response.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
				employeeList = objectMapper.readValue(jsonResponse,new TypeReference<List<EmployeeMaster>>(){
				});	
			}
		}catch(Exception e) {
			log.info("Exception occured in updateEmployeeList...........",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return;
		}
		log.info("<==== Ends BiometricBean.updateEmployeeList ======>");
	}


	public void search() {
		log.info("<==== Starts BiometricBean.search ======>");
		try {
			biometricAttendanceList=new ArrayList<>();
			if(biometricAttendanceDto.getRegionHeadOffice() == null || biometricAttendanceDto.getRegionHeadOffice().getId() == null) {
				log.info("HO/RO is required......");
				errorMap.notify(ErrorDescription.BIOMETRIC_HO_RO_NULL.getCode());
				return;
			}
			/*if(biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster() != null && 
					!biometricAttendanceDto.getRegionHeadOffice().getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE") &&
					(biometricAttendanceDto.getEntityType() == null || biometricAttendanceDto.getEntityType().getId() == null)) {
				log.info("Entity type is required......");
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTITY_TYPE_NULL.getCode());
				return;
			}*/
			/*
			if(searchBiometricAttendance.getWorkLocation() == null || searchBiometricAttendance.getWorkLocation().getId() == null) {
				log.info("Entity is required......");
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTITY_NULL.getCode());
				return;
			}*/
			if(biometricAttendanceDto.getStartDate() == null) {
				log.info("start date is required......");
				errorMap.notify(ErrorDescription.BIOMETRIC_START_DATE_NULL.getCode());
				return;
			}
			if(biometricAttendanceDto.getEndDate()== null) {
				log.info("End date is required......");
				errorMap.notify(ErrorDescription.BIOMETRIC_END_DATE_NULL.getCode());
				return;
			}
			String url = SERVER_URL+appPreference.getOperationApiUrl()+"/empbiometricattendancesync/search";
			BaseDTO response = httpService.post(url,biometricAttendanceDto);
			if(response != null && response.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
				biometricAttendanceList = objectMapper.readValue(jsonResponse,new TypeReference<List<BiometricAttendanceSynch>>(){
				});	
				if(biometricAttendanceList != null)
					log.info("Result size is................"+biometricAttendanceList.size());
			}else if(response != null && response.getStatusCode() !=0) {
				errorMap.notify(response.getStatusCode());
				return;
			}else {
				log.info("Response :........."+response);
			}
		}catch(Exception e) {
			log.info("Exception occured in search...........",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return;
		}
		log.info("<==== Ends BiometricBean.search ======>");
	}



	/*public void loadLazyBiometricAttendaceList() {
		log.info("<==== Starts BiometricBean.loadLazyBiometricAttendaceList =====>");
		biometricAttendanceLazyList = new LazyDataModel<BiometricAttendanceSynch>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<BiometricAttendanceSynch> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL + "/getallcommunitylistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = objectMapper.writeValueAsString(response.getResponseContents());
						biometricAttendanceList = objectMapper.readValue(jsonResponse, new TypeReference<List<BiometricAttendanceSynch>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return biometricAttendanceList;
			}

			@Override
			public Object getRowKey(BiometricAttendanceSynch res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BiometricAttendanceSynch getRowData(String rowKey) {
				for (BiometricAttendanceSynch biometricAttendance : biometricAttendanceList) {
					if (biometricAttendance.getId().equals(Long.valueOf(rowKey))) {
						selectedBiometricAttendance = biometricAttendance;
						return biometricAttendance;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}*/

	public void updateEndDate() {
		log.info("UpdateEndDate is called......."+biometricAttendanceDto.getStartDate());
		if(biometricAttendanceDto.getStartDate() != null && biometricAttendanceDto.getEndDate() != null
				&& biometricAttendanceDto.getEndDate().before(biometricAttendanceDto.getStartDate())) {
			log.info("EndDate is before of startDate.........");
			biometricAttendanceDto.setEndDate(null);
		}
	}
	
}
