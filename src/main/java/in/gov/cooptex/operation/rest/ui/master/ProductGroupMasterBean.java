package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.ThreadLocalConfig;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.InfoCodeDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.operation.dto.ProductGroupMasterDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Abhishek
 *
 */
@Log4j2
@Service
@Scope("session")
public class ProductGroupMasterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Setter
	@Getter
	private Long id;

	@Setter
	@Getter
	private String code;

	@Setter
	@Getter
	private String name;

	@Setter
	@Getter
	private String lname;

	RestTemplate restTemplate;

	String Url = null;

	@Getter
	@Setter
	String statusValue = null;

	@Setter
	@Getter
	ProductGroupMasterDTO productGroupDto = new ProductGroupMasterDTO();

	@Setter
	@Getter
	List<ProductGroupMasterDTO> productGroupList;

	@Setter
	@Getter
	List<ProductCategoryDTO> productCategoryDtoList;

	@Getter
	@Setter
	private List<ProductCategoryDTO> productCategoryListDto = new ArrayList<ProductCategoryDTO>();

	@Setter
	@Getter
	ProductCategoryDTO productCategoryDTO = new ProductCategoryDTO();
	
	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
	
	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup;

	@Getter
	@Setter
	LazyDataModel<ProductGroupMasterDTO> productGrouplazyLoadList;

	@Getter
	@Setter
	private Map<String, Object> filterMap;
	@Getter
	@Setter
	private SortOrder sortingOrder;
	@Getter
	@Setter
	private String sortingField;
	@Getter
	@Setter
	private Integer resultSize;

	@Setter
	@Getter
	List<ProductCategoryDTO> productCategoryList;

	@Setter
	@Getter
	WrapperDTO wrapperDto = new WrapperDTO();

	@Setter
	@Getter
	String categoryCode;

	@Setter
	@Getter
	String categoryName;

	@Setter
	@Getter
	String buttonName;

	@Getter
	@Setter
	private Boolean addButtonFlag = true, editButtonFlag = true, deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	Integer numberOfRecords = 0;
	
	BaseDTO baseDTO = new BaseDTO();

	@Setter
	@Getter
	String action;

	@Setter
	@Getter
	String pageHead;

	@Autowired
	LanguageBean languageBean;

	@Setter
	@Getter
	boolean disable;

	@Autowired
	HttpService httpService;
	@Autowired
	private ErrorMap errorMap;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Setter
	@Getter
	private Boolean activeStatus;

	public ProductGroupMasterBean() {
		restTemplate = new RestTemplate();
		lazyLoadProductGroupList();
		log.info("Inside ProductGroupMasterBean");
		loadValues();
		log.info("call loadValues();");
	}

	private void loadValues() {
		try {
			Url = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	@PostConstruct
	public void init() {
		log.info("====init()=====");
		// getCategoryMasterList();
		getCategoryGroupMasterList();
		lazyLoadProductGroupList();
//		getAllCategory();
		// getAllGroup();
	}

	public String showProductGroup() {

		log.info("showProductGroup ::");
		action = "ADD";
		selectedProductCategoryGroup = new ProductCategoryGroup();
		productCategoryDTO = new ProductCategoryDTO();
		productGroupDto = new ProductGroupMasterDTO();
		code = null;
		lname = null;
		name = null;
		addButtonFlag = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		lazyLoadProductGroupList();
		// getAllGroup();

		// return "/pages/master/productGroupMaster.xhtml?faces-redirect=true";
		return "/pages/masters/listProductGroup.xhtml?faces-redirect=true";
	}

	public String showAddForm() {
		log.info("<<==== show add form  =====>>");
		action = "ADD";
		pageHead = "Create";
		getCategoryGroupMasterList();
//		getAllCategory();
		code = null;
		name = null;
		lname = null;
		productCategoryDTO = new ProductCategoryDTO();
		productGroupDto = new ProductGroupMasterDTO();
		activeStatus = true;

		return "/pages/masters/createProductGroup.xhtml?faces-redirect=true";
	}

	public String submitGroupMaster() {

		log.info("------submitGroupMaster()---->");
		try {
			if (action.equals("ADD") || action.equals("EDIT")) {

				if (code == null || code.trim().length() < 3) {
					log.info(name.length());
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.GRPUP_CODE_VALID_LENGTH.getErrorCode(), languageBean.getLocaleCode()));
					log.info("Name should be greater than 3 characters");
					return null;
				}

				if (name == null || name.trim().length() < 3) {
					log.info(name.length());
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.GRPUP_NAME_VALID_LENGTH.getErrorCode(), languageBean.getLocaleCode()));
					log.info("Name should be greater than 3 characters");
					return null;
				}

				if (lname == null || lname.trim().length() < 3) {
					log.info(lname.length());
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.GROUP_LOCAL_NAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					log.info("Local name should be greater than 3 characters");
					return null;
				}

			}

			ProductGroupMasterDTO productGroupDTOSave = new ProductGroupMasterDTO();
			// restTemplate = new RestTemplate();
			UserDTO userDto = new UserDTO();
			userDto.setId(loginBean.getUserDetailSession().getId());
			if (action.equals("ADD")) {
				log.info("<<====  GROUP ADD  =====>>" + productGroupDto.getId());
				productGroupDTOSave.setCreatedBy(userDto);
				productGroupDTOSave.setCreatedDate(new Date());
			}
			if (action.equals("EDIT")) {
				log.info("<<====  GROUP EDIT  =====>>" + productGroupDto.getId());
				productGroupDTOSave.setId(productGroupDto.getId());
			}

			log.info("<<=====  product category Dto:: " + productCategoryDTO);

			if (productCategoryDTO != null && productCategoryDTO.getId() != null && productCategoryDTO.getId() > 0) {
				log.info("<<=======  Category id::" + productCategoryDTO.getId());
				productGroupDTOSave.setProductCategoryDTO(productCategoryDTO);
			} /*
				 * else { Util.addError(ErrorCodeDescription.getDescription(
				 * ErrorCodeDescription.ERRO_PRODUCT_CATEGORY_REQ.getErrorCode() ,
				 * languageBean.getLocaleCode())); return null; }
				 */
			// productGroupDTO.setProductCategoryDTO(productGroupDto.getProductCategoryDTO());
			log.info("<<====  CODE::" + code);
			log.info("<<===== NAME::: " + name);
			log.info("<<====== lname:: " + lname);
			productGroupDTOSave.setActiveStatus(activeStatus);
			productGroupDTOSave.setCode(code.trim());
			productGroupDTOSave.setName(name.trim());
			productGroupDTOSave.setLname(lname.trim());

			/*
			 * HttpHeaders headers = new HttpHeaders(); headers.set("Track_Id",
			 * ThreadLocalConfig.getId());
			 * headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			 * HttpEntity<ProductGroupMasterDTO> requestEntity = new
			 * HttpEntity<ProductGroupMasterDTO>(productGroupDTO, loginBean.headers);
			 * HttpEntity<BaseDTO> response = restTemplate.exchange(Url + "/group/create",
			 * HttpMethod.POST, requestEntity, BaseDTO.class);
			 */
			BaseDTO baseDTO = null;
			if (action.equals("ADD")) {
				log.info("<<=====  call for add  ===>>");
				baseDTO = httpService.post(Url + "/group/create", productGroupDTOSave);
			}
			if (action.equals("EDIT")) {
				log.info("<<==== call for edit ====>>");
				baseDTO = httpService.post(Url + "/group/update", productGroupDTOSave);
			}
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Product Category saved successfully");
					if (action.equals("ADD")) {
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_GROUP_SAVE_SUCCESS).getErrorCode());
					}
					if (action.equals("EDIT")) {
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_GROUP_UPDATE_SUCCESS).getErrorCode());
					}
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
					// return PROD_CAT_CREATE_PAGE;
				}
			}

		} catch (Exception e) {
			log.error("<<=======  error occur ::  " + e);
		}

		productGroupDto = new ProductGroupMasterDTO();
		name = null;
		code = null;
		lname = null;
		log.info("<<====  group add end ===========>>");
		getCategoryGroupMasterList();
//		getAllCategory();
		// productCategoryDTO=new ProductCategoryDTO();
		addButtonFlag = true;
		return "/pages/masters/listProductGroup.xhtml?faces-redirect=true";
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts DepartmentBean.onRowSelect ========>" + event);
		productGroupDto = ((ProductGroupMasterDTO) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends DepartmentBean.onRowSelect ========>");
	}

	public void lazyLoadProductGroupList() {
		log.info("====== lazyLoadProductGroupList =======");
		productGrouplazyLoadList = new LazyDataModel<ProductGroupMasterDTO>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProductGroupMasterDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				log.info("====== load() Called =======");
				filterMap = filters;
				sortingOrder = sortOrder;
				sortingField = sortField;
				List<ProductGroupMasterDTO> data = new ArrayList<ProductGroupMasterDTO>();
				try {

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if(baseDTO != null)
					{
						log.info("baseDTO ::" + baseDTO);
						ObjectMapper objectMapper = new ObjectMapper();
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						log.info("=== jsonResponse ====" + jsonResponse);
						data = objectMapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMasterDTO>>() {
						});
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}
					
					else
					{
						this.setRowCount(0);
						resultSize = 0;
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(ProductGroupMasterDTO response) {
				log.info("getRowKey is called.");
				return response != null ? response.getId() : null;
			}

			@Override
			public ProductGroupMasterDTO getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<ProductGroupMasterDTO> responseList = (List<ProductGroupMasterDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (ProductGroupMasterDTO response : responseList) {
					if (response.getId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("ProductGroupMasterBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		log.info("filters==> "+filters);
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		ProductGroupMasterDTO request = getRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			baseDTO = httpService.post(Url + "/group/search", request);
			return baseDTO;

		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("ProductCategoryBean.getSearchData Method Completed");
		return baseDTO;
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	private ProductGroupMasterDTO getRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		ProductGroupMasterDTO request = new ProductGroupMasterDTO();
		ProductCategoryDTO dto = new ProductCategoryDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("productCatCode")) {
				dto.setProductCatCode(value);
				request.setProductCategoryDTO(dto);
			}

			if (entry.getKey().equals("code")) {
				log.info("GroupCode : " + value);
				request.setCode(value);
			}
			if (entry.getKey().equals("lname")) {
				log.info("regionalName : " + value);
				request.setLname(value);
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
			 if (entry.getKey().equals("activeStatus")) {
			 log.info("active Status : " + value);
			 request.setActiveStatus(Boolean.parseBoolean(value));
			 }

		}

		return request;
	}

	public String showViewPage() {
		action = "VIEW";
		pageHead = "View";
		if (productGroupDto == null || productGroupDto.getId() == null || productGroupDto.getId() == 0) {
			log.info("<<=====   please slect a group =====>>");
			errorMap.notify(ErrorDescription.SELECT_ANY_PRODUCT_GROUP.getCode());
			return null;
		} else {
			try {
			String url = Url + "/group/get/"+productGroupDto.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productGroupDto = mapper.readValue(jsonResponse, ProductGroupMasterDTO.class);
			code = productGroupDto.getCode();
			name = productGroupDto.getName();
			lname = productGroupDto.getLname();
			productGroupDto.setCreatedBy(productGroupDto.getCreatedBy());
			productGroupDto.setCreatedDate(productGroupDto.getCreatedDate());
			productGroupDto.setModifiedBy(productGroupDto.getModifiedBy());
			
			productCategoryDTO.setId(productGroupDto.getProductCategoryDTO().getId());
			productCategoryDTO.setProductCatCode(productGroupDto.getProductCategoryDTO().getProductCatCode());
			productCategoryDTO.setProductCatName(productGroupDto.getProductCategoryDTO().getProductCatName());
			}catch (Exception e) {
				log.error("<<==== ERROR::: "+e);
			}
			return "/pages/master/viewProductGroup.xhtml?faces-redirect=true";
		}
	}
	
	public void showDeletePage() {
		action = "DELETE";
		pageHead = "Delete";
		if (productGroupDto == null || productGroupDto.getId() == null || productGroupDto.getId() == 0) {
			log.info("<<=====   please slect a group =====>>");
			errorMap.notify(ErrorDescription.SELECT_ANY_PRODUCT_GROUP.getCode());
		} else {

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmDepartmentDelete').show();");
		}
	}

	public String showEditPage() {
		action = "EDIT";
		pageHead = "Edit";
		if (productGroupDto == null || productGroupDto.getId() == null || productGroupDto.getId() == 0) {
			log.info("<<=====   please slect a group =====>>");
			errorMap.notify(ErrorDescription.SELECT_ANY_PRODUCT_GROUP.getCode());
			return null;
		} else {
			try {
			String url = Url + "/group/get/"+productGroupDto.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productGroupDto = mapper.readValue(jsonResponse, ProductGroupMasterDTO.class);
			activeStatus = true;
			getCategoryGroupMasterList();
			selectedProductCategoryGroup = productGroupDto.getProductCategory().getProductCategoryGroup();
			getAllCategory();
			code = productGroupDto.getCode();
			name = productGroupDto.getName();
			lname = productGroupDto.getLname();
			productCategoryDTO.setId(productGroupDto.getProductCategoryDTO().getId());
			productCategoryDTO.setProductCatCode(productGroupDto.getProductCategoryDTO().getProductCatCode());
			productCategoryDTO.setProductCatName(productGroupDto.getProductCategoryDTO().getProductCatName());
			} catch (Exception e) {
				log.error("<<==== ERROR::: "+e);
			}
			return "/pages/masters/createProductGroup.xhtml?faces-redirect=true";
		}
	}

	/**
	 * @this method is used to search group
	 *//*
		 * public String search() { // action = "ADD";
		 * 
		 * log.info("---productGroupMaster search -----# Start" + productGroupDto); try
		 * {
		 * 
		 * if (buttonName.equals("Search")) { log.info("Button Name :" +
		 * buttonName.toString()); if (productGroupDto.getProductCategoryDTO() == null
		 * && Util.isEmpty(code) && Util.isEmpty(name) && Util.isEmpty(lname)) {
		 * 
		 * log.info("Select one fields");
		 * Util.addWarn(ErrorCodeDescription.getDescription(
		 * ErrorCodeDescription.ENTER_ATLEAST_ONE_DETAIL.getErrorCode(),
		 * languageBean.getLocaleCode()));
		 * FacesContext.getCurrentInstance().getExternalContext().getFlash().
		 * setKeepMessages(true); // productGroupDto=null; code = null; name = null;
		 * lname = null; categoryCode = null; categoryName = null; // getAllGroup();
		 * return null;
		 * 
		 * }
		 * 
		 * restTemplate = new RestTemplate(); wrapperDto = new WrapperDTO(); //
		 * productGroupDto=new ProductGroupMasterDTO(); productGroupList = new
		 * ArrayList<ProductGroupMasterDTO>(); //
		 * productGroupDto.setProductCategoryDTO(productCategoryDTO);
		 * productGroupDto.setCode(code); productGroupDto.setName(name);
		 * productGroupDto.setLname(lname); log.info("code  :" +
		 * productGroupDto.getCode()); log.info("Name  :" + productGroupDto.getName());
		 * log.info("Local Name  :" + productGroupDto.getLname());
		 * 
		 * HttpEntity<ProductGroupMasterDTO> requestEntity = new
		 * HttpEntity<ProductGroupMasterDTO>(productGroupDto, loginBean.headers);
		 * HttpEntity<WrapperDTO> response = restTemplate.exchange(Url +
		 * "/group/search", HttpMethod.POST, requestEntity, WrapperDTO.class);
		 * wrapperDto = response.getBody(); log.info("wrapperDto " +
		 * response.getBody()); numberOfRecords = 0; if (wrapperDto != null) { if
		 * (wrapperDto.getProductGroupDTOCollections() != null) { productGroupList =
		 * (List<ProductGroupMasterDTO>) wrapperDto.getProductGroupDTOCollections();
		 * log.info( "--Total Number of records :" + productGroupList.size()); } else {
		 * productGroupList = new ArrayList<ProductGroupMasterDTO>(); String message =
		 * ErrorCodeDescription.getDescription(wrapperDto.getStatusCode());
		 * log.info("status code " + wrapperDto.getStatusCode() + "Error Message " +
		 * message); log.info(" data is not availabe--");
		 * 
		 * Util.addError(ErrorCodeDescription.getDescription(
		 * wrapperDto.getStatusCode(), languageBean.getLocaleCode()));
		 * FacesContext.getCurrentInstance().getExternalContext(
		 * ).getFlash().setKeepMessages(true); return null;
		 * 
		 * 
		 * } }
		 * 
		 * log.info("--Total Number of records :" + productGroupList.size()); } } catch
		 * (Exception e) { e.printStackTrace();
		 * 
		 * }
		 * 
		 * 
		 * code = null; name = null; lname = null; categoryCode = null; categoryName =
		 * null;
		 * 
		 * 
		 * return "/pages/master/productGroupMaster.xhtml?faces-redirect=true";
		 * 
		 * }
		 */

	public String updateData() {
		log.info("productGroupMasterBean updateData--------# START");
		productGroupDto.setCode(code);
		productGroupDto.setName(name.trim());
		productGroupDto.setLname(lname.trim());
		productGroupDto.setActiveStatus(activeStatus);
		//
		// log.info("-------updateFormData code-------" +
		// productGroupDto.getCode());
		// log.info("-------updateFormData name-------" +
		// productGroupDto.getName());
		// log.info("-------updateFormData lname-------" +
		// productGroupDto.getLname());

		UserDTO userDto = new UserDTO();
		userDto.setId(loginBean.getUserDetailSession().getId());
		productGroupDto.setModifiedBy(userDto);
		productGroupDto.setModifiedDate(new Date());

		try {

			HttpHeaders headers = new HttpHeaders();
			headers.set("Track_Id", ThreadLocalConfig.getId());
			String url = Url + "/group/update";
			HttpEntity<ProductGroupMasterDTO> requestEntity = new HttpEntity<ProductGroupMasterDTO>(productGroupDto,
					loginBean.headers);
			HttpEntity<BaseDTO> productResponse = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					BaseDTO.class);
			BaseDTO baseDto = productResponse.getBody();
			log.info("Status code is:" + baseDto.getStatusCode());
			if (baseDto != null) {

				if (baseDto.getStatusCode() == 0) {

					log.info("Status code is:" + baseDto.getStatusCode());

					Util.addInfo(InfoCodeDescription.getDescription(
							InfoCodeDescription.INFO_PRODUCT_GROUP_UPDATED.getInfoCode(),
							languageBean.getLocaleCode()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					action = "ADD";
				} else {
					String message = ErrorCodeDescription.getDescription(baseDto.getStatusCode());
					log.error("Status code:" + baseDto.getStatusCode() + " Error Message: " + message);
					// Util.addInfo(message);
					Util.addError(
							ErrorCodeDescription.getDescription(baseDto.getStatusCode(), languageBean.getLocaleCode()));
					log.error("Status code:" + baseDto.getStatusCode() + " Error Message: " + message);
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					return null;
					/*
					 * FacesContext context = FacesContext.getCurrentInstance();
					 * context.getExternalContext().getFlash().setKeepMessages( true);
					 * context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
					 * message, null)); FacesContext.getCurrentInstance().getExternalContext()
					 * .getFlash().setKeepMessages(true);
					 */
				}
			}
			log.info("BaseDTO---" + productResponse.getBody());

			// getAllGroup();
		} catch (Exception e) {
			e.printStackTrace();
		}

		productGroupDto = new ProductGroupMasterDTO();
		name = null;
		code = null;
		lname = null;

		log.info("ProductGroupMasterBean updateData--------# END");
		return "/pages/master/productGroupMaster.xhtml?faces-redirect=true";

	}

	public String updateFormData(ProductGroupMasterDTO productGroupDTO) {

		action = "EDIT";

		productGroupDto.setId(productGroupDTO.getId());

		code = productGroupDTO.getCode().trim();
		name = productGroupDTO.getName().trim();
		lname = productGroupDTO.getLname().trim();
		productGroupDto.setCreatedBy(productGroupDTO.getCreatedBy());
		productGroupDto.setCreatedDate(productGroupDTO.getCreatedDate());
		productGroupDto.setProductCategoryDTO(productGroupDTO.getProductCategoryDTO());
		log.info("ProductGroupMasterBean updateFormData--------# END");
		return "/pages/master/productGroupMaster.xhtml?faces-redirect=true";
	}

	public void clear() {
		log.info("<---- Clear() Method-----> ");
		code = null;
		name = null;
		lname = null;
		action = "ADD";
		productGroupDto = new ProductGroupMasterDTO();

		// getAllGroup();

	}

	public String deleteGroup() {
		log.info("<<======  product goup delete ====>>" + productGroupDto);
		BaseDTO baseDTO = new BaseDTO();
		if (!action.equalsIgnoreCase("DELETE") || productGroupDto == null || productGroupDto.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ANY_PRODUCT_GROUP.getErrorCode());
			log.info("<<====  PLEASE SELETE ONE GROUP ====>>");
			return null;
		}
		try {
			String url = Url + "/group/delete/" + productGroupDto.getId();
			baseDTO = httpService.delete(url);
			log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
			if (baseDTO != null && baseDTO.getStatusCode()==0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_GROUP_DELETE_SUCCESS).getErrorCode());
				log.info("Product group Master has been deleted");
			}
			 else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
			 }

		} catch (Exception e) {
			log.error("<<=====  Load account category error====>>" + e);
		}
		addButtonFlag = true;
		return showProductGroup();
	}
	
	public void getCategoryGroupMasterList() {
		log.info("===getCategoryGroupMasterList===   Start");

		try {
			productCategoryGroupList.clear();
			String url = Url + "/categoryGroup/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryGroupList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryGroup>>() {
			});
			productCategoryListDto.clear();;
			log.info("<<==== getCategoryGroupMasterList List Sise::: " + productCategoryGroupList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getCategoryGroupMasterList" + e);
		}
	}

	public void getAllCategory() {
		log.info("<<==========ProductGroupMasterBean  getAllCategory=====#Start ::  "+selectedProductCategoryGroup.getId());
		BaseDTO baseDTO = new BaseDTO();
		productCategoryListDto.clear();
		try {

			String url = Url + "/category/getCategoryByCategoryGroup/"+selectedProductCategoryGroup.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryListDto = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryDTO>>() {
			});
			/*
			 * HttpHeaders headers = new HttpHeaders(); headers.set("Track_Id",
			 * ThreadLocalConfig.getId()); HttpEntity<String> entity = new
			 * HttpEntity<String>("parameters", loginBean.headers); HttpEntity<WrapperDTO>
			 * response = restTemplate.exchange(Url + "/category/getAll", HttpMethod.GET,
			 * entity, WrapperDTO.class); log.info("<----Url--->" + Url); wrapperDto =
			 * response.getBody(); log.info("Wrapper Dto :" + response.getBody());
			 * productCategoryListDto = (List<ProductCategoryDTO>)
			 * wrapperDto.getProductCategoryDTOCollections();
			 */
			log.info("<<=======  productCategoryListDto " + productCategoryListDto);
		} catch (Exception e) {
			log.error("Error Occured While loading Product Category ");
		}
	}
}