package in.gov.cooptex.personnel.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GroupDTO;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
public class GroupBean {

	private final String INPUT_FORM_ADD_URL = "/pages/masters/createGroup.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_LIST_URL = "/pages/masters/listGroup.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	private String serverURL;

	@Getter
	@Setter
	private GroupDTO groupDto, selectedGroupDto;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	int groupListSize;

	@Getter
	@Setter
	private List<GroupDTO> groupList = new ArrayList<GroupDTO>();

	@Getter
	@Setter
	String action;

	@PostConstruct
	public void init() {
		loadValues();
		showList();
	}

	public String showList() {
		selectedGroupDto = new GroupDTO();
		log.info("<------------Redirecting to List page--------------->");
		getAllGroup();
		return "/pages/masters/listGroup.xhtml?faces-redirect=true";
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());

		}
	}

	public String getAllGroup() {
		log.info("Get all group is Called...");
		try {
			groupList= new ArrayList<GroupDTO>();
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/group/getallgroups";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				groupList = mapper.readValue(jsonResponse, new TypeReference<List<GroupDTO>>() {});

				if (groupList != null)
					groupListSize = groupList.size();
				else
					groupListSize = 0;
				
				
				
			}
		} catch (IOException e) {
			log.error("IOException at loadValues() >>>> " + e);
		}
		return null;
	}

	public String addGroup() {
		action = "ADD";
		groupDto = new GroupDTO();
		groupDto.setStatus(true);
		getAllGroup();
		return INPUT_FORM_ADD_URL;
	}

	public String editGroup() {
		log.info("<<<<< -------Start Edit Group called ------->>>>>> ");
		action = "EDIT";

		if (selectedGroupDto == null) {
			Util.addWarn("Please select one group");
			return null;
		}

		groupDto = selectedGroupDto;
		if (groupDto.getType().equals("Active"))
			groupDto.setStatus(true);
		else
			groupDto.setStatus(false);
		log.info("<<<<< -------End Edit Group called ------->>>>>> ");
		return INPUT_FORM_ADD_URL;
	}

	public String cancel() {
		groupDto = new GroupDTO();
		selectedGroupDto = new GroupDTO();
		return INPUT_FORM_LIST_URL;
	}

	public String saveGroup() {
		log.info("<-----------Save Group-------->");
		try {
			if (groupDto == null) {
				Util.addWarn("Please Enter group name");
				return null;
			}

			if (groupDto.getName() == null || groupDto.getName().isEmpty()) {
				Util.addWarn("Please Enter group name");
				return null;
			}
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/group/add";
			baseDTO = httpService.post(url, groupDto);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1270) {
					Util.addWarn("Duplicate Group name");
					return null;
				}
				if (baseDTO.getStatusCode() == 1272) {
					log.info(url + "<--- baseDTO.getStatusCode() == 0 --->");
					Util.addInfo("Group added successfully");
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

				}
				if (baseDTO.getStatusCode() == 1275) {
					log.info(url + "<--- baseDTO.getStatusCode() == 0 --->");
					Util.addInfo("Group updated successfully");
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);

				}
			}
			
			selectedGroupDto = new GroupDTO();
			getAllGroup();
		} catch(Exception e)
		{
			log.info("Exception occured while adding group"+e);
		}
		return INPUT_FORM_LIST_URL;
	}



	public String confirmDelete() {
		log.info("==>> GroupBean inside confirmDelete() <<== Start");
		try {
			if (selectedGroupDto == null) {
				Util.addWarn("Please select one group");
				return null;
			}

			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/group/delete";
			baseDTO = httpService.post(url, selectedGroupDto);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info(url + "<--- baseDTO.getStatusCode() == 0 --->");
					Util.addInfo("Group deleted successfully");
				}
				if (baseDTO.getStatusCode() == 4032) {
					Util.addWarn("Internal server error");
					return null;
				}

			}
			selectedGroupDto = new GroupDTO();
			getAllGroup();
		} catch(Exception e)
		{
			log.error("Exception occured while deleting group ..."+e);
		}
		log.info("==>> GroupBean inside confirmDelete() <<== End");
		return null;

	}

	public String deleteGroup() {
		log.info("==>> GroupBean inside deleteGroup() <<== Start");
		if (selectedGroupDto == null) {
			Util.addWarn("Please select one group");
			return null;
		}
		if (selectedGroupDto == null || selectedGroupDto.getName().isEmpty()) {
			Util.addError(
					ErrorCodeDescription.getDescription(ErrorCodeDescription.ERROR_REGION_REQUIRED.getErrorCode()));
			return null;
		}

		else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmdelete').show();");
		}
		log.info("==>> GroupBean inside deleteGroup() <<== End");
		return null;
	}

	public String clear() {
		groupDto = new GroupDTO();
		selectedGroupDto = new GroupDTO();
//		getAllGroup();
		return INPUT_FORM_LIST_URL;
	}

}
