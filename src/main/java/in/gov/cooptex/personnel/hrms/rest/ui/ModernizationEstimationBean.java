package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.ModernizationEstimation;
import in.gov.cooptex.asset.model.ModernizationEstimationLog;
import in.gov.cooptex.asset.model.ModernizationEstimationNote;
import in.gov.cooptex.asset.model.ModernizationRequest;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.VoucherStatus;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.ModernizationEstimationDTO;
import in.gov.cooptex.dto.pos.ModernizationRequestDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("modernizationEstimationBean")
public class ModernizationEstimationBean extends CommonBean implements Serializable {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/assetManagement/createEstimationOfModernization.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/assetManagement/viewEstimationOfModernization.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/assetManagement/listEstimationOfModernization.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private Boolean approveeditFlag = true;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	int forwardfor;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	boolean forceLogin = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	Boolean submitButtonFlag = true;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	LazyDataModel<ModernizationEstimationDTO> modRequestLazyList;

	@Getter
	@Setter
	List<ModernizationEstimationDTO> modRequestList;

	@Getter
	@Setter
	ModernizationEstimationDTO selectedModRequest = new ModernizationEstimationDTO();

	@Getter
	@Setter
	ModernizationEstimation addModernizationEstimation = new ModernizationEstimation();

	@Getter
	@Setter
	List<ModernizationEstimation> modEstimationList = new ArrayList<ModernizationEstimation>();

	@Getter
	@Setter
	List<ModernizationEstimation> modEstimationListRemoveList = new ArrayList<ModernizationEstimation>();

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	ModernizationRequest modernizationRequest = new ModernizationRequest();

	@Getter
	@Setter
	double tot_amount;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<UomMaster> uomMasterList = new ArrayList<>();

	@Getter
	@Setter
	ModernizationEstimationDTO modEstimationDTO = new ModernizationEstimationDTO();

	@Getter
	@Setter
	ModernizationEstimationLog modernizationEstimationLog = new ModernizationEstimationLog();

	@Getter
	@Setter
	ModernizationEstimationNote modernizationEstimationNote = new ModernizationEstimationNote();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@PostConstruct

	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("modernization_request_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("Selected circular idddddddId :" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// modEstimationDTO = new ModernizationEstimationDTO();
			// action = "VIEW";
			// modEstimationDTO.setId(Long.parseLong(circularId));
			selectedNotificationId = Long.parseLong(notificationId);
			selectedModRequest.setId(Long.parseLong(circularId));
			showView();
		}
		log.info("ModerizationBean showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public void init() {
		log.info("ModernizationBean Init is executed.....................");
		mapper = new ObjectMapper();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("ESTIMATION_FOR_MODERNIZATION");
		// forwardToUsersList = commonDataService.loadForwardToUsers();
		uomMasterList = commonDataService.loadUOMMaster();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadLazyModernizationRequestList() {
		log.info("<==== Starts modernizationEstimationBean.loadLazyModernizationRequestList =====>");
		modRequestLazyList = new LazyDataModel<ModernizationEstimationDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ModernizationEstimationDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/modernizationestimation/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						modRequestList = mapper.readValue(jsonResponse,
								new TypeReference<List<ModernizationEstimationDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return modRequestList;
			}

			@Override
			public Object getRowKey(ModernizationEstimationDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ModernizationEstimationDTO getRowData(String rowKey) {
				for (ModernizationEstimationDTO modernizationRequestDTO : modRequestList) {
					if (modernizationRequestDTO.getId().equals(Long.valueOf(rowKey))) {
						selectedModRequest = modernizationRequestDTO;
						return modernizationRequestDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends modernizationEstimationBean.loadLazyModernizationRequestList =====>");
	}

	public void getModRequestByID() {
		log.info("<====  modernizationEstimationBean.getModRequestByID =====>" + selectedModRequest.getId());
		String URL = SERVER_URL + "/modernizationrequest/getbyid/" + selectedModRequest.getId() + "/"
				+ (selectedNotificationId != null ? selectedNotificationId : 0);
		;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				modernizationRequest = mapper.readValue(jsonValue, ModernizationRequest.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public String ShowAddPage() {
		log.info("<==== ShowAddPage Started for Modernization Estimation=====>");
		modEstimationList = new ArrayList<ModernizationEstimation>();

		if (selectedModRequest == null) {

			log.info("<==== Mod Request is NUll =====>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		getModernizationEstimationByModernReqId();
		if (modEstimationList.size() > 0) {
			errorMap.notify(ErrorDescription.MOD_ESTIMATION_DATA_ITEM_DESCRIBED.getCode());
			return null;
		}
		modernizationEstimationNote = new ModernizationEstimationNote();
		addModernizationEstimation = new ModernizationEstimation();
		getModRequestByID();
		log.info("<==== ShowAddPage End for Modernization Estimation=====>");
		return ADD_PAGE;

	}

	public void addItems() {
		if (addModernizationEstimation != null) {
			log.info("<==== Mod Estimation Item Details=====>" + addModernizationEstimation);
			if (addModernizationEstimation.getUomMaster() == null) {
				AppUtil.addWarn("Unit is required");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			if (addModernizationEstimation.getGst_percent() != null && addModernizationEstimation.getGst_percent() >= 0
					&& addModernizationEstimation.getAmount() != null
					&& addModernizationEstimation.getAmount() > addModernizationEstimation.getRate()) {

				// Double gstAmount = addModernizationEstimation.getAmount() / 100 *
				// addModernizationEstimation.getGst_percent();
				Double gstAmount = addModernizationEstimation.getRate() * addModernizationEstimation.getQuantity()
						* (addModernizationEstimation.getGst_percent() / 100);
				addModernizationEstimation.setGst_amount(gstAmount);
				// Double totalAmt = gstAmount + addModernizationEstimation.getAmount();

				Double totalAmountwithoutgst = addModernizationEstimation.getRate()
						* addModernizationEstimation.getQuantity();
				addModernizationEstimation.setAmount(totalAmountwithoutgst);

				// Double qt=addModernizationEstimation.getQuantity();
				// Double rate=addModernizationEstimation.getRate();
				// Double gst=addModernizationEstimation.getGst_percent();
				// Double gstamount = qt * rate * (gst/100);
				// addModernizationEstimation.setAmount(gstamount+ (rate * qt));

			}
			addModernizationEstimation.setModernizationRequest(modernizationRequest);
			modEstimationList.add(addModernizationEstimation);
			addModernizationEstimation = new ModernizationEstimation();
		} else {
			log.info("<==== Mod Estimation Item Details NUll=====>" + addModernizationEstimation);
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		}
	}

	public String submit() {
		log.info("...... Mod Estimation  submit begin .... " + action + " ");
		try {

			if (modEstimationList.size() == 0) {

				Util.addWarn("Estimation of Modernization Should Not Be Empty");
				return null;
			}
			if (modEstimationList.size() != 0 && modEstimationList.size() > 0) {
//				if (modernizationEstimationNote.getNote() == null   || modernizationEstimationNote.getNote().length() ==0) {
//
//					errorMap.notify(ErrorDescription.MOD_ESTIMATION_NOTE_EMPTY.getCode());
//					return null;
//				}

				if (modernizationEstimationNote.getNote() == null
						|| modernizationEstimationNote.getNote().length() == 0) {
					log.info("Note Not Valid");
					errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
					return null;
				}

				modEstimationDTO.setModernizationEstimationNote(modernizationEstimationNote);
				modEstimationDTO.setModernizationEstimationList(modEstimationList);
				modEstimationDTO.setModEstimationListRemoveList(modEstimationListRemoveList);
				modEstimationDTO.setModernizationRequest(modernizationRequest);
				BaseDTO baseDTO = null;
				baseDTO = httpService.post(
						SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/create",
						modEstimationDTO);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("Mod Estimation  saved successfully");
						if (action.equalsIgnoreCase("ADD")) {
							errorMap.notify(ErrorDescription.MOD_ESTIMATION_CREATED_SUCCESSFULLY.getCode());
						} else {
							errorMap.notify(ErrorDescription.MOD_ESTIMATION_UPDATED_SUCCESSFULLY.getCode());
						}
						loadLazyModernizationRequestList();

					} else {
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
								+ baseDTO.getErrorDescription());
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			} else {
				errorMap.notify(ErrorDescription.MOD_ESTIMATION_DATA_ITEM_NOT_DESCRIBED.getCode());

				return null;
			}
		} catch (Exception e) {
			log.error("Error while creating TenderType" + e);
			e.printStackTrace();
		}
		log.info("...... Mod Estimation submit ended ....");
		return LIST_PAGE;
	}

	public void deleteItemDescription(ModernizationEstimation obj) {

		modEstimationListRemoveList.add(obj);
		modEstimationList.remove(obj);

		tot_amount = 0;
		for (ModernizationEstimation value : modEstimationList) {
			tot_amount = tot_amount + value.getAmount();
		}

	}

	public void getModernizationEstimationByModernReqId() {
		log.info(
				"<===Starts ModernizationReportBean:==>getModernizationEstimationByModernReqId Method Start ========>");
		BaseDTO baseDTO = new BaseDTO();
		modEstimationList = new ArrayList<>();
		try {
			log.info("<===Starts ModernizationSubmissionReportBean:==> ========>selectedModRequest Id="
					+ selectedModRequest.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/modernestimation/"
					+ selectedModRequest.getId() + "/" + (selectedNotificationId != null ? selectedNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
//				modEstimationList = mapper.readValue(jsonResponse, new TypeReference<List<ModernizationEstimation>>() {
//				});
				List<ModernizationEstimationDTO> modernizationEstimationDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<ModernizationEstimationDTO>>() {
						});
				if (!CollectionUtils.isEmpty(modernizationEstimationDTOList)) {
					ModernizationEstimationDTO modernizationEstimationDTO = modernizationEstimationDTOList.get(0);
					if (modernizationEstimationDTO != null) {
						modEstimationList = modernizationEstimationDTO.getModernizationEstimationList();
					}
				}
			}
			log.info("<===ModernizationReportBean:==>modernizationEstimationList Size:=" + modEstimationList.size());
		} catch (Exception ex) {
			log.info("ModernizationReportBean:=>Error In getModernizationEstimationByModernReqId Method", ex);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String showDelete() {
		try {
			action = "Delete";
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				RequestContext.getCurrentInstance().update("growls");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEstimationDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		// disableAddButton = false;
		return null;
	}

	public String deleteEstimationConfirm() {

		try {
			log.info("Delete For Mod Estimation started the id is [" + selectedModRequest.getId() + "]");
			BaseDTO baseDTO = new BaseDTO();
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/delete/"
					+ selectedModRequest.getId();
			baseDTO = httpService.get(URL);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Country deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.info("Error occured in Rest api ... ");
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String showList() {
		selectedModRequest = new ModernizationEstimationDTO();
		loadLazyModernizationRequestList();
		addButtonFlag = false;
		return LIST_PAGE;
	}

	public String showView() {
		try {
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			// selectedModRequest.getStatus()

			log.info("appprovalfalg" + approvalFlag);
			if (loginBean.getUserMaster().getUsername().equals("hosuper")) {
				hideflag = true;

			}

//			       if(selectedModRequest.getStatus().equalsIgnoreCase("SUBMITTED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//					
//					submitButtonFlag=false;
//					}
//				   if(selectedModRequest.getStatus().equalsIgnoreCase("SUBMITTED") && !loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//					submitButtonFlag=true;
//					hideflag=false;
//						
//					
//					}
//				   if(selectedModRequest.getStatus().equalsIgnoreCase("FINAL-APPROVED")) {
//					submitButtonFlag=false;
//					}
//				   if(selectedModRequest.getStatus().equalsIgnoreCase("APPROVED")) {
//						submitButtonFlag=false;
//					}
//				   if(selectedModRequest.getStatus().equalsIgnoreCase("REJECTED")) {
//						submitButtonFlag=false;
//					}
			/*
			 * else { buttonFlag=false;
			 * 
			 * }
			 */

			modEstimationList = new ArrayList<ModernizationEstimation>();
			BaseDTO baseDTO = new BaseDTO();
			try {
				log.info("<===Starts ModernizationSubmissionReportBean:==> ========>selectedModRequest Id="
						+ selectedModRequest.getId());
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/modernizationestimation/modernestimation/" + selectedModRequest.getId() + "/"
						+ (selectedNotificationId != null ? selectedNotificationId : 0);
				baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<ModernizationEstimationDTO> modernizationEstimationDTOList = mapper.readValue(jsonResponse,
							new TypeReference<List<ModernizationEstimationDTO>>() {
							});

					if (selectedNotificationId != null) {
						systemNotificationBean.loadTotalMessages();
					}
					if (!CollectionUtils.isEmpty(modernizationEstimationDTOList)) {
						ModernizationEstimationDTO modernizationEstimationDTO = modernizationEstimationDTOList.get(0);
						if (modernizationEstimationDTO != null) {
							selectedModRequest.setModernizationEstimationNote(
									modernizationEstimationDTO.getModernizationEstimationNote());
							modEstimationList = modernizationEstimationDTO.getModernizationEstimationList();
							selectedModRequest.setStatus(modernizationEstimationDTO.getStatus());
							selectedModRequest.setHeadRegionOffice(modernizationEstimationDTO.getHeadRegionOffice());
							selectedModRequest.setEntityType(modernizationEstimationDTO.getEntityType());
							selectedModRequest.setEntity(modernizationEstimationDTO.getEntity());
							selectedModRequest.setNameOfBuinding(modernizationEstimationDTO.getNameOfBuinding());
						}
					}
//					modEstimationList = mapper.readValue(jsonResponse,
//							new TypeReference<List<ModernizationEstimation>>() {
//							});

					String jsonResponse1 = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
					});
				}

				if (selectedModRequest.getModernizationEstimationNote().getFinalApproval() == false) {

					if (selectedModRequest.getModernizationEstimationNote().getForwardTo().getId()
							.equals(loginBean.getUserDetailSession().getId())) {
						submitButtonFlag = true;
						hideflag = true;
						finalapprovalFlag = false;

					} else {
						submitButtonFlag = false;
						hideflag = false;
					}
				} else {

					if (selectedModRequest.getModernizationEstimationNote().getForwardTo().getId()
							.equals(loginBean.getUserDetailSession().getId())) {
						submitButtonFlag = true;
						hideflag = false;
						finalapprovalFlag = true;

					}
					if (!selectedModRequest.getModernizationEstimationNote().getForwardTo().getId()
							.equals(loginBean.getUserDetailSession().getId())) {
						submitButtonFlag = false;
						hideflag = false;
						finalapprovalFlag = true;

					}
				}

				if (selectedModRequest.getStatus().equalsIgnoreCase("FINAL-APPROVED")) {
					submitButtonFlag = false;
					hideflag = false;
				}

				// if(selectedModRequest.getModernizationEstimationNote().getForwardTo().getId()
				// .equals(loginBean.getUserDetailSession().getId())){

				// selectedModRequest.getModernizationEstimationNote().setForwardTo(new
				// UserMaster());

				// }

				// else {
				// submitButtonFlag=false;
				// hideflag=false;
				// }

				// log.info(" EMPNAME : " +
				// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getEmpName());
				// log.info(" USERNAME : " +
				// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getUsername());
				// log.info(" DESIGNATION : " +
				// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getDesignation());
			} catch (Exception e) {
				log.error("Exception while getting ModEstimation ....", e);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

			if (modEstimationList.size() == 0 || modEstimationList == null) {
				errorMap.notify(ErrorDescription.MOD_ESTIMATION_DATA_ITEM_NOT_DESCRIBED.getCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			modernizationEstimationNote = new ModernizationEstimationNote();
			getViewModEstimation();
			getModRequestByID();

			if (selectedModRequest != null) {
				setPreviousApproval(selectedModRequest.getModernizationEstimationNote().getFinalApproval());
				if ("VIEW".equalsIgnoreCase(action)) {
					if (selectedModRequest.getModernizationEstimationNote().getForwardTo() != null) {
						if (loginBean.getUserMaster().getId()
								.equals(selectedModRequest.getModernizationEstimationNote().getForwardTo().getId())) {
							approveeditFlag = false;
							if (AmountMovementType.REJECTED.equals(selectedModRequest.getStatus())) {
								approvalFlag = false;
							} else {
								approvalFlag = true;
							}
						} else {
							approvalFlag = false;
						}
					}
					setFinalApproveFlag(selectedModRequest.getModernizationEstimationNote().getFinalApproval());
					if (VoucherStatus.FINALAPPROVED.equals(selectedModRequest.getStatus())) {
						buttonFlag = true;
					}
					// selectedModRequest.getModEstimationlog().setRemarks(null);

					return VIEW_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("Exception while Viewing ModEstimation ....", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public void getViewModEstimation() {
		log.info("Inside getView() ");
		BaseDTO baseDTO = null;
		modEstimationDTO = new ModernizationEstimationDTO();
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/getdetailsbyid/"
					+ selectedModRequest.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				modEstimationDTO = mapper.readValue(jsonResponse, new TypeReference<ModernizationEstimationDTO>() {
				});

				if (modEstimationDTO != null) {
					log.info("Mod Estimation Value" + modEstimationDTO);
					modernizationRequest = modEstimationDTO.getModernizationRequest();
					modEstimationList = modEstimationDTO.getModernizationEstimationList();
					modernizationEstimationNote = modEstimationDTO.getModernizationEstimationNote();
					selectedModRequest.setModernizationEstimationLog(modEstimationDTO.getModernizationEstimationLog());
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While getView() :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String showEdit() {
		getModRequestByID();
		loadeditModEstimation();
		return ADD_PAGE;
	}

	public void loadeditModEstimation() {
		log.info("Inside loadeditModEstimation() ");
		BaseDTO baseDTO = null;
		modEstimationDTO = new ModernizationEstimationDTO();

		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/getdetailsbyid/"
					+ selectedModRequest.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				modEstimationDTO = mapper.readValue(jsonResponse, new TypeReference<ModernizationEstimationDTO>() {
				});
				if (modEstimationDTO != null) {
					modernizationRequest = modEstimationDTO.getModernizationRequest();
					modEstimationList = modEstimationDTO.getModernizationEstimationList();
					modernizationEstimationNote = modEstimationDTO.getModernizationEstimationNote();
					selectedModRequest.setModernizationEstimationLog(modEstimationDTO.getModernizationEstimationLog());
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadeditModEstimation() :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String approveModEstimation() {
		BaseDTO baseDTO = null;
		try {
			if (selectedModRequest.getId() != null) {

				log.info(" approveModEstimation id----------" + selectedModRequest.getId());
				baseDTO = new BaseDTO();
				if (previousApproval == false
						&& selectedModRequest.getModernizationEstimationNote().getFinalApproval() == true) {
					selectedModRequest.getModernizationEstimationLog().setStage(ApprovalStage.APPROVED);
				} else if (previousApproval == true
						&& selectedModRequest.getModernizationEstimationNote().getFinalApproval() == true) {
					selectedModRequest.getModernizationEstimationLog().setStage(ApprovalStage.FINAL_APPROVED);
				} else {
					selectedModRequest.getModernizationEstimationLog().setStage(ApprovalStage.APPROVED);
				}
				log.info("approve remarks=========>" + selectedModRequest.getModernizationEstimationLog().getRemarks());

				selectedModRequest.setModernizationRequest(modernizationRequest);

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/modernizationestimation/approvemodestimation";

				baseDTO = httpService.post(url, selectedModRequest);
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.MOD_ESTIMATION_APPROVED.getErrorCode());
					log.info("Successfully Approved-----------------");
					RequestContext.getCurrentInstance().update("growls");
					return showList();
				}
			}
		} catch (Exception e) {
			log.error("approveModEstimation method inside exception-------", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String rejectModEstimation() {
		BaseDTO baseDTO = null;
		try {
			if (selectedModRequest.getId() != null) {
				log.info("rejectModEstimation id----------" + selectedModRequest.getId());
				baseDTO = new BaseDTO();
				if (selectedModRequest.getId() != null) {
					selectedModRequest.getModernizationEstimationLog().setStage(ApprovalStage.REJECTED);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/modernizationestimation/rejectmodestimation";
					baseDTO = httpService.post(url, selectedModRequest);
					if (baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.MOD_ESTIMATION_REJECTED.getErrorCode());
						log.info("Successfully Rejected-----------------");
						RequestContext.getCurrentInstance().update("growls");
						return showList();
					}
				}
			}
		} catch (Exception e) {
			log.error("rejectModEstimation method inside exception-------", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		/// addButtonFlag = true;
		log.info("addbuttonflag" + addButtonFlag);
		viewButtonFlag = false;
		selectedModRequest = (ModernizationEstimationDTO) event.getObject();
		if (selectedModRequest.getId() != null) {
			log.error("Selected Request ID -------", +selectedModRequest.getId());
			selectedModRequest
					.setModernizationEstimationLog(modernizationEstimationLog = new ModernizationEstimationLog());
			selectedModRequest.getModernizationEstimationLog().setStage(selectedModRequest.getStatus());
			log.info("selectedBenefit id-------" + selectedModRequest.getId());
			log.info("status----------" + selectedModRequest.getStatus());

			/*
			 * if (!loginBean.getUserMaster().getUsername().equals("hosuper")) {
			 * addButtonFlag = true; }
			 */

			if (ApprovalStage.REJECTED.equals(selectedModRequest.getStatus())) {
				editButtonFlag = false;
				approvalFlag = false;
			}
			if (ApprovalStage.FINAL_APPROVED.equals(selectedModRequest.getStatus())) {

				approvalFlag = true;
				addButtonFlag = true;
				buttonFlag = true;
			}

			/*
			 * if (ApprovalStage.FINAL_APPROVED.equals(selectedModRequest.getStatus()) &&
			 * !loginBean.getUserMaster().getUsername().equals("hosuper")) {
			 * 
			 * approvalFlag = true; addButtonFlag = true; buttonFlag = true; }
			 */

			/*
			 * if (ApprovalStage.FINAL_APPROVED.equals(selectedModRequest.getStatus()) &&
			 * loginBean.getUserMaster().getUsername().equals("hosuper")) {
			 * 
			 * approvalFlag = true; addButtonFlag = true; buttonFlag = true; }
			 */

			if (ApprovalStage.SUBMITTED.equals(selectedModRequest.getStatus())) {

				addButtonFlag = false;

			}

			/*
			 * if (!loginBean.getUserMaster().getUsername().equals("hosuper")) {
			 * 
			 * addButtonFlag = false; }
			 */

			// else {
			// editButtonFlag = true;
			// addButtonFlag = false;
			// buttonFlag=false;
			// }
		}
	}

	public void updateamount() {

		Double qt = addModernizationEstimation.getQuantity();
		Double rate = addModernizationEstimation.getRate();
		Double gst = addModernizationEstimation.getGst_percent();
		Double gstamount = qt * rate * (gst / 100);
		addModernizationEstimation.setAmount(gstamount + (rate * qt));
		log.info("qtyy" + qt + addModernizationEstimation.getItemDescription());
	}

}
