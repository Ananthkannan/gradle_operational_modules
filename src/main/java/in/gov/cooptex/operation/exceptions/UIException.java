package in.gov.cooptex.operation.exceptions;

public class UIException extends Exception {
	/**
	* 
	*/
	private static final long serialVersionUID = 4865158416964813431L;

	public UIException(String exceptionMsg) {
		super(exceptionMsg);
	}

}
