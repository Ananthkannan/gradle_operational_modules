package in.gov.cooptex.master.rest.ui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.LeaveEligibilityMaster;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("leaveEligibilityMasterBean")
public class LeaveEligibilityMasterBean {

	private final String CREATE_LEAVE_ELIGIBLITY_PAGE = "/pages/masters/leaveEligibility/createLeaveEligibility.xhtml?faces-redirect=true;";
	private final String LEAVE_ELIGIBLITY_LIST_URL = "/pages/masters/leaveEligibility/listLeaveEligibility.xhtml?faces-redirect=true;";
	private final String LEAVE_ELIGIBLITY_VIEW_URL = "/pages/masters/leaveEligibility/viewLeaveEligibility.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	LeaveEligibilityMaster leaveEligibilityMaster, selectedLeaveEligibilityMaster;

	@Getter
	@Setter
	LazyDataModel<LeaveEligibilityMaster> lazyLeaveEligibilityMasterList;

	List<LeaveEligibilityMaster> leaveEligibilityMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();
	
	@Getter
	@Setter
	List<Integer> yearList = new ArrayList<Integer>();

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<LeaveTypeMaster>();

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	public String showLeaveEligibilityMasterListPage() {
		log.info("<==== Starts LeaveEligibilityMasterBean.showLeaveEligibilityMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		leaveEligibilityMaster = new LeaveEligibilityMaster();
		selectedLeaveEligibilityMaster = new LeaveEligibilityMaster();
		loadLazyLeaveEligibilityMasterList();
		log.info("<==== Ends LeaveEligibilityMasterBean.showLeaveEligibilityMasterListPage =====>");
		return LEAVE_ELIGIBLITY_LIST_URL;
	}
	
	public void loadYearList()
	{
		log.info("<==== Starts LeaveEligibilityMasterBean.loadYearList =====>");
		    // Gets the current date and time
	     Integer year = Calendar.getInstance().get(Calendar.YEAR);
	     yearList.add(year);
	     yearList.add(year+1);
		log.info("<==== Starts LeaveEligibilityMasterBean.loadYearList =====>");
	}

	public void loadLazyLeaveEligibilityMasterList() {
		log.info("<===== Starts LeaveEligibilityMasterBean.loadLazyLeaveEligibilityMasterList ======>");
		lazyLeaveEligibilityMasterList = new LazyDataModel<LeaveEligibilityMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<LeaveEligibilityMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/leaveEligibility/searchandload";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						leaveEligibilityMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<LeaveEligibilityMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyLeaveEligibilityMasterList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return leaveEligibilityMasterList;
			}

			@Override
			public Object getRowKey(LeaveEligibilityMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public LeaveEligibilityMaster getRowData(String rowKey) {
				try {
					for (LeaveEligibilityMaster leaveEligibilityMaster : leaveEligibilityMasterList) {
						if (leaveEligibilityMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedLeaveEligibilityMaster = leaveEligibilityMaster;
							return leaveEligibilityMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends LeaveEligibilityMasterBean.loadLazyLeaveEligibilityMasterList ======>");
	}

	public String showLeaveEligiblityListPageAction() {
		log.info("<===== Starts LeaveEligibilityMasterBean.showAccountCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedLeaveEligibilityMaster == null || selectedLeaveEligibilityMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create LeaveEligibilityMaster called..");
				leaveTypeMasterList = commonDataService.getAllActveLeaveTupeMasters();
				departmentList = commonDataService.loadDepartmentList();
				leaveEligibilityMaster = new LeaveEligibilityMaster();
				loadYearList();
				return CREATE_LEAVE_ELIGIBLITY_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit LeaveEligibilityMaster called..");
				leaveTypeMasterList = commonDataService.getAllActveLeaveTupeMasters();
				loadYearList();
				departmentList = commonDataService.loadDepartmentList();
				String url = serverURL + "/leaveEligibility/" + selectedLeaveEligibilityMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					leaveEligibilityMaster = mapper.readValue(jsonResponse,
							new TypeReference<LeaveEligibilityMaster>() {
							});
					return CREATE_LEAVE_ELIGIBLITY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete LeaveEligibilityMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view LeaveEligibilityMaster called..");
				String viewurl = serverURL + "/leaveEligibility/" + selectedLeaveEligibilityMaster.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					leaveEligibilityMaster = mapper.readValue(jsonResponse,
							new TypeReference<LeaveEligibilityMaster>() {
							});
					return LEAVE_ELIGIBLITY_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends LeaveEligibilityMasterBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {
		log.info("<==== Starts LeaveEligibilityMasterBean.saveOrUpdate =======>");
		try {
			if (leaveEligibilityMaster.getDepartmentMaster() == null) {
				errorMap.notify(ErrorDescription.DEPARTMENT_REQUIRED.getCode());
				return null;
			}
			if (leaveEligibilityMaster.getLeaveTypeMaster() == null) {
				errorMap.notify(MastersErrorCode.LEAVE_ELIGIBILITY_LEAVETYPE_REQUIRED);
				return null;
			}
			
			if (leaveEligibilityMaster.getYear() == null) {
				log.info("<==== Starts LeaveEligibilityMasterBean.saveOrUpdate leaveEligibilityMaster.getYear is Empty");
				Util.addWarn("Year Cannot Be Empty");
				return null;
			}

			if (action.equalsIgnoreCase("EDIT")) {
				leaveEligibilityMaster.setModifiedBy(loginBean.getUserDetailSession());
			}
			String url = serverURL + "/leaveEligibility/saveorupdate";
			BaseDTO response = httpService.put(url, leaveEligibilityMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("account category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
				    errorMap.notify(ErrorDescription.getError(MastersErrorCode.LEAVE_ELIGIBILITY_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.LEAVE_ELIGIBILITY_UPDATE_SUCCESS).getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends LeaveEligibilityMasterBean.saveOrUpdate =======>");
		return showLeaveEligibilityMasterListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts LeaveEligibilityMasterBean.onRowSelect ========>" + event);
		selectedLeaveEligibilityMaster = ((LeaveEligibilityMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends LeaveEligibilityMasterBean.onRowSelect ========>");
	}
	
	public String deleteConfirm() {
		log.info("<===== Starts LeaveEligibilityMasterBean.deleteConfirm ===========>");
		try {
			String url=serverURL + "/leaveEligibility/delete/" + selectedLeaveEligibilityMaster.getId();
			BaseDTO response = httpService.delete(url);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.LEAVE_ELIGIBILITY_DELETE_SUCCESS).getCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmLeaveEligiblityDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Leave Eligiblity ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends LeaveEligibilityMasterBean.deleteConfirm ===========>");
		return showLeaveEligibilityMasterListPage();
	}
	
	public String clear()
	{
		log.info("<===== Enter into LeaveEligibilityMasterBean.clear ===========>");
		leaveEligibilityMaster=new LeaveEligibilityMaster();
		log.info("<===== Exit into LeaveEligibilityMasterBean.clear ===========>");
		return showLeaveEligibilityMasterListPage();
	}

}
