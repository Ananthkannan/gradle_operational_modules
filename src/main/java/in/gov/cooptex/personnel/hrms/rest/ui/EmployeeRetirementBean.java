package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement.RetirementType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeeRetirementBean")
@Scope("session")
public class EmployeeRetirementBean {

	final String NORMAL_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listNormalRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_CREATE_PAGE = "/pages/admin/retirement/requestNormalRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_VIEW_PAGE = "/pages/admin/retirement/viewNormalRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_APPROVAL_PAGE = "/pages/admin/retirement/approveNormalRetirement.xhtml?faces-redirect=true";

	final String VOLUNTARY_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
	final String VOLUNTARY_RETIREMENT_CREATE_PAGE = "/pages/admin/retirement/requestVoluntaryRetirement.xhtml?faces-redirect=true";

	final String COMPULSORY_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listCompulsoryRetirement.xhtml?faces-redirect=true";
	final String COMPULSORY_RETIREMENT_CREATE_PAGE = "";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	LoginBean loginBean;
	
	@Autowired
	EmpRetirementRequestBean empRetirementRequestBean;

	@Getter
	@Setter
	String action, focusProperty;

	@Getter
	@Setter
	int totalRecords = 0, normalRetirementTotalRecords = 0, voluntaryRetirementTotalRecords,
			compulsoryRetirementTotalRecords;

	@Getter
	@Setter
	EmployeeRetirement normalEmployeeRetirement, voluntaryEmployeeRetirement, compulsoryEmployeeRetirement,
			selectedNormalRetirement, selectedVoluntaryRetirement, voluntaryRetirement, compulsoryRetirement,
			selectedCompulsoryRetirement;

	@Getter
	@Setter
	List<EmployeeRetirement> employeeRetirementList;

	@Getter
	@Setter
	List<EmployeeRetirement> selectedNormalEmployeeRetirementList;

	@Getter
	@Setter
	LazyDataModel<EmployeeRetirement> employeeRetirementLazyList, normalEmployeeRetirementListLazy,
			voluntaryEmployeeRetirementListLazy, compulsoryEmployeeRetirementListLazy;

	@Getter
	@Setter
	List<EntityMaster> selectedEntityList = new ArrayList<>();

	@Autowired
	HttpService httpService;

	@Autowired
	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	String entitytip = "No regions selected";

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	Boolean addButtonFlag = true, viewButtonFlag = true, approveButtonFlag = true;

	@Getter
	@Setter
	private String documentFileName;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	public String showRetirementListPage() {
		log.info("<===== Starts EmployeeRetirementBean.showRetirementListPage ============>" + action);
		addButtonFlag = true;
		approveButtonFlag = true;
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		if (action.equalsIgnoreCase("NORMAL_RETIREMENT_LIST")) {
			normalEmployeeRetirement = new EmployeeRetirement();
			selectedNormalRetirement = new EmployeeRetirement();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			totalRecords = 0;
			loadNormalRetirementLazyList();
			return NORMAL_RETIREMENT_LIST_PAGE;
		} else if (action.equalsIgnoreCase("VOLUNTARY_RETIREMENT_LIST")) {
			voluntaryEmployeeRetirement = new EmployeeRetirement();
			selectedVoluntaryRetirement = new EmployeeRetirement();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			totalRecords = 0;
			loadVoluntaryRetirementLazyList();
			return VOLUNTARY_RETIREMENT_LIST_PAGE;
		} else if (action.equalsIgnoreCase("COMPULSORY_RETIREMENT_LIST")) {
			compulsoryEmployeeRetirement = new EmployeeRetirement();
			selectedCompulsoryRetirement = new EmployeeRetirement();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			loadCompulsoryRetirementLazyList();
			return COMPULSORY_RETIREMENT_LIST_PAGE;
		}
		log.info("<===== Ends EmployeeRetirementBean.showRetirementListPage ============>");
		return null;
	}

	public String retirementAction() {
		log.info("<===== Starts EmployeeRetirementBean.retirementAction ============>" + action);
		if (action.equalsIgnoreCase("NORMAL_RETIREMENT_CREATE")) {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			normalEmployeeRetirement = new EmployeeRetirement();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			totalRecords = 0;
			return NORMAL_RETIREMENT_CREATE_PAGE;
		}
		if (action.equalsIgnoreCase("VOLUNTARY_RETIREMENT_CREATE")) {
			voluntaryEmployeeRetirement = new EmployeeRetirement();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			totalRecords = 0;
			return VOLUNTARY_RETIREMENT_CREATE_PAGE;
		} else if (action.equalsIgnoreCase("NORMAL_RETIREMENT_VIEW")
				|| action.equalsIgnoreCase("NORMAL_RETIREMENT_APPROVAL")) {
			if (selectedNormalRetirement == null || selectedNormalRetirement.getId() == null) {
				errorMap.notify(ErrorDescription.EMP_RETIREMENT_NORAML_SELECT_EMPTY.getCode());
				return null;
			}
			return getNormalRetirementById();
		}
		log.info("<===== Ends EmployeeRetirementBean.retirementAction ============>");
		return null;
	}

	public String approveOrRejectNormalRetirement() {
		log.info("<===== Starts EmployeeRetirementBean.approveOrRejectNormalRetirement ============>"
				+ selectedNormalRetirement.getId());
		try {
			if (selectedNormalRetirement.getRetirementComments() == null
					|| selectedNormalRetirement.getRetirementComments().isEmpty()) {
				errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_COMMENTS_REQUIRED.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("REJECT"))
				selectedNormalRetirement.setRetirementStatus(ApprovalStatus.REJECTED);
			else
				selectedNormalRetirement.setRetirementStatus(ApprovalStatus.APPROVED);

			BaseDTO response = httpService.put(SERVER_URL + "/employee/retirement/approveorreject",
					selectedNormalRetirement);
			if (response != null && response.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("REJECT")) {
					errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_REJECTED_SUCCESSFULLY.getCode());
				} else {
					errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_APPROVED_SUCCESSFULLY.getCode());
				}
				return showRetirementListPage();
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while approveOrReject .....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EmployeeRetirementBean.retirementAction ============>");
		return null;
	}

	public String getNormalRetirementById() {
		log.info("<== Starts EmployeeRetirementBean.getNormalRetirementById ==>" + selectedNormalRetirement.getId());
		try {
			BaseDTO response = httpService
					.get(SERVER_URL + "/employee/retirement/get/" + selectedNormalRetirement.getId());
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				selectedNormalRetirement = mapper.readValue(jsonResponse, new TypeReference<EmployeeRetirement>() {
				});
				if (action.equalsIgnoreCase("NORMAL_RETIREMENT_APPROVAL")) {
					return NORMAL_RETIREMENT_APPROVAL_PAGE;
				} else {
					return NORMAL_RETIREMENT_VIEW_PAGE;
				}
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<== Ends EmployeeRetirementBean.getNormalRetirementById ==>");
		return null;
	}

	/*
	 * public void loadAllEntityList() { log.
	 * info("<===== Starts EmployeeRetirementBean.loadAllEntityList ============>");
	 * try { mapper = new ObjectMapper(); BaseDTO
	 * response=httpService.get(SERVER_URL+"/entitymaster/getallactive");
	 * jsonResponse = mapper.writeValueAsString(response.getResponseContents());
	 * entityMasterList = mapper.readValue(jsonResponse, new
	 * TypeReference<List<EntityMaster>>() {}); }catch(Exception e) {
	 * log.error("Exception occured while loading entity...." , e);
	 * errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode()); }
	 * log.info("<===== Ends EmployeeRetirementBean.loadAllEntityList ============>"
	 * ); }
	 */

	public void changeEntity() {
		entitytip = "";
		for (EntityMaster entity : entityMasterList) {
			if (selectedEntityList.contains(entity.getId())) {
				if (entitytip != "") {
					entitytip = entitytip + ", ";
				}
				entitytip = entitytip + entity.getName();
			}
		}
	}

	public void searchEmployeeList() {
		log.info("<===== Starts EmployeeRetirementBean.searchEmployeeList ============>" + action);
		try {
			validateNormalRetirementSearch();
			BaseDTO response = httpService.post(SERVER_URL + "/employee/retirement/generate/list",
					normalEmployeeRetirement);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				employeeRetirementList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>() {
				});
				if (employeeRetirementList != null)
					totalRecords = employeeRetirementList.size();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				log.error("response :::" + response);
			}
		} catch (Exception e) {
			log.error("Exception occured while search ......" + e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<===== Ends EmployeeRetirementBean.searchEmployeeList ============>");
	}

	public void validateNormalRetirementSearch() {

		if (selectedEntityList == null && selectedEntityList.size() == 0) {
			log.info("Regions list empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_REGION_LIST_EMPTY.getCode());
			return;
		}
		if (normalEmployeeRetirement.getFromDate() == null) {
			log.info("from date is empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_FROMDATE_NULL.getCode());
			return;
		}
		if (normalEmployeeRetirement.getToDate() == null) {
			log.info("todate is empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_TODATE_NULL.getCode());
			return;
		}
		if (normalEmployeeRetirement.getToDate().before(normalEmployeeRetirement.getFromDate())) {
			log.info("todate is less than from date.");
			errorMap.notify(ErrorDescription.RETIREMENT_FROMDATE_GREATER_THAN_TODATE.getCode());
			return;
		}
		normalEmployeeRetirement.setRegionId(new ArrayList<Long>());
		selectedEntityList.stream().forEach(entity -> {
			normalEmployeeRetirement.getRegionId().add(entity.getEntityTypeMaster().getId());
		});

	}

	public void loadNormalRetirementLazyList() {
		normalEmployeeRetirementListLazy = new LazyDataModel<EmployeeRetirement>() {

			private static final long serialVersionUID = 2849181192334586867L;

			List<EmployeeRetirement> empList = new ArrayList<>();

			@Override
			public List<EmployeeRetirement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				PaginationDTO paginationRequestDto = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				try {
					paginationRequestDto.setRetirementType(RetirementType.NORMAL);
					log.info("Pagination request is ........" + paginationRequestDto);
					BaseDTO responseDto = httpService.post(SERVER_URL + "/employee/retirement/lazyload",
							paginationRequestDto);
					if (responseDto != null && responseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(responseDto.getResponseContents());
						empList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>() {
						});
						this.setRowCount(responseDto.getTotalRecords());
						normalRetirementTotalRecords = responseDto.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception in normalEmployeeRetirementListLazy ..", e);
				}
				return empList;
			}

			@Override
			public Object getRowKey(EmployeeRetirement object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmployeeRetirement getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmployeeRetirement emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeeRetirementBean.onRowSelect ========>" + event);
		selectedNormalRetirement = ((EmployeeRetirement) event.getObject());
		if (!selectedNormalRetirement.getRetirementStatus().equals(ApprovalStatus.INPROGRESS)) {
			approveButtonFlag = false;
		}
		addButtonFlag = false;
		viewButtonFlag = true;
		/*
		 * if(selectedDepartment.getStatus() == true ){ deleteButtonFlag=true; }else
		 * if(selectedDepartment.getStatus() == false){ deleteButtonFlag=false; }
		 */
		log.info("<===Ends DepartmentBean.onRowSelect ========>");
	}

	/*
	 * public void loadLazyEmployeeList() { employeeRetirementLazyList = new
	 * LazyDataModel<EmployeeRetirement>() {re
	 * 
	 * private static final long serialVersionUID = 2849181192334586867L;
	 * 
	 * List<EmployeeRetirement> empList = new ArrayList<>();
	 * 
	 * @Override public List<EmployeeRetirement> load(int first, int pageSize,String
	 * sortField,SortOrder sortOrder, Map<String, Object> filters) { PaginationDTO
	 * paginationRequestDto = new PaginationDTO(first / pageSize, pageSize,
	 * sortField,sortOrder.toString(), filters); try {
	 * log.info("Pagination request is ........"+paginationRequestDto); BaseDTO
	 * responseDto = httpService.post(SERVER_URL+"/", paginationRequestDto);
	 * if(responseDto != null && responseDto.getStatusCode() == 0) { jsonResponse =
	 * mapper.writeValueAsString(responseDto.getResponseContents()); empList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>()
	 * { }); this.setRowCount(responseDto.getTotalRecords()); totalRecords =
	 * responseDto.getTotalRecords(); }else {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); }
	 * }catch(Exception e) { log.error("Exception in employee laxy list..",e); }
	 * return empList; }
	 * 
	 * @Override public Object getRowKey(EmployeeRetirement object) { // TODO
	 * Auto-generated method stub return object != null ? object.getId():null; }
	 * 
	 * @Override public EmployeeRetirement getRowData(String rowKey) { // TODO
	 * Auto-generated method stub for(EmployeeRetirement emp :empList) {
	 * if(emp.getId().equals(Long.valueOf(rowKey))) { return emp; } } return null; }
	 * 
	 * }; }
	 */

	public String submit() {
		log.info("<===== Starts EmployeeRetirementBean.submit ============>" + action);
		try {
			if (action.equalsIgnoreCase("NORMAL_RETIREMENT")) {
				if (employeeRetirementList == null || employeeRetirementList.size() == 0) {
					log.info("Employee list is empty");
					errorMap.notify(ErrorDescription.RETIREMENT_EMP_LIST_EMPTY.getCode());
					return null;
				}
				BaseDTO response = httpService.post(SERVER_URL + "/employee/retirement/create/list",
						employeeRetirementList);
				if (response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_SAVED_SUCCESSFULLY.getCode());
					action = "NORMAL_RETIREMENT_LIST";
					return showRetirementListPage();
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					log.info("Response ......" + response);
				}
				return null;
			} else if (action.equalsIgnoreCase("VOLUNTARY_RETIREMENT")) {
				voluntaryEmployeeRetirement = new EmployeeRetirement();
				return VOLUNTARY_RETIREMENT_CREATE_PAGE;
			} else if (action.equalsIgnoreCase("COMPULSORY_RETIREMENT")) {
				compulsoryEmployeeRetirement = new EmployeeRetirement();
				return COMPULSORY_RETIREMENT_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured in submit....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<===== Ends EmployeeRetirementBean.submit ============>");
		return null;
	}

	public void updateTodate() {
		if (normalEmployeeRetirement.getFromDate() != null && normalEmployeeRetirement.getToDate() != null
				&& normalEmployeeRetirement.getToDate().before(normalEmployeeRetirement.getFromDate())) {
			focusProperty = "toDate";
			normalEmployeeRetirement.setToDate(null);
		}
	}

	public String showCompulsory(String viewPage) {
		log.info("[ Retirement page ]" + viewPage);

		if (viewPage.equals(RetirementType.COMPULSORY.name())) {
			compulsoryRetirement = new EmployeeRetirement();
			viewPage = "/pages/admin/retirement/createCompulsoryRetirement.xhtml?faces-redirect=true";
		}

		return viewPage;
	}

	public String showVoluntary(String viewPage) {
		log.info("[ Retirement page ]" + viewPage);

		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		if (viewPage.equals(actionType.ADD.name())) {
			voluntaryRetirement = new EmployeeRetirement();
			documentFileName = "";
			viewPage = "/pages/admin/retirement/requestVoluntaryRetirement.xhtml?faces-redirect=true";
		} else if (viewPage.equals(actionType.VIEW.name())) {
			if (Objects.isNull(selectedVoluntaryRetirement)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else {
				getEmployeeRetirement(selectedVoluntaryRetirement.getId());
				viewPage = "/pages/admin/retirement/viewVoluntaryRetirement.xhtml?faces-redirect=true";
			}
		} else if (viewPage.equals(actionType.EDIT.name())) {
			if (Objects.isNull(selectedVoluntaryRetirement)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else {
				getEmployeeRetirement(selectedVoluntaryRetirement.getId());
				viewPage = "/pages/admin/retirement/requestVoluntaryRetirement.xhtml?faces-redirect=true";
			}
		} else if (viewPage.equals(actionType.APPROVE.name())) {
			if (Objects.isNull(selectedVoluntaryRetirement)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else {
				getEmployeeRetirement(selectedVoluntaryRetirement.getId());
				viewPage = "/pages/admin/retirement/approveVoluntaryRetirement.xhtml?faces-redirect=true";
			}
		} else if (viewPage.equals(actionType.DELETE.name())) {
			if (Objects.isNull(selectedVoluntaryRetirement)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
		}
		return viewPage;
	}

	public void getEmployeeDetails(String search) {
		log.info("[ Get employee details ]");
		try {

			BaseDTO baseDTO = null;

			// For Employee Voluntary Retirement
			if (search.equals(RetirementType.VOLUNTARY.name())) {

				if (Objects.isNull(voluntaryRetirement.getEmployee())
						&& StringUtils.isEmpty(voluntaryRetirement.getEmployee().getEmpCode())) {
					log.error("[ Selected employee code is ]" + voluntaryRetirement.getEmployee());
					return;
				}
				log.info("[ Selected employee code is : ]" + voluntaryRetirement.getEmployee().getEmpCode());

				log.info("[Employee code URL is : ]" + EndPoint.EMP_CODE.URL
						+ voluntaryRetirement.getEmployee().getEmpCode());

				baseDTO = httpService.get(EndPoint.EMP_CODE.URL + voluntaryRetirement.getEmployee().getEmpCode());

			}

			// For Employee Compulsory Retirement
			if (search.equals(RetirementType.COMPULSORY.name())) {
				if (Objects.isNull(compulsoryRetirement.getEmployee())
						&& StringUtils.isEmpty(compulsoryRetirement.getEmployee().getEmpCode())) {
					log.error("[ Selected employee code is ]" + compulsoryRetirement.getEmployee());
					return;
				}
				log.info("[ Selected employee code is : ]" + compulsoryRetirement.getEmployee().getEmpCode());

				log.info("[Employee code URL is : ]" + EndPoint.EMP_CODE.URL
						+ compulsoryRetirement.getEmployee().getEmpCode());

				baseDTO = httpService.get(EndPoint.EMP_CODE.URL + compulsoryRetirement.getEmployee().getEmpCode());
			}

			if (Objects.isNull(baseDTO)) {
				log.info("[ Employee details response dto is : ]" + baseDTO);
				return;
			} else {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				EmployeeMaster employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				log.info("[ Employee details response is : ]" + employee);

				// For Employee Voluntary Retirement
				if (search.equals(RetirementType.VOLUNTARY.name())) {
					if (Objects.isNull(employee)) {
						errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
						voluntaryRetirement = new EmployeeRetirement();
					} else {
						voluntaryRetirement.setEmployee(employee);
					}
					RequestContext.getCurrentInstance().update("voluntaryForm:voluntaryPanel");
				}

				// For Employee Compulsory Retirement
				if (search.equals(RetirementType.COMPULSORY.name())) {
					if (Objects.isNull(employee)) {
						errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
						compulsoryRetirement = new EmployeeRetirement();
					} else {
						compulsoryRetirement.setEmployee(employee);
					}
					RequestContext.getCurrentInstance().update("compulsoryForm:compulsoryPanel");
				}
			}
		} catch (Exception e) {
			log.error("[ Exception while get employee details ]", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public enum EndPoint {
		EMP_CODE(AppUtil.getPortalServerURL() + "/employee/retirement/get/employee/"), CRAETE_RETIREMENT(
				AppUtil.getPortalServerURL() + "/employee/retirement/create"), UPDATE_RETIREMENT(
						AppUtil.getPortalServerURL() + "/employee/retirement/update"), RETIREMENT_BY_ID(
								AppUtil.getPortalServerURL() + "/employee/retirement/get/"), DELETE_BY_ID(
										AppUtil.getPortalServerURL()
												+ "/employee/retirement/delete/"), APPROVAL_RETIREMENT(
														AppUtil.getPortalServerURL()
																+ "/employee/retirement/approval"),;

		private final String URL;

		EndPoint(final String URL) {
			this.URL = URL;
		}

		@Override
		public String toString() {
			return URL;
		}
	}

	public String saveEmployeeRetirement(String retirement) {
		try {

			BaseDTO saveDTO = null;

			log.info("[ Create URL is : ]" + EndPoint.CRAETE_RETIREMENT.URL);

			// For Employee Voluntary Retirement
			if (retirement.equals(RetirementType.VOLUNTARY.name())) {
				if (Objects.isNull(voluntaryRetirement)) {
					log.error("[ Employee voluntary retirement details is : ]" + voluntaryRetirement);
					errorMap.notify(ErrorDescription.EMP_RETIREMENT_NOT_FOUND.getErrorCode());
					return "";
				}
				log.info("[ Employee voluntary retirement response is : ]" + voluntaryRetirement.getRetirementType());
				if (Objects.isNull(voluntaryRetirement.getId())) {
					saveDTO = httpService.post(EndPoint.CRAETE_RETIREMENT.URL, voluntaryRetirement);
				} else {
					saveDTO = httpService.put(EndPoint.UPDATE_RETIREMENT.URL, voluntaryRetirement);
				}
			}
			// For Employee Compulsory Retirement
			if (retirement.equals(RetirementType.COMPULSORY.name())) {
				if (Objects.isNull(compulsoryRetirement)) {
					log.error("[ Employee compulsory retirement details is : ]" + compulsoryRetirement);
					errorMap.notify(ErrorDescription.EMP_RETIREMENT_NOT_FOUND.getErrorCode());
					return "";
				}
				log.info("[ Employee compulsory retirement response is : ]" + compulsoryRetirement.getRetirementType());
				if (Objects.isNull(compulsoryRetirement.getId())) {
					saveDTO = httpService.post(EndPoint.CRAETE_RETIREMENT.URL, compulsoryRetirement);
				} else {
					saveDTO = httpService.post(EndPoint.UPDATE_RETIREMENT.URL, compulsoryRetirement);
				}
			}

			if (Objects.isNull(saveDTO)) {
				log.error("[ Employee retirement response is : ]" + saveDTO);
				errorMap.notify(saveDTO.getStatusCode());
			}

			if (saveDTO.getStatusCode() == 0) {
				log.error("[ Employee retirement success response is : ]" + saveDTO);
				if (retirement.equals(RetirementType.VOLUNTARY.name())) {
					errorMap.notify(ErrorDescription.RETIREMENT_VOLUNTARY_SAVED_SUCCESSFULLY.getErrorCode());
					return "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
				} else {
					errorMap.notify(ErrorDescription.RETIREMENT_COMPULSORY_SAVED_SUCCESSFULLY.getErrorCode());
					return "/pages/admin/retirement/listCompulsoryRetirement.xhtml?faces-redirect=true";
				}
			} else {
				log.error("[ Employee retirement failuer response is : ]" + saveDTO);
				errorMap.notify(saveDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("[ Exception while save employee retirement details ]", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return NORMAL_RETIREMENT_LIST_PAGE;
	}

	public void clear(String clear) {
		log.info("[ Clear employee retirement details ]" + clear);
		if (clear.equals(RetirementType.VOLUNTARY.name())) {
			voluntaryRetirement = new EmployeeRetirement();
			RequestContext.getCurrentInstance().update("voluntaryForm:voluntaryPanel");
		}

		if (clear.equals(RetirementType.COMPULSORY.name())) {
			compulsoryRetirement = new EmployeeRetirement();
			empRetirementRequestBean.empGeneralInfoDto=null;
			RequestContext.getCurrentInstance().update("compulsoryForm:compulsoryPanel");
		}
	}

	public String cancel() {
		voluntaryRetirement = new EmployeeRetirement();
		return NORMAL_RETIREMENT_LIST_PAGE;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			uploadedFile = event.getFile();
			long size = uploadedFile.getSize();
			log.info("Employee retirement request document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				voluntaryRetirement.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_RETIREMENT_REQUEST_UPLOAD_PATH");
			File file = new File(uploadPath);
			uploadPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(uploadPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Uploaded document path is :" + uploadPath);
			documentFileName = uploadedFile.getFileName();
			String append = new Date().toGMTString() + documentFileName;
			voluntaryRetirement.setDocumentPath(uploadPath + append);
			errorMap.notify(ErrorDescription.INFO_EMP_RETIREMENT_DOCUMENT_UPLOADED.getErrorCode());
			inputStream = uploadedFile.getInputstream();
			log.info(" Path Doesn't exixts" + append);
			outputStream = new FileOutputStream(uploadPath + append);
			byte[] buffer = new byte[(int) uploadedFile.getSize()];
			int bytesRead = 0;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			log.info(" upload Path is >>>" + uploadPath);
			log.info(" Retirement Document is uploaded successfully");
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	// voluntary retirement
	public void loadVoluntaryRetirementLazyList() {
		voluntaryEmployeeRetirementListLazy = new LazyDataModel<EmployeeRetirement>() {

			private static final long serialVersionUID = 2849181192334586867L;

			List<EmployeeRetirement> empList = new ArrayList<>();

			@Override
			public List<EmployeeRetirement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				PaginationDTO paginationRequestDto = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				try {
					paginationRequestDto.setRetirementType(RetirementType.VOLUNTARY);
					log.info("Pagination request is ........" + paginationRequestDto);
					BaseDTO responseDto = httpService.post(SERVER_URL + "/employee/retirement/lazyload",
							paginationRequestDto);
					if (responseDto != null && responseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(responseDto.getResponseContents());
						empList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>() {
						});
						this.setRowCount(responseDto.getTotalRecords());
						voluntaryRetirementTotalRecords = responseDto.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception in voluntaryEmployeeRetirementListLazy ..", e);
				}
				return empList;
			}

			@Override
			public Object getRowKey(EmployeeRetirement object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmployeeRetirement getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmployeeRetirement emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	// compulsory retirement
	public void loadCompulsoryRetirementLazyList() {
		compulsoryEmployeeRetirementListLazy = new LazyDataModel<EmployeeRetirement>() {

			private static final long serialVersionUID = 2849181192334586867L;

			List<EmployeeRetirement> empList = new ArrayList<>();

			@Override
			public List<EmployeeRetirement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				PaginationDTO paginationRequestDto = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				try {
					paginationRequestDto.setRetirementType(RetirementType.COMPULSORY);
					log.info("Pagination request is ........" + paginationRequestDto);
					BaseDTO responseDto = httpService.post(SERVER_URL + "/employee/retirement/lazyload",
							paginationRequestDto);
					if (responseDto != null && responseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(responseDto.getResponseContents());
						empList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>() {
						});
						this.setRowCount(responseDto.getTotalRecords());
						compulsoryRetirementTotalRecords = responseDto.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception in compulsoryEmployeeRetirementListLazy ..", e);
				}
				return empList;
			}

			@Override
			public Object getRowKey(EmployeeRetirement object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmployeeRetirement getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmployeeRetirement emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	public String cancelRetirement(String viewPage) {
		log.info("Retirment list page is : " + viewPage);
		if (viewPage.equals(RetirementType.VOLUNTARY.name())) {
			selectedVoluntaryRetirement = new EmployeeRetirement();
			viewPage = "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
		}

		if (viewPage.equals(RetirementType.COMPULSORY.name())) {
			viewPage = "/pages/admin/retirement/listCompulsoryRetirement.xhtml?faces-redirect=true";
		}
		return viewPage;
	}

	public String generateInfo(String viewPage) {
		log.info("[ Retirement page ]" + viewPage);

		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		if (viewPage.equals(RetirementType.VOLUNTARY.name())) {
			if (Objects.isNull(selectedVoluntaryRetirement)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else {
				getEmployeeRetirement(selectedVoluntaryRetirement.getId());
				viewPage = "/pages/admin/retirement/createVoluntaryRetirement.xhtml?faces-redirect=true";
			}
		}

		if (viewPage.equals(RetirementType.COMPULSORY.name())) {
			compulsoryRetirement = new EmployeeRetirement();
			viewPage = "/pages/admin/retirement/createCompulsoryRetirement.xhtml?faces-redirect=true";
		}
		return viewPage;
	}

	public EmployeeRetirement getEmployeeRetirement(Long retirementId) {
		try {
			log.info("[ Employee retirement id is : ]" + selectedVoluntaryRetirement.getId());
			log.info("[ Employee retirement id url is : ]" + EndPoint.RETIREMENT_BY_ID.URL + retirementId);
			BaseDTO baseDTO = httpService.get(EndPoint.RETIREMENT_BY_ID.URL + retirementId);
			log.info("[ Employee retirement id is : ]" + selectedVoluntaryRetirement.getId());
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			voluntaryRetirement = mapper.readValue(jsonResponse, EmployeeRetirement.class);
			documentFileName = voluntaryRetirement.getDocumentPath();
			log.info("[ Retirement details is : ]" + voluntaryRetirement);
		} catch (Exception e) {

		}
		return voluntaryRetirement;
	}

	public enum actionType {
		ADD, VIEW, DELETE, EDIT, APPROVE;
	}

	public String deleteRecord(String retirementType) {
		Long retirementId;
		BaseDTO baseDTO = null;
		if (retirementType.equals(RetirementType.VOLUNTARY.name())) {
			retirementId = selectedVoluntaryRetirement.getId();
			log.info("Selected Voluntary Retirement id url is : " + EndPoint.DELETE_BY_ID.URL + retirementId);
			baseDTO = httpService.delete(EndPoint.DELETE_BY_ID.URL + retirementId);
			retirementType = "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
		}
		if (retirementType.equals(RetirementType.COMPULSORY.name())) {
			retirementId = selectedVoluntaryRetirement.getId();
			log.info("Selected Compulsory Retirement id is : " + selectedVoluntaryRetirement.getId());
			baseDTO = httpService.delete(EndPoint.DELETE_BY_ID.URL + retirementId);
			retirementType = "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.DELETE_SUCCESSFULLY.getErrorCode());
		}
		return retirementType;
	}

	public void confirmStatus() {
		if (voluntaryRetirement == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public String approvalRetirement() {
		try {
			log.info("Selected Compulsory Retirement id is : " + voluntaryRetirement);
			BaseDTO baseDTO = httpService.put(EndPoint.APPROVAL_RETIREMENT.URL, voluntaryRetirement);
			if (baseDTO.getStatusCode() == 0) {

			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("Exception while approval retirement ", e);
		}
		return "";
	}
}
