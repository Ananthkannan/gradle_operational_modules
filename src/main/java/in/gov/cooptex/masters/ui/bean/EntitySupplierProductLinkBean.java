package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dnp.model.EntitySupplierProductLink;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("entitySupplierProductLinkBean")
public class EntitySupplierProductLinkBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final String CREATE_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE = "/pages/masters/createEntitySupplierProductLink.xhtml?faces-redirect=true;";
	private final String LIST_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE = "/pages/masters/listEntitySupplierProductLink.xhtml?faces-redirect=true;";
	private final String VIEW_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE = "/pages/masters/viewEntitySupplierProductLink.xhtml?faces-redirect=true;";
	final String SERVER_URL = AppUtil.getPortalServerURL();
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	int totalRecords = 0;
	ObjectMapper mapper = new ObjectMapper();
	String jsonResponse;
	@Autowired
	CommonDataService commonDataService;
	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	EntitySupplierProductLink entitySupplierProductLink;

	@Getter
	@Setter
	EntitySupplierProductLink addEntitySupplierProductLink;

	@Getter
	@Setter
	LazyDataModel<EntitySupplierProductLink> lazyEntitySupplierProductLinkList;

	@Getter
	@Setter
	EntitySupplierProductLink selectedEntitySupplierProductLink = new EntitySupplierProductLink();
	@Getter
	@Setter
	Long supplierTypeId;
	@Getter
	@Setter
	Long entityTypeId;
	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;
	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;
	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster;
	@Getter
	@Setter
	List<ProductVarietyMaster> selectedProductVarietyMasterList;

	@Setter @Getter
	String groupNames="";
	
	@Getter
	@Setter
	EntityMaster selectedEntityMaster;
	@Getter
	@Setter
	CircleMaster selectedCircleMaster;
	@Getter
	@Setter
	SupplierMaster selectedSupplierMaster;
	@Getter
	@Setter
	SupplierTypeMaster selectedSupplierTypeMaster;
	@Getter
	@Setter
	List<EntitySupplierProductLink> entitySupplierProductLinkList = new ArrayList<>();;

	@Setter
	@Getter
	List<EntityTypeMaster> entityTypetList;
	@Setter
	@Getter
	List<EntityMaster> entityMasterList;
	@Getter
	@Setter
	List<SupplierMaster> supplierList;
	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeList;
	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	public String showEntitySupplierProductLinkPage() {
		log.info("<==== Starts EntitySupplierProductLinkBean.showEntitySupplierProductLinkPage =====>");
		mapper = new ObjectMapper();
		setSelectedEntitySupplierProductLink(null);
		editButtonFlag = true;
		deleteButtonFlag = false;
		addButtonFlag = false;
		loadLazyEntitySupplierProductLinkList();
		log.info("<==== ends EntitySupplierProductLinkBean.showEntitySupplierProductLinkPage =====>");
		return LIST_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EntitySupplierProductLinkBean.onRowSelect ========>" + event);
		selectedEntitySupplierProductLink = ((EntitySupplierProductLink) event.getObject());

		log.info("selectedEntitySupplierProductLink id" + selectedEntitySupplierProductLink.getId());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		log.info("<===Ends EntitySupplierProductLinkBean.onRowSelect ========>");
	}

	public void loadLazyEntitySupplierProductLinkList() {
		log.info("<===== Starts EntitySupplierProductLinkBean.loadLazyEntitySupplierProductLinkList ======>");
		lazyEntitySupplierProductLinkList = new LazyDataModel<EntitySupplierProductLink>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<EntitySupplierProductLink> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/entitysupplierproductlink/getentitysupplierproductlinklist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						entitySupplierProductLinkList = mapper.readValue(jsonResponse,
								new TypeReference<List<EntitySupplierProductLink>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyEntitySupplierProductLinkList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return entitySupplierProductLinkList;
			}

			@Override
			public Object getRowKey(EntitySupplierProductLink res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EntitySupplierProductLink getRowData(String rowKey) {
				try {
					for (EntitySupplierProductLink entitySupplierProductLink : entitySupplierProductLinkList) {
						if (entitySupplierProductLink.getId().equals(Long.valueOf(rowKey))) {
							selectedEntitySupplierProductLink = entitySupplierProductLink;
							return entitySupplierProductLink;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== ends EntitySupplierProductLinkBean.loadLazyEntitySupplierProductLinkList ======>");
	}

	public String addEntitySupplierProductLink() {

		log.info("<--- Start EntitySupplierProductLinkBean.addEntitySupplierProductLink ---->");
		try {
			setSelectedEntityTypeMaster(null);
			setSelectedEntityMaster(null);
			setSelectedProductVarietyMaster(null);
			setSelectedSupplierTypeMaster(null);
			setSelectedCircleMaster(null);
			setSelectedSupplierMaster(null);
			setSelectedProductVarietyMasterList(null);
			action = "ADD";
			entitySupplierProductLinkList = new ArrayList<>();
			entitySupplierProductLink = new EntitySupplierProductLink();
			entityTypetList = commonDataService.loadEntityTypes();
			supplierTypeList = commonDataService.loadSupplierTypeMaster();
			getProductVarietys();
			circleMasterList = loadCircles();
			
			log.info("<---- End  EntitySupplierProductLinkBean.addEntitySupplierProductLink ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;

	}

	public List<CircleMaster> loadCircles() {
		log.info("<<<< ----------starts EntitySupplierProductLinkBean ... loadCircles ------- >>>>");
		try {
			circleMasterList = new ArrayList<CircleMaster>();
			String url = SERVER_URL + "/entitysupplierproductlink/getallcircle";
			log.info(" EntitySupplierProductLinkBean Bean ==>>> loadCircles URL===>>> " + url);
			BaseDTO response = httpService.get(url);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				circleMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<CircleMaster>>() {
						});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End EntitySupplierProductLinkBean ... loadCircles ------- >>>>");
		return circleMasterList;
	}

	public void loadEntityListByEntityType() {
		log.info("<---- starts  EntitySupplierProductLinkBean.loadEntityList ---->");
		entityMasterList = commonDataService.getEntityMaster(selectedEntityTypeMaster);
		log.info("<---- End  EntitySupplierProductLinkBean.loadEntityList ---->");
	}

	public List<ProductVarietyMaster> getProductVarietys() {
		log.info("<<<< ----------starts EntitySupplierProductLinkBean ... getProductVarietys ------- >>>>");
		try {
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			String url = SERVER_URL + "/productvariety/getallproductvarietys";
			log.info(" EntitySupplierProductLinkBean Bean ==>>> getProductVarietys URL===>>> " + url);
			BaseDTO response = httpService.get(url);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End EntitySupplierProductLinkBean ... getProductVarietys ------- >>>>");
		return productVarietyMasterList;
	}

	public void onSupplierSelection() {

		log.info("<====== EntitySupplierProductLinkBean.onSupplierSelection Starts====> ");
		Long circleId = selectedCircleMaster.getId();
		Long supplierTypeId = selectedSupplierTypeMaster.getId();
		supplierList = loadSupplierMasterByCircleAndsupplierType(circleId, supplierTypeId);
		log.info("<====== EntitySupplierProductLinkBean.onSupplierSelection Ends====> ");
	}

	public List<SupplierMaster> loadSupplierMasterByCircleAndsupplierType(Long circleId, Long supplierTypeId) {
		log.info("Inside loadSupplierMasterByCircleAndsupplierType()");
		supplierList = new ArrayList<SupplierMaster>();
		try {
			String Url = SERVER_URL + "/entitysupplierproductlink/getsupplierbycircleMasterandsuppliertype/" + circleId
					+ "/" + supplierTypeId;
			log.info("URL request for entity :: " + Url);
			BaseDTO response = httpService.get(Url);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("Total No. Of supplierList:[" + supplierList.size() + "]");
			}
		} catch (Exception e) {
			log.info("Error in getting supplierList() ::: " + e);
		}
		return supplierList;
	}

	public String clearEntitySupplierProductLinkList() {

		log.info("clear InventotoryConfig list called :");
		setSelectedEntityTypeMaster(null);
		setSelectedEntityMaster(null);
		setSelectedProductVarietyMaster(null);
		setSelectedSupplierTypeMaster(null);
		setSelectedCircleMaster(null);
		setSelectedSupplierMaster(null);
		setSelectedProductVarietyMasterList(null);
		return CREATE_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;
	}

	public void entitySupplierProductLinkAddNewInward() {
		try {
			if (getSelectedEntityTypeMaster() == null) {
				log.info("Invalid Entity Type:");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_ENTITY_TYPE_MASTER.getErrorCode());
			} else if (getSelectedEntityMaster() == null) {
				log.info("Invalid Entity :");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_ENTITY_MASTER.getErrorCode());
			} else if (getSelectedProductVarietyMasterList() == null) {
				log.info("Invalid Product Variety:");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_PRODUCT_VARIETY_MASTER.getErrorCode());
			} else if (getSelectedSupplierTypeMaster() == null) {
				log.info("Invalid SupplierType :");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_TYPE_MASTER.getErrorCode());
			} else if (getSelectedCircleMaster() == null) {
				log.info("Invalid Circle:");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_CIRCLE_MASTER.getErrorCode());
			} else if (getSelectedSupplierMaster() == null) {
				log.info("Invalid Supplier ");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_MASTER.getErrorCode());
			} else {
				
				log.info("EntitySupplierProductLink add new received :");
				Long supplierId=getSelectedSupplierMaster().getId();
				SupplierMaster supplierMaster=getSelectedSupplierMaster();
				Long entityId=getSelectedEntityMaster().getId();
				EntityMaster entityMaster=getSelectedEntityMaster();
				for(ProductVarietyMaster obj:getSelectedProductVarietyMasterList()) {
					selectedProductVarietyMaster=obj;
					entitySupplierProductLink = new EntitySupplierProductLink();
					entitySupplierProductLink.setEntityMaster(entityMaster);
					entitySupplierProductLink.setProductVarietyMaster(obj);
					entitySupplierProductLink.setSupplier(supplierMaster);
					entitySupplierProductLink.setSupplierTypeMaster(getSelectedSupplierTypeMaster());
					entitySupplierProductLink.setCircleMaster(getSelectedCircleMaster());
					entitySupplierProductLink.setEntityTypeMaster(getSelectedEntityTypeMaster());
					
					checkProductSupplier(entityId, obj.getId(), supplierId,entitySupplierProductLink);
					
				}
				if(entitySupplierProductLinkList.size()>=1) {
					errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ADD_SUCCESS.getErrorCode());
				}
				if(entitySupplierProductLinkList.size()==0 && entitySupplierProductLinkList.isEmpty()) {
					errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ALREADY_EXIST.getErrorCode());
				}
				setSelectedEntityTypeMaster(null);
				setSelectedEntityMaster(null);
				setSelectedProductVarietyMaster(null);
				setSelectedSupplierTypeMaster(null);
				setSelectedCircleMaster(null);
				setSelectedSupplierMaster(null);
				setSelectedProductVarietyMasterList(null);

			}
		} catch (Exception e) {
			log.error("entitySupplierProductLink Collection Add Error :" + e);
			errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ADD_FAILED.getErrorCode());
		}
	}
	

	
	public void checkProductSupplier(Long entityId,Long productId, Long supplierId,EntitySupplierProductLink entitySupplierProductLink) {
		log.info("checkProductSupplier called");
		try {
			String Url = SERVER_URL + "/entitysupplierproductlink/getentitySupplierproductlinkuniqlist/" +entityId+ "/"+ productId
					+ "/" + supplierId;
			log.info("URL request for entitySupplierProductLinkUniqList :: " + Url);
			BaseDTO response = httpService.get(Url);

			if (response != null) {
				if (response.getStatusCode() == 0) {
					entitySupplierProductLinkList.add(entitySupplierProductLink);
					log.info("entitySupplierProductLinkList"+entitySupplierProductLinkList.size()+entitySupplierProductLinkList);
				} else {
					log.info("entitySupplierProductLink alredy exist");
					setSelectedSupplierMaster(null);
					setSelectedProductVarietyMaster(null);
					setSelectedEntityMaster(null);
				}
			}
		} catch (Exception e) {
			log.info("Error in getting supplierList() ::: " + e);
		}
	}

	public String saveEntitySupplierProductLink() {
		log.info("<==== Starts EntitySupplierProductLinkBean.saveEntitySupplierProductLink =======>");
		try {
			if (action.equalsIgnoreCase("ADD") && entitySupplierProductLinkList.size() == 0) {
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_ITEMS_NULL.getErrorCode());
				return null;
			}
			BaseDTO response = httpService.post(SERVER_URL + "/entitysupplierproductlink/create",
					entitySupplierProductLinkList);

			if (response != null && response.getStatusCode() == 0) {
				log.info("Entity supplier product link saved successfully..........");
				if (action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_SAVE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EntitySupplierProductLinkBean.saveEntitySupplierProductLink =======>");
		return LIST_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;
	}

	public String UpdateEntitySupplierProductLinkBean() {
		log.info("<==== Starts EntitySupplierProductLinkBean.UpdateEntitySupplierProductLinkBean =======>");
		try {

			if (getSelectedEntityTypeMaster() == null) {
				log.info("Invalid Entity Type:");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_ENTITY_TYPE_MASTER.getErrorCode());
				return null;
			} else if (getSelectedEntityMaster() == null) {
				log.info("Invalid Entity :");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_ENTITY_MASTER.getErrorCode());
				return null;
			} else if (getSelectedProductVarietyMaster() == null) {
				log.info("Invalid Product Variety:");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_PRODUCT_VARIETY_MASTER.getErrorCode());
				return null;
			} else if (getSelectedSupplierTypeMaster() == null) {
				log.info("Invalid SupplierType :");
				errorMap.notify(
						ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_TYPE_MASTER.getErrorCode());
				return null;
			} else if (getSelectedCircleMaster() == null) {
				log.info("Invalid Circle:");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_CIRCLE_MASTER.getErrorCode());
				return null;
			} else if (getSelectedSupplierMaster() == null) {
				log.info("Invalid Supplier ");
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_INVALID_SUPPLIER_MASTER.getErrorCode());
				return null;
			}else {
				if (action.equalsIgnoreCase("EDIT")) {
					entitySupplierProductLink=new EntitySupplierProductLink();
					entitySupplierProductLink.setId(selectedEntitySupplierProductLink.getId());
					entitySupplierProductLink.setEntityMaster(getSelectedEntityMaster());
					entitySupplierProductLink.setProductVarietyMaster(getSelectedProductVarietyMaster());
					entitySupplierProductLink.setSupplier(getSelectedSupplierMaster());

					BaseDTO response = httpService.post(SERVER_URL + "/entitysupplierproductlink/update",
							entitySupplierProductLink);

					if (response != null && response.getStatusCode() == 0) {
						log.info("Entity supplier product link Updated successfully..........");
						errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_UPDATED_SUCCESS.getCode());

					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
						return null;
					}
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EntitySupplierProductLinkBean.UpdateEntitySupplierProductLinkBean =======>");
		return LIST_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;
	}

	public String deleteEntitySupplierProductLink() {
		log.info(" deleteEntitySupplierProductLink called");
		try {
			action = "Delete";
			if (selectedEntitySupplierProductLink == null || selectedEntitySupplierProductLink.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ENTITY_SUPPLIER_PRODUCT_LINK.getErrorCode());
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInventoryConfigDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteEntitySupplierProductLinkConfrom() {
		log.info("<===== Starts EntitySupplierProductLinkBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(
					SERVER_URL + "/entitysupplierproductlink/delete/" + selectedEntitySupplierProductLink.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ENTITY_SUPPLIER_PRODUCT_LINK_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInventoryConfigDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Entitysupplierproductlink ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends EntitySupplierProductLinkBean.deleteConfirm ===========>");
		return showEntitySupplierProductLinkPage();
	}

	public String clear() {
		log.info("<---- Start EntitySupplierProductLinkBean . clear ----->");
		showEntitySupplierProductLinkPage();
		setSelectedEntitySupplierProductLink(null);
		log.info("<---- End EntitySupplierProductLinkBean.clear ----->");

		return LIST_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;

	}

	public String viewEntitySupplierProductLink() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedEntitySupplierProductLink == null || selectedEntitySupplierProductLink.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ENTITY_SUPPLIER_PRODUCT_LINK.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedEntitySupplierProductLink.getId());

		selectedEntitySupplierProductLink = getEntitySupplierProductLinkDetails(
				selectedEntitySupplierProductLink.getId());

		log.info("selectedInventoryConfig Object:::" + selectedEntitySupplierProductLink);
		return VIEW_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;
	}

	private EntitySupplierProductLink getEntitySupplierProductLinkDetails(Long id) {

		log.info("<====== EntitySupplierProductLinkBean.getEntitySupplierProductLinkDetails starts====>" + id);
		entitySupplierProductLink = new EntitySupplierProductLink();
		try {
			if (selectedEntitySupplierProductLink == null || selectedEntitySupplierProductLink.getId() == null) {
				log.error(" No EntitySupplierProductLink selected ");
				errorMap.notify(ErrorDescription.SELECT_ENTITY_SUPPLIER_PRODUCT_LINK.getErrorCode());
			}
			log.info("Selected EntitySupplierProductLink  Id : : " + selectedEntitySupplierProductLink.getId());
			String url = SERVER_URL + "/entitysupplierproductlink/get/" + selectedEntitySupplierProductLink.getId();

			log.info("EntitySupplierProductLink Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			log.info("view response" + baseDTO.getResponseContent());
			entitySupplierProductLink = mapper.readValue(jsonResponse, EntitySupplierProductLink.class);

			if (baseDTO.getStatusCode() == 0) {
				log.info(" EntitySupplierProductLink Retrived  SuccessFully ");
			} else {
				log.error(" EntitySupplierProductLink Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return entitySupplierProductLink;
		} catch (Exception e) {
			log.error("Exception Ocured while get EntitySupplierProductLink Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editEntitySupplierProductLinkDetails() {
		log.info("editEntitySupplierProductLinkDetails started");
		try {
			if (selectedEntitySupplierProductLink == null || selectedEntitySupplierProductLink.getId() == null) {
				log.info("----- selectedEntitySupplierProductLink-------");
				errorMap.notify(ErrorDescription.SELECT_ENTITY_SUPPLIER_PRODUCT_LINK.getCode());
				return null;
			} else {
				entitySupplierProductLink = getEntitySupplierProductLinkDetails(
						selectedEntitySupplierProductLink.getId());
				entityTypetList = commonDataService.loadEntityTypes();
				supplierTypeList = commonDataService.loadSupplierTypeMaster();
				circleMasterList = commonDataService.loadCircles();
				getProductVarietys();
				selectedEntityTypeMaster = entitySupplierProductLink.getEntityTypeMaster();
				selectedEntityMaster = entitySupplierProductLink.getEntityMaster();
				selectedProductVarietyMaster = entitySupplierProductLink.getProductVarietyMaster();
				selectedSupplierTypeMaster = entitySupplierProductLink.getSupplierTypeMaster();
				selectedCircleMaster = entitySupplierProductLink.getCircleMaster();
				selectedSupplierMaster = entitySupplierProductLink.getSupplier();
				entityMasterList = commonDataService.getEntityMaster(selectedEntityTypeMaster);
				Long circleId = selectedCircleMaster.getId();
				Long supplierTypeId = selectedSupplierTypeMaster.getId();
				supplierList = loadSupplierMasterByCircleAndsupplierType(circleId, supplierTypeId);
			
				return CREATE_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;
			}

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		return CREATE_ENTITY_SUPPLIER_PRODUCT_LINK_PAGE;

	}

}
