package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionSource;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class PetitionSourceBean {

	private final String CREATE_PAGE = "/pages/masters/petitionSource/createPetitionSource.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/petitionSource/listPetitionSource.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/petitionSource/viewPetitionSource.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	PetitionSource petitionSource;

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper=new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	List<PetitionSource> petitionSourceList = new ArrayList<PetitionSource>();

	@Getter
	@Setter
	private int petitionSourceListSize;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	boolean disableAddButton = false;

	
	boolean forceLogin = false;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	LazyDataModel<PetitionSource> lazyPetitionSourceList;

	@PostConstruct
	public void init() {
		log.info(" PetitionSourceBean init --->" + serverURL);
		petitionSource = new PetitionSource();
		loadValues();
		loadLazypetitionSourceList();
		mapper=new ObjectMapper();
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			petitionSource = new PetitionSource();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String showList() {
		log.info("<---------- Loading PetitionSourceBean showList list page---------->");
		addButtonFlag=true;
		loadValues();
		loadLazypetitionSourceList();
		disableAddButton = false;
		return LIST_PAGE;
	}

//	private void getAllPetitionSourceList() {
//
//		log.info("<---------- Start PetitionSourceBean loadAllUserList---------->");
//
//		try {
//			BaseDTO baseDTO = new BaseDTO();
//			String url = serverURL + appPreference.getOperationApiUrl() + "/petitionsource/getall";
//			baseDTO = httpService.get(url);
//			petitionSource = new PetitionSource();
//			if (baseDTO != null) {
//				ObjectMapper mapper = new ObjectMapper();
//				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
//				petitionSourceList = mapper.readValue(jsonResponse, new TypeReference<List<PetitionSource>>() {
//				});
//			}
//		} catch (JsonProcessingException jpEx) {
//			log.error("Json processing exception occured while converting .... ", jpEx);
//		} catch (IOException ioEx) {
//			log.error("IO Exception occured .... ", ioEx);
//		} catch (Exception e) {
//			log.error("Exception occured .... ", e);
//		}
//
//		if (petitionSourceList != null) {
//			petitionSourceListSize = petitionSourceList.size();
//		} else {
//			petitionSourceListSize = 0;
//		}
//		// log.info("<<<< ----------End CountryMasterBean . getAllCountriesList -------
//		// >>>>");
//	}

	public String submit() {

		log.info("PetitionSourceMasterBean Submit method called");
		if (petitionSource.getCode() == null) {
			errorMap.notify(ErrorDescription.PETITIONSOURCE_CODE_EMPTY.getErrorCode());
			return null;
		}
		if (petitionSource.getName() == null || petitionSource.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.PETITIONSOURCE_NAME_EMPTY.getErrorCode());
			return null;
		}
		if (petitionSource.getName().trim().length() < 3 || petitionSource.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.PETITIONSOURCE_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (petitionSource.getLocalName() == null || petitionSource.getLocalName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.PETITIONSOURCE_LNAME_EMPTY.getErrorCode());
			return null;
		}

		log.info("...... petitionSource submit begin ....");
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + appPreference.getOperationApiUrl() + "/petitionsource/create",
					petitionSource);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() != 0) {
					log.info("PetitionSourceMasterBean saved successfully");
					showList();
					errorMap.notify(baseDTO.getStatusCode());
				} else if (baseDTO.getStatusCode() == 900109 || baseDTO.getStatusCode() == 900110
						|| baseDTO.getStatusCode() == 900111) {
					log.error(" processing exception occured while converting .... " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating PetitionSource" + e);
		}
		log.info("...... PetitionSource submit ended ....");

		return LIST_PAGE;
	}

	public String cancel() {
		System.out.println("the cancel method is called");
		log.info("<----Redirecting to user List page---->");
		petitionSource = new PetitionSource();
		disableAddButton = false;
		loadLazypetitionSourceList();
		return LIST_PAGE;

	}

	
	public String addPetitionSource() {
		log.info("<---- Start PetitionSourceBean.addPetitionSource ---->");
		try {
			action = "ADD";
			petitionSource = new PetitionSource();
			/* petitionSource.setActiveStatus(true); */

			log.info("<---- End PetitionSourceBean.addPetitionSource ---->");
		} catch (Exception e) {

			log.error("<---- Error occured while redirectin to user add page ---->");
		}
		disableAddButton = false;
		return CREATE_PAGE;

	}

	public String editPetitionSource() {
		log.info("<<<<< -------Start editCountry called ------->>>>>> ");
		action = "EDIT";

		if (petitionSource == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}

		if (petitionSource.getActiveStatus().equals("ACTIVE")) {
			petitionSource.setActiveStatus(petitionSource.getActiveStatus());
		} else {
			petitionSource.setActiveStatus(petitionSource.getActiveStatus());
		}
		log.info("<<<<< -------End editPetitionSource called ------->>>>>> ");
		getrecordById();
		return CREATE_PAGE;

	}

	public String viewPetitionSource() {
		action = "VIEW";

		log.info("<----Redirecting to user view page---->");
		if (petitionSource == null) {
			Util.addWarn("Select Atleast One Record");
			return null;
		}
		getrecordById();
		disableAddButton = false;
		return VIEW_PAGE;

	}

	public String deletePetitionSource() {

		try {
			log.info("<----deletePetitionSource begins---->");
			action = "DELETE";
			if (petitionSource == null) {
				Util.addWarn("Select Atleast One Record");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPetitionSourceDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}

	public String deletePetitionSourceConfirm() {

		try {
			log.info("Delete confirmed for PetitionSource [" + petitionSource.getId() + "]");
			PetitionSource petitionSourceDelete = new PetitionSource();
			petitionSourceDelete.setId(petitionSource.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/petitionsource/delete";
			baseDTO = httpService.post(url, petitionSource);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("PetitionSource deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					petitionSource = new PetitionSource();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting PetitionSource ....", e);
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start PetitionSourceBean . clear ----->");
		loadLazypetitionSourceList();
		petitionSource = new PetitionSource();
		log.info("<---- End PetitionSourceBean . clear ----->");
		addButtonFlag = true;
		return LIST_PAGE;

	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String backPage() {
		log.info("<---- Back to the PetitionSource list page ----->");
		showList();
		return LIST_PAGE;
	}
	
	public void loadLazypetitionSourceList() {
		log.info("<===== Starts petitionsourceBean.loadLazypetitionSourceList ======>");

		lazyPetitionSourceList = new LazyDataModel<PetitionSource>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<PetitionSource> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/petitionsource/getallpetitionsourcelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						petitionSourceList = mapper.readValue(jsonResponse, new TypeReference<List<PetitionSource>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("petitionSourceBean size---------------->" + lazyPetitionSourceList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload \r\n" + 
							"	ObjectMapper mapper;\r\n" + 
							"\r\n" + 
							"	String jsonResponse;...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return petitionSourceList;
			}

			@Override
			public Object getRowKey(PetitionSource res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PetitionSource getRowData(String rowKey) {
				for (PetitionSource petitionSources : petitionSourceList) {
					if (petitionSources.getId().equals(Long.valueOf(rowKey))) {
						petitionSource = petitionSources;
						return petitionSource;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  petitionSourceBean.loadLazySalesTypeList  ======>");
	}
	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		petitionSource = ((PetitionSource) event.getObject());
		addButtonFlag = false;
		log.info("<===Ends PetitionSourceBean.onRowSelect ========>");
	}
	
	public void getrecordById() {
		log.info("Inside getrecordById() ");
		BaseDTO baseDTO = null;
		try {
			String url = serverURL+ appPreference.getOperationApiUrl() + "/petitionsource/get/"
					+ petitionSource.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				petitionSource = mapper.readValue(jsonResponse, new TypeReference<PetitionSource>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While getrecordById() :: ", e);
		}
	}


}
