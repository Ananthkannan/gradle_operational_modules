package in.gov.cooptex.operation.rest.ui.master;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.operation.dto.ProductGroupMasterDTO;
import in.gov.cooptex.operation.dto.ProductVarietyMasterDTO;
import in.gov.cooptex.operation.dto.ProductVariryMastersearchDTO;
import in.gov.cooptex.operation.dto.ProductVarityMasterResponse;
import in.gov.cooptex.operation.dto.YarnTypeMasterDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Abhishek
 *
 */
@Log4j2
@Service("productVarietyMasterBean")
@Scope("session")
public class ProductVarietyMasterBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	String code;
	@Getter
	@Setter
	String name;
	@Getter
	@Setter
	String lname;

	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	String statusValue;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	/** for client server connection */
	RestTemplate restTemplate;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	ProductCategoryDTO productCategoryDto;

	@Getter
	@Setter
	ProductGroupMasterDTO productGroupDto;

	@Getter
	@Setter
	List<ProductCategoryDTO> productCategoryDtoList;

	@Getter
	@Setter
	List<ProductGroupMasterDTO> productGroupDtoList;

	@Getter
	@Setter
	ProductVarietyMasterDTO productVarietyMasterDTO = new ProductVarietyMasterDTO();

	@Getter
	@Setter
	YarnTypeMasterDTO yarnTypeMasterDto;
	@Getter
	@Setter
	List<YarnTypeMasterDTO> yarnTypeMasterDtoList;
	@Getter
	@Setter
	List<ProductVarietyMasterDTO> productVarietyMasterDTOList;
	@Getter
	@Setter
	List<ProductCategoryDTO> productCatSearchList;

	@Getter
	@Setter
	List<ProductGroupMasterDTO> productGroupSearchList;
	@Getter
	@Setter
	boolean visible;

	String jsonResponse;
	ObjectMapper mapper = new ObjectMapper();

	int tol = 0;

	@Getter
	@Setter
	String percentTole;

	// new code properties
	@Getter
	@Setter
	LazyDataModel<ProductVarietyMasterDTO> productVarietyMasterLazyDTOList;

	@Getter
	@Setter
	LazyDataModel<ProductVarityMasterResponse> productVarietyResponseLazyDTOList;

	@Getter
	@Setter
	List<ProductVarityMasterResponse> productVarietyResponseDTOList;
	
	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
	
	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();

	@Getter
	@Setter
	boolean addButtonFlag = false;
	
	@Getter
	@Setter
	Integer planSize;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	BaseDTO baseDTO = new BaseDTO();

	@Getter
	@Setter
	ProductVarityMasterResponse productVarityMasterResponse = new ProductVarityMasterResponse();

	@Getter
	@Setter
	List<UomMaster> uomMasterList;

	@Getter
	@Setter
	UomMaster uomMaster;

	@Getter
	@Setter
	UomMaster lenghtUomMaster;

	@Getter
	@Setter
	UomMaster widthUomMaster;

	@Getter
	@Setter
	String pageHead = "Create";

	// private final Integer PLEASE_SELECT_ANY_ONE_VARIETY = 1803;

	private final Integer PRODUCT_VARIETY_VARIETY_MASTER_ADD = 1812;

	private final String LIST_PRODUCT_VARITY_MASTER = "/pages/masters/listProductVariety.xhtml?faces-redirect=true;";
	private final String CREATE_PRODUCT_VARITY_MASTER = "/pages/masters/createProductVariety.xhtml?faces-redirect=true;";
	private final String VIEW_PRODUCT_VARITY_MASTER = "/pages/masters/viewProductVariety.xhtml?faces-redirect=true;";

	ProductVarietyMasterBean() {
		productGroupDto = new ProductGroupMasterDTO();
		productCategoryDto = new ProductCategoryDTO();
		yarnTypeMasterDto = new YarnTypeMasterDTO();
		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		log.info("---ProductVarietyMasterBean Constructor---");
		loadValues();
	}

	private void loadValues() {
		log.info("---loadValues----");
		try {			
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String getVarityList() {
		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		log.info("<--- Inside getvarityMasterList() ---> ");
		loadRetailProductvarityDTOLazy();
		log.info("<--- Inside getvarityMasterList()  completed ---> ");
		return LIST_PRODUCT_VARITY_MASTER;
	}

	/**
	 * @return
	 * @throws IOException
	 */
	/**
	 * @return
	 * @throws IOException
	 */
	public String submitForm() throws IOException {

		log.info("--submitForm---");
		if (action.equalsIgnoreCase("ADD")) {
			log.info("<<======= Action::" + action);
			visible = false;
			if (productVarietyMasterDTO.getCode().trim().length() != 4) {
				// log.info(code.length());
				errorMap.notify(ErrorDescription.PRODUCT_CODE_VALID_LENGTH.getCode());
				log.info("code should be 4 characters");
				return null;
			}

			if (productVarietyMasterDTO.getHsnCode().trim().length() < 4 || productVarietyMasterDTO.getHsnCode().trim().length() > 16) {
				// log.info(code.length());
				Util.addWarn("Hsn Code should be Minimum 4 Maximum 16 characters only");
				log.info("Hsn Code should be Minimum 4 Maximum 16 characters only");
				return null;
			}
			
			if (productVarietyMasterDTO.getName().trim().length() < 5) {
				log.info(productVarietyMasterDTO.getName().trim().length());
				errorMap.notify(ErrorDescription.PRODUCT_NAME_VALID_LENGTH.getCode());

				log.info("name should be 5 characters");
				return null;
			}

			if (productVarietyMasterDTO.getLname().trim().length() < 5) {
				log.info(productVarietyMasterDTO.getLname().trim().length());

				errorMap.notify(ErrorDescription.PRODUCT_LOCAL_NAME_VALID_LENGTH.getCode());
				log.info(" local name should be 5 characters");
				return null;
			}

			/**
			 * this code for check the tolerance value when perform add operation
			 *
			 */
			/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
			percentTole=commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
			// log.info("===kkkk==" + percentTole);
			tol = Integer.parseInt(percentTole);
			log.info("lot percent :" + tol);

			if (productVarietyMasterDTO.getLengthActual() != null && productVarietyMasterDTO.getLuomMaster() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_LENGTH_UNIT_OF_MEASUREMENT.getCode());
				return null;
			}

			if ((productVarietyMasterDTO.getLengthActual() != null
					|| productVarietyMasterDTO.getLengthTolerance() != null)
					&& productVarietyMasterDTO.getLengthActual() != null) {

				if (productVarietyMasterDTO.getLengthActual() == 0) {
					errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_LENGTH.getCode());
					return null;
				}

				log.info("lot percent :" + tol);
				double lengthPeccent = (productVarietyMasterDTO.getLengthActual() * tol) / 100d;

				log.info("lengthPeccent--------" + lengthPeccent);
				if (productVarietyMasterDTO.getLengthTolerance() != null
						&& lengthPeccent < productVarietyMasterDTO.getLengthTolerance()) {

					log.info("Length of Product Tolerance Value should be 25 % of actual value");

					errorMap.notify(ErrorDescription.CALCULATE_VARIETY_LENGTH_TOLARANCE_VALUE.getCode());

					return null;

				}

			}

			if (productVarietyMasterDTO.getWidthActual() != null && productVarietyMasterDTO.getWuomMaster() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_WIDTH_UNIT_OF_MEASUREMENT.getCode());
				return null;
			}

			if ((productVarietyMasterDTO.getWidthActual() != null
					|| productVarietyMasterDTO.getWidthTolerance() != null)
					&& productVarietyMasterDTO.getWidthActual() != null) {

				if (productVarietyMasterDTO.getWidthActual() == 0) {
					errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_WIDTH.getCode());
					return null;
				}

				double widhtPercent = (productVarietyMasterDTO.getWidthActual() * tol) / 100;
				log.info("widhtPercent--------" + widhtPercent);
				if (productVarietyMasterDTO.getWidthTolerance() != null
						&& widhtPercent < productVarietyMasterDTO.getWidthTolerance()) {

					log.info("Width of Product Tolerance Value should be 25 % of actual value");

					errorMap.notify(ErrorDescription.CALCULATE_VARIETY_WIDTH_TOLARANCE_VALUE.getCode());

					return null;

				}

			}

			if ((productVarietyMasterDTO.getPicksPerInchActual() != null
					|| productVarietyMasterDTO.getPicksPerInchTolerance() != null)
					&& productVarietyMasterDTO.getPicksPerInchActual() != null
					&& productVarietyMasterDTO.getPicksPerInchActual() != null) {

				if (productVarietyMasterDTO.getPicksPerInchActual() == 0) {
					errorMap.notify(ErrorDescription.PRODUCT_VARIETY_REEDS_PICK_ACTUAL.getCode());
					return null;
				}

				double pickPercentage = (productVarietyMasterDTO.getPicksPerInchActual() * tol) / 100;
				log.info("pickPercentage--------" + pickPercentage);
				if (productVarietyMasterDTO.getPicksPerInchTolerance() != null
						&& pickPercentage < productVarietyMasterDTO.getPicksPerInchTolerance()) {

					log.info("Reed Pick per Inch Tolerance Value should be 25 % of actual value");

					errorMap.notify(ErrorDescription.CALCULATE_VARIETY_PICK_PER_TOLARANCE_VALUE.getCode());
					return null;

				}

			}

			if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
					&& productVarietyMasterDTO.getEndspPerInchActual() != null) {

				if (productVarietyMasterDTO.getEndsPerInchTolerance() == 0) {
					errorMap.notify(ErrorDescription.PRODUCT_VARIETY_REEDS_ENDS_ACTUAL.getCode());
					return null;
				}

				double endPercent = (productVarietyMasterDTO.getEndspPerInchActual() * tol) / 100;
				log.info("endPercent--------" + endPercent);
				if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
						&& endPercent < productVarietyMasterDTO.getEndsPerInchTolerance()) {

					log.info("Reed Ends per Inch Tolerance Value should be 25 % of actual value");

					errorMap.notify(ErrorDescription.CALCULATE_VARIETY_END_PER_TOLARANCE_VALUE.getCode());
					return null;

				}

			}

			if (productVarietyMasterDTO.getWarpyarnTypeMasterDTO() != null
					&& productVarietyMasterDTO.getWarpYarnWeight() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ENTER_WARP_YARN_WEIGHT.getCode());
				return null;
			}

			if (productVarietyMasterDTO.getWarpYarnWeight() != null
					&& productVarietyMasterDTO.getWarpyarnTypeMasterDTO() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ENTER_WARP_YARN_TYPE.getCode());
				return null;
			}

			if (productVarietyMasterDTO.getWeftyarnTypeMasterDTO() != null
					&& productVarietyMasterDTO.getWeftYarnWeight() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ENTER_WEFT_YARN_WEIGHT.getCode());
				return null;
			}

			if (productVarietyMasterDTO.getWeftYarnWeight() != null
					&& productVarietyMasterDTO.getWeftyarnTypeMasterDTO() == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ENTER_WEFT_YARN_TYPE.getCode());
				return null;
			}

			UserDTO userDto = new UserDTO();
			userDto.setId(loginBean.getUserDetailSession().getId());

			productVarietyMasterDTO.setCreatedBy(userDto);
			productVarietyMasterDTO.setCreatedDate(new Date());
			productVarietyMasterDTO.setCode(productVarietyMasterDTO.getCode().trim());
			productVarietyMasterDTO.setName(productVarietyMasterDTO.getName().trim());
			productVarietyMasterDTO.setLname(productVarietyMasterDTO.getLname().trim());
			productVarietyMasterDTO.setProductCategoryDTO(productCategoryDto);
			productVarietyMasterDTO.setProductGroupMasterDTO(productGroupDto);

			// String url = serverURL + appPreference.getOperationApiUrl()
			// +"/productvariety/create";
			String url = serverURL + "/productvariety/create";
			baseDTO = httpService.post(url, productVarietyMasterDTO);

			log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(PRODUCT_VARIETY_VARIETY_MASTER_ADD);
					log.info("Product Variety Master has been Added");
				} else {
					log.error("Status code:" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}

		} else if (action.equalsIgnoreCase("EDIT")) {
			log.info("action EDIT");
			visible = true;

			if (productVarietyMasterDTO.getHsnCode().trim().length() < 4 || productVarietyMasterDTO.getHsnCode().trim().length() > 16) {
				// log.info(code.length());
				Util.addWarn("Hsn Code should be Minimum 4 Maximum 16 characters only");
				log.info("Hsn Code should be Minimum 4 Maximum 16 characters only");
				return null;
			}
			
			if (productVarietyMasterDTO.getCode().trim().length() < 4) {
				// log.info(code.length());
				errorMap.notify(ErrorDescription.PRODUCT_CODE_VALID_LENGTH.getCode());
				log.info("code should be 4 characters");
				return null;
			}

			if (productVarietyMasterDTO.getName().trim().length() < 5) {
				log.info(productVarietyMasterDTO.getName().trim().length());
				errorMap.notify(ErrorDescription.PRODUCT_NAME_VALID_LENGTH.getCode());
				log.info("name should be 5 characters");
				return null;
			}

			if (productVarietyMasterDTO.getLname().trim().length() < 5) {
				log.info(productVarietyMasterDTO.getLname().trim().length());
				errorMap.notify(ErrorDescription.PRODUCT_LOCAL_NAME_VALID_LENGTH.getCode());
				log.info(" local name should be 5 characters");
				return null;
			}

			/**
			 * this code for check the tolerance value when perform update operation
			 *
			 */
			/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
			percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
			tol = Integer.parseInt(percentTole);
			log.info("lot percent :" + tol);

			if (productVarietyMasterDTO.getLengthActual() != null
					|| productVarietyMasterDTO.getLengthTolerance() != null) {

				log.info("lot percent :" + tol);
				double lengthPeccent = (productVarietyMasterDTO.getLengthActual() * tol) / 100;

				log.info("d--------" + lengthPeccent);
				if (productVarietyMasterDTO.getLengthTolerance() != null
						&& lengthPeccent < productVarietyMasterDTO.getLengthTolerance()) {
					log.info("Length of Product Tolerance Value should be 25 % of actual value");
					errorMap.notify(ErrorDescription.CALCULATE_LENGTH_TOLARANCE_VALUE.getCode());
					/*
					 * String msg = AppUtil.getFormattedMessage(ErrorCodeDescription.
					 * getDescription( ErrorCodeDescription.CALCULATE_LENGTH_TOLARANCE_VALUE.
					 * getErrorCode(), languageBean.getLocaleCode()), new Object[] { percentTole });
					 * 
					 * Util.addError(msg);
					 */
					return null;

				}

			}
			if (productVarietyMasterDTO.getWidthActual() != null
					|| productVarietyMasterDTO.getWidthTolerance() != null) {

				double widhtPercent = (productVarietyMasterDTO.getWidthActual() * tol) / 100;
				if (productVarietyMasterDTO.getWidthTolerance() != null
						&& widhtPercent < productVarietyMasterDTO.getWidthTolerance()) {

					log.info("Width of Product Tolerance Value should be 25 % of actual value");
					errorMap.notify(ErrorDescription.CALCULATE_WIDTH_TOLARANCE_VALUE.getCode());
					/*
					 * String msg = AppUtil.getFormattedMessage(ErrorCodeDescription.
					 * getDescription( ErrorCodeDescription.CALCULATE_WIDTH_TOLARANCE_VALUE.
					 * getErrorCode(), languageBean.getLocaleCode()), new Object[] { percentTole });
					 * Util.addError(msg);
					 */

					return null;

				}

			}

			if (productVarietyMasterDTO.getPicksPerInchActual() != null
					|| productVarietyMasterDTO.getPicksPerInchTolerance() != null) {

				double pickPercentage = (productVarietyMasterDTO.getPicksPerInchActual() * tol) / 100;

				if (productVarietyMasterDTO.getPicksPerInchTolerance() != null
						&& pickPercentage < productVarietyMasterDTO.getPicksPerInchTolerance()) {

					log.info("Reed Pick per Inch Tolerance Value should be 25 % of actual value");
					errorMap.notify(ErrorDescription.CALCULATE_PICK_PER_TOLARANCE_VALUE.getCode());
					/*
					 * String msg = AppUtil.getFormattedMessage(ErrorCodeDescription.
					 * getDescription( ErrorCodeDescription.CALCULATE_PICK_PER_TOLARANCE_VALUE.
					 * getErrorCode(), languageBean.getLocaleCode()), new Object[] { percentTole });
					 * Util.addError(msg);
					 */
					return null;

				}

			}

			if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
					&& productVarietyMasterDTO.getEndspPerInchActual() != null) {

				double endPercent = (productVarietyMasterDTO.getEndspPerInchActual() * tol) / 100;

				if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
						&& endPercent < productVarietyMasterDTO.getEndsPerInchTolerance()) {

					log.info("Reed Ends per Inch Tolerance Value should be 25 % of actual value");
					errorMap.notify(ErrorDescription.CALCULATE_END_PER_TOLARANCE_VALUE.getCode());

					/*
					 * String msg = AppUtil.getFormattedMessage(ErrorCodeDescription.
					 * getDescription( ErrorCodeDescription.CALCULATE_END_PER_TOLARANCE_VALUE.
					 * getErrorCode(), languageBean.getLocaleCode()), new Object[] { percentTole });
					 * Util.addError(msg);
					 */
					return null;

				}

			}

			UserDTO userDto = new UserDTO();
			userDto.setId(loginBean.getUserDetailSession().getId());
			/*
			 * productVarietyMasterDTO.setModifiedBy(userDto);
			 * productVarietyMasterDTO.setModifiedDate(new Date());
			 */
			productVarietyMasterDTO.setProductCategoryDTO(productCategoryDto);
			productVarietyMasterDTO.setProductGroupMasterDTO(productGroupDto);
			productVarietyMasterDTO.setCode(productVarietyMasterDTO.getCode().trim());
			productVarietyMasterDTO.setName(productVarietyMasterDTO.getName().trim());
			productVarietyMasterDTO.setLname(productVarietyMasterDTO.getLname().trim());
			RestTemplate restTemplate = new RestTemplate();

			log.info("<<===== Active Status:: " + productVarietyMasterDTO.getStatusValue() + "   "
					+ productVarietyMasterDTO.getSelectable());

			String url = serverURL + "/productvariety/update";
			baseDTO = httpService.post(url, productVarietyMasterDTO);
			log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.PRODUCT_VARIETY_VARIETY_MASTER_EDIT.getCode());
					log.info("Product Variety Master has been Updated");
				} else {
					log.error("Status code:" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
			/*
			 * HttpEntity<ProductVarietyMasterDTO> requestEntity = new
			 * HttpEntity<ProductVarietyMasterDTO>( productVarietyMasterDTO,
			 * loginBean.headers); HttpEntity<BaseDTO> responseEntity =
			 * restTemplate.exchange(url, HttpMethod.POST, requestEntity, BaseDTO.class);
			 * BaseDTO baseDto = responseEntity.getBody(); log.info("BaseDTO " +
			 * responseEntity.getBody()); if (baseDto != null) { if (baseDto.getStatusCode()
			 * == 0) { Util.addInfo(InfoCodeDescription.getDescription(
			 * InfoCodeDescription.PRODUCT_VARIETY_MASTER_UPDATE.getInfoCode(),
			 * languageBean.getLocaleCode())); log.info(
			 * "Product Variety Master has been Added");
			 * FacesContext.getCurrentInstance().getExternalContext().getFlash()
			 * .setKeepMessages(true); } else { log.info( "Product Variety Master fail");
			 * String message =
			 * ErrorCodeDescription.getDescription(baseDto.getStatusCode()); Util.addError(
			 * ErrorCodeDescription.getDescription(baseDto.getStatusCode(),
			 * languageBean.getLocaleCode())); log.error("Status code:" +
			 * baseDto.getStatusCode() + " Error Message: " + message);
			 * FacesContext.getCurrentInstance().getExternalContext().getFlash()
			 * .setKeepMessages(true); return null; } }
			 */
			productCategoryDto = null;
			productGroupDto = null;
		}

		productCategoryDto = new ProductCategoryDTO();
		productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();
		code = null;
		name = null;
		lname = null;
		loadRetailProductvarityDTOLazy();
		// getProductVarietyList();
		return LIST_PRODUCT_VARITY_MASTER;
	}

	public String getProductVarietyList() {
		log.info("-getProductVarietyList-----");
		productVarietyMasterDTOList = new ArrayList<ProductVarietyMasterDTO>();
		WrapperDTO WrapperDto = new WrapperDTO();
		RestTemplate restTemplate = new RestTemplate();
		String url = serverURL + "/productvariety/getall";
		try {

			HttpEntity<WrapperDTO> responseEntity = restTemplate.exchange(url, HttpMethod.GET, loginBean.requestEntity,
					WrapperDTO.class);
			WrapperDto = responseEntity.getBody();
			if (WrapperDto != null) {
				if (WrapperDto.getProductVarietyMasterDtoCollection() != null) {

					productVarietyMasterDTOList = (List<ProductVarietyMasterDTO>) WrapperDto
							.getProductVarietyMasterDtoCollection();

					if (productVarietyMasterDTOList == null || productVarietyMasterDTOList.size() < 0) {

						productVarietyMasterDTOList = new ArrayList<ProductVarietyMasterDTO>();
					}
					log.info("No of productVarietyMasterDTOList :" + productVarietyMasterDTOList.size());
				} else {
					productVarietyMasterDTOList = new ArrayList<ProductVarietyMasterDTO>();
					String msg = WrapperDto.getErrorDescription();
					log.error("Status code:" + WrapperDto.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		code = null;
		name = null;
		lname = null;
		productCategoryDto = new ProductCategoryDTO();
		productGroupDto = new ProductGroupMasterDTO();
		productVarietyMasterDTO = new ProductVarietyMasterDTO();

		getCategoryMasterList();

		return "/pages/master/productVarietyMasterList.xhtml?faces-redirect=true;";
	}

	public String cancelProductVariety() {
		log.info("--cancel ProductVarietyMaster---");

		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		productCategoryDto = new ProductCategoryDTO();
		productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();

		// getProductVarietyList();
		return "/pages/master/productVarietyMasterList.xhtml?faces-redirect=true;";
	}

	public void getCategoryGroupMasterList() {
		log.info("===getCategoryGroupMasterList===   Start");

		try {
			productCategoryGroupList.clear();
			String url = serverURL + "/categoryGroup/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryGroupList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryGroup>>() {
			});
			log.info("<<==== getCategoryGroupMasterList List Sise::: " + productCategoryGroupList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getCategoryGroupMasterList" + e);
		}
	}
	
	public void getCategoryMasterList() {
		log.info("===getCategoryMasterList  Start ===>>  selected category group:: "+selectedProductCategoryGroup.getId());

		try {
			String url = serverURL + "/category/getCategoryByCategoryGroup/"+selectedProductCategoryGroup.getId();
			log.info("URL:: "+url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryDtoList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryDTO>>() {
			});
			log.info("<<==== getCategoryMasterList List Sise::: " + productCategoryDtoList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}

		/*
		 * productCategoryDtoList = new ArrayList<ProductCategoryDTO>(); WrapperDTO
		 * wrapperDto = new WrapperDTO(); RestTemplate restTemplate = new
		 * RestTemplate(); try { String url = serverURL + "/category/getAll";
		 * HttpEntity<String> entity = new HttpEntity<String>("parameters",
		 * loginBean.headers); HttpEntity<BaseDTO> response = restTemplate.exchange(url,
		 * HttpMethod.GET, entity, BaseDTO.class); log.info("<----Url--->" + url); //
		 * wrapperDto = response.getBody(); log.info("Wrapper Dto :" +
		 * response.getBody()); productCategoryDtoList =
		 * (List<ProductCategoryDTO>)response.getBody().getResponseContents(); if
		 * (wrapperDto != null) { if (wrapperDto.getProductCategoryDTOCollections() !=
		 * null) { productCategoryDtoList = (List<ProductCategoryDTO>)
		 * wrapperDto.getProductCategoryDTOCollections();
		 * 
		 * log.info("==productCategoryDtoList==" + productCategoryDtoList.size()); if
		 * (productCategoryDtoList == null || productCategoryDtoList.size() < 0) {
		 * productCategoryDtoList = new ArrayList<ProductCategoryDTO>(); }
		 * 
		 * } else { log.info("category code not found"); productCategoryDtoList = new
		 * ArrayList<ProductCategoryDTO>(); } }
		 * log.info("---getCategoryMasterList--   End"+productCategoryDtoList.size()); }
		 * catch (Exception e) { e.printStackTrace(); }
		 */
	}

	public String showForm() throws IOException {
		log.info("inside productVariety  showForm ");
		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		if (action.equalsIgnoreCase("ADD")) {
			log.info("action : ADD");
			visible = false;
			productVarietyMasterDTO = new ProductVarietyMasterDTO();

		} else if (action.equalsIgnoreCase("EDIT")) {
			visible = true;
			log.info("action : Edit");

			if (productVarietyMasterDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			productCategoryDto = productVarietyMasterDTO.getProductGroupMasterDTO().getProductCategoryDTO();
			productGroupDto = productVarietyMasterDTO.getProductGroupMasterDTO();

//			getCategoryMasterList();
			onProductCategoryChange();
			getYarnTypeList();

		} else if (action.equalsIgnoreCase("VIEW")) {
			log.info("action :VIEW");
			visible = false;
			if (productVarietyMasterDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}

			getCategoryMasterList();
			productCategoryDto = productVarietyMasterDTO.getProductGroupMasterDTO().getProductCategoryDTO();
			onProductCategoryChange();
			productGroupDto = productVarietyMasterDTO.getProductGroupMasterDTO();
			getYarnTypeList();
		}
		/*
		 * getCategoryMasterList(); productCategoryDto =
		 * productVarietyMasterDTO.getProductGroupMasterDTO(). getProductCategoryDTO();
		 * if (productCategoryDto != null) { onProductCategoryChange(); }
		 * productGroupDto = productVarietyMasterDTO.getProductGroupMasterDTO();
		 */
		getYarnTypeList();
		return "/pages/master/productVarietyMaster.xhtml?faces-redirect=true;";
	}

	public String showEditForm() {
		visible = true;
		log.info("action :" + action);
		if (action.equals("EDIT"))
			pageHead = "Edit";
		else
			pageHead = "View";

		if (productVarityMasterResponse == null || productVarityMasterResponse.getVarityId() == null) {
			/*
			 * Util.addError(ErrorCodeDescription.getDescription(
			 * ErrorCodeDescription.SELECT_ANY_PRODUCT_VARIETY.getErrorCode(),
			 * languageBean.getLocaleCode()));
			 */
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			log.info("<<====  variety Id:: " + productVarityMasterResponse.getVarityId());
			productVarietyMasterDTO.setId(productVarityMasterResponse.getVarityId());
			log.info("<<=====   selected Dto == " + productVarietyMasterDTO.getId());
			getProductCategoryGroupList();
//			getCategoryMasterList();
			getUOM();
			getYarnTypeList();
			BaseDTO baseDTO = new BaseDTO();
			RestTemplate restTemplate = new RestTemplate();
			String url = serverURL + "/productvariety/getById";
			log.info("<<==  URL:: " + url);
			log.info("Headers:: " + loginBean.headers);
			baseDTO = httpService.post(url, productVarietyMasterDTO);
			log.info("BaseDTO Dto :" + baseDTO.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productVarietyMasterDTO = mapper.readValue(jsonResponse, ProductVarietyMasterDTO.class);
			productCategoryDto = productVarietyMasterDTO.getProductGroupMasterDTO().getProductCategoryDTO();
			selectedProductCategoryGroup = productCategoryDto.getProductCategoryGroup();
			getCategoryGroupMasterList();
			getCategoryMasterList();
			onProductCategoryChange();
			productGroupDto = productVarietyMasterDTO.getProductGroupMasterDTO();
			log.info("<<=== Group ::" + productGroupDto);
			log.info("<<=== Category:: " + productCategoryDto);
			log.info("<<====== CAT GROUP:: "+selectedProductCategoryGroup.getId());
			/*
			 * log.info("<<=====  uOM:: "
			 * +productVarietyMasterDTO.getUomMaster().getName()); log.info(
			 * "<<=====  LuOM:: " +productVarietyMasterDTO.getLuomMaster().getName());
			 * log.info( "<<=====  WuOM:: "
			 * +productVarietyMasterDTO.getWuomMaster().getName());
			 */

			// getCategoryMasterList();
			// onProductCategoryChange();

		} catch (Exception e) {
			log.error("<<====   ERROR::: " + e);
		}
		return CREATE_PRODUCT_VARITY_MASTER;
	}
	
	
	public String showViewForm() {
		visible = true;
		log.info("action :" + action);
		if (action.equals("EDIT"))
			pageHead = "Edit";
		else
			pageHead = "View";

		if (productVarityMasterResponse == null || productVarityMasterResponse.getVarityId() == null) {
			/*
			 * Util.addError(ErrorCodeDescription.getDescription(
			 * ErrorCodeDescription.SELECT_ANY_PRODUCT_VARIETY.getErrorCode(),
			 * languageBean.getLocaleCode()));
			 */
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			productVarietyMasterDTO = new ProductVarietyMasterDTO();
			log.info("<<====  variety Id:: " + productVarityMasterResponse.getVarityId());
			productVarietyMasterDTO.setId(productVarityMasterResponse.getVarityId());
			log.info("<<=====   selected Dto == " + productVarietyMasterDTO.getId());
			getCategoryMasterList();
			getUOM();
			getYarnTypeList();
			BaseDTO baseDTO = new BaseDTO();
			RestTemplate restTemplate = new RestTemplate();
			String url = serverURL + "/productvariety/getById";
			log.info("<<==  URL:: " + url);
			log.info("Headers:: " + loginBean.headers);
			baseDTO = httpService.post(url, productVarietyMasterDTO);
			log.info("BaseDTO Dto :" + baseDTO.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productVarietyMasterDTO = mapper.readValue(jsonResponse, ProductVarietyMasterDTO.class);
			productCategoryDto = productVarietyMasterDTO.getProductGroupMasterDTO().getProductCategoryDTO();
			onProductCategoryChange();
			productGroupDto = productVarietyMasterDTO.getProductGroupMasterDTO();
			log.info("<<=== Group ::" + productGroupDto);
			log.info("<<=== Category:: " + productCategoryDto);
			/*
			 * log.info("<<=====  uOM:: "
			 * +productVarietyMasterDTO.getUomMaster().getName()); log.info(
			 * "<<=====  LuOM:: " +productVarietyMasterDTO.getLuomMaster().getName());
			 * log.info( "<<=====  WuOM:: "
			 * +productVarietyMasterDTO.getWuomMaster().getName());
			 */

			// getCategoryMasterList();
			// onProductCategoryChange();

		} catch (Exception e) {
			log.error("<<====   ERROR::: " + e);
		}
		return VIEW_PRODUCT_VARITY_MASTER;
	}

	public void clear() {
		log.info("--productVarietycMaster   clear--");
		productGroupDto = new ProductGroupMasterDTO();
		productCategoryDto = new ProductCategoryDTO();
		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();
		statusValue = null;

		visible = false;
		code = null;
		name = null;
		lname = null;

		// getProductVarietyList();

	}

	public void onProductCategoryChange() {
		log.info("ProductVarietyMasterBean onProductCategoryChange------#Start");
		WrapperDTO wrapperDto = new WrapperDTO();
		RestTemplate restTemplate = new RestTemplate();
		try {

			String url = serverURL + "/group/getGroupByCategory";
			if (productCategoryDto != null) {
				HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDto,
						loginBean.headers);

				HttpEntity<WrapperDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
						WrapperDTO.class);

				wrapperDto = responseEntity.getBody();
				log.info("WrapperDTO :" + responseEntity.getBody());
			}
			if (wrapperDto != null) {
				if (wrapperDto.getProductGroupDTOCollections() != null) {
					productGroupDtoList = (List<ProductGroupMasterDTO>) wrapperDto.getProductGroupDTOCollections();
					log.info("No of productGroupDtoList :" + productGroupDtoList.size());
					if (productGroupDtoList == null || productGroupDtoList.size() < 0) {
						productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();
					}
				} else {
					log.info("product category not found");
					productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		productGroupDto = new ProductGroupMasterDTO();

	}

	public void getYarnTypeList() {
		log.info("--ProductVarietyMasterBean  getYarnTypeList---");

		yarnTypeMasterDtoList = new ArrayList<YarnTypeMasterDTO>();
		WrapperDTO wrapperDto = new WrapperDTO();
		RestTemplate restTemplate = new RestTemplate();
		try {
			String url = serverURL + "/Yarntypemaster/getall";

			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			HttpEntity<WrapperDTO> response = restTemplate.exchange(url, HttpMethod.GET, entity, WrapperDTO.class);
			log.info("<----Url--->" + url);
			wrapperDto = response.getBody();
			log.info("Wrapper Dto :" + response.getBody());
			if (wrapperDto != null) {
				if (wrapperDto.getYarnTypeMasterDtoCollection() != null) {
					yarnTypeMasterDtoList = (List<YarnTypeMasterDTO>) wrapperDto.getYarnTypeMasterDtoCollection();
					log.info("No of yarnTypeMasterDtoList :" + yarnTypeMasterDtoList.size());
				} else {
					yarnTypeMasterDtoList = new ArrayList<YarnTypeMasterDTO>();
				}
			}
			log.info("---getYarnTypeList--   End");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * This is for product Variety master search and get service
	 */
	public void loadRetailProductvarityDTOLazy() {
		log.info("Lazy load == product varity master  ==>>");
		productVarietyResponseLazyDTOList = new LazyDataModel<ProductVarityMasterResponse>() {

			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<ProductVarityMasterResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				List<ProductVarityMasterResponse> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarityMasterResponse>>() {
								});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(ProductVarityMasterResponse res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getVarityId() : null;
			}

			@Override
			public ProductVarityMasterResponse getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<ProductVarityMasterResponse> responseList = (List<ProductVarityMasterResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (ProductVarityMasterResponse res : responseList) {
					if (res.getVarityId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		productVarityMasterResponse = new ProductVarityMasterResponse();

		BaseDTO baseDTO = new BaseDTO();

		ProductVariryMastersearchDTO request = new ProductVariryMastersearchDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("catageoryCodeOrName")) {
				request.setCatageoryCodeOrName(value);
			} else if (key.equals("groupCodeOrName")) {
				request.setGroupCodeOrName(value);
			} else if (key.equals("varityCodeOrName")) {
				request.setVarityCodeOrName(value);
			} else if (key.equals("varityCodeOrNameInTamil")) {
				request.setVarityCodeOrNameInTamil(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equals("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {
			// String url = serverURL + appPreference.getOperationApiUrl() +
			// "/productvariety/searchvaritys";
			String url = serverURL + "/productvariety/searchvaritys";
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Product Varity Master onRowSelect method started");
		productVarityMasterResponse = ((ProductVarityMasterResponse) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("Product Varity Master onPageLoad method started");
		addButtonFlag = false;
	}

	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (productVarityMasterResponse == null) {
			log.info("<---if(productVarityMasterResponse == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			log.info("Please Select atleast One product variety");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return ;
		} else {
			log.info("<---else(productVarityMasterResponse == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called with varity Id---> " + productVarityMasterResponse.getVarityId());
		/*
		 * String url = serverURL + appPreference.getOperationApiUrl() +
		 * "/productvariety/delete/" + productVarityMasterResponse.getVarityId();
		 */
		String url = serverURL + "/productvariety/delete/" + productVarityMasterResponse.getVarityId();

		BaseDTO baseDTO = httpService.delete(url);

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("<--- product variety has been Deleted Successfully ---> ");
				errorMap.notify(ErrorDescription.INFO_PRODUCT_VARIETY_MASTER_DELETED.getCode());
				getVarityList();
			} else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
		} else {
			log.info("<--- Internal Server Error ---> ");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_PRODUCT_VARITY_MASTER;
	}

	public String varietyAdd() throws Exception {
		productCategoryDto = new ProductCategoryDTO();
		productGroupDto = new ProductGroupMasterDTO();
		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		getCategoryGroupMasterList();		
//		getCategoryMasterList();
		getYarnTypeList();
		getUOM();
		pageHead = "Create";
		log.info("Inside get UOMS" + productVarietyMasterDTO);
		return CREATE_PRODUCT_VARITY_MASTER;
	}

	@SuppressWarnings("unchecked")
	public void getUOM() {
		log.info("Inside get UOMS");
		// String url = serverURL + appPreference.getOperationApiUrl() +
		// "/productvariety/getuoms";
		String url = serverURL + "/productvariety/getuoms";

		try {
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					uomMasterList = (List<UomMaster>) mapper.readValue(jsonValue, new TypeReference<List<UomMaster>>() {
					});
					if (uomMasterList != null) {
						log.info("RetailProductionPlanList with Size of : " + uomMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" RetailProductionPlanList is Empty ");
					}
				}

			}

		} catch (Exception e) {
			log.error("exception in getUOM", e);
		}

	}

	public String cancelProductVarietyAdd() {
		log.info("--cancel ProductVarietyMaster ADD ---");

		productCategoryDto = new ProductCategoryDTO();
		productGroupDto = new ProductGroupMasterDTO();
		productVarietyMasterDTO = new ProductVarietyMasterDTO();
		selectedProductCategoryGroup = new ProductCategoryGroup();
		loadRetailProductvarityDTOLazy();
		return LIST_PRODUCT_VARITY_MASTER;
	}

	public String onChangeproductVarietyLengthTolerance() throws Exception {
		log.info("lot percentssss :", productVarietyMasterDTO);

		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		tol = Integer.parseInt(percentTole);
		log.info("lot percent :" + tol);
		if ((productVarietyMasterDTO.getLengthActual() != null || productVarietyMasterDTO.getLengthTolerance() != null)
				&& productVarietyMasterDTO.getLengthActual() != null) {

			if (productVarietyMasterDTO.getLengthActual() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_LENGTH.getCode());
				return null;
			}

			log.info("lot percentsss :" + tol);
			double lengthPeccent = (productVarietyMasterDTO.getLengthActual() * tol) / 100d;

			log.info("lengthPeccent--------" + lengthPeccent);
			if (productVarietyMasterDTO.getLengthTolerance() != null
					&& lengthPeccent < productVarietyMasterDTO.getLengthTolerance()) {

				log.info("Length of Product Tolerance Value should be 25 % of actual value");

				errorMap.notify(ErrorDescription.CALCULATE_VARIETY_LENGTH_TOLARANCE_VALUE.getCode());

				return null;

			}

		}

		return null;

	}

	public String onChangeproductVarietyWidthTolerance() throws Exception {
		log.info("lot percentssss :", productVarietyMasterDTO);

		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		tol = Integer.parseInt(percentTole);
		log.info("lot percent :" + tol);
		if ((productVarietyMasterDTO.getWidthActual() != null || productVarietyMasterDTO.getWidthTolerance() != null)
				&& productVarietyMasterDTO.getWidthActual() != null) {

			if (productVarietyMasterDTO.getWidthActual() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_ACTUAL_WIDTH.getCode());
				return null;
			}

			double widhtPercent = (productVarietyMasterDTO.getWidthActual() * tol) / 100;
			log.info("widhtPercent--------" + widhtPercent);
			if (productVarietyMasterDTO.getWidthTolerance() != null
					&& widhtPercent < productVarietyMasterDTO.getWidthTolerance()) {

				log.info("Width of Product Tolerance Value should be 25 % of actual value");

				errorMap.notify(ErrorDescription.CALCULATE_VARIETY_WIDTH_TOLARANCE_VALUE.getCode());

				return null;

			}

		}

		return null;

	}

	public String onChangeproductVarietyReedPickperInch() throws Exception {
		log.info("lot percentssss :", productVarietyMasterDTO);

		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		tol = Integer.parseInt(percentTole);
		log.info("lot percent :" + tol);
		if ((productVarietyMasterDTO.getPicksPerInchActual() != null
				|| productVarietyMasterDTO.getPicksPerInchTolerance() != null)
				&& productVarietyMasterDTO.getPicksPerInchActual() != null
				&& productVarietyMasterDTO.getPicksPerInchActual() != null) {

			if (productVarietyMasterDTO.getPicksPerInchActual() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_REEDS_PICK_ACTUAL.getCode());
				return null;
			}

			double pickPercentage = (productVarietyMasterDTO.getPicksPerInchActual() * tol) / 100;
			log.info("pickPercentage--------" + pickPercentage);
			if (productVarietyMasterDTO.getPicksPerInchTolerance() != null
					&& pickPercentage < productVarietyMasterDTO.getPicksPerInchTolerance()) {

				log.info("Reed Pick per Inch Tolerance Value should be 25 % of actual value");

				errorMap.notify(ErrorDescription.CALCULATE_PICK_PER_TOLARANCE_VALUE.getCode());
				return null;

			}

		}

		return null;

	}

	public String onChangeproductVarietyReedEndsperInch() throws Exception {
		log.info("lot percentssss :", productVarietyMasterDTO);

		/*percentTole = Util.getInstance().getPropValues("maxPercent");*/
		percentTole = commonDataService.getAppKeyValue(AppConfigKey.PRODUCT_VARIETY_MAXPERCENT);
		tol = Integer.parseInt(percentTole);
		log.info("lot percent :" + tol);
		if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
				&& productVarietyMasterDTO.getEndspPerInchActual() != null) {

			if (productVarietyMasterDTO.getEndsPerInchTolerance() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_REEDS_ENDS_ACTUAL.getCode());
				return null;
			}

			double endPercent = (productVarietyMasterDTO.getEndspPerInchActual() * tol) / 100;
			log.info("endPercent--------" + endPercent);
			if (productVarietyMasterDTO.getEndsPerInchTolerance() != null
					&& endPercent < productVarietyMasterDTO.getEndsPerInchTolerance()) {

				log.info("Reed Ends per Inch Tolerance Value should be 25 % of actual value");

				errorMap.notify(ErrorDescription.CALCULATE_END_PER_TOLARANCE_VALUE.getCode());
				return null;

			}

		}

		return null;

	}
	
	public void clearProductVarietyList() {
		log.info("start clearProductVarietyList() method");
		try {
			addButtonFlag=false;
			productVarityMasterResponse = new ProductVarityMasterResponse();
		}
		catch (Exception e) {
			// TODO: handle exception
			log.error("Error in clearProductVarietyList method",e);
		}
		log.info("End clearProductVarietyList() method");
	}

}
