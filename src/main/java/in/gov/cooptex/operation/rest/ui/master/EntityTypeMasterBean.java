package in.gov.cooptex.operation.rest.ui.master;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityTypeMasterDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.InfoCodeDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("entityTypeMasterBean")
@Scope("session")
public class EntityTypeMasterBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Getter
	@Setter
	public String action;

	@Getter
	@Setter
	public boolean visible;

	@Getter
	@Setter
	String buttonName;

	@Getter
	@Setter
	List<EntityTypeMasterDTO> entityList = new ArrayList<EntityTypeMasterDTO>();

	@Getter
	@Setter
	EntityTypeMasterDTO entityMasterDto;

	@Getter
	@Setter
	EntityTypeMasterDTO selectedData = new EntityTypeMasterDTO();

	RestTemplate restTemplate;

	String serverURL = null;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		getList();
	}

	public EntityTypeMasterBean() {
		entityMasterDto = new EntityTypeMasterDTO();
		visible = false;
		action = "ADD";
		restTemplate = new RestTemplate();
		log.info("Inside EntityMasterBean()");
		loadValues();
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String showEntityMaster() {
		entityMasterDto = new EntityTypeMasterDTO();
		action = "ADD";
		visible = false;
		getList();
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public void clear() {
		log.info("#==-> Clear Method <-==#");
		entityMasterDto = new EntityTypeMasterDTO();
		visible = false;
		action = "ADD";
		getList();
		// return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public String search() {
		log.info("#==-> Search Method <-==#");
		log.info("<--- Button Name is ---> " + buttonName);
		try {
			if (buttonName.equals("Search")) {
				if (entityMasterDto.getEntityCode() == null || entityMasterDto.getEntityCode().isEmpty()
						&& (entityMasterDto.getEntityName() == null || entityMasterDto.getEntityName().isEmpty())
						&& (entityMasterDto.getLocalName() == null || entityMasterDto.getLocalName().isEmpty())) {

					// Util.addWarn("Please enter at least one value");
					Util.addWarn(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ENTER_ATLEAST_ONE_DETAIL.getErrorCode(),
							languageBean.getLocaleCode()));
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					getList();
					return null;

				}
				String searchUrl = serverURL + "/entitytypemaster/search";
				RestTemplate restTemplate = new RestTemplate();
				HttpEntity<EntityTypeMasterDTO> requestEntity = new HttpEntity<EntityTypeMasterDTO>(entityMasterDto,
						loginBean.headers);
				ResponseEntity<String> districtResponse = restTemplate.exchange(searchUrl, HttpMethod.POST,
						requestEntity, String.class);
				ObjectMapper mapper = new ObjectMapper();
				entityList = mapper.readValue(districtResponse.getBody(),
						mapper.getTypeFactory().constructCollectionType(List.class, EntityTypeMasterDTO.class));
			}
		} catch (Exception e) {
			log.error("<--- Error in Search --->" + e);
		}
		// entityMasterDto = new EntityTypeMasterDTO();
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public String submitForm() {
		log.info("<--- Submit Form Data --->");
		log.info("<--- Button Name is ---> " + buttonName);
		try {
			if (buttonName.equals("ADD")) {
				/*
				 * if(entityMasterDto.getEntityCode()==null ||
				 * entityMasterDto.getEntityCode().isEmpty()){ // Util.addWarn(
				 * "Entity Code Cannot be empty"); return null; }
				 * if(entityMasterDto.getEntityName() == null ||
				 * entityMasterDto.getEntityName().isEmpty()){ // Util.addWarn(
				 * "Entity Name Cannot be empty"); return null; }
				 * if(StringUtils.isEmpty(entityMasterDto.getLocalName())){ //
				 * Util.addWarn("Entity Regional Name Cannot be empty"); return null; }
				 */
				if (entityMasterDto.getEntityCode().trim().length() < 3) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ENTITY_TYPE_CODE_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					log.info("Entity code should be min 3 characters");
					return null;
				}
				if (entityMasterDto.getEntityName().trim().length() < 3) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ENTITY_TYPE_NAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					log.info("Entity name should be min 3 characters");
					return null;
				}

				if (entityMasterDto.getLocalName().trim().length() < 3) {
					Util.addError(ErrorCodeDescription.getDescription(
							ErrorCodeDescription.ENTITY_TYPE_LNAME_VALID_LENGTH.getErrorCode(),
							languageBean.getLocaleCode()));
					log.info("Entity name should be min 3 characters");
					return null;
				}

				UserDTO userDto = new UserDTO();
				userDto.setId(loginBean.getUserDetailSession().getId());
				entityMasterDto.setCreatedBy(userDto);
				entityMasterDto.setCreatedDate(new Date());

				String url = serverURL + "/entitytypemaster/addFormData";
				RestTemplate restTemplate = new RestTemplate();
				HttpEntity<EntityTypeMasterDTO> requestEntity = new HttpEntity<EntityTypeMasterDTO>(entityMasterDto,
						loginBean.headers);
				HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
						BaseDTO.class);
				BaseDTO baseDTO = responseEntity.getBody();
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						Util.addInfo(InfoCodeDescription.getDescription(
								InfoCodeDescription.INFO_ENTITY_MASTER_SAVE.getInfoCode(),
								languageBean.getLocaleCode()));
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						entityMasterDto = new EntityTypeMasterDTO();
						getList();
					} else {
						String msg = ErrorCodeDescription.getDescription(baseDTO.getStatusCode());
						Util.addError(ErrorCodeDescription.getDescription(baseDTO.getStatusCode(),
								languageBean.getLocaleCode()));
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
						return null;
					}
				}
			}
		} catch (Exception e) {
			log.error("<--- Error in Submit Form --->" + e);
		}
		return null;
	}

	public String cancelForm() {
		log.info("Cancel Form");
		entityMasterDto = new EntityTypeMasterDTO();
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public void getList() {
		RestTemplate restTemplate = new RestTemplate();
		entityList = new ArrayList<EntityTypeMasterDTO>();
		try {
			String url = serverURL + "/entitytypemaster/masterList";
			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
			ObjectMapper mapper = new ObjectMapper();
			entityList = mapper.readValue(result.getBody(),
					mapper.getTypeFactory().constructCollectionType(List.class, EntityTypeMasterDTO.class));
			if (entityList.isEmpty()) {
				log.info("#==--> Entity Master List Not Available <--==#" + entityList);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error in Get All Data" + e);
		}
	}

	public String updateData() {
		log.info("--- Updated Id is --->" + entityMasterDto.getId());
		/*
		 * if(StringUtils.isEmpty(entityMasterDto.getEntityCode())){
		 * Util.addWarn("Entity Code Cannot be empty"); return null; }
		 * if(StringUtils.isEmpty(entityMasterDto.getEntityName())){
		 * Util.addWarn("Entity Name Cannot be empty"); return null; }
		 * if(StringUtils.isEmpty(entityMasterDto.getLocalName())){
		 * Util.addWarn("Entity Regional Name Cannot be empty"); return null; }
		 */

		UserDTO userDto = new UserDTO();
		userDto.setId(loginBean.getUserDetailSession().getId());
		entityMasterDto.setModifiedBy(userDto);
		entityMasterDto.setModifiedDate(new Date());
		String url = serverURL + "/entitytypemaster/update/{id}";
		RestTemplate restTemplate = new RestTemplate();
		Map<String, String> params = new HashMap<String, String>();
		params.put("id", entityMasterDto.getId().toString());
		HttpEntity<EntityTypeMasterDTO> entity = new HttpEntity<EntityTypeMasterDTO>(entityMasterDto,
				loginBean.headers);
		HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, entity, BaseDTO.class, params);
		log.info("Updated Response :::: " + responseEntity.getBody());
		BaseDTO baseDTO = responseEntity.getBody();
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				// Util.addInfo(baseDTO.getErrorDescription());
				Util.addInfo(InfoCodeDescription.getDescription(
						InfoCodeDescription.INFO_ENTITY_MASTER_UPDATE.getInfoCode(), languageBean.getLocaleCode()));
				log.info(baseDTO.getErrorDescription());
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				action = "ADD";
				visible = false;
				getList();
				entityMasterDto = new EntityTypeMasterDTO();
			} else {
				String msg = ErrorCodeDescription.getDescription(baseDTO.getStatusCode());
				Util.addError(ErrorCodeDescription.getDescription(baseDTO.getStatusCode()));
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				return null;
			}
		}
		return null;
	}

	public String updateFormData(EntityTypeMasterDTO entityMasterDTO) {
		visible = true;
		action = "EDIT";
		log.info("--- Selected Id is --->" + entityMasterDTO.getId());
		entityMasterDto = new EntityTypeMasterDTO();
		entityMasterDto = entityMasterDTO;
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public String delete(EntityTypeMasterDTO entityMaster) {
		try {
			log.info("--- Deleted Data Id is --->" + entityMaster.getId());
			String url = serverURL + "/entitytypemaster/delete/{id}";
			Map<String, String> params = new HashMap<String, String>();
			params.put("id", entityMaster.getId().toString());
			HttpEntity<EntityTypeMasterDTO> entity = new HttpEntity<EntityTypeMasterDTO>(entityMaster,
					loginBean.headers);
			ResponseEntity<EntityTypeMasterDTO> responseDto = restTemplate.exchange(url, HttpMethod.DELETE, entity,
					EntityTypeMasterDTO.class, params);
			log.info("Delete Response. " + responseDto.getBody());
			AppUtil.addInfo(InfoCodeDescription.getDescription(
					InfoCodeDescription.INFO_ENTITY_MASTER_UPDATE.getInfoCode(), languageBean.getLocaleCode()));
			log.info("Deleted Successfully.");
			getList();
			entityMasterDto = new EntityTypeMasterDTO();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}

	public String cancel() {
		return "/pages/master/entityMaster.xhtml?faces-redirect=true";
	}
}
