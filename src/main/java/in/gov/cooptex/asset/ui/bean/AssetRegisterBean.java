package in.gov.cooptex.asset.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AcquiredType;
import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.asset.model.AssetRegister;
import in.gov.cooptex.asset.model.AssetTypeMaster;
import in.gov.cooptex.asset.model.dto.AssetRegisterDTO;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.employeereport.dto.EmployeeReportDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.InsuranceMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("assetRegisterBean")
public class AssetRegisterBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_ASSET_REGISTER_MASTER_PAGE = "/pages/assetManagement/listAsset.xhtml?faces-redirect=true;";
	private final String CREATE_ASSET_REGISTER_PAGE = "/pages/assetManagement/createAssetRegister.xhtml?faces-redirect=true;";
	private final String VIEW_ASSET_REGISTER_PAGE = "/pages/assetManagement/viewAsset.xhtml?faces-redirect=true;";
	ObjectMapper mapper;
	@Getter
	@Setter
	Boolean addButtonFlag;

	@Getter
	@Setter
	String address;

	@Getter
	@Setter

	Boolean editButtonFlag;
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	AssetRegister assetRegister;

	@Getter
	@Setter
	WrapperDTO wrapperDTO = new WrapperDTO();
	@Getter
	@Setter
	String serverURL = null;

	@Autowired
	LoginBean loginBean;
	RestTemplate restTemplate;
	@Getter
	@Setter
	AssetRegister selectedAssetRegister = new AssetRegister();

	@Getter
	@Setter
	LazyDataModel<AssetRegister> lazyAssetRegisterList;
	@Autowired
	AppPreference appPreference;
	@Autowired
	HttpService httpService;
	String jsonResponse;
	@Getter
	@Setter
	List<AssetRegister> assetRegisterList = new ArrayList<AssetRegister>();

	@Getter
	@Setter
	int totalRecords = 0;
	@Autowired
	ErrorMap errorMap;
	@Setter
	@Getter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Autowired
	CommonDataService commonDataService;
	@Setter
	@Getter
	List<EntityTypeMaster> entityTypeList;
	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;
	@Getter
	@Setter
	private List<Department> departmentList = new ArrayList<>();

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterValuelist;
	@Getter
	@Setter
	String assetType;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	EntityMaster entityMaster;
	@Getter
	@Setter

	Boolean entityFlag = false, assetTypeRender = false;
	@Getter
	@Setter
	EntityMaster selectedHeadRegionOfc;

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = new ArrayList<>();
	@Getter
	@Setter
	private EmployeeReportDTO employeeReportDTO = new EmployeeReportDTO();

	@Setter
	@Getter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;
	@Getter
	@Setter
	InsuranceMaster insuranceMaster;

	@Getter
	@Setter
	List<InsuranceMaster> insuranceMasterList;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	AssetCategoryMaster assetCategoryMaster;

	@Getter
	@Setter
	AssetCategoryMaster assetSubCategoryMaster;

	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterList;
	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterParentList;

	@Getter
	@Setter
	List<AssetCategoryMaster> assetSubCategoryMasterList;

	@Getter
	@Setter
	AssetTypeMaster assetTypeMaster;
	@Getter
	@Setter
	List<AssetTypeMaster> assetTypeMasterList;

	@Getter
	@Setter
	List<EntityMaster> entityList;
	@Getter
	@Setter
	EntityMaster entity;
	CityMaster cityMasterobj = new CityMaster();

	@Getter
	@Setter
	List<CityMaster> cityMasterList;
	@Getter
	@Setter
	CityMaster cityMaster;

	@Getter
	@Setter
	List<AcquiredType> acquiredTypeList;
	@Getter
	@Setter
	AcquiredType acquiredType;

	@Getter
	@Setter
	String assetAddress = null;

	@Getter
	@Setter
	Boolean assetAddressFlag = false;

	@Autowired
	AddressMasterBean addressMasterBean;
	@Setter
	@Getter
	String fileName;
	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Getter
	@Setter
	String filepath;

	@Setter
	@Getter
	private StreamedContent file;

	long letterSize;

	@Getter
	@Setter
	AssetRegisterDTO assetRegisterDTO = new AssetRegisterDTO();

	@Getter
	@Setter
	List<AssetCategoryMaster> parentAssetCategoryMasterList = new ArrayList<AssetCategoryMaster>();

	@Getter
	@Setter
	AssetCategoryMaster parentAssetCategoryMaster;

	@Getter
	@Setter
	AddressMaster memberaddress = new AddressMaster();

	/**
	 * @return
	 */
	public String showAssetRegisterListPageAction() {
		log.info("<===== Starts AssetRegister.showAssetRegisterListPageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedAssetRegister == null || selectedAssetRegister.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ASSETREGISTER.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create AssetRegister called..");
				fileName = "";
				assetRegister = new AssetRegister();
				assetRegisterDTO = new AssetRegisterDTO();
				assetCategoryMaster = new AssetCategoryMaster();
				assetSubCategoryMaster = new AssetCategoryMaster();
				acquiredType = new AcquiredType();

				insuranceMaster = new InsuranceMaster();
				entityMasterList = new ArrayList<>();
				entityTypeList = new ArrayList<>();
				entityTypeList = commonDataService.getAllEntityType();
				headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
				addAssetRegister();
				return CREATE_ASSET_REGISTER_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {

				assetRegister = new AssetRegister();
				addAssetRegister();
				log.info("edit AssetRegister called..");

				assetRegister.setAssetCategory(assetRegister.getAssetCategory());
				assetRegister.setAssetSubCategory(assetRegister.getAssetSubCategory());
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/get/"
						+ selectedAssetRegister.getId();

				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					assetRegister = mapper.readValue(jsonResponse, new TypeReference<AssetRegister>() {
					});

					if (assetRegister.getAddressMaster().getId() != null) {
						assetAddress = addressMasterBean.prepareAddress(assetRegister.getAddressMaster());

					}

					if (assetRegister.getEntityMaster().getId() != null) {
						String URL = AppUtil.getPortalServerURL() + "/entitymaster/region/"
								+ assetRegister.getEntityMaster().getId();

						log.info("entity-- URL-----> " + URL);

						BaseDTO baseDTO = httpService.get(URL);
						if (baseDTO.getStatusCode() == 0) {
							ObjectMapper mapper = new ObjectMapper();
							String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
							entityMaster = mapper.readValue(jsonResponse, EntityMaster.class);
							log.info("entity-- region id----->" + entityMaster.getId());
						}

					}
					String entityTypeCode = assetRegister.getEntityMaster().getEntityTypeMaster().getEntityCode();

					log.info("entityTypeCode -----> " + entityTypeCode);

					if ((EntityType.HEAD_OFFICE).equals(entityTypeCode)) {
						// if (assetRegister.getEntityMaster().getName().equalsIgnoreCase("Head Office
						// Entity")) {
						log.info("Inside head office entity");
						assetRegisterDTO.setRegionHeadOffice(assetRegister.getEntityMaster());
					} else {
						assetRegisterDTO.setRegionHeadOffice(entityMaster);
					}

					log.info("assetRegisterDTO.RegionHeadOffice" + assetRegisterDTO.getRegionHeadOffice());
					updateEntityList();
					assetRegisterDTO.setEntityType(assetRegister.getEntityMaster().getEntityTypeMaster());
					log.info("assetRegisterDTO.EntityType" + assetRegisterDTO.getEntityType());
					onEntityChg();
					log.info("entity------->" + assetRegister.getEntityMaster().getId());
					setEntityMaster(assetRegister.getEntityMaster());
					log.info("asset category------------" + assetRegister.getAssetCategory());

					setAcquiredType(assetRegister.getAcquiredType());
					setInsuranceMaster(assetRegister.getInsurance());
					setAssetCategoryMaster(assetRegister.getAssetCategory());
					setAssetSubCategoryMaster(assetRegister.getAssetSubCategory().getAssetParentCategory());
					setSupplierMaster(assetRegister.getSupplier());

					setAssetTypeMaster(assetRegister.getAssetType());
					setAssetCategoryMaster(assetRegister.getAssetCategory());

//					log.info("selectedAssetRegister.getAddressMaster(=====)" + assetRegister.getAddressMaster());
//					assetAddress = addressMasterBean.prepareAddress(assetRegister.getAddressMaster());

					log.info("asset category------------>" + assetCategoryMaster);
					log.info("sub category---" + assetRegister.getAssetSubCategory());
					setAssetSubCategoryMaster(assetRegister.getAssetSubCategory());
					loadAssetSubCategoryMasterList();

					return CREATE_ASSET_REGISTER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}

			} else if (action.equalsIgnoreCase("View")) {
				log.info("View assetregister called..");
				assetRegister = new AssetRegister();
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/get/"
						+ selectedAssetRegister.getId();
				BaseDTO response = httpService.get(url);

				if (response != null && response.getStatusCode() == 0) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					log.info("Json File for View Data" + jsonResponse);
					assetRegister = mapper.readValue(jsonResponse, new TypeReference<AssetRegister>() {
					});

					address = prepareAddress(assetRegister.getAddressMaster());
					filepath = assetRegister.getDocumentPath();

					if (assetRegister.getAssetType().getName().equalsIgnoreCase("Moveable Asset")) {
						assetTypeRender = true;
					} else {
						assetTypeRender = false;
					}
					return VIEW_ASSET_REGISTER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}

			} else {
				log.info("delete assetregister called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAssetRegisterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showAssetRegisterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends AssetRegisterBean.showAssetRegisterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts AssetRegisterBean.deleteConfirm ===========>" + selectedAssetRegister.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/assetregister/deletebyid/" + selectedAssetRegister.getId());

			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ASSETREGISTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAssetRegisterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete AssetRegister....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends AssetRegisterBean.deleteConfirm ===========>");
		return showAssetRegisterListPage();
	}

	public String saveOrUpdate() {

		log.info("<==== Starts AssetRegisterBean.saveOrUpdateee =======>");

		try {

			assetRegister.setCreatedBy(loginBean.getUserDetailSession());

			assetRegister.setEntityMaster(entityMaster);
			assetRegister.setAssetCategory(assetCategoryMaster);
			assetRegister.setAssetSubCategory(assetSubCategoryMaster);

			log.info("<==== Starts AssetRegisterBean.saveOrUpdateee =======>" + assetRegister.getPurchasedDate());

			String assetType = assetRegister.getAssetType().getName();

			if (assetRegisterDTO.getRegionHeadOffice() == null) {
				log.info("Invalid HO/RO");
				errorMap.notify(ErrorDescription.HO_RO_EMPTY.getErrorCode());
				return null;
			}

			if (!assetRegisterDTO.getRegionHeadOffice().getName().equalsIgnoreCase("HEAD OFFICE")) {
				if (assetRegisterDTO.getEntityType() == null) {
					log.info("Invalid ENTITY TYPE");
					errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
					return null;
				}
			}

			if (!assetRegisterDTO.getRegionHeadOffice().getName().equalsIgnoreCase("HEAD OFFICE")) {
				if (entityMaster == null) {
					log.info("Invalid ENTITY");
					errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_MASTER.getErrorCode());
					return null;
				}
			}

			if (assetCategoryMaster.getName() == null) {
				log.info("ASSET CATEGORY");
				Util.addWarn("Asset Category Should Not Be Empty");
				return null;
			}

			if (assetSubCategoryMaster.getName() == null) {
				log.info("SUB ASSET CATEGORY");
				Util.addWarn("Asset Sub Category Should Not Be Empty");
				return null;
			}

			if (assetRegister.getAssetType() == null) {
				log.info("TYPES OF ASSET");
				Util.addWarn("Types of Asset Should Not Be Empty");
				return null;
			}

			if (assetRegister.getDepreciationType() == null && assetType.equalsIgnoreCase("Moveable Asset")) {
				log.info("Depreieciationtype");
				Util.addWarn("Depreciation Type Should Not Be Empty");
				return null;
			}
			if (assetRegister.getAssetName() == null) {
				log.info("ASSET NAME");
				Util.addWarn("Asset Name Should Not Be Empty");
				return null;
			}

			if (assetRegister.getAssetAge() == null) {
				log.info("ASSET AGE");
				Util.addWarn("Asset Age Should Not Be Empty");
				return null;
			}

			if (assetRegister.getQuantity() == null && assetType.equalsIgnoreCase("Moveable Asset")) {
				log.info("quantity");
				Util.addWarn("Quantity Should Not Be Empty");
				return null;
			}

			if (assetType.equalsIgnoreCase("Immovable Asset")) {
				assetRegister.setQuantity(1D);
			}

			if (assetRegister.getPurchasedDate() == null && assetType.equalsIgnoreCase("Moveable Asset")) {
				log.info("purchasedate");
				Util.addWarn("Puchase Date Should Not Be Null");
				return null;
			}
			if (assetRegister.getCostOfAsset() == null && assetType.equalsIgnoreCase("Moveable Asset")) {
				log.info("costofasset");
				Util.addWarn("Cost Of Should Not Be Null");
				return null;
			}

			if (assetRegisterDTO.getRegionHeadOffice().getName().equalsIgnoreCase("HEAD OFFICE")) {
				assetRegister.setEntityMaster(assetRegisterDTO.getRegionHeadOffice());
			}

			// log.info("(assetRegister.getAssetType()------>" +
			// assetRegister.getAssetType().getId());

			assetRegister.setActiveStatus(true);

			log.info("<=Starts headRegionOfficeList =>" + headRegionOfficeList.size());

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/saveorupdate";
			BaseDTO response = httpService.post(url, assetRegister);

			if (response != null && response.getStatusCode() == 0) {

				log.info("assetregister saved successfully.........." + action);
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.ASSETREGISTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ASSETREGISTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends AssetRegisterBean.saveOrUpdate =======>");
		return showAssetRegisterListPage();
	}

	public String showAssetRegisterListPage() {
		log.info("<==== Starts AssetRegisterBean.showAssetRegisterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		viewButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		assetRegister = new AssetRegister();
		selectedAssetRegister = new AssetRegister();
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		parentAssetCategoryMasterList = loadAssetCategoryMasterList();
		loadLazyAssetRegisterList();
		log.info("<==== Ends AssetRegisterBean.showAssetRegisterListPage =====>");
		return LIST_ASSET_REGISTER_MASTER_PAGE;
	}

	private void loadLazyAssetRegisterList() {
		log.info("<===== Starts AssetRegister.loadLazyAssetRegisterList ======>");
		lazyAssetRegisterList = new LazyDataModel<AssetRegister>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<AssetRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/assetregister/getassetregisterlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						assetRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<AssetRegister>>() {

						});

						entityMasterList = commonDataService.loadHeadAndRegionalOffice();
						log.info("====loadheadofficeregionofficelist inside lazyAssetRegisterList====="
								+ entityMasterList.size());

						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return assetRegisterList;
			}

			@Override
			public Object getRowKey(AssetRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AssetRegister getRowData(String rowKey) {
				try {
					for (AssetRegister assetRegister : assetRegisterList) {
						if (assetRegister.getId().equals(Long.valueOf(rowKey))) {
							selectedAssetRegister = assetRegister;
							return assetRegister;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};

		log.info("<===== Ends  AssetRegister.loadLazyAssetRegisterList======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("select event----");
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		selectedAssetRegister = (AssetRegister) event.getObject();
		log.info("selectedAssetRegister-----" + selectedAssetRegister.getId());
	}

	public String clearButton() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedAssetRegister = null;
		loadLazyAssetRegisterList();
		return LIST_ASSET_REGISTER_MASTER_PAGE;
	}

	public void addAssetRegister() {
		log.info("<--- Start AssetRegisterBean.addAssetRegister ---->");
		try {
			headRegionOfficeList = new ArrayList<>();
			entityTypeList = new ArrayList<>();
			setAssetAddress(null);
			assetRegister = new AssetRegister();
			assetRegister.setActiveStatus(true);
			entityMaster = new EntityMaster();
			loadAssetTypeMasterList();
			insuranceMasterList = commonDataService.getAllInsurance();

			loadAssetCategoryMasterList();
			loadAcquiredTypeList();
			loadSupplierMasterList();
			entityMaster = new EntityMaster();

			entityTypeList = commonDataService.getAllEntityType();

			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			log.info("<=Starts headRegionOfficeList =>" + headRegionOfficeList.size());

			updateEntityList();
			onEntityChg();
			log.info("<---- End AssetRegisterBean.addAssetRegister ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
	}

	public void updateEntityList() {
		log.info("<=Starts AssetRegister.updateEntityList =>");
		try {
			assetRegisterDTO.setEmpId(null);
			if (assetRegisterDTO.getRegionHeadOffice() != null
					&& assetRegisterDTO.getRegionHeadOffice().getEntityTypeMaster() != null
					&& assetRegisterDTO.getRegionHeadOffice().getEntityTypeMaster().getEntityCode()
							.equalsIgnoreCase("HEAD_OFFICE")) {
				assetRegisterDTO.setEntityId(assetRegisterDTO.getRegionHeadOffice().getId());
				visibility = true;
				assetRegisterDTO.setEntityType(null);
				setEntityMaster(null);
			} else {
				visibility = false;
			}
			if (assetRegisterDTO.getRegionHeadOffice() != null && assetRegisterDTO.getRegionHeadOffice().getId() != null
					&& assetRegisterDTO.getEntityType() != null && assetRegisterDTO.getEntityType().getId() != null) {
				Long regionId = assetRegisterDTO.getRegionHeadOffice().getId();
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId,
						assetRegisterDTO.getEntityType().getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends AssetRegister.updateEntityList =>");
	}

	public void onEntityChg() {

		selectedHeadRegionOfc = new EntityMaster();
		selectedEntityTypeMaster = new EntityTypeMaster();

		// log.info("===assetRegisterDTO.getRegionHeadOffice().getId()==="+
		// assetRegisterDTO.getRegionHeadOffice().getId());
		// log.info("===assetRegisterDTO.getRegionHeadOffice().getId()===" +
		// assetRegisterDTO.getEntityType().getId());
		if (assetRegisterDTO.getRegionHeadOffice() != null && assetRegisterDTO.getEntityType() != null) {
			entityMasterList = commonDataService.loadEntityByregionOrentityTypeId(
					assetRegisterDTO.getRegionHeadOffice().getId(), assetRegisterDTO.getEntityType().getId());
		}

	}

	public void onEntityTypeSelection() {

		log.info("<====== AssetRegisterBean.onEntityTypeSelection Starts====> - selectedEntityTypeMaster"
				+ selectedEntityTypeMaster);
		selectedEntityMaster = new EntityMaster();

		entityMasterList = commonDataService.loadEntityByregionOrentityTypeId(selectedHeadRegionOfc.getId(),
				selectedEntityTypeMaster.getId());
		log.info("<====== AssetRegisterBean.onEntityTypeSelection Ends====> ");
	}

	public List<AssetTypeMaster> loadAssetTypeMasterList() {
		log.info("loadAssetTypeMasterList start===>");
		try {
			assetTypeMasterList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/getActiveAssetTypeMaster";
			log.info("loadAssetTypeMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<AssetTypeMaster>>() {
				});
			}

			log.info("loadAssetTypeMasterList end===>");

		} catch (Exception e) {
			log.error("loadAssetTypeMasterList error==>", e);
		}
		return assetTypeMasterList;
	}

	public List<AssetCategoryMaster> loadAssetCategoryMasterList() {
		log.info("loadAssetCategoryMasterList start===>");
		try {
			assetCategoryMasterList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/assetcategorymaster/getActiveAssetCategoryMaster";
			log.info("loadAssetCategoryMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});

			}

			log.info("loadAssetCategoryMasterList end===>");

		} catch (Exception e) {
			log.error("loadAssetCategoryMasterList error==>", e);
		}
		return assetCategoryMasterList;
	}

	public void loadAssetSubCategoryMasterList() {
		log.info("loadAssetCategoryMasterList start===>" + assetCategoryMaster.getId());
		try {
			assetSubCategoryMasterList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/assetcategorymaster/getActiveAssetSubCategoryMaster/"
					+ assetCategoryMaster.getId();

			log.info("loadAssetSubCategoryMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				assetSubCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});

				log.info("assetSubCategoryMasterList console check ===>" + assetSubCategoryMasterList.size());
			}

			log.info("loadAssetSubCategoryMasterList end===>");

		} catch (Exception e) {
			log.error("loadAssetSubCategoryMasterList error==>", e);
		}

	}

	public void loadSupplierMasterList() {
		log.info("loadSupplierMasterList start===>");
		try {
			supplierMasterValuelist = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/getActiveSupplierMaster";

			log.info("loadSupplierMasterList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				supplierMasterValuelist = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});

				log.info("supplierMasterValuelist console check ===>" + supplierMasterValuelist.size());
			}

			log.info("loadsupplierMasterValuelist end===>");

		} catch (Exception e) {
			log.error("loadSupplierMasterValuelist error==>", e);
		}

	}

	public void loadAcquiredTypeList() {
		log.info("loadAcquiredTypeList start===>");
		try {
			acquiredTypeList = new ArrayList<>();
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/assetregister/getActiveAcquiredTypeList";

			log.info("loadAcquiredTypeList url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContent());
				acquiredTypeList = mapper.readValue(jsonResponse, new TypeReference<List<AcquiredType>>() {
				});

				log.info("acquiredTypeList console check ===>" + acquiredTypeList.size());
			}

			log.info("loadacquiredTypeList end===>");

		} catch (Exception e) {
			log.error("loadacquiredTypeList error==>", e);
		}

	}

	public void loadentity() {
		String url;
		try {
			entityList = new ArrayList<>();
			entity = new EntityMaster();
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + "/entitymaster/getallEntityforMultipleregionAndEntityType";
			baseDTO = httpService.post(url, employeeReportDTO);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("entityList---------" + entityList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside loadentity method--", e);
		}
	}

	public void openAssetAddress() {
		log.info(" Inside openAssetAddress " + addressMasterBean.getAddressMaster());
		
		addressMasterBean.setAddressMaster(new AddressMaster());

		if (assetRegister.getAddressMaster() != null && assetRegister.getAddressMaster().getStateMaster() != null) {
			addressMasterBean.setAddressMaster(assetRegister.getAddressMaster());
			addressMasterBean.openAddressDialog();
		} else {
			addressMasterBean.openAddressDialog();
		}
		assetAddressFlag = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').show();");
		context.update("addressDialogForm");
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		log.info("assetAddressFlag ::\t" + assetAddressFlag);
		if (assetAddressFlag) {
			assetRegister.setAddressMaster(addressMasterBean.getAddressMaster());

			assetAddress = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		}

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("addForm");
		assetAddressFlag = false;
	}

	public void fileUpload(FileUploadEvent event) {
		log.info("inside assetRegister fileUpload Method ");
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = size / 1000;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("ASSETREGISTER_UPLOAD_FILE_SIZE"));
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.ASSET_REGISTER_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String docPathName = commonDataService.getAppKeyValue("ASSETREGISTER_UPLOAD_PATH ");

		String filePath = Util.fileUpload(docPathName, uploadedFile);
		setFileName(uploadedFile.getFileName());
		assetRegister.setDocumentPath(filePath);
		errorMap.notify(ErrorDescription.ASSET_REGISTER_FILEUPLOAD.getErrorCode());
		log.info(" assetRegisterFile is uploaded successfully");
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			if (filepath != null) {
				File files = new File(filepath);
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
			} else {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
				log.error("File is empty----->");
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
			errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public String prepareAddress(AddressMaster adressMaster) {
		String addressEnglish = "";

		StringBuilder sb = new StringBuilder();

		if (adressMaster != null) {
			if (adressMaster.getAddressLineOne() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineOne())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineOne() + ",";
				sb.append(adressMaster.getAddressLineOne()).append(", ");
			}
			if (adressMaster.getAddressLineTwo() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineTwo())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineTwo() + ",";
				sb.append(adressMaster.getAddressLineTwo()).append(", ");
			}
			if (adressMaster.getAddressLineThree() != null
					&& StringUtils.isNotEmpty(adressMaster.getAddressLineThree())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineThree() + ",";
				sb.append(adressMaster.getAddressLineThree()).append(", ");
			}
			if (adressMaster.getLocAddressLineOne() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineOne())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineOne() + ",";
				sb.append(adressMaster.getLocAddressLineOne()).append(", ");
			}
			if (adressMaster.getLocAddressLineTwo() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineTwo())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineTwo() + ",";
				sb.append(adressMaster.getLocAddressLineTwo()).append(", ");
			}
			if (adressMaster.getLocAddressLineThree() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineThree())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineThree() +
				// ",";
				sb.append(adressMaster.getLocAddressLineThree()).append(", ");
			}
			if (adressMaster.getLandmark() != null && StringUtils.isNotEmpty(adressMaster.getLandmark())) {
				// addressEnglish = addressEnglish + adressMaster.getLandmark() + ",";
				sb.append(adressMaster.getLandmark()).append(", ");
			}
			if (adressMaster.getCityMaster() != null && adressMaster.getCityMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getCityMaster().getName() +
				// ",";
				sb.append(adressMaster.getCityMaster().getName()).append(", ");
			}
			if (adressMaster.getTalukMaster() != null && adressMaster.getTalukMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getTalukMaster().getName() +
				// ",";
				sb.append(adressMaster.getTalukMaster().getName()).append(", ");
			}
			if (adressMaster.getDistrictMaster() != null && adressMaster.getDistrictMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getDistrictMaster().getName()
				// + ",";
				sb.append(adressMaster.getDistrictMaster().getName()).append(", ");
			}
			if (adressMaster.getStateMaster() != null && adressMaster.getStateMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getStateMaster().getName();
				sb.append(adressMaster.getStateMaster().getName()).append(", ");
			}
			if (StringUtils.isNotEmpty(adressMaster.getPostalCode())) {
				// addressEnglish = addressEnglish + adressMaster.getStateMaster().getName();
				sb.append(" - ").append(adressMaster.getPostalCode());
			}
			addressEnglish = sb.toString();
			// if (adressMaster.getPostalCode() != null &&
			// !adressMaster.getPostalCode().trim().isEmpty()) {
			// if (addressEnglish.isEmpty()) {
			// addressEnglish = adressMaster.getPostalCode();
			// } else {
			// addressEnglish = addressEnglish + "-" + adressMaster.getPostalCode();
			// }
			// }
		}
		return addressEnglish;
	}

}
