/**
 * 
 */
package in.gov.cooptex.personnel.hrms.attendance.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BiometricDeviceRegister;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.model.SupplierMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("biometricDeviceRegisterBean")
@Scope("session")
@Log4j2
public class BiometricDeviceRegisterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6726443730369360541L;

	private static final String LIST_PAGE = "/pages/masters/biometricDevice/listBiometricDeviceRegistration.xhtml?faces-redirect=true";
	private static final String CREATE_PAGE = "/pages/masters/biometricDevice/createBiometricDeviceRegistration.xhtml?faces-redirect=true";
	private static final String VIEW_PAGE = "/pages/masters/biometricDevice/viewBiometricDeviceRegistration.xhtml?faces-redirect=true";

	private final String LIST_API_URL = "";

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private BiometricDeviceRegister biometricDeviceRegister;

	@Getter
	@Setter
	private BiometricDeviceRegister selectedBiometricDeviceRegister = new BiometricDeviceRegister();

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	private String action, pageAction;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<BiometricDeviceRegister> lazyBiometricDeviceRegisterList;

	List<BiometricDeviceRegister> biometricDeviceRegisterList;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String supplierInfo, pageName = "create";

	@Getter
	@Setter
	SupplierMaster supplierMaster = new SupplierMaster();

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	EntityMaster entMaster = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	List<EmployeeMaster> empMasterList = new ArrayList<>();

	public BiometricDeviceRegisterBean() {
	}

	@PostConstruct
	public void init() {
		log.info(".......BiometricDeviceRegisterBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadBioDetail();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadBioDetail() {
		entityTypeMasterList = commonDataService.getEntityTypeByStatus();
	}

	public String showListPage() {

		log.info("<==== Starts BiometricDeviceRegister-showBiometricDeviceRegisterListPage =====>");

		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;

		biometricDeviceRegister = new BiometricDeviceRegister();
		selectedBiometricDeviceRegister = new BiometricDeviceRegister();
		loadLazyBiometricDeviceRegisterList();
		log.info("<==== Ends BiometricDeviceRegister-showBiometricDeviceRegisterListPage =====>");

		return LIST_PAGE;
	}

	public void loadLazyBiometricDeviceRegisterList() {

		lazyBiometricDeviceRegisterList = new LazyDataModel<BiometricDeviceRegister>() {

			private static final long serialVersionUID = -9122090322809059767L;

			List<BiometricDeviceRegister> dataList = new ArrayList<BiometricDeviceRegister>();

			@Override
			public List<BiometricDeviceRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				BiometricDeviceRegister request = new BiometricDeviceRegister();

				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);

					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);
					String url = SERVER_URL + "/biometricdeviceregister/lazyload/search";

					log.info("url==>" + url);

					BaseDTO baseDTO = httpService.post(url, request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					dataList = mapper.readValue(jsonResponse, new TypeReference<List<BiometricDeviceRegister>>() {
					});
					if (dataList == null) {
						log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {

						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: totalRecords ::" + totalRecords);
					} else {
						log.error(":: Employee Loan and Advance Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return dataList;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(BiometricDeviceRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BiometricDeviceRegister getRowData(String rowKey) {
				List<BiometricDeviceRegister> responseList = (List<BiometricDeviceRegister>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (BiometricDeviceRegister res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedBiometricDeviceRegister = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String showAddPage() {

		supplierInfo=null;
		entityTypeMaster = new EntityTypeMaster();
		entMaster = new EntityMaster();
		biometricDeviceRegister = new BiometricDeviceRegister();
		return CREATE_PAGE;
	}

	public String showEditPage() {

		try {
			pageName = "update";
			log.info("<=showEditPage=>");
			if (selectedBiometricDeviceRegister == null || selectedBiometricDeviceRegister.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String apiURL = appPreference.getPortalServerURL() + "/biometricdeviceregister/get/"
					+ selectedBiometricDeviceRegister.getId();
			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				biometricDeviceRegister = mapper.readValue(jsonValue, BiometricDeviceRegister.class);
				supplierInfo = biometricDeviceRegister.getSupplierMaster().getId() + "/"
						+ biometricDeviceRegister.getSupplierMaster().getName();
				entMaster = biometricDeviceRegister.getEntityMaster();
				getEmployee();
				getEntityType();
				getEntityMaster();
				return CREATE_PAGE;
			}

		} catch (Exception e) {
			log.error("showViewPage Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public void getEntityType() {
		String apiURL = appPreference.getPortalServerURL() + "/entitytypemaster/getEntityTypeByEntity/" + entMaster.getId();
		try {

			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityTypeMaster = mapper.readValue(jsonValue, EntityTypeMaster.class);
				log.info("entityTypeMaster==>"+entityTypeMaster);
			}

		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	
	public String showViewPage() {

		try {
			log.info("<=showViewPage=>");
			if (selectedBiometricDeviceRegister == null || selectedBiometricDeviceRegister.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String apiURL = appPreference.getPortalServerURL() + "/biometricdeviceregister/get/"
					+ selectedBiometricDeviceRegister.getId();
			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				biometricDeviceRegister = mapper.readValue(jsonValue, BiometricDeviceRegister.class);
				entMaster = biometricDeviceRegister.getEntityMaster();
				getEntityType();
				return VIEW_PAGE;
			}

		} catch (Exception e) {
			log.error("showViewPage Exception==>", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return null;
	}

	public String goToPage() {

		log.info("Goto Page [" + pageAction + "]");

		if (pageAction.equalsIgnoreCase("LIST")) {
			return showListPage();
		} else if (pageAction.equalsIgnoreCase("ADD")) {
			return showAddPage();
		} else if (pageAction.equalsIgnoreCase("EDIT")) {
			return showEditPage();
		} else if (pageAction.equalsIgnoreCase("VIEW")) {
			return showViewPage();
		} else if(pageAction.equalsIgnoreCase("DELETE")) {
			if (selectedBiometricDeviceRegister == null || selectedBiometricDeviceRegister.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
			return null;
		}else {
			return null;
		}
	}
	
	public String  deleteBiometric() {
		try {

			if (selectedBiometricDeviceRegister == null || selectedBiometricDeviceRegister.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("delete Biometric  Id : : " + selectedBiometricDeviceRegister.getId());
			String getUrl = SERVER_URL + "/biometricdeviceregister/delete/id/:id";
			String url = getUrl.replace(":id", selectedBiometricDeviceRegister.getId().toString());
			log.info("deleteBiometric Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.BIOMETRIC_DELETE_SUCCESSFULLY.getErrorCode());
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				errorMap.notify(ErrorDescription.BIOMETRIC_NOT_ABLE_DELETE.getErrorCode());
			}
			showListPage();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;
	}

	public String clear() {
		log.info("Biometric clear method called..");
		selectedBiometricDeviceRegister = new BiometricDeviceRegister();
		showListPage();
		return LIST_PAGE;
	}

	

	public List<String> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete query==>" + query);
		List<String> results = new ArrayList<String>();
		supplierMasterList = loadSupplierMaster(query);
		if (supplierMasterList != null || !supplierMasterList.isEmpty()) {
			for (SupplierMaster supplier : supplierMasterList) {
				String supp = supplier.getId() + "/" + supplier.getName();
				results.add(supp);
			}
		}

		return results;
	}
	
	
	public List<SupplierMaster> loadSupplierMaster(String query) {
		BaseDTO baseDTO = new BaseDTO();
		List<SupplierMaster> supplierMasterList;
		try {
			log.info(".......BiometricDeviceRegisterBean loadSupplierMaster..............");

			String url = appPreference.getPortalServerURL() + "/biometricdeviceregister/loadsupplierautocomplete/" + query;
			log.info("loadSupplierMaster==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMasterList size==> " + supplierMasterList.size());
			} else {
				supplierMasterList = new ArrayList<>();
			}
			return supplierMasterList;
		} catch (Exception e) {
			log.error("Exception in loadSupplierMaster  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * Propose : load all Entity
	 * 
	 * @author krishnakumar
	 * @param
	 * @return
	 */
	public void getEntityMaster() {
		log.info(": getEntityMaster Started " + entityTypeMaster);
		try {
			if (entityTypeMaster == null) {
				log.error("Entity Type Id Not Exists");
				errorMap.notify(ErrorDescription.ENTITY_TYPE_ID_NOT_EXIST.getErrorCode());
			}
			entityMasterList = commonDataService.getEntityMaster(entityTypeMaster);
			log.info("getEntityMaster end entityTypeMasterList===>" + entityTypeMasterList);

		} catch (Exception e) {
			log.error("Exception Occured While Updating Employee", e);
			errorMap.notify(ErrorDescription.ERROR_SPP_EXISTS.getErrorCode());
		}
	}

	public void getEmployee() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info(".......DistributionBean getEmployee.....id........." + entMaster.getId());

			String url = SERVER_URL + "/employee/getemployeebyentity/" + entMaster.getId();
			log.info("getEmployee url==>" + url);
			baseDTO = httpService.get(url);
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("empMasterList==> " + empMasterList);
			} else {
				empMasterList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error("Exception in getEmployee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String saveUpdateBiometric() {
		try {
			log.info("<=call saveUpdateBiometric=>");
			log.info("<=entMaster=>" + entMaster);
			if (supplierInfo == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_SUPPLIER.getErrorCode());
				return null;
			}
			if (biometricDeviceRegister.getDeviceId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_DEVICE_ID.getErrorCode());
				return null;
			}
			if (biometricDeviceRegister.getDeviceId() != null && biometricDeviceRegister.getDeviceId().length() > 20) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_DEVICE_ID_LEN.getErrorCode());
				return null;
			}
			if (entityTypeMaster == null && entityTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_TYPE_MASTER.getErrorCode());
				return null;
			}
			if (entMaster == null && entMaster.getId() == null) {
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_MASTER.getErrorCode());
				return null;
			}
			if (supplierInfo != null) {
				String supplierArr[] = supplierInfo.split("/");
				supplierMaster.setId(Long.parseLong(supplierArr[0]));
				supplierMaster.setName(supplierArr[1]);
			}
			biometricDeviceRegister.setSupplierMaster(supplierMaster);
			biometricDeviceRegister.setEntityMaster(entMaster);
			log.info("biometricDeviceRegister==>" + biometricDeviceRegister);
			String url = SERVER_URL + "/biometricdeviceregister/create";
			BaseDTO baseDTO = httpService.put(url, biometricDeviceRegister);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
//			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
//			BiometricDeviceRegister bioReg = mapper.readValue(jsonResponse, BiometricDeviceRegister.class);
//			log.info("bioReg==>" + bioReg);
//			if (bioReg != null) {
//				biometricDeviceRegister = bioReg;
//			}
			if (baseDTO.getStatusCode() == 0) {
				if (pageAction.equalsIgnoreCase("EDIT")) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.BIOMETRIC_UPDATED_SUCCESSFULLY).getCode());
				}else {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.BIOMETRIC_INSERTED_SUCCESSFULLY).getCode());
				}
				//errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				selectedBiometricDeviceRegister = new BiometricDeviceRegister();
				showListPage();
				return LIST_PAGE;
			} else {
				log.info("Error while saving biometric with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}	
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.NUMBER_FORMAT_EXCEPTION.getErrorCode());
			log.error("Exception in saveUpdateBiometric  :: ", e);
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		
		addButtonFlag = false;
	}

}
