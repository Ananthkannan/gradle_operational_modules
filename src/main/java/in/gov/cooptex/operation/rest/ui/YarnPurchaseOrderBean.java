package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.ModeOfEnquiry;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CustomerQuotationRequestDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SalesQuotationProductDTO;
import in.gov.cooptex.core.dto.YarnPurchaseOrderDTO;
import in.gov.cooptex.core.dto.YarnPurchaseOrderProductDTO;
import in.gov.cooptex.core.dto.YarnPurchaseOrderRequest;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.YarnTypeMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TaxCalculationUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.OrderStatus;
import in.gov.cooptex.operation.enums.PlanStage;
import in.gov.cooptex.operation.enums.QuotationStatus;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.PurchaseOrderLog;
import in.gov.cooptex.operation.model.PurchaseOrderNote;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.model.YarnPurchaseDetails;
import in.gov.cooptex.operation.model.YarnRequirement;
import in.gov.cooptex.operation.production.dto.PurchaseOrderSearch;
import in.gov.cooptex.operation.production.dto.QuotationResponse;
import in.gov.cooptex.operation.production.dto.YarnPurchaseOrderDetailsDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("yarnPurchaseOrderBean")
@Scope("session")
public class YarnPurchaseOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/operation/yarn/listyarnpurchaseorder.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/yarn/createPurchaseOrderYarn.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/yarn/viewPurchaseOrderYarn.xhtml?faces-redirect=true;";

	private final String PREVIEW_PAGE_URL = "/pages/operation/yarn/previewPurchaseOrderYarn.xhtml?faces-redirect=true;";

	private final String PREVIEW_VIEW_PAGE_URL = "/pages/operation/yarn/previewViewPurchaseOrderYarn.xhtml?faces-redirect=true;";

	@Autowired
	LanguageBean languageBean;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	Double totalTaxAmount;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	EntityMaster headOffice;

	@Getter
	@Setter
	YarnPurchaseOrderDetailsDTO yarnPurchaseOrderDetailsDTO;

	@Getter
	@Setter
	LazyDataModel<PurchaseOrderSearch> purchaseOrderSearchLazyList;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	SupplierMaster supplierMasterEntity;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	YarnPurchaseOrderRequest yarnPurchaseOrderRequest;

	@Getter
	@Setter
	Quotation quotation;

	@Getter
	@Setter
	QuotationResponse quotationResponse;

	@Getter
	@Setter
	String[] headOfficeAddress;

	@Getter
	@Setter
	String[] customerAddress;

	@Getter
	@Setter
	PurchaseOrderSearch purchaseOrderSearch;

	@Getter
	@Setter
	String[] deliveryAddress;

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<CustomerMaster> customerMasterList;

	@Getter
	@Setter
	CustomerTypeMaster customerTypeMaster;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> yarnUnits;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	Integer dispathcedQuantity;

	@Getter
	@Setter
	Integer alreadyOrderedQuanity;

	@Getter
	@Setter
	List<SalesQuotationProductDTO> tempList;

	@Getter
	@Setter
	Double materialValue;

	@Getter
	@Setter
	Double totalCgstAmount;

	@Getter
	@Setter
	Double totalSgstAmount;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	LazyDataModel<QuotationResponse> quotationResponseLazyList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	String raisedFromAddress[];

	@Getter
	@Setter
	String raisedToAddress[];

	@Getter
	@Setter
	Double cgstTaxValue;

	@Getter
	@Setter
	Double sgstTaxValue;

	@Getter
	@Setter
	Double totalCgstSgstTaxValue;

	@Getter
	@Setter
	Double totalValueProductVariety;

	@Getter
	@Setter
	Double totalTaxMaterialAmount;

	@Getter
	@Setter
	Double totalUnitPriceSum;

	@Getter
	@Setter
	Double discountSum;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	List<SupplierMaster> supplierList;

	@Getter
	@Setter
	List<YarnPurchaseDetails> yarnTypeList;

	@Getter
	@Setter
	List<YarnRequirement> yarnRequirementList;

	@Getter
	@Setter
	YarnTypeMaster selectedYarnType;

	@Getter
	@Setter
	PurchaseOrderNote purchaseOrderNote = new PurchaseOrderNote();

	@Getter
	@Setter
	PurchaseOrderNote lastNote = new PurchaseOrderNote();

	@Getter
	@Setter
	PurchaseOrder purchaseOrder;

	@Getter
	@Setter
	List<PurchaseOrderNote> noteList;

	@Getter
	@Setter
	List<ModeOfEnquiry> modeOfEnquiryList;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	private String currentPlanStatus = "SUBMITTED";

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	Map<String, Object> filterMap;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	EntityMaster loggedInEntity;

	@Getter
	@Setter
	List<YarnRequirement> monthList;

	@Getter
	@Setter
	YarnRequirement fromMonthYear;

	@Getter
	@Setter
	YarnRequirement toMonthYear;

	@Getter
	@Setter
	CustomerQuotationRequestDTO customerQuotationRequest;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;

	public YarnPurchaseOrderBean() {
		log.info("Inside YarnPurchaseOrderBean()>>>>>>>>>");
	}

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>");
		loadStatusValues();
	}

	@Getter
	@Setter
	List statusValues;

	@SuppressWarnings("unchecked")
	private void loadStatusValues() {
		Object[] statusArray = QuotationStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status);
		}

		log.info("<--- statusValues ---> " + statusValues);
	}

	public String getAddPage() {
		fromMonthYear = new YarnRequirement();
		purchaseOrderNote = new PurchaseOrderNote();
		purchaseOrder = new PurchaseOrder();
		yarnPurchaseOrderDetailsDTO = new YarnPurchaseOrderDetailsDTO();
		yarnPurchaseOrderRequest = new YarnPurchaseOrderRequest();
		toMonthYear = new YarnRequirement();
		supplierMasterEntity = new SupplierMaster();
		dispathcedQuantity = null;
		alreadyOrderedQuanity = null;
		getMonth();
		loadForwardToUsers();
		loadHeadOffice();
		loadYarnUnits();
		setLoggedInUser();
		loadEmployeeLoggedInUserDetails();
		loadSupplierType();
		return ADD_PAGE_URL;
	}

	// Load Supplier Type List
	public void loadSupplierType() {
		log.info(":::<- Load Supplier TypeStart ->:::");
		BaseDTO baseDTO = null;
		try {
			supplierTypeMasterList = new ArrayList<SupplierTypeMaster>();
			supplierList = new ArrayList<SupplierMaster>();
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllSupplierType";
			log.info("::: Supplier Controller calling  1 :::");
			baseDTO = httpService.get(url);
			log.info("::: get Supplier Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("Supplier load Successfully " + baseDTO.getTotalRecords());
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			} else {
				log.warn("Supplier Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Supplier load ", ee);
		}

	}

	// Load Supplier Code / Name
	public void loadSupplierCodeName() {
		try {
			log.info(":::<- Load loadSupplierCodeName Called ->::: " + supplierTypeMaster.getCode());
			BaseDTO baseDTO = null;
			supplierMasterEntity = new SupplierMaster();
			// supplierMaster.setId(supplierTypeMaster.getId());
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarnconsolidaterequirement/getSelectedSupplierMasterDetails/" + supplierTypeMaster.getId();
			log.info("::: supplierMaster Controller calling  :::");
			baseDTO = httpService.get(url);
			log.info("::: get supplierMaster Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMaster load Successfully " + baseDTO.getTotalRecords());
			} else {
				log.warn("supplierMaster Load Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in supplierMaster load ", ee);
		}
	}

	public String generateValues() {
		productCategoryList = commonDataService.loadProductCategories();
		return null;
	}

	public void getMonth() {

		log.info("getAllCustomer ()");

		monthList = loadMonthList();

		log.info("monthList ()", monthList);

	}

	public void getNote() {
		log.info("==================test======================= " + purchaseOrderNote.getNote());
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + purchaseOrderNote.getNote());
		log.info("=========================================");
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadSupplerDetails() {
		log.info("Loading all loadSupplerDetails ............");

		YarnPurchaseOrderDTO yarnPurchaseOrderDTO = new YarnPurchaseOrderDTO();

		yarnPurchaseOrderDTO.setYarnRequiermentId(toMonthYear.getId());

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/getsupplierdetails";

			BaseDTO baseDTO = httpService.post(url, yarnPurchaseOrderDTO);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}
	}

	public void loadAllYarnType() {
		log.info("Loading all loadAllYarnType ............");

		YarnPurchaseOrderDTO yarnPurchaseOrderDTO = new YarnPurchaseOrderDTO();

		yarnPurchaseOrderDTO.setSupplierId(supplierMasterEntity.getId());

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/getyarntypes";

			BaseDTO baseDTO = httpService.post(url, yarnPurchaseOrderDTO);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				yarnTypeList = mapper.readValue(jsonResponse, new TypeReference<List<YarnPurchaseDetails>>() {
				});

				for (YarnPurchaseDetails details : yarnTypeList) {
					productList.add(details.getItem());
				}

				log.info("yarnTypeList............" + yarnTypeList);

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}
	}

	public void getYarnRequirementFromSupplier() {
		log.info("Loading all getYarnRequirementFromSupplier ............");

		YarnPurchaseOrderDTO yarnPurchaseOrderDTO = new YarnPurchaseOrderDTO();

		yarnPurchaseOrderDTO.setSupplierId(supplierMasterEntity.getId());

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/getyarnrequirement";

			BaseDTO baseDTO = httpService.post(url, yarnPurchaseOrderDTO);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				yarnRequirementList = mapper.readValue(jsonResponse, new TypeReference<List<YarnRequirement>>() {
				});

				log.info("yarnRequirementList............" + yarnRequirementList);

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}
	}

	public String getQuantityForProduct() {

		for (YarnPurchaseDetails details : yarnTypeList) {
			if (details.getItem().getId().equals(yarnPurchaseOrderRequest.getProductVarietyMaster().getId())) {
				dispathcedQuantity = details.getQuantityToPurchase();
			}
		}
		return null;
	}

	public String onChangeOrderedQuantity() {

		if (alreadyOrderedQuanity > dispathcedQuantity) {
			errorMap.notify(ErrorDescription.ALREADY_ORDERED_CANNOT_GREATER.getCode());
			return null;
		}

		yarnPurchaseOrderRequest.setCurrentOrderedQuantity(dispathcedQuantity - alreadyOrderedQuanity);

		return null;
	}

	private List<YarnRequirement> loadMonthList() {

		log.info("Loading all loadMonthList types............");

		List<YarnRequirement> yarnMonthList = null;

		YarnPurchaseOrderDTO yarnPurchaseOrderDTO = new YarnPurchaseOrderDTO();

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/getyarndetails";

			BaseDTO baseDTO = httpService.post(url, yarnPurchaseOrderDTO);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				yarnMonthList = mapper.readValue(jsonResponse, new TypeReference<List<YarnRequirement>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return yarnMonthList;

	}

	public Date getCurrentDate() {
		return new Date();
	}

	public void setLoggedInUser() {
		BaseDTO baseDTO = new BaseDTO();

		if (loginBean.getUserProfile() != null && (EmployeeMaster) loginBean.getUserProfile() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
						.getWorkLocation() != null) {
			loggedInEntity = ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
					.getWorkLocation();
		}

		if (loggedInEntity == null) {

			try {
				String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/sales/quotation/getentitybyuser/" + loginBean.getUserMaster().getId();
				baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					loggedInEntity = mapper.readValue(jsonValue, EntityMaster.class);
				}
			} catch (Exception e) {

				log.error("Exception occured  setLoggedInUser.... ", e);

			}
		}

	}

	public String getListPage() {

		loadStatusValues();

		loadLazyPurchaseOrderList();

		return LIST_PAGE_URL;
	}

	public void calculateTotalValues(List<PurchaseOrderItems> itemsList) {
		materialValue = TaxCalculationUtil.calculatePurchaseOrderMaterialValue(itemsList);
		totalCgstAmount = TaxCalculationUtil.calculateTotalCgstAmount(gstPercentageWiseDTOList);
		totalSgstAmount = TaxCalculationUtil.calculateTotalSgstAmount(gstPercentageWiseDTOList);
		totalTaxAmount = TaxCalculationUtil.calculateTotalTaxAmount(gstPercentageWiseDTOList);
	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");
		log.info("Selected Product Category " + yarnPurchaseOrderRequest.getProductCategory());
		productGroupList = commonDataService.loadProductGroups(yarnPurchaseOrderRequest.getProductCategory());
		log.info("productGroupList " + productGroupList);
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");

		yarnPurchaseOrderRequest.setProductVarietyMaster(null);
		yarnPurchaseOrderRequest.setQuantity(null);

		if (yarnPurchaseOrderRequest != null && yarnPurchaseOrderRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + yarnPurchaseOrderRequest.getProductGroupMaster().getCode());
			// productList =
			// commonDataService.loadProductVarieties(yarnPurchaseOrderRequest.getProductGroupMaster());
			productList = new ArrayList<ProductVarietyMaster>();
			loadAllYarnType();
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public String loadPreviewDetails() {
		String quotationNumberString = "";
		Quotation quotationObj = new Quotation();
		;
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/loadpreviewdetails/" + loggedInEntity.getCode();
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotationObj = mapper.readValue(jsonValue, Quotation.class);
			quotation.setQuotationNumber(quotationObj.getQuotationNumber());
			quotation.setQuotationNumberPrefix(quotationObj.getQuotationNumberPrefix());
			quotation.setEmployeeName(quotationObj.getEmployeeName());
		} catch (Exception e) {
			log.error("Exception in generateQuotationNumber ", e);
		}

		return quotationNumberString;
	}

	public void clear() {
		customerQuotationRequest = new CustomerQuotationRequestDTO();
	}

	public void processDelete() {
		log.info("SupplyRateConfirmationBean processDelete method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public void clearTableData() {
		gstWiseList = new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		totalCgstAmount = 0.0;
		totalSgstAmount = 0.0;
		totalTaxAmount = 0.0;
		materialValue = 0.0;
	}

	public String getViewPage() {
		action = "VIEW";

		loadForwardToUsers();

		enableFlag = false;

		yarnPurchaseOrderRequest = new YarnPurchaseOrderRequest();

		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		get();

		supplierMasterEntity = purchaseOrder.getSupplierMaster();

		getYarnRequirementFromSupplier();
		if (yarnRequirementList != null) {
			fromMonthYear = yarnRequirementList.get(0);
			toMonthYear = yarnRequirementList.get(0);
		}
//		else {
//			loadAllYarnType();
//			if(yarnTypeList!=null) {
//			fromMonthYear = yarnTypeList.get(0);
//			toMonthYear = yarnRequirementList.get(0);
//		}}

		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}

		yarnPurchaseOrderRequest.setPuchaseOrderItemsList(purchaseOrder.getPurchaseOrderItemsList());

		loadTaxDetails();

		noteList = purchaseOrder.getPurchaseOrderNoteList();

		Collections.sort(noteList, new ComparatorId());

		lastNote = noteList.get(noteList.size() - 1);

		purchaseOrderNote.setFinalApproval(lastNote.getFinalApproval());

		purchaseOrderNote.setNote(lastNote.getNote());

		if (lastNote.getUserMaster() != null
				&& lastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
			enableFlag = true;
		}

		eventLogList = new ArrayList<>();

		approvedEventLogList = new ArrayList<>();

		rejectedEventLogList = new ArrayList<>();

		if (purchaseOrder.getPurchaseOrderLogList() != null && !purchaseOrder.getPurchaseOrderLogList().isEmpty()) {
			for (PurchaseOrderLog planLog : purchaseOrder.getPurchaseOrderLogList()) {
				EventLog eventLog = new EventLog("", planLog.getCreatedByName(), planLog.getCreatedByDesignation(),
						planLog.getCreatedDate(), planLog.getRemarks());
				if (planLog.getStage().equals(PlanStage.INITIATED.toString())
						|| planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
					eventLog.setTitle("Created By");
				} else if (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
					eventLog.setTitle("Approved By");
					approvedEventLogList.add(eventLog);
				} else if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
					eventLog.setTitle("Rejected By");
					rejectedEventLogList.add(eventLog);
				} else if (planLog.getStage().equals(QuotationStatus.FINAL_APPROVED.toString())) {
					eventLog.setTitle("Final Approved By");
					approvedEventLogList.add(eventLog);
				}
				eventLogList.add(eventLog);
			}
		}

		log.info("eventLogList", eventLogList);

		Collections.sort(eventLogList, new Comparator<EventLog>() {
			public int compare(EventLog m1, EventLog m2) {
				return m1.getTitle().compareTo(m2.getTitle());
			}
		});

		if (purchaseOrder.getStatus().equals(OrderStatus.FINAL_APPROVED)
				|| purchaseOrder.getStatus().equals(OrderStatus.REJECTED)) {
			enableFlag = false;
		}

		return VIEW_PAGE_URL;
	}

	public String getEditPage() {

		fromMonthYear = new YarnRequirement();
		purchaseOrder = new PurchaseOrder();
		yarnPurchaseOrderDetailsDTO = new YarnPurchaseOrderDetailsDTO();
		yarnPurchaseOrderRequest = new YarnPurchaseOrderRequest();
		toMonthYear = new YarnRequirement();
		supplierMasterEntity = new SupplierMaster();
		dispathcedQuantity = null;
		alreadyOrderedQuanity = null;
		getMonth();
		loadForwardToUsers();
		loadHeadOffice();
		loadYarnUnits();
		setLoggedInUser();

		action = "EDIT";

		yarnPurchaseOrderRequest = new YarnPurchaseOrderRequest();

		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		get();

		supplierMasterEntity = purchaseOrder.getSupplierMaster();

		getYarnRequirementFromSupplier();

		fromMonthYear = yarnRequirementList.get(0);
		toMonthYear = yarnRequirementList.get(0);

		loadSupplerDetails();

		generateValues();

		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}

		yarnPurchaseOrderRequest.setPuchaseOrderItemsList(purchaseOrder.getPurchaseOrderItemsList());

		loadTaxDetails();
		return ADD_PAGE_URL;
	}

	public void get() {
		log.info("PurchaseOrderBean.getPurchaseOrder method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(24011);
		}
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/purchase/order/get/"
				+ purchaseOrderSearch.getId();

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
		} catch (Exception e) {
			log.error("Error in viewPlan", e);
			errorMap.notify(1);
		}

		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);
			} catch (IOException e) {
				log.error(e);
			}
		}
	}

	public void loadLazyPurchaseOrderList() {
		addButtonFlag = false;
		log.info("PurchaseOrderBean.loadLazyPurchaseOrderList method started");
		purchaseOrderSearchLazyList = new LazyDataModel<PurchaseOrderSearch>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<PurchaseOrderSearch> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<PurchaseOrderSearch> data = new ArrayList<PurchaseOrderSearch>();
				try {

					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrderSearch>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(PurchaseOrderSearch res) {
				return res != null ? res.getId() : null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public PurchaseOrderSearch getRowData(String rowKey) {
				List<PurchaseOrderSearch> responseList = (List<PurchaseOrderSearch>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (PurchaseOrderSearch res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	static class ComparatorId implements Comparator<PurchaseOrderNote> {
		@Override
		public int compare(PurchaseOrderNote obj1, PurchaseOrderNote obj2) {
			return obj1.getId().compareTo(obj2.getId());
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("PurchaseOrderBean.onRowSelect method started");
		purchaseOrderSearch = ((PurchaseOrderSearch) event.getObject());
		addButtonFlag = true;
	}

	public void loadLazyList() {
		addButtonFlag = false;
		log.info("SupplyRateConfirmationBean loadLazyDraftProductionList method started");
		quotationResponseLazyList = new LazyDataModel<QuotationResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<QuotationResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<QuotationResponse> data = new ArrayList<QuotationResponse>();
				try {

					filterMap = filters;
					sortingOrder = sortOrder;
					sortingField = sortField;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<QuotationResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(QuotationResponse res) {
				return res != null ? res.getId() : null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public QuotationResponse getRowData(String rowKey) {
				List<QuotationResponse> responseList = (List<QuotationResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (QuotationResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void loadHeadOffice() {
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/supply/rate/getheadoffice";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			headOffice = mapper.readValue(jsonResponse, EntityMaster.class);
			headOfficeAddress = AppUtil.prepareAddress(headOffice.getAddressMaster());

		} catch (Exception e) {
			log.error("Exception in loadHeadOffice ", e);
		}
	}

	public void loadYarnUnits() {
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/loadyarnunits";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			yarnUnits = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});

		} catch (Exception e) {
			log.error("Exception in loadYarnUnits ", e);
		}
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/user/getallforwardtousers";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				forwardToUsersList = mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("PurchaseOrderBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		purchaseOrderSearch = new PurchaseOrderSearch();
		PurchaseOrderSearch request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("PurchaseOrderBean.getSearchData Method Complted");
		return baseDTO;
	}

	private PurchaseOrderSearch getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		PurchaseOrderSearch request = new PurchaseOrderSearch();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("poNumber")) {
				log.info("PoNumber : " + value);
				request.setPoNumber(value);
			}
			if (entry.getKey().equals("societyCodeOrName")) {
				log.info("SocietyCode Or Name : " + value);
				request.setSocietyCodeOrName(value);
			}
			if (entry.getKey().equals("validDate")) {
				log.info("validDate : " + value);
				request.setValidDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public String addProduct() {

		if (checkProductExists(purchaseOrder.getPurchaseOrderItemsList(),
				yarnPurchaseOrderRequest.getProductVarietyMaster().getId())) {
			log.info("Product Already Exists ");
			errorMap.notify(ErrorDescription.PRODUCT_ALREADY_EXISTS.getCode());
			return null;
		}

		YarnPurchaseOrderDTO yarnPurchaseOrderDTO = new YarnPurchaseOrderDTO();

		YarnPurchaseOrderDetailsDTO yarnPurchaseOrderDetailsDTO = new YarnPurchaseOrderDetailsDTO();
		YarnPurchaseOrderProductDTO yarnPurchaseOrderProductDTO = new YarnPurchaseOrderProductDTO();

		yarnPurchaseOrderDTO.setProductId(yarnPurchaseOrderRequest.getProductVarietyMaster().getId());

		if (yarnPurchaseOrderRequest != null && yarnPurchaseOrderRequest.getProductVarietyMaster() != null) {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/getproductdetails";
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.post(url, yarnPurchaseOrderDTO);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					yarnPurchaseOrderDetailsDTO = mapper.readValue(jsonValue, YarnPurchaseOrderDetailsDTO.class);
					yarnPurchaseOrderProductDTO = yarnPurchaseOrderDetailsDTO.getYarnPurchaseOrderProductDTO();
				}

				if (yarnPurchaseOrderProductDTO.getHsnCode() == null) {
					log.info("Hsn Code Null");
					errorMap.notify(ErrorDescription.HSN_CODE_NOT_CONFIGURED.getCode());
					return null;
				}
				if (yarnPurchaseOrderProductDTO.getUnitPrice() == null) {
					log.info("Unit Price Null");
					errorMap.notify(ErrorDescription.PRODUCT_PRICE_NOT_CONFIGURED.getCode());
					return null;
				}
				if (yarnPurchaseOrderProductDTO.getTaxPercent() == null) {
					log.info("Tax Percent Null");
					errorMap.notify(ErrorDescription.PRODUCT_TAX_DETAILS_NOT_CONFIGURED.getCode());
					return null;
				}

			} catch (Exception exp) {
				log.error("Error in addProduct create ", exp);
				errorMap.notify(24010);
			}

			PurchaseOrderItems item = new PurchaseOrderItems();

			item.setProductVarietyMaster(yarnPurchaseOrderRequest.getProductVarietyMaster());
			item.setUomMaster(item.getProductVarietyMaster().getUomMaster());
			item.getProductVarietyMaster().setHsnCode(yarnPurchaseOrderProductDTO.getHsnCode());
			item.setItemQty(yarnPurchaseOrderRequest.getCurrentOrderedQuantity().doubleValue());
			item.setUnitRate(yarnPurchaseOrderProductDTO.getUnitPrice());
			item.setTaxPercent(yarnPurchaseOrderProductDTO.getTaxPercent());
			item.setTaxValue((item.getItemQty() * item.getUnitRate()) * item.getTaxPercent() / 100);
			item.setItemTotal((item.getItemQty() * item.getUnitRate()));

			purchaseOrder.getPurchaseOrderItemsList().add(item);

			yarnPurchaseOrderRequest.setPuchaseOrderItemsList(purchaseOrder.getPurchaseOrderItemsList());

			loadTaxDetails();

			yarnPurchaseOrderRequest.setProductCategory(null);
			yarnPurchaseOrderRequest.setProductVarietyMaster(null);
			yarnPurchaseOrderRequest.setProductGroupMaster(null);
			yarnPurchaseOrderRequest.setQuantity(null);
		}
		return null;
	}

	public String formatNumber(Double number) {
		if (number != null) {
			NumberFormat formatter = new DecimalFormat("#0.00");
			return formatter.format(number);
		}
		return null;
	}

	public void calculateTotalTaxSummary() {
		if (gstPercentageWiseDTOList.size() != 0) {
			cgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			sgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalCgstSgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
		} else {
			cgstTaxValue = null;
			sgstTaxValue = null;
			totalCgstSgstTaxValue = null;
		}
	}

	public String delete() {
		log.info("YarnPurchaseOrderBean delete method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			return null;
		}
		if (!purchaseOrderSearch.getStatus().equals(QuotationStatus.INITIATED.toString())) {
			errorMap.notify(ErrorDescription.CANNOT_DELETE_QUOTATION.getCode());
			return LIST_PAGE_URL;
		}
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/yarn/purchase/order/delete/" + purchaseOrderSearch.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.SALES_QUOTATION_DELETED_SUCCESSFULLY.getCode());
			return LIST_PAGE_URL;
		} else {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return LIST_PAGE_URL;

	}

	public void loadTaxDetails() {
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/yarn/purchase/order/loadtaxdetails";
		BaseDTO baseDTO = null;
		ItemDataWithGstDTO quotationItemWithGstDTO = new ItemDataWithGstDTO();
		try {
			baseDTO = httpService.post(url, yarnPurchaseOrderRequest);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				quotationItemWithGstDTO = mapper.readValue(jsonValue, ItemDataWithGstDTO.class);
				log.info("quotationItemWithGstDTO" + quotationItemWithGstDTO);
				gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
				gstWiseList = quotationItemWithGstDTO.getGstWiseList();
				headOffice = quotationItemWithGstDTO.getHeadOffice();
				calculateTotalTaxSummary();

				calculateTotalValues(purchaseOrder.getPurchaseOrderItemsList());

				if (purchaseOrder.getPurchaseOrderItemsList().size() != 0) {
					totalTaxMaterialAmount = purchaseOrder.getPurchaseOrderItemsList().stream()
							.mapToDouble(PurchaseOrderItems::getItemTotal).sum();

					amountInWords = AmountToWordConverter.converter(
							String.valueOf(AppUtil.ifNullRetunZero(totalTaxMaterialAmount + totalTaxAmount)));
				} else {
					totalTaxMaterialAmount = null;

					amountInWords = null;
				}

			}
		} catch (Exception exp) {
			log.error("Error in loadTaxDetails  ", exp);
		}
	}

	public boolean checkProductExists(List<PurchaseOrderItems> purchaseOrderItemList, Long productId) {
		boolean idExists = purchaseOrderItemList.stream()
				.anyMatch(t -> t.getProductVarietyMaster().getId().equals(productId));
		return idExists;
	}

	public String editRow(int index) {
		log.info("yarnPurchaseOrderBean.editRow method started" + customerQuotationRequest);

		purchaseOrder.getPurchaseOrderItemsList().get(index)
				.setItemTotal(purchaseOrder.getPurchaseOrderItemsList().get(index).getItemQty()
						* purchaseOrder.getPurchaseOrderItemsList().get(index).getUnitRate());

		loadTaxDetails();

		yarnPurchaseOrderRequest.setPuchaseOrderItemsList(purchaseOrder.getPurchaseOrderItemsList());

		return null;
	}

	public String deleteItem(int index) {
		log.info("yarnPurchaseOrderBean.editRow method started");
		purchaseOrder.getPurchaseOrderItemsList().remove(index);
		yarnPurchaseOrderRequest.setPuchaseOrderItemsList(purchaseOrder.getPurchaseOrderItemsList());
		loadTaxDetails();
		return null;
	}

	public String savePurchaseOrder(String requestType) {
		log.info("PurchaseOrderBean.savePurchaseOrder method started");
		if (requestType.equals(OrderStatus.INITIATED.name())) {
			purchaseOrder.setStatus(OrderStatus.INITIATED);
		} else {
			purchaseOrder.setStatus(OrderStatus.SUBMITTED);
		}

		purchaseOrder.setMaterialValue(totalTaxMaterialAmount);
		purchaseOrder.setTotalValue(totalTaxMaterialAmount);
		purchaseOrder.setNetValue(totalTaxMaterialAmount);

		purchaseOrder.setForwardToId(purchaseOrderNote.getUserMaster().getId());
		purchaseOrder.setIsFinalApprove(purchaseOrderNote.getFinalApproval());
		purchaseOrder.setNote(purchaseOrderNote.getNote());

		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/yarn/purchase/order/create";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
		} catch (Exception exp) {
			log.error("Error in Purchase Order create ", exp);
			errorMap.notify(24010);
			return null;
		}

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Purchase Order saved successfully");
				errorMap.notify(24010);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}
		return LIST_PAGE_URL;
	}

	public String previewPurchaseOrder() {

		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/yarn/purchase/order/preview";
		purchaseOrder.setSupplierMaster(supplierMasterEntity);
		purchaseOrder.setEntityMaster(loggedInEntity);
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Purchase Order preview successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);
					raisedFromAddress = AppUtil.prepareAddress(loggedInEntity.getAddressMaster());
					raisedToAddress = AppUtil.prepareAddress(purchaseOrder.getSupplierMaster().getAddressMaster());
					deliveryAddress = AppUtil
							.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in preview Purchase Order", exp);
			errorMap.notify(1);
			return null;
		}

		return PREVIEW_PAGE_URL;
	}

	public String previewViewPurchaseOrder() {

		raisedFromAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
		raisedToAddress = AppUtil.prepareAddress(purchaseOrder.getSupplierMaster().getAddressMaster());
		deliveryAddress = AppUtil.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());

		return PREVIEW_VIEW_PAGE_URL;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current FDS Plan Status ---> " + currentPlanStatus);
		log.info("<--- approveComments ---> " + approveComments);
		log.info("<--- rejectComments ---> " + rejectComments);

		BaseDTO baseDTO = null;

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			PurchaseOrder request = new PurchaseOrder(purchaseOrder.getId(), OrderStatus.APPROVED, approveComments);

			if (purchaseOrderNote.getUserMaster() != null) {
				request.setForwardToId(purchaseOrderNote.getUserMaster().getId());
			}
			request.setIsFinalApprove(purchaseOrderNote.getFinalApproval());
			request.setNote(purchaseOrderNote.getNote());

			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/approve";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				AppUtil.addInfo("Approved Successfully");
			}

		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {

			PurchaseOrder request = new PurchaseOrder(purchaseOrder.getId(), OrderStatus.REJECTED, rejectComments);
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/yarn/purchase/order/reject";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				AppUtil.addInfo("Rejected Successfully");
			}

		}

		/*
		 * if (baseDTO != null) { errorMap.notify(baseDTO.getStatusCode()); }
		 */
		approveComments = null;
		rejectComments = null;

		return LIST_PAGE_URL;
	}

	public void onPageLoad() {
		log.info("PurchaseOrderBean.onPageLoad method started");
		addButtonFlag = false;
	}

	public void changeCurrentStatus(String value) {

		log.info("Changing the status to " + value);

		currentPlanStatus = value;
		approveComments = null;
		rejectComments = null;
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");

		customerQuotationRequest.setQuantity(null);

		if (customerQuotationRequest != null && customerQuotationRequest.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + customerQuotationRequest.getProductVarietyMaster().getCode());
		}
	}

}
