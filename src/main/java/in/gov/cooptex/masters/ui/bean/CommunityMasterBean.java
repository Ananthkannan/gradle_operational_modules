package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("communityMasterBean")
@Scope("session")
@Log4j2
public class CommunityMasterBean {

	final String LIST_PAGE = "/pages/masters/community/listCommunity.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/community/createCommunity.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String COMMUNITY_URL = AppUtil.getPortalServerURL()+"/community";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	Community community,selectedCommunity;

	@Getter @Setter
	LazyDataModel<Community> lazyCommunityList;

	List<Community> communityList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showCommunityListPage() {
		log.info("<==== Starts CommunityMasterBean.showCommunityListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		community = new Community();
		selectedCommunity = new Community();
		loadLazyCommunityList();
		log.info("<==== Ends CommunityMasterBean.showCommunityListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyCommunityList() {
		log.info("<===== Starts CommunityMasterBean.loadLazyCommunityList ======>");

		lazyCommunityList = new LazyDataModel<Community>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<Community> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = COMMUNITY_URL + "/getallcommunitylistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						communityList = mapper.readValue(jsonResponse, new TypeReference<List<Community>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return communityList;
			}

			@Override
			public Object getRowKey(Community res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Community getRowData(String rowKey) {
				for (Community community : communityList) {
					if (community.getId().equals(Long.valueOf(rowKey))) {
						selectedCommunity = community;
						return community;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends CommunityMasterBean.loadLazyCommunityList ======>");
	}


	public String showCommunityListPageAction() {
		log.info("<===== Starts CommunityMasterBean.showCommunityListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedCommunity == null || selectedCommunity.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_COMMUNITY.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create community called..");
				community = new Community();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit community called..");
				String url=COMMUNITY_URL+"/getbyid/"+selectedCommunity.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					community = mapper.readValue(jsonResponse,new TypeReference<Community>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete community called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCommunityDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showCommunityListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends CommunityMasterBean.showCommunityListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts CommunityMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(COMMUNITY_URL+"/deletebyid/"+selectedCommunity.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.COMMUNITY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCommunityDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete community ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends CommunityMasterBean.deleteConfirm ===========>");
		return  showCommunityListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts CommunityMasterBean.saveOrUpdate =======>");
		try {
			if(community.getCode() == null || community.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.COMMUNITY_CODE_EMPTY.getCode());
				return null;
			}
			if(community.getName() == null || community.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.COMMUNITY_NAME_EMPTY.getCode());
				return null;
			}
			if(community.getName().trim().length() < 3 || community.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.COMMUNITY_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(community.getL_name() == null || community.getL_name().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.COMMUNITY_LNAME_EMPTY.getCode());
				return null;
			}
			if(community.getL_name().trim().length() < 3 || community.getL_name().trim().length() > 75) {
				errorMap.notify(ErrorDescription.COMMUNITY_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(community.getStatus() == null) {
				errorMap.notify(ErrorDescription.COMMUNITY_STATUS_EMPTY.getCode());
				return null;
			}
			String url = COMMUNITY_URL+"/saveorupdate";
			BaseDTO response = httpService.post(url,community);
			if(response != null && response.getStatusCode() == 0) {
				log.info("community saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.COMMUNITY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.COMMUNITY_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends CommunityMasterBean.saveOrUpdate =======>");
		return showCommunityListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts CommunityMasterBean.onRowSelect ========>"+event);
		selectedCommunity = ((Community) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends CommunityMasterBean.onRowSelect ========>");
    }
	
}
