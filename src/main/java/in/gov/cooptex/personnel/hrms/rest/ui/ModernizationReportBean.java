package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.asset.model.ModernizationEstimation;
import in.gov.cooptex.asset.model.ModernizationReport;
import in.gov.cooptex.asset.model.ModernizationReportItems;
import in.gov.cooptex.asset.model.ModernizationReportNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.ModernizationEstimationDTO;
import in.gov.cooptex.dto.pos.ModernizationReportDTO;
import in.gov.cooptex.dto.pos.ModernizationRequestDTO;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("modernizationReportBean")
public class ModernizationReportBean extends CommonBean implements Serializable {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String LIST_PAGE = "/pages/assetManagement/listSubmissionOfReport.xhtml?faces-redirect=true";
	final String CREATE_PAGE = "/pages/assetManagement/createSubmissionOfReport.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/assetManagement/viewSubmissionOfReport.xhtml?faces-redirect=true";

	ObjectMapper objectMapper = new ObjectMapper();

	String url, jsonResponse;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	ModernizationRequestDTO selectedModRequest = new ModernizationRequestDTO();

	@Getter
	@Setter
	LazyDataModel<ModernizationRequestDTO> modRequestLazyList;

	@Getter
	@Setter
	List<ModernizationRequestDTO> modRequestList;

	@Getter
	@Setter
	List<ModernizationEstimation> modernizationEstimationList = new ArrayList<ModernizationEstimation>();

	@Getter
	@Setter
	List<ModernizationReportItems> modernizationReportItemsList = new ArrayList<ModernizationReportItems>();

	@Getter
	@Setter
	ModernizationReport modernizationReport = new ModernizationReport();

	@Getter
	@Setter
	ModernizationReportDTO modernizationReportDTO = new ModernizationReportDTO();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	ModernizationReportNote modernizationReportNote = new ModernizationReportNote();

	@Getter
	@Setter
	private UploadedFile modernizationReportfile;

	@Getter
	@Setter
	String docPath, modReportFileName, action, note;

	long fileSize;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean submitButtonFlag = true;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Getter
	@Setter
	Boolean rejectButtonFlag = true;

	@Getter
	@Setter
	Boolean fowardsFlag = true;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		objectMapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("modernization_request_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circularrr :" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			selectedModRequest = new ModernizationRequestDTO();
			action = "VIEW";
			selectedModRequest.setId(Long.parseLong(circularId));
			selectedNotificationId = Long.parseLong(notificationId);
			viewModernizationReport();
		}
		log.info("ModerizationBean showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public String loadList() {
		addButtonFlag = true;
		log.info("<==== Starts ModernizationReportBean.loadList =====>");
		loadLazyModernizationRequestList();
		log.info("<==== ModernizationReportBean.loadList End=====>");

		return LIST_PAGE;
	}

	public String getSubmissionReport() {
		log.info("<==== Starts ModernizationReportBean.getSubmissionReport =====>");
		try {
			action = "ADD";
			modernizationReportDTO = new ModernizationReportDTO();
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
				return null;
			}
			loadAppConfigValues();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REPORT_FOR_MODERNIZATION");
			modernizationReport = getReportByRequestId();
			getModernizationEstimationByModernReqId();
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error in getData =====>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<==== End ModernizationReportBean.getSubmissionReport =====>");
		return CREATE_PAGE;
	}

	private void loadAppConfigValues() {
		try {
			fileSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void loadLazyModernizationRequestList() {

		log.info("<==== Starts ModernizationReportBean.loadLazyModernizationRequestList122222222222 =====>");
		modRequestLazyList = new LazyDataModel<ModernizationRequestDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ModernizationRequestDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/modernizationreport/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
						modRequestList = objectMapper.readValue(jsonResponse,
								new TypeReference<List<ModernizationRequestDTO>>() {
								});
						log.info("Ends lazy load.... sizeee : " + modRequestList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return modRequestList;
			}

			@Override
			public Object getRowKey(ModernizationRequestDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ModernizationRequestDTO getRowData(String rowKey) {
				for (ModernizationRequestDTO modernizationRequestDTO : modRequestList) {
					if (modernizationRequestDTO.getId().equals(Long.valueOf(rowKey))) {
						selectedModRequest = modernizationRequestDTO;
						return modernizationRequestDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends ModernizationReportBean.loadLazyModernizationRequestList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ModernizationReportBean.onRowSelect ========>" + event);
		selectedModRequest = ((ModernizationRequestDTO) event.getObject());
		addButtonFlag = false;
		viewButtonFlag = true;
		if (ApprovalStage.REJECTED.equalsIgnoreCase(selectedModRequest.getStage())) {
			editButtonFlag = false;
		}

		if (ApprovalStage.SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			editButtonFlag = true;

		}
		if (ApprovalStage.FINAL_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())) {
			addButtonFlag = true;

		}

		if (ApprovalStage.SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())) {
			addButtonFlag = true;

		}

		if (ApprovalStage.MBOOKENTRY_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())) {
			addButtonFlag = true;

		}

		if (ApprovalStage.SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())
				|| ApprovalStage.FINAL_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())) {
			editButtonFlag = true;

		}

		if (ApprovalStage.MBOOKENTRY_FINAL_APPROVED.equalsIgnoreCase(selectedModRequest.getStage())) {
			addButtonFlag = true;

		}
		if (ApprovalStage.MBOOKENTRY_SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())) {
			addButtonFlag = true;

		}

		log.info("checkinggg" + selectedModRequest.getStage());
//		if (!loginBean.getUserMaster().getUsername().equals("hosuper")) {
//			addButtonFlag = true;
//		}

		log.info("<===Ends ModernizationReportBean.onRowSelect ========>");
	}

	public void getModernizationEstimationByModernReqId() {
		log.info(
				"<===Starts ModernizationReportBean:==>getModernizationEstimationByModernReqId Method Start ========>");
		BaseDTO baseDTO = new BaseDTO();
		ModernizationReportItems modernizationReportItems = null;
		List<ModernizationReportItems> modernizationReportItemsObjList = new ArrayList<ModernizationReportItems>();
		modernizationReportItemsList = new ArrayList<ModernizationReportItems>();
		try {
			log.info("<===Starts ModernizationSubmissionReportBean:==> ========>selectedModRequest Id="
					+ selectedModRequest.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/modernizationestimation/modernestimation/"
					+ selectedModRequest.getId() + "/" + (selectedNotificationId != null ? selectedNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
//				modernizationEstimationList = objectMapper.readValue(jsonResponse,
//						new TypeReference<List<ModernizationEstimation>>() {
//						});

				List<ModernizationEstimationDTO> modernizationEstimationDTOList = objectMapper.readValue(jsonResponse,
						new TypeReference<List<ModernizationEstimationDTO>>() {
						});
				if (!CollectionUtils.isEmpty(modernizationEstimationDTOList)) {
					ModernizationEstimationDTO modernizationEstimationDTO = modernizationEstimationDTOList.get(0);
					if (modernizationEstimationDTO != null) {
						modernizationEstimationList = modernizationEstimationDTO.getModernizationEstimationList();
					}
				}
			}

			if (modernizationReport != null && modernizationReport.getId() != null) {
				modernizationReportDTO.setId(modernizationReport.getId());
				modernizationReportItemsObjList = getReportItemListByReportId();
			}

			if (modernizationReport != null && modernizationReportItemsObjList.size() > 0) {

				for (ModernizationReportItems reportItem : modernizationReportItemsObjList) {
					modernizationReportItems = new ModernizationReportItems();
					modernizationReportItems.setId(reportItem.getId());
					modernizationReportItems.setModernizationEstimation(reportItem.getModernizationEstimation());
					modernizationReportItems.setStatus(reportItem.getStatus());
					modernizationReportItems.setEstimationQty(reportItem.getEstimationQty());
					if (reportItem.getBuiltQty() != null && reportItem.getReportQty() != null) {
						modernizationReportItems.setBuiltQty(reportItem.getBuiltQty() + reportItem.getReportQty());
					} else {
						modernizationReportItems.setBuiltQty(0.0);
					}
					modernizationReportItemsList.add(modernizationReportItems);
				}

			} else if (modernizationEstimationList != null) {
				for (ModernizationEstimation modernizationEstimation : modernizationEstimationList) {
					modernizationReportItems = new ModernizationReportItems();
					modernizationReportItems.setModernizationEstimation(modernizationEstimation);
					modernizationReportItems.setEstimationQty(modernizationEstimation.getQuantity());
					modernizationReportItems.setBuiltQty(0.0);
					modernizationReportItems.setReportQty(0.0);
					modernizationReportItemsList.add(modernizationReportItems);
				}
			}

			log.info("<===ModernizationReportBean:==>modernizationEstimationList Size:="
					+ modernizationEstimationList.size());
		} catch (Exception ex) {
			log.info("ModernizationReportBean:=>Error In getModernizationEstimationByModernReqId Method", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===ModernizationReportBean:==>getModernizationEstimationByModernReqId Method End========>");

	}

	public void uploadModernizationReport(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		ModernizationReportItems modernizationReportItems = new ModernizationReportItems();
		try {

			modernizationReportItems = (ModernizationReportItems) event.getComponent().getAttributes().get("object");

			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CER_DOC_EMPTY.getErrorCode());
				return;
			}
			modernizationReportfile = event.getFile();
			long size = modernizationReportfile.getSize();
			log.info("uploadModernizationRequest size==>" + size);
			size = size / 1000;
			if (size > fileSize) {
				modernizationReportDTO.setDocumentPath(null);
				docPath = null;
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String pathName = commonDataService.getAppKeyValue("MODERNIZATION_REPORT_PATH");
			log.info("uploadModernizationReport path==>" + pathName);
			docPath = Util.fileUpload(pathName, modernizationReportfile);
			log.info("uploadModernizationReport docPath==>" + docPath);
			modReportFileName = modernizationReportfile.getFileName();
			modernizationReportDTO.setDocumentPath(docPath);

			if (modernizationReportItemsList.contains(modernizationReportItems)) {
				int index = modernizationReportItemsList.indexOf(modernizationReportItems);
				modernizationReportItems.setDocumentPath(docPath);
				modernizationReportItems.setFileName(modernizationReportfile.getFileName());
				modernizationReportItemsList.set(index, modernizationReportItems);
			}
			errorMap.notify(
					ErrorDescription.getError(AdminErrorCode.MODERNIZATION_REPORT_UPLOAD_SUCCESS).getErrorCode());
			log.info(" uploadModernizationReport Document is uploaded successfully");

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String createModernizationReport() {
		log.info("ModernizationReportBean:createModernizationReport Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
//			if (modernizationReportDTO.getNote() == null) {
//				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
//				return null;
//			}
			if (modernizationReportDTO.getForwardto() != null) {
				modernizationReportDTO.setForwardToId(modernizationReportDTO.getForwardto().getId());
			}
			if (modernizationReportItemsList.size() == 0) {

				Util.addWarn("Estimation of Modernization Should Not Be Empty");
				return null;
			}
			if (modernizationReportDTO.getNote() == null || modernizationReportDTO.getNote().length() == 0) {
				log.info("Note Not Valid");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}
			log.info("ModernizationReportBean:Request Id" + selectedModRequest.getId());
			modernizationReportDTO.setModernizationRequestId(selectedModRequest.getId());
			modernizationReportDTO.setModernizationReportItemsList(modernizationReportItemsList);
			String url = SERVER_URL + "/modernizationreport/create";
			log.info("url=" + url);
			baseDTO = httpService.post(url, modernizationReportDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("ModernizationReport Created Successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.MODERNIZATION_REPORT_SAVE_SUCCESS.getCode());
					else
						errorMap.notify(ErrorDescription.MODERNIZATION_REPORT_UPDATE_SUCCESS.getCode());
					return LIST_PAGE;

				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In createModernizationReport Method ===>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:createModernizationReport Method End===>");
		return null;
	}

	public void removeModernizationReportItem(ModernizationReportItems remove) {
		log.info("ModernizationReportBean:removeModernizationReportItem Method Start==>");
		try {
			if (modernizationReportItemsList.contains(remove)) {
				modernizationReportItemsList.remove(remove);
			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error in removeModernizationEstimation Method==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:removeModernizationEstimation Method End==>");
	}

	public void createNote() {
		log.info("ModernizationReportBean:createNote Method Start==>");
		modernizationReportDTO.setNote(note);
		log.info("ModernizationReportBean:createNote Method End==>");
	}

	public ModernizationReport getReportByRequestId() {
		log.info("ModernizationReportBean:getReportByRequestId Method Start==>");
		BaseDTO baseDTO = new BaseDTO();
		ModernizationReport modernizationReport = new ModernizationReport();
		try {
			log.info("ModernizationReportBean:Request Id==>", selectedModRequest.getId());
			String url = SERVER_URL + "/modernizationreport/getreport/" + selectedModRequest.getId();
			log.info("ModernizationReportBean:url==>", url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				modernizationReport = objectMapper.readValue(jsonResponse, new TypeReference<ModernizationReport>() {
				});
			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error getReportByRequestId Method ==>", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:getReportByRequestId Method End==>");
		return modernizationReport;
	}

	public String clear() {
		addButtonFlag = false;
		log.info("ModernizationReportBean:clear Method Start==>");
		selectedModRequest = new ModernizationRequestDTO();
		log.info("ModernizationReportBean:clear Method End==>");
		return LIST_PAGE;
	}

	public String viewModernizationReport() {
		action = "VIEW";
		if (selectedModRequest == null) {
			errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
			return null;
		}

//		if (ApprovalStage.FINAL_APPROVED.equals(selectedModRequest.getStatus())) {
//			
//		approveButtonFlag=true;
//			rejectButtonFlag=true;
//		}

//		log.info("ModernizationReportBean Stage -> "+selectedModRequest.getStage());
////	
//		
//		if(selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")) {
//			
//			approveButtonFlag=true;
//			 editButtonFlag=true;
//			
//		}
//		     if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//				
//				submitButtonFlag=false;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && !loginBean.getUserMaster().getUsername().equals("hosuper")  ) {
//				submitButtonFlag=true;
//				hideflag=false;
//					
//				
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")) {
//				submitButtonFlag=false;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("APPROVED")) {
//					submitButtonFlag=false;
//				}
//			   if(selectedModRequest.getStage().equalsIgnoreCase("REJECTED")) {
//					submitButtonFlag=false;
//				}
//			   	
//			   if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-SUBMITTED")  ) {
//				   submitButtonFlag=true;  
//			   }
//			   
//			   if(selectedModRequest.getStage().equalsIgnoreCase("MBOOKENTRY-FINAL-APPROVED")  ) {
//				   submitButtonFlag=true;  
//			   }
//			   
//		
//	         if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && loginBean.getUserMaster().getUsername().equals("hosuper") ) {
//			
//		      editButtonFlag=true;
//			
//		       }
//	   else if(selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED") && !loginBean.getUserMaster().getUsername().equals("hosuper")) {
//		   editButtonFlag=false;
//	   }

		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REPORT_FOR_MODERNIZATION");
		modernizationReport = getReportByRequestId();
		modernizationReportDTO.setId(modernizationReport.getId());
		modernizationReportItemsList = getReportItemListByReportId();
		getModernizationReportNoteDetailsByReportId();

		if (modernizationReportNote.getForwardTo() != null) {
			if (modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
				approveButtonFlag = false;
				rejectButtonFlag = false;
			} else {
				approveButtonFlag = true;
				rejectButtonFlag = true;
			}
			if (modernizationReportNote.getFinalApproval().equals(true)) {
				fowardsFlag = false;
				finalapprovalFlag = true;
			}
			log.info("forwardddd" + modernizationReportNote.getForwardTo().getId());
			if (modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
				approveButtonFlag = false;
				rejectButtonFlag = false;
				hideflag = true;
				submitButtonFlag = true;
			}

			if (!modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
				submitButtonFlag = false;
				hideflag = false;
			}

			if (modernizationReportNote.getFinalApproval().equals(true)
					&& modernizationReportNote.getForwardTo().getId().equals(loginBean.getUserMaster().getId())) {
				fowardsFlag = false;
				finalapprovalFlag = true;
				submitButtonFlag = true;
			}
			if (selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")
					|| selectedModRequest.getStage().equalsIgnoreCase("REJECTED")) {

				hideflag = false;
				submitButtonFlag = false;

			}
		}
		return VIEW_PAGE;
	}

	public List<ModernizationReportItems> getReportItemListByReportId() {
		log.info("ModernizationReportBean:getReportItemListByReportId Method Start====>");
		BaseDTO baseDTO = new BaseDTO();
		List<ModernizationReportItems> modernizationReportItemsList = new ArrayList<ModernizationReportItems>();
		try {
			log.info("ModernizationReportBean:Report Id=", modernizationReport.getId());
			if (modernizationReport != null && modernizationReport.getId() != null) {
				String url = SERVER_URL + "/modernizationreport/getreportItem/" + modernizationReport.getId();
				log.info("ModernizationReportBean:url==>", url);
				baseDTO = httpService.get(url);
				objectMapper = new ObjectMapper();
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
					modernizationReportItemsList = objectMapper.readValue(jsonResponse,
							new TypeReference<List<ModernizationReportItems>>() {
							});
				}
			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In getReportItemListByReportId Method ==>", exp);
		}
		log.info("ModernizationReportBean:getReportItemListByReportId Method End====>");
		return modernizationReportItemsList;
	}

	public void downloadfile(ModernizationReportItems fileObj) {
		log.error("ModernizationReportBean:downloadfile Method Start=====>");
		InputStream input = null;
		try {
			if (fileObj.getDocumentPath() != null) {
				File files = new File(fileObj.getDocumentPath());
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
			}
			log.error("exception in downloadfile method------- " + file.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in downloadfile method------- " + e);
		} catch (Exception exp) {
			log.error("exception in downloadfile method------- " + exp);
		}
		log.error("ModernizationReportBean:downloadfile Method End=====>");

	}

	public String approveModernizationReport() {
		log.info("ModernizationReportBean:approveModernizationReport Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {

			if (modernizationReportDTO.getForwardto() != null) {
				modernizationReportDTO.setForwardToId(modernizationReportDTO.getForwardto().getId());
			}
			if (previousApproval == false && modernizationReportDTO.getForwardfor() == true) {
				modernizationReportDTO.setStage(ApprovalStage.APPROVED);
			} else if (previousApproval == true && modernizationReportDTO.getForwardfor() == true) {
				modernizationReportDTO.setStage(ApprovalStage.FINAL_APPROVED);
			} else {
				modernizationReportDTO.setStage(ApprovalStage.APPROVED);
			}
			String url = SERVER_URL + "/modernizationreport/approve";
			baseDTO = httpService.post(url, modernizationReportDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MODERNIZATION_REPORT_APPROVE_SUCCESS.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				loadList();
				return LIST_PAGE;
			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In approveModernizationReport Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:approveModernizationReport Method End====>");
		return null;
	}

	public void getModernizationReportNoteDetailsByReportId() {
		log.info("ModernizationReportBean:getModernizationReportNoteDetailsByReportId Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("ModernizationReportBean:Report Id=", modernizationReport.getId());
			String url = SERVER_URL + "/modernizationreport/getreportnote/" + modernizationReport.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			log.info("ModernizationReportBean:url==>", url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
//				modernizationReportNote = objectMapper.readValue(jsonResponse,
//						new TypeReference<ModernizationReportNote>() {
//						});
				if (selectedNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}

				List<ModernizationReportDTO> modernizationReportDTOList = objectMapper.readValue(jsonResponse,
						new TypeReference<List<ModernizationReportDTO>>() {
						});
				if (!CollectionUtils.isEmpty(modernizationReportDTOList)) {
					ModernizationReportDTO modernizationReportDTO = modernizationReportDTOList.get(0);
					if (modernizationReportDTO != null) {
						modernizationReportNote = modernizationReportDTO.getModernizationReportNote();
						selectedModRequest.setStage(modernizationReportDTO.getStage());
						selectedModRequest.setHeadRegionOffice(modernizationReportDTO.getHeadRegionOffice());
						selectedModRequest.setEntityType(modernizationReportDTO.getEntityType());
						selectedModRequest.setEntity(modernizationReportDTO.getEntity());
						selectedModRequest.setNameOfBuinding(modernizationReportDTO.getNameOfBuinding());
					}
				}

				String jsonResponse1 = objectMapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterData = objectMapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
				});
			}
			note = modernizationReportNote.getNote();
			if (modernizationReportNote.getForwardTo() != null) {
				modernizationReportDTO.setForwardToId(modernizationReportNote.getForwardTo().getId());
			}
			modernizationReportDTO.setForwardfor(modernizationReportNote.getFinalApproval());
			modernizationReportDTO.setNote(note);
			previousApproval = modernizationReportNote.getFinalApproval();
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error in approveModernizationReport Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:getModernizationReportNoteDetailsByReportId Method End===>");
	}

	public String rejectModernizationReport() {
		log.info("ModernizationReportBean:rejectModernizationReport Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			modernizationReportDTO.setStage(ApprovalStage.REJECTED);
			String url = SERVER_URL + "/modernizationreport/reject";
			baseDTO = httpService.post(url, modernizationReportDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MODERNIZATION_REPORT_REJECT_SUCCESS.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return LIST_PAGE;
			}
		} catch (Exception ex) {
			log.info("ModernizationReportBean:Error In rejectModernizationReport Method", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:rejectModernizationReport Method End===>");
		return null;
	}

	public String editModernizationReport() {
		log.info("ModernizationReportBean:editModernizationReport Method Start===>");
		try {
			action = "EDIT";
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
				return null;
			}
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REPORT_FOR_MODERNIZATION");
			modernizationReport = getReportByRequestId();
			modernizationReportDTO.setId(modernizationReport.getId());
			modernizationReportItemsList = getReportItemListByReportId();
			getModernizationReportNoteDetailsByReportId();
			modernizationReportDTO
					.setReportSubmissionDate(modernizationReportItemsList.get(0).getReportSubmissionDate());
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In editModernizationReport Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:editModernizationReport Method End===>");
		return CREATE_PAGE;
	}

	public String delete() {
		log.info("ModernizationReportBean:delete Method Start===>");
		action = "DELETE";
		if (selectedModRequest == null) {
			errorMap.notify(ErrorDescription.SELECT_ERROR.getErrorCode());
			return null;
		}
		modernizationReport = getReportByRequestId();
		deleteModernizationReport();
		log.info("ModernizationReportBean:delete Method End===>");
		return LIST_PAGE;
	}

	public void deleteModernizationReport() {
		log.info("ModernizationReportBean:deleteModernizationReport Method Start===>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("ModernizationReportBean:Report Id=" + modernizationReport.getId());
			String url = SERVER_URL + "/modernizationreport/delete";
			baseDTO = httpService.post(url, modernizationReport);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MODERNIZATION_REPORT_DELETE_SUCCESS.getErrorCode());
				log.info("Deleted Successfully -----------------");
				RequestContext.getCurrentInstance().update("growls");

			}
		} catch (Exception exp) {
			log.info("ModernizationReportBean:Error In deleteModernizationReport Method", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("ModernizationReportBean:deleteModernizationReport Method End==>");

	}

	public String cancel() {
		log.info("cancel Method Start===>");
		modernizationReportDTO = new ModernizationReportDTO();
		note = "";
		return LIST_PAGE;
	}
}
