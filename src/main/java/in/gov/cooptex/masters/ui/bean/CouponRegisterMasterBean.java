package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IncrementListDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.GiftCouponRegister;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("couponRegisterMasterBean")
@Scope("session")
@Log4j2
public class CouponRegisterMasterBean {

	final String LIST_PAGE = "/pages/masters/listCouponRegisterMaster.xhtml?faces-redirect=true";

	final String ADD_PAGE = "/pages/masters/createCouponRegisterMaster.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/masters/viewCouponRegisterMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	EntityMaster loginUserEntity;

	@Getter
	@Setter
	String pageAction;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	LazyDataModel<GiftCouponRegister> lazyGiftCouponRegisterList;

	@Getter
	@Setter
	List<EntityMaster> regionOfficeList;

	@Getter
	@Setter
	EntityMaster regionOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityMaster> showroomList;

	@Getter
	@Setter
	EntityMaster showroom = new EntityMaster();

	@Getter
	@Setter
	List<GiftCouponRegister> giftCouponRegisterList = null;

	@Getter
	@Setter
	GiftCouponRegister selectedGiftCouponRegister = new GiftCouponRegister();

	@Getter
	@Setter
	Boolean isAddButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	Integer couponFrom, couponTo;

	@Getter
	@Setter
	Double couponValue;

	@Getter
	@Setter
	Date couponDate;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean couponStatus;

	@Getter
	@Setter
	GiftCouponRegister giftCouponRegister = null;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@PostConstruct
	public void init() {
		log.info("CouponRegisterMasterBean Init is executed.....................");
		mapper = new ObjectMapper();
		regionOfficeList = commonDataService.loadAllRegionalOffices();
		EmployeeMaster employee = loginBean.getEmployee();
		log.info("employee obj-------" + employee);
		if (employee != null && employee.getPersonalInfoEmployment() != null
				&& employee.getPersonalInfoEmployment().getWorkLocation() != null) {
			loginUserEntity = employee.getPersonalInfoEmployment().getWorkLocation();
			if (loginUserEntity != null) {
				sectionMaster = employee.getPersonalInfoEmployment().getCurrentSection();
			}
		}
		regionOffice = loginUserEntity;
		loadShowRoom();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public String callCouponRegisterPage() {
		log.info("CouponRegisterMasterBean :: callCouponRegisterPage");
		try {

			if (pageAction.equals("LIST")) {
				isAddButton = false;
				isEditButton = true;
				isDeleteButton = true;
				loadLazyCouponRegister();
				return LIST_PAGE;
			} else if (pageAction.equals("ADD")) {

				regionOffice = loginUserEntity;
				return ADD_PAGE;
			} else if (pageAction.equals("EDIT")) {
				if (selectedGiftCouponRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return editCouponRegisterPage();
			} else if (pageAction.equals("VIEW")) {
				if (selectedGiftCouponRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewCouponRegisterPage();
			} else if (pageAction.equals("DELETE")) {
				if (selectedGiftCouponRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
				// return LIST_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in callCouponRegisterPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editCouponRegisterPage() {
		log.info("CouponRegisterMasterBean :: editCouponRegisterPage");
		try {
			if (selectedGiftCouponRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			regionOffice = loginUserEntity;
			log.info("Selected increment  Id :: " + selectedGiftCouponRegister.getId());
			String getUrl = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/get/:id";
			String url = getUrl.replace(":id", selectedGiftCouponRegister.getId().toString());
			log.info("viewCouponRegisterPage url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				giftCouponRegister = mapper.readValue(jsonResponse, GiftCouponRegister.class);
				showroom = giftCouponRegister.getEntityMaster();
				log.info("showroom id==> " + showroom.getId());
				getRegionByShowroom(showroom.getId());
				loadShowRoom();
				couponFrom = Integer.parseInt(giftCouponRegister.getCouponNumber());
				log.info("couponFrom==> " + couponFrom);
				couponTo = Integer.parseInt(giftCouponRegister.getCouponNumber());
				log.info("couponTo==> " + couponTo);
				couponValue = giftCouponRegister.getCouponValue();
				log.info("couponValue==> " + couponValue);
				couponDate = giftCouponRegister.getValidityDate();
				log.info("couponDate==> " + couponDate);
				couponStatus = giftCouponRegister.getActiveStatus();
				log.info("couponStatus==> " + couponStatus);
				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.error(" Exception Occured While viewCouponRegisterPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String viewCouponRegisterPage() {
		log.info("CouponRegisterMasterBean :: viewCouponRegisterPage");
		try {
			if (selectedGiftCouponRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected increment  Id :: " + selectedGiftCouponRegister.getId());
			String getUrl = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/get/:id";
			String url = getUrl.replace(":id", selectedGiftCouponRegister.getId().toString());
			log.info("viewCouponRegisterPage url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				giftCouponRegister = mapper.readValue(jsonResponse, GiftCouponRegister.class);
				showroom = giftCouponRegister.getEntityMaster();
				log.info("showroom id==> " + showroom.getId());
				getRegionByShowroom(showroom.getId());
				couponFrom = Integer.parseInt(giftCouponRegister.getCouponNumber());
				log.info("couponFrom==> " + couponFrom);
				couponTo = Integer.parseInt(giftCouponRegister.getCouponNumber());
				log.info("couponTo==> " + couponTo);
				couponValue = giftCouponRegister.getCouponValue();
				log.info("couponValue==> " + couponValue);
				couponDate = giftCouponRegister.getValidityDate();
				log.info("couponDate==> " + couponDate);
				couponStatus = giftCouponRegister.getActiveStatus();
				log.info("couponStatus==> " + couponStatus);
				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error(" Exception Occured While viewCouponRegisterPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void getRegionByShowroom(Long showroomId) {
		log.info("CouponRegisterMasterBean :: getRegionByShowroom");
		try {
			log.info("getRegionByShowroom :: showroomId==> " + showroomId);
			if (showroomId != null) {
				String URL = AppUtil.getPortalServerURL() + "/entitymaster/getregionbyshowroom/" + showroomId;
				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					regionOffice = mapper.readValue(jsonResponse, new TypeReference<EntityMaster>() {
					});
					if (regionOffice != null) {
						log.info("regionOffice id==>" + regionOffice.getId());
					} else {
						log.error("Region office not found in showroom id : " + showroomId);
					}
				}
			} else {
				log.error("Show room id not found");
			}
		} catch (Exception e) {
			log.error(" Exception Occured While viewCouponRegisterPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String deleteCouponRegister() {
		log.info("CouponRegisterMasterBean :: deleteCouponRegister");
		try {
			if (selectedGiftCouponRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected increment  Id :: " + selectedGiftCouponRegister.getId());
			String getUrl = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/delete/id/:id";
			String url = getUrl.replace(":id", selectedGiftCouponRegister.getId().toString());
			log.info("deleteCouponRegister url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Coupon Register Deleted SuccessFully ");
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.COUPON_REGISTER_DELETED_SUCCESSFULLY).getCode());
				clearCouponRegister();
				loadLazyCouponRegister();
				return LIST_PAGE;
			} else {
				log.error("CouponRegister Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While deleteCouponRegister  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadShowRoom() {
		log.info("CouponRegisterMasterBean :: loadShowRoom");
		try {
			Long regionId = regionOffice.getId();
			// Long regionId = loginUserEntity.getEntityMasterRegion().getId();
			log.info("loadShowRoom :: regionId==> " + regionId);
			showroomList = commonDataService.loadShowroomListForRegion(regionId);
			if (showroomList != null) {
				log.info("loadShowRoom :: showroomList size==> " + showroomList.size());
			} else {
				log.error("Showroom not found");
			}
		} catch (Exception e) {
			log.error("Exception in loadShowRoom  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearCouponRegister() {
		regionOffice = new EntityMaster();
		showroom = new EntityMaster();
		selectedGiftCouponRegister = new GiftCouponRegister();
		couponFrom = null;
		couponTo = null;
		couponValue = null;
		couponDate = null;
		couponStatus = null;
		isAddButton = true;
		isEditButton = true;
		isDeleteButton = true;
	}

	public String cancelCouponRegister() {
		regionOffice = new EntityMaster();
		showroom = new EntityMaster();
		selectedGiftCouponRegister = new GiftCouponRegister();
		couponFrom = null;
		couponTo = null;
		couponValue = null;
		couponDate = null;
		couponStatus = null;
		isAddButton = false;
		isEditButton = true;
		isDeleteButton = true;
		return LIST_PAGE;
	}

	public void loadLazyCouponRegister() {
		log.info("<==== Starts CouponRegisterMasterBean.loadLazyCouponRegister =====>");
		lazyGiftCouponRegisterList = new LazyDataModel<GiftCouponRegister>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<GiftCouponRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/search";
					log.info("loadLazyCouponRegister :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						giftCouponRegisterList = mapper.readValue(jsonResponse,
								new TypeReference<List<GiftCouponRegister>>() {
								});
						log.info("loadLazyCouponRegister :: giftCouponRegisterList size==> "
								+ giftCouponRegisterList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyCouponRegister :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return giftCouponRegisterList;
			}

			@Override
			public Object getRowKey(GiftCouponRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public GiftCouponRegister getRowData(String rowKey) {
				for (GiftCouponRegister giftCouponRegister : giftCouponRegisterList) {
					if (giftCouponRegister.getId().equals(Long.valueOf(rowKey))) {
						selectedGiftCouponRegister = giftCouponRegister;
						return giftCouponRegister;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedGiftCouponRegister = ((GiftCouponRegister) event.getObject());
		if (selectedGiftCouponRegister != null) {
			isAddButton = true;
		} else {
			isAddButton = false;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public String saveGiftCouponRegister() {
		log.info("CouponRegisterMasterBean :: saveGiftCouponRegister");
		try {
			if (regionOffice == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.REGION_OFFICE_NOT_EMPTY).getCode());
				return null;
			}
			if (showroom == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SHOWROOM_NOT_EMPTY).getCode());
				return null;
			}

			if (couponFrom == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_NUMBER_FROM_NOT_EMPTY).getCode());
				return null;
			}
			if (couponFrom == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_FROM_ZERO_NOT_ALLOWED).getCode());
				return null;
			}
			if (couponTo == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_NUMBER_TO_NOT_EMPTY).getCode());
				return null;
			}
			if (couponTo == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_TO_ZERO_NOT_ALLOWED).getCode());
				return null;
			}
			if (couponFrom > couponTo) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.COUPON_NUMBER_FROM_GRATER_THAN_NUMBER_TO).getCode());
				return null;
			}
			if (couponValue == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_NUMBER_VALUE_NOT_EMPTY).getCode());
				return null;
			}
			if (couponValue == 0) {
				AppUtil.addWarn("Value Should Not be Zero");
				return null;
			}
			if (couponDate == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.COUPON_NUMBER_DATE_NOT_EMPTY).getCode());
				return null;
			}
			if (couponStatus == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.STATUS_NOT_EMPTY).getCode());
				return null;
			}
			if (pageAction.equals("EDIT")) {
				return updateCouponRegister();
			} else if (pageAction.equals("ADD")) {
				return saveCouponRegister();
			}
		} catch (Exception exception) {
			log.error("Exception in saveGiftCouponRegister  :: ", exception);
		}
		return null;
	}

	public String updateCouponRegister() {
		log.info("CouponRegisterMasterBean :: updateCouponRegister");
		try {
			log.info("giftcoupon id==> " + giftCouponRegister.getId());
			log.info("showroom id==> " + showroom.getId());
			log.info("couponFrom==> " + couponFrom);
			log.info("couponValue==> " + couponValue);
			log.info("couponDate==> " + couponDate);
			log.info("couponStatus==> " + couponStatus);

			giftCouponRegister.setId(giftCouponRegister.getId());
			giftCouponRegister.setEntityMaster(showroom);
			giftCouponRegister.setCouponNumber(Integer.toString(couponFrom));
			giftCouponRegister.setCouponValue(couponValue);
			giftCouponRegister.setValidityDate(couponDate);
			giftCouponRegister.setActiveStatus(couponStatus);

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/update";
			log.info("updateCouponRegister :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, giftCouponRegister);
			log.info("saveCouponRegister ::  baseDTO ==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				cancelCouponRegister();
				loadLazyCouponRegister();
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.COUPON_REGISTER_UPDATED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while update coupon rigister with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exception) {
			log.error("Exception in updateCouponRegister  :: ", exception);
		}
		return null;
	}

	public String saveCouponRegister() {
		log.info("CouponRegisterMasterBean :: saveCouponRegister");
		try {
			log.info("saveCouponRegister :: couponFrom==> " + couponFrom);
			log.info("saveCouponRegister :: couponTo==> " + couponTo);
			int balanceCouponValue = couponTo - couponFrom;
			log.info("saveCouponRegister :: balanceCouponValue==> " + balanceCouponValue);
			List<GiftCouponRegister> giftCouponRegisterList = new ArrayList<>();
			for (int couponStart = 0; couponStart <= balanceCouponValue; couponStart++) {
				giftCouponRegister = new GiftCouponRegister();
				log.info("saveCouponRegister :: showroom id==> " + showroom.getId());
				log.info("saveCouponRegister :: couponStart==> " + couponStart);
				log.info("saveCouponRegister :: couponValue==> " + couponValue);
				log.info("saveCouponRegister :: couponDate==> " + couponDate);
				log.info("saveCouponRegister :: couponStatus==> " + couponStatus);
				giftCouponRegister.setEntityMaster(showroom);
				giftCouponRegister.setCouponNumber(Integer.toString(couponFrom + couponStart));
				giftCouponRegister.setCouponValue(couponValue);
				giftCouponRegister.setValidityDate(couponDate);
				giftCouponRegister.setActiveStatus(couponStatus);
				giftCouponRegisterList.add(giftCouponRegister);

			}
			log.info("saveCouponRegister :: giftCouponRegisterList==> " + giftCouponRegisterList.size());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcouponregister/create";
			log.info("saveCouponRegister :: url==> " + url);
			BaseDTO baseDTO = httpService.post(url, giftCouponRegisterList);
			log.info("saveCouponRegister ::  baseDTO ==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				cancelCouponRegister();
				loadLazyCouponRegister();
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.COUPON_REGISTER_INSERTED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving coupon register with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in saveCouponRegister  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
}
