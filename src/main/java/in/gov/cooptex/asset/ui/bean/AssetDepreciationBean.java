package in.gov.cooptex.asset.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.asset.model.dto.AssetDepreciationRequestDTO;
import in.gov.cooptex.asset.model.dto.AssetDepreciationResponseDTO;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.MisPdfUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("assetDepreciationBean")
@Scope("session")

public class AssetDepreciationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6240298778894774766L;
	final String LIST_PAGE = "/pages/assetManagement/createAssetDepreciation.xhtml?faces-redirect=true";
	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	@Getter
	@Setter
	List<Map<String, Object>> responseList = new ArrayList<Map<String, Object>>();

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	String action;
	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String downloadPath;

	@Autowired
	LoginBean loginBean;

	MisPdfUtil misPdfUtil = new MisPdfUtil();

	@Getter
	@Setter
	String downloadFileName = "";

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	AssetDepreciationResponseDTO assetDepreciationResponseDTOPdf = new AssetDepreciationResponseDTO();

	@Getter
	@Setter
	List<EntityMaster> headandregionalofficelist = new ArrayList<>();

	@Getter
	@Setter
	List<EntityTypeMaster> meetingentitytypelist = new ArrayList<>();
	@Getter
	@Setter
	EntityMaster headangregionaofficeid;

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	EntityTypeMaster selctedentitytypeid;
	@Getter
	@Setter
	List<EntityMaster> assetDepreciationentitylist = new ArrayList<>();

	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterList = new ArrayList<AssetCategoryMaster>();

	int assetCategoryMasterListsize;

	@Getter
	@Setter
	List<AssetCategoryMaster> assetSubCategoryMasterList = new ArrayList<AssetCategoryMaster>();
	int assetSubCategoryMasterListsize;

	@Getter
	@Setter
	Long selectedId;

	@Getter
	@Setter
	Long subcatgryId;

	@Getter
	@Setter
	Long year;

	@Getter
	@Setter
	Long entityId;

	@Getter
	@Setter

	AssetDepreciationRequestDTO assetDepreciationRequestDTO = new AssetDepreciationRequestDTO();

	@Getter
	@Setter
	List<AssetDepreciationResponseDTO> searchResponseList = null;
	@Getter
	@Setter
	Double totalCurrentAmount = 0.0;
	@Getter
	@Setter
	Double totalDepreciationValue = 0.0;

	public String showAssetDepreciationPage() {
		userMasters = commonDataService.loadForwardToUsers();
		log.info("forward to user info" + userMasters);
		// lazyLoadAssetslist();
		loadheadandregionaloffice();
		loadmeetingentitytypelist();
		loadAssetCategoryList();
		searchResponseList = new ArrayList<>();
		setTotalCurrentAmount(0d);
		setTotalDepreciationValue(0d);
		setHeadangregionaofficeid(null);
		setEntityId(null);
		setSelctedentitytypeid(null);
		setSelectedId(null);
		setSubcatgryId(null);
		setYear(null);
		setVisibility(false);
		return LIST_PAGE;

	}

	public void loadheadandregionaloffice() {
		log.info("<<<< ----------Starts loadHeadAndRegionalOffice ------- >>>>");

		headandregionalofficelist = commonDataService.loadHeadAndRegionalOffice();
		log.info("ho/ro list////////////////", headandregionalofficelist);

		log.info("<<<< ----------Ends loadHeadAndRegionalOffice ------- >>>>");
	}

	public void loadmeetingentitytypelist() {
		log.info("<<<< ----------Starts loadMeretingEntityTypeList ------- >>>>");
		meetingentitytypelist = commonDataService.getAllEntityType();
		log.info("meeting entity type list..", meetingentitytypelist);

		log.info("<<<< ----------Ends loadMeretingEntityTypeList ------- >>>>");
	}

	public void headandregional() {
		log.info("<<<< ----------Starts filternig based on Head And Regional Office------- >>>>");
		log.info("entity master", headangregionaofficeid);

		try {

			if (headangregionaofficeid != null && headangregionaofficeid.getEntityTypeMaster() != null
					&& headangregionaofficeid.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
				visibility = true;
				setSelctedentitytypeid(null);
				setEntityId(null);

			} else {
				visibility = false;
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}

		log.info("<<<< ----------End filternig based on Head And Regional Office------- >>>>");
	}

	public void onSelectEntityType() {

		log.info("<<<< ----------Starts loadMeretinEntityList ------- >>>>");
		log.info("entity master", selctedentitytypeid);

		try {

			if (headangregionaofficeid != null && headangregionaofficeid.getId() != null && selctedentitytypeid != null
					&& selctedentitytypeid.getId() != null) {
				Long regionId = headangregionaofficeid.getId();
				assetDepreciationentitylist = commonDataService.loadEntityByregionOrentityTypeId(regionId,
						selctedentitytypeid.getId());
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<<<< ----------End loadMeretinEntityList ------- >>>>");

	}

	public void loadAssetCategoryList() {

		log.info("<<<< ----------Start laoMeetingType ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			assetCategoryMasterList = new ArrayList<AssetCategoryMaster>();

			String url = SERVER_URL + "/assetcategorymaster/getActiveAssetCategoryMaster";

			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				assetCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});
			}

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (assetCategoryMasterList != null) {
			assetCategoryMasterListsize = assetCategoryMasterList.size();
		} else {
			assetCategoryMasterListsize = 0;
		}
		log.info("<<<< ----------loadMeetingType  ------- >>>> " + assetCategoryMasterListsize);

	}

	public void onSelectAssetCategory() {
		log.info("<==============Selected Category==========>" + selectedId);

		log.info("<<<< ----------Start laoMeetingType ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			assetSubCategoryMasterList = new ArrayList<AssetCategoryMaster>();

			String url = SERVER_URL + "/assetcategorymaster/getActiveAssetSubCategoryMaster/" + selectedId;

			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				assetSubCategoryMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});
			}

		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (assetSubCategoryMasterList != null) {
			assetSubCategoryMasterListsize = assetSubCategoryMasterList.size();
		} else {
			assetSubCategoryMasterListsize = 0;
		}
		log.info("<<<< ----------loadMeetingType  ------- >>>> " + assetSubCategoryMasterListsize);

	}

	public void generateAssetDepreciation() {

		// AssetDepreciationResponseDTO searchResponseList = new
		// AssetDepreciationResponseDTO();

		try {

			String appValue = commonDataService.getAppKeyValueWithserverurl("PLANT_AD_MACHINARY", SERVER_URL);
			setTotalCurrentAmount(0d);
			setTotalDepreciationValue(0d);
			assetDepreciationRequestDTO.setYear(year);
			assetDepreciationRequestDTO.setPercentage(Double.parseDouble(appValue));

			assetDepreciationRequestDTO.setAssetCategoryId(selectedId);
			assetDepreciationRequestDTO.setAssetSubCategoryId(subcatgryId);
			if (headangregionaofficeid != null && headangregionaofficeid.getEntityTypeMaster() != null
					&& headangregionaofficeid.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
				assetDepreciationRequestDTO.setEntityId(headangregionaofficeid.getId());

			} else {
				assetDepreciationRequestDTO.setEntityId(entityId);
				assetDepreciationRequestDTO.setEntityTypeId(selctedentitytypeid.getId());
			}

			log.info("<========Search request=========>" + assetDepreciationRequestDTO);

			BaseDTO baseDTO = new BaseDTO();

			String url = SERVER_URL + "/assetdepreciation/search";

			baseDTO = httpService.post(url, assetDepreciationRequestDTO);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				searchResponseList = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetDepreciationResponseDTO>>() {

						});
				prepareStatement1HeaderNames(searchResponseList);

				for (AssetDepreciationResponseDTO totlcrrntamount : searchResponseList) {
					totalCurrentAmount = totlcrrntamount.getCurrentValue();
					totalDepreciationValue = totlcrrntamount.getDepreciationValue();
				}
			}

			log.info("create response", searchResponseList);
		} catch (Exception e) {
			log.error("found exception in onchangeEntityType: ", e);
		}

	}

	public String clear() {
		log.info("<========== assetDepreciationBean clear() ===========>");
		headangregionaofficeid = null;
		selctedentitytypeid = null;
		entityId = null;
		subcatgryId = null;
		selectedId = null;
		year = null;
		/*
		 * setEntityId(null); setSelctedentitytypeid(null); setSelectedId(null);
		 * setSubcatgryId(null); setYear(null); setVisibility(false);
		 */
		return LIST_PAGE;
	}

	public void downloadPdf() {
		InputStream input = null;
		try {
			generatePDF(responseList);
			File files = new File(downloadPath + "/" + downloadFileName);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
		} catch (FileNotFoundException e) {
			log.error("Exception at GstPaymentBean.downloadPdf() " + e);
		} catch (Exception exp) {
			log.error("Exception at GstPaymentBean.downloadPdf()" + exp);
		}
	}

	private void generatePDF(List<Map<String, Object>> response) {
		log.info("===========START ShowroomRegionwiseDnsReportBean.generatePDF =============");
		try {
			String content = commonDataService.getTemplateDetailsWithServerURL(TemplateCode.REPORT_TEMPLATE.toString(),
					AppUtil.getPortalServerURL());
			String finalContent = VelocityStringUtil.getFinalContent("", content, null);
			// log.info("finalContent: " + finalContent);
			String htmlData = generateHtml(assetDepreciationResponseDTOPdf, response, finalContent);
			downloadPath = commonDataService.getAppKeyValueWithserverurl(
					AppConfigEnum.REPORT_PDF_DOWNLOAD_PATH.toString(), AppUtil.getPortalServerURL());
			log.info("===========downloadPath is=============" + downloadPath.toString());

			String reportName = null;

			reportName = ReportNames.ASSET_DEPRIZIATION_REPORT;
			downloadFileName = AppUtil.getReportFileName(reportName, "pdf");
			misPdfUtil.createPDFlandscape(htmlData, downloadPath + "/" + downloadFileName, loginBean);

		} catch (Exception e) {
			log.info("===========Exception Occured in ShowroomRegionwiseDnsReportBean.generatePDF =============");
			log.info("Exception is =============", e);
		}
		log.info("===========END ShowroomRegionwiseDnsReportBean.generatePDF =============");
	}

	private String generateHtml(AssetDepreciationResponseDTO assetDepreciationResponseDTO,
			List<Map<String, Object>> response, String finalContent) {
		log.info("===========START CashClosingBalanceBean.generateHtml =============");
		try {

			Long index = (long) 0;
			DateFormat dateFormat = AppUtil.DATE_FORMAT;
			String fromDate = dateFormat.format(new Date());
			String toDate = dateFormat.format(new Date());
			String officeLocation = loginBean.getEmployee() == null ? ""
					: loginBean.getEmployee().getPersonalInfoEmployment() == null ? ""
							: loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() == null ? ""
									: loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getName();
			StringBuffer finalTableData = new StringBuffer(110);
			StringBuffer finalHeaderData = new StringBuffer(110);
			StringBuffer finalFooterData = new StringBuffer(110);
			String filterTypeData = "<tr> " + "<td style='width: 10%'>From Date</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>" + fromDate
					+ "</b></td>" + "<td style='width: 10%'>To Date</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>" + toDate
					+ "</b></td></tr> ";

			String[] stringHeaderNames = { "Entity", "Asset Category", "Asset Sub Category", "Asset Name", "Year",
					"Current Value", "Depriziation Value" };

			Set<String> headerNames = new HashSet<String>();
			for (Map<String, Object> map : response) {
				StringBuffer list = new StringBuffer();
				index++;

				headerNames = map.keySet();
				String beginTr = "<tr> " + "<td class='bor-left'>" + index + "</td>";
				list.append(beginTr);
				Iterator<String> setItr = headerNames.iterator();
				while (setItr.hasNext()) {
					String keyString = setItr.next();
					String td = new String();
					boolean isMatch = Arrays.stream(stringHeaderNames).anyMatch(keyString::equals);
					if (!isMatch) {
						if (map.get(keyString) != null) {
							td = "<td class='text-right'>" + AppUtil.formatIndianCommaSeparated(
									Double.valueOf(AppUtil.getFormattedValue(map.get(keyString)))) + "</td>";
						} else {
							td = "<td class='text-right'>" + "</td>";
						}
					} else {
						if (map.get(keyString) == null) {
							td = "<td class='text-left'> - </td>";
						} else {
							td = "<td class='text-left'>" + (map.get(keyString)) + "</td>";
						}

					}
					list.append(td);

				}
				String endTr = "</tr>";
				list.append(endTr);
				finalTableData.append(list);
			}
			String headerData = "<th style='width: 3%' class='bor-left'>#</th>";
			finalHeaderData.append(headerData);
			Iterator<String> setItr = headerNames.iterator();
			while (setItr.hasNext()) {
				String keyString = setItr.next();
				keyString = keyString.replace("{", "");
				keyString = keyString.replace("}", "");
				headerData = "<th>" + keyString + "</th>";
				finalHeaderData.append(headerData);
			}

			Iterator<String> footerItr = headerNames.iterator();

			prepareStatement1FooterStr(footerItr, finalFooterData);

			finalContent = finalContent.replace("$reportName", ReportNames.ASSET_DEPRIZIATION_REPORT);

			finalContent = finalContent.replace("$filterType", filterTypeData);
			finalContent = finalContent.replace("$columnData", finalHeaderData.toString());
			finalContent = finalContent.replace("$worklocation", officeLocation);
			finalContent = finalContent.replace("$reportListData", finalTableData.toString());
			finalContent = finalContent.replace("$footerData", finalFooterData.toString());

		} catch (

		Exception e) {
			log.info("Exception at GstPaymentBean.generateHtml() ", e);
		}

		return finalContent;
	}

	private void prepareStatement1FooterStr(Iterator<String> footerItr, StringBuffer finalFooterData) {
		Long footerIndex = (long) 0;
		try {
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Current Value")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + 6 + "'>Total</td> ";
					tfoot.append(tr);
					String td = new String();
					td = "<td class='text-center'>" + totalCurrentAmount + "</td>";
					tfoot.append(td);
					td = "<td class='text-center'>" + totalDepreciationValue + "</td>";
					tfoot.append(td);
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}
			}
		} catch (Exception e) {
			log.error("Exception at PrepareStatment1FooterData() ", e);
		}
	}

	private void prepareStatement1HeaderNames(List<AssetDepreciationResponseDTO> taxInvoiceList) {
		try {
			for (AssetDepreciationResponseDTO list : taxInvoiceList) {
				Map<String, Object> data = new LinkedHashMap<String, Object>();
				data.put("Entity", list.getEntity() == null ? "" : String.valueOf(list.getEntity()));
				data.put("Asset Category",
						list.getAssetCategory() == null ? "" : String.valueOf(list.getAssetCategory()));
				data.put("Asset Sub Category", list.getAssetSubCategory());
				data.put("Asset Name", list.getAssetName() == null ? "" : String.valueOf(list.getAssetName()));
				data.put("Year", list.getYear());
				data.put("Current Value", list.getCurrentValue());
				data.put("Depriziation Value", list.getDepreciationValue());

				responseList.add(data);
			}

		} catch (Exception e) {

		}
	}
}
