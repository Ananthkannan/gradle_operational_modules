package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.AppConfigRequest;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.GeneralErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.personnel.hrms.dto.DeathRegistrationDTO;
import in.gov.cooptex.personnel.hrms.dto.EmpSelfContributionDTO;
import in.gov.cooptex.personnel.hrms.model.EmpDeathRegister;
import in.gov.cooptex.personnel.hrms.model.EmpFbfContribution;
import in.gov.cooptex.personnel.hrms.model.EmpFlagdayContribution;
import in.gov.cooptex.personnel.hrms.model.NatureOfDeath;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("flagDayFundContributionBean")
@Scope("session")
@Log4j2
public class FlagDayFundContributionBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6473720670374867013L;
	
	private static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	private static final String FLAG_DAY_CONTRIBUTION_CREATE_PAGE = "/pages/personnelHR/employeeProfile/createFlagDayFundContribution.xhtml?faces-redirect=true";
	
	private static final String FLAG_DAY_CONTRIBUTION_LIST_PAGE = "/pages/personnelHR/employeeProfile/listFlagDayFundContribution.xhtml?faces-redirect=true";

	private static final String FLAG_DAY_CONTRIBUTION_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewFlagDayFundContribution.xhtml?faces-redirect=true";

	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	String jsonResponse;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Getter @Setter
	String action;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter @Setter
	List<String> finYearList = new ArrayList<>();
	
	@Getter @Setter
	boolean addButtonFlag = true;
	
	@Getter @Setter
	EmpFlagdayContribution empFlagdayContribution;
	
	@Getter @Setter
	EmpFlagdayContribution selectedEmpFlagdayContribution = new EmpFlagdayContribution();
	
	@Getter @Setter
	LazyDataModel<EmpFlagdayContribution> empFlagDayContributionLazyList;
	
	@Getter @Setter
	Integer totalRecords;
	
	public String showFlagDayContributionListPage() {
		log.info("<<====== FlagDayFundContributionBean:showFlagDayContributionListPage() ======>> ");
		try {
			empFlagdayContribution = new EmpFlagdayContribution();
			selectedEmpFlagdayContribution = new EmpFlagdayContribution();
			addButtonFlag = true;
			loadFlagDayContributionLazyList();
		}catch(Exception e) {
			log.error("Exception at FlagDayFundContributionBean:showFlagDayContributionListPage() - ",e);
		}
		return FLAG_DAY_CONTRIBUTION_LIST_PAGE;
	}
	
	public void loadValues() {
		log.info("<<======= FlagDayFundContributionBean:loadValues() Starts =======>> ");
		AppConfigRequest appConfigRequest = null;
		double flagDayMinContributionAmount = 0.00;
		try {
			appConfigRequest = new AppConfigRequest();
			Date currentDate = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentDate);
			empFlagdayContribution.setYear(cal.get(Calendar.YEAR));
			appConfigRequest.setAppKey(AppConfigEnum.FLAG_DAY_MIN_CONTRIBUTION_AMOUNT.toString());
			flagDayMinContributionAmount = getFlagDayMinContributionAmt(appConfigRequest);
			log.info("<<======== FlagDay Minimum Contribution Amount =======>> "+flagDayMinContributionAmount);
			empFlagdayContribution.setMinimumContribution(flagDayMinContributionAmount);
			empFlagdayContribution.setTotalContribution(flagDayMinContributionAmount);
		} catch (Exception exp) {
			log.error("Exception in FlagDayFundContributionBean:loadValues() ", exp);
		}
		log.info("<<======= FlagDayFundContributionBean:loadValues() Ends =======>> ");
	}
	
	public void voluntaryContributionChange() {
		log.info("<<======= FlagDayFundContributionBean:voluntaryContributionChange() Starts =======>> ");
		double minContributionAmount = 0.00;
		double voluntaryAmount = 0.00;
		double totalAmount = 0.00;
		try {
			minContributionAmount = empFlagdayContribution.getMinimumContribution() == null ?
														0.00 : empFlagdayContribution.getMinimumContribution();
			voluntaryAmount =  empFlagdayContribution.getVoluntaryContribution() == null ?
					0.00 : empFlagdayContribution.getVoluntaryContribution();
			totalAmount = minContributionAmount + voluntaryAmount;
			empFlagdayContribution.setTotalContribution(totalAmount);
		}catch (Exception exp) {
			log.error("Exception in FlagDayFundContributionBean:voluntaryContributionChange() ", exp);
		}
		log.info("<<======= FlagDayFundContributionBean:voluntaryContributionChange() Ends =======>> ");
	}
	
	public double getFlagDayMinContributionAmt(AppConfigRequest appConfigRequest) {
		log.info("<<======= FlagDayFundContributionBean:getFlagDayMinContributionAmt() Starts =======>> ");
		double amount = 0.00;
		try {
			String requestPath = SERVER_URL + "/appConfig/getvalue";
			BaseDTO baseDTO = httpService.post(requestPath,appConfigRequest);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					amount = mapper.readValue(jsonResponse, double.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in FlagDayFundContributionBean:getFlagDayMinContributionAmt() ", exp);
		}
		log.info("<<======= FlagDayFundContributionBean:getFlagDayMinContributionAmt() Ends =======>> ");
		return amount;
	}
	
	public String flagDayContributionListAction() {
		log.info("<====== FlagDayFundContributionBean:flagDayContributionListAction() Starts =====>");
		try {
			if(action.equalsIgnoreCase("CREATE")) {
				log.info("<====== Flag Day Fund Contribution Create Page Calling =====>");
				empFlagdayContribution = new EmpFlagdayContribution();
				loadValues();
				return FLAG_DAY_CONTRIBUTION_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception Occured in FlagDayFundContributionBean:flagDayContributionListAction", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== FlagDayFundContributionBean:flagDayContributionListAction Ends====>");
		return null;
	}
	
	public void getEmployeeDetailsByUserId() {
		log.info("<<======= FlagDayFundContributionBean:getEmployeeDetailsByUserId() =======>> ");
		try {
			Long userId = loginBean.getUserDetailSession().getId();
			if(userId != null) {
				String url = SERVER_URL+ "/employee/getemployeedetailsbyloginuser/"+userId;
				log.info("url" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					EmployeeMaster employeeMaster = mapper.readValue(jsonValue, EmployeeMaster.class);
					if(employeeMaster == null) {
						log.info("Employee Master Not Found");
					}
					empFlagdayContribution.setEmpId(employeeMaster == null ? null : employeeMaster.getId());
					empFlagdayContribution.setCurrentDepartmentCode(employeeMaster.getPersonalInfoEmployment() == null ? null :
						employeeMaster.getPersonalInfoEmployment().getCurrentDepartment() == null ? null :
							employeeMaster.getPersonalInfoEmployment().getCurrentDepartment().getCode());
				}
			}
		}catch(Exception e) {
			log.error("Exception at FlagDayFundContributionBean:getEmployeeDetailsByUserId() - ",e);
		}
	}
	
	public String saveFlagDayFundContribution() {
		log.info("<<======= FlagDayFundContributionBean:saveFlagDayFundContribution() Starts ======>> ");
		BaseDTO baseDTO = null;
		try {
			String URL = SERVER_URL + "/flagdaycontribution/save";
			getEmployeeDetailsByUserId();
			empFlagdayContribution.setTotalContribution(empFlagdayContribution.getVoluntaryContribution() == 0D ? 0D
					: empFlagdayContribution.getTotalContribution());
			baseDTO = httpService.post(URL, empFlagdayContribution);
			if (baseDTO.getStatusCode() == 0) {
				if ("CREATE".equalsIgnoreCase(action)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.FLAG_DAY_CONTRIBUTION_SAVE_SUCCESS).getErrorCode());
				} else if ("EDIT".equalsIgnoreCase(action)) {
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.FLAG_DAY_CONTRIBUTION_UPDATE_SUCCESS).getErrorCode());
				}
				return showFlagDayContributionListPage();
			}else if (baseDTO.getStatusCode() == ErrorDescription.getError(PersonnalErrorCode.FLAG_DAY_CONTRIBUTION_EXIST).getErrorCode()) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.FLAG_DAY_CONTRIBUTION_EXIST).getErrorCode());
				return null;
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception at FlagDayFundContributionBean:saveFlagDayFundContribution()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<<======= FlagDayFundContributionBean:onRowSelect() Calling =======>>");
		try {
			addButtonFlag = false;
		}catch(Exception e) {
			log.error("Exception at FlagDayFundContributionBean:onRowSelect()",e);
		}
	}

	
	private void loadFlagDayContributionLazyList() {
		log.info("<<============== FlagDayFundContributionBean:loadFlagDayContributionLazyList() Strats ===========>> ");
		empFlagDayContributionLazyList = new LazyDataModel<EmpFlagdayContribution>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 2559910409973223615L;

			public List<EmpFlagdayContribution> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<EmpFlagdayContribution> empFlagdayContributionList = null;
				try {
					empFlagdayContributionList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					getEmployeeDetailsByUserId();
					paginationRequest.setEmpId(empFlagdayContribution.getEmpId());
					paginationRequest.setCurrentDepartmentCode(empFlagdayContribution.getCurrentDepartmentCode());
					log.info("Pagination request :::" + paginationRequest);
					url = AppUtil.getPortalServerURL() + "/flagdaycontribution/loadLazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						empFlagdayContributionList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpFlagdayContribution>>() {
								});
						log.info("Employee Flag Day Contribtuion List Size - " + empFlagdayContributionList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadEmployeeFBFContributionLazyList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return empFlagdayContributionList;
			}

			@Override
			public Object getRowKey(EmpFlagdayContribution res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpFlagdayContribution getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmpFlagdayContribution> responseList = (List<EmpFlagdayContribution>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmpFlagdayContribution empFlagDayContributionObj : responseList) {
					if (empFlagDayContributionObj.getId().longValue() == value.longValue()) {
						selectedEmpFlagdayContribution = empFlagDayContributionObj;
						return empFlagDayContributionObj;
					}
				}
				return null;
			}
		};
	}
	
	public String gotoViewPage() {
		log.info("<<========= FlagDayFundContributionBean:gotoViewPage() =======>> ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (selectedEmpFlagdayContribution == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if(action.equals("EDIT")) {
				String status = selectedEmpFlagdayContribution.getContributionStatus();
				if(StringUtils.isNotEmpty(status)) {
					if(status.equals(ApprovalStage.ACCEPTED)) {
						errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_EDIT_ACCEPTED_RECORD).getErrorCode());
						log.info("<========== Inside Cannot Edit Record =========>");
						return null;
					}
				}
			}
				
			String URL = SERVER_URL+"/flagdaycontribution/getById/"+selectedEmpFlagdayContribution.getId();
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				empFlagdayContribution = objectMapper.readValue(jsonResponse, EmpFlagdayContribution.class);
				if(action.equals("VIEW")) {
					return FLAG_DAY_CONTRIBUTION_VIEW_PAGE;
				}else if(action.equals("EDIT")) {
					return FLAG_DAY_CONTRIBUTION_CREATE_PAGE;
				}
			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at EmployeePfSelfContributionBean:gotoVoluntaryProcessViewPage()", e);
		}
		return null;
	}
	
	public String deleteFlagDayContribution() {
		log.info("<<========= FlagDayFundContributionBean:deleteFlagDayContribution() =======>> ");
		try {
			if (selectedEmpFlagdayContribution == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String status = selectedEmpFlagdayContribution.getContributionStatus();
			if(StringUtils.isNotEmpty(status)) {
				if(status.equals(ApprovalStage.ACCEPTED)) {
					errorMap.notify(ErrorDescription.getError(GeneralErrorCode.CANNOT_DELETE_ACCEPTED_RECORD).getErrorCode());
					log.info("<========== Inside Cannot Delete Record =========>");
					return null;
				}
			}
			RequestContext.getCurrentInstance().execute("PF('confirmContributionDelete').show();");
		}catch(Exception e) {
			log.error("Exception at FlagDayFundContributionBean:deleteFlagDayContribution()",e);
		}
		return null;
	}
	
	public String deleteConfirmFlagDayContribution() {
		log.info("<<========= FlagDayFundContributionBean:deleteConfirmFlagDayContribution() =======>> ");
		String url;
		try {
			log.info("Request id ----------->" + selectedEmpFlagdayContribution.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = SERVER_URL+"/flagdaycontribution/deleteById/"+selectedEmpFlagdayContribution.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.FLAG_DAY_CONTRIBUTION_DELETE_SUCCESS).getErrorCode());
				RequestContext.getCurrentInstance().execute("PF('confirmContributionDelete').hide();");
				addButtonFlag = true;
				return showFlagDayContributionListPage();
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("Exception at FlagDayFundContributionBean:deleteConfirmFlagDayContribution()", e);
		}
		return null;
	}
	
}
