package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.asset.model.ModernizationRequest;
import in.gov.cooptex.asset.model.ModernizationRequestLog;
import in.gov.cooptex.asset.model.ModernizationRequestNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityInfoDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.RequestModForwardDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dto.pos.ModernizationRequestDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("modernizationBean")
public class ModernizationBean extends CommonBean implements Serializable {

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/modernizationrequest";

	final String ADD_PAGE = "/pages/assetManagement/createRequestForModernization.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/assetManagement/viewRequestForModernization.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/assetManagement/listRequestForModernization.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String pageAction, fileName;

	@Getter
	@Setter
	Long loggedInuserId;

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	EntityMaster headRegionOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();

	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	ModernizationRequest modernizationRequest = new ModernizationRequest();

	@Getter
	@Setter
	EntityInfoDTO entityInfoDTO = new EntityInfoDTO();

	@Getter
	@Setter
	List<ModernizationRequestLog> logList = new ArrayList<ModernizationRequestLog>();

	@Getter
	@Setter
	List<ModernizationRequestNote> requestNoteList = new ArrayList<ModernizationRequestNote>();

	@Getter
	@Setter
	ModernizationRequestNote lastNote;

	@Getter
	@Setter
	LazyDataModel<ModernizationRequestDTO> modRequestLazyList;

	@Getter
	@Setter
	List<ModernizationRequestDTO> modRequestList;

	@Getter
	@Setter
	ModernizationRequestDTO selectedModRequest = new ModernizationRequestDTO();

	@Getter
	@Setter
	String nameOfBuinding, docPath, note, modFileName;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	ObjectMapper objectMapper = new ObjectMapper();

	long employeePhotoSize;

	@Getter
	@Setter
	private UploadedFile modernizationfile;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	String modRemarks;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalRenderFlag = true;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean submitButtonFlag = true;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	// @Autowired
//	UserMaster userMaster;

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("modernization_request_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			init();
			selectedModRequest = new ModernizationRequestDTO();
			pageAction = "VIEW";
			selectedModRequest.setId(Long.parseLong(circularId));
			selectedNotificationId = Long.parseLong(notificationId);

			viewModernizationRequest();
		}
		log.info("ModerizationBean showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public void init() {

		log.info("ModernizationBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadModernizationValues();
		loadAppConfigValues();
		loadLazyModernizationRequestList();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		loggedInuserId = loginBean.getUserMaster().getId();

	}

	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public void loadModernizationValues() {
		try {
			log.info("ModernizationBean loadModernizationValues.........");
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			entityTypeList = commonDataService.getEntityTypeByStatus();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REQUEST_FOR_MODERNIZATION");
			// forwardToUsersList = commonDataService.loadForwardToUsers();
		} catch (Exception e) {
			log.error("Exception in loadModernizationValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String loadModernizationPage() {
		try {
			log.info("ModernizationBean loadModernizationPage.........");
			if (pageAction.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				loadLazyModernizationRequestList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				modernizationRequest.setDescription(null);
				headRegionOffice = null;
				modFileName = "";
				entityTypeMaster = null;
				entityMaster = null;
				nameOfBuinding = "";

				return ADD_PAGE;
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedModRequest == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return editModernizationRequest();
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedModRequest == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewModernizationRequestData();
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: Modernization Request Delete Response :: ");
				if (selectedModRequest == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			}
		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editModernizationRequest() {
		try {

			log.info("modReqId==>" + selectedModRequest.getId());

			// String getUrl = SERVER_URL + "/getbyid/:id";
			String getUrl = SERVER_URL + "/getbyid/:id" + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			// String getUrl = SERVER_URL + "/getbyid/:id";
			url = getUrl.replace(":id", selectedModRequest.getId().toString());

			log.info("editModernizationRequest URL called : - " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ModernizationRequest modRequest = mapper.readValue(jsonResponse, ModernizationRequest.class);

			String jsonResponse1 = mapper.writeValueAsString(baseDTO.getResponseContents());
			employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
			});

			if (baseDTO.getStatusCode() == 0) {
				if (modRequest != null) {
					modernizationRequest = modRequest;
					entityMaster = modernizationRequest.getEntityMaster();
					nameOfBuinding = modernizationRequest.getBuildingName();
					modFileName = modernizationRequest.getDocumentPath();
					log.info("entity id==>" + entityMaster.getId());
					entityTypeMaster = commonDataService.getEntityType(entityMaster.getId());

					log.info("entity type id==>" + entityTypeMaster.getId());

					getHoRoOffice(modernizationRequest.getId());
					loadEntityList();

					if (modernizationRequest.getModernizationRequestNoteList() != null
							&& !modernizationRequest.getModernizationRequestNoteList().isEmpty()) {
						forwardToUser = modernizationRequest.getModernizationRequestNoteList()
								.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo();
						forwardFor = modernizationRequest.getModernizationRequestNoteList()
								.get(modernizationRequest.getModernizationRequestNoteList().size() - 1)
								.getFinalApproval();
						note = modernizationRequest.getModernizationRequestNoteList()
								.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getNote();
					}
				} else {
					log.error("employee object failed to desearlize");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				log.info(" Employee Retrived  SuccessFully ");
			}

			else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return ADD_PAGE;
		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void getHoRoOffice(Long modId) {
		log.info("<--- Inside getDnpOfficeById() with D&P Office ID---> " + modId);

		String URL = SERVER_URL + "/gethorooffice/" + modId;
		log.info("<--- getHoRoOffice() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		try {
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					// headRegionOffice = objectMapper.readValue(jsonResponse, EntityMaster.class);
					List<EntityMaster> entMaster = objectMapper.readValue(jsonResponse,
							new TypeReference<List<EntityMaster>>() {
							});

					log.info("entMaster==>" + entMaster.size());
					if (entMaster != null) {
						headRegionOffice = entMaster.get(0);
					}

				} else {
					log.error("<--- Entity Not Found ---> ");
					errorMap.notify(ErrorDescription.ERROR_ENTITY_NOTFOUND.getCode());
				}
			} else {
				log.error("<--- Internal Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("<--- Failure Response ---> ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String viewModernizationRequest() {
		log.info("<<============ ModernizationBean --- viewModernizationRequest ====##STARTS  ");
		try {

			modernizationRequest = new ModernizationRequest();

			finalapprovalFlag = false;

			log.info("<<============ document path ====##STARTS  " + modernizationRequest.getDocumentPath());
			modernizationRequest.setDocumentPath(null);
			fileName = null;

//			if(getForwardToUser().getUsername().equals(loginBean.getUserDetailSession().getUsername())){
//				
//				forwardToUser = new UserMaster();
//			}

			log.info("<<============ document path ====##ends  " + modernizationRequest.getDocumentPath());

			/*
			 * if(!loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper"
			 * )){ loginBean.getUserDetailSession().setUserDisplayName(null); }
			 */

			Long modReqId = selectedModRequest.getId();
			String status = selectedModRequest.getStage();
			log.info("statussss" + status);
			log.info("submittedstatus" + submitButtonFlag);
			log.info("modReqId==>" + modReqId);

			String url = SERVER_URL + "/getbyid/" + modReqId + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null || baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				modernizationRequest = mapper.readValue(jsonResponse, ModernizationRequest.class);
				String jsonResponse1 = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
				});

				if (selectedNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}
			}
			// log.info(" EMPNAME : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getEmpName());
			// log.info(" USERNAME : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getUsername());
			// log.info(" DESIGNATION : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getDesignation());
			url = SERVER_URL + "/getentityinfo/" + modReqId;
			baseDTO = httpService.get(url);
			if (baseDTO != null || baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityInfoDTO = mapper.readValue(jsonResponse, EntityInfoDTO.class);
			}
			logList = modernizationRequest.getModRequestLogList();
			requestNoteList = modernizationRequest.getModernizationRequestNoteList();

			// log.info("noteeeeee" +requestNoteList );
			if (requestNoteList != null && requestNoteList.size() > 0) {
				lastNote = requestNoteList.get(requestNoteList.size() - 1);
				forwardToUser = modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo();
				forwardFor = modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getFinalApproval();
			}
			if (modernizationRequest.getDocumentPath() != null) {
				String[] filepatharray = modernizationRequest.getDocumentPath().split("/");
				fileName = filepatharray[filepatharray.length - 1];
			}

			log.info("checkingggg" + modernizationRequest.getModernizationRequestNoteList()
					.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());
			log.info("forrwardddd" + (modernizationRequest.getModernizationRequestNoteList().size() - 1));
			log.info("ssss" + modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo());

			log.info("finalapprovestatusss" + forwardFor);
			ModernizationRequestNote mrNote = new ModernizationRequestNote();
//			ModernizationRequestNote mrnote = modernizationRequest.getModernizationRequestNoteList().stream().filter(o->o.getFinalApproval() == true).collect(Collectors.mapping(ModernizationRequestNote))).;
			List<ModernizationRequestNote> modernizationRequestNoteList = modernizationRequest
					.getModernizationRequestNoteList().stream()
					.sorted(Comparator.comparingLong(ModernizationRequestNote::getId)).collect(Collectors.toList());
			modernizationRequest.setModernizationRequestNoteList(modernizationRequestNoteList);
			for (ModernizationRequestNote mr : modernizationRequest.getModernizationRequestNoteList()) {
				log.info("Forward : " + mr.getForwardTo());
				if (mr.getFinalApproval() == true) {
					mrNote = mr;
					break;
				}
			}
			log.info("finalapprovestatusss" + mrNote.getFinalApproval());
			log.info("finalapproveid" + mrNote.getForwardTo());
			log.info("list sizeeeee" + modernizationRequest.getModernizationRequestNoteList().size());
			log.info("forrrrrr" + modernizationRequest.getModernizationRequestNoteList()
					.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());
			// if
			// (modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getUsername().equalsIgnoreCase(loginBean.getUserDetailSession().getUsername()))
			// {

			if (loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper") || mrNote != null) {

				setHideflag(false);
				setSubmitButtonFlag(false);
				// forwardToUser = new UserMaster();

			}

			// if(mrNote.getFinalApproval()== true &&
			// loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper") ){

			// setHideflag(false);
			/// setSubmitButtonFlag(false);

			// }

			// if(mrNote.getFinalApproval()== true &&
			// !loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper")
			// ){

			// setHideflag(false);
			// setSubmitButtonFlag(false);

			// }

			if (mrNote != null && mrNote.getForwardTo() != null) {
				log.info("loginiddddd" + loginBean.getUserDetailSession().getId());
				log.info("forwardded" + mrNote.getForwardTo().getId());
				log.info("forwardded" + mrNote.getFinalApproval());
				if (mrNote.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())) {

					setSubmitButtonFlag(true);
					setFinalapprovalFlag(true);
					setHideflag(false);

				} else {
					setHideflag(false);
					setSubmitButtonFlag(false);
				}
			}

			if (mrNote.getForwardTo() == null) {

				log.info("ffff" + modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());

				if (!modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo().getId()
						.equals(loginBean.getUserDetailSession().getId())) {
					setHideflag(false);
					setSubmitButtonFlag(false);

				}

				if (modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo().getId()
						.equals(loginBean.getUserDetailSession().getId())) {
					setHideflag(true);
					setSubmitButtonFlag(true);

				}

			}
			if (selectedNotificationId != null || selectedNotificationId == 0) {
				forwardToUser = new UserMaster();
			}

			// if(
			// mrNote.getForwardTo().equals(loginBean.getUserMaster().getUsername().equals("hosuper")))
			// {

			// setHideflag(true);
			// setSubmitButtonFlag(true);

			// }

			if (getForwardToUser().getUsername().equals(loginBean.getUserDetailSession().getUsername())) {

				forwardToUser = new UserMaster();
			}

			forwardToUser = new UserMaster();

			log.info("<<==================  Log List:: " + logList.size());
			log.info("<<==================  documentpath:: " + modernizationRequest.getDocumentPath());
			log.info("<<==================  Note:: " + requestNoteList.size());
			log.info("<<============ ModernizationBean --- viewModernizationRequest ====##ENDS  ");
			return VIEW_PAGE;
		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<<============ ModernizationBean --- viewModernizationRequest ====##ENDS  ");
		return null;
	}

	public String viewModernizationRequestData() {
		log.info("<<============ ModernizationBean --- viewModernizationRequest ====##STARTS  ");
		try {

			modernizationRequest = new ModernizationRequest();

			finalapprovalFlag = false;

			log.info("<<============ document path ====##STARTS  " + modernizationRequest.getDocumentPath());
			modernizationRequest.setDocumentPath(null);
			fileName = null;

//			if(getForwardToUser().getUsername().equals(loginBean.getUserDetailSession().getUsername())){
//				
//				forwardToUser = new UserMaster();
//			}

			log.info("<<============ document path ====##ends  " + modernizationRequest.getDocumentPath());

			/*
			 * if(!loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper"
			 * )){ loginBean.getUserDetailSession().setUserDisplayName(null); }
			 */

			Long modReqId = selectedModRequest.getId();
			String status = selectedModRequest.getStage();
			log.info("statussss" + status);
			log.info("submittedstatus" + submitButtonFlag);
			log.info("modReqId==>" + modReqId);

			String url = SERVER_URL + "/getbyid/" + modReqId + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null || baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				modernizationRequest = mapper.readValue(jsonResponse, ModernizationRequest.class);
				String jsonResponse1 = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
				});

			}
			// log.info(" EMPNAME : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getEmpName());
			// log.info(" USERNAME : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getUsername());
			// log.info(" DESIGNATION : " +
			// modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getDesignation());
			url = SERVER_URL + "/getentityinfo/" + modReqId;
			baseDTO = httpService.get(url);
			if (baseDTO != null || baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityInfoDTO = mapper.readValue(jsonResponse, EntityInfoDTO.class);
			}
			logList = modernizationRequest.getModRequestLogList();
			requestNoteList = modernizationRequest.getModernizationRequestNoteList();

			// log.info("noteeeeee" +requestNoteList );
			if (requestNoteList != null && requestNoteList.size() > 0) {
				lastNote = requestNoteList.get(requestNoteList.size() - 1);
				forwardToUser = modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo();
				forwardFor = modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getFinalApproval();
			}
			if (modernizationRequest.getDocumentPath() != null) {
				String[] filepatharray = modernizationRequest.getDocumentPath().split("/");
				fileName = filepatharray[filepatharray.length - 1];
			}

			log.info("checkingggg" + modernizationRequest.getModernizationRequestNoteList()
					.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());
			log.info("forrwardddd" + (modernizationRequest.getModernizationRequestNoteList().size() - 1));
			log.info("ssss" + modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo());

			log.info("finalapprovestatusss" + forwardFor);
			ModernizationRequestNote mrNote = new ModernizationRequestNote();
//			ModernizationRequestNote mrnote = modernizationRequest.getModernizationRequestNoteList().stream().filter(o->o.getFinalApproval() == true).collect(Collectors.mapping(ModernizationRequestNote))).;
			List<ModernizationRequestNote> modernizationRequestNoteList = modernizationRequest
					.getModernizationRequestNoteList().stream()
					.sorted(Comparator.comparingLong(ModernizationRequestNote::getId)).collect(Collectors.toList());
			modernizationRequest.setModernizationRequestNoteList(modernizationRequestNoteList);
			for (ModernizationRequestNote mr : modernizationRequest.getModernizationRequestNoteList()) {
				log.info("Forward : " + mr.getForwardTo());
				if (mr.getFinalApproval() == true) {
					mrNote = mr;
					break;
				}
			}
			log.info("finalapprovestatusss" + mrNote.getFinalApproval());
			log.info("finalapproveid" + mrNote.getForwardTo());
			log.info("list sizeeeee" + modernizationRequest.getModernizationRequestNoteList().size());
			log.info("forrrrrr" + modernizationRequest.getModernizationRequestNoteList()
					.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());
			// if
			// (modernizationRequest.getModernizationRequestNoteList().get(0).getForwardTo().getUsername().equalsIgnoreCase(loginBean.getUserDetailSession().getUsername()))
			// {

			if (loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper") || mrNote != null) {

				setHideflag(false);
				setSubmitButtonFlag(false);
				// forwardToUser = new UserMaster();

			}

			// if(mrNote.getFinalApproval()== true &&
			// loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper") ){

			// setHideflag(false);
			/// setSubmitButtonFlag(false);

			// }

			// if(mrNote.getFinalApproval()== true &&
			// !loginBean.getUserDetailSession().getUsername().equalsIgnoreCase("hosuper")
			// ){

			// setHideflag(false);
			// setSubmitButtonFlag(false);

			// }

			if (mrNote != null && mrNote.getForwardTo() != null) {
				log.info("loginiddddd" + loginBean.getUserDetailSession().getId());
				log.info("forwardded" + mrNote.getForwardTo().getId());
				log.info("forwardded" + mrNote.getFinalApproval());
				if (mrNote.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())) {

					setSubmitButtonFlag(true);
					setFinalapprovalFlag(true);
					setHideflag(false);

				} else {
					setHideflag(false);
					setSubmitButtonFlag(false);
				}
			}

			if (mrNote.getForwardTo() == null) {

				log.info("ffff" + modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo());

				if (!modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo().getId()
						.equals(loginBean.getUserDetailSession().getId())) {
					setHideflag(false);
					setSubmitButtonFlag(false);

				}

				if (modernizationRequest.getModernizationRequestNoteList()
						.get(modernizationRequest.getModernizationRequestNoteList().size() - 1).getForwardTo().getId()
						.equals(loginBean.getUserDetailSession().getId())) {
					setHideflag(true);
					setSubmitButtonFlag(true);

				}

			}

			if (selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")) {
				setHideflag(false);
				setSubmitButtonFlag(false);
			}
			if (selectedModRequest.getStage().equalsIgnoreCase("REJECTED")) {
				setHideflag(false);
				setSubmitButtonFlag(false);
			}

			// if(
			// mrNote.getForwardTo().equals(loginBean.getUserMaster().getUsername().equals("hosuper")))
			// {

			// setHideflag(true);
			// setSubmitButtonFlag(true);

			// }

			if (getForwardToUser().getUsername().equals(loginBean.getUserDetailSession().getUsername())) {

				forwardToUser = new UserMaster();
			}

			forwardToUser = new UserMaster();

			log.info("<<==================  Log List:: " + logList.size());
			log.info("<<==================  documentpath:: " + modernizationRequest.getDocumentPath());
			log.info("<<==================  Note:: " + requestNoteList.size());
			log.info("<<============ ModernizationBean --- viewModernizationRequest ====##ENDS  ");
			return VIEW_PAGE;
		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<<============ ModernizationBean --- viewModernizationRequest ====##ENDS  ");
		return null;
	}

	public void loadEntityList() {
		log.info("ModernizationBean loadEntityList.........");
		try {

			if (headRegionOffice != null && headRegionOffice.getId() != null && entityTypeMaster != null
					&& entityTypeMaster.getId() != null) {
				Long regionId = headRegionOffice.getId();
				Long entityId = entityTypeMaster.getId();
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityId);
			} else {
				log.info("entity not found.");
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<=Ends ModernizationBean loadEntityList =>");
	}

	public void loadEntityTypeBasedOnHoRo() {
		if (headRegionOffice != null && headRegionOffice.getId() != null) {
			entityTypeList = new ArrayList<>();
			entityList = new ArrayList<>();

			entityTypeList = commonDataService.loadEntityTypeBasedOnHoRo(headRegionOffice.getId());
			entityList = commonDataService.getEntityByHeadOrRegionalOfficeId(headRegionOffice.getId());
		}

	}

	public String deleteModernizationRequest() {
		try {
			if (selectedModRequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + selectedModRequest.getId());
			String getUrl = SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedModRequest.getId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Modernization Request Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.MODERNIZATION_REQUEST_DELETED_SUCCESS.getErrorCode());
				clear();
				loadLazyModernizationRequestList();
				return LIST_PAGE;
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String saveModernizationRequest() {
		log.info("ModernizationBean saveModernizationRequest.........");
		try {

			if (headRegionOffice == null) {
				log.info("Invalid HO/RO");
				errorMap.notify(ErrorDescription.HO_RO_EMPTY.getErrorCode());
				return null;
			}

			/*
			 * if(headRegionOffice.getEntityTypeMaster() != null) {
			 * if(!EntityType.HEAD_OFFICE.equalsIgnoreCase(headRegionOffice.
			 * getEntityTypeMaster().getEntityCode())) { if(entityTypeMaster == null) {
			 * log.info("Invalid ENTITY TYPE");
			 * errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
			 * return null; } if(entityMaster == null) { log.info("Invalid ENTITY");
			 * errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_MASTER.getErrorCode()
			 * ); return null; } } if ("HEAD_OFFICE"
			 * .equalsIgnoreCase(headRegionOffice.getEntityTypeMaster().getEntityCode())) {
			 * setEntityMaster(headRegionOffice); } }
			 */

			if (headRegionOffice.getEntityTypeMaster() != null) {
				if ("HEAD_OFFICE".equalsIgnoreCase(headRegionOffice.getEntityTypeMaster().getEntityCode())) {
					setEntityMaster(headRegionOffice);
				}
			}

			if (!headRegionOffice.getName().equalsIgnoreCase("HEAD OFFICE")) {
				if (entityTypeMaster == null) {
					log.info("Invalid ENTITY TYPE");
					errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
					return null;
				}
			}

			if (!headRegionOffice.getName().equalsIgnoreCase("HEAD OFFICE")) {
				if (entityMaster == null) {
					log.info("Invalid ENTITY");
					errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_MASTER.getErrorCode());
					return null;
				}
			}

			if (nameOfBuinding == null || nameOfBuinding.isEmpty()) {
				log.info("Invalid ENTITY");
				errorMap.notify(ErrorDescription.NAME_OF_BUILDING_EMPTY.getErrorCode());
				return null;
			}

			if (forwardToUser == null) {
				log.info("Invalid forwardTo");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_FORWARD_ALREADY_EXIST.getErrorCode());
				return null;
			}
			if (forwardFor == null) {
				log.info("Invalid forward FOR");
				errorMap.notify(ErrorDescription.NEW_INVESTMENT_FORWARD_FOR_NULL.getErrorCode());
				return null;
			}
			if (note == null || note.length() == 0) {
				log.info("Note Not Valid");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_USER_INVALID_NOTE.getErrorCode());
				return null;
			}
			log.info("saveModernizationRequest docPath==>" + docPath);
			modernizationRequest.setEntityMaster(entityMaster);
			modernizationRequest.setBuildingName(nameOfBuinding);
			modernizationRequest.setActiveStatus(true);
			modernizationRequest.setDocumentPath(docPath);

			if (modernizationRequest.getModernizationRequestNoteList() != null
					&& !modernizationRequest.getModernizationRequestNoteList().isEmpty()) {
				for (ModernizationRequestNote mNote : modernizationRequest.getModernizationRequestNoteList()) {
					mNote.setNote(note);
					mNote.setForwardTo(forwardToUser);
					mNote.setFinalApproval(forwardFor);
				}
			} else {
				List<ModernizationRequestNote> requestNoteList = new ArrayList<ModernizationRequestNote>();
				ModernizationRequestNote modernizationRequestNote = new ModernizationRequestNote();
				modernizationRequestNote.setNote(note);
				modernizationRequestNote.setForwardTo(forwardToUser);
				modernizationRequestNote.setFinalApproval(forwardFor);
				requestNoteList.add(modernizationRequestNote);
				modernizationRequest.setModernizationRequestNoteList(requestNoteList);
			}

			if (modernizationRequest != null && modernizationRequest.getModRequestLogList() != null
					&& !modernizationRequest.getModRequestLogList().isEmpty()) {
				modernizationRequest.setModRequestLogList(modernizationRequest.getModRequestLogList());
			} else {
				List<ModernizationRequestLog> modLogList = new ArrayList<>();
				ModernizationRequestLog modernizationRequestLog = new ModernizationRequestLog();
				modernizationRequestLog.setModernizationRequest(modernizationRequest);
				modernizationRequestLog.setStage(ApprovalStage.SUBMITTED);
				modLogList.add(modernizationRequestLog);
				modernizationRequest.setModRequestLogList(modLogList);
			}

			String url = SERVER_URL + "/create";
			BaseDTO baseDTO = httpService.put(url, modernizationRequest);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ModernizationRequest modRequest = mapper.readValue(jsonResponse, ModernizationRequest.class);
			if (modRequest != null) {
				modernizationRequest = modRequest;
			}
			if (baseDTO.getStatusCode() == 0) {
				clear();
				loadLazyModernizationRequestList();
				errorMap.notify(ErrorDescription.MODERNIZATION_REQUEST_SAVE_SUCCESS.getErrorCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving ModernizationRequest with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void uploadModernizationRequest(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CER_DOC_EMPTY.getErrorCode());
				return;
			}
			modernizationfile = event.getFile();
			long size = modernizationfile.getSize();
			log.info("uploadModernizationRequest size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				modernizationRequest.setDocumentPath(null);
				docPath = null;
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("MODERNIZATION_REQUEST_PATH");
			log.info("uploadModernizationRequest path==>" + photoPathName);
			docPath = Util.fileUpload(photoPathName, modernizationfile);
			log.info("uploadModernizationRequest docPath==>" + docPath);
			modFileName = modernizationfile.getFileName();
			modernizationRequest.setDocumentPath(docPath);
			errorMap.notify(ErrorDescription.MODERNIZATION_REQUEST_UPLOAD_SUCCESS.getErrorCode());
			log.info(" uploadModernizationRequest Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void clear() {
		headRegionOffice = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		nameOfBuinding = null;
		forwardToUser = new UserMaster();
		forwardFor = null;
		selectedModRequest = new ModernizationRequestDTO();
		note = null;
		docPath = null;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		modernizationRequest = new ModernizationRequest();
	}

	public String cancelModernizationRequest() {
		headRegionOffice = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		nameOfBuinding = null;
		forwardToUser = new UserMaster();
		forwardFor = null;
		selectedModRequest = new ModernizationRequestDTO();
		note = null;
		docPath = null;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		loadLazyModernizationRequestList();
		return LIST_PAGE;
	}

	public void loadLazyModernizationRequestList() {
		log.info("<==== Starts BiometricBean.loadLazyBiometricAttendaceList =====>");
		modRequestLazyList = new LazyDataModel<ModernizationRequestDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ModernizationRequestDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = objectMapper.writeValueAsString(response.getResponseContent());
						modRequestList = objectMapper.readValue(jsonResponse,
								new TypeReference<List<ModernizationRequestDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return modRequestList;
			}

			@Override
			public Object getRowKey(ModernizationRequestDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ModernizationRequestDTO getRowData(String rowKey) {
				for (ModernizationRequestDTO modernizationRequestDTO : modRequestList) {
					if (modernizationRequestDTO.getId().equals(Long.valueOf(rowKey))) {
						selectedModRequest = modernizationRequestDTO;
						return modernizationRequestDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedModRequest = ((ModernizationRequestDTO) event.getObject());
		log.info("status==>" + selectedModRequest.getStatus());
		log.info("status==>" + selectedModRequest.getStage());
		if (selectedModRequest.getStage().equalsIgnoreCase("SUBMITTED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			isAddButton = false;
			isEditButton = false;
			isDeleteButton = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("FINAL-APPROVED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			isAddButton = false;
			isEditButton = false;
			isDeleteButton = false;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("REJECTED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			isAddButton = false;
			isEditButton = true;
			isDeleteButton = true;
		}
		if (selectedModRequest.getStage().equalsIgnoreCase("APPROVED")
				&& loginBean.getUserMaster().getUsername().equals("hosuper")) {
			isAddButton = false;
			isEditButton = false;
			isDeleteButton = false;
		}

		if (!loginBean.getUserMaster().getUsername().equals("hosuper")) {
			isAddButton = false;
			isEditButton = false;
			isDeleteButton = false;
		}
//		if (selectedModRequest.getStatus() == true) {
//			isAddButton = false;
//			isApproveButton = false;
//			isEditButton = false;
//			isDeleteButton = true;
//		}else if (selectedModRequest.getStatus() == false) {
//			isAddButton = false;
//			isApproveButton = false;
//			isEditButton = false;
//			isDeleteButton = true;
//		}
//		else if((ApprovalStage.SUBMITTED.equalsIgnoreCase(selectedModRequest.getStage())) &&  loginBean.getUserMaster().getUsername().equals("hosuper") ){
//			isEditButton = false;
//			isDeleteButton = false;
//		}
//        else if(selectedModRequest.getStage().equalsIgnoreCase("REJECTED") && loginBean.getUserMaster().getUsername().equals("hosuper") ){
//        	isEditButton = false;
//			isDeleteButton = false;	
//		}
//        
//		
//		
//		
//		else {
//			isAddButton = true;
//			isApproveButton = true;
//			isEditButton = true;
//			isDeleteButton = true;
//		}

	}

	public String showListPage() {
		pageAction = "LIST";
		return loadModernizationPage();
	}

	public String submitRejectedStatus(String status) {
		try {
			log.info("status==>" + status);
			log.info("modRemarks==>" + modRemarks);
			log.info("logList id==>" + logList.get(0).getId());

			RequestModForwardDTO requestModForwardDTO = new RequestModForwardDTO();
			requestModForwardDTO.setLogList(logList);
			requestModForwardDTO.setForwardRemarks(modRemarks);
			requestModForwardDTO.setModernizationRequest(modernizationRequest);
			requestModForwardDTO.setForwardToUser(forwardToUser.getId());
			log.info("iddddddd" + requestModForwardDTO.getForwardToUser());
			requestModForwardDTO.setStage(forwardFor);
			log.info("approvestatus" + requestModForwardDTO.isStage());
			requestModForwardDTO.setFinalapprovalFlag(finalapprovalFlag);
			if (status.equals("reject")) {
				requestModForwardDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals("approved") && finalapprovalFlag == false) {
				requestModForwardDTO.setStatus(ApprovalStage.APPROVED);

			} else if (status.equals("approved") && finalapprovalFlag == true) {
				requestModForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + "/updatelog";
			BaseDTO baseDTO = httpService.put(url, requestModForwardDTO);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clear();
				loadLazyModernizationRequestList();
				errorMap.notify(ErrorDescription.MODERNIZATION_REQUEST_SAVE_SUCCESS.getErrorCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving ModernizationRequest with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_PAGE;
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			file = null;
			if (modernizationRequest.getDocumentPath() != null) {
				File files = new File(modernizationRequest.getDocumentPath());
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));

			}

			else {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
				log.error("File is empty----->");
			}

		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
			errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);

		if (finalapprovalFlag == true) {

			value = "FINAL-APPROVED";
			selectedModRequest.setStage(value);
		} else {
			value = "APPROVED";
			selectedModRequest.setStage(value);
		}

	}

}
