package in.gov.cooptex.operation.intend.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.intend.model.IntendRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("intendRequestBean")
@Scope("session")
public class IntendRequestBean implements Serializable {

	private static final long serialVersionUID = 4578136862113789205L;
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String INPUT_FORM_LIST_URL = "/pages/printingAndStationary/listInitiateRequirementRequest.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_ADD_URL  = "/pages/printingAndStationary/initiateRequirementRequest.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	IntendRequest intendRequest = new IntendRequest();

	@Getter
	@Setter
	RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	
	@Getter
	@Setter
	private List<IntendRequest> intendRequestList;
	
	@Getter
	@Setter
	private IntendRequest selectedIntendRequest;
	
	@Getter
	@Setter
	private IntendRequest intendRequestResponse;
	

	@Getter
	@Setter
	private IntendRequest intendRequestLazy;

	
	@Getter
	@Setter
	private Boolean deleteFlag = false;

	@Getter
	@Setter
	private Boolean addButtonFlag = false;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	private Boolean editButtonFlag = false;

	@Getter
	@Setter
	private Boolean downloadFileFlag;

	@Autowired
	AppPreference appPreference;
	
	
	@Getter
	@Setter
	Date fromDate;
	
	@Getter
	@Setter
	Date toDate;

	@Getter
	@Setter
	Date dueDate;
	
	@Getter
	@Setter
	LazyDataModel<IntendRequest> intendRequestLazyList;
	
	@Getter
	@Setter
	SortOrder sortingOrder;
	
	@Getter
	@Setter
	String sortingField;
	
	@Getter @Setter
	int size;
	
	@Setter
	@Getter
	StreamedContent file;
	
	@PostConstruct
	public void init() {
		log.info("PostConstruct IntendRequest load called");
		intendRequest = new IntendRequest();
		selectedIntendRequest = new IntendRequest();
		intendRequest.setDueDate(new Date());
		intendRequest.setFromDate(new Date());
		intendRequest.setToDate(new Date());
		intendRequest.setRequirementCode(null);
		intendRequest.setRequirementName(null);
		loadLazyIntRequirementList();
		editButtonFlag = true;
		deleteFlag = true;
		viewButtonFlag = true;
		addButtonFlag = false;
//		loadLazyIntendRequestList();
		log.info("PostConstruct IntendRequest load Success");
	}
	
	public String addIntend()
	{ 
		log.info("add called");
		intendRequest = new IntendRequest();
		return INPUT_FORM_ADD_URL;
	}

	public void dueDateUpdate() {
		log.info("set dueDate Update called");
		intendRequest.setDueDate(getToDate());
		log.info("dueDate Update Success");
	}

	public void toDateUpdate() {
		log.info("set toDate Update called");
		intendRequest.setToDate(getFromDate());
		log.info("toDate Update Success");
	}

	public void resetData() {
		intendRequest.setFromDate(new Date());
		intendRequest.setToDate(new Date());
		intendRequest.setDueDate(new Date());
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("IntendRequestBean onRowSelect method started");
		editButtonFlag = false;
		deleteFlag = false;
		viewButtonFlag = false;
//		selectedIntendRequest = new IntendRequest();
//		selectedIntendRequest = ((IntendRequest) event.getObject());
//		log.info("IntendRequestBean Selected "+selectedIntendRequest.getRequirementName());
		addButtonFlag=true;
    }
	
	 
	public String createIntendRequest() {
		log.info(":::<- Create IntendRequestStart ->:::");
		try {
			if(intendRequest.getRequirementName() == null || intendRequest.getRequirementName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.INTEND_REQUEST_REQUIREMENT_NAME_REQUIRED.getErrorCode());
				return null;
			}
			else if(intendRequest.getFromDate() == null) {
				errorMap.notify(ErrorDescription.INTEND_REQUEST_FROM_DATE_REQUIRED.getErrorCode());
				return null;
			}
			else if(intendRequest.getToDate() == null) {
				errorMap.notify(ErrorDescription.INTEND_REQUEST_TO_DATE_REQUIRED.getErrorCode());
				return null;
			}
			else if(intendRequest.getDueDate() == null) {
				errorMap.notify(ErrorDescription.INTEND_REQUEST_DUE_DATE_REQUIRED.getErrorCode());
				return null;
			}
			
			BaseDTO baseDTO = null;
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/intendrequest/create";
			log.info("::: IntendRequest Controller calling  1 :::");
			baseDTO = httpService.post(url, intendRequest);
			log.info("::: Save createIntendRequest Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("IntendRequest Created Successfully ");
				loadLazyIntRequirementList();
				log.info("IntendRequestList Page Redirecting  -->");
				errorMap.notify(ErrorDescription.INTEND_REQUEST_INSERTED_SUCCESSFULLY.getErrorCode());
//				FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_LIST_URL);
				return INPUT_FORM_LIST_URL;
			}
			else{
				log.warn("IntendRequest Error");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception ee) {
			Util.addError("IntendRequest Created Failed");
			log.error("Exception Occured in IntendRequest create ", ee);
		}
		return null;
	}
	public void loadLazyIntRequirementList() {
		log.info("<--- loadLazyIntendRequestRequirementList ---> ");
		intendRequestLazyList = new LazyDataModel<IntendRequest>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<IntendRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<IntendRequest> data = new ArrayList<IntendRequest>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<IntendRequest>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyIntendRequestRequirementList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(IntendRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public IntendRequest getRowData(String rowKey) {
				List<IntendRequest> responseList = (List<IntendRequest>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (IntendRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						intendRequestLazy = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		intendRequestLazy = new IntendRequest();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		IntendRequest yarnRequirement = new IntendRequest();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

//		log.info("IntendRequest " + yarnRequirement);
		yarnRequirement.setFilters(filters);

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/intendrequest/searchData";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
	
	public String clearButon() {
		log.info("<==== Starts IndendBean.clearPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteFlag = true;
		viewButtonFlag = true;
		addButtonFlag = false;
		intendRequest = new IntendRequest();
		selectedIntendRequest = new IntendRequest();
		loadLazyIntRequirementList();
		log.info("<==== Ends IndendBean.clearPage =====>");
		return INPUT_FORM_LIST_URL;
	}
	 
	
	public String deletedInendRequest() {
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL+ appPreference.getOperationApiUrl() + "/intendrequest/deleteIntendRequest";
			log.info("::: IntendRequest Controller Delete calling  1 :::");
			baseDTO = httpService.post(url, selectedIntendRequest);
			log.info("::: Save createIntendRequest Response :");
			if (baseDTO.getStatusCode()==0) {
				log.info("IntendRequest Deleted Successfully ");
				errorMap.notify(ErrorDescription.INTEND_REQUEST_DELETE_SUCCESS.getErrorCode());
				init();
				loadLazyIntRequirementList();
				return null;
			}
			else{
				log.warn("IntendRequest Delete Error");
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception ee) {
			Util.addError("IntendRequest Delete Failed");
			log.error("Exception Occured in IntendRequest Delete", ee);
		}
		return null;
	}

}

