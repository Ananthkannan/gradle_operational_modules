package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.dto.AssetRequestListDTO;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AdditionalChargeStage;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeInterchangeDetails;
import in.gov.cooptex.core.model.EmployeeInterchangeDetailsLog;
import in.gov.cooptex.core.model.EmployeeInterchangeDetailsNote;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.hrms.dto.EmployeeInterchangeDetailsDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("employeeInterchangeDetailsBean")
@Scope("session")
@Log4j2
public class EmployeeInterchangeDetailsBean extends CommonBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1286393143341713875L;

	final String LIST_PAGE = "/pages/personnelHR/listInterchange.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/personnelHR/createInterchange.xhtml	?faces-redirect=true";
	final String VIEW_PAGE = "/pages/personnelHR/viewInterchange.xhtml?faces-redirect=true";
	final String RELIVE_PAGE = "/pages/personnelHR/interchangeRelive.xhtml?faces-redirect=true";
	final String JOINEDON_PAGE = "/pages/personnelHR/interchangeJoinedon.xhtml?faces-redirect=true";

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean claim = false;

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	private EmployeeInterchangeDetails employeeInterchangeDetails = new EmployeeInterchangeDetails();

	@Getter
	@Setter
	List<EntityMaster> headOfficeAndRegionEntityList;

	@Getter
	@Setter
	private EmployeeInterchangeDetailsDTO employeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();

	@Getter
	@Setter
	private EmployeeInterchangeDetailsLog employeeInterchangeDetailsLog = new EmployeeInterchangeDetailsLog();

	@Getter
	@Setter
	private EmployeeInterchangeDetailsNote employeeInterchangeDetailsNote = new EmployeeInterchangeDetailsNote();

	@Getter
	@Setter
	EntityMaster headOfficeAndRegionEntity = new EntityMaster();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse = "";

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalRenderFlag = true;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	private Boolean approveeditFlag = true;

	@Getter
	@Setter
	int forwardfor;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityTypeMaster> regionalOfficeEntityTypeList;

	@Getter
	@Setter
	EntityTypeMaster regionalOfficeEntityType = new EntityTypeMaster();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	List<Department> departmentList, selectedDepartmentList;

	@Getter
	@Setter
	Department department = new Department();

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	Designation designation = new Designation();

	@Getter
	@Setter
	LazyDataModel<EmployeeInterchangeDetailsDTO> lazyemployeeInterchangeList;

	@Getter
	@Setter
	List<EmployeeInterchangeDetailsDTO> employeeInterchangeDetailsDTOList = new ArrayList<EmployeeInterchangeDetailsDTO>();

	@Getter
	@Setter
	EmployeeInterchangeDetailsDTO selectedemployeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();

	@Getter
	@Setter
	EmployeeInterchangeDetailsDTO viewResponseDTO = new EmployeeInterchangeDetailsDTO();

	@Getter
	@Setter
	List<EmployeeInterchangeDetailsDTO> viewInterchangeEmployeeDetails = new ArrayList<EmployeeInterchangeDetailsDTO>();

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	EntityMaster reliveHeadOfficeAndRegionEntity = new EntityMaster();

	@Getter
	@Setter
	EntityTypeMaster reliveRegionalOfficeEntityType = new EntityTypeMaster();

	@Getter
	@Setter
	EntityMaster reliveEntityMaster = new EntityMaster();

	@Getter
	@Setter
	boolean interchangeDetailsFlag = false;

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	UploadedFile uploadFile;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@PostConstruct
	public void init() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		forwardToUsersList = new ArrayList<>();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.INTERCHANGE.name());
		headOfficeAndRegionEntityList = new ArrayList<>();
		headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
		designationList = commonDataService.loadActiveDesignations();
		lazyemployeeInterchangeListResponse();
		showViewListPage();

	}

	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("emp_interchange_details_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// init();
			//selectedemployeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
			action = "VIEW";

			log.info("valueeee" + selectedemployeeInterchangeDetailsDTO.getId());
			selectedemployeeInterchangeDetailsDTO.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedemployeeInterchangeDetailsDTO.getId());
			selectedNotificationId = Long.parseLong(notificationId);

			viewemployeeInterchangeDetails();
		}
		log.info("Employee Intechange Ends");

		return VIEW_PAGE;
	}

	public void headandRegional() {

		regionalOfficeEntityTypeList = new ArrayList<>();
		regionalOfficeEntityType = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<>();
		employeeInterchangeDetailsDTO.setEmployeeMaster(new EmployeeMaster());
		employeeMasterList = new ArrayList<EmployeeMaster>();
		reliveEntityMaster = new EntityMaster();

		if (headOfficeAndRegionEntity != null) {
			log.info("<====== EmployeeAdditionalChargeBean.headandRegional Starts====>entity master",
					headOfficeAndRegionEntity.getId());
			try {
				if (headOfficeAndRegionEntity.getId() != null && headOfficeAndRegionEntity.getEntityTypeMaster() != null
						&& headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode()
								.equalsIgnoreCase("HEAD_OFFICE")) {

					visibility = true;
					entityMaster.setId(headOfficeAndRegionEntity.getId());
					loadEmployeeList();
				} else {
					visibility = false;
					regionalOfficeEntityTypeList = commonDataService.getActiveEntityType();
				}
				if ("RELIVE".equalsIgnoreCase(action)) {
					if (reliveHeadOfficeAndRegionEntity.getId() != null
							&& reliveHeadOfficeAndRegionEntity.getEntityTypeMaster() != null
							&& reliveHeadOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode()
									.equalsIgnoreCase("HEAD_OFFICE")) {
						reliveEntityMaster = reliveHeadOfficeAndRegionEntity;
						visibility = true;
					} else {
						visibility = false;
						regionalOfficeEntityTypeList = commonDataService.getActiveEntityType();
					}
				}
			} catch (Exception exp) {
				log.error("found exception in onchangeEntityType: ", exp);
			}
			log.info("<======= EmployeeAdditionalChargeBean.headandRegiona Ends =========>");

		}
	}

	/* Load Designation Type By Department Id */
	public void loadDesignationByDept() {
		log.info("<======== employeeInterchangeDetailsBean.loadDesignationByDept() calling ======>");
		try {
			String url = AppUtil.getPortalServerURL() + "/designation/getalldesignation/" + department.getId();
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				designationList = mapper.readValue(jsonValue, new TypeReference<List<Designation>>() {
				});
			}
			log.info("designationList size" + designationList.size());
		} catch (Exception e) {
			log.error("<========== Exception employeeInterchangeDetailsBean.loadDesignationByDept()==========>", e);
		}
	}

	/* Load All Entity By Region Id and Entity Type */
	public void loadAllEntityByRegionIdandEntityTypeId() {
		log.info("<======== employeeInterchangeDetailsBean.loadAllEntityByRegionIdandEntityTypeId() calling ======>");
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<>();
		employeeInterchangeDetailsDTO.setEmployeeMaster(new EmployeeMaster());
		employeeInterchangeDetails.setCurrentDepartment(null);
		employeeInterchangeDetails.setCurrentDesignation(null);
		employeeMasterList = new ArrayList<EmployeeMaster>();
		Long regionId = 0l;
		Long entityTypeId = 0l;
		try {
			if (headOfficeAndRegionEntity != null) {
				regionId = headOfficeAndRegionEntity.getId();
			}
			if (regionalOfficeEntityType != null) {
				entityTypeId = regionalOfficeEntityType.getId();
			}
			if (action.equalsIgnoreCase("RELIVE")) {
				if (reliveHeadOfficeAndRegionEntity != null) {
					regionId = reliveHeadOfficeAndRegionEntity.getId();
				}
				if (reliveRegionalOfficeEntityType != null) {
					entityTypeId = reliveRegionalOfficeEntityType.getId();
				}
			}
			if (regionId != null && entityTypeId != null) {
				entityMasterList = commonDataService.loadActiveEntityByregionOrentityTypeId(regionId, entityTypeId);
				log.info("loadAllEntityByRegionIdandEntityTypeId size" + entityMasterList.size());
			}
		} catch (Exception e) {
			log.error(
					"<========== Exception employeeInterchangeDetailsBean.loadAllEntityByRegionIdandEntityTypeId()==========>",
					e);
		}
	}

	public void loadEmployeeList() {
		if (entityMaster != null) {
			employeeInterchangeDetailsDTO.setEmployeeMaster(new EmployeeMaster());
			employeeMasterList = new ArrayList<EmployeeMaster>();
			if (entityMaster.getId() != null) {
				// employeeMasterList =
				// commonDataService.getEmployeeMasterByEntityId(entityMaster.getId());
				employeeMasterList = loadEmployeeListByEntityId(entityMaster.getId());
			}
		}

		if (headOfficeAndRegionEntity.getId() != null && headOfficeAndRegionEntity.getEntityTypeMaster() != null
				&& headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {

			employeeInterchangeDetailsDTO.setEmployeeMaster(new EmployeeMaster());
			employeeMasterList = new ArrayList<EmployeeMaster>();
			// employeeMasterList =
			// commonDataService.getEmployeeMasterByEntityId(headOfficeAndRegionEntity.getId());
			employeeMasterList = loadEmployeeListByEntityId(entityMaster.getId());

		}
	}

	public void selectEmployee() {
		if (employeeInterchangeDetailsDTO.getEmployeeMaster() != null) {
			employeeInterchangeDetails.setCurrentDesignation(employeeInterchangeDetailsDTO.getEmployeeMaster()
					.getPersonalInfoEmployment().getCurrentDesignation());
			employeeInterchangeDetails.setCurrentDepartment(employeeInterchangeDetailsDTO.getEmployeeMaster()
					.getPersonalInfoEmployment().getCurrentDepartment());

			selectedDepartmentList = new ArrayList<>();
			selectedDepartmentList.addAll(departmentList);
			if (departmentList.contains(employeeInterchangeDetails.getCurrentDepartment())) {

				selectedDepartmentList.remove(employeeInterchangeDetails.getCurrentDepartment());
			}

		}
	}

	public String submit() {
		try {
			employeeInterchangeDetails.setInterchangeDesignation(designation);
			employeeInterchangeDetails.setInterDepartment(department);

			if (StringUtils.isEmpty(fileName) || fileName == null) {
				AppUtil.addWarn("Upload documents is required");
				return null;
			}

			if (headOfficeAndRegionEntity.getId() != null && headOfficeAndRegionEntity.getEntityTypeMaster() != null
					&& headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode()
							.equalsIgnoreCase("HEAD_OFFICE")) {
				employeeInterchangeDetails.setEntity(headOfficeAndRegionEntity);
				employeeInterchangeDetails.setEntityType(headOfficeAndRegionEntity.getEntityTypeMaster());
			} else {
				employeeInterchangeDetails.setEntity(entityMaster);
				employeeInterchangeDetails.setEntityType(regionalOfficeEntityType);
			}

			// employeeInterchangeDetails.setEmployeeMaster(employeeMaster);
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsNote(employeeInterchangeDetailsNote);
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsLog(employeeInterchangeDetailsLog);

			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetails(employeeInterchangeDetails);

			log.info("<=======Starts employeeInterchangeDetailsBean.submit ======>" + employeeInterchangeDetails);

			String url = SERVER_URL + "/employeeinterchange/create";
			BaseDTO response = httpService.post(url, employeeInterchangeDetailsDTO);
			if (response.getStatusCode() == 0) {
				if (action.equals("ADD")) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_CREATED).getErrorCode());
				} else {

					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_UPDATED).getErrorCode());
				}
				log.info("<=======Ends employeeInterchangeDetailsBean.submited ======>");
				return showListPage();
			} else {
				errorMap.notify(response.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.info("<=======employeeInterchangeDetailsBean.submit Error ==================>" + e);
			return null;
		}
	}

	public void lazyemployeeInterchangeListResponse() {
		log.info("<===== Starts employeeInterchangeBean.lazyemployeeInterchangeList ======>");
		lazyemployeeInterchangeList = new LazyDataModel<EmployeeInterchangeDetailsDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EmployeeInterchangeDetailsDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/employeeinterchange/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeeInterchangeDetailsDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeInterchangeDetailsDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						for (EmployeeInterchangeDetailsDTO p : employeeInterchangeDetailsDTOList) {
							log.info("p.getId ::::::::::" + p.getId());
						}
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in employeeInterchangeBean.lazyemployeeInterchangeList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeeInterchangeDetailsDTOList;
			}

			@Override
			public Object getRowKey(EmployeeInterchangeDetailsDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeInterchangeDetailsDTO getRowData(String rowKey) {
				try {
					for (EmployeeInterchangeDetailsDTO benefit : employeeInterchangeDetailsDTOList) {
						if (benefit.getId().equals(Long.valueOf(rowKey))) {
							selectedemployeeInterchangeDetailsDTO = benefit;
							return benefit;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends employeeInterchangeBean.lazyemployeeInterchangeList ======>");
	}

	public String showListPage() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		selectedemployeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
		setFileName("");
		employeeInterchangeDetails.setSupportingDocuments("");
		return LIST_PAGE;
	}

	public String clearButton() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		selectedemployeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
		lazyemployeeInterchangeListResponse();
		return LIST_PAGE;
	}

	public String addPageAction() {
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create  called..");
				headOfficeAndRegionEntityList = new ArrayList<>();
				regionalOfficeEntityTypeList = new ArrayList<>();
				entityMasterList = new ArrayList<>();
				employeeMasterList = new ArrayList<>();
				designation = new Designation();
				department = new Department();
				employeeMaster = new EmployeeMaster();
				entityMaster = new EntityMaster();
				regionalOfficeEntityType = new EntityTypeMaster();
				employeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
				employeeInterchangeDetailsNote = new EmployeeInterchangeDetailsNote();
				employeeInterchangeDetails = new EmployeeInterchangeDetails();
				headOfficeAndRegionEntity = new EntityMaster();
				headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
				// designationList = commonDataService.loadActiveDesignations();
				departmentList = commonDataService.loadDepartmentList();
				selectedDepartmentList = departmentList;
				// forwardToUsersList = new ArrayList<>();
				// forwardToUsersList =
				// commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.INTERCHANGE.name());
				setFileName(null);
				employeeInterchangeDetails.setSupportingDocuments(null);
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("RELIVE")) {
				getViewemployeeInterchangeDetails();
				
				employeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
				employeeInterchangeDetailsNote = new EmployeeInterchangeDetailsNote();
				employeeInterchangeDetailsLog = new EmployeeInterchangeDetailsLog();
				headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
				forwardToUsersList = new ArrayList<>();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.INTERCHANGE.name());
				viewInterchangeReliving();
				return RELIVE_PAGE;
			} else if (action.equalsIgnoreCase("JOINEDON")) {
				//getViewemployeeInterchangeDetails();
				viewemployeeInterchangeDetails();
				//employeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
				employeeInterchangeDetailsNote = new EmployeeInterchangeDetailsNote();
				employeeInterchangeDetailsLog = new EmployeeInterchangeDetailsLog();
				headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
				forwardToUsersList = new ArrayList<>();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.INTERCHANGE.name());
				viewInterchangeReliving();
				return JOINEDON_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		addButtonFlag = true;
		viewButtonFlag = false;
		selectedemployeeInterchangeDetailsDTO = (EmployeeInterchangeDetailsDTO) event.getObject();
		log.info("selectedBenefit id-------" + selectedemployeeInterchangeDetailsDTO.getId());
		log.info("status----------" + selectedemployeeInterchangeDetailsDTO.getStatus());

		if (ApprovalStage.REJECTED.equals(selectedemployeeInterchangeDetailsDTO.getStatus())) {
			editButtonFlag = false;
			approvalFlag = false;
		} else {
			editButtonFlag = true;
		}
	}

	public String viewemployeeInterchangeDetails() {

		getViewemployeeInterchangeDetails();

		if (selectedemployeeInterchangeDetailsDTO != null) {
			setPreviousApproval(
					selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote().getFinalApproval());
			if ("VIEW".equalsIgnoreCase(action)) {
				if (selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote().getUserMaster() != null) {
					if (loginBean.getUserMaster().getId().equals(selectedemployeeInterchangeDetailsDTO
							.getEmployeeInterchangeDetailsNote().getUserMaster().getId())) {
						approveeditFlag = false;
						if (ApprovalStage.REJECTED.equals(selectedemployeeInterchangeDetailsDTO.getStatus())) {
							approvalFlag = false;
						} else {
							approvalFlag = true;
						}
					} else {
						approvalFlag = false;
						finalApproveFlag = true;
					}
				}
				// setFinalApproveFlag(selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote().getFinalApproval());
				if (ApprovalStage.FINAL_APPROVED.equals(selectedemployeeInterchangeDetailsDTO.getStatus())
						|| AdditionalChargeStage.RELIEVE_FINAL_APPROVED
								.equals(selectedemployeeInterchangeDetailsDTO.getStatus())
						|| AdditionalChargeStage.JOINING_FINAL_APPROVED
								.equals(selectedemployeeInterchangeDetailsDTO.getStatus())) {
					buttonFlag = true;
					setFinalApproveFlag(selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
							.getFinalApproval());
				}
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog().setRemarks(null);
				if (selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote().getFinalApproval()
						.equals(true)) {
					finalapprovalRenderFlag = false;
				} else {
					finalapprovalRenderFlag = true;
				}
				return VIEW_PAGE;
			}
		}
		return null;
	}

	public void getViewemployeeInterchangeDetails() {
		log.info("Inside getViewJV() ");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeInterchangeDetailsDTO();
		interchangeDetailsFlag = false;
		try {
			String url = SERVER_URL + "/employeeinterchange/getdetailsbyid/"
					+ selectedemployeeInterchangeDetailsDTO.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<EmployeeInterchangeDetailsDTO>() {
				});

				if (selectedNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}

				designation = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeDesignation();
				department = viewResponseDTO.getEmployeeInterchangeDetails().getInterDepartment();
				regionalOfficeEntityType = viewResponseDTO.getEmployeeInterchangeDetails().getEntityType();
				employeeInterchangeDetailsLog = viewResponseDTO.getEmployeeInterchangeDetailsLog();
				employeeInterchangeDetailsNote = viewResponseDTO.getEmployeeInterchangeDetailsNote();
				employeeInterchangeDetails = viewResponseDTO.getEmployeeInterchangeDetails();
				entityMaster = viewResponseDTO.getEmployeeInterchangeDetails().getEntity();
				selectedemployeeInterchangeDetailsDTO
						.setEmployeeInterchangeDetailsNote(viewResponseDTO.getEmployeeInterchangeDetailsNote());
				selectedemployeeInterchangeDetailsDTO
						.setEmployeeInterchangeDetailsLog(viewResponseDTO.getEmployeeInterchangeDetailsLog());
				selectedemployeeInterchangeDetailsDTO
				.setStatus(viewResponseDTO.getEmployeeInterchangeDetailsLog().getStage());
				
				headOfficeAndRegionEntity = viewResponseDTO.getHeadOfficeRegionOffice();
				viewInterchangeEmployeeDetails = viewResponseDTO.getViewList();
				if (viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeEntity() != null) {
					reliveEntityMaster = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeEntity();
				}

				if (reliveEntityMaster.getEntityTypeMaster() != null) {
					reliveRegionalOfficeEntityType = reliveEntityMaster.getEntityTypeMaster();
				}
				// reliveRegionalOfficeEntityType=viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeEntity().getEntityTypeMaster();
				if (reliveEntityMaster != null && reliveRegionalOfficeEntityType != null
						&& employeeInterchangeDetails.getRelievedOn() != null) {
					interchangeDetailsFlag = true;
				}
				if (viewResponseDTO != null) {
					employeeInterchangeDetails = viewResponseDTO.getEmployeeInterchangeDetails();
					setFileName(employeeInterchangeDetails.getSupportingDocuments());
					File f = new File(employeeInterchangeDetails.getSupportingDocuments());
					fileName = f.getName();
				}

			}
		} catch (Exception e) {
			log.error("Exception Occured While getViewJV() :: ", e);
		}
	}

	public String editemployeeInterchangeDetails() {
		loadeditemployeeInterchangeDetails();
		return ADD_PAGE;
	}

	public void loadeditemployeeInterchangeDetails() {
		log.info("Inside loadeditTaBill() ");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeInterchangeDetailsDTO();
		employeeInterchangeDetailsDTO = new EmployeeInterchangeDetailsDTO();
		headOfficeAndRegionEntity = new EntityMaster();
		regionalOfficeEntityType = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		EmployeeMaster employeeData = new EmployeeMaster();
		designation = new Designation();
		department = new Department();

		try {

			String url = SERVER_URL + "/employeeinterchange/getdetailsbyid/"
					+ selectedemployeeInterchangeDetailsDTO.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<EmployeeInterchangeDetailsDTO>() {
				});
				employeeInterchangeDetailsDTO = viewResponseDTO;

				log.info("Employee Data ID==========" + viewResponseDTO.getEmployeeMaster().getId());
				employeeInterchangeDetailsDTO.setEmployeeMaster(viewResponseDTO.getEmployeeMaster());
				employeeData = viewResponseDTO.getEmployeeMaster();

				designation = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeDesignation();
				department = viewResponseDTO.getEmployeeInterchangeDetails().getInterDepartment();

				departmentList = commonDataService.loadDepartmentList();
				selectedDepartmentList = departmentList;
				loadDesignationByDept();
				selectEmployee();

				log.info("headOfficeAndRegionEntity==========" + viewResponseDTO.getHeadOfficeRegionOffice().getName());
				headOfficeAndRegionEntity = viewResponseDTO.getHeadOfficeRegionOffice();
				headandRegional();
				regionalOfficeEntityType = viewResponseDTO.getEmployeeInterchangeDetails().getEntityType();

				if (!headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
					loadAllEntityByRegionIdandEntityTypeId();
					entityMaster = viewResponseDTO.getEmployeeInterchangeDetails().getEntity();
				}
				loadEmployeeList();

				employeeInterchangeDetailsDTO.setEmployeeMaster(employeeData);

				employeeInterchangeDetailsLog = viewResponseDTO.getEmployeeInterchangeDetailsLog();
				employeeInterchangeDetailsNote = viewResponseDTO.getEmployeeInterchangeDetailsNote();
				employeeInterchangeDetails = viewResponseDTO.getEmployeeInterchangeDetails();

				selectedemployeeInterchangeDetailsDTO
						.setEmployeeInterchangeDetailsLog(viewResponseDTO.getEmployeeInterchangeDetailsLog());
				if (viewResponseDTO != null) {
					employeeInterchangeDetails = viewResponseDTO.getEmployeeInterchangeDetails();
					setFileName(employeeInterchangeDetails.getSupportingDocuments());
					File f = new File(employeeInterchangeDetails.getSupportingDocuments());
					fileName = f.getName();
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadeditemployeeInterchangeDetails() :: ", e);
		}
	}

	public String approveemployeeInterchangeDetails() {
		BaseDTO baseDTO = null;
		try {
			
			log.info(" approveemployeeInterchangeDetails id----------" + selectedemployeeInterchangeDetailsDTO.getId());
			baseDTO = new BaseDTO();
			if (previousApproval == false && selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
					.getFinalApproval() == true) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(ApprovalStage.APPROVED);
			} else if (previousApproval == true && selectedemployeeInterchangeDetailsDTO
					.getEmployeeInterchangeDetailsNote().getFinalApproval() == true) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(ApprovalStage.FINAL_APPROVED);
			} else {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(ApprovalStage.APPROVED);
			}
			if (previousApproval == false
					&& selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
							.getFinalApproval() == true
					&& (selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_SUBMITTED)
							|| selectedemployeeInterchangeDetailsDTO.getStatus()
									.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_APPROVED))) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.RELIEVE_APPROVED);
			} else if (previousApproval == true
					&& selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
							.getFinalApproval() == true
					&& (selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_SUBMITTED)
							|| selectedemployeeInterchangeDetailsDTO.getStatus()
									.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_APPROVED))) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.RELIEVE_FINAL_APPROVED);
			} else if (selectedemployeeInterchangeDetailsDTO.getStatus()
					.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_SUBMITTED)
					|| selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_APPROVED)) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.RELIEVE_APPROVED);
			}
			if (previousApproval == false
					&& selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
							.getFinalApproval() == true
					&& (selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.JOINING_SUBMITTED)
							|| selectedemployeeInterchangeDetailsDTO.getStatus()
									.equalsIgnoreCase(AdditionalChargeStage.JOINING_APPROVED))) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.JOINING_APPROVED);
			} else if (previousApproval == true
					&& selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
							.getFinalApproval() == true
					&& (selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.JOINING_SUBMITTED)
							|| selectedemployeeInterchangeDetailsDTO.getStatus()
									.equalsIgnoreCase(AdditionalChargeStage.JOINING_APPROVED))) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.JOINING_FINAL_APPROVED);
			} else if (selectedemployeeInterchangeDetailsDTO.getStatus()
					.equalsIgnoreCase(AdditionalChargeStage.JOINING_SUBMITTED)
					|| selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.JOINING_APPROVED)) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.JOINING_APPROVED);
			}
			log.info("approve remarks=========>"
					+ selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog().getRemarks());

			if (employeeInterchangeDetailsNote != null && employeeInterchangeDetailsNote.getUserMaster() != null) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsNote()
						.setUserMaster(employeeInterchangeDetailsNote.getUserMaster());
			}

			selectedemployeeInterchangeDetailsDTO.setEmployeeInterchangeDetails(employeeInterchangeDetails);

			String url = SERVER_URL + "/employeeinterchange/approveemployeeinterchange";

			baseDTO = httpService.post(url, selectedemployeeInterchangeDetailsDTO);
			if (baseDTO.getStatusCode() == 0) {

				if (selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog().getStage()
						.equals(ApprovalStage.APPROVED)) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_APPROVED).getErrorCode());

				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_FINAL_APPROVED)
							.getErrorCode());

				}
				log.info("Successfully Approved------------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("approveemployeeInterchangeDetails method inside exception-------", e);
		}
		return null;
	}

	public String rejectemployeeInterchangeDetails() {
		BaseDTO baseDTO = null;
		try {
			log.info("amount transfer id----------" + selectedemployeeInterchangeDetailsDTO.getId());
			baseDTO = new BaseDTO();
			selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog().setStage(ApprovalStage.REJECTED);
			if (selectedemployeeInterchangeDetailsDTO.getStatus()
					.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_SUBMITTED)
					|| selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_APPROVED)) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.RELIVING_REJECTED);
			}
			if (selectedemployeeInterchangeDetailsDTO.getStatus()
					.equalsIgnoreCase(AdditionalChargeStage.JOINING_SUBMITTED)
					|| selectedemployeeInterchangeDetailsDTO.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.JOINING_APPROVED)) {
				selectedemployeeInterchangeDetailsDTO.getEmployeeInterchangeDetailsLog()
						.setStage(AdditionalChargeStage.JOINING_REJECTED);
			}
			String url = SERVER_URL + "/employeeinterchange/rejectemployeeinterchange";
			baseDTO = httpService.post(url, selectedemployeeInterchangeDetailsDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_REJECTED).getErrorCode());

				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("rejectemployeeInterchangeDetails method inside exception-------", e);
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts employeeInterchangeDetailsBean.deleteConfirm ===========>"
				+ selectedemployeeInterchangeDetailsDTO.getId());
		try {
			BaseDTO response = httpService.delete(
					SERVER_URL + "/employeeinterchange/deletebyid/" + selectedemployeeInterchangeDetailsDTO.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_DELETED).getErrorCode());

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEmpTransferDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete employeeInterchangeDetails....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends employeeInterchangeDetailsBean.deleteConfirm ===========>");
		return showListPage();
	}

	public void interchangeFileUpload(FileUploadEvent event) {
		log.info(" <------InterchangeFileUpload FileUpload Method Start--->");

		try {

			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				return;
			}
			uploadFile = event.getFile();
			long size = uploadFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;

			String docPathName = commonDataService.getAppKeyValue("INTERCHANGE_FILE_PATH");

			String filePath = Util.fileUpload(docPathName, uploadFile);
			setFileName(uploadFile.getFileName());
			employeeInterchangeDetails.setSupportingDocuments(filePath);
		} catch (Exception exp) {
			log.info("<<===  Error in fileUpload :: " + exp);
		}

	}

	public void interchangeFileDownload() {
		InputStream input = null;
		try {
			if (employeeInterchangeDetails.getSupportingDocuments() != null) {
				File file = new File(employeeInterchangeDetails.getSupportingDocuments());
				input = new FileInputStream(file);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				this.setFile(
						new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			File files = new File(employeeInterchangeDetails.getSupportingDocuments());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void viewInterchangeReliving() {
		log.info("<====Employee InterChangeBean::viewInterchangeReliving Method Start===>");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeInterchangeDetailsDTO();
		try {
			String url = SERVER_URL + "/employeeinterchange/getdetailsbyid/"
					+ selectedemployeeInterchangeDetailsDTO.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<EmployeeInterchangeDetailsDTO>() {
				});

				designation = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeDesignation();
				department = viewResponseDTO.getEmployeeInterchangeDetails().getInterDepartment();
				regionalOfficeEntityType = viewResponseDTO.getEmployeeInterchangeDetails().getEntityType();
				employeeInterchangeDetails = viewResponseDTO.getEmployeeInterchangeDetails();
				entityMaster = viewResponseDTO.getEmployeeInterchangeDetails().getEntity();
				headOfficeAndRegionEntity = viewResponseDTO.getHeadOfficeRegionOffice();
				reliveEntityMaster = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeEntity();
				reliveRegionalOfficeEntityType = viewResponseDTO.getEmployeeInterchangeDetails().getInterchangeEntity()
						.getEntityTypeMaster();
			}
		} catch (Exception ex) {
			log.error("Error in viewInterchangeReliving Method", ex);
		}
		log.info("<====Employee InterChangeBean::viewInterchangeReliving Method End===>");

	}

	public String relivingCurrentToAnotherDepartment() {
		log.info("===>EmployeeInterchangeDetailsBean:relivingCurrentToAnotherDepartment Method Start===>");
		try { 
			employeeInterchangeDetails = new EmployeeInterchangeDetails();
			employeeInterchangeDetails.setId(selectedemployeeInterchangeDetailsDTO.getId());

			employeeInterchangeDetails.setInterchangeEntity(reliveEntityMaster);
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsNote(employeeInterchangeDetailsNote);
			employeeInterchangeDetailsLog.setStage("RELIVING_SUBMITTED");
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsLog(employeeInterchangeDetailsLog);

			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetails(employeeInterchangeDetails);
			String url = SERVER_URL + "/employeeinterchange/relive";
			BaseDTO response = httpService.post(url, employeeInterchangeDetailsDTO);
			if (response.getStatusCode() == 0) {
				if (action.equals("RELIVE")) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_CREATED).getErrorCode());
				}
				log.info("<=======Ends employeeInterchangeDetailsBean.submited ======>");
				return showListPage();
			} else {
				errorMap.notify(response.getStatusCode());
				return null;
			}
		} catch (Exception ex) {
			log.error("====>Error in relivingCurrentToAnotherDepartment Method", ex);
		}
		log.info("===>EmployeeInterchangeDetailsBean:relivingCurrentToAnotherDepartment Method End===>");
		return null;
	}

	public String interchangeJoinedOn() {
		log.info("===>EmployeeInterchangeDetailsBean:interchangeJoinedOn Method Start===>");
		try {
			employeeInterchangeDetails = new EmployeeInterchangeDetails();
			employeeInterchangeDetails.setId(selectedemployeeInterchangeDetailsDTO.getId());
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsNote(employeeInterchangeDetailsNote);
			employeeInterchangeDetailsLog.setStage(AdditionalChargeStage.JOINING_SUBMITTED);
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetailsLog(employeeInterchangeDetailsLog);
			employeeInterchangeDetailsDTO.setEmployeeInterchangeDetails(employeeInterchangeDetails);
			String url = SERVER_URL + "/employeeinterchange/joinedOn";
			BaseDTO response = httpService.post(url, employeeInterchangeDetailsDTO);
			if (response.getStatusCode() == 0) {
				if (action.equals("JOINEDON")) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INTERCHANGE_CREATED).getErrorCode());
				}
				log.info("<=======Ends employeeInterchangeDetailsBean.submited ======>");
				return showListPage();
			} else {
				errorMap.notify(response.getStatusCode());
				return null;
			}

		} catch (Exception ex) {
			log.error("====>Error in interchangeJoinedOn Method", ex);
		}
		log.info("===>EmployeeInterchangeDetailsBean:interchangeJoinedOn Method End===>");
		return null;
	}

	public List<EmployeeMaster> loadEmployeeListByEntityId(Long workLocationId) {
		log.info("==================>loadEmployeeListByEntityId Method Start=============>");
		List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();
		try {
			// String url = SERVER_URL +
			// "/employeeinterchange/loadEmployee/"+workLocationId;
			String url = SERVER_URL + "/employeeinterchange/getInterchangedEmployee/" + workLocationId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
			log.info("Employee list size=" + employeeMasterList.size());
		} catch (Exception ex) {
			log.error("====>Error in loadEmployeeListByEntityId Method", ex);
		}
		log.info("=============loadEmployeeListByEntityId Method End==============");
		return employeeMasterList;
	}

}
