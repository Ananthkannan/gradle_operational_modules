package in.gov.cooptex.admin.tapal.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.IncomingTapal;
import in.gov.cooptex.admin.tapal.model.OutgoingTapal;
import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.admin.tapal.model.TapalFor;
import in.gov.cooptex.admin.tapal.model.TapalPodRegister;
import in.gov.cooptex.admin.tapal.model.TapalSenderType;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TapalDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("outgoingTapalBean")
@Scope("session")
@Log4j2
public class OutgoingTapalBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private final String OUTGOING_TAPAL_CREATE_PAGE = "/pages/admin/tapal/createOutgoingTapal.xhtml?faces-redirect=true;";
	private final String OUTGOING_TAPAL_LIST_PAGE = "/pages/admin/tapal/listOutgoingTapal.xhtml?faces-redirect=true;";
	private final String OUTGOING_TAPAL_VIEW_PAGE = "/pages/admin/tapal/viewOutgoingTapal.xhtml?faces-redirect=true;";
	private final String OUTGOING_TAPAL_POD_PAGE = "/pages/admin/tapal/createPODOutgoingTapal.xhtml?faces-redirect=true;";
	private final String OUTGOING_TAPAL_RETURNED_PAGE = "/pages/admin/tapal/viewReturnOutgoingTapal.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	LazyDataModel<OutgoingTapal> lazyOutgoingTapalList;

	@Getter
	@Setter
	private String action;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	Boolean returnedButtonFlag = true, podTapalFlag = false;

	@Getter
	@Setter
	Boolean podButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<DistrictMaster> districtList;

	@Getter
	@Setter
	List<TapalDeliveryType> tapalDeliveryTypeList;

	@Getter
	@Setter
	TapalDeliveryType tapalDeliveryType;

	@Getter
	@Setter
	List<TapalSenderType> tapalSenderTypeList;

	@Getter
	@Setter
	TapalSenderType tapalSenderType;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	DistrictMaster districtMaster;

	@Getter
	@Setter
	StateMaster stateMaster;

	@Getter
	@Setter
	List<SectionMaster> sectionList;

	@Getter
	@Setter
	SectionMaster sectionMaster;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	List<EntityMaster> selectedHoRoList;

	@Getter
	@Setter
	List<EntityTypeMaster> selectedEntityTypeList;

	@Getter
	@Setter
	List<EntityMaster> selectedEntityMasterList;

	@Getter
	@Setter
	Set<String> entityName = new HashSet<>();

	@Getter
	@Setter
	List<String> hoRoName = new ArrayList<>();

	@Getter
	@Setter
	Set<String> entityTypeName = new HashSet<>();

	@Getter
	@Setter
	EntityMaster entity;

	@Getter
	@Setter
	OutgoingTapal outgoingTapal;

	@Getter
	@Setter
	Date sendingDate;

	@Getter
	@Setter
	IncomingTapal incomingTapal;

	@Getter
	@Setter
	List<IncomingTapal> referenceNumberList;

	@Getter
	@Setter
	TapalFor tapalFor;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<TapalFor> tapalForList;

	@Getter
	@Setter
	Boolean entityFlag = false;

	@Getter
	@Setter
	Boolean podFlag = false;

	@Getter
	@Setter
	Boolean returnedFlag = false;

	@Getter
	@Setter
	OutgoingTapal selectoutgoingTapal;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	long letterSize;

	@Setter
	@Getter
	String fileName;

	@Setter
	@Getter
	SupplierMaster supplierMaster;

	@Setter
	@Getter
	List<SupplierMaster> supplierList;

	@Setter
	@Getter
	TapalPodRegister tapalPodRegister = new TapalPodRegister();

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	private TapalDTO tapalDTO;

	@Getter
	@Setter
	String referenceNumber;

	@Getter
	@Setter
	Date referenceDate;

	@Getter
	@Setter
	String dateFormat;

	@Getter
	@Setter
	Date returnDate;

	@Getter
	@Setter
	String returnReason;

	@PostConstruct
	public void init() {
		tapalDeliveryTypeList = commonDataService.loadTapalDeliveryType();
		tapalSenderTypeList = commonDataService.loadTapalSenderType();
		tapalForList = commonDataService.getTapalForList();
		stateList = commonDataService.getStateList();
		dateFormat = loginBean.getUserInfoDTO().getDateFormat();

		getReferenceNo();
		log.info("dateFormat is....." + dateFormat);
	}

	public String showListPage() {
		try {
			outgoingTapal = new OutgoingTapal();
			selectoutgoingTapal = new OutgoingTapal();
			tapalPodRegister = new TapalPodRegister();
			mapper = new ObjectMapper();
			editButtonFlag = true;
			podButtonFlag = true;
			viewButtonFlag = true;
			deleteButtonFlag = true;
			returnedButtonFlag = true;
			addButtonFlag = false;
			supplierMaster = new SupplierMaster();
			loadLazyOutgoingTapalList();
		} catch (Exception e) {
			log.info("inside list page exception------", e);
		}
		return OUTGOING_TAPAL_LIST_PAGE;
	}

	private void loadLazyOutgoingTapalList() {
		lazyOutgoingTapalList = new LazyDataModel<OutgoingTapal>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -5530342116331811930L;

			@Override

			public List<OutgoingTapal> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<OutgoingTapal> outgoingTapalList = null;
				try {
					outgoingTapalList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("filterdate-----------" + filters.get("createdDate"));
					log.info("Pagination request :::" + paginationRequest);
					url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
							+ "/outgoingtapal/getActiveOutgoingTapal";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						outgoingTapalList = mapper.readValue(jsonResponse, new TypeReference<List<OutgoingTapal>>() {
						});
						log.info("list----------" + outgoingTapalList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyoutgoingTapal List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return outgoingTapalList;
			}

			@Override
			public Object getRowKey(OutgoingTapal res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OutgoingTapal getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<OutgoingTapal> responseList = (List<OutgoingTapal>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (OutgoingTapal outgoingTapalObj : responseList) {
					if (outgoingTapalObj.getId().longValue() == value.longValue()) {
						outgoingTapal = outgoingTapalObj;
						return outgoingTapalObj;
					}
				}
				return null;
			}
		};

	}

	public String createOutgoingTapal() {
		try {
			outgoingTapal = new OutgoingTapal();
			entityFlag = false;
			stateMaster = new StateMaster();
			tapalDeliveryType = new TapalDeliveryType();
			sendingDate = new Date();
			tapalSenderType = new TapalSenderType();
			department = new Department();
			sectionMaster = new SectionMaster();
			entityMaster = new EntityMaster();
			entityTypeMaster = new EntityTypeMaster();
			districtMaster = new DistrictMaster();
			incomingTapal = new IncomingTapal();
			tapalFor = new TapalFor();
			// entity = new EntityMaster();
			getReferenceNo();
			selectedHoRoList = new ArrayList<>();
			selectedEntityTypeList = new ArrayList<>();
			selectedEntityMasterList = new ArrayList<>();
			tapalDTO = new TapalDTO();
			departmentList = commonDataService.getDepartment();
			referenceNumberList = commonDataService.getIncomeTapalReferenceNo();
			fileName = null;
		} catch (Exception e) {
			log.info("inside create page exception------", e);
		}
		return OUTGOING_TAPAL_CREATE_PAGE;
	}

	public void stateChange() {
		String url;
		try {
			log.info("statename--------" + stateMaster.getName());
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/incomingtapal/getDistrictByState/" + stateMaster.getName();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				districtList = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
				});
				log.info("districtList---------" + districtList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside stateChange method--", e);
		}
	}

	public void departmentChange() {
		String url;
		try {
			log.info("cepartment--------" + department.getName());
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/incomingtapal/getSectionByDesignation/" + department.getName();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				sectionList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
				log.info("sectionList---------" + sectionList.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside departmentChange method--", e);
		}
	}

	public void tapalForChange() {
		log.info("inside-------change-------");
		try {
			entityFlag = false;
			if (tapalFor.getName().equalsIgnoreCase("Entity")) {
				selectedHoRoList = new ArrayList<>();
				selectedEntityMasterList = new ArrayList<>();
				selectedEntityTypeList = new ArrayList<>();
				entityTypeList = commonDataService.getActiveEntityType();
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				log.info("entityMasterList size-------" + entityMasterList.size());
			}
		} catch (Exception e) {
			log.info("inside tapalForChange method--", e);
		}
	}

	public void headOfficeChange() {
		String url;
		try {

			entityTypeList = new ArrayList<>();
			// selectedEntityTypeList = new ArrayList<>();
			// entityList = new ArrayList<>();
			// entity = new EntityMaster();
			// selectedEntityMasterList=new ArrayList<>();
			if (selectedHoRoList != null && !selectedHoRoList.isEmpty()) {
				log.info("selected hoRo--------" + selectedHoRoList.size());
				List<Long> entityList = new ArrayList<>();
				for (EntityMaster obj : selectedHoRoList) {
					if (obj != null) {
						entityList.add(obj.getId());
					}
				}
				entityFlag = true;
				BaseDTO baseDTO = new BaseDTO();
				url = appPreference.getPortalServerURL() + "/entitytypemaster/getEntityTypeByRegionIdArrayay";
				baseDTO = httpService.post(url, entityList);

				log.info("selected regionId" + selectedHoRoList);
				if (baseDTO != null) {
					mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					entityTypeList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
					});
					log.info("entityTypeList---------" + entityTypeList.size());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			} else {
				entityFlag = false;
			}
		} catch (Exception e) {
			log.info("inside headOfficeChange method--", e);
		}
	}

	public void entityTypeChange() {
		log.info("<------- Start OutgoingTapalBean.entityTypeChange() -------->");
		String url;
		try {
			entityList = new ArrayList<>();
			selectedEntityMasterList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/outgoingtapal/getentityInfoByList";

			TapalDTO tapalDTO = new TapalDTO();
			tapalDTO.setEntityMasterList(selectedHoRoList);
			tapalDTO.setEntityTypeMasterList(selectedEntityTypeList);
			baseDTO = httpService.post(url, tapalDTO);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("entityList---------" + entityList.size());
			} else if (baseDTO.getStatusCode() == 8) {
				errorMap.notify(baseDTO.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside entityTypeChange method--", e);
		}
	}

	public void clearPage() {
		try {
			outgoingTapal = new OutgoingTapal();
			selectoutgoingTapal = new OutgoingTapal();
			tapalPodRegister = new TapalPodRegister();
			mapper = new ObjectMapper();
			editButtonFlag = true;
			podButtonFlag = true;
			viewButtonFlag = true;
			deleteButtonFlag = true;
			returnedButtonFlag = true;
			addButtonFlag = false;
			supplierMaster = new SupplierMaster();
			loadLazyOutgoingTapalList();
			selectedHoRoList = new ArrayList<>();
			selectedEntityTypeList = new ArrayList<>();
			selectedEntityMasterList = new ArrayList<>();
		} catch (Exception e) {
			log.info("inside list page exception------", e);
		}

	}

	public String saveOutgoingTapal() {
		BaseDTO baseDTO = null;
		String url = "";
		try {
			if (saveValidation(true)) {
				baseDTO = new BaseDTO();
				if (outgoingTapal.getPincode() != null) {
					Integer pincodeSize = String.valueOf(outgoingTapal.getPincode()).length();
					if (pincodeSize < 6) {
						log.info("======Pincode format is Not valid====");
						Util.addWarn("Postal code should be max 6 characters");
						return null;
					}

				}
				outgoingTapal.setSendingDate(sendingDate);
				outgoingTapal.setTapalFor(tapalFor);
				outgoingTapal.setTapalDeliveryType(tapalDeliveryType);
				outgoingTapal.setIncomingTapal(incomingTapal);
				outgoingTapal.setStateMaster(stateMaster);
				outgoingTapal.setDistrictMaster(districtMaster);
				outgoingTapal.setReferenceNumber(referenceNumber);
				// outgoingTapal.setReferenceDate(referenceDate);
				outgoingTapal.setEntityMaster(null);
				outgoingTapal.setDepartmentMaster(department);
				outgoingTapal.setSectionMaster(sectionMaster);

				tapalDTO.setOutgoingTapal(outgoingTapal);
				tapalDTO.setEntityMasterList(selectedEntityMasterList);

				url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/create";

				log.info("url...." + url);
				baseDTO = httpService.post(url, tapalDTO);
				if (baseDTO.getStatusCode() == 0) {
					clearPage();
					if ("ADD".equalsIgnoreCase(action)) {
						errorMap.notify(ErrorDescription.OUTGOINGTAPAL_SAVE.getErrorCode());
					} else if ("EDIT".equalsIgnoreCase(action)) {
						errorMap.notify(ErrorDescription.OUTGOINGTAPAL_UPDATE.getErrorCode());
					}
					return OUTGOING_TAPAL_LIST_PAGE;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.info("inside saveOutgoingTapal method--", e);
		}
		return null;
	}

	private boolean saveValidation(boolean valid) {
		valid = true;
		FacesContext fc = FacesContext.getCurrentInstance();
		if (tapalDeliveryType == null) {
			fc.addMessage("delivertype", new FacesMessage(ErrorCodeDescription.getDescription(35013)));
			valid = false;
		}
		if (StringUtils.isEmpty(outgoingTapal.getSubject())) {
			fc.addMessage("subject", new FacesMessage(ErrorCodeDescription.getDescription(35014)));
			valid = false;
		}
		if (tapalFor == null) {
			fc.addMessage("tapalforid", new FacesMessage(ErrorCodeDescription.getDescription(35015)));
			valid = false;
		} else {
			if (tapalFor.getName().equalsIgnoreCase("Entity")) {
				if (selectedHoRoList == null) {
					fc.addMessage("headoffice", new FacesMessage(ErrorCodeDescription.getDescription(35020)));
					valid = false;
				} else {
					// if (!"Head Office Entity".equalsIgnoreCase(entityMaster.getName())) {
					if (selectedEntityTypeList == null || selectedEntityTypeList.isEmpty()) {
						fc.addMessage("entityType", new FacesMessage(ErrorCodeDescription.getDescription(35021)));
						valid = false;
					}
					if (selectedEntityMasterList == null || selectedEntityMasterList.isEmpty()) {
						fc.addMessage("entityid", new FacesMessage(ErrorCodeDescription.getDescription(35022)));
						valid = false;
					}
					// }
				}
			} else {
				if (StringUtils.isEmpty(outgoingTapal.getAddressLine1())) {
					fc.addMessage("adress", new FacesMessage(ErrorCodeDescription.getDescription(35017)));
					valid = false;
				}
				if (StringUtils.isEmpty(outgoingTapal.getName())) {
					fc.addMessage("name", new FacesMessage(ErrorCodeDescription.getDescription(35016)));
					valid = false;
				}
//				if (stateMaster == null) {
//					fc.addMessage("stateid", new FacesMessage(ErrorCodeDescription.getDescription(35018)));
//					valid = false;
//				}
//				if (districtMaster == null) {
//					fc.addMessage("districtid", new FacesMessage(ErrorCodeDescription.getDescription(35019)));
//					valid = false;
//				}
//				if (outgoingTapal.getPincode() == null) {
//					fc.addMessage("pincode", new FacesMessage(ErrorCodeDescription.getDescription(35024)));
//					valid = false;
//				}
			}
		}
		/*
		 * if (incomingTapal == null) { fc.addMessage("referenceno", new
		 * FacesMessage(ErrorCodeDescription.getDescription(35023))); valid = false; }
		 */
		return valid;
	}

	public void onRowSelect(SelectEvent event) {
		editButtonFlag = false;
		podButtonFlag = false;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		returnedButtonFlag = false;

		selectoutgoingTapal = (OutgoingTapal) event.getObject();
		Date returnedDate = selectoutgoingTapal.getReturnedDate();
		boolean podstatus = selectoutgoingTapal.getPodStatus();
		if (returnedDate != null) {
			returnedButtonFlag = true;
			editButtonFlag = true;
			podButtonFlag = false;
			deleteButtonFlag = true;
		}
		if (returnedDate != null && podstatus == true) {
			returnedButtonFlag = true;
			editButtonFlag = true;
			podButtonFlag = true;
			deleteButtonFlag = true;
		}
		podTapalFlag = selectoutgoingTapal.getPodStatus();
		log.info("podTapalFlag==>" + podTapalFlag);
	}

	public String clearButton() {
		editButtonFlag = true;
		viewButtonFlag = true;
		podButtonFlag = true;
		returnedButtonFlag = true;
		podTapalFlag = false;
		deleteButtonFlag = true;
		addButtonFlag = false;
		selectoutgoingTapal = null;
		returnDate = null;
		returnReason = null;
		loadLazyOutgoingTapalList();
		return OUTGOING_TAPAL_LIST_PAGE;
	}

	public String viewOutgoinTapalPOD() {
		String url = "";
		String status = "";
		List<EntityMaster> entityMasterSel = new ArrayList<>();
		List<EntityTypeMaster> entityTypeSel = new ArrayList<>();
		try {
			if (selectoutgoingTapal.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			status = checkPOD(outgoingTapal);
			editButtonFlag = true;
			viewButtonFlag = true;
			podButtonFlag = true;
			returnedButtonFlag = true;
			podTapalFlag = false;
			deleteButtonFlag = true;
			addButtonFlag = false;
			fileName = null;
			if (status != "") {
				if ("Fail".equalsIgnoreCase(status)) {
					errorMap.notify(ErrorDescription.TAPAL_POD_ALREADY_EXISTS.getErrorCode());
					clearButton();
					return null;
				}
			}

			BaseDTO baseDTO = new BaseDTO();
			tapalDTO = new TapalDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/get/"
					+ selectoutgoingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				outgoingTapal = new OutgoingTapal();
				tapalPodRegister = new TapalPodRegister();

				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalDTO = mapper.readValue(jsonResponse, new TypeReference<TapalDTO>() {
				});

				setOutgoingTapal(tapalDTO.getOutgoingTapal());
				setTapalDeliveryType(outgoingTapal.getTapalDeliveryType());
				setTapalFor(outgoingTapal.getTapalFor());
				setIncomingTapal(outgoingTapal.getIncomingTapal());
				setStateMaster(outgoingTapal.getStateMaster());
				setDistrictMaster(outgoingTapal.getDistrictMaster());

				department = outgoingTapal.getDepartmentMaster();
				if (department != null) {
					log.info("department id==> " + department.getId());
				} else {
					log.error("Department not found");
				}
				departmentList = commonDataService.getDepartment();
				if (departmentList != null && !departmentList.isEmpty()) {
					log.error("departmentList size==> " + departmentList.size());
					departmentChange();
				}
				sectionMaster = outgoingTapal.getSectionMaster();
				if (sectionMaster != null) {
					log.info("sectionMaster id==> " + sectionMaster.getId());
				} else {
					log.error("Section Master not found");
				}

				if (tapalDTO.getOutgoingTapal().getFilePath() != null) {
					String filePath[] = tapalDTO.getOutgoingTapal().getFilePath().split("uploaded/");
					log.info("filePath 0==>" + filePath[0]);
					log.info("filePath 1==>" + filePath[1]);
					fileName = filePath[1];
					log.info("fileName==>" + fileName);
				} else {
					log.error("File path not found");
				}

				if (tapalFor.getName().equals("Entity")) {
					tapalForChange();
					if (tapalDTO.getEntityMasterList() != null) {
						List<EntityMaster> entityMasterList = tapalDTO.getEntityMasterList();
						for (EntityMaster obj : entityMasterList) {
							EntityTypeMaster entityType = new EntityTypeMaster();
							entityType = obj.getEntityTypeMaster();
							if (!entityTypeSel.contains(entityType))
								entityTypeSel.add(entityType);
						}
					}
					setSelectedHoRoList(tapalDTO.getHoRoList());
					setSelectedEntityTypeList(entityTypeSel);
//					headOfficeChange();
					entityFlag = true;
				} else {
					setEntityMaster(null);
					entityFlag = false;
				}
				if (selectedHoRoList != null && selectedHoRoList.size() > 0) {
					setSelectedHoRoList(new ArrayList<>());
					entityTypeChange();
				}
				if (tapalDTO.getTapalPodRegister() != null) {
					setTapalPodRegister(tapalDTO.getTapalPodRegister());
					setSupplierMaster(tapalPodRegister.getSupplierMaster());
					podFlag = true;
				} else {
					setTapalPodRegister(null);
					setSupplierMaster(null);
					podFlag = false;
				}
				setSelectedEntityMasterList(tapalDTO.getEntityMasterList());
				setTapalFor(outgoingTapal.getTapalFor());
				log.info("action-----" + action);
				if ("EDIT".equalsIgnoreCase(action)) {
					outgoingTapalEdit(outgoingTapal);
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("VIEW".equalsIgnoreCase(action)) {
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("POD".equalsIgnoreCase(action)) {
					tapalPodRegister = new TapalPodRegister();
					status = checkPOD(outgoingTapal);
					if (status != "") {
						loadSupplierList();
						if ("Fail".equalsIgnoreCase(status)) {
							errorMap.notify(ErrorDescription.TAPAL_POD_ALREADY_EXISTS.getErrorCode());
						}
					}
					return OUTGOING_TAPAL_POD_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("inside view method exception----", e);
		}
		return "";
	}

	public String viewOutgoinTapal() {
		String url = "";
		String status = "";
		List<EntityMaster> entityMasterSel = new ArrayList<>();
		List<EntityTypeMaster> entityTypeSel = new ArrayList<>();
		try {
			if (selectoutgoingTapal.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("id-----------" + selectoutgoingTapal.getId());

			status = checkPOD(outgoingTapal);
			editButtonFlag = true;
			viewButtonFlag = true;
			podButtonFlag = true;
			returnedButtonFlag = true;
			podTapalFlag = false;
			deleteButtonFlag = true;
			addButtonFlag = false;
			fileName = null;
			hoRoName = new ArrayList<>();
			entityTypeName = new HashSet<>();

			BaseDTO baseDTO = new BaseDTO();
			tapalDTO = new TapalDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/get/"
					+ selectoutgoingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				outgoingTapal = new OutgoingTapal();
				// tapalPodRegister=new TapalPodRegister();

				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalDTO = mapper.readValue(jsonResponse, new TypeReference<TapalDTO>() {
				});
				setOutgoingTapal(tapalDTO.getOutgoingTapal());
				setTapalDeliveryType(outgoingTapal.getTapalDeliveryType());
				setTapalFor(outgoingTapal.getTapalFor());
				setIncomingTapal(outgoingTapal.getIncomingTapal());
				setStateMaster(outgoingTapal.getStateMaster());
				setDistrictMaster(outgoingTapal.getDistrictMaster());

				// fileName = tapalDTO.getOutgoingTapal().getFilePath();
				if (tapalDTO.getOutgoingTapal().getFilePath() != null) {
					String filePath[] = tapalDTO.getOutgoingTapal().getFilePath().split("uploaded/");
					log.info("filePath 0==>" + filePath[0]);
					log.info("filePath 1==>" + filePath[1]);
					fileName = filePath[1];
					log.info("fileName==>" + fileName);
				} else {
					log.error("File path not found");
				}
				department = outgoingTapal.getDepartmentMaster();
				if (department != null) {
					log.info("department id==> " + department.getId());
				} else {
					log.error("Department not found");
				}
				sectionMaster = outgoingTapal.getSectionMaster();
				sendingDate = outgoingTapal.getSendingDate();
				if (sectionMaster != null) {
					log.info("sectionMaster id==> " + sectionMaster.getId());
				} else {
					log.error("Section Master not found");
				}
				entityName = new HashSet<>();
				if (tapalFor.getName().equals("Entity")) {
					if (tapalDTO.getEntityMasterList() != null) {
						List<EntityMaster> entityMasterList = tapalDTO.getEntityMasterList();
						for (EntityMaster obj : entityMasterList) {
							EntityTypeMaster entityType = new EntityTypeMaster();
							entityType = obj.getEntityTypeMaster();
							log.info("entityType list.." + entityType);
							log.info("entity list.." + obj.getName());
							String entName = entityType.getEntityName() + " - " + obj.getName();
							entityName.add(entName);
							if (!entityTypeSel.contains(entityType)) {
								entityTypeSel.add(entityType);
								entityTypeName.add(entityType.getEntityName());
							}
						}

					}
					setSelectedHoRoList(tapalDTO.getHoRoList());
					setSelectedEntityTypeList(entityTypeSel);
					headOfficeChange();
					if (tapalDTO.getHoRoList() != null && tapalDTO.getHoRoList().size() > 0) {
						List<EntityMaster> ent = tapalDTO.getHoRoList();
						log.info("entity list size().." + ent.size());
						for (EntityMaster obj : ent) {
							if (!hoRoName.isEmpty() && obj != null) {
								if (!hoRoName.contains(obj.getName())) {
									String entity = obj.getName();
									hoRoName.add(entity);
									log.info("entity horo.." + obj.getName());
								}
							}
						}
					}
					entityFlag = true;
				} else {
					setEntityMaster(null);
					entityFlag = false;
				}
				if (selectedHoRoList != null && selectedHoRoList.size() > 0) {
					setSelectedEntityMasterList(tapalDTO.getEntityMasterList());
					entityTypeChange();
				}
				if (tapalDTO.getTapalPodRegister() != null) {
					setTapalPodRegister(tapalDTO.getTapalPodRegister());
					setSupplierMaster(tapalPodRegister.getSupplierMaster());
					podFlag = true;
				} else {
					setTapalPodRegister(null);
					setSupplierMaster(null);
					podFlag = false;
				}
				if (tapalDTO.getOutgoingTapal().getReturnedDate() != null) {
					returnedFlag = true;
				} else {
					returnedFlag = false;
				}
				log.info("action-----" + action);
				if ("EDIT".equalsIgnoreCase(action)) {
					outgoingTapalEdit(outgoingTapal);
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("VIEW".equalsIgnoreCase(action)) {
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("POD".equalsIgnoreCase(action)) {
					tapalPodRegister = new TapalPodRegister();
					status = checkPOD(outgoingTapal);
					if (status != "") {
						loadSupplierList();
						if ("Fail".equalsIgnoreCase(status)) {
							errorMap.notify(ErrorDescription.TAPAL_POD_ALREADY_EXISTS.getErrorCode());
						}
					}
					return OUTGOING_TAPAL_POD_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("inside view method exception----", e);
		}
		return "";
	}

	private String checkPOD(OutgoingTapal outgoingTapal) {
		String status = "", url = "";
		try {
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/checkPOD/"
					+ outgoingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				status = "Success";
			} else if (baseDTO.getStatusCode().equals(ErrorDescription.TAPAL_POD_ALREADY_EXISTS.getErrorCode())) {
				status = "Fail";
			}
		} catch (Exception e) {
			log.error("inside checkPOD method exception----", e);
		}
		return status;
	}

	private void loadSupplierList() {
		String url;
		try {
			supplierList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/outgoingtapal/getSupplierList";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplier list-------" + supplierList.size());
			}
		} catch (Exception e) {
			log.error("inside loadSupplierList method exception----", e);
		}
	}

	private void getReferenceNo() {
		log.error("<!------Start  getReferenceNo method ----->");
		String url;
		List<String> refNoList = new ArrayList<>();
		try {
			supplierList = new ArrayList<>();
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/getRefNo";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				refNoList = mapper.readValue(jsonResponse, new TypeReference<List<String>>() {
				});
				log.info("refNoList list-------" + refNoList.size());
			}

			if (refNoList != null && refNoList.get(0) != null)
				referenceNumber = refNoList.get(0);

			log.error("<!------referenceNumber is:" + referenceNumber);

		} catch (Exception e) {
			log.error("inside getReferenceNo method exception----", e);
		}
		log.error("<!------End  getReferenceNo method ----->");
	}

	private void outgoingTapalEdit(OutgoingTapal outgoingTapal2) {
		try {
			referenceNumberList = commonDataService.getIncomeTapalReferenceNo();
			if (stateMaster != null) {
				stateChange();
			}
			if (tapalFor.getName().equalsIgnoreCase("Entity")) {
				entityMasterList = commonDataService.loadHeadAndRegionalOffice();
				entityFlag = true;
			} else {
				entityFlag = false;
			}
			if (selectedHoRoList != null && selectedHoRoList.size() > 0) {
				List<EntityMaster> entityMasterSel = new ArrayList<>();
				List<EntityTypeMaster> entityTypeSel = new ArrayList<>();
				if (tapalFor.getName().equals("Entity")) {
					if (tapalDTO.getEntityMasterList() != null) {
						List<EntityMaster> entityMasterList = tapalDTO.getEntityMasterList();
						for (EntityMaster obj : entityMasterList) {
							EntityTypeMaster entityType = new EntityTypeMaster();
							entityType = obj.getEntityTypeMaster();
							if (!entityTypeSel.contains(entityType))
								entityTypeSel.add(entityType);
						}
					}
					headOfficeChange();
					setSelectedHoRoList(tapalDTO.getHoRoList());
					setSelectedEntityTypeList(entityTypeSel);
				}
			}
			if (selectedEntityMasterList != null && !selectedEntityMasterList.isEmpty()) {
				entityTypeChange();
				setSelectedEntityMasterList(tapalDTO.getEntityMasterList());
			}
		} catch (Exception e) {
			log.error("inside edit method exception----", e);
		}
	}

	public void fileUpload(FileUploadEvent event) {
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();

		String type = FilenameUtils.getExtension(uploadedFile.getFileName());
		log.info(" File Type :: " + type);
		if (!type.equalsIgnoreCase("png") && !type.equalsIgnoreCase("jpg") && !type.equalsIgnoreCase("doc")
				&& !type.equalsIgnoreCase("jpeg") && !type.equalsIgnoreCase("gif") && !type.equalsIgnoreCase("docx")
				&& !type.equalsIgnoreCase("pdf") && !type.equalsIgnoreCase("xlsx") && !type.equalsIgnoreCase("xls")) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
			return;
		}

		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("TAPAL_UPLOAD_FILE_SIZE"));
		letterSize = letterSize / 1000;
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}

		String outGoingTapalUploadPath = commonDataService.getFilePath("OUTGOING_TAPAL_UPLOAD_PATH", "FILE");
		log.info("outGoingTapalUploadPath >>>> " + outGoingTapalUploadPath);

		String filePath = Util.fileUpload(outGoingTapalUploadPath, uploadedFile);
		setFileName(uploadedFile.getFileName());
		outgoingTapal.setFilePath(filePath);
		errorMap.notify(ErrorDescription.OUTGOINGTAPAL_FILEUPLOAD.getErrorCode());
		log.info(" tapalFileUpload  is uploaded successfully");
	}

	public void deleteOutgoingTapal() {
		String url;
		try {
			log.info("id-----------" + selectoutgoingTapal.getId());
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/deleteById/"
					+ selectoutgoingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.OUTGOINGTAPAL_DELETE.getErrorCode());
				loadLazyOutgoingTapalList();
				editButtonFlag = true;
				viewButtonFlag = true;
				podButtonFlag = true;
				returnedButtonFlag = true;
				podTapalFlag = false;
				deleteButtonFlag = true;
				addButtonFlag = false;
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("inside deleteIncomeingTapal method--", e);
		}
	}

	public String savePOD() {
		String url;
		try {
			if (savePODValidation(true)) {
				tapalPodRegister.setSupplierMaster(supplierMaster);
				tapalPodRegister.setOutgoingTapal(outgoingTapal);
				BaseDTO baseDTO = new BaseDTO();
				url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/outgoingtapal/savePOD/";
				baseDTO = httpService.post(url, tapalPodRegister);
				if (baseDTO.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.TAPAL_POD_SAVE.getErrorCode());
					return clearButton();
				} else if (baseDTO.getStatusCode().equals(ErrorDescription.TAPAL_POD_UPDATE.getErrorCode())) {
					errorMap.notify(ErrorDescription.TAPAL_POD_UPDATE.getErrorCode());
					return clearButton();
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.info("inside savePOD method--", e);
		}
		return null;
	}

	private boolean savePODValidation(boolean valid) {
		valid = true;
		if (supplierMaster == null) {
			errorMap.notify(ErrorDescription.TAPAL_SUPPLIER_NAME.getErrorCode());
			valid = false;
		}
		if (StringUtils.isEmpty(tapalPodRegister.getPodNo())) {
			errorMap.notify(ErrorDescription.TAPAL_POD_NO.getErrorCode());
			valid = false;
		}
		if (tapalPodRegister.getPodAmount() == null) {
			errorMap.notify(ErrorDescription.TAPAL_POD_DATE.getErrorCode());
			valid = false;
		}
		if (tapalPodRegister.getPodDate() == null) {
			errorMap.notify(ErrorDescription.TAPAL_POD_AMOUNT.getErrorCode());
			valid = false;
		}
		return valid;
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			log.info("Outgoing Tapal File Path - " + outgoingTapal == null ? null : outgoingTapal.getFilePath());
			File file = new File(outgoingTapal.getFilePath());
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public String returnedOutgoinTapal() {
		String url = "";
		String status = "";
		List<EntityMaster> entityMasterSel = new ArrayList<>();
		List<EntityTypeMaster> entityTypeSel = new ArrayList<>();
		try {
			if (selectoutgoingTapal.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("id-----------" + selectoutgoingTapal.getId());

			status = checkPOD(outgoingTapal);
			editButtonFlag = true;
			viewButtonFlag = true;
			podButtonFlag = true;
			returnedButtonFlag = true;
			podTapalFlag = false;
			deleteButtonFlag = true;
			addButtonFlag = false;

			BaseDTO baseDTO = new BaseDTO();
			tapalDTO = new TapalDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/outgoingtapal/get/"
					+ selectoutgoingTapal.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				outgoingTapal = new OutgoingTapal();
				// tapalPodRegister=new TapalPodRegister();

				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalDTO = mapper.readValue(jsonResponse, new TypeReference<TapalDTO>() {
				});
				setOutgoingTapal(tapalDTO.getOutgoingTapal());
				setTapalDeliveryType(outgoingTapal.getTapalDeliveryType());
				setTapalFor(outgoingTapal.getTapalFor());
				setIncomingTapal(outgoingTapal.getIncomingTapal());
				setStateMaster(outgoingTapal.getStateMaster());
				setDistrictMaster(outgoingTapal.getDistrictMaster());
				if (tapalFor.getName().equals("Entity")) {
					if (tapalDTO.getEntityMasterList() != null) {
						List<EntityMaster> entityMasterList = tapalDTO.getEntityMasterList();
						for (EntityMaster obj : entityMasterList) {
							EntityTypeMaster entityType = new EntityTypeMaster();
							entityType = obj.getEntityTypeMaster();
							log.info("entityType list.." + entityType);
							log.info("entity list.." + obj.getName());
							String entName = entityType.getEntityName() + " - " + obj.getName();
							entityName.add(entName);
							if (!entityTypeSel.contains(entityType)) {
								entityTypeSel.add(entityType);
								entityTypeName.add(entityType.getEntityName());
							}
						}

					}
					headOfficeChange();
					setSelectedHoRoList(tapalDTO.getHoRoList());
					setSelectedEntityTypeList(entityTypeSel);
					if (tapalDTO.getHoRoList() != null && tapalDTO.getHoRoList() != null) {
						List<EntityMaster> ent = tapalDTO.getHoRoList();
						log.info("entity list size().." + ent.size());
						for (EntityMaster obj : ent) {
							if (obj != null && obj.getName() != null && !hoRoName.contains(obj.getName())) {
								String entity = obj.getName();
								hoRoName.add(entity);
								log.info("entity horo.." + obj.getName());
							}
						}
					}
					entityFlag = true;
				} else {
					setEntityMaster(null);
					entityFlag = false;
				}
				if (selectedHoRoList != null && selectedHoRoList.size() > 0) {
					entityTypeChange();
					setSelectedEntityMasterList(tapalDTO.getEntityMasterList());
				}
				if (tapalDTO.getTapalPodRegister() != null) {
					setTapalPodRegister(tapalDTO.getTapalPodRegister());
					setSupplierMaster(tapalPodRegister.getSupplierMaster());
					podFlag = true;
				} else {
					setTapalPodRegister(null);
					setSupplierMaster(null);
					podFlag = false;
				}
				log.info("action-----" + action);
				if ("EDIT".equalsIgnoreCase(action)) {
					outgoingTapalEdit(outgoingTapal);
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("VIEW".equalsIgnoreCase(action)) {
					return OUTGOING_TAPAL_VIEW_PAGE;
				} else if ("POD".equalsIgnoreCase(action)) {
					tapalPodRegister = new TapalPodRegister();
					status = checkPOD(outgoingTapal);
					if (status != "") {
						loadSupplierList();
						if ("Fail".equalsIgnoreCase(status)) {
							errorMap.notify(ErrorDescription.TAPAL_POD_ALREADY_EXISTS.getErrorCode());
						}
					}
					return OUTGOING_TAPAL_POD_PAGE;
				} else if ("RETURNED".equalsIgnoreCase(action)) {
					return OUTGOING_TAPAL_RETURNED_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("inside view method exception----", e);
		}
		return "";
	}

	public String saveReturnedOutgoingTapal() {
		BaseDTO baseDTO = null;
		String url = "";

		try {
			if (returnDate == null || returnReason == null) {
				return null;
			}
			log.info("id-----------" + outgoingTapal.getId());
			outgoingTapal.setReturnedDate(returnDate);
			outgoingTapal.setReturnedReason(returnReason);
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/outgoingtapal/saveReturnedTapal";
			baseDTO = httpService.post(url, outgoingTapal);
			if (baseDTO.getStatusCode() == 0) {
				if ("RETURNED".equalsIgnoreCase(action)) {
					errorMap.notify(
							ErrorDescription.getError(AdminErrorCode.OUTGOINGTAPAL_RETURNED_UPDATE).getErrorCode());
				}
				return clearButton();
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

		} catch (Exception e) {
			log.info("inside ReturnedOutgoingTapal method--", e);
		}
		return null;

	}

	public void downloadOutgoingFile(String downloadFile) {
		InputStream input = null;
		try {
			log.info("downloadIncomingFile downloadFile==> " + downloadFile);

			String outgoingTapalUploadPath = commonDataService.getAppKeyValue("OUTGOING_TAPAL_UPLOAD_PATH");
			log.info("download incomingTapalUploadPath==> " + outgoingTapalUploadPath);
			File files = new File(outgoingTapalUploadPath + "/uploaded/" + downloadFile);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void sectionChange() {
		String url;
		try {
//			employeeList = new ArrayList<>();
//			BaseDTO baseDTO = new BaseDTO();
//			log.info("selectedSection Master--------" + selectedSectionList.size());
//			url = APP_SERVER_URL +  "/employee/findEmployeeDetailsBySectionList";
//			baseDTO = httpService.post(url,selectedSectionList);
//			if (baseDTO.getStatusCode() == 0) {
//				mapper = new ObjectMapper();
//				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
//				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
//				});
//				log.info("employeeList---------" + employeeList.size());
//			} else {
//				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
//			}
		} catch (Exception e) {
			log.info("inside sectionChange method--", e);
		}
	}

}
