package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.MedicalLeaveConfig;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.LeaveRequestDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.LeaveEligibilityMaster;
import in.gov.cooptex.core.model.LeaveRequest;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@ManagedBean("employeeLeaveRequestBean")
@Scope("session")
@Log4j2
public class EmployeeLeaveRequestBean {

	private static final long serialVersionUID = 1L;
	private final String EMP_LEAVE_REQ_LIST_PAGE = "/pages/personnelHR/employeeProfile/listEmployeeLeaveRequest.xhtml?faces-redirect=true;";
	private final String EMP_LEAVE_REQ_ADD_PAGE = "/pages/personnelHR/employeeProfile/createEmployeeLeaveRequests.xhtml?faces-redirect=true;";
	private final String EMP_LEAVE_REQ_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewEmployeeLeaveRequest.xhtml?faces-redirect=true;";
	private final String EMP_LEAVE_REQ_EDIT_PAGE = "/pages/personnelHR/employeeProfile/editEmployeeLeaveRequests.xhtml?faces-redirect=true;";
	private final String EMP_LEAVE_REQ_CANCEL_PAGE = "/pages/personnelHR/employeeProfile/createLeaveCancelRequest.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	// Entities
	@Getter
	@Setter
	LeaveRequest leaveRequest;

	@Getter
	@Setter
	LeaveRequestDTO leaveRequestDTO = new LeaveRequestDTO();

	EmployeeMaster employeeMaster;

	// variables
	@Getter
	@Setter
	String action, leaveDocumentFileName;

	@Getter
	@Setter
	Date dateSelect;

	@Getter
	@Setter
	Date today = new Date();

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasters;

	@Getter
	@Setter
	String focusProperty;

	@Getter
	@Setter
	private UploadedFile leaveDocument;

	long employeePhotoSize;

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	LazyDataModel<LeaveRequest> employeeLeaveRequests;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	LeaveRequest selectedLeaveRequest;

	@Getter
	@Setter
	List<LeaveRequest> filteredLeaveRequests = new ArrayList<>();

	@Getter
	@Setter
	int count = 0;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;

	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean cancelLeaveButtonFlag = false;

	@Getter
	@Setter
	LeaveEligibilityMaster leaveEligibilityMaster = new LeaveEligibilityMaster();

	@Getter
	@Setter
	Double leaveConsumed = 0.0;

	@Getter
	@Setter
	Double leaveBalance = 0.0;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@PostConstruct
	public void init() {
		log.info("EmployeeLeaveRequestBean Init is executed.....................");
		loadAppConfigValues();
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public List<LeaveTypeMaster> loadLeaveTypes() {
		log.info("Loading all active leave types");
		List<LeaveTypeMaster> leaveTypeList = new ArrayList<>();
		try {
			BaseDTO leaveTypeMasterDto = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/leavetype/getallactive";

			leaveTypeMasterDto = httpService.get(url);
			if (leaveTypeMasterDto != null) {
				String jsonResponse = mapper.writeValueAsString(leaveTypeMasterDto.getResponseContents());
				leaveTypeMasters = mapper.readValue(jsonResponse, new TypeReference<List<LeaveTypeMaster>>() {
				});
				if (leaveTypeMasters.isEmpty() || leaveTypeMasters.size() == 0) {
					log.info("Leave types list size is empty :" + leaveTypeMasters.size());
				} else {
					log.info("Leave types list size is :" + leaveTypeList.size());
				}
			}
		} catch (Exception e) {
			log.error(": Exception while retiveing leave type masters :", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return leaveTypeMasters;
	}

	public String showEmployeeLeaveRequestListPage() {
		log.info("<======== Starts EmployeeLeaveRequestBean.showEmployeeLeaveRequestListPage..========>");
		mapper = new ObjectMapper();
		userMaster = loginBean.getUserDetailSession();
		leaveTypeMasters = loadLeaveTypes();
		addButtonFlag = false;
		approveButtonFlag = false;
		editButtonFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		cancelLeaveButtonFlag = false;
		selectedLeaveRequest = new LeaveRequest();
		BaseDTO response = httpService.get(SERVER_URL + "/employee/getbyuserid/" + userMaster.getId());
		try {
			if (response.getStatusCode() == 0) {

				jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				employeeMaster = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
				});
			}
			loadLazyLeaveRequestList();
		} catch (Exception e) {
			log.info("Exception occured while fetching employee master");
		}
		log.info("<======== Ends EmployeeLeaveRequestBean.showEmployeeLeaveRequestListPage..========>");
		return EMP_LEAVE_REQ_LIST_PAGE;
	}

	public String employeeLeaveRequestAction() {
		log.info("<======== Starts EmployeeLeaveRequestBean.employeeLeaveRequestAction..========>");

		log.info("User Details :::" + userMaster);
		try {
			leaveTypeMasters = loadLeaveTypes();
			if (action.equals("ADD")) {
				leaveDocumentFileName = null;
				leaveRequest = new LeaveRequest();
				leaveEligibilityMaster = new LeaveEligibilityMaster();
				BaseDTO response = httpService.get(SERVER_URL + "/employee/getbyuserid/" + userMaster.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					employeeMaster = mapper.readValue(jsonResponse, new TypeReference<EmployeeMaster>() {
					});
					log.info("<====== EmployeeLeaveRequestBean.employeeLeaveRequestAction Ends====>" + employeeMaster);
					String appValue = commonDataService.getAppKeyValueWithserverurl("ENABLE_PAST_DATE", SERVER_URL);
					if (appValue.equalsIgnoreCase("true")) {
						dateSelect = null;
					} else {
						dateSelect = new Date();
					}
					return EMP_LEAVE_REQ_ADD_PAGE;
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== EmployeeLeaveRequestBean.employeeLeaveRequestAction Ends====>");
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in EmployeeLeaveRequestBean.employeeLeaveRequestAction ", e);
		}
		log.info("<======== Ends EmployeeLeaveRequestBean.employeeLeaveRequestAction..========>");
		return null;
	}

	public String submitLeaveRequest() {
		log.info("<======== Starts EmployeeLeaveRequestBean.submitLeaveRequest..========>" + leaveRequest);
		if (leaveRequest == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveType() == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVETYPE_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveType().getName().equalsIgnoreCase("Maternity Leave")
				&& leaveRequest.getDocumentPath() == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_DOCUMENT_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveType().getName().equalsIgnoreCase("Medical Leave")
				&& leaveRequest.getDocumentPath() == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_DOCUMENT_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveBalance() == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_BALANCE_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getNoOfDaysRequested() == null) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_REQUESTED_LEAVE_DAYS_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveFromDate() == null) {
			// errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DAY_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveToDate() == null) {
			// errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_TO_DAY_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getReason() == null || leaveRequest.getReason().isEmpty()) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_REASON_REQUIRED.getErrorCode());
			return null;
		}
		if (leaveRequest.getLeaveBalanceDouble() < leaveRequest.getNoOfDaysRequested()) {
			// errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
			AppUtil.addWarn(
					"You are not eligible to take leave.Leave Balance is " + leaveRequest.getLeaveBalanceDouble());
			return null;
		}
		if (leaveRequest.getLeaveToDate().before(leaveRequest.getLeaveFromDate())) {
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_INVALID_FROM_TO_DATE.getErrorCode());
			return null;
		}

		if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("UEL")) {
			Calendar retirementDateCal = Calendar.getInstance();
			retirementDateCal.setTime(employeeMaster.getPersonalInfoEmployment().getRetirementDate());
			LocalDate retirementDate = LocalDate.of(retirementDateCal.get(Calendar.YEAR),
					retirementDateCal.get(Calendar.MONTH) + 1, retirementDateCal.get(Calendar.DAY_OF_MONTH));
			Calendar currentDateCal = Calendar.getInstance();
			currentDateCal.setTime(new Date());
			LocalDate currentDate = LocalDate.of(currentDateCal.get(Calendar.YEAR),
					currentDateCal.get(Calendar.MONTH) + 1, currentDateCal.get(Calendar.DAY_OF_MONTH));
			Period difference = Period.between(currentDate, retirementDate);
			Integer serviceYears = 0;
			serviceYears = difference.getYears();
			if (serviceYears > 2) {
				Util.addWarn("your Not eligible for this type leave");
				return null;
			}
		}

		if (leaveRequest.getType() != null && leaveRequest.getType().equalsIgnoreCase("Surrendered")) {
			leaveRequestDTO = new LeaveRequestDTO();
			leaveRequest.setEmployeeMaster(employeeMaster);
			leaveRequestDTO.setLeaveRequest(leaveRequest);
			if (checkAlreadyLoanPreclosured()) {
				Util.addWarn("Maximum 15 days only allowed to apply leave within one year for Surrendered");
				return null;
			}
		}
		if (leaveRequest.getLeaveType().getCode() != null
				&& (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("CL")
						|| leaveRequest.getLeaveType().getCode().equalsIgnoreCase("ML"))) {
			if (leaveRequest.getType() != null && leaveRequest.getType().equalsIgnoreCase("HALF_DAY")) {
				if ((StringUtils.isEmpty(leaveRequest.getTimeSession())) || leaveRequest.getTimeSession() == null
						|| leaveRequest.getTimeSession().equals("null")) {
					AppUtil.addWarn("Please select Session");
					return null;
				}
			}
		}
		if (leaveRequest.getLeaveType() != null && leaveRequest.getLeaveType().getCode().equalsIgnoreCase("EL")) {
			if ((StringUtils.isEmpty(leaveRequest.getType()) && leaveRequest.getType() == null)) {
				AppUtil.addWarn("Please select Leave Category");
				return null;
			}
		}
		leaveRequest.setCreatedBy(userMaster);
		leaveRequest.setCreatedDate(new Date());
		leaveRequest.setEmployeeMaster(employeeMaster);
		leaveRequest.setDepartment(employeeMaster.getPersonalInfoEmployment().getDepartment());
		leaveRequest.setDateOfApplication(new Date());
		leaveRequest.setHeadOrRegionOffice(employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice());
		leaveRequest.setEntityMaster(employeeMaster.getPersonalInfoEmployment().getWorkLocation());
		leaveRequest.setEntityTypeMaster(employeeMaster.getPersonalInfoEmployment().getEntityType());
		if (action.equalsIgnoreCase("ADD")) {
			String resource = SERVER_URL + "/leaverequest/createEmployeeRequest";
			// BeanUtils.copyProperties(leaveRequestDTO,leaveRequest);
			BaseDTO baseDto = httpService.post(resource, leaveRequest);
			if (baseDto != null && baseDto.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_SAVED_SUCCESS.getErrorCode());
				return showEmployeeLeaveRequestListPage();
			} else if (baseDto != null
					&& baseDto.getStatusCode().equals(MastersErrorCode.REPORTING_MANAGER_NOT_AVAILABLE_TO_YOU)) {
				AppUtil.addWarn("Reporting Manager Not available for you");
				return null;
			} else if (baseDto != null
					&& baseDto.getStatusCode().equals(MastersErrorCode.PREVIOUS_APPROVAL_IS_PENDING)) {
				AppUtil.addWarn("Previous approval is pending");
				return null;
			} else if (baseDto != null && baseDto.getStatusCode()
					.equals(ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_ALREADY_EXISTS).getCode())) {
				AppUtil.addWarn("Already leave applied for this dates");
				return null;
			}
			errorMap.notify(baseDto.getStatusCode());
		} else if (action.equalsIgnoreCase("EDIT")) {
			// Update function also same function calling.
			String resource = SERVER_URL + "/leaverequest/createEmployeeRequest";
			BaseDTO baseDto = httpService.post(resource, leaveRequest);
			if (baseDto != null && baseDto.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_UPDATED_SUCCESS.getErrorCode());
				return showEmployeeLeaveRequestListPage();
			}
			errorMap.notify(baseDto.getStatusCode());
		}
		selectedLeaveRequest = new LeaveRequest();
		log.info("<======== Ends EmployeeLeaveRequestBean.submitLeaveRequest..========>");
		return null;
	}

	public void getLeaveTypeDetails(LeaveTypeMaster leaveType) {
		log.info("<====Starts getLeaveTypeDetails called...=>" + leaveType + "\n ::" + leaveType.getEligibility());
		try {
			// leaveRequest.setLeaveEligibility(leaveType.getEligibility());
			if (leaveType.getEligibility() != null) {
				leaveRequest.setLeaveEligibility(leaveType.getEligibility());
			}
			/*
			 * String resource = SERVER_URL + "/leaverequest/leave/" +
			 * employeeMaster.getId() + "/" + leaveRequest.getLeaveType().getId();
			 */
			if (!action.isEmpty() && action.equalsIgnoreCase("EDIT")) {
				leaveRequest.setType(null);
				leaveRequest.setTimeSession(null);
			}

			EmployeeMaster employeeMaster = loginBean.getEmployee();
			leaveRequest.setEmployeeMaster(employeeMaster);

			if (leaveType.getCode().equals("MAT")) {
				if (employeeMaster != null && employeeMaster.getGender() != null
						&& employeeMaster.getGender().getCode().equals("M")) {
					AppUtil.addWarn("Maternity Leave only for Female Employees");
					leaveRequest.setLeaveType(null);
					return;
				}
			}

			if (leaveType.getCode().equalsIgnoreCase("UEL")) {
				if (employeeMaster != null && employeeMaster.getPersonalInfoEmployment() != null
						&& employeeMaster.getPersonalInfoEmployment().getDateOfJoining() != null) {
					LocalDate dateBefore = LocalDate.parse(AppUtil.REPORT_DATE_FORMAT
							.format(employeeMaster.getPersonalInfoEmployment().getDateOfJoining()));
					LocalDate dateAfter = LocalDate.parse(AppUtil.REPORT_DATE_FORMAT.format(new Date()));
					long noOfYearssBetween = ChronoUnit.YEARS.between(dateBefore, dateAfter);
					log.info("no of years between joining date------------>" + noOfYearssBetween);
					if (noOfYearssBetween < 15) {
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode());
						leaveRequest.setLeaveType(null);
						return;
					}
				}
			}

			if (!leaveType.getCode().equals("ML")) {
				BaseDTO baseDto = new BaseDTO();
				if (employeeMaster.getPersonalInfoEmployment() != null
						&& employeeMaster.getPersonalInfoEmployment().getCurrentDepartment() != null) {
					String url = AppUtil.getPortalServerURL() + "/leaveEligibility/getByDepartmentIdAndLeaveTypeId/"
							+ employeeMaster.getPersonalInfoEmployment().getCurrentDepartment().getId() + "/"
							+ leaveRequest.getLeaveType().getId();
					baseDto = httpService.get(url);
				} else {
					throw new Exception("Login User Department is not Created");
				}
				leaveRequest.setLeaveEligibility(leaveRequest.getLeaveEligibility());
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
				leaveEligibilityMaster = mapper.readValue(jsonResponse, LeaveEligibilityMaster.class);
				if (!Objects.isNull(leaveEligibilityMaster)) {
					if (!employeeMaster.getEmployeeLeaveDeatailsList().isEmpty()) {
						leaveConsumed = employeeMaster.getEmployeeLeaveDeatailsList().stream()
								.filter(i -> i.getNoOfDays() != null && i.getEmployeeMaster() != null
										&& i.getLeaveTypeMaster() != null
										&& i.getLeaveTypeMaster().getId().longValue() == leaveRequest.getLeaveType()
												.getId()
										&& i.getEmployeeMaster().getId().longValue() == leaveRequest.getEmployeeMaster()
												.getId())
								.collect(Collectors.summingDouble(o -> o.getNoOfDays().doubleValue()));
						leaveRequest.setLeaveConsumed(leaveConsumed);
					} else {
						leaveRequest.setLeaveConsumed(0.0);
					}
					if (leaveEligibilityMaster.getNumberOfDays() != null && leaveRequest.getLeaveConsumed() != null) {
						leaveBalance = (leaveEligibilityMaster.getNumberOfDays().doubleValue() - leaveConsumed);
						leaveRequest.setLeaveBalance(leaveBalance);
					} else {
						leaveRequest.setLeaveBalance(0.0);
					}
					Double d = new Double(leaveEligibilityMaster.getNumberOfDays() == null ? 0.0
							: leaveEligibilityMaster.getNumberOfDays());
					Integer numberOfDays = d.intValue();

					leaveRequest.setLeaveBalance(numberOfDays - leaveRequest.getLeaveConsumed());
					// leaveRequest.setLeaveEligibility(numberOfDays);
					leaveRequest.setLeaveEligibility(d);
					log.info(" Leave consumed is :" + leaveRequest.getLeaveConsumed());
					log.info(" Leave balance is :" + leaveRequest.getLeaveBalance());
					RequestContext.getCurrentInstance().update("employeeLeaveRequestBean:leaveRequestPanel");
				} else {
					leaveEligibilityMaster = new LeaveEligibilityMaster();
					leaveRequest.setLeaveConsumed(0.0);
					leaveRequest.setLeaveBalance(0.0);
					RequestContext.getCurrentInstance().update("employeeLeaveRequestBean:leaveRequestPanel");
				}
			}
			if (leaveRequest.getLeaveType().getId() != null) {
				String resource = AppUtil.getPortalServerURL() + "/leaverequest/leave/"
						+ leaveRequest.getEmployeeMaster().getId() + "/" + leaveRequest.getLeaveType().getId();
				log.info("onLeaveTypeChange url==>" + resource);
				BaseDTO baseDTO = httpService.get(resource);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						LeaveRequest leaveDetail = mapper.readValue(jsonResponse, LeaveRequest.class);
						if (leaveDetail.getLeaveConsumedDouble() != null) {
							leaveRequest.setLeaveConsumedDouble(leaveDetail.getLeaveConsumedDouble());
						} else {
							leaveRequest.setLeaveConsumedDouble(0.0);
						}
						if (leaveDetail.getLeaveBalanceDouble() != null) {
							leaveRequest.setLeaveBalanceDouble(leaveDetail.getLeaveBalanceDouble());
						} else {
							leaveRequest.setLeaveBalanceDouble(0.0);
						}
						leaveRequest.setLeaveEligibleDouble(leaveDetail.getLeaveEligibleDouble());
						if (leaveDetail.getLeaveEligibleDouble() != null) {
							leaveEligibilityMaster.setNumberOfDays(leaveDetail.getLeaveEligibleDouble());
							if (leaveRequest.getLeaveType().getCode().equalsIgnoreCase("EL")
									|| leaveRequest.getLeaveType().getCode().equalsIgnoreCase("ML")) {
								leaveRequest.setLeaveEligibility(leaveRequest.getLeaveEligibleDouble());
								leaveRequest.setLeaveConsumed(leaveRequest.getLeaveConsumedDouble());
								leaveRequest.setLeaveBalance(leaveRequest.getLeaveBalanceDouble());
							}
						}
					} else if (baseDTO.getStatusCode().equals(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode())) {
						errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getCode());
						leaveRequest.setLeaveType(null);
					} else {
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
						leaveRequest.setLeaveType(null);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in upateLeaveTypeDetails ", e);
		}
		log.info("<====Ends getLeaveTypeDetails called...=>");
	}

	public void updateNoOfDays() {
		log.info("from Date :::" + leaveRequest.getLeaveFromDate() + " todate ::" + leaveRequest.getLeaveToDate());
		try {
			if (leaveRequest.getLeaveFromDate() == null) {
				AppUtil.addWarn("Please choose the from date");
				return;
			}

			if (leaveRequest.getLeaveFromDate().compareTo(leaveRequest.getLeaveToDate()) == 0) {
				leaveRequest.setNoOfDaysRequested(1.0);
			}
			if (leaveRequest.getLeaveFromDate() != null
					&& (leaveRequest.getLeaveFromDate() != leaveRequest.getLeaveToDate())) {
				if (leaveRequest.getType() != null) {
					leaveRequest.setType(null);
				}
				leaveRequest.setTimeSession(null);
			}
			log.info(" leaveRequest is : " + leaveRequest.getTimeSession());
			/*
			 * if (leaveRequest.getLeaveFromDate() != null && leaveRequest.getLeaveToDate()
			 * != null) { Integer days =
			 * Util.noOfDaysBetweenTwoDates(leaveRequest.getLeaveFromDate(),
			 * leaveRequest.getLeaveToDate()); log.info("Days  ::::" + days);
			 * leaveRequest.setNoOfDaysRequested(days); }
			 */
			if (leaveRequest.getLeaveToDate() != null && leaveRequest.getLeaveFromDate() != null) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String fromDate = dateFormat.format(leaveRequest.getLeaveFromDate());
				String toDate = dateFormat.format(leaveRequest.getLeaveToDate());

				log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

				Date firstDate = dateFormat.parse(fromDate);
				Date secondDate = dateFormat.parse(toDate);

				long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

				log.info("diff------------->" + diff);

				if (secondDate.before(firstDate)) {
					Util.addWarn("To date should not be allowed for before from date");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}
				if (firstDate.compareTo(secondDate) == 0) {
					log.info("Number of days for leave is : " + 1);
					leaveRequest.setNoOfDaysRequested(1.0);
				} else {
					log.info("Number of days for leave is : " + diff);
					// int leaveDays=AppUtil.calculateWorkDays(firstDate, secondDate);
					double leaveDays = AppUtil.calculateNoOfDays(firstDate, secondDate);
					leaveRequest.setNoOfDaysRequested(leaveDays);
				}

				log.info("leavebalance------------>" + leaveRequest.getLeaveBalanceDouble());
				if (Objects.isNull(leaveRequest.getLeaveBalanceDouble())) {
					log.info(" Leave balance is null ");
					errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
					leaveRequest.setLeaveFromDate(null);
					leaveRequest.setLeaveToDate(null);
					return;
				} else if (leaveRequest.getLeaveBalanceDouble() == null
						|| diff >= leaveRequest.getLeaveBalanceDouble()) {
					log.info(" Not eligible to take leave ");
					AppUtil.addWarn("Leave apply days not greater than " + leaveRequest.getLeaveBalanceDouble());
					leaveRequest.setNoOfDaysRequested(0.0);
					leaveRequest.setLeaveFromDate(null);
					leaveRequest.setLeaveToDate(null);
					return;
				}

				if (leaveRequest.getLeaveType() != null) {
					if (leaveRequest.getLeaveType().getCode().equals("MAT")) {
						Double eligibleDays = Double.valueOf(commonDataService.getAppKeyValue("MATERNITY_LEAVE_DAYS"));
						eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
						if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
							AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
							leaveRequest.setLeaveToDate(null);
							leaveRequest.setNoOfDaysRequested(null);
						}
					}

					if (leaveRequest.getLeaveType().getCode().equals("ML")) {
						Double eligibleDays = Double.valueOf(MedicalLeaveConfig.TOTAL_LEAVE);
						eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
						if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
							AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
							leaveRequest.setLeaveToDate(null);
							leaveRequest.setNoOfDaysRequested(null);
							return;
						}
						if (leaveRequest.getLeaveConsumedDouble() != null) {
							if (leaveRequest.getLeaveConsumedDouble() >= eligibleDays) {
								errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
								leaveRequest.setNoOfDaysRequested(null);
								leaveRequest.setLeaveToDate(null);
								return;
							}

							Double consumedLeave = leaveRequest.getLeaveConsumedDouble() + diff;
							if (consumedLeave >= eligibleDays) {
								AppUtil.addWarn("Your total leave eligiblity is " + eligibleDays);
								leaveRequest.setLeaveToDate(null);
								leaveRequest.setNoOfDaysRequested(null);
								return;
							}
						}
					}
				}

			}
			log.info(" leaveRequest time session : " + leaveRequest.getTimeSession());
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

	}

	public void updateNoOfDaysChangeFromDate() {
		log.info("from Date :::" + leaveRequest.getLeaveFromDate());
		try {

			if (leaveRequest.getLeaveToDate() != null) {
				if ((leaveRequest.getLeaveFromDate() != null)
						&& leaveRequest.getLeaveFromDate() != leaveRequest.getLeaveToDate()) {
					if (leaveRequest.getType() != null) {
						leaveRequest.setType(null);
					}
					leaveRequest.setTimeSession(null);
				}

				SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
				String fromDate = dateFormat.format(leaveRequest.getLeaveFromDate());
				String toDate = dateFormat.format(leaveRequest.getLeaveToDate());

				log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

				Date firstDate = dateFormat.parse(fromDate);
				Date secondDate = dateFormat.parse(toDate);

				long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
				long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

				log.info("diff------------->" + diff);

				if (secondDate.before(firstDate)) {
					Util.addWarn("To date should not be allowed for before from date");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}
				if (firstDate.compareTo(secondDate) == 0) {
					log.info("Number of days for leave is : " + 1);
					leaveRequest.setNoOfDaysRequested(1.0);
				} else {
					log.info("Number of days for leave is : " + diff);
					double leaveDays = AppUtil.calculateNoOfDays(firstDate, secondDate);
					leaveRequest.setNoOfDaysRequested(leaveDays);
				}
				if (leaveRequest.getLeaveType() != null) {
					if (leaveRequest.getLeaveType().getCode().equals("MAT")) {
						Double eligibleDays = Double.valueOf(commonDataService.getAppKeyValue("MATERNITY_LEAVE_DAYS"));
						eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
						if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
							AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
							leaveRequest.setLeaveFromDate(null);
							leaveRequest.setLeaveToDate(null);
							leaveRequest.setNoOfDaysRequested(null);
						}
					}
					if (leaveRequest.getLeaveType().getCode().equals("ML")) {
						if (leaveRequest.getLeaveToDate() != null) {
							Double eligibleDays = Double.valueOf(MedicalLeaveConfig.TOTAL_LEAVE);
							eligibleDays = eligibleDays == null ? 0.0 : eligibleDays;
							if (leaveRequest.getNoOfDaysRequested() > eligibleDays) {
								AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
								leaveRequest.setLeaveFromDate(null);
								leaveRequest.setLeaveToDate(null);
								leaveRequest.setNoOfDaysRequested(null);
								return;
							}
							if (leaveRequest.getLeaveConsumedDouble() >= eligibleDays) {
								errorMap.notify(ErrorDescription.LEAVE_REQUEST_NOT_ELIGIBLE.getErrorCode());
								leaveRequest.setNoOfDaysRequested(null);
								leaveRequest.setLeaveFromDate(null);
								leaveRequest.setLeaveToDate(null);
								leaveRequest.setNoOfDaysRequested(null);
								return;
							}
							if (leaveRequest.getLeaveConsumedDouble() != null) {
								Double consumedLeave = leaveRequest.getLeaveConsumedDouble() + diff;
								if (consumedLeave >= eligibleDays) {
									AppUtil.addWarn("Leave apply days not greater than " + eligibleDays);
									leaveRequest.setLeaveFromDate(null);
									leaveRequest.setLeaveToDate(null);
									leaveRequest.setNoOfDaysRequested(null);
									return;
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

	}

	public void updateNoOfDaysSelectLeavetype() {
		log.info("from Date :::" + leaveRequest.getLeaveFromDate() + " todate ::" + leaveRequest.getLeaveToDate());
		try {
			if (leaveRequest.getLeaveFromDate() != null && leaveRequest.getLeaveToDate() != null) {
				if (leaveRequest.getType() != null && !leaveRequest.getType().isEmpty()
						&& leaveRequest.getType().equals("HALF_DAY")) {
					log.info("Number of days for leave is : " + 0.5);
					leaveRequest.setNoOfDaysRequested(0.5);
				}
				if (leaveRequest.getType() != null && !leaveRequest.getType().isEmpty()
						&& leaveRequest.getType().equals("FULL_DAY")) {
					log.info("Number of days for leave is : " + 1.0);
					leaveRequest.setNoOfDaysRequested(1.0);
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void updateToDate() {
		focusProperty = "todateId";
		if (leaveRequest.getLeaveFromDate() != null && leaveRequest.getLeaveToDate() != null
				&& (leaveRequest.getLeaveFromDate().after(leaveRequest.getLeaveToDate()))) {
			leaveRequest.setLeaveToDate(null);
		}
	}

	/*
	 * public void uploadDocument(FileUploadEvent event) {
	 * log.info("Upload button is pressed..");
	 * 
	 * try {
	 * 
	 * InputStream inputStream = null; OutputStream outputStream = null; if (event
	 * == null || event.getFile() == null) { log.error(" Upload document is null ");
	 * errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode()); return; }
	 * leaveDocument = event.getFile(); long size = leaveDocument.getSize();
	 * log.info("Leave uploaded document size is : " + size); size = size / 1000; if
	 * (size > employeePhotoSize) { leaveRequest.setDocumentPath(null);
	 * FacesContext.getCurrentInstance().getExternalContext().getFlash().
	 * setKeepMessages(true);
	 * errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode()); return;
	 * } String uploadPath =
	 * commonDataService.getAppKeyValue("LEAVE_REQUEST_UPLOAD_PATH"); File file =
	 * new File(uploadPath); uploadPath = file.getAbsolutePath() + "/"; Path path =
	 * Paths.get(uploadPath); if (Files.notExists(path)) {
	 * log.info(" Path Doesn't exixts"); Files.createDirectories(path); }
	 * log.info(" Uploaded document oath is :" + uploadPath); leaveDocumentFileName
	 * = leaveDocument.getFileName(); leaveRequest.setDocumentPath(uploadPath +
	 * leaveDocumentFileName); //
	 * errorMap.notify(ErrorDescription.DOCUMENT_UPLOADED.getErrorCode());
	 * inputStream = leaveDocument.getInputstream(); outputStream = new
	 * FileOutputStream(uploadPath + leaveDocumentFileName); byte[] buffer = new
	 * byte[(int) leaveDocument.getSize()]; int bytesRead = 0; while ((bytesRead =
	 * inputStream.read(buffer)) != -1) { outputStream.write(buffer, 0, bytesRead);
	 * } log.info(" upload Path is >>>" + uploadPath);
	 * log.info(" Leave Document is uploaded successfully"); if (outputStream !=
	 * null) { outputStream.close(); } if (inputStream != null) {
	 * inputStream.close(); } } catch (Exception exp) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * log.error("found exception in file upload", exp); } }
	 */

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			leaveDocument = event.getFile();
			String type = FilenameUtils.getExtension(leaveDocument.getFileName());
			log.info(" File Type :: " + type);
			boolean validFileType = AppUtil.isValidFileType(type, new String[] { "png", "jpg", "JPG", "doc", "jpeg",
					"gif", "docx", "pdf", "xlsx", "xls", "txt", "PNG" });
			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				// errorMap.notify(ErrorDescription.getError(AdminErrorCode.TAPAL_DOC_UPLOAD_TYPE_ERROR).getErrorCode());
				Util.addWarn("Upload only jpg,jpeg,png,pdf,doc,txt,gif,xls,xlsx and docx files");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}

			long size = leaveDocument.getSize();
			log.info("Leave uploaded document size is : " + size);
			/*
			 * size = size / 1000; if (size > employeePhotoSize) {
			 * leaveRequest.setDocumentPath(null);
			 * FacesContext.getCurrentInstance().getExternalContext().getFlash().
			 * setKeepMessages(true);
			 * errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode()); return
			 * ; }
			 */
			size = (size / AppUtil.ONE_MB_IN_KB);
			log.info("file size---------------" + size);
			if (size > 50000) {
				Util.addWarn("Upload File Size Within 50 MB");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("LEAVE_REQUEST_UPLOAD_PATH");
			String filePath = Util.fileUpload(uploadPath, leaveDocument);

			log.info(" Uploaded document oath is :" + uploadPath);
			leaveDocumentFileName = leaveDocument.getFileName();
			leaveRequest.setDocumentPath(filePath);
			errorMap.notify(ErrorDescription.LEAVE_REQUEST_DOCUMENT_UPLOADED.getErrorCode());

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void loadLazyLeaveRequestList() {

		log.info("< --  Start Employee Lazy Load -- > ");
		employeeLeaveRequests = new LazyDataModel<LeaveRequest>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<LeaveRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<LeaveRequest> data = mapper.readValue(jsonResponse, new TypeReference<List<LeaveRequest>>() {
					});
					if (data == null) {
						log.info(" Roster search response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Roster search Successfully Completed");
					} else {
						log.error(" Roster search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading roster data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(LeaveRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public LeaveRequest getRowData(String rowKey) {
				List<LeaveRequest> responseList = (List<LeaveRequest>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (LeaveRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedLeaveRequest = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			PaginationDTO request = new PaginationDTO();
			request.setFirst(first);
			request.setPageSize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			filters.put("employeeMaster.id", employeeMaster.getId());
			request.setFilters(filters);
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/leaverequest/lazyload", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Roster search generate data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String editLeaveRequest() {
		log.info(":: Selected leave request ::");
		try {
			if (Objects.isNull(selectedLeaveRequest.getId())) {
				log.info(" Selected Leave request id is null : " + selectedLeaveRequest.getId());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_ID_ISNULL.getErrorCode());
				return null;
			}
			getSystemNotificationId();

			String resource = SERVER_URL + "/leaverequest/getbyid/" + selectedLeaveRequest.getId() + "/"
					+ systemNotificationId;
			log.info(":: Selected leave request URI is ::" + resource);
			BaseDTO baseDTO = httpService.get(resource);
			if (Objects.isNull(baseDTO)) {
				log.info(":: Selected Leave request BaseDTO response is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				log.info(":: Selected Leave request success response is ::" + baseDTO.getStatusCode());
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				leaveRequest = mapper.readValue(jsonResponse, new TypeReference<LeaveRequest>() {
				});
				if (leaveRequest != null) {
					if (leaveRequest.getTimeSession() != null) {
						leaveRequest.setTimeSession(leaveRequest.getTimeSession());
					}
				}
			}
			if (action.equalsIgnoreCase("EDIT"))
				return EMP_LEAVE_REQ_EDIT_PAGE;
			else if (action.equalsIgnoreCase("VIEW"))
				return EMP_LEAVE_REQ_VIEW_PAGE;
			/*
			 * else return EMP_LEAVE_REQ_CANCEL_PAGE;
			 */
		} catch (Exception e) {
			log.error(":: Exception while getting selected leave request ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose:each time row selected in list page this method will be called to
	 * enable,disable buttons in list page
	 * 
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedLeaveRequest = ((LeaveRequest) event.getObject());

		addButtonFlag = true;
		viewButtonFlag = true;
		cancelLeaveButtonFlag = true;
		editButtonFlag = false;
		deleteFlag = false;
		if (selectedLeaveRequest.getStatus() == LeaveRequest.LeaveRequestStatus.APPROVED) {
			approveButtonFlag = false;
			generateFlag = true;
		} else if (selectedLeaveRequest.getStatus() == LeaveRequest.LeaveRequestStatus.REJECTED) {
			approveButtonFlag = false;
			generateFlag = false;
			editButtonFlag = true;
			deleteFlag = true;
			cancelLeaveButtonFlag = false;
		} else if (selectedLeaveRequest.getStatus() == LeaveRequest.LeaveRequestStatus.INPROGRESS) {
			generateFlag = false;
			approveButtonFlag = true;
			editButtonFlag = false;
		} else if (selectedLeaveRequest.getStatus() == LeaveRequest.LeaveRequestStatus.LEAVE_CANCEL) {
			generateFlag = false;
			approveButtonFlag = false;
			cancelLeaveButtonFlag = false;
		} else if (selectedLeaveRequest.getStatus() == LeaveRequest.LeaveRequestStatus.FINAL_APPROVED) {
			approveButtonFlag = false;
			cancelLeaveButtonFlag = false;
		}

		if ((!selectedLeaveRequest.getStatus().equals(LeaveRequest.LeaveRequestStatus.LEAVE_CANCEL)
				&& !selectedLeaveRequest.getStatus().equals(LeaveRequest.LeaveRequestStatus.REJECTED))
				&& !selectedLeaveRequest.getLeaveFromDate().before(new Date())) {
			cancelLeaveButtonFlag = true;
		}
	}

	public String cancel() {
		log.info("Cancel........");
		selectedLeaveRequest = new LeaveRequest();
		cancelLeaveButtonFlag = false;
		leaveEligibilityMaster.setNumberOfDays(null);
		return showEmployeeLeaveRequestListPage();
	}

	public String deleteLeaveRequest() {
		try {
			if (selectedLeaveRequest == null) {
				AppUtil.addWarn("Please select one leave request");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmLeaveDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting .... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose:will delete the selected record if confirmed in dialog and redirect
	 * to list page
	 * 
	 * @return
	 */
	public String confirmDeleteLeave() {
		log.info("... delete Leave Request called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.delete(SERVER_URL + "/leaverequest/deletebyid/" + selectedLeaveRequest.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_DELETED_SUCCESS.getErrorCode());
			} else {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return showEmployeeLeaveRequestListPage();
	}

	public String cancelRequestLeave() {
		log.info("<===============Starts LeaveRequestBean cancelRequestLeave====================>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			if (selectedLeaveRequest.getLeaveToDate().after(new Date())
					|| selectedLeaveRequest.getLeaveToDate().equals(new Date())) {
				String url = AppUtil.getPortalServerURL() + "/leaverequest/employeeCancelLeaveRequest";
				baseDTO = httpService.post(url, leaveRequest);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("<========== Leave Request Cancel Successfully ==========>");
					errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_CANCEL_SUCCESSFULLY)
							.getErrorCode());
				}
			} else {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.LEAVE_REQUEST_DATE_ERROR).getErrorCode());
			}
			addButtonFlag = false;
			approveButtonFlag = false;
			editButtonFlag = false;
			viewButtonFlag = false;
			deleteFlag = false;
			selectedLeaveRequest = new LeaveRequest();
			loadLazyLeaveRequestList();
		} catch (Exception ex) {
			log.error("error in cancelRequestLeave", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===============End LeaveRequestBean cancelRequestLeave====================>");
		return "/pages/personnelHR/employeeProfile/listEmployeeLeaveRequest.xhtml?faces-redirect=true;";

	}

	public void getSystemNotificationId() {
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {

			systemNotificationId = Long.parseLong(notificationId);

		}
	}

	private boolean checkAlreadyLoanPreclosured() {

		boolean isSurrenderLeave = false;
		try {
			String getUrl = AppUtil.getPortalServerURL() + "/leaverequest/checksurrenderLeaveavilable";
			log.info("submitLoanPreclosure URL : " + getUrl);
			BaseDTO baseDTO = httpService.post(getUrl, leaveRequestDTO);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				isSurrenderLeave = mapper.readValue(jsonValue, Boolean.class);
			}

		} catch (Exception e) {
			log.info("Exception occured at checkAlreadyLoanPreclosured() ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return isSurrenderLeave;
	}

	public String showConfirmLeaveDeleteDialog() {
		log.info("showConfirmLeaveDeleteDialog Method Start");
		try {
			editLeaveRequest();
			RequestContext.getCurrentInstance().execute("PF('confirmLeaveRequestDelete').show();");
			return null;
		} catch (Exception ex) {
			log.info("Exception", ex);
		}
		log.info("showConfirmLeaveDeleteDialog Method Start");
		return null;
	}

}