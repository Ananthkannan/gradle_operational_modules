package in.gov.cooptex.masters.ui.bean;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.TenderCategory;
import in.gov.cooptex.core.model.TenderType;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("tenderCategoryBean")
@Scope("session")
public class TenderCategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String CREATE_PAGE = "/pages/masters/tendercategory/createTenderCategory.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/tendercategory/listTenderCategory.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/tendercategory/viewTenderCategory.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_URL = "/tenderCategory/getallTenderCategory";
	private final String CREATE_URL = "/tenderCategory/create";
	private final String DELETE_URL = "/tenderCategory";

	private String serverURL = null;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	ObjectMapper mapper;

	String jsonResponse;
	
	@Getter
	@Setter
	LazyDataModel<TenderCategory> lazyTenderCategoryList;
	

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	TenderCategory tenderCategory;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<TenderCategory> tenderCategoryList = new ArrayList<TenderCategory>();

	@Getter
	@Setter
	private int tenderCategoryistSize;

	boolean forceLogin = false;

	@PostConstruct
	public void init() {
		log.info(" TenderCategoryBean init --->" + serverURL);
		loadValues();
		addTenderCategory();
		addButtonFlag = true;
		loadLazytenderCategoryList();
		mapper=new ObjectMapper();
		getTenderCategoryView(tenderCategory);
	}

	public String showList() {
		log.info("<---------- Loading TenderCategoryBean showList list page---------->");
		loadValues();
		loadLazytenderCategoryList();
		return LIST_PAGE;
	}

	public String addTenderCategory() {
		log.info("<---- Start TenderCategoryBean.addTenderCategory ---->");
		try {
			action = "ADD";
			tenderCategory = new TenderCategory();
			tenderCategory.setActiveStatus(true);

			log.info("<---- End TenderCategory.addTenderCategory ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	private void getAllTenderCategoryList() {
		log.info("<<<< ----------Start tenderCategoryBean-getAllTenderCategoryList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.get(url);
			tenderCategory = new TenderCategory();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<TenderCategory>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		// if (tenderCategoryList != null) {
		// tenderCategoryListSize = tenderCategoryList.size();
		// } else {
		// tenderCategoryListSize = 0;
		// }
		log.info("<<<< ----------End TenderCategoryBean . getAllTenderCategoryList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			tenderCategory = new TenderCategory();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to TenderCategory List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... TenderCategoryBean submit begin .... " + action + " " + tenderCategory);
		try {
addButtonFlag=true;
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + CREATE_URL, tenderCategory);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("TenderCategoryBean saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.TENDER_CATEGORY_CREATED_SUCCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.TENDER_CATEGORY_UPDATED_SUCCESSFULLY.getCode());
					showList();
					// errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating TenderCategory" + e);
		}
		log.info("...... TenderCategoryBean submit ended ....");
		return LIST_PAGE;
	}

	public String editTenderCategory() {
		log.info("<<<<< -------Start editTenderCategory called ------->>>>>> ");
		action = "EDIT";

		if (tenderCategory == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}

		if (tenderCategory.getActiveStatus().equals("ACTIVE")) {
			tenderCategory.setActiveStatus(tenderCategory.getActiveStatus());
		} else {
			tenderCategory.setActiveStatus(tenderCategory.getActiveStatus());
		}
		log.info("<<<<< -------End editTenderCategory called ------->>>>>> ");
		getRecordById();
		return CREATE_PAGE;

	}

	public String deleteTenderCategory() {
		try {
			action = "Delete";
			if (tenderCategory == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteTenderCategoryConfirm() {

		try {
			log.info("Delete confirmed for tenderategory [" + tenderCategory.getName() + "]" + tenderCategory.getId());
			TenderCategory tenderCategoryDelete = new TenderCategory();
			tenderCategoryDelete.setId(tenderCategory.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + DELETE_URL;
			baseDTO = httpService.delete(url + "/delete/" + tenderCategory.getId());

			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("TenderCategory deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					tenderCategory = new TenderCategory();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					// errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.TENDER_CATEGORY_DELETED_SUCCESSFULLY.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start tenderCategoryBean . clear ----->");
		loadLazytenderCategoryList();
		tenderCategory = new TenderCategory();
		addButtonFlag=true;
		log.info("<---- End TenderCategoryBean . clear ----->");

		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewTenderCategory() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (tenderCategory == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		getRecordById();
		return VIEW_PAGE;
	}

	private void getTenderCategoryView(TenderCategory tenderCategory) {
		log.info("<<<< ----------Start tenderCategoryBean . getTenderCategoryiew ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.post(url, tenderCategory);
			tenderCategory = new TenderCategory();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tenderCategory = mapper.readValue(jsonResponse, new TypeReference<TenderCategory>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tenderCategory != null) {
			log.info("tendertategory getTenderCategoryView" + tenderCategory);
		} else {
			log.info("tendercategory getTenderCategoryView" + tenderCategory);
		}

		log.info("<<<< ----------End TenderCategoryBean . getTenderCategoryView ------- >>>>");
	}
	
	
	public void loadLazytenderCategoryList() {
		log.info("<===== Starts petitiontypeBean.loadLazytenderCategoryList ======>");

		lazyTenderCategoryList = new LazyDataModel<TenderCategory>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<TenderCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL 
							+ "/tenderCategory/getalltendercategorylistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						tenderCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<TenderCategory>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("tenderCategoryBean size---------------->" + lazyTenderCategoryList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tenderCategoryList;
			}

			@Override
			public Object getRowKey(TenderCategory res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TenderCategory getRowData(String rowKey) {
				for (TenderCategory tenderCategorys : tenderCategoryList) {
					if (tenderCategorys.getId().equals(Long.valueOf(rowKey))) {
						tenderCategory = tenderCategorys;
						return tenderCategory;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  tenderCategoryBean.loadLazytenderCategoryList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TenderCategoryBean.onRowSelect ========>" + event);
		tenderCategory = ((TenderCategory) event.getObject());
		addButtonFlag = false;
		log.info("<===Ends TenderCategoryBean.onRowSelect ========>" + tenderCategory.getId());
	}
	
	public void getRecordById() {
		String URL = serverURL + "/tenderCategory/" + tenderCategory.getId();
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				tenderCategory = mapper.readValue(jsonValue, TenderCategory.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}


}