package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.hrms.model.ProfessionalTaxConfig;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("professionalTaxConfigBean")
public class ProfessionalTaxConfigBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/masters/taxConfig/createTaxConfig.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/masters/taxConfig/viewTaxConfig.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/masters/taxConfig/listTaxConfig.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private List<StateMaster> listStateMaster;

	@Getter
	@Setter
	StateMaster stateMaster = new StateMaster();

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	Boolean isAddButton = true, isDeleteButton = true, isEditButton = true, isViewButton = true;

	@Getter
	@Setter
	List<FinancialYear> financialYearList = new ArrayList<>();

	@Getter
	@Setter
	FinancialYear financialYear = new FinancialYear();

	@Getter
	@Setter
	ProfessionalTaxConfig professionalTaxConfig = new ProfessionalTaxConfig();

	@Getter
	@Setter
	Double grossSalaryFrom = 0.0, grossSalaryTo = 0.0, totalTax = 0.0;

	@Getter
	@Setter
	Boolean status;

	@Getter
	@Setter
	LazyDataModel<ProfessionalTaxConfig> professionalTaxConfigLazyList;

	@Getter
	@Setter
	List<ProfessionalTaxConfig> professionalTaxConfigList = new ArrayList<>();

	@Getter
	@Setter
	ProfessionalTaxConfig selectedProfessionalTaxConfig = new ProfessionalTaxConfig();

	@Getter
	@Setter
	Integer totalRecords = 0;

	@PostConstruct
	public void init() {
		log.info("ProfessionalTaxConfigBean Init is executed.....................");
		mapper = new ObjectMapper();
		selectedProfessionalTaxConfig = new ProfessionalTaxConfig();
		loadTaxInformation();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadTaxInformation() {
		log.info("ProfessionalTaxConfigBean :: loadTaxInformation is started.....................");
		try {
			loadStateMaster();
			loadFinancialYear();
		} catch (Exception exception) {
			log.error("loadTaxInformation :: exception => ", exception);
		}
	}

	public void loadFinancialYear() {

		log.info("<===== Start ProfessionalTaxConfigBean.loadFinancialYear ======>");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + "/reserveCreations/getFinanceYear";
			log.info("loadFinancialYear :: url==> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				financialYearList = mapper.readValue(jsonResponse, new TypeReference<List<FinancialYear>>() {
				});
				if (financialYearList != null && !financialYearList.isEmpty()) {
					log.info("financialYearList size==> " + financialYearList.size());
				} else {
					log.error("financialYearList is null");
				}
			}
		} catch (Exception e) {
			log.error("Load loadFinancialYear : exception==> ", e);
		}

	}

	public void loadStateMaster() {
		log.info("ProfessionalTaxConfigBean :: loadStateMaster is started.....................");
		try {
			String url = SERVER_URL + "/stateMaster/getAllStates";
			log.info("ProfessionalTaxConfigBean :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listStateMaster = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
			});
			if (listStateMaster != null && !listStateMaster.isEmpty()) {
				log.info("listStateMaster size=> " + listStateMaster.size());
			}

		} catch (Exception exception) {
			log.error("ProfessionalTaxConfigBean :: loadTaxInformation :: exception =>  " + exception);
		}
	}

	public String loadTaxPage() {
		log.info("ProfessionalTaxConfigBean :: loadTaxPage is started.....................");
		try {
			if (pageAction.equals("LIST")) {
				loadLazyTaxConfigList();
				selectedProfessionalTaxConfig = new ProfessionalTaxConfig();
				return LIST_PAGE;
			} else if (pageAction.equals("ADD")) {
				return ADD_PAGE;
			} else if (pageAction.equals("EDIT")) {
				
				if(selectedProfessionalTaxConfig==null)
				{
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
					return null;
				}
				String url = SERVER_URL + "/professionaltax/getById/" + selectedProfessionalTaxConfig.getId();
				log.info("=======Url is=====" + url);
				BaseDTO baseDto = httpService.get(url);
				if (baseDto != null && baseDto.getStatusCode() == 0) {
					ObjectMapper objectMapper = new ObjectMapper();
					jsonResponse = objectMapper.writeValueAsString(baseDto.getResponseContent());
					professionalTaxConfig = objectMapper.readValue(jsonResponse,
							new TypeReference<ProfessionalTaxConfig>() {
							});
					stateMaster = professionalTaxConfig.getStateMaster();
					grossSalaryFrom = professionalTaxConfig.getGrossSalaryFrom();
					grossSalaryTo = professionalTaxConfig.getGrossSalaryTo();
					totalTax = professionalTaxConfig.getTaxRate();
					financialYear=professionalTaxConfig.getFinancialYear();
					loadFinancialYearById(professionalTaxConfig.getFinancialYear().getId());
					status = professionalTaxConfig.getActiveStatus();
					return ADD_PAGE;
				} else {
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}

			} else if (pageAction.equals("VIEW")) {
				if(selectedProfessionalTaxConfig==null)
				{
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
					return null;
				}
				String url = SERVER_URL + "/professionaltax/getById/" + selectedProfessionalTaxConfig.getId();
				log.info("=======Url is=====" + url);
				BaseDTO baseDto = httpService.get(url);
				if (baseDto != null && baseDto.getStatusCode() == 0) {
					ObjectMapper objectMapper = new ObjectMapper();
					jsonResponse = objectMapper.writeValueAsString(baseDto.getResponseContent());
					professionalTaxConfig = objectMapper.readValue(jsonResponse,
							new TypeReference<ProfessionalTaxConfig>() {
							});
					stateMaster = professionalTaxConfig.getStateMaster();
					grossSalaryFrom = professionalTaxConfig.getGrossSalaryFrom();
					grossSalaryTo = professionalTaxConfig.getGrossSalaryTo();
					totalTax = professionalTaxConfig.getTaxRate();
					status = professionalTaxConfig.getActiveStatus();
					financialYear=professionalTaxConfig.getFinancialYear();
					return VIEW_PAGE;
				} else {
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			} else if (pageAction.equals("DELETE")) {
				if(selectedProfessionalTaxConfig==null)
				{
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProfessionalTaxDelete').show();");
				
			}
		} catch (Exception e) {
			log.info("=====Exception Occured in loadTaxPage =====");
			log.info("=====Exception is =====" + e);
		}

		return null;
	}
	
	public void loadFinancialYearById(Long financialId) {

		log.info("<===== Start ProfessionalTaxConfigBean.loadFinancialYearById ======>");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + "/reserveCreations/getFinanceYearById/"+financialId;
			log.info("loadFinancialYearById :: url==> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				financialYear = mapper.readValue(jsonResponse, new TypeReference<FinancialYear>() {
				});
				if (financialYear != null ) {
					log.info("financialYearList size==> " + financialYear.getId());
				} else {
					log.error("financialYearList is null");
				}
			}
		} catch (Exception e) {
			log.error("Load loadFinancialYear : exception==> ", e);
		}

	}

	public String cancelTax() {
		stateMaster = new StateMaster();
		financialYear = new FinancialYear();
		grossSalaryFrom = 0d;
		selectedProfessionalTaxConfig = new ProfessionalTaxConfig();
		grossSalaryTo = 0d;
		totalTax = 0d;
		isAddButton = true;
		return LIST_PAGE;
	}

	public String saveTax() {
		log.info("ProfessionalTaxConfigBean :: saveTax is started.....................");
		try {
			if (stateMaster == null) {
				AppUtil.addError("State master not found");
				return null;
			}
			if (financialYear == null) {
				AppUtil.addError("Financial year not found");
				return null;
			}
			if (grossSalaryFrom == null || grossSalaryFrom == 0.0) {
				AppUtil.addError("Taxable start value not found");
				return null;
			}
			if (grossSalaryTo == null || grossSalaryTo == 0.0) {
				AppUtil.addError("Taxable end value not found");
				return null;
			}
			if (totalTax == null || totalTax == 0.0) {
				AppUtil.addError("Total tax not found");
				return null;
			}
			if (grossSalaryTo < grossSalaryFrom) {
				AppUtil.addError("Taxable start value should less than taxable end value");
				return null;
			}
			professionalTaxConfig.setStateMaster(stateMaster);
			professionalTaxConfig.setFinancialYear(financialYear);
			professionalTaxConfig.setGrossSalaryFrom(grossSalaryFrom);
			professionalTaxConfig.setGrossSalaryTo(grossSalaryTo);
			professionalTaxConfig.setTaxRate(totalTax);
			professionalTaxConfig.setActiveStatus(status);

			String url = SERVER_URL + "/professionaltax/createprofessionaltax";
			log.info("saveEmployeeIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, professionalTaxConfig);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			mapper.readValue(jsonResponse, EmpIncrement.class);
			if (baseDTO.getStatusCode() == 0) {
				cancelTax();
				loadLazyTaxConfigList();
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_INSERTED_SUCCESSFULLY).getCode());
				AppUtil.addInfo("Professional tax config inserted successfully");
				return cancelTax();
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exception) {
			log.error("ProfessionalTaxConfigBean :: saveTax :: exception =>  " + exception);
		}
		return LIST_PAGE;
	}

	public void loadLazyTaxConfigList() {
		log.info("<==== Starts IncrementBean.loadLazyIncrementList =====>");
		professionalTaxConfigLazyList = new LazyDataModel<ProfessionalTaxConfig>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ProfessionalTaxConfig> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/professionaltax/loadprofessionaltax";
					log.info("loadLazyTaxConfigList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						professionalTaxConfigList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProfessionalTaxConfig>>() {
								});
						if (professionalTaxConfigList != null && !professionalTaxConfigList.isEmpty()) {
							log.info("loadLazyTaxConfigList :: professionalTaxConfigList size==> "
									+ professionalTaxConfigList.size());
						} else {
							log.error("No tax config found");
						}
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyTaxConfigList :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return professionalTaxConfigList;
			}

			@Override
			public Object getRowKey(ProfessionalTaxConfig res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProfessionalTaxConfig getRowData(String rowKey) {
				for (ProfessionalTaxConfig professionalTaxConfig : professionalTaxConfigList) {
					if (professionalTaxConfig.getId().equals(Long.valueOf(rowKey))) {
						selectedProfessionalTaxConfig = professionalTaxConfig;
						return professionalTaxConfig;
					}
				}
				return null;
			}
		};

	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedProfessionalTaxConfig = ((ProfessionalTaxConfig) event.getObject());
		isAddButton = false;
		isDeleteButton = true;
	}
	
	public String deleteConfirm() {
		log.info("<===== Starts ProfessionalTaxConfigBean.deleteConfirm ===========>");
		try {
			String url = SERVER_URL + "/professionaltax/deleteById/" + selectedProfessionalTaxConfig.getId();
			log.info("=======Url is=====" + url);
			BaseDTO baseDto = httpService.delete(url);
			if (baseDto != null && baseDto.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.CRADEWISE_PAYHEADCONFIG_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProfessionalTaxDelete').hide();");
			} else if (baseDto != null && baseDto.getStatusCode() != 0) {
				errorMap.notify(baseDto.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Professional Tax ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ProfessionalTaxConfigBean.deleteConfirm ===========>");
		return cancelTax();
	}

}
