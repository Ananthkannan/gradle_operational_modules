package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import java.util.Iterator;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DeliveryChallanDTO;
import in.gov.cooptex.core.dto.ItemQRCodeDetailsDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.StockTransferRefDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.ShowroomType;
import in.gov.cooptex.core.enums.StockMovementTo;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.enums.WarehouseType;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.IncrementCycle;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferGroup;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.MisPdfUtil;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferScanTypeEnum;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import in.gov.cooptex.operation.stock.dto.StockOutwardAcknowledgementReportDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardPWHBean")
@Scope("session")
public class StockOutwardPWHBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST = "/pages/productWarehouse/outward/listStockOutwardPWH.xhtml?faces-redirect=true;";

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE = "/pages/productWarehouse/outward/createStockOutwardPWH.xhtml?faces-redirect=true;";

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_VIEW = "/pages/productWarehouse/outward/viewStockOutwardPWH.xhtml?faces-redirect=true;";

	private final String DELIVERY_CHALLAN_ACKNOWLEDGE = "/pages/operation/deliveryChellanAcknowledgement.xhtml?faces-redirect=true;";

	private final String STOCK_TRANSFER_INVOICE = "/pages/operation/stockTransferAcknowledgement.xhtml?faces-redirect=true;";

	private final String SOCITY_OUTWARD_ACK = "/pages/productWarehouse/outward/createSocietyOutwardAcknowledgement.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	boolean isPagePWH = true;
	
	@Getter
	@Setter
	String pageName;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnProductAdd = true;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	LazyDataModel<StockTransferRequest> stockTransferRequestLazyList;

	@Getter
	@Setter
	StockTransferRequest stockTransferRequest;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	List<Object> stockMovementToValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	List<DeliveryChallanDTO> transferItemsList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> hsnSummaryList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> productCodeSummaryList;

	@Getter
	@Setter
	List<DeliveryChallanDTO> abstractList;

	@Getter
	@Setter
	List<StockTransferRefDTO> stockTransferRefDTOList = new ArrayList<>();

	@Getter
	@Setter
	StockTransferRefDTO stockTransferRefDTO = new StockTransferRefDTO();

	@Getter
	@Setter
	List<CircleMaster> circleMasterList = new ArrayList<CircleMaster>();

	@Getter
	@Setter
	CircleMaster circleMaster = new CircleMaster();

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList = new ArrayList<SupplierMaster>();

	@Getter
	@Setter
	SupplierMaster supplierMaster = new SupplierMaster();

	@Getter
	@Setter
	List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<PurchaseInvoice>();

	@Getter
	@Setter
	PurchaseInvoice purchaseInvoice = new PurchaseInvoice();

	@Getter
	@Setter
	List<StockOutwardAcknowledgementReportDTO> stockOutwardAcknowledgementReportDTOList = new ArrayList<StockOutwardAcknowledgementReportDTO>();

	@Getter
	@Setter
	StockOutwardAcknowledgementReportDTO stockOutwardAcknowledgementReportDTO = new StockOutwardAcknowledgementReportDTO();

	@Getter
	@Setter
	String downloadPath;

	@Getter
	@Setter
	String downloadFileName = "";

	@Setter
	@Getter
	private StreamedContent pdfFile;

	MisPdfUtil misPdfUtil = new MisPdfUtil();

	@Getter
	@Setter
	List<String> totalList = new ArrayList<String>();

	@Getter
	@Setter
	String ppValue = "";

	@Getter
	@Setter
	String rpValue = "";

	@Getter
	@Setter
	private String inwordNumber;

	@Getter
	@Setter
	private Date inwardDate;

	@Getter
	@Setter
	private Double totalQty = 0D, totGstQty;

	@Getter
	@Setter
	private Double totalValue = 0.0, totalPaisa = 0.0, totTaxableValue = 0.0, totTaxablePaisa = 0.0, totIgstValue = 0.0,
			totIgstPaisa = 0.0, netTotalValue = 0.0, netTotalPaisa = 0.0;

	@Getter
	@Setter
	Double totalDispatchedQty;

	@Getter
	@Setter
	List<StockTransferRefDTO> selectStockTransferRefDTOList;

	@Getter
	@Setter
	List<EntityMaster> issrList;

	public String getListPage() {
		log.info("Going to list page");
		disableAddButton = false;
		SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		stockMovementToValues = stockTransferUtility.loadStockMovementToList(isPagePWH);
		stockTransferRequest = new StockTransferRequest();
		loadLazyList();
		selectStockTransferRefDTOList = new ArrayList<>();
		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("senderCodeOrName")) {
				request.setSenderCodeOrName(value);
			}

			if(entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}
			
			if (entry.getKey().equals("stockMovementTo")) {
				request.setStockMovementTo(value);
			}

			if (entry.getKey().equals("receiverCodeOrName")) {
				request.setReceiverCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}

			if (entry.getKey().equals("dateTransferred")) {
				request.setDateTransferred(AppUtil.serverDateFormat(value));
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/outward/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferRequestLazyList = new LazyDataModel<StockTransferRequest>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferRequest> data = new ArrayList<StockTransferRequest>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferRequest>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferRequest getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferRequest> responseList = (List<StockTransferRequest>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferRequest = ((StockTransferRequest) event.getObject());
		disableAddButton = true;
	}

	public void onAckStatusClick(StockTransferRequest obj) {
		log.info("onAckStatusClick method started");
		stockTransferRequest = obj != null ? obj : null;
		RequestContext.getCurrentInstance().execute("PF('stockAckdlg').show();");
		disableAddButton = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferRequest = new StockTransferRequest();
		disableAddButton = false;
	}

	/*********************************************************************************************************************************/

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<Object> warehouseTypeList;

	@Getter
	@Setter
	String selectedWarehouseType;

	@Getter
	@Setter
	List<EntityMaster> warehouseList;

	@Getter
	@Setter
	List<Object> showroomTypeList;

	@Getter
	@Setter
	String selectedShowroomType;

	@Getter
	@Setter
	List<EntityMaster> regionalOfficeList;

	@Getter
	@Setter
	EntityMaster selectedRegionalOffice;

	@Getter
	@Setter
	List<EntityMaster> showroomList;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean refFlag = false, stockFlag = false;

	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {

			String loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();
			
			boolean isAppConfigValue = false;
			final String APP_CONFIG_SERVER_URL = AppUtil.getPortalServerURL();
			String appValue = commonDataService.getAppKeyValueWithserverurl("SHOWROOM_TO_SHOWROOM_OUTWARD_ALLOWED_ENTITY", APP_CONFIG_SERVER_URL);
			int entityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getCode();
			if(null != appValue  && Integer.valueOf(appValue) == entityCode){
				isAppConfigValue = true;
			}else{
				isAppConfigValue = false;
			}

			if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)
					|| loginEntityCode.equals(EntityType.DISTRIBUTION_WAREHOUSE)
					|| isAppConfigValue) {
				
				if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)
						|| isAppConfigValue) {
					isPagePWH = true;
					if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)){
						pageName = "Product Warehouse";
					}else{
						pageName = "Ecommerce";
					}
				} else {
					isPagePWH = false;
					pageName = "Distribution Warehouse";
				}

				if ("ADD".equals(page) || "EDIT".equals(page)) {
					redirectSVInProgressPage();
				}
				
				if (page.equals("ADD")) {
					action = "ADD";
//					loadStockTransferRefNumberDate();
					return gotoAddPage();
				} else if (page.equals("EDIT")) {
					stockTransferRefDTO = new StockTransferRefDTO();
					action = "EDIT";
					return gotoEditPage();
				} else if (page.equals("VIEW")) {
					stockTransferRefDTO = new StockTransferRefDTO();
					action = "VIEW";
					return gotoViewPage();
				} else if (page.equals("LIST")) {
					action = "LIST";
					return getListPage();
				} else {
					return null;
				}
			} else {
				log.info("--------gotoPage not valid user----------");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.NOT_VALID_USER).getErrorCode());
			}
		}

		return null;
	}

//	public String redirectSVInProgressPage() {
//		if(loginBean.getStockVerificationIsInProgess()) {
//			loginBean.setMessage("Outward");
//			loginBean.redirectSVInProgressPgae();
//		}
//		return null;
//	}
	
	public String redirectSVInProgressPage() {
		try {
			Boolean stockVerificationIsInProgress = loginBean.getStockVerificationIsInProgess();
			if (stockVerificationIsInProgress != null && stockVerificationIsInProgress.booleanValue()) {
				loginBean.setMessage("Outward");
				loginBean.redirectSVInProgressPgae();
			}
		} catch (Exception ex) {
			log.error("Exception at redirectSVInProgressPage()", ex);
		}
		return null;
	}
	
	public void loadStockTransferRefNumberDate() {
		log.info("<=stockOutwardPWHBean :: loadStockTransferRefNumberDate :: start=>");
		try {
			Long entityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/stock/transfer/loadstocktransferrefnumber/" + entityId;
			log.info("loadStockTransferRefNumberDate :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					stockTransferRefDTOList = mapper.readValue(jsonValue,
							new TypeReference<List<StockTransferRefDTO>>() {
							});
					if (stockTransferRefDTOList != null && !stockTransferRefDTOList.isEmpty()) {
						log.info("StockOutwardPWHBean :: loadStockTransferRefNumberDate :: stockTransferRefDTOList==> "
								+ stockTransferRefDTOList.size());
					} else {
						log.info("stockTransferRefDTOList is null or empty");
					}

				} else {
					log.info("stockTransferRefDTOList is null or empty");
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadStockTransferRefNumberDate  :: ", e);

		}
	}

	public void changeRefStatus() {
		log.info("<=stockOutwardPWHBean :: changeRefStatus :: start=>");
		try {
			log.info("changeRefStatus :: refType==> " + stockTransfer.getScanType());
			if (!action.equals("EDIT")) {
				stockTransferItemsDisplayList = new ArrayList<>();
				stockTransferRefDTO = new StockTransferRefDTO();
				stockTransfer.setStockMovementTo(null);
				setSelectedWarehouseType(null);
				stockTransfer.setToEntityMaster(null);
				setSelectedShowroomType(null);
				setSelectedRegionalOffice(null);
			}
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
				refFlag = false;
				// stockFlag = true;
				disableBtnProductAdd = true;
			} else {
				// stockFlag = false;
				if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.BULK.toString())) {
					loadStockTransferRefNumberDate();
				}
				refFlag = true;
				disableBtnProductAdd = true;
			}
		} catch (Exception e) {
			log.error("Exception in changeRefStatus  :: ", e);

		}
	}

	public String gotoAddPage() {
		disableBtnProductAdd = true;
		selectedWarehouseType = null;
		selectedShowroomType = null;
		stockMovementToValues = stockTransferUtility.loadStockMovementToList(isPagePWH);
		warehouseTypeList = stockTransferUtility.loadWarehouseTypeList(isPagePWH);
		showroomTypeList = stockTransferUtility.loadShowroomTypeList();
		stockTransfer = new StockTransfer();
		stockTransfer.setFromEntityMaster(loginEmployee.getPersonalInfoEmployment().getWorkLocation());
		stockTransferItems = new StockTransferItems();
		bundleNumberSet = new HashSet<String>();
		stockTransferItemsDisplayListTemp = new ArrayList<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		displayRemoveButtonItemDetails = true;
		stockTransferRefDTO = new StockTransferRefDTO();
		selectStockTransferRefDTOList = new ArrayList<>();
		refFlag = false;
		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	public String gotoEditPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
			return null;
		}

		if (stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED.name())) {
			stockTransfer = commonDataService.getStockTransferById(stockTransferRequest.getId());
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
				disableBtnProductAdd = false;
				refFlag = false;
			} else {
				refFlag = true;
				disableBtnProductAdd = true;
			}
			stockTransferRefDTO.setStockTransferId(stockTransfer.getId());
			stockTransferRefDTO.setRefNumber(stockTransfer.getReferenceNumber());
			stockTransferRefDTO.setCreatedDate(stockTransfer.getCreatedDate());
			if (stockTransfer.getDateTransferred() != null) {
				stockTransferRefDTO.setTransferDate(AppUtil.DATE_FORMAT.format(stockTransfer.getDateTransferred()));
			}
			stockMovementToValues = stockTransferUtility.loadStockMovementToList(isPagePWH);
			warehouseTypeList = stockTransferUtility.loadWarehouseTypeList(isPagePWH);
			showroomTypeList = stockTransferUtility.loadShowroomTypeList();

			if (stockTransfer.getToEntityMaster() != null) {

				if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.SHOW_ROOM)) {
					selectedRegionalOffice = commonDataService
							.findRegionByShowroomId(stockTransfer.getToEntityMaster().getId());
					if (stockTransfer.getToEntityMaster().getIsLocalState()) {
						selectedShowroomType = ShowroomType.Intrastate.name();
					} else {
						selectedShowroomType = ShowroomType.Interstate.name();
					}
				} else if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.PRODUCT_WAREHOUSE)) {
					selectedWarehouseType = WarehouseType.Product_Warehouse.name();
				} else if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.DISTRIBUTION_WAREHOUSE)) {
					selectedWarehouseType = WarehouseType.Distribution_Warehouse.name();
				}

			}

			if (stockTransfer.getTransportMaster() != null) {
				selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
			}

			populateFormData();
			if (selectedTransportTypeMaster != null) {
				transportMasterList = commonDataService
						.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
			}

			bundleNumberSet = new HashSet<>();
			stockTransferItemsDisplayList = new ArrayList<>();
			for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
				addItemToDisplayViewList(items);
			}

			displayRemoveButtonItemDetails = true;

		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	public String gotoViewPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferRequest.getId());
		if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
			refFlag = false;
		} else {
			refFlag = true;
		}
		stockTransferRefDTO.setRefNumber(stockTransfer.getReferenceNumber());
		bundleNumberSet = new HashSet<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
			addItemToDisplayViewList(items);
		}
		displayRemoveButtonItemDetails = false;
		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_VIEW;
	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {
		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
		} else {
			if (!stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferRequest.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(OperationErrorCode.STOCK_OUTWARD_DELETED_SUCCESSFULLY);
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public void onChangeStockMovementTo() {
		log.info("onChangeStockMovementTo [" + stockTransfer.getStockMovementTo() + "]");

		stockTransfer.setToEntityMaster(null);

		selectedWarehouseType = null;

		selectedShowroomType = null;
		selectedRegionalOffice = null;

		if (stockTransfer.getStockMovementTo().equals(StockMovementTo.ISSR)) {
			issrList = new ArrayList<>();
			issrList = commonDataService.findEntityByEntityType(EntityType.INSTITUTIONAL_SALES_SHOWROOM);
		}
	}

	public void onChangeWarehouseType() {
		log.info("onChangeWarehouseType [" + selectedWarehouseType + "]");

		stockTransfer.setToEntityMaster(null);

		if (selectedWarehouseType != null) {
			if (selectedWarehouseType.equals(WarehouseType.Product_Warehouse.name())) {
				warehouseList = commonDataService.findEntityByEntityType(EntityType.PRODUCT_WAREHOUSE);
			} else if (selectedWarehouseType.equals(WarehouseType.Distribution_Warehouse.name())) {
				warehouseList = commonDataService.findEntityByEntityType(EntityType.DISTRIBUTION_WAREHOUSE);
			} else {
				warehouseList = null;
			}
		}
	}

	public void onChangeWarehouse() {
		log.info("onChangeWarehouse [" + stockTransfer.getToEntityMaster() + "]");
		onChangeToEntityMaster();
	}

	public void onChangeShowroomType() {
		log.info("onChangeShowroomType [" + selectedShowroomType + "]");

		stockTransfer.setToEntityMaster(null);
		selectedRegionalOffice = null;
		showroomList = null;

		if (selectedShowroomType != null) {

			if (selectedShowroomType.equals(ShowroomType.Intrastate.name())) {
				regionalOfficeList = commonDataService.findRegionalOfficeOfShowroomsByLocalState(true);
			} else if (selectedShowroomType.equals(ShowroomType.Interstate.name())) {
				regionalOfficeList = commonDataService.findRegionalOfficeOfShowroomsByLocalState(false);
			} else {
				regionalOfficeList = null;
			}
		}
	}

	public void onChangeRegionalOffice() {
		log.info("onChangeRegionalOffice [" + selectedRegionalOffice + "]");

		stockTransfer.setToEntityMaster(null);

		if (selectedRegionalOffice != null && selectedShowroomType != null) {

			if (selectedShowroomType.equals(ShowroomType.Intrastate.name())) {
				showroomList = commonDataService
						.findShowroomByRegionalOfficeAndLocalState(selectedRegionalOffice.getId(), true);
			} else if (selectedShowroomType.equals(ShowroomType.Interstate.name())) {
				showroomList = commonDataService
						.findShowroomByRegionalOfficeAndLocalState(selectedRegionalOffice.getId(), false);
			} else {
				showroomList = null;
			}

		}

	}

	public void onChangeShowroom() {
		log.info("onChangeRegionalOffice [" + stockTransfer.getToEntityMaster() + "]");
		onChangeToEntityMaster();

		log.info("onChange complete");
	}

	public void onChangeToEntityMaster() {
		transportTypeMasterList = null;
		selectedTransportTypeMaster = null;

		if (stockTransfer.getToEntityMaster() != null) {
			// Reset the Stock Transfer Object
			stockTransferItems = new StockTransferItems();
			stockTransfer.setDateTransferred(new Date());
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
				disableBtnProductAdd = false;
			}
			populateFormData();
		}
	}

	/***************************************************************************************************************************************/

	@Getter
	@Setter
	StockTransferItems stockTransferItems = new StockTransferItems();

	@Getter
	@Setter
	Set<String> bundleNumberSet = new HashSet<String>();

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayListTemp;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDisplay;

	@Getter
	@Setter
	boolean displayRemoveButtonItemDetails = false;

	@Getter
	@Setter
	Long transferId;

	public void onSelectProductVarietyMaster() {
		log.info("Stock Transfer " + stockTransfer);
	}

	public void loadProductDetails() {
		log.info("<<< --- StockOutwardPWHBean :: loadProductDetails --->>> ");
		stockTransferItemsDisplayList = new ArrayList<>();
		stockTransferItemsDisplayListTemp = new ArrayList<>();
		stockTransfer.setStockTransferItemsList(new ArrayList<StockTransferItems>());
		stockTransferRefDTO.setStockTransferDTOList(new ArrayList<>());
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stock/transfer/product/warehouse/getProductDetailsByTransferId";
			log.info("loadProductDetails :: url==> " + url);
			if (selectStockTransferRefDTOList != null) {
				stockTransferRefDTO.setStockTransferDTOList(selectStockTransferRefDTOList);

				BaseDTO baseDTO = httpService.post(url, stockTransferRefDTO);
				Long fromEntityId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						if (baseDTO.getResponseContent() != null) {
							ObjectMapper mapper = new ObjectMapper();
							String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
							stockTransferItemsDisplayList = mapper.readValue(jsonResponse,
									new TypeReference<List<StockTransferItems>>() {
									});
							if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {
								log.info("stockTransferItemsDisplayList==> " + stockTransferItemsDisplayList.size());
								for (StockTransferItems StockTransferItemsObj : stockTransferItemsDisplayList) {
									log.info("stocktransferid-------" + StockTransferItemsObj.getStockTransferId());
									List<StockTransferItems> stockTransferItemsLists = getStockTranferItemsListByTransferIdAndProduct(
											StockTransferItemsObj.getStockTransferId(),
											StockTransferItemsObj.getProductVarietyMaster().getId(), fromEntityId);
									if (stockTransferItemsLists != null && !stockTransferItemsLists.isEmpty()) {
										stockTransferItemsLists.stream().forEach(items -> {
											StockTransferItems itemsObj = new StockTransferItems();
											itemsObj = items;
											stockTransfer.getStockTransferItemsList().add(itemsObj);
										});
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());

		}
	}

	private List<StockTransferItems> getStockTranferItemsListByTransferIdAndProduct(Long transferId, Long productId,
			Long fromEntityId) {
		log.info("findItemByQRCode method is executing..");
		List<StockTransferItems> stockTransferItemsLists = null;
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/items/bytransferproductid/"
				+ transferId + "/" + productId + "/" + fromEntityId;
		try {
			stockTransferItemsLists = new ArrayList<>();
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer Items get successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					stockTransferItemsLists = mapper.readValue(jsonResponse,
							new TypeReference<List<StockTransferItems>>() {
							});
				} else if (baseDTO.getStatusCode()
						.equals(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode())) {
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getErrorCode());
					stockTransferItemsDisplayList = new ArrayList<>();
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		return stockTransferItemsLists;
	}

	String qrCode = "";

	public String listenQrCodeOrBarCode() {
		log.info("<<< --- Start the listenQrCodeOrBarCode() --->>> ");
		log.info(stockTransferItems.getQrCode());
		qrCode = "";
		try {
			if (stockTransfer.getStockMovementTo() != null
					&& stockTransfer.getStockMovementTo().equals(StockMovementTo.Soceity)) {
				if (stockTransferItems.getQrCode() != null && stockTransferItems.getQrCode().length() > 0) {
					log.info("Society");
				}
			} else {
				if (stockTransferItems.getQrCode() != null && stockTransferItems.getQrCode().length() > 0) {
					ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();
					if (AppUtil.isJSONValid(stockTransferItems.getQrCode())) {
						itemQRCodeDetailsDTO = new Gson().fromJson(stockTransferItems.getQrCode(),
								ItemQRCodeDetailsDTO.class);
						if (itemQRCodeDetailsDTO != null) {
							String qrCode = itemQRCodeDetailsDTO.getQRCode();
							log.info("QR Code :: " + qrCode);
							itemQRCodeDetailsDTO = getItemQrcodeDetails(qrCode);
						} else {
							log.info("itemQRCodeDetailsDTO is null");
						}
					} else {
						itemQRCodeDetailsDTO = getItemQrcodeDetails(stockTransferItems.getQrCode());
					}

					if (itemQRCodeDetailsDTO == null) {
						itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();
					}
					itemQRCodeDetailsDTO.setModeOfTransfer(
							itemQRCodeDetailsDTO.getModeOfTransfer() != null ? itemQRCodeDetailsDTO.getModeOfTransfer()
									: "");
					//
					log.info("== ===============================================");
					log.info("QR Code :: " + itemQRCodeDetailsDTO.getQRCode());
					log.info("ATNumber :: " + itemQRCodeDetailsDTO.getATNumber());
					log.info("ProductVarietyCode :: " + itemQRCodeDetailsDTO.getProductVarietyCode());
					log.info("== ===============================================");

					if (itemQRCodeDetailsDTO.getQRCode() != null
							&& itemQRCodeDetailsDTO.getProductVarietyCode() != null) {

						qrCode = itemQRCodeDetailsDTO.getQRCode();
						/*if (stockTransferUtility.isAtNumberDuplicated(itemQRCodeDetailsDTO.getATNumber(),
								stockTransfer.getStockTransferItemsList())
								&& !itemQRCodeDetailsDTO.getModeOfTransfer().equalsIgnoreCase("UPLOADED")) {
							errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getCode());
							String bundleNumber = stockTransferItems.getBundleNumber();
							stockTransferItems = new StockTransferItems();
							stockTransferItems.setBundleNumber(bundleNumber);
							return null;
						}*/

						StockTransferItems items = findItemByQRCode(itemQRCodeDetailsDTO,
								stockTransfer.getFromEntityMaster().getId(), isPagePWH);
						if (items != null) {
							items.setQrCode(itemQRCodeDetailsDTO.getQRCode());
							items.setUomMaster(items.getProductVarietyMaster().getUomMaster());
							items.setBundleNumber(stockTransferItems.getBundleNumber());
							
							// items.setDispatchedQty(items.getReceivedQty());
							if (!items.getUomMaster().getCode().equalsIgnoreCase("METR")) {
								/*
								 * Double totalDispatchedQty = stockTransferItemsDisplayList.stream().filter(o
								 * -> (o.getStockTransferItemsBundlesList().stream(). filter(bud ->
								 * bud.getQrCode() != null &&
								 * itemQRCodeDetailsDTO.getQRCode().equals(bud.getQrCode()))).
								 * collect(StockTransferItems::getDispatchedQty).;
								 */
//								Double quantity = 0D;
								totalDispatchedQty = 0D;
								stockTransferItemsDisplayList.forEach(itemsObj -> {
									if (itemsObj.getProductVarietyMaster() != null
											&& items.getProductVarietyMaster() != null) {
										if (itemsObj.getProductVarietyMaster().getCode()
												.equals(items.getProductVarietyMaster().getCode())) {
											totalDispatchedQty = itemsObj.getStockTransferItemsBundlesList().stream()
													.filter(k -> k.getQrCode() != null && qrCode.equals(k.getQrCode()))
													.collect(Collectors
															.summingDouble(StockTransferItemsBundles::getQuantity));
										}
									}
								});
								if (totalDispatchedQty.equals(items.getDispatchedQty())
										|| totalDispatchedQty >= items.getDispatchedQty()) {
									errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getCode());
									String bundleNumber = stockTransferItems.getBundleNumber();
									stockTransferItems = new StockTransferItems();
									stockTransferItems.setBundleNumber(bundleNumber);
									return null;
								} else {
									items.setDispatchedQty(Double.valueOf(1));
									stockTransfer.getStockTransferItemsList().add(items);
								}
							} else {
								totalDispatchedQty = 0D;
								stockTransferItemsDisplayList.forEach(itemsObj -> {
									if (itemsObj.getProductVarietyMaster() != null
											&& items.getProductVarietyMaster() != null) {
										if (itemsObj.getProductVarietyMaster().getCode()
												.equals(items.getProductVarietyMaster().getCode())) {
											totalDispatchedQty = itemsObj.getStockTransferItemsBundlesList().stream()
													.filter(k -> k.getQrCode() != null && qrCode.equals(k.getQrCode()))
													.collect(Collectors
															.summingDouble(StockTransferItemsBundles::getQuantity));
										}
									}
								});
								if (stockTransferUtility.isQrCodeDuplicated(itemQRCodeDetailsDTO.getQRCode(),
										stockTransfer.getStockTransferItemsList())
										&& items.getUomMaster().getCode().equalsIgnoreCase("METR")) {
									errorMap.notify(ErrorDescription.STOCK_TRANSFER_QR_CODE_DUPLICATED.getCode());
									String bundleNumber = stockTransferItems.getBundleNumber();
									stockTransferItems = new StockTransferItems();
									stockTransferItems.setBundleNumber(bundleNumber);
									return null;
								}
								if (totalDispatchedQty.equals(items.getDispatchedQty())
										|| totalDispatchedQty >= items.getDispatchedQty()) {
									errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getCode());
									String bundleNumber = stockTransferItems.getBundleNumber();
									stockTransferItems = new StockTransferItems();
									stockTransferItems.setBundleNumber(bundleNumber);
									return null;
								} else {
									stockTransfer.getStockTransferItemsList().add(items);
								}
							}
							String bundleNumber = stockTransferItems.getBundleNumber();
							stockTransferItems = new StockTransferItems();
							stockTransferItems.setBundleNumber(bundleNumber);

							addItemToDisplayList(items);
						} else {
							errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getCode());
							String bundleNumber = stockTransferItems.getBundleNumber();
							stockTransferItems = new StockTransferItems();
							stockTransferItems.setBundleNumber(bundleNumber);
							return null;
						}

					} else {
						AppUtil.addError("Wrong QR Code");
						String bundleNumber = stockTransferItems.getBundleNumber();
						stockTransferItems = new StockTransferItems();
						stockTransferItems.setBundleNumber(bundleNumber);
						return null;
					}

				}
			}
			qrCode = "";
		} catch (Exception e) {
			log.error("Exeption at listenQrCodeOrBarCode() ", e);
		}
		log.info("<<< --- Start the listenQrCodeOrBarCode() --->>> ");
		return null;
	}

	public void addItemToDisplayList(StockTransferItems items) {
		StockTransferItems stiItems = new StockTransferItems(items);
		if (items.getStockTransferId() != null) {
			stiItems.setStockTransferId(items.getStockTransferId());
		}
		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setBundleNumber(stiItems.getBundleNumber());
		bundles.setAtNumber(stiItems.getAtNumber());
		bundles.setQuantity(AppUtil.ifNullRetunZero(stiItems.getDispatchedQty()));
		bundles.setStockTransferItems(stiItems);
		log.info("unit rate----------------" + stiItems.getUnitRate());
		bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stiItems.getUnitRate()) * bundles.getQuantity());
		bundles.setQrCode(stiItems.getQrCode() == null ? "" : stiItems.getQrCode());

		if (stiItems.getBundleNumber() != null) {
			bundleNumberSet.add(stiItems.getBundleNumber());
			updateBundleNumbers(bundleNumberSet);
		}

		if (stockTransferItemsDisplayList == null) {
			stockTransferItemsDisplayList = new ArrayList<>();
		}
		int count = 0;
		if (!stockTransferItemsDisplayList.isEmpty()) {
			for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
				if (sti.getProductVarietyMaster().getId().equals(stiItems.getProductVarietyMaster().getId())) {
					sti.setDispatchedQty(sti.getDispatchedQty() + stiItems.getDispatchedQty());
					// sti.setItemNetTotal(sti.getItemNetTotal() + stiItems.getItemNetTotal());
					// sti.setItemNetTotal((sti.getItemNetTotal()!=null?sti.getItemNetTotal():0) +
					// (stiItems.getUnitRate()!=null?stiItems.getUnitRate():0));
					sti.setItemNetTotal(sti.getItemNetTotal() + (stiItems.getUnitRate() * stiItems.getDispatchedQty()));
					if (sti.getStockTransferItemsBundlesList().stream()
							.anyMatch(stib -> stib.getQrCode().equalsIgnoreCase(bundles.getQrCode()))) {
						for (StockTransferItemsBundles stockTransferItemsBundles : sti
								.getStockTransferItemsBundlesList()) {
							if (stockTransferItemsBundles.getQrCode().equalsIgnoreCase(bundles.getQrCode())) {
								stockTransferItemsBundles
										.setQuantity(stockTransferItemsBundles.getQuantity() + bundles.getQuantity());
								stockTransferItemsBundles.setItemNetTotal(
										stockTransferItemsBundles.getItemNetTotal() + bundles.getItemNetTotal());
							}
						}
						/*
						 * sti.getStockTransferItemsBundlesList().forEach(stbl ->{
						 * 
						 * });
						 */
						++count;
						break;
					} else {
						sti.getStockTransferItemsBundlesList().add(bundles);
						++count;
						break;
					}
				}

			}
			if (count <= 0) {
				stiItems.getStockTransferItemsBundlesList().add(bundles);
				if (stiItems.getDispatchedQty() <= 1) {
					stiItems.setItemNetTotal(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0);
				} else {
					stiItems.setItemNetTotal((stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0)
							* stiItems.getDispatchedQty());
				}
				stockTransferItemsDisplayList.add(stiItems);
			}
		} else {
			stiItems.getStockTransferItemsBundlesList().add(bundles);
			// stiItems.setDispatchedQty(stiItems.getReceivedQty());
			if (stiItems.getDispatchedQty() <= 1) {
				stiItems.setItemNetTotal(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0);
			} else {
				stiItems.setItemNetTotal(
						(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0) * stiItems.getDispatchedQty());
			}
			stockTransferItemsDisplayList.add(stiItems);
		}

		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
	}

	public void addItemToDisplayViewList(StockTransferItems items) {

		StockTransferItems stiItems = new StockTransferItems(items);

		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setBundleNumber(stiItems.getBundleNumber());
		bundles.setAtNumber(stiItems.getAtNumber());
		bundles.setQuantity(AppUtil.ifNullRetunZero(stiItems.getDispatchedQty()));
		bundles.setStockTransferItems(stiItems);
		log.info("unit rate----------------" + stiItems.getUnitRate());
		bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stiItems.getUnitRate()) * bundles.getQuantity());
		bundles.setQrCode(stiItems.getQrCode() == null ? "" : stiItems.getQrCode());

		if (stiItems.getBundleNumber() != null) {
			bundleNumberSet.add(stiItems.getBundleNumber());
			updateBundleNumbers(bundleNumberSet);
		}

		if (stockTransferItemsDisplayList == null) {
			stockTransferItemsDisplayList = new ArrayList<>();
		}
		int count = 0;
		if (!stockTransferItemsDisplayList.isEmpty()) {
			for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
				if (sti.getProductVarietyMaster().getId().equals(stiItems.getProductVarietyMaster().getId())) {
					sti.setDispatchedQty(sti.getDispatchedQty() + stiItems.getDispatchedQty());
					// sti.setItemNetTotal(sti.getItemNetTotal() + stiItems.getItemNetTotal());
					// sti.setItemNetTotal((sti.getItemNetTotal()!=null?sti.getItemNetTotal():0) +
					// (stiItems.getUnitRate()!=null?stiItems.getUnitRate():0));
					sti.setItemNetTotal(sti.getItemNetTotal() + (stiItems.getUnitRate() * stiItems.getDispatchedQty()));
					if (sti.getStockTransferItemsBundlesList().stream()
							.anyMatch(stib -> stib.getQrCode().equalsIgnoreCase(bundles.getQrCode()))) {
						for (StockTransferItemsBundles stockTransferItemsBundles : sti
								.getStockTransferItemsBundlesList()) {
							if (stockTransferItemsBundles.getQrCode().equalsIgnoreCase(bundles.getQrCode())) {
								stockTransferItemsBundles
										.setQuantity(stockTransferItemsBundles.getQuantity() + bundles.getQuantity());
								stockTransferItemsBundles.setItemNetTotal(
										stockTransferItemsBundles.getItemNetTotal() + bundles.getItemNetTotal());
							}
						}
						/*
						 * sti.getStockTransferItemsBundlesList().forEach(stbl ->{
						 * 
						 * });
						 */
						++count;
						break;
					} else {
						sti.getStockTransferItemsBundlesList().add(bundles);
						++count;
						break;
					}
				}

			}
			if (count <= 0) {
				stiItems.getStockTransferItemsBundlesList().add(bundles);
				if (stiItems.getDispatchedQty() <= 1) {
					stiItems.setItemNetTotal(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0);
				} else {
					stiItems.setItemNetTotal((stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0)
							* stiItems.getDispatchedQty());
				}
				stockTransferItemsDisplayList.add(stiItems);
			}
		} else {
			stiItems.getStockTransferItemsBundlesList().add(bundles);
			// stiItems.setDispatchedQty(stiItems.getReceivedQty());
			if (stiItems.getDispatchedQty() <= 1) {
				stiItems.setItemNetTotal(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0);
			} else {
				stiItems.setItemNetTotal(
						(stiItems.getUnitRate() != null ? stiItems.getUnitRate() : 0) * stiItems.getDispatchedQty());
			}
			stockTransferItemsDisplayList.add(stiItems);
		}

		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
	}

	public void removeReceiveProduct(StockTransferItemsBundles bundles) {
		log.info("removeReceiveProduct ", bundles);
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			if (!bundles.getAtNumber().isEmpty() && !bundles.getAtNumber().equalsIgnoreCase("000000")) {
				stockTransfer.getStockTransferItemsList()
						.removeIf(item -> item.getAtNumber().equals(bundles.getAtNumber()));
			} else {
				stockTransfer.getStockTransferItemsList()
						.removeIf(item -> item.getQrCode().equals(bundles.getQrCode()));
			}
//			removeItemFromDisplayList(bundles);
			for (StockTransferItems items : stockTransferItemsDisplayList) {
				items.getStockTransferItemsBundlesList()
						.removeIf(b -> b.getQrCode().equalsIgnoreCase(bundles.getQrCode()));
			}
			bundleNumberSet = new HashSet<String>();
			stockTransferItemsDisplayList = new ArrayList<>();
			for (StockTransferItems stockTransferItems : stockTransfer.getStockTransferItemsList()) {
				addItemToDisplayList(stockTransferItems);
			}
		}
	}

	public void removeItemFromDisplayList(StockTransferItemsBundles bundles) {
		if (bundles.getBundleNumber() != null && bundleNumberSet != null && !bundleNumberSet.isEmpty()) {
			bundleNumberSet.removeIf(bundleNumber -> bundleNumber.equals(bundles.getBundleNumber()));
			updateBundleNumbers(bundleNumberSet);
		}

		if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {
			StockTransferItems itemToRemove = null;
			boolean removeItemFlag = false;
			for (StockTransferItems items : stockTransferItemsDisplayList) {
				boolean isBundleRemoved = false;
				if (items.getProductVarietyMaster().getId()
						.equals(bundles.getStockTransferItems().getProductVarietyMaster().getId())) {
					if (items.getStockTransferItemsBundlesList() != null
							&& !items.getStockTransferItemsBundlesList().isEmpty()
							&& items.getStockTransferItemsBundlesList()
									.removeIf(b -> b.getQrCode().equalsIgnoreCase(bundles.getQrCode()))) {
						isBundleRemoved = true;
					}
					if (isBundleRemoved) {
						items.setDispatchedQty(items.getDispatchedQty() - bundles.getQuantity());
						items.setItemNetTotal(AppUtil.ifNullRetunZero(items.getItemNetTotal())
								- (bundles.getQuantity() * (AppUtil.ifNullRetunZero(bundles.getItemNetTotal()))));
						if (items.getDispatchedQty() == 0) {
							removeItemFlag = true;
							itemToRemove = items;
						}
					}
				}
			}
			if (removeItemFlag) {
				stockTransferItemsDisplayList.removeIf(stid -> stid.getProductVarietyMaster().getCode()
						.equalsIgnoreCase(bundles.getStockTransferItems().getProductVarietyMaster().getCode()));
//				stockTransferItemsDisplayList.remove(itemToRemove);
			}
		}

	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null && !setOfBundleNumbers.isEmpty()) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	public void viewItemDetails(StockTransferItems items) {
		if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
			stockTransferItemsDisplay = items;
		} else {
			stockTransferItemsDisplay = items;
			stockTransferItemsDisplay.setStockTransferItemsBundlesList(new ArrayList<>());
			for (StockTransferItems stockTransferItems : stockTransfer.getStockTransferItemsList()) {
				if (items.getProductVarietyMaster().getId()
						.equals(stockTransferItems.getProductVarietyMaster().getId())) {
					StockTransferItems stiItems = new StockTransferItems(stockTransferItems);

					StockTransferItemsBundles bundles = new StockTransferItemsBundles();
					bundles.setBundleNumber(stiItems.getBundleNumber());
					bundles.setAtNumber(stiItems.getAtNumber());
					bundles.setQuantity(AppUtil.ifNullRetunZero(stiItems.getDispatchedQty()));
					bundles.setStockTransferItems(stiItems);
					log.info("viewItemDetails unit rate----------------->" + stiItems.getUnitRate());
					bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stiItems.getUnitRate()));
					bundles.setItemNetTotal(bundles.getQuantity() * bundles.getItemNetTotal());
					bundles.setQrCode(stiItems.getQrCode() == null ? "" : stiItems.getQrCode());

					if (stiItems.getBundleNumber() != null) {
						bundleNumberSet.add(stiItems.getBundleNumber());
						updateBundleNumbers(bundleNumberSet);
					}
					stiItems.setDispatchedQty(stiItems.getDispatchedQty());
					stockTransferItemsDisplay.getStockTransferItemsBundlesList().add(bundles);
				}
			}
		}
	}

	public void viewStockTransferItemDetails(StockTransferItems items) {
		try {
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.INDIVIDUAL.toString())) {
				stockTransferItemsDisplay = items;
			} else {
				stockTransferItemsDisplay = items;
				Long itemId = items.getProductVarietyMaster() != null ? items.getProductVarietyMaster().getId() : null;
				log.info("items product varity id=1--------->" + itemId);
				log.info("items Unit rate =1--------->" + items.getUnitRate());
				stockTransferItemsDisplay.setStockTransferItemsBundlesList(new ArrayList<>());
				for (StockTransferItems stockTransferItems : stockTransfer.getStockTransferItemsList()) {
					Long productId = stockTransferItems.getProductVarietyMaster() != null ? stockTransferItems.getProductVarietyMaster().getId() : null;
					log.info("items product varity id=-2-------->" + productId);
					log.info("items  Unit rate=-2 -------->" + stockTransferItems.getUnitRate());
					if (itemId.longValue() == productId.longValue()) {
						StockTransferItems stiItems = new StockTransferItems(stockTransferItems);

						StockTransferItemsBundles bundles = new StockTransferItemsBundles();
						bundles.setBundleNumber(stiItems.getBundleNumber());
						bundles.setAtNumber(stiItems.getAtNumber());
						bundles.setQuantity(AppUtil.ifNullRetunZero(stiItems.getDispatchedQty()));
						bundles.setStockTransferItems(stiItems);
						log.info("viewItemDetails unit rate----------------->" + stiItems.getUnitRate());
						bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stiItems.getUnitRate()));
						bundles.setItemNetTotal(bundles.getQuantity() * bundles.getItemNetTotal());
						bundles.setQrCode(stiItems.getQrCode() == null ? "" : stiItems.getQrCode());

						if (stiItems.getBundleNumber() != null) {
							bundleNumberSet.add(stiItems.getBundleNumber());
							updateBundleNumbers(bundleNumberSet);
						}
						stiItems.setDispatchedQty(stiItems.getDispatchedQty());
						stockTransferItemsDisplay.getStockTransferItemsBundlesList().add(bundles);
					}
				}
			}
		} catch (Exception e) {
			log.error("viewStockTransferItemDetails exception", e);
		}
	}

	/*************************************************************************************************************************************/

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<Object> payToPayList;

	public void populateFormData() {
		transportTypeMasterList = commonDataService.getAllTransportTypes();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
	}

	public void onChangeTransportTypeMaster() {
		transportMasterList = null;
		stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void onChangeTransport() {
		log.info("onSelectTransport is called");
		if (stockTransfer.getTransportMaster() != null) {
			log.info("Selected Transport ID = [" + stockTransfer.getTransportMaster().getId() + "]");
		}
	}

	/*************************************************************************************************************************************/

	public String submit(String action) {

		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}
		clearBundleDetails();

		if (isPagePWH && "Product Warehouse".equalsIgnoreCase(pageName)) {
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.BULK.toString())) {
				stockTransfer.setTransferType(StockTransferType.WAREHOUSE_OUTWARD_BULK);
			} else {
				stockTransfer.setTransferType(StockTransferType.WAREHOUSE_OUTWARD);
			}
		}else if(isPagePWH && "Ecommerce".equalsIgnoreCase(pageName)){
			if (stockTransfer.getScanType().equals(StockTransferScanTypeEnum.BULK.toString())) {
				stockTransfer.setTransferType(StockTransferType.ESHOPPING_OUTWARD_BULK);
			} else {
				stockTransfer.setTransferType(StockTransferType.ESHOPPING_OUTWARD);
			}
		}
			else {
			stockTransfer.setTransferType(StockTransferType.DISTRIB_OUTWARD);
		}

		/*
		 * if (stockTransferRefDTO != null) {
		 * log.info("inward reference number-----------" +
		 * stockTransferRefDTO.getRefNumber());
		 * log.info("inward stocktransfer id-----------" +
		 * stockTransferRefDTO.getStockTransferId());
		 * stockTransfer.setStockInwardId(stockTransferRefDTO.getStockTransferId()); }
		 */

		return create();
	}

	public void clearBundleDetails() {
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList().stream().forEach(items -> {
				items.getStockTransferItemsBundlesList().clear();
			});
		}
	}

	public String create() {
		log.info("create method is executing..");

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/outward/create";
		try {
			List<StockTransferItems> stockTransferItemListtemp = new ArrayList<StockTransferItems>();
			for (StockTransferItems stockTransferItemObj : stockTransfer.getStockTransferItemsList()) {
				if (stockTransferItemListtemp.stream().filter(st -> st.getQrCode() != null)
						.anyMatch(sti -> sti.getQrCode().equalsIgnoreCase(stockTransferItemObj.getQrCode()))) {
					for (StockTransferItems stockTransferitemsObjTemp : stockTransferItemListtemp) {
						if (stockTransferItemObj.getQrCode().equalsIgnoreCase(stockTransferitemsObjTemp.getQrCode())) {
							stockTransferitemsObjTemp.setDispatchedQty(stockTransferitemsObjTemp.getDispatchedQty()
									+ stockTransferItemObj.getDispatchedQty());
						}
					}
				} else {
					stockTransferItemListtemp.add(stockTransferItemObj);
				}
			}
			stockTransfer.setStockTransferItemsList(new ArrayList<>());
			stockTransfer.setStockTransferItemsList(stockTransferItemListtemp);

			stockTransfer.setStockTransferGroupsList(new ArrayList<>());
			int stockTransferListSize = selectStockTransferRefDTOList == null ? 0
					: selectStockTransferRefDTOList.size();
			log.info("stocktransfer selected list size--------->" + stockTransferListSize);
			if (stockTransferListSize > 0) {
				for (StockTransferRefDTO dto : selectStockTransferRefDTOList) {
					StockTransferGroup stockTransferGroup = new StockTransferGroup();
					stockTransferGroup
							.setFromStockTransfer(new StockTransfer(dto.getStockTransferId(), dto.getRefNumber()));
					stockTransfer.getStockTransferGroupsList().add(stockTransferGroup);
				}
				log.info("stocktransfer obj list size--------->" + stockTransfer.getStockTransferGroupsList().size());
			}

			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SAVED_SUCCESSFULLY);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					if(StringUtils.isNotEmpty(msg)) {
						AppUtil.addError(msg);
						return null;
					} else {
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

	private StockTransferItems findItemByQRCode(ItemQRCodeDetailsDTO itemQRCodeDetailsDTO, Long entityId,
			boolean searchForPwh) {
		log.info("findItemByQRCode method is executing..");

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/product/warehouse/qrcode/"
				+ entityId + "/" + searchForPwh;
		try {
			BaseDTO baseDTO = httpService.post(URL, itemQRCodeDetailsDTO);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Stock Transfer Items get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				StockTransferItems stockTransferItems = mapper.readValue(jsonResponse, StockTransferItems.class);
				return stockTransferItems;
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		return null;

	}

	public String authorization() throws IOException {
		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		/*
		 * if(!loginBean.getUserFeatures().containsKey("PWH_STOCK_OUTWARD_ADD") &&
		 * FacesContext.getCurrentInstance().getPartialViewContext() != null &&
		 * !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()){
		 * log.info("User dont have feature for this page..................");
		 * FacesContext.getCurrentInstance().getExternalContext().redirect(AppUtil.
		 * getUnAuthorizedPageUrl()); }
		 */
		return null;
	}

	@Getter
	@Setter
	Double deliverychallanTotalAmount = 0.0;

	public String createDeliveryChallan() {
		log.info("<==== Starts createDeliveryChallan ====>");
		try {
			deliverychallanTotalAmount = 0.0;
			if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
				errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				return null;
			}

			if (!stockTransferRequest.getStatus().equals(StockTransferStatus.SUBMITTED.name())) {
				// errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
				AppUtil.addWarn("Delivery challan only for submitted records");
				return null;
			}

			stockTransfer = commonDataService.getStockTransferById(stockTransferRequest.getId());

			transferItemsList = new ArrayList<>();

			if (stockTransfer != null) {

				Map<Long, List<StockTransferItems>> stockTransferItems = stockTransfer.getStockTransferItemsList()
						.stream().collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));

				if (stockTransferItems != null) {

					for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItems.entrySet()) {

						Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));

						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount
								.entrySet()) {

							DeliveryChallanDTO dto = new DeliveryChallanDTO();

							dto.setId(entrynew.getValue().get(0).getId());
							dto.setHsnCode(entrynew.getValue().get(0).getProductVarietyMaster().getHsnCode());
							dto.setItem(entrynew.getValue().get(0).getProductVarietyMaster().getCode());
							dto.setProductName(entrynew.getValue().get(0).getProductVarietyMaster().getName());
							dto.setUnitRate(entrynew.getValue().get(0).getUnitRate());
							Double Qty = entrynew.getValue().stream().mapToDouble(t -> t.getDispatchedQty()).sum();
							dto.setQuantity(Qty);
//							dto.setQuantity(entrynew.getValue().size());

							Double rate = entrynew.getValue().get(0).getUnitRate() * dto.getQuantity();
							dto.setProductRate(rate);

							DecimalFormat decim = new DecimalFormat("0.00");

							String price = decim.format(dto.getUnitRate());
							dto.setUnitPrice(price.split("\\.")[0]);
							dto.setUnitPaisa(price.split("\\.")[1]);

							String p = decim.format(rate);
							dto.setTotalPriceValue(rate);
							dto.setTotalPrice(p.split("\\.")[0]);
							dto.setTotalPaisa(p.split("\\.")[1]);
							deliverychallanTotalAmount = deliverychallanTotalAmount + (rate != null ? rate : 0.0);
							transferItemsList.add(dto);
						}
					}
					/*
					 * for(DeliveryChallanDTO deliveryChallanDTO: transferItemsList) {
					 * DeliveryChallanDTO dto=new DeliveryChallanDTO(); dto=deliveryChallanDTO;
					 * transferItemsList.add(dto); }
					 */

					log.info("transferItemsList size is ::::::" + transferItemsList.size());

					Map<String, List<DeliveryChallanDTO>> hsnCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(r -> r.getHsnCode()));
					hsnSummaryList = new ArrayList<>();
					productCodeSummaryList = new ArrayList<>();
					if (hsnCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : hsnCodeWiseGroupingDTO.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setHsnCode(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							hsnSummaryList.add(deliveryChallanDTO);
						}
					}

					Map<String, List<DeliveryChallanDTO>> productCodeWiseGroupingDTO = transferItemsList.stream()
							.collect(Collectors.groupingBy(c -> c.getItem()));
					if (productCodeWiseGroupingDTO != null) {
						for (Map.Entry<String, List<DeliveryChallanDTO>> entry : productCodeWiseGroupingDTO
								.entrySet()) {
							DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
							deliveryChallanDTO.setItem(entry.getKey());
							deliveryChallanDTO
									.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
							deliveryChallanDTO.setTotalPriceValue(
									entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
							productCodeSummaryList.add(deliveryChallanDTO);
						}
					}
					transferItemsList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					hsnSummaryList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
					productCodeSummaryList.sort((s1, s2) -> s1.getItem().compareTo(s2.getItem()));
					/*
					 * abstractList = new ArrayList<>();
					 * 
					 * if (transferItemsList.size() <= 10) { List<DeliveryChallanDTO> tempList =
					 * transferItemsList.subList(0, transferItemsList.size()); DeliveryChallanDTO
					 * abs = new DeliveryChallanDTO(); abs.setQuantity(tempList.stream().mapToInt(t
					 * -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs); } else { for(int i = 1; i
					 * < transferItemsList.size(); i++) {
					 * 
					 * if(i==1) { List<DeliveryChallanDTO> tempList = transferItemsList.subList(0,
					 * 10); DeliveryChallanDTO abs = new DeliveryChallanDTO();
					 * abs.setQuantity(tempList.stream().mapToInt(t -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs); i=9; }else { if
					 * (transferItemsList.size() > 30) { List<DeliveryChallanDTO> tempList =
					 * transferItemsList.subList(i, i+20); DeliveryChallanDTO abs = new
					 * DeliveryChallanDTO(); abs.setQuantity(tempList.stream().mapToInt(t ->
					 * t.getQuantity()).sum()); abs.setProductRate(tempList.stream().mapToDouble(t
					 * -> t.getProductRate()).sum()); abstractList.add(abs); i=i+20;
					 * 
					 * }else { List<DeliveryChallanDTO> tempList = transferItemsList.subList(i,
					 * transferItemsList.size()); DeliveryChallanDTO abs = new DeliveryChallanDTO();
					 * abs.setQuantity(tempList.stream().mapToInt(t -> t.getQuantity()).sum());
					 * abs.setProductRate(tempList.stream().mapToDouble(t ->
					 * t.getProductRate()).sum()); abstractList.add(abs);
					 * i=transferItemsList.size(); } }
					 * 
					 * } }
					 */

				}

//				log.info("abstractList size is ::::::" + abstractList.size());

				return DELIVERY_CHALLAN_ACKNOWLEDGE;
			} else {
				log.info("Object is null");
			}
			log.info("<==== Ends createDeliveryChallan ====>");
			return null;
		} catch (Exception e) {
//			log.info("abstractList size is ::::::" + abstractList.size());
			log.error("Error in createDeliveryChallan Method", e);
		}
		return null;
	}

	public String getValue(Double p) {
		DecimalFormat decim = new DecimalFormat("0.00");

		String price = decim.format(p);

		return price;
	}

	@SuppressWarnings("unused")
	public String createStockTransferInvoice() {
		log.info("<==== Starts createStockTransferInvoice ====>");

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
			return null;
		}

		if (!stockTransferRequest.getStatus().equals(StockTransferStatus.SUBMITTED.name())) {
			// errorMap.notify(OperationErrorCode.STOCK_OUTWARD_SELECT_ONE_RECORD);
			errorMap.notify(OperationErrorCode.TRANSFER_INVOICE_ONLY_FOR_SUBMITED_RECORDS);
			return null;
		}

		if (stockTransferRequest.getSenderGstNumber() != null && stockTransferRequest.getReceiverGstNumber() != null) {
			if (stockTransferRequest.getSenderGstNumber()
					.equalsIgnoreCase(stockTransferRequest.getReceiverGstNumber())) {
				AppUtil.addWarn("Sender and Receiver Gst Number both is same so transfer invoice is not applicable");
				return null;
			}
		}
		if (stockTransferRequest.getSenderGstNumber() == null || stockTransferRequest.getReceiverGstNumber() == null) {
			AppUtil.addWarn("Need Sender and Receiver Gst Number");
			return null;
		}

		stockTransfer = commonDataService.getStockTransferDetailsByStockTransferId(stockTransferRequest.getId());

		if (stockTransfer.getToEntityMaster() != null) {
			EntityMaster entityMaster = commonDataService
					.findRegionByShowroomId(stockTransfer.getToEntityMaster().getId());
			stockTransfer.getToEntityMaster().setEntityMasterRegion(entityMaster);
		}
		transferItemsList = new ArrayList<>();

		if (stockTransfer != null) {

			Map<Long, List<StockTransferItems>> stockTransferItems = stockTransfer.getStockTransferItemsList().stream()
					.collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));

			if (stockTransferItems != null) {

				for (Map.Entry<Long, List<StockTransferItems>> entry : stockTransferItems.entrySet()) {

					Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
							.filter(unit -> unit.getUnitRate() != null)
							.collect(Collectors.groupingBy(i -> i.getUnitRate()));

					for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount.entrySet()) {

						DeliveryChallanDTO dto = new DeliveryChallanDTO();

						dto.setId(entrynew.getValue().get(0).getId());
						dto.setHsnCode(entrynew.getValue().get(0).getProductVarietyMaster().getHsnCode());
						dto.setItem(entrynew.getValue().get(0).getProductVarietyMaster().getCode());
						dto.setProductName(entrynew.getValue().get(0).getProductVarietyMaster().getName());
						dto.setUnitRate(entrynew.getValue().get(0).getUnitRate());
						dto.setTaxableUnit(entrynew.getValue().get(0).getPurchasePrice());
						Double Qty = entrynew.getValue().stream().mapToDouble(t -> t.getDispatchedQty()).sum();
						dto.setQuantity(Qty);
//						dto.setQuantity(entrynew.getValue().size());
						dto.setGstQuantity(dto.getQuantity());

						DecimalFormat decim = new DecimalFormat("0.00");
						dto.setTaxableUnit(dto.getTaxableUnit() != null ? dto.getTaxableUnit() : 0D);
						String taxableUnitPaisa = decim.format(dto.getTaxableUnit());
						dto.setTaxableUnitPrice(taxableUnitPaisa.split("\\.")[0]);
						dto.setTaxableUnitPaisa(taxableUnitPaisa.split("\\.")[1]);

						Double unitPrice = dto.getUnitRate() * dto.getQuantity();
						unitPrice = (unitPrice == null ? 0D : unitPrice);
						dto.setTotalPriceValue(unitPrice);
						String price = decim.format(unitPrice);
						dto.setUnitPrice(price.split("\\.")[0]);
						dto.setUnitPaisa(price.split("\\.")[1]);
						Double taxableValue = dto.getTaxableUnit() * dto.getGstQuantity();
						if (taxableValue == null) {
							taxableValue = 0.00;
						}
						dto.setTaxableValue(taxableValue);

						String val = decim.format(taxableValue);
						dto.setTaxableVale(val.split("\\.")[0]);
						dto.setTaxablePaisa(val.split("\\.")[1]);
						dto.setTaxPercentage(entrynew.getValue().get(0).getTaxPercent());

						/*
						 * Double rate = entrynew.getValue().get(0).getPurchasePrice() *
						 * entrynew.getValue().size(); if(rate==null) { rate=0.00; } String p =
						 * decim.format(rate); dto.setTotPrice(p.split("\\.")[0]);
						 * dto.setTotPaisa(p.split("\\.")[1]);
						 */

						Double igstValue = (dto.getTaxableUnit() * dto.getTaxPercentage() / 100) * dto.getQuantity();
						if (igstValue == null) {
							igstValue = 0.00;
						}
						dto.setIgstValue(igstValue);

						Double totValue = taxableValue + igstValue;
						if (totValue == null) {
							totValue = 0.00;
						}
						dto.setTotalValue(totValue);
						String igst = decim.format(igstValue);
						String totalVal = decim.format(totValue);
						dto.setIgstTotalPrice(igst.split("\\.")[0]);
						dto.setIgstTotalPaisa(igst.split("\\.")[1]);
						dto.setTotalPrice(totalVal.split("\\.")[0]);
						dto.setTotalPaisa(totalVal.split("\\.")[1]);

						transferItemsList.add(dto);
					}
				}

				log.info("transferItemsList size is ::::::" + transferItemsList.size());

				Map<String, List<DeliveryChallanDTO>> hsnCodeWiseGroupingDTO = transferItemsList.stream()
						.collect(Collectors.groupingBy(r -> r.getHsnCode()));
				hsnSummaryList = new ArrayList<>();
				productCodeSummaryList = new ArrayList<>();
				if (hsnCodeWiseGroupingDTO != null) {
					for (Map.Entry<String, List<DeliveryChallanDTO>> entry : hsnCodeWiseGroupingDTO.entrySet()) {
						DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
						deliveryChallanDTO.setHsnCode(entry.getKey());
						deliveryChallanDTO
								.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
						deliveryChallanDTO
								.setTaxableValue(entry.getValue().stream().mapToDouble(q -> q.getTaxableValue()).sum());
						deliveryChallanDTO
								.setIgstValue(entry.getValue().stream().mapToDouble(q -> q.getIgstValue()).sum());
						deliveryChallanDTO
								.setTotalValue(entry.getValue().stream().mapToDouble(q -> q.getTotalValue()).sum());
						deliveryChallanDTO.setTotalPriceValue(
								entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());
						hsnSummaryList.add(deliveryChallanDTO);
					}
				}

				Map<String, List<DeliveryChallanDTO>> productCodeWiseGroupingDTO = transferItemsList.stream()
						.collect(Collectors.groupingBy(c -> c.getItem()));
				if (productCodeWiseGroupingDTO != null) {
					for (Map.Entry<String, List<DeliveryChallanDTO>> entry : productCodeWiseGroupingDTO.entrySet()) {
						DeliveryChallanDTO deliveryChallanDTO = new DeliveryChallanDTO();
						deliveryChallanDTO.setItem(entry.getKey());
						deliveryChallanDTO
								.setQuantity(entry.getValue().stream().mapToDouble(q -> q.getQuantity()).sum());
						deliveryChallanDTO
								.setTaxableValue(entry.getValue().stream().mapToDouble(q -> q.getTaxableValue()).sum());
						deliveryChallanDTO
								.setIgstValue(entry.getValue().stream().mapToDouble(q -> q.getIgstValue()).sum());
						deliveryChallanDTO
								.setTotalValue(entry.getValue().stream().mapToDouble(q -> q.getTotalValue()).sum());
						deliveryChallanDTO.setTotalPriceValue(
								entry.getValue().stream().mapToDouble(q -> q.getTotalPriceValue()).sum());

						productCodeSummaryList.add(deliveryChallanDTO);
					}
				}
				transferItemsList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
				hsnSummaryList.sort((s1, s2) -> s1.getHsnCode().compareTo(s2.getHsnCode()));
				productCodeSummaryList.sort((s1, s2) -> s1.getItem().compareTo(s2.getItem()));

				totalQty = transferItemsList.stream().collect(Collectors.summingDouble(totQty -> totQty.getQuantity()));
				totalValue = transferItemsList.stream().collect(Collectors
						.summingDouble(totValue -> Double.valueOf(totValue.getUnitRate() * totValue.getQuantity())));
				totalPaisa = transferItemsList.stream()
						.collect(Collectors.summingDouble(totPaisa -> Double.valueOf(totPaisa.getUnitPaisa())));
				totGstQty = transferItemsList.stream()
						.collect(Collectors.summingDouble(totGstQty -> totGstQty.getGstQuantity()));
				totTaxableValue = transferItemsList.stream().collect(Collectors.summingDouble(
						totPaisa -> Double.valueOf(totPaisa.getTaxableUnit() * totPaisa.getGstQuantity())));
				totTaxablePaisa = transferItemsList.stream()
						.collect(Collectors.summingDouble(totPaisa -> Double.valueOf(totPaisa.getTaxablePaisa())));
				totIgstValue = transferItemsList.stream().collect(Collectors.summingDouble(totPaisa -> Double.valueOf(
						(totPaisa.getTaxableUnit() * totPaisa.getTaxPercentage() / 100) * totPaisa.getQuantity())));
				totIgstPaisa = transferItemsList.stream()
						.collect(Collectors.summingDouble(totPaisa -> Double.valueOf(totPaisa.getIgstTotalPaisa())));
				netTotalValue = transferItemsList.stream()
						.collect(Collectors.summingDouble(
								totPaisa -> Double.valueOf((totPaisa.getTaxableUnit() * totPaisa.getGstQuantity())
										+ (totPaisa.getTaxableUnit() * totPaisa.getTaxPercentage() / 100)
												* totPaisa.getQuantity())));
				netTotalPaisa = transferItemsList.stream()
						.collect(Collectors.summingDouble(totPaisa -> Double.valueOf(totPaisa.getTotalPaisa())));
			}

		}

		log.info("<==== Ends createStockTransferInvoice ====>");
		return STOCK_TRANSFER_INVOICE;
	}

	/**
	 * @param qrCode
	 * @return
	 */
	private ItemQRCodeDetailsDTO getItemQrcodeDetails(String qrCode) {
		log.info("Start getItemQrcodeDetails Method======>" + qrCode);
		ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = null;
		try {
			BaseDTO baseDTO = new BaseDTO();
			/**
			 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller
			 * 
			 * StockTransferPWHController.java
			 */
			String URL = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stock/transfer/product/warehouse/getqrcodedetails/" + qrCode;
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Item Qr code details get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				itemQRCodeDetailsDTO = mapper.readValue(jsonResponse, ItemQRCodeDetailsDTO.class);

			}

		} catch (Exception ex) {
			log.error("Error in getItemQrcodeDetails Method", ex);
		}
		log.info("Start getItemQrcodeDetails Method======>");
		return itemQRCodeDetailsDTO;
	}

	public void loadCircleMaster() {
		log.info("loadCircleMaster Method Start===>");
		try {
			circleMasterList = commonDataService.loadCircles();
			log.info("Circle List Size" + circleMasterList.size());
		} catch (Exception ex) {
			log.error("Error in loadCircleMaster Method", ex);
		}
		log.info("loadCircleMaster Method End===>");
	}

	public List<SupplierMaster> loadSupplierMaster(String query) {

		try {
			log.info(".......DistributionBean loadProPlanDistrict..............");

			String url = AppUtil.getPortalServerURL() + "/supplier/master/loadsupplierlistautocomplete/" + query;
			log.info("getNotficationNumber==>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
				log.info("supplierMasterList size==> " + supplierMasterList.size());
			} else {
				supplierMasterList = new ArrayList<>();
			}
			return supplierMasterList;
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return supplierMasterList;
	}

	public String loadDate() {
		loadCircleMaster();
		clearReportData();
		return SOCITY_OUTWARD_ACK;
	}

	public void getAllPurchaseInvoiceListBySupplierId() {
		log.info("getAllPurchaseInvoiceListBySupplierId method is Start");
		try {
			log.info("Supplier id=" + supplierMaster.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societyinvoice/getpurchaseInvoiceList/"
					+ supplierMaster.getId();
			log.info("URL=" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				purchaseInvoiceList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseInvoice>>() {
				});
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("getAllPurchaseInvoiceListBySupplierId method is End");
	}

	public void getAcknowledgementReportDetails() {
		log.info("getAcknowledgementReportDetails Method Start====>");
		try {
			stockOutwardAcknowledgementReportDTO.setSupplierId(supplierMaster.getId());
			stockOutwardAcknowledgementReportDTO.setSupplierCode(supplierMaster.getCode());
			stockOutwardAcknowledgementReportDTO.setSupplierName(supplierMaster.getName());
			stockOutwardAcknowledgementReportDTO.setInvoiceNumber(purchaseInvoice.getInvoiceNumber());
			stockOutwardAcknowledgementReportDTO.setInvoiceDate(purchaseInvoice.getInvoiceDate());
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stock/transfer/product/warehouse/getackdetails";
			log.info("URL=" + url);
			BaseDTO baseDTO = httpService.post(url, stockOutwardAcknowledgementReportDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				stockOutwardAcknowledgementReportDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<StockOutwardAcknowledgementReportDTO>>() {
						});
			}
			if (!stockOutwardAcknowledgementReportDTOList.isEmpty()
					&& stockOutwardAcknowledgementReportDTOList.get(0) != null) {
				inwordNumber = stockOutwardAcknowledgementReportDTOList.get(0).getInwordNumber();
				inwardDate = stockOutwardAcknowledgementReportDTOList.get(0).getInwardDate();
			}
			stockOutwardAcknowledgementReportDTO.setInwardDate(inwardDate);
			stockOutwardAcknowledgementReportDTO.setInwordNumber(inwordNumber);
			DecimalFormat df = new DecimalFormat("#,###,##0.00");
			/*
			 * Double ppValues = stockOutwardAcknowledgementReportDTOList.stream()
			 * .collect(Collectors.summingDouble(ppval -> ppval.getPpValue()));
			 */
			Double ppValues = 0.0;
			Double rpValues = 0.0;
			for (StockOutwardAcknowledgementReportDTO sac : stockOutwardAcknowledgementReportDTOList) {
				if (sac.getPpValue() != null) {
					ppValues = ppValues + sac.getPpValue();
				}
				if (sac.getRpValue() != null) {
					rpValues = rpValues + sac.getRpValue();
				}
			}
			ppValue = df.format(ppValues);
			/*
			 * Double rpValues = stockOutwardAcknowledgementReportDTOList.stream()
			 * .collect(Collectors.summingDouble(rpval -> rpval.getPpValue()));
			 */
			rpValue = df.format(rpValues);
			List<Map<String, Object>> response = new ArrayList<Map<String, Object>>();
			for (StockOutwardAcknowledgementReportDTO soar : stockOutwardAcknowledgementReportDTOList) {
				Map<String, Object> stockOutwardAckReport = new LinkedHashMap<String, Object>();
				stockOutwardAckReport.put("Product Variety Code / Name",
						soar.getProductVarityCode().concat("/").concat(soar.getProductVarityName()));
				if (soar.getQuantity() != null) {
					stockOutwardAckReport.put("Quantity", soar.getQuantity());
				} else {
					stockOutwardAckReport.put("Quantity", 0);
				}
				if (soar.getPpRate() != null) {
					stockOutwardAckReport.put("PP Rate", soar.getPpRate());
				} else {
					stockOutwardAckReport.put("PP Rate", 0);
				}
				if (soar.getPpValue() != null) {
					stockOutwardAckReport.put("PP Value", soar.getPpValue());
				} else {
					stockOutwardAckReport.put("PP Value", 0);
				}
				if (soar.getRpRate() != null) {
					stockOutwardAckReport.put("RP Rate", soar.getRpRate());
				} else {
					stockOutwardAckReport.put("RP Rate", 0);
				}
				if (soar.getRpValue() != null) {
					stockOutwardAckReport.put("RP Value", soar.getRpValue());
				} else {
					stockOutwardAckReport.put("RP Value", 0);
				}
				response.add(stockOutwardAckReport);
			}
			totalList = new ArrayList<String>();
			totalList.add(ppValue);
			totalList.add(" ");
			totalList.add(rpValue);
			generatePDF(response);
		} catch (Exception ex) {
			log.error("error in getAcknowledgementReportDetails", ex);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("getAcknowledgementReportDetails Method End====>");
	}

	private void generatePDF(List<Map<String, Object>> response) {
		log.info("===========START StockOutwardPWHBean.generatePDF =============");
		try {
			log.info("===========response is=============" + response.toString());
			String content = commonDataService.getTemplateDetailsforPortal(TemplateCode.REPORT_TEMPLATE.toString(),
					appPreference);
			String finalContent = VelocityStringUtil.getFinalContent("", content, null);
			String htmlData = generateHtml(stockOutwardAcknowledgementReportDTO, response, finalContent);
			downloadPath = commonDataService.getAppKeyValueforPortal(AppConfigEnum.REPORT_PDF_DOWNLOAD_PATH.toString());
			log.info("===========downloadPath is=============" + downloadPath.toString());
			downloadFileName = AppUtil.getReportFileName(ReportNames.SOCIETY_OUTWARD_ACKNOWLEDGEMENT_REPORT, "pdf");
			misPdfUtil.createPDF(htmlData, downloadPath + "/" + downloadFileName, loginBean);
		} catch (Exception e) {
			log.info("===========Exception Occured in StockOutwardPWHBean.generatePDF =============");
			log.info("Exception is =============" + e.getMessage());
		}
		log.info("===========END StockOutwardPWHBean.generatePDF =============");
	}

	private String generateHtml(StockOutwardAcknowledgementReportDTO stockOutwardAcknowledgementReportDTO,
			List<Map<String, Object>> response, String finalContent) {
		log.info("===========START StockOutwardPWHBean.generateHtml =============");
		try {
			Long index = (long) 0;
			Long footerIndex = (long) 0;
			DateFormat dateFormat = AppUtil.DATE_FORMAT;
			StringBuffer finalTableData = new StringBuffer(110);
			StringBuffer finalHeaderData = new StringBuffer(110);
			StringBuffer finalFooterData = new StringBuffer(110);
			String date = "";
			String inwardDate = "";
			if (stockOutwardAcknowledgementReportDTO.getInvoiceDate() != null) {
				date = dateFormat.format(stockOutwardAcknowledgementReportDTO.getInvoiceDate());
			}
			if (stockOutwardAcknowledgementReportDTO.getInwardDate() != null) {
				inwardDate = dateFormat.format(stockOutwardAcknowledgementReportDTO.getInwardDate());
			}

			String filterTypeData = "<tr> " + "<td style='width: 10%'>Society Code</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>"
					+ stockOutwardAcknowledgementReportDTO.getSupplierCode() + "</b></td> "
					+ "<td style='width: 10%'>Society Name</td> " + "<td align='right' style='width: 1%'>:</td> "
					+ "<td style='width: 24%'><b>" + stockOutwardAcknowledgementReportDTO.getSupplierName()
					+ "</b></td>" + "</tr>";

			filterTypeData = filterTypeData + "<tr> " + "<td style='width: 10%'>Invoice Numbe</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>"
					+ stockOutwardAcknowledgementReportDTO.getInvoiceNumber() + "</b></td> "
					+ "<td style='width: 10%'>Invoice Date</td> " + "<td align='right' style='width: 1%'>:</td> "
					+ "<td style='width: 24%'><b>" + date + "</b></td>" + "</tr>";

			filterTypeData = filterTypeData + "<tr> " + "<td style='width: 10%'>Inward Reference Number</td> "
					+ "<td align='right' style='width: 1%'>:</td> " + "<td style='width: 22%'><b>"
					+ stockOutwardAcknowledgementReportDTO.getInwordNumber() + "</b></td> "
					+ "<td style='width: 10%'>Inward Date</td> " + "<td align='right' style='width: 1%'>:</td> "
					+ "<td style='width: 24%'><b>" + inwardDate + "</b></td>" + "</tr>";

			Set<String> headerNames = new HashSet<String>();
			for (Map<String, Object> map : response) {
				StringBuffer list = new StringBuffer(110);
				index++;
				headerNames = map.keySet();
				String beginTr = "<tr> " + "<td  class='bor-left'>" + index + "</td>";
				list.append(beginTr);
				Iterator<String> setItr = headerNames.iterator();
				while (setItr.hasNext()) {
					String keyString = setItr.next();
					String td = new String();
					if (isDecimal(map.get(keyString))) {
						td = "<td class='text-right'>" + getFormattedValue(map.get(keyString)) + "</td>";
					} else {
						td = "<td class='text-left'>" + (map.get(keyString)) + "</td>";
					}
					list.append(td);

				}
				String endTr = "</tr>";
				list.append(endTr);
				finalTableData.append(list);
			}
			String headerData = "<th style='width: 3%' class='text-center bor-left'>#</th>";
			finalHeaderData.append(headerData);
			Iterator<String> setItr = headerNames.iterator();
			while (setItr.hasNext()) {
				String keyString = setItr.next();
				headerData = "<th>" + keyString + "</th>";
				finalHeaderData.append(headerData);
			}
			Iterator<String> footerItr = headerNames.iterator();
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("PP Value")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + footerIndex + "'>Total</td> ";
					tfoot.append(tr);
					for (String totalCount : totalList) {
						String td = new String();

						td = "<td class='text-center'>" + totalCount + "</td>";

						tfoot.append(td);
					}
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}

			}

			finalContent = finalContent.replace("$filterType", filterTypeData);
			finalContent = finalContent.replace("$columnData", finalHeaderData.toString());
			finalContent = finalContent.replace("$reportListData", finalTableData.toString());
			finalContent = finalContent.replace("$reportName", ReportNames.SOCIETY_OUTWARD_ACKNOWLEDGEMENT_REPORT);
			finalContent = finalContent.replace("$footerData", finalFooterData.toString());
			log.info("===========END StockOutwardPWHBean.generateHtml =============");

		} catch (Exception e) {
			log.info("===========Exception Occured in StockOutwardPWHBean.generateHtml =============");
			log.info("===========Exception is =============" + e.getMessage());
		}

		return finalContent;
	}

	/**
	 * @param obj
	 * @return
	 */
	private String getFormattedValue(Object obj) {
		String value = null;
		try {

			// if (isDecimal(String.valueOf(obj))) {
			value = String.format("%.2f", (double) obj);
			// } else {
			// return (String) obj;
			// }

		} catch (Exception ex) {
			value = obj != null ? String.valueOf(obj) : null;
		}
		return value;
	}

	/**
	 * @param value
	 * @return
	 */
	private boolean isDecimal(Object value) {
		boolean isDecimal = true;
		try {
			Double.parseDouble(String.valueOf(value));
		} catch (Exception ex) {
			isDecimal = false;
		}
		return isDecimal;
	}

	public void downloadPdf() {
		InputStream input = null;
		try {
			File files = new File(downloadPath + "/" + downloadFileName);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			pdfFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
					files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void clearReportData() {
		purchaseInvoice = new PurchaseInvoice();
		supplierMaster = null;
		stockOutwardAcknowledgementReportDTOList = new ArrayList<StockOutwardAcknowledgementReportDTO>();
		ppValue = null;
		rpValue = null;
		inwordNumber = "";
		inwardDate = null;
	}

	public void clearTransportDetails() {
		if (!stockTransfer.getWaybillAvailable()) {
			stockTransfer.setWaybillNumber("");
		}
		if (stockTransfer.getTransportChargeAvailable() != null && !stockTransfer.getTransportChargeAvailable()) {
			stockTransfer.setTransportChargeType(null);
			stockTransfer.setTransportChargeAmount(null);
		}
	}

	public void meterQtyEdit(StockTransferItemsBundles bundles) {
		log.info("Startmethod meterQtyEdit" + bundles.getQrCode());
		log.info("Startmethod meterQtyEdit" + bundles.getQuantity());
		try {
			log.info("meterQtyEdit", bundles);

			ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();
			itemQRCodeDetailsDTO = getItemQrcodeDetails(bundles.getQrCode());

			if (itemQRCodeDetailsDTO == null) {
				itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();
			}
			StockTransferItems items = findItemByQRCode(itemQRCodeDetailsDTO,
					stockTransfer.getFromEntityMaster().getId(), isPagePWH);

			if (stockTransfer.getStockTransferItemsList() != null
					&& !stockTransfer.getStockTransferItemsList().isEmpty()) {
				totalDispatchedQty = 0D;
				stockTransferItemsDisplayList.forEach(itemsObj -> {
					if (itemsObj.getProductVarietyMaster() != null && items.getProductVarietyMaster() != null) {
						if (itemsObj.getProductVarietyMaster().getCode()
								.equals(items.getProductVarietyMaster().getCode())) {
							totalDispatchedQty = itemsObj.getStockTransferItemsBundlesList().stream()
									.filter(k -> k.getQrCode() != null && bundles.getQrCode().equals(k.getQrCode()))
									.collect(Collectors.summingDouble(StockTransferItemsBundles::getQuantity));
						}
					}
				});
				if (totalDispatchedQty.equals(items.getDispatchedQty())
						|| totalDispatchedQty >= items.getDispatchedQty()) {
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_NOT_AVAILABLE.getCode());
					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);
					bundles.setQuantity(bundles.getQuantity());
					RequestContext.getCurrentInstance().execute("PF('dlgItemsDetail').hide();");
					return;
				} else {
					stockTransfer.getStockTransferItemsList().stream().forEach(item -> {
						if (item.getQrCode().equals(bundles.getQrCode())) {
							item.setDispatchedQty(bundles.getQuantity());
						} else {
							log.info("QrCode Not Match");
						}
					});

					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);

					for (StockTransferItems itemsObj : stockTransferItemsDisplayList) {
						itemsObj.getStockTransferItemsBundlesList()
								.removeIf(b -> b.getQrCode().equalsIgnoreCase(bundles.getQrCode()));
					}
				}

				bundleNumberSet = new HashSet<String>();
				stockTransferItemsDisplayList = new ArrayList<>();
				for (StockTransferItems stockTransferItems : stockTransfer.getStockTransferItemsList()) {
					addItemToDisplayList(stockTransferItems);
				}
			}
			RequestContext.getCurrentInstance().execute("PF('dlgItemsDetail').hide();");
		} catch (Exception exp) {
			log.error("exception in meterQtyEdit method------- ", exp);
		}
	}

}