/**
 * 
 */
package in.gov.cooptex.operation.purchaseorder.ui;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PurchaseOrderDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.OrderStatus;
import in.gov.cooptex.operation.model.OrderItemTax;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Praveen
 *
 */
@Log4j2
@Scope("session")
@Service("purchaseOrderItemBean")
public class PurchaseOrderItemBean {

	private final String CREATE_PURCHASE_ORDERITEM_PAGE = "/pages/operation/createPurchaseOrderItem.xhtml?faces-redirect=true;";
	private final String LIST_PURCHASE_ORDERITEM_PAGE = "/pages/operation/listPurchaseOrderItem.xhtml?faces-redirect=true;";
	private final String VIEW_PURCHASE_ORDERITEM_PAGE = "/pages/operation/viewPurchaseOrderItem.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Long supplierTypeId;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean poSearchBtn = false;

	@Getter
	@Setter
	Boolean purchaseItemFlag = true;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeList;

	@Getter
	@Setter
	List<SupplierMaster> supplierList;

	private String serverURL = null;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	List<Quotation> quotationNumberList;

	@Getter
	@Setter
	List<UomMaster> uomMasterLists;

	@Getter
	@Setter
	List<EntityMaster> entityMasterAllList;

	@Getter
	@Setter
	List<ProductVarietyTax> productVarietyTaxs;

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	Quotation quotation;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	private PurchaseOrderItems purchaseOrderItems;

	@Getter
	@Setter
	OrderItemTax orderItemTax;

	@Getter
	@Setter
	PurchaseOrder purchaseOrder;

	@Getter
	@Setter
	List<PurchaseOrder> purchaseOrderList;

	@Getter
	@Setter
	LazyDataModel<PurchaseOrder> purchaseOrderLazyModelList;

	@Getter
	@Setter
	List<PurchaseOrderItems> purchaseOrderItemList;

	@Getter
	@Setter
	List<OrderItemTax> orderItemTaxList;

	@Getter
	@Setter
	List<PurchaseOrderDTO> poTaxSummaryList;

	@Getter
	@Setter
	List<Object> statusValues;

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String purchaseOrderByQuotation;

	@Getter
	@Setter
	String orderType = "";

	@Getter
	@Setter
	String discountType;

	@Getter
	@Setter
	String discountApplicable;

	@Getter
	@Setter
	Double unitRateTotal;

	@Getter
	@Setter
	Double itemAmountTotal;

	@Getter
	@Setter
	Double netAmountTotal;

	@Getter
	@Setter
	Double discountValueTotal;

	@Getter
	@Setter
	Double taxValueTotal;

	@Getter
	@Setter
	Double taxValue;

	@Getter
	@Setter
	Double taxPercent;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	CircleMaster selectedCircleMaster;

	Double itemTotal = 0.0;

	Double itemdiscount;

	@PostConstruct
	public void init() {
		getAllSupplierTypeList();
		getUomMasterList();
		loadStatusValues();
		loadLazyPurchaseOrderList();
		getEntityMasterList();
		purchaseOrderItems = new PurchaseOrderItems();
		purchaseOrder = new PurchaseOrder();
		orderItemTax = new OrderItemTax();
		purchaseOrderItemList = new ArrayList<PurchaseOrderItems>();
		poTaxSummaryList = new ArrayList<PurchaseOrderDTO>();
		circleMasterList = new ArrayList<CircleMaster>();
		selectedCircleMaster = new CircleMaster();
	}

	public String showAllPurchaseOrderList() {
		loadStatusValues();
		loadLazyPurchaseOrderList();
		return LIST_PURCHASE_ORDERITEM_PAGE;
	}

	private void loadStatusValues() {

		Object[] statusArray = OrderStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
//		for (Object status : statusArray) {
//			statusValues.add(status);
//		}
		statusValues.add("SUBMITTED");

	}

	public void getAllSupplierTypeList() {
		try {
			supplierTypeList = new ArrayList<SupplierTypeMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/suppliertypemaster/getall";
			log.info(" PurchaseOrderItem Bean ==>>> getAllSupplierTypeList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean . getAllSupplierTypeList ------- >>>>");
	}

	public void getSupplierMasterListBySupplierTypeId() {
		try {
			if (supplierTypeId == null) {
				supplierTypeId = getSupplierTypeMaster().getId();
			}
			supplierList = new ArrayList<SupplierMaster>();
			if (supplierTypeId != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/supplier/master/getBySupplierType/" + supplierTypeId;
				// String url = SERVER_URL + "/supplier/master/getSupplierByTypeAndCircle/" +
				// supplierTypeId+"/"+selectedCircleMaster.getId();
				log.info(" PurchaseOrderItem Bean ==>>> getSupplierMasterListBySupplierTypeId URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean... getSupplierMasterListBySupplierTypeId ------- >>>>");
	}

	public void purchaseOrderChange() {
		if (purchaseOrderByQuotation.equalsIgnoreCase("yes")) {
			// getAllQuotationList();
			// getQuotationList();
			getQuotationListBySupplierId();
			poSearchBtn = false;
			purchaseItemFlag = false;
		} else {
			quotationNumberList = new ArrayList<Quotation>();
			poSearchBtn = true;
			purchaseItemFlag = true;
		}
		log.info(" Purchase Order Based Quotation ===>>> " + purchaseItemFlag);
	}

	public void clearPurchaseOrder() {
		purchaseOrderByQuotation = "";
		quotationNumberList = new ArrayList<Quotation>();
		supplierList = new ArrayList<SupplierMaster>();
		purchaseItemFlag = true;
		supplierTypeMaster = null;
		purchaseOrder.setSupplierMaster(null);
		purchaseOrderByQuotation = null;
		purchaseOrderItemList = new ArrayList<>();
		selectedCircleMaster = new CircleMaster();
	}

	public void getAllQuotationList() {
		try {
			quotationNumberList = new ArrayList<Quotation>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/getAll";
			log.info(" PurchaseOrderItem Bean ==>>> getAllQuotationList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				quotationNumberList = mapper.readValue(jsonResponse, new TypeReference<List<Quotation>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End..PurchaseOrderItem Bean... getAllQuotationList ------- >>>>");
	}

	public void getQuotationList() {
		try {
			quotationNumberList = new ArrayList<Quotation>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/getquotationlist";
			log.info(" PurchaseOrderItem Bean ==>>> getQuotationList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				quotationNumberList = mapper.readValue(jsonResponse, new TypeReference<List<Quotation>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End..PurchaseOrderItem Bean... getQuotationList ------- >>>>");
	}

	public void purchaseOrderTypeChange() {

		if (orderType.equalsIgnoreCase("purchase")) {
			log.info(" called purchase order type");
			getProductCategoryGrpList();
		} else {
			log.info(" called service order type");
			productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupList = new ArrayList<ProductGroupMaster>();
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void getProductCategoryGrpList() {
		try {
			productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/productCategoryGroup/getall";
			log.info(" START....PurchaseOrderItem Bean ==>>> getProductCategoryGrpList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productCategoryGroupList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductCategoryGroup>>() {
						});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean . getProductCategoryGrpList ------- >>>>");
	}

	public void getProductCategoryListByProductCategoryGRPId() {
		try {
			productCategoryList = new ArrayList<ProductCategory>();
			if (productCategoryGroup != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/quotation/productCategory/getByProductCategoryGrp/"
						+ productCategoryGroup.getId();
				log.info(" PurchaseOrderItem Bean ==>>> getProductCategoryListByProductCategoryGRPId URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
					});
				}
				productGroupList.clear();
				productVarietyMasterList.clear();
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info(
				"<<<< ----------End PurchaseOrderItem Bean. getProductCategoryListByProductCategoryGRPId ------- >>>>");
	}

	public void getProductGroupListByProductCategoryId() {
		productGroupList.clear();
		productGroupList = new ArrayList<ProductGroupMaster>();
		try {
			log.info(
					"<<<< ----------START PurchaseOrderItem Bean . getProductGroupListByProductCategoryId ------- >>>>");
			if (productCategory != null) {
				log.info(" getProductGroupListByProductCategoryId ==>>> " + productCategory.getId());
				productGroupList = commonDataService.loadActiveProductGroups(productCategory);
			}
		} catch (Exception e) {
			log.error(
					" Exception occurred..PurchaseOrderItem Bean ==>> getProductGroupListByProductCategoryId... " + e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean . getProductGroupListByProductCategoryId ------- >>>>");
	}

	public void getProductVarietyListByProductGroupId() {
		try {
			log.info(" START PurchaseOrderItem Bean... getProductVarietyListByProductGroupId ");
			if (productGroupMaster != null) {
				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				log.info(" PurchaseOrderItem Bean ...getProductVarietyListByProductGroupId ==>>> "
						+ productGroupMaster.getId());
				productVarietyMasterList = commonDataService.loadActiveProductVarieties(productGroupMaster);
			}
		} catch (Exception e) {
			log.error("Exception occured ...PurchaseOrderItem Bean ===>>> .getProductVarietyListByProductGroupId.. ",
					e);
		}
		log.info(" END PurchaseOrderItem Bean ==>>getProductVarietyListByProductGroupId ");
	}

	public void getUomMasterList() {
		try {
			uomMasterLists = new ArrayList<UomMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/uomMaster/getAllUom";
			log.info(" PurchaseOrderItem Bean ==>>> getUomMasterList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				uomMasterLists = mapper.readValue(jsonResponse, new TypeReference<List<UomMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean ... getUomMasterList ------- >>>>");
	}

	public void getTaxListByProductVarietyTax() {
		try {
			Long productVarietyId = null;
			productVarietyTaxs = new ArrayList<ProductVarietyTax>();
			if (purchaseOrderItems != null) {
				productVarietyId = purchaseOrderItems.getProductVarietyMaster().getId();
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/quotation/productVarietyTax/getByProductVarietyMaster/" + productVarietyId;
				log.info(" PurchaseOrderItem Bean ==>>> getTaxListByProductVarietyTax URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productVarietyTaxs = mapper.readValue(jsonResponse, new TypeReference<List<ProductVarietyTax>>() {
					});
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean ... getTaxListByProductVarietyTax ------- >>>>");
	}

	/*** Get the entity master list here ***/
	public void getEntityMasterList() {
		try {
			entityMasterAllList = new ArrayList<EntityMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/purchaseOrderItemController/getAllEntityList";
			log.info(" PurchaseOrderItem Bean ==>>> getEntityMasterList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterAllList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End EntityMaster . getEntityMasterList ------- >>>>");
	}

	public void loadLazyPurchaseOrderList() {
		log.info("<===== Starts PurchaseOrderItem Bean.loadLazyPurchaseOrderList  ======>");
		String PURCHASEORDER_LIST_URL = SERVER_URL + "/purchaseOrderItemController/getallPurchaseOrderListlazy";

		purchaseOrderLazyModelList = new LazyDataModel<PurchaseOrder>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<PurchaseOrder> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for PurchaseOrder List=============>" + PURCHASEORDER_LIST_URL);
					BaseDTO response = httpService.post(PURCHASEORDER_LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						purchaseOrderList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrder>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyQuotationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return purchaseOrderList;
			}

			@Override
			public Object getRowKey(PurchaseOrder res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PurchaseOrder getRowData(String rowKey) {
				try {
					for (PurchaseOrder purchaseOrderObj : purchaseOrderList) {
						if (purchaseOrderObj.getId().equals(Long.valueOf(rowKey))) {
							purchaseOrder = purchaseOrderObj;
							return purchaseOrderObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Quotation Bean.loadLazyQuotationList List ======>");
	}

	public String addPurchaseOrderItemDtls() {
		log.info(" Called.. PurchaseOrderItem Bean ====>>> addPurchaseOrderItemDtls");
		try {
			if (poTaxSummaryList == null || poTaxSummaryList.size() == 0) {
				Util.addWarn("Please add purchase order item tax details");
				return null;
			}

			if (purchaseOrderItems != null) {
				log.info(" START PurchaseOrderItem Bean ====>>> addPurchaseOrderItemDtls");
				PurchaseOrderItems purchaseOrderItemsObj = new PurchaseOrderItems();
				purchaseOrderItemsObj.setProductVarietyMaster(purchaseOrderItems.getProductVarietyMaster());
				purchaseOrderItemsObj.setItemName(purchaseOrderItems.getItemName());
				purchaseOrderItemsObj.setItemDescription(purchaseOrderItems.getItemDescription());
				purchaseOrderItemsObj.setUomMaster(purchaseOrderItems.getUomMaster());
				purchaseOrderItemsObj.setItemQty(purchaseOrderItems.getItemQty());
				purchaseOrderItemsObj.setUnitRate(purchaseOrderItems.getUnitRate());
				if (purchaseOrderItems.getUnitRate() != null && purchaseOrderItems.getItemQty() != null) {
					purchaseOrderItemsObj
							.setItemAmount((purchaseOrderItems.getUnitRate()) * (purchaseOrderItems.getItemQty()));
				} else {
					purchaseOrderItemsObj.setItemAmount(0.0);
				}
				purchaseOrderItemsObj.setDiscountPercent(purchaseOrderItems.getDiscountPercent());
				purchaseOrderItemsObj.setDiscountValue(purchaseOrderItems.getDiscountValue());
				purchaseOrderItemsObj.setTaxPercent(purchaseOrderItems.getTaxPercent());
				if (taxValue != null) {
					purchaseOrderItemsObj.setTaxValue(taxValue);
				}
				purchaseOrderItemsObj.setItemTotal(purchaseOrderItems.getItemTotal());
				if (poTaxSummaryList != null) {
					orderItemTaxList = new ArrayList<OrderItemTax>();
					for (PurchaseOrderDTO purchaseOrderDTO : poTaxSummaryList) {
						OrderItemTax itemTax = new OrderItemTax();
						itemTax.setProductVarietyTax(purchaseOrderDTO.getOrderItemTax().getProductVarietyTax());
						itemTax.setTaxValue(purchaseOrderDTO.getOrderItemTax().getTaxValue());
						orderItemTaxList.add(itemTax);
					}
					purchaseOrderItemsObj.setOrderItemTaxList(orderItemTaxList);
				}
				purchaseOrderItemList.add(purchaseOrderItemsObj);
				log.info("addPurchaseOrderItemDtls ===>>> added successfully");
				sumPurchaseOrderItemDetails();
				/*** Clear all Purchase order Item Details ***/
				clearPurchaseOrderDtlsTable();

			}
		} catch (Exception e) {
			log.error(" Exception occurred at addPurchaseOrderItemDtls.. " + e);
		}
		return null;
	}

	public void clearPurchaseOrderDtlsTable() {
		log.info(" Start...  clearPurchaseOrderDtlsTable");
		purchaseOrderItems = new PurchaseOrderItems();
		orderItemTax = new OrderItemTax();
		orderType = "";

		productCategoryGroup = new ProductCategoryGroup();
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();

		productCategory = new ProductCategory();
		productCategoryList = new ArrayList<ProductCategory>();

		productGroupMaster = new ProductGroupMaster();
		productGroupList = new ArrayList<ProductGroupMaster>();

		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

		// uomMasterLists = new ArrayList<>();

		poTaxSummaryList = new ArrayList<PurchaseOrderDTO>();

		productVarietyTaxs = new ArrayList<ProductVarietyTax>();

		taxPercent = 0.0;
		taxValue = 0.0;
		log.info(" End...  clearPurchaseOrderDtlsTable");
	}

	public void sumPurchaseOrderItemDetails() {
		log.info(" Called.. PurchaseOrder Bean ===>>> sumPurchaseOrderItemDetails");
		if (purchaseOrderItemList != null && purchaseOrderItemList.size() > 0) {
			log.info(" START PurchaseOrder Bean ===>>> sumPurchaseOrderItemDetails");

			unitRateTotal = purchaseOrderItemList.stream().filter(p -> p.getUnitRate() != null)
					.collect(Collectors.summingDouble(PurchaseOrderItems::getUnitRate));

			itemAmountTotal = purchaseOrderItemList.stream().filter(p -> p.getItemAmount() != null)
					.collect(Collectors.summingDouble(PurchaseOrderItems::getItemAmount));

			discountValueTotal = purchaseOrderItemList.stream().filter(p -> p.getDiscountValue() != null)
					.collect(Collectors.summingDouble(PurchaseOrderItems::getDiscountValue));

			netAmountTotal = purchaseOrderItemList.stream().filter(p -> p.getItemTotal() != null)
					.collect(Collectors.summingDouble(PurchaseOrderItems::getItemTotal));

			taxValueTotal = purchaseOrderItemList.stream().filter(p -> p.getTaxValue() != null)
					.collect(Collectors.summingDouble(PurchaseOrderItems::getTaxValue));

			log.info(" Unit Rate Total Count ===>>>> " + unitRateTotal);
			log.info(" Item Amount Total Count ===>>>> " + itemAmountTotal);
			log.info(" ENDED.. PurchaseOrder Bean ===>>> sumPurchaseOrderItemDetails");
		} else {
			unitRateTotal = 0.0;
			itemAmountTotal = 0.0;
			discountValueTotal = 0.0;
			netAmountTotal = 0.0;
			taxValueTotal = 0.0;
		}
	}

	public String addTaxSummaryListDtls() {
		try {
			log.info(" Called... PurchaseOrder Bean ==>>> addTaxSummaryDtls");
			if (orderItemTax != null) {
				PurchaseOrderDTO orderDTO = new PurchaseOrderDTO();
				orderDTO.getOrderItemTax().setProductVarietyTax(orderItemTax.getProductVarietyTax());
				orderDTO.getOrderItemTax().setTaxValue(orderItemTax.getTaxValue());
				orderDTO.getPurchaseOrderItems().setTaxPercent(purchaseOrderItems.getTaxPercent());
				orderDTO.setTaxPercentage(purchaseOrderItems.getTaxPercent());
				ProductVarietyTax varietyTax = orderItemTax.getProductVarietyTax();
				if (varietyTax != null) {
					orderDTO.setTaxType(varietyTax.getTaxMaster().getTaxCode());
				} else {
					orderDTO.setTaxType("");
				}
				poTaxSummaryList.add(orderDTO);
				sumTaxSummaryDtls();
				purchaseOrderItems.setTaxPercent(0.0);
				orderItemTax.setTaxValue(0.0);
			}
			log.info(" End... PurchaseOrder Bean ==>>> addTaxSummaryDtls");
		}

		catch (Exception e) {
			log.error(" Errro at PurchaseOrderItem Bean ==>>>> addTaxSummaryDtls... " + e);
		}
		return null;
	}

	public void sumTaxSummaryDtls() {
		double taxValue = 0.0;
		double taxPercent = 0.0;
		if (poTaxSummaryList != null && poTaxSummaryList.size() > 0) {
			for (PurchaseOrderDTO poDto : poTaxSummaryList) {
				if (poDto.getOrderItemTax().getTaxValue() != null) {
					taxValue += poDto.getOrderItemTax().getTaxValue();
				}
				if (poDto.getPurchaseOrderItems().getTaxPercent() != null) {
					taxPercent += poDto.getPurchaseOrderItems().getTaxPercent();
				}
			}
			this.taxValue = taxValue;
			this.taxPercent = taxPercent;
		} else {
			this.taxPercent = 0.0;
			this.taxValue = 0.0;
		}
	}

	public void generatePurchaseOrderItemByQuotation(Quotation quotation) {
		try {
			log.info(" PurchaseOrderItem Bean....Called ==>> generatePurchaseOrderItemByQuotation");
			BaseDTO baseDTO = new BaseDTO();
			purchaseOrderItemList = new ArrayList<PurchaseOrderItems>();
			if (quotation.getId() != null) {
				String url = SERVER_URL + "/purchaseOrderItemController/poItemByQuotation/" + quotation.getId();
				log.info(" PurchaseOrderItem Bean ==>>> generatePurchaseOrderItemByQuotation URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					purchaseOrderItemList = mapper.readValue(jsonResponse,
							new TypeReference<List<PurchaseOrderItems>>() {
							});
					sumPurchaseOrderItemDetails();
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ---------PurchaseOrderItem Bean..End... generateItemByQuotation ------- >>>>");
	}

	public void editByPurchaseOrderId() {
		try {
			log.info(" PurchaseOrderItem Bean....Called ==>> editByPurchaseOrderId");
			BaseDTO baseDTO = new BaseDTO();
			purchaseOrderList = new ArrayList<PurchaseOrder>();
			String url = SERVER_URL + "/purchaseOrderItemController/getPurchaseOrderById/" + purchaseOrder.getId();
			log.info(" PurchaseOrderItem Bean ==>>> editByPurchaseOrderId URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrder = mapper.readValue(jsonResponse, new TypeReference<PurchaseOrder>() {
				});
				if (action.equalsIgnoreCase("EDIT")) {
					if (purchaseOrder != null) {
						supplierTypeMaster = purchaseOrder.getSupplierMaster().getSupplierTypeMaster();
						supplierTypeId = purchaseOrder.getSupplierMaster().getSupplierTypeMaster().getId();
						getSupplierMasterListBySupplierTypeId();
						if (purchaseOrder.getQuotation() != null && purchaseOrder.getQuotation().getId() != null) {
							getAllQuotationList();
							setPurchaseOrderByQuotation("Yes");
							poSearchBtn = true;
							purchaseItemFlag = false;
						} else {
							setPurchaseOrderByQuotation("No");
						}
					}
				} else {
					if (purchaseOrder.getQuotation() != null && purchaseOrder.getQuotation().getId() != null) {
						setPurchaseOrderByQuotation("Yes");
					} else {
						setPurchaseOrderByQuotation("No");
					}
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ---------PurchaseOrderItem Bean..End... editByPurchaseOrderId ------- >>>>");
	}

	public void purchaseOrderItemAndTaxDtls() {
		if (purchaseOrder != null) {
			List<PurchaseOrderItems> purchaseOrderItems1 = purchaseOrder.getPurchaseOrderItemsList();
			purchaseOrderItemList = purchaseOrderItems1;
			sumPurchaseOrderItemDetails();
		}
	}

	public String submitPurchaseOrder() {

		log.info(" called..PurcahseOrderItem Bean ===>>> submitPurchaseOrder");
		try {
			if (purchaseOrderItemList != null && purchaseOrderItemList.size() > 0) {
				purchaseOrder.setMaterialValue(unitRateTotal);
				purchaseOrder.setTotalValue(itemAmountTotal);
				purchaseOrder.setNetValue(netAmountTotal);
				purchaseOrder.setStatus(OrderStatus.SUBMITTED);
				purchaseOrder.setPurchaseOrderItemsList(purchaseOrderItemList);

				String PURCHASE_ORDER_CREATE = SERVER_URL + "/purchaseOrderItemController/create";

				log.info(" PurchaseOrderItem Bean ====> submitPurchaseOrder ==>> URL " + PURCHASE_ORDER_CREATE);
				BaseDTO response = httpService.post(PURCHASE_ORDER_CREATE, purchaseOrder);
				if (response != null && response.getStatusCode() == 0) {
					if (action.equalsIgnoreCase("Create")) {
						log.info(" Purchase Order Bean :: successfully saved..");
						errorMap.notify(ErrorDescription.PURCHASE_ORDERS_SAVE_SUCCESS.getCode());
					} else {
						log.info(" Purchase Order Bean :: successfully updated..");
						errorMap.notify(ErrorDescription.PURCHASE_ORDER_UPDATE_SUCCESS.getCode());
					}
				} else if (response != null && response.getStatusCode() != 0) {
					if (action.equalsIgnoreCase("Create")) {
						log.info(" Purchase Order Bean :: Save failed");
						errorMap.notify(ErrorDescription.PURCHASE_ORDER_SAVE_FAILED.getCode());
					} else {
						log.info(" Purchase Order Bean :: update failed");
						errorMap.notify(ErrorDescription.PURCHASE_ORDER_UPDATE_FAILED.getCode());
					}
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					log.info("Purchase Order save failed .....");
				}
			} else {
				log.info(" Purchase Order Item List is EMPTY ====>>>> Quotation ITEM...");
				errorMap.notify(ErrorDescription.PURCHASE_ORDER_ITEM_EMPTY.getCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update Purchase Order .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return showAllListForPurchaseOrder();

	}

	public void removePoTaxSummaryList(PurchaseOrderDTO purchaseOrderDTO) {
		if (poTaxSummaryList != null) {
			poTaxSummaryList.remove(purchaseOrderDTO);
			sumTaxSummaryDtls();
		}
	}

	public void removePurchaseOrderItemDtls(PurchaseOrderItems orderItem) {
		if (purchaseOrderItemList != null) {
			purchaseOrderItemList.remove(orderItem);
			if (action.equalsIgnoreCase("Edit")) {
				deletePurchaseOrderItems(orderItem);
			}
			sumPurchaseOrderItemDetails();
		}
	}

	public void deletePurchaseOrderItems(PurchaseOrderItems orderItem) {
		Long purchaseOrderItemId = null;
		purchaseOrderItemId = orderItem.getId();
		try {
			if (purchaseOrderItemId != null) {
				log.info(
						" Called ..PurchaseOrderItem Bean. deletePurchaseOrderItems  ID==  >>> " + purchaseOrderItemId);
				BaseDTO response = httpService.delete(
						SERVER_URL + "/purchaseOrderItemController/deletePurchaseOrderItem/" + purchaseOrderItemId);
				if (response != null && response.getStatusCode() == 0) {
					log.info(" PurchaseOrderItem Item deleted successfully..");
					errorMap.notify(ErrorDescription.PURCHASE_ORDER_ITEM_DELETE_SUCCESS.getErrorCode());
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while .PurchaseOrderItem Bean. deletePurchaseOrderItems  Grid ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends .PurchaseOrderItem Bean. deletePurchaseOrderItems  ===========>");
	}

	public String purchaseOrderListPageAction() {
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("called add PurchaseOrder page");
				resetAllPurchaseOrderForms();
				purchaseOrder = new PurchaseOrder();
				circleMasterList = commonDataService.loadCircles();
				getAllSupplierTypeList();
				getUomMasterList();
				getEntityMasterList();
				return CREATE_PURCHASE_ORDERITEM_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("called view PurchaseOrder  page");
				editByPurchaseOrderId();
				purchaseOrderItemAndTaxDtls();
				return VIEW_PURCHASE_ORDERITEM_PAGE;

			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("called Edit PurchaseOrder page");
				getUomMasterList();
				getEntityMasterList();
				editByPurchaseOrderId();
				purchaseOrderItemAndTaxDtls();
				circleMasterList = commonDataService.loadCircles();
				return CREATE_PURCHASE_ORDERITEM_PAGE;

			} else if (action.equalsIgnoreCase("Delete")) {
				log.info("delete PurchaseOrder  called.." + purchaseOrder.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmQuotationDelete').show();");
			} else if (action.equalsIgnoreCase("Clear")) {
				log.info("called Clear PurchaseOrder page");
				showAllListForPurchaseOrder();
				return LIST_PURCHASE_ORDERITEM_PAGE;
			}
		} catch (Exception e) {
			log.error(" Exception at quotationListPageAction " + e);
		}
		return null;
	}

	/*** Quotation Grid OnRow Select Here ***/
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PurchaseOrder.onRowSelect ========>" + event);
		purchaseOrder = ((PurchaseOrder) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends PurchaseOrder.onRowSelect ========>");
	}

	public String showAllListForPurchaseOrder() {
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		loadStatusValues();
		resetAllPurchaseOrderForms();
		loadLazyPurchaseOrderList();
		return LIST_PURCHASE_ORDERITEM_PAGE;
	}

	public String backToHomePage() {
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		resetAllPurchaseOrderForms();
		return LIST_PURCHASE_ORDERITEM_PAGE;
	}

	public void resetAllPurchaseOrderForms() {
		log.info(" Start...  resetAllPurchaseOrderForms");
		purchaseOrderItems = new PurchaseOrderItems();
		orderItemTax = new OrderItemTax();
		orderType = "";

		productCategoryGroup = new ProductCategoryGroup();
		productCategoryGroupList = new ArrayList<ProductCategoryGroup>();

		productCategory = new ProductCategory();
		productCategoryList = new ArrayList<ProductCategory>();

		productGroupMaster = new ProductGroupMaster();
		productGroupList = new ArrayList<ProductGroupMaster>();

		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

		uomMasterLists = new ArrayList<>();

		poTaxSummaryList = new ArrayList<PurchaseOrderDTO>();

		productVarietyTaxs = new ArrayList<ProductVarietyTax>();

		purchaseOrder = new PurchaseOrder();

		quotationNumberList = new ArrayList<Quotation>();

		supplierList = new ArrayList<SupplierMaster>();

		purchaseOrderItemList = new ArrayList<PurchaseOrderItems>();

		purchaseOrderByQuotation = "";
		purchaseItemFlag = true;
		taxPercent = 0.0;
		taxValue = 0.0;
		unitRateTotal = 0.0;
		itemAmountTotal = 0.0;
		netAmountTotal = 0.0;
		discountValueTotal = 0.0;
		taxValueTotal = 0.0;

		supplierTypeMaster = new SupplierTypeMaster();

		log.info(" End...  resetAllPurchaseOrderForms");

	}

	public void getQuotationListBySupplierId() {
		try {
			quotationNumberList = new ArrayList<Quotation>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/quotation/getquotationlist/supplier/"
					+ purchaseOrder.getSupplierMaster().getId();
			log.info(" PurchaseOrderItem Bean ==>>> getQuotationListBySupplierId URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				quotationNumberList = mapper.readValue(jsonResponse, new TypeReference<List<Quotation>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End..PurchaseOrderItem Bean... getQuotationList ------- >>>>");
	}

	public void convertTaxPercentToTaxvalue(Double taxpercent) {
		Double discountValue1 = discountValueTotal;

		Double taxValue = (itemAmountTotal - discountValue1) * (taxpercent / 100);
		taxValue = truncateDecimal(taxValue, 2);
		orderItemTax.setTaxValue(taxValue);
//		purchaseOrderItems.setTaxPercent(formatDouble(taxpercent));
	}

	private static Double truncateDecimal(Double value, int numberofDecimals) {
		BigDecimal truncateValue = new BigDecimal(String.valueOf(value)).setScale(numberofDecimals,
				BigDecimal.ROUND_FLOOR);
		return truncateValue.doubleValue();
	}

	public void getItemAmountByevent(Double amount) {
		itemAmountTotal = amount;
	}

	private Double formatDouble(Double inputValue) {
		Double formattedValue = null;
		try {
			inputValue = inputValue == null ? 0D : inputValue;
			if (inputValue > 0D) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.00");
				formattedValue = decimalFormat.parse(decimalFormat.format(inputValue)).doubleValue();
			} else {
				formattedValue = inputValue;
			}
		} catch (Exception ex) {
			formattedValue = inputValue;
			log.error("Exception at formatDouble", ex);
		}
		return formattedValue;
	}

	public void discountpercent(Double dis) {
		Double discountValue1 = 0.0;

		if (getDiscountType().equalsIgnoreCase("Percentage")) {
			discountValue1 = (itemAmountTotal == null ? 0.0 : itemAmountTotal) * (dis == null ? 1 : (dis / 100));
		} else {
			discountValue1 = dis;
		}
		discountValueTotal = discountValue1;
		purchaseOrderItems.setDiscountValue(discountValueTotal);
	}

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete Autocomplete query==>" + query);
		List<SupplierMaster> results = new ArrayList<SupplierMaster>();
		Long circleId = null;

		String supplierCode = query;
		if (selectedCircleMaster != null) {
			circleId = selectedCircleMaster.getId();
		}
		if (circleId == null) {
			if (supplierList != null) {
				for (SupplierMaster master : supplierList) {
					if (master.getCode() == null) {
						master.setCode("None");
					}
					if (master.getName() == null) {
						master.setName("None");
					}
					String name = master.getName().toUpperCase();
					String code = master.getCode().toUpperCase();
					if (name.contains(supplierCode.toUpperCase()) || code.contains(supplierCode)) {
//							log.info("supplierAutocomplete Autocomplete query==> step6" );
						results.add(master);
						if (master.getCode().equals("None")) {
							master.setCode(null);
						}

						if (master.getName().equals("None")) {
							master.setName(null);
						}
					}
				}
			}
		} else {
			log.info("supplierAutocomplete Autocomplete query==> step7" + " supplierCode : " + supplierCode
					+ " circleId : " + circleId + " serverURL : " + serverURL);
			supplierList = new ArrayList<>();
			supplierList = commonDataService.loadSupplierListByAutoCompleteCode(serverURL, supplierCode);
			log.info("supplierAutocomplete Autocomplete query==> step8 : " + supplierList.size());
			results.addAll(supplierList);
		}

		log.info("=====================supplierMasterList.size():======================= " + results.size());

		return results;
	}

	// LOAD ENTITY MASTER DETAILS
	public List<EntityMaster> loadforEntityMasterList(String searchParam) {
		log.info("<============= PettyCashReceiptBean loadShowRoomList Method starts ============> ");
		try {
			log.info(" SearchParam : " + searchParam);
			String url = AppUtil.getPortalServerURL() + "/quotation/loadallentity/" + searchParam;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());

				entityMasterAllList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});

				// entityMasterList =
				// organizationCollectionService.loadShowRoomDetailsByAutoComplete(searchParam);
				if (entityMasterAllList.size() == 0) {
					AppUtil.addWarn("Please give valid entity Code / Name");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			} else {
				AppUtil.addError("Internal Error!!");
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in organizationCollectionBean loadShowRoomList Method : ", e);
		}
		log.info("<============= organizationCollectionBean loadShowRoomList Method End ============> ");
		return entityMasterAllList;
	}
}
