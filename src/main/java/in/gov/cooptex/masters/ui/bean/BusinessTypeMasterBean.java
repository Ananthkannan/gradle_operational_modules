package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BusinessTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("businessTypeMasterBean")
@Scope("session")
@Log4j2
public class BusinessTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listBusinessTypeMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createBusinessTypeMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String BUSINESS_TYPE_MASTER_URL = AppUtil.getPortalServerURL() + "/businesstypemaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	BusinessTypeMaster businessTypeMaster, selectedBusinessTypeMaster;

	@Getter
	@Setter
	LazyDataModel<BusinessTypeMaster> lazyBusinessTypeMasterList;

	List<BusinessTypeMaster> businessTypeMasterList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showBusinessTypeMasterListPage() {
		log.info("<==== Starts BusinessTypeMasterBean.showBusinessTypeMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		businessTypeMaster = new BusinessTypeMaster();
		selectedBusinessTypeMaster = new BusinessTypeMaster();
		loadLazyBusinessTypeMasterList();
		log.info("<==== Ends BusinessTypeMasterBean.showBusinessTypeMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyBusinessTypeMasterList() {
		log.info("<===== Starts BusinessTypeMasterBean.loadLazyBusinessTypeMasterList ======>");

		lazyBusinessTypeMasterList = new LazyDataModel<BusinessTypeMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<BusinessTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = BUSINESS_TYPE_MASTER_URL + "/getallbusinesstypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						businessTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<BusinessTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return businessTypeMasterList;
			}

			@Override
			public Object getRowKey(BusinessTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BusinessTypeMaster getRowData(String rowKey) {
				for (BusinessTypeMaster businessTypeMaster : businessTypeMasterList) {
					if (businessTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedBusinessTypeMaster = businessTypeMaster;
						return businessTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends BusinessTypeMasterBean.loadLazyBusinessTypeMasterList ======>");
	}

	public String showBusinessTypeMasterListPageAction() {
		log.info("<===== Starts BusinessTypeMasterBean.showBusinessTypeMasterListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedBusinessTypeMaster == null || selectedBusinessTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_BUSINESS_TYPE_MASTER.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create businessTypeMaster called..");
				businessTypeMaster = new BusinessTypeMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit businessTypeMaster called..");
				String url = BUSINESS_TYPE_MASTER_URL + "/getbyid/" + selectedBusinessTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					businessTypeMaster = mapper.readValue(jsonResponse, new TypeReference<BusinessTypeMaster>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete businessTypeMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBusinessTypeMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showBusinessTypeMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends BusinessTypeMasterBean.showBusinessTypeMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts BusinessTypeMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(BUSINESS_TYPE_MASTER_URL + "/deletebyid/" + selectedBusinessTypeMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBusinessTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete businessTypeMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends BusinessTypeMasterBean.deleteConfirm ===========>");
		return showBusinessTypeMasterListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts BusinessTypeMasterBean.saveOrUpdate =======>");
		try {
			if (businessTypeMaster.getCode() == null || businessTypeMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_CODE_EMPTY.getCode());
				return null;
			}
			if (businessTypeMaster.getName() == null || businessTypeMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_NAME_EMPTY.getCode());
				return null;
			}
			if (businessTypeMaster.getName().trim().length() < 3 || businessTypeMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (businessTypeMaster.getLocalName() == null || businessTypeMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_LNAME_EMPTY.getCode());
				return null;
			}
			if (businessTypeMaster.getLocalName().trim().length() < 3
					|| businessTypeMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (businessTypeMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_STATUS_EMPTY.getCode());
				return null;
			}
			String url = BUSINESS_TYPE_MASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, businessTypeMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("businessTypeMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.BUSINESS_TYPE_MASTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends BusinessTypeMasterBean.saveOrUpdate =======>");
		return showBusinessTypeMasterListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts BusinessTypeMasterBean.onRowSelect ========>" + event);
		selectedBusinessTypeMaster = ((BusinessTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends BusinessTypeMasterBean.onRowSelect ========>");
	}

}
