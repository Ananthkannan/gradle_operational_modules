package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockInwardICBean")
@Scope("session")
public class StockInwardICBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/inspectionCenter/inward/listStockInwardInspection.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/inspectionCenter/inward/createStockInwardInspection.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/inspectionCenter/inward/viewStockInwardInspection.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnSearch = true;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}

	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {

			String loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();

			if (loginEntityCode.equals(EntityType.INSPECTION_CENTER)) {

				if (page.equals("ADD")) {
					return gotoAddPage(loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId());
				} else if (page.equals("EDIT")) {
					return gotoEditPage();
				} else if (page.equals("VIEW")) {
					return gotoViewPage();
				} else if (page.equals("LIST")) {
					return getListPage();
				} else {
					return null;
				}
			} else {
				errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_INSPECTION_CENTER.getCode());
			}
		}

		return null;
	}

	public String getListPage() {
		log.info("Going to list page");

		SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
		loadLazyList();

		return LIST_PAGE_URL;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();

		request.setStockTransferType(StockTransferType.INSPECTION_INWARD);
		if (getLoginEmployeeWorkLocation() != null && getLoginEmployeeWorkLocation().getId() != null) {
			request.setId(getLoginEmployeeWorkLocation().getId());
		}

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/list";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
	}

	/***************************************************************************************************/

	/*
	 * This method is called when Edit Button is pressed
	 */

	public String gotoEditPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {

			stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

			if (stockTransfer.getStockTransfer() != null) {
				stockTransfer.getStockTransfer().setCreatedByName(stockTransfer.getStockTransfer().getReferenceNumber()
						+ " / " + AppUtil.DATE_FORMAT.format(stockTransfer.getStockTransfer().getDateTransferred()));
			}

			disableBtnSearch = false;

			return ADD_PAGE_URL;
		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

	}

	public String gotoViewPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferResponse.getId());

		return VIEW_PAGE_URL;
	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {

		// Step 1: Check the object is selected from the list
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {

			// Step 2: Find the Object is removable
			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferResponse.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	/*********************************************************************************************************************************/

	public String gotoAddPage(Long loginEmployeeWorkLocationId) {
		log.info("Add Button is pressed....");
		disableBtnSearch = true;
		stockTransfer = new StockTransfer();
		stockTransfer.setStockTransfer(null);
		stockTransfer.setSupplierMaster(null);
		stockTransferList = null;
		supplierMasterList = commonDataService.findSupplierByStatusAndTransferType(StockTransferStatus.SUBMITTED,
				StockTransferType.SOCIETY_OUTWARD, loginEmployeeWorkLocationId);
		return ADD_PAGE_URL;
	}

	public void onSelectSupplierMaster() {
		log.info("onSelectSupplierMaster called");

		stockTransferList = new ArrayList<>();
		stockTransfer.setStockTransfer(null);

		EntityMaster employeeWorkLocation = getLoginEmployeeWorkLocation();

		if (stockTransfer.getSupplierMaster() != null && employeeWorkLocation != null) {
			stockTransferList = commonDataService.findStockBySupplierAndStatusAndTransferType(
					stockTransfer.getSupplierMaster().getId(), StockTransferStatus.SUBMITTED,
					StockTransferType.SOCIETY_OUTWARD, employeeWorkLocation.getId());

			if (stockTransferList != null) {
				stockTransferList.stream().forEach(st -> {
					st.setCreatedByName(
							st.getReferenceNumber() + " / " + AppUtil.DATE_FORMAT.format(st.getDateTransferred()));
				});
			}
		}
	}

	public void onSelectStockTransfer() {
		if (stockTransfer.getSupplierMaster() != null && stockTransfer.getStockTransfer() != null) {
			disableBtnSearch = false;
		} else {
			disableBtnSearch = true;
		}
	}

	public void searchStock() {
		log.info("Seach Stock Button is pressed.....");
		if (stockTransfer.getSupplierMaster() != null && stockTransfer.getStockTransfer() != null) {

			log.info("Stock Transfer ID : [" + stockTransfer.getStockTransfer().getId() + "]");

			StockTransfer selectedStockTransfer = commonDataService
					.getStockTransferById(stockTransfer.getStockTransfer().getId());

			prepareStockTransfer(selectedStockTransfer);

		}
	}

	public void prepareStockTransfer(StockTransfer selectedStockTransfer) {

		stockTransfer.setStockTransfer(selectedStockTransfer);
		stockTransfer.setToEntityMaster(selectedStockTransfer.getToEntityMaster());
		stockTransfer.setSupplierMaster(selectedStockTransfer.getSupplierMaster());
		stockTransfer.setDateTransferred(selectedStockTransfer.getDateTransferred());

		stockTransfer.setTransportMaster(selectedStockTransfer.getTransportMaster());
		stockTransfer.setWaybillAvailable(selectedStockTransfer.getWaybillAvailable());
		stockTransfer.setWaybillNumber(selectedStockTransfer.getWaybillNumber());
		stockTransfer.setTransportChargeAvailable(selectedStockTransfer.getTransportChargeAvailable());
		stockTransfer.setTransportChargeType(selectedStockTransfer.getTransportChargeType());
		stockTransfer.setTransportChargeAmount(selectedStockTransfer.getTransportChargeAmount());

		stockTransfer.setDateReceived(new Date());
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		stockTransfer.setTransferType(StockTransferType.INSPECTION_INWARD);

		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		int count = 0;

		stockTransfer.setBundleNumbers("");
		if (selectedStockTransfer.getStockTransferItemsList() != null
				&& !selectedStockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : selectedStockTransfer.getStockTransferItemsList()) {
				if (items.getStockTransferItemsBundlesList() != null
						&& !items.getStockTransferItemsBundlesList().isEmpty()) {
					for (StockTransferItemsBundles bundles : items.getStockTransferItemsBundlesList()) {
						StockTransferItems newItems = new StockTransferItems(items);
						newItems.setReceivedQty(bundles.getQuantity());
						newItems.setBundleNumber(bundles.getBundleNumber());

						if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
							stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
						}

						if (bundles.getBundleNumber() != null) {
							stockTransfer.setBundleNumbers(
									stockTransfer.getBundleNumbers() + String.valueOf(bundles.getBundleNumber()));
						}

						count += 1;
						stockTransfer.setTotalBundles(count);
						stockTransfer.getStockTransferItemsList().add(newItems);
					}
				}
			}
		}
	}

	/********************************************************************************************************************************************/

	public String create(boolean isSubmit) {

		log.info("create method is executing..");

		if (isSubmit) {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		}

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/inward/create";

		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

}
