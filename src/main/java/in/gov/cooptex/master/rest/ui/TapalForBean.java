package in.gov.cooptex.master.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.TapalFor;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("tapalForBean")

public class TapalForBean implements Serializable {

	private static final long serialVersionUID = -176895761871211745L;

	private final String LIST_TAPALFOR_MASTER_PAGE = "/pages/masters/tapalfor/listTapalFor.xhtml?faces-redirect=true;";
	private final String CREATE_TAPALFOR_MASTER_PAGE = "/pages/masters/tapalfor/createTapalFor.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/tapalfor/viewTapalFor.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String jsonResponse;
	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private String action;

	@Getter

	@Setter
	Boolean addButtonFlag;

	@Getter

	@Setter
	Boolean editButtonFlag;

	@Getter

	@Setter
	Boolean deleteButtonFlag;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	TapalFor tapalFor;

	@Getter
	@Setter
	TapalFor selectedTapalFor = new TapalFor();

	@Getter
	@Setter
	List<TapalFor> tapalForList = new ArrayList<TapalFor>();

	@Getter
	@Setter
	LazyDataModel<TapalFor> lazyTapalForList;

	List<TapalFor> TapalForList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private int tapalForListSize;

	boolean forceLogin = false;

	public String showTapalForListPage() {
		log.info("<==== Starts TapalForBean.showTapalForListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		tapalFor = new TapalFor();
		selectedTapalFor = new TapalFor();
		loadLazyTapalForList();
		log.info("<==== Ends TapalForBean.showTapalForListPage =====>");
		return LIST_TAPALFOR_MASTER_PAGE;
	}

	public String addTapalFor() {

		log.info("<--- Start TapalForBean.addTapalFor ---->");
		try {
			action = "ADD";
			tapalFor = new TapalFor();
			tapalFor.setActiveStatus(true);

			log.info("<---- End TapalForBean.addTapalFor ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_TAPALFOR_MASTER_PAGE;
	}


	public String submit() {
		log.info("<=======Starts  TapalForBean.submit ======>");

		if (tapalFor.getCode() == null || tapalFor.getCode().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_NAME_REQUIRED.getErrorCode());
			return null;
		}

		if (tapalFor.getName() == null || tapalFor.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_lNAME_REQUIRED.getErrorCode());
			return null;
		}
		if (tapalFor.getLname() == null || tapalFor.getLname().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_lNAME_REQUIRED.getErrorCode());
			return null;
		}

		if (tapalFor.getActiveStatus() == null) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_STATUS_REQUIRED.getErrorCode());
			return null;
		}

		if (action.equalsIgnoreCase("ADD")) {
			tapalFor.setCreatedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("EDIT")) {
			tapalFor.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL +appPreference.getOperationApiUrl() + "/tapalfor/create", tapalFor);
		// errorMap.notify(response.getStatusCode());
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.TAPAL_FOR_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.TAPAL_FOR_UPDATE_SUCCESS.getErrorCode());
			// return showListQualificationPage();
			return showTapalForListPage();
		} 
		else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends TapalForBean.submit ======>");
		return null;
	}

	public String editTapalFor() {
		log.info("<<<<< -------Start editTapalFor called ------->>>>>> ");

		try {
			if (selectedTapalFor == null || selectedTapalFor.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_TAPAL_FOR.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit TapalFor called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tapalfor/get/" + selectedTapalFor.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					tapalFor = mapper.readValue(jsonResponse, new TypeReference<TapalFor>() {
					});
					return CREATE_TAPALFOR_MASTER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in editTapalFor ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends TapalFor.editTapalFor ===========>" + action);
		return null;
	}

	public String viewTapalFor() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedTapalFor == null || selectedTapalFor.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_TAPAL_FOR.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedTapalFor.getId());

		selectedTapalFor = getTapalForDetails(selectedTapalFor.getId());

		log.info("selected TapalFor Object:::" + selectedTapalFor);
		return VIEW_PAGE;
	}

	public String deleteTapalFor() {
		try {
			action = "Delete";
			if (selectedTapalFor == null || selectedTapalFor.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_TAPAL_FOR.getErrorCode());
				return null;
			
			} else {
				
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteTapalForConfirm() {
		log.info("<===== Starts TapalForBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL+appPreference.getOperationApiUrl() + "/tapalfor/delete/"+selectedTapalFor.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.TAPAL_FOR_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete TapalFor ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends TapalForBean.deleteConfirm ===========>");
		return  showTapalForListPage();
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start TapalForBean . clear ----->");
		showTapalForListPage();

		log.info("<---- End TapalForBean.clear ----->");

		return LIST_TAPALFOR_MASTER_PAGE;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TapalFor.onRowSelect ========>" + event);
		selectedTapalFor = ((TapalFor) event.getObject());
		log.info("selectedTapalFor id" + selectedTapalFor.getId());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends TapalFor.onRowSelect ========>");
	}

	public void loadLazyTapalForList() {
		log.info("<===== Starts TapalFor.loadLazyTapalForList ======>");
		lazyTapalForList = new LazyDataModel<TapalFor>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TapalFor> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tapalfor/gettapalforlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						tapalForList = mapper.readValue(jsonResponse, new TypeReference<List<TapalFor>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tapalForList;
			}

			@Override
			public Object getRowKey(TapalFor res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TapalFor getRowData(String rowKey) {
				try {
					for (TapalFor tapalFor : tapalForList) {
						if (tapalFor.getId().equals(Long.valueOf(rowKey))) {
							selectedTapalFor = tapalFor;
							return tapalFor;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TapalFor.loadLazyTapalForList ======>");
	}

	private TapalFor getTapalForDetails(Long id) {
		log.info("<====== TapalForBean.getTapalForDetails starts====>" + id);
		try {
			if (selectedTapalFor == null || selectedTapalFor.getId() == null) {
				log.error(" No TapalFor selected ");
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICATION_NO_SELECTED.getErrorCode());
			}
			log.info("Selected TapalFor  Id : : " + selectedTapalFor.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/tapalfor/get/" + selectedTapalFor.getId();

			log.info("TapalFor Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			TapalFor tapalForDetail = mapper.readValue(jsonResponse, TapalFor.class);

			if (tapalForDetail != null) {
				tapalFor = tapalForDetail;
			} else {
				log.error("TapalFor object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" TapalFor Retrived  SuccessFully ");
			} else {
				log.error(" TapalFor Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return tapalFor;
		} catch (Exception e) {
			log.error("Exception Ocured while get TapalFor Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String backPage() {

		showTapalForListPage();
		return LIST_TAPALFOR_MASTER_PAGE;
	}

}
