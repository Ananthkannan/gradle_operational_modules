package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("sectionMasterBean")
@Scope("session")
public class SectionMasterBean {

	private static final long serialVersionUID = 1L;
	private final String SECTION_LIST_PAGE = "/pages/masters/section/listSection.xhtml?faces-redirect=true;";
	private final String SECTION_ADD_PAGE = "/pages/masters/section/addSection.xhtml?faces-redirect=true;";
	private final String SECTION_VIEW_PAGE = "/pages/masters/section/viewSection.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	SectionMaster selectedSectionMaster = new SectionMaster();

	@Getter
	@Setter
	SectionMaster section = new SectionMaster();

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;

	@Getter
	@Setter
	LazyDataModel<SectionMaster> sectionMasterlazyLoadList;

	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	String sectionCode;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<SectionMaster> sectionList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> enitymasterList = new ArrayList<>();

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<>();

	public String showListSectionMasterPage() {
		log.info("showListSectionMasterPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		addButtonFlag = true;
		selectedSectionMaster = new SectionMaster();
		sectionList = commonDataService.getSectionMaster();
		departmentList = commonDataService.getDepartment();
		//getNextSequenceConfigValueForSectionCode();
		loadSectionMasterListLazy();
		return SECTION_LIST_PAGE;
	}

	public void loadSectionMasterListLazy() {

		log.info("<-----SectionMaster list lazy load starts------>");
		sectionMasterlazyLoadList = new LazyDataModel<SectionMaster>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<SectionMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<SectionMaster> sectionmasterList = mapper.readValue(jsonResponse,
							new TypeReference<List<SectionMaster>>() {
							});
					if (sectionmasterList == null) {
						log.info(" SectionMaster list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" SectionMaster list search Successfully Completed");
					} else {
						log.error(" SectionMaster list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return sectionmasterList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading SectionMaster list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(SectionMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SectionMaster getRowData(String rowKey) {
				List<SectionMaster> responseList = (List<SectionMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (SectionMaster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedSectionMaster = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			SectionMaster request = new SectionMaster();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("name")) {
					log.info("sectionName : " + value);
					searchFilters.put("sectionName", value);
				}
				if (filter.getKey().equals("code")) {
					log.info("sectionCode : " + value);
					searchFilters.put("sectionCode", value);
				}
				if (filter.getKey().equals("lname")) {
					log.info("sectionName in tamil : " + value);
					searchFilters.put("sectionNameTamil", value);
				}
				if (filter.getKey().equals("department.name")) {
					log.info("department Name : " + value);
					searchFilters.put("departmentName", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("createdDate : " + value);
					searchFilters.put("createdDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/sectionmaster/getallsectionmasterlistlazy", request);
			log.info("Search request response " + dataList);

		} catch (Exception e) {
			log.error("Exception Occured in SectionMaster load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String sectionMasterListAction() {
		log.info("<====== sectionBeanBean.sectionmasterListAction Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== SectionMaster add page called.......====>");
				//getNextSequenceConfigValueForSectionCode();
				selectedSectionMaster = new SectionMaster();
				section = new SectionMaster();
				// section.setDepartment(new Department());
				return SECTION_ADD_PAGE;
			}
			if (selectedSectionMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_SECTION.getErrorCode());
				return null;
			}
			
		 if (action.equalsIgnoreCase("View")) {
				log.info("View SectionMaster called..");

				BaseDTO response = httpService
						.get(SERVER_URL + "/sectionmaster/getsectionmaster/" + selectedSectionMaster.getId());
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						section = mapper.readValue(jsonResponse, new TypeReference<SectionMaster>() {
						});
						return SECTION_VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			}
			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSectionDelete').show();");
			} else {
				log.info("<----Loading SectionMaster Edit page.....---->" + selectedSectionMaster.getId());
				section = new SectionMaster();
				BaseDTO response = httpService
						.get(SERVER_URL + "/sectionmaster/getsectionmaster/" + selectedSectionMaster.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					section = mapper.readValue(jsonResponse, new TypeReference<SectionMaster>() {
					});
					log.info("<====== SectionMasterBean.sectionmasterListAction Ends====>");
					return SECTION_ADD_PAGE;
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== SectionMasterBean.sectionmasterListAction Ends====>");
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in SectionMasterBean.sectionmasterListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== SectionMasterBean.sectionmasterListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts SectionMasterBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/sectionmaster/delete/" + selectedSectionMaster.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.SECTION_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting SectionMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends SectionMasterBean.deleteConfirm ---->");
		return showListSectionMasterPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SectionMasterBean.onRowSelect ========>" + event);
		selectedSectionMaster = ((SectionMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		/*
		 * if(selectedSection.getStatus() == true ){ deleteButtonFlag=true; }else
		 * if(selectedSection.getStatus() == false){ deleteButtonFlag=false; }
		 */
		log.info("<===Ends SectionMasterBean.onRowSelect ========>");
	}

	public String submit() {
		log.info("<=======Starts  SectionMasterBean.submit ======>" + section);

		/*
		 * if(section.getDepartment() == null || section.getDepartment().getId() == null
		 * ) { errorMap.notify(ErrorDescription.DEPARTMENT_NAME_EMPTY.getErrorCode());
		 * return null; }
		 */
		if (section.getName() == null || section.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.SECTION_NAME_EMPTY.getErrorCode());
			return null;
		}
		
		if (section.getName().trim().length() < 3 || section.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.SECTION_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (section.getLname() == null || section.getLname().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.SECTION_LNAME_EMPTY.getErrorCode());
			return null;
		}
		if (section.getLname().trim().length() < 3 || section.getLname().trim().length() > 75) {
			errorMap.notify(ErrorDescription.SECTION_LNAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if (section.getStatus() == null) {
			errorMap.notify(ErrorDescription.SECTION_STATUS_EMPTY.getErrorCode());
			return null;
		}
		if (action.equalsIgnoreCase("ADD")) {
			section.setCreatedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("EDIT")) {
			section.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL + "/sectionmaster/save", section);
		// errorMap.notify(response.getStatusCode());
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.SECTION_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.SECTION_UPDATE_SUCCESS.getErrorCode());
			return showListSectionMasterPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends SectionMasterBean.submit ======>");
		return null;
	}
	/*public String getNextSequenceConfigValueForSectionCode() {
		log.info("getNextSequenceConfigValueForSectionCode called......");
		try {
			
			String url = SERVER_URL+ "/sectionmaster/nextsequenceconfigvalue";
			BaseDTO	response = httpService.get(url);
			log.info("BaseDTO Dto :" + response.getResponseContent());
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			sectionCode = mapper.readValue(jsonResponse, String.class);

			

			log.info("<<=======  INFORMATIONS::  " + sectionCode);
		} catch (Exception e) {
			log.error("<<<=======  Error in view sectionmaster =====>>");
		}
		
		return sectionCode;
		
	}*/
}
