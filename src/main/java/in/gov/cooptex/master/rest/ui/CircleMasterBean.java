package in.gov.cooptex.master.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.MasterSearchDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("circleMasterBean")
public class CircleMasterBean {

	private final String LIST_PAGE_URL = "/pages/masters/circle/listCircleMaster.xhtml?faces-redirect=true;";

	private final String CREATE_PAGE_URL = "/pages/masters/circle/createCircleMaster.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/circle/viewCircleMaster.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	String action;

	public String gotoPage(String URL) {

		log.info("Goto Page [" + URL + "]");

		if (URL.equalsIgnoreCase("LIST")) {
			return gotoListPage();
		} else if (URL.equalsIgnoreCase("ADD")) {
			action = "ADD";
			return gotoAddPage();
		} else if (URL.equalsIgnoreCase("EDIT")) {
			action = "EDIT";
			return gotoEditPage();
		} else if (URL.equalsIgnoreCase("VIEW")) {
			action = "VIEW";
			return gotoViewPage();
		} else {
			return null;
		}

	}

	@Autowired
	LanguageBean languageBean;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	private String address = "";

	@Getter
	@Setter
	private AddressMaster adressMaster;

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());

		String addressArray[] = AppUtil.prepareAddress(addressMasterBean.getAddressMaster());
		circleMaster.setAddressMaster(addressMasterBean.getAddressMaster());

		adressMaster = circleMaster.getAddressMaster();
		prepareAddress();
		/*
		 * if(languageBean.getLocaleCode().equals("en_IN")) { address = addressArray[0];
		 * }else { address = addressArray[1]; }
		 */
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("createCircleMasterForm");
		// onsuccess="PF('addressDialog').hide();"
	}

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<MasterSearchDTO> circleMasterLazyList;

	@Getter
	@Setter
	MasterSearchDTO masterSearchDTO;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	CircleMaster circleMaster;

	private String gotoListPage() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	private String gotoAddPage() {

		log.info("gotoAddPage");

		circleMaster = new CircleMaster();
		circleMaster.setActiveStatus(true);
		address = "";

		entityMasterList = commonDataService.findEntityByEntityType(EntityType.D_AND_P_OFFICE);

		return CREATE_PAGE_URL;
	}

	private String gotoViewPage() {

		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_CIRCLE_MASTER.getCode());
			return null;
		}

		circleMaster = getCircleMasterById(masterSearchDTO.getId());
		adressMaster = circleMaster.getAddressMaster();
		log.info("<<=====  adressMaster:: " + adressMaster);
		prepareAddress();
		return VIEW_PAGE_URL;
	}

	private String gotoEditPage() {

		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_CIRCLE_MASTER.getCode());
			return null;
		}
		log.info("<<===== masterSearchDTO :: " + masterSearchDTO.getId());
		circleMaster = getCircleMasterById(masterSearchDTO.getId());
		entityMasterList = commonDataService.findEntityByEntityType(EntityType.D_AND_P_OFFICE);
		adressMaster = circleMaster.getAddressMaster();
		if (adressMaster != null && adressMaster.getId() != null) {
			addressMasterBean.setAddressMaster(adressMaster);
			addressMasterBean.loadDistrict();
			addressMasterBean.loadTalukAndCity();
			log.info("<<=====  adressMaster:: " + adressMaster);
			prepareAddress();
		}
		return CREATE_PAGE_URL;

	}

	public CircleMaster getCircleMasterById(Long id) {
		String URL = appPreference.getPortalServerURL() + "/circleMaster/get/" + id;
		CircleMaster circleMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				circleMaster = mapper.readValue(jsonValue, CircleMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return circleMaster;
	}

	/*******************************************************************************************************/

	public void loadLazyList() {

		circleMasterLazyList = new LazyDataModel<MasterSearchDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<MasterSearchDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<MasterSearchDTO> data = new ArrayList<MasterSearchDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<MasterSearchDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(MasterSearchDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MasterSearchDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<MasterSearchDTO> responseList = (List<MasterSearchDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (MasterSearchDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		masterSearchDTO = ((MasterSearchDTO) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		MasterSearchDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = appPreference.getPortalServerURL() + "/circleMaster/list";

		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	private MasterSearchDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		MasterSearchDTO request = new MasterSearchDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("circleCodeOrName")) {
				log.info("circleCodeOrName : " + value);
				request.setCircleCodeOrName(value);
			}

			if (entry.getKey().equals("regionCodeOrName")) {
				log.info("regionCodeOrName : " + value);
				request.setRegionCodeOrName(value);
			}

			if (entry.getKey().equals("contactName")) {
				log.info("contactName : " + value);
				request.setContactName(value);
			}

			if (entry.getKey().equals("contactNumber")) {
				log.info("contactNumber : " + value);
				request.setContactNumber(value);
			}

			if (entry.getKey().equals("email")) {
				log.info("email : " + value);
				request.setEmail(value);
			}

			if (entry.getKey().equals("status")) {
				log.info("status : " + value);
				request.setStatus(Boolean.valueOf(value));
			}

		}

		return request;
	}

	/*******************************************************************************************************/

	public String create() {

		log.info("create method is executing..");
		
		if(circleMaster.getEmailId()!=null && !circleMaster.getEmailId().isEmpty()) {
			String emailRegex="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
			Pattern pat = Pattern.compile(emailRegex); 
			if(!pat.matcher(circleMaster.getEmailId()).matches()) {
				AppUtil.addWarn("Please give valid email id");
				return null;
			}
		}
		String URL = appPreference.getPortalServerURL();
		if (action.equalsIgnoreCase("ADD"))
			URL = URL + "/circleMaster/create";
		if (action.equalsIgnoreCase("EDIT"))
			URL = URL + "/circleMaster/update";

		log.info("<<====  circleMaster ::" + circleMaster.getId());
		try {
			BaseDTO baseDTO = httpService.post(URL, circleMaster);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.CIRCLE_SAVED_SUCCESSFULLY.getCode());
					else
						errorMap.notify(ErrorDescription.CIRCLE_UPDATED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public void processDelete() {
		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_CIRCLE_MASTER.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {

		String URL = appPreference.getPortalServerURL() + "/circleMaster/delete/" + masterSearchDTO.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.CIRCLE_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return gotoPage("LIST");
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public void prepareAddress() {
		String addressEnglish = "";

		if (adressMaster != null) {
			if (adressMaster.getAddressLineOne() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineOne())) {
				addressEnglish = addressEnglish + adressMaster.getAddressLineOne() + ",";
			}
			if (adressMaster.getAddressLineTwo() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineTwo())) {
				addressEnglish = addressEnglish + adressMaster.getAddressLineTwo() + ",";
			}
			if (adressMaster.getAddressLineThree() != null
					&& StringUtils.isNotEmpty(adressMaster.getAddressLineThree())) {
				addressEnglish = addressEnglish + adressMaster.getAddressLineThree() + ",";
			}
			if (adressMaster.getLandmark() != null && StringUtils.isNotEmpty(adressMaster.getLandmark())) {
				addressEnglish = addressEnglish + adressMaster.getLandmark() + ",";
			}
			if (adressMaster.getCityMaster() != null && adressMaster.getCityMaster().getName() != null) {
				addressEnglish = addressEnglish + adressMaster.getCityMaster().getName() + ",";
			}
			if (adressMaster.getTalukMaster() != null && adressMaster.getTalukMaster().getName() != null) {
				addressEnglish = addressEnglish + adressMaster.getTalukMaster().getName() + ",";
			}
			if (adressMaster.getDistrictMaster() != null && adressMaster.getDistrictMaster().getName() != null) {
				addressEnglish = addressEnglish + adressMaster.getDistrictMaster().getName() + ",";
			}
			if (adressMaster.getStateMaster() != null && adressMaster.getStateMaster().getName() != null) {
				addressEnglish = addressEnglish + adressMaster.getStateMaster().getName();
			}
			if (adressMaster.getPostalCode() != null && !adressMaster.getPostalCode().trim().isEmpty()) {
				if (addressEnglish.isEmpty()) {
					addressEnglish = adressMaster.getPostalCode();
				} else {
					addressEnglish = addressEnglish + "-" + adressMaster.getPostalCode();
				}
			}
		}
		address = addressEnglish;
	}

}
