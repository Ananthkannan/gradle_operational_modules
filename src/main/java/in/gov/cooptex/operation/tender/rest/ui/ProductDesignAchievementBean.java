package in.gov.cooptex.operation.tender.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import in.gov.cooptex.core.dto.PaginationDTO;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceFiles;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.production.dto.ProductDesignTargetDTO;
import in.gov.cooptex.operation.production.model.ProductDesignAchievement;
import in.gov.cooptex.operation.production.model.ProductDesignTarget;
import in.gov.cooptex.operation.production.model.ProductDesignTargetDetails;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import java.text.ParseException;

@Log4j2
@Service("productDesignAchievementBean")
@Scope("session")
public class ProductDesignAchievementBean implements Serializable {
	/**
	* 
	*/

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	List<ProductDesignTargetDetails> entityMasterProductDesignTargetList;

	@Getter
	@Setter
	ProductDesignTargetDetails entityMasterProductDesignTarget = new ProductDesignTargetDetails();

	@Getter
	@Setter
	List<ProductDesignTarget> productDesignTargetEntityList;

	@Getter
	@Setter
	ProductDesignTarget selectedproductDesignTargetEntity;

	@Getter
	@Setter
	List<ProductDesignTarget> targetFixationNameList;

	@Getter
	@Setter
	ProductDesignTarget targetFixationName;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String productDesignFilePathName;

	@Getter
	@Setter
	String productDesignFilePath;

	// second table

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;

	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterListFilter;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper;

	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	Boolean viewButtonFlag;

	@Getter
	@Setter
	Boolean addButtonFlag;

	@Getter
	@Setter
	private UploadedFile fileDocument;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetTableCreatedList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignAchievementDTOList;

	@Getter
	@Setter
	List<ProductDesignAchievement> productDesignAchievementList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignAchievementNewList;

	@Getter
	@Setter
	ProductDesignAchievement productDesignAchievement;

	@Getter
	@Setter
	ProductDesignTargetDTO productDesignTargetDTO;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Long numberOfDesignedQuantity;

	@Getter
	@Setter
	String designBased;

	@Getter
	@Setter
	String description;

	@Getter
	@Setter
	LazyDataModel<ProductDesignTargetDTO> productDesignAchievementLazyList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignAchievementLogList;

	@Getter
	@Setter
	int totalRecords;

	String jsonResponse;

	@Getter
	@Setter
	ProductDesignTargetDTO productDesignAchievementLazy;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetDTOList;

	@Getter
	@Setter
	List<ProductDesignTargetDTO> productDesignTargetDTOGroupList;

	@Getter
	@Setter
	List<String> designBasedOnList;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	Set<EntityMaster> entityMasterList = new HashSet<>();

	@Setter
	@Getter
	private StreamedContent file;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	private final String LIST_PAGE_URL = "/pages/operation/listProductDevelopmentAndDesignNDR.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE_URL = "/pages/operation/createProductDevelopmentAndDesignNDR.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/operation/viewProductDevelopmentAndDesignNDR.xhtml?faces-redirect=true;";
	private final String EDIT_URL = "/pages/operation/editProductDevelopmentAndDesignNDR.xhtml?faces-redirect=true;";

	public String createNewDesignNDR() {
		log.info("Received NDR create new");
		designBased = null;
		description = null;
		numberOfDesignedQuantity = null;
		productDesignTargetDTO = new ProductDesignTargetDTO();
		productCategory = null;
		productGroupMaster = null;
		productVarietyMaster = null;
		designBased = null;
		description = null;
		numberOfDesignedQuantity = null;
		targetFixationName = null;
		targetFixationNameList = new ArrayList<ProductDesignTarget>();
		deleteButtonFlag = false;
		viewButtonFlag = false;
		addButtonFlag = true;
		productDesignFilePathName = null;
		productDesignTargetDTOList = new ArrayList<ProductDesignTargetDTO>();
		productDesignTargetDTOGroupList = new ArrayList<ProductDesignTargetDTO>();
		productDesignAchievementNewList = new ArrayList<>();
		entityMasterProductDesignTarget = null;

		productCategoryList = new ArrayList<>();
		productCategory = new ProductCategory();
		productGroupMasterList = new ArrayList<>();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMasterList = new ArrayList<>();
		productVarietyMaster = new ProductVarietyMaster();

		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productDesignAchievementList = new ArrayList<ProductDesignAchievement>();
		productDesignTargetTableCreatedList = new ArrayList<ProductDesignTargetDTO>();
		loadTargetFixationNameList();
		return CREATE_PAGE_URL;
	}

	@PostConstruct
	public void init() {
		deleteButtonFlag = false;
		viewButtonFlag = false;
		addButtonFlag = true;
		designBasedOnList = new ArrayList<>();
		designBasedOnList.add("Market Sample Received From MD and Other HO Officer");
		designBasedOnList.add("Market Sample Selected By RM / PM / D &P");
		designBasedOnList.add("Design Created by the Designer at the Society");
		try {
			entityMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();

		} catch (Exception see) {
			log.error("ProductDesignAchievementBean() ::load user Entity Error " + see);
		}

		loadTargetFixationNameList();
		lazyQueryAchievementList();
		loadEntityProductVarietyMasterList();
	}

	// Entity Master ->
	public void loadEntityMasterListSecond() {
		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productDesignAchievementList = new ArrayList<ProductDesignAchievement>();
		productDesignTargetTableCreatedList = new ArrayList<ProductDesignTargetDTO>();
		selectedproductDesignTargetEntity = null;
		try {
			if (entityMaster == null) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_NULL_ENTITY.getErrorCode());
			} else {
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/productdesignachievement/productDesignTargetEntityList/" + entityMaster.getId();
				baseDTO = httpService.get(url);

				if (baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					productDesignTargetEntityList = mapper.readValue(jsonResponse,
							new TypeReference<List<ProductDesignTarget>>() {
							});
				} else {
					log.warn("loadEntityTypeMaster MasterList Error ");
				}
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	public void loadTargetFixationNameList() {

		BaseDTO baseDTO = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productDesignAchievementList = new ArrayList<ProductDesignAchievement>();
		productDesignTargetTableCreatedList = new ArrayList<ProductDesignTargetDTO>();
		targetFixationNameList = new ArrayList<ProductDesignTarget>();

		productDesignTargetDTOGroupList = new ArrayList<ProductDesignTargetDTO>();
		productDesignAchievementNewList = new ArrayList<>();

		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();

		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/getTargetFixationNameByEnityID/" + entityMaster.getId();
			baseDTO = httpService.get(url);
			log.info("::: get loadTargetFixationName MasterList Response :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				targetFixationNameList = mapper.readValue(jsonResponse, new TypeReference<List<ProductDesignTarget>>() {
				});

			} else {
				log.warn("loadTargetFixationName MasterList Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadTargetFixationName MasterList load ", ee);
		}
	}

	public String clearAction() {
		log.info("Received NDR Clear Action");
		deleteButtonFlag = false;
		viewButtonFlag = false;
		addButtonFlag = true;
		productDesignTargetDTO = null;
		return LIST_PAGE_URL;
	}

	public String viewAction() {
		log.info("Received NDR View Action");
		viewSelectedProductDetailsTargetDetailsList();
		return VIEW_URL;
	}

	public String editAction() {
		log.info("Received NDR Edit Action");
		viewSelectedProductDetailsTargetDetailsList();
		return EDIT_URL;
	}

	public void onRowSelect(SelectEvent event) {
		addButtonFlag = false;
		viewButtonFlag = true;
		deleteButtonFlag = true;
	}

	// load Product Category Code / Name
	public void productCategoryListLoad() {
		log.info("Received productCategoryGroupList ");
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productCategory = null;
		productGroupMaster = null;
		productVarietyMaster = null;
		try {
			BaseDTO baseDTO = null;

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/testingLabStockInward/getAllProductCategoryCodeName";
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/stockItemInwardPNS/getAllProductCategoryList/"
					+ productDesignTargetDTOList.get(0).getProductCategoryGroup().getId();
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				log.info("productCategoryList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
			} else {
				log.warn("productCategoryList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productCategoryList load ", ee);
		}
	}

	// product category -> 3
	public void getAllProductGroupMasterList() {
		log.info("Received productGroupMasterList ");
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productGroupMaster = null;
		productVarietyMaster = null;

		try {
			BaseDTO baseDTO = null;
//			String url = SERVER_URL + appPreference.getOperationApiUrl()
//					+ "/stockItemInwardPNS/getAllProductGroupMasterList/"
//					+ productCategory.getProductCategoryGroup().getId();

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductGroupMasterList/" + productCategory.getId();

			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				log.info("productGroupMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productGroupMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
			} else {
				log.warn("productGroupMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productGroupMasterList load ", ee);
		}
	}

	// product category -> 4
	public void listenerProductVarietyMasterList() {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		productVarietyMaster = null;
		try {
			if (productGroupMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODCUT_GROUP_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProVarietyMasterList/" + productGroupMaster.getId();
			log.info("listenerProductVarietyMasterList url==> " + url);
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				log.info("productVarietyMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
	}

	// getProductWise Design Status
	public void loadProductDetailsTargetDetailsList() {
		log.info("Received productGroupMasterList ");
		productDesignTargetDTOList = new ArrayList<ProductDesignTargetDTO>();

		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/viewProductDesignAchievementList/" + targetFixationName.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productDesignTargetDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignTargetDTO>>() {
						});

				if (productDesignTargetDTOList.size() > 0) {
					productCategoryListLoad();
				}
				productDesignAchievementNewList = new ArrayList<>();
				groupingProductDevelopmentAndDesign();

			} else {
				log.warn("productDesignTargetDetails Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productView TargetDesign load:", ee);
		}
	}

	// getProductWise Design selected
	public void viewSelectedProductDetailsTargetDetailsList() {
		log.info("Received productGroupMasterList ");

		productDesignTargetDTOList = new ArrayList<ProductDesignTargetDTO>();
		ProductDesignTargetDTO dto = new ProductDesignTargetDTO();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/viewSelectedProductDesignAchievementList/"
					+ productDesignTargetDTO.getProductDesignTargetID() + "/"
					+ productDesignTargetDTO.getProductVarietyMaster().getId() + "/"
					+ productDesignTargetDTO.getEntityMaster().getId() + "/" + productDesignTargetDTO.getId();
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				dto = mapper.readValue(jsonResponse, new TypeReference<ProductDesignTargetDTO>() {
				});
				log.info("received view process suceessfully  *********************** ");
				productDesignTargetDTOList = dto.getProductDesignTargetDTOList();
				log.info("productDesignTargetDTOList Size is :" + productDesignTargetDTOList.size());

				productDesignAchievementDTOList = dto.getProductDesignAchievementDTOList();
				productDesignAchievement = dto.getProductDesignAchievement();
				productDesignTargetDTO = dto;

				targetFixationName = dto.getProductDesignTarget();
				log.info("productDesignAchievement " + productDesignAchievement.getId() + ","
						+ productDesignAchievement.getDesignBased());
				groupingProductDevelopmentAndDesign();
				log.info("Done Fetch Data *** :");
			} else {
				log.warn("productDesignTargetDetails Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productView TargetDesign load:", ee);
		}

	}

	public Double getSumTargetQuantity() {
		try {
			double quantity = 0.0;
			for (ProductDesignTargetDTO productDTO : productDesignTargetDTOList) {
				quantity += productDTO.getTargetQuantity();
			}
			return quantity;
		} catch (Exception e) {
			log.error("getSumTargetQuantity Add Error " + e);
		}
		return 0.0;
	}

	public Long getSumDesignQuantity() {

		try {
			Long quantity = 0l;
			for (ProductDesignTargetDTO productDTO : productDesignAchievementNewList) {
				quantity += productDTO.getNumberOfDesignedQuantity();
			}

			return quantity;
		} catch (Exception e) {
			log.error("getSumDesignQuantity Add Error " + e);
		}
		return 0l;
	}

	public Double getSumCompletedQuantity() {
		try {
			double quantity = 0.0;
			for (ProductDesignTargetDTO productDTO : getProductDesignTargetDTOGroupList()) {
				quantity += productDTO.getCompletedQuantity();
			}
			return quantity;
		} catch (Exception e) {
			log.error("getCompletedQuantity Add Error " + e);
		}
		return 0.0;
	}

	public Double getSumBalanceQuantity() {
		try {
			double quantity = 0.0;
			for (ProductDesignTargetDTO productDTO : getProductDesignTargetDTOGroupList()) {
				quantity += productDTO.getBalQtyAvailable();
			}
			return quantity;
		} catch (Exception e) {
			log.error("getCompletedQuantity Add Error " + e);
		}
		return 0.0;
	}

	public Double getSumDesignedQuantity() {
		try {
			double quantity = 0.0;
			for (ProductDesignTargetDTO productDTO : getProductDesignAchievementDTOList()) {
				quantity += productDTO.getNumberOfDesignedQuantity();
			}
			return quantity;
		} catch (Exception e) {
			log.error("getCompletedQuantity Add Error " + e);
		}
		return 0.0;
	}

	// add new collector
	public String addNewProductDesignAchievement() {
		log.info("received add new collector Achievement ");
		try {
			if (numberOfDesignedQuantity == null) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_INVALID_QNTY.getErrorCode());
				return null;
			}
			if (designBased == null) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_INVALID_DESIGN_BASED.getErrorCode());
				return null;
			}
			if (description == null || description.trim().length() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_INVALID_DESCRIPTION.getErrorCode());
				return null;
			}
			productCategory.setProductCategoryGroup(productCategoryGroup);

			productGroupMaster.setProductCategory(productCategory);

			productVarietyMaster.setProductGroupMaster(productGroupMaster);

			ProductDesignTargetDTO productDTO = new ProductDesignTargetDTO();

			productDTO.setEntityMaster(entityMaster);

			productDTO.setProductCategoryGroup(productCategoryGroup);

			productDTO.setProductCategory(productCategory);

			productDTO.setProductGroupMaster(productGroupMaster);

			productDTO.setProductVarietyMaster(productVarietyMaster);

			productDTO.setNumberOfDesignedQuantity(numberOfDesignedQuantity);

			productDTO.setDesignBased(designBased);

			productDTO.setDescription(description);

			productDesignAchievementNewList.add(productDTO);

			designBased = null;
			description = null;
			numberOfDesignedQuantity = null;
			clearNewDesignDetails();
			log.info("New Achievement add Successfully ");
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_COLLECTOR_SAVE_SUCCESS.getErrorCode());
		} catch (Exception e) {
			log.error("New Achievement add Error " + e);
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_COLLECTOR_SAVE_ERROR.getErrorCode());
		}
		return null;
	}

	// save to db
	public String saveAchievement() {
		try {
			log.info("Received saveDesign Achievement");
			if (productDesignAchievementNewList == null || productDesignAchievementNewList.size() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_INVALID_LIST.getErrorCode());
				return null;
			}

			productDesignAchievementList = new ArrayList<ProductDesignAchievement>();

			for (ProductDesignTargetDTO productDTO : productDesignAchievementNewList) {
				productDesignAchievement = new ProductDesignAchievement();
				productDesignAchievement.setProductDesignTarget(targetFixationName);
				productDesignAchievement.setEntityMaster(productDTO.getEntityMaster());
				productDesignAchievement.setProductVarietyMaster(productDTO.getProductVarietyMaster());
				productDesignAchievement.setQuantity(productDTO.getNumberOfDesignedQuantity());
				productDesignAchievement.setDesignBased(productDTO.getDesignBased());
				productDesignAchievement.setDescription(productDTO.getDescription());
				productDesignAchievementList.add(productDesignAchievement);

			}
			productDesignTargetDTO = new ProductDesignTargetDTO();
			productDesignTargetDTO.setEntityMaster(entityMaster);
			productDesignTargetDTO.setProductDesignTarget(targetFixationName);
			productDesignTargetDTO.setProductDesignAchievementList(productDesignAchievementList);
			productDesignTargetDTO.setProductVarietyMaster(productVarietyMaster);
			productDesignTargetDTO.setProductDesignFilePath(productDesignFilePath);
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/saveDesignAchievement";
			baseDTO = httpService.post(url, productDesignTargetDTO);

			if (baseDTO.getStatusCode() == 0) {
				log.info("SaveProductTargetDesign Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_SAVE_SUCCESS.getErrorCode());
				return LIST_PAGE_URL;
			} else {
				log.warn("SaveProductTargetDesign Error *** :");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_SAVE_ERROR.getErrorCode());
			}
		} catch (Exception ee) {
			log.error("Exception Occured in SaveProductTargetDesign", ee);
		}
		return null;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			productDesignFilePathName = null;
			productDesignFilePath = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			fileDocument = event.getFile();
			long size = fileDocument.getSize();
			log.info("ProductDesignAchievementBean request document size is : " + size);
			size = size / 1000;

			String uploadPath = commonDataService.getAppKeyValue("PRODUCT_DESIGN_ACHIEVEMENT_UPLOAD_PATH");

			String filePath = Util.fileUpload(uploadPath, fileDocument);

			log.info(" Uploaded document oath is :" + uploadPath);
			productDesignFilePathName = fileDocument.getFileName();
			// productDesignTargetDTO.setFileType(filePath.substring(filePath.lastIndexOf(".")));
			productDesignFilePath = filePath;

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	// lazy search
	public void loadLazyIntItemConsumptionList() {
		log.info("<--- loadLazy productDesignAchievement RequirementList ---> ");

		productDesignAchievementLazyList = new LazyDataModel<ProductDesignTargetDTO>() {

			private static final long serialVersionUID = 1213111804540391535L;

			@Override
			public List<ProductDesignTargetDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<ProductDesignTargetDTO> data = new ArrayList<ProductDesignTargetDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<ProductDesignTargetDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyproductDesignAchievement()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(ProductDesignTargetDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductDesignTargetDTO getRowData(String rowKey) {
				List<ProductDesignTargetDTO> responseList = (List<ProductDesignTargetDTO>) getWrappedData();
				try {
					Long value = Long.valueOf(rowKey);
					for (ProductDesignTargetDTO res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							productDesignAchievementLazy = res;
							return res;
						}
					}
				} catch (Exception see) {
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		productDesignAchievementLazy = new ProductDesignTargetDTO();

		BaseDTO baseDTO = new BaseDTO();

		ProductDesignTargetDTO yarnRequirement = new ProductDesignTargetDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);

		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/searchDataLazytDesignAchievement";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	// group for selected Product Wise Status
	public void groupingProductDevelopmentAndDesign() {
		try {
			productDesignTargetDTOGroupList = new ArrayList<ProductDesignTargetDTO>();
			productDesignAchievementNewList = new ArrayList<ProductDesignTargetDTO>();
			// productDesignAchievementList
			java.util.Map<ProductVarietyMaster, Double> designationGroupByAvg = productDesignTargetDTOList.stream()
					.collect(Collectors.groupingBy(ProductDesignTargetDTO::getProductVarietyMaster,
							Collectors.summingDouble(ProductDesignTargetDTO::getTargetQuantity)));
			designationGroupByAvg.forEach((designation, values) -> {
				ProductDesignTargetDTO dto = new ProductDesignTargetDTO();
				dto.setTargetQuantity(values);
				dto.setProductVarietyMaster(designation);
				productDesignTargetDTOGroupList.add(dto);

			});

			for (ProductDesignTargetDTO dto : productDesignTargetDTOList) {
				for (ProductDesignTargetDTO dto2 : productDesignTargetDTOGroupList) {
					if (dto.getProductVarietyMaster().getId().equals(dto2.getProductVarietyMaster().getId())) {
						dto2.setProductCategory(dto.getProductCategory());
						dto2.setProductGroupMaster(dto.getProductGroupMaster());

						if (dto2.getCompletedQuantity() != null) {
							dto2.setCompletedQuantity(dto.getCompletedQuantity());
							dto2.setBalQtyAvailable(dto2.getTargetQuantity() - dto.getCompletedQuantity());
						} else {
							dto2.setCompletedQuantity(0.0);
							dto2.setBalQtyAvailable(0.0);
						}
						break;
					}
				}
			}

		} catch (Exception se) {
			log.warn("grouping error " + se);
		}
	}

	// Delete DesignAchievement
	public String deleteAchievement() {
		log.info("Received deleteAchievement ");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/deleteDesignAchievement/" + productDesignTargetDTO.getId();
			baseDTO = httpService.get(url);
			log.info("::: Retrieved deleteAchievement :");
			if (baseDTO.getStatusCode() == 0) {
				log.info("deleteAchievement Successfully Completed ******");
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_DELETE_SUCCESS.getErrorCode());
				return LIST_PAGE_URL;
			} else {
				errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_DELETE_FAILED.getErrorCode());
				log.warn("deleteAchievement Error *** :");
			}
		} catch (Exception ee) {
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_ACHIEVEMENT_DELETE_ERROR.getErrorCode());
			log.error("Exception Occured in deleteAchievement load ", ee);
		}
		return null;
	}

	// lazy new view
	public void lazyQueryAchievementList() {
		log.info("<===== Starts lazyQueryAchievementList List ======>");
		String lazyType = "CODE_ALLOTTED";
		Long entityMasterID = 0l;
		// Long entityMasterID =
		// loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId();
		if (entityMaster != null) {
			entityMasterID = entityMaster.getId();
		}
		log.info("entityMasterID :entityMasterID :" + entityMasterID + ":");
		String lazylistUrl = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/productdesignachievement/getallProductDesignAchievementlazy/" + lazyType + "/" + entityMasterID;
		mapper = new ObjectMapper();
		productDesignAchievementLogList = new ArrayList<>();

		productDesignAchievementLazyList = new LazyDataModel<ProductDesignTargetDTO>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1553357030678469897L;

			@Override
			public List<ProductDesignTargetDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for lazySocietyRegistrationList List=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						productDesignAchievementLogList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductDesignTargetDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = productDesignAchievementLogList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazySocietyRegistrationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + productDesignAchievementLogList.size());
				return productDesignAchievementLogList;
			}

			@Override
			public Object getRowKey(ProductDesignTargetDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductDesignTargetDTO getRowData(String rowKey) {
				try {
					for (ProductDesignTargetDTO societyRegReqLog : productDesignAchievementLogList) {
						if (societyRegReqLog.getId().equals(Long.valueOf(rowKey))) {
							productDesignAchievementLazy = societyRegReqLog;
							return societyRegReqLog;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.lazySocietyRegistrationList List ======>");
	}

	public void loadEntityProductVarietyMasterList() {
		log.info(":::<- loadEntityTypeMaster MasterList ->::: ");
		BaseDTO baseDTO = null;
		productVarietyMasterListFilter = new ArrayList<ProductVarietyMaster>();
		entityMasterList = new HashSet<>();

		try {

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productdesignachievement/productDesignFilterSelection/" + entityMaster.getId();
			baseDTO = httpService.get(url);

			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productDesignTargetDTO = mapper.readValue(jsonResponse, new TypeReference<ProductDesignTargetDTO>() {
				});
				productVarietyMasterListFilter = productDesignTargetDTO.getProductVarietyMasterList();
				entityMasterList = productDesignTargetDTO.getEntityMasterList();

				log.info("load Entity And Variety Success");
			} else {
				log.warn("loadEntityTypeMaster MasterList Error ");
			}

		} catch (Exception ee) {
			log.error("Exception Occured in loadEntityTypeMaster MasterList load ", ee);
		}
	}

	public void clearNewDesignDetails() {
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		numberOfDesignedQuantity = null;
		designBased = "";
		description = "";
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			File files = new File(productDesignAchievement.getProductDesignFilePath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}
}
