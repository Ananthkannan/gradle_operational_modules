package in.gov.cooptex.master.rest.ui;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("transportMasterBean")
public class TransportMasterBean {

	private static final String TRANSPORT_MASTER_LIST_PAGE = "/pages/masters/listTransportMaster.xhtml?faces-redirect=true";
	
	public String showListPage() {
		log.info("TransportMasterBean. showListPage() ");
		
		return TRANSPORT_MASTER_LIST_PAGE;
	}
	
}
