
/**
 * 
 */
package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.ProfitMarginMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.dto.ProductVarietyTaxDto;
import in.gov.cooptex.operation.dto.ProfitMarginDTO;
import in.gov.cooptex.operation.enums.ProfitMarginTypeEnum;
import in.gov.cooptex.operation.exceptions.UIException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Praveen
 *
 */
@Log4j2
@Scope("session")
@Service("profitMarginMasterBean")
public class ProfitMarginMasterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String LIST_PROFIRMARGIN_MASTER_PAGE = "/pages/masters/listProfitMargin.xhtml?faces-redirect=true;";
	private final String CREATE_PROFITMARGIN_MASTER_PAGE = "/pages/masters/createProfitMargin.xhtml?faces-redirect=true;";
	private final String VIEW_PROFITMARGIN_MASTER_PAGE = "/pages/masters/viewProfitMargin.xhtml?faces-redirect=true;";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	Boolean addButtonFlag;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	ProfitMarginMaster profitMarginMaster;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = null;

	@Getter
	@Setter
	LazyDataModel<ProfitMarginMaster> lazyProfitMarginMasterList;

	@Getter
	@Setter
	ProfitMarginMaster selectedprofitMarginMaster = new ProfitMarginMaster();

	@Getter
	@Setter
	List<ProfitMarginMaster> profitMarginMasterList = new ArrayList<ProfitMarginMaster>();

	@Getter
	@Setter
	private int profitMarginMasterListSize;

	@Getter
	@Setter
	int totalRecords = 0;

	boolean forceLogin = false;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	ProfitMarginDTO profitMarginDTO;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProfitMarginDTO selectedProfitMarginDTO = new ProfitMarginDTO();

	@PostConstruct
	public void init() {

		addButtonFlag = true;
		editButtonFlag = true;
		log.info(" ProfitMarginMaster init --->" + serverURL);
		loadValues();
		addProfitMarginMaster();
		loadLazyProfitMarginMasterList();
	}

	/** Get Portal URL Method ***/
	private void loadValues() {
		log.info("<<=========== ProfitMarginMasterBean. loadValues() Calling ==============>> ");
		try {
			serverURL = appPreference.getPortalServerURL();
			productCategoryList = new ArrayList<>();
			productCategoryList = commonDataService.loadProductCategories();
		} catch (Exception e) {
			log.fatal("Exception at ProfitMarginMasterBean. loadValues() ", e);
		}
	}

	/*** Add Profit Margin Master Method Here ***/
	public String addProfitMarginMaster() {
		log.info("<<=========== ProfitMarginMasterBean. addProfitMarginMaster() Starts ==============>> ");
		try {
			action = "ADD";
			profitMarginDTO = new ProfitMarginDTO();
			profitMarginDTO.setProfitMarginType(ProfitMarginTypeEnum.PERCENTAGE_BASED.toString());
			profitMarginDTO.setActiveStatus(true);
		} catch (Exception e) {
			log.error("Exception at ProfitMarginMasterBean. addProfitMarginMaster() ", e);
		}
		log.info("<<=========== ProfitMarginMasterBean. addProfitMarginMaster() Ends ==============>> ");
		return CREATE_PROFITMARGIN_MASTER_PAGE;
	}

	/*** Cancel Here ***/
	public String cancel() {
		log.info("<<============ ProfitMarginMasterBean. cancel() Calling ===============>> ");
		selectedprofitMarginMaster = new ProfitMarginMaster();
		addButtonFlag = true;
		return LIST_PROFIRMARGIN_MASTER_PAGE;
	}

	/**** Profit Master Save and Update function Here ***/
	public String submit() {
		log.info("<<============ ProfitMarginMasterBean. submit() Starts ===============>> ");
		try {
			// selectedProfitMarginDTO = new ProfitMarginDTO();
			if (action.equalsIgnoreCase("EDIT")) {
				profitMarginDTO.setId(selectedProfitMarginDTO == null ? null : selectedProfitMarginDTO.getId());
			}

			if (profitMarginDTO.getMinValue() > profitMarginDTO.getMaxValue()) {
				log.info("Minimum Value Should be less than maximum value");
				errorMap.notify(ErrorDescription
						.getError(MastersErrorCode.PROFIT_MARGIN_VALUE_TO_GREATER_THEN_VALUE_FROM).getErrorCode());
				return null;
			}

			String URL = serverURL + appPreference.getOperationApiUrl() + "/profitmarginmaster/saveOrUpdate";
			BaseDTO baseDTO = httpService.post(URL, profitMarginDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode().equals(MastersErrorCode.RANGE_IS_ALREADY_EXIST)) {
					AppUtil.addWarn("The Min & Max value range is already exist");
					return null;
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getErrorCode()) {
					if (action.equalsIgnoreCase("ADD")) {
						log.info("Profit Margin Master Saved Successfully");
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.PROFIT_MARGIN_SAVE_SUCCESS)
								.getErrorCode());
					}
					if (action.equalsIgnoreCase("EDIT")) {
						log.info("profit Margin Master Updated Successfully");
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.PROFIT_MARGIN_UPDATE_SUCCESS)
								.getErrorCode());
					}
					return backPage();
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception at ProfitMarginMasterBean. submit()", e);
		}
		log.info("<<============ ProfitMarginMasterBean. submit() Ends ===============>> ");
		return null;
	}

	/*** GET All Profit Margin List Here ***/
	private void getAllProfitMarginMasterList() {
		log.info("<<<< ----------Start ProfitMarginMaster-getAllProfitMarginMasterList() ------- >>>>");
		try {
			addButtonFlag = true;
			editButtonFlag = true;
			deleteButtonFlag = true;
			profitMarginMasterList = new ArrayList<ProfitMarginMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/profitmarginmaster/getActiveAllProfitMarginMaster";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				profitMarginMasterList = mapper.readValue(jsonResponse, new TypeReference<List<ProfitMarginMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (profitMarginMasterList != null) {
			profitMarginMasterListSize = profitMarginMasterList.size();
		} else {
			profitMarginMasterListSize = 0;
		}
		log.info("<<<< ----------End ProfitMarginMaster . getAllProfitMarginMasterList ------- >>>>");
	}

	/*** Profit Master Grid OnRow Select Here ***/
	public void onRowSelect(SelectEvent event) {
		log.info("<<========== ProfitMarginMasterBean. onRowSelect() Starts =============>> ");
		selectedprofitMarginMaster = ((ProfitMarginMaster) event.getObject());
		selectedProfitMarginDTO.setId(selectedprofitMarginMaster == null ? null : selectedprofitMarginMaster.getId());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<<========== ProfitMarginMasterBean. onRowSelect() Ends =============>> ");
	}

	/*** Edit Profit Master Here ***/
	public String editProfitMarginMaster() {
		log.info("<<========== ProfitMarginMasterBean. editProfitMarginMaster() Starts =============>> ");
		String page = null;
		try {
			if (selectedProfitMarginDTO == null || selectedProfitMarginDTO.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			String url = serverURL + appPreference.getOperationApiUrl() + "/profitmarginmaster/getById/"
					+ selectedProfitMarginDTO.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				profitMarginDTO = mapper.readValue(jsonResponse, new TypeReference<ProfitMarginDTO>() {
				});
				if (action.equalsIgnoreCase("EDIT")) {
					page = CREATE_PROFITMARGIN_MASTER_PAGE;
				} else if (action.equalsIgnoreCase("VIEW")) {
					page = VIEW_PROFITMARGIN_MASTER_PAGE;
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured at ProfitMarginMasterBean. editProfitMarginMaster() ", jpEx);
		} catch (Exception e) {
			log.error("Exception at ProfitMarginMasterBean. editProfitMarginMaster() ", e);
		}
		log.info("<<========== ProfitMarginMasterBean. editProfitMarginMaster() Ends =============>> ");
		return page;
	}

	/*** Common Profit Margin List Here ***/
	public void showList() {
		log.info("<---------- Loading ProfitMarginMasterBean showList list page---------->");
		loadValues();
		getAllProfitMarginMasterList();
	}

	/** Back to Profit Margin List Page ***/
	public String backPage() {
		selectedprofitMarginMaster = new ProfitMarginMaster();
		showList();
		return LIST_PROFIRMARGIN_MASTER_PAGE;
	}

	/*** delete profit master Here ***/
	public String deleteProfitMarginMaster() {
		try {
			action = "Delete";
			if (selectedProfitMarginDTO == null || selectedProfitMarginDTO.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProfitMarginMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/*** COnfirm delete profit margin master Here ***/
	public String deleteProfitMarginMasterConfirm() {
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/profitmarginmaster/deleteProfitMarginMaster/" + selectedProfitMarginDTO.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Profit Margin Master deleted successfully....!!!");
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.PROFIT_MARGIN_DELETE_SUCCESS).getErrorCode());
					return backPage();
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	/*** Profit margin clear here ***/
	public String clear() {
		log.info("<---- Start ProfitMarginMasterBean . clear ----->");
		selectedprofitMarginMaster = new ProfitMarginMaster();
		loadValues();
		getAllProfitMarginMasterList();
		log.info("<---- End ProfitMarginMasterBean . clear ----->");
		return LIST_PROFIRMARGIN_MASTER_PAGE;

	}

	public String showProfitMarginMasterListPage() {
		loadValues();
		getAllProfitMarginMasterList();
		return LIST_PROFIRMARGIN_MASTER_PAGE;
	}

	/** Go to force login page ***/
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public void loadLazyProfitMarginMasterList() {
		log.info("<===== Starts ProfitMarginMasterBean.loadLazyProfitMarginMasterList ======>");
		lazyProfitMarginMasterList = new LazyDataModel<ProfitMarginMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProfitMarginMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl() + "/profitmarginmaster/search";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						profitMarginMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProfitMarginMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyProfitMarginMasterList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return profitMarginMasterList;
			}

			@Override
			public Object getRowKey(ProfitMarginMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProfitMarginMaster getRowData(String rowKey) {
				try {
					for (ProfitMarginMaster glAccountCategory : profitMarginMasterList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedprofitMarginMaster = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ProfitMarginMasterBean.loadLazyProfitMarginMasterList ======>");
	}

	public void loadAllGroupsByProductCategorys() {
		log.info("<======== ProfitMarginMasterBean.loadAllGroupsByProductCategorys() calling ======>");
		productGroupMasterList = new ArrayList<>();
		try {
			if (profitMarginDTO.getSelectedCategoryList() != null
					&& !profitMarginDTO.getSelectedCategoryList().isEmpty()) {
				String url = AppUtil.getPortalServerURL() + "/group/getproductgroupbycategorylist";
				log.info("url" + url);
				BaseDTO baseDTO = httpService.post(url, profitMarginDTO.getSelectedCategoryList());
				if (baseDTO != null) {
					if (baseDTO.getResponseContents() != null) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						productGroupMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductGroupMaster>>() {
								});
						log.info("Total no. of Groups : " + productGroupMasterList.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} else {
				log.info("Selected Product Category List is Empty");
			}
		} catch (Exception e) {
			log.error("<========== Exception ProfitMarginMasterBean.loadAllGroupsByProductCategorys()==========>", e);
		}
	}

	public void loadAllProductVarietysByProductGroups() {
		log.info("<======== ProfitMarginMasterBean.loadAllProductVarietysByProductGroups() calling ======>");
		productVarietyMasterList = new ArrayList<>();
		try {
			if (profitMarginDTO.getSelectedCategoryList() != null
					&& !profitMarginDTO.getSelectedCategoryList().isEmpty()) {
				String url = AppUtil.getPortalServerURL() + "/productvariety/getproductvarietybygroupid";
				log.info("url" + url);
				BaseDTO baseDTO = httpService.post(url, profitMarginDTO.getSelectedGroupList());
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContents() != null) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						productVarietyMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarietyMaster>>() {
								});
						log.info("Total no. of ProductVarietys : " + productVarietyMasterList.size());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				}
			} else {
				log.info("Selected Product Group List is Empty");
			}
		} catch (Exception e) {
			log.error("<========== Exception ProfitMarginMasterBean.loadAllProductVarietysByProductGroups()==========>",
					e);
		}
	}

	public void rangValidation() {
		String outerRange = AppConfigKey.OUTER_STATE_PERCENTAGE_PROFIT;
		String innerRange = AppConfigKey.INNER_STATE_PERCENTAGE_PROFIT;
		String outer_percentage = commonDataService.getAppKeyValue(outerRange);
		String inner_percentage = commonDataService.getAppKeyValue(innerRange);
		Double outerPercantage;
		Double innerPercantage;
		outerPercantage = Double.parseDouble(outer_percentage);
		innerPercantage = Double.parseDouble(inner_percentage);
//		Double outerPercantage_Dob = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(outerPercantage));
//		Double innerPercantage_Dob = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(innerPercantage));

		if (profitMarginDTO.getOuterStatePercentage() != null) {
			if (profitMarginDTO.getOuterStatePercentage() > outerPercantage) {
				AppUtil.addWarn("Outer State Percentage Profit should be" +" "+ outerPercantage);
				profitMarginDTO.setOuterStatePercentage(null);
			} else {
				profitMarginDTO.setOuterStatePercentage(0.0);
			}
		}
		if (profitMarginDTO.getInnerStatePercentage() != null) {
			if (profitMarginDTO.getInnerStatePercentage() > innerPercantage) {
				AppUtil.addWarn("Inner State Percentage Profit should be " +" "+ innerPercantage);
				profitMarginDTO.setInnerStatePercentage(null);
			} else {
				profitMarginDTO.setInnerStatePercentage(0.0);
			}
		}

	}
}
