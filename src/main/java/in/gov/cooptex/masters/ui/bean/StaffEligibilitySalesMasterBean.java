package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.operation.hrms.model.StaffEligibilitySalesMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class StaffEligibilitySalesMasterBean {
	private final String CREATE_FuelCoupon_MASTER_PAGE = "/pages/masters/createStaffEligibilityMaster.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listStaffEligibilityMaster.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewStaffEligibilityMaster.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	StaffEligibilitySalesMaster staffEligibilitySalesMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<StaffEligibilitySalesMaster> staffEligibilitySalesMasterList = new ArrayList<StaffEligibilitySalesMaster>();

	@Getter
	@Setter
	private int staffEligibilitySalesMasterListSize;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<StaffEligibilitySalesMaster> lazyStaffEligibilitySalesMasterList;

	boolean forceLogin = false;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	StaffEligibilitySalesMaster selectedStaffEligibilitySalesMaster;

	@PostConstruct
	public void init() {
		log.info(" fuelCoupon method  init --->" + serverURL);
		staffEligibilitySalesMaster = new StaffEligibilitySalesMaster();
		loadValues();
		loadLazyStaffEligibilitySalesMasterList();
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			staffEligibilitySalesMaster = new StaffEligibilitySalesMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	private void validateInputForStaffEligibilitySalesMaster() throws UIException {
		if (staffEligibilitySalesMaster == null) {
			throw new UIException("Invalid input");
		} else if (staffEligibilitySalesMaster.getMinSales() == null) {
			throw new UIException("Please Enter Min Sales");
		} else if (staffEligibilitySalesMaster.getMaxSales() == null) {
			throw new UIException("Please Enter Max Sales");
		} else if (staffEligibilitySalesMaster.getStaffEligibility() == null) {
			throw new UIException("Please Enter Staff Eligibility");
		}

	}

	private void showClientError(String errorMessage) {
		AppUtil.addError(errorMessage);
	}

	public String saveStaffEligibilitySalesMaster() {

		if (staffEligibilitySalesMaster != null) {

			log.info("the staffEligibilitySalesMaster from page" + staffEligibilitySalesMaster.toString());
			try {

				validateInputForStaffEligibilitySalesMaster();

			} catch (UIException ex) {
				showClientError(ex.getMessage());
				log.error("UIException  >>>> " + ex.getMessage());
				return null;
			} catch (Exception e1) {
				log.error("Exception >>>> ", e1);
			}
			try {
				log.info("...... staffEligibilitySalesMaster submit begin ...."
						+ staffEligibilitySalesMaster.toString());
				BaseDTO baseDTO = null;
				String url = serverURL
						+ "/staffEligibilitySalesMasterControler/createOrUpdateStaffEligibilitySalesMaster";
				baseDTO = httpService.post(url, staffEligibilitySalesMaster);

				log.info(" baseDTo return From Data Base" + baseDTO.toString());

				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.STAFFELIGIBILITYMASTER_SAVE_SUCCESS.getCode());
					else
						errorMap.notify(ErrorDescription.STAFFELIGIBILITYMASTER_UPDATE_SUCCESS.getCode());
				} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} catch (Exception e) {
				log.error("Error while creating staffEligibilitySalesMaster" + e);
			}

			log.info("...... staffEligibilitySalesMaster submit ended ....");

		}
		clear();
		return INPUT_FORM_LIST_URL;
	}

	public void loadLazyStaffEligibilitySalesMasterList() {

		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		log.info("<===== StartsStaffEligibilitySalesMaster Bean.loadLazyStaffEligibilitySalesMasterList======>");
		lazyStaffEligibilitySalesMasterList = new LazyDataModel<StaffEligibilitySalesMaster>() {
			private static final long serialVersionUID = -7740994270523016922L;

			@Override
			public List<StaffEligibilitySalesMaster> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL
							+ "/staffEligibilitySalesMasterControler/getStaffEligibilitySalesMasterBYLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get staffEligibilitySalesMaster list");
					log.info("StaffEligibilitySalesMaster Total records" + response.getTotalRecords());

					log.info("the object returnend from StaffEligibilitySalesMasterControler"
							+ response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						staffEligibilitySalesMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<StaffEligibilitySalesMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e.getCause());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"
						+ staffEligibilitySalesMasterList.toString());
				log.info("list returned from data base while slecting Lazyload method"
						+ staffEligibilitySalesMasterList.size());
				log.info("Ends lazy load....");

				return staffEligibilitySalesMasterList;
			}

			@Override
			public Object getRowKey(StaffEligibilitySalesMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StaffEligibilitySalesMaster getRowData(String rowKey) {
				try {
					for (StaffEligibilitySalesMaster staffEligibilitySalesMaster : staffEligibilitySalesMasterList) {
						if (staffEligibilitySalesMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedStaffEligibilitySalesMaster = staffEligibilitySalesMaster;
							return staffEligibilitySalesMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info(
				"<===== Ends selectedStaffEligibilitySalesMaster Bean.loadLazyStaffEligibilitySalesMasterList ======>");
	}

	public String goToStaffEligibilitySalesMasterListPage() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;

		loadLazyStaffEligibilitySalesMasterList();

		return INPUT_FORM_LIST_URL;
	}

	public String goToStaffEligibilitySalesMasterCreatePage() {
		addButtonFlag = true;
		staffEligibilitySalesMaster = new StaffEligibilitySalesMaster();
		return CREATE_FuelCoupon_MASTER_PAGE;
	}

	public String goToStaffEligibilitySalesMasterViewPage() {

		action = "VIEW";

		log.info("<----Redirecting to user view page---->");
		if (selectedStaffEligibilitySalesMaster == null || selectedStaffEligibilitySalesMaster.getId() == null) {
			log.info("----- selectedInventoryConfigDTO-------");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		disableAddButton = false;
		return INPUT_FORM_VIEW_URL;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		staffEligibilitySalesMaster = ((StaffEligibilitySalesMaster) event.getObject());
		disableAddButton = true;

		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
	}

	public String goToStaffEligibilitySalesMasterEditPage() {
		log.info("<<<<< -------Start editFuelCoupon called ------->>>>>> ");
		action = "EDIT";

		if (selectedStaffEligibilitySalesMaster == null || selectedStaffEligibilitySalesMaster.getId() == null) {
			log.info("----- selectedInventoryConfigDTO-------");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}

		/*
		 * if (staffEligibilitySalesMaster.getId() == null) {
		 * Util.addWarn("Please Select Atleast One Record"); return null; }
		 */
		log.info("<<<<< -------End editCountry called ------->>>>>> ");
		//addButtonFlag = false;
		return CREATE_FuelCoupon_MASTER_PAGE;

	}

	public String deletetStaffEligibilitySalesMaster() {
		try {
			action = "DELETE";

			if (selectedStaffEligibilitySalesMaster == null || selectedStaffEligibilitySalesMaster.getId() == null) {
				log.info("----- selectedInventoryConfigDTO-------");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStaffEligibilitySalesMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}

	public String deletetStaffEligibilitySalesMasterConfirm() {

		try {
			log.info("Delete deletetEmployeeOtConfirm started the id is [" + staffEligibilitySalesMaster.getId() + "]");
			StaffEligibilitySalesMaster staffEligibilitySalesMasterId = new StaffEligibilitySalesMaster();
			staffEligibilitySalesMasterId.setId(staffEligibilitySalesMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/staffEligibilitySalesMasterControler/deleteStaffEligibilitySalesMasterById";
			baseDTO = httpService.post(url, staffEligibilitySalesMasterId);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				log.info("StaffEligibilitySalesMaster deleted successfully....!!!");
				errorMap.notify(ErrorDescription.STAFFELIGIBILITYMASTER_DELETE_SUCCESS.getErrorCode());
				staffEligibilitySalesMaster = new StaffEligibilitySalesMaster();
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while deleting EmployeeOtConfirm  ....", e);
		}
		return goToStaffEligibilitySalesMasterListPage();
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start countryMasterBean . clear ----->");
		loadLazyStaffEligibilitySalesMasterList();
		selectedStaffEligibilitySalesMaster = new StaffEligibilitySalesMaster();
		log.info("<---- End CountryMasterBean . clear ----->");
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;

	}

}
