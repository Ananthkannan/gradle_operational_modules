package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.admin.tapal.model.TapalSenderType;
import in.gov.cooptex.admin.tapal.model.TapalSenderType;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("tapalSenderTypeBean")
public class TapalSenderTypeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -176895761871211745L;

	private final String LIST_PAGE = "/pages/masters/tapalsendertype/listTapalSenderType.xhtml?faces-redirect=true;";
	private final String CREATE_PAGE = "/pages/masters/tapalsendertype/createTapalSenderType.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/tapalsendertype/viewTapalSenderType.xhtml?faces-redirect=true;";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	Boolean addButtonFlag=true;

	@Getter
	@Setter
	Boolean editButtonFlag;

	@Getter
	@Setter
	Boolean deleteButtonFlag;

	@Getter
	@Setter
	TapalSenderType tapalSenderType,selectedTapalSenderType;

	@Getter
	@Setter
	TapalSenderType selectedtapalSenderType = new TapalSenderType();

	@Getter
	@Setter
	List<TapalSenderType> tapalSenderTypeList = new ArrayList<TapalSenderType>();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;
	
	@Getter
	@Setter
	LazyDataModel<TapalSenderType> lazyTapalSenderTypeList;
	
	@Getter
	@Setter
	private int tapalSenderTypeListSize;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	int totalRecords = 0;

	@PostConstruct
	public void init() {

		log.info(" CountryMasterBean init --->" + serverURL);
		loadValues();
		addTapalSenderType();
		getAllTapalSenderTypeList();
		loadLazyTapalSenderList();

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			tapalSenderType = new TapalSenderType();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String addTapalSenderType() {
		log.info("<---- Start CountryMasterBean.addCountry ---->");
		try {
			action = "ADD";
			tapalSenderType = new TapalSenderType();
			tapalSenderType.setActiveStatus(true);

			log.info("<---- End CountryMaster.addCountry ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		tapalSenderType = new TapalSenderType();
		return clear();

	}

	public String submit() {
		log.info("...... TapalSenderType submit begin ....");
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + appPreference.getOperationApiUrl() + "/tapalsendertype/create",
					tapalSenderType);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("TapalSenderType saved successfully");
					//errorMap.notify(baseDTO.getStatusCode());
					if(action.equals("ADD"))
					{
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.TAPAL_SENDER_TYPE_CREATE).getCode());

					}else
					{
						errorMap.notify(ErrorDescription.getError(MastersErrorCode.TAPAL_SENDER_UPDATED_SUCCESSFULLY).getCode());
					}
					showList();
					return clear();
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					//errorMap.notify(ErrorDescription.getError(MastersErrorCode.TAPAL_SENDER_TYPE_UPDATED_SUCCESSFULLY).getCode());
					//errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating TapalSenderType" + e);
			
		}
		log.info("...... TapalSenderType submit ended ....");
		return null;
	}

	private void getAllTapalSenderTypeList() {
		log.info("<<<< ----------Start TapalSenderType-getAllTapalSenderTypeList() ------- >>>>");
		try {
			tapalSenderTypeList = new ArrayList<TapalSenderType>();
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/tapalsendertype/getActiveTapalSenderType";
			baseDTO = httpService.get(url);
			tapalSenderType = new TapalSenderType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				tapalSenderTypeList = mapper.readValue(jsonResponse, new TypeReference<List<TapalSenderType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tapalSenderTypeList != null) {
			tapalSenderTypeListSize = tapalSenderTypeList.size();
		} else {
			tapalSenderTypeListSize = 0;
		}
		log.info("<<<< ----------End TapalSenderType . getAllTapalSenderTypeList ------- >>>>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TapalSenderType.onRowSelect ========>" + event);
		selectedtapalSenderType = ((TapalSenderType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends TapalSenderType.onRowSelect ========>");
	}

	public String editTapalSenderType() {
		log.info("<<<<< -------Start TapalSenderType called ------->>>>>> ");
		action = "EDIT";

		if (tapalSenderType == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}

		if (tapalSenderType.getActiveStatus().equals("ACTIVE")) {
			tapalSenderType.setActiveStatus(tapalSenderType.getActiveStatus());
		} else {
			tapalSenderType.setActiveStatus(tapalSenderType.getActiveStatus());
		}
		log.info("<<<<< -------End TapalSenderType called ------->>>>>> ");

		return CREATE_PAGE;

	}

	public String viewTapalSenderType() {
		try {
			action = "View";
			if (tapalSenderType == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			else
			{
				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	private void getTapalSenderTypeView() {
		log.info("<<<< ----------Start TapalSenderTypeBean . getTapalSenderTypeView() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();

			String url = serverURL + appPreference.getOperationApiUrl() + "/tapalsendertype/gettapalSenderType";
			baseDTO = httpService.post(url, tapalSenderType);
			tapalSenderType = new TapalSenderType();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				tapalSenderType = mapper.readValue(jsonResponse, new TypeReference<TapalSenderType>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (tapalSenderType != null)
			log.info("TapalSenderTypeBean getTapalSenderTypeView" + tapalSenderType);
		else
			log.info("TapalSenderTypeBean getTapalSenderTypeView" + tapalSenderType);

		log.info("<<<< ----------End TapalSenderTypeBean . getTapalSenderTypeView ------- >>>>");
	}

	public String showList() {
		log.info("<---------- Loading TapalSenderTypeBean showList list page---------->");
		loadValues();
		getAllTapalSenderTypeList();
		loadLazyTapalSenderList();
		addButtonFlag = true;
		return LIST_PAGE;
	}

	public String backPage() {
		showList();
		return LIST_PAGE;
	}

	public String deleteTapalSenderType() {
		try {
			action = "Delete";
			if (tapalSenderType == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTapalDeliverTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start TapalSenderTypeBean . clear ----->");
		getAllTapalSenderTypeList();
		loadLazyTapalSenderList();
		tapalSenderType = new TapalSenderType();
		addButtonFlag = true;
		log.info("<---- End TapalSenderTypeBean . clear ----->");

		return LIST_PAGE;

	}

	public String deleteTapalSenderTypeConfirm() {

		try {
			log.info("Delete confirmed for Tapal Sender Type [" + tapalSenderType.getName() + "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/tapalsendertype/delete";
			baseDTO = httpService.post(url, tapalSenderType);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("Tapal Sender Type deleted successfully....!!!");
					//errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.TAPAL_SENDER_DELETE_SUCCESSFULLY).getCode());
					tapalSenderType = new TapalSenderType();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					//errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.TAPAL_SENDER_DELETE_SUCCESSFULLY).getCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	
	public void loadLazyTapalSenderList() {
		log.info("<===== Starts TapalSenderTypeBean.loadLazyTapalSenderTypeList ======>");
		lazyTapalSenderTypeList = new LazyDataModel<TapalSenderType>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TapalSenderType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl() +"/tapalsendertype/search";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						tapalSenderTypeList = mapper.readValue(jsonResponse,new TypeReference<List<TapalSenderType>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyTapalSenderTypeList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return tapalSenderTypeList;
			}

			@Override
			public Object getRowKey(TapalSenderType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TapalSenderType getRowData(String rowKey) {
				try {
					for (TapalSenderType glAccountCategory : tapalSenderTypeList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedTapalSenderType = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends TapalSenderTypeBean.loadLazyTapalSenderTypeList ======>"+totalRecords);
	}

}
