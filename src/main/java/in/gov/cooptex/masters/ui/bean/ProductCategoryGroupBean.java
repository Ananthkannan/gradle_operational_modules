package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("productCategoryGroupBean")
public class ProductCategoryGroupBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private final String CREATE_PRODUCT_CATEGORY_GROUP_PAGE = "/pages/masters/createProductCategoryGroup.xhtml?faces-redirect=true;";
	private final String LIST_PRODUCT_CATEGORY_GROUP_PAGE = "/pages/masters/listProductCategoryGroup.xhtml?faces-redirect=true;";
	private final String VIEW_PRODUCT_CATEGORY_GROUP_PAGE = "/pages/masters/viewProductCategoryGroup.xhtml?faces-redirect=true;";
	final String SERVER_URL = AppUtil.getPortalServerURL();
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	int totalRecords = 0;
	ObjectMapper mapper = new ObjectMapper();
	String jsonResponse;

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	ProductCategoryGroup productCategoryGroup;
	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();
	@Getter
	@Setter
	LazyDataModel<ProductCategoryGroup> lazyProductCategoryGroupList;
	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList = new ArrayList<ProductCategoryGroup>();

	public String showProductCategoryGroupListPage() {
		log.info("<==== Starts ProductCategoryGroupBean.showProductCategoryGroupListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		productCategoryGroup = new ProductCategoryGroup();
		selectedProductCategoryGroup = new ProductCategoryGroup();
		loadLazyProductCategoryGroupList();
		log.info("<==== Ends ProductCategoryGroupBean.showProductCategoryGroupListPage =====>");
		return LIST_PRODUCT_CATEGORY_GROUP_PAGE;
	}

	private void loadLazyProductCategoryGroupList() {
		log.info("<===== Starts ProductCategoryGroupBean.loadLazyProductCategoryGroupList ======>");
		lazyProductCategoryGroupList = new LazyDataModel<ProductCategoryGroup>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProductCategoryGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/categoryGroup/getproductCategoryGrouplist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						productCategoryGroupList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductCategoryGroup>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return productCategoryGroupList;
			}

			@Override
			public Object getRowKey(ProductCategoryGroup res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductCategoryGroup getRowData(String rowKey) {
				try {
					for (ProductCategoryGroup productCategoryGroup : productCategoryGroupList) {
						if (productCategoryGroup.getId().equals(Long.valueOf(rowKey))) {
							selectedProductCategoryGroup = productCategoryGroup;
							return productCategoryGroup;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ProductCategoryGroupBean.loadLazyProductCategoryGroupList ======>");
	}

	public String addProductCategoryGroup() {

		log.info("<--- Start ProductCategoryGroupBean.addProductCategoryGroup ---->");
		try {
			action = "ADD";
			productCategoryGroup = new ProductCategoryGroup();
			productCategoryGroup.setActiveStatus(true);

			log.info("<---- End ProductCategoryGroupBean.addProductCategoryGroup ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PRODUCT_CATEGORY_GROUP_PAGE;
	}

	public String submit() {
		log.info("<=======Starts  ProductCategoryGroupBean.submit ======>");

		if (productCategoryGroup.getGroupCode() == null || productCategoryGroup.getGroupCode().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_CODE_REQUIRED.getErrorCode());
			return null;
		}

		if (productCategoryGroup.getGroupName() == null || productCategoryGroup.getGroupName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_NAME_REQUIRED.getErrorCode());
			return null;
		}
		if (productCategoryGroup.getRegionalName() == null || productCategoryGroup.getRegionalName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_LNAME_REQUIRED.getErrorCode());
			return null;
		}

		if (action.equalsIgnoreCase("ADD")) {
			productCategoryGroup.setCreatedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("EDIT")) {
			productCategoryGroup.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL + "/categoryGroup/create", productCategoryGroup);
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_UPDATE_SUCCESS.getErrorCode());
			return showProductCategoryGroupListPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends ProductCategoryGroupBean.submit ======>");
		return null;
	}

	public String clear() {
		log.info("<---- Start ProductCategoryGroupBean.clear ----->");
		showProductCategoryGroupListPage();
		log.info("<---- End ProductCategoryGroupBean.clear ----->");
		return LIST_PRODUCT_CATEGORY_GROUP_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ProductCategoryGroupBean.onRowSelect ========>" + event);
		selectedProductCategoryGroup = ((ProductCategoryGroup) event.getObject());
		log.info("selectedProductCategoryGroup id" + selectedProductCategoryGroup.getId());
		addButtonFlag = true;
		log.info("<===Ends ProductCategoryGroupBean.onRowSelect ========>");
	}

	public String deleteProductCategoryGroup() {
		try {
			action = "Delete";
			if (selectedProductCategoryGroup == null || selectedProductCategoryGroup.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_PRODUCT_CATEGORY_GROUP.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProductCategoryGroupDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteProductCategoryGroupConfirm() {
		log.info("<===== Starts ProductCategoryGroupBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/categoryGroup/delete/" + selectedProductCategoryGroup.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_GROUP_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmProductCategoryGroupDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete BankMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ProductCategoryGroupBean.deleteConfirm ===========>");
		return showProductCategoryGroupListPage();
	}

	public String viewProductCategoryGroup() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedProductCategoryGroup == null || selectedProductCategoryGroup.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT_CATEGORY_GROUP.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedProductCategoryGroup.getId());

		selectedProductCategoryGroup = getProductCategoryGroupDetails(selectedProductCategoryGroup.getId());

		log.info("selected ProductCategoryGroup Object:::" + selectedProductCategoryGroup);
		return VIEW_PRODUCT_CATEGORY_GROUP_PAGE;
	}

	public String editProductCategoryGroup() {

		action = "EDIT";
		log.info("<----Redirecting to user edit page---->");
		if (selectedProductCategoryGroup == null || selectedProductCategoryGroup.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT_CATEGORY_GROUP.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedProductCategoryGroup.getId());

		selectedProductCategoryGroup = getProductCategoryGroupDetails(selectedProductCategoryGroup.getId());

		log.info("selected ProductCategoryGroup Object:::" + selectedProductCategoryGroup);
		return CREATE_PRODUCT_CATEGORY_GROUP_PAGE;
	}

	private ProductCategoryGroup getProductCategoryGroupDetails(Long id) {
		log.info("<====== ProductCategoryGroupBean.getProductCategoryGroupDetails starts====>" + id);
		try {
			if (selectedProductCategoryGroup == null || selectedProductCategoryGroup.getId() == null) {
				log.error(" No ProductCategoryGroup selected ");
				errorMap.notify(ErrorDescription.SELECT_PRODUCT_CATEGORY_GROUP.getErrorCode());
			}
			log.info("selectedProductCategoryGroup  Id : : " + selectedProductCategoryGroup.getId());
			String url = SERVER_URL + "/categoryGroup/get/" + selectedProductCategoryGroup.getId();

			log.info("ProductCategoryGroup Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ProductCategoryGroup productCategoryGroupDetail = mapper.readValue(jsonResponse,
					ProductCategoryGroup.class);
			if (productCategoryGroupDetail != null) {
				productCategoryGroup = productCategoryGroupDetail;
			} else {
				log.error("productCategoryGroup object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" productCategoryGroup Retrived  SuccessFully ");
			} else {
				log.error(" productCategoryGroup Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return productCategoryGroup;
		} catch (Exception e) {
			log.error("Exception Ocured while get productCategoryGroup Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

}
