package in.gov.cooptex.admin.meeting.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.meeting.model.Meeting;
import in.gov.cooptex.admin.meeting.model.MeetingAttendees;
import in.gov.cooptex.admin.meeting.model.MeetingReportRequestDto;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.meetingreport.dto.MeetingReportDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppFeatureCode;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.personnel.hrms.dto.CircularListDTO;
import in.gov.cooptex.personnel.hrms.dto.ExternalEmployees;
import in.gov.cooptex.personnel.hrms.dto.MeetingListDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("meetingReportBean")
@Scope("session")
public class MeetingReportBean extends CommonBean implements Serializable  {

	/**
	 * `
	 *
	 */
	private static final long serialVersionUID = 2264546941281084511L;

	final String LIST_PAGE = "/pages/admin/meeting/listMeetingReport.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/admin/meeting/createMeetingReport.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/admin/meeting/viewMeetingReport.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Long totalatendees = null;

	@Getter
	@Setter
	List<MeetingAttendees> meetingAttendeesList = new ArrayList<MeetingAttendees>();
	
	@Getter
	@Setter
	List<ExternalEmployees> extenalEmployeeList = new ArrayList<>();
	
	@Getter
	@Setter
	List<MeetingAttendees> selectedMeetingAttendeesList = new ArrayList<MeetingAttendees>();

	@Getter
	@Setter
	List<MeetingListDTO> meetingList = new ArrayList<MeetingListDTO>();
	
	@Getter
	@Setter
	List<Designation> designationList = new ArrayList<>();
	
	@Getter
	@Setter
	List<Long> selectDesignationList = new ArrayList<>();
	

	@Getter
	@Setter
	Meeting meeting, selectedMeeting;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();
	
	@Getter
	@Setter
	List<EmployeeMaster> addEmployeeMasterList = new ArrayList<EmployeeMaster>();
	
	@Getter
	@Setter
	List<EmployeeMaster> empMasterList = new ArrayList<EmployeeMaster>();
	
	@Getter
	@Setter
	List<EmployeeMaster> selectedEmployeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	EmployeeMaster selectedEmployeeMaster = new EmployeeMaster();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	LazyDataModel<MeetingListDTO> lazymeetingList;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter
	@Setter
	Boolean previousApproval = false;
	
	@Getter
	@Setter
	Boolean rejectFlag = false;
	
	@Getter
	@Setter
	String action;
	
	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();
	
	@Getter
	@Setter
	MeetingListDTO selectedeeting=new MeetingListDTO();
	
	@Getter
	@Setter
	private UploadedFile relievingLetterFile;
	
	@Getter
	@Setter
	MeetingReportRequestDto meetingReportRequestDto=new MeetingReportRequestDto();
	
	@Getter
	@Setter
	String fileName="";
	
	@Getter
	@Setter
	String filePath="";
	
	@Getter
	@Setter
	MeetingReportDTO viewMeeting;
	
	@Setter
	@Getter
	private StreamedContent file;
	
	@Getter
	@Setter
	List<UserMaster> userMasters;
	
	@Getter
	@Setter
	String remarkscomment;
	
	@Getter
	@Setter
	String stage;
	
	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;
	
	@Getter
	@Setter
	Boolean approveRejectStatus = false;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	String changeCurrentStatus;
	
	@Getter
	@Setter
	MeetingReportRequestDto newMeetingReportRequestDto=new MeetingReportRequestDto();
	
	@Getter
	@Setter
	Long precentEmployeeCount = 0l;
	
	@Getter
	@Setter
	Long appsentEmployeeCount = 0l;
	
	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};
	
	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;
	
	@Getter
	@Setter
	List<MeetingAttendees> selectedMeetingAttList = new ArrayList<MeetingAttendees>();
	
	@Getter
	@Setter
	Boolean hideflag = false;
	
	

	public String showMeetingRepostList() {
		departmentList=commonDataService.getDepartment();
		loadLazyMeetingList();

		return LIST_PAGE;
	}
	
	public String selectedMeetingDateformat(Date d)

    {
        log.info("<<<< ----------Starts MeetingDate formate changing------ >>>>");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

        log.info("<<<< ----------Ends MeetingDate formate changing------ >>>>");

        return simpleDateFormat.format(d);

    }

    public String meetingStartTimeFormat(Date d)
    {
        log.info("<<<< ----------Starts MeetingStartTime formate changing------ >>>>");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

        log.info("<<<< ----------Ends MeetingStartTime formate changing------ >>>>");
        return simpleDateFormat.format(d);
    }

    public String meetingEndTimeFormat(Date d)
    {
        log.info("<<<< ----------Starts MeetingSEndTime formate changing------ >>>>");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

        log.info("<<<< ----------Ends MeetingSEndTime formate changing------ >>>>");
        return simpleDateFormat.format(d);
    }
 
	public void attendedNominees() {
		log.info("Enter into attended Nominees:::::");
		try {
			selectedMeetingAttList = new ArrayList<MeetingAttendees>();
			BaseDTO response = httpService.get(SERVER_URL + "/meetingreport/getmeetingattendees/"+selectedeeting.getId());
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				newMeetingReportRequestDto = mapper.readValue(jsonResponse, new TypeReference<MeetingReportRequestDto>() {
				});
				meetingAttendeesList=newMeetingReportRequestDto.getNewMeetingAttendees();
				selectedMeetingAttList.addAll(meetingAttendeesList);
				addEmployeeMasterList=newMeetingReportRequestDto.getEmployeeMasterList();
				log.info("MeetingAttendeesList List is" + meetingAttendeesList);
				
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.info("Exception occured in attendedNominees exception is" + e);
		}
		log.info("Exit into attended Nominees:::::");
	}

	public void employeeList() {
		log.info("Enter into employeeList:::::");
		try {
			employeeMasterList =commonDataService.getAllEmployee();
			
			addEmployeeMasterList=commonDataService.getAllEmployee();
			 
			createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();

		} catch (Exception e) {
			log.info("Exception Occured in employeeList:::::");
			log.info("Exception is:::::" + e);

		}
		log.info("Exit into employeeList:::::");
	}

	
	public void addAttendees() {
		log.info("Enter into addAttendees:::::");
		try {
			List<EmployeeMaster> empList=new ArrayList<EmployeeMaster>();
			for (MeetingAttendees ma : meetingAttendeesList) {
				if(ma.getEmpMaster()!=null) {
					empList.add(ma.getEmpMaster());
				}
			}
			
			selectedMeetingAttendeesList=new ArrayList<MeetingAttendees>();
			Iterator<EmployeeMaster> employeemasterListItr=selectedEmployeeMasterList.iterator();
			while(employeemasterListItr.hasNext())
			{
				EmployeeMaster employeeMaster=employeemasterListItr.next();
				if(!empList.contains(employeeMaster)) {
				MeetingAttendees meetingAttendees = new MeetingAttendees();
				meetingAttendees.setAttended(true);
				meetingAttendees.setName(employeeMaster.getFirstName()+" "+employeeMaster.getLastName());
				/*if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getDepartment() != null) {
					meetingAttendees.setDepartment(employeeMaster.getPersonalInfoEmployment().getDepartment());
				}
				if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getSectionMaster() != null) {
					meetingAttendees.setSection(employeeMaster.getPersonalInfoEmployment().getSectionMaster());
				}
				if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getDesignation() != null) {
					meetingAttendees.setDesignation(employeeMaster.getPersonalInfoEmployment().getDesignation().getName());
				}*/
				if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getCurrentDepartment() != null) {
					meetingAttendees.setDepartment(employeeMaster.getPersonalInfoEmployment().getCurrentDepartment());
				}
				if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getCurrentSection() != null) {
					meetingAttendees.setSection(employeeMaster.getPersonalInfoEmployment().getCurrentSection());
				}
				if(employeeMaster.getPersonalInfoEmployment() != null && employeeMaster.getPersonalInfoEmployment().getCurrentDesignation() != null) {
					meetingAttendees.setDesignation(employeeMaster.getPersonalInfoEmployment().getCurrentDesignation().getName());
				}
					
				meetingAttendees.setEmpMaster(employeeMaster);
				selectedMeetingAttendeesList.add(meetingAttendees);
				}
			}
		} catch (Exception e) {
			log.info("Exception Occured in addAttendees:::::");
			log.info("Exception is:::::" + e);

		}
		log.info("Exit into addAttendees:::::");
	}

	public void loadLazyMeetingList() {
	    log.info("<<<< ----------Starts lazyLoadMeetingList------- >>>>");

	    lazymeetingList = new LazyDataModel<MeetingListDTO>() {
	        private static final long serialVersionUID = 8422543223567350599L;

	        @Override
	        public List<MeetingListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
	                                                Map<String, Object> filters) {
	            try {
	                PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
	                        sortOrder.toString(), filters);
	                log.info("Pagination request :::" + paginationRequest);
	                String url = SERVER_URL + "/meetingreport/getlistmeeting";
	                BaseDTO response = httpService.post(url, paginationRequest);
	                if (response != null && response.getStatusCode() == 0) {
	                    ObjectMapper mapper= new ObjectMapper();
	                    jsonResponse = mapper.writeValueAsString(response.getResponseContents());
	                            meetingList = mapper.readValue(jsonResponse,
	                            new TypeReference<List<MeetingListDTO>>() {
	                            });
	                    this.setRowCount(response.getTotalRecords());
	                    totalRecords = response.getTotalRecords();
	                } else {
	                    meetingList = new ArrayList<>();
	                    errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	                }
	            } catch (Exception e) /*
	             * showBloodGroupListPageAction method used to action based view pages and
	             * render viewing
	             */ {
	                log.error("Exception occured in meetingList ...", e);
	                errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	            }
	            log.info("Ends lazy load....");
	            return meetingList;
	        }

	        @Override
	        public Object getRowKey(MeetingListDTO res) {
	            return res != null ? res.getId() : null;
	        }

	        @Override
	        public MeetingListDTO getRowData(String rowKey) {
	            try {
	                for (MeetingListDTO invoice : meetingList) {
	                    if (invoice.getId().equals(Long.valueOf(rowKey))) {
	                        selectedeeting = invoice;
	                        return invoice;
	                    }
	                }
	            } catch (Exception e) {
	                log.error("Exception occured in getRowData ...", e);
	                errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	            }
	            return null;
	        }

	    };
	    log.info("<<<< ----------Ends lazyLoadMeetingList------- >>>>");

}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts MeetingReportBean.onRowSelect ========>" + event);
		selectedeeting = ((MeetingListDTO) event.getObject());
		
		if(selectedeeting.getStatus().equalsIgnoreCase("FINAL_APPROVED"))
		{
		 editButtonFlag = false;	
		 addButtonFlag = true;
		 viewButtonFlag= false;
		}
		else if(selectedeeting.getStatus().equalsIgnoreCase("REPORT_SUBMITTED")
				|| (selectedeeting.getStatus().equalsIgnoreCase("REPORT_FINAL_APPROVED")
				||(selectedeeting.getStatus().equalsIgnoreCase("REPORT_APPROVED"))) )
		{
			editButtonFlag = false;	
			 addButtonFlag = false;
			 viewButtonFlag= true;
		}
		else if(selectedeeting.getStatus().equalsIgnoreCase("REPORT_REJECTED"))
		{
			editButtonFlag = true;	
			 addButtonFlag = false;
			 rejectFlag=true;
			 viewButtonFlag= true;
		}
		else if(selectedeeting.getStatus().equalsIgnoreCase("REPORT_APPROVED"))
		{
		 editButtonFlag = false;
		 addButtonFlag = false;
		 viewButtonFlag= true;
		}else {
			viewButtonFlag= true;
		}
		log.info("<===Ends MeetingReportBean.onRowSelect ========>");
	}
    
	public void cancelviewnote() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
	}

	
	
	public String showMeetingListPageAction() {
		log.info("<===== Starts MeetingBean.showAccountCategoryListPageAction ===========>" + action);
		try {
			if(selectedeeting == null)
			{
				errorMap.notify(ErrorDescription.SELECT_ONE.getCode());
				return null;
			}
			empMasterList = new ArrayList<>();
			selectDesignationList = new ArrayList<>();
			selectedEmployeeMasterList = new ArrayList<>();
			designationList = commonDataService.loadAllDesignationList();

			if(designationList != null && !designationList.isEmpty()) {
				log.info("designationList size==> "+designationList.size());
			}
			if (action.equalsIgnoreCase("CREATE")) {
				addButtonFlag = true;
			    editButtonFlag = false;
			    userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureCode.MEETING_REPORT);
				log.info("forward to user info" + userMasters.size());
				
				String viewurl = SERVER_URL + "/meetingreport/getbyid/" + selectedeeting.getId()+"/"+(selectedNotificationId!=null?selectedNotificationId:0);
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					viewMeeting = mapper.readValue(jsonResponse, new TypeReference<MeetingReportDTO>() {
					});

				}
				//loadEmployeeAttendes();

				extenalEmployeeList = new ArrayList<>();
				ExternalEmployees extremplyes = new ExternalEmployees();
				extenalEmployeeList.add(extremplyes);
				attendedNominees();
				employeeList();
				log.info("create Meeting called..");
				meeting = new Meeting();
				userMaster = new UserMaster();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
			    addButtonFlag = false;
			    userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureCode.MEETING_REPORT);
			    editButtonFlag = true;
			    if(selectedeeting == null)
				{
					errorMap.notify(ErrorDescription.SELECT_ONE.getCode());
				}
				String viewurl = SERVER_URL + "/meetingreport/getbyid/" + selectedeeting.getId()+"/"+(selectedNotificationId!=null?selectedNotificationId:0);
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
		
					viewMeeting = mapper.readValue(jsonResponse, new TypeReference<MeetingReportDTO>() {
					});
				}
				loadEmployeeAttendes();
				if(null == extenalEmployeeList || extenalEmployeeList.size() == 0){
				extenalEmployeeList = new ArrayList<>();
				ExternalEmployees extremplyes = new ExternalEmployees();
				extenalEmployeeList.add(extremplyes);
				}
				attendedNominees();
				employeeList();
				log.info("create Meeting called..");
				meeting = new Meeting(); 
				return ADD_PAGE;
			}else if (action.equalsIgnoreCase("VIEW")) {
				 userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureCode.MEETING_REPORT);
				setApprovalFlag(false);
				if(selectedeeting == null)
				{
					errorMap.notify(ErrorDescription.SELECT_ONE.getCode());
				}
				log.info("view Meeting called..");
				String viewurl = SERVER_URL + "/meetingreport/getbyid/" + selectedeeting.getId()+"/"+(selectedNotificationId!=null?selectedNotificationId:0);
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					viewMeeting = mapper.readValue(jsonResponse, new TypeReference<MeetingReportDTO>() {
					});

					loadEmployeeAttendes();
					
					
					commentAndLogList =viewresponse.getCommentAndLogList();

					previousApproval = viewMeeting.getForwardFor();
					filePath=viewMeeting.getDownloadExtensionType();
					fileName=viewMeeting.getFileName();
					if(viewMeeting!=null && viewMeeting.getNote()!=null) {
					meetingReportRequestDto.setNote(viewMeeting.getNote());
					}
					if(viewMeeting.getForwardTo() !=null) {
					meetingReportRequestDto.setForwardTo(userMaster.getId());
					}
					if (viewMeeting.getForwardTo().equals(loginBean.getUserDetailSession().getId())) {
						log.info("============approval flag===============");
						if(viewMeeting.getForwardFor()) {
							userMaster.setId(viewMeeting.getForwardTo());
						}
						setApprovalFlag(true);
					}
					setFinalapprovalFlag(viewMeeting.getForwardFor());
					
					if(viewMeeting.getStatus().equals("REPORT_FINAL_APPROVED") || viewMeeting.getStatus().equals("REPORT_REJECTED")  )
					{
						//finalapprovalFlag = true;
						//approveRejectStatus = true;
						setApprovalFlag(false);
					}
					if(finalapprovalFlag.equals(true)) {
						setHideflag(true);
					}else {
						setHideflag(false);
					}
					
					return VIEW_PAGE;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends MeetingBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}
	
	public void addRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		log.info("-------extenalEmployeeList.get(rowIndex).getEmpName()----------------------->"
				+ extenalEmployeeList.get(rowIndex).getEmpName());
		if (extenalEmployeeList.get(rowIndex).getEmpName().equalsIgnoreCase(null)
				|| extenalEmployeeList.get(rowIndex).getEmpName().equalsIgnoreCase("")) {
			log.info("<----------inside empty emp name ------>");
			errorMap.notify(ErrorDescription.MEETING_REQUEST_EXTERNAL_NAME_EMPTY.getCode());
			return;
		} else {
			ExternalEmployees extremplyes = new ExternalEmployees();
			extenalEmployeeList.add(extremplyes);
		}
	}
	
	public void deleteRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		extenalEmployeeList.remove(rowIndex);

		if (extenalEmployeeList.size() == 0) {
			ExternalEmployees extremplyes = new ExternalEmployees();
			extenalEmployeeList.add(extremplyes);
		}

	}
	
	public void loadEmployeeAttendes() {
		precentEmployeeCount = 0l;
		appsentEmployeeCount = 0l;
		
		if(viewMeeting.getMeetingAttendees() != null && !viewMeeting.getMeetingAttendees().isEmpty()) {
			for(MeetingAttendees meetingAttendees : viewMeeting.getMeetingAttendees()) {
				log.info("Attended employee status== > "+meetingAttendees.getAttended());
				if(meetingAttendees.getAttended() == true) {
					precentEmployeeCount = precentEmployeeCount+1;
				}else {
					appsentEmployeeCount = appsentEmployeeCount+1;
				}
			}
		}
		log.info("precentEmployeeCount==> "+precentEmployeeCount);
		log.info("appsentEmployeeCount==> "+appsentEmployeeCount);
	}
	
	public String clear()
	{
		selectedMeeting=null;
		addButtonFlag = true;	
		editButtonFlag= true;
		viewButtonFlag= true;
		loadLazyMeetingList();
		selectedeeting=null;
		fileName = null;
		meetingReportRequestDto = new MeetingReportRequestDto();
		selectedMeetingAttendeesList = new ArrayList<>();
		extenalEmployeeList = new ArrayList<>();
		return LIST_PAGE;
	}
	

	
	public void uplaodMeetingReportDocuments(FileUploadEvent event) {
		log.info("uplaodMeeting Report Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.MEETING_REPORT_DOCUMENTS_NOT_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			Double size = (double) relievingLetterFile.getSize();
			log.info("uploadFile size==>" + size);
			size = size / 1000;
			
			String photoPathName = commonDataService.getAppKeyValue("MEETING_REPORT_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			meeting.setReportFilePath(filePath);
			fileName = relievingLetterFile.getFileName();
			meeting.setReportFileName(fileName);
			meeting.setReportFileSize(size);
			errorMap.notify(ErrorDescription.MEETING_REPORT_DOCUMENT_SUCCESS.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}
	
	public String saveMeetingReport() {
		log.info("Enter into saveMeetingReport ::::...."+meeting);
		try {
			//Boolean checkFlag = false;
			meetingAttendeesList = new ArrayList<MeetingAttendees>();
			meetingAttendeesList.addAll(selectedMeetingAttList);
			if(meetingAttendeesList != null || !meetingAttendeesList.isEmpty()) {
				for(MeetingAttendees meetingAttendees : meetingAttendeesList) {
					//if(meetingAttendees.getAttended() == true) {
						 //checkFlag = true;
					     meetingAttendees.setAttended(true);
						//break;
					//}
				}
			}
			if(meetingAttendeesList==null || meetingAttendeesList.isEmpty()) {
				//AppUtil.addError("Please select meeting attend employee");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.PLSASE_SELSECT_MEETING_ATTEND_EMPLOYEE).getCode());
				return null;
			}
			if(fileName == null || fileName.isEmpty()) {
				//AppUtil.addError("Minutes upload should not be empty");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.MINUTES_UPLOADED_SHOULD_NOT_EMPTY).getCode());
				return null;
			}
			Validate.notNullOrEmpty(userMaster,
					ErrorDescription.MEETING_REQUEST_FORWARD_TO_EMPTY);
			Validate.notNullOrEmpty(meetingReportRequestDto.getForwardFor(),
					ErrorDescription.MEETING_REQUEST_FORWARD_FOR_EMPTY);
			if(meetingReportRequestDto.getNote() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITIONAL_EARNINGS_DETAILS_NOTE_NOT_FOUND).getCode());
				return null;
			}
			
			meetingReportRequestDto.setMeetingAttendies(meetingAttendeesList);
			meetingReportRequestDto.setExternalMeetingAttendies(selectedMeetingAttendeesList);
			meetingReportRequestDto.setUploadFileName(meeting.getReportFileName());
			meetingReportRequestDto.setUploadFilePath(meeting.getReportFilePath());
			meetingReportRequestDto.setUploadFileSize(meeting.getReportFileSize());
			//meetingReportRequestDto.setExternalMeetingAttendies(extenalEmployeeList);
			if (extenalEmployeeList.size() > 0) {
				for (ExternalEmployees externalEmployeeObj : extenalEmployeeList) {
					if (!externalEmployeeObj.getLandLineNumber().equalsIgnoreCase(null) && !externalEmployeeObj.getLandLineNumber().equalsIgnoreCase("")) {
						if (!EmployeeValidator.validateMobileNo(externalEmployeeObj.getLandLineNumber())) {
							log.error(" Employee Invalid Contact Number ");
							errorMap.notify(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getErrorCode());
							return null;
						}
					}
					if (!externalEmployeeObj.getEmailId().equalsIgnoreCase(null) && !externalEmployeeObj.getEmailId().equalsIgnoreCase("")) {
						if (!EmployeeValidator.validateEmail(externalEmployeeObj.getEmailId())) {
							log.error(" Employee Invalid Email Id ");
							errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getErrorCode());
							return null;
						}
					}
					if (!externalEmployeeObj.getEmpName().equalsIgnoreCase(null) && !externalEmployeeObj.getEmpName().equalsIgnoreCase("")) {
						meetingReportRequestDto.getExternalEmployess().add(externalEmployeeObj);
					}
				}
			}
			meetingReportRequestDto.setId(selectedeeting.getId());
			String saveUrl = SERVER_URL + "/meetingreport/save";
			if(userMaster!=null && userMaster.getId()!=null)
			{
				meetingReportRequestDto.setForwardTo(userMaster.getId());
			}

			BaseDTO response = httpService.post(saveUrl,meetingReportRequestDto);
			if(response !=null && response.getStatusCode()==0)
			{
				if(action.equalsIgnoreCase("CREATE")) {
				errorMap.notify(ErrorDescription.MEETING_REPORT_INSERTED_SUCCESSFULLY.getErrorCode());
				log.info("Meeting Report Data Added Successfully ::::::::");
				}else {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MEETING_REPORT_UPDATED_SUCCESSFULLY).getCode());
					log.info("Meeting Report Data Added Successfully ::::::::");
				}
				clear();
				return LIST_PAGE;
			}
			else
			{
				log.info("Status Code is ::::::::"+response.getStatusCode());
				errorMap.notify(response.getStatusCode());
				
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		
		log.info("Exit into saveMeetingReport ::::....");
		return null;
	}
	
	public void downloadfile() {
		InputStream input = null;
		try {
			File files = new File(filePath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file=(new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
		catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}
		
	}
	
	// status method
	public String submitApproveRejectStatus() {

		log.info("Start Meeting Report Bean approveRejectStatus method------- ");
		try {

			if(changeCurrentStatus != null)
			{
				if (changeCurrentStatus.equalsIgnoreCase("APPROVED")) {
					meetingReportRequestDto.setStage("REPORT_APPROVED");
				} else if (changeCurrentStatus.equalsIgnoreCase("REJECTED")) {
					meetingReportRequestDto.setStage("REPORT_REJECTED");
				}
			}
			
			if(finalapprovalFlag !=null && finalapprovalFlag) {
				if(meetingReportRequestDto.getStage()!=null && !meetingReportRequestDto.getStage().equalsIgnoreCase("REPORT_REJECTED")) {
					meetingReportRequestDto.setStage("REPORT_FINAL_APPROVED");
				}
			}
			
			meetingReportRequestDto.setId(selectedeeting.getId());
			meetingReportRequestDto.setRemark(remarkscomment);
			meetingReportRequestDto.setForwardTo(userMaster.getId());
			meetingReportRequestDto.setFinalapproval(finalapprovalFlag);
			String viewurl = SERVER_URL + "/meetingreport/approvereject";
			BaseDTO viewresponse = httpService.post(viewurl, meetingReportRequestDto);
			if (viewresponse != null && viewresponse.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
				viewMeeting = mapper.readValue(jsonResponse, new TypeReference<MeetingReportDTO>() {
					
				});
				if (meetingReportRequestDto.getStage().equalsIgnoreCase("REPORT_FINAL_APPROVED")
						|| meetingReportRequestDto.getStage().equalsIgnoreCase("REPORT_APPROVED")) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MEETING_REPORT_APPROVED_SUCCESSFULLY).getCode());
				} else if (meetingReportRequestDto.getStage().equalsIgnoreCase("REPORT_REJECTED")) {
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.MEETING_REPORT_REJECT_SUCCESSFULLY).getCode());
				}
				
				//loadLazyMeetingList();
				 selectedeeting=new MeetingListDTO();
				return showMeetingRepostList();
				
			}

		} catch (Exception e) {
			log.error("Exceptin Occured in approveRejectStatus method------- ");
			log.error("Exceptin Exceptin is------- " + e);
			
		}
		log.error("End Meeting Report Bean approveRejectStatus method------- ");
		return null;
		
	}
	
	public void deleteMeetingAttendes(Long id) {
		log.info("Start Meeting Report Bean deleteMeetingAttendes method------- ");
		try {
			String viewurl = SERVER_URL + "/meetingreport/deleteattendee/"+id;
			BaseDTO viewresponse = httpService.get(viewurl);
			if (viewresponse != null && viewresponse.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
				viewMeeting = mapper.readValue(jsonResponse, new TypeReference<MeetingReportDTO>() {
				});
			}
		} catch (Exception e) {
			log.error("Exceptin Occured in deleteMeetingAttendes method------- ");
			log.error("Exceptin Exceptin is------- " + e);
		}
		log.error("End Meeting Report Bean deleteMeetingAttendes method------- ");
	}
	
	
	
	public void loadEmployeeByDes() {
		log.info("... getAllEmployee called ....");
		
		try {

			String URL = SERVER_URL + "/employee/getempbydesignationids";

			BaseDTO baseDTO = httpService.post(URL,selectDesignationList);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			empMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if (empMasterList != null && !empMasterList.isEmpty()) {
				log.info("empMasterList size==> "+empMasterList.size());
				for (MeetingAttendees ma : meetingAttendeesList) {
					if(ma.getEmpMaster()!=null) {
						empMasterList.remove(ma.getEmpMaster());
					}
				}
			}else {
				log.error("empMasterList null or not found");
			}
		} catch (Exception e) {
			log.error("Exception occured while getting loadEmployeeByDes....", e);
			
		}
		
	}
	@PostConstruct
	public String showViewListPage() {
		log.info("MeetingReportBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String meetingReportId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			meetingReportId = httpRequest.getParameter("meetingReportId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(meetingReportId)) {
			selectedeeting = new MeetingListDTO();
			action = "VIEW";
			selectedeeting.setId(Long.parseLong(meetingReportId));
			selectedNotificationId = Long.parseLong(notificationId);
			showMeetingListPageAction();
		}
		log.info("MeetingReportBean showViewListPage() Ends");
		addButtonFlag = true;
		return VIEW_PAGE;
	}

}
