package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ExamCentreSearchRequest;
import in.gov.cooptex.core.dto.ExamCentreSearchResponse;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ExamCentre;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.SubjectMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.JobAdvertisement;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("examCentreBean")
public class ExamCentreBean implements Serializable {

	private static final long serialVersionUID = -2424316751385252849L;

	public static final String EXAM_CENTRE_SERVER_URL = AppUtil.getPortalServerURL() + "/examcentre";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	public String action;

	String serverURL = null;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	ExamCentre examCentre = new ExamCentre();

	@Getter
	@Setter
	int totalRecords = 0;

	@Setter
	@Getter
	List<ExamCentre> examCentreList = new ArrayList<>();

	@Setter
	@Getter
	List<EntityMaster> preferedRegionList = new ArrayList<>();

	@Setter
	@Getter
	List<CountryMaster> countryMasterList = new ArrayList<>();

	@Setter
	@Getter
	List<StateMaster> stateMasterList = new ArrayList<>();

	@Setter
	@Getter
	List<DistrictMaster> districtMasterList = new ArrayList<>();

	@Setter
	@Getter
	List<TalukMaster> talukMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<SubjectMaster> subjectMasterList;

	@Getter
	@Setter
	List<JobAdvertisement> jobNotificationList;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	boolean editButtonFlag = false;

	@Getter
	@Setter
	boolean approveButtonFlag = false;

	@Getter
	@Setter
	boolean deleteButtonFlag = false;

	ObjectMapper mapper;

	@Getter
	@Setter
	ExamCentreSearchResponse selectedExamCentre;

	String url;

	@Getter
	@Setter
	private Date startTime, endTime;

	@PostConstruct
	public void init() {
		examCentre = new ExamCentre();
		loadMasterValue();
		loadLazyExamCentreList();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	}

	@Setter
	@Getter
	String FormattedTime = "HH:mm";

	@Getter
	@Setter
	LazyDataModel<ExamCentreSearchResponse> examCentreResponseLazyList;

	String jsonResponse;

	public void loadLazyExamCentreList() {

		log.info("< --  Start ExamCentre Lazy Load -- > ");
		examCentreResponseLazyList = new LazyDataModel<ExamCentreSearchResponse>() {

			private static final long serialVersionUID = -4079882975619852359L;

			@Override
			public List<ExamCentreSearchResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<ExamCentreSearchResponse> data = mapper.readValue(jsonResponse,
							new TypeReference<List<ExamCentreSearchResponse>>() {
							});
					if (data == null) {
						log.info(" ExamCentreSearchResponse Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Exam Centre Search Successfully Completed");
					} else {
						log.error(" Exam Centre Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Exam Centre data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(ExamCentreSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ExamCentreSearchResponse getRowData(String rowKey) {
				List<ExamCentreSearchResponse> responseList = (List<ExamCentreSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (ExamCentreSearchResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedExamCentre = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			ExamCentreSearchRequest request = new ExamCentreSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("region")) {
					log.info("region : " + value);
					request.setRegion(value);
				}
				if (entry.getKey().equals("examCentreName")) {
					log.info("examCentreName : " + value);
					request.setExamCentreName(value);
				}
				if (entry.getKey().equals("primaryContact")) {
					log.info("primaryContact : " + value);
					request.setPrimaryContact(value);
				}
				if (entry.getKey().equals("status")) {
					log.info("status : " + value);
					request.setStatus(value);
				}
				if (entry.getKey().equals("examDate")) {
					log.info("examDate : " + value);
					request.setExamDate(AppUtil.serverDateFormat(value));
				}

			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(EXAM_CENTRE_SERVER_URL + "/search", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in exam centre search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String showPage() {
		log.info(":: Exam Centre Page Button ::" + action);
		if (action.equalsIgnoreCase("ADD")) {
			examCentre = new ExamCentre();
			showTime();
			return "/pages/personnelHR/createExamCenter.xhtml?faces-redirect=true;";
		} else if (action.equalsIgnoreCase("EDIT")) {
			if (selectedExamCentre == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedExamCentre.getStatus().equals("APPROVED")) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else if (selectedExamCentre.getStatus().equals("REJECTED")) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			}
			examCentre = getById(selectedExamCentre.getId());
			onchangeState();
			onchangeDistrict();
			showTime();
			return "/pages/personnelHR/createExamCenter.xhtml?faces-redirect=true;";
		} else if (action.equalsIgnoreCase("CANCEL") || action.equalsIgnoreCase("EXAMLIST")) {
			selectedExamCentre = new ExamCentreSearchResponse();
			addButtonFlag = false;
			return "/pages/personnelHR/listExamCenter.xhtml?faces-redirect=true;";
		}
		return null;
	}

	private String getFormattedTime(Date time, String format) {
		if (time == null) {
			return null;
		}

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);

		return simpleDateFormat.format(time);
	}

	public void loadMasterValue() {
		log.info("loadMasterValue start");
		try {
			preferedRegionList = commonDataService.loadRegionalOffice();
			stateMasterList = commonDataService.getStateList();
			countryMasterList = commonDataService.loadCountry();
			subjectMasterList = commonDataService.loadSubject();
			jobNotificationList = commonDataService.loadJobNotificationNo();
			log.info("stateMasterList Obj :[" + stateMasterList);
		} catch (Exception e) {
			log.error("found exception while loadMasterValue ", e);
		}

	}

	public String deleteExamCentre() {
		try {
			if (selectedExamCentre == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				addButtonFlag = false;
				return null;
			} else if (selectedExamCentre.getStatus().equals("APPROVED")) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
		}
		return null;
	}

	public String deleteExamCentreeConfirm() {
		try {
			log.info("Selected ExamCentre  Id : : " + selectedExamCentre.getId());
			String getUrl = EXAM_CENTRE_SERVER_URL + "/delete/:id";
			url = getUrl.replace(":id", selectedExamCentre.getId().toString());
			log.info("ExamCentre Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("ExamCentre Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" ExamCentre Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EXAM_CENTRE_DELETED_SUCCESSFULLY.getErrorCode());
				addButtonFlag = false;
				selectedExamCentre = new ExamCentreSearchResponse();
				return "/pages/personnelHR/listExamCenter.xhtml?faces-redirect=true;";
			} else {
				log.error(" ExamCentre Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String viewExamCentre() {
		if (selectedExamCentre == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		try {
			examCentre = getById(selectedExamCentre.getId());
			return "/pages/personnelHR/viewExamCenter.xhtml?faces-redirect=true";
		} catch (Exception e) {
			log.error("exam centre view failed", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String approveExamCentre() {
		log.info("approve examination centre [" + selectedExamCentre + "]");
		if (selectedExamCentre == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		if (selectedExamCentre.getStatus().equalsIgnoreCase("APPROVED")) {
			errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
			return null;
		}

		if (selectedExamCentre.getStatus().equalsIgnoreCase("REJECTED")) {
			errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
			return null;
		}
		examCentre = getById(selectedExamCentre.getId());
		return "/pages/personnelHR/examCenterApproval.xhtml?faces-redirect=true";
	}

	public void onchangeState() {
		try {
			districtMasterList = commonDataService.loadDistrictByState(examCentre.getStateMaster().getId());

		} catch (Exception e) {
			log.error("found exception while state change", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onchangeDistrict() {
		try {
			if (examCentre.getDistrictMaster().getId() != null) {
				talukMasterList = commonDataService.loadTalukByDistrict(examCentre.getDistrictMaster().getId());
			}
		} catch (Exception e) {
			log.error("found exception while district change", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String saveExamCentre() {
		BaseDTO baseDto = new BaseDTO();
		log.info(":: Create Exam Centre start::");
		try {
			if (examCentre == null) {
				log.info(":: Create Exam Centre  request object nul l::");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (!examCentre.getSecondaryContactNumber().isEmpty()
					&& !validateMobileNumber(examCentre.getSecondaryContactNumber())) {

				return null;

			}
			if (examCentre.getPrimaryContactNumber() != null
					&& !validateMobileNumber(examCentre.getPrimaryContactNumber())) {

				return null;

			}

			String url = EXAM_CENTRE_SERVER_URL + "/create";
			log.info("Create Exam Centre  URL :" + url);
			baseDto = httpService.post(url, examCentre);
			log.info("Create Exam Centre BaseDTO response:" + baseDto);
			if (baseDto == null) {
				log.info(":: Exam centre details response null. ::" + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDto.getStatusCode() == 0) {
					log.info(":: Exam centre details saved successfully. ::" + baseDto);
					errorMap.notify(ErrorDescription.EXAM_CENTRE_SAVED_SUCCESSFULLY.getErrorCode());
					return "/pages/personnelHR/listExamCenter.xhtml?faces-redirect=true;";
				} else {
					log.info(":: Exam centre details failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(":: Exception in creating exam centre", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	public void approveOrRejectDialogue() {

		if (selectedExamCentre == null) {
			log.info(":: exam centre request object can not be null while approving or reject::");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			if (action.equals("APPROVED")) {
				examCentre.setStatus("APPROVED");
			} else if (action.equals("REJECTED")) {
				examCentre.setStatus("REJECTED");
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public ExamCentre getById(Long id) {
		ExamCentre examCentre = new ExamCentre();
		BaseDTO baseDto = new BaseDTO();
		try {
			url = AppUtil.getPortalServerURL() + "/examcentre/get/id/" + id;
			baseDto = httpService.get(url);
			if (baseDto != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDto.getResponseContent());
				examCentre = mapper.readValue(jsonValue, ExamCentre.class);
			}

		} catch (Exception e) {
			log.error("exception occured in getById", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return examCentre;
	}

	public boolean validateMobileNumber(Object value) {
		Pattern pattern = Pattern.compile("^[6789]\\d{9}$");
		Matcher matcher = pattern.matcher(value.toString());
		if (!matcher.matches()) {
			errorMap.notify(ErrorDescription.EXAM_CENTRE_MOBILE_VALID.getErrorCode());
			return false;
		}
		return true;
	}

	public String updateExamCentre() {
		BaseDTO baseDto = new BaseDTO();
		log.info("updateExamCentre ExamCentre start");
		try {
			if (examCentre != null) {
				if (action.equals("APPROVED")) {
					examCentre.setStatus("APPROVED");
				} else if (action.equals("REJECTED")) {
					examCentre.setStatus("REJECTED");
				}

				String url = EXAM_CENTRE_SERVER_URL + "/update";
				log.info("updateExamCentre ExamCentre URL :" + url);

				if (!examCentre.getSecondaryContactNumber().isEmpty()
						&& !validateMobileNumber(examCentre.getSecondaryContactNumber())) {

					return null;

				}
				if (examCentre.getPrimaryContactNumber() != null
						&& !validateMobileNumber(examCentre.getPrimaryContactNumber())) {

					return null;

				}
				baseDto = httpService.put(url, examCentre);
				log.info("Save Exam Centre BaseDTO response:" + baseDto);
				if (baseDto == null) {
					log.info(":: JApproveOrReject ExamCentre BaseDTO Response Null:: " + baseDto);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				} else {
					if (baseDto.getStatusCode() == 0) {
						log.info(":: updateExamCentre updatedsuccessfully. ::" + baseDto);
						if (examCentre.getStatus().equals("APPROVED")) {
							errorMap.notify(ErrorDescription.EXAM_CENTRE_APPROVED_SUCCESSFULLY.getErrorCode());
						} else if (examCentre.getStatus().equals("REJECTED")) {
							errorMap.notify(ErrorDescription.EXAM_CENTRE_REJECTED_SUCCESSFULLY.getErrorCode());
						}
						if (action.equals("EDIT")) {
							errorMap.notify(ErrorDescription.EXAM_CENTRE_UPDATED_SUCCESSFULLY.getErrorCode());
						}
						addButtonFlag = false;
						selectedExamCentre = new ExamCentreSearchResponse();
						return "/pages/personnelHR/listExamCenter.xhtml?faces-redirect=true";
					} else {
						log.info(":: update ExamCentre failed to save. ::" + baseDto);
						errorMap.notify(baseDto.getStatusCode());
						return null;
					}
				}

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error(":: Exception in Updating ExamCentre", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	private void showTime() {
		if (examCentre.getExamStartTime() == null && examCentre.getExamEndTime() == null) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.AM_PM, Calendar.AM);
			calendar.set(Calendar.HOUR, 8);
			calendar.set(Calendar.MINUTE, 15);
			startTime = calendar.getTime();
			log.info(":: Oral exam venue start time :: " + startTime);
			examCentre.setExamStartTime(startTime);

			calendar.set(Calendar.HOUR, 11);
			calendar.set(Calendar.MINUTE, 40);
			endTime = calendar.getTime();
			log.info(":: Oral exam venue end time :: " + endTime);
			examCentre.setExamEndTime(endTime);
		}
	}

	public String clear() {
		log.info(":: clear exam centre called..");
		selectedExamCentre = new ExamCentreSearchResponse();
		addButtonFlag = false;
		editButtonFlag = false;
		approveButtonFlag = false;
		deleteButtonFlag = false;
		return "/pages/personnelHR/listExamCenter.xhtml?faces-redirect=true";
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedExamCentre = ((ExamCentreSearchResponse) event.getObject());
		if (selectedExamCentre.getStatus().equalsIgnoreCase("INPROGRESS")) {
			addButtonFlag = true;
			editButtonFlag = false;
			approveButtonFlag = false;
			deleteButtonFlag = false;

		} else if (selectedExamCentre.getStatus().equalsIgnoreCase("APPROVED")) {
			addButtonFlag = true;
			editButtonFlag = true;
			approveButtonFlag = true;
			deleteButtonFlag = true;

		} else if (selectedExamCentre.getStatus().equalsIgnoreCase("REJECTED")) {
			addButtonFlag = true;
			editButtonFlag = true;
			approveButtonFlag = true;
			deleteButtonFlag = false;
		}
	}

}
