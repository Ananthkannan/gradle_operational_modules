package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("bankMasterBean")
@Scope("session")
public class BankMasterBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String BANK_MASTER_CREATE_PAGE = "/pages/masters/createBankMaster.xhtml?faces-redirect=true;";
	private final String BANK_MASTER_LIST_PAGE = "/pages/masters/listBankMaster.xhtml?faces-redirect=true;";
	private final String BANK_MASTER_VIEW_PAGE = "/pages/masters/viewBankMaster.xhtml?faces-redirect=true;";
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String jsonResponse;
	@Autowired
	AppPreference appPreference;
	@Getter
	@Setter
	int totalRecords = 0;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	HttpService httpService;
	ObjectMapper mapper;
	@Getter
	@Setter
	private String action;
	@Autowired
	LoginBean loginBean;
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	BankMaster bankMaster;
	@Getter
	@Setter
	BankMaster selectedBankMaster = new BankMaster();
	@Getter
	@Setter
	LazyDataModel<BankMaster> lazyBankMasterList;
	@Getter
	@Setter
	List<BankMaster> bankMasterList = new ArrayList<BankMaster>();

	public String showBankMasterListPage() {
		log.info("<==== Starts BankMasterBean.showBankMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		bankMaster = new BankMaster();
		selectedBankMaster = new BankMaster();
		loadLazyBankMasterList();
		log.info("<==== Ends BankMasterBean.showBankMasterListPage =====>");
		return BANK_MASTER_LIST_PAGE;
	}

	public void loadLazyBankMasterList() {
		log.info("<===== Starts BankMasterBean.loadLazyBankMasterList ======>");
		lazyBankMasterList = new LazyDataModel<BankMaster>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<BankMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/bankmaster/getbankmasterlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						bankMasterList = mapper.readValue(jsonResponse, new TypeReference<List<BankMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return bankMasterList;
			}

			@Override
			public Object getRowKey(BankMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public BankMaster getRowData(String rowKey) {
				try {
					for (BankMaster bankMaster : bankMasterList) {
						if (bankMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedBankMaster = bankMaster;
							return bankMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends BankMasterBean.loadLazyBankMasterList ======>");
	}

	public String addBankMaster() {

		log.info("<--- Start BankMasterBean.addBankMaster ---->");
		try {
			action = "ADD";
			bankMaster = new BankMaster();
			bankMaster.setActivestatus(true);
			log.info("<---- End BankMasterBean.addBankMaster ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return BANK_MASTER_CREATE_PAGE;
	}

	public String submitBankMaster() {
		log.info("<=======Starts  BankMasterBean.submitBankMaster ======>");

		if (bankMaster.getBankCode() == null || bankMaster.getBankCode().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.BANK_MASTER_CODE_REQUIRED.getErrorCode());
			return null;
		}

		if (bankMaster.getBankName() == null || bankMaster.getBankName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.BANK_MASTER_NAME_REQUIRED.getErrorCode());
			return null;
		}

		if (bankMaster.getActivestatus() == null) {
			errorMap.notify(ErrorDescription.BANK_MASTER_STATUS_REQUIRED.getErrorCode());
			return null;
		}

		if (action.equalsIgnoreCase("ADD")) {
			bankMaster.setCreatedBy(loginBean.getUserDetailSession());
		} else if (action.equalsIgnoreCase("EDIT")) {
			bankMaster.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL + "/bankmaster/create", bankMaster);
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.BANK_MASTER_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.BANK_MASTER_UPDATED_SUCCESS.getErrorCode());
			return showBankMasterListPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends BankMasterBean.submitBankMaster ======>");
		return null;
	}

	public String clear() {
		log.info("<---- Start BankMasterBean.clear ----->");
		showBankMasterListPage();
		log.info("<---- End BankMasterBean.clear ----->");
		return BANK_MASTER_LIST_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts BankMasterBean.onRowSelect ========>" + event);
		selectedBankMaster = ((BankMaster) event.getObject());
		log.info("selectedBankMaster id" + selectedBankMaster.getId());
		addButtonFlag = true;
		log.info("<===Ends BankMasterBean.onRowSelect ========>");
	}

	public String deleteBankMaster() {
		try {
			action = "Delete";
			if (selectedBankMaster == null || selectedBankMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_BANK_MASTER.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBankMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteBankMasterConfirm() {
		log.info("<===== Starts BankMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + "/bankmaster/delete/" + selectedBankMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.BANK_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBankMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete BankMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends BankMasterBean.deleteConfirm ===========>");
		return showBankMasterListPage();
	}

	public String viewBankMaster() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedBankMaster == null || selectedBankMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_BANK_MASTER.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedBankMaster.getId());

		selectedBankMaster = getBankMasterDetails(selectedBankMaster.getId());

		log.info("selected BankMaster Object:::" + selectedBankMaster);
		return BANK_MASTER_VIEW_PAGE;
	}

	public String editBankMaster() {

		action = "EDIT";
		log.info("<----Redirecting to user edit page---->");
		if (selectedBankMaster == null || selectedBankMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_BANK_MASTER.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedBankMaster.getId());

		selectedBankMaster = getBankMasterDetails(selectedBankMaster.getId());

		log.info("selected BankMaster Object:::" + selectedBankMaster);
		return BANK_MASTER_CREATE_PAGE;
	}

	private BankMaster getBankMasterDetails(Long id) {
		log.info("<====== BankMasterBean.getBankMasterDetails starts====>" + id);
		try {
			if (selectedBankMaster == null || selectedBankMaster.getId() == null) {
				log.error(" No BankMaster selected ");
				errorMap.notify(ErrorDescription.SELECT_BANK_MASTER.getErrorCode());
			}
			log.info("Selected BankMaster  Id : : " + selectedBankMaster.getId());
			String url = SERVER_URL + "/bankmaster/get/" + selectedBankMaster.getId();

			log.info("BankMaster Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			BankMaster bankMasterDetail = mapper.readValue(jsonResponse, BankMaster.class);
			if (bankMasterDetail != null) {
				bankMaster = bankMasterDetail;
			} else {
				log.error("BankMaster object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" BankMaster Retrived  SuccessFully ");
			} else {
				log.error(" BankMaster Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return bankMaster;
		} catch (Exception e) {
			log.error("Exception Ocured while get BankMaster Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

}