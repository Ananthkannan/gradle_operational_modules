package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.accounts.model.GlAccountGroup;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("glAccountGroupMasterBean")
@Scope("session")
public class GlAccountGroupMasterBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String GL_ACCOUNT_GROUP_CREATE_PAGE = "/pages/masters/createGLAccountGroup.xhtml?faces-redirect=true;";
	@Getter
	@Setter
	private String accountCategory;
	@Getter
	@Setter
	private String parentGroup;
	@Getter
	@Setter
	private String accountGroupCode;
	@Getter
	@Setter
	private String accountGroupName;
	@Getter
	@Setter
	private String regionalName;
	@Getter
	@Setter
	private String Url;
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	private UserDTO userDTO;
	@Autowired
	private HttpService httpService;
	@Autowired
	private LoginBean loginBean;
	@Getter
	@Setter
	private GlAccountGroup glAccountGroup;
	@Getter
	@Setter
	private GlAccountGroup pglAccountGroup;
	@Autowired
	private ErrorMap errorMap;
	@Getter
	@Setter
	private List<GlAccountCategory> glAccountCategoryList;
	@Getter
	@Setter
	private GlAccountCategory glAccountCategory;

	@PostConstruct
	public void init() {
		getCategory();
	}

	public GlAccountGroupMasterBean() {
		loadUrl();
		log.info("SERVER URL----" + Url);
	}

	private void loadUrl() {
		try {			
			Url = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() >>>> " + e.toString());
		}
	}

	public String submitAccountGroup() {

		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(Url + "/accounts/create/group", glAccountGroup);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("GlAccount Group saved successfully");
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating Account Group" + e);
		}
		return GL_ACCOUNT_GROUP_CREATE_PAGE;
	}

	public List<GlAccountCategory> getCategory() {
		glAccountCategoryList = new ArrayList<GlAccountCategory>();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(Url + "/accounts/getAll/category");
			glAccountCategoryList = (List<GlAccountCategory>) baseDTO.getResponseContent();
			if (baseDTO != null) {
				glAccountCategoryList = (List<GlAccountCategory>) baseDTO.getResponseContent();

			}
		} catch (Exception e) {
			log.error("Error while fetching Account Group category", e);
		}
		return glAccountCategoryList;
	}

}
