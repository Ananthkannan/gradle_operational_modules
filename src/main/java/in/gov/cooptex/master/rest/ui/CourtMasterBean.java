package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CourtMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("courtMasterBean")
public class CourtMasterBean {

	private final String CREATE_PAGE = "/pages/masters/createCourtMaster.xhtml?faces-redirect=true";
	private final String LIST_PAGE = "/pages/masters/listCourtMaster.xhtml?faces-redirect=true";
	private final String VIEW_PAGE = "/pages/masters/viewCourtMaster.xhtml?faces-redirect=true";

	private final String LIST_URL = "/courtmaster/getall";
	private final String CREATE_URL = "/courtmaster/create";
	private final String DELETE_URL = "/courtmaster/delete";

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	CourtMaster courtMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<CourtMaster> courtMasterList = new ArrayList<CourtMaster>();

	@Getter
	@Setter
	private int courtMasterListSize;

	boolean forceLogin = false;
	
	@Getter
	@Setter
	Boolean addButtonFlag=true;

	@PostConstruct
	public void init() {
		log.info(" CourtMasterBean init --->" + serverURL);
		loadValues();
		addCourtMaster();
		getAllCourtMasterList();
		getCourtMasterView(courtMaster);
	}

	public String showList() {
		log.info("<---------- Loading CourtMasterBean showList list page---------->");
		loadValues();
		getAllCourtMasterList();
		return LIST_PAGE;
	}

	public String addCourtMaster() {
		log.info("<---- Start CourtMasterBean.addCourtMaster ---->");
		try {
			action = "ADD";
			courtMaster = new CourtMaster();
			courtMaster.setActiveStatus(true);

			log.info("<---- End CourtMaster.addCourtMaster ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_PAGE;

	}
	public void onRowSelect(SelectEvent event) {
		addButtonFlag=false;
	}

	private void getAllCourtMasterList() {
		log.info("<<<< ----------Start courtMasterBean-getAllCourtMasterList() ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.get(url);
			courtMaster = new CourtMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				courtMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CourtMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (courtMasterList != null) {
			courtMasterListSize = courtMasterList.size();
		} else {
			courtMasterListSize = 0;
		}
		log.info("<<<< ----------End CourtMasterBean . getAllCourtMasterList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			courtMaster = new CourtMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to CourtMaster List page---->");
		return showList();
	}

	public String submit() {
		log.info("...... CourtMasterBean submit begin ...."+courtMaster);
		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(serverURL + CREATE_URL, courtMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("CourtMasterBean saved successfully");
					if(action.equalsIgnoreCase("ADD"))
					    errorMap.notify(ErrorDescription.COURT_MASTER_CREATED_SUCCESSFULLY.getCode());
						else
					    errorMap.notify(ErrorDescription.COURT_MASTER_UPDATED_SUCCESSFULLY.getCode());
					showList();
					//errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					 return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating CourtMaster" + e);
		}
		log.info("...... CourtMasterBean submit ended ....");
		return LIST_PAGE;
	}

	public String editCourtMaster() {
		log.info("<<<<< -------Start editCourtMaster called ------->>>>>> ");
		action = "EDIT";

		if (courtMaster == null) {
			Util.addWarn("Please select one court");
			return null;
		}

		if (courtMaster.getActiveStatus().equals("ACTIVE")) {
			courtMaster.setActiveStatus(courtMaster.getActiveStatus());
		} else {
			courtMaster.setActiveStatus(courtMaster.getActiveStatus());
		}
		log.info("<<<<< -------End editCourtMaster called ------->>>>>> ");

		return CREATE_PAGE;

	}

	public String deleteCourtMaster() {
		try {
			action = "Delete";
			if (courtMaster == null) {
				Util.addWarn("Please select one court");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCourtMasterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteCourtMasterConfirm() {

		try {
			log.info("Delete confirmed for court [" + courtMaster.getName() + "]");
			CourtMaster courtMasterDelete = new CourtMaster();
			courtMasterDelete.setId(courtMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + DELETE_URL;
			baseDTO = httpService.post(url, courtMaster);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("CourtMaster deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					courtMaster = new CourtMaster();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else if(baseDTO.getStatusCode() == 0) {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					//errorMap.notify(baseDTO.getStatusCode());
					 errorMap.notify(ErrorDescription.COURT_MASTER_DELETED_SUCCESSFULLY.getCode());
				}
				else
				{
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start courtMasterBean . clear ----->");
		getAllCourtMasterList();
		courtMaster = new CourtMaster();
		addButtonFlag=true;
		log.info("<---- End CourtMasterBean . clear ----->");

		return LIST_PAGE;

	}

	public String backPage() {

		showList();
		return LIST_PAGE;
	}

	public String viewCourtMaster() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (courtMaster == null) {
			Util.addWarn("Please Select One CourtMaster");
			return null;
		}
		return VIEW_PAGE;

	}

	private void getCourtMasterView(CourtMaster courtMaster) {
		log.info("<<<< ----------Start courtMasterBean . getCourtMasterView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + LIST_URL;
			baseDTO = httpService.post(url, courtMaster);
			courtMaster = new CourtMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				courtMaster = mapper.readValue(jsonResponse, new TypeReference<CourtMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (courtMaster != null) {
			log.info("courtMaster getCourtMasterView" + courtMaster);
		} else {
			log.info("courtMaster getCourtMasterView is null");
		}

		log.info("<<<< ----------End CourtMasterBean-getCourtMasterView ------- >>>>");
	}

}
