package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CallLetterNotificationMasterDto;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.NominalDetails;
import in.gov.cooptex.core.model.OralTestCallLetter;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.OralExamVenue;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Log4j2
@Scope("session")
@Service("oralTestCallLetterBean")
public class OralTestCallLetterBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();

	String url;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = "";

	@Getter
	@Setter
	LazyDataModel<OralTestCallLetter> callLetterLazyList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private List<OralTestCallLetter> selectedCallLetterList, callLetterList, tempCallLetterList, otcallLetterList,
			otCallLetterFilteredList;

	@Getter
	@Setter
	OralTestCallLetter selectedCallLetter, callLetter, selectedOralTestCallLetter;

	@Getter
	@Setter
	List<CallLetterNotificationMasterDto> notificationList;

	@Getter
	@Setter
	CallLetterNotificationMasterDto callLetterNotificationDto = new CallLetterNotificationMasterDto();

	@Setter
	@Getter
	List<EntityMaster> entityMasterList;

	@Setter
	@Getter
	EntityMaster entityMaster = new EntityMaster();

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	List<OralExamVenue> oralTestVenueList, selectedOralTestVenueList, updateVenueList;

	int totalVenue = 0;

	@Getter
	@Setter
	private List<OralTestCallLetter> data, otdata;

	private String callLetterPath, coOpTexlogoPath, downloadPath, contactNo, address;

	@Setter
	private DefaultStreamedContent downloadFile = null;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Boolean addButtonFlag;

	int count = 0;

	@PostConstruct
	public void init() {
		selectedOralTestCallLetter = new OralTestCallLetter();
		loadData();
	}

	public void loadData() {
		designationList = commonDataService.loadDesignation();
		entityMasterList = commonDataService.loadRegionalOffice();
		notificationList = commonDataService.loadOralSeletionNotficationNumber();
		loadCallLetterPath();
	}

	public void loadCallLetterPath() {
		try {
			coOpTexlogoPath = commonDataService.getAppKeyValue("CALL_LETTER_LOGO_PATH");
			downloadPath = commonDataService.getAppKeyValue("CALL_LETTER_DOWNLOAD_PATH");
			File file = new File(downloadPath);
			callLetterPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(callLetterPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Hall Ticket Path ::" + path);
		} catch (Exception e) {
			log.error("Exception Occured while loading hallticket Path ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadLazyCallLetter() {
		log.info("< --  Start Oral Test Call Letter Lazy Load -- > ");
		callLetterLazyList = new LazyDataModel<OralTestCallLetter>() {
			private static final long serialVersionUID = 1L;

			public List<OralTestCallLetter> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO request = new PaginationDTO(first, pageSize, sortField, sortOrder.toString(),
							filters);
					log.info("Search request data" + request);
					BaseDTO baseDTO = httpService.post(SERVER_URL + "/oraltestcallletter/getalllazy", request);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					data = mapper.readValue(jsonResponse, new TypeReference<List<OralTestCallLetter>>() {
					});

					if (data == null) {
						log.info(" OralTestCallLetter Response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" OralTestCallLetter Search Successfully Completed");
					} else {
						log.error(" OralTestCallLetter Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}

				} catch (Exception e) {
					log.error("Exception Occured  :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return data;

			}

			@Override
			public Object getRowKey(OralTestCallLetter res) {
				log.info(" Selected Object :: " + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public OralTestCallLetter getRowData(String rowKey) {
				log.info(" Selected Object :: " + data);
				// List<OralTestCallLetter> responseList = (List<OralTestCallLetter>)
				// getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (OralTestCallLetter res : data) {
					if (res.getId().equals(value)) {
						selectedOralTestCallLetter = res;
						log.info(" Selected Object :: " + selectedOralTestCallLetter);
						return res;
					}
				}
				return null;
			}

		};

	}

	public String showPage() {
		log.info("<---: Inside Show Page  :-----> action :-" + action);
		if (action.equalsIgnoreCase("ADD")) {
			log.info("<--  inside add  -->");
			clear();
			return "/pages/personnelHR/createCallLetter.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("VIEW")) {
			log.info("<--  inside view  -->");
			log.info(" Selected Call Letter Object ::" + selectedOralTestCallLetter);
			if (selectedOralTestCallLetter == null || selectedOralTestCallLetter.getNotificationNo() == null
					|| selectedOralTestCallLetter.getNotificationNo().isEmpty()) {
				log.info(" Selected Call Letter is Empty");
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			loadCallLetterByNotificationNo();
			return "/pages/personnelHR/viewCallLetter.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("CALL_LETTER_LIST")) {
			selectedOralTestCallLetter = new OralTestCallLetter();
			loadLazyCallLetter();
			clear();
			return "/pages/personnelHR/listCallLetter.xhtml?faces-redirect=true";
		}
		return null;

	}

	private void loadCallLetterByNotificationNo() {
		BaseDTO baseDto = new BaseDTO();
		otcallLetterList = new ArrayList<>();
		try {
			OralTestCallLetter request = new OralTestCallLetter();
			request.setNotificationNo(selectedOralTestCallLetter.getNotificationNo());
			baseDto = httpService.post(SERVER_URL + "/oraltestcallletter/findbynotification", request);
			if (baseDto == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			otcallLetterList = mapper.readValue(jsonResponse, new TypeReference<List<OralTestCallLetter>>() {
			});

			if (otcallLetterList == null) {
				log.info(" OralTestCallLetter Response Failed to Deserialize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			if (baseDto.getStatusCode() == 0) {
				log.info(" OralTestCallLetter Search Successfully Completed");
			} else {
				log.error(" OralTestCallLetter Search Failed With status code :: " + baseDto.getStatusCode());
				errorMap.notify(baseDto.getStatusCode());
			}

		} catch (Exception e) {
			log.error("Exception Occured  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clear() {
		log.info(" <---: Inside clear :---->");
		selectedCallLetter = new OralTestCallLetter();
		selectedCallLetterList = new ArrayList<>();
		selectedOralTestCallLetter = new OralTestCallLetter();
		entityMaster = new EntityMaster();
		oralTestVenueList = new ArrayList<>();
		addButtonFlag = true;
		callLetterNotificationDto = new CallLetterNotificationMasterDto();
		selectedOralTestVenueList = new ArrayList<>();
		callLetterList = new ArrayList<>();

	}

	public String clearView() {
		log.info(" <---: Inside View clear  :---->");
		selectedCallLetter = new OralTestCallLetter();
		selectedCallLetterList = new ArrayList<>();
		callLetterList = new ArrayList<>();
		callLetterNotificationDto = new CallLetterNotificationMasterDto();
		entityMaster = new EntityMaster();
		oralTestVenueList = new ArrayList<>();
		callLetter = new OralTestCallLetter();
		loadCallLetterByNotificationNo();
		return "/pages/personnelHR/viewCallLetter.xhtml?faces-redirect=true";

	}

	public void changeByNotificationNo() {
		log.info(" Indise changeByNotificationNo for notification number  ::"
				+ callLetterNotificationDto.getNotificationNo());
		notificationList.forEach(list -> {
			if (list.getNotificationNo().equals(callLetterNotificationDto.getNotificationNo())) {
				callLetterNotificationDto.setPostName(list.getPostName());
				callLetterNotificationDto.setReqYear(list.getReqYear());
				callLetterNotificationDto.setPostId(list.getPostId());
				log.info(" Notification DTO ::" + callLetterNotificationDto);
			}
		});
		entityMaster = new EntityMaster();
		oralTestVenueList = new ArrayList<>();
		log.info(" Entity Master :: " + entityMaster);
	}

	public void getVenueListByLocationAndNotificationNo() {
		log.info("Entity Master ( Regoin ) id :: " + entityMaster.getId());
		try {
			callLetter = new OralTestCallLetter();
			selectedOralTestVenueList = new ArrayList<>();
			if (callLetterNotificationDto != null && callLetterNotificationDto.getNotificationNo() != null
					&& !callLetterNotificationDto.getNotificationNo().isEmpty()) {
				callLetter.setNotificationNo(callLetterNotificationDto.getNotificationNo().trim());
			} else {
				log.info("<--- Notification No is Empty ---->");
				errorMap.notify(ErrorDescription.SELECT_NOTIFICATION_NO.getErrorCode());
				return;
			}

			if (entityMaster != null && entityMaster.getId() != null) {
				callLetter.setLoactionId(entityMaster.getId());
			} else {
				log.info("<--- Location is Empty ---->");
				errorMap.notify(ErrorDescription.SELECT_OT_LOC.getErrorCode());
				return;
			}
			url = SERVER_URL + "/oraltestcallletter/getvenuebyentitymaster";
			log.info(" Request Url :: " + url);
			BaseDTO baseDTO = httpService.post(url, callLetter);
			if (baseDTO == null) {
				log.error(" BaseDto Returns null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			oralTestVenueList = mapper.readValue(jsonResponse, new TypeReference<List<OralExamVenue>>() {
			});
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Oral Test Venue list fetched successfully :: " + oralTestVenueList);
			} else {
				log.error("Oral Test Venue list failed to fetch  with status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void fetchSelectedCallLetter() {
		totalVenue = 0;
		log.info(" Selected Venues are :" + selectedOralTestVenueList);
		log.info("Notification Number " + callLetterNotificationDto.getNotificationNo());
		try {
			if (callLetterNotificationDto.getNotificationNo() == null
					|| callLetterNotificationDto.getNotificationNo().isEmpty()) {
				log.info(" Notification Number is Null");
				errorMap.notify(ErrorDescription.SELECT_NOTIFICATION_NO.getErrorCode());
				return;
			}

			if (callLetterNotificationDto.getReqYear() == null) {
				log.info(" Requirment year is empty");
				errorMap.notify(ErrorDescription.REQ_YEAR_IS_EMPTY.getErrorCode());
				return;
			}

			if (callLetterNotificationDto.getPostName() == null || callLetterNotificationDto.getPostName().isEmpty()) {
				log.info(" Requirment Post is empty");
				errorMap.notify(ErrorDescription.REQ_POST_IS_EMPTY.getErrorCode());
				return;
			}

			if (entityMaster == null || entityMaster.getId() == null) {
				log.info(" Loaction is empty");
				errorMap.notify(ErrorDescription.SELECT_OT_LOC.getErrorCode());
				return;
			}

			if (selectedOralTestVenueList == null || selectedOralTestVenueList.size() == 0) {
				log.info(" Oral Test Venue is empty ");
				errorMap.notify(ErrorDescription.SELECT_OT_VENUE.getErrorCode());
				return;
			}

			url = SERVER_URL + "/oraltestcallletter/fetchselectedcandidates";
			BaseDTO baseDTO = httpService.post(url, callLetterNotificationDto);

			log.info(" Fetch Selcetd Candidates URL :: " + url);
			log.info(" Fetch Selcetd Candidates request :: " + callLetterNotificationDto);

			if (baseDTO == null) {
				log.error("BaseDto return null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			callLetterList = mapper.readValue(jsonResponse, new TypeReference<List<OralTestCallLetter>>() {
			});
			if (callLetterList != null && callLetterList.size() > 0) {
				log.info(" Selected Candidate Fetched Successfully " + callLetterList);
			} else {
				log.info(" No Selected Candidate found based on request paramete");
				errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
				return;
			}
			selectedOralTestVenueList.forEach(otvenue -> {
				totalVenue = totalVenue + otvenue.getAvilableSeats();
			});

			log.info(" Total Avalable Seats :: " + totalVenue);
			log.info(" Total Candidates :: " + callLetterList.size());

			if (totalVenue > 0 && totalVenue >= callLetterList.size() && selectedOralTestVenueList.size() > 0) {
				callLetterList = allocateVenue(callLetterList, selectedOralTestVenueList);
			} else {
				log.info(" Call Letter list exceeded  ");
				errorMap.notify(ErrorDescription.INSUFFICIENT_ORAL_TEST_VENUE.getErrorCode());
				callLetterList = new ArrayList<>();
			}

		} catch (Exception e) {
			log.error(" Exception Occure :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	private List<OralTestCallLetter> allocateVenue(List<OralTestCallLetter> clList, List<OralExamVenue> venueList) {
		log.info(" Inside assign venue ");
		DateFormat dateFormat = new SimpleDateFormat("hh:mm a");

		OralExamVenue testVenue = venueList.get(0);
		int venueIndex = 0;
		Set<OralExamVenue> venueTreeSet = new TreeSet<>();

		int avSeats = testVenue.getAvilableSeats();

		for (int i = 0; i < clList.size(); i++) {
			log.info(" Call Letter Object ::  " + clList.get(i));
			if (avSeats > 0) {
				clList.get(i).setOralTestVenue(testVenue);
				clList.get(i).setTimeSlot(dateFormat.format(testVenue.getOralExamStartTime()));
				testVenue.setAvilableSeats(--avSeats);
			} else {
				++venueIndex;
				testVenue = venueList.get(venueIndex);
				avSeats = testVenue.getAvilableSeats();
				clList.get(i).setOralTestVenue(testVenue);
				clList.get(i).setTimeSlot(dateFormat.format(testVenue.getOralExamStartTime()));
				testVenue.setAvilableSeats(--avSeats);
			}

			venueTreeSet.add(testVenue);
		}

		log.info(" Final Tree Set Value :: " + venueTreeSet);
		updateVenueList = new ArrayList<>(venueTreeSet);
		log.info(" Allocated Venue List :: " + clList);
		return clList;

	}

	private List<OralTestCallLetter> prepairCallLetter() {
		log.info("<== Inside Prepair call Letter == >" + callLetterNotificationDto);
		callLetterList.forEach(callLetter -> {
			callLetter.setNotificationNo(callLetterNotificationDto.getNotificationNo());
			callLetter.setRecruitmentYear(callLetterNotificationDto.getReqYear());
			callLetter.setAppliedPostId(callLetterNotificationDto.getPostId());
			callLetter.setCreatedBy(loginBean.getUserDetailSession());
			callLetter.setCreatedDate(new Date());

		});
		return callLetterList;
	}

	public String saveCallLetterList() {
		BaseDTO baseDTO = new BaseDTO();
		log.info(" Call Letter list for save :: " + callLetterList);
		if (callLetterList == null || callLetterList.size() == 0) {
			log.info(" Call letter list is empty");
			errorMap.notify(ErrorDescription.OTCL_ISNULL.getErrorCode());
			return null;
		}
		if (updateVenueList == null || updateVenueList.size() == 0) {
			log.info(" Oral test venue list is empty");
			errorMap.notify(ErrorDescription.OTCL_VENUE_LIST_IS_EMPTY.getErrorCode());
			return null;
		}
		try {
			callLetterList = prepairCallLetter();
			OralTestCallLetter request = new OralTestCallLetter();
			request.setCallLetterList(callLetterList);
			url = SERVER_URL + "/oraltestcallletter/save";

			log.info(" Save Call Letter URL :: " + url);
			log.info(" Save Call Letter Request :: " + request);
			baseDTO = httpService.post(url, request);
			if (baseDTO == null) {
				log.error("BaseDto return null values");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Oral Test Call Letter saved successfully");
				errorMap.notify(ErrorDescription.OTCL_SAVED_SUCCESS.getErrorCode());
				action = "CALL_LETTER_LIST";
				updateSelectedCandidateDetails(callLetterList);
				updateVenueSeats(updateVenueList);
				return showPage();
			} else {
				log.info(" Call letter list failed to save with status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error(" Exception Occured while saving call letter list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private void updateVenueSeats(List<OralExamVenue> updateList) {
		log.info(" Venue List To Update Seats :: " + updateList);

		BaseDTO baseDTO = new BaseDTO();
		OralTestCallLetter request = new OralTestCallLetter();
		request.setVenueList(updateList);
		url = SERVER_URL + "/oraltestcallletter/updatevenueseats";
		log.info(" Update Oral Test Call Letter Venue URL :: " + url);
		log.info(" Update Oral Test Call Letter Venue Request :: " + request);
		baseDTO = httpService.put(url, request);

		if (baseDTO == null) {
			log.error(" BaseDto returned null values ");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
		if (baseDTO.getStatusCode() == 0) {
			log.info(" Oral Test Venue Update Succesffully ");
		} else {

			log.info(" Oral Test Call Letter Venue Updation Failed with Status Code ::" + baseDTO.getStatusCode());
			errorMap.notify(baseDTO.getStatusCode());
		}

	}

	private void updateSelectedCandidateDetails(List<OralTestCallLetter> selectedcallLetter) {

		OralTestCallLetter request = new OralTestCallLetter();
		request.setCallLetterList(selectedcallLetter);
		url = SERVER_URL + "/oraltestcallletter/updatecandidatedetails";
		log.info(" Save Call Letter URL :: " + url);
		log.info(" Save Call Letter Request :: " + request);
		BaseDTO baseDTO = httpService.put(url, request);
		if (baseDTO == null) {
			log.error("BaseDto return null values");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}

		if (baseDTO.getStatusCode() == 0) {
			log.info(" Oral Test Call Letter saved successfully");
		} else {
			log.info(" Call letter list failed to save with status code :: " + baseDTO.getStatusCode());
			return;
		}
	}

	public void downloadcallLetterPdf() {
		if (selectedCallLetterList == null || selectedCallLetterList.size() == 0) {
			log.error(" Please Select one Hall Ticket");
			errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
			return;
		}
		log.info("<< ===  Insdide Download Hall Ticket PDF === >>" + selectedCallLetterList);
		try {
			if (selectedCallLetterList.size() > 1) {
				log.info("Call Letter Path is :: " + callLetterPath);
				File fPath = new File(callLetterPath);
				//FileUtils.cleanDirectory(fPath);
				selectedCallLetterList.forEach(htList -> {
					createFile(htList, selectedCallLetterList.size());
				});
				ByteArrayInputStream bis = new ByteArrayInputStream(AppUtil.zip(callLetterPath));
				InputStream stream = bis;
				downloadFile = new DefaultStreamedContent(stream, "application/zip", "oraltestCallLetter.zip",
						Charsets.UTF_8.name());
				//FileUtils.cleanDirectory(fPath);
			} else {
				OralTestCallLetter oralTestCallLetter = selectedCallLetterList.get(0);
				createFile(oralTestCallLetter, selectedCallLetterList.size());
			}

		} catch (Exception e) {
			log.error("Exception Occured While Downloading Hall Ticket", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void createFile(OralTestCallLetter oralTestCallLetter, Integer fileSize) {
		log.info("< == = == =  Inside Create File = == = = = ==  >>" + fileSize);
		try {
			JasperReport jasperReportMCQon;
			JasperDesign jasperDesignMCQon;
			JasperPrint report = null;
			Map<String, Object> map = new HashMap<String, Object>();
			List<NominalDetails> data = new ArrayList<NominalDetails>();
			data.add(null);
			jasperDesignMCQon = JRXmlLoader.load(getClass().getResourceAsStream("/oral_call_letter.jrxml"));
			jasperReportMCQon = JasperCompileManager.compileReport(jasperDesignMCQon);
			Map<String, Object> params = new HashMap<String, Object>();
			map.put("RC_NO", oralTestCallLetter.getNotificationNo());
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String genDate = sdf.format(new Date());
			map.put("RE_GEN_DATE", genDate);
			map.put("CANDIDATE_NAME", oralTestCallLetter.getCandidateName());
			// String timeSlot = examDate + " / " + hallTicket.getTimeSlot();
			map.put("APPLIED_POST", oralTestCallLetter.getAppliedPost().getName());
			map.put("CANDIDATE_FATHER_SPOUSE_NAME", oralTestCallLetter.getCandidateFatherOrSpouseName());
			String writtenExamDate = sdf.format(oralTestCallLetter.getWrittenExamDate());
			map.put("WRITTEN_EXAM_DT", writtenExamDate);

			address = oralTestCallLetter.getOralTestVenue().getVenueName();
			if (oralTestCallLetter.getOralTestVenue().getAddressLineOne() != null
					&& !oralTestCallLetter.getOralTestVenue().getAddressLineOne().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getAddressLineOne();
			}

			if (oralTestCallLetter.getOralTestVenue().getAddressLineTwo() != null
					&& !oralTestCallLetter.getOralTestVenue().getAddressLineTwo().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getAddressLineTwo();
			}

			if (oralTestCallLetter.getOralTestVenue().getAddressLineThree() != null
					&& !oralTestCallLetter.getOralTestVenue().getAddressLineThree().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getAddressLineThree();
			}

			if (oralTestCallLetter.getOralTestVenue().getTalukMaster() != null
					&& oralTestCallLetter.getOralTestVenue().getTalukMaster().getName() != null
					&& !oralTestCallLetter.getOralTestVenue().getTalukMaster().getName().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getTalukMaster().getName();
			}

			if (oralTestCallLetter.getOralTestVenue().getDistrictMaster() != null
					&& oralTestCallLetter.getOralTestVenue().getDistrictMaster().getName() != null
					&& !oralTestCallLetter.getOralTestVenue().getDistrictMaster().getName().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getDistrictMaster().getName();
			}

			if (oralTestCallLetter.getOralTestVenue().getStateMaster() != null
					&& oralTestCallLetter.getOralTestVenue().getStateMaster().getName() != null
					&& !oralTestCallLetter.getOralTestVenue().getStateMaster().getName().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getStateMaster().getName();
			}

			if (oralTestCallLetter.getOralTestVenue().getPincode() != null
					&& !oralTestCallLetter.getOralTestVenue().getPincode().isEmpty()) {
				address = address + "," + oralTestCallLetter.getOralTestVenue().getPincode();
			}

			map.put("ORAL_TEST_VENUE_ADDRESS", address);

			contactNo = oralTestCallLetter.getOralTestVenue().getPrimaryContactNumber();

			if (oralTestCallLetter.getOralTestVenue().getSecondaryContactNumber() != null
					&& !oralTestCallLetter.getOralTestVenue().getSecondaryContactNumber().isEmpty()) {
				contactNo = contactNo + " / " + oralTestCallLetter.getOralTestVenue().getSecondaryContactNumber();
			}

			if (oralTestCallLetter.getOralTestVenue().getOralExamDate() != null) {
				String oralExamDt = sdf.format(oralTestCallLetter.getOralTestVenue().getOralExamDate());
				map.put("ORAL_EXAM_DT", oralExamDt);
			}

			map.put("ORAL_TEST_CONTACT_NUMBER", contactNo);
			map.put("CO_OP_TEX_LOGO", coOpTexlogoPath.trim());

			map.put("ORAL_EXAM_TIME", oralTestCallLetter.getTimeSlot());

			params.put("map", map);
			log.info(" Call Letter Jasper report parameters are :: " + params);
			JRBeanCollectionDataSource datasource = new JRBeanCollectionDataSource(data);
			report = JasperFillManager.fillReport(jasperReportMCQon, params, datasource);

			ByteArrayInputStream bais = new ByteArrayInputStream(JasperExportManager.exportReportToPdf(report));
			if (fileSize > 1) {
				JasperExportManager.exportReportToPdfFile(report,
						callLetterPath + oralTestCallLetter.getRegisterNumber() + "_callLetter.pdf");
			} else {
				downloadFile = new DefaultStreamedContent(bais, "pdf",
						oralTestCallLetter.getRegisterNumber() + "_callLetter.pdf");
			}
		} catch (Exception e) {
			log.error(" Exception in Call Letter Generation", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public DefaultStreamedContent getDownloadFile() {
		return downloadFile;
	}

	public String cancel() {
		action = "CALL_LETTER_LIST";
		return showPage();
	}

	public void onRowSelect(SelectEvent event) {
		selectedCallLetter = ((OralTestCallLetter) event.getObject());
		log.info("selectedCallLetter ::" + selectedCallLetter);
		addButtonFlag = false;
	}

	public int getCount() {
		if (otCallLetterFilteredList != null) {
			return otCallLetterFilteredList.size();
		} else {
			return otcallLetterList.size();
		}
	}
}
