package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StaffEligibilityDetails;
import in.gov.cooptex.core.model.StaffEligibilityStatement;
import in.gov.cooptex.core.model.StaffEligibilityStatementNote;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Saranya
 *
 */
/**
 * @author FINATEL-ADMIN6
 *
 */
/**
 * @author FINATEL-ADMIN6
 *
 */
@Log4j2
@Service("staffEligibilityBean")
@Scope("session")
public class StaffEligibilityBean extends CommonBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	LazyDataModel<StaffEligibilityStatement> staffEligibilityList;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	StaffEligibilityStatement selectedStaffEligibilityStmt;

	@Getter
	@Setter
	UserMaster forwardToUser;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	StaffEligibilityStatement staffEligibilityStmt = new StaffEligibilityStatement();

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	List<Long> recruitmentYearList;

	@Getter
	@Setter
	StaffEligibilityDetails selectedStaffEligibilityDetail = new StaffEligibilityDetails();

	@Getter
	@Setter
	Date todayDate = new Date();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;

	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	String salesFromMonthYear;

	@Getter
	@Setter
	String salesToMonthYear;

	@Getter
	@Setter
	Integer salesFromMonth;

	@Getter
	@Setter
	Integer salesFromYear;

	@Getter
	@Setter
	Integer salesToMonth;

	@Getter
	@Setter
	Integer salesToYear;

	@Getter
	@Setter
	Set<String> salesFromMonthYearList;

	@Getter
	@Setter
	Set<String> salesToMonthYearList;

	@Setter
	@Getter
	Boolean showSalesPeriodPanel = false;

	@Setter
	@Getter
	String entitytip = "No regions selected";

	@Setter
	@Getter
	String designationtip = "No designations selected";

	@Setter
	@Getter
	Boolean generateButtonFlag = false;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	StaffEligibilityStatementNote staffEligibilityStatementNote;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	String panelValue = "collapse in";

	@Getter
	@Setter
	String approveRejectAction;

	final String VIEW_PAGE = "/pages/personnelHR/viewStaffEligibilityStatement.xhtml?faces-redirect=true";
	final String APPROVING_PAGE = "/pages/personnelHR/staffEligibilityStatementMdApproval.xhtml?faces-redirect=true";

	/**
	 * purpose:used to load staff eligibility list by calling server
	 * 
	 * @return url to redirect to list page
	 */
	public String loadStaffEligibility() {
		mapper = new ObjectMapper();
		staffEligibilityStmt = new StaffEligibilityStatement();
		selectedStaffEligibilityStmt = new StaffEligibilityStatement();
		selectedStaffEligibilityDetail = new StaffEligibilityDetails();
		log.info("StaffEligibilityBean.loadStaffEligibility");
		try {
			action = "LIST";
			addButtonFlag = false;
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
			loadLazyStaffEligibility();
			loadDepartment();
			loadAllDesignationList();
			loadAllEntityList();
			loadStates();
			staffEligibilityStatementNote = new StaffEligibilityStatementNote();
		} catch (Exception e) {

			log.error("Exception occured while fetching initial data...", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return "/pages/personnelHR/listStaffEligibilityStatement.xhtml?faces-redirect=true";
	}

	public void loadLazyStaffEligibility() {

		log.info("< --  Start Staff Eligibility Lazy Load -- > ");
		staffEligibilityList = new LazyDataModel<StaffEligibilityStatement>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<StaffEligibilityStatement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<StaffEligibilityStatement> data = mapper.readValue(jsonResponse,
							new TypeReference<List<StaffEligibilityStatement>>() {
							});
					if (data == null) {
						log.info(" StaffEligibilityStatement Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" StaffEligibility Search Successfully Completed");
					} else {
						log.error(" StaffEligibility Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading StaffEligibility data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(StaffEligibilityStatement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StaffEligibilityStatement getRowData(String rowKey) {
				List<StaffEligibilityStatement> responseList = (List<StaffEligibilityStatement>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StaffEligibilityStatement res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedStaffEligibilityStmt = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			StaffEligibilityStatement request = new StaffEligibilityStatement();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("referenceNumber")) {
					log.info("ReferenceNumber : " + value);
					request.setReferenceNumber(value);
				}

				if (entry.getKey().equals("state.id")) {
					log.info("State name : " + value);
					StateMaster des = new StateMaster();
					des.setId(Long.parseLong(value));
					request.setState(des);
				}

				if (entry.getKey().equals("recruitmentForPost.id")) {
					log.info("recruitmentForPost name : " + value);
					Designation des = new Designation();
					des.setId(Long.parseLong(value));
					request.setRecruitmentForPost(des);

				}
				if (entry.getKey().equals("recruitmentYear")) {
					log.info("recruitmentYear  : " + value);
					request.setRecruitmentYear(Integer.valueOf(value));

				}

				if (entry.getKey().equals("department.id")) {
					log.info("Department id : " + value);
					Department dep = new Department();
					dep.setId(Long.parseLong(value));
					request.setDepartment(dep);
				}
				if (entry.getKey().equals("entity")) {
					log.info("entity name : " + value);
					request.setEntity(value);
				}

				if (entry.getKey().equals("status")) {
					log.info("status : " + value);
					request.setStatus(value);
				}

			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/staffeligibility/search", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Staff Eligibility search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * purpose:used to undo the changes done by generate button
	 * 
	 * @return
	 */
	public String clearFilter() {
		staffEligibilityStmt = new StaffEligibilityStatement();
		designationList = new ArrayList<>();
		entityMasterList = new ArrayList<>();
		// staffEligibilityStmt.setDepartment(null);
		// staffEligibilityStmt.setState(null);
		// staffEligibilityStmt.setDepartment(null);
		// loadDepartment();
		// loadStates();
		salesToMonthYear = null;
		salesFromMonthYear = null;
		showSalesPeriodPanel = false;
		entitytip = "";
		designationtip = "";
		return null;
	}

	/**
	 * purpose:used to load state master in staff eligibility add and list page
	 */
	public void loadStates() {
		log.info("... Load States called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/stateMaster/getAllStates");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			stateList = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading states....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * purpose:used to load region list in staff eligibility add and list page
	 * 
	 */
	public void loadEntityList() {
		log.info("... Load Entity called ...." + staffEligibilityStmt.getState().getId());
		staffEligibilityStmt.setRegionList(null);
		try {
			mapper = new ObjectMapper();

			/*
			 * EntityMasterController.java / EntityMasterService.java -
			 * getAllActiveRegionByStateId
			 */
			BaseDTO response = httpService
					.get(SERVER_URL + "/entitymaster/getallactiveregion/" + staffEligibilityStmt.getState().getId());
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading entity....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * purpose:load department list for drop down while adding staff eligibility
	 * 
	 */
	public void loadDepartment() {
		log.info("... Load department called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/department/load");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			departmentList = mapper.readValue(jsonResponse, new TypeReference<List<Department>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading department....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * purpose:used to load designation in staff eligibility add and list page
	 * 
	 */
	public void loadDesignationList() {
		log.info("... Load Designation List called ...." + staffEligibilityStmt.getDepartment().getId());
		staffEligibilityStmt.setDesignation(null);
		if (staffEligibilityStmt.getDepartment().getName().equalsIgnoreCase("Marketing")) {
			showSalesPeriodPanel = true;
		} else {
			showSalesPeriodPanel = false;
		}
		try {
			mapper = new ObjectMapper();
			/*
			 * DesignationController.java (Method: getAllDesignationByDepartment)
			 */
			BaseDTO response = httpService
					.get(SERVER_URL + "/designation/getalldesignation/" + staffEligibilityStmt.getDepartment().getId());
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});

			log.info("designationList---->" + designationList.size());
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * purpose : used to clear search fields and reload the list
	 * 
	 * @return url to redirect to list page
	 */
	public String clear() {
		selectedStaffEligibilityStmt = new StaffEligibilityStatement();
		addButtonFlag = false;
		approveButtonFlag = false;
		generateFlag = false;
		deleteFlag = false;
		viewButtonFlag = false;
		generateButtonFlag = false;
		entitytip = "";
		designationtip = "";

		return loadStaffEligibility();

	}

	/**
	 * purpose:used to load all masters when add button is clicked in list page
	 * 
	 * @return url to redirect to list page
	 */
	public String addStaffEligibility() {
		staffEligibilityStmt = new StaffEligibilityStatement();
		salesToMonthYear = null;
		salesFromMonthYear = null;
		showSalesPeriodPanel = false;
		designationList = new ArrayList<>();
		entityMasterList = new ArrayList<>();
		generateButtonFlag = false;
		loadStates();
		loadDepartment();
		prepareSalesMonthYear();
		employeeMaster = commonDataService.loadEmployeeByUser(loginBean.getUserMaster().getId());
		// loadDesignationList();
		// loadEntityList();
		List<Long> recruitmentYears = new ArrayList<Long>();
		recruitmentYears.add((long) Calendar.getInstance().get(Calendar.YEAR));
		recruitmentYears.add((long) Calendar.getInstance().get(Calendar.YEAR) + 1);
		recruitmentYearList = recruitmentYears;
		return "/pages/personnelHR/createStaffEligibilityStatement.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:will generate staff eligibility statments based on given parameters
	 * 
	 * @return
	 */
	public String generateStaffEligibilityStatement() {
		panelValue = "collapse";
		if (staffEligibilityStmt.getDepartment().getName().equalsIgnoreCase("Marketing")) {
			salesFromMonth = AppUtil.getMonthNumber(salesFromMonthYear.split("-")[0]);
			salesFromYear = Integer.valueOf(salesFromMonthYear.split("-")[1]);

			salesToMonth = AppUtil.getMonthNumber(salesToMonthYear.split("-")[0]);
			salesToYear = Integer.valueOf(salesToMonthYear.split("-")[1]);

			Date salesFrom = new GregorianCalendar(salesFromYear, salesFromMonth - 1, 01).getTime();
			staffEligibilityStmt.setSalesPeriodFrom(salesFrom);

			Date firstDay = new GregorianCalendar(salesToYear, salesToMonth - 1, 1).getTime();
			Date salesTo = AppUtil.getLastDateOfMonth(firstDay);
			staffEligibilityStmt.setSalesPeriodTo(salesTo);

			if (staffEligibilityStmt.getSalesPeriodFrom().after(staffEligibilityStmt.getSalesPeriodTo())) {
				log.info("incorrect date value");
				Util.addWarn("Sales period to should be greater than sales period from date");
				return null;
			}
		}

		log.info("... Generate Staff Eligibility List called ...." + staffEligibilityStmt);
		try {

			mapper = new ObjectMapper();
			BaseDTO response = httpService.post(SERVER_URL + "/staffeligibility/generatestaffeligibility",
					staffEligibilityStmt);
			if (response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				generateButtonFlag = false;
				return null;
			} else {
				generateButtonFlag = true;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			List<StaffEligibilityDetails> staffDetilsList = mapper.readValue(jsonResponse,
					new TypeReference<List<StaffEligibilityDetails>>() {
					});
			if (!CollectionUtils.isEmpty(staffDetilsList)) {
				if (staffDetilsList.size() < staffEligibilityStmt.getRegionList().size()) {
					Util.addInfo("Records found for Below Region Only");
				}

				staffEligibilityStmt.setStaffEligibilityDetails(staffDetilsList);
				/*** Sum of shortage count ***/
				if (staffEligibilityStmt != null && staffEligibilityStmt.getStaffEligibilityDetails() != null) {
					int shortageCountTotal = staffEligibilityStmt.getStaffEligibilityDetails().stream()
							.filter(sed -> sed.getShortageCount() != null && sed.getShortageCount() >= 0)
							.mapToInt(sed -> sed.getShortageCount()).sum();
					staffEligibilityStmt.setShortageCountTotal(shortageCountTotal);
					log.info(" generateStaffEligibilityStatement ::: shortageCountTotal ==>>> " + shortageCountTotal);
				}
			} else {
				AppUtil.addWarn("No Record Found");
				return null;
			}

		} catch (Exception e) {
			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	/**
	 * purpose:to redirect to list page from add,view pages
	 * 
	 * @return list page url
	 */
	public String cancel() {
		addButtonFlag = false;
		approveButtonFlag = false;
		generateFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		generateButtonFlag = false;
		entitytip = "";
		designationtip = "";
		return loadStaffEligibility();
	}

	/**
	 * purpose:post staffeligibility details to server
	 * 
	 * @return list page url if success or will notify errors
	 */
	public String saveStaffEligibility() {
		if (staffEligibilityStmt == null) {
			Util.addWarn("StaffEligigbility is empty");
			return null;
		}
		if (staffEligibilityStmt.getStaffEligibilityDetails() == null) {
			Util.addWarn("Generate and submit. StaffEligibility Statement"
					+ staffEligibilityStmt.getDepartment().getName() + " is empty");
			return null;
		}
		if (StringUtils.isEmpty(staffEligibilityStmt.getNote())) {
			Util.addWarn("Note is empty");
			return null;
		}

		if (userMaster != null && userMaster.getId() != null) {
			staffEligibilityStmt.setForwardToId(userMaster.getId());
		}

		try {

			mapper = new ObjectMapper();
			BaseDTO response = httpService.post(SERVER_URL + "/staffeligibility/add", staffEligibilityStmt);
			if (response.getStatusCode() == 0) {
				log.info("StaffEligigbility saved Successfully .");
				Util.addInfo("Staff Eligibility Statement Created  Successfully ");
				// errorMap.notify(response.getStatusCode());
				return loadStaffEligibility();
			} else {
				log.info("StaffEligigbility saved Failed .");
				Util.addError("Staff Eligibility Statement Create Failed. ");
				// errorMap.notify(response.getStatusCode());
				return null;
			}

		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	/**
	 * purpose: get roster details
	 * 
	 * @return redirect to approval page
	 */
	public String getStaffEligibilityForApproval() {
		if (selectedStaffEligibilityStmt == null) {
			Util.addWarn("Please Select One StaffEligigbility");
			return null;
		}
		getStaffEligibility();
		if (staffEligibilityStmt.getStatus().equalsIgnoreCase("Approved")) {
			Util.addWarn("Record is already approved");
			return null;
		}
		if (staffEligibilityStmt.getStatus().equalsIgnoreCase("Rejected")) {
			Util.addWarn("Record is already rejected");
			return null;
		}

		return "/pages/personnelHR/staffEligibilityStatementMdApproval.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:when row is selected this method will be called for disabling or
	 * enabling buttons in list page
	 * 
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		selectedStaffEligibilityStmt = ((StaffEligibilityStatement) event.getObject());
		addButtonFlag = true;
		if (selectedStaffEligibilityStmt.getStatus().equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
			deleteFlag = true;
			approveButtonFlag = true;
			generateFlag = false;
			editButtonFlag = false;

		} else if (selectedStaffEligibilityStmt.getStatus().equalsIgnoreCase(ApprovalStatus.REJECTED.toString())) {
			approveButtonFlag = true;
			generateFlag = true;
			deleteFlag = false;
			editButtonFlag = true;
		} else if (selectedStaffEligibilityStmt.getStatus().equalsIgnoreCase(ApprovalStatus.INPROGRESS.toString())) {
			generateFlag = true;
			approveButtonFlag = false;
			deleteFlag = false;
			editButtonFlag = false;
		}
	}

	/**
	 * purpose:used to find selected staff eligibility
	 * 
	 */
	public void getStaffEligibility() {
		log.info("... get Staff Eligibility  called ...." + staffEligibilityStmt);
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.get(SERVER_URL + "/staffeligibility/get/" + selectedStaffEligibilityStmt.getId() + "/"
							+ (selectedNotificationId != null ? selectedNotificationId : 0));
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			staffEligibilityStmt = mapper.readValue(jsonResponse, new TypeReference<StaffEligibilityStatement>() {
			});
			if (selectedNotificationId != null) {
				systemNotificationBean.loadTotalMessages();
			}
			if (staffEligibilityStmt.getStatus() != null && staffEligibilityStmt.getStatus().equals("InProgress")) {
				String diffDays = Util.diffDaysBetweenTwoDates(staffEligibilityStmt.getCreatedDate(), new Date());
				staffEligibilityStmt.setNoOfDaysForApproval(diffDays);
			}

		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	/**
	 * purpose:will open dialog box if approved or reject is clicked in approval
	 * page
	 * 
	 * @return
	 */
	public String approveStaffEligibility() {
		try {
			if (staffEligibilityStmt != null) {
				for (StaffEligibilityDetails staffEligibilityDetails : staffEligibilityStmt
						.getStaffEligibilityDetails()) {
					log.info("staffEligibilityDetails" + staffEligibilityDetails.getApprovedCount());
					if (staffEligibilityDetails.getApprovedCount() < 0) {
						AppUtil.addError("Approved count should not allowed negative value");
						return null;
					}
				}
			}
			if (selectedStaffEligibilityStmt == null) {
				AppUtil.addError("Please Select One Staff Eligibility");
				return null;
			} else {
				approveRejectAction = ApprovalStatus.APPROVED.toString();
				staffEligibilityStmt.setStatus(ApprovalStatus.FINAL_APPROVED.toString());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStaffEligiApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String rejectStaffEligibility() {
		try {
			if (selectedStaffEligibilityStmt == null) {
				Util.addWarn("Please Select One Staff Eligibility");
				return null;
			} else {
				approveRejectAction = ApprovalStatus.REJECTED.toString();
				staffEligibilityStmt.setStatus(ApprovalStatus.REJECTED.toString());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStaffEligiApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/**
	 * purpose:once it is confirmed to approve or reject in dialog box will put
	 * approve status to server
	 * 
	 * @return redirect to list page url
	 */
	public String confirmApproval() {
		log.info("... Connfirm approval for staffeligibility ....");
		try {

			mapper = new ObjectMapper();
			BaseDTO response = httpService.put(SERVER_URL + "/staffeligibility/approve", staffEligibilityStmt);
			errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadStaffEligibility();
	}

	/**
	 * purpose:to redirect to view page
	 * 
	 * @return
	 */
	public String viewStaffEligibility() {
		if (selectedStaffEligibilityStmt == null) {
			Util.addWarn("Please Select One Staff Eligibility");
			return null;
		}
		getStaffEligibility();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());

		/*** Sum of shortage count ***/
		if (staffEligibilityStmt != null && staffEligibilityStmt.getStaffEligibilityDetails() != null) {
			int shortageCountTotal = staffEligibilityStmt.getStaffEligibilityDetails().stream()
					.filter(sed -> sed.getShortageCount() != null && sed.getShortageCount() >= 0)
					.mapToInt(sed -> sed.getShortageCount()).sum();
			staffEligibilityStmt.setShortageCountTotal(shortageCountTotal);
			log.info(" viewStaffEligibility ::: shortageCountTotal ==>>> " + shortageCountTotal);
		}

		if (selectedStaffEligibilityStmt != null) {
			setPreviousApproval(staffEligibilityStmt.getFinalApprove());
			if ("VIEW".equalsIgnoreCase(action)) {
				if (staffEligibilityStmt.getForwardToId() != null) {
					if (loginBean.getUserMaster().getId().equals(staffEligibilityStmt.getForwardToId())) {
						if (ApprovalStatus.REJECTED.equals(selectedStaffEligibilityStmt.getFinalstage())) {
							approvalFlag = false;
						} else {
							approvalFlag = true;
						}
					} else {
						approvalFlag = false;
					}
				}
				setFinalApproveFlag(staffEligibilityStmt.getFinalApprove());
				if (staffEligibilityStmt.getFinalApprove()
						&& loginBean.getUserMaster().getId().equals(staffEligibilityStmt.getForwardToId())) {

					buttonFlag = true;
					log.info("buttonflagggg" + buttonFlag);
					return APPROVING_PAGE;
				}
				selectedStaffEligibilityStmt.setApprovalRemarks(null);

				return VIEW_PAGE;
			}
		}
		return null;

//		if (staffEligibilityStmt.getStatus().equalsIgnoreCase("Approved")) {
//			Util.addWarn("Record is already approved");
//			return null;
//		}
//		if (staffEligibilityStmt.getStatus().equalsIgnoreCase("Rejected")) {
//			Util.addWarn("Record is already rejected");
//			return null;
//		}
//
//		return "/pages/personnelHR/staffEligibilityStatementMdApproval.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:dialog will be opened if delete button clicked in list page
	 * 
	 * @return
	 */
	public String deleteStaffEligibility() {
		try {
			if (selectedStaffEligibilityStmt == null) {
				Util.addWarn("Please Select One StaffEligigbility");
				return null;
			} else {
				getStaffEligibility();
				if (staffEligibilityStmt.getStatus().equalsIgnoreCase("Approved")) {
					Util.addWarn("Record is already approved");
					return null;
				}

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStaffEligiDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/**
	 * purpose:once delete confirmation given record will be deleted. and redirected
	 * to list page
	 * 
	 * @return
	 */
	public String deleteStaffEligiConfirm() {
		log.info("... Connfirm Delete for staffeligibility ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.delete(SERVER_URL + "/staffeligibility/delete/" + selectedStaffEligibilityStmt.getId());
			errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			addButtonFlag = false;
		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadStaffEligibility();
	}

	/**
	 * purpose:drill down pop up when each region selected
	 * 
	 * @param detail
	 * @return
	 */
	public String viewShowroomDetails(StaffEligibilityDetails detail) {
		log.info("inside drill down chart............" + detail);
		try {
			if (detail == null) {
				Util.addWarn("Please Select One StaffEligigbility");
				return null;
			} else {
				selectedStaffEligibilityDetail = detail;
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('showroomdetail').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public void prepareSalesMonthYear() {
		String sDate = "01/01/2016";
		Date startDate;
		try {
			startDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
			salesFromMonthYearList = AppUtil.generatePastMnthYear(startDate);
			salesToMonthYearList = AppUtil.generatePastMnthYear(startDate);
		} catch (ParseException e) {
			log.error("Error while generation sales month year period--->", e);
		}
		log.info("RetailProductionPlanBean.preparePlanFromMonthYear Method Completed : " + salesFromMonthYearList);
	}

	public void changeEntity() {
		entitytip = "";
		if (staffEligibilityStmt.getStaffEligibilityDetails() != null)
			for (EntityMaster entity : entityMasterList) {

				if (staffEligibilityStmt.getRegionList().contains(entity.getId())) {
					if (entitytip != "") {
						entitytip = entitytip + ", ";
					}
					entitytip = entitytip + entity.getName();
				}
			}
	}

	public void changeDesignations() {
		designationtip = "";
		if (staffEligibilityStmt.getDesignation() != null)
			for (Designation desig : staffEligibilityStmt.getDesignation()) {
				if (designationtip != "") {
					designationtip = designationtip + ", ";
				}
				designationtip = designationtip + desig.getName();
			}
	}

	public void loadAllEntityList() {
		log.info("... Load Entity called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/entitymaster/getallactive");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading entity....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadAllDesignationList() {
		log.info("... Load Designation List called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/designation/getalldesignation");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public String currencyFormat(Double value) {
		if (value < 1000) {
			if (value == 0)
				return "0.00";
			return format("###.00", value);
		} else {
			double hundreds = value % 1000;
			int other = (int) (value / 1000);
			return format(",##", other) + ',' + format("000.00", hundreds);
		}
	}

	private static String format(String pattern, Object value) {
		return new DecimalFormat(pattern).format(value);
	}

	public String editStaffEligibility() {
		if (selectedStaffEligibilityStmt == null) {
			Util.addWarn("Please Select One StaffEligigbility");
			return null;
		}
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
		List<Long> recruitmentYears = new ArrayList<Long>();
		recruitmentYears.add((long) Calendar.getInstance().get(Calendar.YEAR));
		recruitmentYears.add((long) Calendar.getInstance().get(Calendar.YEAR) + 1);
		recruitmentYearList = recruitmentYears;
		getStaffEligibility();
		/*** Sum of shortage count ***/
		if (staffEligibilityStmt != null && staffEligibilityStmt.getStaffEligibilityDetails() != null) {
			int shortageCountTotal = staffEligibilityStmt.getStaffEligibilityDetails().stream()
					.filter(sed -> sed.getShortageCount() != null && sed.getShortageCount() >= 0)
					.mapToInt(sed -> sed.getShortageCount()).sum();
			staffEligibilityStmt.setShortageCountTotal(shortageCountTotal);
			log.info(" viewStaffEligibility ::: shortageCountTotal ==>>> " + shortageCountTotal);
		}
		return "/pages/personnelHR/createStaffEligibilityStatement.xhtml?faces-redirect=true";
	}

	public String editStaffEligibilityDetails() {
		if (staffEligibilityStmt == null) {
			Util.addWarn("StaffEligigbility is empty");
			return null;
		}
		if (staffEligibilityStmt.getStaffEligibilityDetails() == null) {
			Util.addWarn("Generate and submit. StaffEligibility Statement"
					+ staffEligibilityStmt.getDepartment().getName() + " is empty");
			return null;
		}
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.post(SERVER_URL + "/staffeligibility/edit", staffEligibilityStmt);
			if (response.getStatusCode() == 0) {
				log.info("StaffEligigbility Updated Successfully .");
				Util.addInfo("StaffEligigbility Updated Successfully ");
				// errorMap.notify(response.getStatusCode());
				return loadStaffEligibility();
			} else {
				log.info("StaffEligigbility Updated Failed .");
				Util.addInfo("StaffEligigbility Updated Failed. ");
				// errorMap.notify(response.getStatusCode());
				return null;
			}

		} catch (Exception e) {

			log.error("Exception occured while Generating StaffEligibility....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	public String approveFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info(" approveFLow StaggEligibilty id----------" + selectedStaffEligibilityStmt.getId());
			baseDTO = new BaseDTO();
			if (previousApproval == false && selectedStaffEligibilityStmt.getFinalApprove() == true) {
				selectedStaffEligibilityStmt.setFinalstage(ApprovalStatus.APPROVED.toString());
			} else if (previousApproval == true && selectedStaffEligibilityStmt.getFinalApprove() == true) {
				selectedStaffEligibilityStmt.setFinalstage(ApprovalStatus.FINAL_APPROVED.toString());
			} else {
				selectedStaffEligibilityStmt.setFinalstage(ApprovalStatus.APPROVED.toString());
			}
			if (forwardToUser != null && forwardToUser.getId() != null) {
				selectedStaffEligibilityStmt.setForwardToId(forwardToUser.getId());
			}

			log.info("approve remarks=========>" + selectedStaffEligibilityStmt.getApprovalRemarks());
			String url = SERVER_URL + "/staffeligibility/approveFlow";
			baseDTO = httpService.post(url, selectedStaffEligibilityStmt);
			if (baseDTO.getStatusCode() == 0) {
				Util.addInfo("Staff Eligibility Approved Successfully");
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadStaffEligibility();
			}
		} catch (Exception e) {
			log.error("approveFLow method inside exception-------", e);
		}
		return null;
	}

	public String rejectFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info("approveFLow StaggEligibilty id----------" + selectedStaffEligibilityStmt.getId());
			baseDTO = new BaseDTO();
			selectedStaffEligibilityStmt.setFinalstage(ApprovalStatus.REJECTED.toString());
			String url = SERVER_URL + "/staffeligibility/rejectFlow";
			baseDTO = httpService.post(url, selectedStaffEligibilityStmt);
			if (baseDTO.getStatusCode() == 0) {
				Util.addWarn("Staff Eligibility Rejected Successfully");
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadStaffEligibility();
			}
		} catch (Exception e) {
			log.error("rejectjv method inside exception-------", e);
		}
		return null;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("Claims showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String staffEligibilityStmtId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			staffEligibilityStmtId = httpRequest.getParameter("staffEligibilityStmtId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("staffEligibilityStmtId" + staffEligibilityStmtId);
		}
		if (StringUtils.isNotEmpty(staffEligibilityStmtId)) {
			// init();
			selectedStaffEligibilityStmt = new StaffEligibilityStatement();
			action = "VIEW";

			selectedStaffEligibilityStmt.setId(Long.parseLong(staffEligibilityStmtId));
			selectedNotificationId = Long.parseLong(notificationId);

			viewStaffEligibility();
		}
		log.info(" showViewListPage() Ends");

		return null;
	}

}
