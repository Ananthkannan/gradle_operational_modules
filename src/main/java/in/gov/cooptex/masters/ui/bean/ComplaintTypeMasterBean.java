package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.ComplaintTypeMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service
@Scope("session")
@Log4j2
public class ComplaintTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listComplaintTypeMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createComplaintTypeMaster.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/viewComplaintTypeMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;
	@Autowired
	AppPreference appPreference;

	
	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	ComplaintTypeMaster complaintTypeMaster, selectedComplaintTypeMaster;

	@Getter
	@Setter
	LazyDataModel<ComplaintTypeMaster> lazyComplaintTypeMasterList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;
	
	@Getter
	@Setter
	List<ComplaintTypeMaster> complaintTypeMasterList;

	public String showListPage() {
		log.info("-----strat showListPage -----");
		addButtonFlag = false;
		editButtonFlag = true;
		viewButtonFlag = true;
		selectedComplaintTypeMaster = new ComplaintTypeMaster();
		complaintTypeMaster = new ComplaintTypeMaster();
		loadLazyComplaintTypeMasterList();
		return LIST_PAGE;
	}
	
	public void loadLazyComplaintTypeMasterList() {
		log.info("<===== Starts ComplaintTypeMasterBean.loadLazyComplaintTypeMasterList ======>");
		 complaintTypeMasterList = new ArrayList<ComplaintTypeMaster>();
		lazyComplaintTypeMasterList = new LazyDataModel<ComplaintTypeMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<ComplaintTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/complainttype/getalllazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						complaintTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<ComplaintTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return complaintTypeMasterList;
			}

			@Override
			public Object getRowKey(ComplaintTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ComplaintTypeMaster getRowData(String rowKey) {
				for (ComplaintTypeMaster complaintTypeMasters : complaintTypeMasterList) {
					if (complaintTypeMasters.getId().equals(Long.valueOf(rowKey))) {
						complaintTypeMaster = complaintTypeMasters;
						return complaintTypeMasters;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  ComplaintTypeMasterBean.loadLazyComplaintTypeMasterList  ======>");
	}

	public String createPage() {
		complaintTypeMaster = new ComplaintTypeMaster();
		return ADD_PAGE;
	}
 
	public String saveOrUpdate() {
		log.info("saveOrUpdate method starts---"); 
		
		try {
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/complainttype/create";
			BaseDTO baseDTO = httpService.post(URL, complaintTypeMaster);
			if (baseDTO != null) {
				if(baseDTO.getStatusCode().equals(MastersErrorCode.DUPLICATE_COMPLAINT_TAMIL_NAME)) {
					AppUtil.addError("Duplicate  Complaint Name(In Tamil) Should not be allowed");	
					return null;
				}
				if (baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
					if ("ADD".equals(action)) {
						AppUtil.addInfo("Complaint Type saved successfully");
					} else if ("EDIT".equals(action)) {
						AppUtil.addInfo("Complaint Type updated successfully");
					}
				} else if(baseDTO.getStatusCode().equals(ErrorDescription.LEAVETYPE_NAME_DUPLICATE.getCode())){
					AppUtil.addError("Complaint Type name already exists");
					return null;
				}else {
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("saveOrUpdate exception----" + e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("saveOrUpdate method ends---");
		return showListPage();
	}

	public String viewOrEdit() {
		log.info("viewOrEdit method starts------");
		try {
			if (selectedComplaintTypeMaster == null && selectedComplaintTypeMaster.getId() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE.getErrorCode());
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/complainttype/get/"
					+ selectedComplaintTypeMaster.getId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				complaintTypeMaster = mapper.readValue(jsonResponse, new TypeReference<ComplaintTypeMaster>() {
				});
			}
			if ("VIEW".equals(action)) {
				return VIEW_PAGE;
			} else if ("EDIT".equals(action)) {
				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("saveOrUpdate exception----" + e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		log.info("viewOrEdit method ends------");
		return null;
	}
	
	public void onRowSelect(SelectEvent event) {
		selectedComplaintTypeMaster  = (ComplaintTypeMaster) event.getObject();
		addButtonFlag = true;
		editButtonFlag = false;
		viewButtonFlag = false;
	}

	
}
