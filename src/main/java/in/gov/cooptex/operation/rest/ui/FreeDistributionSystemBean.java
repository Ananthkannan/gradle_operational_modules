package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StatusMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.dto.AdditionalBudgetDTO;
import in.gov.cooptex.operation.dto.CircleWiseDTO;
import in.gov.cooptex.operation.dto.FreeDistributionSystemDTO;
import in.gov.cooptex.operation.dto.GovtSchemePlanResponseDTO;
import in.gov.cooptex.operation.enums.GovtSchemePlanStatus;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemePlanItems;
import in.gov.cooptex.operation.model.GovtSchemeType;
import in.gov.cooptex.operation.production.model.GovtSchemePlanNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("freeDistributionSystemBean")
@Scope("session")
public class FreeDistributionSystemBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlan;

	@Getter
	@Setter
	LazyDataModel<GovtSchemePlan> govtSchemePlanList;

	@Getter
	@Setter
	LazyDataModel<GovtSchemePlanResponseDTO> govtSchemePlanResponseDTOList;

	@Getter
	@Setter
	GovtSchemePlanResponseDTO govtSchemePlanResponseDTO;

	ObjectMapper mapper;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	List<GovtSchemePlan> planNameList;

	@Getter
	@Setter
	GovtSchemePlanItems govtSchemePlanItems;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	CircleMaster circleMaster;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	StatusMaster statusMaster;

	@Getter
	@Setter
	Date currentDate = new Date();

	@Getter
	@Setter
	List<StatusMaster> statusMasterList;

	@Getter
	@Setter
	Boolean commentShow = false;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Set<String> schemePeriodList = new LinkedHashSet<String>();

	@Getter
	@Setter
	String viewStatus = null;

	private static final String schemePeriods = "Pongal,Diwali,Annual,Other";

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String disableAction = null;

	WrapperDTO wrapperDTO = null;

	int isItFirstTime;

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlanSearch;

	@Getter
	@Setter
	String schemePeriodCode;

	@Getter
	@Setter
	private Date planCreatedFrom;

	@Getter
	@Setter
	private Date planCreatedTo;

	@Getter
	@Setter
	String commentName;

	@Getter
	@Setter
	Set<GovtSchemePlanItems> planItems = new HashSet<>();

	@Getter
	@Setter
	Map<String, Set<GovtSchemePlanItems>> circleMap;

	@Getter
	@Setter
	List<GovtSchemeType> govtSchemeTypeList;

	@Getter
	@Setter
	List<GovtSchemePlanResponseDTO> viewNoteEmployeeDetails;

	private static final String CREATE_FDS = "/pages/operation/govtscheme/fds/createFDS.xhtml?faces-redirect=true;";

	private static final String LIST_FDS = "/pages/operation/govtscheme/fds/listFDS.xhtml?faces-redirect=true;";

	private static final String VIEW_FDS = "/pages/operation/govtscheme/fds/viewFDS.xhtml?faces-redirect=true;";

	private static final String PREVIEW_FDS = "/pages/operation/govtscheme/fds/previewFDS.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	String schemeTypeCode;

	@Getter
	@Setter
	Double currentYearProductionQuantity;

	@Getter
	@Setter
	Double openingStockQuantity;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<GovtSchemePlanItems> planItemsList = new ArrayList<>();

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<FreeDistributionSystemDTO> approveCommentList = new ArrayList<>();

	@Getter
	@Setter
	FreeDistributionSystemDTO userReplyComment = new FreeDistributionSystemDTO();

	@Getter
	@Setter
	List<FreeDistributionSystemDTO> rejectAndReplyCommentList = new ArrayList<>();

	@Getter
	@Setter
	Map<Long, Set<FreeDistributionSystemDTO>> approvedCommentMap = new LinkedHashMap<Long, Set<FreeDistributionSystemDTO>>();

	@Getter
	@Setter
	Map<Long, Set<FreeDistributionSystemDTO>> rejectedCommentMap = new LinkedHashMap<Long, Set<FreeDistributionSystemDTO>>();

	@Getter
	@Setter
	List<FreeDistributionSystemDTO> staticReplyCommentList = new ArrayList<>();

	@Getter
	@Setter
	GovtSchemePlanNote planNote = new GovtSchemePlanNote();

	@Getter
	@Setter
	GovtSchemePlanNote lastNote = new GovtSchemePlanNote();

	@Getter
	@Setter
	int selectedIndex;

	@Getter
	@Setter
	Date minDate = null;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String planNoteCreatdDate;

	@Getter
	@Setter
	String userReplycomment;

	@Getter
	@Setter
	Map<Long, Set<GovtSchemePlanItems>> groupWiseMap = new LinkedHashMap<Long, Set<GovtSchemePlanItems>>();

	/*
	 * Current Qty Key wise map
	 */
	@Getter
	@Setter
	Map<String, Double> keyWiseMap = new LinkedHashMap<>();

	/*
	 * Opening stock key wise map
	 */
	@Getter
	@Setter
	Map<String, Double> osKeyWiseMap = new LinkedHashMap<>();

	@Getter
	@Setter
	Map<String, Double> itemWiseTotalMap = new LinkedHashMap<>();

	@Getter
	@Setter
	Map<String, Double> osItemWiseTotalMap = new LinkedHashMap<>();

	@Getter
	@Setter
	Map<String, Map<String, Set<GovtSchemePlanItems>>> circleWiseMap = new LinkedHashMap<String, Map<String, Set<GovtSchemePlanItems>>>();

	@Getter
	@Setter
	List<CircleWiseDTO> circlewiseList;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	boolean showApprovedTable = false;

	@Getter
	@Setter
	boolean showRejectTable = false;

	@Getter
	@Setter
	boolean showReplyBox = true;

	@Getter
	@Setter
	boolean viewenableFlag = false;

	public FreeDistributionSystemBean() {
		log.info("Inside FreeDistributionSystemBean()>>>>>>>>>");
		isItFirstTime = 0;
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	/**
	 * 
	 */
	public void processProductGroups() {
		log.info("Inside processProductGroups()>>>>>>>>>");
		if (productCategory != null) {
			log.info("load productGroups with productCategory  " + productCategory.getProductCatCode() + "/"
					+ productCategory.getProductCatName());
			productGroupList = commonDataService.loadActiveProductGroups(productCategory);
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();
		}

	}

	/**
	 * 
	 */
	public void processProductVarieties() {
		log.info("Inside processProductVarieties()>>>>>>>>>");
		if (productGroupMaster != null) {
			log.info("load ProductVarieties with productGroupMaster  " + productGroupMaster.getCode() + "/"
					+ productGroupMaster.getName());
			productList = commonDataService.loadActiveProductVarieties(productGroupMaster);
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void prepareSchemePeriod() {
		log.info("Inside prepareSchemePeriod()>>>>>>>>>");

		String schemePeriod[] = schemePeriods.split(",");
		for (String periodName : schemePeriod) {
			schemePeriodList.add(periodName);
		}
	}

	public String cancel() {
		log.info("Inside cancel()>>>>>>>>>");
		govtSchemePlan = new GovtSchemePlan();
		govtSchemePlanItems = new GovtSchemePlanItems();
		disableAction = null;
		planItems = new LinkedHashSet<GovtSchemePlanItems>();
		circleMap = new LinkedHashMap<String, Set<GovtSchemePlanItems>>();
		isItFirstTime = 0;
		return LIST_FDS;
	}

	public String submitPlan(String statusForLog) {
		log.info("Inside submitPlan() action>>>>>>>" + action + ", Status For Log " + statusForLog);

		log.info("Forward to >>>>>>>>> " + planNote.getForwardTo().getUsername());
		log.info("Is Final Approve >>>>>>>>> " + planNote.getIsFinalApproval());
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");

		govtSchemePlan.setStatusForLog(statusForLog);

		govtSchemePlan.setForwardToId(planNote.getForwardTo().getId());
		govtSchemePlan.setIsFinalApprove(planNote.getIsFinalApproval());
		govtSchemePlan.setNote(planNote.getNote());

		BaseDTO baseDTO = null;
		List<GovtSchemePlanItems> itemsList = new ArrayList<>();
		/**
		 * this loop is used to save the record in child table along with circle name.
		 */
		for (Map.Entry<String, Set<GovtSchemePlanItems>> entry : circleMap.entrySet()) {
			Set<GovtSchemePlanItems> value = entry.getValue();
			for (GovtSchemePlanItems item : value) {
				itemsList.add(item);
			}
		}
		govtSchemePlan.setPlanItems(itemsList);
		String createPath = null;
		try {
			if (action.equals("EDIT")) {
				if (govtSchemePlanResponseDTO.getGovtSchemePlanId() != null) {
					govtSchemePlan.setId(govtSchemePlanResponseDTO.getGovtSchemePlanId());
				}
			}
			if (action.equals("ADD")) {
				createPath = appPreference.getPortalServerURL() + "/fdsplan/create";
			} else if (action.equals("EDIT")) {
				createPath = appPreference.getPortalServerURL() + "/fdsplan/update";
			}
			log.info(" FDS plan createPath " + createPath);

			baseDTO = httpService.post(createPath, govtSchemePlan);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Government Scheme Production Plan has been Added Successfully");
				if (action.equals("ADD")) {
					AppUtil.addInfo("Government Scheme Production Plan has been Added Successfully");
				}
				if (action.equals("EDIT")) {
					AppUtil.addInfo("Government Scheme Production Plan has been Updated Successfully");
				}
				showFdsList();
			} else if (baseDTO.getStatusCode() == 9300) {
				AppUtil.addWarn("Plan Name Already Exist");
				return null;
			} else if (baseDTO.getStatusCode() == 9301) {
				errorMap.notify(ErrorDescription.EMPLOYEE_REF_NUMBER_EXISTS.getCode());
				return null;
			} else {
				log.error("Error while creating FDS Plan");
				return null;
			}
			circleMap = new LinkedHashMap<String, Set<GovtSchemePlanItems>>();
			planItems = new LinkedHashSet<GovtSchemePlanItems>();
		} catch (Exception e) {
			log.error("Error in submitPlan() >>>>>>>> ", e);
		}
		disableAction = null;

		return LIST_FDS;
	}

	public void clear() {
		log.info("Inside clear()>>>>>>>>>");
		govtSchemePlanSearch = new GovtSchemePlan();
		schemePeriodCode = null;
		statusMaster = null;
		planCreatedFrom = null;
		planCreatedTo = null;
		showFdsList();
	}

	public void setDate() {
		log.info("Inside minDate()>>>>>>>>>");
		minDate = govtSchemePlan.getPeriodFrom();
		log.info("Min Date>>>>>>>>>" + minDate);
	}

	private void loadValues() {
		log.info("<<<<<<<<<<< Inside loadValues() >>>>>>>>>>>>");
		loadEmployeeLoggedInUserDetails();
		govtSchemeTypeList = commonDataService.govtSchemeTypeList();
		prepareSchemePeriod();
		circleMasterList = commonDataService.loadAllActiveCircles();
		log.info("<=========== circleMasterList size ========>" + circleMasterList.size());
		// productCategoryList = commonDataService.loadProductCategories();
		loadFAndGProductCategories();
		loadForwardToUsers();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		productCategory = new ProductCategory();
		circleMaster = new CircleMaster();
		circleWiseMap = new LinkedHashMap<String, Map<String, Set<GovtSchemePlanItems>>>();
		circleMap = new LinkedHashMap<>();
	}

	public List<ProductCategory> loadFAndGProductCategories() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("[FreeDistributionSystemBean - loadFAndGProductCategories ]");
			String url = appPreference.getPortalServerURL() + "/category/getfandgproductcategories";
			log.info("FreeDistributionSystem  URL called : - " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
				log.info("[ FreeDistributionSystemBean -loadFAndGProductCategories : {} ]", productCategoryList.size());
			}
		} catch (Exception e) {
			log.error(" Error Occured :  : ", e);
		}
		return productCategoryList;
	}

	public String gotoFdsCreateForm() {
		disableAction = null;
		log.info("Inside gotoFdsCreateForm()>>>>>>>>>");
		schemeTypeCode = null;
		govtSchemePlan = new GovtSchemePlan();
		govtSchemePlanItems = new GovtSchemePlanItems();
		// circleMasterList = commonDataService.loadAllCircles();
		productGroupList = new ArrayList<>();
		productList = new ArrayList<>();
		currentYearProductionQuantity = null;
		openingStockQuantity = null;
		planItemsList = new ArrayList<>();
		planNote = new GovtSchemePlanNote();
		loadValues();
		return CREATE_FDS;
	}

	/*
	 * public void loadForwardToUsers() { log.info("Inside loadForwardToUsers() ");
	 * BaseDTO baseDTO = null; try { String url = appPreference.getPortalServerURL()
	 * + "/user/getallforwardtousers"; baseDTO = httpService.get(url); if (baseDTO
	 * != null) { ObjectMapper mapper = new ObjectMapper(); String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); forwardToUsersList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() { }); }
	 * } catch (Exception e) {
	 * log.error("Exception Occured While loadForwardToUsers() :: ", e); } }
	 */

	public void loadForwardToUsers() {
		log.info("Starts loadForwardToUsers() ");
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("GOVT_SCHEME_PRODUCTION_PLAN");
		log.info("Ends loadForwardToUsers() ");
	}

	public void selectItemToRemove(int index, GovtSchemePlanItems govtSchemePlanItems) {
		log.info("Start Of Selecting Item to remove from govtSchemePlanItems " + index);

		planItemsList.remove(index);

		circleMap.get(govtSchemePlanItems.getCircleMaster().getName()).remove(govtSchemePlanItems);

	}

	public void prepareCircleWiseProducts() {
		log.info("Inside prepareSchemePlan()>>>>>>>>> " + govtSchemePlan.getSchemeType());
		if (action.equalsIgnoreCase("EDIT")) {
			for (GovtSchemePlanItems planItemsListObj : planItemsList) {
				Set<GovtSchemePlanItems> setGovtSchemePlanItems = new HashSet<>(planItemsList);
				circleMap.put(planItemsListObj.getCircleMaster().getName(), setGovtSchemePlanItems);
			}
		}
		if (action.equalsIgnoreCase("ADD") || action.equalsIgnoreCase("EDIT")) {

			/**
			 * To disable the plan details after user adds the first product
			 */
			if (isItFirstTime == 0) {
				isItFirstTime++;
				disableAction = "DISABLE";
			}

			GovtSchemePlanItems addItem = new GovtSchemePlanItems();
			addItem.setCircleMaster(circleMaster);
			addItem.setProductCategoryMaster(productCategory);
			addItem.setProductGroupMaster(productGroupMaster);
			addItem.setProductVarietyMaster(productVarietyMaster);
			addItem.setCurrentYearProductionQuantity(currentYearProductionQuantity);
			addItem.setOpeningStockQuantity(openingStockQuantity);

			String circleName = null;
			if (circleMaster != null) {
				circleName = circleMaster.getName();
			}

			// for circle wise product listing
			if (circleName != null && circleMap.containsKey(circleName)) {
				planItems = circleMap.get(circleName);
				boolean productExist = true;
				// to check the duplicate product
				if (planItems != null) {
					if (planItems.contains(addItem)) {
						productExist = true;
					} else {
						productExist = false;
					}
				}
				if (!productExist) {
					planItems.add(addItem);
					currentYearProductionQuantity = null;
					openingStockQuantity = null;
					circleMap.put(circleName, planItems);
				} else {
					log.info(" Product Duplicate ");
					AppUtil.addError("Product Duplicate");
				}
			} else {
				planItems = new LinkedHashSet<GovtSchemePlanItems>();
				if (circleMaster != null) {
					planItems.add(addItem);
					currentYearProductionQuantity = null;
					openingStockQuantity = null;
				}
				circleMap.put(circleName, planItems);
			}

			planItemsList = new ArrayList<>();

			for (Map.Entry<String, Set<GovtSchemePlanItems>> entry : circleMap.entrySet()) {
				Set<GovtSchemePlanItems> planItems = entry.getValue();
				for (GovtSchemePlanItems planItem : planItems) {
					planItemsList.add(planItem);
				}
			}
			if (planItemsList != null && planItemsList.size() > 0) {
				log.info("planItemsList size" + planItemsList.size());
				// RequestContext.getCurrentInstance().execute("collaspeclose();");
			}
		}
	}

	public String backToCreatePage() {
		log.info("Inside backToCreatePage()");

		return CREATE_FDS;
	}

	private String log(Object... objts) {
		StringBuilder sb = new StringBuilder();
		if (objts != null) {

			for (Object obj : objts) {
				sb.append(obj);
			}
		}
		return sb.toString();
	}

	private String buildKey(Object... objts) {
		String rntVal = null;
		StringBuilder sb = new StringBuilder();
		if (objts != null) {

			for (Object str : objts) {
				sb.append(str).append("_");
			}
		}
		rntVal = sb.toString();
		int len = rntVal.length();
		if (rntVal.length() > 0) {
			// Remove last '_'
			rntVal = rntVal.substring(0, len - 1);
		}
		return rntVal;
	}

	public String gotoPreviewFdsPlan() {

		if ("ADD".equalsIgnoreCase(action) && StringUtils.isEmpty(planNote.getNote())) {
			log.info("Plan Note Cannot be Empty");
			// AppUtil.addError("Plan Note Cannot be Empty");
			errorMap.notify(ErrorDescription.DEPARTMENT_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
			return null;
		}

		if (planItemsList == null || planItemsList.isEmpty()) {
			log.info("Circle Wise Production Plan is Empty");
			AppUtil.addError("Circle Wise Production Plan is Empty");
			return null;
		}
		if (govtSchemePlan.getPlanName() != null && !action.equalsIgnoreCase("VIEW")
				&& !action.equalsIgnoreCase("EDIT")) {
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + "/fdsplan/getplan/" + govtSchemePlan.getPlanName();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == ErrorCodeDescription.ERROR_PLAN_EXIST.getErrorCode()) {
				AppUtil.addWarn("Plan Name Already Exist");
				return null;
			}
		}

		circleWiseMap = new LinkedHashMap<String, Map<String, Set<GovtSchemePlanItems>>>();
		groupWiseMap = new LinkedHashMap<Long, Set<GovtSchemePlanItems>>();
		keyWiseMap = new LinkedHashMap<>();
		osKeyWiseMap = new LinkedHashMap<>();
		Set<Long> circleIdSet = new LinkedHashSet<Long>();
		Set<Long> groupIdSet = new LinkedHashSet<Long>();
		Set<Long> itemIdSet = new LinkedHashSet<Long>();

		itemWiseTotalMap = new LinkedHashMap<String, Double>();
		osItemWiseTotalMap = new LinkedHashMap<String, Double>();

		Set<String> keyIdSet = new LinkedHashSet<String>();
		if (planItemsList != null) {
			for (GovtSchemePlanItems item : planItemsList) {
				CircleMaster itemCircleMaster = item.getCircleMaster();
				ProductGroupMaster itemGroupMaster = item.getProductGroupMaster();
				Long circleId = itemCircleMaster.getId();
				Long groupId = itemGroupMaster.getId();
				Long itemId = item.getProductVarietyMaster().getId();

				circleIdSet.add(circleId);
				groupIdSet.add(groupId);
				itemIdSet.add(itemId);

				String keyId = buildKey(circleId, groupId, itemId);
				String groupItemKey = buildKey(groupId, itemId);
				log.info("keyId : " + keyId);
				log.info("groupItemKey : " + groupItemKey);
				keyWiseMap.put(keyId, item.getCurrentYearProductionQuantity());
				osKeyWiseMap.put(keyId, item.getOpeningStockQuantity());
				/*
				 * Footer Total for current year qty
				 */
				if (itemWiseTotalMap.containsKey(groupItemKey)) {
					Double value = itemWiseTotalMap.get(groupItemKey) + item.getCurrentYearProductionQuantity();
					itemWiseTotalMap.remove(groupItemKey);
					itemWiseTotalMap.put(groupItemKey, value);
				} else {
					itemWiseTotalMap.put(groupItemKey, item.getCurrentYearProductionQuantity());
				}

				if (osItemWiseTotalMap.containsKey(groupItemKey)) {
					Double value = osItemWiseTotalMap.get(groupItemKey) + item.getOpeningStockQuantity();
					osItemWiseTotalMap.remove(groupItemKey);
					osItemWiseTotalMap.put(groupItemKey, value);
				} else {
					osItemWiseTotalMap.put(groupItemKey, item.getOpeningStockQuantity());
				}

				log.info(log("circleId : ", circleId, " groupId : ", groupId, " itemId : ", itemId));
				String circleName = itemCircleMaster.getName();
				String productGroupName = itemGroupMaster.getName();
				String circleIdAndName = buildKey(circleId, circleName);

				log.info("circleName >>>> " + circleName);
				log.info("productGroupName >>>> " + productGroupName);

				if (groupWiseMap.containsKey(groupId)) {
					Set<GovtSchemePlanItems> itemSet = groupWiseMap.get(groupId);
					itemSet.add(item);
				} else {
					Set<GovtSchemePlanItems> itemSet = new LinkedHashSet<>();
					itemSet.add(item);
					groupWiseMap.put(groupId, itemSet);
				}

				if (!circleWiseMap.containsKey(circleIdAndName)) {
					Map<String, Set<GovtSchemePlanItems>> groupWiseMap = new LinkedHashMap<String, Set<GovtSchemePlanItems>>();
					Set<GovtSchemePlanItems> productItemSet = new LinkedHashSet<GovtSchemePlanItems>();
					productItemSet.add(item);
					groupWiseMap.put(productGroupName, productItemSet);
					circleWiseMap.put(circleIdAndName, groupWiseMap);
				}

				if (!circleWiseMap.isEmpty()) {
					log.info("circleWiseMap not empty");
					Map<String, Set<GovtSchemePlanItems>> groupWiseMap = circleWiseMap.get(circleIdAndName);
					Set<GovtSchemePlanItems> productItemSet = null;

					if (groupWiseMap == null) {
						log.info("groupWiseMap is null");
						groupWiseMap = new LinkedHashMap<String, Set<GovtSchemePlanItems>>();
						productItemSet = new LinkedHashSet<GovtSchemePlanItems>();
						productItemSet.add(item);
						groupWiseMap.put(productGroupName, productItemSet);
						circleWiseMap.put(circleIdAndName, groupWiseMap);
					} else {
						log.info("groupWiseMap not null");
						productItemSet = groupWiseMap.get(productGroupName);
						if (productItemSet == null) {
							log.info("productItemSet is null");
							productItemSet = new LinkedHashSet<GovtSchemePlanItems>();
							productItemSet.add(item);
							groupWiseMap.put(productGroupName, productItemSet);
						} else {
							log.info("productItemSet not null");
							productItemSet.add(item);
						}
					}

				} else {
					log.info("circleWiseMap is null");
					Map<String, Set<GovtSchemePlanItems>> groupWiseMap = new LinkedHashMap<String, Set<GovtSchemePlanItems>>();
					Set<GovtSchemePlanItems> productItemSet = new LinkedHashSet<GovtSchemePlanItems>();
					// item.setKeyId(keyId);
					productItemSet.add(item);
					groupWiseMap.put(productGroupName, productItemSet);
					circleWiseMap.put(circleIdAndName, groupWiseMap);
				}
			}
		}

		for (Long circleId : circleIdSet) {
			for (Long groupId : groupIdSet) {
				for (Long itemId : itemIdSet) {
					keyIdSet.add(buildKey(circleId, groupId, itemId));
				}
			}
		}

		for (String keyIdStr : keyIdSet) {
			if (!keyWiseMap.containsKey(keyIdStr)) {
				log.info("keyWiseMap does not contain key: " + keyIdStr);
				keyWiseMap.put(keyIdStr, 0.00D);
			}
		}

		for (String keyIdStr : keyIdSet) {
			if (!osKeyWiseMap.containsKey(keyIdStr)) {
				log.info("osKeyWiseMap does not contain key: " + keyIdStr);
				osKeyWiseMap.put(keyIdStr, 0.00D);
			}
		}

		if ("VIEW".equalsIgnoreCase(action)) {
			return VIEW_FDS;
		}
		return PREVIEW_FDS;
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");
	}

	public void test() {
		log.info("=========================================");
		log.info("=========================================");
	}

	public String showFdsList() {
		log.info("Inside FreeDistributionSystemBean showList()>>>>>>>>>");
		loadLazyGovtSchemePlanList();
		// prepareSchemePeriod();
		loadStatusValues();
		return LIST_FDS;
	}

	public void loadLazyGovtSchemePlanList() {
		log.info("<--- loadLazyPaymentVoucherList ---> ");
		planNote = new GovtSchemePlanNote();
		govtSchemePlanResponseDTOList = new LazyDataModel<GovtSchemePlanResponseDTO>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<GovtSchemePlanResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<GovtSchemePlanResponseDTO> data = new ArrayList<GovtSchemePlanResponseDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<GovtSchemePlanResponseDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyGovtSchemePlanList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(GovtSchemePlanResponseDTO res) {
				return res != null ? res.getGovtSchemePlanId() : null;
			}

			@Override
			public GovtSchemePlanResponseDTO getRowData(String rowKey) {
				List<GovtSchemePlanResponseDTO> responseList = (List<GovtSchemePlanResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (GovtSchemePlanResponseDTO res : responseList) {
					if (res.getGovtSchemePlanId().longValue() == value.longValue()) {
						govtSchemePlanResponseDTO = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		govtSchemePlanResponseDTO = new GovtSchemePlanResponseDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		GovtSchemePlan govtSchemePlan = new GovtSchemePlan();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		govtSchemePlan.setPaginationDTO(paginationDTO);

		log.info("govtSchemePlan  " + govtSchemePlan);
		govtSchemePlan.setFilters(filters);

		try {
			String govtSchemePlanSearchUrl = appPreference.getPortalServerURL() + "/fdsplan/search";
			log.info("Govt Scheme Plan Search Url " + govtSchemePlanSearchUrl);
			baseDTO = httpService.post(govtSchemePlanSearchUrl, govtSchemePlan);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	List statusValues;

	private void loadStatusValues() {
		Object[] statusArray = GovtSchemePlanStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status.toString().replace("_", "-"));
		}

		log.info("<--- statusValues ---> " + statusValues);
	}

	public void onRowSelect(SelectEvent event) {
		log.info("FDS Plan onRowSelect method started");
		govtSchemePlanResponseDTO = ((GovtSchemePlanResponseDTO) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("FDS Plan onPageLoad method started");
		addButtonFlag = false;
	}

	public String gotoViewPage() {
		log.info("Inside gotoViewPage()");

		enableFlag = false;

		employeeMaster = new EmployeeMaster();

		planItemsList = new ArrayList<>();

		planNote = new GovtSchemePlanNote();

		viewNoteEmployeeDetails = new ArrayList<>();

		BaseDTO baseDTO = null;

		try {
			if (govtSchemePlanResponseDTO == null) {
				log.info("<---Please Select any one Plan--->");
				AppUtil.addWarn("Please Select any one Plan");
				return null;
			}
			if (action.equals("EDIT")) {
				if (govtSchemePlanResponseDTO != null) {
					if (!govtSchemePlanResponseDTO.getPlanStatus().equals(ApprovalStatus.REJECTED.toString())) {
						errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getErrorCode());
						log.info("<========== Inside Cannot Edit Record =========>");
						return null;
					}
				}
			}
			String URL = appPreference.getPortalServerURL() + "/fdsplan/get";
			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.post(URL, govtSchemePlanResponseDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				govtSchemePlan = objectMapper.readValue(jsonResponse, GovtSchemePlan.class);

				log.info("govtSchemePlan " + govtSchemePlan.getPlanName());

				log.info("govtSchemePlan " + govtSchemePlan.getPlanItems().size());

				planItemsList = govtSchemePlan.getPlanItems();

				if (govtSchemePlanResponseDTO.getNotificationId() != null) {
					systemNotificationBean.loadTotalMessages();
				}

				planNote = govtSchemePlan.getGovtSchemePlanNoteList()
						.get(govtSchemePlan.getGovtSchemePlanNoteList().size() - 1);

				lastNote = govtSchemePlan.getGovtSchemePlanNoteList()
						.get(govtSchemePlan.getGovtSchemePlanNoteList().size() - 1);

				if (govtSchemePlan.getGovtSchemePlanLogList() != null) {
					viewStatus = govtSchemePlan.getGovtSchemePlanLogList()
							.get(govtSchemePlan.getGovtSchemePlanLogList().size() - 1).getStage();
					log.info("viewStatus==> " + viewStatus);
				} else {
					viewStatus = null;
				}

				// getEmployeeDetailsByUserId();
				String employeeDatajsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = objectMapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<GovtSchemePlanResponseDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewNoteEmployeeDetails.size()
						+ viewNoteEmployeeDetails);

				loadForwardToUsers();

				if (lastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
					enableFlag = true;
				}
				viewenableFlag = false;
				if ((lastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster().getId().longValue())
						&& lastNote.getIsFinalApproval()) {
					viewenableFlag = true;
				}
				gotoPreviewFdsPlan();
				if (action.equals("VIEW")) {
					getRejectedCommentsAndReplayCommentList();
					getApprovedCommentsList();
					return VIEW_FDS;

				} else if (action.equals("EDIT")) {

					loadValues();
					String employeeDatajsonResponse = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
					viewNoteEmployeeDetails = objectMapper.readValue(employeeDatajsonResponse,
							new TypeReference<List<GovtSchemePlanResponseDTO>>() {
							});
					log.info("<======= view Note Employee Details List ==========>" + viewNoteEmployeeDetails.size()
							+ viewNoteEmployeeDetails);
					return CREATE_FDS;
				}
			} else {
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return null;
	}

	@Getter
	@Setter
	private String currentPlanStatus;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;
	@Getter
	@Setter
	String finalApproveComments;

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current FDS Plan Status ---> " + currentPlanStatus);
		log.info("<---FDS Id ---> " + govtSchemePlanResponseDTO.getGovtSchemePlanId());
		log.info("<--- approveComments ---> " + approveComments);
		log.info("<--- rejectComments ---> " + rejectComments);
		log.info("<--- finalApproveComments ---> " + finalApproveComments);

		BaseDTO baseDTO = null;

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			GovtSchemePlan request = new GovtSchemePlan(govtSchemePlanResponseDTO.getGovtSchemePlanId(),
					currentPlanStatus, approveComments);
			if (planNote.getForwardTo() == null) {
				log.info("Please Select ForwardTo");
				AppUtil.addInfo("Please Select ForwardTo");
				return null;
			}
			if (planNote.getIsFinalApproval() == null) {
				log.info("Please Select ForwardFor");
				AppUtil.addInfo("Please Select ForwardFor");
				return null;
			}
			log.info("planNote.getForwardTo().getId()" + planNote.getForwardTo().getId());
			request.setForwardToId(planNote.getForwardTo().getId());
			request.setIsFinalApprove(planNote.getIsFinalApproval());
			request.setNote(planNote.getNote());

			String URL = appPreference.getPortalServerURL() + "/fdsplan/approve";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				if (govtSchemePlanResponseDTO.getSchemeTypeName() != null) {
					AppUtil.addInfo(govtSchemePlanResponseDTO.getSchemeTypeName().toString()
							+ " Plan type Approved Successfully");
				} else {
					AppUtil.addInfo("Government scheme production plan has been Approved successfully");
				}
			}

		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			GovtSchemePlan request = new GovtSchemePlan(govtSchemePlanResponseDTO.getGovtSchemePlanId(),
					currentPlanStatus, rejectComments);
			String URL = appPreference.getPortalServerURL() + "/fdsplan/reject";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				if (govtSchemePlanResponseDTO.getSchemeTypeName() != null) {
					AppUtil.addInfo(govtSchemePlanResponseDTO.getSchemeTypeName().toString()
							+ " Plan type Rejected Successfully");
				} else {
					AppUtil.addInfo("Government scheme production plan has been Rejected successfully");
				}
			}

		} else if (currentPlanStatus != null && currentPlanStatus.equals("FINAL_APPROVED")) {
			GovtSchemePlan request = new GovtSchemePlan(govtSchemePlanResponseDTO.getGovtSchemePlanId(),
					currentPlanStatus, finalApproveComments);
			String URL = appPreference.getPortalServerURL() + "/fdsplan/finalapprove";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				if (govtSchemePlanResponseDTO.getSchemeTypeName() != null) {
					AppUtil.addInfo(govtSchemePlanResponseDTO.getSchemeTypeName().toString()
							+ " Plan type final Approved Successfully");
				} else {
					AppUtil.addInfo("Government scheme production plan has been final Approved successfully");
				}
			}

		}

		/*
		 * if (baseDTO != null) { errorMap.notify(baseDTO.getStatusCode()); }
		 */
		approveComments = null;
		rejectComments = null;
		finalApproveComments = null;

		return showFdsList();
	}

	public void changeCurrentStatus(String value) {
		log.info("<---changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;

		approveComments = null;
		rejectComments = null;
		finalApproveComments = null;
	}

	public void getEmployeeDetailsByUserId() {
		log.info("Inside getEmployeeDetailsByUserId()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ planNote.getCreatedBy().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + planNote.getCreatedBy().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void getApprovedCommentsList() {

		log.info("inside the method is getApprovedCommentsList()  ");

		try {
			BaseDTO baseDTO = new BaseDTO();

			log.info("govtSchemePlan" + govtSchemePlan);
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/fdsControler/getallApprovedCommentsListByGovermentScoietyPlanId";
			baseDTO = httpService.post(url, govtSchemePlan);

			if (baseDTO.getResponseContent() != null) {
				log.info("baseDTO" + baseDTO.toString());
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				approveCommentList = mapper.readValue(jsonResponse,
						new TypeReference<List<FreeDistributionSystemDTO>>() {
						});
			}

			Set<FreeDistributionSystemDTO> approvedSet = new LinkedHashSet<>();
			approvedCommentMap = new LinkedHashMap<Long, Set<FreeDistributionSystemDTO>>();
			for (FreeDistributionSystemDTO obj : approveCommentList) {
				approvedSet = new LinkedHashSet<>();
				approvedSet.add(obj);
				approvedCommentMap.put(obj.getId(), approvedSet);
			}
			if (approvedCommentMap.size() > 0)
				showApprovedTable = true;

			log.info("approvedCommentMap" + approvedCommentMap);
			log.info("approveCommentList" + approveCommentList);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

	}

	public void getRejectedCommentsAndReplayCommentList() {
		log.info("inside the  getRejectedCommentsAndReplayCommentList mehtod");

		try {
			BaseDTO baseDTO = new BaseDTO();
			log.info("govtSchemePlan" + govtSchemePlan);
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/fdsControler/getallRejectedCommentsAndReplayCommentListByGovermentScoietyPlanId";
			baseDTO = httpService.post(url, govtSchemePlan);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				rejectAndReplyCommentList = mapper.readValue(jsonResponse,
						new TypeReference<List<FreeDistributionSystemDTO>>() {
						});
			}
			List<Long> userIdList = new ArrayList<>();
			if (rejectAndReplyCommentList.size() > 0) {
				showRejectTable = true;
				Long loginId = loginBean.getUserMaster().getId();
				log.info("date format......" + loginBean.getUserInfoDTO().getDateFormat());
				userReplyComment = new FreeDistributionSystemDTO();
				Set<FreeDistributionSystemDTO> rejectSet = new LinkedHashSet<>();
				rejectedCommentMap = new LinkedHashMap<Long, Set<FreeDistributionSystemDTO>>();
				for (FreeDistributionSystemDTO obj : rejectAndReplyCommentList) {
					rejectSet = new LinkedHashSet<>();

					userIdList = obj.getForwardToList();
					userIdList.add(govtSchemePlan.getCreatedBy().getId());
					if (obj.getForwardTo() != null) {
						if (obj.getForwardTo() == loginId)
							commentName = "Reject Comments";
						else
							commentName = "Reply Comments";
					}
					if (obj.getRejectedComments() != null)
						obj.setRejectShow(true);
					if (obj.getReplyComments() != null)
						obj.setReplyShow(true);
					if (userIdList.contains(loginId))
						commentShow = true;
					if (obj.getCurrentStage().equals(true))
						showReplyBox = false;
					if (obj.getCurrentLogStatus().equals("FINAL_APPROVED"))
						showReplyBox = false;
					if ((obj.getCurrentLogStatus().trim().equals("REJECTED")) && userIdList.contains(loginId))
						showReplyBox = true;
					if ((obj.getCurrentLogStatus().trim().equals("APPROVED"))
							&& loginId.equals(govtSchemePlan.getCreatedBy().getId()))
						showReplyBox = false;
					if ((obj.getCurrentLogStatus().trim().equals("APPROVED"))
							&& obj.getCurrentForwardToUser().equals(loginId))
						showReplyBox = true;

					log.info("current Status and showReplyBox is..." + obj.getCurrentLogStatus() + "  " + showReplyBox);
					rejectSet.add(obj);
					rejectedCommentMap.put(obj.getId(), rejectSet);
					userReplyComment.setForwardTo(obj.getForwardTo());
					userReplyComment.setId(govtSchemePlan.getId());
				}
				log.info("rejectedCommentMap" + rejectedCommentMap);
			}

			log.info("showReplayBox" + showReplyBox);

			log.info("rejectAndReplyCommentList size" + rejectAndReplyCommentList.size());
			log.info("rejectAndReplyCommentList" + rejectAndReplyCommentList);

		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

	}

	public String saveReplyCommentsByGovSchemeplanId() {
		BaseDTO baseDTO = new BaseDTO();
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/fdsControler/saveReplyCommentsByGovSchemePlanId";
		if (userReplyComment != null) {
			if (userReplyComment.getUserReplyComments() == null) {
				errorMap.notify(ErrorDescription.PLEASE_ENTER_REJECT_COMMENTS.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			log.info("govtSchemePlan" + userReplyComment);

			log.info("the reply comments is going to save is " + userReplyComment);
			try {
				baseDTO = httpService.post(url, userReplyComment);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("saveReplyCommentsByGovSchemeplanId is saved successfully");
						errorMap.notify(baseDTO.getStatusCode());

						return LIST_FDS;
					}
				} else {
					log.info("base dto is empty from data base for  saveReplyCommentsByGovSchemeplanId method");
				}

			} catch (Exception e) {
				log.error("Error while saving  ReplyCommentsByGovSchemeplanId" + e);
			}

		} else {
			log.info("the reply comments is null");
		}

		return null;
	}

	public void showapproveCommentsByDefault() {
		log.info("inside the rest method");
		getApprovedCommentsList();

	}

	public void clearFDS() {
		productGroupMaster = new ProductGroupMaster();
		productCategory = new ProductCategory();
		circleMaster = new CircleMaster();
		productVarietyMaster = new ProductVarietyMaster();
		currentYearProductionQuantity = null;
		openingStockQuantity = null;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String freeDistributionId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			freeDistributionId = httpRequest.getParameter("freeDistributionId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		govtSchemePlanResponseDTO = new GovtSchemePlanResponseDTO();
		if (!StringUtils.isEmpty(notificationId)) {
			govtSchemePlanResponseDTO.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(freeDistributionId)) {
			govtSchemePlanResponseDTO.setGovtSchemePlanId(Long.parseLong(freeDistributionId));
			action = "VIEW";
			gotoViewPage();
		}
		return VIEW_FDS;
	}

}
