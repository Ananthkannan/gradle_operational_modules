package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.http.impl.client.HttpClients;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.model.ProductCategoryGroup;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.ThreadLocalConfig;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.operation.enums.ProductCategoryStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("productCatBean")
@Scope("session")
public class ProductCategoryBean implements Serializable {
	private final String PROD_CAT_LIST_PAGE = "/pages/masters/listProductCategoryNew.xhtml?faces-redirect=true;";
	private final String PROD_CAT_CREATE_PAGE = "/pages/masters/createProductCategoryNew.xhtml?faces-redirect=true;";
	private final String PROD_CAT_VIEW_PAGE = "/pages/masters/viewProductCategoryNew.xhtml?faces-redirect=true;";
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	private String categoryCode;
	@Getter
	@Setter
	private String categoryName;
	@Getter
	@Setter
	private String activeStatusMsg = null;
	@Getter
	@Setter
	private boolean activeStatus = false;
	@Getter
	@Setter
	private String regionalName;
	@Getter
	@Setter
	private UserDTO userDto;
	@Autowired
	private LoginBean loginBean;
	@Autowired
	LanguageBean languageBean;
	@Autowired
	HttpService httpService;
	@Getter
	@Setter
	ProductCategoryDTO productCatDto;
	@Getter
	@Setter
	private RestTemplate restTemplate;
	@Getter
	@Setter
	private String Url = null;
	@Getter
	@Setter
	private List<ProductCategoryDTO> productCategoryListDto;
	@Getter
	@Setter
	private WrapperDTO wrapperDto;
	@Getter
	@Setter
	private ArrayList<ProductCategoryDTO> productCategoryList;
	/**
	 * For getting which button is clicked
	 */
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	private boolean disable;

	@Autowired
	private ErrorMap errorMap;

	@Getter
	@Setter
	private List<Object> statusValues;
	@Getter
	@Setter
	private Integer numberOfRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	LazyDataModel<ProductCategoryDTO> productCategorylazyLoadList;
	
	@Getter
	@Setter
	List<ProductCategoryGroup> productCategoryGroupList = new ArrayList<ProductCategoryGroup>();
	
	@Getter
	@Setter
	ProductCategoryGroup selectedProductCategoryGroup = new ProductCategoryGroup();
	
	BaseDTO baseDTO = new BaseDTO();
	
	@Getter
	@Setter
	private Map<String, Object> filterMap;
	@Getter
	@Setter
	private SortOrder sortingOrder;
	@Getter
	@Setter
	private String sortingField;
	@Getter
	@Setter
	private Integer resultSize;
	@Getter
	@Setter
	private Integer defaultRowSize;

	@Getter
	@Setter
	private String pageHead;
	
	ObjectMapper mapper = new ObjectMapper();
	
	String jsonResponse;

	@PostConstruct
	public void init() {
		loadStatusValues();
		lazyLoadProductcategoryList();

		// getAllCategory();
	}

	public ProductCategoryBean() {

		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
				HttpClients.createDefault());
		restTemplate = new RestTemplate(requestFactory);
		loadUrl();
		log.info("SERVER URL----" + Url);

	}

	private void loadUrl() {
		try {
			Url = AppUtil.getPortalServerURL();

		} catch (Exception e) {
			log.fatal("Exception at loadUrl() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	/**
	 * This Method Insert The Product Category In Db via REST API
	 * Table(product_category)
	 * 
	 * @return
	 */
	public String createProducCategory() {
		log.info("<<====Inside createProductCategory()======>>");
		userDto = new UserDTO();

		userDto.setId(loginBean.getUserDetailSession().getId());
		productCatDto = new ProductCategoryDTO();
		productCatDto.setProductCatName(categoryName.trim());
		productCatDto.setRegionalName(regionalName.trim());
		productCatDto.setProductCatCode(categoryCode.trim());
		productCatDto.setCreatedDate(new Date());
		productCatDto.setCreatedBy(userDto);
		productCatDto.setProductCategoryGroup(selectedProductCategoryGroup);

		if (action.equals("ADD")) {
			activeStatus = true;
		}
		if (activeStatusMsg != null && activeStatusMsg.equalsIgnoreCase("Active")) {
			activeStatus = true;
			log.info("Inside IF");
		}
		productCatDto.setActiveStatus(activeStatus);
		log.info(activeStatusMsg);
		log.info("User Id=" + userDto.getId());

		try {
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(Url + "/category/create", productCatDto);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Product Category saved successfully");
					errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_CREATE_SUCCESS.getErrorCode());

				}else if(baseDTO.getStatusCode().equals(ErrorDescription.getError(MastersErrorCode.PRODUCT_CATEGORY_LNAME_ALREADY_EXIST).getCode())) { 
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_CATEGORY_LNAME_ALREADY_EXIST).getCode());
					return null;
				}
				else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
					// return PROD_CAT_CREATE_PAGE;
				}
			}
		} catch (Exception e) {
			log.error("Error while saving Product category", e);
		}
		getAllCategory();
		activeStatus = false;
		productCatDto = null;
		categoryName = null;
		categoryCode = null;
		regionalName = null;

		return PROD_CAT_LIST_PAGE;
	}

	private void getAllCategory() {
		log.info("ProductCatagoryBean  getAllCategory================#Start");

		productCategoryListDto = new ArrayList<ProductCategoryDTO>();
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Track_Id", ThreadLocalConfig.getId());
			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			HttpEntity<WrapperDTO> response = restTemplate.exchange(Url + "/category/getAll", HttpMethod.GET, entity,
					WrapperDTO.class);
			log.info("<----Url--->" + Url);
			wrapperDto = response.getBody();
			log.info("Wrapper Dto :" + response.getBody());
			productCategoryListDto = (List<ProductCategoryDTO>) wrapperDto.getProductCategoryDTOCollections();

			log.info("productCategoryListDto " + productCategoryListDto);
			log.info(" getAllCategory ProductCategorylazyLoadList" + productCategorylazyLoadList);
		} catch (Exception e) {
			log.error("Error Occured While loading Product Category ");
		}
	}

	public String updateProductCategory() {

		log.info("Inside ProductCategoryBean=======updateProductCategory ");
		log.info("Button Pressed " + action);
		log.info("productCatDto>>>>>>>>>>>>>>>>>>> " + productCatDto.getProductCatCode());
		log.info("productCatDto>>>>>>>>>>>>>>>>>>> " + productCatDto.getId());

		try {

			if (Util.isEmpty(categoryCode) && Util.isEmpty(categoryName) && Util.isEmpty(regionalName)) {

				log.info("Select Atleast One Field:");

				categoryName = null;
				categoryCode = null;
				regionalName = null;

				return null;

			}

			if (activeStatusMsg.equals("Active")) {
				activeStatus = true;
			} else {
				activeStatus = false;
			}
			UserDTO userDto = new UserDTO();
			userDto.setId(loginBean.getUserDetailSession().getId());
			log.info("user id" + userDto.getId());
			log.info("---productCatagoryBean  --->updateData()------> ");
			productCatDto.setProductCatCode(categoryCode.trim());
			productCatDto.setProductCatName(categoryName.trim());
			productCatDto.setRegionalName(regionalName.trim());
			productCatDto.setActiveStatus(activeStatus);
			productCatDto.setModifiedBy(userDto);
			productCatDto.setModifiedDate(new Date());
			productCatDto.setProductCategoryGroup(selectedProductCategoryGroup);
			// productCatDto.setActiveStatusMsg(activeStatusMsg);

			log.info("<<<<<<<<<<>>>>>>>>>>>>>>>" + productCatDto.getProductCatCode());

			BaseDTO baseDTO = null;
			baseDTO = httpService.post(Url + "/category/update", productCatDto);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Data succesfully updated");
					errorMap.notify(ErrorDescription.PRODUCT_CATEGORY_UPDATE_SUCCESS.getErrorCode());

				} else if(baseDTO.getStatusCode().equals(ErrorDescription.getError(MastersErrorCode.PRODUCT_CATEGORY_LNAME_ALREADY_EXIST).getErrorCode())) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_CATEGORY_LNAME_ALREADY_EXIST).getErrorCode());
					return null;
				}
				else {

					errorMap.notify(baseDTO.getStatusCode());
					return PROD_CAT_CREATE_PAGE;
					// return null; growl message prob
				}
			}
			getAllCategory();
		} catch (Exception e) {
			log.error("Error occured while updating product category", e);
		}
		disable = false;
		productCatDto = null;
		categoryName = null;
		categoryCode = null;
		regionalName = null;
		// activeStatus = false;
		log.info("productCtegory updateData===============#End");
		// log.info(">>>>>>>>>>>"+productCatDto.getActiveStatus());
		addButtonFlag = true;
		return PROD_CAT_LIST_PAGE;

	}

	public String populateCreateForm() {
		log.info("inside populate create form>>>>>>>>>");

		if (productCatDto == null) {
			action = null;
			log.info("inside populate create form >>> IF  >>>>>>>>>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return null;
		}

		log.info(" The Button Action is>>>>>>>>>>" + action);
		try {
		
		baseDTO = httpService.get(Url + "/category/get/"+productCatDto.getId());
		ObjectMapper mapper = new ObjectMapper();
		String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
		productCatDto = mapper.readValue(jsonResponse, ProductCategoryDTO.class);
		selectedProductCategoryGroup = productCatDto.getProductCategoryGroup();
		categoryCode = productCatDto.getProductCatCode();
		categoryName = productCatDto.getProductCatName();
		regionalName = productCatDto.getRegionalName();
		activeStatus = productCatDto.getActiveStatus();
		if (activeStatus) {
			activeStatusMsg = "Active";
		} else {
			activeStatusMsg = "Inactive";
		}

		productCatDto.setCreatedBy(productCatDto.getCreatedBy());
		productCatDto.setCreatedDate(productCatDto.getCreatedDate());
		log.info("code+++++ " + productCatDto.getProductCatCode());
		log.info("status+++++ " + productCatDto.getActiveStatus());
		} catch (Exception e) {
			log.error("<<======  ERROR:: ",e);
		} 

		return PROD_CAT_LIST_PAGE;

	}

	public String showCreateProductCatPage() {
		log.info("inside ProductCategoryBean  showCreateProductCatPage ======");
		log.info("Button Pressed " + action);
		if (action.equals("EDIT")) {
			pageHead = "Edit";
			log.info("Inside showCreateProductCatPage()>>> IF >>>>");
			populateCreateForm();
		} else {
			pageHead = "Create";
			categoryCode = null;
			categoryName = null;
			regionalName = null;
		}
		if (action == null) {
			return PROD_CAT_LIST_PAGE;
		}
		getCategoryGroupMasterList();
		log.info("showCreateProductCatPage()>>>> outside IF>>>");
		return PROD_CAT_CREATE_PAGE;
	}

	public String showListProductCategoryPage() {
		log.info("Inside ProductCategoryBean>>>> showListProductCategoryPage()");
		addButtonFlag = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		productCatDto = new ProductCategoryDTO();
		lazyLoadProductcategoryList();

		// getAllCategory();
		return PROD_CAT_LIST_PAGE;
	}

	public String deleteProductCategory() {
		log.info("Inside ProductCategoryBean>>>>>> deleteProductCategory>>>>> ");
		log.info(" >>>>>>>>>>>> " + productCatDto);
		try {
			if (action.equals("DELETE") && productCatDto == null) {

				log.info("inside deleteProductCategory>>> IF  >>>>>>>>>");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				// return null;
				return PROD_CAT_LIST_PAGE;
			}
			BaseDTO baseDTO = httpService.post(Url + "/category/delete", productCatDto);
			log.info("-deleted-BaseDto Object--  " + baseDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					//errorMap.notify(baseDTO.getStatusCode());
					log.info("ProductCategory Deleted............................................");
					AppUtil.addInfo("Product Category Deleted Successfuly");
					//errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_CATEGORY_DELETED_SUCCESSFULLY).getCode());
				} else {
					//errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addWarn("ProductCategory Cannot be Deleted");
					return null;
				}
			}
			getAllCategory();

			disable = false;
			productCatDto = null;
			categoryName = null;
			categoryCode = null;
			regionalName = null;

			log.info("productCtegory deleteProductCategory ===============#End");
		} catch (Exception e) {
			getAllCategory();
			log.error("<<=====  exception in delete ....==>>>>>");
		}
		addButtonFlag = true;
		return PROD_CAT_LIST_PAGE;
	}

	public String showProductCategoryViewPage() {
		log.info("ProductCategoryBean>>>>>>>>> showViewPage() ");
		if (action.equals("VIEW") && productCatDto == null) {
			action = null;

			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			return null;
		}
		populateCreateForm();
		return PROD_CAT_VIEW_PAGE;
	}

	public String back() {
		getAllCategory();
		return PROD_CAT_LIST_PAGE;
	}

	public String cancel() {
		getAllCategory();
		return PROD_CAT_LIST_PAGE;
	}

	public String clear() {
		log.info("productCtegory clear===============#Start");
		addButtonFlag = true;
		disable = false;
		categoryName = null;
		categoryCode = null;
		regionalName = null;
		productCatDto = null;
		selectedProductCategoryGroup = new ProductCategoryGroup();
		// action = "ADD";
		getAllCategory();
		log.info("productCtegory clear===============#End");
		return PROD_CAT_LIST_PAGE;
	}

	private void loadStatusValues() {
		Object[] statusArray = ProductCategoryStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);

		}

		log.info("Load Status Values" + statusValues.get(1));
	}

	public void lazyLoadProductcategoryList() {
		log.info("====== lazyLoadProductcategoryList =======");
		productCategorylazyLoadList = new LazyDataModel<ProductCategoryDTO>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProductCategoryDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				log.info("====== load() Called =======");
				filterMap = filters;
				sortingOrder = sortOrder;
				sortingField = sortField;
				List<ProductCategoryDTO> data = new ArrayList<ProductCategoryDTO>();
				try {

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if(baseDTO != null)
					{
						ObjectMapper objectMapper = new ObjectMapper();
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						log.info("=== jsonResponse ====" + jsonResponse);
						data = objectMapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryDTO>>() {
						});
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}
					else
					{
						this.setRowCount(0);
						resultSize = 0;
					}
					
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(ProductCategoryDTO response) {
				log.info("getRowKey is called.");
				return response != null ? response.getId() : null;
			}

			@Override
			public ProductCategoryDTO getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<ProductCategoryDTO> responseList = (List<ProductCategoryDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (ProductCategoryDTO response : responseList) {
					if (response.getId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("ProductCategoryBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		ProductCategoryDTO request = getRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			baseDTO = httpService.post(Url + "/category/product/search", request);
			return baseDTO;

		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("ProductCategoryBean.getSearchData Method Completed");
		return baseDTO;
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	private ProductCategoryDTO getRequestObject(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		ProductCategoryDTO request = new ProductCategoryDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("productCatCode")) {
				request.setProductCatCode(value);
			}

			if (entry.getKey().equals("productCatName")) {
				log.info("categoryName : " + value);
				request.setProductCatName(value);
			}
			if (entry.getKey().equals("regionalName")) {
				log.info("regionalName : " + value);
				request.setRegionalName(value);
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("activeStatusMsg")) {
				log.info("active Status : " + value);
				request.setStatusValue(value);
			}

		}

		return request;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts DepartmentBean.onRowSelect ========>" + event);
		productCatDto = ((ProductCategoryDTO) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		/*
		 * if(selectedDepartment.getStatus() == true ){ deleteButtonFlag=true; }else
		 * if(selectedDepartment.getStatus() == false){ deleteButtonFlag=false; }
		 */
		log.info("<===Ends DepartmentBean.onRowSelect ========>");
	}
	
	public void showDeletePage() {
		action = "DELETE";
		pageHead = "Delete";
		if (action.equals("DELETE") && productCatDto == null) {
			log.info("<<=====   please slect a group =====>>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		} else {

			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmDepartmentDelete').show();");
		}
	}
	
	public void getCategoryGroupMasterList() {
		log.info("===getCategoryGroupMasterList===   Start");

		try {
			productCategoryGroupList.clear();
			String url = Url + "/categoryGroup/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryGroupList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategoryGroup>>() {
			});
			log.info("<<==== getCategoryGroupMasterList List Sise::: " + productCategoryGroupList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getCategoryGroupMasterList" + e);
		}
	}
}
