package in.gov.cooptex.masters.ui.bean;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.KNTPlanDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.KNTPlanMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("kNTPlanMasterBean")
public class KNTPlanMasterBean {
	private final String CREATE_PAGE = "/pages/masters/createKNTPlan.xhtml?faces-redirect=true";

	private final String LIST_PAGE_URL = "/pages/masters/listKNTPlan.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/viewKNTPlan.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	KNTPlanMaster kntPlanMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	KNTPlanDTO kntPlanDTO;

	@Getter
	@Setter
	LazyDataModel<KNTPlanDTO> kntPlanLazyList;

	@Getter
	@Setter
	private List<Long> productCatList = new ArrayList<>();

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;
	
	@Getter
	@Setter
	List<ProductCategoryDTO> productCatDTOList;
	
	@Getter
	@Setter
	List<String> productCategoryNameList;

	public String gotoPage(String URL) {

		log.info("Goto Page [" + URL + "]");

		if (URL.equalsIgnoreCase("ADD")) {
			action = "ADD";
			return create();
		} else if (URL.equalsIgnoreCase("EDIT")) {
			action = "EDIT";
			return editKNTPlan();
		} else if (URL.equalsIgnoreCase("VIEW")) {
			action = "VIEW";
			return gotoViewPage();
		} else {
			return null;
		}

	}

	public String showList() {
		kntPlanMaster = new KNTPlanMaster();
		return LIST_PAGE_URL;
	}

	public String create() {
		log.info("create method is executing..");

		String URL = appPreference.getPortalServerURL();
		log.info("create method is executing.." + URL + action);
		if (kntPlanMaster.getMaxAmount() < kntPlanMaster.getMinAmount()) {
			log.info("Minimum value cannot more than Maximum Value");
			Util.addWarn("Minimum Installment Amount must be less than Maximum Installment Amount");
			return null;
		}
		if (action.equalsIgnoreCase("ADD")) {
			log.info("inside add method");
			URL = URL + "/kntplanmaster/create";
		}

		if (action.equalsIgnoreCase("EDIT")) {
			log.info("inside edit method");
			URL = URL + "/kntplanmaster/update";
		}

		try {
			log.info("listsize" + kntPlanMaster.getProductCategoryList().size());
			BaseDTO baseDTO = httpService.post(URL, kntPlanMaster);
			log.info("--saved" + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Saved successfully");
					if (action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.KNTPLAN_CREATED_SUCCESSFULLY.getCode());
					if (action.equalsIgnoreCase("EDIT"))
						errorMap.notify(ErrorDescription.KNTPLAN_UPDATED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return showList();
	}

	public String gotoListPage() {
		kntPlanDTO = new KNTPlanDTO();
		disableAddButton = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	public String addKNTPlan() {
		log.info("<---- Start kntPlanMasterBean.addKNTPlan ---->");
		try {
			action = "ADD";
			kntPlanMaster = new KNTPlanMaster();
			kntPlanMaster.setActiveStatus(true);
			loadProductCategory();
		} catch (Exception e) {
			log.error("Error occured while redirectin", e);
		}
		return CREATE_PAGE;
	}

	public void getDenominations() {
		if (kntPlanMaster.getMinAmount() != null && kntPlanMaster.getMaxAmount() != null) {
			if (kntPlanMaster.getMinAmount() >= 300 && kntPlanMaster.getMaxAmount()<= 1000) {
				kntPlanMaster.setDenominations(100);
			} else if (kntPlanMaster.getMaxAmount() > 1000) {
				kntPlanMaster.setDenominations(500);
			}else {
				kntPlanMaster.setDenominations(0);
			}
		}
	}

	public void loadLazyList() {

		kntPlanLazyList = new LazyDataModel<KNTPlanDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<KNTPlanDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<KNTPlanDTO> data = new ArrayList<KNTPlanDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<KNTPlanDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) throws ParseException {

				BaseDTO baseDTO = new BaseDTO();

				log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
						+ "sortField:[" + sortField + "]");

				for (Map.Entry<String, Object> entry : filters.entrySet()) {
					log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
				}

				KNTPlanDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

				String URL = appPreference.getPortalServerURL() + "/kntplanmaster/list";

				baseDTO = httpService.post(URL, request);

				return baseDTO;
			}

			private KNTPlanDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
				KNTPlanDTO request = new KNTPlanDTO();

				PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
				request.setPaginationDTO(paginationDTO);

				for (Map.Entry<String, Object> entry : filters.entrySet()) {
					String value = entry.getValue().toString();

					if (entry.getKey().equals("planName")) {
						log.info("planName : " + value);
						request.setPlanName(value);
					}

					if (entry.getKey().equals("maxAmount")) {
						log.info("maxAmount : " + value);
						request.setMaxAmount(Long.valueOf(value));
					}
					if (entry.getKey().equals("minAmount")) {
						log.info("minAmount : " + value);
						request.setMinAmount(Long.valueOf(value));
					}
					if (entry.getKey().equals("instalmentMonth")) {
						log.info("instalmentMonth : " + value);
						request.setInstalmentMonth(Long.valueOf(value));
					}
					if (entry.getKey().equals("createdDate")) {
						request.setCreatedDate(AppUtil.serverDateFormat(value));
					}
					if (entry.getKey().equals("activeStatus")) {
						request.setActiveStatus(Boolean.valueOf(value));
					}

				}

				return request;
			}

			@Override
			public Object getRowKey(KNTPlanDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public KNTPlanDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<KNTPlanDTO> responseList = (List<KNTPlanDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (KNTPlanDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		kntPlanDTO = ((KNTPlanDTO) event.getObject());
		disableAddButton = true;
	}

	public void processDelete() {
		if (kntPlanDTO == null || kntPlanDTO.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_KNTPLAN_MASTER.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		log.info(kntPlanDTO.getId());
		String URL = appPreference.getPortalServerURL() + "/kntplanmaster/delete/" + kntPlanDTO.getId();

		try {

			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				disableAddButton = false;
				errorMap.notify(ErrorDescription.KNTPLAN_DELETE_SUCCESSFULLY.getCode());
				return LIST_PAGE_URL;
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public void loadProductCategory() {
		log.info(" loadProductCategory() calling ");
		try {
			String url = appPreference.getPortalServerURL() + "/category/getAll";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
			log.info("baseDTO.getResponseContents()" + baseDTO.getResponseContents());
			productCatList = mapper.readValue(jsonValue, new TypeReference<List<ProductCategoryDTO>>() {
			});
			
			productCatDTOList=mapper.readValue(jsonValue, new TypeReference<List<ProductCategoryDTO>>() {
			});
			log.info("productCategoryList" + productCatList.size());
		} catch (Exception e) {
			log.error(" Exception loadProductCategory()" + e);
		}
	}
	
	public void getSelectedProdCategory() {
		if(productCatDTOList!=null && kntPlanMaster!=null) {
			List<Long> prdCatList=kntPlanMaster.getProductCategoryList();
			log.info("get form product list size....."+prdCatList.size());
			List<String> prodCatName=new ArrayList<>();
			for(ProductCategoryDTO obj:productCatDTOList) {
				if(prdCatList.contains(obj.getId())) {
					String productCat=obj.getProductCatCode()+"/"+obj.getProductCatName();
					prodCatName.add(productCat);
				}
				log.info("product category name list....."+prodCatName.size());
			}
			productCategoryNameList=prodCatName;
		}
	}

	public KNTPlanMaster getKNTPlanById(Long id) {
		String URL = appPreference.getPortalServerURL() + "/kntplanmaster/get/" + id;
		KNTPlanMaster kntPlanMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				kntPlanMaster = mapper.readValue(jsonValue, KNTPlanMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return kntPlanMaster;
	}

	private String gotoViewPage() {

		if (kntPlanDTO == null || kntPlanDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_KNTPLAN_MASTER.getCode());
			return null;
		}
		loadProductCategory();
		kntPlanMaster = getKNTPlanById(kntPlanDTO.getId());
		getSelectedProdCategory();
		return VIEW_PAGE_URL;
	}

	public String editKNTPlan() {
		try {
			if (kntPlanDTO == null || kntPlanDTO.getId() == null) {
				log.info("----select KNTPlanMaster-------");
				errorMap.notify(ErrorDescription.SELECT_KNTPLAN_MASTER.getCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<===== kntPlanDTO :: " + kntPlanDTO.getId());
		loadProductCategory();
		kntPlanMaster = getKNTPlanById(kntPlanDTO.getId());

		return CREATE_PAGE;

	}

}
