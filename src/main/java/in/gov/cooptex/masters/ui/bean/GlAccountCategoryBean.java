package in.gov.cooptex.masters.ui.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("glAccountCategoryBean")
public class GlAccountCategoryBean {

	private final String CREATE_ACCOUNT_CATEGORY_PAGE = "/pages/masters/createAccountCategory.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listAccountCategory.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewAccountCategory.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ACCOUNTCATEGORY_URL = AppUtil.getPortalServerURL() + "/accounts";

	ObjectMapper mapper=new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	GlAccountCategory glAccountCategory, selectedGlAccountCategory;

	@Getter
	@Setter
	LazyDataModel<GlAccountCategory> lazyGlAccountCategoryList;

	List<GlAccountCategory> glAccountCategoryList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	
	@Autowired
	LoginBean loginBean;

	public String showGlAccountCategoryListPage() {
		log.info("<==== Starts GlAccountCategoryBean.showAccountCategoryListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		glAccountCategory = new GlAccountCategory();
		selectedGlAccountCategory = new GlAccountCategory();
		loadLazyGlAccountCategoryList();
		log.info("<==== Ends GlAccountCategoryBean.showAccountCategoryListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	public void loadLazyGlAccountCategoryList() {
		log.info("<===== Starts GlAccountCategoryBean.loadLazyGlAccountCategoryList ======>");
		lazyGlAccountCategoryList = new LazyDataModel<GlAccountCategory>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<GlAccountCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = ACCOUNTCATEGORY_URL + "/getaccounts";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						glAccountCategoryList = mapper.readValue(jsonResponse,
								new TypeReference<List<GlAccountCategory>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGlAccountCategoryList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return glAccountCategoryList;
			}

			@Override
			public Object getRowKey(GlAccountCategory res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public GlAccountCategory getRowData(String rowKey) {
				try {
					for (GlAccountCategory glAccountCategory : glAccountCategoryList) {
						if (glAccountCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedGlAccountCategory = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends GlAccountCategoryBean.loadLazyGlAccountCategoryList ======>");
	}

	public String showAccountCategoryListPageAction() {
		log.info("<===== Starts GlAccountCategoryBean.showAccountCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedGlAccountCategory == null || selectedGlAccountCategory.getId() == null)) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_SELECT.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create GlAccountCategory called..");
				glAccountCategory = new GlAccountCategory();
				return CREATE_ACCOUNT_CATEGORY_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit GlAccountCategory called..");
				String url = ACCOUNTCATEGORY_URL + "/getaccounttcategory/" + selectedGlAccountCategory.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					glAccountCategory = mapper.readValue(jsonResponse, new TypeReference<GlAccountCategory>() {
					});
					return CREATE_ACCOUNT_CATEGORY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete GlAccountCategory called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view GlAccountCategory called..");
				String viewurl = ACCOUNTCATEGORY_URL + "/getaccounttcategory/" + selectedGlAccountCategory.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					glAccountCategory = mapper.readValue(jsonResponse, new TypeReference<GlAccountCategory>() {
					});
					return INPUT_FORM_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends GlAccountCategoryBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {
		log.info("<==== Starts GlAccountCategoryBean.saveOrUpdate =======>");
		try {
			if (glAccountCategory.getCode() == null || glAccountCategory.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_CODE_EMPTY.getCode());
				return null;
			}
			if (glAccountCategory.getName() == null || glAccountCategory.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_NAME_EMPTY.getCode());
				return null;
			}
			if (glAccountCategory.getName().trim().length() < 3 || glAccountCategory.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (action.equalsIgnoreCase("CREATE")) {
				glAccountCategory.setCreatedBy(loginBean.getUserDetailSession());
				glAccountCategory.setCreatedDate(new Date());
				glAccountCategory.setStatus(true);
			} else if (action.equalsIgnoreCase("EDIT")) {
				glAccountCategory.setModifiedBy(loginBean.getUserDetailSession());
				glAccountCategory.setModifiedDate(new Date());
				if (glAccountCategory.getStatus() == null) {
					errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_STATUS_EMPTY.getCode());
					return null;
				}
			}
			String url = ACCOUNTCATEGORY_URL + "/saveorupdate";
			BaseDTO response = httpService.put(url, glAccountCategory);
			if (response != null && response.getStatusCode() == 0) {
				log.info("account category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends GlAccountCategoryBean.saveOrUpdate =======>");
		return showGlAccountCategoryListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts GlAccountCategoryBean.onRowSelect ========>" + event);
		selectedGlAccountCategory = ((GlAccountCategory) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends GlAccountCategoryBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts GlAccountCategoryBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(ACCOUNTCATEGORY_URL + "/delete/" + selectedGlAccountCategory.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete account category ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends GlAccountCategoryBean.deleteConfirm ===========>");
		return showGlAccountCategoryListPage();
	}
}