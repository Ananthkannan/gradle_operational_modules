package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EngagemenDetailsDTO;
import in.gov.cooptex.core.dto.IncrementForwardDTO;
import in.gov.cooptex.core.dto.IncrementListDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TemporaryEngagementForwardDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.EmpIncrementLog;
import in.gov.cooptex.core.model.EmpIncrementNote;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.IncrementCycle;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TemporaryEngagement;
import in.gov.cooptex.core.model.TemporaryEngagementDetails;
import in.gov.cooptex.core.model.TemporaryEngagementDetailsLog;
import in.gov.cooptex.core.model.TemporaryEngagementDetailsNote;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("temporaryEngagementRegionBean")
@Scope("session")
public class TemporaryEngagementRegionBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	ObjectMapper mapper;
	
	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	final String REJECT = "REJECT";
	
	final String APPROVED = "APPROVED";
	
	@Getter
	@Setter
	LazyDataModel<TemporaryEngagement> temporaryEngagementLazyList;
	
	@Getter
	@Setter
	LazyDataModel<EngagemenDetailsDTO> engagemenDetailsDTOLazyList;
	
	@Getter
	@Setter
	List<EngagemenDetailsDTO> engagemenDetailsDTOList = new ArrayList<>();
	
	@Getter
	@Setter
	EngagemenDetailsDTO selectedEngagemenDetailsDTO = new EngagemenDetailsDTO();
	
	
	@Getter
	@Setter
	TemporaryEngagement selectedTemporaryEngagement;
	
	@Autowired
	AppPreference appPreference;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Setter
	@Getter
	List<StateMaster> stateList;
	
	@Setter
	@Getter
	List<Department> departmentList;
	
	@Setter
	@Getter
	List<EntityMaster> regionList;
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	TemporaryEngagement temporaryEngagement;
	
	@Getter
	@Setter
	TemporaryEngagementDetails selectedTemporaryEngagementDetail = new TemporaryEngagementDetails();
	
	@Getter
	@Setter
	String action;
	
	@Getter
	@Setter
	String engRemarks;
	
	@Getter
	@Setter
	Long additionalRequirment;
	
	@Getter
	@Setter
	String additionalRemarks;
	
	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();
	
	@Getter
	@Setter
	Boolean forwardFor;
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	
	@Getter
	@Setter
	EntityTypeMaster entitytypeMaster;
	
	@Getter
	@Setter
	EntityMaster loginEntityMaster;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	@Getter
	@Setter
	Boolean requestFlag = false;
	
	@Getter
	@Setter
	Boolean viewFlag = false;
	
	@Getter
	@Setter
	Long noteId;
	
	@Getter
	@Setter
	TemporaryEngagementDetails temporaryEngagementDetails = new TemporaryEngagementDetails();
	
	@Getter
	@Setter
	List<TemporaryEngagementDetailsLog> temEngagementDetailsLogList = new ArrayList<>();
	
	public void loadMasterValues() {
		stateList = commonDataService.getStateList();
		departmentList = commonDataService.getDepartment();
		regionList = commonDataService.loadRegionalOffice();
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		forwardToUsersList = commonDataService.loadForwardToUsersByHeadOffice();
	}
	
	public String loadTemporaryEngagementRegionList() {
		mapper = new ObjectMapper();
		selectedTemporaryEngagement = new TemporaryEngagement();
		action = "LIST";
		loadMasterValues();
		loginEntityMaster =  getLoggedInUserDetails();
		if(loginEntityMaster != null) {
			entitytypeMaster = loginEntityMaster.getEntityTypeMaster();
			log.info("entitytypeMaster id==> "+entitytypeMaster.getId());
			log.info("entitytypeMaster name==> "+entitytypeMaster.getEntityCode());
			log.info("entitytypeMaster id==> "+loginEntityMaster.getId());
			log.info("entityMaster name==> "+loginEntityMaster.getName());
			if(entitytypeMaster.getEntityCode().equals("HEAD_OFFICE")) {
				//--Head Office
				requestFlag = false;
				//loadTemporaryEngagemenLazy();
				return loadEngagementDetail();
			}else {
				//--Region login
				requestFlag = true;
				loadTemporaryEngagemenRegionLazy();
				return "/pages/personnelHR/temporaryEngagement/listTemporaryEngagementRegion.xhtml?faces-redirect=true";
			}
		}else {
			log.error("Entity master not found");
		}
		return null;
		
	}
	
	public String loadEngagementDetail() {
		
		loadTemporaryEngagemenDetailsLazy();
		return "/pages/personnelHR/temporaryEngagement/listTemporaryEngagementDetails.xhtml?faces-redirect=true";
	}
	
	/**
	 * purpose : to load increment list page
	 */
	public void loadTemporaryEngagemenDetailsLazy() {
		log.info("<==== Starts IncrementBean.loadLazyIncrementList =====>");
		engagemenDetailsDTOLazyList = new LazyDataModel<EngagemenDetailsDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EngagemenDetailsDTO> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					//log.info("engId==> "+selectedTemporaryEngagement.getId());
					//paginationRequest.setEngId(50l);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL + "/temporaryengagement/getallengagementdetailslazy";
					log.info("loadLazyIncrementList :: url==> "+url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						engagemenDetailsDTOList = mapper.readValue(jsonResponse, new TypeReference<List<EngagemenDetailsDTO>>() {});
						log.info("loadLazyIncrementList :: engagemenDetailsDTOList size==> "+engagemenDetailsDTOList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyIncrementList :: totalRecords==> "+totalRecords);
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return engagemenDetailsDTOList;
			}

			@Override
			public Object getRowKey(EngagemenDetailsDTO res) {
				return res != null ? res.getEngDetailsId() : null;
			}

			@Override
			public EngagemenDetailsDTO getRowData(String rowKey) {
				for (EngagemenDetailsDTO engagemenDetailsDTO : engagemenDetailsDTOList) {
					if (engagemenDetailsDTO.getEngDetailsId().equals(Long.valueOf(rowKey))) {
						selectedEngagemenDetailsDTO = engagemenDetailsDTO;
						return engagemenDetailsDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}
	
	public void loadTemporaryEngagemenLazy() {
		temporaryEngagementLazyList = new LazyDataModel<TemporaryEngagement>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TemporaryEngagement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Response retrurn null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<TemporaryEngagement> temporaryEngagementList = mapper.readValue(jsonResponse,
							new TypeReference<List<TemporaryEngagement>>() {
							});
					if (temporaryEngagementList == null) {
						log.info(" Temporary Engagement list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Temporary Engagement list search Successfully Completed");
					} else {
						log.error(" Temporary Engagement list search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return temporaryEngagementList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Temporary Engagement :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(TemporaryEngagement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TemporaryEngagement getRowData(String rowKey) {
				List<TemporaryEngagement> responseList = (List<TemporaryEngagement>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (TemporaryEngagement res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedTemporaryEngagement = res;
						return res;
					}
				}
				return null;
			}
		};
	}
	
	public void loadTemporaryEngagemenRegionLazy() {
		temporaryEngagementLazyList = new LazyDataModel<TemporaryEngagement>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TemporaryEngagement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Response retrurn null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<TemporaryEngagement> temporaryEngagementList = mapper.readValue(jsonResponse,
							new TypeReference<List<TemporaryEngagement>>() {
							});
					if (temporaryEngagementList == null) {
						log.info(" Temporary Engagement list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Temporary Engagement list search Successfully Completed");
					} else {
						log.error(" Temporary Engagement list search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return temporaryEngagementList;

				} catch (Exception e) {
					e.printStackTrace();
					log.error("ExceptionException Ocured while Loading Temporary Engagement :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(TemporaryEngagement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TemporaryEngagement getRowData(String rowKey) {
				List<TemporaryEngagement> responseList = (List<TemporaryEngagement>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (TemporaryEngagement res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedTemporaryEngagement = res;
						return res;
					}
				}
				return null;
			}
		};
	}
	
	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			TemporaryEngagement request = new TemporaryEngagement();
			String url = null;
			if(!entitytypeMaster.getEntityCode().equals("HEAD_OFFICE")) {
				url = "/temporaryengagement/getallregionlazy";
				if(loginEntityMaster != null) {
					log.info("worklocation id==> "+loginEntityMaster.getId());
					request.setWorkLocationId(loginEntityMaster.getId());
				}else {
					log.error("worklocation not found");
				}
			}else {
				url = "/temporaryengagement/getalllazy";
				
			}
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("referenceNumber")) {
					log.info("Reference Number: " + value);
					searchFilters.put("referenceNumber", value);
				}
				if (filter.getKey().equals("state.name")) {
					log.info("State Name : " + value);
					searchFilters.put("stateName", value);
				}
				if (filter.getKey().equals("entity")) {
					log.info("Entity Name: " + value);
					searchFilters.put("entityName", value);
				}
				if (filter.getKey().equals("department.name")) {
					log.info("department: " + value);
					searchFilters.put("department", value);
				}
				if (filter.getKey().equals("selectionYear")) {
					log.info("Selection Year: " + value);
					searchFilters.put("selectionYear", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("Created Date: " + value);
					searchFilters.put("createdDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}
			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + url, request);

		} catch (Exception e) {
			log.error("Exception Occured in temporary engagement load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}
	
	public void onRowApproveSelect(SelectEvent event) {
		log.info("onRowApproveSelect called");
		selectedEngagemenDetailsDTO = ((EngagemenDetailsDTO) event.getObject());

	}
	
	/*public void viewEngagementDetailsList() {
		log.info("<=viewEngagementDetailsList :: called=>");
		try {
			log.info("selectedEngagemenDetailsDTO id==> "+selectedEngagemenDetailsDTO);
			log.info("selectedEngagemenDetailsDTO==> "+selectedEngagemenDetailsDTO.getEngId());
			log.info("selectedEngagemenDetailsDTO==> "+selectedEngagemenDetailsDTO.getEngDetailsId());
			s
		}catch(Exception e) {
			log.error("Exception Occured in viewEngagementDetailsList :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}*/
	
	public String viewEngagementDetailsList() {
		try {
			if (selectedEngagemenDetailsDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_TEMPORARY_ENGAGEMENT.getErrorCode());
				return null;
			}

			
				log.info("Engagement Id---->" + selectedEngagemenDetailsDTO.getEngId());
				log.info("Engagement details Id---->" + selectedEngagemenDetailsDTO.getEngDetailsId());
				temporaryEngagement = new TemporaryEngagement();
				BaseDTO response = httpService
						.get(SERVER_URL + "/temporaryengagement/getdetailbyid/" + selectedEngagemenDetailsDTO.getEngDetailsId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					temporaryEngagementDetails = mapper.readValue(jsonResponse, new TypeReference<TemporaryEngagementDetails>() {
					});
					if(temporaryEngagementDetails != null) {
						log.info("Temporary engagement id==> "+temporaryEngagementDetails.getId());
						//--load temporary engagement
						loadTemporaryEnagement(selectedEngagemenDetailsDTO.getEngId());
						//-- to get getTemporaryEngagementDetailsNoteList note details
						if(temporaryEngagementDetails.getTemporaryEngagementDetailsNoteList() != null && !temporaryEngagementDetails.getTemporaryEngagementDetailsNoteList().isEmpty()) {
							for(TemporaryEngagementDetailsNote temporaryEngagementDetailsNote : temporaryEngagementDetails.getTemporaryEngagementDetailsNoteList()) {
								noteId = temporaryEngagementDetailsNote.getId();
								forwardToUser = temporaryEngagementDetailsNote.getForwardTo();
								log.info("forwardToUser id=> "+forwardToUser.getId());
								forwardFor = temporaryEngagementDetailsNote.getFinalApproval();
								log.info("forwardFor=> "+forwardFor);
								additionalRemarks = temporaryEngagementDetailsNote.getNote();
								log.info("note=> "+additionalRemarks);
							}
							//if(forwardToUser.getId() == )
							Long loginUserId = loginBean.getUserMaster().getId();
							Long noteUserId = forwardToUser.getId();
							
							log.info("loginUserId=> "+loginUserId);
							log.info("noteUserId=> "+noteUserId);
							if(loginUserId.equals(noteUserId)) {
								viewFlag = true;
							}else {
								viewFlag = false;
							}
						}else {
							log.error("Increment note not found");
						}
						//--to get temporaryEngagementDetails log details
						if(temporaryEngagementDetails.getTemporaryEngagementDetailsLogList() != null && !temporaryEngagementDetails.getTemporaryEngagementDetailsLogList().isEmpty()) {
							temEngagementDetailsLogList = temporaryEngagementDetails.getTemporaryEngagementDetailsLogList();
						}else {
							log.error("Increment log not found");
						}
					}else {
						log.error("Temporary Engagement not found");
					}
					return "/pages/personnelHR/temporaryEngagement/viewTemporaryHOEngagement.xhtml?faces-redirect=true";
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			//}
		} catch (Exception e) {
			log.error("Exception Occured in temporary engagement ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public void loadTemporaryEnagement(Long enagementId) {
		log.info("<=IncrementBean :: loadIncrementCycle=>");
		try {
			/*String url = SERVER_URL + "/incrementcycle/getAll";
			log.info("loadIncrementCycle :: url ==> "+url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					incrementCycleList = mapper.readValue(jsonValue, new TypeReference<List<IncrementCycle>>() {});
					log.info("IncrementBean :: incrementCycleList==> " + incrementCycleList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}*/
			
			BaseDTO response = httpService
					.get(SERVER_URL + "/temporaryengagement/getenagement/" + enagementId);
			if (response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				temporaryEngagement = mapper.readValue(jsonResponse, new TypeReference<TemporaryEngagement>() {
				});
				if(temporaryEngagement != null) {
					log.info("Temporary engagement id==> "+temporaryEngagement.getId());
				}else {
					log.error("Temporary Engagement not found");
				}
			}
		}catch (Exception e) {
			log.error("Exception in loadIncrementCycle  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}
	
	
	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedTemporaryEngagement = ((TemporaryEngagement) event.getObject());

		
	}
	
	public EntityMaster getLoggedInUserDetails() {
		BaseDTO baseDTO = new BaseDTO();
		EntityMaster loggedInEntity = null;
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/getentitybyuser/" + loginBean.getUserMaster().getId();
			baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				loggedInEntity = mapper.readValue(jsonValue, EntityMaster.class);
			}
		} catch (Exception e) {

			log.error("Exception occured  setLoggedInUser.... ", e);

		}

		return loggedInEntity;
	}
	
	public String cancel() {
		selectedTemporaryEngagement = new TemporaryEngagement();
		return loadTemporaryEngagementRegionList();
	}
	
	public String temporaryEngagementListAction() {
		try {
			if (selectedTemporaryEngagement == null) {
				errorMap.notify(ErrorDescription.SELECT_TEMPORARY_ENGAGEMENT.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("REQUEST") && selectedTemporaryEngagement.getStatus().endsWith("InProgress")) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.APPROVED_ENGAGEMENT_STATUS).getCode());
				return null;
			}

			/*if (action.equalsIgnoreCase("DELETE")) {
				if (selectedTemporaryEngagement.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_TEMPORARY_ENGAGEMENT_CANNOT_DELETE.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAppointmentDelete').show();");
			} else {*/
				log.info("<----Loading  page.....---->" + selectedTemporaryEngagement.getId());
				temporaryEngagement = new TemporaryEngagement();
				BaseDTO response = httpService
						.get(SERVER_URL + "/temporaryengagement/get/" + selectedTemporaryEngagement.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					temporaryEngagement = mapper.readValue(jsonResponse, new TypeReference<TemporaryEngagement>() {
					});
					if(temporaryEngagement != null) {
						log.info("Temporary engagement id==> "+temporaryEngagement.getId());
					}else {
						log.error("Temporary Engagement not found");
					}
					if (action.equalsIgnoreCase("VIEW")) {
						return "/pages/personnelHR/temporaryEngagement/viewTemporaryRegionEngagement.xhtml?faces-redirect=true";
					} else if (action.equalsIgnoreCase("REQUEST")) {
						return "/pages/personnelHR/temporaryEngagement/requestTemporaryEngagement.xhtml?faces-redirect=true";
					}
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			//}
		} catch (Exception e) {
			log.error("Exception Occured in temporary engagement ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String viewShowroomDetails(TemporaryEngagementDetails detail) {
		log.info("inside drill down chart............" + detail);
		try {
			if (detail == null) {
				Util.addWarn("Please Select One Temporary engagement");
				return null;
			} else {
				selectedTemporaryEngagementDetail = detail;
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('showroomdetail').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	public void submitNote() {
		log.info("=========================================");
		log.info("additionalRemarks >>>>>>>>> " + additionalRemarks);
		log.info("=========================================");
	}
	
	public String submitAdditionalRequirement() {
		log.info("<=call submitAdditionalRequirement=>");
		try {
			log.info("addRequirment==> "+additionalRequirment);
			log.info("additionalRemarks==> "+additionalRemarks);
			log.info("temporaryEngagement==> "+temporaryEngagement);
			if (selectedTemporaryEngagement == null) {
				errorMap.notify(ErrorDescription.SELECT_TEMPORARY_ENGAGEMENT.getErrorCode());
				return null;
			}
			if(additionalRequirment == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITIONAL_REQUIREMENT_NOT_FOUND).getCode());
				return null;
			}
			if(additionalRemarks == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.NOTE_NOT_FOUND).getCode());
				return null;
			}
			if(forwardToUser == null || forwardToUser.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
				return null;
			}
			if(forwardFor == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_FOR_NOT_FOUND).getCode());
				return null;
			}
			TemporaryEngagementDetails temporaryEngagementDetail = new TemporaryEngagementDetails();
			temporaryEngagementDetail.setAdditionalRequirement(additionalRequirment);
			temporaryEngagementDetail.setTemporaryEngagementId(temporaryEngagement.getId());
			temporaryEngagementDetail.setRegion(temporaryEngagement.getTemporaryEngagementDetails().get(0).getRegion());
			temporaryEngagementDetail.setParent(temporaryEngagement.getTemporaryEngagementDetails().get(0).getParent());
			
			List<TemporaryEngagementDetailsNote> incrementNoteList = new ArrayList<>();
			TemporaryEngagementDetailsNote empIncrementNote = new TemporaryEngagementDetailsNote();
			log.info("submitAdditionalRequirement :: additionalRemarks==> "+additionalRemarks);
			empIncrementNote.setNote(additionalRemarks);
			empIncrementNote.setTemporaryEngagementDetails(temporaryEngagementDetail);
			log.info("saveEmployeeIncrement: forwardToUser id==> "+forwardToUser.getId());
			empIncrementNote.setForwardTo(forwardToUser);
			log.info("saveEmployeeIncrement: forwardFor==> "+forwardFor);
			empIncrementNote.setFinalApproval(forwardFor);
			incrementNoteList.add(empIncrementNote);
			temporaryEngagementDetail.setTemporaryEngagementDetailsNoteList(incrementNoteList);
			
			List<TemporaryEngagementDetailsLog> incrementLogList = new ArrayList<>();
			TemporaryEngagementDetailsLog empIncrementLog = new TemporaryEngagementDetailsLog();
			empIncrementLog.setTemporaryEngagementDetails(temporaryEngagementDetail);
			empIncrementLog.setStage(ApprovalStage.SUBMITTED);
			incrementLogList.add(empIncrementLog);
			temporaryEngagementDetail.setTemporaryEngagementDetailsLogList(incrementLogList);
			
			String url = SERVER_URL + "/temporaryengagement/createtemporaryengagementregion";
			log.info("saveEmployeeIncrement :: url==> "+url);
			BaseDTO baseDTO = httpService.put(url, temporaryEngagementDetail);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			mapper.readValue(jsonResponse, TemporaryEngagementDetails.class);
			if (baseDTO.getStatusCode() == 0) {
				clear();
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITIONAL_REQUIREMENT_INSERTED_SUCCESSFULLY).getCode());
				return loadTemporaryEngagementRegionList();
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}catch (Exception e) {
			log.error("Exception Occured in temporary engagement ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public void clear() {
		selectedTemporaryEngagement = new TemporaryEngagement();
		temporaryEngagement = new TemporaryEngagement();
		employeeMaster = new EmployeeMaster();
		additionalRemarks = null;
	}
	
	/**
	 * purpose : to save approved or rejected status in log table
	 * @param status
	 * @return
	 */
	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> "+status);
			log.info("engRemarks==> "+engRemarks);
			log.info("logList id==> "+temEngagementDetailsLogList.get(0).getId());
			log.info("noteId==> "+noteId);
			log.info("forwardFor==> "+forwardFor);
			TemporaryEngagementForwardDTO temporaryEngagementForwardDTO = new TemporaryEngagementForwardDTO();
			temporaryEngagementForwardDTO.setLogList(temEngagementDetailsLogList);
			temporaryEngagementForwardDTO.setForwardRemarks(engRemarks);
			temporaryEngagementForwardDTO.setTemEngagementDetails(temporaryEngagementDetails);
			temporaryEngagementForwardDTO.setNoteId(noteId);
			log.info("forward user id==> "+forwardToUser.getId());
			temporaryEngagementForwardDTO.setForwardUserId(forwardToUser.getId());
			temporaryEngagementForwardDTO.setForwardFor(forwardFor);
			if(status.equals(REJECT)) {
				temporaryEngagementForwardDTO.setStatus(ApprovalStage.REJECTED);
			}else if(status.equals(APPROVED)){
				temporaryEngagementForwardDTO.setStatus(ApprovalStage.APPROVED);
			}else {
				temporaryEngagementForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + "/temporaryengagement/updateEngApprovelog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, temporaryEngagementForwardDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITIONAL_REQUIREMENT_LOG_INSERTED_SUCCESSFULLY).getCode());
				return clearEngagementDetails();
			} else {
				log.info("Error while saving submitApprovedRejectedStatus :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return null;
	}
	
	public String clearTemporaryEngRegion() {
		selectedTemporaryEngagement = new TemporaryEngagement();
		requestFlag = true;
		loadTemporaryEngagemenRegionLazy();
		return "/pages/personnelHR/temporaryEngagement/listTemporaryEngagementRegion.xhtml?faces-redirect=true";
	}
	
	public String clearEngagementDetails() {
		selectedEngagemenDetailsDTO = new EngagemenDetailsDTO();
		requestFlag = false;
		loadTemporaryEngagemenDetailsLazy();
		return "/pages/personnelHR/temporaryEngagement/listTemporaryEngagementDetails.xhtml?faces-redirect=true";
	}
	
	
}
