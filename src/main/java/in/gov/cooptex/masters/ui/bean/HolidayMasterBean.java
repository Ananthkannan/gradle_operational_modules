package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.HolidayMaster;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("holidayMasterBean")
public class HolidayMasterBean {

	private final String CREATE_HOLIDAY_MASTER_PAGE = "/pages/masters/createHoliday.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listHoliday.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewHoliday.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String HOLIDAYMASTER_URL = AppUtil.getPortalServerURL() + "/holidayMaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	HolidayMaster holidayMaster, selectedHolidayMaster;

	@Getter
	@Setter
	LazyDataModel<HolidayMaster> lazyHolidayMasterList;

	List<HolidayMaster> holidayMasterList = null;
	@Setter
	@Getter
	List<CountryMaster> countryMasterList= new ArrayList<CountryMaster>();
	@Setter
	@Getter
	CountryMaster selectedCountryMaster;
	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();
	@Getter
	@Setter
	StateMaster selectedStateMaster;
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	@Autowired
	LoginBean loginBean;

	
    @Getter
    @Setter
    List<EntityMaster> headAndRegionalOfficeList = new ArrayList<>();
    
    @Getter
    @Setter
    EntityMaster headandRegionaOfficeId = new EntityMaster();
    
    @Getter
    @Setter
    boolean visibility = true;
    
    @Getter
    @Setter
    List<EntityTypeMaster> entityTypeLists = new ArrayList<>();
    
    @Getter
    @Setter
    EntityTypeMaster selectedEntityTypeId;
    
    @Getter
    @Setter
    List<EntityMaster> entityList = new ArrayList<>();
    
    @Getter
	@Setter
	EntityMaster entityMaster;
    
	
	/*
	 * showHolidayListPage method used to load all details in starting master page
	 */
	public String showHolidayListPage() {
		log.info("<==== Starts HolidayBean.showHolidayListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		holidayMaster = new HolidayMaster();
		selectedHolidayMaster = new HolidayMaster();
		
		headandRegionaOfficeId = new EntityMaster();
		
		loadLazyHolidayList();
		log.info("<==== Ends HolidayBean.showHolidayListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	/* loadLazyHolidayList method used to load list using lazy loading */
	public void loadLazyHolidayList() {
		log.info("<===== Starts HolidayBean.loadLazyHolidayList ======>");
		lazyHolidayMasterList = new LazyDataModel<HolidayMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<HolidayMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = HOLIDAYMASTER_URL + "/getholidaylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						holidayMasterList = mapper.readValue(jsonResponse, new TypeReference<List<HolidayMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHolidayList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return holidayMasterList;
			}

			@Override
			public Object getRowKey(HolidayMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public HolidayMaster getRowData(String rowKey) {
				try {
					for (HolidayMaster holidayMaster : holidayMasterList) {
						if (holidayMaster.getId().equals(Long.valueOf(rowKey))) {
							selectedHolidayMaster = holidayMaster;
							return holidayMaster;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends HolidayBean.loadLazyHolidayList ======>");
	}

	/*
	 * showHolidayListPageAction method used to action based view pages and render
	 * viewing
	 */

	public String showHolidayListPageAction() {
		log.info("<===== Starts HolidayBean.showBloodGroupListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedHolidayMaster == null || selectedHolidayMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.HOLIDAY_MASTER_SELECT.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create HOLIDAYMASTER called..");
				
				
				headandRegionaOfficeId = new EntityMaster();
				
				setSelectedCountryMaster(null);
				setSelectedStateMaster(null);
				headandRegionaOfficeId=null;
				selectedEntityTypeId = null;
				holidayMaster = new HolidayMaster();
				countryMasterList=commonDataService.loadCountry();
				
				
				loadHeadandRegionalOffice();
				loadEntityTypeList();
				
				
				return CREATE_HOLIDAY_MASTER_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit HOLIDAYMASTER called..");
				
				headandRegionaOfficeId=null;
				selectedEntityTypeId = null;
				
				String url = HOLIDAYMASTER_URL + "/getholidaymaster/" + selectedHolidayMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					holidayMaster = mapper.readValue(jsonResponse, new TypeReference<HolidayMaster>() {
					});
			
			        
			        if (holidayMaster.getEntityMaster().getId() != null) {
						String URL = AppUtil.getPortalServerURL() + "/entitymaster/region/"
								+ holidayMaster.getEntityMaster().getId();

						log.info("entity-- URL-----> " + URL);

						BaseDTO baseDTO = httpService.get(URL);
						if (baseDTO.getStatusCode() == 0) {
							ObjectMapper mapper = new ObjectMapper();
							String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
							 entityMaster = mapper.readValue(jsonResponse, EntityMaster.class);
							log.info("entity-- region id----->" + entityMaster.getId());
						}

					}
			        
			       
			        
			        String entityTypeCode = holidayMaster.getEntityMaster().getEntityTypeMaster().getEntityCode();

					log.info("entityTypeCode -----> " + entityTypeCode);

					if ((EntityType.HEAD_OFFICE).equals(entityTypeCode)) {
						
						log.info("Inside head office entity");
						setHeadandRegionaOfficeId(holidayMaster.getEntityMaster());
						headAndRegional();
					} else {
						setHeadandRegionaOfficeId(entityMaster);
						headAndRegional();
						setSelectedEntityTypeId(holidayMaster.getEntityMaster().getEntityTypeMaster());
						onSelectType(); 
					    setEntityMaster(holidayMaster.getEntityMaster());
					}
			       
					
			        
			        loadHeadandRegionalOffice();
					loadEntityTypeList();
					
			        
			        log.info("HolidayMaster EntityType" + holidayMaster.getEntityMaster().getEntityTypeMaster());
			        
			        log.info("HolidayMaster RegionHeadOffice" + headandRegionaOfficeId.getName());
					
					return CREATE_HOLIDAY_MASTER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete HolidayMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmHolidayMasterDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view HolidayMaster called..");
				String viewurl = HOLIDAYMASTER_URL + "/getholidaymaster/" + selectedHolidayMaster.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					holidayMaster = mapper.readValue(jsonResponse, new TypeReference<HolidayMaster>() {
					});
					
					
					 if (holidayMaster.getEntityMaster().getId() != null) {
							String URL = AppUtil.getPortalServerURL() + "/entitymaster/region/"
									+ holidayMaster.getEntityMaster().getId();

							log.info("entity-- URL-----> " + URL);

							BaseDTO baseDTO = httpService.get(URL);
							if (baseDTO.getStatusCode() == 0) {
								ObjectMapper mapper = new ObjectMapper();
								String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
								 entityMaster = mapper.readValue(jsonResponse, EntityMaster.class);
								log.info("entity-- region id----->" + entityMaster.getId());
							}

						}
				        
				        
				        String entityTypeCode = holidayMaster.getEntityMaster().getEntityTypeMaster().getEntityCode();

						log.info("entityTypeCode -----> " + entityTypeCode);

						if ((EntityType.HEAD_OFFICE).equals(entityTypeCode)) {
							
							log.info("Inside head office entity");
							holidayMaster.getEntityMaster().setEntityMasterRegion(holidayMaster.getEntityMaster());							
						} else {
							holidayMaster.getEntityMaster().setEntityMasterRegion(entityMaster);
						}
					log.info("EntityMaster HO/RO Name", holidayMaster.getEntityMaster().getEntityMasterRegion().getName().toString());
					
					return INPUT_FORM_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showHolidayListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends HolidayBean.showHolidayListPageAction ===========>" + action);
		return null;
	}
public void loadAllStates() {
	try {
		stateMasterList = commonDataService.loadActiveStates(selectedCountryMaster.getId());
		log.info("stateMasterList======size===>"+stateMasterList.size());
	}catch (Exception e) {
	log.info("exception at====>",e);
	}
}
	/* saveOrUpdate method used to save and update in Holiday entity */
	public String saveOrUpdate() {
		log.info("<==== Starts HolidayBean.saveOrUpdate =======>");
		try {
			if (holidayMaster.getName() == null || holidayMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.HOLIDAY_MASTER_HOLIDAY_NAME_FIELD_EMPTY.getCode());
				return null;
			}
			if (holidayMaster.getName().trim().length() < 3 || holidayMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.HOLIDAY_MASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if(entityMaster != null) {
				holidayMaster.setEntityMaster(entityMaster);
			}else {
				holidayMaster.setEntityMaster(headandRegionaOfficeId);
			}
			
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("getSelectedStateMaster===>"+getSelectedStateMaster());
				holidayMaster.setStateMaster(getSelectedStateMaster());
			} else if (action.equalsIgnoreCase("EDIT")) {
				if (holidayMaster.getStatus() == null) {
					errorMap.notify(ErrorDescription.HOLIDAY_MASTER_HOLIDAY_STATUS_FIELD_EMPTY.getCode());
					return null;
				}
			}
			String url = HOLIDAYMASTER_URL + "/saveorupdate";
			BaseDTO response = httpService.put(url, holidayMaster);
			if (response != null && response.getStatusCode() == 0) {
				log.info("HolidayMaster saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.HOLIDAY_MASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.HOLIDAY_MASTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends HolidayBean.saveOrUpdate =======>");
		return showHolidayListPage();
	}

	/*
	 * onRowSelect method used to select particular row entity details in
	 * listHoliday page
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts HolidayBean.onRowSelect ========>" + event);
		selectedHolidayMaster = ((HolidayMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends HolidayBean.onRowSelect ========>");
	}

	/*
	 * deleteConfirm method used to delete particular row entity details in
	 * listHoliday page
	 */
	public String deleteConfirm() {
		log.info("<===== Starts HolidayBean.deleteConfirm ===========>");
		try {
			String url = HOLIDAYMASTER_URL + "/delete/" + selectedHolidayMaster.getId();
			log.info("deleteConfirm :: url==> "+url);
			BaseDTO response = httpService.delete(url);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.HOLIDAY_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmHolidayMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete HolidayMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends HolidayBean.deleteConfirm ===========>");
		return showHolidayListPage();
	}
	
	 public void loadHeadandRegionalOffice() {
	        log.info("<<<< ----------Starts loadHeadAndRegionalOffice ------- >>>>");

	        headAndRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
	        log.info("ho/ro list////////////////", headAndRegionalOfficeList);

	        log.info("<<<< ----------Ends loadHeadAndRegionalOffice ------- >>>>");
	    }
	 
	 
	 public void loadEntityTypeList() {
	        log.info("<<<< ----------Starts loadEntityTypeList ------- >>>>");
	        entityTypeLists = commonDataService.getAllEntityType();
	        log.info("Holiday entity type list..", entityTypeLists);

	        log.info("<<<< ----------Ends loadEntityTypeList ------- >>>>");
	    }
	 
	 public void headAndRegional() {
	        log.info("<<<< ----------Starts filternig based on Head And Regional Office------- >>>>");
	        log.info("entity master", headandRegionaOfficeId);

	        selectedEntityTypeId = null;
	        entityMaster = null;
	        try {

	            if (headandRegionaOfficeId != null
	                    && headandRegionaOfficeId.getEntityTypeMaster() != null && headandRegionaOfficeId.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
	               // createCircularDTO.setEntityId(headangregionaofficeid.getId());
	            	
	                visibility = true;

	            } else {
	                visibility = false;
	            }

	        } catch (Exception exp) {
	            log.error("found exception in onchangeEntityType: ", exp);
	        }

	        log.info("<<<< ----------End filternig based on Head And Regional Office------- >>>>");
	    }
	 
	 
	
	 
	   public void onSelectType() {

	        log.info("<<<< ----------Starts onSelectEntity ------- >>>>");
	        log.info("entity master", selectedEntityTypeId);

	        try {


	            if (headandRegionaOfficeId != null
	                    && headandRegionaOfficeId.getId() != null
	                    && selectedEntityTypeId != null
	                    && selectedEntityTypeId.getId() != null) {
	                Long regionId = headandRegionaOfficeId.getId();
	                entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, selectedEntityTypeId.getId());
	            } else {
	                log.info("entity not found.");
	            }
	        } catch (Exception exp) {
	            log.error("found exception in onchangeEntityType: ", exp);
	        }
	        log.info("<<<< ----------End onSelectEntity ------- >>>>");


	    }
}