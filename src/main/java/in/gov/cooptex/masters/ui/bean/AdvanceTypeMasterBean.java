package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AdvanceTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("advanceTypeMasterBean")
public class AdvanceTypeMasterBean {
	@Getter
	@Setter
	AdvanceTypeMaster advanceTypeMaster, selectedAdvanceTypeMaster;

	@Getter
	@Setter
	LazyDataModel<AdvanceTypeMaster> lazyAdvanceTypeMasterList;

	List<AdvanceTypeMaster> advanceTypeMasterList = null;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	String action;

	@Autowired
	AppPreference appPreference;

	final String SERVER_URL = AppUtil.getPortalServerURL();
	//final String Advance_Type_Master_URL = AppUtil.getPortalServerURL() + "/advancetypemaster";

	ObjectMapper mapper;

	String jsonResponse;
	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter @Setter
	List<GlAccount> glAccountList;
	
	@Getter @Setter
	String glAccountCode,glAccountName;
	
	@Autowired
	CommonDataService commonDataService;

	private final String CREATE_PAGE = "/pages/masters/advanceType/createAdvanceType.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/advanceType/listAdvanceType.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE = "/pages/masters/advanceType/viewAdvanceType.xhtml?faces-redirect=true;";

	public String showAdvanceTypeMasterListPageAction() {
		log.info("<===== Starts advancetypemaster.showpetitionTypePageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedAdvanceTypeMaster == null || selectedAdvanceTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create advancetypemaster called..");
				setGlAccountName("");
				advanceTypeMaster = new AdvanceTypeMaster();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit advancetypemaster called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/advancetypemaster/get/"
						+ selectedAdvanceTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					advanceTypeMaster = mapper.readValue(jsonResponse, new TypeReference<AdvanceTypeMaster>() {
					});
					setGlAccountName(advanceTypeMaster.getGlAccountCode());
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View advancetypemaster called..");

				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/advancetypemaster/get/"
						+ selectedAdvanceTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						advanceTypeMaster = mapper.readValue(jsonResponse, new TypeReference<AdvanceTypeMaster>() {
						});
						setGlAccountName(advanceTypeMaster.getGlAccountCode());
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete advancetypemaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAdvanceTypeMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showAdvanceTypeMasterListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends advanceTypeMasterBean.showAdvanceTypeMasterListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts AdvanceTypeMasterBean.deleteConfirm ===========>" + selectedAdvanceTypeMaster.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/advancetypemaster/deletebyid/" + selectedAdvanceTypeMaster.getId());

			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAdvanceTypeMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete AdvanceTypeMaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends AdvanceTypeMasterBean.deleteConfirm ===========>");
		return showAdvanceTypeMasterListPage();
	}

	public String saveOrUpdate() {
	log.info("<==== Starts AdvanceTypeMasterBean.saveOrUpdate =======>" + advanceTypeMaster.getLocName());
		try {
			if (advanceTypeMaster.getMinAdvanceAmount() > advanceTypeMaster.getMaxAdvanceAmount()) {
				log.error("Minimum Amount Should be less than Maximum Amount");
				Util.addWarn("Minimum Amount Should be less than Maximum Amount");
				return null;
			}
			if (advanceTypeMaster.getCode() == null || advanceTypeMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_CODE_EMPTY.getCode());
				return null;
			}
			if (advanceTypeMaster.getName() == null || advanceTypeMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_NAME_EMPTY.getCode());
				return null;
			}
			if (advanceTypeMaster.getLocName() == null || advanceTypeMaster.getLocName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_LOC_NAME_EMPTY.getCode());
				return null;
			}

			if (advanceTypeMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_STATUS_EMPTY.getCode());
				return null;
			}

			if (advanceTypeMaster.getCode().trim().length() < 3 || advanceTypeMaster.getCode().trim().length() > 75) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_CODE_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (advanceTypeMaster.getName().trim().length() < 3 || advanceTypeMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (advanceTypeMaster.getLocName().trim().length() < 3 || advanceTypeMaster.getLocName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_LOC_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(glAccountName == null && glAccountName.isEmpty()) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_NAME_EMPTY.getErrorCode());
				return null;
			}
			/*if(StringUtils.isNotEmpty(glAccountName)) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_NAME_EMPTY.getErrorCode());
				return null;
			}*/
			
			log.info("GlAccount Id ===> "+glAccountName);
			advanceTypeMaster.setGlAccountCode(glAccountName.split("/")[0]);
			log.info("GlAccount Code ===> "+advanceTypeMaster.getGlAccountCode());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/advancetypemaster/saveorupdate";
			BaseDTO response = httpService.post(url, advanceTypeMaster);

			if (response != null && response.getStatusCode() == 0) {
				log.info("advancetypemaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ADVANCETYPEMASTER_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends advancetypemaster.saveOrUpdate =======>");
		return showAdvanceTypeMasterListPage();
	}

	public String showAdvanceTypeMasterListPage() {
		log.info("<==== Starts advancetypemaster.showpetitiontypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		advanceTypeMaster = new AdvanceTypeMaster();
		selectedAdvanceTypeMaster = new AdvanceTypeMaster();
		loadLazyAdvanceTypeMasterList();
		log.info("<==== Ends advanceTypeMasterBean.showAdvanceTypeMasterListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyAdvanceTypeMasterList() {
		log.info("<===== Starts advanceTypeMasterBean.loadLazyAdvanceTypeMasterList ======>");

		lazyAdvanceTypeMasterList = new LazyDataModel<AdvanceTypeMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<AdvanceTypeMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/advancetypemaster/getalladvancetypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						advanceTypeMasterList = mapper.readValue(jsonResponse,
								new TypeReference<List<AdvanceTypeMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("advanceTypeMasterBean size---------------->"
								+ lazyAdvanceTypeMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return advanceTypeMasterList;
			}

			@Override
			public Object getRowKey(AdvanceTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AdvanceTypeMaster getRowData(String rowKey) {
				for (AdvanceTypeMaster advanceTypeMaster : advanceTypeMasterList) {
					if (advanceTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedAdvanceTypeMaster = advanceTypeMaster;
						return advanceTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  advanceTypeMasterBean.loadLazyAdvanceTypeMasterList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts advanceTypeMasterBean.onRowSelect ========>" + event);
		selectedAdvanceTypeMaster = ((AdvanceTypeMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends advanceTypeMasterBean.onRowSelect ========>" + selectedAdvanceTypeMaster.getId());
	}
	
	public List<String> glaccountAutoComplete(String query) {
		log.info("glaccountAutoComplete query==>" + query);
		List<String> results = new ArrayList<String>();
		glAccountList = new ArrayList<>();
		glAccountList = commonDataService.loadGlAccountsAutoCompleteSearch(query);
		if (glAccountList != null || !glAccountList.isEmpty()) {
			for (GlAccount account : glAccountList) {
				String acc = account.getCode()+"/"+account.getName();
				results.add(acc);
			}
		}
		return results;
	}

}
