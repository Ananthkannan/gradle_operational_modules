package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement.RetirementType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeeVoluntaryRetirementBean")
@Scope("session")
public class EmployeeVoluntaryRetirementBean {

	final String EMP_VOLUNTARY_RETIREMENT_LIST_PAGE = "/pages/personnelHR/employeeProfile/listVolundaryRetirement.xhtml?faces-redirect=true";
	final String EMP_VOLUNTARY_RETIREMENT_CREATE_PAGE = "/pages/personnelHR/employeeProfile/requestVolundaryRetirement.xhtml?faces-redirect=true";
	final String EMP_VOLUNTARY_RETIREMENT_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewVoluntaryRetirement.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action, focusProperty, header;

	@Getter
	@Setter
	EmployeeRetirement selectedVoluntaryRetirement, voluntaryRetirement;

	@Setter
	@Getter
	Boolean addButtonFlag = true, editButtonFlag = true, deleteButtonFlag = true, viewButtonFlag = true,
			approveButtonFlag = true;

	@Getter
	@Setter
	private String documentFileName;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Getter
	@Setter
	LazyDataModel<EmployeeRetirement> voluntaryRetirementLazyList;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Getter
	@Setter
	int totalRecords = 0;

	@Autowired
	ErrorMap errorMap;

	public String showVoluntaryRetirementListPage() {
		log.info("<== Starts  EmployeeVoluntaryRetirementBean.showVoluntaryRetirementListPage==>");
		mapper = new ObjectMapper();
		addButtonFlag = true;
		editButtonFlag = true;
		viewButtonFlag = true;
		approveButtonFlag = true;
		deleteButtonFlag = true;
		selectedVoluntaryRetirement = new EmployeeRetirement();
		voluntaryRetirement = new EmployeeRetirement();
		loadLazyVoluntaryRetirementList();
		documentFileName = "";
		log.info("<== ends  EmployeeVoluntaryRetirementBean.showVoluntaryRetirementListPage==>");
		return EMP_VOLUNTARY_RETIREMENT_LIST_PAGE;
	}

	public void loadLazyVoluntaryRetirementList() {
		voluntaryRetirementLazyList = new LazyDataModel<EmployeeRetirement>() {

			private static final long serialVersionUID = 2849181192334586867L;

			List<EmployeeRetirement> empList = new ArrayList<>();

			@Override
			public List<EmployeeRetirement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				PaginationDTO paginationRequestDto = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				try {
					paginationRequestDto.setUserId(
							commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId()).getId());
					paginationRequestDto.setRetirementType(RetirementType.VOLUNTARY);
					log.info("Pagination request is ........" + paginationRequestDto);
					BaseDTO responseDto = httpService.post(SERVER_URL + "/employee/retirement/lazyload",
							paginationRequestDto);
					if (responseDto != null && responseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(responseDto.getResponseContents());
						empList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeRetirement>>() {
						});
						this.setRowCount(responseDto.getTotalRecords());
						totalRecords = responseDto.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception in voluntaryEmpRetirementListLazy ..", e);
				}
				return empList;
			}

			@Override
			public Object getRowKey(EmployeeRetirement object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmployeeRetirement getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmployeeRetirement emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	public String voluntaryRetirementAction() {
		log.info("<== Starts  EmployeeVoluntaryRetirementBean.voluntaryRetirementAction==>" + action);
		switch (action) {
		case "Create":
			return getEmployeeDetails();
		case "Edit":
			return showVoluntaryRetirement();
		case "View":
			return showVoluntaryRetirement();
		case "Delete":
			if (selectedVoluntaryRetirement == null || selectedVoluntaryRetirement.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmRetirementDelete').show();");
			return null;
		default:
			break;
		}
		log.info("<== Ends  EmployeeVoluntaryRetirementBean.voluntaryRetirementAction==>");
		return null;
	}

	public String showVoluntaryRetirement() {
		log.info("<== Starts  EmployeeVoluntaryRetirementBean.getVoluntaryRetirement==>");
		try {
			if (selectedVoluntaryRetirement == null || selectedVoluntaryRetirement.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			BaseDTO response = httpService
					.get(SERVER_URL + "/employee/retirement/get/" + selectedVoluntaryRetirement.getId());
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				voluntaryRetirement = mapper.readValue(jsonResponse, new TypeReference<EmployeeRetirement>() {
				});
				documentFileName = voluntaryRetirement.getDocumentPath();
				voluntaryRetirement.setAge(calculateAge(voluntaryRetirement.getEmployee().getDob()));
				if (action.equalsIgnoreCase("View"))
					return EMP_VOLUNTARY_RETIREMENT_VIEW_PAGE;
				else
					return EMP_VOLUNTARY_RETIREMENT_CREATE_PAGE;
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.info("Exception occured in voluntaryRetirementAction...");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<== Ends  EmployeeVoluntaryRetirementBean.getVoluntaryRetirement==>");
		return null;
	}

	public String deleteVoluntaryRetirement() {
		log.info("<== Starts  EmployeeVoluntaryRetirementBean.deleteVoluntaryRetirement==>");
		try {
			if (selectedVoluntaryRetirement == null || selectedVoluntaryRetirement.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			BaseDTO response = httpService
					.delete(SERVER_URL + "/employee/retirement/delete/" + selectedVoluntaryRetirement.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.RETIREMENT_VOLUNTARY_DELETED_SUCCESSFULLY.getCode());
				return showVoluntaryRetirementListPage();
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.info("Exception occured in voluntaryRetirementAction...");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<== Ends  EmployeeVoluntaryRetirementBean.deleteVoluntaryRetirement==>");
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeeVoluntaryRetirementBean.onRowSelect ========>" + event);
		selectedVoluntaryRetirement = ((EmployeeRetirement) event.getObject());
		if (!selectedVoluntaryRetirement.getRetirementStatus().equals(ApprovalStatus.INPROGRESS)) {
			editButtonFlag = false;
			deleteButtonFlag = false;
		}
		addButtonFlag = false;
		viewButtonFlag = true;
		/*
		 * if(selectedDepartment.getStatus() == true ){ deleteButtonFlag=true; }else
		 * if(selectedDepartment.getStatus() == false){ deleteButtonFlag=false; }
		 */
		log.info("<===Ends EmployeeVoluntaryRetirementBean.onRowSelect ========>");
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			uploadedFile = event.getFile();
			long size = uploadedFile.getSize();
			log.info("Employee retirement request document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				voluntaryRetirement.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_RETIREMENT_REQUEST_UPLOAD_PATH");
			File file = new File(uploadPath);
			uploadPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(uploadPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Uploaded document path is :" + uploadPath);
			documentFileName = uploadedFile.getFileName();
			String append = new Date().toGMTString() + documentFileName;
			voluntaryRetirement.setDocumentPath(uploadPath + append);
			errorMap.notify(ErrorDescription.INFO_EMP_RETIREMENT_DOCUMENT_UPLOADED.getErrorCode());
			inputStream = uploadedFile.getInputstream();
			log.info(" Path Doesn't exixts" + append);
			outputStream = new FileOutputStream(uploadPath + append);
			byte[] buffer = new byte[(int) uploadedFile.getSize()];
			int bytesRead = 0;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			log.info(" upload Path is >>>" + uploadPath);
			log.info(" Retirement Document is uploaded successfully");
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String saveOrUpdateEmployeeRetirement() {
		log.info("<== Starts  EmployeeVoluntaryRetirementBean.saveOrUpdateEmployeeRetirement==>");
		try {
			BaseDTO saveDTO = null;
			if (Objects.isNull(voluntaryRetirement)) {
				log.error("[ Employee voluntary retirement details is : ]" + voluntaryRetirement);
				errorMap.notify(ErrorDescription.EMP_RETIREMENT_NOT_FOUND.getErrorCode());
				return null;
			}

			log.info("[ Employee voluntary retirement response is : ]" + voluntaryRetirement.getRetirementType());
			saveDTO = httpService.post(SERVER_URL + "/employee/retirement/create", voluntaryRetirement);

			if (Objects.isNull(saveDTO)) {
				log.error("[ Employee retirement response is : ]" + saveDTO);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				return null;
			}

			if (saveDTO.getStatusCode() == 0) {
				log.error("[ Employee retirement success response is : ]" + saveDTO);
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.RETIREMENT_VOLUNTARY_REQUEST_SUCCESSFULLY.getErrorCode());
				else
					errorMap.notify(ErrorDescription.RETIREMENT_VOLUNTARY_UPDATED_SUCCESSFULLY.getErrorCode());
				return showVoluntaryRetirementListPage();
			} else {
				log.error("[ Employee retirement failuer response is : ]" + saveDTO);
				errorMap.notify(saveDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("[ Exception while saveOrUpdateEmployeeRetirement details ]", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<== Ends  EmployeeVoluntaryRetirementBean.saveOrUpdateEmployeeRetirement==>");
		return null;
	}

	public String getEmployeeDetails() {
		log.info("[ Get employee details ]");
		try {
			EmployeeMaster empMaster = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
			voluntaryRetirement.setEmployee(empMaster);
			voluntaryRetirement.setRetirementType(RetirementType.VOLUNTARY);
			return EMP_VOLUNTARY_RETIREMENT_CREATE_PAGE;
		} catch (Exception e) {
			log.error("[ Exception while get employee details ]", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return null;
	}

	public int calculateAge(Date dateOfBirth) {
		Calendar dob = Calendar.getInstance();
		dob.setTime(dateOfBirth);
		Calendar today = Calendar.getInstance();
		int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR))
			age--;
		return age;
	}
}
