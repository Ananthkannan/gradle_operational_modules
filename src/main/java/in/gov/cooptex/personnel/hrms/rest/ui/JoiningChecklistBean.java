package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AppointmentDetailsDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DashboardDTO;
import in.gov.cooptex.core.dto.DocumentDTO;
import in.gov.cooptex.core.dto.JoiningCheckedListDTO;
import in.gov.cooptex.core.dto.JoiningChecklistDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.DocumentTypeMaster;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dashboard.ui.bean.DashboardBean;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.model.AppointmentOrderDetail;
import in.gov.cooptex.personnel.hrms.dto.CircularListDTO;
import in.gov.cooptex.personnel.hrms.model.EmpJoiningChecklist;
import in.gov.cooptex.personnel.hrms.model.EmpJoiningChecklistDocs;
import in.gov.cooptex.personnel.hrms.model.EmpJoiningChecklistLog;
import in.gov.cooptex.personnel.hrms.model.EmpJoiningChecklistNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("joiningChecklistBean")
public class JoiningChecklistBean extends CommonBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/personnelHR/joiningChecklist/createJoiningChecklist.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/personnelHR/joiningChecklist/viewJoiningChecklist.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/personnelHR/joiningChecklist/listJoiningChecklist.xhtml?faces-redirect=true";

	final String REJECT = "REJECT";

	final String APPROVED = "APPROVED";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster forwardTo = new UserMaster();

	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	List<DocumentTypeMaster> documentTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	DocumentTypeMaster documentTypeMaster = new DocumentTypeMaster();

	@Getter
	@Setter
	String pageAction, appointmentNumber, note, remarks;

	@Getter
	@Setter
	AppointmentDetailsDTO appointmentDetailsDTO = new AppointmentDetailsDTO();

	@Getter
	@Setter
	String fileName;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Setter
	@Getter
	private List<DocumentDTO> documentFileList = new ArrayList<>();

	@Setter
	@Getter
	private List<BankMaster> bankMasterList = new ArrayList<>();

	@Setter
	@Getter
	BankMaster bankMaster = new BankMaster();

	@Setter
	@Getter
	private List<BankBranchMaster> bankBranchMasterList = new ArrayList<>();

	@Setter
	@Getter
	BankBranchMaster bankBranchMaster = new BankBranchMaster();

	@Setter
	@Getter
	List<PaymentMode> modeOfPaymentList = new ArrayList<>();

	@Setter
	@Getter
	PaymentMode paymentMode = new PaymentMode();

	@Setter
	@Getter
	Boolean cheqFlag = false, ddFlag = false;

	@Setter
	@Getter
	Double ddAmount, cheqAmount, docAmount;

	@Setter
	@Getter
	String cheqNumber, cheqName, ddNumber, payableAt, docNumber;

	@Setter
	@Getter
	Date cheqDate, issuedDate, expDate, docDate;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	LazyDataModel<JoiningChecklistDTO> joiningChecklistDTOLazyList;

	@Getter
	@Setter
	List<JoiningChecklistDTO> joiningChecklistDTOList;

	@Getter
	@Setter
	JoiningChecklistDTO selectedJoiningChecklistDTO = new JoiningChecklistDTO();

	@Getter
	@Setter
	EmpJoiningChecklist empJoiningChecklist = new EmpJoiningChecklist();

	@Getter
	@Setter
	List<EmpJoiningChecklistLog> empJoiningChecklistLogList = new ArrayList<>();

	@Getter
	@Setter
	String joiningChecklistStatus;

	@Getter
	@Setter
	Boolean viewFlag = false, addFlag = false, editFlag = false, deleteFlag = false;

	@Getter
	@Setter
	Long noteId;

	long allowedDocSize;

	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Autowired
	DashboardBean dashBoardBean;
	
	@Autowired
	SystemNotificationBean systemNotificationBean;
	
	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedJoiningChecklistDTO = new JoiningChecklistDTO();
			selectedJoiningChecklistDTO.setCheckListId(Long.parseLong(id));
			pageAction = "VIEW";
			selectedNotificationId = Long.parseLong(notificationId);
			viewJoiningChecklist();
		}
		log.info("joiningChecklistBean showViewListPage() Ends");
		return VIEW_PAGE;
	}

	public void loadJoiningCheckList() {
		log.info("joiningChecklistBean loadJoiningCheckList is executed.....................");
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CHECKLIST");
		documentTypeMasterList = commonDataService.getDocumentType();
		bankMasterList = commonDataService.loadAllActiveBanks();
		modeOfPaymentList = commonDataService.loadPaymentModes();

	}

	public void changeBankFlag() {
		log.info("joiningChecklistBean :: changeBankFlag start");
		if (paymentMode == null) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.SELECT_THE_PAYMENT_MODE).getCode());
			return;
		}
		if (paymentMode.getCode().equals("DD")) {
			cheqFlag = false;
			ddFlag = true;
		} else if (paymentMode.getCode().equals("CHEQUE")) {
			cheqFlag = true;
			ddFlag = false;
		} else {
			// cheqFlag = false;
			// ddFlag = false;
			cheqFlag = true;
			ddFlag = false;
		}
	}

	/*
	 * public void changeBankFlag() {
	 * log.info("joiningChecklistBean :: changeBankFlag start");
	 * if(modeOfPayment.equals(ModeOfPaymentEnum.Cheque.toString())) { cheqFlag =
	 * true; ddFlag = false; }else
	 * if(modeOfPayment.equals(ModeOfPaymentEnum.DD.toString())) { cheqFlag = false;
	 * ddFlag = true; }else { cheqFlag = false; ddFlag = false; } }
	 */

	public void loadBranchByBank() {
		log.info("joiningChecklistBean :: loadBranchByBank start");
		try {
			if (bankMaster == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SELECT_THE_BANK).getCode());
				return;
			}
			log.info("bank id==> " + bankMaster.getId());
			bankBranchMasterList = commonDataService.loadAllActiveBranchByBankId(bankMaster.getId());
			if (bankBranchMasterList != null && !bankBranchMasterList.isEmpty()) {
				log.info("bankBranchMasterList size==> " + bankBranchMasterList.size());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadBranchByBank() --->", exp);
		}
	}

	public String showJoiningCheckList() {
		log.info("joiningChecklistBean :: showJoiningCheckList start");
		viewFlag = true;
		addFlag = true;
		editFlag = false;
		deleteFlag = false;
		if (pageAction.equals("LIST")) {
			clearJoiningCheckList();
			loadLazyJoiningList();
			return LIST_PAGE;
		} else if (pageAction.equals("ADD")) {
			loadJoiningCheckList();
			return ADD_PAGE;
		} else if (pageAction.equals("EDIT")) {
			if (selectedJoiningChecklistDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			return editJoiningChecklist();
			// return ADD_PAGE;
		} else if (pageAction.equals("VIEW")) {
			if (selectedJoiningChecklistDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			return viewJoiningChecklist();
		} else if (pageAction.equals("DELETE")) {
			if (selectedJoiningChecklistDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");
			return "";
			// return LIST_PAGE;
		}
		return null;
	}

	public String deleteJoiningChecklist() {
		try {
			if (selectedJoiningChecklistDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected checklist  Id :: " + selectedJoiningChecklistDTO.getCheckListId());
			String getUrl = SERVER_URL + "/joiningChecklist/delete/id/:id";
			url = getUrl.replace(":id", selectedJoiningChecklistDTO.getCheckListId().toString());
			log.info("deleteIncrement url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Joining checklist Deleted SuccessFully ");
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_DELETED_SUCCESSFULLY).getCode());
				clearJoiningCheckList();
				loadLazyJoiningList();
				return LIST_PAGE;
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void searchEmployeeDetails() {
		log.info("joiningChecklistBean :: showJoiningCheckList start");
		try {
			if (appointmentNumber == null || appointmentNumber.isEmpty()) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.APPOINTMENT_ORDER_NUMBER_NOT_EMPTY).getCode());
				return;
			}
			String url = AppUtil.getPortalServerURL() + "/appointmentorderdetail/getemployeebyregnumber/"
					+ appointmentNumber;
			log.info("searchEmployeeDetails :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<AppointmentDetailsDTO> appointmentDetailsDTOList = mapper.readValue(jsonValue,
							new TypeReference<List<AppointmentDetailsDTO>>() {
							});
					if (appointmentDetailsDTOList != null && !appointmentDetailsDTOList.isEmpty()) {
						log.info("searchEmployeeDetails :: searchEmployeeDetails size==> "
								+ appointmentDetailsDTOList.size());
						appointmentDetailsDTO = appointmentDetailsDTOList.get(0);
					} else {
						log.error("Employee information not found in appointmentNumber : " + appointmentNumber);
						appointmentDetailsDTO = new AppointmentDetailsDTO();
					}
					log.info("searchEmployeeDetails :: appointmentDetailsDTO => " + appointmentDetailsDTO);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in searchEmployeeDetails() --->", exp);
		}
	}

	public void clearOrderNumber() {
		appointmentNumber = null;
		appointmentDetailsDTO = new AppointmentDetailsDTO();
	}

	public void documentFileUpload(FileUploadEvent event) {
		log.info("<====== joiningChecklistBean Upload Event =====>" + event);
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		String type = FilenameUtils.getExtension(uploadedFile.getFileName());
		log.info(" File Type :: " + type);
		//
		boolean validFileType = AppUtil.isValidFileType(type,
				new String[] { "png", "jpg", "JPG", "doc", "jpeg", "gif", "docx", "pdf", "xlsx", "xls" });

		if (!validFileType) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_UPLOAD_FORMAT).getCode());
			return;
		}
		long size = uploadedFile.getSize();
		log.info("uploadSignature size==> - 1: " + size);
		size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
		log.info("uploadSignature size==> - 2: " + size);
		allowedDocSize = Long.valueOf(commonDataService.getAppKeyValue("EMP_CHECKLIST_DOC_SIZE_LIMIT_IN_MB"));
		log.info("allowedDocSize ==> " + allowedDocSize);
		if (size > allowedDocSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_FILE_SIZE).getCode());
			return;
		}
		String incomingTapalUploadPath = commonDataService.getAppKeyValue("EMP_CHECKLIST_DOC_UPLOAD_PATH");
		log.info("incomingTapalUploadPath >>>> " + incomingTapalUploadPath);
		setFileName(uploadedFile.getFileName());
		RequestContext.getCurrentInstance().execute("PF('pridocupload').hide();");
		errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_UPLOAD_SUCCESSFULLY).getCode());
		log.info(" tapalFileUpload  is uploaded successfully");
	}

	public void loadDocumentList() {
		log.info("<====== joiningChecklistBean :: loadDocumentList =====>");
		if (fileName == null) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.UPLOAD_DOCUMENTS_NOT_EMPTY).getCode());
			return;
		}
		if (documentTypeMaster == null) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_NOT_EMPTY).getCode());
			return;
		}
		log.info("loadDocumentList fileName==> " + fileName);
		log.info("loadDocumentList documentTypeMaster==> " + documentTypeMaster);
		if (documentFileList != null && !documentFileList.isEmpty()) {
			for (DocumentDTO docDTO : documentFileList) {
				if (docDTO.getFileName().contains(fileName)) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_ALREDY_EXISTS).getCode());
					return;
				}
				if (docDTO.getDocumentName().contains(documentTypeMaster.getName())) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_ALREDY_EXISTS).getCode());
					return;
				}
			}
		}
		DocumentDTO documentDTO = new DocumentDTO();
		documentDTO.setDocumentName(documentTypeMaster.getName());
		documentDTO.setFileName(fileName);
		log.info("documentDTO==> " + documentDTO);
		documentFileList.add(documentDTO);
		documentTypeMaster = new DocumentTypeMaster();
		fileName = null;
	}

	public void clearDocument() {
		log.info("<====== joiningChecklistBean :: clearDocument =====>");
		documentTypeMaster = new DocumentTypeMaster();
		fileName = null;
		documentFileList = null;
		documentFileList = new ArrayList<>();
	}

	public void loadLazyJoiningList() {
		log.info("<==== Starts IncrementBean.loadLazyIncrementList =====>");
		joiningChecklistDTOLazyList = new LazyDataModel<JoiningChecklistDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<JoiningChecklistDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/joiningChecklist/loadjoiningchecklist";
					log.info("loadLazyIncrementList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						joiningChecklistDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<JoiningChecklistDTO>>() {
								});
						log.info("loadLazyIncrementList :: joiningChecklistDTOList size==> "
								+ joiningChecklistDTOList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyIncrementList :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return joiningChecklistDTOList;
			}

			@Override
			public Object getRowKey(JoiningChecklistDTO res) {
				return res != null ? res.getCheckListId() : null;
			}

			@Override
			public JoiningChecklistDTO getRowData(String rowKey) {
				for (JoiningChecklistDTO joiningChecklistDTO : joiningChecklistDTOList) {
					if (joiningChecklistDTO.getCheckListId().equals(Long.valueOf(rowKey))) {
						selectedJoiningChecklistDTO = joiningChecklistDTO;
						return joiningChecklistDTO;
					}
				}
				return null;
			}
		};

	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedJoiningChecklistDTO = ((JoiningChecklistDTO) event.getObject());

		if (selectedJoiningChecklistDTO.getStatus().equals("APPROVED")) {
			viewFlag = true;
			addFlag = true;
			editFlag = false;
			deleteFlag = false;
		} else if (selectedJoiningChecklistDTO.getStatus().equals("REJECTED")) {
			viewFlag = true;
			addFlag = true;
			editFlag = true;
			deleteFlag = true;
		} else {
			viewFlag = true;
			addFlag = false;
			editFlag = false;
			deleteFlag = false;
		}

	}

	public void deleteJoiningCheckList(DocumentDTO documentDTO) {
		log.info(" Delete Single Record  for deleteJoiningCheckList :: " + documentDTO);
		try {
			if (documentFileList != null && !documentFileList.isEmpty()) {
				documentFileList.remove(documentDTO);
			} else {
				log.error(" document File List  Is Empty ");
			}
		} catch (Exception e) {
			log.error(" Exception in deleteJoiningCheckList : ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String saveCheckList() {
		log.info("<====== joiningChecklistBean :: saveCheckList =====>");
		try {
			if (appointmentNumber == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.APPOINTMENT_ORDER_NO_NOT_EMPTY).getCode());
				return null;
			}
			if (appointmentDetailsDTO == null || appointmentDetailsDTO.getDes() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DESIGNATION_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (appointmentDetailsDTO == null || appointmentDetailsDTO.getEmpName() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.EMPLOYEE_NAME_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (appointmentDetailsDTO == null || appointmentDetailsDTO.getSecurityDeposit() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SECURITY_DEPOSIT_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (documentFileList == null && documentFileList.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (paymentMode == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.MODE_OF_PAYMENT_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (paymentMode.getCode().equals("ECS")) {
				if (bankMaster == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.BANK_NAME_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (bankBranchMaster == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.BRANCH_NAME_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (payableAt == null) {
					AppUtil.addWarn("Payable At is required");
					return null;
				}
			}

			if (paymentMode.getCode().equals("CHEQUE")) {
				if (docAmount == null) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CHECK_OR_DD_AMOUNT_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (docDate == null) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CHECK_OR_DD_DATE_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (docNumber == null) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CHECK_OR_DD_NUMBER_IS_NOT_EMPTY).getCode());
					return null;
				}
			}

			if (forwardTo == null || forwardTo.getId() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_TO_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (forwardFor == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_FOR_IS_NOT_EMPTY).getCode());
				return null;
			}
			if (note == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NOTE_IS_NOT_EMPTY).getCode());
				return null;
			}
			EmpJoiningChecklist empJoiningChecklist = new EmpJoiningChecklist();
			log.info("joining checklist id==> " + appointmentDetailsDTO.getJoiningChecklistId());
			AppointmentOrderDetail appointmentOrderDetail = new AppointmentOrderDetail();
			appointmentOrderDetail.setId(appointmentDetailsDTO.getAppointmentOrderId());
			if (appointmentDetailsDTO.getJoiningChecklistId() != null) {
				empJoiningChecklist.setId(appointmentDetailsDTO.getJoiningChecklistId());
				empJoiningChecklist.setCreatedBy(appointmentDetailsDTO.getCreatedBy());
				empJoiningChecklist.setCreatedDate(appointmentDetailsDTO.getCreatedDate());
				empJoiningChecklist.setModifiedBy(appointmentDetailsDTO.getModifiedBy());
				empJoiningChecklist.setModifiedDate(appointmentDetailsDTO.getModifiedDate());
				empJoiningChecklist.setVersion(appointmentDetailsDTO.getVersion());
			}
			Designation designationMaster = new Designation();
			designationMaster.setId(appointmentDetailsDTO.getDesId());
			empJoiningChecklist.setDesignationMaster(designationMaster);
			empJoiningChecklist.setSecurityDeposit(appointmentDetailsDTO.getSecurityDeposit());
			empJoiningChecklist.setEmpName(appointmentDetailsDTO.getEmpName());
			empJoiningChecklist.setPaymentMode(paymentMode);
			empJoiningChecklist.setAppointmentOrderDetail(appointmentOrderDetail);
			empJoiningChecklist.setDocNumber(docNumber);
			empJoiningChecklist.setAmount(docAmount);
			empJoiningChecklist.setDocDate(docDate);
			empJoiningChecklist.setDocExpiryDate(expDate);
			empJoiningChecklist.setBankMaster(bankMaster);
			empJoiningChecklist.setPayableAt(payableAt);
			empJoiningChecklist.setBankBranchMaster(bankBranchMaster);

			List<EmpJoiningChecklistDocs> empJoiningChecklistDocsList = new ArrayList<>();
			if (documentFileList != null && !documentFileList.isEmpty()) {
				for (DocumentDTO documentDTO : documentFileList) {
					EmpJoiningChecklistDocs empJoiningChecklistDocs = new EmpJoiningChecklistDocs();
					if (documentDTO.getJoiningChecklistDocId() != null) {

						log.info("documentDTO.getJoiningChecklistDocId==> " + documentDTO.getJoiningChecklistDocId());
						empJoiningChecklistDocs.setId(documentDTO.getJoiningChecklistDocId());
						empJoiningChecklistDocs.setCreatedBy(documentDTO.getCreatedBy());
						empJoiningChecklistDocs.setCreatedDate(documentDTO.getCreatedDate());
						empJoiningChecklistDocs.setModifiedBy(documentDTO.getModifiedBy());
						empJoiningChecklistDocs.setModifiedDate(documentDTO.getModifiedDate());
						empJoiningChecklistDocs.setVersion(appointmentDetailsDTO.getVersion());
					}
					empJoiningChecklistDocs.setEmpJoiningChecklist(empJoiningChecklist);
					log.info("document name==> " + documentDTO.getDocumentName());
					empJoiningChecklistDocs.setDocumentName(documentDTO.getDocumentName());
					log.info("file name==> " + documentDTO.getFileName());
					empJoiningChecklistDocs.setDocumentPath(documentDTO.getFileName());
					empJoiningChecklistDocsList.add(empJoiningChecklistDocs);
				}
				empJoiningChecklist.setEmpJoiningChecklistList(empJoiningChecklistDocsList);
			} else {
				log.error("Document file not found");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOCUMENT_IS_NOT_EMPTY).getCode());
				return null;
			}

			List<EmpJoiningChecklistNote> empJoiningChecklistNoteList = new ArrayList<>();
			EmpJoiningChecklistNote empJoiningChecklistNote = new EmpJoiningChecklistNote();
			log.info("saveCheckList: note==> " + note);
			empJoiningChecklistNote.setNote(note);
			empJoiningChecklistNote.setEmpJoiningChecklist(empJoiningChecklist);
			log.info("saveCheckList: forwardTo id==> " + forwardTo.getId());
			empJoiningChecklistNote.setForwardTo(forwardTo);
			log.info("saveCheckList: forwardFor==> " + forwardFor);
			empJoiningChecklistNote.setFinalApproval(forwardFor);
			empJoiningChecklistNoteList.add(empJoiningChecklistNote);
			empJoiningChecklist.setEmpJoiningChecklistNoteList(empJoiningChecklistNoteList);

			List<EmpJoiningChecklistLog> empJoiningChecklistLogList = new ArrayList<>();
			EmpJoiningChecklistLog empJoiningChecklistLog = new EmpJoiningChecklistLog();
			empJoiningChecklistLog.setEmpJoiningChecklist(empJoiningChecklist);
			empJoiningChecklistLog.setStage(ApprovalStage.SUBMITTED);
			empJoiningChecklistLogList.add(empJoiningChecklistLog);
			empJoiningChecklist.setEmpJoiningChecklistLogList(empJoiningChecklistLogList);

			String url = SERVER_URL + "/joiningChecklist/createjoiningchecklist";
			log.info("saveEmployeeIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, empJoiningChecklist);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			mapper.readValue(jsonResponse, EmpIncrement.class);
			if (baseDTO.getStatusCode() == 0) {
				clearJoiningCheckList();
				loadLazyJoiningList();
				if (pageAction.equals("ADD")) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_INSERTED_SUCCESSFULLY)
							.getCode());
				} else if (pageAction.equals("EDIT")) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_UPDATED_SUCCESSFULLY)
							.getCode());
				}
				return LIST_PAGE;
			} else {
				log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("JoiningChecklistBean :: saveCheckList ::  Exception ==> ", e);
		}
		return null;
	}

	public void clearJoiningCheckList() {
		log.info("<====== joiningChecklistBean :: clearJoiningCheckList =====>");
		appointmentDetailsDTO = new AppointmentDetailsDTO();
		selectedJoiningChecklistDTO = new JoiningChecklistDTO();
		fileName = null;
		documentTypeMaster = new DocumentTypeMaster();
		documentFileList = new ArrayList<>();
		appointmentNumber = null;
		paymentMode = null;
		docNumber = null;
		docNumber = null;
		docDate = null;
		expDate = null;
		bankMaster = new BankMaster();
		payableAt = null;
		bankBranchMaster = new BankBranchMaster();
		forwardTo = new UserMaster();
		forwardFor = null;
		note = null;
		viewFlag = true;
		addFlag = true;
		editFlag = false;
		deleteFlag = false;
	}

	public String cancelCheckList() {
		clearJoiningCheckList();
		loadLazyJoiningList();
		return LIST_PAGE;
	}

	public String editJoiningChecklist() {

		log.info("<==== Starts IncrementBean.editJoiningChecklist =====>");
		try {
			if (selectedJoiningChecklistDTO == null && selectedJoiningChecklistDTO.getCheckListId() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_ID_NOT_FOUND).getCode());
				return null;
			}
			Long checklistId = selectedJoiningChecklistDTO.getCheckListId();
			log.info("viewJoiningChecklist :: checklistId==> " + checklistId);
			String url = SERVER_URL + "/joiningChecklist/getchecklistbyid/" + checklistId;
			log.info("viewJoiningChecklist :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get joiningchecklist value
				empJoiningChecklist = mapper.readValue(jsonResponse, EmpJoiningChecklist.class);
				log.info("joining checklist id==> " + empJoiningChecklist.getId());
				appointmentNumber = empJoiningChecklist.getAppointmentOrderDetail().getRegistrationNumber();
				log.info("appointmentNumber==> " + appointmentNumber);

				appointmentDetailsDTO.setJoiningChecklistId(empJoiningChecklist.getId());
				appointmentDetailsDTO.setEmpName(empJoiningChecklist.getEmpName());
				appointmentDetailsDTO.setDes(empJoiningChecklist.getDesignationMaster().getName());
				appointmentDetailsDTO.setDesId(empJoiningChecklist.getDesignationMaster().getId());
				appointmentDetailsDTO.setSecurityDeposit(empJoiningChecklist.getSecurityDeposit());
				appointmentDetailsDTO.setAppointmentOrderId(empJoiningChecklist.getAppointmentOrderDetail().getId());
				appointmentDetailsDTO.setVersion(empJoiningChecklist.getVersion());

				appointmentDetailsDTO.setCreatedBy(empJoiningChecklist.getCreatedBy());
				appointmentDetailsDTO.setCreatedDate(empJoiningChecklist.getCreatedDate());
				appointmentDetailsDTO.setModifiedBy(empJoiningChecklist.getModifiedBy());
				appointmentDetailsDTO.setModifiedDate(empJoiningChecklist.getModifiedDate());

				if (empJoiningChecklist.getEmpJoiningChecklistList() != null
						&& !empJoiningChecklist.getEmpJoiningChecklistList().isEmpty()) {

					for (EmpJoiningChecklistDocs empJoiningChecklistDocs : empJoiningChecklist
							.getEmpJoiningChecklistList()) {
						DocumentDTO documentDTO = new DocumentDTO();
						log.info("empJoiningChecklistDocs.getId==> " + empJoiningChecklistDocs.getId());
						documentDTO.setJoiningChecklistDocId(empJoiningChecklistDocs.getId());
						documentDTO.setDocumentName(empJoiningChecklistDocs.getDocumentName());
						documentDTO.setFileName(empJoiningChecklistDocs.getDocumentPath());
						documentDTO.setCreatedBy(empJoiningChecklistDocs.getCreatedBy());
						documentDTO.setCreatedDate(empJoiningChecklistDocs.getCreatedDate());
						documentDTO.setModifiedBy(empJoiningChecklistDocs.getModifiedBy());
						documentDTO.setModifiedDate(empJoiningChecklistDocs.getModifiedDate());
						documentDTO.setVersion(empJoiningChecklist.getVersion());
						documentFileList.add(documentDTO);
					}
				}
				paymentMode = empJoiningChecklist.getPaymentMode();
				if (paymentMode.getCode().equals("DD")) {
					ddFlag = true;
					cheqFlag = false;
				} else if (paymentMode.getCode().equals("CHEQUE")) {
					cheqFlag = true;
					ddFlag = false;
				} else {
					cheqFlag = true;
					ddFlag = false;
				}
				bankMaster = empJoiningChecklist.getBankMaster();
				bankBranchMaster = empJoiningChecklist.getBankBranchMaster();
				payableAt = empJoiningChecklist.getPayableAt();
				log.info("payableAt==> " + payableAt);
				docNumber = empJoiningChecklist.getDocNumber();
				log.info("docNumber==> " + docNumber);
				docAmount = empJoiningChecklist.getAmount();
				log.info("docAmount==> " + docAmount);
				cheqName = empJoiningChecklist.getCreatedByName();
				log.info("cheqName==> " + cheqName);
				docDate = empJoiningChecklist.getDocDate();
				log.info("docDate==> " + docDate);
				expDate = empJoiningChecklist.getDocExpiryDate();
				log.info("expDate==> " + expDate);
				loadBranchByBank();

				// -- to get joiningchecklist note details
				if (empJoiningChecklist.getEmpJoiningChecklistNoteList() != null
						&& !empJoiningChecklist.getEmpJoiningChecklistNoteList().isEmpty()) {
					for (EmpJoiningChecklistNote empJoiningChecklistNote : empJoiningChecklist
							.getEmpJoiningChecklistNoteList()) {
						noteId = empJoiningChecklistNote.getId();
						forwardTo = empJoiningChecklistNote.getForwardTo();
						log.info("forwardTo id=> " + forwardTo.getId());
						forwardFor = empJoiningChecklistNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						note = empJoiningChecklistNote.getNote();
						log.info("note=> " + note);
					}
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = forwardTo.getId();

					log.info("loginUserId=> " + loginUserId);
					log.info("noteUserId=> " + noteUserId);

				} else {
					log.error("joiningchecklist note not found");
				}
				/*
				 * //--to get joiningchecklist log details
				 * if(empJoiningChecklist.getEmpJoiningChecklistLogList() != null &&
				 * !empJoiningChecklist.getEmpJoiningChecklistLogList().isEmpty()) {
				 * empJoiningChecklistLogList =
				 * empJoiningChecklist.getEmpJoiningChecklistLogList(); //empJoiningChecklistLog
				 * = ; joiningChecklistStatus =
				 * empJoiningChecklistLogList.get(empJoiningChecklistLogList.size()-1).getStage(
				 * ); }else { log.error("joining check list log not found"); }
				 */
				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in editJoiningChecklist  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	public String viewJoiningChecklist() {
		log.info("<==== Starts IncrementBean.viewIncrement =====>");
		try {
			if (selectedJoiningChecklistDTO == null && selectedJoiningChecklistDTO.getCheckListId() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_ID_NOT_FOUND).getCode());
				return null;
			}
			Long checklistId = selectedJoiningChecklistDTO.getCheckListId();
			log.info("viewJoiningChecklist :: checklistId==> " + checklistId);
			String url = SERVER_URL + "/joiningChecklist/getchecklistbyid/" + checklistId;
			log.info("viewJoiningChecklist :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get joiningchecklist value
				empJoiningChecklist = mapper.readValue(jsonResponse, EmpJoiningChecklist.class);
				commentAndLogList =baseDTO.getCommentAndLogList();
				if (empJoiningChecklist.getPaymentMode().getCode().equals("DD")) {
					cheqFlag = false;
					ddFlag = true;
				} else if (empJoiningChecklist.getPaymentMode().getCode().equals("CHEQUE")) {
					cheqFlag = true;
					ddFlag = false;
				} else {
					cheqFlag = true;
					ddFlag = false;
				}

				cheqName = empJoiningChecklist.getCreatedByName();
				log.info("cheqName==> " + cheqName);
				docNumber = empJoiningChecklist.getDocNumber();
				log.info("docNumber==> " + docNumber);
				docAmount = empJoiningChecklist.getAmount();
				log.info("docAmount==> " + docAmount);
				cheqName = empJoiningChecklist.getCreatedByName();
				log.info("cheqName==> " + cheqName);
				docDate = empJoiningChecklist.getDocDate();
				log.info("docDate==> " + docDate);
				expDate = empJoiningChecklist.getDocExpiryDate();
				log.info("expDate==> " + expDate);

				appointmentNumber = empJoiningChecklist.getAppointmentOrderDetail().getRegistrationNumber();
				log.info("appointmentNumber==> " + appointmentNumber);

				// -- to get joiningchecklist note details
				if (empJoiningChecklist.getEmpJoiningChecklistNoteList() != null
						&& !empJoiningChecklist.getEmpJoiningChecklistNoteList().isEmpty()) {
					for (EmpJoiningChecklistNote empJoiningChecklistNote : empJoiningChecklist
							.getEmpJoiningChecklistNoteList()) {
						noteId = empJoiningChecklistNote.getId();
						forwardTo = empJoiningChecklistNote.getForwardTo();
						log.info("forwardTo id=> " + forwardTo.getId());
						forwardFor = empJoiningChecklistNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						note = empJoiningChecklistNote.getNote();
						log.info("note=> " + note);
					}
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = forwardTo.getId();

					log.info("loginUserId=> " + loginUserId);
					log.info("noteUserId=> " + noteUserId);
					if (loginUserId.equals(noteUserId)) {
						viewFlag = true;
					} else {
						viewFlag = false;
					}
				} else {
					log.error("joiningchecklist note not found");
				}
				// --to get joiningchecklist log details
				if (empJoiningChecklist.getEmpJoiningChecklistLogList() != null
						&& !empJoiningChecklist.getEmpJoiningChecklistLogList().isEmpty()) {
					empJoiningChecklistLogList = empJoiningChecklist.getEmpJoiningChecklistLogList();
					// empJoiningChecklistLog = ;
					joiningChecklistStatus = empJoiningChecklistLogList.get(empJoiningChecklistLogList.size() - 1)
							.getStage();
				} else {
					log.error("joining check list log not found");
				}
				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in viewIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> " + status);
			log.info("remarks==> " + remarks);
			log.info("logList id==> " + empJoiningChecklistLogList.get(0).getId());
			log.info("noteId==> " + noteId);
			log.info("forwardFor==> " + forwardFor);
			JoiningCheckedListDTO joiningCheckedListDTO = new JoiningCheckedListDTO();
			joiningCheckedListDTO.setLogList(empJoiningChecklistLogList);
			joiningCheckedListDTO.setForwardRemarks(remarks);
			joiningCheckedListDTO.setEmpJoiningChecklist(empJoiningChecklist);
			joiningCheckedListDTO.setNoteId(noteId);
			log.info("forward user id==> " + forwardTo.getId());
			joiningCheckedListDTO.setForwardUserId(forwardTo.getId());
			joiningCheckedListDTO.setForwardFor(forwardFor);
			if (status.equals(REJECT)) {
				joiningCheckedListDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals(APPROVED)) {
				joiningCheckedListDTO.setStatus(ApprovalStage.APPROVED);
			} else {
				joiningCheckedListDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			joiningCheckedListDTO.setNotificationId(selectedNotificationId);
			String url = SERVER_URL + "/joiningChecklist/updatejoiningChecklistlog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, joiningCheckedListDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				clearJoiningCheckList();
				loadLazyJoiningList();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.INCREMENT_LOG_INSERTED_SUCCESSFULLY).getCode());
				systemNotificationBean.loadTotalMessages();
				return LIST_PAGE;
			} else {
				log.info("Error while saving submitApprovedRejectedStatus :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_PAGE;
	}

}
