/**
 * 
 */
package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.operation.model.ExhibitionAddress;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Praveen
 *
 */
@Log4j2
@Scope("session")
@Service("exhibitionAddressBean")
public class ExhibitionAddressBean {
	
	@Autowired
	CommonDataService commonDataService;
	
	@Autowired
	CommonBean commonBean;

	@Getter
	@Setter
	private List<StateMaster> stateMasterList;

	@Getter
	@Setter
	private List<DistrictMaster> districtMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<TalukMaster> talukMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<CityMaster> cityMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<AreaMaster> areaMasterList = new ArrayList<>();

	@Getter
	@Setter
	ExhibitionAddress exhibitionAddress = new ExhibitionAddress();
	
	
	
	public void openExhibitionAddressDialog() {
		log.info("Open Exhibition Address Dialog");
		exhibitionAddress = new ExhibitionAddress();
		stateMasterList = commonDataService.loadStateList();
		stateMasterList.sort(Comparator.comparing(StateMaster::getName));
		/*
		 * districtMasterList = new ArrayList<>(); talukMasterList = new ArrayList<>();
		 * cityMasterList = new ArrayList<>(); areaMasterList = new ArrayList<>();
		 */
	}
	
	
	
	public void loadDistrict() {
		log.info("loadDistrict [" + exhibitionAddress.getStateMaster().getName() + "]");
		if (exhibitionAddress.getStateMaster() != null) {
			commonBean.loadActiveDistrictsByState(exhibitionAddress.getStateMaster().getId());
			districtMasterList = commonBean.getDistrictMasterList();
			talukMasterList.clear();
			cityMasterList.clear();
		}
	}

	public void loadTaluk() {
		log.info("loadTaluk [" + exhibitionAddress.getDistrictMaster() + "]");
		if (exhibitionAddress.getDistrictMaster() != null) {
			commonBean.loadActiveTaluksByDistrict(exhibitionAddress.getDistrictMaster().getId());
			talukMasterList = commonBean.getTalukMasterList();
		}
	}

	public void loadCity() {
		log.info("loadCity [" + exhibitionAddress.getDistrictMaster() + "]");
		if (exhibitionAddress.getDistrictMaster() != null) {
			cityMasterList =commonDataService.getCityByDistrictId(exhibitionAddress.getDistrictMaster().getId());
		}
	}

	public void loadTalukAndCity() {
		log.info("<<======= loadTalukAndCity [ " + exhibitionAddress.getDistrictMaster() + " ]");
		loadTaluk();
		loadCity();
	}

	public void loadArea() {
		log.info("loadArea [" + exhibitionAddress.getCityMaster() + "]");
		if (exhibitionAddress.getCityMaster() != null) {
			areaMasterList = commonDataService.loadActiveAreasByCity(exhibitionAddress.getCityMaster().getId());
		}

	}
	
	
	public String prepareExhibitionAddress(ExhibitionAddress exhibitionAddress) {
		String addressEnglish = "";

		if (exhibitionAddress != null) {
			if (exhibitionAddress.getLineOne() != null && StringUtils.isNotEmpty(exhibitionAddress.getLineOne())) {
				addressEnglish = addressEnglish + exhibitionAddress.getLineOne() + ",";
			}
			if (exhibitionAddress.getLineTwo() != null && StringUtils.isNotEmpty(exhibitionAddress.getLineTwo())) {
				addressEnglish = addressEnglish + exhibitionAddress.getLineTwo() + ",";
			}
			if (exhibitionAddress.getLineThree() != null
					&& StringUtils.isNotEmpty(exhibitionAddress.getLineThree())) {
				addressEnglish = addressEnglish + exhibitionAddress.getLineThree() + ",";
			}
			if (exhibitionAddress.getLandMark() != null && StringUtils.isNotEmpty(exhibitionAddress.getLandMark())) {
				addressEnglish = addressEnglish + exhibitionAddress.getLandMark() + ",";
			}
			if (exhibitionAddress.getCityMaster() != null && exhibitionAddress.getCityMaster().getName() != null) {
				addressEnglish = addressEnglish + exhibitionAddress.getCityMaster().getName() + ",";
			}
			if (exhibitionAddress.getTalukMaster() != null && exhibitionAddress.getTalukMaster().getName() != null) {
				addressEnglish = addressEnglish + exhibitionAddress.getTalukMaster().getName() + ",";
			}
			if (exhibitionAddress.getDistrictMaster() != null && exhibitionAddress.getDistrictMaster().getName() != null) {
				addressEnglish = addressEnglish + exhibitionAddress.getDistrictMaster().getName() + ",";
			}
			if (exhibitionAddress.getStateMaster() != null && exhibitionAddress.getStateMaster().getName() != null) {
				addressEnglish = addressEnglish + exhibitionAddress.getStateMaster().getName();
			}
			if (exhibitionAddress.getPostalCode() != null && !exhibitionAddress.getPostalCode().trim().isEmpty()) {
				if (addressEnglish.isEmpty()) {
					addressEnglish = exhibitionAddress.getPostalCode();
				} else {
					addressEnglish = addressEnglish + "-" + exhibitionAddress.getPostalCode();
				}
			}
		}
		return addressEnglish;
	}

	

}
