package in.gov.cooptex.admin.vehicle.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.vehicle.model.VehicleDeparture;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.VehicleDepartureSearchRequest;
import in.gov.cooptex.core.dto.VehicleDepartureSearchResponse;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.VehicleRegister;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("vehicleDepartureBean")
@Scope("session")
public class VehicleDepartureBean {

	private final String VEHICLE_DEPARTURE_CREATE_PAGE = "/pages/admin/vehicleManagement/createVehicleDeparture.xhtml?faces-redirect=true;";
	private final String VEHICLE_DEPARTURE_LIST_PAGE = "/pages/admin/vehicleManagement/listVehicle.xhtml?faces-redirect=true;";

	private String serverURL = null;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;
	
	@Getter
	@Setter
	EmployeeMaster empmaster;
	
	@Getter
	@Setter
	private VehicleDeparture vehicledeparture;

	@Getter
	@Setter
	private VehicleDeparture selectedvehicledeparture;

	@Getter
	@Setter
	private List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	private List<EmployeeMaster> driverList;

	@Getter
	@Setter
	private BaseDTO baseDTO = new BaseDTO();

	@Getter
	@Setter
	VehicleDepartureSearchResponse selectedvehicledepartureResponse;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	ObjectMapper mapper;

	
	@Getter
	@Setter
	private String jsonResponse;

	@Getter
	@Setter
	private List<VehicleRegister> vehicleRegisterList;

	@Getter
	@Setter
	private Date startTime;

	@Getter
	@Setter
	private Date endTime;

	@Autowired
	LoginBean loginBean;

	int vehicleRegisterListsize;

	@Autowired
	CommonDataService commondataservice;

	@Getter
	@Setter
	List<VehicleDeparture> vehicleDepartureList;

	@Getter
	@Setter
	LazyDataModel<VehicleDepartureSearchResponse> vehicledepartureResponseLazyList;

	@Getter
	@Setter
	private String vehicle_make;

	@Getter
	@Setter
	private String vehicle_variant;

	@Getter
	@Setter
	int totalRecords;

	@PostConstruct
	public void init() {
		log.info(" VehicleDepartureBean init");
		serverURL = AppUtil.getPortalServerURL();
		vehicledeparture = new VehicleDeparture();
		loadLazyVehicleDepartureList();
		loadValues();

	}
	
	public String showList() {
		log.info("<---------- Loading TapalDeliveryTypeBean showList list page---------->");
		loadValues();
		loadLazyVehicleDepartureList();
		return VEHICLE_DEPARTURE_LIST_PAGE;
	}

	private void loadValues() {
		try {

			loadVehicle();
			loadVehicleDriver();
			getAllEmployee();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
	}
	
	public String clear() {
		log.info("<---- Start TapalDeliveryTypeBean . clear ----->");
		loadLazyVehicleDepartureList();
		selectedvehicledeparture = new VehicleDeparture();
		log.info("<---- End TapalDeliveryTypeBean . clear ----->");

		return VEHICLE_DEPARTURE_LIST_PAGE;

	}
	
	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		vehicledeparture = new VehicleDeparture();
		return VEHICLE_DEPARTURE_LIST_PAGE;

	}

	public String showListPage() {

		log.info("<==== Starts IncomingTapal-showIncomingTapalListPage =====>");

		mapper = new ObjectMapper();

		boolean editButtonFlag = true;

		deleteButtonFlag = true;

		// tapalTypeDeliveryTypeList=commonDataService.loadTapalDeliveryType();
		vehicledeparture = new VehicleDeparture();
		selectedvehicledeparture = new VehicleDeparture();
		loadLazyVehicleDepartureList();
		log.info("<==== Ends IncomingTapal-showIncomingTapalListPage =====>");

		return VEHICLE_DEPARTURE_LIST_PAGE;
	}

	public void getAllEmployee() {
		log.info("... getAllEmployee called ....");
		employeeList = new ArrayList<EmployeeMaster>();
		try {

			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/vehiclemanagement/getallEmployee/";

			baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while getting All Employee....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadVehicle() {
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleTypeList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicleRegisterList = new ArrayList<VehicleRegister>();

			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadVehicle";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicleRegisterList != null) {
			vehicleRegisterListsize = vehicleRegisterList.size();
		} else {
			vehicleRegisterListsize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-vehicleTypeListSize ------- >>>> " + vehicleRegisterListsize);

	}

	private void loadVehicleDriver() {
		log.info("<<<< ----------Start loadVehicleDriver ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			driverList = new ArrayList<EmployeeMaster>();

			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadDriverList";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				driverList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------loadVehicleDriver-------end----- >>>> " + driverList);
	}

	public String submit() {
		log.info("...... vehicledeparture submit begin ....");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/create";
			vehicledeparture.setCreatedBy(loginBean.getUserDetailSession());
			vehicledeparture.setCreatedDate(new Date());
			empmaster=new EmployeeMaster();
			empmaster=loginBean.getEmployee();
			log.info("empmaster"+empmaster);
			vehicledeparture.setEntityMaster(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation());

			baseDTO = httpService.post(URL, vehicledeparture);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("vehicledeparture saved successfully");
					showListPage();
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating vehicledeparture" + e);
		}
		log.info("...... vehicledeparture submit ended ....");
		return VEHICLE_DEPARTURE_LIST_PAGE;
	}

	public void loadLazyVehicleDepartureList() {

		log.info("< --  Start VehicleDeparture Lazy Load -- > ");
		vehicledepartureResponseLazyList = new LazyDataModel<VehicleDepartureSearchResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<VehicleDepartureSearchResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						List<VehicleDepartureSearchResponse> data = mapper.readValue(jsonResponse,
								new TypeReference<List<VehicleDepartureSearchResponse>>() {
								});
						if (data == null) {
							log.info(" VehicleDepartureSearchResponse Failed to Deserialize");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}

						if (baseDTO.getStatusCode() == 0) {
							this.setRowCount(baseDTO.getTotalRecords());
							totalRecords = baseDTO.getTotalRecords();
							log.info(" Vehicle Departure Search  Successfully Completed");
						} else {
							log.error(
									" Vehicle Departure  Search Failed With status code :: " + baseDTO.getStatusCode());
							errorMap.notify(baseDTO.getStatusCode());
						}
						return data;
					}
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Vehicle Departure data :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(VehicleDepartureSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VehicleDepartureSearchResponse getRowData(String rowKey) {
				List<VehicleDepartureSearchResponse> responseList = (List<VehicleDepartureSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (VehicleDepartureSearchResponse res : responseList) {
					if (res.getId() == value.longValue()) {
						selectedvehicledepartureResponse = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public void onRowSelect(SelectEvent event) {
		selectedvehicledepartureResponse = ((VehicleDepartureSearchResponse) event.getObject());

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			VehicleDepartureSearchRequest request = new VehicleDepartureSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("vehicleRegistrarNo")) {
					log.info("vehicleRegistrarNo : " + value);
					request.setVehicleRegistrarNo(value);
				}

				if (entry.getKey().equals("driverName")) {
					log.info("employeeName : " + value);
					request.setDriverName(value);
				}
				if (entry.getKey().equals("destination")) {
					log.info("destination : " + value);
					request.setDestination(value);
				}

				if (entry.getKey().equals("employeeName")) {
					log.info("employeeName : " + value);
					request.setEmployeeName(value);
				}
				if (entry.getKey().equals("departureDate")) {
					log.info("departureDate : " + value);
					request.setDepartureDate(AppUtil.serverDateFormat(value));
				}

			}
			log.info("Search request data" + request);
			String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/vehiclemanagement/search";
			baseDTO = httpService.post(URL, request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Vehicle Departure  search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

}
