package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetCategoryMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("assetCategoryBean")
public class AssetCategoryMasterBean {

	private final String CREATE_ASSET_CATEGORY_PAGE = "/pages/masters/createAssetCategoryMaster.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listAssetCategoryMaster.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewAssetCategoryMaster.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ASSET_CATEGORY_URL = AppUtil.getPortalServerURL() + "/assetcategorymaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	AssetCategoryMaster assetCategory;
	@Getter
	@Setter
	List<AssetCategoryMaster> parentAssetCategoryMasterList = new ArrayList<AssetCategoryMaster>();

	@Getter
	@Setter
	List<AssetCategoryMaster> assetCategoryMasterList = new ArrayList<AssetCategoryMaster>();

	@Getter
	@Setter
	AssetCategoryMaster selectedAssetCategory;

	@Getter
	@Setter
	LazyDataModel<AssetCategoryMaster> lazyAssetCategoryList;

	List<AssetCategoryMaster> assetCategoryList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	private String pageHead = "";
	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;

	public String showAssetCategoryListPage() {
		log.info("<==== Starts Asset Category Bean.showAssetCategirtListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		assetCategoryMasterList = new ArrayList<>();
		assetCategory = new AssetCategoryMaster();
		selectedAssetCategory = new AssetCategoryMaster();
		parentAssetCategoryMasterList = loadAssetCategoryMasterList();
		
		loadLazyAssetCategoryList();

		log.info("<==== Ends AssetCategory Bean.showListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	private List<AssetCategoryMaster> loadAssetCategoryMasterList() {
		log.info("AssetCategoryMasterBean - loadAssetCategoryMasterList-->");
		List<AssetCategoryMaster> assetCategoryMasterValue = new ArrayList<>();
		try {
			String url;
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL()+ "/assetcategorymaster/getActiveAssetCategoryMaster";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				assetCategoryMasterValue = mapper.readValue(jsonResponse,
						new TypeReference<List<AssetCategoryMaster>>() {
						});
				log.info("loadAssetCategoryMasterList---------" + assetCategoryMasterValue.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside loadAssetCategoryMasterList method--", e);
		}
		return assetCategoryMasterValue;

	}

	public void loadLazyAssetCategoryList() {
		log.info("<===== Starts Asset Category Bean.load AssetCategory List ======>");
		lazyAssetCategoryList = new LazyDataModel<AssetCategoryMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<AssetCategoryMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = ASSET_CATEGORY_URL + "/getAssetCategoryList";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get asset category list");
					if (response != null && response.getStatusCode() == 0) {
						log.info("get asset category list" + response.getTotalRecords());
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						assetCategoryList = mapper.readValue(jsonResponse,
								new TypeReference<List<AssetCategoryMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyAssetCategory List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return assetCategoryList;
			}

			@Override
			public Object getRowKey(AssetCategoryMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AssetCategoryMaster getRowData(String rowKey) {
				try {
					for (AssetCategoryMaster assetCategory : assetCategoryList) {
						if (assetCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedAssetCategory = assetCategory;
							return assetCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends AssetCategory Bean.loadLazyAssetCategory List ======>");
	}

	public String showAssetCategoryListPageAction() {
		log.info("<===== Starts Asset Category Bean.showAssetCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedAssetCategory == null || selectedAssetCategory.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Asset Category called..");
				statusButtonFlag = false;
				pageHead = "Add";
				assetCategory = new AssetCategoryMaster();
				parentAssetCategoryMasterList = loadAssetCategoryMasterList();
				return CREATE_ASSET_CATEGORY_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit Asset Category called..");
				statusButtonFlag = true;
				pageHead = "Edit";
				String url = ASSET_CATEGORY_URL + "/get/" + selectedAssetCategory.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					assetCategory = mapper.readValue(jsonResponse, new TypeReference<AssetCategoryMaster>() {
					});
					return CREATE_ASSET_CATEGORY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete asset category called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view Asset category called..");
				String viewurl = ASSET_CATEGORY_URL + "/get/" + selectedAssetCategory.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					assetCategory = mapper.readValue(jsonResponse, new TypeReference<AssetCategoryMaster>() {
					});
					return INPUT_FORM_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAssetCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends AssetCategory Bean.showAssetCategoryListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {

		try {

			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedAssetCategory == null || selectedAssetCategory.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				assetCategory.setCreatedBy(loginBean.getUserDetailSession());
				assetCategory.setCreatedDate(new Date());
				if (assetCategory.getActiveStatus() == null) {
					assetCategory.setActiveStatus(true);
				}
			} else if (action.equalsIgnoreCase("EDIT")) {
				assetCategory.setModifiedBy(loginBean.getUserDetailSession());
				assetCategory.setModifiedDate(new Date());
				if (assetCategory.getActiveStatus() == null) {
					errorMap.notify(ErrorDescription.ASSETCATEGORY_ACTIVESTATUS_EMPTY.getCode());
					return null;
				}
			}

			String url = ASSET_CATEGORY_URL + "/create";
			BaseDTO response = httpService.post(url, assetCategory);
			if (response != null && response.getStatusCode() == 0) {
				log.info("Asset category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.ASSETCATEGORY_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ASSETCATEGORY_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends Asset category Bean.saveOrUpdate =======>");
		return showAssetCategoryListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Asset Category Bean.onRowSelect ========>" + event);
		selectedAssetCategory = ((AssetCategoryMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends Asset Category Bean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts Asset Category Bean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(ASSET_CATEGORY_URL + "/delete/" + selectedAssetCategory.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ASSETCATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAssetCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete asset category ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends Asset Category Bean.deleteConfirm ===========>");
		return showAssetCategoryListPage();
	}

	public String clear() {
		showAssetCategoryListPage();
		return INPUT_FORM_LIST_URL;
	}

}
