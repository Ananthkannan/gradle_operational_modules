package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TemporaryEngagement;
import in.gov.cooptex.core.model.TemporaryEngagementDetails;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("temporaryEngagementBean")
@Scope("session")
public class TemporaryEngagementBean {

	ObjectMapper mapper;
	String jsonResponse;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Autowired
	CommonDataService commonDataService;
	@Getter
	@Setter
	int totalRecords = 0;

	@Setter
	@Getter
	String entitytip = "No regions selected";

	@Setter
	@Getter
	String designationtip = "No designations selected";

	@Setter
	@Getter
	List<StateMaster> stateList;
	@Setter
	@Getter
	List<Department> departmentList;
	@Setter
	@Getter
	List<EntityMaster> regionList;

	@Getter
	@Setter
	List<Long> selectionYearList;

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	Set<String> salesFromMonthYearList;

	@Getter
	@Setter
	Set<String> salesToMonthYearList;

	@Getter
	@Setter
	LazyDataModel<TemporaryEngagement> temporaryEngagementLazyList;

	@Getter
	@Setter
	TemporaryEngagement selectedTemporaryEngagement;

	@Getter
	@Setter
	TemporaryEngagementDetails selectedTemporaryEngagementDetail = new TemporaryEngagementDetails();

	@Getter
	@Setter
	TemporaryEngagement temporaryEngagement;

	@Getter
	@Setter
	String salesFromMonthYear;

	@Getter
	@Setter
	String salesToMonthYear;

	@Getter
	@Setter
	Integer salesFromMonth;

	@Getter
	@Setter
	Integer salesFromYear;

	@Getter
	@Setter
	Integer salesToMonth;

	@Getter
	@Setter
	Integer salesToYear;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;

	@Setter
	@Getter
	Boolean generateButtonFlag = false;

	@Setter
	@Getter
	Boolean showSalesPeriodPanel = false;
	
	
	
	@Getter
	@Setter
	String action;

	public String loadTemporaryEngagementList() {
		log.info("<-----Loading appointment order list page----->");
		mapper = new ObjectMapper();
		selectedTemporaryEngagement = new TemporaryEngagement();
		action = "LIST";
		addButtonFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		approveButtonFlag = false;
		loadMasterValues();
		loadTemporaryEngagemenLazy();
		return "/pages/personnelHR/temporaryEngagement/listTemporaryEngagement.xhtml?faces-redirect=true";
	}
	
	
	
	

	public void loadTemporaryEngagemenLazy() {
		temporaryEngagementLazyList = new LazyDataModel<TemporaryEngagement>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<TemporaryEngagement> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Response retrurn null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<TemporaryEngagement> temporaryEngagementList = mapper.readValue(jsonResponse,
							new TypeReference<List<TemporaryEngagement>>() {
							});
					if (temporaryEngagementList == null) {
						log.info(" Temporary Engagement list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Temporary Engagement list search Successfully Completed");
					} else {
						log.error(" Temporary Engagement list search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return temporaryEngagementList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Temporary Engagement :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(TemporaryEngagement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TemporaryEngagement getRowData(String rowKey) {
				List<TemporaryEngagement> responseList = (List<TemporaryEngagement>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (TemporaryEngagement res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedTemporaryEngagement = res;
						return res;
					}
				}
				return null;
			}
		};
	}
	
	
	

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			TemporaryEngagement request = new TemporaryEngagement();
			
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("referenceNumber")) {
					log.info("Reference Number: " + value);
					searchFilters.put("referenceNumber", value);
				}
				if (filter.getKey().equals("state.name")) {
					log.info("State Name : " + value);
					searchFilters.put("stateName", value);
				}
				if (filter.getKey().equals("entity")) {
					log.info("Entity Name: " + value);
					searchFilters.put("entityName", value);
				}
				if (filter.getKey().equals("department.name")) {
					log.info("department: " + value);
					searchFilters.put("department", value);
				}
				if (filter.getKey().equals("selectionYear")) {
					log.info("Selection Year: " + value);
					searchFilters.put("selectionYear", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("Created Date: " + value);
					searchFilters.put("createdDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}
			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/temporaryengagement/getalllazy", request);

		} catch (Exception e) {
			log.error("Exception Occured in temporary engagement load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String temporaryEngagementListAction() {
		try {
			if (selectedTemporaryEngagement == null) {
				errorMap.notify(ErrorDescription.SELECT_TEMPORARY_ENGAGEMENT.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("DELETE")) {
				if (selectedTemporaryEngagement.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_TEMPORARY_ENGAGEMENT_CANNOT_DELETE.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAppointmentDelete').show();");
			} else {
				log.info("<----Loading  page.....---->" + selectedTemporaryEngagement.getId());
				temporaryEngagement = new TemporaryEngagement();
				BaseDTO response = httpService
						.get(SERVER_URL + "/temporaryengagement/get/" + selectedTemporaryEngagement.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					temporaryEngagement = mapper.readValue(jsonResponse, new TypeReference<TemporaryEngagement>() {
					});
					if (action.equalsIgnoreCase("VIEW")) {
						return "/pages/personnelHR/temporaryEngagement/viewTemporaryEngagement.xhtml?faces-redirect=true";
					} else if (action.equalsIgnoreCase("APPROVE")) {
						return "/pages/personnelHR/temporaryEngagement/approveTemporaryEngagement.xhtml?faces-redirect=true";
					}
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured in temporary engagement ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String addTemporaryEngagement() {
		temporaryEngagement = new TemporaryEngagement();

		salesFromMonthYear = "";
		salesToMonthYear = "";
		prepareSalesMonthYear();
		generateButtonFlag = false;
		showSalesPeriodPanel = false;
		selectionYearList = Util.generateNextFiftyYears();
		return "/pages/personnelHR/temporaryEngagement/createTemporaryEngagement.xhtml?faces-redirect=true";
	}

	public String generateTemporaryEngagement() {
		if (temporaryEngagement.getDepartment().getName().equalsIgnoreCase("Marketing")) {
			salesFromMonth = AppUtil.getMonthNumber(salesFromMonthYear.split("-")[0]);
			salesFromYear = Integer.valueOf(salesFromMonthYear.split("-")[1]);

			salesToMonth = AppUtil.getMonthNumber(salesToMonthYear.split("-")[0]);
			salesToYear = Integer.valueOf(salesToMonthYear.split("-")[1]);

			Date salesFrom = new GregorianCalendar(salesFromYear, salesFromMonth - 1, 01).getTime();
			temporaryEngagement.setSalesPeriodFrom(salesFrom);

			Date firstDay = new GregorianCalendar(salesToYear, salesToMonth - 1, 1).getTime();
			Date salesTo = AppUtil.getLastDateOfMonth(firstDay);
			temporaryEngagement.setSalesPeriodTo(salesTo);

			if (temporaryEngagement.getSalesPeriodFrom().after(temporaryEngagement.getSalesPeriodTo())) {
				log.info("incorrect date value");
				Util.addWarn("Sales period to should be greater than sales period from date");
				return null;
			}
		}

		log.info("... Generate Temporary engagement List called ...." + temporaryEngagement);
		try {

			mapper = new ObjectMapper();
			BaseDTO response = httpService.post(SERVER_URL + "/temporaryengagement/generate", temporaryEngagement);
			if (response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				generateButtonFlag = false;
			} else {
				generateButtonFlag = true;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			List<TemporaryEngagementDetails> temporaryEngagements = mapper.readValue(jsonResponse,
					new TypeReference<List<TemporaryEngagementDetails>>() {
					});
			temporaryEngagement.setTemporaryEngagementDetails(temporaryEngagements);
		} catch (Exception e) {
			log.error("Exception occured while Generating temporary Engagements...", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	public String saveTemporaryEngagement() {
		if (temporaryEngagement == null) {
			Util.addWarn("Temporary Engagement is empty");
			return null;
		}
		if (temporaryEngagement.getTemporaryEngagementDetails() == null) {
			Util.addWarn("Generate and submit. StaffEligibility Statement"
					+ temporaryEngagement.getDepartment().getName() + " is empty");
			return null;
		}
		try {
			mapper = new ObjectMapper();
			temporaryEngagement.setStatus("InProgress");

			if (!action.equals("APPROVE")) {
				temporaryEngagement.getTemporaryEngagementDetails().forEach(tempEngDet -> {
					tempEngDet.setApproveCount(tempEngDet.getShortageCount());

					// tempEngDet.getTemporaryEngagementShowroomDetails()

				});

			}
			BaseDTO response = httpService.post(SERVER_URL + "/temporaryengagement/add", temporaryEngagement);
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.TEMPORARY_ENGAGEMENT_INSTERTED_SUCCESSFULLY).getCode());
				//errorMap.notify(response.getStatusCode());
				return loadTemporaryEngagementList();
			} else {
				errorMap.notify(response.getStatusCode());
				return null;
			}

		} catch (Exception e) {

			log.error("Exception occured while saving temporary engagement....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}

	public void loadEntityList() {
		log.info("... Load Entity called ...." + temporaryEngagement.getState().getId());
		temporaryEngagement.setRegionList(null);
		try {
			mapper = new ObjectMapper();

			BaseDTO response = httpService
					.get(SERVER_URL + "/entitymaster/getallactiveregion/" + temporaryEngagement.getState().getId());
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			regionList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading entity....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void changeEntity() {
		entitytip = "";
		if (temporaryEngagement.getTemporaryEngagementDetails() != null)
			for (EntityMaster entity : regionList) {

				if (temporaryEngagement.getRegionList().contains(entity.getId())) {
					if (entitytip != "") {
						entitytip = entitytip + ", ";
					}
					entitytip = entitytip + entity.getName();
				}
			}
	}

	public void loadDesignationList() {
		log.info("... Load Designation List called ...." + temporaryEngagement.getDepartment().getId());
		temporaryEngagement.setDesignation(null);
		if (temporaryEngagement.getDepartment().getName().equalsIgnoreCase("Marketing")) {
			showSalesPeriodPanel = true;
		} else {
			showSalesPeriodPanel = false;
		}
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.get(SERVER_URL + "/designation/getalldesignation/" + temporaryEngagement.getDepartment().getId());
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});

			log.info("designationList---->" + designationList.size());
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void changeDesignations() {
		designationtip = "";
		if (temporaryEngagement.getDesignation() != null)
			for (Designation desig : temporaryEngagement.getDesignation()) {
				if (designationtip != "") {
					designationtip = designationtip + ", ";
				}
				designationtip = designationtip + desig.getName();
			}
	}

	public void prepareSalesMonthYear() {
		String sDate = "01/01/2016";
		Date startDate;
		try {
			startDate = new SimpleDateFormat("dd/MM/yyyy").parse(sDate);
			salesFromMonthYearList = AppUtil.generatePastMnthYear(startDate);
			salesToMonthYearList = AppUtil.generatePastMnthYear(startDate);
		} catch (ParseException e) {
			log.error("Error while generation sales month year period--->", e);
		}
		log.info("RetailProductionPlanBean.preparePlanFromMonthYear Method Completed : " + salesFromMonthYearList);
	}

	public void loadMasterValues() {
		stateList = commonDataService.getStateList();
		departmentList = commonDataService.getDepartment();
		regionList = commonDataService.loadRegionalOffice();
	}

	public String clearFilter() {
		temporaryEngagement = new TemporaryEngagement();
		designationList = new ArrayList<>();
		regionList = new ArrayList<>();
		salesToMonthYear = null;
		salesFromMonthYear = null;
		showSalesPeriodPanel = false;
		entitytip = "";
		designationtip = "";
		approveButtonFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedTemporaryEngagement = ((TemporaryEngagement) event.getObject());

		viewButtonFlag = true;
		addButtonFlag = true;
		if (selectedTemporaryEngagement.getStatus().equalsIgnoreCase("Approved")) {
			deleteFlag = false;
			approveButtonFlag = false;
			viewButtonFlag = true;
		} else if (selectedTemporaryEngagement.getStatus().equalsIgnoreCase("Rejected")) {
			approveButtonFlag = false;
			deleteFlag = true;
			viewButtonFlag = true;
		} else if (selectedTemporaryEngagement.getStatus().equalsIgnoreCase("InProgress")) {
			approveButtonFlag = true;
			deleteFlag = true;
			viewButtonFlag = true;
		}
	}

	public String viewShowroomDetails(TemporaryEngagementDetails detail) {
		log.info("inside drill down chart............" + detail);
		try {
			if (detail == null) {
				Util.addWarn("Please Select One Temporary engagement");
				return null;
			} else {
				selectedTemporaryEngagementDetail = detail;
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('showroomdetail').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String approveTemporaryEngagement() {
		try {
			log.info("Approve Temporary engagement...... called");
			if (selectedTemporaryEngagement == null) {
				AppUtil.addWarn("Please select one Temporary Engagement");
				return null;
			}
			if (selectedTemporaryEngagement.getStatus().equals("Approved")) {
				AppUtil.addWarn("Temporary Engagement already approved");
				return null;
			}
			if (selectedTemporaryEngagement.getStatus().equals("Rejected")) {
				AppUtil.addWarn("Temporary Engagement rejected once cannot be approved");
				return null;
			}
			temporaryEngagement = selectedTemporaryEngagement;
			log.info("Server url...." + SERVER_URL + "/temporaryengagement/get/" + selectedTemporaryEngagement.getId());
			BaseDTO response = httpService
					.get(SERVER_URL + "/temporaryengagement/get/" + selectedTemporaryEngagement.getId());

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			temporaryEngagement = mapper.readValue(jsonResponse, new TypeReference<TemporaryEngagement>() {
			});

		} catch (Exception e) {
			log.error("Exception occured while approving roster", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return "/pages/personnelHR/temporaryEngagement/approveTemporaryEngagement.xhtml?faces-redirect=true";
	}

	public String beforeTemporaryApproval() {
		log.info("before TemporaryEngagement...." + selectedTemporaryEngagement.getId());
		try {
			if (selectedTemporaryEngagement == null) {
				Util.addWarn("Please select one Temporary Engagement");
				return null;
			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTemporaryApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String confirmApproveTemporaryEngagement() {
		log.info("confirm approval....." + temporaryEngagement.getStatus());
		log.info(
				"confirm approval....." + temporaryEngagement.getTemporaryEngagementDetails().get(0).getApproveCount());
		try {
			
			if(temporaryEngagement != null && temporaryEngagement.getTemporaryEngagementDetails() != null) {
				List<TemporaryEngagementDetails> temporaryEngagementDetails = temporaryEngagement.getTemporaryEngagementDetails();
				for(TemporaryEngagementDetails temEngagementDetails : temporaryEngagementDetails) {
					Integer shortageCount = temEngagementDetails.getShortageCount();
					log.info("confirmApproveTemporaryEngagement :: shortageCount==> "+shortageCount);
					Integer approveCount = temEngagementDetails.getApproveCount();
					log.info("confirmApproveTemporaryEngagement :: approveCount==> "+approveCount);
					if(shortageCount < approveCount) {
						AppUtil.addError("Approve count should be less than shortage count");
						return null;
					}
				}
			}else {
				log.error("Temporary Engagement details not found");
			}
			mapper = new ObjectMapper();
			BaseDTO response = httpService.post(SERVER_URL + "/temporaryengagement/add", temporaryEngagement);
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while approving temporary engagement....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadTemporaryEngagementList();
	}

	public String cancel() {
		selectedTemporaryEngagement = new TemporaryEngagement();
		approveButtonFlag = false;
		viewButtonFlag = false;
		deleteFlag = false;
		entitytip = "";
		designationtip = "";
		return loadTemporaryEngagementList();
	}

	public String deleteTemporaryEngagement() {
		try {
			if (selectedTemporaryEngagement == null) {
				AppUtil.addWarn("Please select one Temporary Engagement");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTemoraryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting .... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	public String confirmTemporaryEngagement() {
		log.info("... delete Roster called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.get(SERVER_URL + "/temporaryengagement/delete/" + selectedTemporaryEngagement.getId());
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadTemporaryEngagementList();
	}

	public String currencyFormat(Double value) {
		if (value < 1000) {
			if (value == 0)
				return "0.00";
			return format("###.00", value);
		} else {
			double hundreds = value % 1000;
			int other = (int) (value / 1000);
			return format(",##", other) + ',' + format("000.00", hundreds);
		}
	}

	private static String format(String pattern, Object value) {
		return new DecimalFormat(pattern).format(value);
	}

}
