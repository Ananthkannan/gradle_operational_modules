package in.gov.cooptex.dashboard.ui.bean;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.ui.CommonBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("portalDashboardBean")
@Scope("session")
@Log4j2
public class PortalDashboardBean extends CommonBean {

	@Setter
	@Getter
	private String popupData = "Please wait .... ";

	public void updatePopupContent() {
		log.info("Inside updatePopupContent()");
		this.popupData = new java.util.Date().toString();
	}
}
