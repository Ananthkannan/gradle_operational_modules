/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.LoomTypeMaster;
import in.gov.cooptex.core.model.SalutationMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.personnel.rest.util.Util;
import in.gov.cooptex.weavers.model.SocietyEnrollment;
import in.gov.cooptex.weavers.model.SocietyEnrollmentFiles;
import in.gov.cooptex.weavers.model.SocietyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("societyEnrollmentBean")
@Scope("session")
@Log4j2
public class SocietyEnrollmentBean {

	@Autowired
	AppPreference appPreference;

	private final String CREATE_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/createSocietyEnrollment.xhtml?faces-redirect=true;";
	private final String LIST_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/listSocietyEnrollment.xhtml?faces-redirect=true;";
	private final String VIEW_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/viewSocietyEnrollment.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	
	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	List<SocietyRegistrationRequest> societyRequestList = null;

	@Getter
	@Setter
	SocietyEnrollment societyEnrollment;

	@Getter
	@Setter
	List<LoomTypeMaster> loomTypeList = null;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	String societyAddress, societyPresidentAddress;

	@Getter
	@Setter
	List<SocietyEnrollmentFiles> societEnrollmentFilesList;

	@Getter
	@Setter
	Boolean societyAddressFlag = false, societyPresidentAddressFlag = false;

	@Getter
	@Setter
	private UploadedFile file1 = null, file2 = null;

	@Getter
	@Setter
	private String file1Path = "", file2Path = "", fileName = "";

	@Getter
	@Setter
	SocietyEnrollmentFiles selectedFile = new SocietyEnrollmentFiles();

	@Getter
	@Setter
	int total = 12, attached = 0, remaining = 0;

	@Getter
	@Setter
	LazyDataModel<SocietyEnrollment> lazySocietyEnrollmentList;

	List<SocietyEnrollment> societyEnrollmentList;

	@Getter
	@Setter
	List<SalutationMaster> salutationList;

	String SOCIETY_ENROLLMENT_URL = "";

	@Getter
	@Setter
	SocietyEnrollment selectedSocietyEnrollment;

	private StreamedContent file;
	
	@Getter
	@Setter
	SocietyEnrollmentFiles socEnrollmtDelFileObj=new SocietyEnrollmentFiles();

	public String showSocietyEnrollmentListPage() {
		log.info("<==== Starts societyEnrollmentBean-showSocietyEnrollmentListPage =====>");
		mapper = new ObjectMapper();
		selectedSocietyEnrollment = new SocietyEnrollment();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		action = "";
		SOCIETY_ENROLLMENT_URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/societyenrollment";
		loadLazySocietyEnrollmentList();

		log.info("<==== Ends societyEnrollmentBean-showSocietyEnrollmentListPage =====>");
		return LIST_SOCIETY_PAGE;
	}

	/**
	 * @return Society Enrollment List Page
	 */
	public String societyEnrollmentListPageAction() {
		log.info("<========inside societyEnrollmentBean.showSocietyEnrollmentListPage=======>");
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedSocietyEnrollment == null || selectedSocietyEnrollment.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}

			societyRequestList = commonDataService.getAllsocietyRegReqOffinalApproved();
			loadAttachmentList();
			if (action.equalsIgnoreCase("Create")) {
				log.info("create Society Enrollment called..");
				societyEnrollment = new SocietyEnrollment();
				societyAddress = "";
				societyPresidentAddress = "";
				attached = 0;
				remaining = 0;
				addressMasterBean.openAddressDialog();
				salutationList = commonDataService.getAllSalutations();
//				loadActiveLoomType();
				return CREATE_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("view Society Enrollment called..");
				log.info("edit societyEnrollment called..");
				String url = SOCIETY_ENROLLMENT_URL + "/getbyid/" + selectedSocietyEnrollment.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					societyEnrollment = mapper.readValue(jsonResponse, new TypeReference<SocietyEnrollment>() {
					});
					if (societyEnrollment != null && societyEnrollment.getSocietyAddress() != null) {
						societyAddress = addressMasterBean.prepareAddress(societyEnrollment.getSocietyAddress());
					}
					if (societyEnrollment != null && societyEnrollment.getPresidentAddress() != null) {
						societyPresidentAddress = addressMasterBean
								.prepareAddress(societyEnrollment.getPresidentAddress());
					}
					attached = 0;
					remaining = 0;

					if (societyEnrollment != null && societyEnrollment.getSocietyEnrollmentFilesList() != null)
						attached = societyEnrollment.getSocietyEnrollmentFilesList().size();

					remaining = total - attached;
					log.info("Total files ..." + total + "\t Attached..." + attached + "\t Remaining..." + remaining);
					return VIEW_SOCIETY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("Edit")) {
				societyRequestList = new ArrayList<>();
				if (selectedSocietyEnrollment.getEnrollmentStatus() != null
						&& selectedSocietyEnrollment.getEnrollmentStatus().equalsIgnoreCase("SUBMITTED")) {
					errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_CANNOT_EDIT_SUBMITTED_RECORD.getCode());
					return null;
				}
				log.info("Edit Society Enrollment called..");
//				addressMasterBean.setAddressMaster(profileMaster.getAddressMaster());
//				addressMasterBean.openAddressDialogWithAction(action);
				log.info("edit societyEnrollment called..");
				salutationList = commonDataService.getAllSalutations();
				String url = SOCIETY_ENROLLMENT_URL + "/getbyid/" + selectedSocietyEnrollment.getId();

				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					societyEnrollment = mapper.readValue(jsonResponse, new TypeReference<SocietyEnrollment>() {
					});
					if (societyEnrollment != null && societyEnrollment.getSocietyAddress() != null) {
						societyAddress = addressMasterBean.prepareAddress(societyEnrollment.getSocietyAddress());
					}
					if (societyEnrollment != null && societyEnrollment.getPresidentAddress() != null) {
						societyPresidentAddress = addressMasterBean
								.prepareAddress(societyEnrollment.getPresidentAddress());
					}
					societyRequestList.add(societyEnrollment.getSocietyRegistrationRequest());
					attached = 0;
					remaining = 0;
					if (societyEnrollment != null && societyEnrollment.getSocietyEnrollmentFilesList() != null) {
						attached = societyEnrollment.getSocietyEnrollmentFilesList().size();
						remaining = total - attached;
						societEnrollmentFilesList = prepareSocietyEnrollmentFiles(
								societyEnrollment.getSocietyEnrollmentFilesList());
					}
					return CREATE_SOCIETY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Society Enrollment called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyEnrollmentDelete').show();");
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void onSocietyChange() {
		log.info("<=== Starts societyEnrollmentBean.onSocietyChange ====>"
				+ societyEnrollment.getSocietyRegistrationRequest());
		if (societyEnrollment.getSocietyRegistrationRequest() == null) {
		}
		log.info("<=== Ends societyEnrollmentBean.onSocietyChange ====>");
	}

	/*
	 * public void loadActiveLoomType() {
	 * log.info("Inside loadActiveLoomType Method"); loomTypeList = new
	 * ArrayList<>(); BaseDTO baseDTO = new BaseDTO(); String url = SERVER_URL +
	 * "/loomtype/getAllActiveLoomType"; try {
	 * log.info("URL===LOOM_TYPE_LIST_URL================>" + url); baseDTO =
	 * httpService.get(url); log.info("<=========== after URL =======>" + baseDTO);
	 * if (baseDTO != null) { mapper = new ObjectMapper();
	 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	 * String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContents()); loomTypeList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<LoomTypeMaster>>() {
	 * }); log.info("=============activeloomTypeList=====================" +
	 * loomTypeList.size()); } } catch (Exception e) { log.error("Error Occured ::",
	 * e); } }
	 */

	public String saveOrUpdate(String type) {
		log.info("<== Starts societyEnrollmentBean.saveorupdate.===>");
		try {
			BaseDTO response = null;

			Validate.objectNotNull(societyEnrollment.getSocietyRegistrationRequest(),
					ErrorDescription.SOCIETY_ENROLLMENT_SOCIETY_NULL);
			Validate.objectNotNull(societyEnrollment.getSocietyAddress(),
					ErrorDescription.SOCIETY_ENROLLMENT_SOCIETY_ADDRESS_REQUIRED);
			Validate.objectNotNull(societyEnrollment.getPresidentName(),
					ErrorDescription.SOCIETY_ENROLLMENT_SOCIETY_PRESIDENT_NAME__NULL);
			Validate.objectNotNull(societyEnrollment.getPresidentAddress(),
					ErrorDescription.SOCIETY_ENROLLMENT_PRESIDENT_ADDRESS_REQUIRED);
			Validate.objectNotNull(societyEnrollment.getTotalMembers(),
					ErrorDescription.SOCIETY_ENROLLMENT_TOTAL_NUMBERS__NULL);
			Validate.objectNotNull(societyEnrollment.getDdNumber(),
					ErrorDescription.SOCIETY_ENROLLMENT_DEMAND_DRAFT_NUMBER_NULL);
			Validate.objectNotNull(societyEnrollment.getDdDate(),
					ErrorDescription.SOCIETY_ENROLLMENT_DEMAND_DRAFT_DATE_NULL);
			Validate.objectNotNull(societyEnrollment.getDdAmount(),
					ErrorDescription.SOCIETY_ENROLLMENT_DEMAND_DRAFT_AMOUNT);
			Validate.objectNotNull(societyEnrollment.getSalutation(),
					ErrorDescription.SOCIETY_ENROLLMENT_SALUTATION_REQUIRED);
			Validate.objectNotNull(societyEnrollment.getSalutation().getSalutationId(),
					ErrorDescription.SOCIETY_ENROLLMENT_SALUTATION_REQUIRED);

			if (!documentUploadValidation()) {
				return null;
			}

			String url = SOCIETY_ENROLLMENT_URL + "/saveorupdate";
			if (type.equalsIgnoreCase("submit"))
				societyEnrollment.setEnrollmentStatus("SUBMITTED");
			else
				societyEnrollment.setEnrollmentStatus("INITIATED");

			if (action.equalsIgnoreCase("Create")) {
				societyEnrollment.setCreatedDate(new Date());
				societyEnrollment.setCreatedBy(loginBean.getUserDetailSession());
				societyEnrollment.setSocietyEnrollmentFilesList(societEnrollmentFilesList);
				response = httpService.post(url, societyEnrollment);
				if (response != null && response.getStatusCode() == 0) {
					if (type.equalsIgnoreCase("submit"))
						errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_UPDATE_SUCCESS.getCode());
					else
						errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_SAVE_SUCCESS.getCode());
					return showSocietyEnrollmentListPage();
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					log.error("Response............" + response);
				}
			} else if (action.equalsIgnoreCase("Edit")) {
				societyEnrollment.setSocietyEnrollmentFilesList(societEnrollmentFilesList);
				response = httpService.post(url, societyEnrollment);
				if (response != null && response.getStatusCode() == 0) {
					if (type.equalsIgnoreCase("submit"))
						errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_UPDATE_SUCCESS.getCode());
					else
						errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_SAVE_SUCCESS.getCode());
					return showSocietyEnrollmentListPage();
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					log.error("Response............" + response);
				}
			}
		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured in saveorupdate method...........", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<== Ends societyEnrollmentBean.saveorupdate.===>");
		return null;
	}

	// SocietyEnrollment File upload validation
	private boolean documentUploadValidation() {
		boolean valid = true;
		for (SocietyEnrollmentFiles files : societEnrollmentFilesList) {
			log.info("documentUploadValidation file------" + files.getFileName());
			if (files.getFilePath() == null) {
				valid = false;
				AppUtil.addWarn("Please upload " + files.getFileName() + " file");
				return valid;
			}
		}
		return valid;
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		log.info("societyAddressFlag ::\t" + societyAddressFlag + "\t societyPresidentAddressFlag:::"
				+ societyPresidentAddressFlag);
		if (societyAddressFlag) {
			societyEnrollment.setSocietyAddress(addressMasterBean.getAddressMaster());
			societyAddress = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		}
		if (societyPresidentAddressFlag) {
			societyEnrollment.setPresidentAddress(addressMasterBean.getAddressMaster());
			societyPresidentAddress = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("societyEnrollmentForm");
		societyAddressFlag = false;
		societyPresidentAddressFlag = false;
	}

	public void openSocietyPresidentAddress() {
		log.info(action + " Inside openSocietyPresidentAddress " + addressMasterBean.getAddressMaster());
		if (action.equalsIgnoreCase("Create")) {
			addressMasterBean.setAddressMaster(new AddressMaster());
		}
		if (societyEnrollment.getPresidentAddress() != null
				&& societyEnrollment.getPresidentAddress().getStateMaster() != null) {
			addressMasterBean.setAddressMaster(societyEnrollment.getPresidentAddress());
		} else {
			addressMasterBean.openAddressDialog();
		}
		societyAddressFlag = false;
		societyPresidentAddressFlag = true;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').show();");
	}

	public void openSocietyAddress() {
		log.info(action + " Inside openSocietyAddress " + addressMasterBean.getAddressMaster());
		if (action.equalsIgnoreCase("Create")) {
			addressMasterBean.setAddressMaster(new AddressMaster());
		}

		if (societyEnrollment.getSocietyAddress() != null
				&& societyEnrollment.getSocietyAddress().getStateMaster() != null) {
			addressMasterBean.setAddressMaster(societyEnrollment.getSocietyAddress());
		} else {
			addressMasterBean.openAddressDialog();
		}
		societyAddressFlag = true;
		societyPresidentAddressFlag = false;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').show();");
		context.update("addressDialogForm");
	}

	public void loadAttachmentList() {
		log.info("<==== Starts societyEnrollmentBean.loadAttachmentList==========>");
		/*
		 * societEnrollmentFilesList = new ArrayList<>();
		 * societEnrollmentFilesList.add(new SocietyEnrollmentFiles(1l,
		 * true,"Form No.16 duly filled by the society to admit the society as Co-optex member."
		 * )); societEnrollmentFilesList.add(new SocietyEnrollmentFiles(2l,
		 * true,"List of members attested by the deputy director of Handlooms and textiles."
		 * )); societEnrollmentFilesList.add(new
		 * SocietyEnrollmentFiles(3l,true,"List of board members of the society."));
		 * societEnrollmentFilesList.add(new
		 * SocietyEnrollmentFiles(4l,true,"Declaration of society."));
		 * societEnrollmentFilesList.add(new SocietyEnrollmentFiles(5l,
		 * true,"By-law of the society duly attested by the Deputy Director of Handlooms and Textiles."
		 * )); societEnrollmentFilesList.add(new
		 * SocietyEnrollmentFiles(6l,true,"D.D. for Rs.550/- for share capital."));
		 * societEnrollmentFilesList.add(new SocietyEnrollmentFiles(7l,
		 * true,"Society Resolution to join as member in Co-optex."));
		 * societEnrollmentFilesList.add(new SocietyEnrollmentFiles(8l,
		 * true,"Requisition of the society to admit as member in Co-optex."));
		 * societEnrollmentFilesList.add(new SocietyEnrollmentFiles(9l,
		 * true,"Recommendation letter from concerned Deputy Director of Handlooms and Textiles."
		 * )); societEnrollmentFilesList.add(new SocietyEnrollmentFiles(10l,
		 * true,"Proceedings issued by the Deputy Director of Handlooms & Textiles, to commence the business of the society."
		 * )); societEnrollmentFilesList.add(new SocietyEnrollmentFiles(11l,
		 * true,"Acknowledgement, for the Powerloom issued by the Regional Office of the Textiles Commissioner, Coimbatore."
		 * ));
		 */

		societEnrollmentFilesList = new ArrayList<>();
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"Form No.16 duly filled by the society to admit the society as Co-optex member."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"List of members attested by the deputy director of Handlooms and textiles."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true, "List of board members of the society."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true, "Declaration of society."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"By-law of the society duly attested by the Deputy Director of Handlooms and Textiles."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true, "D.D. for Rs.550/- for share capital."));
		societEnrollmentFilesList
				.add(new SocietyEnrollmentFiles(true, "Society Resolution to join as member in Co-optex."));
		societEnrollmentFilesList
				.add(new SocietyEnrollmentFiles(true, "Requisition of the society to admit as member in Co-optex."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"Recommendation letter from concerned Deputy Director of Handlooms and Textiles."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"Certificate of Registration issued by the Deputy Director of Handlooms & Textiles."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"Proceedings issued by the Deputy Director of Handlooms & Textiles, to commence the business of the society."));
		societEnrollmentFilesList.add(new SocietyEnrollmentFiles(true,
				"Acknowledgement for the Powerloom issued by the Regional Office of the Textiles Commissioner, Coimbatore."));

		log.info("<==== Ends societyEnrollmentBean.loadAttachmentList==========>");
	}

	public String handleFileUpload(FileUploadEvent event) {
		log.info("<<===  File Upload method called" + selectedFile.getFileName());
		try {
			selectedFile = (SocietyEnrollmentFiles) event.getComponent().getAttributes().get("object");
			UploadedFile file = event.getFile();
//			String fileType = file.getFileName();
//			log.info("fileType: "+fileType);
			String type = FilenameUtils.getExtension(file.getFileName());
			log.info(" File Type :: " + type);
//			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
//					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")) {
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
//				log.info("<<====  error type in file upload");
//				return null;
//			}
			log.info(" before validFileType :: ");
			boolean validFileType = AppUtil.isValidFileType(type,
					new String[] { "doc","docx", "pdf", "xlsx", "xls" });
			log.info(" after validFileType :: ");
			if (!validFileType) {
				log.info(" after validFileType inside if  condition:: ");
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOC_PDF_XLS_FORMAT).getErrorCode());
				return null;
			}
			log.info(" after validFileType outside if  condition:: ");
//			long size = file.getSize();
			//			size = size /  (1024*1024);
			float size=file.getSize();
			log.info("File size >>>>> " + file.getSize());
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			log.info("File size >>>>> " + size);
			long fileSize = Long.valueOf(commonDataService.getAppKeyValue("DISCIPLINARY_ACTION_UPLOAD_FILE_MAX_SIZE_IN_MB"));
			if (size > fileSize) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
				return null;
			}
			if (size > 0) {
				String billcopyPathName = commonDataService.getAppKeyValue("SOCIETY_ENROLLMENT_PATH");
				String filePath = Util.fileUpload(billcopyPathName, file);
				selectedFile.setFilePath(filePath);
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Relieving Document is uploaded successfully");
			}

		} catch (Exception e) {
			log.info("<<==  Error in file upload " + e);
		}
		attached = 0;
		remaining = 0;
		for (SocietyEnrollmentFiles file : societEnrollmentFilesList) {
			if (file.getFilePath() != null && !file.getFilePath().isEmpty())
				attached++;
			else
				remaining++;
		}
		return null;
	}

	public void loadLazySocietyEnrollmentList() {
		log.info("<===== Starts societyEnrollmentBean.loadLazySocietyEnrollmentList ======>");

		lazySocietyEnrollmentList = new LazyDataModel<SocietyEnrollment>() {
			private static final long serialVersionUID = 5221554353084853593L;

			@Override
			public List<SocietyEnrollment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SOCIETY_ENROLLMENT_URL + "/getlazyenrollmentlist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						societyEnrollmentList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyEnrollment>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return societyEnrollmentList;
			}

			@Override
			public Object getRowKey(SocietyEnrollment res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyEnrollment getRowData(String rowKey) {
				for (SocietyEnrollment societyEnrollment : societyEnrollmentList) {
					if (societyEnrollment.getId().equals(Long.valueOf(rowKey))) {
						selectedSocietyEnrollment = societyEnrollment;
						return societyEnrollment;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends societyEnrollmentBean.loadLazySocietyEnrollmentList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts societyEnrollmentBean.onRowSelect ========>" + event);
		selectedSocietyEnrollment = ((SocietyEnrollment) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		if (selectedSocietyEnrollment.getEnrollmentStatus().equals(ApprovalStage.SUBMITTED)) {
			deleteButtonFlag = true;
		} else if (selectedSocietyEnrollment.getEnrollmentStatus().equals(ApprovalStage.REJECTED)
				|| selectedSocietyEnrollment.getEnrollmentStatus().equals(ApprovalStage.INITIATED)) {
			deleteButtonFlag = false;
		}

		viewButtonFlag = false;
		log.info("<===Ends societyEnrollmentBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts societyEnrollmentBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SOCIETY_ENROLLMENT_URL + "/deletebyid/" + selectedSocietyEnrollment.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_DELETED_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyEnrollmentDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete SocietyEnrollment ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends societyEnrollmentBean.deleteConfirm ===========>");
		return showSocietyEnrollmentListPage();
	}

	public void validateTotalMember(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validate method");
		Long member = (Long) value;
		if (member != null && (member < 25 || member > 999)) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Should be in the range of 25 - 999");
			context.addMessage(comp.getClientId(context), message);
		}

	}

	public void validatePresidentName(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validatePresidentName method");
		String text = (String) value;
		if (text == null || (text != null && text.length() < 3 || text.length() > 50)) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Length should be in 3 to 50 character");
			context.addMessage(comp.getClientId(context), message);
		}

	}

	public void pathClear(SocietyEnrollmentFiles file) {
		log.info("<== pathClear called..==>");
		if (file != null && file.getFilePath() != null && !file.getFilePath().isEmpty()) {
			file.setFilePath(null);
			attached--;
			remaining++;
		}

	}

	public List<SocietyEnrollmentFiles> prepareSocietyEnrollmentFiles(List<SocietyEnrollmentFiles> filesList) {
		log.info("<=== Starts prepareSocietyEnrollmentFiles==>");
		List<SocietyEnrollmentFiles> enrollmentFilesList = new ArrayList<>();
		enrollmentFilesList.addAll(filesList);
		for (SocietyEnrollmentFiles file : societEnrollmentFilesList) {
			boolean flag = false;
			for (SocietyEnrollmentFiles f : filesList) {
				log.info("file.getFileName() " + file.getFileName());
				log.info("f.getFileName() " + f.getFileName());
				if (file.getFileName().equalsIgnoreCase(f.getFileName())) {
					flag = true;
					break;
				}
			}
			if (!flag)
				enrollmentFilesList.add(file);
		}
		return enrollmentFilesList;
	}

	public void getDownloadFile(String path) {
		try {

			byte[] downloadName = null;
			try {
				File file = new File(path);

				downloadName = new byte[(int) file.length()];

				log.info("downloadName==>" + downloadName);

				FileInputStream fis = new FileInputStream(file);
				fis.read(downloadName); // read file into bytes[]
				fis.close();
			} catch (Exception e) {
				log.error("convertImage error==>" + e);
			}
			/*
			 * log.info(" Value :: " + value); if (value == null || value == null) {
			 * errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode()); return;
			 * }
			 */
			if (path.contains("uploaded")) {
				String filePathValue[] = path.split("uploaded/");
				fileName = filePathValue[1];
			}
			log.info(" FIle Name :: " + fileName);
			String ext = FilenameUtils.getExtension(fileName);
			if (ext.contains("jpg") || ext.contains("jpeg") || ext.contains("png")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "image/jpg", fileName);
			} else if (ext.contains("pdf")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "pdf", fileName);
			} else if (ext.contains("doc")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			} else if (ext.contains("docx")) {
				file = new DefaultStreamedContent(new ByteArrayInputStream(downloadName), "doc", fileName);
			}
			return;
		} catch (Exception e) {
			log.error(" Error in while getting uploaded file", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}
	}

	public StreamedContent getFile() {
		return file;
	}
	
	public void delete(SocietyEnrollmentFiles file) {
		socEnrollmtDelFileObj=file;
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmsocEnrollmtfileDelete').show()");
	}
}
