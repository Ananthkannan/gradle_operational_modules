package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.ProcurementCostingRequestDto;
import in.gov.cooptex.operation.dto.ProcurementCostingResponseDto;
import in.gov.cooptex.operation.model.ProcurementCosting;
import in.gov.cooptex.operation.model.ProcurementCostingNote;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("procurementCostingBean")
@Scope("session")
@Data
public class ProcurementCostingBean extends CommonBean implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 8295605454734837955L;

	private final String CREATE_PROCUREMENT_COSTING_PAGE = "/pages/operation/procurement/createProcurementCosting.xhtml?faces-redirect=true;";
	private final String PROCUREMENT_COSTING_LIST_URL = "/pages/operation/procurement/listProcurementCosting.xhtml?faces-redirect=true;";
	private final String PROCUREMENT_COSTING_VIEW_URL = "/pages/operation/procurement/viewProcurementCosting.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<ProcurementCosting> procurementCostingLazyList;

	@Getter
	@Setter
	List<ProcurementCosting> procurementCostingList = new ArrayList<ProcurementCosting>();

	@Getter
	@Setter
	private ProcurementCosting procurementCosting = new ProcurementCosting();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList = new ArrayList<ProductGroupMaster>();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList = new ArrayList<ProductDesignMaster>();

	@Getter
	@Setter
	ProcurementCostingResponseDto selectedProcurementCostingResponseDto;

	@Getter
	@Setter
	ProcurementCostingResponseDto procurementCostingResponseDto;

	@Getter
	@Setter
	LazyDataModel<ProcurementCostingResponseDto> lazyProcurementCostingList;

	@Getter
	@Setter
	List<ProcurementCostingResponseDto> procurementCostingResponseDtoList = new ArrayList<ProcurementCostingResponseDto>();

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Long productGroupCode;

	@Getter
	@Setter
	private ProductVarietyMaster productVarietyCode;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	Long productDesignCode;

	@Getter
	@Setter
	ProductVarietyMaster productVariety;

	@Getter
	@Setter
	ProductCategory productCategoryCode;

	@Getter
	@Setter
	Long forwardTo;

	@Getter
	@Setter
	ProcurementCostingRequestDto procurementCostingRequestDto = new ProcurementCostingRequestDto();

	@Getter
	@Setter
	ProcurementCostingNote procurementCostingNote;

	@Getter
	@Setter
	private Double warpWastagePercetnage, weftWastagePercentage, warpYarnRate, weftYarnRate,
			numberOfUnits, weightPerUnit, warpYarnRequired, weftYarnRequired,
			costWarpYarn, costWeftYarn, warpWastageAmount, weftWastageAmount,
			weavingWages, profitPercetage, totalRate, totalRateTemp, costWarpYarnTemp,
			costWeftYarnTemp, ratePerUnit, preparatoryCharges, sticker, knotting,
			totalPurchasePrice, box, polythin_cover, otherChargesAmount, printcharges,
			covercharges;

	@Getter
	@Setter
	String otherChargesName;

	@Getter
	@Setter
	private Date periodFrom;

	@Getter
	@Setter
	private Date periodTo;

	@Getter
	@Setter
	private Long version;

	@Getter
	@Setter
	private List<UserMaster> userMasters = new ArrayList<UserMaster>();

	@Getter
	@Setter
	private Double warpWastageAmountTemp, warpWastageTemp, ratePerUnitTemp, weftWastageAmountTemp;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	boolean diseabledForwardTo;

	@Getter
	@Setter
	boolean renderforwardfor;

	@Getter
	@Setter
	boolean diseabledForwardFor;

	@Getter
	@Setter
	boolean approveRender = true, forwardForRender = true, rejectRender = true, forwardToRender = true,
			renderedForwardForAndTo = true;

	@Getter
	@Setter
	boolean diseabledCatgory = false, diseabledGroup = false, diseabledVariety = false, diseabledDesign = false;

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingDtoList = new ArrayList<>();

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingResponseDtoListCharges = new ArrayList<>();

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingResponseDtoListremove = new ArrayList<>();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<ProcurementCostingResponseDto> viewLogResponseList;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@PostConstruct
	public void init() {
		procurementCostingRequestDto = null;
		productGroupMasterList = null;
		productCategoryList = null;
		productVarietyMasterList = null;
		productDesignMasterList = null;
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		productCategoryCode = null;
		productVarietyCode = null;
		procurementCosting = new ProcurementCosting();
//		costWarpYarn = 0.0;
//		costWeftYarn = 0.0;
//		warpWastageAmount = 0.0;
//		weftWastageAmount = 0.0;
//		weavingWages = 0.0;
//		profitPercetage = 0.0;
//		totalRate = 0.0;
//		totalRateTemp = 0.0;
//		costWarpYarnTemp = 0.0;
//		costWeftYarnTemp = 0.0;
//		ratePerUnit = 0.0;
//		preparatoryCharges = 0.0;
//		sticker = 0.0;
//		knotting = 0.0;
//		totalPurchasePrice = 0.0;
		procurementCostingDtoList = new ArrayList<>();
		procurementCostingResponseDtoListremove = new ArrayList<>();
		procurementCostingResponseDto = new ProcurementCostingResponseDto();

	}

	public String showProcurementCostingListPage() {
		log.info("<==== Starts ProcurementCostingBean.showProcurementCostingListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		diseabledCatgory = false;
		diseabledGroup = false;
		diseabledVariety = false;
		diseabledDesign = false;
		selectedProcurementCostingResponseDto = null;
		productCategoryList = commonDataService.loadProductCategories();
		loadLazyGlAccountCategoryList();
		log.info("<==== Ends ProcurementCostingBean.showProcurementCostingListPage =====>");
		return PROCUREMENT_COSTING_LIST_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ProcurementCostingBean.onRowSelect ========>" + event);
		selectedProcurementCostingResponseDto = ((ProcurementCostingResponseDto) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends ProcurementCostingBean.onRowSelect ========>");
	}

	public String addProcurementCostingPage() {
		log.info("<==== Starts ProcurementCostingBean.addProcurementCostingListPage =====>");
		mapper = new ObjectMapper();
		init();
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROCUREMENT_COSTING.toString());
		procurementCostingRequestDto = new ProcurementCostingRequestDto();
		procurementCostingResponseDto = new ProcurementCostingResponseDto();
		productCategoryList = commonDataService.loadProductCategories();
		loadEmployeeLoggedInUserDetails();
		log.info("<==== Ends ProcurementCostingBean.showProcurementCostingListPage =====>");
		return CREATE_PROCUREMENT_COSTING_PAGE;
	}

	public void loadProductVarietyList() {

		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList =====>");
		try {
			String requestPath = SERVER_URL + "/productvariety/getproductvarietybygroupid/"
					+ procurementCostingRequestDto.getProductGroupCode();
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productVarietyMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductVarietyMaster>>() {
				});
				log.info("VariteyList with Size of : " + productVarietyMasterList.size());

			}
		} catch (Exception e) {
			log.error("Error Product Group List --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList  =====>");

	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadProductDesignList() {
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList =====>");
		try {
			String url = SERVER_URL + "/productDesign/getdesign/" + productVarietyCode.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignMaster>>() {
						});
			}
			log.info("Product Design Master List Size" + productDesignMasterList.size());

		} catch (Exception e) {
			log.error("Error Product Design List --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList  =====>");
	}

	public void loadProductGroupList() {
		log.info("<==== Starts ProcurementCostingBean.loadProductGroupList =====>");
		try {

			String requestPath = SERVER_URL + "/group/getproductgroupbycategoryid/" + productCategoryCode.getId();
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productGroupMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductGroupMaster>>() {
				});
				log.info("GroupList with Size of : " + productGroupMasterList.size());

			}
		} catch (Exception e) {
			log.error("Error Product Group List --==>", e);
		}
		log.info(
				"<userMasters = commonDataService.loadForwardToUsers();==== Starts ProcurementCostingBean.loadProductGroupList =====>");

	}

	public String showProcurementCostingListPageAction() {
		log.info("<===== Starts ProcurementCostingBean.showProcurementCostingListPageAction ===========>" + action);

		try {

			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create ProcurementCosting called..");
				userMasters = commonDataService
						.loadForwardToUsersByFeature(AppFeatureEnum.PROCUREMENT_COSTING.toString());
				productCategoryList = commonDataService.loadProductCategories();
				return CREATE_PROCUREMENT_COSTING_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit ProcurementCosting called..");
				if(selectedProcurementCostingResponseDto==null || selectedProcurementCostingResponseDto.getProcurementId()==null) {
					errorMap.notify(ErrorDescription.SELECT_CIRCLE_MASTER.getCode());
					return null;
				}

				if (selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("REJECTED")) {

					String url = SERVER_URL + "/procurementcost/getbyid/"
							+ selectedProcurementCostingResponseDto.getProcurementId();
					BaseDTO response = httpService.get(url);

					if (response != null && response.getStatusCode() == 0) {
						procurementCostingRequestDto = new ProcurementCostingRequestDto();
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						procurementCostingResponseDto = mapper.readValue(jsonResponse,
								new TypeReference<ProcurementCostingResponseDto>() {
								});

					 callOtherChargesDetails(
								selectedProcurementCostingResponseDto.getProcurementId());

						diseabledCatgory = true;
						diseabledGroup = true;
						diseabledVariety = true;
						diseabledDesign = true;

						setValuesToCostingDetails(procurementCostingResponseDto);

						return CREATE_PROCUREMENT_COSTING_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					}
				} else {
					errorMap.notify(ErrorDescription.PROCUREMENT_CAN_NOT_EDIT.getCode());
					// errorMap.notify(MastersErrorCode.PROCUREMENT_COSTING_SAVE_SUCESS);
				}

			} else if (action.equalsIgnoreCase("VIEW")) {
				if(selectedProcurementCostingResponseDto==null || selectedProcurementCostingResponseDto.getProcurementId()==null) {
					errorMap.notify(ErrorDescription.SELECT_CIRCLE_MASTER.getCode());
					return null;
				}
				productVarietyCode = new ProductVarietyMaster();
				productDesignMaster = new ProductDesignMaster();
				log.info("view ProcurementCosting called..");
				String viewurl = SERVER_URL + "/procurementcost/getbyid/"
						+ selectedProcurementCostingResponseDto.getProcurementId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					procurementCostingResponseDto = mapper.readValue(jsonResponse,
							new TypeReference<ProcurementCostingResponseDto>() {
							});

					String employeeDatajsonResponses = mapper.writeValueAsString(viewresponse.getResponseContents());
					viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
							new TypeReference<List<ProcurementCostingResponseDto>>() {
							});

					callOtherChargesDetails(selectedProcurementCostingResponseDto.getProcurementId());

					setPriviligesAccessButton(procurementCostingResponseDto, selectedProcurementCostingResponseDto);

					userMasters = commonDataService
							.loadForwardToUsersByFeature(AppFeatureEnum.PROCUREMENT_COSTING.toString());

					return PROCUREMENT_COSTING_VIEW_URL;
				}

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showProcurementCostingListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends ProcurementCostingBean.showProcurementCostingListPageAction ===========>" + action);
		return null;
	}

	public void setPriviligesAccessButton(ProcurementCostingResponseDto procurementCostingResponseDto2,
			ProcurementCostingResponseDto selectedProcurementCostingResponseDto2) {
		if (procurementCostingResponseDto.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())
				&& procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval().equals(true)
				&& selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("REJECTED")) {
			log.info("<====== Inside showProcurementCostingListPageAction REJECTED clause  ===>");
			approveRender = false;
			rejectRender = false;
			renderedForwardForAndTo = false;

		} else if (procurementCostingResponseDto.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())
				&& procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval().equals(false)
				&& selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("REJECTED")) {
			log.info("<====== Inside showProcurementCostingListPageAction REJECTED clause  ===>");
			approveRender = false;
			rejectRender = false;
			renderedForwardForAndTo = false;

		} else if (procurementCostingResponseDto.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())
				&& procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval().equals(false)) {

			renderedForwardForAndTo = false;
			approveRender = true;
			rejectRender = true;

		} else if (procurementCostingResponseDto.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())
				&& procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval().equals(true)
				&& selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("FINAL_APPROVED")) {
			log.info("<====== Inside showProcurementCostingListPageAction FINAL_APPROVED clause  ===>");
			approveRender = false;
			rejectRender = false;
			renderedForwardForAndTo = false;

		} else if (procurementCostingResponseDto.getForwardTo().getId().equals(loginBean.getUserDetailSession().getId())
				&& procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval().equals(true)
				&& selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("SUBMITTED")) {
			log.info("<====== Inside showProcurementCostingListPageAction Submitted clause  ===>");

			diseabledForwardFor = true;
			diseabledForwardTo = true;
			approveRender = true;
			rejectRender = true;

		}

		else if (procurementCostingResponseDto.getForwardTo().getId() != (loginBean.getUserDetailSession().getId())) {
			log.info("<====== Inside showProcurementCostingListPageAction Forward To Unassigned User clause  ===>");
			approveRender = false;
			rejectRender = false;
			renderedForwardForAndTo = false;

		}
		
		 if (procurementCostingResponseDto.getForwardTo().getId().equals((loginBean.getUserDetailSession().getId()))
				 && !procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval()) {
			 renderedForwardForAndTo = true;
		 }
		 
		 if(procurementCostingResponseDto.getForwardTo().getId().equals((loginBean.getUserDetailSession().getId()))) {
			 approveRender = true;
			 rejectRender = true;
		 }

	}

	public void setValuesToCostingDetails(ProcurementCostingResponseDto procurementCostingResponseDto2) {
		setProcurementCosting(procurementCostingResponseDto.getProcurementCosting());
		setProductVarietyCode(procurementCostingResponseDto.getProductVarietyMaster());
		setProductCategoryCode(procurementCostingResponseDto.getProductCategory());

		procurementCostingRequestDto
				.setProductDesignCode(procurementCostingResponseDto.getProductDesignMaster().getId());

		procurementCostingRequestDto.setProductGroupCode(procurementCostingResponseDto.getProductGroupMaster().getId());
		setCostWeftYarn(procurementCostingResponseDto.getProcurementCosting().getCostWeftYarn());
		setWarpWastageAmount(procurementCostingResponseDto.getProcurementCosting().getWarpWastageAmount());
		setWeftWastageAmount(procurementCostingResponseDto.getProcurementCosting().getWeftWastageAmount());
		setWeavingWages(procurementCostingResponseDto.getProcurementCosting().getWeavingWages());
		setProfitPercetage(procurementCostingResponseDto.getProcurementCosting().getProfitPercetage());
		setTotalRate(procurementCostingResponseDto.getProcurementCosting().getTotalRate());
		setRatePerUnit(procurementCostingResponseDto.getProcurementCosting().getRatePerUnit());
		setPreparatoryCharges(procurementCostingResponseDto.getProcurementCosting().getPreparatoryCharges());
		setSticker(procurementCostingResponseDto.getProcurementCosting().getSticker());
		setKnotting(procurementCostingResponseDto.getProcurementCosting().getKnotting());
		setTotalPurchasePrice(procurementCostingResponseDto.getProcurementCosting().getTotalPurchasePrice());
		procurementCostingRequestDto.setForwardTo(procurementCostingResponseDto.getForwardTo());
		procurementCostingRequestDto.setForwardFor(procurementCostingResponseDto.getForwardFor());

		setBox(procurementCostingResponseDto.getProcurementCosting().getBoxCharge());
		setPolythin_cover(procurementCostingResponseDto.getProcurementCosting().getPolythinCoverCharge());
		setPrintcharges(procurementCostingResponseDto.getProcurementCosting().getPrintingCharge());
		setCovercharges(procurementCostingResponseDto.getProcurementCosting().getCoverCharge());
		loadAllSelectOneMenuBox();

	}

	public ProcurementCostingResponseDto callOtherChargesDetails(Long procurementId) {

		String viewurl = SERVER_URL + "/procurementcost/getchargesbycostingid/"
				+ selectedProcurementCostingResponseDto.getProcurementId();

		try {

			BaseDTO viewresponse = httpService.get(viewurl);
			if (viewresponse != null && viewresponse.getStatusCode() == 0) {
				String employeeDatajsonResponses = mapper.writeValueAsString(viewresponse.getResponseContents());

				procurementCostingDtoList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<ProcurementCostingRequestDto>>() {
						});
				// procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
			}
		} catch (IOException e) {
			log.error("Exception occured in callOtherChargesDetails ...", e);

		}
		return null;
	}

	public void loadAllSelectOneMenuBox() {
		log.info("<====== Inside loadAllSelectOneMenuBox For Edit Options  ===>");
		productCategoryList = commonDataService.loadProductCategories();
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROCUREMENT_COSTING.toString());
		loadProductGroupList();
		loadProductVarietyList();
		loadProductDesignList();
	}

	public void loadLazyGlAccountCategoryList() {
		log.info("<===== Starts GlAccountCategoryBean.loadLazyGlAccountCategoryList ======>");
		lazyProcurementCostingList = new LazyDataModel<ProcurementCostingResponseDto>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProcurementCostingResponseDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/procurementcost/getprocurementcosting";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						procurementCostingResponseDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProcurementCostingResponseDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGlAccountCategoryList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return procurementCostingResponseDtoList;
			}

			@Override
			public Object getRowKey(ProcurementCostingResponseDto res) {
				return res != null ? res.getProcurementId() : null;
			}

			@Override
			public ProcurementCostingResponseDto getRowData(String rowKey) {
				try {
					for (ProcurementCostingResponseDto glAccountCategory : procurementCostingResponseDtoList) {
						if (glAccountCategory.getProcurementId().equals(Long.valueOf(rowKey))) {
							selectedProcurementCostingResponseDto = glAccountCategory;
							return glAccountCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ProcurementCostingBean.loadLazyGlAccountCategoryList ======>");
	}

	public String approveorReject() {

		log.info("<===== Starts ProcurementCostingBean.approveorReject ======>");
		try {
			if(!procurementCostingResponseDto.getProcurementCostingNote().getFinalApproval() && 
			(selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("APPROVED")|| selectedProcurementCostingResponseDto.getStage().equalsIgnoreCase("SUBMITTED"))) {
			procurementCostingResponseDto.setStage("APPROVED");
			}else {
				procurementCostingResponseDto.setStage("FINAL_APPROVED");
			}
			// procurementCostingResponseDto.getProcurementCostingNote().setFinalApproval(true);
			String url = SERVER_URL + "/procurementcost/approveReject";

			BaseDTO response = httpService.post(url, procurementCostingResponseDto);

			if (response != null && response.getStatusCode() == 0) {
				log.info("<====== Inside approveorReject response caluse  ===>");
				return PROCUREMENT_COSTING_LIST_URL;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.info("<===== Exception Occured in ProcurementCostingBean.approveorReject ======>" + e);

		}
		return CREATE_PROCUREMENT_COSTING_PAGE;

	}

	public String reject() {

		log.info("<===== Starts ProcurementCostingBean.Reject ======>");
		try {
			procurementCostingResponseDto.setStage("REJECTED");
			String url = SERVER_URL + "/procurementcost/approveReject";
			BaseDTO response = httpService.post(url, procurementCostingResponseDto);

			if (response != null && response.getStatusCode() == 0) {
				log.info("<====== Inside Reject Method response caluse  ===>");
				return PROCUREMENT_COSTING_LIST_URL;

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.info("<===== Exception Occured in ProcurementCostingBean.Reject ======>" + e);

		}
		return null;

	}

	public String saveProcurementCosting() {
		log.info("<===== Starts ProcurementCostingBean.saveProcurementCosting ======>" + procurementCostingRequestDto);
		try {
            
			if(procurementCostingRequestDto.getNote()==null || procurementCostingRequestDto.getNote()=="") {
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getCode());
				return null;
			}
			procurementCosting.setBoxCharge(box);
			procurementCosting.setPolythinCoverCharge(polythin_cover);
			procurementCosting.setPrintingCharge(printcharges);
			procurementCosting.setCoverCharge(covercharges);
			procurementCostingRequestDto.setProcurementCosting(procurementCosting);
			if (procurementCosting.getId() != null) {
				procurementCostingRequestDto.setStage(selectedProcurementCostingResponseDto.getStage());
			}
			if (procurementCostingDtoList.size() > 0) {
				procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
			}

			procurementCostingRequestDto.setProudctVarietyMaster(productVarietyCode);

			String url = SERVER_URL + "/procurementcost/save";

			BaseDTO response = httpService.post(url, procurementCostingRequestDto);
			if (response != null && response.getStatusCode() == 0) {
				log.info("<===== Inside ProcurementCostingBean.saveProcurementCosting Reponse Clause  ======>");
				errorMap.notify(ErrorDescription.PROCUREMENT_SAVED_SUCCESS.getCode());
			} else {
				errorMap.notify(ErrorDescription.PROCUREMENT_SAVED_SUCCESS.getCode());
			}
		} catch (Exception e) {
			log.info("<===== Exception Occured in ProcurementCostingBean.saveProcurementCosting ======>" + e);

		}
		log.info("<===== Ends ProcurementCostingBean.saveProcurementCosting ======>");
		return PROCUREMENT_COSTING_LIST_URL;

	}

	public void calculateFormula() {

		try {

			log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

			costWarpYarn = 0.0;
			costWeftYarn = 0.0;
			warpWastageAmount = 0.0;
			weftWastageAmount = 0.0;
			totalRate = 0.0;
			ratePerUnit = 0.0;
			totalPurchasePrice = 0.0;

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRate() > 0
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() > 0) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula costWarpYarn =====>");
				costWarpYarn = costWarpYarn + (double) Math
						.round(procurementCosting.getWarpYarnRate() * procurementCosting.getWarpYarnRequired());
				costWarpYarnTemp = costWarpYarn;
				procurementCosting.setCostWarpYarn(costWarpYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWarpYarn =====>" + costWarpYarn);

			}
			if (procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRate() > 0
					&& procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftYarnRequired() > 0) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula costWeftYarn =====>");
				costWeftYarn = costWeftYarn + (double) Math
						.round(procurementCosting.getWeftYarnRate() * procurementCosting.getWeftYarnRequired());
				costWeftYarnTemp = costWeftYarn;
				procurementCosting.setCostWeftYarn(costWeftYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWeftYarn =====>" + costWeftYarn);
			}
			if (procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula warpWastageAmount =====>");
				warpWastageAmount = warpWastageAmount + (double) Math.round(
						(procurementCosting.getWarpYarnRequired() * procurementCosting.getWarpWastagePercetnage()) / 100
								* procurementCosting.getWarpYarnRate());
				warpWastageAmountTemp = warpWastageAmount;
				procurementCosting.setWarpWastageAmount(warpWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula warpWastageAmount =====>"
						+ warpWastageAmount);
			}

			if (procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula weftWastageAmount =====>");
				weftWastageAmount = weftWastageAmount + (double) Math.round(
						(procurementCosting.getWeftYarnRequired() * procurementCosting.getWeftWastagePercentage()) / 100
								* procurementCosting.getWeftYarnRate());
				weftWastageAmountTemp = weftWastageAmount;
				procurementCosting.setWeftWastageAmount(weftWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula weftWastageAmount =====>"
						+ weftWastageAmount);
			}

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null
					&& profitPercetage != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula totalRate =====>");
				totalRate = totalRate + (double) Math
						.round(((costWarpYarnTemp + costWeftYarnTemp + totalRate + weftWastageAmountTemp + weavingWages)
								* (profitPercetage) / 100 + costWarpYarnTemp + costWeftYarnTemp + warpWastageAmountTemp
								+ weftWastageAmountTemp + weavingWages));
				totalRateTemp = totalRate;
				procurementCosting.setTotalRate(totalRate);
				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalRate);
			}
			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null && profitPercetage != null
					&& procurementCosting.getNumberOfUnits() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula ratePerUnit =====>");
				ratePerUnit = ratePerUnit + (double) Math.round(totalRateTemp / procurementCosting.getNumberOfUnits());
				ratePerUnitTemp = ratePerUnit;
				procurementCosting.setRatePerUnit(ratePerUnit);
				procurementCosting.setWeavingWages((double) Math.round(weavingWages));
				procurementCosting.setProfitPercetage(profitPercetage);
				log.info("<==== END ProcurementCostingBean.calculateFormula ratePerUnit =====>" + totalRate);
			}
			if (preparatoryCharges != null && sticker != null && knotting != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula totalPurchasePrice =====>");
				totalPurchasePrice = totalPurchasePrice + (double) Math
						.round(ratePerUnit + preparatoryCharges + sticker + knotting + box + polythin_cover);
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				procurementCosting.setPreparatoryCharges((double) Math.round(preparatoryCharges));
				procurementCosting.setSticker((double) Math.round(sticker));
				procurementCosting.setKnotting((double) Math.round(knotting));

				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalPurchasePrice);
			}

		} catch (Exception e) {
			log.error("Error Ajax Calculation --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

	}

	public void addOtherCharges() {

		ProcurementCostingRequestDto procurementcostingdto = new ProcurementCostingRequestDto();
		try {
			if (otherChargesName != null && otherChargesAmount != 0) {
				log.info("<==== Starts ProcurementCostingBean.addOtherCharges Inside If =====>" + otherChargesName
						+ otherChargesAmount);
				procurementcostingdto.setOtherChargesName(otherChargesName);
				procurementcostingdto.setOtherChargesAmount(otherChargesAmount);
				procurementCostingDtoList.add(procurementcostingdto);
				procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
				totalPurchasePrice = totalPurchasePrice + otherChargesAmount;
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				setOtherChargesAmount(null);
				setOtherChargesName(null);
				log.info("<==== END ProcurementCostingBean.addOtherCharges =============>");

			}
		} catch (Exception e) {
			log.info("<==== Exception Occured In ProcurementCostingBean.addOtherCharges Inside Catch=============>");
		}

	}

	public void deleteRow(ProcurementCostingRequestDto procurementcostingDTO) {

		try {
			if (procurementCostingDtoList.contains(procurementcostingDTO)) {
				log.info("<==== Starts ProcurementCostingBean.deleteRow Inside If =====>" + procurementcostingDTO);
				procurementCostingDtoList.remove(procurementcostingDTO);
				// procurementCostingRequestDto.getProcurementCostingRequestDtolist().remove(procurementcostingDTO);
				totalPurchasePrice = totalPurchasePrice - procurementcostingDTO.getOtherChargesAmount();
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				log.info("<==== END ProcurementCostingBean.deleteRow =====>");
			}
		} catch (Exception exp) {
			log.info("Error In deleteRow Metod", exp);
		}

	}

}
