package in.gov.cooptex.operation.purchaseorder.ui;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.accounts.model.PurchaseInvoiceItems;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PurchaseOrderDTO;
import in.gov.cooptex.core.purchase.model.PurchaseInvoiceItemTax;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.InvoiceStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.operation.model.TaxMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("purchaseOrderBillBean")
public class PurchaseOrderBillBean {

	private final String CREATE_BILL_PAGE = "/pages/operation/createBill.xhtml?faces-redirect=true;";
	private final String LIST_BILL_PAGE = "/pages/operation/listBill.xhtml?faces-redirect=true;";
	private final String VIEW_BILL_PAGE = "/pages/operation/viewBill.xhtml?faces-redirect=true;";
	
	final String SERVER_URL = AppUtil.getPortalServerURL();
	
	final String orderType = "BILL";

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Long supplierTypeId;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeList;

	@Getter
	@Setter
	List<SupplierMaster> supplierList;
	
	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;

	@Getter
	@Setter
	List<PurchaseOrderDTO> poTaxSummaryList;

	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	List<TaxMaster> taxMasterList;
	
	@Getter
	@Setter
	TaxMaster taxMaster;
	
	@Getter
	@Setter
	PurchaseInvoice purchaseInvoice;
	
	@Getter
	@Setter
	PurchaseInvoiceItems purchaseInvoiceItems;
	
	@Getter
	@Setter
	PurchaseInvoiceItemTax invoiceItemTax;
	
	@Getter
	@Setter
	PurchaseOrderDTO purchaseOrderDTO;
	
	@Getter
	@Setter
	String discountType;

	@Getter
	@Setter
	String discountApplicable;
	
	@Getter
	@Setter
	List<PurchaseInvoiceItems> purchaseInvoiceItemsList;
	
	@Getter
	@Setter
	List<PurchaseInvoiceItemTax> purchaseInvoiceItemTaxList;
	
	@Getter @Setter
	List<String> statusValuesList;
	
	@Getter
	@Setter
	LazyDataModel<PurchaseInvoice> purchaseInvoiceBillList;
	
	public String billListPage() {
		addButtonFlag = false;
		viewButtonFlag = true;
		loadStatusValues();
		loadLazyBillList();
		return LIST_BILL_PAGE;
	}
	
	private void loadStatusValues() {
		Object[] statusArray = InvoiceStatus.class.getEnumConstants();

		statusValuesList = new ArrayList<>();

		for (Object status : statusArray) {
			statusValuesList.add(status.toString());
		}

		log.info("<--- statusValuesList ---> " + statusValuesList);
	}
	
	public void loadLazyBillList() {
		log.info("<===== Starts PurchaseOrderItem Bean.loadLazyPurchaseOrderList  ======>");
		String LIST_URL = SERVER_URL+ appPreference.getOperationApiUrl() + "/societyinvoice/getallbilllistlazy";

		purchaseInvoiceBillList = new LazyDataModel<PurchaseInvoice>() {
			
			List<PurchaseInvoice> purchaseInvoiceList = new ArrayList<>();
			/**
			 * 
			 */
			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<PurchaseInvoice> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for PurchaseInvoice List=============>" + LIST_URL);
					BaseDTO response = httpService.post(LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						purchaseInvoiceList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseInvoice>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyBillList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return purchaseInvoiceList;
			}

			@Override
			public Object getRowKey(PurchaseInvoice res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PurchaseInvoice getRowData(String rowKey) {
				try {
					for (PurchaseInvoice obj : purchaseInvoiceList) {
						if (obj.getId().equals(Long.valueOf(rowKey))) {
							purchaseInvoice = obj;
							return obj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Quotation Bean.loadLazyQuotationList List ======>");
	}
	
	public String createBill() {
		purchaseInvoice = new PurchaseInvoice();
		purchaseInvoiceItems = new PurchaseInvoiceItems();
		invoiceItemTax = new PurchaseInvoiceItemTax();
		purchaseOrderDTO = new PurchaseOrderDTO();
		getAllSupplierTypeList();
		taxMasterList = new ArrayList<>();
		taxMasterList = loadTaxMasterList();
		purchaseInvoiceItemsList = new ArrayList<>();
		poTaxSummaryList = new ArrayList<>();
		supplierTypeMaster = new SupplierTypeMaster();
		purchaseInvoiceItemTaxList = null;
		return CREATE_BILL_PAGE;
	}
	
	public void getAllSupplierTypeList() {
		try {
			supplierTypeList = new ArrayList<SupplierTypeMaster>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/suppliertypemaster/getall";
			log.info(" PurchaseOrderItem Bean ==>>> getAllSupplierTypeList URL===>>> " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierTypeList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean . getAllSupplierTypeList ------- >>>>");
	}
	
	private List<TaxMaster> loadTaxMasterList() {
		log.info("getTaxMasterList method starts------");
		List<TaxMaster> taxMasterList = null;
		try {
			String URL = SERVER_URL + "/purchaseOrderItemController/getTaxMasterList";
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				taxMasterList = mapper.readValue(jsonValue, new TypeReference<List<TaxMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("getTaxMasterList exception-----", e);
		}
		log.info("getTaxMasterList method ends------");
		return taxMasterList;
	}
	
	public void getSupplierMasterListBySupplierTypeId() {
		try {
			if(supplierTypeMaster != null) {
				supplierTypeId = getSupplierTypeMaster().getId();
			}
			log.info("supplier type id---->"+supplierTypeId);
			supplierList = new ArrayList<SupplierMaster>();
			if (supplierTypeId != null) {
				BaseDTO baseDTO = new BaseDTO();
				String url = SERVER_URL + "/supplier/master/getBySupplierType/" + supplierTypeId;
				log.info(" PurchaseOrderItem Bean ==>>> getSupplierMasterListBySupplierTypeId URL===>>> " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					supplierList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("<<<< ----------End PurchaseOrderItem Bean... getSupplierMasterListBySupplierTypeId ------- >>>>");
	}
	
	public void calculateItemTotal() {
		try {
			if(purchaseInvoiceItems.getUnitRate() != null && !purchaseInvoiceItems.getUnitRate().equals(0.0)) {
				purchaseInvoiceItems.setItemAmount(purchaseInvoiceItems.getUnitRate());
			}else {
				log.info("unitrate fields is required");
			}
		}catch (Exception e) {
			log.error("calculateItemTotal exception-----",e);
		}
	}
	
	public void calculateTax() {
		Double taxValue = 0.0;
		Double itemAmount = 0.0;
		try {
			DecimalFormat decimalFormat  = AppUtil.DECIMAL_FORMAT_TWO;
			if(purchaseInvoiceItems.getItemAmount() != null && !purchaseInvoiceItems.getItemAmount().equals(0.0)) {
				if(purchaseInvoiceItems.getDiscountValue() != null && !purchaseInvoiceItems.getDiscountValue().equals(0.0)) {
					itemAmount = purchaseInvoiceItems.getItemAmount() - purchaseInvoiceItems.getDiscountValue();
				}else {
					itemAmount = purchaseInvoiceItems.getItemAmount();
				}
				log.info("item amount----------"+itemAmount);
				if(purchaseOrderDTO.getTaxPercentage() != null && !purchaseOrderDTO.getTaxPercentage().equals(0.0)) {
					taxValue = itemAmount * (purchaseOrderDTO.getTaxPercentage()/100);
					log.info("tax value----------"+taxValue);
					String tax =  decimalFormat.format(taxValue);
					log.info("tax value----------"+tax);
					purchaseOrderDTO.setTaxValue(Double.valueOf(tax));
				}else {
					log.info("taxPrecent fields is required");
				}
			}
		}catch (Exception e) {
			log.error("calculateItemTotal exception-----",e);
		}
	}
	
	public void addTaxSummaryListDtls() {
		Double discountValue = 0.0;
		double taxValue = 0.0;
		try {
			log.info(" Called... Bean ==>>> addTaxSummaryDtls");
			if(purchaseOrderDTO != null) {
				PurchaseOrderDTO DTO = new PurchaseOrderDTO();
				if(taxMaster != null) {
					DTO.setTaxType(taxMaster.getTaxCode());
				}
				DTO.setTaxPercentage(purchaseOrderDTO.getTaxPercentage());
				DTO.setTaxValue(purchaseOrderDTO.getTaxValue());
				poTaxSummaryList.add(DTO);
			}
			if (poTaxSummaryList != null && poTaxSummaryList.size() > 0) {
				for (PurchaseOrderDTO poDto : poTaxSummaryList) {
					if (poDto.getTaxValue() != null) {
						taxValue += poDto.getTaxValue();
					}
				}
			} 
			if(purchaseInvoiceItems.getItemAmount() != null) {
				if(purchaseInvoiceItems.getDiscountValue() != null) {
					discountValue = purchaseInvoiceItems.getDiscountValue();
				}
				purchaseInvoiceItems.setItemTotal((purchaseInvoiceItems.getItemAmount() - discountValue) + taxValue);
			}
			purchaseOrderDTO.setTaxPercentage(0.0);
			purchaseOrderDTO.setTaxValue(0.0);
			taxMaster = null;
			log.info(" End... PurchaseOrder Bean ==>>> addTaxSummaryDtls");
		}
		catch (Exception e) {
			log.error(" Errro at  Bean ==>>>> addTaxSummaryDtls... " , e);
		}
	}
	
	public void removePoTaxSummaryList(PurchaseOrderDTO purchaseOrderDTO) {
		if(poTaxSummaryList != null) {
			poTaxSummaryList.remove(purchaseOrderDTO);
		}
	}
	
	public void clearOrderItems() {
		purchaseInvoiceItems = new PurchaseInvoiceItems();
		poTaxSummaryList = new ArrayList<PurchaseOrderDTO>();
		purchaseOrderDTO = new PurchaseOrderDTO();
		discountApplicable = null;
		discountType = null;
		taxMaster = new TaxMaster();
	}
	
	public void addOrderDetails() {
		log.info("Starts addOrderDetails method------");
		purchaseInvoiceItemTaxList = new ArrayList<>();
		double taxValue = 0.0;
		double taxPercentage = 0.0;
		try {
			if(poTaxSummaryList != null) {
				for (PurchaseOrderDTO purchaseOrderDTO : poTaxSummaryList) {
					PurchaseInvoiceItemTax itemTax = new PurchaseInvoiceItemTax();
					itemTax.setTaxValue(purchaseOrderDTO.getTaxValue());
					itemTax.setPurchaseInvoiceItems(purchaseInvoiceItems);
					purchaseInvoiceItemTaxList .add(itemTax);
					taxValue += purchaseOrderDTO.getTaxValue();
					taxPercentage += purchaseOrderDTO.getTaxPercentage();
				}
			}
			purchaseInvoiceItems.setTaxPercentage(taxPercentage);
			purchaseInvoiceItems.setTaxValue(taxValue);
			purchaseInvoiceItems.setPurchaseInvoiceItemTaxList(purchaseInvoiceItemTaxList);
			purchaseInvoiceItemsList.add(purchaseInvoiceItems);
			clearOrderItems();
		}catch (Exception e) {
			log.error(" Errro at  Bean ==>>>> addOrderDetails... " , e);
		}
		log.info("Ends addOrderDetails method------");
	}
	
	public void removePurchaseOrderItemDtls(PurchaseInvoiceItems invoiceItems) {
		if(purchaseInvoiceItemsList != null) {
			purchaseInvoiceItemsList.remove(invoiceItems);
		}
	}
	
	public String submitBill() {
		log.info("submitBill methods starts-----");
		try {
			if(purchaseInvoiceItemsList != null && !purchaseInvoiceItemsList.isEmpty()) {
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/societyinvoice/savebill";
				purchaseInvoice.setOrderType(orderType);
				purchaseInvoice.setPurchaseInvoiceItems(purchaseInvoiceItemsList);
				BaseDTO response = httpService.post(URL, purchaseInvoice);
				log.info("submitBill --------------->"+response.getStatusCode());
				if(response != null) {
					if(response.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getErrorCode())) {
						AppUtil.addInfo("Bill saved successfully");
					}else {
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					}
				}
				clearOrderItems();
				purchaseInvoice = new PurchaseInvoice();
				supplierTypeMaster = new SupplierTypeMaster();
				purchaseInvoiceItemsList = new ArrayList<>();
			}else {
				AppUtil.addWarn("Item details is required");
				return null;
			}
		}catch (Exception e) {
			log.error("submitBill methods exception-----",e);
		}
		log.info("submitBill methods ends-----");
		return billListPage();
	}
	
	public String clear() {
		addButtonFlag = false;
		viewButtonFlag = true;
		purchaseInvoice = null;
		return LIST_BILL_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts onRowSelect ========>" + event);
		purchaseInvoice = ((PurchaseInvoice) event.getObject());
		addButtonFlag = true;
		viewButtonFlag = false;
		log.info("<===Ends onRowSelect ========>");
	}
	
	public String viewBill() {
		log.info("viewBill method starts-------");
		try {
			purchaseInvoiceItemsList = new ArrayList<>();
			if(purchaseInvoice != null && purchaseInvoice.getId() != null) {
				String url = SERVER_URL + appPreference.getOperationApiUrl() +"/societyinvoice/viewbill/"+purchaseInvoice.getId();
				BaseDTO baseDTO = httpService.get(url);
				if(baseDTO != null) {
					if(baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						purchaseInvoice = mapper.readValue(jsonResponse,new TypeReference<PurchaseInvoice>(){
						});
						
						if(purchaseInvoice != null) {
							log.info("purchaseInvoiceitems list---------"+purchaseInvoice.getPurchaseInvoiceItems().size());
							purchaseInvoiceItemsList.addAll(purchaseInvoice.getPurchaseInvoiceItems());
						}
					}
				}
			}
		}catch (Exception e) {
			log.error("view method exception----",e);
		}
		log.info("viewBill method ends-------");
		return VIEW_BILL_PAGE;
	}
}
