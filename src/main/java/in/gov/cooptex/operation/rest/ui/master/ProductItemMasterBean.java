package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ProductMasterDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.operation.dto.ProductCategoryDTO;
import in.gov.cooptex.operation.dto.ProductGroupMasterDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
public class ProductItemMasterBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	String code;
	@Getter
	@Setter
	String name;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	/** for client server connection */
	RestTemplate restTemplate;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	ProductCategoryDTO productCategoryDto;

	@Getter
	@Setter
	ProductGroupMasterDTO productGroupDto;

	@Getter
	@Setter
	List<ProductCategoryDTO> productCategoryDtoList;

	@Getter
	@Setter
	List<ProductGroupMasterDTO> productGroupDtoList;

	@Getter
	@Setter
	ProductMasterDTO productMasterDto;

	@PostConstruct
	public void init() {
		log.info("ProductItemMasterBean ---init()");
		getCategoryMasterList();
		// getProductGroupByProductCategory();
	}

	ProductItemMasterBean() {
		log.info("---ProductItemMasterBean Constructor---");
		loadValues();
	}

	private void loadValues() {
		log.info("---loadValues----");
		try {			
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String submitForm() {
		log.info("--submitForm---");
		UserDTO userDto = new UserDTO();
		userDto.setId(loginBean.getUserDetailSession().getId());
		log.info("code is :" + productMasterDto.getCode());
		log.info("name is :" + productMasterDto.getName());
		log.info("group code is :" + productGroupDto.getCode());
		log.info("group id is :" + productGroupDto.getId());
		productMasterDto.setCreatedBy(userDto);
		productMasterDto.setCreatedDate(new Date());
		productMasterDto.setModifiedBy(userDto);
		productMasterDto.setModifiedBy(userDto);
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		String url = serverURL + "/productMaster/saveAll";
		HttpEntity<ProductMasterDTO> requestEntity = new HttpEntity<ProductMasterDTO>(productMasterDto,
				loginBean.headers);
		HttpEntity<BaseDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, BaseDTO.class);
		BaseDTO baseDto = responseEntity.getBody();
		return null;
	}

	public void getCategoryMasterList() {
		log.info("---getCategoryMasterList--   Start");
		RestTemplate restTemplate = new RestTemplate();

		productCategoryDtoList = new ArrayList<ProductCategoryDTO>();
		WrapperDTO wrapperDto = new WrapperDTO();
		try {
			String url = serverURL + "/category/getCategoryList";

			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			HttpEntity<WrapperDTO> response = restTemplate.exchange(url, HttpMethod.GET, entity, WrapperDTO.class);
			log.info("<----Url--->" + url);
			wrapperDto = response.getBody();
			log.info("Wrapper Dto :" + response.getBody());
			if (wrapperDto != null) {
				if (wrapperDto.getProductCategoryDTOCollections() != null) {
					productCategoryDtoList = (List<ProductCategoryDTO>) wrapperDto.getProductCategoryDTOCollections();
					// productCategoryList = new
					// ArrayList<ProductCategoryDTO>(productCategoryDtoList);
				} else {
					productCategoryDtoList = new ArrayList<ProductCategoryDTO>();
				}
			}
			log.info("---getCategoryMasterList--   End");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onProductCategoryChange() {
		log.info("ProductItemMasterBean getProductGroupByProductCategory------#Start");
		WrapperDTO wrapperDto = new WrapperDTO();
		try {
			String url = serverURL + "/group/getGroupByCategory";
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<ProductCategoryDTO> requestEntity = new HttpEntity<ProductCategoryDTO>(productCategoryDto,
					loginBean.headers);

			HttpEntity<WrapperDTO> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
					WrapperDTO.class);

			wrapperDto = responseEntity.getBody();
			log.info("WrapperDTO :" + responseEntity.getBody());
			if (wrapperDto != null) {
				if (wrapperDto.getProductGroupDTOCollections() != null) {
					productGroupDtoList = (List<ProductGroupMasterDTO>) wrapperDto.getProductGroupDTOCollections();
				} else {
					productGroupDtoList = new ArrayList<ProductGroupMasterDTO>();
				}
			}
		} catch (Exception e) {
			log.error("Error in onProductCategoryChange", e);
		}
	}
}
