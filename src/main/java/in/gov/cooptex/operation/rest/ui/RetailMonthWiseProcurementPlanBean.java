package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeLeaveDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.RetailProductionPlanStage;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.model.RetailMonthwiseProcurement;
import in.gov.cooptex.operation.production.dto.RetailMonthwisePlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanDetails;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Sreekanth
 *
 */
@Service("retailMonthWiseProcurementPlanBean")
@Scope("session")
@Log4j2
public class RetailMonthWiseProcurementPlanBean {

	@Getter
	@Setter
	String serverURL = null;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	List<String> listMonths;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	List<RetailProductionPlan> retailProductionPlanList;

	@Getter
	@Setter
	RetailMonthwiseProcurement retailMonthwiseProcurementPlan;

	@Getter
	@Setter
	List<RetailMonthwiseProcurement> retailMonthWiseProcuremnetPlanList = new ArrayList<RetailMonthwiseProcurement>();

	@Getter
	@Setter
	RetailMonthwisePlanRequest retailMonthwisePlanRequest;

	@Getter
	@Setter
	Integer count = 0;

	private final String MONTH_WISE_PROCUREMNET_LIST = "/pages/operation/retailproduction/listMonthWiseProcurementPlan.xhtml?faces-redirect=true;";

	private final String MONTH_WISE_PROCUREMNET_ADD = "/pages/operation/retailproduction/createMonthWiseProcurementPlan.xhtml?faces-redirect=true;";

	private final String MONTH_WISE_PROCUREMNET_VIEW = "/pages/operation/retailproduction/viewMonthWiseProcurementPlan.xhtml?faces-redirect=true;";

	private final String DP_PROCUREMNET_PLAN_LIST = "/pages/dnp/listDnpProcurementPlan.xhtml?faces-redirect=true;";

	private final String SOCIETY_PROCUREMNET_PLAN_LIST = "/pages/dnp/listSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String PRORDER_PROCUREMNET_PLAN_LIST = "/pages/dnp/listProcurementOrder.xhtml?faces-redirect=true;";

	private final String LISTQUOTE_PROCUREMNET_PLAN_LIST = "/pages/weavers/listQuotation.xhtml?faces-redirect=true;";

	private final String STOCKOUTWARD_PROCUREMNET_PLAN_LIST = "/pages/weaversModule/listStockOutward.xhtml?faces-redirect=true;";

	public String getDPListPage() {
		return DP_PROCUREMNET_PLAN_LIST;
	}

	public String getSOCIETYListPage() {
		return SOCIETY_PROCUREMNET_PLAN_LIST;
	}

	public String getPROCUREMENTListPage() {
		return PRORDER_PROCUREMNET_PLAN_LIST;
	}

	public String getQUOTEListPage() {
		return LISTQUOTE_PROCUREMNET_PLAN_LIST;
	}

	public String getSTOCKOUTWARDListPage() {
		return STOCKOUTWARD_PROCUREMNET_PLAN_LIST;
	}

	RestTemplate restTemplate;

	@Getter
	@Setter
	Double productionPlanTotalEligibiltyValue = null;

	@Getter
	@Setter
	String planFromMonth = null;

	@Getter
	@Setter
	String planToMonth = null;

	@Getter
	@Setter
	Integer planFromYear = null;

	@Getter
	@Setter
	Integer planToYear = null;

	@Getter
	@Setter
	Date previousSalesFromDate = null;

	@Getter
	@Setter
	Date previousSalesToDate = null;

	@Getter
	@Setter
	Double remainingPercentage = null;

	@Getter
	@Setter
	Double baseStockPercentage = null;

	@Getter
	@Setter
	Date createdDate;

	@Getter
	@Setter
	String createdBy;

	@Getter
	@Setter
	Double remainingValue = null;

	@Getter
	@Setter
	Double calculatedProcurementValue = null;

	@Getter
	@Setter
	StringBuilder regionValues = new StringBuilder();

	@Getter
	@Setter
	StringBuilder productCategoryValues = new StringBuilder();

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

	@Getter
	@Setter
	LazyDataModel<RetailProductionPlanResponse> retailProductionPlanResponseLazyList;

	@Getter
	@Setter
	private String currentPlanStatus;

	@Getter
	@Setter
	String comments,showroomStatus;

	@Autowired
	RetailProductionPlanLogBean retailProductionPlanLogBean;

	@Getter
	@Setter
	Integer resultSize;

	@Getter
	@Setter
	List statusAndStageValues;

	@Getter
	@Setter
	Double footerPercentage;

	@Getter
	@Setter
	BigDecimal bigDecimalFooterpercentage = null;// = new BigDecimal(footerPercentage);

	@Getter
	@Setter
	Double footerValue;

	@Getter
	@Setter
	BigDecimal bigDecimalFooterVlaue = null; // = new BigDecimal(footerValue);

	@Getter
	@Setter
	boolean addButtonFlag = false;
	
	@Getter
	@Setter
	boolean editButtonFlag = true;
	
	@Getter
	@Setter
	List<RetailProductionPlanDetails> retailProductionPlanDetailsList=new ArrayList<RetailProductionPlanDetails>();
	

	public RetailMonthWiseProcurementPlanBean() {
		log.info("<--- Inside RetailMonthWiseProcuremnetPlanBean() --->");
		restTemplate = new RestTemplate();
		loadValues();
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
			log.info("<--- portal.server.url ---> " + serverURL);
		} catch (Exception e) {
			log.error("Exception at loadValues() >>>> ", e);
		}
	}

	public String showCreateRetailMonthWiseProcuremnetPlan() {
		log.info("<--- Inside createRetailMonthWiseProcuremnetPlan() --->");

		retailProductionPlanResponse = new RetailProductionPlanResponse();
		retailProductionPlanResponse.setStage(RetailProductionPlanStage.DRAFT);
		retailProductionPlanResponse.setStatus(RetailProductionPlanStatus.APPROVED);

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		bigDecimalFooterpercentage = null;
		bigDecimalFooterVlaue = null;

		retailProductionPlan = new RetailProductionPlan();
		planFromMonth = null;
		planFromYear = null;
		planToMonth = null;
		planToYear = null;
		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();
		previousSalesFromDate = null;
		previousSalesToDate = null;
		baseStockPercentage = null;
		retailMonthWiseProcuremnetPlanList = new ArrayList<>();
		productionPlanTotalEligibiltyValue = null;
		remainingPercentage = null;
		remainingValue = null;
		calculatedProcurementValue = null;
		editButtonFlag = true;
		// this is to check whether any plans are available to create Monthwise
		// Procurement
		List<RetailProductionPlan> retailProductionPlanList = getProductionPlanList();
		if (retailProductionPlanList != null && retailProductionPlanList.size() > 0) {
			return MONTH_WISE_PROCUREMNET_ADD;
		} else {
			log.info("No Approved Initial Plan is available to Create Monthly Procurement Plan");
			AppUtil.addWarn("No Approved Initial Plan is available to Create Monthly Plan");
			return MONTH_WISE_PROCUREMNET_LIST;
		}

	}

	public List<RetailProductionPlan> getProductionPlanList() {
		log.info("<--- Inside getProductionPlanList() --->");
		String requestPath = serverURL + appPreference.getOperationApiUrl()
				+ "/monthwiseprocurement/getretailproductionplanlist";
		log.info("<--- getRetailProductionPlanList() URL ---> " + requestPath);
		BaseDTO baseDTO = httpService.get(requestPath);
		try {
			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailProductionPlanList = (List<RetailProductionPlan>) mapper.readValue(jsonValue,
						new TypeReference<List<RetailProductionPlan>>() {
						});
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("<--- Error in getProductionPlanList() --->", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return retailProductionPlanList;
	}

	public void onChangeProductionPlan() {
		log.info("<--- Inside onChangePropductionPlan() ---> ");

		footerPercentage = null;
		footerValue = null;

		if (retailProductionPlan == null) {
			log.info("<--- if selected nothing clear all fields. Do this --->");
			planFromMonth = null;
			planFromYear = null;
			planToMonth = null;
			planToYear = null;
			regionValues = new StringBuilder();
			productCategoryValues = new StringBuilder();
			previousSalesFromDate = null;
			previousSalesToDate = null;
			baseStockPercentage = null;
			retailMonthWiseProcuremnetPlanList = new ArrayList<>();
			productionPlanTotalEligibiltyValue = null;
			remainingPercentage = null;
			remainingValue = null;
			calculatedProcurementValue = null;
			return;
		}

		Date planForm = retailProductionPlan.getPlanFrom();
		Date planTo = retailProductionPlan.getPlanTo();
		planFromMonth = Util.getOnlyMonthFromDate(planForm);

		planToMonth = Util.getOnlyMonthFromDate(planTo);

		planFromYear = Util.getOnlyYearFromDate(planForm);
		planToYear = Util.getOnlyYearFromDate(planTo);

		previousSalesFromDate = retailProductionPlan.getSalesFrom();
		previousSalesToDate = retailProductionPlan.getSalesTo();
		baseStockPercentage = retailProductionPlan.getBaseStockPercentage();

		getRegionandCategoryDetailsFromPlan(retailProductionPlan.getId());

		processMonthwiseListBasedOnPlanFromAndPlanToDate(planForm, planTo);

		getPlanTotalEligibilityValue(retailProductionPlan.getId());
		
		
	}

	public void getRegionandCategoryDetailsFromPlan(Long planId) {
		log.info("<--- Inside getRegionandCategoryDetailsFromPlan() with plan ID---> " + planId);

		regionValues = new StringBuilder();

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getplanbyid/" + planId;
			log.info("<--- getRegionandCategoryDetailsFromPlan() URL ---> " + URL);
			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				retailProductionPlan = objectMapper.readValue(jsonResponse, RetailProductionPlan.class);

				regionValues.setLength(0);
				List<RetailProductionPlanRegion> retailProductionPlanRegionList= retailProductionPlan.getRetailProductionPlanRegionList();
				int retailProductionPlanRegionListSize = retailProductionPlanRegionList != null ? retailProductionPlanRegionList.size() : 0;
				log.info("<--- retailProductionPlan.getRetailProductionPlanRegionList() Size: ---> "
						+ retailProductionPlanRegionListSize);
				if(retailProductionPlanRegionListSize > 0) {
					
					Collections.sort(retailProductionPlan.getRetailProductionPlanRegionList(),
							new Comparator<RetailProductionPlanRegion>() {
						@Override
						public int compare(RetailProductionPlanRegion o1, RetailProductionPlanRegion o2) {
							return o1.getEntityMaster().getCode().compareTo(o2.getEntityMaster().getCode());
						}

					});
					
					for (RetailProductionPlanRegion regionValue : retailProductionPlan
							.getRetailProductionPlanRegionList()) {
						if (regionValue != null && regionValue.getEntityMaster().getCode() != null
								&& regionValue.getEntityMaster().getName() != null)
							regionValues.append(regionValue.getEntityMaster().getCode()).append("/")
									.append(regionValue.getEntityMaster().getName()).append(",<br/>");
					}
				}

				regionValues.deleteCharAt(regionValues.length() - 6);
				log.info("<--regionValues --> " + regionValues);

				productCategoryValues.setLength(0);
				List<ProductCategory> productCategoryList = retailProductionPlan.getProductCategoryList();
				int productCategoryListSize = productCategoryList != null ? productCategoryList.size() : 0;
				log.info("<--- retailProductionPlan.getProductCategoryList() ---> "
						+ productCategoryListSize);
				if(productCategoryListSize > 0) {
					Collections.sort(retailProductionPlan.getProductCategoryList(),
							new Comparator<ProductCategory>() {
						@Override
						public int compare(ProductCategory o1, ProductCategory o2) {
							return o1.getProductCatCode().compareTo(o2.getProductCatCode());
						}

					});
					
					for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
						if (categoryValue != null && categoryValue.getProductCatCode() != null
								&& categoryValue.getProductCatName() != null)
							productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
									.append(categoryValue.getProductCatName()).append(",<br/>");
					}
				}
				productCategoryValues.deleteCharAt(productCategoryValues.length() - 6);
				log.info("<--productCategoryValues --> " + productCategoryValues);
			} else {
				log.info("<--- Retail Production Plan Not Found ---> ");
				errorMap.notify(ErrorDescription.ERROR_RETAIL_PRODUCTION_PLAN_NOT_FOUND.getCode());
			}
		} catch (Exception e) {
			log.error("Error at getRegionandCategoryDetailsFromPlan >>> ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	private void processMonthwiseListBasedOnPlanFromAndPlanToDate(Date planForm, Date planTo) {
		log.info("<--- Inside processMonthwiseListBasedOnPlanFromAndPlanToDate() ---> ");

		retailMonthWiseProcuremnetPlanList = new ArrayList<>();

		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();

		try {
			beginCalendar.setTime(planForm);
			finishCalendar.setTime(planTo);

			while (beginCalendar.before(finishCalendar)) {
				String monthName = Util.getOnlyMonthFromDate(beginCalendar.getTime());
				Integer yearValue = Util.getOnlyYearFromDate(beginCalendar.getTime());
				beginCalendar.add(Calendar.MONTH, 1);
				RetailMonthwiseProcurement retailMonthwiseProcurement = new RetailMonthwiseProcurement();
				RetailProductionPlan retailProdPlan = new RetailProductionPlan();
				retailProdPlan.setId(retailProductionPlan.getId());
				//retailMonthwiseProcurement.setRetailProductionPlan(retailProductionPlan);
				retailMonthwiseProcurement.setRetailProductionPlan(retailProdPlan);
				retailMonthwiseProcurement.setMonthName(monthName);
				retailMonthwiseProcurement.setYearValue(yearValue);
				retailMonthwiseProcurement.setVersion(0l);
				retailMonthWiseProcuremnetPlanList.add(retailMonthwiseProcurement);
			}
			log.info("<--- MonthwiseList BasedOn PlanFrom And PlanTo Date() is ---> "
					+ retailMonthWiseProcuremnetPlanList.size());
		} catch (Exception e) {
			log.error("<--- Error in processMonthwiseListBasedOnPlanFromAndPlanToDate() ---> ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	private void getPlanTotalEligibilityValue(Long planId) {
		log.info("<--- Inside getPlanTotalEligibilityValue() with planId---> " + planId);

		retailProductionPlan.setId(planId);

		String requestPath = appPreference.getOperationApiUrl() + "/monthwiseprocurement/getplantotaleligibilityvalue";

		log.info("<--- getPlanTotalEligibilityValue() URl ---> " + requestPath);

		try {
			BaseDTO baseDTO = AppUtil.getResponseObject(requestPath, HttpMethod.POST, retailProductionPlan,
					BaseDTO.class, loginBean.headers);
			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				productionPlanTotalEligibiltyValue = (Double) baseDTO.getResponseContent();
				remainingValue = productionPlanTotalEligibiltyValue;
				remainingPercentage = 100D;
				log.info("<--- Production Plan Total Eligibilty Value ---> " + productionPlanTotalEligibiltyValue);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
//				errorMap.notify(baseDTO.getStatusCode());
				return;
			}
		} catch (Exception exception) {
			log.error("<--- Error in showRetailMonthWiseProcuremnetPlanList ---> " + exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void calculateValue() {
		log.info("<--- Inside calculateValue() ---> " + retailMonthWiseProcuremnetPlanList.size());

		double totalProcurementPercent = 100;
		double tempProcurementPercentage = 0;
		double tempProcuremnetValue = 0;
		double procurementPercent = 0;
		double procurementValue = 0;
		int temp = 0;
		for (RetailMonthwiseProcurement monthwiseProcuremnet : retailMonthWiseProcuremnetPlanList) {
			temp = temp + 1;
			if (monthwiseProcuremnet.getProcurementPercentage() != null) {

				procurementPercent = monthwiseProcuremnet.getProcurementPercentage().doubleValue();

				tempProcurementPercentage = tempProcurementPercentage + procurementPercent;

				footerPercentage = tempProcurementPercentage;
				footerValue = tempProcuremnetValue;

				bigDecimalFooterpercentage = new BigDecimal(footerPercentage);
				bigDecimalFooterpercentage = bigDecimalFooterpercentage.setScale(2, BigDecimal.ROUND_HALF_UP);
				/*
				 * bigDecimalFooterVlaue = new BigDecimal(footerValue); bigDecimalFooterVlaue =
				 * bigDecimalFooterVlaue.setScale(2, BigDecimal.ROUND_HALF_UP);
				 */
				if (tempProcurementPercentage > 100D) {
					log.info("<--- Should not exceed 100% ---> ");
					errorMap.notify(ErrorDescription.ERROR_INVALID_MONTH_WISE_PROC_PRCNT.getCode());
					monthwiseProcuremnet.setProcurementPercentage(0D);
					/*
					 * monthwiseProcuremnet = retailMonthWiseProcuremnetPlanList .get(temp - 1);
					 * monthwiseProcuremnet.setProcurementPercentage(null);
					 * monthwiseProcuremnet.setProcurementValue(null);
					 */
					return;
				}

				if(productionPlanTotalEligibiltyValue!=null) {
				procurementValue = (productionPlanTotalEligibiltyValue * procurementPercent) / 100;
				}

				monthwiseProcuremnet.setProcurementValue(procurementValue);

				tempProcuremnetValue = tempProcuremnetValue + monthwiseProcuremnet.getProcurementValue();
			} else if (monthwiseProcuremnet.getProcurementPercentage() == null) {
				monthwiseProcuremnet.setProcurementPercentage(0D);
				monthwiseProcuremnet.setProcurementValue(0D);
			}
		}
		remainingPercentage = totalProcurementPercent - tempProcurementPercentage;
		if(productionPlanTotalEligibiltyValue!=null) {
		remainingValue = productionPlanTotalEligibiltyValue.doubleValue() - tempProcuremnetValue;
		}
		calculatedProcurementValue = tempProcuremnetValue;

	}

	public String showEditRetailMonthWiseProcurementPlan() {
		log.info("<--- Inside showEditRetailMonthWiseProcurementPlan() ---> ");

		footerPercentage = null;
		footerValue = null;

		if (retailProductionPlanResponse == null) {
			log.info("<--- Inside (retailProductionPlan==null) showEditRetailMonthWiseProcurementPlan() ---> ");
			Util.addWarn("Please Select any one");
			return null;
		}

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		if (retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.PROCUREMENT)
				&& (retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.APPROVED))
				|| retailProductionPlanResponse.getStage().equals(RetailProductionPlanStage.PROCUREMENT)
						&& (retailProductionPlanResponse.getStatus().equals(RetailProductionPlanStatus.SUBMITTED))) {
			log.info("Already Month wise Submitted ");
			errorMap.notify(ErrorDescription.INFO_RETAIL_MONTHWISE_ALREADY_SUBMITTED.getCode());
			return null;
		}

		planFromMonth = Util.getOnlyMonthFromDate(retailProductionPlanResponse.getPlanFrom());
		planToMonth = Util.getOnlyMonthFromDate(retailProductionPlanResponse.getPlanTo());

		planFromYear = Util.getOnlyYearFromDate(retailProductionPlanResponse.getPlanFrom());
		planToYear = Util.getOnlyYearFromDate(retailProductionPlanResponse.getPlanTo());

		previousSalesFromDate = retailProductionPlanResponse.getSalesFrom();
		previousSalesToDate = retailProductionPlanResponse.getSalesTo();
		baseStockPercentage = retailProductionPlanResponse.getBaseStockPercentage();

		Long productionPlanId = retailProductionPlanResponse.getPlanId();
		String getByIdPath = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/get/"
				+ productionPlanId;

		log.info("<--- showEditRetailMonthWiseProcurementPlan() getByIdPath URL ---> " + getByIdPath);

		BaseDTO baseDTO3 = httpService.get(getByIdPath);
		try {
			if (baseDTO3 != null && baseDTO3.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO3.getResponseContent());
				retailMonthWiseProcuremnetPlanList = (List<RetailMonthwiseProcurement>) mapper.readValue(jsonValue,
						new TypeReference<List<RetailMonthwiseProcurement>>() {
						});
				log.info("retailMonthWiseProcuremnetPlanList Size : " + retailMonthWiseProcuremnetPlanList.size());
			} else {
				String msg = baseDTO3.getErrorDescription();
				log.error("Status code:" + baseDTO3.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO3.getStatusCode());
			}
		} catch (Exception e) {
			log.info("<--- Error in showEditRetailMonthWiseProcurementPlan() --->" + e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		footerPercentage = retailMonthWiseProcuremnetPlanList.stream()
				.mapToDouble(RetailMonthwiseProcurement::getProcurementPercentage).sum();
		footerValue = retailMonthWiseProcuremnetPlanList.stream()
				.mapToDouble(RetailMonthwiseProcurement::getProcurementValue).sum();

		bigDecimalFooterpercentage = new BigDecimal(footerPercentage);
		bigDecimalFooterpercentage = bigDecimalFooterpercentage.setScale(2, BigDecimal.ROUND_HALF_UP);
		bigDecimalFooterVlaue = new BigDecimal(footerValue);
		bigDecimalFooterVlaue = bigDecimalFooterVlaue.setScale(2, BigDecimal.ROUND_HALF_UP);

		getRegionandCategoryDetailsFromPlan(productionPlanId);
		getPlanTotalEligibilityValue(productionPlanId);
		remainingValue = 0.0;
		remainingPercentage = 0.0;
		return MONTH_WISE_PROCUREMNET_ADD;
	}

	public String saveRetailMonthWiseProcurementPlan(String buttonName) {
		log.info("<--- Inside submitRetailMonthWiseProcurementPlan() --->");

		retailMonthwisePlanRequest = new RetailMonthwisePlanRequest();
		retailMonthwisePlanRequest.setCommandName(buttonName);

		String requestPath = "";

		if (action.equalsIgnoreCase("ADD")) {

			log.info("<--- RetailMonthWiseProcurementPlan() action ---> " + action);

			requestPath = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/create";

			log.info("<--- RetailMonthWiseProcurementPlan() create URL ---> " + requestPath);

			try {
				if(retailMonthWiseProcuremnetPlanList != null && retailMonthWiseProcuremnetPlanList.size() > 0) {
					
				retailMonthwisePlanRequest.setRetailMonthWiseProcuremnetPlanList(retailMonthWiseProcuremnetPlanList);

				BaseDTO baseDTO = httpService.post(requestPath, retailMonthwisePlanRequest);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("Retail Monthwise Procuremnet Plan Saved Successfully");
						if(buttonName.equalsIgnoreCase("INITIATED")) {
					      AppUtil.addInfo("Retail monthwise procuremnet plan saved successfully");
						}else {
						errorMap.notify(7969);
						}
						getMonthwisePlanList();
					} else {
						String msg = baseDTO.getErrorDescription();
						errorMap.notify(baseDTO.getStatusCode());
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						return null;
					}
				}
			 }else {
				    AppUtil.addWarn("Please Select Plan Code / Name");
					return null;
			 }
			} catch (Exception exception) {
				log.error("<--- Error in showRetailMonthWiseProcuremnetPlanList ---> ", exception);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} else if (action.equalsIgnoreCase("EDIT")) {

			log.info("<--- RetailMonthWiseProcurementPlan() action ---> " + action);

			requestPath = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/update";

			log.info("<--- RetailMonthWiseProcurementPlan() update URL ---> " + requestPath);

			try {
				retailMonthwisePlanRequest.setRetailMonthWiseProcuremnetPlanList(retailMonthWiseProcuremnetPlanList);

				BaseDTO baseDTO = httpService.put(requestPath, retailMonthwisePlanRequest);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("Retail Monthwise Procuremnet Plan Updated Successfully");
					errorMap.notify(7970);
					getMonthwisePlanList();
				} else {
					String msg = baseDTO.getErrorDescription();
					errorMap.notify(baseDTO.getStatusCode());
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					return null;
				}
			} catch (Exception exception) {
				log.info("<--- Error in showRetailMonthWiseProcuremnetPlanList ---> " + exception);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		}

		return MONTH_WISE_PROCUREMNET_LIST;
	}

	public String cancelRetailMonthwiseProcurementPlan() {
		log.info("<--- Inside cancelRetailMonthwiseProcurementPlan() --->");
		getMonthwisePlanList();
		return MONTH_WISE_PROCUREMNET_LIST;
	}

	@SuppressWarnings("unchecked")
	public String viewRetailMonthWiseProcuremnetPlan() {
		log.info("<--- Inside viewRetailMonthWiseProcuremnetPlan() --->" + retailProductionPlanResponse);

		footerPercentage = null;
		footerValue = null;

		retailMonthWiseProcuremnetPlanList = new ArrayList<>();

		if (retailProductionPlanResponse == null) {
			Util.addWarn("Please Select any one");
			return null;
		}

		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);

		planFromMonth = Util.getOnlyMonthFromDate(retailProductionPlanResponse.getPlanFrom());
		planToMonth = Util.getOnlyMonthFromDate(retailProductionPlanResponse.getPlanTo());

		planFromYear = Util.getOnlyYearFromDate(retailProductionPlanResponse.getPlanFrom());
		planToYear = Util.getOnlyYearFromDate(retailProductionPlanResponse.getPlanTo());

		previousSalesFromDate = retailProductionPlanResponse.getSalesFrom();
		previousSalesToDate = retailProductionPlanResponse.getSalesTo();
		baseStockPercentage = retailProductionPlanResponse.getBaseStockPercentage();

		createdDate = retailProductionPlanResponse.getCreatedDate();
		createdBy = retailProductionPlanResponse.getUserMaster().getUsername();

		Long productionPlanId = retailProductionPlanResponse.getPlanId();
		String requestPath = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/get/"
				+ productionPlanId;

		log.info("<--- viewRetailMonthWiseProcuremnetPlan() URL --->" + requestPath);

		try {
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailMonthWiseProcuremnetPlanList = (List<RetailMonthwiseProcurement>) mapper.readValue(jsonValue,
						new TypeReference<List<RetailMonthwiseProcurement>>() {
						});
				log.info("retailMonthWiseProcuremnetPlanList Size : " + retailMonthWiseProcuremnetPlanList.size());

				getRegionandCategoryDetailsFromPlan(productionPlanId);
				getPlanTotalEligibilityValue(productionPlanId);

				footerPercentage = retailMonthWiseProcuremnetPlanList.stream()
						.mapToDouble(RetailMonthwiseProcurement::getProcurementPercentage).sum();
				footerValue = retailMonthWiseProcuremnetPlanList.stream()
						.mapToDouble(RetailMonthwiseProcurement::getProcurementValue).sum();

				bigDecimalFooterpercentage = new BigDecimal(footerPercentage);
				bigDecimalFooterpercentage = bigDecimalFooterpercentage.setScale(2, BigDecimal.ROUND_HALF_UP);
				bigDecimalFooterVlaue = new BigDecimal(footerValue);
				bigDecimalFooterVlaue = bigDecimalFooterVlaue.setScale(2, BigDecimal.ROUND_HALF_UP);

				remainingValue = 0.0;
				remainingPercentage = 0.0;
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
			}
			getProductWiseTotalEligibilityValue(retailProductionPlanResponse.getPlanId());
		} catch (Exception e) {
			log.error("<--- Error in retailMonthWiseProcuremnetPlanList() ---> " + e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return MONTH_WISE_PROCUREMNET_VIEW;
	}

	public void loadLazyDraftProductionList() {

		log.info("LoadLazyData");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetailProductionPlanResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse res) {
				return res != null ? res.getPlanId() : null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RetailProductionPlanResponse res : responseList) {
					if (res.getPlanId().longValue() == value.longValue()) {
						retailProductionPlanResponse = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();
		log.info("<---Inside search called--->");
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		retailProductionPlanResponse = new RetailProductionPlanResponse();

		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				log.info("planFromSearch : " + value);
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("planTo")) {
				log.info("planToSearch : " + value);
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("stageOrStatus")) {
				log.info("selectedStatus : " + value);
				request.setStageOrStatus(value);
			}
		}
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/search";

			log.info("<--- search URL ---> " + URL);

			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("<--- Error in getSearchData()---> ", e);
		}

		return baseDTO;
	}

	public String getMonthwisePlanList() {
		log.info("<--- Inside getMonthwisePlanList() ---> ");
		retailProductionPlan = new RetailProductionPlan();
		action = null;
		planSize = 0;
		loadLazyDraftProductionList();
		loadStatusValues();
		return MONTH_WISE_PROCUREMNET_LIST;
	}

	private void loadStatusValues() {
		Object[] statusArray = RetailProductionPlanStatus.class.getEnumConstants();
		Object[] stageArray = RetailProductionPlanStage.class.getEnumConstants();

		statusAndStageValues = new ArrayList<>();

		for (Object stage : stageArray) {
			for (Object status : statusArray) {
				statusAndStageValues.add(stage + "-" + status);
			}
		}
		
		statusAndStageValues.remove(RetailProductionPlanStage.SHOWROOM+ "-" +RetailProductionPlanStatus.APPROVED);
		statusAndStageValues.remove(RetailProductionPlanStage.SHOWROOM+ "-" +RetailProductionPlanStatus.REJECTED);
		statusAndStageValues.remove(RetailProductionPlanStage.DRAFT+ "-" +RetailProductionPlanStatus.INITIATED);
		statusAndStageValues.remove(RetailProductionPlanStage.DRAFT+ "-" +RetailProductionPlanStatus.APPROVED);
		statusAndStageValues.remove(RetailProductionPlanStage.DRAFT+ "-" +RetailProductionPlanStatus.SUBMITTED);
		statusAndStageValues.remove(RetailProductionPlanStage.DRAFT+ "-" +RetailProductionPlanStatus.REJECTED);
		log.info("statusAndStageValues " + statusAndStageValues);
	}

	public String clear() {
		log.info("<--- Inside clear() ---> ");
		retailProductionPlan = new RetailProductionPlan();
		loadLazyDraftProductionList();
		editButtonFlag = true;
		return MONTH_WISE_PROCUREMNET_LIST;
	}

	public String statusUpdate() {
		log.info("Status Updated is called");
		log.info(currentPlanStatus);
		log.info(retailProductionPlan.getId());
		log.info(comments);

		BaseDTO baseDTO = null;
		RetailProductionPlanRequest request = new RetailProductionPlanRequest(retailProductionPlan.getId(),
				currentPlanStatus, comments);

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/approve";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/reject";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		comments = null;
		return MONTH_WISE_PROCUREMNET_LIST;
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		currentPlanStatus = value;
		comments = null;
	}

	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (retailProductionPlanResponse == null) {
			log.info("<---if(retailProductionPlanResponse == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			Util.addWarn("Please Select any one");
			return;
		} else {
			log.info("<---else(retailProductionPlanResponse == null)--->");
			if (!retailProductionPlanResponse.getStatus().toString().equals("INITIATED")) {
				log.info("Month Wise Plan cannot be Deleted ");
				errorMap.notify(ErrorDescription.INFO_RETAIL_MONTHWISE_PROCUREMNET_CANNOT_DELETED.getCode());
				return;
			}
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called---> " + retailProductionPlanResponse.getPlanId());
		String url = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/delete/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Month Wise Plan has been Deleted Successfully");
				errorMap.notify(ErrorDescription.INFO_RETAIL_MONTHWISE_PROCUREMNET_DELETED_SUCCESSFULLY.getCode());
				getMonthwisePlanList();
			} else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
		}
		return MONTH_WISE_PROCUREMNET_LIST;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Retail Monthwise Procurement Plan onRowSelect method started");
		retailProductionPlanResponse = ((RetailProductionPlanResponse) event.getObject());
		addButtonFlag = true;
		editButtonFlag = true;
		String stage = retailProductionPlanResponse.getStage().name();
		String status = retailProductionPlanResponse.getStatus().name();
		showroomStatus=stage+"-"+status;
		log.info("stage------------->"+stage);
		log.info("status------------->"+status);
		if ((stage.equalsIgnoreCase(RetailProductionPlanStage.DRAFT.toString())
				&& status.equalsIgnoreCase(RetailProductionPlanStatus.SUBMITTED.toString()))
				|| (stage.equalsIgnoreCase(RetailProductionPlanStage.PROCUREMENT.toString())
						&& status.equalsIgnoreCase(RetailProductionPlanStatus.REJECTED.toString()))
				|| (stage.equalsIgnoreCase(RetailProductionPlanStage.PROCUREMENT.toString())
						&& status.equalsIgnoreCase(RetailProductionPlanStatus.INITIATED.toString()))) {
			log.info("inside if --------" + editButtonFlag);
			editButtonFlag = false;
		}
	}

	public void onPageLoad() {
		log.info("Retail Monthwise Procurement Plan onPageLoad method started");
		addButtonFlag = false;
	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("MONTHWISE_RETAIL_PROCUREMENT_PLAN_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have a  privilege for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}
public String getProductWiseTotalEligibilityValue(Long productionPlanId) {
	log.info("<===getProductWiseTotalEligibilityValue Method Start===>");
	List<Long> categoryId=new ArrayList<Long>();
	try {
		String URL = serverURL + appPreference.getOperationApiUrl() + "/monthwiseprocurement/getretailproductiondetails/"
				+ productionPlanId;
		log.info("<--- getRegionandCategoryDetailsFromPlan() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		if (baseDTO != null && baseDTO.getResponseContents() != null) {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContents());
			retailProductionPlanDetailsList =  objectMapper.readValue(jsonResponse, new TypeReference<List<RetailProductionPlanDetails>>() { });
		}
		
		for (RetailProductionPlanDetails  retailProductionPlanDetails : retailProductionPlanDetailsList) {
			categoryId.add(retailProductionPlanDetails.getProductCategory().getId());
		}
		
			/*for (ProductCategory productCategory : retailProductionPlan.getProductCategoryList()) {
			if(productCategory.getId()!=null && !(categoryId.contains(productCategory.getId()))) {
				RetailProductionPlanDetails retailProductionPlanDetails=new RetailProductionPlanDetails();
				retailProductionPlanDetails.setProductCategory(productCategory);
				retailProductionPlanDetails.setTotalEligibilityPp(0.0);
				retailProductionPlanDetailsList.add(retailProductionPlanDetails);
				}
			}*/
		

	}
	catch(Exception ex) {
		log.info("<===getProductWiseTotalEligibilityValue Method End===>",ex);
	}
	log.info("<===getProductWiseTotalEligibilityValue Method End===>");
	return null;
}
}
