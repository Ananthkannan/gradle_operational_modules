package in.gov.cooptex.personnel.admin.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeConfirmation;
import in.gov.cooptex.core.model.EmployeeConfirmationDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.hrms.dto.EmployeeConfirmationDto;
import in.gov.cooptex.operation.hrms.dto.EmployeeConfirmationListDto;
import in.gov.cooptex.personnel.hrms.dto.CircularListDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeConfirmationBean")
public class EmployeeConfirmationBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9016498047119213397L;

	final String LIST_PAGE = "/pages/personnelHR/listEmployeeConfirmation.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/personnelHR/createEmployeeConfirmation.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/personnelHR/viewEmployeeConfirmation.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Setter
	@Getter
	List<Designation> designationList = new ArrayList<Designation>();

	@Setter
	@Getter
	List<Long> designationIds = new ArrayList<Long>();

	@Setter
	@Getter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	EmployeeConfirmationListDto selectedEmployeeConfirmationListDto = new EmployeeConfirmationListDto();

	@Getter
	@Setter
	List<EmployeeConfirmationDetails> employeeConfirmationList = new ArrayList<EmployeeConfirmationDetails>();

	@Getter
	@Setter
	List<EmployeeConfirmationListDto> employeeConfirmationDtoList = new ArrayList<EmployeeConfirmationListDto>();

	@Getter
	@Setter
	LazyDataModel<EmployeeConfirmationListDto> lazyEmployeeConfirmationList;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean previousApproval = false;

	@Getter
	@Setter
	String action;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String currentDate;

	@Getter
	@Setter
	List<UserMaster> userMasters = new ArrayList<UserMaster>();

	@Getter
	@Setter
	EmployeeConfirmationDto employeeConfirmationDto = new EmployeeConfirmationDto();
	
	@Getter
	@Setter
	List<EmployeeConfirmationDetails> employeeConfirmationDetailsList=new ArrayList<EmployeeConfirmationDetails>();
	
	@Getter
	@Setter
	Boolean buttonShowsCondition = false;
	
	@Getter
	@Setter
	Boolean approveButtonAction = false;

	@Getter
	@Setter
	Boolean submitButtonAction= false;
	
	@Getter
	@Setter
	Boolean editFieldAction= false;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Getter
	@Setter
	List<UserMaster> forwardToUsersList=new ArrayList<UserMaster>();
	
	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};
	
	@Autowired
	SystemNotificationBean systemNotificationBean;

	public String showEmployeeConfirmationListPage() {
		log.info("<==== Starts EmployeeConfirmationBean.showEmployeeConfirmationListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		loadLazyEmployeeConfirmationList();
		log.info("<==== Ends EmployeeConfirmationBean.showEmployeeConfirmationListPage =====>");
		return LIST_PAGE;
	}
	
public String designationClear()
{
	log.info("Enter into EmployeeConfirmationBean.designationClear:::::");
	employeeConfirmationList= new ArrayList<EmployeeConfirmationDetails>();
	designationIds = new ArrayList<Long>();
	log.info("Enter into EmployeeConfirmationBean.designationClear:::::");
	return ADD_PAGE;
}
	public void getEmployeesByDesignationIds() {
		log.info("Enter into EmployeeConfirmationBean.getEmployeesByDesignationIds:::::");
		try {
			employeeConfirmationList= new ArrayList<EmployeeConfirmationDetails>();
			String url = SERVER_URL + "/employee/getemployeebydesignationids";
			log.info("getEmployeesByDesignationIds Url is================>>>>>>>" + url);
			log.info("getEmployeesByDesignationIds designationIds is================>>>>>>>"+ designationIds.toString());
			if(!designationIds.isEmpty())
			{
				BaseDTO response = httpService.post(url, designationIds);
				if (response != null && response.getStatusCode() == 0) {
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info("employeeMasterList is================>>>>>>>" + employeeMasterList.toString());
					Iterator<EmployeeMaster> employeeMasterIterator = employeeMasterList.iterator();
					while (employeeMasterIterator.hasNext()) {
						EmployeeMaster employeeMaster = employeeMasterIterator.next();
						EmployeeConfirmationDetails employeeConfirmation = new EmployeeConfirmationDetails();
						employeeConfirmation.setEmployeeMaster(employeeMaster);
						employeeConfirmationList.add(employeeConfirmation);
					}
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			}
			else
			{
				log.info("designationIds is Empty================>>>>>>>");
				Util.addWarn("Please Choose Designation Id to Dropdown");
			}
		} catch (Exception e) {
			log.info("Exception occured in EmployeeConfirmationBean.getEmployeesByDesignationIds Exception is=>>>>>"
					+ e);
		}
		log.info("Exit into EmployeeConfirmationBean.getEmployeesByDesignationIds:::::");
	}

	public void loadLazyEmployeeConfirmationList() {
		log.info("<===== Starts EmployeeConfirmationBean.loadLazyEmployeeConfirmationList ======>");
		lazyEmployeeConfirmationList = new LazyDataModel<EmployeeConfirmationListDto>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<EmployeeConfirmationListDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/employeeconfirmation/getemployeeconfirmation";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeeConfirmationDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeConfirmationListDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in employeeConfirmationList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeeConfirmationDtoList;
			}

			@Override
			public Object getRowKey(EmployeeConfirmationListDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeConfirmationListDto getRowData(String rowKey) {
				try {
					for (EmployeeConfirmationListDto employeeConfirmationListDto : employeeConfirmationDtoList) {
						if (employeeConfirmationListDto.getId().equals(Long.valueOf(rowKey))) {
							selectedEmployeeConfirmationListDto = employeeConfirmationListDto;
							return employeeConfirmationListDto;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends EmployeeConfirmationBean.loadLazyEmployeeConfirmationList ======>");
	}

	public String showEmployeeConfirmationButtonAction() {
		log.info("<===== Starts EmployeeConfirmationBean.showEmployeeConfirmationButtonAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE") && (selectedEmployeeConfirmationListDto == null
					|| selectedEmployeeConfirmationListDto.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create GlAccountCategory called..");
				Date date = new Date();
				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
				currentDate = dateFormat.format(date);
				designationIds = new ArrayList<Long>();
				forwardToUsersList=commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_CONFIRMATION.name());
				designationList = commonDataService.loadActiveDesignations();
				editFieldAction= true;
				employeeConfirmationDto = new EmployeeConfirmationDto();
				userMaster = null;
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				employeeConfirmationDto.setId(selectedEmployeeConfirmationListDto.getId());
				employeeConfirmationDto.setSystemNotificationId(selectedNotificationId);
				if(selectedEmployeeConfirmationListDto.getStatus().equals(ApprovalStage.REJECTED))
				{
					editFieldAction= true;
				}
				forwardToUsersList=commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.EMPLOYEE_CONFIRMATION.name());
				employeeConfirmationList= new ArrayList<EmployeeConfirmationDetails>();
				log.info("edit Employee Confirmation called..");
				String url = SERVER_URL + "/employeeconfirmation/getById";
				log.info("Get By Id Url is==================>>>>>>>>>>>>"+url);
				BaseDTO response = httpService.post(url,employeeConfirmationDto);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					selectedEmployeeConfirmationListDto = mapper.readValue(jsonResponse, new TypeReference<EmployeeConfirmationListDto>() {
					});
					employeeConfirmationList=selectedEmployeeConfirmationListDto.getEmployeeConfirmationDetailsList();
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
				 
			}else if (action.equalsIgnoreCase("VIEW")) {
				employeeConfirmationDto.setId(selectedEmployeeConfirmationListDto.getId());
				employeeConfirmationDto.setSystemNotificationId(selectedNotificationId);
				log.info("view Employee Confirmation called..");
				String url = SERVER_URL + "/employeeconfirmation/getById";
				log.info("Get By Id Url is==================>>>>>>>>>>>>"+url);
				BaseDTO viewresponse = httpService.post(url,employeeConfirmationDto);
				if (viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					selectedEmployeeConfirmationListDto = mapper.readValue(jsonResponse, new TypeReference<EmployeeConfirmationListDto>() {
					});
					systemNotificationBean.loadTotalMessages();
					if(loginBean.getUserDetailSession().getId().equals(selectedEmployeeConfirmationListDto.getEmployeeConfirmationNote().getUserMaster().getId()))
					{
						buttonShowsCondition = true;
						approveButtonAction=true;
					}
					if(selectedEmployeeConfirmationListDto.getEmployeeConfirmationNote().getFinalApproval())
					{
						approveButtonAction=false;
					}
					if(selectedEmployeeConfirmationListDto.getEmployeeConfirmationLog().getStage().equals(ApprovalStage.FINAL_APPROVED) || selectedEmployeeConfirmationListDto.getEmployeeConfirmationLog().getStage().equals(ApprovalStage.REJECTED))
					{
						buttonShowsCondition = false;
					}
					commentAndLogList =viewresponse.getCommentAndLogList();
					return VIEW_PAGE;
				}

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in EmployeeConfirmationBean.showEmployeeConfirmationButtonAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EmployeeConfirmationBean.showEmployeeConfirmationButtonAction ===========>" + action);
		return null;
	}

	public String saveEmployeeConfirmation() {
		log.info("<===== Starts EmployeeConfirmationBean.saveEmployeeConfirmation ===========>");
		try {
			if(employeeConfirmationList.isEmpty())
			{
				log.info("====Employee Regularization List is Empty====");
				Util.addWarn("Employee Regularization List Cannot be Empty");
				return null;
			}
			if(StringUtils.isEmpty(employeeConfirmationDto.getNote())) {
				AppUtil.addWarn("Note is required");
				return null;
			}
			if (employeeConfirmationList != null) {
				for (EmployeeConfirmationDetails employeeConfirmationDetails : employeeConfirmationList) {
					if (employeeConfirmationDetails.getEmployeeMaster() != null
							&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment() != null
							&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
									.getDateOfConfirmation() != null
							&& employeeConfirmationDetails.getEffectFromDate() == null) {
						Date date = employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
								.getDateOfConfirmation();
						if (date.before(new Date())) {
							Util.addWarn("Please Select Effective From Date For Emloyee Code:" +employeeConfirmationDetails.getEmployeeMaster().getEmpCode());
							return null;
						}
					}
					if (employeeConfirmationDetails.getEmployeeMaster() != null
							&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment() != null
							&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
									.getDateOfConfirmation() != null
							&& employeeConfirmationDetails.getBasicPay() == null) {
						Date date = employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
								.getDateOfConfirmation();
						if (date.before(new Date())) {
							Util.addWarn("Please Enter Basic Pay For Emloyee Code:" +employeeConfirmationDetails.getEmployeeMaster().getEmpCode());
							return null;
						}
					}
				}
			}
			
			Date date = new Date();
			SimpleDateFormat month = new SimpleDateFormat("MMMM");
			if(action.equalsIgnoreCase("EDIT"))
			{
				employeeConfirmationDto.setId(selectedEmployeeConfirmationListDto.getId());
			}
			EmployeeConfirmation empConfirmation = new EmployeeConfirmation();
			empConfirmation.setCreatedBy(loginBean.getUserDetailSession());
			empConfirmation.setCreatedDate(new Date());
			empConfirmation.setVersion((long) 1);
			empConfirmation.setMonth(month.format(date));
			empConfirmation.setYear(Year.now().getValue());
			empConfirmation.setEmployeeCount(employeeConfirmationList.size());
			String url = SERVER_URL + "/employeeconfirmation/saveorupdate";
			log.info("saveEmployeeConfirmation Url is================>>>>>>>" + url);
			log.info("saveEmployeeConfirmation employeeConfirmationDto is================>>>>>>>"
					+ employeeConfirmationDto.toString());
			employeeConfirmationDto.setUserId(userMaster.getId());
			employeeConfirmationDto.setEmployeeConfirmationList(employeeConfirmationList);
			employeeConfirmationDto.setEmployeeConfirmation(empConfirmation);
			BaseDTO response = httpService.put(url, employeeConfirmationDto);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
				log.info("employeeMasterList is================>>>>>>>" + employeeMasterList.toString());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			log.info("Exception occured in EmployeeConfirmationBean.getEmployeesByDesignationIds Exception is=>>>>>"
					+ e);
		}
		log.info("Exit into EmployeeConfirmationBean.getEmployeesByDesignationIds:::::");

		log.info("<===== Ends EmployeeConfirmationBean.saveEmployeeConfirmation ===========>");
		return clear();
	}
	
	public String checkConformationDate() {
		if (employeeConfirmationList != null) {
			for (EmployeeConfirmationDetails employeeConfirmationDetails : employeeConfirmationList) {
				if (employeeConfirmationDetails.getEmployeeMaster() != null
						&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment() != null
						&& employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
								.getDateOfConfirmation() != null
						&& employeeConfirmationDetails.getEffectFromDate() == null) {
					Date date = employeeConfirmationDetails.getEmployeeMaster().getPersonalInfoEmployment()
							.getDateOfConfirmation();
					if (date.after(new Date())) {
						Util.addWarn("You are not eligible to conform For Emloyee Code:"+employeeConfirmationDetails.getEmployeeMaster().getEmpCode());
						return null;
					}
				}
			}
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeeConfirmationBean.onRowSelect ========>" + event);
		selectedEmployeeConfirmationListDto = ((EmployeeConfirmationListDto) event.getObject());
		addButtonFlag = false;
		deleteButtonFlag = true;
		if (selectedEmployeeConfirmationListDto.getStatus().equals("FINAL-APPROVED")
				|| selectedEmployeeConfirmationListDto.getStatus().equals("APPROVED")) {
			editButtonFlag = false;
		} else {
			editButtonFlag = true;
		}

		log.info("<===Ends EmployeeConfirmationBean.onRowSelect ========>");
	}
	
	public void buttonShowHide() {
		log.info("<===Starts EmployeeConfirmationBean.buttonShowHide ========>");
		if (employeeConfirmationDto.getUserId() != null) {
			approveButtonAction = false;
			submitButtonAction = true;
		} else {
			approveButtonAction = true;
			submitButtonAction = false;
		}
		log.info("<===End EmployeeConfirmationBean.buttonShowHide ========>");
	}
	
	public String clear()
	{
		log.info("<===Starts EmployeeConfirmationBean.clear ========>");
		selectedEmployeeConfirmationListDto =new EmployeeConfirmationListDto();
		employeeConfirmationList=new ArrayList<EmployeeConfirmationDetails>();
		designationList= new ArrayList<Designation>();
		log.info("<===Starts EmployeeConfirmationBean.clear ========>");
		return showEmployeeConfirmationListPage();
	}
	
	public String approveReject()
	{
		log.info("<===Starts EmployeePromotionRequestBean.approveReject ========>");
		if (employeeConfirmationDto.getApproveReject()==null || employeeConfirmationDto.getApproveReject().equals("APPROVE")) {
			if (userMaster == null || userMaster.getId()==null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (employeeConfirmationDto.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
		}
		/*if (employeeConfirmationDto.getApproveReject().equals("REJECT")) {
			if (employeeConfirmationDto.getNote() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}
		}*/
		employeeConfirmationDto.setEmployeeConfirmationList(selectedEmployeeConfirmationListDto.getEmployeeConfirmationDetailsList());
		employeeConfirmationDto.setId(selectedEmployeeConfirmationListDto.getId());
		employeeConfirmationDto.setNote(selectedEmployeeConfirmationListDto.getEmployeeConfirmationNote().getNote());
		employeeConfirmationDto.setUserId(userMaster.getId());
		String url = SERVER_URL + "/employeeconfirmation/approvereject";
		log.info("approveReject Url is==================>>>>>>>>>>>>"+url);
		BaseDTO response = httpService.post(url,employeeConfirmationDto);
		if (response != null && response.getStatusCode() == 0) {
			if(employeeConfirmationDto.getApproveReject().equals("REJECT"))
			{
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.EMPLOYEE_CONFIRMATION_REJECTED_SUCCESS).getErrorCode());
			}else
			{
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.EMPLOYEE_CONFIRMATION_APPROVED_SUCCESS).getErrorCode());
			}
			return clear();
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===End EmployeePromotionRequestBean.approveReject ========>");
		return null;
	}
	
	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedEmployeeConfirmationListDto = new EmployeeConfirmationListDto();
			action = "VIEW";
			selectedEmployeeConfirmationListDto.setId(Long.parseLong(id));
			selectedNotificationId = Long.parseLong(notificationId);
			action="VIEW";
			showEmployeeConfirmationButtonAction();
		}
		log.info("CircularBean showViewListPage() Ends");
		addButtonFlag = true;
		return VIEW_PAGE;
	}

}
