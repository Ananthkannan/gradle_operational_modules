package in.gov.cooptex.personnel.hrms.rest.ui;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.EmpOTRegister;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.hrms.dto.MonthDTO;
import in.gov.cooptex.operation.hrms.dto.OTRegisterDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("otRegisterBean")
public class OTRegisterBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/otregister";

	String ADD_PAGE = "/pages/personnelHR/payRoll/createOTRegister.xhtml?faces-redirect=true";

	String LIST_PAGE = "/pages/personnelHR/payRoll/listOTRegister.xhtml?faces-redirect=true";

	String VIEW_PAGE = "/pages/personnelHR/payRoll/viewOTRegister.xhtml?faces-redirect=true";

	ObjectMapper mapper;
	
	List<Long> otRegisterId = new ArrayList<>();
	
	OTRegisterDTO otRegisterDTO = new OTRegisterDTO();

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	EntityMaster headRegionOffice = new EntityMaster();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department department = new Department();

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList;

	@Getter
	@Setter
	List<SectionMaster> secMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<Integer> yearListData;

	@Getter
	@Setter
	Integer yearValue;

	@Getter
	@Setter
	String pageAction, jsonResponse;

	String url;

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	List<MonthDTO> monthList = new ArrayList<>();

	@Getter
	@Setter
	List<String> monthIdList = new ArrayList<>();

	@Getter
	@Setter
	Boolean activeStatus;

	@Getter
	@Setter
	LazyDataModel<EmpOTRegister> otRegisterLazyList;

	@Getter
	@Setter
	List<EmpOTRegister> empOTRegisterList;

	@Getter
	@Setter
	Integer totalRecords;

	@Getter
	@Setter
	EmpOTRegister selectedEmpOTRegister = new EmpOTRegister();

	@Getter
	@Setter
	EmpOTRegister empOTRegister = new EmpOTRegister();

	@PostConstruct
	public void init() {
		log.info("OTRegisterBean Init is executed.....................");
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadOTRegisterValues() {
		log.info("OTRegisterBean loadOTRegisterValues.....");
		try {
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			entityTypeList = commonDataService.getEntityTypeByStatus();
			departmentList = commonDataService.loadDepartmentList();
			yearListData = loadYearList();
			monthList = loadMonth();
		} catch (Exception e) {
			log.error("Exception in loadOTRegisterValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public List<Integer> loadYearList() {

		log.info("OTRegisterBean loadYearList.....");
		List<Integer> yearListData = new ArrayList<>();

		try {
			Calendar beginCalendar = Calendar.getInstance();
			beginCalendar.set(Calendar.YEAR, beginCalendar.get(Calendar.YEAR));
			int year = beginCalendar.get(Calendar.YEAR) - 1;
			for (int i = 0; i < 3; i++) {
				yearListData.add(year + i);
			}
			log.info("yearListData==>" + yearListData);

		} catch (Exception e) {
			log.error("Exception in PolicyNotesBean  :: loadYearList ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return yearListData;
	}

	public List<MonthDTO> loadMonth() {
		log.info("OTRegisterBean loadMonth.....");
		List<MonthDTO> monthsList = new ArrayList<>();
		try {

			String[] months = new DateFormatSymbols().getMonths();
			for (int i = 0; i < months.length - 1; i++) {
				String month = months[i];

				MonthDTO monthDTO = new MonthDTO();
				monthDTO.setMonthId(i + 1);
				monthDTO.setMonthName(month);

				System.out.println("month ===> " + month);
				monthsList.add(monthDTO);
			}
		} catch (Exception e) {
			log.error("Exception in PolicyNotesBean  :: loadMonth ==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return monthsList;
	}

	public String loadOTRegister() {
		log.info("OTRegisterBean loadOTRegister.....");
		try {
			otRegisterId = new ArrayList<>();
			otRegisterDTO = new OTRegisterDTO();
			if (pageAction.equalsIgnoreCase("LIST")) {
				loadLazyOTRegisterList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				loadOTRegisterValues();
				return ADD_PAGE;
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				log.info(":: OTRegisterBean Edit Response :: ");
				loadOTRegisterValues();
				if (selectedEmpOTRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return editOTRegister();
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				log.info(":: OTRegisterBean View Response :: ");
				if (selectedEmpOTRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewOTRegister();
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: OTRegisterBean Delete Response :: ");
				if (selectedEmpOTRegister == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			}

		} catch (Exception e) {
			log.error("Exception in loadOTRegisterValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String editOTRegister() {
		try {
			if (selectedEmpOTRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected Employee  Id : : " + selectedEmpOTRegister.getId());
			String getUrl = SERVER_URL + "/get/id/:id";
			url = getUrl.replace(":id", selectedEmpOTRegister.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmpOTRegister otRegister = mapper.readValue(jsonResponse, EmpOTRegister.class);

			if (otRegister != null) {
				empOTRegister = otRegister;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				entityMaster = empOTRegister.getEntityMaster();
				if(entityMaster.getName().equals("HEAD_OFFICE")) {
					headRegionOffice = empOTRegister.getEntityMaster();
				}else {
					getHoRoOffice(empOTRegister.getId());
				}
				
				entityTypeMaster = commonDataService.getEntityType(entityMaster.getId());
				loadEntityList();
				department = empOTRegister.getDepartmentMaster();
				loadSecList();
				secMasterList.add(empOTRegister.getSectionMaster());
				yearValue = Integer.valueOf(empOTRegister.getOtYear().toString());
				monthIdList.add(empOTRegister.getOtMonth());
				activeStatus = empOTRegister.getActiveStatus();
				log.info(" OT Register Retrived  SuccessFully ");
				return ADD_PAGE;
			} else {
				log.error(" OT Register Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return null;

		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public String viewOTRegister() {
		try {
			if (selectedEmpOTRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected Employee  Id : : " + selectedEmpOTRegister.getId());
			String getUrl = SERVER_URL + "/get/id/:id";
			url = getUrl.replace(":id", selectedEmpOTRegister.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmpOTRegister otRegister = mapper.readValue(jsonResponse, EmpOTRegister.class);

			if (otRegister != null) {
				empOTRegister = otRegister;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				entityMaster = empOTRegister.getEntityMaster();
				getHoRoOffice(empOTRegister.getId());
				entityTypeMaster = commonDataService.getEntityType(entityMaster.getId());
				department = empOTRegister.getDepartmentMaster();
				secMasterList.add(empOTRegister.getSectionMaster());
				log.info(" OT Register Retrived  SuccessFully ");
				return VIEW_PAGE;
			} else {
				log.error(" OT Register Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return null;

		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public void getHoRoOffice(Long modId) {
		log.info("<--- Inside getDnpOfficeById() with D&P Office ID---> " + modId);

		String URL = SERVER_URL + "/gethorooffice/" + modId;
		log.info("<--- getHoRoOffice() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		try {
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					List<EntityMaster> entityMasterList = objectMapper.readValue(jsonResponse,
							new TypeReference<List<EntityMaster>>() {
							});
					if (entityMasterList != null && !entityMasterList.isEmpty()) {
						log.info("entityMasterList==>" + entityMasterList.size());
						headRegionOffice = entityMasterList.get(0);
					}else {
						headRegionOffice = empOTRegister.getEntityMaster();
						log.error("Entity master list null or empty");
					}

				} else {
					log.error("<--- Entity Not Found ---> ");
					errorMap.notify(ErrorDescription.ERROR_ENTITY_NOTFOUND.getCode());
				}
			} else {
				log.error("<--- Internal Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("<--- Failure Response ---> ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public String deleteOTRegister() {
		try {

			if (selectedEmpOTRegister == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected OT Register : : " + selectedEmpOTRegister.getId());
			String getUrl = SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedEmpOTRegister.getId().toString());
			log.info("deleteOTRegister Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("OT Register SuccessFully");
				
				loadLazyOTRegisterList();
				errorMap.notify(ErrorDescription.OT_REGISTER_DELETED_SICCESSFULLY.getErrorCode());
			} else {
				log.error("OT Register Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			otRegisterId = new ArrayList<>();
			otRegisterDTO = new OTRegisterDTO();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete OT Register  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "LIST_PAGE";
	}

	/*public void loadEntityList() {
		log.info("OTRegisterBean loadEntityList.........");
		try {

			if (headRegionOffice != null && headRegionOffice.getId() != null && entityTypeMaster != null
					&& entityTypeMaster.getId() != null) {
				Long regionId = headRegionOffice.getId();
				Long entityId = entityTypeMaster.getId();
				log.info("regionId==>" + regionId);
				log.info("entityId==>" + entityId);
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityId);
			} else {
				log.info("entity not found.");
			}
		} catch (Exception e) {
			log.error("Exception in loadOTRegisterValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=Ends OTRegisterBean loadEntityList =>");
	}*/
	
	public void loadEntityList() {
		log.info(":: Entity type change ::");
		try {

			if (Objects.isNull(headRegionOffice)
					|| Objects.isNull(entityTypeMaster)) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}

			if (Objects.isNull(headRegionOffice.getId())
					&& Objects.isNull(entityTypeMaster.getId())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}
			Long entityId = headRegionOffice.getId();
			Long entiyTypeId = entityTypeMaster.getId();
			log.info("loadEntityList :: entityId==> "+entityId);
			log.info("loadEntityList :: entiyTypeId==> "+entiyTypeId);
			String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/activeEntity/" + entityId + "/"
					+ entiyTypeId;
			log.info("URL request for entity :: " + resource);
			BaseDTO baseDto = httpService.get(resource);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			entityList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if(entityList != null && !entityList.isEmpty()) {
				log.info("Entity location list size is" + entityList.size());
			}else {
				log.error("entityList null or empty");
			}
			

		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void loadSecList() {
		log.info("OTRegisterBean loadSecList.........");
		try {
			Long depId = department.getId();
			log.info("depId==>" + depId);
			sectionMasterList = commonDataService.getActiveSectionMastersByDepartment(depId);
		} catch (Exception e) {
			log.error("Exception in loadSecList  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String saveOTRegister() {
		log.info("ModernizationBean saveModernizationRequest.........");
		try {

			if (headRegionOffice == null) {
				log.info("Invalid HO/RO");
				errorMap.notify(ErrorDescription.HO_RO_EMPTY.getErrorCode());
				return null;
			}
			if (entityTypeMaster == null) {
				log.info("Invalid ENTITY TYPE");
				errorMap.notify(ErrorDescription.WARN_EMP_ENTITY_TYPE_EMPTY.getErrorCode());
				return null;
			}
			if (entityMaster == null) {
				log.info("Invalid ENTITY");
				errorMap.notify(ErrorDescription.BIOMETRIC_ENTER_ENTITY_MASTER.getErrorCode());
				return null;
			}
			if (department == null) {
				errorMap.notify(ErrorDescription.DEPARTMENT_EMPTY.getErrorCode());
				return null;
			}
			if (secMasterList == null || secMasterList.isEmpty()) {
				errorMap.notify(ErrorDescription.SECTION_NOT_FOUND.getErrorCode());
				return null;
			}
			if (yearValue == null) {
				errorMap.notify(ErrorDescription.STAFF_PF_PAYMENT_YEAR_EMPTY.getErrorCode());
				return null;
			}
			if (monthIdList == null || monthIdList.isEmpty()) {
				errorMap.notify(ErrorDescription.EMP_PROMOTION_MONTH_IS_MANDATORY.getErrorCode());
				return null;
			}
			if (activeStatus == null) {
				errorMap.notify(ErrorDescription.OT_REGISTER_STATUS_EMPTY.getErrorCode());
				return null;
			}
			if (secMasterList.size() < monthIdList.size()) {
				errorMap.notify(ErrorDescription.OT_REGISTER_GREATER.getErrorCode());
				return null;
			}
			
			for(int s=0;s<secMasterList.size();s++) {
				if(s==0 && pageAction.equals("EDIT")) {
					otRegisterId.add(empOTRegister.getId());
				}else {
					otRegisterId.add(null); 
				}
			}
			
			otRegisterDTO.setOtRegisterId(otRegisterId);
			if(entityMaster != null && entityMaster.getCode() != null) {
				otRegisterDTO.setEntityMaster(entityMaster);
			}else {
				otRegisterDTO.setEntityMaster(headRegionOffice);
			}
			
			otRegisterDTO.setDepartment(department);
			otRegisterDTO.setSecMasterList(secMasterList);
			otRegisterDTO.setYearValue(yearValue);
			otRegisterDTO.setMonthIdList(monthIdList);
			otRegisterDTO.setStatus(activeStatus);

			String url = SERVER_URL + "/create";
			BaseDTO baseDTO = httpService.put(url, otRegisterDTO);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			if (baseDTO.getStatusCode() == 0) {
				otRegisterDTO = new OTRegisterDTO();
				otRegisterId = new ArrayList<>();
				clear();
				loadLazyOTRegisterList();
				if(pageAction.equals("ADD")) {
					errorMap.notify(ErrorDescription.OT_REGISTER_INSERTED_SICCESSFULLY.getErrorCode());
				}else {
					errorMap.notify(ErrorDescription.OT_REGISTER_UPDATED_SICCESSFULLY.getErrorCode());
				}
				return LIST_PAGE;
			} else {
				log.info("Error while saving ModernizationRequest with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in loadModernizationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void clear() {
		selectedEmpOTRegister = new EmpOTRegister();
		empOTRegister = new EmpOTRegister();
		entityMaster = new EntityMaster();
		department = new Department();
		secMasterList = new ArrayList<>();
		yearValue = null;
		monthIdList = new ArrayList<>();
		activeStatus = null;
		headRegionOffice = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
	}
	
	public String clearPage() {
		selectedEmpOTRegister = new EmpOTRegister();
		empOTRegister = new EmpOTRegister();
		entityMaster = new EntityMaster();
		department = new Department();
		secMasterList = new ArrayList<>();
		yearValue = null;
		monthIdList = new ArrayList<>();
		activeStatus = null;
		headRegionOffice = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		loadLazyOTRegisterList();
		return LIST_PAGE;
	}

	public void loadLazyOTRegisterList() {
		log.info("<==== Starts BiometricBean.loadLazyBiometricAttendaceList =====>");
		otRegisterLazyList = new LazyDataModel<EmpOTRegister>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EmpOTRegister> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/loadlazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						empOTRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<EmpOTRegister>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return empOTRegisterList;
			}

			@Override
			public Object getRowKey(EmpOTRegister res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpOTRegister getRowData(String rowKey) {
				for (EmpOTRegister empOTRegister : empOTRegisterList) {
					if (empOTRegister.getId().equals(Long.valueOf(rowKey))) {
						selectedEmpOTRegister = empOTRegister;
						return empOTRegister;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		selectedEmpOTRegister = ((EmpOTRegister) event.getObject());

		/*
		 * disableButton = true; if
		 * (selectedJobApplication.getStatus().equalsIgnoreCase("Approved")) {
		 * isAddButton = false; isApproveButton = false; isEditButton = false;
		 * isDeleteButton = false; } else if
		 * (selectedJobApplication.getStatus().equalsIgnoreCase("Rejected")) {
		 * isAddButton = false; isApproveButton = false; isEditButton = false;
		 * isDeleteButton = true; } else if
		 * (selectedJobApplication.getStatus().equalsIgnoreCase("InProgress")) {
		 * isAddButton = false; isApproveButton = true; isEditButton = true;
		 * isDeleteButton = true; }
		 */
	}
}
