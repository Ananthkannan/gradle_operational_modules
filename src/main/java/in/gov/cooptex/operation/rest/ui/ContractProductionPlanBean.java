package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ContractExportPlanRequest;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TaxCalculationUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GovtSchemePlanResponseDTO;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.ContractExportPlanBase;
import in.gov.cooptex.operation.enums.ContractExportPlanType;
import in.gov.cooptex.operation.enums.PlanStage;
import in.gov.cooptex.operation.model.ContractExportPlan;
import in.gov.cooptex.operation.model.ContractExportPlanDetails;
import in.gov.cooptex.operation.model.ContractExportPlanLog;
import in.gov.cooptex.operation.model.ContractExportPlanNote;
import in.gov.cooptex.operation.model.SalesOrder;
import in.gov.cooptex.operation.model.SalesOrderItems;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("contractProductionPlanBean")
@Scope("session")
public class ContractProductionPlanBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/operation/productionplan/contract/listContractProductionPlan.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/productionplan/contract/createContractProductionPlan.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/productionplan/contract/viewContractProductionPlan.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	List<Object> planBasedOnList;

	@Getter
	@Setter
	List<Object> planTypeList;

	@Getter
	@Setter
	ContractExportPlan contractExportPlan;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList;

	@Getter
	@Setter
	ContractExportPlanNote contractExportPlanNote;

	@Getter
	@Setter
	ContractExportPlanLog contractExportPlanLog;

	@Getter
	@Setter
	LazyDataModel<ContractExportPlanRequest> contractExportPlanRequestList;

	@Getter
	@Setter
	ContractExportPlanRequest selectedPlanRequest;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	boolean editButtonFlag = true;

	@Getter
	@Setter
	boolean viewPageFlag = false;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	List planTypeValues;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeMasterList;

	@Getter
	@Setter
	List<CustomerMaster> customerMasterList;

	@Setter
	@Getter
	ContractExportPlanRequest contractExportPlanRequest;

	ObjectMapper mapper;

	@Setter
	@Getter
	List<SalesOrder> salesOrderList;

	@Setter
	@Getter
	Double totalQuantity;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	Double totalCgstAmount;

	@Getter
	@Setter
	Double totalSgstAmount;

	@Getter
	@Setter
	Double totalTaxAmount;

	@Getter
	@Setter
	Double materialValue;

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	String[] headOfficeAddress;

	@Getter
	@Setter
	String[] billingAddress;

	@Getter
	@Setter
	String[] deliveryAddress;

	@Getter
	@Setter
	String[] salesOrderFromAddress;

	@Getter
	@Setter
	String[] salesOrderToAddress;

	@Setter
	@Getter
	ContractExportPlanRequest contractExportPlanRequestObj;

	@Setter
	@Getter
	String action;

	public String gotoPage(String page) {

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();
		action = page;
		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;
		} else {

			if (page.equals("ADD")) {
				return gotoAddPage();
			} else if (page.equals("EDIT")) {
				if (contractExportPlanRequestObj == null) {
					errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getCode());
					return null;
				}
				return gotoEditPage();
			} else if (page.equals("VIEW")) {
				if (contractExportPlanRequestObj == null) {
					errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getCode());
					return null;
				}
				return gotoViewPage();
			} else if (page.equals("LIST")) {
				contractExportPlanRequestObj = null;
				return getListPage();
			} else if (page.equals("DELETE")) {
				if (contractExportPlanRequestObj == null) {
					errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
				// return getListPage();
			} else {
				return null;
			}
		}
	}

	public String deleteContractProductionPlan() {
		try {
			if (contractExportPlanRequest == null) {
				errorMap.notify(ErrorDescription.VEHICLE_REGISTER_SELECT_ONE_RECORD.getCode());
				return null;
			}
			log.info("Contract emport plan request id==> " + contractExportPlanRequest.getId());
			/*
			 * String getUrl = SERVER_URL + "/joiningChecklist/delete/id/:id"; String url =
			 * getUrl.replace(":id",
			 * selectedJoiningChecklistDTO.getCheckListId().toString());
			 */
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/delete/" + contractExportPlanRequest.getId();
			log.info("deleteIncrement url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Contract Production Plan Deleted SuccessFully ");
				AppUtil.addInfo("Contract Production Plan Deleted SuccessFully");
				// errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_DELETED_SUCCESSFULLY).getCode());
				return getListPage();
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While delete Contract Production Plan  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private String getListPage() {

		selectedPlanRequest = new ContractExportPlanRequest();

		statusValues = Arrays.asList((Object[]) PlanStage.class.getEnumConstants());

		planTypeValues = Arrays.asList((Object[]) ContractExportPlanType.class.getEnumConstants());

		loadLazyPlanList();

		return LIST_PAGE_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		contractExportPlanRequest = ((ContractExportPlanRequest) event.getObject());
		addButtonFlag = true;
		contractExportPlanRequestObj = contractExportPlanRequest;
		if (contractExportPlanRequest.getPlanStage().equals(PlanStage.INITIATED)
				|| contractExportPlanRequest.getPlanStage().equals(PlanStage.REJECTED)) {
			editButtonFlag = false;
		} else {
			editButtonFlag = true;
		}
	}

	public void onPageLoad() {
		log.info("onPageLoad method started");
		addButtonFlag = false;
	}

	public void loadLazyPlanList() {

		log.info("loadLazyPlanList");

		contractExportPlanRequestList = new LazyDataModel<ContractExportPlanRequest>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<ContractExportPlanRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<ContractExportPlanRequest> data = new ArrayList<ContractExportPlanRequest>();

				try {

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<ContractExportPlanRequest>>() {
					});

					if (data != null) {

						this.setRowCount(baseDTO.getTotalRecords());

						log.info("List Count " + baseDTO.getTotalRecords());

						size = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error in loadLazyPlanList()  ", e);
				}
				return data;

			}

			@Override
			public Object getRowKey(ContractExportPlanRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ContractExportPlanRequest getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<ContractExportPlanRequest> responseList = (List<ContractExportPlanRequest>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (ContractExportPlanRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						contractExportPlanRequest = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		ContractExportPlanRequest contractExportPlanRequest = new ContractExportPlanRequest();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		contractExportPlanRequest.setPaginationDTO(paginationDTO);

		log.info("request  " + contractExportPlanRequest);

		contractExportPlanRequest.setFilters(filters);

		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/search";

			baseDTO = httpService.post(URL, contractExportPlanRequest);

		} catch (Exception e) {

			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());

			log.error("Exception in getSearchData() ", e);

		}

		return baseDTO;
	}

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	ContractExportPlanNote lastNote = new ContractExportPlanNote();

	private String gotoViewPage() {

		if (selectedPlanRequest == null || selectedPlanRequest.getId() == null) {
			errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_SELECT_ONE_RECORD.getCode());
			return null;
		}

		contractExportPlan = commonDataService.getContractExportPlan(selectedPlanRequest);
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PRODUCTION_PLAN_");

		if (selectedPlanRequest.getNotificationId() != null) {
			systemNotificationBean.loadTotalMessages();
		}
		eventLogList = new ArrayList<>();
		approvedEventLogList = new ArrayList<>();
		rejectedEventLogList = new ArrayList<>();

		if (contractExportPlan != null && contractExportPlan.getContractExportPlanLogList() != null
				&& !contractExportPlan.getContractExportPlanLogList().isEmpty()) {
			for (ContractExportPlanLog planLog : contractExportPlan.getContractExportPlanLogList()) {
				EventLog eventLog = new EventLog("", planLog.getCreatedByName(), planLog.getCreatedByDesignation(),
						planLog.getCreatedDate(), planLog.getRemarks());

				if (planLog.getStage().equals(PlanStage.INITIATED) || planLog.getStage().equals(PlanStage.SUBMITTED)) {
					eventLog.setTitle("Created By");
				} else if (planLog.getStage().equals(PlanStage.REJECTED)) {
					eventLog.setTitle("Rejected By");
					rejectedEventLogList.add(eventLog);
				} else if (planLog.getStage().equals(PlanStage.APPROVED)) {
					eventLog.setTitle("Approved By");
					approvedEventLogList.add(eventLog);
				} else if (planLog.getStage().equals(PlanStage.FINAL_APPROVED)) {
					eventLog.setTitle("Final Approved By");
					approvedEventLogList.add(eventLog);
				}
				if (!planLog.getStage().equals(PlanStage.INITIATED)) {
					eventLogList.add(eventLog);
				}

			}
		}

		totalQuantity = 0D;

		if (contractExportPlan != null && contractExportPlan.getContractExportPlanDetails() != null
				&& !contractExportPlan.getContractExportPlanDetails().isEmpty()) {
			totalQuantity = contractExportPlan.getContractExportPlanDetails().stream()
					.collect(Collectors.summingDouble(ContractExportPlanDetails::getQuantity));
		}

		contractExportPlanNote = new ContractExportPlanNote();
		if (contractExportPlan.getContractExportPlanNoteList() != null
				&& !contractExportPlan.getContractExportPlanNoteList().isEmpty()
				&& contractExportPlan.getContractExportPlanNoteList().get(0) != null) {
			contractExportPlanNote.setNote(contractExportPlan.getContractExportPlanNoteList().get(0).getNote());
			contractExportPlanNote
					.setFinalApproval(contractExportPlan.getContractExportPlanNoteList().get(0).getFinalApproval());
			contractExportPlanNote
					.setForwardTo(contractExportPlan.getContractExportPlanNoteList().get(0).getForwardTo());

			lastNote = contractExportPlan.getContractExportPlanNoteList()
					.get(contractExportPlan.getContractExportPlanNoteList().size() - 1);
		}

		log.info("lastNote id---------------" + lastNote.getForwardTo().getId());
		log.info("userMaster id-------------" + loginBean.getUserMaster().getId());

		viewPageFlag = true;

		if (lastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
			enableFlag = true;
		}

		return VIEW_PAGE_URL;
	}

	public void viewCustomerDetails() {
		if (contractExportPlan.getSalesOrder() != null) {
			contractExportPlanRequest = new ContractExportPlanRequest();
			contractExportPlanRequest.setCustomerMaster(contractExportPlan.getSalesOrder().getCustomerMaster());
		}
	}

	private String gotoEditPage() {

		if (selectedPlanRequest == null || selectedPlanRequest.getId() == null) {
			errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (selectedPlanRequest.getPlanStage().equals(PlanStage.INITIATED)
				|| selectedPlanRequest.getPlanStage().equals(PlanStage.REJECTED)) {

			contractExportPlan = commonDataService.getContractExportPlan(selectedPlanRequest);

			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PRODUCTION_PLAN_");

			planTypeList = Arrays.asList((Object[]) ContractExportPlanType.class.getEnumConstants());

			planBasedOnList = Arrays.asList((Object[]) ContractExportPlanBase.class.getEnumConstants());

			eventLogList = new ArrayList<>();
			EventLog eventLog = new EventLog(loginEmployee, new Date());
			eventLog.setTitle("Created By");
			eventLogList.add(eventLog);

			onChangePlanBase();

			contractExportPlanNote = new ContractExportPlanNote();
			if (contractExportPlan.getContractExportPlanNoteList() != null
					&& !contractExportPlan.getContractExportPlanNoteList().isEmpty()
					&& contractExportPlan.getContractExportPlanNoteList().get(0) != null) {
				contractExportPlanNote.setNote(contractExportPlan.getContractExportPlanNoteList().get(0).getNote());
				contractExportPlanNote
						.setFinalApproval(contractExportPlan.getContractExportPlanNoteList().get(0).getFinalApproval());
				contractExportPlanNote
						.setForwardTo(contractExportPlan.getContractExportPlanNoteList().get(0).getForwardTo());
			}

			contractExportPlanLog = new ContractExportPlanLog();

			viewPageFlag = false;

		} else {
			errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_CANNOT_BE_EDITED.getCode());
			return null;
		}

		return ADD_PAGE_URL;
	}

	private String gotoAddPage() {

		planTypeList = Arrays.asList((Object[]) ContractExportPlanType.class.getEnumConstants());

		planBasedOnList = Arrays.asList((Object[]) ContractExportPlanBase.class.getEnumConstants());

		contractExportPlan = new ContractExportPlan();

		contractExportPlan.setContractExportPlanDetails(new ArrayList<>());

		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PRODUCTION_PLAN_");

		contractExportPlan.setContractExportPlanLogList(new ArrayList<>());

		contractExportPlanLog = new ContractExportPlanLog();

		contractExportPlan.setContractExportPlanNoteList(new ArrayList<>());

		contractExportPlanNote = new ContractExportPlanNote();

		eventLogList = new ArrayList<>();
		EventLog eventLog = new EventLog(loginEmployee, new Date());
		eventLog.setTitle("Created By");
		eventLogList.add(eventLog);

		viewPageFlag = false;

		return ADD_PAGE_URL;
	}

	public void onChangePlanBase() {

		log.info("onChangePlanBase [" + contractExportPlan + "]");

		if (contractExportPlan != null) {
			if (contractExportPlan.getPlanBase().equals(ContractExportPlanBase.Sales_Order)) {
				customerTypeMasterList = commonDataService.loadAllCustomerType();
				productCategoryList = null;
			} else {
				productCategoryList = commonDataService.loadProductCategories();
				customerTypeMasterList = null;
			}
			contractExportPlanRequest = new ContractExportPlanRequest();
		}
	}

	public void onSelectCustomerTypeMaster() {
		log.info("onSelectCustomerTypeMaster [" + contractExportPlanRequest + "]");

		if (contractExportPlanRequest != null) {
			contractExportPlanRequest.setCustomerMaster(null);
			contractExportPlanRequest.setSalesOrder(null);

			if (contractExportPlanRequest.getCustomerTypeMaster() != null
					&& contractExportPlanRequest.getCustomerTypeMaster().getId() != null) {
				customerMasterList = commonDataService
						.loadAllCustomerByCustomerType(contractExportPlanRequest.getCustomerTypeMaster().getId());
			}
		}
	}

	public void onSelectCustomerMaster() {
		log.info("onSelectCustomerMaster [" + contractExportPlanRequest + "]");

		if (contractExportPlanRequest != null) {

			contractExportPlanRequest.setSalesOrder(null);

			if (contractExportPlanRequest.getCustomerMaster() != null
					&& contractExportPlanRequest.getCustomerMaster().getId() != null) {
				salesOrderList = loadAllSalesOrderByCustomer(contractExportPlanRequest.getCustomerMaster().getId());
			}
		}

	}

	public void onSelectSalesOrder() {
		log.info("onSelectSalesOrder [" + contractExportPlanRequest + "]");
	}

	public void searchSalesOrderItems() {
		log.info("searchSalesOrderItems [" + contractExportPlanRequest + "]");

		if (contractExportPlanRequest != null && contractExportPlanRequest.getSalesOrder() != null
				&& contractExportPlanRequest.getSalesOrder().getId() != null) {
			List<SalesOrderItems> salesOrderItemsList = loadAllSalesOrderItemsBySalesOrder(
					contractExportPlanRequest.getSalesOrder().getId());

			if (salesOrderItemsList != null && !salesOrderItemsList.isEmpty()) {
				contractExportPlan.setContractExportPlanDetails(new ArrayList<>());
				salesOrderItemsList.stream().forEach(salesOrderItems -> {

					ContractExportPlanDetails details = new ContractExportPlanDetails();
					details.setProductVarietyMaster(salesOrderItems.getProductVarietyMaster());
					details.setQuantity((double) salesOrderItems.getItemQty());

					contractExportPlan.getContractExportPlanDetails().add(details);

				});

				contractExportPlan.setSalesOrder(contractExportPlanRequest.getSalesOrder());
				contractExportPlanRequest = new ContractExportPlanRequest();

				customerMasterList = new ArrayList<>();
				salesOrderList = new ArrayList<>();

			}
		}
	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");

		contractExportPlanRequest.setProductGroupMaster(null);
		contractExportPlanRequest.setProductVarietyMaster(null);
		contractExportPlanRequest.setQuantity(null);

		if (contractExportPlanRequest != null && contractExportPlanRequest.getProductCategory() != null) {
			log.info("Selected Product Category " + contractExportPlanRequest.getProductCategory().getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(contractExportPlanRequest.getProductCategory());
		} else {

			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");

		contractExportPlanRequest.setProductVarietyMaster(null);
		contractExportPlanRequest.setQuantity(null);

		if (contractExportPlanRequest != null && contractExportPlanRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + contractExportPlanRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(contractExportPlanRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");

		contractExportPlanRequest.setQuantity(null);

		if (contractExportPlanRequest != null && contractExportPlanRequest.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + contractExportPlanRequest.getProductVarietyMaster().getCode());
		}
	}

	public void addProduct() {

		if (contractExportPlanRequest != null && contractExportPlanRequest.getProductVarietyMaster() != null) {
			ContractExportPlanDetails details = new ContractExportPlanDetails();
			details.setProductVarietyMaster(contractExportPlanRequest.getProductVarietyMaster());
			details.setQuantity(contractExportPlanRequest.getQuantity());
			details.getProductVarietyMaster().setProductGroupMaster(contractExportPlanRequest.getProductGroupMaster());
			details.getProductVarietyMaster().getProductGroupMaster()
					.setProductCategory(contractExportPlanRequest.getProductCategory());
			details.setProductGroupMaster(contractExportPlanRequest.getProductGroupMaster());
			details.setProductCategory(contractExportPlanRequest.getProductCategory());
			contractExportPlan.getContractExportPlanDetails().add(details);
			contractExportPlanRequest = new ContractExportPlanRequest();
		}

	}

	public void removeProduct(ContractExportPlanDetails details) {
		contractExportPlan.getContractExportPlanDetails().remove(details);

		if (details.getContractExportPlan().getId() != null) {
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/removeData/" + details.getId();
			log.info("deleteIncrement url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Contract Production Plan Deleted SuccessFully ");
				AppUtil.addInfo("Contract Production Plan Deleted SuccessFully");
				// errorMap.notify(ErrorDescription.getError(MastersErrorCode.JOINING_CHECKLIST_DELETED_SUCCESSFULLY).getCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		}

	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + contractExportPlanNote.getNote());
		log.info("=========================================");
	}

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	String finalApproveComments;

	public String statusUpdate(String status) {
		log.info("StatusUpdate " + status + " " + approveComments + " " + rejectComments);

		ContractExportPlanRequest request = new ContractExportPlanRequest();
		request.setId(contractExportPlan.getId());

		if (status.equals("APPROVED")) {
			request.setComments(approveComments);
			request.setPlanStage(PlanStage.APPROVED);
			request.setNote(contractExportPlanNote.getNote());
			request.setIsFinalApproval(contractExportPlanNote.getFinalApproval());
			request.setForwardToId(contractExportPlanNote.getForwardTo().getId());
		} else if (status.equals("REJECTED")) {
			request.setComments(rejectComments);
			request.setPlanStage(PlanStage.REJECTED);
		} else {
			request.setComments(finalApproveComments);
			request.setPlanStage(PlanStage.FINAL_APPROVED);
		}

		return statusUpdate(request);
	}

	private List<SalesOrder> loadAllSalesOrderByCustomer(Long customerId) {

		log.info("Loading all sales order............ for customer id " + customerId);

		List<SalesOrder> salesOrderList = null;

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/salesorder/" + customerId;

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				salesOrderList = mapper.readValue(jsonResponse, new TypeReference<List<SalesOrder>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return salesOrderList;

	}

	@Getter
	@Setter
	SalesOrder salesOrder;

	public void getSalesOrderData(Long salesOrderId) {
		log.info("ContractProductionPlanBean.getSalesOrder method started");
		if (salesOrderId == null) {
			errorMap.notify(24011);
		}
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/salesorder/get/"
				+ salesOrderId;

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				try {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					salesOrder = mapper.readValue(jsonValue, SalesOrder.class);
					if (salesOrder.getHeadOffice() != null) {
						headOfficeAddress = AppUtil.prepareAddress(salesOrder.getHeadOffice().getAddressMaster());
					}
					if (salesOrder.getCustomerMaster() != null) {
						billingAddress = AppUtil.prepareAddress(salesOrder.getCustomerMaster().getAddressMaster());
						deliveryAddress = AppUtil
								.prepareAddress(salesOrder.getCustomerMaster().getShippingAddressMaster());
						salesOrderToAddress = AppUtil.prepareAddress(salesOrder.getCustomerMaster().getAddressMaster());
					}
					if (salesOrder.getEntityMaster() != null) {
						salesOrderFromAddress = AppUtil.prepareAddress(salesOrder.getEntityMaster().getAddressMaster());
					}

				} catch (IOException e) {
					log.error(e);
				}
			}
		} catch (Exception e) {
			log.error("Error in getSalesOrder", e);
			errorMap.notify(1);
		}

	}

	private List<SalesOrderItems> loadAllSalesOrderItemsBySalesOrder(Long salesOrderId) {

		log.info("Loading all sales order items............ for salesOrderId id " + salesOrderId);

		List<SalesOrderItems> salesOrderItemsList = null;

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/salesorderitems/" + salesOrderId;

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null && baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
				log.info(":::::::::baseDTO::::SUCCESS_RESPONSE:::::");
				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				salesOrderItemsList = mapper.readValue(jsonResponse, new TypeReference<List<SalesOrderItems>>() {
				});
				log.info(":::::::salesOrderItemsList:::::::::::" + salesOrderItemsList.size());
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				log.info(":::::::::baseDTO::::FAILURE_RESPONSE:::::");

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return salesOrderItemsList;

	}

	public String statusUpdate(ContractExportPlanRequest request) {
		String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/contract/plan/updatestatus";
		try {
			BaseDTO baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Status Updated Successfully");
					if (request.getPlanStage().equals(PlanStage.APPROVED)) {
						errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_PLAN_APPROVE_SUCCESSFULLY.getCode());
					} else if (request.getPlanStage().equals(PlanStage.REJECTED)) {
						errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_PLAN_REJECT_SUCCESSFULLY.getCode());
					} else if (request.getPlanStage().equals(PlanStage.FINAL_APPROVED)) {
						errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_PLAN_FINALAPPROVE_SUCCESSFULLY.getCode());
					}

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return getListPage();
	}

	public String create(boolean isSubmit) {

		log.info("create method is executing..[" + isSubmit + "]");

		if (contractExportPlanNote.getNote() == null || contractExportPlanNote.getNote().equals("")) {
			errorMap.notify(ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY.getCode());
			return null;
		}
		if (contractExportPlan.getContractExportPlanDetails() == null
				|| contractExportPlan.getContractExportPlanDetails().isEmpty()) {
			AppUtil.addWarn("Please add Product Details");
			return null;
		}

		if (contractExportPlan.getContractExportPlanLogList() == null) {
			contractExportPlan.setContractExportPlanLogList(new ArrayList<>());
		}

		if (isSubmit) {
			contractExportPlanLog.setStage(PlanStage.SUBMITTED);
		} else {

			contractExportPlanLog.setStage(PlanStage.INITIATED);

		}
		contractExportPlan.getContractExportPlanLogList().add(contractExportPlanLog);

		contractExportPlan.setContractExportPlanNoteList(new ArrayList<>());
		contractExportPlan.getContractExportPlanNoteList().add(contractExportPlanNote);

		String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/contract/plan/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, contractExportPlan);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					if (contractExportPlanLog.getStage().equals(PlanStage.INITIATED)) {
						AppUtil.addInfo("Contract production plan Saved successfully");
					} else if (contractExportPlanLog.getStage().equals(PlanStage.SUBMITTED)) {

						errorMap.notify(ErrorDescription.CONTRACT_PRODUCTION_PLAN_SAVED_SUCCESSFULLY.getCode());
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public void viewSalesOrder(boolean isCreatePage) {
		headOfficeAddress = new String[2];
		billingAddress = new String[2];
		deliveryAddress = new String[2];
		salesOrderFromAddress = new String[2];
		salesOrderToAddress = new String[2];
		salesOrder = new SalesOrder();
		gstWiseList = new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		amountInWords = "";
		materialValue = 0.0;
		totalTaxAmount = 0.0;
		Long salesOrderId = null;
		if (isCreatePage) {
			if (contractExportPlanRequest.getSalesOrder() != null) {
				salesOrderId = contractExportPlanRequest.getSalesOrder().getId();
			}
		} else {
			if (contractExportPlan.getSalesOrder() != null) {
				salesOrderId = contractExportPlan.getSalesOrder().getId();
			}
		}

		getSalesOrderData(salesOrderId);

		if (salesOrder != null && salesOrder.getSalesOrderItemsList() != null
				&& salesOrder.getSalesOrderItemsList().size() > 0) {
			calculateTax(salesOrder.getSalesOrderItemsList());
		}
		try {
			amountInWords = AmountToWordConverter.converter(
					String.valueOf(AppUtil.ifNullRetunZero(materialValue) + AppUtil.ifNullRetunZero(totalTaxAmount)));
		} catch (Exception e) {
			log.error("Error while amountInWords", e);
			errorMap.notify(1);
			return;
		}
	}

	public void calculateTax(List<SalesOrderItems> itemsList) {
		if (itemsList != null && itemsList.size() > 0) {

			List<GstSummeryDTO> gstList = new ArrayList<>();
			for (SalesOrderItems item : itemsList) {
				if (item.getProductVarietyMaster() != null) {
					StringBuilder productId = new StringBuilder(item.getProductVarietyMaster().getId().toString());
					gstList.addAll(commonDataService.loadTaxForProduct(productId,
							AppUtil.ifNullRetunZero(item.getItemAmount())));
				}
			}

			ItemDataWithGstDTO salesOrderItemWithGstDTO = new ItemDataWithGstDTO();
			salesOrderItemWithGstDTO = TaxCalculationUtil.calculateSalesOrderTax(gstList, itemsList,
					salesOrderItemWithGstDTO);
			gstWiseList = salesOrderItemWithGstDTO.getGstWiseList();
			gstPercentageWiseDTOList = salesOrderItemWithGstDTO.getGstPercentageWiseDTOList();
			calculateTotalValues(itemsList);
		}
	}

	public static List<SalesOrderItems> getOrderItem(Long productId, List<SalesOrderItems> salesOrderItemList) {
		List<SalesOrderItems> matchedItemList = new ArrayList<>();
		log.info("getOrderItem method started.");
		for (SalesOrderItems item : salesOrderItemList) {
			if (productId.equals(item.getProductVarietyMaster().getId())) {
				log.info("productIds : " + item.toString());
				matchedItemList.add(item);
			}
		}
		log.info("getOrderItem method finished.");
		return matchedItemList;
	}

	public void calculateTotalValues(List<SalesOrderItems> itemsList) {
		materialValue = TaxCalculationUtil.calculateSalesOrderMaterialValue(itemsList);
		totalCgstAmount = TaxCalculationUtil.calculateTotalCgstAmount(gstPercentageWiseDTOList);
		totalSgstAmount = TaxCalculationUtil.calculateTotalSgstAmount(gstPercentageWiseDTOList);
		totalTaxAmount = TaxCalculationUtil.calculateTotalTaxAmount(gstPercentageWiseDTOList);
	}

	public void fromDateChange() {
		log.info(":::::::::::Inside::::::fromDateChange:::::::::" + contractExportPlan.getFromDate());
		try {
			if (contractExportPlan.getToDate() != null) {
				if (contractExportPlan.getFromDate().compareTo(contractExportPlan.getToDate()) < 0) {
					log.info("inside condition from Date is Less than To Date");
				} else {
					log.info("inside condition from Date is Greater than To Date");
					contractExportPlan.setToDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in getRowData ...", e);
		}
	}

	public void planDateChangeWithDueDate() {
		log.info(":::::::::::Inside::::::planDateChangeWithDueDate:::::::::" + contractExportPlan.getToDate());
		try {
			if (contractExportPlan.getDueDate() != null) {
				if (contractExportPlan.getToDate().compareTo(contractExportPlan.getDueDate()) < 0) {
					log.info("inside condition ToDate is Less than DueDate");
				} else {
					log.info("inside condition ToDate is Greater than DueDate");
					contractExportPlan.setDueDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in getRowData ...", e);
		}
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String contractExportProductionId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			contractExportProductionId = httpRequest.getParameter("contractExportProductionId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		selectedPlanRequest = new ContractExportPlanRequest();
		if (!StringUtils.isEmpty(notificationId)) {
			selectedPlanRequest.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(contractExportProductionId)) {
			selectedPlanRequest.setId(Long.parseLong(contractExportProductionId));
			action = "VIEW";
			gotoViewPage();
		}
		return VIEW_PAGE_URL;
	}

}
