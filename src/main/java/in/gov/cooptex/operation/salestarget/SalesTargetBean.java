package in.gov.cooptex.operation.salestarget;

import java.io.Serializable;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.*;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.*;
import in.gov.cooptex.operation.model.ConcurrentAudit;
import in.gov.cooptex.operation.model.SalesTarget;
import in.gov.cooptex.operation.model.SalesTargetDetails;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

import org.apache.commons.collections.CollectionUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Log4j2
@Service("salesTargetBean")
@Scope("session")
public class SalesTargetBean implements Serializable {

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	private static final long serialVersionUID = -3692509443246631999L;

	final String REPORT_PAGE = "/pages/operation/salesTargetReport.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();// controller path

	final String SALES_REPORT_PAGE = "/pages/operation/createSalesTarget.xhtml?faces-redirect=true";

	final String SALES_REPORT_LIST_PAGE = "/pages/operation/listSalesTargetPage.xhtml?faces-redirect=true";

	@Autowired
	private AppPreference appPreference;

	@Getter
	@Setter
	List<EntityMaster> headRegionalOfficeList, headOrRegionalOfficeFrom, headOrRegionalOfficeTo;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductCategory productCategory;

	private ObjectMapper mapper;

	private String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<SalesTarget> lazySalesTargetList;

	List<SalesTarget> salesTargetList = null;

	@Getter
	@Setter
	SalesTarget selectedSalesTarget = new SalesTarget();

	String stage = null;

	@Getter
	@Setter
	int totalRecords = 0;

	private BaseDTO baseDTO;

	@Getter
	@Setter
	Map<String, Integer> value;

	@Autowired
	private HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String action;

	@Setter
	@Getter
	List<EntityMaster> regionCodeNameValuesList = new ArrayList<>();

	@Setter
	@Getter
	List<ProductCategoryDTO> categoryCodeNameValuesList = new ArrayList<>();

	@Setter
	@Getter
	List<EntityMaster> entityMasterList = new ArrayList<>();

	List<ProductCategoryDTO> productCategoryDTOTypeList = new ArrayList<>();

	@Getter
	@Setter
	List<Integer> yearListDropdown;

	///////
	@Setter
	@Getter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	public String selectedForwardTo;

	@Setter
	@Getter
	private String selectedForwardFor;

	@Setter
	@Getter
	private List<Integer> fromYearDropdown;

	@Setter
	@Getter
	private List<Integer> toYearDropdown;

	@Setter
	@Getter
	private int monthFromId;

	@Setter
	@Getter
	private int monthToId;

	@Setter
	@Getter
	private int regionCodeId;

	@Setter
	@Getter
	private int categoryCodeNameId;

	@Setter
	@Getter
	private String regionCode;
	//////

	@Getter
	@Setter
	ProductCategoryDTO productCategoryDTO = new ProductCategoryDTO();

	@Getter
	@Setter
	private Integer entitymasters;

	@Getter
	@Setter
	List<SalesDetailsDTO> salesDetailsTableDtos;

	@Getter
	@Setter
	EntityMaster headOrRegionFrom, entityFrom;

	@Getter
	@Setter
	EmployeeTransferDetails employeeTransfer = new EmployeeTransferDetails();

	@Getter
	@Setter
	private List<EntityTypeMaster> entityTypeMasterList, entityTypeFromList, entityTypeTo;

	String url;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	List<SalesTargetDetailsDTO> salesTargetDetailsDTOList;

	@Getter
	@Setter
	List<String> tableHeaders;

	@Getter
	@Setter
	Double targetAmount;

	@Getter
	@Setter
	FinancialYear financialYear;

	@Getter
	@Setter
	List<FinancialYear> financialYearList;

	@Getter
	@Setter
	List<String> financialYearLists = new ArrayList<>();

	@Getter
	@Setter
	List<ProductCategoryDTO> listOfProductCategoryDto = new ArrayList<>();

	@Getter
	@Setter
	SalesTargetDetailsDTO salesTargetDetailsRequestDto = new SalesTargetDetailsDTO();

	@Getter
	@Setter
	SalesTargetDetailsResponseDTO salesTargetDetailsResponseDTO;

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-yyyy");

	@Getter
	@Setter
	SalesTargetRequestDTO salesTargetRequestDTOSubmitButton;

	public String showListSalesTargetRegisterPage() {
		log.info("<===== Starts SalesTargetBean.showListSalesTargetRegisterPage() =====> ");
		mapper = new ObjectMapper();
		addButtonFlag = true;
		regionCodeNameDropDown();
		categoryCodeNameDropDown();
		monthListDropdwon();
		yearListDropdown();
		forwardToUsersList = commonDataService.loadForwardToUsers();
		headOrRegionalOfficeFrom = commonDataService.loadHeadAndRegionalOffice();

		// nullifying table data
		clearDynamicTable();

		log.info("<===== Ends SalesTargetBean.showListSalesTargetRegisterPage() ======> ");
		return REPORT_PAGE;
	}

	public String showListPageSalesTarget() {

		if (action.equalsIgnoreCase("CREATE")) {

			headOrRegionalOfficeFrom = commonDataService.loadHeadAndRegionalOffice();
			productCategoryList = commonDataService.loadProductCategories();
			productCategory = new ProductCategory();
			financialYear = null;
			employeeTransfer = new EmployeeTransferDetails();
			headOrRegionFrom = new EntityMaster();
			salesTargetDetailsDTOList = null;
			onEntityOfficeChange();
			getFinYear();
			return SALES_REPORT_PAGE;

		}
		return REPORT_PAGE;

	}

	public String onLoadSalesTargetRegisterPage() {
		log.info("<===== Starts SalesTargetBean.showListSalesTargetRegisterPage() =====> ");
		mapper = new ObjectMapper();
		addButtonFlag = true;
		regionCodeNameDropDown();
		categoryCodeNameDropDown();
		monthListDropdwon();
		yearListDropdown();
		forwardToUsersList = commonDataService.loadForwardToUsers();
		clearDynamicTable();
		loadLazySalesTargetList();

		log.info("<===== Ends SalesTargetBean.showListSalesTargetRegisterPage() ======> ");
		return SALES_REPORT_LIST_PAGE;
	}

	public void clearDynamicTable() {
		salesDetailsTableDtos = null;
		tableHeaders = null;
	}

	public void onEntityOfficeChange() {

		try {
			if (headOrRegionFrom.getId() != null) {
				headOrRegionFrom.setEntityTypeMaster(commonDataService.getEntityType(headOrRegionFrom.getId()));
			}
			if (Objects.isNull(headOrRegionFrom)) {
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else if (headOrRegionFrom.getEntityTypeMaster() != null
					&& headOrRegionFrom.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
				log.info("Selected Head office code is :" + headOrRegionFrom.getEntityTypeMaster().getEntityCode());

				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else {
				if (headOrRegionFrom.getEntityTypeMaster() != null) {

					url = AppUtil.getPortalServerURL() + "/loanandadvance/loadEntityTypeMaster";
					BaseDTO response = httpService.get(url);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						entityTypeFromList = mapper.readValue(jsonResponse,
								new TypeReference<List<EntityTypeMaster>>() {
								});

					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					log.info("Selected Regional office code is :"
							+ headOrRegionFrom.getEntityTypeMaster().getEntityCode());
				}

				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			}
		} catch (Exception e) {

		}
	}

	public void onEntityTypeChange() {
		log.info(":: Entity type change ::");
		try {

			if (Objects.isNull(headOrRegionFrom) || Objects.isNull(employeeTransfer.getEntityFrom())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}

			if (Objects.isNull(headOrRegionFrom.getId()) && Objects.isNull(employeeTransfer.getEntityFrom().getId())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}
			Long entityId = headOrRegionFrom.getId();
			Long entiyTypeId = employeeTransfer.getEntityFrom().getId();
			String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/entity/" + entityId + "/"
					+ entiyTypeId;
			log.info("URL request for entity :: " + resource);
			BaseDTO baseDto = httpService.get(resource);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if (entityMasterList != null) {
				log.info("Entity location list size is : " + entityMasterList.size());
			}
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	// For Region Dropdown
	private void regionCodeNameDropDown() {
		log.info("<===== Starts SalesTargetBean.regionCodeNameDropDown() ======> ");
		try {
			regionCodeNameValuesList = new ArrayList<>();
			String url = SERVER_URL + "/entitymaster/region/getall";// method url
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			List<EntityMaster> entityMasterTypeList = mapper.readValue(jsonResponse,
					new TypeReference<List<EntityMaster>>() {
					});
			if (entityMasterTypeList.size() == 0) {
				log.info(" court Case Type List is empty");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			regionCodeNameValuesList = entityMasterTypeList;
			log.info(" court Case Type List Successfully Completed");

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading court Case Type List :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends SalesTargetBean.regionCodeNameDropDown() ======> ");
	}

	// For category Code/Name Dropdown
	private void categoryCodeNameDropDown() {
		log.info("<===== Starts SalesTargetBean.categoryCodeNameDropDown() ======> ");
		try {
			categoryCodeNameValuesList = new ArrayList<>();
			String url = SERVER_URL + "/category/getAll";// method url
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			List<ProductCategoryDTO> productCategoryDTOTypeList = mapper.readValue(jsonResponse,
					new TypeReference<List<ProductCategoryDTO>>() {
					});
			if (productCategoryDTOTypeList.size() == 0) {
				log.info(" court Case Type List is empty");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			categoryCodeNameValuesList = productCategoryDTOTypeList;
			log.info(" court Case Type List Successfully Completed");

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading court Case Type List :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends SalesTargetBean.categoryCodeNameDropDown() ======> ");
	}

	// For Month Dropdown
	public void monthListDropdwon() {
		log.info("<===== Starts SalesTargetBean.monthListDropdwon() ======> ");
		value = Util.getMonthIdsByName();
		log.info("<===== Ends SalesTargetBean.monthListDropdwon() ======> ");
	}

	public void submit() {
		log.info("<=== Starts SalesTargetBean.submit");
		try {
			if (salesDetailsTableDtos != null && !salesDetailsTableDtos.isEmpty()) {
				salesTargetRequestDTOSubmitButton = new SalesTargetRequestDTO();
				salesTargetRequestDTOSubmitButton.setEntityMasterId(salesTargetDetailsRequestDto.getEntityMasterId());
				salesTargetRequestDTOSubmitButton.setFromMonth(salesTargetDetailsRequestDto.getFromMonth());
				salesTargetRequestDTOSubmitButton.setFromYear(salesTargetDetailsRequestDto.getFromYear());
				salesTargetRequestDTOSubmitButton.setToMonth(salesTargetDetailsRequestDto.getToMonth());
				salesTargetRequestDTOSubmitButton.setToYear(salesTargetDetailsRequestDto.getToYear());
				salesTargetRequestDTOSubmitButton.setSalesTargetDetailsList(new ArrayList<>());
				List<SalesTargetDetailsDTO> detailsDtoList = salesTargetRequestDTOSubmitButton
						.getSalesTargetDetailsList();

				salesDetailsTableDtos.forEach(salesDetailsDTO -> {
					for (Map.Entry<String, SalesData> dataEntry : salesDetailsDTO.getData().entrySet()) {

						SalesTargetDetailsDTO detailsDTO = new SalesTargetDetailsDTO();

						detailsDTO.setProductCategoryIds(salesDetailsDTO.getCategoryId().toString());
						detailsDTO.setEntityMasterId(salesTargetDetailsRequestDto.getEntityMasterId());

						String[] monthYearArray = dataEntry.getKey().split("-");
						detailsDTO.setTargetYear(Long.parseLong(monthYearArray[1]));
						detailsDTO.setTargetMonth(monthYearArray[0]);

						SalesData salesData = dataEntry.getValue();
						detailsDTO.setTargetAmount(salesData.getCurrentTarget());
						detailsDTO.setTargetPercentage(salesData.getPercentage().doubleValue());
						detailsDtoList.add(detailsDTO);
					}
				});

				BaseDTO response = httpService.post(
						SERVER_URL + appPreference.getOperationApiUrl() + "/salestarget/create",
						salesTargetRequestDTOSubmitButton);
				if (response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.SALES_TARGET_SAVE_SUCCESS.getErrorCode());
					log.info("Sales Target Saved Successful");
				} else {
					log.error("Sales Target Saved Failed");
					errorMap.notify(ErrorDescription.SALES_TARGET_SAVE_FAILURE.getErrorCode());
				}
			} else {
				errorMap.notify(ErrorDescription.REQUEST_OBJECT_SHOULD_NOT_EMPTY.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving Sales Target " + e.getMessage());
		}
		log.info("<=== Ends SalesTargetBean.submit");
	}

	// For Year DropDown
	public void yearListDropdown() {
		log.info("<===== Starts SalesTargetBean.yearListDropdown()=====> ");
		fromYearDropdown = Util.getNextSixYearList();
		log.info("<===== Ends SalesTargetBean.yearListDropdown() =====> ");
	}

	// For Year DropDown
	public void getNextSixYearListFromGivenYear() {
		log.info("<===== Starts SalesTargetBean.getNextSixYearListFromGivenYear()=====> ");
		if (salesTargetDetailsRequestDto.getFromYear() == null) {
			toYearDropdown = Util.getNextSixYearList();
		} else {
			toYearDropdown = Util
					.getNextSixYearListFromGivenYear((int) salesTargetDetailsRequestDto.getFromYear().longValue());
		}
		log.info("<===== Ends SalesTargetBean.getNextSixYearListFromGivenYear() =====> ");
	}

	// For Generate Button

	public void generateSalesTargetReport() {
		log.info("<====== Starts ClaimForExpoBean.generateSalesTargetReport() ====>");
		try {
			StringBuilder stringOfId = new StringBuilder();
			for (ProductCategoryDTO categoryDTO : listOfProductCategoryDto) {
				if (categoryDTO != null) {
					if (stringOfId.length() == 0) {
						stringOfId.append(categoryDTO.getId());
					} else {
						stringOfId.append(",");
						stringOfId.append(categoryDTO.getId());
					}
				}
			}
			salesTargetDetailsRequestDto.setProductCategoryIds(stringOfId.toString());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/salestarget/gesalestargetdetails";

			BaseDTO baseDTO = httpService.post(url, salesTargetDetailsRequestDto);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				salesTargetDetailsResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<SalesTargetDetailsResponseDTO>() {
						});
				if (salesTargetDetailsResponseDTO == null) {
					log.info(" SalesTargetDetails Response is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				} else {
					process();
					log.info("Getting SalesTargetDetails Successfully Completed");
				}
			} else {
				log.error("Getting SalesTargetDetails Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception Occurred while Loading SalesTargetDetails :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== Ends SalesTargetBean.generateSalesTargetReport() ====>");
	}

	// For Cancel Button
	public String cancelButtonForSalesTargetReport() {
		log.info("<===== Starts SalesTargetBean.cancelButtonForSalesTargetReport()=====> ");
		String result = REPORT_PAGE;
		nullifyingCreateTrainingRequestObject();
		log.info("<=== Ends EmpTrainingBean.cancelButtonForSalesTargetReport() ======>");
		return result;
	}

	private void nullifyingCreateTrainingRequestObject() {
		log.info("<===== Starts SalesTargetBean.nullifyingCreateTrainingRequestObject()=====> ");
		regionCodeId = 0;
		monthFromId = 0;
		monthToId = 0;
		categoryCodeNameId = 0;
		selectedForwardTo = null;
		selectedForwardFor = null;
		log.info("<===== Ends SalesTargetBean.nullifyingCreateTrainingRequestObject()=====> ");
	}

	// For Clear Button
	public void clearButton() {
		log.info("<===== Starts SalesTargetBean.clearButton()=====> ");
		salesTargetDetailsRequestDto = new SalesTargetDetailsDTO();
		listOfProductCategoryDto = new ArrayList<>();
		fromYearDropdown = Util.getNextSixYearList();
		toYearDropdown = null;
		log.info("<===== Ends SalesTargetBean.clearButton()=====> ");

	}

	public void updatePercentage(SalesDetailsDTO salesDetailsDTO, String month) {
		SalesData salesData = salesDetailsDTO.getDetails(month);
		Integer percent = salesData.getPercentage();
		Double previousSale = salesData.getPreviousSales();

		Double currentTarget = (previousSale * percent) / 100;
		salesData.setCurrentTarget(currentTarget);
	}

	public void getSalesReport() {
		log.info("======getSalesReport Start======");
		try {

			salesTargetDetailsRequestDto.setProductCategoryIds(productCategory.getProductCatCode());
			salesTargetDetailsRequestDto.setEntityMasterId(employeeTransfer.getTransferFrom().getId());
			salesTargetDetailsRequestDto.setFinancialYear(financialYear);
			String url = SERVER_URL + "/salesTargetReportController/getSalesTargerReport";
			baseDTO = httpService.put(url, salesTargetDetailsRequestDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				salesTargetDetailsDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<SalesTargetDetailsDTO>>() {
						});
			}
		} catch (Exception e) {
			log.info("======Exception is======", e);
		}
	}

	public void getFinYear() {
		log.info("======getFinYear Start======");
		try {
			String url = SERVER_URL + "/salesTargetReportController/getCurrentFinYear";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				financialYearList = mapper.readValue(jsonResponse, new TypeReference<List<FinancialYear>>() {
				});

			}
		} catch (Exception e) {
			log.info("======Exception is======", e);
		}
	}

	public String saveSalesTarget() {

		try {
			if (CollectionUtils.isEmpty(salesTargetDetailsDTOList)) {
				AppUtil.addWarn("Sales Target List Is Emplty");
				return null;
			}
			salesTargetDetailsRequestDto.setEntityMasterId(employeeTransfer.getTransferFrom().getId());
			salesTargetDetailsRequestDto.setRegionId(headOrRegionFrom);
			salesTargetDetailsRequestDto.setTargetAmount(targetAmount);
			salesTargetDetailsRequestDto.setFinancialYear(financialYear);
			salesTargetDetailsRequestDto.setSalesTargetDetailsDTOList(salesTargetDetailsDTOList);
			String url = SERVER_URL + "/salesTargetReportController/saveSalesTarget";
			baseDTO = httpService.post(url, salesTargetDetailsRequestDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				AppUtil.addInfo("Sales Target Saved Successfully");
				return SALES_REPORT_LIST_PAGE;
			} else {
				AppUtil.addError("InterNet Connection Failure");
			}
		} catch (Exception e) {
			log.info("======Exception is======", e);
		}
		return null;

	}

	public void loadLazySalesTargetList() {
		log.info("<===== Starts concurrentAuditBean.loadLazyConcurretAuditList ======>");
		lazySalesTargetList = new LazyDataModel<SalesTarget>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<SalesTarget> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/salesTargetReportController/getlazyload";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						salesTargetList = mapper.readValue(jsonResponse, new TypeReference<List<SalesTarget>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyConcurretAuditList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return salesTargetList;
			}

			@Override
			public Object getRowKey(SalesTarget res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SalesTarget getRowData(String rowKey) {
				try {
					for (SalesTarget salesTarget : salesTargetList) {
						if (salesTarget.getId().equals(Long.valueOf(rowKey))) {
							selectedSalesTarget = salesTarget;
							// stage = selectedSalesTarget.getActiveStatus();
							return salesTarget;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends UnitInspectionEntityBean.loadLazyUnitInspectionEntityList ======>");
	}

	public void clear() {
		selectedSalesTarget = null;
		financialYear = null;
		employeeTransfer = new EmployeeTransferDetails();
		salesTargetDetailsDTOList = null;
		productCategory = new ProductCategory();
		headOrRegionFrom = null;
	}

	public String cancel() {
		selectedSalesTarget = new SalesTarget();
		return SALES_REPORT_LIST_PAGE;

	}

	public void process() {

		// final list of data and header for view table
		salesDetailsTableDtos = new ArrayList<>();
		tableHeaders = new ArrayList<>();

		String fromMonth = salesTargetDetailsRequestDto.getFromMonth();
		if (fromMonth.length() == 1) {
			fromMonth = "0".concat(fromMonth);
		}
		String fromYear = salesTargetDetailsRequestDto.getFromYear().toString();

		String toMonth = salesTargetDetailsRequestDto.getToMonth();
		if (toMonth.length() == 1) {
			toMonth = "0".concat(toMonth);
		}
		String toYear = salesTargetDetailsRequestDto.getToYear().toString();

		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");

		Calendar beginCalendar = Calendar.getInstance();
		Calendar finishCalendar = Calendar.getInstance();
		try {
			beginCalendar.setTime(dateFormat.parse(fromMonth.concat("-").concat(fromYear)));
			finishCalendar.setTime(dateFormat.parse(toMonth.concat("-").concat(toYear)));
		} catch (ParseException e) {
			e.printStackTrace();
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		}

		// creating dynamic headers
		while (!beginCalendar.after(finishCalendar)) {
			// add one month to date per loop
			String date = dateFormat.format(beginCalendar.getTime());
			tableHeaders.add(date);
			beginCalendar.add(Calendar.MONTH, 1);
		}

		// Initializing empty value data for table
		// As no. of row as user selected no. of category
		// Map<categoryDTO.getId(),salesDetailsDTO>
		Map<Long, SalesDetailsDTO> detailsDTOMap = new HashMap<>();

		listOfProductCategoryDto.forEach(categoryDTO -> {
			SalesDetailsDTO salesDetailsDTO = new SalesDetailsDTO();

			salesDetailsDTO.setCategoryId(categoryDTO.getId());
			salesDetailsDTO.setProductCategoryCodeAndName(categoryDTO.getProductCatName());

			detailsDTOMap.put(categoryDTO.getId(), salesDetailsDTO);
		});

		List<SalesTargetDetails> salesTargetDetailsList = salesTargetDetailsResponseDTO.getSalesTargetDetailsList();
		List<SalesTargetAmountDTO> salesTargetAmountList = salesTargetDetailsResponseDTO.getSalesTargetAmountDTOList();

		// grouped previous sales grouped by category id.
		// Map<CategoryId, Map<Month-Year,Amount>>
		Map<Long, Map<String, Double>> salesTargetAmountListMonthWise = new HashMap<>();

		// calculate salesTargetAmountListMonthWise
		// note: adding 1 year while calculating monthAndYearSale
		Calendar calendar = Calendar.getInstance();
		if (salesTargetAmountList != null) {
			for (SalesTargetAmountDTO saleDto : salesTargetAmountList) {
				String monthAndYearSale;
				if (saleDto.getCreateDate() != null) {
					calendar.setTime(saleDto.getCreateDate());
					calendar.add(Calendar.YEAR, 1);
					monthAndYearSale = simpleDateFormat.format(calendar.getTime());
				} else {
					continue;
				}

				if (salesTargetAmountListMonthWise.containsKey(saleDto.getProductCategoryId())) {
					Map<String, Double> previousSaleTarget = salesTargetAmountListMonthWise
							.get(saleDto.getProductCategoryId());
					if (previousSaleTarget.containsKey(monthAndYearSale)) {
						Double prevAmt = previousSaleTarget.get(monthAndYearSale);
						previousSaleTarget.put(monthAndYearSale, saleDto.getSalesAmount() + prevAmt);
					} else {
						previousSaleTarget.put(monthAndYearSale, saleDto.getSalesAmount());
					}
				} else {
					Map<String, Double> previousSaleTarget = new HashMap<>();
					previousSaleTarget.put(monthAndYearSale, saleDto.getSalesAmount());
					salesTargetAmountListMonthWise.put(saleDto.getProductCategoryId(), previousSaleTarget);
				}
			}
		}
		// Setting all previous sales amount
		for (Map.Entry<Long, Map<String, Double>> entry : salesTargetAmountListMonthWise.entrySet()) {
			SalesDetailsDTO salesDetailsDTO = detailsDTOMap.get(entry.getKey());
			if (salesDetailsDTO != null) {
				for (Map.Entry<String, Double> mapEntry : entry.getValue().entrySet()) {
					SalesData salesData = salesDetailsDTO.getDetails(mapEntry.getKey());
					salesData.setPreviousSales(mapEntry.getValue());
				}
			}
		}

		if (salesTargetDetailsList != null) {
			for (SalesTargetDetails salesTargetDetails : salesTargetDetailsList) {
				salesTargetDetails.setTargetYear(salesTargetDetails.getTargetYear() + 1);
				String monthYearKey = salesTargetDetails.getTargetMonth().concat("-")
						.concat(salesTargetDetails.getTargetYear().toString());
				SalesDetailsDTO salesDetailsDTO = detailsDTOMap.get(salesTargetDetails.getCategoryId());
				if (salesDetailsDTO != null) {
					SalesData salesData = salesDetailsDTO.getDetails(monthYearKey);
					salesData.setPreviousTarget(salesTargetDetails.getTargetAmount());
				}
			}
		}

		// Final List of Data for Viewing
		for (Map.Entry<Long, SalesDetailsDTO> entry : detailsDTOMap.entrySet()) {
			salesDetailsTableDtos.add(entry.getValue());
		}
	}

	public class SalesDetailsDTO {

		@Setter
		@Getter
		private Long categoryId;

		@Setter
		@Getter
		private String productCategoryCodeAndName;

		@Getter
		private Map<String, SalesData> data = new HashMap<>();

		public void setData(String monthYear, SalesData salesData) {
			this.data.put(monthYear, salesData);
		}

		public SalesData getDetails(String monthYearKey) {
			if (this.data.containsKey(monthYearKey)) {
				return data.get(monthYearKey);
			} else {
				SalesData salesData = new SalesData(0.0, 0.0, 0, 0.0);
				data.put(monthYearKey, salesData);
				return salesData;
			}
		}

		public SalesData getDetailsNotForView(String monthYearKey) {
			if (this.data.containsKey(monthYearKey)) {
				return data.get(monthYearKey);
			} else {
				return null;
			}
		}
	}

	@Data
	public class SalesData {
		private Double previousTarget;
		private Double previousSales;
		private Integer percentage;
		private Double currentTarget;

		public SalesData() {
		}

		public SalesData(Double previousTarget, Double previousSales, Integer percentage, Double currentTarget) {
			this.previousTarget = previousTarget;
			this.previousSales = previousSales;
			this.percentage = percentage;
			this.currentTarget = currentTarget;
		}
	}
}
