package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AdditionalSocietyDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ContractSocietyDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.GovtSocietyDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.UserType;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
//import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TourProgramConstant;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import in.gov.cooptex.operation.dto.GovtSocietyPlanResponseDTO;
import in.gov.cooptex.operation.enums.GovtSocietyPlanStatus;
import in.gov.cooptex.operation.enums.PlanStage;
import in.gov.cooptex.operation.model.AdditionalProductionPlan;
import in.gov.cooptex.operation.model.AdditionalSocietyPlan;
import in.gov.cooptex.operation.model.AdditionalSocietyPlanDetails;
import in.gov.cooptex.operation.model.AdditionalSocietyPlanLog;
import in.gov.cooptex.operation.model.AdditionalSocietyPlanNote;
import in.gov.cooptex.operation.model.ContractExportPlan;
import in.gov.cooptex.operation.model.ContractExportPlanDetails;
import in.gov.cooptex.operation.model.ContractExportSocietyPlan;
import in.gov.cooptex.operation.model.ContractExportSocietyPlanDetails;
import in.gov.cooptex.operation.model.ContractExportSocietyPlanLog;
import in.gov.cooptex.operation.model.ContractExportSocietyPlanNote;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSchemePlanItems;
import in.gov.cooptex.operation.model.GovtSocietyPlan;
import in.gov.cooptex.operation.model.GovtSocietyPlanDetailsMaster;
import in.gov.cooptex.operation.model.GovtSocietyPlanLog;
import in.gov.cooptex.operation.model.GovtSocietyPlanNote;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.model.GovtSchemePlanLog;
import in.gov.cooptex.operation.production.model.RetailProductionPlanDetails;
import in.gov.cooptex.personnel.hrms.dto.CircularListDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
public class GovtSocietyWiseProductionPlanBean extends CommonBean {

	private final String LIST_PAGE_URL = "/pages/operation/retailproduction/plan/listGovtSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String LIST_CON_EXPORT_URL = "/pages/operation/retailproduction/plan/listConExportSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String LIST_ADDTIONAL_EXPORT_URL = "/pages/operation/retailproduction/plan/listAddtionalSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/retailproduction/plan/createGovtSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String ADD_ADDTIONAL_PAGE_URL = "/pages/operation/retailproduction/plan/createAdditionalSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/retailproduction/plan/viewGovtSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String VIEW_ADDTIONAL_PAGE_URL = "/pages/operation/retailproduction/plan/viewAddtionalSocietyProductionPlan.xhtml?faces-redirect=true;";

	@Autowired
	LoginBean loginBean;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<GovtSchemePlan> govtProductionPlanList;

	@Getter
	@Setter
	List<ContractExportPlan> contractExportPlanList = new ArrayList<>();

	@Getter
	@Setter
	GovtSchemePlan govtSchemePlan;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	String govtProductionPlan, planType, govtProductionPlanType;

	@Getter
	@Setter
	int conExportTotalRecord = 0;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	CircleMaster circleMaster = new CircleMaster();

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Setter
	@Getter
	UomMaster uomMaster = new UomMaster();

	@Setter
	@Getter
	List<UomMaster> uomMasterList;

	@Getter
	@Setter
	GovtSchemePlanItems govtSchemePlanItems;

	@Getter
	@Setter
	RetailProductionPlanDetails retailProductionPlanDetails;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	private Double totalCurrentReqQty = 0.00, totalOpeningStockQty = 0.00;

	@Getter
	@Setter
	GovtSocietyPlanNote planNote, lastNote = new GovtSocietyPlanNote();

	@Getter
	@Setter
	String planNoteDetails;

	@Getter
	@Setter
	ContractExportSocietyPlanNote contractExportPlanNote = new ContractExportSocietyPlanNote();

	@Getter
	@Setter
	ContractExportSocietyPlanNote contractExportlastNote = new ContractExportSocietyPlanNote();

	@Getter
	@Setter
	AdditionalSocietyPlanNote additionalSocietyPlanNote = new AdditionalSocietyPlanNote();

	@Getter
	@Setter
	private Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean previousApproval;

	@Getter
	@Setter
	AdditionalSocietyPlanLog additionalSocietyPlanLog = new AdditionalSocietyPlanLog();

	@Getter
	@Setter
	String noteText, approveComments, rejectComments, finalApproveComments;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	LazyDataModel<GovtSocietyPlanResponseDTO> govtSocietyPlanResponseDTOList;

	@Getter
	@Setter
	LazyDataModel<ContractExportSocietyPlan> contractExportSocietyPlanList;

	@Getter
	@Setter
	GovtSocietyPlanResponseDTO govtSocietyPlanResponseDTO;

	@Getter
	@Setter
	ContractExportSocietyPlan selectedContractExportSocietyPlan;

	@Getter
	@Setter
	GovtSocietyPlan govtSocietyPlan;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	Double totalQty;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	String action = null;

	ObjectMapper mapper;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	ContractExportPlan contractExportPlan = new ContractExportPlan();

	@Getter
	@Setter
	ContractExportSocietyPlan contractExportSocietyPlan = new ContractExportSocietyPlan();

	@Getter
	@Setter
	private Boolean editFlag = false, govConExportFlag = false, circleCodeFlag = true, qtyFlag = false,
			conExportTableFlag = false, additionalEditFlag = false;

	@Getter
	@Setter
	List<AdditionalProductionPlan> additionalProductionPlanList = new ArrayList<>();

	@Getter
	@Setter
	AdditionalProductionPlan additionalProductionPlan = new AdditionalProductionPlan();

	@Getter
	@Setter
	AdditionalSocietyPlan additionalSocietyPlan = new AdditionalSocietyPlan();

	@Getter
	@Setter
	AdditionalSocietyPlan selectedAdditionalSocietyPlan = new AdditionalSocietyPlan();

	@Getter
	@Setter
	LazyDataModel<AdditionalSocietyPlan> additionalSocietyPlanList;

	@Autowired
	CommonDataService commonDataSerive;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	List<GovtSocietyPlanResponseDTO> viewLogResponseList = new ArrayList<>();

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>");
		govtProductionPlan = null;
		govtSchemePlan = null;
		planType = null;
		editFlag = false;
		loadUrl();
		loadStatusValues();
		getDPOfficeCodeName();
		loadForwardToUsers();
		if (!UserType.SUPPLIER.equals(loginBean.getUserMaster().getUserType())) {
			loadEmployeeLoggedInUserDetails();
		}
		showViewListPage();
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getDPOfficeCodeName() {
		log.info("<--- Inside getDPOfficeCodeName() ---> ");

		try {
			log.info("user id==>" + loginBean.getUserMaster());
			if (loginBean.getUserMaster() != null) {
				String requestPath = SERVER_URL + "/govt/societyPlan/getDPOfficeCodeName/"
						+ loginBean.getUserMaster().getId();
				log.info("requestPath : " + requestPath);
				BaseDTO baseDTO = httpService.get(requestPath);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContent() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						entityMasterList = (List<EntityMaster>) mapper.readValue(jsonValue,
								new TypeReference<List<EntityMaster>>() {
								});
						if (entityMasterList != null && !entityMasterList.isEmpty()) {
							for (int j = 0; j < entityMasterList.size(); j++) {
								if (entityMasterList.get(j) != null && entityMasterList.get(j).getId() != null) {
									entityMaster = entityMasterList.get(j);
								}
							}
						}

						/*
						 * if (entityMaster != null) { log.info("entityMaster : " + entityMaster); }
						 * else { errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						 * log.info(" entityMaster is Empty "); }
						 */

					} else {
						errorMap.notify(baseDTO.getStatusCode());
						log.info(" Error Message: " + baseDTO.getErrorDescription());
					}
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					log.info("<--- Requested Service Error in entityMaster() ---> ");
				}
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in entityMaster() --->", e);
		}
	}

	public String getListPage() {
		editFlag = false;
		return LIST_PAGE_URL;
	}

	public String getAddPage(String processType) {
		log.info("getAddPage processType==>" + processType);
		if (processType.equalsIgnoreCase("Govt_Scheme")) {
			govtProductionPlan = processType;
		} else {
			govtProductionPlan = "";
		}
		govtProductionPlanType = processType;
		selectedContractExportSocietyPlan = new ContractExportSocietyPlan();
		govtSocietyPlanResponseDTO = new GovtSocietyPlanResponseDTO();
		govtSocietyPlan = null;
		govtSchemePlan = null;
		govtProductionPlanList = new ArrayList<GovtSchemePlan>();
		eventLogList = new ArrayList<>();
		approvedEventLogList = new ArrayList<>();
		rejectedEventLogList = new ArrayList<>();
		editFlag = false;
		circleMaster = null;
		conExportTableFlag = false;
		govConExportFlag = false;
		circleMasterList = new ArrayList<CircleMaster>();
		productCategory = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMaster = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		planNote = new GovtSocietyPlanNote();
		contractExportPlan = new ContractExportPlan();
		contractExportPlanNote = new ContractExportSocietyPlanNote();
		planNoteDetails = "";
		totalQty = 0.00;
		getDPOfficeCodeName();
		loadGovtProductionPlanApprovedPlanList();
		return ADD_PAGE_URL;
	}

	public String getViewPage() {
		return VIEW_PAGE_URL;
	}

	public void loadGovtProductionPlanApprovedPlanList() {

		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			govConExportFlag = true;
			contractExportPlanList = new ArrayList<>();
			loadGovtSchemePlanApprovedPlanList();
		} else {
			govConExportFlag = false;
			govtProductionPlanList = new ArrayList<GovtSchemePlan>();
			if (!govtProductionPlan.equalsIgnoreCase("")) {
				loadContactExportPlanApprovedPlanList(govtProductionPlan);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void loadContactExportPlanApprovedPlanList(String planType) {
		log.info("<--- Inside loadContactExportPlanApprovedPlanList() ---> ");
		try {
			circleMaster = null;
			circleMasterList = new ArrayList<CircleMaster>();
			productCategory = null;
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupMaster = null;
			productGroupMasterList = new ArrayList<ProductGroupMaster>();
			productVarietyMaster = null;
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			govtSchemePlanItems = null;
			supplierMasterList = new ArrayList<SupplierMaster>();
			totalCurrentReqQty = 0.00;
			totalOpeningStockQty = 0.00;
			String url = SERVER_URL + "/govt/societyPlan/getfinalapprovecontactexportplanlist/" + planType;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					contractExportPlanList = (List<ContractExportPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<ContractExportPlan>>() {
							});
					if (contractExportPlanList != null) {
						log.info("contractExportPlanList with Size of : " + contractExportPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadContactExportPlanApprovedPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					contractExportPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadGovtSchemePlanApprovedPlanList() {
		log.info("<--- Inside loadRetailProductionPlanFinalApprovedList() ---> ");
		circleMaster = null;
		circleMasterList = new ArrayList<CircleMaster>();
		productCategory = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMaster = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
//		String requestPath = SERVER_URL+ "/govt/societyPlan/getGovtproductionplanlist";
		String requestPath = SERVER_URL + "/govt/societyPlan/getgovtproductionplanlistnotinprocurrement";
		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					govtProductionPlanList = (List<GovtSchemePlan>) mapper.readValue(jsonValue,
							new TypeReference<List<GovtSchemePlan>>() {
							});

					if (govtProductionPlanList != null) {
						log.info("govtProductionPlanList with Size of : " + govtProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" govtProductionPlanList is Empty ");
						govtProductionPlanList = new ArrayList<GovtSchemePlan>();
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
					govtProductionPlanList = new ArrayList<GovtSchemePlan>();
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadRetailProductionPlanFinalApprovedList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadRetailProductionPlanFinalApprovedList() --->", e);
		}
	}

	public void loadCircleList() {
		if (govtProductionPlan.equals("Govt_Scheme")) {
			loadCircleByGovt();
			circleCodeFlag = true;
		} else {
			circleCodeFlag = false;

			loadProductCategoryByContractExport();
		}
	}

	public void loadProductCategoryByContractExport() {
		log.info("<--- Inside loadProductCategoryByContractExport() ---> ");
		try {
			productCategory = null;
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupMaster = null;
			productGroupMasterList = new ArrayList<ProductGroupMaster>();
			productVarietyMaster = null;
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			govtSchemePlanItems = null;
			supplierMasterList = new ArrayList<SupplierMaster>();
			totalCurrentReqQty = 0.00;
			totalOpeningStockQty = 0.00;
			if (contractExportPlan == null || contractExportPlan.getId() == null) {
				return;
			}
			log.info("contractExportPlan id==>" + contractExportPlan.getId() + contractExportPlan.getFromDate());
			String requestPath = SERVER_URL + "/govt/societyPlan/loadprdcategorylistbycontractexport/"
					+ contractExportPlan.getId();
			log.info("loadProductCategoryByContractExport requestPath==>" + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productCategoryList = (List<ProductCategory>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductCategory>>() {
							});

					if (productCategoryList != null) {
						log.info("productCategoryList with Size of : " + productCategoryList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productCategoryList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadProductCategoryByContractExport() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadProductCategoryByContractExport() --->", e);
		}
	}

	public void loadCircleByContractExport() {
		try {
			log.info("<--- Inside loadCircleByContractExport() ---> ");
			circleMaster = null;
			circleMasterList = new ArrayList<CircleMaster>();
			productCategory = null;
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupMaster = null;
			productGroupMasterList = new ArrayList<ProductGroupMaster>();
			productVarietyMaster = null;
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			govtSchemePlanItems = null;
			supplierMasterList = new ArrayList<SupplierMaster>();
			totalCurrentReqQty = 0.00;
			totalOpeningStockQty = 0.00;

			if (contractExportPlan == null || contractExportPlan.getPlanCode() == null) {
				return;
			}

			log.info("code==>" + contractExportPlan.getPlanCode());

			String requestPath = SERVER_URL + "/govt/societyPlan/loadcirclebycontractexport/"
					+ contractExportPlan.getPlanCode();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					circleMasterList = (List<CircleMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<CircleMaster>>() {
							});

					if (circleMasterList != null) {
						log.info("circleMasterList with Size of : " + circleMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" circleMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in circleMasterList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadCircleByContractExport() --->", e);
		}
	}

	public void loadCircleByGovt() {
		log.info("<--- Inside loadCircleList() ---> ");
		circleMaster = null;
		circleMasterList = new ArrayList<CircleMaster>();
		productCategory = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMaster = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		if (govtSchemePlan == null || govtSchemePlan.getId() == null) {
			return;
		}
		if (entityMaster == null || entityMaster.getId() == null) {
			return;
		}

		try {
			log.info("govtSchemePlan==>" + govtSchemePlan.getId());
			log.info("entityMaster==>" + entityMaster.getId());
			String requestPath = SERVER_URL + "/govt/societyPlan/loadCircleList/" + entityMaster.getId() + "/"
					+ govtSchemePlan.getId();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					circleMasterList = (List<CircleMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<CircleMaster>>() {
							});

					if (circleMasterList != null) {
						log.info("circleMasterList with Size of : " + circleMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" circleMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in circleMasterList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in circleMasterList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadPrdCategoryList() {
		log.info("<--- Inside loadPrdCategoryList() ---> ");

		productCategory = null;
		productCategoryList = new ArrayList<ProductCategory>();
		productGroupMaster = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		if (circleMaster == null || circleMaster.getId() == null) {
			return;
		}

		log.info("circle id==>" + circleMaster.getId());
		log.info("plan id==>" + govtSchemePlan.getId());

		try {
			String requestPath = SERVER_URL + "/govt/societyPlan/loadPrdCategoryList/" + circleMaster.getId() + "/"
					+ govtSchemePlan.getId();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productCategoryList = (List<ProductCategory>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductCategory>>() {
							});

					if (productCategoryList != null) {
						log.info("productCategoryList with Size of : " + productCategoryList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productCategoryList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadPrdCategoryList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadPrdCategoryList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadPrdGroupList() {
		log.info("<--- Inside loadPrdGroupList() ---> ");
		productGroupMaster = null;
		productGroupMasterList = new ArrayList<ProductGroupMaster>();
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		String requestPath = null;
		if (productCategory == null || productCategory.getId() == null) {
			return;
		}
		if (govtProductionPlan.equals("Govt_Scheme")) {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdGroupListByPlanAndCat/" + govtSchemePlan.getId() + '/'
					+ productCategory.getId() + "/" + govtProductionPlan;
		} else if ("Additional_Production_Plan".equals(govtProductionPlan)) {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdGroupListByPlanAndCat/"
					+ additionalProductionPlan.getId() + '/' + productCategory.getId() + "/" + govtProductionPlan;
		} else {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdGroupListByPlanAndCat/" + contractExportPlan.getId()
					+ '/' + productCategory.getId() + "/" + govtProductionPlan;
		}
		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productGroupMasterList = (List<ProductGroupMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductGroupMaster>>() {
							});

					if (productGroupMasterList != null) {
						log.info("productGroupMasterList with Size of : " + productGroupMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productGroupMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadPrdGroupList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadPrdGroupList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadPrdVarityList() {
		log.info("<--- Inside loadPrdVarityList() ---> ");
		productVarietyMaster = null;
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		String requestPath = null;
		if (productGroupMaster == null || productGroupMaster.getId() == null) {
			return;
		}
		if (govtProductionPlan.equals("Govt_Scheme")) {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdVarityListBygroupAndCat/" + govtSchemePlan.getId()
					+ '/' + productCategory.getId() + '/' + productGroupMaster.getId() + "/" + govtProductionPlan;
		} else if ("Additional_Production_Plan".equals(govtProductionPlan)) {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdVarityListBygroupAndCat/"
					+ additionalProductionPlan.getId() + '/' + productCategory.getId() + '/'
					+ productGroupMaster.getId() + "/" + govtProductionPlan;
		} else {
			requestPath = SERVER_URL + "/govt/societyPlan/loadPrdVarityListBygroupAndCat/" + contractExportPlan.getId()
					+ '/' + productCategory.getId() + '/' + productGroupMaster.getId() + "/" + govtProductionPlan;
		}

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productVarietyMasterList = (List<ProductVarietyMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductVarietyMaster>>() {
							});

					if (productVarietyMasterList != null) {
						log.info("productVarietyMasterList with Size of : " + productVarietyMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productVarietyMasterList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadPrdVarityList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadPrdVarityList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getUOM() {
		log.info("<--- Inside getUOM() ---> ");
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		govtSchemePlanItems = null;
		supplierMasterList = new ArrayList<SupplierMaster>();
		totalCurrentReqQty = 0.00;
		totalOpeningStockQty = 0.00;
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			return;
		}
		String requestPath = SERVER_URL + "/govt/societyPlan/getUOM/" + productVarietyMaster.getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					uomMasterList = (List<UomMaster>) mapper.readValue(jsonValue, new TypeReference<List<UomMaster>>() {
					});
					if (uomMasterList != null && !uomMasterList.isEmpty()) {
						for (int j = 0; j < uomMasterList.size(); j++) {
							if (uomMasterList.get(j) != null && uomMasterList.get(j).getId() != null) {
								uomMaster = uomMasterList.get(j);
							}
						}
					}

					if (uomMaster != null) {
						log.info("uomMaster : " + uomMaster);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" uomMaster is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getUOM() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getUOM() --->", e);
		}
	}

	public void generate() {

		log.info("<--- Inside generate() ---> ");
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			loadGovInfo();
		} else {
			loadContractExportInfo();
		}

	}

	public void loadContractExportInfo() {
		log.info("<--- Inside loadGovInfo() ---> ");
		govtSchemePlanItems = null;
		if ((productVarietyMaster == null || productVarietyMaster.getId() == null)
				&& (contractExportPlan == null || contractExportPlan.getId() == null)) {
			errorMap.notify(ErrorDescription.SELECT_PLAN_PRODUCT.getCode());
			return;
		}
		getItemsForQuantityByContract();
		getSuppliers();
	}

	public void getItemsForQuantityByContract() {
		log.info("<--- Inside getItemsForQuantityByContract() ---> ");

		try {
			qtyFlag = true;
			log.info("product variety id ---> " + productVarietyMaster.getId());
			log.info("contractExportPlan id ---> " + contractExportPlan.getId());

			String requestPath = SERVER_URL + "/govt/societyPlan/getitemsforquantitybycontract/"
					+ productVarietyMaster.getId() + "/" + contractExportPlan.getId();
			log.info("getItemsForQuantity requestPath ==> " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					Double totalQt = (Double) mapper.readValue(jsonValue, new TypeReference<Double>() {
					});
					if (totalQt != null) {
						totalQty = totalQt;
						log.info("totalQty : " + totalQty);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" totalQty is null ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getItemsForQuantity() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getItemsForQuantity() --->", e);
		}
	}

	public void loadGovInfo() {
		log.info("<--- Inside loadGovInfo() ---> ");
		govtSchemePlanItems = null;
		if ((productVarietyMaster == null || productVarietyMaster.getId() == null)
				&& (circleMaster == null || circleMaster.getId() == null)
				&& (govtSchemePlan == null || govtSchemePlan.getId() == null)) {
			errorMap.notify(ErrorDescription.SELECT_PLAN_CIRCLE_PRODUCT.getCode());
			return;
		}
		getItemsForQuantity();
		getSuppliers();
	}

	public void getItemsForQuantity() {
		log.info("<--- Inside getItemsForQuantity() ---> ");

		try {

			log.info("product variety id ---> " + productVarietyMaster.getId());
			log.info("gove plan ID ---> " + govtSchemePlan.getId());
			log.info("circle ID ---> " + circleMaster.getId());

			String requestPath = SERVER_URL + "/govt/societyPlan/getItemsForQuantity/" + govtSchemePlan.getId() + "/"
					+ circleMaster.getId() + "/" + productVarietyMaster.getId();
			log.info("getItemsForQuantity requestPath ==> " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					govtSchemePlanItems = (GovtSchemePlanItems) mapper.readValue(jsonValue,
							new TypeReference<GovtSchemePlanItems>() {
							});

					if (govtSchemePlanItems != null) {
						log.info("govtSchemePlanItems : " + govtSchemePlanItems);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" govtSchemePlanItems is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getItemsForQuantity() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getItemsForQuantity() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getSuppliers() {
		log.info("<--- Inside getSuppliers() ---> ");
		supplierMasterList = new ArrayList<SupplierMaster>();
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			return;
		}

		try {
			log.info("product variety id==>" + productVarietyMaster.getId());
			log.info("entityMaster id==>" + entityMaster.getId());

			String requestPath = SERVER_URL + "/govt/societyPlan/getSuppliers/" + entityMaster.getId() + "/"
					+ productVarietyMaster.getId();
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMasterList = (List<SupplierMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<SupplierMaster>>() {
							});

					if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
						log.info("supplierMasterList size : " + supplierMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" supplierMasterList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getSuppliers() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getSuppliers() --->", e);
		}
	}

	public void calculateTotalCurrentReqQty() {
		log.info("<=calculateTotalCurrentReqQty call=>");
		totalCurrentReqQty = 0.00;
		for (SupplierMaster sm : supplierMasterList) {
			log.info("totalCurrentReqQty :" + sm);
			if (sm != null && sm.getCurrentReqQty() != null) {
				totalCurrentReqQty = totalCurrentReqQty + sm.getCurrentReqQty();
			}
		}
		log.info("totalCurrentReqQty call=>" + totalCurrentReqQty);
	}

	public void calculateTotalOpeningStockQty() {
		totalOpeningStockQty = 0.00;
		for (SupplierMaster sm : supplierMasterList) {
			log.info("totalOpeningStockQty :" + sm);
			if (sm != null && sm.getOpeningStockQty() != null)
				totalOpeningStockQty = totalOpeningStockQty + sm.getOpeningStockQty();
		}
	}

	public String saveGovtSocietyPlan(String status) {
		log.info("saveGovtSocietyPlan Method Start=========>");
		try {
			if (planNoteDetails == null || planNoteDetails.equals("")) {
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY.getCode());
				return null;
			}

			if (govtProductionPlan != null && !govtProductionPlan.equals("")
					&& govtProductionPlan.equals("Govt_Scheme")) {
				return saveGovtSocietyPlanGov(status);
			} else {
				return saveGovtSocietyPlanConExport(status);
			}
		} catch (Exception ex) {
			log.error("Error in saveGovtSocietyPlan Method", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("saveGovtSocietyPlan Method End=========>");
		return null;
	}

	public String saveGovtSocietyPlanGov(String status) {
		log.info("from saveGovtSocietyPlan...........");
		GovtSocietyPlan govtSocietyPlans = new GovtSocietyPlan();
		List<GovtSocietyPlanDetailsMaster> govtSocietyPlanDetailsMasterList = new ArrayList<GovtSocietyPlanDetailsMaster>();
		GovtSocietyDTO govtSocietyDTO = new GovtSocietyDTO();

		/*
		 * if (totalOpeningStockQty == null || totalOpeningStockQty.equals("") ||
		 * totalOpeningStockQty.equals(0.00) || totalCurrentReqQty == null ||
		 * totalCurrentReqQty.equals("") || totalCurrentReqQty.equals(0.00)) {
		 * errorMap.notify(ErrorDescription.ENTER_OPENING_CURRENT_STOCK_QTY.getCode());
		 * return null; }
		 */
		if (!totalOpeningStockQty.equals(govtSchemePlanItems.getOpeningStockQuantity())) {
			errorMap.notify(ErrorDescription.TOT_SUPPLER_COMPARE_OPENING_STOCK_QTY.getCode());
			return null;
		}
		if (!totalCurrentReqQty.equals(govtSchemePlanItems.getCurrentYearProductionQuantity())) {
			errorMap.notify(ErrorDescription.TOT_SUPPLER_COMPARE_CURRENT_STOCK_QTY.getCode());
			return null;
		}

		if (entityMaster != null && entityMaster.getId() != null) {
			govtSocietyPlans.setEntityMaster(entityMaster);
		}
		if (govtSchemePlan != null && govtSchemePlan.getId() != null) {
			govtSocietyPlans.setGovtSchemePlan(govtSchemePlan);
		} else {
			errorMap.notify(ErrorDescription.SELECT_PLAN.getCode());
			return null;
		}
		if (productVarietyMaster != null && productVarietyMaster.getId() != null) {
			govtSocietyPlans.setProductVariety(productVarietyMaster);
		} else {
			errorMap.notify(ErrorDescription.SELECT_PRODUCT.getCode());
			return null;
		}
		if (circleMaster == null || circleMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_CIRCLE.getCode());
			return null;
		}
		log.info("planNoteDetails==>" + planNoteDetails);
		planNote.setNote(planNoteDetails);
		log.info("planNote : " + planNote);
		if (planNote == null || planNote.getNote() == null || planNote.getNote().equals("")
				|| planNote.getUserMaster() == null || planNote.getUserMaster().getId() == null
				|| planNote.getFinalApproval() == null || planNote.getFinalApproval().equals("")) {
			errorMap.notify(ErrorDescription.ADD_PLAN_NOTES.getCode());
			return null;
		}

		if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
			for (SupplierMaster sm : supplierMasterList) {
				if (sm != null && sm.getId() != null) {
					GovtSocietyPlanDetailsMaster govtSocietyPlanDetailsMaster = new GovtSocietyPlanDetailsMaster();
					govtSocietyPlanDetailsMaster.setCircleMaster(circleMaster);
					if (sm.getOpeningStockQty() != null)
						govtSocietyPlanDetailsMaster.setOpeningStockQty(sm.getOpeningStockQty());
					else
						govtSocietyPlanDetailsMaster.setOpeningStockQty(0.00);
					if (sm.getCurrentReqQty() != null)
						govtSocietyPlanDetailsMaster.setCurrentReqQty(sm.getCurrentReqQty());
					else
						govtSocietyPlanDetailsMaster.setCurrentReqQty(0.00);
					govtSocietyPlanDetailsMaster.setSupplierMaster(sm);
					govtSocietyPlanDetailsMasterList.add(govtSocietyPlanDetailsMaster);
				}
			}
		}

		if (govtSocietyPlans != null && govtSocietyPlanDetailsMasterList != null
				&& !govtSocietyPlanDetailsMasterList.isEmpty()) {
			govtSocietyPlans.setStage(status);
			govtSocietyDTO.setGovtSocietyPlan(govtSocietyPlans);
			govtSocietyDTO.setGovtSocietyPlanDetailsMasterList(govtSocietyPlanDetailsMasterList);
			govtSocietyDTO.setPlanNote(planNote);
		}

		String url = SERVER_URL + "/govt/societyPlan/submitSocietyPlan";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, govtSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Govt Society Plan Submitted successfully");
					errorMap.notify(ErrorDescription.SOCIETY_INSERTED_SUCCESSFULLY.getCode());
					showFdsList();
					errorMap.notify(8545);
				} else if (baseDTO.getStatusCode() == 999) {
					errorMap.notify(ErrorDescription.DUPLICATE_COMBINATION_NOT_ALLOWED.getCode());
					return null;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.SOCIETY_PLAN_NOT_SUBMITED.getCode());
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String saveGovtSocietyPlanConExport(String status) {

		log.info("from saveGovtSocietyPlanConExport...........");
		BaseDTO baseDTO = null;
		try {

			ContractExportSocietyPlan contractExportSocietyPlan = new ContractExportSocietyPlan();
			List<ContractExportSocietyPlanDetails> contractExportSocietyPlanList = new ArrayList<>();
			ContractSocietyDTO contractSocietyDTO = new ContractSocietyDTO();
			if (totalCurrentReqQty == null || totalCurrentReqQty.equals("") || totalCurrentReqQty.equals(0.00)) {
				errorMap.notify(ErrorDescription.ENTER_VALID_CURRENT_QTY.getCode());
				return null;
			}
			if (totalQty < totalCurrentReqQty) {
				AppUtil.addWarn("Current Requirement Quantity give between " + totalQty);
				return null;
			}

			if (entityMaster != null && entityMaster.getId() != null) {
				contractExportSocietyPlan.setEntityMaster(entityMaster);
			}
			if (contractExportPlan != null && contractExportPlan.getId() != null) {
				contractExportSocietyPlan.setContractExportPlan(contractExportPlan);
			} else {
				errorMap.notify(ErrorDescription.SELECT_PLAN.getCode());
				return null;
			}
			if (productVarietyMaster != null && productVarietyMaster.getId() != null) {
				contractExportSocietyPlan.setProductVariety(productVarietyMaster);
			} else {
				errorMap.notify(ErrorDescription.SELECT_PRODUCT.getCode());
				return null;
			}

			log.info("planNoteDetails==>" + planNoteDetails);
			contractExportPlanNote.setNote(planNoteDetails);
			log.info("contractExportPlanNote : " + contractExportPlanNote);
			if (contractExportPlanNote == null || contractExportPlanNote.getNote() == null
					|| contractExportPlanNote.getNote().equals("") || contractExportPlanNote.getUserMaster() == null
					|| contractExportPlanNote.getUserMaster().getId() == null
					|| contractExportPlanNote.getFinalApproval() == null
					|| contractExportPlanNote.getFinalApproval().equals("")) {
				errorMap.notify(ErrorDescription.ADD_PLAN_NOTES.getCode());
				return null;
			}

			if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
				for (SupplierMaster sm : supplierMasterList) {
					if (sm != null && sm.getId() != null) {
						ContractExportSocietyPlanDetails contractExportSocietyPlanDetails = new ContractExportSocietyPlanDetails();
						if (sm.getOpeningStockQty() != null) {
							contractExportSocietyPlanDetails.setOpeningStockQty(sm.getOpeningStockQty());
						} else {
							contractExportSocietyPlanDetails.setOpeningStockQty(0.00);
						}
						if (sm.getCurrentReqQty() != null) {
							contractExportSocietyPlanDetails.setCurrentReqQty(sm.getCurrentReqQty());
						} else {
							contractExportSocietyPlanDetails.setCurrentReqQty(0.00);
						}
						contractExportSocietyPlanDetails.setSupplierMaster(sm);
						contractExportSocietyPlanList.add(contractExportSocietyPlanDetails);
					}
				}
			}

			if (contractExportSocietyPlan != null && contractExportSocietyPlanList != null
					&& !contractExportSocietyPlanList.isEmpty()) {
				contractExportSocietyPlan.setStage(status);
				contractSocietyDTO.setContractExportSocietyPlan(contractExportSocietyPlan);
				contractSocietyDTO.setContractExportSocietyPlanList(contractExportSocietyPlanList);
				contractSocietyDTO.setPlanNote(contractExportPlanNote);
			}

			String url = SERVER_URL + "/govt/societyPlan/submitSocietyPlanByConExport";

			baseDTO = httpService.post(url, contractSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Contract/Export Society Plan Submitted successfully");
					if (status.equals("INITIATED")) {

						errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONTRACT_EXPORT_SAVE_SUCCESSFULLY)
								.getCode());
					} else if (status.equals("SUBMITTED")) {
						errorMap.notify(ErrorDescription.CONTRACT_EXPORT_INSERTED_SUCCESSFULLY.getCode());
					}

					loadLazyConExportSocietyPlanList();
					return LIST_CON_EXPORT_URL;
					// errorMap.notify(8545);
				} else if (baseDTO.getStatusCode() == 999) {
					// errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.DUPLICATE_COMBINATION_NOT_ALLOWED.getCode());
					return null;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.CONTRACT_EXPORT_SOCIETY_PLAN_NOT_SUBMITED.getCode());
					return null;
				}
			}
			return null;

		} catch (Exception e) {
			log.error("Error in submitDraftPlan", e);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}

	}

	public void test() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNoteDetails);
		log.info("=========================================");
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNoteDetails);
		log.info("=========================================");
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		// BaseDTO baseDTO = null;
		try {
			forwardToUsersList = commonDataSerive
					.loadForwardToUsersByFeature(AppFeatureEnum.GOVT_SOCIETY_WISE_PRODUCTION.toString());
			log.info("forwardToUsersList size()=====>" + forwardToUsersList.size());
			/*
			 * String url = appPreference.getPortalServerURL() +
			 * "/user/getallforwardtousers"; baseDTO = httpService.get(url); if (baseDTO !=
			 * null) { ObjectMapper mapper = new ObjectMapper(); String jsonResponse =
			 * mapper.writeValueAsString(baseDTO.getResponseContent()); forwardToUsersList =
			 * mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() { }); }
			 */
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;
		try {
			if (loginBean == null && loginBean.getUserMaster() == null && loginBean.getUserMaster().getId() == null) {
				return;
			}
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void onRowConExportSelect(SelectEvent event) {
		log.info("FDS Plan onRowConExportSelect method started");
		selectedContractExportSocietyPlan = ((ContractExportSocietyPlan) event.getObject());

	}

	public void onRowSelect(SelectEvent event) {
		log.info("FDS Plan onRowSelect method started");
		govtSocietyPlanResponseDTO = ((GovtSocietyPlanResponseDTO) event.getObject());
		// addButtonFlag = true;
	}

	private void loadStatusValues() {
		Object[] statusArray = GovtSocietyPlanStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status.toString().replace("_", "-"));
		}
		log.info("<--- statusValues ---> " + statusValues);
	}

	public String showFdsList() {
		log.info("Inside GovtSocietyWiseProductionPlanBean showList()>>>>>>>>>");
		loadLazyGovtSocietyPlanList();
		// prepareSchemePeriod();
		loadStatusValues();
		return LIST_PAGE_URL;
	}

	public String showConExportList() {
		log.info("Inside GovtSocietyWiseProductionPlanBean showConExportList()>>>>>>>>>");
		loadLazyConExportSocietyPlanList();
		return LIST_CON_EXPORT_URL;
	}

	public void loadLazyConExportSocietyPlanList() {

		contractExportSocietyPlanList = new LazyDataModel<ContractExportSocietyPlan>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<ContractExportSocietyPlan> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					ContractExportSocietyPlan request = new ContractExportSocietyPlan();

					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/govt/societyPlan/lazycontractexportsearch",
							request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					ObjectMapper mapper = new ObjectMapper();

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<ContractExportSocietyPlan> data = mapper.readValue(jsonResponse,
							new TypeReference<List<ContractExportSocietyPlan>>() {
							});
					if (data == null) {
						log.info(" :: Employee Interchange Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						conExportTotalRecord = baseDTO.getTotalRecords();
						log.info(":: load LazyCon ExportSocietyPlanList Successfully Completed ::");
					} else {
						log.error(":: loadLazyConExportSocietyPlanList Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading loadLazyConExportSocietyPlanList data ::",
							e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(ContractExportSocietyPlan res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ContractExportSocietyPlan getRowData(String rowKey) {
				List<ContractExportSocietyPlan> responseList = (List<ContractExportSocietyPlan>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (ContractExportSocietyPlan res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedContractExportSocietyPlan = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void loadLazyGovtSocietyPlanList() {
		log.info("<--- loadLazyPaymentVoucherList ---> ");
		planNote = new GovtSocietyPlanNote();
		govtSocietyPlanResponseDTOList = new LazyDataModel<GovtSocietyPlanResponseDTO>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<GovtSocietyPlanResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<GovtSocietyPlanResponseDTO> data = new ArrayList<GovtSocietyPlanResponseDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<GovtSocietyPlanResponseDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazyGovtSchemePlanList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(GovtSocietyPlanResponseDTO res) {
				return res != null ? res.getGovtSocietyPlanId() : null;
			}

			@Override
			public GovtSocietyPlanResponseDTO getRowData(String rowKey) {
				List<GovtSocietyPlanResponseDTO> responseList = (List<GovtSocietyPlanResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (GovtSocietyPlanResponseDTO res : responseList) {
					if (res.getGovtSocietyPlanId().longValue() == value.longValue()) {
						govtSocietyPlanResponseDTO = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		govtSocietyPlanResponseDTO = new GovtSocietyPlanResponseDTO();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		GovtSocietyPlan govtSocietyPlan = new GovtSocietyPlan();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		govtSocietyPlan.setPaginationDTO(paginationDTO);

		log.info("govtSocietyPlan  " + govtSocietyPlan);
		govtSocietyPlan.setFilters(filters);

		try {
			String govtSchemePlanSearchUrl = SERVER_URL + "/govt/societyPlan/search";
			log.info("Govt Scheme Plan Search Url " + govtSchemePlanSearchUrl);
			baseDTO = httpService.post(govtSchemePlanSearchUrl, govtSocietyPlan);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public String gotoViewPage(String screenType) {
		if (screenType.equals("Govt_Scheme")) {
			setPlanType(screenType);
			return gotoGovViewPage();
		} else {
			govtProductionPlan = null;
			return gotoConExportViewPage();
		}
	}

	public String gotoConExportViewPage() {
		log.info("<= gotoConExportViewPage =>");
		try {
			enableFlag = false;
			govtSocietyPlanResponseDTO = new GovtSocietyPlanResponseDTO();
			BaseDTO baseDTO = null;
			if (selectedContractExportSocietyPlan == null) {
				log.info("<---Please Select any one Plan--->");
				errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
				return null;
			}

			/*
			 * if(selectedContractExportSocietyPlan.getContractExportSocietyPlanLog().equals
			 * (GovtSocietyPlanStatus.SUBMITTED.toString()) ||
			 * selegoctedContractExportSocietyPlan.getContractExportSocietyPlanLog().equals(
			 * GovtSocietyPlanStatus.APPROVED.toString()) ||
			 * selectedContractExportSocietyPlan.getContractExportSocietyPlanLog().equals(
			 * GovtSocietyPlanStatus.REJECTED.toString())) {
			 * errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode()); return null; }
			 */

			String URL = SERVER_URL + "/govt/societyPlan/getcontractexport";

			log.info("<--- gotoConExportEditPage() URL ---> " + URL);
			baseDTO = httpService.post(URL, selectedContractExportSocietyPlan);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				contractExportSocietyPlan = objectMapper.readValue(jsonResponse, ContractExportSocietyPlan.class);

				editFlag = true;
				govConExportFlag = false;
				circleCodeFlag = false;
				qtyFlag = true;
				conExportTableFlag = true;
				govtProductionPlan = null;

				log.info("==>" + contractExportSocietyPlan.getContractExportPlan().getPlanType().name());

				if (selectedContractExportSocietyPlan.getNotificationId() != null) {
					systemNotificationBean.loadTotalMessages();
				}

				contractExportPlan = contractExportSocietyPlan.getContractExportPlan();
				contractExportPlanList = new ArrayList<>();
				contractExportPlanList.add(contractExportPlan);

				entityMaster = contractExportSocietyPlan.getEntityMaster();

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = contractExportSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = contractExportSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = contractExportSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				entityMaster = contractExportSocietyPlan.getEntityMaster();
				/*
				 * if(contractExportSocietyPlan.getContractExportPlanNoteList() != null &&
				 * !contractExportSocietyPlan.getContractExportPlanNoteList().isEmpty()) {
				 * contractExportlastNote =
				 * contractExportSocietyPlan.getContractExportPlanNoteList().get(
				 * contractExportSocietyPlan.getContractExportPlanNoteList().size()-1); }
				 */

				getUOM();
				getSuppliers();

				totalCurrentReqQty = 0.00;

				for (ContractExportPlanDetails schemeDetails : contractExportPlan.getContractExportPlanDetails()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getQuantity();

					}
				}
				totalQty = totalCurrentReqQty;

				contractExportPlanNote = contractExportSocietyPlan.getContractExportPlanNoteList().get(0);

				planNoteDetails = contractExportSocietyPlan.getContractExportPlanNoteList().get(0).getNote();

				loadForwardToUsers();

				contractExportlastNote = contractExportSocietyPlan.getContractExportPlanNoteList()
						.get(contractExportSocietyPlan.getContractExportPlanNoteList().size() - 1);
				log.info("contractExportlastNote ======> " + contractExportlastNote.getUserMaster().getId());
				if (contractExportlastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId()
						.longValue()) {
					enableFlag = true;
				}

				String employeeDatajsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = objectMapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<GovtSocietyPlanResponseDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size()
						+ viewLogResponseList);
				/*
				 * eventLogList = new ArrayList<>();
				 * 
				 * approvedEventLogList = new ArrayList<>();
				 * 
				 * rejectedEventLogList = new ArrayList<>();
				 * 
				 * if(contractExportSocietyPlan.getContractExportPlanLogList() != null &&
				 * !contractExportSocietyPlan.getContractExportPlanLogList().isEmpty()) {
				 * EmployeeMaster empDetails = null; for(ContractExportSocietyPlanLog planLog :
				 * contractExportSocietyPlan.getContractExportPlanLogList()) { String url =
				 * appPreference.getPortalServerURL() +
				 * "/employee/findempdetailsbyloggedinuser/"+ planLog.getCreatedBy().getId();
				 * 
				 * BaseDTO dto = httpService.get(url); if (dto != null && dto.getStatusCode() ==
				 * 0) { ObjectMapper maper = new ObjectMapper(); String jsonResp =
				 * maper.writeValueAsString(dto.getResponseContent()); empDetails =
				 * maper.readValue(jsonResp, EmployeeMaster.class); } else {
				 * AppUtil.addError(" Employee Details Not Found for the User " ); return null;
				 * } } }
				 * 
				 * log.info("eventLogList", eventLogList);
				 * 
				 * Collections.sort(eventLogList, new Comparator<EventLog>() { public int
				 * compare(EventLog m1, EventLog m2) { return
				 * m1.getTitle().compareTo(m2.getTitle()); } });
				 */
				setPlanType(contractExportPlan.getPlanType().name());
				return VIEW_PAGE_URL;

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String gotoGovViewPage() {
		log.info("Inside gotoGovEditPage()==>");
		enableFlag = false;

		BaseDTO baseDTO = null;

		if (govtSocietyPlanResponseDTO == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
			return null;
		}

		/*
		 * if(govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.
		 * SUBMITTED.toString()) ||
		 * govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.
		 * APPROVED.toString()) ||
		 * govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.
		 * REJECTED.toString())) {
		 * errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode()); return null; }
		 */

		try {
			editFlag = true;
			conExportTableFlag = false;
			govConExportFlag = true;

			String URL = SERVER_URL + "/govt/societyPlan/get/" + govtSocietyPlanResponseDTO.getGovtSocietyPlanId();

			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				govtSocietyPlan = objectMapper.readValue(jsonResponse, GovtSocietyPlan.class);

				log.info("===================== ");
				editFlag = true;
				govtProductionPlan = "Govt_Scheme";
				govConExportFlag = true;

				for (GovtSchemePlanLog schemeLog : govtSocietyPlan.getGovtSchemePlan().getGovtSchemePlanLogList()) {
					if (schemeLog != null && schemeLog.getCreatedBy() != null
							&& schemeLog.getStage().equals("FINAL_APPROVED")) {
						govtSocietyPlan.getGovtSchemePlan().setApprovedDate(schemeLog.getCreatedDate());
						govtSocietyPlan.getGovtSchemePlan().setApprovedBy(schemeLog.getCreatedBy());
						break;
					}
				}
				govtSchemePlan = govtSocietyPlan.getGovtSchemePlan();
				govtProductionPlanList = new ArrayList<GovtSchemePlan>();
				govtProductionPlanList.add(govtSchemePlan);

				entityMaster = govtSocietyPlan.getEntityMaster();

				circleMasterList = new ArrayList<CircleMaster>();
				circleMaster = govtSocietyPlan.getCircleMaster();
				circleMasterList.add(circleMaster);

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = govtSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = govtSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = govtSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				getUOM();
				getItemsForQuantity();
				getSuppliers();

				totalCurrentReqQty = 0.00;
				totalOpeningStockQty = 0.00;
				for (GovtSocietyPlanDetailsMaster schemeDetails : govtSocietyPlan.getGovtSocietyPlanDetailsList()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getCurrentReqQty();
						totalOpeningStockQty = totalOpeningStockQty + schemeDetails.getOpeningStockQty();
					}
				}
				planNote = govtSocietyPlan.getGovtSocietyPlanNoteList().get(0);
				log.info("note==>" + govtSocietyPlan.getGovtSocietyPlanNoteList().get(0).getNote());
				planNoteDetails = govtSocietyPlan.getGovtSocietyPlanNoteList().get(0).getNote();

				getEmployeeDetailsByUserId();
				loadForwardToUsers();
				lastNote = govtSocietyPlan.getGovtSocietyPlanNoteList()
						.get(govtSocietyPlan.getGovtSocietyPlanNoteList().size() - 1);
				log.info("lastNote ======> " + lastNote.getUserMaster().getId());
				if (lastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
					enableFlag = true;
				}

				/*
				 * eventLogList = new ArrayList<>();
				 * 
				 * approvedEventLogList = new ArrayList<>();
				 * 
				 * rejectedEventLogList = new ArrayList<>();
				 * 
				 * if(govtSocietyPlan.getGovtSocietyPlanLogList() != null &&
				 * !govtSocietyPlan.getGovtSocietyPlanLogList().isEmpty()) { EmployeeMaster
				 * empDetails = null; for(GovtSocietyPlanLog planLog :
				 * govtSocietyPlan.getGovtSocietyPlanLogList()) { String url =
				 * appPreference.getPortalServerURL() +
				 * "/employee/findempdetailsbyloggedinuser/"+ planLog.getCreatedBy().getId();
				 * 
				 * BaseDTO dto = httpService.get(url); if (dto != null && dto.getStatusCode() ==
				 * 0) { ObjectMapper maper = new ObjectMapper(); String jsonResp =
				 * maper.writeValueAsString(dto.getResponseContent()); empDetails =
				 * maper.readValue(jsonResp, EmployeeMaster.class); } else {
				 * errorMap.notify(ErrorDescription.EMP_NOT_FOUND_THIS_USER.getCode()); return
				 * null; }
				 * 
				 * EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
				 * empDetails.getPersonalInfoEmployment().getDesignation().getName(),
				 * planLog.getCreatedDate(), planLog.getRemarks());
				 * if(planLog.getStage().equals(PlanStage.INITIATED.toString())) {
				 * eventLog.setTitle("Created By"); approvedEventLogList.add(eventLog); } else
				 * if(planLog.getStage().equals(PlanStage.APPROVED.toString())){
				 * eventLog.setTitle("Approved By"); approvedEventLogList.add(eventLog); } else
				 * if(planLog.getStage().equals(PlanStage.REJECTED.toString())){
				 * eventLog.setTitle("Rejected By"); rejectedEventLogList.add(eventLog); } else
				 * if(planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())){
				 * eventLog.setTitle("Submited By"); approvedEventLogList.add(eventLog); }
				 * eventLogList.add(eventLog); } }
				 * 
				 * log.info("eventLogList" + eventLogList);
				 * 
				 * Collections.sort(eventLogList, new Comparator<EventLog>() { public int
				 * compare(EventLog m1, EventLog m2) { return
				 * m1.getTitle().compareTo(m2.getTitle()); } });
				 */

				String employeeDatajsonResponses = objectMapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = objectMapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<GovtSocietyPlanResponseDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size()
						+ viewLogResponseList);

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return VIEW_PAGE_URL;
	}

	/*
	 * public String gotoGovViewPage() { log.info("Inside gotoViewPage()");
	 * 
	 * 
	 * BaseDTO baseDTO =null;
	 * 
	 * if (govtSocietyPlanResponseDTO == null) {
	 * log.info("<---Please Select any one Plan--->");
	 * errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode()); return null; }
	 * 
	 * try {
	 * 
	 * editFlag=true; conExportTableFlag=false; govConExportFlag=true;
	 * 
	 * String URL = SERVER_URL + "/govt/societyPlan/get/"+
	 * govtSocietyPlanResponseDTO.getGovtSocietyPlanId();
	 * log.info("<--- gotoViewPage() URL ---> " + URL); baseDTO =
	 * httpService.get(URL); if (baseDTO != null && baseDTO.getStatusCode() == 0) {
	 * ObjectMapper objectMapper = new ObjectMapper(); String jsonResponse =
	 * objectMapper.writeValueAsString(baseDTO.getResponseContent());
	 * govtSocietyPlan = objectMapper.readValue(jsonResponse,
	 * GovtSocietyPlan.class);
	 * 
	 * log.info("===================== ");
	 * 
	 * for(GovtSchemePlanLog schemeLog :
	 * govtSocietyPlan.getGovtSchemePlan().getGovtSchemePlanLogList()) {
	 * if(schemeLog != null && schemeLog.getCreatedBy() != null &&
	 * schemeLog.getStage().equals("FINAL_APPROVED")) {
	 * govtSocietyPlan.getGovtSchemePlan().setApprovedDate(schemeLog.getCreatedDate(
	 * ));
	 * govtSocietyPlan.getGovtSchemePlan().setApprovedBy(schemeLog.getCreatedBy());
	 * break; } } totalCurrentReqQty = 0.00; totalOpeningStockQty = 0.00;
	 * for(GovtSocietyPlanDetailsMaster schemeDetails :
	 * govtSocietyPlan.getGovtSocietyPlanDetailsList()) { if(schemeDetails != null)
	 * { totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getCurrentReqQty();
	 * totalOpeningStockQty = totalOpeningStockQty +
	 * schemeDetails.getOpeningStockQty(); } } planNote =
	 * govtSocietyPlan.getGovtSocietyPlanNoteList().get(0);
	 * 
	 * 
	 * getEmployeeDetailsByUserId(); loadForwardToUsers();
	 * 
	 * eventLogList = new ArrayList<>();
	 * 
	 * approvedEventLogList = new ArrayList<>();
	 * 
	 * rejectedEventLogList = new ArrayList<>();
	 * 
	 * if(govtSocietyPlan.getGovtSocietyPlanLogList() != null &&
	 * !govtSocietyPlan.getGovtSocietyPlanLogList().isEmpty()) { EmployeeMaster
	 * empDetails = null; for(GovtSocietyPlanLog planLog :
	 * govtSocietyPlan.getGovtSocietyPlanLogList()) { String url =
	 * appPreference.getPortalServerURL() +
	 * "/employee/findempdetailsbyloggedinuser/"+ planLog.getCreatedBy().getId();
	 * 
	 * BaseDTO dto = httpService.get(url); if (dto != null && dto.getStatusCode() ==
	 * 0) { ObjectMapper maper = new ObjectMapper(); String jsonResp =
	 * maper.writeValueAsString(dto.getResponseContent()); empDetails =
	 * maper.readValue(jsonResp, EmployeeMaster.class); } else {
	 * errorMap.notify(ErrorDescription.EMP_NOT_FOUND_THIS_USER.getCode()); return
	 * null; }
	 * 
	 * EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
	 * empDetails.getPersonalInfoEmployment().getDesignation().getName(),
	 * planLog.getCreatedDate(), planLog.getRemarks());
	 * if(planLog.getStage().equals(PlanStage.INITIATED.toString()) ||
	 * planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
	 * eventLog.setTitle("Created By"); } else
	 * if(planLog.getStage().equals(PlanStage.APPROVED.toString())){
	 * eventLog.setTitle("Approved By"); approvedEventLogList.add(eventLog); } else
	 * if(planLog.getStage().equals(PlanStage.REJECTED.toString())){
	 * eventLog.setTitle("Rejected By"); rejectedEventLogList.add(eventLog); } else
	 * if(planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())){
	 * eventLog.setTitle("Submited By"); approvedEventLogList.add(eventLog); }
	 * eventLogList.add(eventLog); } }
	 * 
	 * log.info("eventLogList", eventLogList);
	 * 
	 * Collections.sort(eventLogList, new Comparator<EventLog>() { public int
	 * compare(EventLog m1, EventLog m2) { return
	 * m1.getTitle().compareTo(m2.getTitle()); } });
	 * 
	 * } else { log.error( "Status code:" + baseDTO.getStatusCode() +
	 * " Error Message: " + baseDTO.getErrorDescription());
	 * errorMap.notify(baseDTO.getStatusCode()); return null; }
	 * 
	 * } catch (Exception e) { log.error("Error in gotoViewPage  ", e);
	 * errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode()); }
	 * 
	 * return VIEW_PAGE_URL; }
	 */

	public void getEmployeeDetailsByUserId() {
		log.info("Inside getEmployeeDetailsByUserId()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ planNote.getCreatedBy().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + planNote.getCreatedBy().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void calculateTotalCurrentConExportQty() {
		log.info("<=calculateTotalCurrentConExportQty=>");
		totalCurrentReqQty = 0.00;
		for (ContractExportSocietyPlanDetails dt : contractExportSocietyPlan.getContractExportPlanDetailsList()) {
			if (dt != null && dt.getCurrentReqQty() != null) {
				totalCurrentReqQty = totalCurrentReqQty + dt.getCurrentReqQty();
			}
		}
		log.info("<=calculateTotalCurrentConExportQty totalCurrentReqQty=>" + totalCurrentReqQty);
	}

	public void calculateTotalCurrentAdditionalQty() {
		log.info("<=calculateTotalCurrentAdditionalQty=>");
		totalCurrentReqQty = 0.00;
		for (AdditionalSocietyPlanDetails dt : additionalSocietyPlan.getAdditionalSocietyPlanDetailsList()) {
			if (dt != null && dt.getCurrentReqQty() != null) {
				totalCurrentReqQty = totalCurrentReqQty + dt.getCurrentReqQty();
			}
		}
		log.info("<=calculateTotalCurrentAdditionalQty totalCurrentReqQty=>" + totalCurrentReqQty);
	}

	public void calculateTotalCurrentReqQtyForView() {
		totalCurrentReqQty = 0.00;
		for (GovtSocietyPlanDetailsMaster sm : govtSocietyPlan.getGovtSocietyPlanDetailsList()) {
			// log.info("totalCurrentReqQty :"+sm);
			if (sm != null && sm.getCurrentReqQty() != null) {
				totalCurrentReqQty = totalCurrentReqQty + sm.getCurrentReqQty();
			}
		}
	}

	public void calculateTotalOpeningStockQtyForView() {
		totalOpeningStockQty = 0.00;
		for (GovtSocietyPlanDetailsMaster sm : govtSocietyPlan.getGovtSocietyPlanDetailsList()) {
			// log.info("totalOpeningStockQty :"+sm);
			if (sm != null && sm.getOpeningStockQty() != null) {
				totalOpeningStockQty = totalOpeningStockQty + sm.getOpeningStockQty();
			}
		}
	}

	public String cancelSocietyPlan() {

		govtSchemePlan = new GovtSchemePlan();
		contractExportPlan = new ContractExportPlan();
		entityMaster = new EntityMaster();
		circleMaster = new CircleMaster();
		productCategory = new ProductCategory();
		productGroupMaster = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		uomMaster = new UomMaster();
		govtSchemePlanItems = new GovtSchemePlanItems();
		planNote = new GovtSocietyPlanNote();
		contractExportPlanNote = new ContractExportSocietyPlanNote();
		totalQty = null;
		selectedContractExportSocietyPlan = new ContractExportSocietyPlan();
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			circleCodeFlag = true;
			govConExportFlag = true;
			editFlag = false;
			conExportTableFlag = false;
			loadLazyGovtSocietyPlanList();
			loadStatusValues();
			return LIST_PAGE_URL;
		} else {
			circleCodeFlag = false;
			editFlag = true;
			conExportTableFlag = true;
			govConExportFlag = false;
			loadLazyConExportSocietyPlanList();
			return LIST_CON_EXPORT_URL;
		}
	}

	public String approveRejectGovtSocietyPlan(String status) {
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			return updateGovtSocietyPlanGov(status);
		} else {
			return updateGovtSocietyPlanConExport(status);
		}
	}

	public String updateGovtSocietyPlanConExport(String status) {
		// ContractExportSocietyPlan contractExportSocietyPlan = new
		// ContractExportSocietyPlan();
		ContractSocietyDTO contractSocietyDTO = new ContractSocietyDTO();
		if (totalCurrentReqQty == null || totalCurrentReqQty.equals("") || totalCurrentReqQty.equals(0.00)) {
			errorMap.notify(ErrorDescription.ENTER_VALID_CURRENT_QTY.getCode());
			return null;
		}

		contractExportPlanNote.setNote(planNoteDetails);

		contractExportSocietyPlan.setStage(status);

		for (ContractExportSocietyPlanLog log : contractExportSocietyPlan.getContractExportPlanLogList()) {
			log.setStage(status);
			log.setContractExportSocietyPlan(contractExportSocietyPlan);

		}

		contractExportSocietyPlan.getContractExportPlanNoteList().clear();

		contractExportSocietyPlan.getContractExportPlanNoteList().add(contractExportPlanNote);

		contractSocietyDTO.setContractExportSocietyPlan(contractExportSocietyPlan);

		String url = SERVER_URL + "/govt/societyPlan/approveSocietyPlanByConExport";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, contractSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Govt Society Plan Submitted successfully");
					AppUtil.addInfo("Contract/Export Society Plan Submitted successfully");
					loadLazyConExportSocietyPlanList();
					return LIST_CON_EXPORT_URL;

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addError("Govt Society Plan not able to Submitted");
					return null;
				}
			}
			return null;
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String updateGovtSocietyPlanGov(String status) {
		GovtSocietyDTO govtSocietyDTO = new GovtSocietyDTO();

		/*
		 * if (totalOpeningStockQty == null || totalOpeningStockQty.equals("") ||
		 * totalOpeningStockQty.equals(0.00) || totalCurrentReqQty == null ||
		 * totalCurrentReqQty.equals("") || totalCurrentReqQty.equals(0.00)) {
		 * errorMap.notify(ErrorDescription.ENTER_OPENING_CURRENT_STOCK_QTY.getCode());
		 * return null; }
		 */
		if (!totalOpeningStockQty.equals(govtSocietyPlan.getGovtSchemePlanItems().getOpeningStockQuantity())) {
			errorMap.notify(ErrorDescription.TOT_SUPPLER_COMPARE_OPENING_STOCK_QTY.getCode());
			return null;
		}
		if (!totalCurrentReqQty.equals(govtSocietyPlan.getGovtSchemePlanItems().getCurrentYearProductionQuantity())) {
			errorMap.notify(ErrorDescription.TOT_SUPPLER_COMPARE_CURRENT_STOCK_QTY.getCode());
			return null;
		}

		planNote.setNote(planNoteDetails);

		// log.info("planNote : "+planNote);
		if (planNote == null || planNote.getNote() == null || planNote.getNote().equals("")
				|| planNote.getUserMaster() == null || planNote.getUserMaster().getId() == null
				|| planNote.getFinalApproval() == null || planNote.getFinalApproval().equals("")) {
			errorMap.notify(ErrorDescription.ADD_PLAN_NOTES.getCode());
			return null;
		}

		govtSocietyPlan.setStage(status);

		GovtSocietyPlanLog societyLog = new GovtSocietyPlanLog();

		govtSocietyPlan.getGovtSocietyPlanNoteList().clear();
		// govtSocietyPlan.getGovtSocietyPlanLogList().clear();

		govtSocietyPlan.getGovtSocietyPlanNoteList().add(planNote);
		societyLog.setStage(status);
		societyLog.setGovtSocietyPlan(govtSocietyPlan);
		govtSocietyPlan.getGovtSocietyPlanLogList().add(societyLog);
		govtSocietyDTO.setGovtSocietyPlan(govtSocietyPlan);

		// govtSocietyDTO.setGovtSocietyPlanDetailsMasterList(govtSocietyPlanDetailsMasterList);
		// govtSocietyDTO.setPlanNote(planNote);

		String url = SERVER_URL + "/govt/societyPlan/approveRejectSocietyPlan";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, govtSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Govt Society Plan Submitted successfully");
					errorMap.notify(ErrorDescription.SOCIETY_INSERTED_SUCCESSFULLY.getCode());
					showFdsList();
					// errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.SOCIETY_PLAN_NOT_SUBMITED.getCode());
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String deletePlan(String screenType) {
		if (screenType.equals("Govt_Scheme")) {
			return govDeletePage();
		} else {
			return conExportDeletePage();
		}
	}

	public String conExportDeletePage() {

		log.info(
				"conExportDeletePage selectedContractExportSocietyPlan==>" + selectedContractExportSocietyPlan.getId());
		if (selectedContractExportSocietyPlan == null && selectedContractExportSocietyPlan.getId() == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
			return null;
		}
		String url = SERVER_URL + "/govt/societyPlan/conexportdeleteplan";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, selectedContractExportSocietyPlan);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Contract/Export Society Plan Deleted successfully");
					errorMap.notify(ErrorDescription.CONTRACT_EXPORT_DELETED_SUCCESSFULLY.getCode());
					loadLazyConExportSocietyPlanList();
					return LIST_CON_EXPORT_URL;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.CONTRACT_EXPORT_DELETED_NOT_DELETED.getCode());
					return null;
				}
			}
			return null;
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String govDeletePage() {
		if (govtSocietyPlanResponseDTO == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
			return null;
		}
		// GovtSocietyDTO govtSocietyDTO = new GovtSocietyDTO();
		String url = SERVER_URL + "/govt/societyPlan/deletePlan";
		BaseDTO baseDTO = null;
		try {
			// govtSocietyDTO.setGovtSocietyPlan(govtSocietyPlan);
			baseDTO = httpService.post(url, govtSocietyPlanResponseDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Govt Society Plan Deleted successfully");
					errorMap.notify(ErrorDescription.GOVT_SOCIETY_DELETED_SUCCESSFULLY.getCode());
					showFdsList();
					// errorMap.notify(8545);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.GOVT_SOCIETY_DELETED_NOT_DELETED.getCode());
					return null;
				}
			}
			return getListPage();
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String gotoEditPage(String screenType) {
		if (screenType.equals("Govt_Scheme")) {
			return gotoGovEditPage();
		} else {
			return gotoConExportEditPage();
		}
	}

	public String gotoConExportEditPage() {
		log.info("<=gotoConExportEditPage =>");
		try {
			// employeeMaster = new EmployeeMaster();
			BaseDTO baseDTO = null;
			if (selectedContractExportSocietyPlan == null) {
				log.info("<---Please Select any one Plan--->");
				errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
				return null;
			}

			if (selectedContractExportSocietyPlan.getContractExportSocietyPlanLog()
					.equals(GovtSocietyPlanStatus.SUBMITTED.toString())
					|| selectedContractExportSocietyPlan.getContractExportSocietyPlanLog()
							.equals(GovtSocietyPlanStatus.APPROVED.toString())
					|| selectedContractExportSocietyPlan.getContractExportSocietyPlanLog()
							.equals(GovtSocietyPlanStatus.REJECTED.toString())) {
				errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode());
				return null;
			}

			String URL = SERVER_URL + "/govt/societyPlan/getcontractexport/"
					+ selectedContractExportSocietyPlan.getId();
			log.info("<--- gotoConExportEditPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				/*
				 * ObjectMapper mapper = new ObjectMapper();
				 * mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				 * String jsonResponse =
				 * mapper.writeValueAsString(baseDTO.getResponseContent());
				 * contractExportSocietyPlan = mapper.readValue(jsonResponse, new
				 * TypeReference<ContractExportSocietyPlan>() { });
				 */

				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				contractExportSocietyPlan = objectMapper.readValue(jsonResponse, ContractExportSocietyPlan.class);

				editFlag = true;
				govConExportFlag = false;
				circleCodeFlag = false;
				qtyFlag = true;
				conExportTableFlag = true;

				log.info("==>" + contractExportSocietyPlan.getContractExportPlan().getPlanType().name());
				govtProductionPlan = contractExportSocietyPlan.getContractExportPlan().getPlanType().name();

				contractExportPlan = contractExportSocietyPlan.getContractExportPlan();
				contractExportPlanList = new ArrayList<>();
				contractExportPlanList.add(contractExportPlan);

				entityMaster = contractExportSocietyPlan.getEntityMaster();

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = contractExportSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = contractExportSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = contractExportSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				getUOM();
				getSuppliers();

				totalCurrentReqQty = 0.00;

				for (ContractExportPlanDetails schemeDetails : contractExportPlan.getContractExportPlanDetails()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getQuantity();

					}
				}
				totalQty = totalCurrentReqQty;

				contractExportPlanNote = contractExportSocietyPlan.getContractExportPlanNoteList().get(0);

				planNoteDetails = contractExportSocietyPlan.getContractExportPlanNoteList().get(0).getNote();

				loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();

				if (contractExportSocietyPlan.getContractExportPlanLogList() != null
						&& !contractExportSocietyPlan.getContractExportPlanLogList().isEmpty()) {
					EmployeeMaster empDetails = null;
					for (ContractExportSocietyPlanLog planLog : contractExportSocietyPlan
							.getContractExportPlanLogList()) {
						String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
								+ planLog.getCreatedBy().getId();

						BaseDTO dto = httpService.get(url);
						if (dto != null && dto.getStatusCode() == 0) {
							ObjectMapper maper = new ObjectMapper();
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);
						} else {
							AppUtil.addError(" Employee Details Not Found for the User ");
							return null;
						}

						EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
								empDetails.getPersonalInfoEmployment().getDesignation().getName(),
								planLog.getCreatedDate(), planLog.getRemarks());
						if (planLog.getStage().equals(PlanStage.INITIATED.toString())
								|| planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
							eventLog.setTitle("Created By");
						} else if (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
							eventLog.setTitle("Approved By");
							approvedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
							eventLog.setTitle("Rejected By");
							rejectedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())) {
							eventLog.setTitle("Submited By");
							approvedEventLogList.add(eventLog);
						}
						eventLogList.add(eventLog);
					}
				}

				log.info("eventLogList", eventLogList);

				Collections.sort(eventLogList, new Comparator<EventLog>() {
					public int compare(EventLog m1, EventLog m2) {
						return m1.getTitle().compareTo(m2.getTitle());
					}
				});

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return ADD_PAGE_URL;
	}

	public String gotoGovEditPage() {
		log.info("Inside gotoGovEditPage()==>");

		BaseDTO baseDTO = null;

		if (govtSocietyPlanResponseDTO == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
			return null;
		}

		if (govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.SUBMITTED.toString())
				|| govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.APPROVED.toString())
				|| govtSocietyPlanResponseDTO.getPlanStatus().equals(GovtSocietyPlanStatus.REJECTED.toString())) {
			errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode());
			return null;
		}

		try {
			String URL = SERVER_URL + "/govt/societyPlan/get/" + govtSocietyPlanResponseDTO.getGovtSocietyPlanId();

			log.info("<--- gotoViewPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				govtSocietyPlan = objectMapper.readValue(jsonResponse, GovtSocietyPlan.class);

				log.info("===================== ");
				editFlag = true;
				govtProductionPlan = "Govt_Scheme";
				govConExportFlag = true;

				for (GovtSchemePlanLog schemeLog : govtSocietyPlan.getGovtSchemePlan().getGovtSchemePlanLogList()) {
					if (schemeLog != null && schemeLog.getCreatedBy() != null
							&& schemeLog.getStage().equals("FINAL_APPROVED")) {
						govtSocietyPlan.getGovtSchemePlan().setApprovedDate(schemeLog.getCreatedDate());
						govtSocietyPlan.getGovtSchemePlan().setApprovedBy(schemeLog.getCreatedBy());
						break;
					}
				}
				govtSchemePlan = govtSocietyPlan.getGovtSchemePlan();
				govtProductionPlanList = new ArrayList<GovtSchemePlan>();
				govtProductionPlanList.add(govtSchemePlan);

				entityMaster = govtSocietyPlan.getEntityMaster();

				circleMasterList = new ArrayList<CircleMaster>();
				circleMaster = govtSocietyPlan.getCircleMaster();
				circleMasterList.add(circleMaster);

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = govtSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = govtSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = govtSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				getUOM();
				getItemsForQuantity();
				getSuppliers();

				totalCurrentReqQty = 0.00;
				totalOpeningStockQty = 0.00;
				for (GovtSocietyPlanDetailsMaster schemeDetails : govtSocietyPlan.getGovtSocietyPlanDetailsList()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getCurrentReqQty();
						totalOpeningStockQty = totalOpeningStockQty + schemeDetails.getOpeningStockQty();
					}
				}
				planNote = govtSocietyPlan.getGovtSocietyPlanNoteList().get(0);
				log.info("note==>" + govtSocietyPlan.getGovtSocietyPlanNoteList().get(0).getNote());
				planNoteDetails = govtSocietyPlan.getGovtSocietyPlanNoteList().get(0).getNote();

				getEmployeeDetailsByUserId();
				loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();

				if (govtSocietyPlan.getGovtSocietyPlanLogList() != null
						&& !govtSocietyPlan.getGovtSocietyPlanLogList().isEmpty()) {
					EmployeeMaster empDetails = null;
					for (GovtSocietyPlanLog planLog : govtSocietyPlan.getGovtSocietyPlanLogList()) {
						String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
								+ planLog.getCreatedBy().getId();

						BaseDTO dto = httpService.get(url);
						if (dto != null && dto.getStatusCode() == 0) {
							ObjectMapper maper = new ObjectMapper();
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);
						} else {
							errorMap.notify(ErrorDescription.EMP_NOT_FOUND_THIS_USER.getCode());
							return null;
						}

						EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
								empDetails.getPersonalInfoEmployment().getDesignation().getName(),
								planLog.getCreatedDate(), planLog.getRemarks());
						if (planLog.getStage().equals(PlanStage.INITIATED.toString())
								|| planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
							eventLog.setTitle("Created By");
						} else if (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
							eventLog.setTitle("Approved By");
							approvedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
							eventLog.setTitle("Rejected By");
							rejectedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())) {
							eventLog.setTitle("Submited By");
							approvedEventLogList.add(eventLog);
						}
						eventLogList.add(eventLog);
					}
				}

				log.info("eventLogList", eventLogList);

				Collections.sort(eventLogList, new Comparator<EventLog>() {
					public int compare(EventLog m1, EventLog m2) {
						return m1.getTitle().compareTo(m2.getTitle());
					}
				});

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return ADD_PAGE_URL;
	}

	public void showDelete(String screenType) {
		log.info(":: Delete Response :: ");
		if (screenType.equals("Govt_Scheme")) {
			if (govtSocietyPlanResponseDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
		} else {
			if (selectedContractExportSocietyPlan == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
		}

	}

	public String clearPlan(String screenType) {
		if (screenType.equals("Govt_Scheme")) {
			editFlag = false;
			govtSocietyPlanResponseDTO = new GovtSocietyPlanResponseDTO();
			loadLazyGovtSocietyPlanList();
			return LIST_PAGE_URL;
		} else {
			selectedContractExportSocietyPlan = new ContractExportSocietyPlan();
			loadLazyConExportSocietyPlanList();
			return LIST_CON_EXPORT_URL;
		}
	}

	public String showAdditionlProList() {
		log.info("Inside GovtSocietyWiseProductionPlanBean showAdditionlProList()>>>>>>>>>");
		loadLazyAdditionalSocietyPlanList();
		return LIST_ADDTIONAL_EXPORT_URL;
	}

	public void onRowAddtionalSelect(SelectEvent event) {
		log.info("FDS Plan onRowAddtionalSelect method started");
		selectedAdditionalSocietyPlan = ((AdditionalSocietyPlan) event.getObject());

	}

	public void loadLazyAdditionalSocietyPlanList() {

		additionalSocietyPlanList = new LazyDataModel<AdditionalSocietyPlan>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<AdditionalSocietyPlan> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					ContractExportSocietyPlan request = new ContractExportSocietyPlan();

					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/govt/societyPlan/lazyadditionalSocietyPlansearch",
							request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					ObjectMapper mapper = new ObjectMapper();

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<AdditionalSocietyPlan> data = mapper.readValue(jsonResponse,
							new TypeReference<List<AdditionalSocietyPlan>>() {
							});
					if (data == null) {
						log.info(" :: Employee Interchange Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						conExportTotalRecord = baseDTO.getTotalRecords();
						log.info(":: load LazyCon ExportSocietyPlanList Successfully Completed ::");
					} else {
						log.error(":: loadLazyConExportSocietyPlanList Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading loadLazyConExportSocietyPlanList data ::",
							e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(AdditionalSocietyPlan res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AdditionalSocietyPlan getRowData(String rowKey) {
				List<AdditionalSocietyPlan> responseList = (List<AdditionalSocietyPlan>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (AdditionalSocietyPlan res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedAdditionalSocietyPlan = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String getAddAdditionalPage() {
		govtProductionPlan = "Additional_Production_Plan";
		additionalEditFlag = false;
		loadAdditionalList();
		return ADD_ADDTIONAL_PAGE_URL;
	}

	@SuppressWarnings("unchecked")
	public void loadAdditionalList() {
		log.info("<--- Inside loadAdditionalList() ---> ");
		try {
			circleMaster = null;
			additionalProductionPlanList = new ArrayList<>();
			additionalSocietyPlanNote = new AdditionalSocietyPlanNote();
			circleMasterList = new ArrayList<CircleMaster>();
			productCategory = null;
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupMaster = null;
			productGroupMasterList = new ArrayList<ProductGroupMaster>();
			productVarietyMaster = null;
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			govtSchemePlanItems = null;
			supplierMasterList = new ArrayList<SupplierMaster>();
			totalCurrentReqQty = 0.00;
			totalOpeningStockQty = 0.00;
			planNoteDetails = "";
			String url = SERVER_URL + "/govt/societyPlan/getcontactadditionalplanlist";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					additionalProductionPlanList = (List<AdditionalProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<AdditionalProductionPlan>>() {
							});
					if (additionalProductionPlanList != null) {
						log.info("AdditionalProductionPlanList with Size of : " + additionalProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadAdditionalList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					additionalProductionPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	public void loadAdditionProductCat() {
		log.info("<--- Inside loadAdditionProductCat() ---> ");
		try {
			productCategory = null;
			productCategoryList = new ArrayList<ProductCategory>();
			productGroupMaster = null;
			productGroupMasterList = new ArrayList<ProductGroupMaster>();
			productVarietyMaster = null;
			productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			govtSchemePlanItems = null;
			supplierMasterList = new ArrayList<SupplierMaster>();
			totalCurrentReqQty = 0.00;
			totalOpeningStockQty = 0.00;
			if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
				return;
			}
			log.info("additionalProductionPlan id==>" + additionalProductionPlan.getId());
			String requestPath = SERVER_URL + "/govt/societyPlan/loadadditionalproductionplan/"
					+ additionalProductionPlan.getId();
			log.info("loadAdditionProductCat requestPath==>" + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productCategoryList = (List<ProductCategory>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductCategory>>() {
							});

					if (productCategoryList != null) {
						log.info("productCategoryList with Size of : " + productCategoryList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" productCategoryList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadProductCategoryByContractExport() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadProductCategoryByContractExport() --->", e);
		}
	}

	public void generateAdditionalSociety() {
		log.info("<--- Inside loadGovInfo() ---> ");
		govtSchemePlanItems = null;
		if ((productVarietyMaster == null || productVarietyMaster.getId() == null)
				&& (contractExportPlan == null || contractExportPlan.getId() == null)) {
			errorMap.notify(ErrorDescription.SELECT_PLAN_PRODUCT.getCode());
			return;
		}
		getItemsForQuantityByAdditional();
		getSuppliers();
	}

	public void getItemsForQuantityByAdditional() {
		log.info("<--- Inside getItemsForQuantityByAdditional() ---> ");

		try {
			qtyFlag = true;
			log.info("product variety id ---> " + productVarietyMaster.getId());
			log.info("additionalProductionPlan id ---> " + additionalProductionPlan.getId());

			String requestPath = SERVER_URL + "/govt/societyPlan/getitemsforquantitybyadditional/"
					+ productVarietyMaster.getId() + "/" + additionalProductionPlan.getId();
			log.info("getItemsForQuantity requestPath ==> " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					Double totalQt = (Double) mapper.readValue(jsonValue, new TypeReference<Double>() {
					});
					if (totalQt != null) {
						totalQty = totalQt;
						log.info("totalQty : " + totalQty);
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" totalQty is null ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getItemsForQuantity() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getItemsForQuantity() --->", e);
		}
	}

	public String cancelAdditionalPlan() {
		log.info("Inside GovtSocietyWiseProductionPlanBean cancelAdditionalPlan()>>>>>>>>>");
		selectedAdditionalSocietyPlan = new AdditionalSocietyPlan();
		loadLazyAdditionalSocietyPlanList();
		return LIST_ADDTIONAL_EXPORT_URL;

	}

	public String saveAdditionalPlan(String status) {

		log.info("from saveGovtSocietyPlanConExport...........");
		BaseDTO baseDTO = null;
		try {

			AdditionalSocietyPlan additionalSocietyPlan = new AdditionalSocietyPlan();
			List<AdditionalSocietyPlanDetails> additionalSocietyPlanList = new ArrayList<>();
			AdditionalSocietyDTO additionalSocietyDTO = new AdditionalSocietyDTO();
			if (totalCurrentReqQty == null || totalCurrentReqQty.equals("") || totalCurrentReqQty.equals(0.00)) {
				AppUtil.addWarn(" please generate Society Code / Name ");
				return null;
			}

			if (entityMaster != null && entityMaster.getId() != null) {
				additionalSocietyPlan.setEntityMaster(entityMaster);
			}
			if (additionalProductionPlan != null && additionalProductionPlan.getId() != null) {
				additionalSocietyPlan.setAdditionalProductionPlan(additionalProductionPlan);
			} else {
				errorMap.notify(ErrorDescription.SELECT_PLAN.getCode());
				return null;
			}
			if (productVarietyMaster != null && productVarietyMaster.getId() != null) {
				additionalSocietyPlan.setProductVariety(productVarietyMaster);
			} else {
				errorMap.notify(ErrorDescription.SELECT_PRODUCT.getCode());
				return null;
			}

			log.info("planNoteDetails==>" + planNoteDetails);
			additionalSocietyPlanNote.setNote(planNoteDetails);
			log.info("additionalSocietyPlanNote : " + additionalSocietyPlanNote);
			if (additionalSocietyPlanNote == null || additionalSocietyPlanNote.getNote() == null
					|| additionalSocietyPlanNote.getNote().equals("")
					|| additionalSocietyPlanNote.getUserMaster() == null
					|| additionalSocietyPlanNote.getUserMaster().getId() == null
					|| additionalSocietyPlanNote.getFinalApproval() == null
					|| additionalSocietyPlanNote.getFinalApproval().equals("")) {
				errorMap.notify(ErrorDescription.ADD_PLAN_NOTES.getCode());
				return null;
			}

			if (supplierMasterList != null && !supplierMasterList.isEmpty()) {
				for (SupplierMaster sm : supplierMasterList) {
					if (sm != null && sm.getId() != null) {
						AdditionalSocietyPlanDetails contractExportSocietyPlanDetails = new AdditionalSocietyPlanDetails();
						if (sm.getOpeningStockQty() != null) {
							contractExportSocietyPlanDetails.setOpeningStockQty(sm.getOpeningStockQty());
						} else {
							contractExportSocietyPlanDetails.setOpeningStockQty(0.00);
						}
						if (sm.getCurrentReqQty() != null) {
							contractExportSocietyPlanDetails.setCurrentReqQty(sm.getCurrentReqQty());
						} else {
							contractExportSocietyPlanDetails.setCurrentReqQty(0.00);
						}
						contractExportSocietyPlanDetails.setSupplierMaster(sm);
						additionalSocietyPlanList.add(contractExportSocietyPlanDetails);
					}
				}
			}

			if (additionalSocietyPlan != null && additionalSocietyPlanList != null
					&& !additionalSocietyPlanList.isEmpty()) {
				additionalSocietyPlan.setStage(status);
				additionalSocietyDTO.setAdditionalSocietyPlanVal(additionalSocietyPlan);
				additionalSocietyDTO.setAdditionalSocietyPlanDetailList(additionalSocietyPlanList);
				additionalSocietyDTO.setAdditionalPlanNote(additionalSocietyPlanNote);
			}

			String url = SERVER_URL + "/govt/societyPlan/submitsocietyplanbyadditional";

			baseDTO = httpService.post(url, additionalSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Additional Society Plan Submitted successfully");
					errorMap.notify(ErrorDescription.ADDITIONAL_SOCIETY_PLAN_SUBMITED_SUCCESSFULLY.getCode());
					loadLazyAdditionalSocietyPlanList();
					return LIST_ADDTIONAL_EXPORT_URL;
					// errorMap.notify(8545);
				} else if (baseDTO.getStatusCode() == 999) {
					// errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.DUPLICATE_COMBINATION_NOT_ALLOWED.getCode());
					return null;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					// errorMap.notify(ErrorDescription.CONTRACT_EXPORT_SOCIETY_PLAN_NOT_SUBMITED.getCode());
					AppUtil.addError("Additional Society Plan Not Submitted");
					return null;
				}
			}
			return null;

		} catch (Exception e) {
			log.error("Error in submitDraftPlan", e);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}

	}

	public void showAddtionalDelete() {
		if (selectedAdditionalSocietyPlan == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");
		}
	}

	public String deleteAdditionalPlan() {

		log.info("deleteAdditionalPlan selectedAdditionalSocietyPlan==>" + selectedAdditionalSocietyPlan.getId());
		if (selectedAdditionalSocietyPlan == null && selectedAdditionalSocietyPlan.getId() == null) {
			log.info("<---Please Select any one Plan--->");
			errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
			return null;
		}
		String url = SERVER_URL + "/govt/societyPlan/addtionaldeleteplan";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, selectedAdditionalSocietyPlan);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Additional Society Plan Deleted successfully");
					errorMap.notify(ErrorDescription.ADDITIONAL_DELETED_SUCCESSFULLY.getCode());
					loadLazyAdditionalSocietyPlanList();
					return LIST_ADDTIONAL_EXPORT_URL;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.CONTRACT_EXPORT_DELETED_NOT_DELETED.getCode());
					return null;
				}
			}
			return null;
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String additionalEditPage() {
		log.info("<=additionalEditPage =>");
		try {

			BaseDTO baseDTO = null;
			log.info("selectedAdditionalSocietyPlan==>" + selectedAdditionalSocietyPlan);
			if (selectedAdditionalSocietyPlan == null || selectedAdditionalSocietyPlan.getId() == null) {
				log.info("<---Please Select any one Plan--->");
				errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
				return null;
			}
			log.info("deleteAdditionalPlan selectedAdditionalSocietyPlan==>" + selectedAdditionalSocietyPlan.getId());
			if (selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog()
					.equals(GovtSocietyPlanStatus.SUBMITTED.toString())
					|| selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog()
							.equals(GovtSocietyPlanStatus.APPROVED.toString())
					|| selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog()
							.equals(GovtSocietyPlanStatus.REJECTED.toString())) {
				errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode());
				return null;
			}

			String URL = SERVER_URL + "/govt/societyPlan/getaddtionalplan/" + selectedAdditionalSocietyPlan.getId();
			log.info("<--- gotoConExportEditPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				additionalSocietyPlan = objectMapper.readValue(jsonResponse, AdditionalSocietyPlan.class);

				govtProductionPlan = "Additional_Production_Plan";

				additionalEditFlag = true;

				additionalProductionPlan = additionalSocietyPlan.getAdditionalProductionPlan();
				additionalProductionPlanList = new ArrayList<>();
				additionalProductionPlanList.add(additionalProductionPlan);

				entityMaster = additionalSocietyPlan.getEntityMaster();

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = additionalSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = additionalSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = additionalSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				getUOM();
				getSuppliers();
				totalCurrentReqQty = 0.00;

				for (AdditionalSocietyPlanDetails schemeDetails : additionalSocietyPlan
						.getAdditionalSocietyPlanDetailsList()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getCurrentReqQty();

					}
				}
				totalQty = totalCurrentReqQty;

				additionalSocietyPlanNote = additionalSocietyPlan.getAdditionalSocietyPlanNoteList().get(0);

				planNoteDetails = additionalSocietyPlan.getAdditionalSocietyPlanNoteList().get(0).getNote();

				loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();

				if (additionalSocietyPlan.getAdditionalSocietyPlanLogList() != null
						&& !additionalSocietyPlan.getAdditionalSocietyPlanLogList().isEmpty()) {
					EmployeeMaster empDetails = null;
					for (AdditionalSocietyPlanLog planLog : additionalSocietyPlan.getAdditionalSocietyPlanLogList()) {
						String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
								+ planLog.getCreatedBy().getId();

						BaseDTO dto = httpService.get(url);
						if (dto != null && dto.getStatusCode() == 0) {
							ObjectMapper maper = new ObjectMapper();
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);

						} else {
							AppUtil.addError(" Employee Details Not Found for the User ");
							return null;
						}

						EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
								empDetails.getPersonalInfoEmployment().getDesignation().getName(),
								planLog.getCreatedDate(), planLog.getRemarks());
						if (planLog.getStage().equals(PlanStage.INITIATED.toString())
								|| planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
							eventLog.setTitle("Created By");
						} else if (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
							eventLog.setTitle("Approved By");
							approvedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
							eventLog.setTitle("Rejected By");
							rejectedEventLogList.add(eventLog);
						} else if (planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())) {
							if (selectedContractExportSocietyPlan == null) {
								log.info("<---Please Select any one Plan--->");
								errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
								return null;
							}
							eventLog.setTitle("Submited By");
							approvedEventLogList.add(eventLog);
						}
						eventLogList.add(eventLog);
					}
				}

				log.info("eventLogList", eventLogList);

				Collections.sort(eventLogList, new Comparator<EventLog>() {
					public int compare(EventLog m1, EventLog m2) {
						return m1.getTitle().compareTo(m2.getTitle());
					}
				});
				return ADD_ADDTIONAL_PAGE_URL;

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String additionalViewPage() {
		log.info("<=additionalViewPage =>");
		try {

			BaseDTO baseDTO = null;
			log.info("selectedAdditionalSocietyPlan==>" + selectedAdditionalSocietyPlan);
			if (selectedAdditionalSocietyPlan == null || selectedAdditionalSocietyPlan.getId() == null) {
				log.info("<---Please Select any one Plan--->");
				errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode());
				return null;
			}
			log.info("additionalViewPage selectedAdditionalSocietyPlan==>" + selectedAdditionalSocietyPlan.getId());
			/*
			 * if(selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog().equals(
			 * GovtSocietyPlanStatus.SUBMITTED.toString()) ||
			 * selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog().equals(
			 * GovtSocietyPlanStatus.APPROVED.toString()) ||
			 * selectedAdditionalSocietyPlan.getAdditionalSocietyPlanLog().equals(
			 * GovtSocietyPlanStatus.REJECTED.toString())) {
			 * errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode()); return null; }
			 */

			String URL = SERVER_URL + "/govt/societyPlan/getaddtionalplan/" + selectedAdditionalSocietyPlan.getId();
			log.info("<--- gotoConExportEditPage() URL ---> " + URL);
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				additionalSocietyPlan = objectMapper.readValue(jsonResponse, AdditionalSocietyPlan.class);

				govtProductionPlan = "Additional_Production_Plan";

				additionalEditFlag = true;

				additionalProductionPlan = additionalSocietyPlan.getAdditionalProductionPlan();
				additionalProductionPlanList = new ArrayList<>();
				additionalProductionPlanList.add(additionalProductionPlan);

				entityMaster = additionalSocietyPlan.getEntityMaster();

				productGroupMasterList = new ArrayList<ProductGroupMaster>();
				productGroupMaster = additionalSocietyPlan.getProductGroupMaster();
				productGroupMasterList.add(productGroupMaster);

				productCategoryList = new ArrayList<ProductCategory>();
				productCategory = additionalSocietyPlan.getProductCategory();
				productCategoryList.add(productCategory);

				productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
				productVarietyMaster = additionalSocietyPlan.getProductVariety();
				productVarietyMasterList.add(productVarietyMaster);

				getUOM();
				getSuppliers();
				totalCurrentReqQty = 0.00;

				for (AdditionalSocietyPlanDetails schemeDetails : additionalSocietyPlan
						.getAdditionalSocietyPlanDetailsList()) {
					if (schemeDetails != null) {
						totalCurrentReqQty = totalCurrentReqQty + schemeDetails.getCurrentReqQty();

					}
				}
				totalQty = totalCurrentReqQty;

				additionalSocietyPlanNote = additionalSocietyPlan.getAdditionalSocietyPlanNoteList().get(0);

				if (additionalSocietyPlanNote.getUserMaster().getId()
						.equals(loginBean.getUserDetailSession().getId())) {
					log.info("============approval flag===============");
					setApprovalFlag(true);
				} else {
					setApprovalFlag(false);
				}

				previousApproval = additionalSocietyPlanNote.getFinalApproval();

				additionalSocietyPlanLog = additionalSocietyPlan.getAdditionalSocietyPlanLogList()
						.get(additionalSocietyPlan.getAdditionalSocietyPlanLogList().size() - 1);

				planNoteDetails = additionalSocietyPlan.getAdditionalSocietyPlanNoteList().get(0).getNote();

				// loadForwardToUsers();

				eventLogList = new ArrayList<>();

				approvedEventLogList = new ArrayList<>();

				rejectedEventLogList = new ArrayList<>();

				if (additionalSocietyPlan.getAdditionalSocietyPlanLogList() != null
						&& !additionalSocietyPlan.getAdditionalSocietyPlanLogList().isEmpty()) {
					EmployeeMaster empDetails = null;
					for (AdditionalSocietyPlanLog planLog : additionalSocietyPlan.getAdditionalSocietyPlanLogList()) {
						String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
								+ planLog.getCreatedBy().getId();

						BaseDTO dto = httpService.get(url);
						if (dto != null && dto.getStatusCode() == 0) {
							ObjectMapper maper = new ObjectMapper();
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);

						} else {
							AppUtil.addError(" Employee Details Not Found for the User ");
							return null;
						}

						/*
						 * EventLog eventLog = new EventLog("", planLog.getCreatedByName(),
						 * empDetails.getPersonalInfoEmployment().getDesignation().getName(),
						 * planLog.getCreatedDate(), planLog.getRemarks()); if
						 * (planLog.getStage().equals(PlanStage.INITIATED.toString()) ||
						 * planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
						 * eventLog.setTitle("Created By"); } else if
						 * (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
						 * eventLog.setTitle("Approved By"); approvedEventLogList.add(eventLog); } else
						 * if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
						 * eventLog.setTitle("Rejected By"); rejectedEventLogList.add(eventLog); } else
						 * if (planLog.getStage().equals(GovtSocietyPlanStatus.SUBMITTED.toString())) {
						 * if (selectedContractExportSocietyPlan == null) {
						 * log.info("<---Please Select any one Plan--->");
						 * errorMap.notify(ErrorDescription.SELECT_ONE_PLAN.getCode()); return null; }
						 * eventLog.setTitle("Submited By"); approvedEventLogList.add(eventLog); }
						 * eventLogList.add(eventLog);
						 */
					}
				}

				log.info("eventLogList", eventLogList);

				Collections.sort(eventLogList, new Comparator<EventLog>() {
					public int compare(EventLog m1, EventLog m2) {
						return m1.getTitle().compareTo(m2.getTitle());
					}
				});
				return VIEW_ADDTIONAL_PAGE_URL;

			} else {
				log.error(
						"Status code:" + baseDTO.getStatusCode() + " Error Message: " + baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Error in gotoViewPage  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String clearAddtionalPlan() {

		selectedAdditionalSocietyPlan = new AdditionalSocietyPlan();
		loadLazyAdditionalSocietyPlanList();
		return LIST_ADDTIONAL_EXPORT_URL;

	}

	public String approveRejectAdditionalPlan(String status) {

		AdditionalSocietyDTO additionalSocietyDTO = new AdditionalSocietyDTO();

		if (totalCurrentReqQty == null || totalCurrentReqQty.equals(0.00)) {
			errorMap.notify(ErrorDescription.ENTER_VALID_CURRENT_QTY.getCode());
			return null;
		}
		additionalSocietyPlan.getAdditionalSocietyPlanNoteList().clear();
		if ("APPROVED".equals(status)) {
			AdditionalSocietyPlanNote additionalSocietyPlanNoteObj = new AdditionalSocietyPlanNote();
			additionalSocietyPlanNoteObj.setNote(planNoteDetails);
			additionalSocietyPlanNoteObj.setAdditionalSocietyPlan(additionalSocietyPlan);
			additionalSocietyPlanNoteObj.setUserMaster(additionalSocietyPlanNote.getUserMaster());
			additionalSocietyPlanNoteObj.setFinalApproval(additionalSocietyPlanNote.getFinalApproval());
			additionalSocietyPlan.getAdditionalSocietyPlanNoteList().add(additionalSocietyPlanNoteObj);
		}

		AdditionalSocietyPlanLog additionalSocietyPlanLog = new AdditionalSocietyPlanLog();
		additionalSocietyPlanLog.setStage(status);
		additionalSocietyPlanLog.setAdditionalSocietyPlan(additionalSocietyPlan);
		if ("FINAL_APPROVED".equals(status)) {
			additionalSocietyPlanLog.setRemarks(finalApproveComments);
		} else if ("REJECTED".equals(status)) {
			additionalSocietyPlanLog.setRemarks(rejectComments);
		} else if ("APPROVED".equals(status)) {
			additionalSocietyPlanLog.setRemarks(approveComments);
		}
		additionalSocietyPlan.getAdditionalSocietyPlanLogList().clear();
		additionalSocietyPlan.getAdditionalSocietyPlanLogList().add(additionalSocietyPlanLog);
		additionalSocietyPlan.setStage(status);
//		for (AdditionalSocietyPlanLog log : additionalSocietyPlan.getAdditionalSocietyPlanLogList()) {
//			log.setStage(status);
//			log.setAdditionalSocietyPlan(additionalSocietyPlan);
//
//		}
		additionalSocietyDTO.setAdditionalSocietyPlanVal(additionalSocietyPlan);

		String url = SERVER_URL + "/govt/societyPlan/approvesocietyplanbyaddtional";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, additionalSocietyDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					if (additionalSocietyPlan.getStage().equalsIgnoreCase("FINAL_APPROVED")) {
						AppUtil.addInfo("Additional Society Plan Final Approved successfully");
					} else if (additionalSocietyPlan.getStage().equalsIgnoreCase("APPROVED")) {
						AppUtil.addInfo("Additional Society Plan  Approved successfully");
					} else {
						AppUtil.addInfo("Additional Society Plan  Rejected successfully");
					}
					log.info("Additional Society Plan Approved successfully");

					loadLazyAdditionalSocietyPlanList();
					return LIST_ADDTIONAL_EXPORT_URL;

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addError("Govt Society Plan not able to Submitted");
					return null;
				}
			}
			return null;
		} catch (Exception exp) {
			log.error("Error in submitDraftPlan", exp);
			log.info("Plan is submitted successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(8545);
			return null;
		}
	}

	public String submitApprovedRejectStatus(String status) {
		log.info("<=========== Starts GovtSocietyWiseProductionPlanBean submitApprovedRejectStatus =========> "
				+ status);
		Long id = 0l;
		if (planType.equalsIgnoreCase("Govt_Scheme")) {
			if (govtSocietyPlanResponseDTO.getGovtSocietyPlanId() != null) {
				if (planNote.getUserMaster() == null || planNote.getUserMaster().getId() == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_TO_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (planNote.getFinalApproval() == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_FOR_IS_NOT_EMPTY).getCode());
					return null;
				}
				id = govtSocietyPlanResponseDTO.getGovtSocietyPlanId();
				log.info("<===== Govt SocietyWise Production PlanId ========> "
						+ govtSocietyPlanResponseDTO.getGovtSocietyPlanId());
				govtSocietyPlanResponseDTO.setPlanId(id);
				govtSocietyPlanResponseDTO.setForwardTo(planNote.getUserMaster());
				govtSocietyPlanResponseDTO.setFinalApproval(planNote.getFinalApproval());
			}
		} else {
			if (selectedContractExportSocietyPlan != null) {
				if (contractExportPlanNote.getUserMaster() == null
						|| contractExportPlanNote.getUserMaster().getId() == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_TO_IS_NOT_EMPTY).getCode());
					return null;
				}
				if (contractExportPlanNote.getFinalApproval() == null) {
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.FORWARD_FOR_IS_NOT_EMPTY).getCode());
					return null;
				}
				id = selectedContractExportSocietyPlan.getId();
				log.info("<===== Contract Export SocietyWise Production PlanId ========> "
						+ selectedContractExportSocietyPlan.getId());
				govtSocietyPlanResponseDTO.setPlanId(id);
				govtSocietyPlanResponseDTO.setForwardTo(contractExportPlanNote.getUserMaster());
				govtSocietyPlanResponseDTO.setFinalApproval(contractExportPlanNote.getFinalApproval());
			}
		}
		if (status.equalsIgnoreCase(CreditSalesDemandStatus.FINAL_APPROVED.toString())) {
			govtSocietyPlanResponseDTO.setPlanStatus(CreditSalesDemandStatus.FINAL_APPROVED.toString());
			log.info("<===== finalApproveComments =====> " + finalApproveComments);
			govtSocietyPlanResponseDTO.setPlanLogRemarks(finalApproveComments);
		} else if (status.equalsIgnoreCase(CreditSalesDemandStatus.APPROVED.toString())) {
			govtSocietyPlanResponseDTO.setPlanStatus(CreditSalesDemandStatus.APPROVED.toString());
			log.info("<===== ApproveComments =====> " + approveComments);
			govtSocietyPlanResponseDTO.setPlanLogRemarks(approveComments);
		} else {
			govtSocietyPlanResponseDTO.setPlanStatus(CreditSalesDemandStatus.REJECTED.toString());
			log.info("<===== rejectComments =====> " + rejectComments);
			govtSocietyPlanResponseDTO.setPlanLogRemarks(rejectComments);
		}
		govtSocietyPlanResponseDTO.setPlanType(planType);
		log.info("submited info" + govtSocietyPlanResponseDTO);
		try {
			String url = SERVER_URL + "/govt/societyPlan/approverejectgovtcontractsocietyproductionplan";
			BaseDTO baseDTO = httpService.post(url, govtSocietyPlanResponseDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				if (status.equalsIgnoreCase(CreditSalesDemandStatus.FINAL_APPROVED.toString())
						|| status.equalsIgnoreCase(CreditSalesDemandStatus.APPROVED.toString())) {
					log.info("<========== GovtSocietyWiseProductionPlanBean Approved Successfully ==========>");
					errorMap.notify(ErrorDescription.CIRCULAR_APPROVE_SUCCESS.getCode());
				} else {
					log.info("<========== GovtSocietyWiseProductionPlanBean Rejected Successfully ==========>");
					errorMap.notify(ErrorDescription.CIRCULAR_REJECTED_SUCCESS.getCode());
				}
				selectedContractExportSocietyPlan = new ContractExportSocietyPlan();
				if (planType.equalsIgnoreCase("Govt_Scheme")) {
					return getListPage();
				} else {
					return showConExportList();
				}
			}
		} catch (Exception e) {
			log.error("Exception :: submitApprovedRejectStatus==> ", e);
			// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=========== Ends GovtSocietyWiseProductionPlanBean submitApprovedRejectStatus =========>");
		return null;
	}

	public void clearDandPofficeDetails() {
		productVarietyMaster = new ProductVarietyMaster();
		productGroupMaster = new ProductGroupMaster();
		productCategory = new ProductCategory();
		uomMaster = new UomMaster();
	}

	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String contractExportId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			contractExportId = httpRequest.getParameter("contractExportId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(contractExportId)) {
			selectedContractExportSocietyPlan = new ContractExportSocietyPlan();
			action = "VIEW";
			selectedContractExportSocietyPlan.setId(Long.parseLong(contractExportId));

			selectedContractExportSocietyPlan.setNotificationId(Long.parseLong(notificationId));
			gotoConExportViewPage();
		}
		log.info("CircularBean showViewListPage() Ends");
		return VIEW_PAGE_URL;
	}

}
