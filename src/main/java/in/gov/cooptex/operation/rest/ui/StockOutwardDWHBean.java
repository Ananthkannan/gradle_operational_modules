package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ShowroomType;
import in.gov.cooptex.core.enums.WarehouseType;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.model.TransportMaster;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNoType;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockOutwardDWHBean")
@Scope("session")
public class StockOutwardDWHBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST = "/pages/productWarehouse/outward/listStockOutwardPWH.xhtml?faces-redirect=true;";

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE = "/pages/productWarehouse/outward/createStockOutwardPWH.xhtml?faces-redirect=true;";

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_VIEW = "/pages/productWarehouse/outward/viewStockOutwardPWH.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnProductAdd = true;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	LazyDataModel<StockTransferRequest> stockTransferRequestLazyList;

	@Getter
	@Setter
	StockTransferRequest stockTransferRequest;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	List<Object> stockMovementToValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	public String getListPage() {
		log.info("Going to list page");
		disableAddButton = false;
		SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		// stockMovementToValues = stockTransferUtility.loadStockMovementToList();
		stockTransferRequest = new StockTransferRequest();
		loadLazyList();

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("senderCodeOrName")) {
				request.setSenderCodeOrName(value);
			}

			if (entry.getKey().equals("stockMovementTo")) {
				request.setStockMovementTo(value);
			}

			if (entry.getKey().equals("receiverCodeOrName")) {
				request.setReceiverCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}

			if (entry.getKey().equals("dateTransferred")) {
				request.setDateTransferred(AppUtil.serverDateFormat(value));
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/outward/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferRequestLazyList = new LazyDataModel<StockTransferRequest>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferRequest> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferRequest> data = new ArrayList<StockTransferRequest>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferRequest>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferRequest res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferRequest getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferRequest> responseList = (List<StockTransferRequest>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferRequest res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferRequest = ((StockTransferRequest) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferRequest = new StockTransferRequest();
		disableAddButton = false;
	}

	/*********************************************************************************************************************************/

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<Object> stockMovementToList;

	@Getter
	@Setter
	List<Object> warehouseTypeList;

	@Getter
	@Setter
	String selectedWarehouseType;

	@Getter
	@Setter
	List<EntityMaster> warehouseList;

	@Getter
	@Setter
	List<Object> showroomTypeList;

	@Getter
	@Setter
	String selectedShowroomType;

	@Getter
	@Setter
	List<EntityMaster> regionalOfficeList;

	@Getter
	@Setter
	EntityMaster selectedRegionalOffice;

	@Getter
	@Setter
	List<EntityMaster> showroomList;

	public String gotoPage(String page) {
		log.info("Button is pressed....", page);

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null
				|| !loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.PRODUCT_WAREHOUSE)) {
			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_PRODUCT_WAREHOUSE.getCode());

			return null;
		} else {
			if (page.equals("ADD")) {
				return gotoAddPage();
			} else if (page.equals("EDIT")) {
				return gotoEditPage();
			} else if (page.equals("VIEW")) {
				return gotoViewPage();
			} else {
				return null;
			}
		}
	}

	public String gotoAddPage() {
		disableBtnProductAdd = true;
		selectedWarehouseType = null;
		selectedShowroomType = null;
		// stockMovementToList = stockTransferUtility.loadStockMovementToList();
		// warehouseTypeList = stockTransferUtility.loadWarehouseTypeList();
		showroomTypeList = stockTransferUtility.loadShowroomTypeList();
		stockTransfer = new StockTransfer();
		stockTransfer.setFromEntityMaster(loginEmployee.getPersonalInfoEmployment().getWorkLocation());
		stockTransferItems = new StockTransferItems();
		productVarietyMasterSet = new HashSet<>();
		bundleNumberSet = new HashSet<String>();
		stockTransferItemsDisplayListTemp = new ArrayList<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		displayRemoveButtonItemDetails = true;
		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	public String gotoEditPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		if (stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED.name())) {
			stockTransfer = commonDataService.getStockTransferById(stockTransferRequest.getId());
			disableBtnProductAdd = false;
			// stockMovementToList = stockTransferUtility.loadStockMovementToList();
			// warehouseTypeList = stockTransferUtility.loadWarehouseTypeList();
			showroomTypeList = stockTransferUtility.loadShowroomTypeList();

			if (stockTransfer.getToEntityMaster() != null) {

				if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.SHOW_ROOM)) {
					selectedRegionalOffice = commonDataService
							.findRegionByShowroomId(stockTransfer.getToEntityMaster().getId());
					if (stockTransfer.getToEntityMaster().getIsLocalState()) {
						selectedShowroomType = ShowroomType.Intrastate.name();
					} else {
						selectedShowroomType = ShowroomType.Interstate.name();
					}
				} else if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.PRODUCT_WAREHOUSE)) {
					selectedWarehouseType = WarehouseType.Product_Warehouse.name();
				} else if (stockTransfer.getToEntityMaster().getEntityTypeMaster().getEntityCode()
						.equals(EntityType.DISTRIBUTION_WAREHOUSE)) {
					selectedWarehouseType = WarehouseType.Distribution_Warehouse.name();
				}

			}

			if (stockTransfer.getTransportMaster() != null) {
				selectedTransportTypeMaster = stockTransfer.getTransportMaster().getTransportTypeMaster();
			}

			populateFormData();
			if (selectedTransportTypeMaster != null) {
				transportMasterList = commonDataService
						.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
			}

			displayRemoveButtonItemDetails = true;

		} else {
			errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
			return null;
		}

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	public String gotoViewPage() {

		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = commonDataService.getStockTransferById(stockTransferRequest.getId());
		displayRemoveButtonItemDetails = false;
		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_VIEW;
	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {
		if (stockTransferRequest == null || stockTransferRequest.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {
			if (!stockTransferRequest.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferRequest.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public void onChangeStockMovementTo() {
		log.info("onChangeStockMovementTo [" + stockTransfer.getStockMovementTo() + "]");

		stockTransfer.setToEntityMaster(null);

		selectedWarehouseType = null;

		selectedShowroomType = null;
		selectedRegionalOffice = null;
	}

	public void onChangeWarehouseType() {
		log.info("onChangeWarehouseType [" + selectedWarehouseType + "]");

		stockTransfer.setToEntityMaster(null);

		if (selectedWarehouseType != null) {
			if (selectedWarehouseType.equals(WarehouseType.Product_Warehouse.name())) {
				warehouseList = commonDataService.findEntityByEntityType(EntityType.PRODUCT_WAREHOUSE);
			} else if (selectedWarehouseType.equals(WarehouseType.Distribution_Warehouse.name())) {
				warehouseList = commonDataService.findEntityByEntityType(EntityType.DISTRIBUTION_WAREHOUSE);
			} else {
				warehouseList = null;
			}
		}
	}

	public void onChangeWarehouse() {
		log.info("onChangeWarehouse [" + stockTransfer.getToEntityMaster() + "]");
		onChangeToEntityMaster();
	}

	public void onChangeShowroomType() {
		log.info("onChangeShowroomType [" + selectedShowroomType + "]");

		stockTransfer.setToEntityMaster(null);
		selectedRegionalOffice = null;
		showroomList = null;

		if (selectedShowroomType != null) {

			if (selectedShowroomType.equals(ShowroomType.Intrastate.name())) {
				regionalOfficeList = commonDataService.findRegionalOfficeOfShowroomsByLocalState(true);
			} else if (selectedShowroomType.equals(ShowroomType.Interstate.name())) {
				regionalOfficeList = commonDataService.findRegionalOfficeOfShowroomsByLocalState(false);
			} else {
				regionalOfficeList = null;
			}
		}
	}

	public void onChangeRegionalOffice() {
		log.info("onChangeRegionalOffice [" + selectedRegionalOffice + "]");

		stockTransfer.setToEntityMaster(null);

		if (selectedRegionalOffice != null && selectedShowroomType != null) {

			if (selectedShowroomType.equals(ShowroomType.Intrastate.name())) {
				showroomList = commonDataService
						.findShowroomByRegionalOfficeAndLocalState(selectedRegionalOffice.getId(), true);
			} else if (selectedShowroomType.equals(ShowroomType.Interstate.name())) {
				showroomList = commonDataService
						.findShowroomByRegionalOfficeAndLocalState(selectedRegionalOffice.getId(), false);
			} else {
				showroomList = null;
			}

		}

	}

	public void onChangeShowroom() {
		log.info("onChangeRegionalOffice [" + stockTransfer.getToEntityMaster() + "]");
		onChangeToEntityMaster();

		log.info("onChange complete");
	}

	public void onChangeToEntityMaster() {
		productVarietyMasterSet = null;
		transportTypeMasterList = null;
		selectedTransportTypeMaster = null;

		if (stockTransfer.getToEntityMaster() != null) {
			// Reset the Stock Transfer Object
			stockTransferItems = new StockTransferItems();
			stockTransfer.setDateTransferred(new Date());
			disableBtnProductAdd = false;
			populateFormData();
		}
	}

	/***************************************************************************************************************************************/

	@Getter
	@Setter
	Set<ProductVarietyMaster> productVarietyMasterSet;

	@Getter
	@Setter
	StockTransferItems stockTransferItems = new StockTransferItems();

	@Getter
	@Setter
	Set<String> bundleNumberSet = new HashSet<String>();

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayListTemp;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDisplay;

	@Getter
	@Setter
	boolean displayRemoveButtonItemDetails = false;

	public void onSelectProductVarietyMaster() {
		log.info("Stock Transfer " + stockTransfer);
	}

	public String addProduct() {
		log.info("addProduct is called");

		if (stockTransferItems != null && stockTransferItems.getBundleNumber() != null
				&& stockTransferItems.getAtNumber() != null) {

			if (stockTransferUtility.isAtNumberDuplicated(stockTransferItems.getAtNumber(),
					stockTransfer.getStockTransferItemsList())) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getCode());
				return null;
			}

			StockTransferItems items = findQCProductByAtNumber(stockTransferItems.getAtNumber());
			if (items != null) {
				items.setBundleNumber(stockTransferItems.getBundleNumber());
				stockTransfer.getStockTransferItemsList().add(items);
				String bundleNumber = stockTransferItems.getBundleNumber();
				stockTransferItems = new StockTransferItems();
				stockTransferItems.setBundleNumber(bundleNumber);

				addItemToDisplayList(items);
			} else {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_PRODUCT_NOT_FOUND_FOR_ATNUMBER.getCode());
				return null;
			}
		}

		return null;
	}

	public void addItemToDisplayList(StockTransferItems items) {

		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setBundleNumber(items.getBundleNumber());
		bundles.setAtNumber(items.getAtNumber());
		bundles.setQuantity(1D);
		bundles.setStockTransferItems(items);
		bundles.setItemNetTotal(items.getItemNetTotal());
		bundleNumberSet.add(items.getBundleNumber());
		updateBundleNumbers(bundleNumberSet);

		if (stockTransferItemsDisplayList == null) {
			stockTransferItemsDisplayList = new ArrayList<>();
		}

		if (!stockTransferItemsDisplayList.isEmpty()) {
			stockTransferItemsDisplayList = new ArrayList<>();
			for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
				if (sti.getProductVarietyMaster().getId().equals(items.getProductVarietyMaster().getId())) {
					sti.setReceivedQty(sti.getReceivedQty() + 1);
					sti.setItemNetTotal(sti.getItemNetTotal() + bundles.getItemNetTotal());
					sti.getStockTransferItemsBundlesList().add(bundles);
				}
				stockTransferItemsDisplayList.add(sti);
			}
		} else {
			items.getStockTransferItemsBundlesList().add(bundles);
			items.setReceivedQty(1D);
			stockTransferItemsDisplayList.add(items);
		}

		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
	}

	public void removeReceiveProduct(StockTransferItemsBundles bundles) {
		log.info("removeReceiveProduct ", bundles);
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList()
					.removeIf(item -> item.getAtNumber().equals(bundles.getAtNumber()));
			removeItemFromDisplayList(bundles);
		}
	}

	public void removeItemFromDisplayList(StockTransferItemsBundles bundles) {
		if (bundleNumberSet != null && !bundleNumberSet.isEmpty()) {
			bundleNumberSet.removeIf(bundleNumber -> bundleNumber.equals(bundles.getBundleNumber()));
			updateBundleNumbers(bundleNumberSet);
		}

		if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {
			StockTransferItems itemToRemove = null;
			boolean removeItemFlag = false;
			for (StockTransferItems items : stockTransferItemsDisplayList) {
				boolean isBundleRemoved = false;
				if (items.getProductVarietyMaster().getId()
						.equals(bundles.getStockTransferItems().getProductVarietyMaster().getId())) {

					if (items.getStockTransferItemsBundlesList() != null
							&& !items.getStockTransferItemsBundlesList().isEmpty()
							&& items.getStockTransferItemsBundlesList()
									.removeIf(b -> b.getAtNumber().equals(bundles.getAtNumber()))) {
						isBundleRemoved = true;
					}
					if (isBundleRemoved) {
						items.setReceivedQty(items.getReceivedQty() - 1);
						items.setItemNetTotal(AppUtil.ifNullRetunZero(items.getItemNetTotal())
								- AppUtil.ifNullRetunZero(bundles.getItemNetTotal()));
						if (items.getReceivedQty() == 0) {
							removeItemFlag = true;
							itemToRemove = items;
						}
					}
				}
			}
			if (removeItemFlag) {
				stockTransferItemsDisplayList.remove(itemToRemove);
			}
		}

	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null && !setOfBundleNumbers.isEmpty()) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	public void viewItemDetails(StockTransferItems items) {
		stockTransferItemsDisplay = items;
	}

	/*************************************************************************************************************************************/

	@Getter
	@Setter
	List<TransportTypeMaster> transportTypeMasterList;

	@Getter
	@Setter
	List<TransportMaster> transportMasterList;

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	List<YesNoType> yesOrNoList;

	@Getter
	@Setter
	List<Object> payToPayList;

	public void populateFormData() {
		transportTypeMasterList = commonDataService.getAllTransportTypes();
		yesOrNoList = stockTransferUtility.loadYesNoList();
		payToPayList = stockTransferUtility.loadTransportChargeType();
	}

	public void onChangeTransportTypeMaster() {
		transportMasterList = null;
		stockTransfer.setTransportMaster(null);
		if (selectedTransportTypeMaster != null) {
			transportMasterList = commonDataService
					.getAllTransportsByTransportType(selectedTransportTypeMaster.getId());
		}
	}

	public void onChangeTransport() {
		log.info("onSelectTransport is called");
		if (stockTransfer.getTransportMaster() != null) {
			log.info("Selected Transport ID = [" + stockTransfer.getTransportMaster().getId() + "]");
		}
	}

	/*************************************************************************************************************************************/

	public StockTransferItems findQCProductByAtNumber(String atNumber) {

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/outward/items/" + atNumber;
		StockTransferItems stockTransferItems = null;
		try {

			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					stockTransferItems = mapper.readValue(jsonResponse, StockTransferItems.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("Exception", e);
		}
		return stockTransferItems;
	}

	public String submit(String action) {

		if (action.equals("save")) {
			stockTransfer.setStatus(StockTransferStatus.INITIATED);
		} else {
			stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		}

		stockTransfer.setTransferType(StockTransferType.WAREHOUSE_OUTWARD);

		return create();
	}

	public String create() {
		log.info("create method is executing..");

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/outward/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

}
