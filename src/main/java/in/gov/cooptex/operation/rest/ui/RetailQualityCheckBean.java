package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.ItemStatus;
import in.gov.cooptex.operation.enums.RetailQualityCheckStatus;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.QualityCheckProductWise;
import in.gov.cooptex.operation.model.RetailQualityCheck;
import in.gov.cooptex.operation.model.RetailQualityCheckDetails;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.RetailQualityCheckSearch;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailQualityCheckBean")
@Scope("session")
public class RetailQualityCheckBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String RETAIL_QUALITY_CHECK_LIST = "/pages/productWarehouse/qc/listQualityCheck.xhtml?faces-redirect=true;";

	private final String RETAIL_QUALITY_CHECK_CREATE = "/pages/productWarehouse/qc/createQualityCheck.xhtml?faces-redirect=true;";

	private final String RETAIL_QUALITY_CHECK_VIEW = "/pages/productWarehouse/qc/viewQualityCheck.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String activeStatus;

	@Getter
	@Setter
	RetailQualityCheckSearch retailQualityCheckSearch;

	@Getter
	@Setter
	RetailQualityCheck retailQualityCheck;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	LazyDataModel<RetailQualityCheckSearch> retailQualityCheckSearchLazyList;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	@Getter
	@Setter
	QualityCheckProductWise selectedQualityCheckProductWise;

	@Getter
	@Setter
	Integer totalProducts;

	@Getter
	@Setter
	Integer qcCompleted;

	@Getter
	@Setter
	Integer qcInprogress;

	@Getter
	@Setter
	Integer qcYetToStart;

	@Getter
	@Setter
	Integer totalItems;

	@Getter
	@Setter
	Integer itemsAccepted;

	@Getter
	@Setter
	Integer itemsRejected;

	@Getter
	@Setter
	Integer itemsToBeCheck;

	@Getter
	@Setter
	Integer totalItemsForProduct;

	@Getter
	@Setter
	Integer itemsAcceptedForProduct;

	@Getter
	@Setter
	Integer itemsRejectedForProduct;

	@Getter
	@Setter
	Integer itemsToBeCheckForProduct;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	String errorMsg;

	@Getter
	@Setter
	Long loggedInWarehouseId;

	@Getter
	@Setter
	int index;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	Boolean multiSelectFlag = false;

	@Getter
	@Setter
	List<RetailQualityCheckDetails> selectedretailQualityCheckDetailsList;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		getProductWarehouseQualityCheckList();
		loadStatusValues();
	}

	public RetailQualityCheckBean() {
		log.info("<#======Inside PurchaseOrderBean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	private void loadStatusValues() {

		Object[] statusArray = RetailQualityCheckStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public void loadLazyRetailQualityCheckList() {
		addButtonFlag = false;
		log.info("RetailQualityCheckBean.loadLazyPurchaseOrderList method started");
		retailQualityCheckSearchLazyList = new LazyDataModel<RetailQualityCheckSearch>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailQualityCheckSearch> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<RetailQualityCheckSearch> data = new ArrayList<RetailQualityCheckSearch>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetailQualityCheckSearch>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(RetailQualityCheckSearch res) {
				return res != null ? res.getStockInwardNumber() : null;
			}

			@Override
			public RetailQualityCheckSearch getRowData(String rowKey) {
				List<RetailQualityCheckSearch> responseList = (List<RetailQualityCheckSearch>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RetailQualityCheckSearch res : responseList) {
					if (res.getId() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	private RetailQualityCheckSearch getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		RetailQualityCheckSearch request = new RetailQualityCheckSearch();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("stockInwardNumber")) {
				log.info("stock Inward Number : " + value);
				request.setStockInwardNumber(value);
			}

			if (entry.getKey().equals("senderCodeOrName")) {
				log.info("Sender Code Or Name : " + value);
				request.setSenderCodeOrName(value);
			}
			if (entry.getKey().equals("receiverCodeOrName")) {
				log.info("Receiver Code Or Name : " + value);
				request.setReceiverCodeOrName(value);
			}
			if (entry.getKey().equals("stockReceivedDate")) {
				log.info("stock Received Date : " + value);
				request.setStockReceivedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("RetailQualityCheckBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		retailQualityCheckSearch = new RetailQualityCheckSearch();
		RetailQualityCheckSearch request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null
				&& loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
			log.info("login user entity id==> "+loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
			request.setLoginUserEntityId(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
		}
		
		if(loginBean.getUserMaster() != null && loginBean.getUserMaster().getId() != null) {
			log.info("login user id==> "+loginBean.getUserMaster().getId());
			request.setLoginUserId(loginBean.getUserMaster().getId());
		}

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/quality/check/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("RetailQualityCheckBean.getSearchData Method Complted");
		return baseDTO;
	}

	public String getProductWarehouseQualityCheckList() {
		retailQualityCheckSearch = new RetailQualityCheckSearch();
		loadLazyRetailQualityCheckList();
		return RETAIL_QUALITY_CHECK_LIST;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailQualityCheckBean.onRowSelect method started");
		retailQualityCheckSearch = ((RetailQualityCheckSearch) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		addButtonFlag = false;
	}

	public String getProductWarehouseQualityCheckCreate() {
		retailQualityCheck = new RetailQualityCheck();
		retailQualityCheck.setStockTransfer(new StockTransfer());
		supplierMaster = new SupplierMaster();
		productCategory = new ProductCategory();
		retailQualityCheck.setQualityCheckProductWiseList(new ArrayList<>());
		totalProducts = null;
		qcCompleted = null;
		qcInprogress = null;
		qcYetToStart = null;
		totalItems = null;
		itemsAccepted = null;
		itemsRejected = null;
		itemsToBeCheck = null;
		setLoggedInWarehouse();
		getSocietyList();
		stockTransferList = new ArrayList<>();

		return RETAIL_QUALITY_CHECK_CREATE;
	}

	public String getEditPage() {
		addButtonFlag = false;
		setLoggedInWarehouse();
		getSocietyList();
		productCategory = new ProductCategory();

		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {
			errorMap.notify(34254);
			return null;
		} else if (retailQualityCheckSearch.getStatus().equals("")) {
			return null;
		} else {
			if (retailQualityCheckSearch.getStatus().equals(RetailQualityCheckStatus.QC_COMPLETE.name())) {
				errorMap.notify(8517);
				return null;
			}
			retailQualityCheck = getRetailQualityCheck(retailQualityCheckSearch.getId());

			if (retailQualityCheck.getStockTransfer() != null
					&& retailQualityCheck.getStockTransfer().getSupplierMaster() != null) {
				supplierMaster = retailQualityCheck.getStockTransfer().getSupplierMaster();
				// getStockTransfers();
				stockTransferList = commonDataService.findStockBySupplierAndStatusAndTransferType(
						supplierMaster.getId(), StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD,
						loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
				getProductCountDetails();
				getItemCountDetails();
			}
			return RETAIL_QUALITY_CHECK_CREATE;
		}

	}

	public String getViewPage() {

		setLoggedInWarehouse();
		getSocietyList();
		productCategory = new ProductCategory();

		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {
			errorMap.notify(34254);
			return null;
		} else if (retailQualityCheckSearch.getStatus().equals("")) {

			return null;
		} else {
			retailQualityCheck = getRetailQualityCheck(retailQualityCheckSearch.getId());
			getProductCountDetails();
			getItemCountDetails();
			return RETAIL_QUALITY_CHECK_VIEW;
		}

	}

	public void processDelete() {
		log.info("RetailQualityCheckBean.processDelete method started");
		addButtonFlag = false;
		if (retailQualityCheckSearch == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(24103);
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public void setLoggedInWarehouse() {
		if (loginBean.getUserProfile() != null && (EmployeeMaster) loginBean.getUserProfile() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
						.getWorkLocation() != null) {
			loggedInWarehouseId = ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
					.getWorkLocation().getId();
		}
	}

	public void getSocietyList() {
		supplierMasterList = commonDataService.findSupplierByProductWarehouse(loggedInWarehouseId,
				StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD);
	}

	public void getStockTransfers() {
		if (supplierMaster != null && supplierMaster.getId() != null)
			stockTransferList = commonDataService.findStockBySupplierAndQCNotCreated(supplierMaster.getId(),
					StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD);

	}

	public List<QualityCheckProductWise> qcItemDetails(Long stockTransferId) {
		List<QualityCheckProductWise> list = null;
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/get/qcitemdetails/" + stockTransferId;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			list = mapper.readValue(jsonResponse, new TypeReference<List<QualityCheckProductWise>>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return list;
	}

	public String getStockTransferData() {
		log.info("getStockTransferData started " + retailQualityCheck);
		if (retailQualityCheck == null || retailQualityCheck.getStockTransfer() == null
				|| retailQualityCheck.getStockTransfer().getId() == null) {
			errorMap.notify(24103);
			return null;
		}
		// StockTransfer stockTransfer = commonDataService
		// .getStockTransferById(retailQualityCheck.getStockTransfer().getId());

		StockTransfer stockTransfer = getStockTransferById(retailQualityCheck.getStockTransfer().getId());

		if (stockTransfer != null) {
			List<QualityCheckProductWise> productWiseList = qcItemDetails(
					retailQualityCheck.getStockTransfer().getId());
			for(QualityCheckProductWise q :productWiseList) {
				for(RetailQualityCheckDetails retailQualityCheckDetails: q.getRetailQualityCheckDetailList()) {
					retailQualityCheckDetails.setQualityCheckProductWise(null);
				}
			}
			retailQualityCheck.setQualityCheckProductWiseList(productWiseList);
			retailQualityCheck.setToEntity(stockTransfer.getToEntityMaster());
			retailQualityCheck.setFromEntity(stockTransfer.getFromEntityMaster());
			retailQualityCheck.setStockTransfer(stockTransfer);
			getProductCountDetails();
			getItemCountDetails();
			return null;
		}
		return null;
	}

	public StockTransfer getStockTransferById(Long stockTransferId) {
		StockTransfer stockTransfer = new StockTransfer();

		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/get/stocktransferbyid/" + stockTransferId;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			stockTransfer = mapper.readValue(jsonResponse, StockTransfer.class);
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}

		return stockTransfer;
	}

	public String delete() {
		log.info("RetailQualityCheckBean.delete method started");
		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {
			errorMap.notify(24103);
			return null;
		}
		if (retailQualityCheckSearch.getStatus().equals(RetailQualityCheckStatus.QC_IN_PROGRESS.name())) {
			String url = serverURL + appPreference.getOperationApiUrl() + "/quality/check/delete/"
					+ retailQualityCheckSearch.getId();
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.delete(url);
			} catch (Exception exp) {
				log.error("Error in delete draft plan", exp);
				errorMap.notify(1);
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(24101);
				return getProductWarehouseQualityCheckList();
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				return getProductWarehouseQualityCheckList();
			}
		} else {
			AppUtil.addError(errorMap.getMessage(24111));
			return null;
		}

	}

	public void selectProductWiseData(QualityCheckProductWise qualityCheckProductWise, int idx) {
		index = idx;
		if (retailQualityCheck.getQualityCheckProductWiseList().get(index).getProductVarietyMaster() != null
				&& retailQualityCheck.getQualityCheckProductWiseList().get(index).getProductVarietyMaster()
						.getProductGroupMaster() != null) {
			productCategory = retailQualityCheck.getQualityCheckProductWiseList().get(index).getProductVarietyMaster()
					.getProductGroupMaster().getProductCategory();
		}
		log.info("RetailQualityCheckBean.selectProductWiseData method started");
		if (qualityCheckProductWise != null) {
			selectedQualityCheckProductWise = qualityCheckProductWise;
			totalItemsForProduct = qualityCheckProductWise.getRetailQualityCheckDetailList().size();
			itemsAcceptedForProduct = calculateItemCountsForProduct(qualityCheckProductWise, ItemStatus.ACCEPTED);
			itemsRejectedForProduct = calculateItemCountsForProduct(qualityCheckProductWise, ItemStatus.REJECTED);
			itemsToBeCheckForProduct = calculateItemCountsForProduct(qualityCheckProductWise, ItemStatus.PENDING);
		}
		if(qualityCheckProductWise.getProductStatus().toString().equalsIgnoreCase("QC_IN_PROGRESS") ||
				qualityCheckProductWise.getProductStatus().toString().equalsIgnoreCase("QC_PENDING")) {
			RequestContext.getCurrentInstance().execute("PF('qualitycheckDlg').show();");
			RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
		}else if(qualityCheckProductWise.getProductStatus().toString().equalsIgnoreCase("QC_COMPLETE")){
			RequestContext.getCurrentInstance().execute("PF('qualitycheckDlgview').show();");
			RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
		}
	}
	public void getProductCountDetails() {
		if (retailQualityCheck.getQualityCheckProductWiseList() == null) {
			totalProducts = 0;
			qcCompleted = 0;
			qcInprogress = 0;
			qcYetToStart = 0;
			return;
		}
		totalProducts = retailQualityCheck.getQualityCheckProductWiseList().size();
		qcCompleted = calculateProductCounts(retailQualityCheck.getQualityCheckProductWiseList(),
				RetailQualityCheckStatus.QC_COMPLETE);
		qcInprogress = calculateProductCounts(retailQualityCheck.getQualityCheckProductWiseList(),
				RetailQualityCheckStatus.QC_IN_PROGRESS);
		qcYetToStart = calculateProductCounts(retailQualityCheck.getQualityCheckProductWiseList(),
				RetailQualityCheckStatus.QC_PENDING);

	}

	private int calculateProductCounts(List<QualityCheckProductWise> items, RetailQualityCheckStatus productStatus) {
		int count = 0;
		for (QualityCheckProductWise item : items) {
			if (item.getProductStatus().equals(productStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	public void getItemCountDetails() {
		totalItems = 0;
		if (retailQualityCheck.getQualityCheckProductWiseList() == null) {
			itemsAccepted = 0;
			itemsRejected = 0;
			itemsToBeCheck = 0;
			return;
		}

		for (QualityCheckProductWise item : retailQualityCheck.getQualityCheckProductWiseList()) {
			totalItems = totalItems + item.getRetailQualityCheckDetailList().size();
		}

		itemsAccepted = calculateItemCounts(retailQualityCheck.getQualityCheckProductWiseList(), ItemStatus.ACCEPTED);
		itemsRejected = calculateItemCounts(retailQualityCheck.getQualityCheckProductWiseList(), ItemStatus.REJECTED);
		itemsToBeCheck = calculateItemCounts(retailQualityCheck.getQualityCheckProductWiseList(), ItemStatus.PENDING);

	}

	private int calculateItemCounts(List<QualityCheckProductWise> items, ItemStatus itemStatus) {
		int count = 0;
		for (QualityCheckProductWise item : items) {
			for (RetailQualityCheckDetails detail : item.getRetailQualityCheckDetailList()) {
				if (detail.getItemStatus().equals(itemStatus)) {
					count = count + 1;
				}
			}
		}
		return count;
	}

	private int calculateItemCountsForProduct(QualityCheckProductWise item, ItemStatus itemStatus) {
		int count = 0;
		for (RetailQualityCheckDetails detail : item.getRetailQualityCheckDetailList()) {
			if (detail.getItemStatus().equals(itemStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	public String saveQualityCheckItems(String requestType) {
		log.info("Retail Quality Check save method started");
		addButtonFlag = false;
		
		
		if (requestType.equals(RetailQualityCheckStatus.QC_IN_PROGRESS.name())) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index)
					.setProductStatus(RetailQualityCheckStatus.QC_IN_PROGRESS);
			retailQualityCheck.setQualityCheckStatus(RetailQualityCheckStatus.QC_IN_PROGRESS);
		} else {
			for (int ix = 0; ix < retailQualityCheck.getQualityCheckProductWiseList().get(index)
					.getRetailQualityCheckDetailList().size(); ix++) {
				if (!retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList()
						.get(ix).isVerified()) {
					errorMap.notify(24110);
					return null;
				}
			}

			retailQualityCheck.getQualityCheckProductWiseList().get(index)
					.setProductStatus(RetailQualityCheckStatus.QC_COMPLETE);
			retailQualityCheck.setQualityCheckStatus(RetailQualityCheckStatus.QC_COMPLETE);
		}
		
		stockTransferList = commonDataService.findStockBySupplierAndStatusAndTransferType(supplierMaster.getId(),
				StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD,
				loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());

		String url = serverURL + appPreference.getOperationApiUrl() + "/quality/check/create/"+index;
		BaseDTO baseDTO = null;
		try {
//			ObjectMapper map = new ObjectMapper();
//			String jsonValu = map.writeValueAsString(retailQualityCheck);
//			System.out.print("Json Value"+jsonValu);

			baseDTO = httpService.post(url, retailQualityCheck);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					try {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						retailQualityCheck = mapper.readValue(jsonValue, RetailQualityCheck.class);
						retailQualityCheck = getRetailQualityCheck(retailQualityCheck.getId());
						
					} catch (Exception e) {
						log.error("Error in Retail Quality Check save", e);
						errorMap.notify(1);
						return RETAIL_QUALITY_CHECK_CREATE;
					}
					log.info("Retail Quality Check Submitted successfully");
					errorMap.notify(24105);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in Retail Quality Check save", exp);
			errorMap.notify(1);
			return RETAIL_QUALITY_CHECK_CREATE;
		}
		if (retailQualityCheck.getStockTransfer() != null
				&& retailQualityCheck.getStockTransfer().getSupplierMaster() != null) {
			supplierMaster = retailQualityCheck.getStockTransfer().getSupplierMaster();
			//getStockTransfers();
			getProductCountDetails();
			getItemCountDetails();
		}
		return RETAIL_QUALITY_CHECK_CREATE;

	}

	public RetailQualityCheck getRetailQualityCheck(Long id) {
		String URL = serverURL + appPreference.getOperationApiUrl() + "/quality/check/get/" + id;
		RetailQualityCheck retailQualityCheck = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailQualityCheck = mapper.readValue(jsonValue, RetailQualityCheck.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return retailQualityCheck;
	}

	public void updateLength(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setProductLength(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateWidth(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setProductWidth(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateEndsPerInch(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setEndsPerInch(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updatePicksPerInch(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setPicksPerInch(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateProductWeight(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setProductWeight(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateStitchingStatus(int idx, boolean Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setStitchingStatus(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void validateData(int idx) {
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void selectCheckBox(int idx) {
		log.info(":::::selectCheckBox::::method index:::" + idx);
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setVerified(true);
		validateData(idx);
	}

	public void unselectCheckBox(int idx) {
		log.info(":::::unselectCheckBox::::method index:::" + idx);
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setVerified(false);
		validateData(idx);
	}

	public void validateDataMultiSelect() {
		RequestContext.getCurrentInstance().execute("PF('statusDialog').show();");
		log.info(selectedretailQualityCheckDetailsList.size() + ":::::::retailQualityCheckDetailList::::::::"
				+ retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList()
						.size());
		if(retailQualityCheck.getQualityCheckProductWiseList()
				.get(index).getRetailQualityCheckDetailList().size() > 300) {
		if (selectedretailQualityCheckDetailsList.size() == 0
				|| selectedretailQualityCheckDetailsList.size() < retailQualityCheck.getQualityCheckProductWiseList()
						.get(index).getRetailQualityCheckDetailList().size()) {
		selectedretailQualityCheckDetailsList=new ArrayList<>();
		selectedretailQualityCheckDetailsList.addAll(retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList());
		}else {
			selectedretailQualityCheckDetailsList=new ArrayList<>();
			RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
			return;
		}log.info(selectedretailQualityCheckDetailsList.size() + ":::::::retailQualityCheckDetailList::::::::"
				+ retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList()
						.size());
		}
		int idx = 0;
		if (selectedretailQualityCheckDetailsList.size() == retailQualityCheck.getQualityCheckProductWiseList()
				.get(index).getRetailQualityCheckDetailList().size()) {
			multiSelectFlag = true;
		} else {
			multiSelectFlag = false;
		}
		for (RetailQualityCheckDetails retailQualityCheckDetails : retailQualityCheck.getQualityCheckProductWiseList()
				.get(index).getRetailQualityCheckDetailList()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setVerified(multiSelectFlag);
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList();
			validateData(idx);
			idx = idx + 1;
		}
		RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
	}

	public boolean validateATNumberWiseDetails(int idx) {

		log.info("validateATNumberWiseDetails called ");
		RetailQualityCheckDetails qualityCheckDetails = retailQualityCheck.getQualityCheckProductWiseList().get(index)
				.getRetailQualityCheckDetailList().get(idx);
		if (!qualityCheckDetails.isVerified()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.PENDING);
		}
		ProductVarietyMaster productVarietyMaster = retailQualityCheck.getQualityCheckProductWiseList().get(index)
				.getProductVarietyMaster();

		Double lowestLengthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getLengthActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getLengthTolerance());
		Double lowestWidthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getWidthActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getWidthTolerance());
		Double lowestPicksPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchTolerance());
		Double lowestEndsPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getEndspPerInchActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getEndsPerInchTolerance());

		Double highestLengthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getLengthActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getLengthTolerance());
		Double highestWidthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getWidthActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getWidthTolerance());
		Double highestPicksPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchTolerance());
		Double highestEndsPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getEndspPerInchActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getEndsPerInchTolerance());

		lowestLengthTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestLengthTolerance);
		lowestWidthTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestWidthTolerance);
		lowestPicksPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestPicksPerInchTolerance);
		lowestEndsPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestEndsPerInchTolerance);

		highestLengthTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestLengthTolerance);
		highestWidthTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestWidthTolerance);
		highestPicksPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestPicksPerInchTolerance);
		highestEndsPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestEndsPerInchTolerance);

		if (qualityCheckDetails.getProductLength() != null
				&& (qualityCheckDetails.getProductLength() < lowestLengthTolerance
						|| qualityCheckDetails.getProductLength() > highestLengthTolerance)) {
			log.info("Length range should be : (" + lowestLengthTolerance + '-' + highestLengthTolerance + ')');
			errorMsg = "Length " + errorMap.getMessage(24109) + '(' + lowestLengthTolerance + '-'
					+ highestLengthTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			qualityCheckDetails.setProductLength(null);
			return false;
		}
		// if (qualityCheckDetails.getProductWidth() != null
		// && (qualityCheckDetails.getProductWidth() < lowestWidthTolerance
		// || qualityCheckDetails.getProductWidth() > highestWidthTolerance)) {
		// log.info("Width range should be : (" + lowestWidthTolerance + '-' +
		// highestWidthTolerance + ')');
		// errorMsg = "Width " + errorMap.getMessage(24109) + '(' + lowestWidthTolerance
		// + '-' + highestWidthTolerance
		// + ')';
		// retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
		// .setItemStatus(ItemStatus.REJECTED);
		// qualityCheckDetails.setProductWeight(null);
		// return false;
		// }

		if (qualityCheckDetails.getEndsPerInch() != null
				&& (qualityCheckDetails.getEndsPerInch() < lowestEndsPerInchTolerance
						|| qualityCheckDetails.getEndsPerInch() > highestEndsPerInchTolerance)) {
			log.info("Ends Per Inch range should be : (" + lowestEndsPerInchTolerance + '-'
					+ highestEndsPerInchTolerance + ')');
			errorMsg = "Ends Per Inch " + errorMap.getMessage(24109) + '(' + lowestEndsPerInchTolerance + '-'
					+ highestEndsPerInchTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			qualityCheckDetails.setEndsPerInch(null);
			return false;
		}
		if (qualityCheckDetails.getPicksPerInch() != null
				&& (qualityCheckDetails.getPicksPerInch() < lowestPicksPerInchTolerance
						|| qualityCheckDetails.getPicksPerInch() > highestPicksPerInchTolerance)) {
			log.info("Picks Per Inch range should be : (" + lowestPicksPerInchTolerance + '-'
					+ highestPicksPerInchTolerance + ')');
			errorMsg = "Picks Per Inch " + errorMap.getMessage(24109) + '(' + lowestPicksPerInchTolerance + '-'
					+ highestPicksPerInchTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			qualityCheckDetails.setPicksPerInch(null);
			return false;
		}

		errorMsg = null;
		if (retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.isVerified()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.ACCEPTED);
		} else {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.PENDING);
		}
		if (!qualityCheckDetails.isStitchingStatus()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
		}
		return true;

	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("QUALITY_CHECKING_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}

	public void selectAutoSearch() {
		log.info("<<=== inside selectAutoSearch");
		if (supplierMasterList.contains(supplierMaster)) {
			log.info("<<=== selected SupplierMaster", supplierMaster);
			getStockTransfers();
		}
	}

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete Autocomplete query==>" + query);
		if (supplierMaster != null) {
			String supplierCode = query;
			supplierMasterList = commonDataService.findSupplierByProductWarehouseAutoComplete(loggedInWarehouseId,
					StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD, supplierCode);

		} else {
			AppUtil.addWarn("====");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

		return supplierMasterList;
	}
	
	public void clearQualityCheckDetails() {
		log.info("======Start clearQualityCheckDetails()========");
		retailQualityCheck=new RetailQualityCheck();
		supplierMaster=new SupplierMaster();
		log.info("======End clearQualityCheckDetails()========");
	}
}