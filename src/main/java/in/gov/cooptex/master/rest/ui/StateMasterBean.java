package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class StateMasterBean {

	private final String CREATE_STATE_MASTER_PAGE = "/pages/masters/createStateMaster.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listStateMaster.xhtml?faces-redirect=true;";

	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewStateMaster.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	StateMaster stateMaster;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();

	@Getter
	@Setter
	List<CountryMaster> countryMasterList = new ArrayList<CountryMaster>();

	@Getter
	@Setter
	private int stateMasterListSize;

	boolean forceLogin = false;
	@Getter
	@Setter
	private int countryMasterListSize;
	
	@Getter	@Setter
	List<DistrictMaster> districtList = null;
	
	@Getter	@Setter
	List<TalukMaster> talukList = null;

	@Getter	@Setter
	List<CityMaster> cityList = null;

	@Getter	@Setter
	List<AreaMaster> areaList = null;
	
	@Getter	@Setter
	List<StateMaster> stateList = null;
	
	@Getter	
	@Setter
	Integer totalRecords = 0;
	
	@Getter	
	@Setter
	Boolean addButtonFlag=true;
	
	

	@PostConstruct
	public void init() {
		log.info(" stateMasterBean init --->" + serverURL);
		loadValues();
		addSupplier();
		getStateView(stateMaster);
		getAllStates();
		getAllCountriesList();
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			stateMaster = new StateMaster();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String showList() {
		log.info("<---------- Loading stateMasterBean showList list page---------->");
		loadValues();
		getAllStates();
		return INPUT_FORM_LIST_URL;
	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		stateMaster = new StateMaster();
		return INPUT_FORM_LIST_URL;

	}

	public String addSupplier() {
		log.info("<---- Start stateMasterBean.addstate ---->");
		try {
			action = "ADD";
			stateMaster = new StateMaster();
			stateMaster.setStatus(true);
			log.info("<---- End stateMaster.addState ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_STATE_MASTER_PAGE;
	}

	public String submit() {
		log.info("...... stateMasterBean submit begin ....");
		try {
			BaseDTO baseDTO = null;
			if (action == "ADD") {
				baseDTO = httpService.post(serverURL + "/stateMaster/createState", stateMaster);
			}else {
				baseDTO = httpService.put(serverURL + "/stateMaster/updateState", stateMaster);
			}
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("stateMasterBean saved successfully");
					showList();
					errorMap.notify(baseDTO.getStatusCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			log.error("Error while creating stateMaster" + e);
		}
		log.info("...... stateMasterBean submit ended ....");
		clear();
		return INPUT_FORM_LIST_URL;
	}

	private void getAllStates() {
		log.info("<<<< ----------Start stateMasterBean . getAllStates------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/stateMaster/getAllStates";
			baseDTO = httpService.get(url);
			stateMaster = new StateMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				stateMasterList = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
				});
				
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (stateMasterList != null) {
			stateMasterListSize = stateMasterList.size();
		} else {
			stateMasterListSize = 0;
		}
		log.info("<<<< ----------End stateMasterBean . getAllCountriesList ------- >>>>");
	}

	
	
	public String backPage() {

		showList();
		return INPUT_FORM_LIST_URL;
	}

	private void getAllCountriesList() {
		log.info("<<<< ----------Start stateMasterBean . getAllCountriesList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/country/getall";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				countryMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CountryMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (countryMasterList != null) {
			countryMasterListSize = countryMasterList.size();
		} else {
			countryMasterListSize = 0;
		}
		log.info("<<<< ----------End stateMasterBean . getAllCountriesList ------- >>>>");
	}

	public String editState() {
		log.info("<<<<< -------Start editState called ------->>>>>> ");
		action = "EDIT";
		if (stateMaster == null) {
			Util.addWarn("Please select one state");
			return null;
		}

		if (stateMaster.getStatus() != null && stateMaster.getStatus().equals("ACTIVE")) {
			stateMaster.setStatus(stateMaster.getStatus());
		} else {
			stateMaster.setStatus(stateMaster.getStatus());
		}
		log.info("<<<<< -------End editState called ------->>>>>> ");

		return CREATE_STATE_MASTER_PAGE;

	}

	public void onRowSelect()
	{
		addButtonFlag=false;
	}
	public String deleteState() {
		try {
			action = "Delete";
			if (stateMaster == null) {
				Util.addWarn("Please Select One country");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmStateDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	
	public String deleteStateConfirm() {

		try {
			log.info("Delete confirmed for state [" + stateMaster.getName() + "]");
			StateMaster stateMasterDelete = new StateMaster();
			stateMasterDelete.setId(stateMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/stateMaster/delete";
			baseDTO = httpService.post(url, stateMaster);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("state deleted successfully....!!!");
//					errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addInfo("state deleted successfully");
				stateMaster = new StateMaster();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update   invalidating session-------->");
					forceLogin = true;
					return forceLogin();

				} else if(baseDTO.getStatusCode() != 0){
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start stateMasterBean . clear ----->");
		getAllStates();
		stateMaster = new StateMaster();
		addButtonFlag=true;
		log.info("<---- End stateMasterBean . clear ----->");

		return INPUT_FORM_LIST_URL;

	}

	public String viewState() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (stateMaster == null) {
			Util.addWarn("Please Select One Country");
			return null;
		}
		return INPUT_FORM_VIEW_URL;

	}

	private void getStateView(StateMaster stateMaster) {
		log.info("<<<< ----------Start stateMasterBean . getstateView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/stateMaster/getById";
			baseDTO = httpService.post(url, stateMaster.getId());
			stateMaster = new StateMaster();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				stateMaster = mapper.readValue(jsonResponse, new TypeReference<CountryMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (stateMaster != null)
			log.info("statemaster getCountryView" + stateMaster);
		else
			log.info("statemaster getCountryView" + stateMaster);

		log.info("<<<< ----------End stateMasterBean . getCountryView ------- >>>>");
	}
	
}
