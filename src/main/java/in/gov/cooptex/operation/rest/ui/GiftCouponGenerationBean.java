package in.gov.cooptex.operation.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.ManagedBean;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.OrganizationMaster;
import in.gov.cooptex.core.model.PaymentMode;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.operation.model.GiftCoupon;
import in.gov.cooptex.operation.model.GiftCouponCategory;
import in.gov.cooptex.operation.model.GiftCouponRegister;
import in.gov.cooptex.operation.model.GiftCouponType;
import in.gov.cooptex.operation.model.OccasionType;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@ManagedBean("giftCouponGenerationBean")
@Scope("session")
@Log4j2
public class GiftCouponGenerationBean {

	private static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_GIFT_VOUCHER = "/pages/operation/sales/listGiftVoucherGeneration.xhtml?faces-redirect=true";
	private final String CREATE_GIFT_VOUCHER = "/pages/operation/sales/createGiftVoucherGeneration.xhtml?faces-redirect=true";
	private final String VIEW_GIFT_VOUCHER = "/pages/operation/sales/viewGiftVoucherGeneration.xhtml?faces-redirect=true";

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginbean;

	String url, jsonResponse;

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	GiftCoupon giftCoupon;

	@Getter
	@Setter
	GiftCouponCategory giftCouponCategory;

	@Getter
	@Setter
	GiftCouponType giftCouponType;

	@Getter
	@Setter
	OrganizationMaster organizationMaster;

	@Getter
	@Setter
	List<OrganizationMaster> organizationMasterList;

	@Getter
	@Setter
	List<GiftCouponType> giftCouponTypeList;

	@Getter
	@Setter
	List<GiftCouponCategory> giftCouponCategoryList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	List<OccasionType> occasionTypeList;

	@Getter
	@Setter
	OccasionType occasionType;

	@Getter
	@Setter
	private Long noOfCoupons;

	@Getter
	@Setter
	private Long serialNo;

	@Getter
	@Setter
	private Long serialNoFrom;

	@Getter
	@Setter
	private String emailId;

	@Getter
	@Setter
	List<String> emailIdList;
	@Getter
	@Setter
	LazyDataModel<GiftCoupon> lazyGiftCouponList;
	@Getter
	@Setter
	GiftCoupon selectedGiftCoupon = new GiftCoupon();

	@Getter
	@Setter
	List<GiftCoupon> GiftCouponList = new ArrayList<>();

	@Getter
	@Setter
	List<GiftCouponRegister> giftCouponRegisterList = new ArrayList<>();

	@Getter
	@Setter
	List<GiftCouponRegister> selectedGiftCouponList = new ArrayList<>();

	@Getter
	@Setter
	GiftCouponRegister giftCouponRegister = new GiftCouponRegister();

	@Getter
	@Setter
	Double totalGiftCouponAmount = 0.0;

	@Getter
	@Setter
	String existingCoupon;

	@Getter
	@Setter
	List<PaymentMode> paymentModeList = new ArrayList<>();

	@Getter
	@Setter
	PaymentMode paymentMode = new PaymentMode();

	public String showListPage() {
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		giftCoupon = new GiftCoupon();
		mapper = new ObjectMapper();

		selectedGiftCouponList = new ArrayList<>();
		paymentMode = new PaymentMode();
		totalGiftCouponAmount = 0.0;
		existingCoupon = null;

		giftCouponTypeList = commonDataService.getGiftCouponTypeList();
		paymentModeList = commonDataService.loadPaymentModes();
		loadLazyGiftCouponList();
		return LIST_GIFT_VOUCHER;
	}

	public void loadLazyGiftCouponList() {
		log.info("<===== Starts GiftCouponGenerationBean.loadLazyGiftCouponList ======>");
		lazyGiftCouponList = new LazyDataModel<GiftCoupon>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<GiftCoupon> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcoupon/getgiftCouponlist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						GiftCouponList = mapper.readValue(jsonResponse, new TypeReference<List<GiftCoupon>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGiftCouponList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return GiftCouponList;
			}

			@Override
			public Object getRowKey(GiftCoupon res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public GiftCoupon getRowData(String rowKey) {
				try {
					for (GiftCoupon giftCoupon : GiftCouponList) {
						if (giftCoupon.getId().equals(Long.valueOf(rowKey))) {
							selectedGiftCoupon = giftCoupon;
							return giftCoupon;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== ends GiftCouponGenerationBean.loadLazyGiftCouponList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts GiftCouponGenerationBean.onRowSelect ========>" + event);
		selectedGiftCoupon = ((GiftCoupon) event.getObject());
		log.info("selectedGiftCoupon id" + selectedGiftCoupon.getId());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		log.info("<===Ends GiftCouponGenerationBean.onRowSelect ========>");
	}

	public String createGiftCoupon() {
		setNoOfCoupons(null);
		setSerialNo(null);
		setEmailId("");
		setSerialNoFrom(null);
		// organizationMasterList = commonDataService.getOrganisationList();
		organizationMasterList = commonDataService.getOrganisationParticularDataList(SERVER_URL);
		giftCouponCategoryList = commonDataService.getGiftCouponCategoryList();
		occasionTypeList = commonDataService.getoccasionTypeList();
		emailIdList = new ArrayList<>();
		giftCouponType = new GiftCouponType();
		giftCouponCategory = new GiftCouponCategory();
		organizationMaster = new OrganizationMaster();
		occasionType = new OccasionType();
		return CREATE_GIFT_VOUCHER;
	}

	public void couponNoChange() {
		BaseDTO baseDTO = null;
		try {
			serialNo = null;
			serialNoFrom = null;
			giftCoupon.setEndSerialNumber(null);
			log.info("no of coupons-------------" + noOfCoupons);
			baseDTO = new BaseDTO();
			url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcoupon/getCouponSerialNo";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				serialNo = mapper.readValue(jsonResponse, new TypeReference<Long>() {
				});
				log.info("serial no---------------" + serialNo);
				if (serialNo != null) {
					serialNoFrom = serialNo + 1;
					giftCoupon.setEndSerialNumber(serialNo + noOfCoupons);
				}
			}
		} catch (Exception e) {
			log.error("inside couponNoChange method exception-----", e);
		}
	}

	public void addEmailIds() {
		if (Validate.validateEmail(emailId)) {
			emailIdList.add(emailId);
			setEmailId("");
		} else {
			errorMap.notify(ErrorDescription.INAVALID_EMAIL.getErrorCode());
		}
	}

	public void deleteEmailIds(int rowIndex) {
		log.info("index--------" + rowIndex);
		emailIdList.remove(rowIndex);
	}

	public void showEmailDialog() {
		setEmailId("");
		emailIdList = new ArrayList<>();
	}

	public void submitEmailDialog() {
		giftCoupon.setRecipientEmailAddress(String.join(",", emailIdList));
	}

	public String saveGiftCoupon() {
		log.info("saveGiftCoupon started");
		BaseDTO baseDTO = null;
		try {
			if (saveValidation(true)) {
				baseDTO = new BaseDTO();
				giftCoupon.setActiveStatus(true);
				giftCoupon.setGiftCouponCategory(giftCouponCategory);
				giftCoupon.setGiftCouponType(giftCouponType);
				giftCoupon.setOccasionType(occasionType);
				giftCoupon.setOrganizationMaster(organizationMaster);
				giftCoupon.setCouponValue(totalGiftCouponAmount);
				if (existingCoupon.equalsIgnoreCase("yes")) {
					giftCoupon.setIsCouponExists(true);
					giftCoupon.setGiftCouponRegisterList(selectedGiftCouponList);
					giftCoupon.setPaymentMode(paymentMode);
				} else {
					giftCoupon.setIsCouponExists(false);
					giftCoupon.setSerialNoFrom(serialNoFrom);
				}
				url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcoupon/create";
				baseDTO = httpService.post(url, giftCoupon);
				if (baseDTO.getStatusCode() == 0) {
					log.info("Gift Coupon Saved Successfuly");
					AppUtil.addInfo("Gift Coupon Saved Successfuly");
					// errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					return clear();
				}
			}
		} catch (Exception e) {
			log.error("inside saveGiftCoupon method exception----", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("saveGiftCoupon ended.");
		return null;
	}

	private boolean saveValidation(boolean valid) {
		valid = true;
		try {
			Validate.notNull(giftCouponCategory, ErrorDescription.LIBRARY_REGISTER_VISITER_TYPE);
		} catch (RestException e) {
			valid = false;
			errorMap.notify(e.getStatusCode());
		}
		return valid;
	}

	public String clear() {
		log.info("<---- Start GiftCouponGenerationBean.clear ----->");
		showListPage();
		setSelectedGiftCoupon(null);
		log.info("<---- End GiftCouponGenerationBean.clear ----->");

		return LIST_GIFT_VOUCHER;

	}

	public String deleteGiftCoupon() {
		log.info(" deleteGiftCoupon called");
		try {
			pageAction = "Delete";
			if (selectedGiftCoupon == null || selectedGiftCoupon.getId() == null) {
				AppUtil.addWarn("Please select GiftCoupon");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmGiftCouponDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteGiftCouponConfrom() {
		log.info("<===== Starts GiftCouponGenerationBean.deleteGiftCouponConfrom ===========>");
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcoupon/delete/"
					+ selectedGiftCoupon.getId();
			log.info("url===>" + url);
			BaseDTO response = httpService.delete(url);
			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("Gift Coupon Deleted Successfuly");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmGiftCouponDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete GiftCoupon ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends GiftCouponGenerationBean.deleteGiftCouponConfrom ===========>");
		return showListPage();
	}

	public String viewGiftCoupon() {
		pageAction = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedGiftCoupon == null || selectedGiftCoupon.getId() == null) {
			AppUtil.addWarn("Please select GiftCoupon");
			return null;
		}
		log.info("ID-->" + selectedGiftCoupon.getId());
		selectedGiftCoupon = getGiftCouponDetails(selectedGiftCoupon.getId());
		log.info("selectedInventoryConfig Object:::" + selectedGiftCoupon);
		return VIEW_GIFT_VOUCHER;
	}

	public GiftCoupon getGiftCouponDetails(Long id) {
		log.info("<====== GiftCouponGenerationBean.getGiftCouponDetails starts====>" + id);
		giftCoupon = new GiftCoupon();
		try {
			if (selectedGiftCoupon == null || selectedGiftCoupon.getId() == null) {
				log.error(" No GiftCoupon selected ");
				AppUtil.addWarn("Please select GiftCoupon");
			}
			log.info("Selected GiftCoupon  Id : : " + selectedGiftCoupon.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/giftcoupon/get/"
					+ selectedGiftCoupon.getId();
			log.info("GiftCoupon Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			log.info("view response" + baseDTO.getResponseContent());
			giftCoupon = mapper.readValue(jsonResponse, GiftCoupon.class);
			if (baseDTO.getStatusCode() == 0) {
				log.info(" GiftCoupon Retrived  SuccessFully ");
			} else {
				log.error(" GiftCoupon Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return giftCoupon;
		} catch (Exception e) {
			log.error("Exception Ocured while get giftCoupon Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadGiftCouponRegister() {
		log.info("<====== GiftCouponGenerationBean.loadGiftCouponRegister starts====>");
		try {
			log.info("loadGiftCoupanRigster :: existingCoupon==> " + existingCoupon);
			if (existingCoupon.equalsIgnoreCase("yes")) {
				Long userId = loginbean.getUserDetailSession().getId();
				log.info("loadGiftCouponRegister :: loggin userId ==> " + userId);
				EntityMaster entityMaster = commonDataService.getEntityInfoByLoggedInUser(SERVER_URL, userId);
				if (entityMaster == null) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
					return;
				}
				log.info("loadGiftCouponRegister :: entityMaster id ==> " + entityMaster.getId());
				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/giftcouponregister/loadcouponregisterbyentityid/" + entityMaster.getId();
				log.info("loadGiftCouponRegister :: url==> " + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					giftCouponRegisterList = mapper.readValue(jsonResponse,
							new TypeReference<List<GiftCouponRegister>>() {
							});
					if (giftCouponRegisterList != null && !giftCouponRegisterList.isEmpty()) {
						log.info("giftCouponRegisterList size==> " + giftCouponRegisterList.size());
					} else {
						log.error("Gift Coupon Register Not Found");
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception Ocured while get loadGiftCoupanRigster==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadSelectCoupon() {
		log.info("<====== GiftCouponGenerationBean.loadSelectCoupon starts====>");
		try {
			log.info("loadSelectCoupon :: selectedGiftCouponList size==> " + selectedGiftCouponList.size());
			totalGiftCouponAmount = 0.0;
			for (GiftCouponRegister giftCouponRegister : selectedGiftCouponList) {
				Double giftCouponRegisterValue = giftCouponRegister.getCouponValue();
				log.info("loadSelectCoupon :: giftCouponRegisterValue==> " + giftCouponRegisterValue);
				totalGiftCouponAmount = totalGiftCouponAmount + giftCouponRegisterValue;
				log.info("loadSelectCoupon :: totalGiftCouponAmount==> " + totalGiftCouponAmount);
			}
		} catch (Exception e) {
			log.error("Exception Ocured while get loadSelectCoupon==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public List<EntityMaster> loadShowroomList(String searchParam) {
		log.info("<====== GiftCouponGenerationBean.loadShowroomList starts====>" + searchParam);
		List<EntityMaster> showroomList = new ArrayList<>();
		try {
			String url = SERVER_URL + "/entitymaster/getallactiveshowrooms/" + searchParam;
			BaseDTO response = httpService.get(url);
			if (response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				showroomList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Ocured while get loadShowroomList==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return showroomList;
	}

	public List<OrganizationMaster> organizationCodeOrNameAutoComplete(String organizationCodeOrName) {
		log.info("====START organizationAutoComplete Method============");
		try {
			if (organizationCodeOrName != null) {
				log.info("name-------------->" + organizationCodeOrName);

				String url = SERVER_URL + appPreference.getOperationApiUrl()
						+ "/giftcouponregister/loadOrgCodeAutoComplete/" + organizationCodeOrName;
				log.info("loadGiftCouponRegister :: url==> " + url);

				BaseDTO response = httpService.get(url);
				if (response.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					organizationMasterList = mapper.readValue(jsonResponse,
							new TypeReference<List<OrganizationMaster>>() {
							});
				}

				if (organizationMasterList == null || organizationMasterList.isEmpty()) {
					AppUtil.addWarn("Please give valid organization code/name ");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
		} catch (Exception e) {
			log.error("==== exception----", e);
		}
		log.info("====END organizationAutoComplete Method============");
		return organizationMasterList;

	}
}
