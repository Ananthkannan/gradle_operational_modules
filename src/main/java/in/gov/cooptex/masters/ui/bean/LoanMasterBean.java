package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.LoanMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("loanMasterBean")
@Scope("session")
public class LoanMasterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String LOAN_LIST_PAGE = "/pages/masters/Loan/listLoan.xhtml?faces-redirect=true;";
	private final String LOAN_ADD_PAGE = "/pages/masters/Loan/createLoanMaster.xhtml?faces-redirect=true;";
	private final String LOAN_VIEW_PAGE = "/pages/masters/Loan/viewLoan.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;
	
	@Getter @Setter
	LoanMaster selectedLoan = new LoanMaster();

	@Getter @Setter
	LoanMaster loan = new LoanMaster();

	@Getter @Setter
	private RestTemplate restTemplate;

	@Getter @Setter
	private String Url = null;

	@Getter @Setter
	LazyDataModel<LoanMaster> loanlazyLoadList;

	@Getter @Setter
	private Map<String, Object> filterMap;

	@Getter @Setter
	private SortOrder sortingOrder;

	@Getter @Setter
	private String sortingField;

	@Getter @Setter
	private Integer resultSize;

	@Getter @Setter 
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<LoanMaster> loanList;
	
	@Autowired
	AppPreference appPreference;
	
	@Getter @Setter
	List<GlAccount> glAccountList;
	
	@Getter @Setter
	String glAccountCode,glAccountName;
	
	@Autowired
	CommonDataService commonDataService;
	
	public String showListLoanPage() {
		log.info("showListQualificationPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedLoan = new LoanMaster();
		loan = new LoanMaster();
		loadLoanListLazy();
		return LOAN_LIST_PAGE;
	}
	
	public void loadLoanListLazy() {

		log.info("<-----Loan list lazy load starts------>");
		loanlazyLoadList = new LazyDataModel<LoanMaster>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<LoanMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters){
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					loanList = mapper.readValue(jsonResponse,
							new TypeReference<List<LoanMaster>>() {
					});
					if (loanList == null) {
						log.info(" Loan is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Loan list search Successfully Completed");
					} else {
						log.error(" Loan list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					log.info("<-----Loan lists------>"+loanList.size());
					return loanList;

				}catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Loan list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}



			@Override
			public Object getRowKey(LoanMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public LoanMaster getRowData(String rowKey) {
				List<LoanMaster> responseList = (List<LoanMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (LoanMaster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedLoan = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			LoanMaster request = new LoanMaster();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);
				if (filter.getKey().equals("code")) {
					log.info("code : " + value);
					searchFilters.put("code", value);
				}
				if (filter.getKey().equals("name")) {
					log.info("name : " + value);
					searchFilters.put("name", value);
				}
				if (filter.getKey().equals("loanAmount")) {
					log.info("loanAmount : " + value);
					searchFilters.put("loanAmount",value);
				}
				if (filter.getKey().equals("loanStartDate")) {
					log.info("loanStartDate : " + value);
					searchFilters.put("loanStartDate",value);
				}	
				
				if (filter.getKey().equals("loanEndDate")) {
					log.info("loanEndDate : " + value);
					searchFilters.put("loanEndDate", value);
				}
				if (filter.getKey().equals("installmentAmount")) {
					log.info("installmentAmount : " + value);
					searchFilters.put("installmentAmount",value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("createdDate : " + value);
					searchFilters.put("createdDate",value);
				}	
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status",value);
				}	

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/loan/loadlazylist", request);
			log.info("Search request response " + dataList);

		}catch(Exception e) {
			log.error("Exception Occured in Loan load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}
	
	public String loanListAction() {
		log.info("<====== Loan.loanListAction Starts====> action   :::"+action);
		try {
			if(action.equalsIgnoreCase("ADD")) {
				log.info("<====== Loan add page called.......====>");
				selectedLoan = new LoanMaster();
				glAccountName = "";
				loan = new LoanMaster();
				return LOAN_ADD_PAGE;
			}
			if(selectedLoan==null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			
			if(action.equalsIgnoreCase("EDIT")) {
				log.info("<====== EducationQualificationBean add page called.......====>");
				if(selectedLoan==null ||  selectedLoan.getId() == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				loan = getLoanDetails(selectedLoan.getId());
				
				log.info("seleted Loan::::::::"+loan);
				return LOAN_ADD_PAGE;
			}
			if(action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmLoanDelete').show();");
			} if(action.equalsIgnoreCase("VIEW")) {
				if(selectedLoan == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->"+selectedLoan.getId());
				
				selectedLoan = getLoanDetails(selectedLoan.getId());
				
				log.info("selected Qualification Object:::"+selectedLoan);
				return LOAN_VIEW_PAGE;
			}
			

		} catch (Exception e) {
			log.error("Exception Occured in LaonMasterBean.loanListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== LoanMasterBean.loanListAction Ends====>");
		return null;
	}
	
	private LoanMaster getLoanDetails(Long loanId) {
		log.info("<====== Loan.getLoanDetails starts====>"+loanId);
		try {
			if (selectedLoan == null || selectedLoan.getId() == null) {
				log.error(" No Loan selected ");
				errorMap.notify(ErrorDescription.LOAN_ID_SHOULD_NOTBE_EMPTY.getErrorCode());
			}
			log.info("Selected Loan  Id : : " + selectedLoan.getId());
			String getUrl = SERVER_URL + "/loan/view/id/:id";
			String url = getUrl.replace(":id", selectedLoan.getId().toString());

			log.info("Loan Viw By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			LoanMaster loanDetail = mapper.readValue(jsonResponse, LoanMaster.class);
			if (loanDetail != null) {
				loan = loanDetail;
				setGlAccountName(loanDetail.getGlAccountCode());
			} else {
				log.error("Loan object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Loan Retrived  SuccessFully ");
			} else {
				log.error(" Loan Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return loan;
		}catch(Exception e) {
			log.error("ExceptionException Ocured while get Loan Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	public String submit() {
		log.info("<=======Starts  LoanMasterBean.submit ======>");
		
		if(loan.getName() == null || loan.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.LOAN_NAME_REQUIRED.getErrorCode());
			return null;
		}
		
		/*if(loan.getLName() == null || loan.getLName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.LOAN_lNAME_REQUIRED.getErrorCode());
			return null;
		}*/
		
		if(loan.getLoanAmount() == null && loan.getLoanAmount()<0) {
			errorMap.notify(ErrorDescription.LOAN_AMOUNT.getErrorCode());
			return null;
		}
		/*if(loan.getLoanStartDate()== null) {
			errorMap.notify(ErrorDescription.LOAN_START_DATE_MANDATORY.getErrorCode());
			return null;
		}
		
		if(loan.getLoanEndDate()==null) {
			errorMap.notify(ErrorDescription.LOAN_END_MANDATORY.getErrorCode());
			return null;
		}*/
		if(loan.getInstallmentAmount()==null && loan.getInstallmentAmount()<0) {
			errorMap.notify(ErrorDescription.LOAN_INSTALLMENT_AMOUNT_MANDATORY.getErrorCode());
			return null;
		}
		/*if(loan.getLoanPremium()==null && loan.getLoanPremium()<0) {
			errorMap.notify(ErrorDescription.LOAN_PREMIMIUM_AMOUNT_MANDATORY.getErrorCode());
			return null;
		}*/
		
		if(loan.getStatus() == null) {
			errorMap.notify(ErrorDescription.LOAN_STATUS.getErrorCode());
			return null;
		}
		log.info("glAccountName"+glAccountName);
		/*if(StringUtils.isNotEmpty(glAccountName)) {
			errorMap.notify(ErrorDescription.GL_ACCOUNT_HEAD_NAME_EMPTY.getErrorCode());
			return null;
		}
		*/
		if(action.equalsIgnoreCase("ADD")) {
			loan.setCreatedBy(loginBean.getUserDetailSession());
		}else if (action.equalsIgnoreCase("EDIT")) {
			loan.setModifiedBy(loginBean.getUserDetailSession());
		}
		log.info("GlAccount Id ===> "+glAccountName);
		loan.setGlAccountCode(glAccountName.split("/")[0]);
		log.info("GlAccount Code ===> "+loan.getGlAccountCode());
		BaseDTO response= httpService.post(SERVER_URL + "/loan/save",loan);
//		errorMap.notify(response.getStatusCode());
		if(response.getStatusCode()==0) {
			if(action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.LOAN_SAVE_SUCCESS.getErrorCode());
			else 
				errorMap.notify(ErrorDescription.LOAN_UPDATE_SUCCESS.getErrorCode());
			return showListLoanPage();
		}else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends LoanMasterBean.submit ======>");
		return null;
	}
	
	public String deleteConfirm() {
		log.info("<--- Starts LaonMasterBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/loan/delete/" + selectedLoan.getId());
			if(response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.LOAN_DELETED_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting Loan....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends LoanMasterBean.deleteConfirm ---->");
		return showListLoanPage();
	}
	
	private String gotoViewPage() {

		if (selectedLoan == null || selectedLoan.getId() == null) {
			errorMap.notify(ErrorDescription.LOAN_ID_SHOULD_NOTBE_EMPTY.getCode());
			return null;
		}

		loan = getLoanMasterById(selectedLoan.getId());

		return LOAN_VIEW_PAGE;
	}

	public LoanMaster getLoanMasterById(Long id) {
		String URL = SERVER_URL + "/loan/view/id/" + id;
		LoanMaster loanMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				loanMaster = mapper.readValue(jsonValue, LoanMaster.class);
				if(loanMaster != null) {
					setGlAccountName(loanMaster.getGlAccountCode());
				}
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return loanMaster;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Loan.onRowSelect ========>" + event);
		selectedLoan = ((LoanMaster) event.getObject());
		addButtonFlag = false;
		log.info("<===Ends Loan.onRowSelect ========>" );
	}
	
	public void loanStartDateChange() {
		log.info(":::::::::::Inside::::::loanStartDateChange:::::::::"+loan.getLoanStartDate());
		try {
			if (loan.getLoanEndDate() != null) {
				if (loan.getLoanStartDate().compareTo(loan.getLoanEndDate())<0) {
				log.info("inside condition Start Date is Less then End Date");
				}
				else {
				log.info("inside condition Start Date is Greater then End Date");
					loan.setLoanEndDate(null);
				}
			}
		} catch (Exception e) {
			log.error("Exception occured in  loanStartDateChange...", e);
		}
	}

	public List<String> glaccountAutoComplete(String query) {
		log.info("glaccountAutoComplete query==>" + query);
		List<String> results = new ArrayList<String>();
		glAccountList = new ArrayList<>();
		glAccountList = commonDataService.loadGlAccountsAutoCompleteSearch(query);
		if (glAccountList != null || !glAccountList.isEmpty()) {
			for (GlAccount account : glAccountList) {
				String acc = account.getCode()+"/"+account.getName();
				results.add(acc);
			}
		}
		return results;
	}
}
