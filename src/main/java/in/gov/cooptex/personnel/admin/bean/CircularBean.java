package in.gov.cooptex.personnel.admin.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.meeting.model.MeetingType;
import in.gov.cooptex.admin.model.CircularRegister;
import in.gov.cooptex.admin.model.CircularRegisterLog;
import in.gov.cooptex.admin.model.CircularRegisterNote;
import in.gov.cooptex.asset.model.dto.AssetRequestApproveDTO;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DashboardDTO;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.VenueMaster;
import in.gov.cooptex.core.ui.CircularForType;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.dashboard.ui.bean.DashboardBean;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import in.gov.cooptex.personnel.hrms.dto.CircularListDTO;
import in.gov.cooptex.personnel.hrms.dto.CircularRequestViewDTO;
import in.gov.cooptex.personnel.hrms.dto.CreateCircularDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("circularBean")
@Scope("session")
public class CircularBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4050146343705250034L;

	final String LIST_PAGE = "/pages/admin/circular/listCircular.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/admin/circular/createCircular.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/admin/circular/viewCircular.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	Date date;

	@Getter
	@Setter
	Long initiatedby;

	@Getter
	@Setter
	Boolean status = false;

	@Getter
	@Setter
	Boolean remaindstatus;

	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	Long forwardTo;

	@Getter
	@Setter
	String circularFor;

	@Getter
	@Setter
	CircularListDTO selectedcircular;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	private Long circularId;

	@Getter
	@Setter
	private Long notificationId;

	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasters = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	LazyDataModel<CircularListDTO> lazyCircularList;

	@Getter
	@Setter
	List<CircularListDTO> circularList = null;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<VenueMaster> venueMasterList = new ArrayList<VenueMaster>();

	@Getter
	@Setter
	List<Designation> designationList = new ArrayList<Designation>();

	int venueMasterListsize;

	@Getter
	@Setter
	List<MeetingType> meetingMasterList = new ArrayList<MeetingType>();

	int meetingMasterListsize;

	@Getter
	@Setter
	List<EntityMaster> horoList = new ArrayList<>();

	@Setter
	@Getter
	private List<EntityMaster> selectedHoRoList;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<>();

	@Getter
	@Setter
	List<Department> meetingdepartmentlist = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster headangregionaofficeid = new EntityMaster();

	@Getter
	@Setter
	boolean visibility = true;

	@Getter
	@Setter
	List<SectionMaster> meetingsectionlist = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> meetingentitylist = new ArrayList<>();

	@Getter
	@Setter
	Long department;

	@Getter
	@Setter
	Long entityid;

	@Getter
	@Setter
	Long sectionMaster;

	@Getter
	@Setter
	EntityTypeMaster selctedentitytypeid;

	@Getter
	@Setter
	CreateCircularDTO createCircularDTO = new CreateCircularDTO();

	@Getter
	@Setter
	List<CircularRegisterLog> createresponse = null;

	@Getter
	@Setter
	CreateCircularDTO selectedmeeting;

	@Getter
	@Setter
	CircularRequestViewDTO viewcirculardetails;

	@Getter
	@Setter
	boolean isVisible = false;

	@Getter
	@Setter
	String apprremarks;
	@Getter
	@Setter
	String rejtremark;

	@Getter
	@Setter
	CircularRegisterLog circularstatus;
	@Getter
	@Setter
	AssetRequestApproveDTO circularApproveRequestDTO = new AssetRequestApproveDTO();

	@Setter
	@Getter
	String fileType = "", filepath;

	@Getter
	@Setter
	String circularDocumentFileName;

	@Getter
	@Setter
	String selectedstatus;

	@Getter
	@Setter
	CircularRegisterNote lastNote = new CircularRegisterNote();

	@Getter
	@Setter
	List<CircularRegisterNote> lastNoteList = new ArrayList();

	@Getter
	@Setter
	CircularRegisterLog lastLog = new CircularRegisterLog();

	@Autowired
	LoginBean loginBean;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	private List<EmployeeMaster> employeeList = new ArrayList<>();

	@Getter
	@Setter
	private List<EmployeeMaster> selectedEmployees = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	private List<Designation> selectedDesignations = new ArrayList<>();

	@Getter
	@Setter
	private String employeeNames;

	@Getter
	@Setter
	private String designationNames;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	DashboardBean dashBoardBean;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	EmployeeMaster createdByEmployee;

	@Getter
	@Setter
	String commentRemarks = "";

	@Getter
	@Setter
	String approveRemarks = "";

	@Getter
	@Setter
	String finalApprovalRemarks = "";
	@Getter
	@Setter
	String rejectRemarks = "";

	public String showCircularListPage() {
		headangregionaofficeid = new EntityMaster();
		userMasters = new ArrayList<UserMaster>();
		employeeMasters = new ArrayList<EmployeeMaster>();
		addButtonFlag = false;
		viewButtonFlag = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.CIRCULAR.toString());
		selectedcircular = new CircularListDTO();
		log.info("forward to user info" + userMasters);
		loadVenue();
		lazyLoadCircularlist();
		lastNote = new CircularRegisterNote();
		lastLog = new CircularRegisterLog();
		lastNoteList = new ArrayList();
		employeeList = new ArrayList<>();
		selectedEmployees = new ArrayList<>();
		employeeNames = "";
		designationNames = "";
		return LIST_PAGE;

	}

	public String addCircular() {
		log.info("CircularBean. addCircular() Starts ");
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create circular list called..");
				createCircularDTO = new CreateCircularDTO();
				headangregionaofficeid = null;
				selctedentitytypeid = null;
				userMaster = new UserMaster();
				loadHeadAndRegionalOfficeList();
				loadmeetingdepartmentlist();
				loadmeetingtypelist();
				loadInitiatedEmployeeList();
				employeeList = new ArrayList<>();
				selectedEmployees = new ArrayList<>();
				employeeNames = "";
				designationNames ="";
				setStatus(null);
				setFileType(null);
				String getEmployeeListUrl = SERVER_URL + "/employee/getcircularselectedemplist";
				log.info("==== getEmployeeListUrl is  ==== " + getEmployeeListUrl);
				BaseDTO baseDto = httpService.post(getEmployeeListUrl, createCircularDTO);
				if (baseDto != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
					employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
				}

				// Create Note Employee Details
				loadCreateNoteEmpDetails();
				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.info("Exception at CircularBean. addCircular()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("CircularBean. addCircular() Ends ");
		return null;
	}

	public void loadCreateNoteEmpDetails() {
		log.info("CircularBean. loadCreateNoteEmpDetails() Starts ");
		try {
			Long loginUserId = loginBean.getUserDetailSession() == null ? null
					: loginBean.getUserDetailSession().getId();
			createdByEmployee = new EmployeeMaster();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/employee/findempdetailsbyloggedinuserdetails/" + loginUserId;
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				createdByEmployee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception at CircularBean. loadCreateNoteEmpDetails()", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		log.info("CircularBean. loadCreateNoteEmpDetails() Ends ");
	}

	/*
	 * public void loadEmployee() {
	 * log.info("<<<< ----------Starts lazyLoadCircularList------- >>>>"); Long
	 * entityTypeId=null; Long selectentityId=null; Long depId=null; Long
	 * secId=null; employeeMasters=new ArrayList<>(); try{
	 * if(selctedentitytypeid!=null && selctedentitytypeid.getId()!=null)
	 * entityTypeId=selctedentitytypeid.getId(); if(createCircularDTO!=null) {
	 * if(createCircularDTO.getEntityId()!=null)
	 * selectentityId=createCircularDTO.getEntityId();
	 * if(createCircularDTO.getDepartmentId()!=null)
	 * depId=createCircularDTO.getDepartmentId();
	 * if(createCircularDTO.getSectionId()!=null)
	 * secId=createCircularDTO.getSectionId();
	 * 
	 * }
	 * 
	 * employeeMasters = commonDataService.getActiveEmployeeByEntityDeptSectionId(
	 * headangregionaofficeid.getId(),entityTypeId,selectentityId,depId,secId);
	 * 
	 * log.info("<<== employeeMasters size....."+employeeMasters.size());
	 * }catch(Exception e) { log.error("<<=== Exception while loadEmployee...",e); }
	 * }
	 */

	public void lazyLoadCircularlist() {
		log.info("<<<< ----------Starts lazyLoadCircularList------- >>>>");

		lazyCircularList = new LazyDataModel<CircularListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<CircularListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/circular/getcircularlist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						circularList = mapper.readValue(jsonResponse, new TypeReference<List<CircularListDTO>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						circularList = new ArrayList<>();
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in circularList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return circularList;
			}

			@Override
			public Object getRowKey(CircularListDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CircularListDTO getRowData(String rowKey) {
				try {
					for (CircularListDTO circular : circularList) {
						if (circular.getId().equals(Long.valueOf(rowKey))) {
							selectedcircular = circular;
							return circular;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<<<< ----------Ends lazyLoadCircularList------- >>>>");

	}

	public String createddate(Date d)

	{
		log.info("<<<< ----------Starts CircularDate format changing------ >>>>");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		log.info("<<<< ----------Ends CircularDate format changing------ >>>>");

		return simpleDateFormat.format(d);

	}

	public void loadVenue() {
		log.info("<<<< ----------Start loadVenue ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			venueMasterList = new ArrayList<VenueMaster>();

			String url = SERVER_URL + "/meeting/request/loadvenue";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				venueMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VenueMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (venueMasterList != null) {
			venueMasterListsize = venueMasterList.size();
		} else {
			venueMasterListsize = 0;
		}
		log.info("<<<< ----------loadVenue-venueMasterListsize ------- >>>> " + venueMasterListsize);
	}

	public void loadmeetingdepartmentlist() {
		log.info("<<<< ----------Starts loadMeretingDepartmentList ------- >>>>");
		meetingdepartmentlist = commonDataService.getDepartment();
		log.info("meeting department list", meetingdepartmentlist);
		log.info("<<<< ----------End loadMeretingDepartmentList ------- >>>>");
	}

	public void onSelectdepartment() {
		log.info("<<<< ----------Starts loadMeretinSectionList ------- >>>>" + createCircularDTO.getDepartmentId());
		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames ="";
			if (createCircularDTO != null && createCircularDTO.getDepartmentId() != null) {
				meetingsectionlist = commonDataService
						.getActiveSectionByDepartmentId(createCircularDTO.getDepartmentId());
				loadSelectedEmployeeList();
			} else {
				meetingsectionlist = new ArrayList<>();
			}
			// loadEmployee();
		} catch (Exception e) {
			log.error("<<=== Exception while load the department...", e);
		}
		log.info("Section list.........", meetingsectionlist);
		log.info("<<<< ----------End loadMeretinSectionList ------- >>>>");
	}

	public void loadmeetingtypelist() {

		log.info("<<<< ----------Start laoMeetingType ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			meetingMasterList = new ArrayList<MeetingType>();

			String url = SERVER_URL + "/meeting/request/loadmeetingtype";

			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				meetingMasterList = mapper.readValue(jsonResponse, new TypeReference<List<MeetingType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (meetingMasterList != null) {
			meetingMasterListsize = meetingMasterList.size();
		} else {
			meetingMasterListsize = 0;
		}
		log.info("<<<< ----------loadMeetingType  ------- >>>> " + meetingMasterListsize);
	}

	public void createParams() {

		if (createCircularDTO.getNote() == null || createCircularDTO.getNote().trim().length() == 0) {
			errorMap.notify(ErrorDescription.PRODUCT_DESIGN_CREATE_NOTE_EMPTY.getErrorCode());
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('notedialog').hide();");
		}

	}

	public String createCircular() {

		log.info("<================createCircularRequest==================>" + createCircularDTO);

		Log.info("<===========filepathhhh===========>" + circularDocumentFileName);
		if (headangregionaofficeid != null) {
			if (createCircularDTO.getEntityId() == null) {
				createCircularDTO.setEntityId(headangregionaofficeid.getId());
			}
		}

		if (createCircularDTO.getId() == null) {
			try {
				BaseDTO baseDTO = new BaseDTO();
				log.info("statusssd" + status);
				createCircularDTO.setMeetingRequired(status);
				createCircularDTO.setFilePath(circularDocumentFileName);
				if (fileType == null) {
					Util.addWarn("Upload Document Should Not be Empty");
					return null;
				}
				int selectedEmployeesSize = selectedEmployees == null ? 0 : selectedEmployees.size();
				log.info("selectedEmployeesSize :" + selectedEmployeesSize);
				if (selectedEmployeesSize > 0) {
					selectedEmployees.forEach(emp -> {
						createCircularDTO.getSelectedEmployeeList().add(emp == null ? null : emp.getId());
					});
				}
				if (CollectionUtils.isNotEmpty(selectedDesignations)) {
					selectedDesignations.forEach(desgn -> {
						createCircularDTO.getSelectedDesignationList().add(desgn == null ? null : desgn.getId());
					});
				}

				if (status.equals(true)) {
					if (createCircularDTO.getEndTime().before(createCircularDTO.getStartTime())) {
						Util.addWarn("End Time should be greater than the Start time");
						RequestContext.getCurrentInstance().update("growls");
						return null;
					}
				}
				createCircularDTO.setForwardTo(userMaster.getId());
				String url = SERVER_URL + "/circular/create";

				baseDTO = httpService.post(url, createCircularDTO);

				if (baseDTO != null && baseDTO.getStatusCode() == 0) {

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					createresponse = mapper.readValue(jsonResponse, new TypeReference<List<CircularRegisterLog>>() {
					});
					Util.addInfo("Circular Has been Saved Successfully");
					errorMap.notify(ErrorDescription.CIRCULAR_SAVE_SUCCESS.getCode());
				}
				Util.addInfo("Circular Has been Saved Successfully");
				log.info("create response", createresponse);
			} catch (Exception e) {
				Util.addInfo("Circular Has been Saved Successfully");
				log.error("found exception in onchangeEntityType: ", e);
			}

		} else {
			try {
				BaseDTO baseDTO = new BaseDTO();
				createCircularDTO.setMeetingRequired(status);
				if (createCircularDTO.getMeetingRequired() == true) {
					if (createCircularDTO.getEndTime().before(createCircularDTO.getStartTime())) {
						Util.addWarn("End Time should not be allowed for before Start Time");
						RequestContext.getCurrentInstance().update("growls");
						return null;
					}
				}
				String url = SERVER_URL + "/circular/update";
				baseDTO = httpService.post(url, createCircularDTO);

				if (baseDTO != null) {

					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					createresponse = mapper.readValue(jsonResponse, new TypeReference<List<CircularRegisterLog>>() {

					});
					errorMap.notify(ErrorDescription.CIRCULAR_UPDATE_SUCCESS.getCode());
					selectedcircular = new CircularListDTO();
					createCircularDTO = new CreateCircularDTO();
				}
				log.info("create response", createresponse);
			} catch (Exception e) {
				log.error("found exception in onchangeEntityType: ", e);
			}

		}
		selectedcircular = new CircularListDTO();
		createCircularDTO = new CreateCircularDTO();
		headangregionaofficeid = new EntityMaster();
		circularDocumentFileName = "";
		fileType = "";

		return LIST_PAGE;

	}

	public void onSelectmeetingType() {

		log.info("<<<< ----------Starts loadMeretinEntityList ------- >>>>");
		log.info("entity master", selctedentitytypeid);

		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames = "";
			if (headangregionaofficeid != null && headangregionaofficeid.getId() != null && selctedentitytypeid != null
					&& selctedentitytypeid.getId() != null) {
				Long regionId = headangregionaofficeid.getId();
				// meetingentitylist =
				// commonDataService.loadEntityByregionOrentityTypeId(regionId,
				// selctedentitytypeid.getId());
				meetingentitylist = commonDataService.loadActiveEntityByregionOrentityTypeId(regionId,
						selctedentitytypeid.getId());
				loadSelectedEmployeeList();
			} else {
				log.info("entity not found.");
			}
			// loadEmployee();
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
		log.info("<<<< ----------End loadMeretinEntityList ------- >>>>");

	}

	public void onRowSelect(SelectEvent event) {

		log.info("<<<< ----------Starts onRowSelect------ >>>>");
		selectedcircular = ((CircularListDTO) event.getObject());

		addButtonFlag = true;
		viewButtonFlag = false;
		EmployeeMaster employeeMaster = loginBean.getEmployee();

		if (selectedcircular != null
				&& selectedcircular.getStatus().equals(CreditSalesDemandStatus.REJECTED.toString())) {
			if (employeeMaster != null && employeeMaster.getUserId() != null && selectedcircular.getCreatedBy() != null
					&& selectedcircular.getCreatedBy().longValue() == (employeeMaster.getUserId().longValue())) {
				deleteButtonFlag = false;
				editButtonFlag = false;
			} else {
				deleteButtonFlag = true;
				editButtonFlag = true;
			}

		} else {
			deleteButtonFlag = true;
			editButtonFlag = true;
		}
		log.info("<<<< ----------Ends onRowSelect------ >>>>");

	}

	public String deletecircular() {
		log.info("<<<< ----------Starts deletecircular----- >>>>");
		long circularid = selectedcircular.getId();
		log.info("circular id", circularid);
		try {
			String url = SERVER_URL + "/circular/delete/" + circularid;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				// ObjectMapper mapper = new ObjectMapper();
				// jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				errorMap.notify(ErrorDescription.CIRCULAR_DELETE_SUCCESS.getCode());

			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends deletecircular----- >>>>");
		return LIST_PAGE;

	}

	public String viewDetails() {
		log.info("<<<< ----------Starts viewDetails------ >>>>");
		isVisible = false;
		log.info("selected view id", selectedcircular);
		long circularid = selectedcircular.getId();
		log.info("circular id" + circularid);
		try {

			viewcirculardetails = new CircularRequestViewDTO();
			if (action.equalsIgnoreCase("VIEW")) {

				log.info("view meeting list called..");

				viewcirculardetails.setId(circularid);
				viewcirculardetails.setNotificationId(selectedNotificationId);
				viewcirculardetails.setAction(action);
				String url = SERVER_URL + "/circular/getbyid";
				BaseDTO baseDTO = httpService.post(url, viewcirculardetails);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					viewcirculardetails = mapper.readValue(jsonResponse, new TypeReference<CircularRequestViewDTO>() {
					});

					if (viewcirculardetails != null
							&& !CollectionUtils.isEmpty(viewcirculardetails.getCircularRegisterNoteList())) {
						lastNote = viewcirculardetails.getCircularRegisterNoteList().get(0);
						lastNoteList = viewcirculardetails.getCircularRegisterNoteList();
					}

					if (viewcirculardetails != null
							&& !CollectionUtils.isEmpty(viewcirculardetails.getCircularRegisterNoteList())) {
						lastLog = viewcirculardetails.getCircularRegisterLogList().get(0);
					}
					isVisible = getVisibleStatus();
					log.info("Last note:::::::::::::::" + lastNote);
					log.info("Last Log:::::::::::::::" + lastLog);
					log.info("visibleStatus::::::::::: " + isVisible);
					if (viewcirculardetails != null && viewcirculardetails.getFilePath() != null) {
						filepath = viewcirculardetails.getFilePath();
					}

					int employeeListSize = viewcirculardetails.getSelectedEmployeeNameList() == null ? 0
							: viewcirculardetails.getSelectedEmployeeNameList().size();
					if (employeeListSize > 0) {

						employeeNames = "";
						for (String employee : viewcirculardetails.getSelectedEmployeeNameList()) {
							if (employeeNames != "") {
								employeeNames = employeeNames + ",<br/>";
							}
							employeeNames = employeeNames + employee;
						}
						viewcirculardetails.setSelectedEmployeeNames(employeeNames);
					}

					int designationListSize = viewcirculardetails.getSelectedDesignationNameList() == null ? 0
							: viewcirculardetails.getSelectedDesignationNameList().size();
					if (designationListSize > 0) {

						designationNames = "";
						for (String desg : viewcirculardetails.getSelectedDesignationNameList()) {
							if (designationNames != "") {
								designationNames = designationNames + ",<br/>";
							}
							designationNames = designationNames + desg;
						}
						viewcirculardetails.setSelectedDesignations(designationNames);
					}
					
					systemNotificationBean.loadTotalMessages();
					if (viewcirculardetails.getViewCountStaus() != null && viewcirculardetails.getViewCountStaus()) {
						Integer circularViewed = dashBoardBean.getHeaderDTO().getCircularViewed();
						Integer circularPending = dashBoardBean.getHeaderDTO().getCircularPending();
						DashboardDTO headerDTO = dashBoardBean.getHeaderDTO();
						headerDTO.setCircularPending(circularPending - 1);
						headerDTO.setCircularViewed(circularViewed + 1);
						dashBoardBean.setHeaderDTO(headerDTO);
					}
				} else {

					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}

				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception at viewDetails()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends viewDetails------ >>>>");

		return null;
	}

	/**
	 * @param status
	 * @return
	 */
	public String submitApprovedStatus(String status) {

		log.info("<<<< ----------Starts submitApprovedStatuss------ >>>>" + status);

		log.info("Create Note" + approveRemarks);
		log.info("Create Note" + finalApprovalRemarks);
		if (approveRemarks.trim().length() == 0 && finalApprovalRemarks.trim().length() == 0) {

			Util.addWarn("Remarks Should not Be Empty");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgComments2').hide();");
			return null;
		}
		if (!status.equalsIgnoreCase("FINAL_APPROVED")) {
			if (userMaster == null || userMaster.getId() == null) {
				Util.addWarn("Forward To Cannot be Empty");
				return null;
			}
		}

		circularApproveRequestDTO.setId(viewcirculardetails.getId());

		if (status.equalsIgnoreCase("FINAL_APPROVED")) {
			circularApproveRequestDTO.setStatus(CreditSalesDemandStatus.FINAL_APPROVED.toString());
			circularApproveRequestDTO.setRemarks(finalApprovalRemarks);
		} else {
			if (userMaster == null || userMaster.getId() == null) {
				Util.addWarn("Forward To Cannot be Empty");
				return null;
			}
			if (viewcirculardetails.getForwardFor() == null) {
				Util.addWarn("Forward For Cannot be Empty");
				return null;
			}
			circularApproveRequestDTO.setStatus(CreditSalesDemandStatus.APPROVED.toString());
			circularApproveRequestDTO.setRemarks(approveRemarks);
		}

		circularApproveRequestDTO.setForwardFor(viewcirculardetails.getForwardFor());
		circularApproveRequestDTO.setForwardTo(userMaster.getId());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgComments2').hide();");

		log.info("submited info", circularApproveRequestDTO);

		try {

			circularstatus = new CircularRegisterLog();
			log.info("view meeting list called..");
			/**
			 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/personnel/admin/um/controller
			 * 
			 * CircularController.java
			 */
			String url = SERVER_URL + "/circular/approvecircularregister";
			BaseDTO baseDTO = httpService.post(url, circularApproveRequestDTO);
			//
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				circularstatus = mapper.readValue(jsonResponse, new TypeReference<CircularRegisterLog>() {
				});
				errorMap.notify(ErrorDescription.CIRCULAR_APPROVE_SUCCESS.getCode());
				return showCircularListPage();
			}
		} catch (Exception ex) {
			log.error("Exception at submitApprovedStatus()", ex);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<<<< ----------Ends submitApprovedStatus------ >>>>");
		selectedcircular = new CircularListDTO();
		return null;

	}

	public String submitRejectedStatus() {
		log.info("<<<< ----------Starts submitApprovedStatus------ >>>>");

		if (rejectRemarks.trim().length() == 0) {

			Util.addWarn("Remarks Should not Be Empty");
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('dlgComments2').hide();");
			return null;
		}
		log.info("Create Note", circularApproveRequestDTO.getRemarks());
		circularApproveRequestDTO.setId(viewcirculardetails.getId());
		circularApproveRequestDTO.setRemarks(rejectRemarks);
		circularApproveRequestDTO.setStatus(CreditSalesDemandStatus.REJECTED.toString());
		circularApproveRequestDTO.setForwardFor(viewcirculardetails.getForwardFor());
		circularApproveRequestDTO.setForwardTo(userMaster.getId());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgComments1').hide();");

		log.info("submited info", circularApproveRequestDTO);

		try {

			circularstatus = new CircularRegisterLog();
			log.info("view meeting list called..");

			String url = SERVER_URL + "/circular/approvecircularregister";
			BaseDTO baseDTO = httpService.post(url, circularApproveRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				circularstatus = mapper.readValue(jsonResponse, new TypeReference<CircularRegisterLog>() {
				});
				errorMap.notify(ErrorDescription.CIRCULAR_REJECTED_SUCCESS.getCode());
				return showCircularListPage();
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<<<< ----------Ends submitApprovedStatus------ >>>>");
		selectedcircular = new CircularListDTO();
		return null;

	}

	public String backtolistpage() {
		selectedcircular = new CircularListDTO();
		return LIST_PAGE;

	}

	public String editcircular() {
		log.info("<<<< ----------Starts editcircular------ >>>>");

		log.info("selected view id", selectedcircular);

		if (selectedcircular == null || selectedcircular.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
			return null;
		}

		if (!selectedcircular.getStatus().equalsIgnoreCase("REJECTED")) {
			errorMap.notify(ErrorDescription.CANNOT_BE_EDITED.getCode());
			return null;
		}

		long circularid = selectedcircular.getId();
		log.info("meeting id", circularid);

		try {

			viewcirculardetails = new CircularRequestViewDTO();
			if (action.equalsIgnoreCase("EDIT")) {
				loadInitiatedEmployeeList();
				loadCreateNoteEmpDetails();
				log.info("view meeting list called..");

				viewcirculardetails.setId(circularid);
				viewcirculardetails.setAction(action);
				String url = SERVER_URL + "/circular/getbyid";
				BaseDTO baseDTO = httpService.post(url, viewcirculardetails);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					viewcirculardetails = mapper.readValue(jsonResponse, new TypeReference<CircularRequestViewDTO>() {
					});

					createCircularDTO.setId(viewcirculardetails.getId());
					createCircularDTO.setToWhom(viewcirculardetails.getToWhom());
					createCircularDTO.setEntity(viewcirculardetails.getEntity());
					createCircularDTO.setEntityType(viewcirculardetails.getEntityType());
					createCircularDTO.setDepartmentId(viewcirculardetails.getDepartmentId());
					createCircularDTO.setSectionId(viewcirculardetails.getSectionId());
					createCircularDTO.setInitiatedBy(viewcirculardetails.getInitiatedById());
					createCircularDTO.setMeetingRequired(viewcirculardetails.getMeeting());
					createCircularDTO.setMeetingType(viewcirculardetails.getMeetingType());
					createCircularDTO.setVenue(viewcirculardetails.getVenue());
					createCircularDTO.setVenueId(viewcirculardetails.getVenueId());
					createCircularDTO.setMeetingTypeId(viewcirculardetails.getMeetingTypeId());

					createCircularDTO.setMeetingDate(viewcirculardetails.getMeetingDate());
					createCircularDTO.setStartTime(viewcirculardetails.getStartTime());
					createCircularDTO.setEndTime(viewcirculardetails.getEndTime());
					createCircularDTO.setRemainderByMail(viewcirculardetails.getRemainderByMail());
					createCircularDTO.setSubject(viewcirculardetails.getSubject());
					createCircularDTO.setCircularDescription(viewcirculardetails.getCircularDescription());
					createCircularDTO.setForwardFor(viewcirculardetails.getForwardFor());
					createCircularDTO.setForwardTo(viewcirculardetails.getForwardTo());
					createCircularDTO.setNote(viewcirculardetails.getNote());
					createCircularDTO.setFilePath(viewcirculardetails.getFilePath());
					createCircularDTO.setEntityId(viewcirculardetails.getEntityId());
					if (viewcirculardetails != null && viewcirculardetails.getFilePath() != null) {
						File myFile = new File(viewcirculardetails.getFilePath());
						fileType = myFile.getName();
					}
					if (viewcirculardetails.getCircularRegister() != null
							&& viewcirculardetails.getCircularRegister().getId() != null) {
						CircularRegister circularRegister = viewcirculardetails.getCircularRegister();
						if (circularRegister != null && circularRegister.getEntityMaster() != null) {
							selctedentitytypeid = circularRegister.getEntityMaster().getEntityTypeMaster();
						}
						status = circularRegister.getMeetingRequired();
						headangregionaofficeid = circularRegister.getEntityMaster();
						if (headangregionaofficeid != null) {
							loadHeadAndRegionalOfficeList();
							horoValueChange();
							loadEntityTypeListByEntityId(headangregionaofficeid.getId());
							onSelectmeetingType();
						}
						if (createCircularDTO.getDepartmentId() != null) {
							onSelectdepartment();
						}
						loadmeetingdepartmentlist();
						loadmeetingtypelist();
						loadVenue();

					}

				} else {

					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}

				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("===== Error is  ===  " + e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<<<< ----------Ends editcircular------ >>>>");
		return null;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("<<=== ClaimForExpoBean.uploadDocument() File Upload method called");
		try {
			UploadedFile file = event.getFile();
			fileType = file.getFileName();
			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")
					&& !fileType.contains(".xls") && !fileType.contains(".xlsx")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				setFileType("");
				return;

			}
			//
			// UploadedFile file = event.getFile();
			// fileType = file.getFileName();
			// log.info(" File Type :: " + fileType);
			// boolean validFileType = AppUtil.isValidFileType(fileType,
			// new String[] { "png", "jpg", "JPG", "doc", "jpeg", "gif", "docx", "pdf",
			// "xlsx", "xls" });
			// if (!validFileType) {
			// FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			// errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
			// return;
			// }

			long size = file.getSize();
			// size = size / (1024*1024);
			if ((size / (1024 * 1024)) > 100) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true); 
				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
			}
			if (size > 0) {
				String createRaiseComplaintPathName = commonDataService.getAppKeyValue("CREATE_CIRCULAR_UPLOAD_DOC");
				String filePath = Util.fileUpload(createRaiseComplaintPathName, file);
				circularDocumentFileName = filePath;
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Uploading Document is uploaded successfully");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("<<==  Error in file upload " + e.getMessage());
		}
		log.info("<======Ends ClaimForExpoBean.uploadDocument() ====> ");
	}

	public String canclerequest() {

		// return LIST_PAGE;
		return showCircularListPage();
	}

	public String cleasrCircularSession() {
		viewButtonFlag = true;
		deleteButtonFlag = true;
		editButtonFlag = true;
		selectedcircular = new CircularListDTO();

		return LIST_PAGE;
	}

	public void cancelviewnote() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");

	}

	private Date todayDate = new Date();

	public Date getTodayDate() {
		return todayDate;
	}

	public boolean getVisibleStatus() {
		log.info("getVisibleStatus called....");
		boolean valid = false;
		if (lastNote != null) {
			Long loginId = loginBean.getUserDetailSession().getId();
			if (lastNote != null) {
				if (lastNote.getForwardTo() != null) {
					if (lastNote.getForwardTo().getId().equals(loginId)) {
						valid = true;
					} else {
						valid = false;
					}
				}
			}
		} else {
			log.info("lastNote is null");
			valid = false;
		}
		return valid;
	}

	public void downloadfile() {
		InputStream input = null;
		try {
			if (filepath != null) {
				File files = new File(filepath);
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
			} else {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
				log.error("File is empty----->");
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
			errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public EntityMasterDTO loadEntityByEntityId(Long id) {
		EntityMasterDTO entityMaster = new EntityMasterDTO();
		try {
			String URL = AppUtil.getPortalServerURL() + "/entitymaster/getentitymasterbyentityId/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			entityMaster = mapper.readValue(jsonResponse, EntityMasterDTO.class);

			if (entityMaster == null) {
				log.info("EmployeeMasterBean loadEntityByEntityId value is null");
			}

			log.info("SupplyRateConfirmationBean entityMasterObject " + entityMaster.toString());
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
		return entityMaster;
	}

	public void updateCircularFor() {
		employeeMasters = new ArrayList<>();
		createCircularDTO.setInitiatedBy(null);
		if (createCircularDTO.getToWhom().equalsIgnoreCase("ALL")) {
			headangregionaofficeid = null;
			selctedentitytypeid = null;
			createCircularDTO.setEntityId(null);
			createCircularDTO.setDepartmentId(null);
			createCircularDTO.setSectionId(null);
			if (loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
				employeeMasters = commonDataService.getEmployeeMasterByEntityId(
						loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
			}

		} else if (createCircularDTO.getToWhom().equalsIgnoreCase("ALLSECTIONS")) {
			createCircularDTO.setSectionId(null);
		} else if (createCircularDTO.getToWhom().equalsIgnoreCase("ALLDEPARTMENTS")) {
			createCircularDTO.setDepartmentId(null);

		}
	}

	/*
	 * Load Employee List For Selected Employees used for Circular for option is
	 * selected employees
	 */
	public void loadSelectedEmployeeList() {
		log.info("CircularBean. loadSelectedEmployeeList() Started");
		Long entityTypeId = null;
		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames = "";
			String headRegionCode = headangregionaofficeid == null ? null
					: headangregionaofficeid.getEntityTypeMaster() == null ? null
							: headangregionaofficeid.getEntityTypeMaster().getEntityCode();
			createCircularDTO.setHoroId(headangregionaofficeid.getId());
			if (StringUtils.isNotEmpty(headRegionCode) && headRegionCode.equals(EntityType.HEAD_OFFICE)) {
				entityTypeId = headangregionaofficeid.getEntityTypeMaster() == null ? null
						: headangregionaofficeid.getEntityTypeMaster().getId();
				createCircularDTO.setEntityTypeId(entityTypeId);
				createCircularDTO.setEntityId(headangregionaofficeid.getId());
			} else {
				entityTypeId = selctedentitytypeid == null ? null : selctedentitytypeid.getId();
				createCircularDTO.setEntityTypeId(entityTypeId);
				createCircularDTO.setEntityId(createCircularDTO.getEntityId());
			}
			String url = SERVER_URL + "/employee/getcircularselectedemplist";

			BaseDTO baseDTO = httpService.post(url, createCircularDTO);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
			int employeeListSize = employeeList == null ? 0 : employeeList.size();
			log.info("Circular Selected Employee List Size :" + employeeListSize);
		} catch (Exception e) {
			log.error("Exception at CircularBean. loadSelectedEmployeeList() ", e);
		}
		log.info("CircularBean. loadSelectedEmployeeList() Ends");
	}

	public void changeEmpNames() {
		log.info("CircularBean changeEmpNames() Starts");
		try {
			employeeNames = "";
			for (EmployeeMaster employee : selectedEmployees) {
				if (employeeNames != "") {
					employeeNames = employeeNames + ",<br/>";
				}
				employeeNames = employeeNames + employee.getPersonalInfoEmployment().getPfNumber() + "/"
						+ employee.getFirstName();
			}
			log.info("CircularBean selected employee name : " + employeeNames);
		} catch (Exception e) {
			log.error("Exception at CircularBean. changeEmpNames() ", e);
		}
		log.info("CircularBean changeEmpNames() Ends");
	}

	public void changeDesignationNames() {
		log.info("CircularBean changeDesignationNames() Starts");
		try {
			designationNames = "";
			if (CollectionUtils.isNotEmpty(selectedDesignations)) {
				for (Designation designation : selectedDesignations) {
					if (designationNames != "") {
						designationNames = designationNames + ",<br/>";
					}
					designationNames = designationNames + designation.getName();
				}
			}
			log.info("CircularBean selected designation name : " + designationNames);
		} catch (Exception e) {
			log.error("Exception at CircularBean. changeDesignationNames() ", e);
		}
		log.info("CircularBean changeDesignationNames() Ends");
	}

	public void loadHeadAndRegionalOfficeList() {
		log.info("CircularBean loadHeadAndRegionalOffice() Starts");
		try {
			horoList = commonDataService.loadHeadAndRegionalOffice();
			int horoListSize = horoList == null ? 0 : horoList.size();
			log.info("CircularBean Head and Regional Office List Size :" + horoListSize);
		} catch (Exception exp) {
			log.error("Exception at loadHeadAndRegionalOffice() ", exp);
		}
		log.info("CircularBean loadHeadAndRegionalOffice() Ends");
	}

	public List<EmployeeMaster> loadInitiatedEmployeeList() {
		log.info("CircularBean.loadInitiatedEmployeeList() Starts");
		try {
			EntityMaster workLocationEntity = null;
			EmployeePersonalInfoEmployment personalInfoEmployment = null;
			Long regionId = null;
			EntityMasterDTO entityMaster = null;
			EmployeeMaster employeMaster = loginBean.getEmployee();
			if (employeMaster != null) {
				personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
				if (personalInfoEmployment != null) {
					workLocationEntity = personalInfoEmployment.getWorkLocation();
				} else {
					log.error("EmployeePersonalInfoEmployment is empty");
				}
			} else {
				log.error("EmployeeMaster is empty");
			}
			if (workLocationEntity != null) {
				entityMaster = loadEntityByEntityId(workLocationEntity.getId());
				if (entityMaster.getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE)
						|| entityMaster.getEntityTypeMaster().getEntityCode().equals(EntityType.REGIONAL_OFFICE)) {
					regionId = personalInfoEmployment.getWorkLocation().getId();
				} else {
					regionId = entityMaster.getEntityMasterRegion().getId();
				}
			} else {
				log.info("<======== workLocationEntity is null ===========> ");
			}

			log.info("<======== Employee Region Id:   " + regionId);
			if (entityMaster != null && entityMaster.getId() != null) {
				String url = AppUtil.getPortalServerURL() + "/employee/getemployeelistbyworklocationid/" + regionId;
				log.info("loadEmployeeList url is" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasters = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
				}
			}
			int employeeMastersSize = employeeMasters == null ? 0 : employeeMasters.size();
			log.info("Initiated by Employee List Size :" + employeeMastersSize);
		} catch (Exception e) {
			log.error("Exception at CircularBean. loadInitiatedEmployeeList()", e);
		}
		log.info("CircularBean.loadEmployeeList() Ends");
		return employeeMasters;
	}

	public void horoValueChange() {
		log.info("CircularBean horoValueChange() Starts");
		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames = "";
			createCircularDTO.setEntityTypeId(null);
			createCircularDTO.setEntityId(null);
			if (headangregionaofficeid != null && headangregionaofficeid.getEntityTypeMaster() != null
					&& headangregionaofficeid.getEntityTypeMaster().getEntityCode()
							.equalsIgnoreCase(EntityType.HEAD_OFFICE)) {
				createCircularDTO.setEntityId(headangregionaofficeid.getId());
				visibility = true;
			} else {
				loadEntityTypeListByEntityId(headangregionaofficeid.getId());
				visibility = false;
			}
			loadSelectedEmployeeList();
		} catch (Exception exp) {
			log.error("Exception at CircularBean.  horoValueChange()", exp);
		}
		log.info("CircularBean. horoValueChange() Ends");
	}

	public void circularForChange() {
		log.info("CircularBean circularForChange() - START");
		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames = "";
			createCircularDTO.setEntityTypeId(null);
			createCircularDTO.setEntityId(null);
			createCircularDTO.setDepartmentId(null);
			createCircularDTO.setSectionId(null);
			if (CircularForType.ALL_REGIONS.equals(createCircularDTO.getToWhom())) {
				if (!CollectionUtils.isEmpty(meetingdepartmentlist)) {
					List<Department> adminDeptList = meetingdepartmentlist.stream()
							.filter(o -> o.getCode() != null && "ADMIN".equalsIgnoreCase(o.getCode()))
							.collect(Collectors.toList());
					int adminDeptListSize = adminDeptList != null ? adminDeptList.size() : 0;
					log.info("CircularBean circularForChange() - adminDeptListSize: " + adminDeptListSize);
					if (!CollectionUtils.isEmpty(adminDeptList)) {
						Department adminDept = adminDeptList.get(0);
						if (adminDept != null) {
							Long deptId = adminDept.getId();
							createCircularDTO.setDepartmentId(deptId);
							if (deptId != null) {
								meetingsectionlist = commonDataService.getActiveSectionByDepartmentId(deptId);
								if (!CollectionUtils.isEmpty(meetingsectionlist)) {
									List<SectionMaster> adminSectionList = meetingsectionlist.stream()
											.filter(o -> o.getCode() != null && "ADMIN".equalsIgnoreCase(o.getCode()))
											.collect(Collectors.toList());
									int adminSectionListSize = adminSectionList != null ? adminSectionList.size() : 0;
									log.info("CircularBean circularForChange() - adminSectionListSize: "
											+ adminSectionListSize);
									if (!CollectionUtils.isEmpty(adminSectionList)) {
										SectionMaster sectionMaster = adminSectionList.get(0);
										if (sectionMaster != null) {
											createCircularDTO.setSectionId(sectionMaster.getId());
										}
									}
								}
							}
						}
					}
				}
			} else if (CircularForType.SELECTED_DESIGNATIONS.equals(createCircularDTO.getToWhom())) {
				designationList = commonDataService.loadDesignation();
			}
		} catch (Exception e) {
			log.error("Exception at CircularBean circularForChange()", e);
		}
		log.info("CircularBean circularForChange() - END");
	}

	/* Load Entity Type List based Region Id */
	public void loadEntityTypeListByEntityId(Long regionId) {
		log.info("CircularBean loadEntityTypeListByEntityId() Starts");
		try {
			employeeList = new ArrayList<>();
			selectedEmployees = new ArrayList<>();
			employeeNames = "";
			designationNames = "";
			entityTypeList = commonDataService.loadEntityTypeBasedOnHoRo(regionId);
			int entityTypelistSzie = entityTypeList == null ? 0 : entityTypeList.size();
			log.info("CircularBean EntityTypeList Size :" + entityTypelistSzie);
		} catch (Exception e) {
			log.error("Exception at CircularBean. loadEntityTypeListByEntityId() ", e);
		}
		log.info("CircularBean loadEntityTypeListByEntityId() Ends");
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("circularId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			selectedcircular = new CircularListDTO();
			action = "VIEW";
			selectedcircular.setId(Long.parseLong(circularId));
			selectedNotificationId = Long.parseLong(notificationId);
			viewDetails();
		}
		log.info("CircularBean showViewListPage() Ends");
		addButtonFlag = true;
		return VIEW_PAGE;
	}

	public String showNotification() {
		log.info("id------------------------->" + getCircularId());
		try {
			if (!Objects.isNull(circularId)) {
				selectedcircular = new CircularListDTO();
				selectedcircular.setId(circularId);
				Long systemNotificationId = (notificationId != null ? Long.valueOf(notificationId) : 0l);
				selectedNotificationId = systemNotificationId;
				action = "VIEW";
				// selectedProductDesignTarget.setNotificationId(systemNotificationId);

				viewDetails();

			}
		} catch (Exception e) {
			log.error("notification method error---", e);
		}
		return VIEW_PAGE;
	}
}
