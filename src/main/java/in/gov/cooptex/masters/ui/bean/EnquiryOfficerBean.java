package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.disciplinary.model.EnquiryOfficer;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.Validate;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("enquiryOfficerBean")
public class EnquiryOfficerBean {

	private final String CREATE_ENQUIRY_OFFICER_PAGE = "/pages/masters/enquiryofficer/createEnquiryOfficer.xhtml?faces-redirect=true;";
	private final String ENQUIRY_OFFICER_LIST_URL = "/pages/masters/enquiryofficer/listEnquiryOfficer.xhtml?faces-redirect=true;";
	private final String ENQUIRY_OFFICER_VIEW_URL = "/pages/masters/enquiryofficer/viewEnquiryOfficer.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	LazyDataModel<EnquiryOfficer> lazyEnquiryOfficerModel;

	@Getter
	@Setter
	EnquiryOfficer enquiryOfficer = new EnquiryOfficer();

	@Getter
	@Setter
	EnquiryOfficer selectedEnquiryOfficer = new EnquiryOfficer();

	@Getter
	@Setter
	List<EnquiryOfficer> enquiryOfficerList = new ArrayList<EnquiryOfficer>();

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	public String init() {
		log.info("EnquiryOfficerBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadLazyEnquiryOfficerList();
		addButtonFlag = true;
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return ENQUIRY_OFFICER_LIST_URL;
	}

	public String saveEnquiryOfficer() {
		try {
			log.info("<=call saveEnquiryOfficer=>");
			log.info("<=enquiryOfficer=>" + enquiryOfficer);

			if (enquiryOfficer.getOrganizationName() == null || enquiryOfficer.getOrganizationName().isEmpty()) {
				errorMap.notify(ErrorDescription.ENQUIRY_ORGANIZATION_NAME_REQUIRED.getErrorCode());
				return null;
			}
			if (enquiryOfficer.getOfficerName() == null || enquiryOfficer.getOfficerName().isEmpty()) {
				errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_NAME_REQUIRED.getErrorCode());
				return null;
			}
			if (enquiryOfficer.getEmailId() == null || enquiryOfficer.getEmailId() == null) {
				errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_EMAILID_REQUIRED.getErrorCode());
				Validate.validateEmail(enquiryOfficer.getEmailId());
				return null;
			}

			if (enquiryOfficer.getContactNumber() == null || enquiryOfficer.getContactNumber().isEmpty()) {
				errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_CONTACTNUMBER_REQUIRED.getErrorCode());
				return null;
			}

			if (enquiryOfficer.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_STATUS_REQUIRED.getErrorCode());
				return null;
			}
			Boolean emailId = Validate.validateEmail(enquiryOfficer.getEmailId());
			if (!emailId) {
				errorMap.notify(ErrorDescription.INAVALID_EMAIL.getErrorCode());
				return null;
			}
			Boolean contactNumber = Validate.validateMobileNo(enquiryOfficer.getContactNumber());
			if (!contactNumber) {
				errorMap.notify(ErrorDescription.INVALID_CONTACT_NUMBER.getErrorCode());
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/enquiryofficer/create";
			BaseDTO baseDTO = httpService.post(url, enquiryOfficer);
			log.info("Save Enquiry Officer : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EnquiryOfficer enquiryOfficer = mapper.readValue(jsonResponse, EnquiryOfficer.class);
			log.info("Enquiry Officer==>" + enquiryOfficer);
			if (baseDTO.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_ADDED_SUCCESSFULLY.getErrorCode());
				else
					errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_UPDATED_SUCCESSFULLY.getErrorCode());

				clear();
				loadLazyEnquiryOfficerList();
				return ENQUIRY_OFFICER_LIST_URL;

			} else {
				log.info("Error while saving biometric with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in Enquiry Officer  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadLazyEnquiryOfficerList() {

		log.info("<=EnquiryOfficerBean :: loadLazyEnquiryOfficerList=>");
		lazyEnquiryOfficerModel = new LazyDataModel<EnquiryOfficer>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<EnquiryOfficer> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				EnquiryOfficer request = new EnquiryOfficer();
				try {
					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));
					log.info("Search request data" + request);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/enquiryofficer/lazyload/search";
					log.info("loadLazyEnquiryOfficerList url==>" + url);
					BaseDTO baseDTO = httpService.post(url, request);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					enquiryOfficerList = mapper.readValue(jsonResponse, new TypeReference<List<EnquiryOfficer>>() {
					});
					if (enquiryOfficerList == null) {
						log.info(" :: loadLazyEnquiryOfficerList Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: totalRecords ::" + totalRecords);
					} else {
						log.error(":: loadLazyEnquiryOfficerList Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return enquiryOfficerList;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading loadLazyEnquiryOfficerList ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EnquiryOfficer res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EnquiryOfficer getRowData(String rowKey) {
				List<EnquiryOfficer> responseList = (List<EnquiryOfficer>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EnquiryOfficer res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEnquiryOfficer = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void onRowSelect() {
		addButtonFlag = false;

	}

	public void clear() {
		enquiryOfficer = new EnquiryOfficer();
		selectedEnquiryOfficer = new EnquiryOfficer();
	}

	public String clearPage() {
		addButtonFlag = true;
		enquiryOfficer = new EnquiryOfficer();
		selectedEnquiryOfficer = new EnquiryOfficer();
		loadLazyEnquiryOfficerList();
		return ENQUIRY_OFFICER_LIST_URL;
	}

	public String showCommunalRoasterData() {
		log.info("<===== Starts CommunalRosterResisterBean.showCommunalRoasterData ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedEnquiryOfficer == null || selectedEnquiryOfficer.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Enquiry Officer called..");
				return CREATE_ENQUIRY_OFFICER_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit GlAccountCategory called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/enquiryofficer/get/"
						+ selectedEnquiryOfficer.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					enquiryOfficer = mapper.readValue(jsonResponse, new TypeReference<EnquiryOfficer>() {
					});
					return CREATE_ENQUIRY_OFFICER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete GlAccountCategory called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEnquiryOfficerDelete').show();");

			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("<=EnquiryOfficerBean :: viewEnquiryOfficer=>");
				try {
					if (selectedEnquiryOfficer == null) {
						errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
						return null;
					}
					String apiURL = SERVER_URL + appPreference.getOperationApiUrl() + "/enquiryofficer/get/"
							+ selectedEnquiryOfficer.getId();
					log.info("<=EnquiryOfficerBean :: view Enquiry Officer :: url=>" + apiURL);
					BaseDTO baseDTO = httpService.get(apiURL);
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						enquiryOfficer = mapper.readValue(jsonValue, EnquiryOfficer.class);
						return ENQUIRY_OFFICER_VIEW_URL;
					}

				} catch (Exception e) {
					log.error(" Exception Occured While view Enquiry Officer :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showEnquriyOfficerData ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EnquiryOfficerBean.showEnquiryOfficerData ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		try {

			if (selectedEnquiryOfficer == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("selected Enquiry Officer  Id : : " + selectedEnquiryOfficer.getId());
			String getUrl = SERVER_URL + appPreference.getOperationApiUrl() + "/enquiryofficer/delete/id/:id";
			String url = getUrl.replace(":id", selectedEnquiryOfficer.getId().toString());
			log.info("deleteEnquiry Officer Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Roster Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.ENQUIRY_OFFICER_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			clear();
			init();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Enquiry Officer  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return ENQUIRY_OFFICER_LIST_URL;
	}
}
