package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AdvertisementDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.MediaMaster;
import in.gov.cooptex.core.model.MediaType;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.advt.model.Advertisement;
import in.gov.cooptex.operation.advt.model.AdvertisementCategory;
import in.gov.cooptex.operation.advt.model.AdvertisementType;
import in.gov.cooptex.operation.dto.AdvertisementSearchRequestDTO;
import in.gov.cooptex.operation.dto.AdvertisementSearchResponseDTO;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service
@Scope("session")
@Log4j2
public class AdvertisementBean {

	private final String CREATE_PAGE = "/pages/operation/advertisement/createAdvertisement.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/operation/advertisement/listAdvertisement.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/operation/advertisement/viewAdvertisement.xhtml?faces-redirect=true;";

	String serverURL;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private Boolean theaterDetailsFlag = false, newsPaperDetailsFlag = false, radioDetailsFlag = false, magazineDetailsFlag=false, socialMediaDetailsFlag=false,
			smsDetailsFlag = false, bannerDetailsFlag = false, approveStatus = false, televisionDetailsFlag = false;

	@Getter
	@Setter
	List<MediaType> mediaTypeList;

	@Getter
	@Setter
	List<AdvertisementType> advertisementTypeList;

	@Setter
	@Getter
	AdvertisementType selectedAdvertisementType;

	@Getter
	@Setter
	List<AdvertisementCategory> advertisementCategoryList;
	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeMasterList;
	@Getter
	@Setter
	CustomerTypeMaster selectedCustomerTypeMaster;
	@Getter
	@Setter
	List<CustomerMaster>  customerMasterList;

	@Setter
	@Getter
	AdvertisementCategory selectedAdvertisementCategory;

	@Getter
	@Setter
	String selectedMediaType;

	@Setter
	@Getter
	MediaMaster mediaMaster;

	@Setter
	@Getter
	MediaMaster selectedMediaMaster;

	@Getter
	@Setter
	List<MediaMaster> mediaMasterList = new ArrayList<MediaMaster>();

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	SupplierTypeMaster selectedSupplierTypeMaster;

	@Setter
	@Getter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	private Advertisement advertisement;

	@Getter
	@Setter
	List<Advertisement> advertisementList = new ArrayList<>();;

	@Getter
	@Setter
	MediaType seletedMedia;

	@Getter
	@Setter
	AdvertisementSearchResponseDTO responseDto;

	@Getter
	@Setter
	private UploadedFile bannerFile = null;

	@Getter
	@Setter
	private String[] languageList = { "English", "Hindi", "Tamil", "Telegu" };

	@Getter
	@Setter
	private String[] colorList = { "Black & White", "Color" };

	@Getter
	@Setter
	String action, bannerFileName = "";

	@Getter
	@Setter
	Integer totalRecords;

	@Getter
	@Setter
	LazyDataModel<AdvertisementSearchResponseDTO> lazyList;

	@Getter
	@Setter
	List<UserMaster> forwardToList;
	@Getter
	@Setter
	AdvertisementDTO advertisementDTO = new AdvertisementDTO();

	@Getter
	@Setter
	UserMaster forwardTo = new UserMaster();

	@Getter
	@Setter
	String note, address, forwardToName, pageHead,remark;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AddressMasterBean addressMasterBean;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	BaseDTO baseDTO;
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	private Boolean previousStatus=false;
	
	@Autowired
	LoginBean loginBean;
	
	
	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean rejectButtonFlag = false;
	
	
	@Getter
	@Setter
	Boolean finalButtonFlag = false;
	
	@Getter @Setter
	String fileName;
	
	@Setter
	@Getter
	private StreamedContent file;

	public String showAddPage() {
		log.info(":: Start showAddPage method ::");
		try {
		action = "ADD";
		pageHead = "Create";
		advertisement = new Advertisement();
		advertisementList = new ArrayList<>();
		mediaMaster = new MediaMaster();
		selectedAdvertisementCategory = new AdvertisementCategory();
		selectedAdvertisementType = new AdvertisementType();
		selectedSupplierTypeMaster = new SupplierTypeMaster();
		selectedMediaMaster = new MediaMaster();
		seletedMedia = new MediaType();
		getAllCustomerType();
		loadData();
		loadMediaTypeList();
		loadAdvertisementCategoryList();
		loadAdvertisementTypeList();
		theaterDetailsFlag = false;
		newsPaperDetailsFlag = false;
		smsDetailsFlag = false;
		radioDetailsFlag = false;
		bannerDetailsFlag = false;
		magazineDetailsFlag=false;
		televisionDetailsFlag = false;
		socialMediaDetailsFlag=false;
		bannerFileName = "";
		loadForwardUser();
		forwardTo = new UserMaster();
		approveStatus = null;
		note = "";
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		}
		catch (Exception e) {
			log.info("Exception at ShowAddPage ::",e);
		}
		return CREATE_PAGE;
	}

	public String showEditPage() {
		if (responseDto == null || responseDto.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		action = "EDIT";
		pageHead = "Edit";
		/*if (!validateEdit()) {
			errorMap.notify(ErrorDescription.EDIT_ERROR.getErrorCode());
			return null;
		}*/
		loadMediaTypeList();
		loadAdvertisementCategoryList();
		loadAdvertisementTypeList();
		loadForwardUser();
		getAdvertisementInfo();
		mediaMaster = advertisement.getMediaMaster();
		seletedMedia = advertisement.getMediaMaster().getMediaType();
		selectedAdvertisementCategory = advertisement.getAdvertisementCategory();
		selectedAdvertisementType = advertisement.getAdvertisementType();
		selectedMediaMaster = mediaMaster;
		forwardTo = new UserMaster();
		approveStatus = null;
		note = "";
		return CREATE_PAGE;
	}

	public String showViewPage() {
		if (responseDto == null || responseDto.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}

		action = "VIEW";
		forwardToList =commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ADVERTISEMENT.toString());
		getAdvertisementInfo();
		return VIEW_PAGE;
	}

	public String showListPage() {
		action = "LIST";
		seletedMedia = new MediaType();
		selectedMediaType = "";
		responseDto = new AdvertisementSearchResponseDTO();
		loadData();
		list();
		return LIST_PAGE;
	}

	public void onSelectMediaType() {
		log.info("<<=====  On select media type::" + seletedMedia);
		mediaMasterList.clear();
		selectedMediaMaster = new MediaMaster();
		mediaMaster = new MediaMaster();
		selectedMediaType = "";
		if (seletedMedia != null)
			selectedMediaType = seletedMedia.getName();
		log.info("<<=====  On select media type::" + selectedMediaType);
		selectedMediaMaster = new MediaMaster();
		advertisement = new Advertisement();
		loadMediaMasterList();
		selectPanel(seletedMedia.getCode());

		if (selectedMediaType.contains("Theater")) {
		} else if (selectedMediaType.contains("Newspaper")) {
		} else if (selectedMediaType.contains("Radio")) {
		} else if (selectedMediaType.contains("Banner")) {
			loadSupplierTypeList();
		}

	}

	private void loadData() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {

			log.error("<<====  Error In ProfileMasterBean Constructor:: ");
		}
	}

	public void loadMediaTypeList() {
		log.info("<<====== AdvertisementBean ---  getMediaTypeList  ====  ##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/mediatype/getActiveMediaType";
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				mediaTypeList = mapper.readValue(jsonResponse, new TypeReference<List<MediaType>>() {
				});
			}
		} catch (Exception e) {
			log.error("<<====   ERROR Occored :: " + e);
		}
		if (mediaTypeList != null)
			log.info("<<===  LIST SIZE:: " + mediaTypeList.size());
		log.info("<<====== AdvertisementBean ---  getMediaTypeList  ====  ##ENDS");
	}

	public void loadAdvertisementCategoryList() {
		log.info("<<====== AdvertisementBean ---  loadAdvertisementCategoryList  ====  ##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/advtcategory/getActiveAdvertisementCategory";
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				advertisementCategoryList = mapper.readValue(jsonResponse,
						new TypeReference<List<AdvertisementCategory>>() {
						});
			}
		} catch (Exception e) {
			log.error("<<====   ERROR Occored :: " + e);
		}
		log.info("<<== advertisementCategoryList:: " + advertisementCategoryList);
		if (advertisementCategoryList != null)
			log.info("<<===  LIST SIZE:: " + advertisementCategoryList.size());
		log.info("<<====== AdvertisementBean ---  loadAdvertisementCategoryList  ====  ##ENDS");
	}

	public void loadAdvertisementTypeList() {
		log.info("<<====== AdvertisementBean ---  loadAdvertisementTypeList  ====  ##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/advttype/getActiveAdvertisementType";
	 		log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				advertisementTypeList = mapper.readValue(jsonResponse, new TypeReference<List<AdvertisementType>>() {
				});
			}
		} catch (Exception e) {
			log.error("<<====   ERROR Occored :: " + e);
		}
		if (advertisementTypeList != null)
			log.info("<<===  LIST SIZE:: " + advertisementTypeList.size());
		log.info("<<====== AdvertisementBean ---  loadAdvertisementTypeList  ====  ##ENDS");
	}

	public void loadMediaMasterList() {
		log.info("<<======   AdvertisementBean ----  loadMediaMasterList  ====##STARTS  " + seletedMedia);

		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/mediamaster/getMediaByType/"
					+ seletedMedia.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				mediaMasterList = mapper.readValue(jsonResponse, new TypeReference<List<MediaMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("<<========  ERROR :::  " + e);
		}
		// selectedMediaMaster = new MediaMaster();
		// mediaMaster = new MediaMaster();
		log.info("<<======   AdvertisementBean ----  loadMediaMasterList  ====##ENDS");
	}

	public void onSelectMediaMaster() {
		log.info("<<========  AdvertisementBean --- onSelectMediaMaster ====##STARTS   " + selectedMediaMaster);
		try {
			
			if (selectedMediaMaster!=null) {
				String url = serverURL + appPreference.getOperationApiUrl() + "/mediamaster/get/"
						+ selectedMediaMaster.getId();
				log.info("<<======  URL  :: " + url);
				baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					mediaMaster = mapper.readValue(jsonResponse, MediaMaster.class);
				}
			}else {
				mediaMaster=new MediaMaster();
			}
		} catch (Exception e) {
			log.error("<<=====  ERROR::: " + e);
		}
		log.info("<<========  AdvertisementBean --- onSelectMediaMaster ====##ENDS");
	}

	public void loadNewsPaperList() {
		log.info("<<========  AdvertisementBean --- onSelectMediaMaster ====##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/mediamaster/get/"
					+ selectedMediaMaster.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				mediaMaster = mapper.readValue(jsonResponse, MediaMaster.class);
			}
		} catch (Exception e) {
			log.error("<<=====  ERROR::: " + e);
		}
		log.info("<<========  AdvertisementBean --- onSelectMediaMaster ====##ENDS");
	}

	public void loadSupplierTypeList() {
		log.info("<<========  AdvertisementBean --- loadSupplierTypeList ====##STARTS");
		try {
			String url = serverURL + "/suppliertypemaster/getAllWithoutSociety";
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				supplierTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierTypeMaster>>() {
				});
			}
			log.info("<<======  supplierMasterList  LIST SIZE:: " + supplierTypeMasterList.size());
		} catch (Exception e) {
			log.error("<<=====  ERROR::: " + e);
		}
		selectedSupplierTypeMaster = new SupplierTypeMaster();
		log.info("<<========  AdvertisementBean --- loadSupplierTypeList ====##ENDS");
	}

	public void onselectSupplierType() {
		log.info("<<========  AdvertisementBean --- onselectSupplierType ====##STARTS");
		try {
			String url = serverURL + "/supplier/master/getBySupplierType/" + selectedSupplierTypeMaster.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
			}
			log.info("<<======  supplierMasterList  LIST SIZE:: " + supplierMasterList.size());
		} catch (Exception e) {
			log.error("<<=====  ERROR::: " + e);
		}
		log.info("<<========  AdvertisementBean --- onselectSupplierType ====##ENDS");
	}

	public String handleFileUpload(FileUploadEvent event) {
		log.info("<<===  File Upload method called");
		try {
			bannerFile = event.getFile();
			String fileType = bannerFile.getFileName();
			fileName = fileType;
			if (!fileType.contains(".jpg") && !fileType.contains(".png") && !fileType.contains(".jpeg") && !fileType.contains(".mp4")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
				return null;
			}
			long size = bannerFile.getSize();
			// size = size / (1024*1024);
			if ((size / (1024 * 1024)) > 2) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
				return null;
			}
			if (size > 0) {
				String billcopyPathName = commonDataService.getAppKeyValue("ADVERTISE_BANNER_FILE_PATH");
				String filePath = Util.fileUpload(billcopyPathName, bannerFile);
				advertisement.setFileUpload(filePath);
				if (!filePath.isEmpty()) {
					String[] names = filePath.split("/");
					bannerFileName = names[names.length - 1];
				}
				errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Relieving Document is uploaded successfully");
			}

		} catch (Exception e) {
			log.info("<<==  Error in file upload " + e);
		}
		return null;
	}

	
	
	public Boolean creatValidation() {
		
		if(televisionDetailsFlag==true) {
		log.info(" mediaMaster.getId()::"+ mediaMaster.getId());
			if (mediaMaster == null || mediaMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TELEVISION_CODE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getLanguage() == null || advertisement.getLanguage().trim().isEmpty() ) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_LANGUAGE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getRatings() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TRP_RATING_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getDuration() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_DURATION).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getFromDate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_FROM_DATE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getToDate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TO_DATE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getTotalAmount() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TOTAL_RATE_EMPTY).getCode());
				return true;
			}
		}

		if(newsPaperDetailsFlag==true) {
			if (mediaMaster == null || mediaMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NEWSPAPER_CODE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getLanguage() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_LANGUAGE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getColorType() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_COLOR_TYPE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getDuration() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_DURATION).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getTotalSize() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TOTAL_SIZE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getPageNumber() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_PAGE_NUMBER_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfDays() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_DAYS_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfInsertion() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_INSERTION_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getRatePerSpot() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_RATE_PER_SPOT).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getTotalAmount() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TOTAL_EMPTY).getCode());
				return true;
			}
		}
		
		if(theaterDetailsFlag==true) {
			if (mediaMaster == null || mediaMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_THEATER_CODE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getLanguage() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_LANGUAGE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getDuration() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_DURATION).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getFromDate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_FROM_DATE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getToDate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TO_DATE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfDays() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_DAYS_EMPTY).getCode());
				return true;
			}
			
			if (advertisement == null || advertisement.getNumberOfWeeks() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_WEEKS_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfShows() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_SHOWS_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfSpots() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_SPOTS_PER_DAY_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getNumberOfSpotsPerShow() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_NUMBER_OF_SPOTS_PER_SHOWS_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getRatePerSpot() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_RATE_PER_SPOT).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getTotalAmount() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_TOTAL_EMPTY).getCode());
				return true;
			}
		}
		
		if(smsDetailsFlag==true) {
			if (	mediaMaster == null || mediaMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_SMS_CODE_EMPTY).getCode());
				return true;
			}
			if (selectedCustomerTypeMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_CUSTOMER_TYPE_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getCustomer() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_CUSTOMER_EMPTY).getCode());
				return true;
			}
		}
		
		if(bannerDetailsFlag==true) {
			if (	mediaMaster == null || mediaMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_BANNER_NAME_EMPTY).getCode());
				return true;
			}
			if (selectedSupplierTypeMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_SUPPLIER_TYPE_EMPTY).getCode());
				return true;
			}
			if (supplierMaster == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_SUPPLIER_CODE_NAME_EMPTY).getCode());
				return true;
			}
			
			if (advertisement == null || advertisement.getLength() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_BANNER_LENGTH_EMPTY).getCode());
				return true;
			}
			if (advertisement == null || advertisement.getWidth() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_BANNER_WIDTH_EMPTY).getCode());
				return true;
			}
		}
		return false;
	}
	
	
	public String createAdvertisement() {
		log.info("<<==================  AdvertisementBean ----  createAdvertisement    =======##STARTS");

		try {
			
			if (action.equals("ADD")) {
				
				/*if (selectedAdvertisementType == null || selectedAdvertisementType.getId() == null) {
					errorMap.notify(ErrorDescription.ADVERTISEMENT_TYPE_REQUIRED.getErrorCode());
					return null;
				}
				if (selectedAdvertisementCategory == null || selectedAdvertisementCategory.getId() == null) {
					errorMap.notify(ErrorDescription.ADVERTISEMENT_CATEGORY_REQUIRED.getErrorCode());
					return null;
				}*/
				/*if (mediaMaster == null || mediaMaster.getId() == null) {
					errorMap.notify(ErrorDescription.ADVERTISEMENT_MEDIA_NAME_REQUIRED.getErrorCode());
					return null;
				}
				*/
				/*if(creatValidation()) {
					log.info(":: Validation return true ::");
					return null;
				}*/
				advertisement.setMediaMaster(mediaMaster);
				advertisement.setAdvertisementCategory(selectedAdvertisementCategory);
				advertisement.setAdvertisementType(selectedAdvertisementType);
				//advertisement.setActiveStatus(true);
				if(supplierMaster!=null) {
				advertisement.setSupplierMaster(supplierMaster);
				}
			}
			/*if (forwardTo == null || forwardTo.getId() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}
			if (approveStatus == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}*/
			if (note.isEmpty()) {
				log.info("<<=======  note null ====>>>");
				errorMap.notify(ErrorDescription.ADVERTISEMENT_NOTE_REQUIRED.getErrorCode());
				return null;
			}

			AdvertisementDTO dto = new AdvertisementDTO();
			if (advertisementList != null && advertisementList.size() != 0) {
				dto.setAdvertisementList(advertisementList);
			}
			dto.setAdvertisement(advertisement);
			dto.setForwardTo(forwardTo);
			dto.setApproveStatus(approveStatus);
			dto.setNote(note);
			log.info("<<================  advertisement :: " + advertisement);
			String url = "";
			if (action.equals("ADD"))
				url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/create";
			else
				url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/update";
			log.info("<<======  URL :: " + url);
			baseDTO = httpService.post(url, dto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				if (action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.ADVERTISEMENT_CREATE_SUCCESS.getErrorCode());
				else
					errorMap.notify(ErrorDescription.ADVERTISEMENT_UPDATE_SUCCESS.getErrorCode());
			} else {
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("<<==========  ERROR ::: " + e);
		}
		log.info("<<==================  AdvertisementBean ----  createAdvertisement    =======##ENDS");
		return LIST_PAGE;
	}

	public void list() {
		lazyList = new LazyDataModel<AdvertisementSearchResponseDTO>() {
			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<AdvertisementSearchResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				List<AdvertisementSearchResponseDTO> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<AdvertisementSearchResponseDTO>>() {
								});
					}
					this.setRowCount(baseDTO.getTotalRecords());
					totalRecords = baseDTO.getTotalRecords();
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(AdvertisementSearchResponseDTO res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public AdvertisementSearchResponseDTO getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<AdvertisementSearchResponseDTO> responseList = (List<AdvertisementSearchResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (AdvertisementSearchResponseDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		responseDto = new AdvertisementSearchResponseDTO();

		BaseDTO baseDTO = new BaseDTO();

		AdvertisementSearchRequestDTO request = new AdvertisementSearchRequestDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("advertisementCategory")) {
				request.setAdvertisementCategory(value);
			} else if (key.equals("advertisementType")) {
				request.setAdvertisementType(value);
			} else if (key.equals("mediaCode")) {
				request.setMediaCode(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equalsIgnoreCase("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}else if (key.equals("stage")) {
				request.setStage(value);
			}
		}

		try {

			String url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/search";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Advertisement Master onRowSelect method started");
		responseDto = ((AdvertisementSearchResponseDTO) event.getObject());
	}

	public void loadForwardUser() {
		log.info("<<===============    AdvertisementBean ---   doForward  =====##STARTS");
		forwardToList =commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ADVERTISEMENT.toString());
		log.info("<<===============    AdvertisementBean ---   doForward  =====##ENDS");
	}

	public void clearNote() {
		log.info("<<=====  Clear Note ::");
		note = "";
	}

	public Boolean validateEdit() {
		log.info("<<========= AdvertisementBean -----  validateEdit  ======##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/validate/edit/"
					+ responseDto.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				return false;
			}

		} catch (Exception e) {
			log.error("<<=======  ERROR :::: " + e);
		}
		log.info("<<========= AdvertisementBean -----  validateEdit  ======##ENDS");
		return true;
	}

	public void getAdvertisementInfo() {
		log.info("<<========= AdvertisementBean -----  getAdvertisementInfo  ======##STARTS");
		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/get/" + responseDto.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				advertisement = mapper.readValue(jsonResponse, Advertisement.class);
			}
			url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/getLogAndNote/"
					+ responseDto.getId();
			log.info("<<======  URL  :: " + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				AdvertisementDTO advertisementDto = mapper.readValue(jsonResponse, AdvertisementDTO.class);
				forwardTo = advertisementDto.getForwardTo();
				approveStatus = advertisementDto.getApproveStatus();
				note = advertisementDto.getNote();
				previousStatus=approveStatus;
			}
			if(forwardTo.getId().equals(loginBean.getUserMaster().getId())) {
				approveButtonFlag=true;
				rejectButtonFlag=true;
				
			}
			if(responseDto.getStage().equals("APPROVED") && previousStatus || forwardTo.getId().equals(loginBean.getUserMaster().getId()) && approveStatus) {
				finalButtonFlag=true;
			}else {
				finalButtonFlag=false;
			}

		} catch (Exception e) {
			log.error("<<=====  ERROR::: " + e);
		}
		if (advertisement != null && advertisement.getMediaMaster() != null
				&& advertisement.getMediaMaster().getMediaType() != null)
			selectPanel(advertisement.getMediaMaster().getMediaType().getCode());
		if (advertisement != null && advertisement.getMediaMaster() != null
				&& advertisement.getMediaMaster().getAddressMaster() != null)
			address = addressMasterBean.prepareAddress(advertisement.getMediaMaster().getAddressMaster());
		log.info("<<========= AdvertisementBean -----  getAdvertisementInfo  ======##ENDS");
	}

	public void selectPanel(String code) {
		log.info("<<==== MEDIA TYPE CODE:: " + code);
		code=code.trim();
		switch (code) {
		case "THEATER":
			theaterDetailsFlag = true;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			bannerDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;
		case "NEWSPAPER":
			newsPaperDetailsFlag = true;
			theaterDetailsFlag = false;
			radioDetailsFlag = false;
			bannerDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;
		case "RADIO":
			radioDetailsFlag = true;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			bannerDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;
		case "BANNER":
			bannerDetailsFlag = true;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;
		case "TELEVISION":
			bannerDetailsFlag = false;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			televisionDetailsFlag = true;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;

		case "BULK SMS":
			bannerDetailsFlag = false;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = true;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=false;
			break;
		case "MAGAZINE":
			bannerDetailsFlag = false;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=true;
			socialMediaDetailsFlag=false;
			break;
		case "SOCIAL MEDIA":
			bannerDetailsFlag = false;
			theaterDetailsFlag = false;
			newsPaperDetailsFlag = false;
			radioDetailsFlag = false;
			televisionDetailsFlag = false;
			smsDetailsFlag = false;
			magazineDetailsFlag=false;
			socialMediaDetailsFlag=true;
			break;


		}
	}

	public String deleteAdvertisement() {
		log.info(" deleteAdvertisement called");
		try {
			action = "Delete";
			if (responseDto.getId() == null || responseDto == null) {
				log.info("Please Select Advertisement");
				AppUtil.addWarn("Please Select Advertisement");
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAdvertisementDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteAdvertisementConfrom() {
		log.info("<===== Starts AdvertisementBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(
					serverURL + appPreference.getOperationApiUrl() + "/advertisement/delete/" + responseDto.getId());
			if (response != null && response.getStatusCode() == 0) {
				log.info("Advertisement Deleted Successfuly");
				AppUtil.addInfo("Advertisement Deleted Successfuly");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAdvertisementDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Advertisement ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends AdvertisementBean.deleteConfirm ===========>");
		return showListPage();
	}

	public void calculatNumberOfdays() {
		log.info("calculatNumberOfdays called");
		if (advertisement.getToDate() != null && advertisement.getFromDate() != null) {
			int days = calculateWorkDays(advertisement.getFromDate(), advertisement.getToDate());
			advertisement.setNumberOfDays((long) days);
		}

	}

	public static int calculateWorkDays(Date startDate, Date endDate) {
		Calendar startCal = Calendar.getInstance();
		startCal.setTime(startDate);
		Calendar endCal = Calendar.getInstance();
		endCal.setTime(endDate);
		int workDays = 0;
		if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
			startCal.setTime(endDate);
			endCal.setTime(startDate);
		}
		do {
			startCal.add(Calendar.DAY_OF_MONTH, 1);
			workDays++;
		} while (startCal.getTimeInMillis() <= endCal.getTimeInMillis());

		return workDays;
	}

	public void addRadioList() {
		log.info("addRadioList method called");
		advertisement.setMediaMaster(selectedMediaMaster);
		advertisement.setAdvertisementCategory(getSelectedAdvertisementCategory());
		advertisement.setAdvertisementType(getSelectedAdvertisementType());
		advertisement.setActiveStatus(true);
		advertisementList.add(advertisement);
		log.info("advertisementList====size===>" + advertisementList.size());
		advertisement = new Advertisement();
	}

	public void getAllCustomerType() {
		log.info("getAllCustomerType method called");
		customerTypeMasterList = commonDataService.loadAllCustomerType();
	}
	
	
	public void getAllCustomerByCustomerType() {
		log.info("getAllCustomerType method called");
		if(selectedCustomerTypeMaster!=null && selectedCustomerTypeMaster.getId()!=null) {
			Long customerTypeId=selectedCustomerTypeMaster.getId();
			customerMasterList = commonDataService.loadAllCustomerByCustomerType(customerTypeId);
		}
		
	}
	
	
	public String approvAdvertisement() {
		log.info("=====>Start approvAdvertisement Method<=============");
		try {
			BaseDTO baseDTO=new BaseDTO();
			AdvertisementDTO advertisementDTO = new AdvertisementDTO();
			advertisementDTO.setAdvertisement(advertisement);
			advertisementDTO.setForwardTo(forwardTo);
			advertisementDTO.setApproveStatus(approveStatus);
			advertisementDTO.setRemarks(remark);
			advertisementDTO.setNote(note);
			if(!finalButtonFlag) {
		    advertisementDTO.setStage(ApprovalStatus.APPROVED.toString());
			}else {
				 advertisementDTO.setStage(ApprovalStatus.FINAL_APPROVED.toString());
			}
			String url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/approve";
			baseDTO = httpService.post(url, advertisementDTO);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_APPROVED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			}
			
		}
		catch(Exception ex) {
			log.error("Error in approvAdvertisement Method",ex);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		log.info("=====>End approvAdvertisement Method<=============");
		return null;
	}
	
	public String rejectAdvertisement() {
	log.info("=====>Start rejectAdvertisement Method<=============");
	try {
		AdvertisementDTO advertisementDTO = new AdvertisementDTO();
		advertisementDTO.setAdvertisement(advertisement);
		advertisementDTO.setRemarks(remark);
		advertisementDTO.setStage(ApprovalStatus.REJECTED.toString());
		String url = serverURL + appPreference.getOperationApiUrl() + "/advertisement/reject";
		baseDTO = httpService.post(url, advertisementDTO);
		if(baseDTO!=null && baseDTO.getStatusCode()==0) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADVERTISEMENT_REJECTED_SUCCESSFULLY).getCode());
			return LIST_PAGE;
		}
			
	}
	catch(Exception ex) {
		log.error("Error in rejectAdvertisement Method",ex);
		baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
	}
	log.info("=====>End rejectAdvertisement Method<=============");
	return null;
	}
	
	public void download() {
		InputStream input = null;
		try {
			if(advertisement!=null && advertisement.getFileUpload()!=null) {
			File files = new File(advertisement.getFileUpload());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
			}else {
				errorMap.notify(ErrorDescription.FILE_PATH_NOT_FOUND.getErrorCode());
			}
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}
	}

}
