package in.gov.cooptex.masters.ui.bean;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


/**
 * 
 * @author Vimalsharma OASYS Cybernetics... 17-03-2018
 *
 */

@Log4j2
@Scope("session")
@Service("maritalStatusBean")
public class MaritalStatusBean {

	private final String CREATE_MARITAL_STATUS_PAGE = "/pages/masters/createMaritalStatus.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/listMaritalStatus.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/masters/viewMaritalStatus.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String MARITALSTATUS_URL = AppUtil.getPortalServerURL() + "/maritalStatus";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	MaritalStatus maritalStatus;
	
	@Getter
	@Setter
	MaritalStatus selectedMaritalStatus;

	@Getter
	@Setter
	LazyDataModel<MaritalStatus> lazyMaritalStatusList;

	List<MaritalStatus> MaritalStatusList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter
	@Setter
	Boolean statusButtonFlag = true;
	
	@Autowired
	LoginBean loginBean;

	public String showMaritalStatusListPage() {
		log.info("<==== Starts Marital Ststus Bean.showAccountCategoryListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		maritalStatus = new MaritalStatus();
		selectedMaritalStatus = new MaritalStatus();
		loadLazyMaritalStatusList();
		log.info("<==== Ends Marital Ststus Bean.showAccountCategoryListPage =====>");
		return INPUT_FORM_LIST_URL;
	}

	public void loadLazyMaritalStatusList() {
		log.info("<===== Starts Marital Ststus Bean.loadLazyMaritalStstus List ======>");
		lazyMaritalStatusList = new LazyDataModel<MaritalStatus>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<MaritalStatus> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = MARITALSTATUS_URL + "/getmaritalist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						MaritalStatusList = mapper.readValue(jsonResponse,
								new TypeReference<List<MaritalStatus>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyMaritalStstus List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return MaritalStatusList;
			}

			@Override
			public Object getRowKey(MaritalStatus res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MaritalStatus getRowData(String rowKey) {
				try {
				for (MaritalStatus maritalStatus : MaritalStatusList) {
					if (maritalStatus.getId().equals(Long.valueOf(rowKey))) {
						selectedMaritalStatus = maritalStatus;
						return maritalStatus;
					}
				}				
				}
				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
			
		};
		log.info("<===== Ends Marital Ststus Bean.loadLazyMaritalStstus List ======>");
	}
	
	

	public String showMaritalStatusListPageAction() {
		log.info("<===== Starts Marital Status Bean.showMaritalStatusListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedMaritalStatus == null || selectedMaritalStatus.getId() == null)) {
				errorMap.notify(ErrorDescription.MARITALSTATUS_SELECT.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Martal Status called..");
				statusButtonFlag=false;
				maritalStatus = new MaritalStatus();
				return CREATE_MARITAL_STATUS_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit MaritalStatus called..");
				statusButtonFlag=true;
				String url = MARITALSTATUS_URL + "/getmaritalstatus/" + selectedMaritalStatus.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					maritalStatus = mapper.readValue(jsonResponse, new TypeReference<MaritalStatus>() {
					});
					return CREATE_MARITAL_STATUS_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Marital Status called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view Marital Status called..");
				String viewurl = MARITALSTATUS_URL + "/getmaritalstatus/" + selectedMaritalStatus.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					maritalStatus = mapper.readValue(jsonResponse, new TypeReference<MaritalStatus>() {
					});
					return INPUT_FORM_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showMaritalStatusListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends Marital Ststus Bean.showMaritalStatusListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {
		log.info("<==== Starts Marital Ststus Bean.saveOrUpdate =======>");
		try {
			
			if (maritalStatus.getName() == null || maritalStatus.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.MARITALSTATUS__NAME_EMPTY.getCode());
				return null;
			}
			if (maritalStatus.getName().trim().length() < 2 || maritalStatus.getName().trim().length() > 100) {
				errorMap.notify(ErrorDescription.MARITALSTATUS_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			
			if (action.equalsIgnoreCase("CREATE")) {
				maritalStatus.setCreatedBy(loginBean.getUserDetailSession());
				maritalStatus.setCreatedDate(new Date());
			} else if (action.equalsIgnoreCase("EDIT")) {
				maritalStatus.setModifiedBy(loginBean.getUserDetailSession());
				maritalStatus.setModifiedDate(new Date());
				if (maritalStatus.getStatus() == null) {
					errorMap.notify(ErrorDescription.MARITALSTATUS_STATUS_EMPTY.getCode());
					return null;
				}
			}
			String url = MARITALSTATUS_URL + "/saveorupdate";
			BaseDTO response = httpService.put(url, maritalStatus);
			if (response != null && response.getStatusCode() == 0) {
				log.info("account category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					errorMap.notify(ErrorDescription.MARITALSTATUS_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.MARITALSTATUS_UPDATE_SUCCESS.getCode());
			} else if(response.getStatusCode().equals(ErrorDescription.CIRCLE_NAME_ALREADY_EXISTS.getErrorCode())) {
				AppUtil.addWarn("Name already exists");
				return null;
			}
			else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends Marital Status Bean.saveOrUpdate =======>");
		return showMaritalStatusListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Marital Status Bean.onRowSelect ========>" + event);
		selectedMaritalStatus = ((MaritalStatus) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends Marital Status Bean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts Marital Status Bean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(MARITALSTATUS_URL + "/delete/" + selectedMaritalStatus.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MARITALSTATUS_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete account category ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends Marital Status Bean.deleteConfirm ===========>");
		return showMaritalStatusListPage();
	}
}