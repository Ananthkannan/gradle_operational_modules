package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.RetailSocietyPlanDetailsDTO;
import in.gov.cooptex.operation.dto.RetailSocietySearchDTO;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.enums.RetailSocietyPlanStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.RetailSocietyPlanListDTO;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailSocietywisePlanBean")
@Scope("session")
public class RetailSocietyWisePlanBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	RetailSocietyPlanDetailsDTO retailSocietyPlanDetailsDTO;

	@Getter
	@Setter
	RetailSocietyPlanListDTO retailSocietyPlanListDTO;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	LazyDataModel<RetailSocietyPlanListDTO> retailSocietywisePlanLazyDTOList;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	String fromMonth;

	@Getter
	@Setter
	String toMonth;

	@Getter
	@Setter
	Long fromYear;

	@Getter
	@Setter
	Long toYear;

	@Getter
	@Setter
	List<String> statusList;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Setter
	@Getter
	ProductCategory productCategory = new ProductCategory();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	ProductGroupMaster productGroup = new ProductGroupMaster();

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList = new ArrayList<ProductGroupMaster>();

	@Getter
	@Setter
	ProductVarietyMaster productVarity = new ProductVarietyMaster();

	@Getter
	@Setter
	List<ProductVarietyMaster> productvarityList = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<String> dateIntervalList;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	List<RetailProductionPlan> retailProductionPlanList;

	@Getter
	@Setter
	Set<RetailProductionPlan> retailProductionPlanUniqueList;

	@Getter
	@Setter
	String planFromMonth = null;

	@Getter
	@Setter
	String planToMonth = null;

	@Getter
	@Setter
	Integer planFromYear = null;

	@Getter
	@Setter
	Integer planToYear = null;

	@Getter
	@Setter
	Long retailSocietyPlanId;

	@Getter
	@Setter
	StringBuilder regionValues = null;

	@Getter
	@Setter
	StringBuilder productCategoryValues = null;

	@Getter
	@Setter
	Date planCreatedDate;

	@Getter
	@Setter
	String planCreatedBy;

	@Getter
	@Setter
	Date planApprovedDate;

	@Getter
	@Setter
	String planApprovedBy;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Setter
	@Getter
	List<EntityMaster> dnpOfficeList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Autowired
	ErrorMap errorMap;

	private final String LIST_SOCIETY_WISE_PLAN_PAGE = "/pages/operation/retailproduction/plan/listSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String CREATE_SOCIETY_WISE_PLAN_PAGE = "/pages/operation/retailproduction/plan/createSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	private final String VIEW_SOCIETY_WISE_PLAN_PAGE = "/pages/operation/retailproduction/plan/viewSocietyWiseProductionPlan.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	Map<String, List<RetailSocietyPlanDetailsDTO>> societyPlanDetailsMap = new LinkedHashMap<String, List<RetailSocietyPlanDetailsDTO>>();

	@Getter
	@Setter
	List<RetailSocietyPlanDetailsDTO> societyList = new ArrayList<RetailSocietyPlanDetailsDTO>();

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMapForPrevious;

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMapForCurrent;

	@Getter
	@Setter
	private String currentPlanStatus;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	Map<String, List<ProductVarietyMaster>> productVarietyMap = new LinkedHashMap<String, List<ProductVarietyMaster>>();

	@Getter
	@Setter
	Map<String, List<ProductGroupMaster>> productGroupMap = new LinkedHashMap<String, List<ProductGroupMaster>>();

	@Setter
	@Getter
	String planfromMonthYear = "";

	@Setter
	@Getter
	String plantoMonthYear = "";

	@Setter
	@Getter
	List<SupplierMaster> societiesList;

	@Getter
	@Setter
	Set<String> planFromMonthYearList;

	@Getter
	@Setter
	Set<String> planToMonthYearList;

	@Getter
	@Setter
	String societyPlanFromMonthYear;

	@Getter
	@Setter
	String societyPlanToMonthYear;

	@Getter
	@Setter
	String productFlag;

	@Getter
	@Setter
	String growlFlag = null;
	
	@Getter
	@Setter
	String buttonNameFlag = null;

	public RetailSocietyWisePlanBean() {
		log.info("<--- RetailSocietyWisePlanBean() ---> ");
		loadValues();
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String showRetailSocietyPlanList() {
		log.info("<--- showRetailSocietyPlanList() ---> ");
		retailProductionPlan = new RetailProductionPlan();
		loadRetailSocietyPlanDTOLazy();
		loadStatusValues();

		return LIST_SOCIETY_WISE_PLAN_PAGE;
	}

	public void loadStatusValues() {
		RetailProductionPlanStatus[] statusValues = RetailProductionPlanStatus.class.getEnumConstants();
		statusList = new ArrayList<String>();
		for (RetailProductionPlanStatus status : statusValues) {
			statusList.add(status.toString());
		}
	}

	public void loadRetailSocietyPlanDTOLazy() {
		log.info("<--- loadRetailSocietyPlanDTOLazy() --->");
		addButtonFlag = false;
		retailSocietywisePlanLazyDTOList = new LazyDataModel<RetailSocietyPlanListDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailSocietyPlanListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<RetailSocietyPlanListDTO> data = new ArrayList<RetailSocietyPlanListDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<RetailSocietyPlanListDTO>>() {
								});

						if (data == null) {
							log.info("<--- No Records Found --->");
							errorMap.notify(baseDTO.getStatusCode());
						} else {
							this.setRowCount(baseDTO.getTotalRecords());
							planSize = baseDTO.getTotalRecords();
						}

					} else {
						log.info("<--- Empty List  --->");
						errorMap.notify(baseDTO.getStatusCode());
					}
				} catch (Exception e) {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					log.error("Error in loadRetailSocietyPlanDTOLazy() ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(RetailSocietyPlanListDTO res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public RetailSocietyPlanListDTO getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<RetailSocietyPlanListDTO> responseList = (List<RetailSocietyPlanListDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RetailSocietyPlanListDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside getSearchData called---> ");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		retailSocietyPlanListDTO = new RetailSocietyPlanListDTO();
		BaseDTO baseDTO = new BaseDTO();

		RetailSocietySearchDTO request = new RetailSocietySearchDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			if (key.equals("planCodeorName")) {
				request.setPlanCodeorName(value);
			} else if (key.equals("groupCodeorName")) {
				request.setGroupCodeorName(value);
			} else if (key.equals("varityCodeorName")) {
				request.setVarityCodeorName(value);
			} else if (key.equals("dnpofficeCodeorName")) {
				request.setDnpofficeCodeorName(value);
			} else if (key.equals("societyPlanFrom")) {
				request.setSocietyPlanFrom(value);
			} else if (key.equals("societyPlanTo")) {
				request.setSocietyPlanTo(value);
			} else if (key.equals("status")) {
				request.setStatus(value);
			}
		}

		try {
			String url = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/search";
			baseDTO = httpService.post(url, request);
			if (baseDTO != null) {
				log.info("<--- Successfully fetched data from DB --->");
				baseDTO.setStatusCode(ErrorDescription.SUCCESS_RESPONSE.getCode());
				return baseDTO;
			} else {
				log.info("<--- No Records Found --->");
				errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			}
		} catch (Exception e) {
			log.error("Exception in getSearchData ", e);
			baseDTO.setStatusCode(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return baseDTO;

	}

	public String createSocietywiseProductionPlan() {
		log.info("<--- createSocietywiseProductionPlan() --->");

		growlFlag = null;

		retailProductionPlan = new RetailProductionPlan();
		loadRetailProductionPlanApprovedPlanList();
		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();
		
		//entityMaster = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();
		Long userId = loginBean.getUserDetailSession().getId();
		log.info("Login userId : " + userId);
		entityMaster = commonDataService.getEntityInfoByLoggedInUser(serverURL,userId);
		
		dnpOfficeList = new ArrayList<>();
		productCategoryList = new ArrayList<>();
		productGroupList = new ArrayList<>();
		productvarityList = new ArrayList<>();

		planFromMonthYearList = new LinkedHashSet<>();
		planToMonthYearList = new LinkedHashSet<>();

		societyPlanFromMonthYear = null;
		societyPlanToMonthYear = null;

		planCreatedDate = null;
		planCreatedBy = null;
		planApprovedDate = null;
		planApprovedBy = null;

		societyPlanDetailsMap = new LinkedHashMap<>();
		totalFinalValueMapForPrevious = new LinkedHashMap<>();
		totalFinalValueMapForCurrent = new LinkedHashMap<>();

		societyList = new ArrayList<>();

		return CREATE_SOCIETY_WISE_PLAN_PAGE;
	}

	@SuppressWarnings("unchecked")
	public void loadRetailProductionPlanApprovedPlanList() {
		log.info("<--- Inside loadRetailProductionPlanFinalApprovedList() ---> ");

		String requestPath = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/societyPlan/getretailproductionplanlist";

		try {
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailProductionPlanList = (List<RetailProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<RetailProductionPlan>>() {
							});

					retailProductionPlanUniqueList = new LinkedHashSet<>(retailProductionPlanList);

					if (retailProductionPlanList != null) {
						log.info("RetailProductionPlanList with Size of : " + retailProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" RetailProductionPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadRetailProductionPlanFinalApprovedList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadRetailProductionPlanFinalApprovedList() --->", e);
		}
	}

	public void onChangeProductionPlan() {
		log.info("<--- Inside onChangePropductionPlan() ---> ");

		dnpOfficeList = new ArrayList<>();
//		entityMaster = null;
		productCategoryList = new ArrayList<>();
		productGroupList = new ArrayList<>();
		productvarityList = new ArrayList<>();
		planFromMonthYearList = new LinkedHashSet<>();
		planToMonthYearList = new LinkedHashSet<>();

		if (retailProductionPlan == null) {
			log.info("<--- if selected nothing clear all fields. Do this --->");
			retailProductionPlan = new RetailProductionPlan();
			planFromMonth = null;
			planFromYear = null;
			planToMonth = null;
			planToYear = null;
			regionValues = new StringBuilder();
			productCategoryValues = new StringBuilder();

			societyPlanFromMonthYear = null;
			societyPlanToMonthYear = null;

			planCreatedDate = null;
			planCreatedBy = null;
			planApprovedDate = null;
			planApprovedBy = null;

			return;
		}

		Long planId = retailProductionPlan.getId();

		getRegionAndCategoryDetailsFromPlan(planId);

		getListOfDnpOfficesForPlan(planId);
		
		if(entityMaster!=null) {
			onchangeDnpOffice();
		}
		
		

	}

	private void processSocietyPlanFromDate(Date planFromDate, Date planToDate) {
		log.info("<--- Inside processSocietyPlanFromDate() planFromDate ---> " + planFromDate);
		log.info("<--- Inside processSocietyPlanFromDate() planToDate ---> " + planToDate);

		planFromMonthYearList = AppUtil.calculateMonthYear(planFromDate, 0, 1);
		log.info("<--- planFromMonthYearList ---> " + planFromMonthYearList);

		int mounthCount = AppUtil.monthCountBetweenTwoDates(planFromDate, planToDate);
		log.info("<--- mounthCount ---> " + mounthCount);

		planToMonthYearList = AppUtil.calculateMonthYear(planFromDate, 0, mounthCount + 1);
		log.info("<--- planToMonthYearList ---> " + planToMonthYearList);

	}
	

	private void getRegionAndCategoryDetailsFromPlan(Long planId) {
		log.info("<--- Inside getRegionandCategoryDetailsFromPlan() with plan ID---> " + planId);

		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/get/" + planId;
			log.info("<--- getRegionandCategoryDetailsFromPlan() URL ---> " + URL);

			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			retailProductionPlan = objectMapper.readValue(jsonResponse, RetailProductionPlan.class);

			if (retailProductionPlan == null || retailProductionPlan.getRetailProductionPlanRegionList() == null
					|| retailProductionPlan.getProductCategoryList() == null) {
				AppUtil.addError("Production Plan Details Not Found");
				return;
			}

			regionValues.setLength(0);
			log.info("<--- retailProductionPlan.getRetailProductionPlanRegionList() ---> "
					+ retailProductionPlan.getRetailProductionPlanRegionList());
			for (RetailProductionPlanRegion regionValue : retailProductionPlan.getRetailProductionPlanRegionList()) {
				if (regionValue != null && regionValue.getEntityMaster().getCode() != null
						&& regionValue.getEntityMaster().getName() != null)
					regionValues.append(regionValue.getEntityMaster().getCode()).append("/")
							.append(regionValue.getEntityMaster().getName()).append(",<br/>");
			}

			regionValues.deleteCharAt(regionValues.length() - 6);

			productCategoryValues.setLength(0);
			log.info("<--- retailProductionPlan.getProductCategoryList() ---> "
					+ retailProductionPlan.getProductCategoryList());
			for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
				if (categoryValue != null && categoryValue.getProductCatCode() != null
						&& categoryValue.getProductCatName() != null)
					productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
							.append(categoryValue.getProductCatName()).append(",<br/>");
			}

			productCategoryValues.deleteCharAt(productCategoryValues.length() - 6);

			planCreatedDate = retailProductionPlan.getCreatedDate();
			planCreatedBy = retailProductionPlan.getCreatedByName();
			if(retailProductionPlan.getDate()!=null) {
			planApprovedDate =AppUtil.getFormattedGivenDateTime(retailProductionPlan.getDate());
			}
			planApprovedBy = retailProductionPlan.getApprovedByEmployeeName();

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception at getRegionAndCategoryDetailsFromPlan() ---> ", e);
		}
	}

	private void getListOfDnpOfficesForPlan(Long planId) {
		log.info("<--- Inside loadDAndpOfficeList() ---> ");
		dnpOfficeList = commonDataService.loadDAndpOffices(planId);

		if (dnpOfficeList != null) {
			log.info("<--- dnpOfficeList size() ---> " + dnpOfficeList.size());
		} else {
			errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
			log.info("<--- dnpOfficeList is Empty ---> ");
		}

	}

	public void onchangeDnpOffice() {
		log.info("<--- Inside onchangeDnpOffices() ---> ");

		productCategoryList = new ArrayList<>();
		productCategory = null;
		productGroupList = new ArrayList<>();
		productvarityList = new ArrayList<>();

		if (entityMaster != null) {
			Long productionPlanId = retailProductionPlan.getId();
			Long dnpOfficeId = entityMaster.getId();

			getListOfDnpProducts(productionPlanId, dnpOfficeId);
		}
	}

	private void getListOfDnpProducts(Long productionPlanId, Long dnoOfficeId) {
		log.info("<--- Inside getListOfDnpProducts() --->");
		retailSocietyPlanListDTO = new RetailSocietyPlanListDTO();
		String requestPath = serverURL + appPreference.getOperationApiUrl() + "/dnp/getallvarities/" + productionPlanId
				+ "/" + dnoOfficeId;

		log.info(" getvarityMasters() request url" + requestPath);

		try {
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailSocietyPlanListDTO = (RetailSocietyPlanListDTO) mapper.readValue(jsonValue,
							RetailSocietyPlanListDTO.class);

					if (retailSocietyPlanListDTO != null) {
						productCategoryList = retailSocietyPlanListDTO.getProductCategoryList();

						productGroupMap = retailSocietyPlanListDTO.getProductGroupMap();

						productVarietyMap = retailSocietyPlanListDTO.getProductVarietyMap();
					} else {
						log.info("Retail Societywise plan Objcect is null");
						errorMap.notify(baseDTO.getStatusCode());
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					errorMap.notify(baseDTO.getStatusCode());
					log.info("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Service not available --->");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Error in getListOfDnpProducts() --->", e);
		}

	}

	public void onchangeCategory() {
		log.info("<--- Inside onchangeCategory()---> ");

		productGroupList = new ArrayList<>();
		productGroup = null;

		productvarityList = new ArrayList<>();

		if (productCategory != null) {
			String categoryKey = productCategory.getProductCatCode();
			if (productGroupMap.containsKey(categoryKey)) {
				productGroupList = productGroupMap.get(categoryKey);
				if (productGroupList != null && productGroupList.size() > 0) {
					log.info("ProductGroupList List " + productGroupList.size());
				} else {
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
					log.info("ProductGroupList List is Empty ");
				}

			}
		}
	}

	public void onchangeGroup() {
		log.info("<--- Inside onchangeGroup()---> ");

		productvarityList = new ArrayList<>();
		productVarity = null;

		if (productGroup != null) {
			String groupCode = productGroup.getCode();
			if (productVarietyMap.containsKey(groupCode)) {
				productvarityList = productVarietyMap.get(groupCode);

				if (productvarityList != null && productvarityList.size() > 0) {
					log.info("ProductvarityList List " + productvarityList.size());
				} else {
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
					log.info("ProductvarityList List is Empty ");
				}
			}
		}
	}

	public void onchangeVariety() {
		log.info("<--- Inside onchangeVariety()---> ");
		planFromMonthYearList = new LinkedHashSet<>();
		planToMonthYearList = new LinkedHashSet<>();

		if (productVarity == null) {
			societyPlanFromMonthYear = null;
			societyPlanToMonthYear = null;

			return;
		}

		if (productVarity != null) {
			log.info(retailProductionPlan.getId() + ":::" + entityMaster.getId() + ":::" + productVarity.getId());
		}

		String checkSocietyPlanFromAndToDatesPath = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/societyPlan/checksocietyplanfromandtodate";
		RetailSocietyPlanDetailsDTO requset = new RetailSocietyPlanDetailsDTO();
		requset.setProductionPlanId(retailProductionPlan.getId());
		requset.setDnpOfficeId(entityMaster.getId());
		requset.setProductId(productVarity.getId());

		try {

			BaseDTO baseDTO = httpService.post(checkSocietyPlanFromAndToDatesPath, requset);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailSocietyPlanDetailsDTO = mapper.readValue(jsonValue, RetailSocietyPlanDetailsDTO.class);
					if (retailSocietyPlanDetailsDTO != null) {

						String forFromDate = 1 + "-" + retailSocietyPlanDetailsDTO.getToMonth() + "-"
								+ retailSocietyPlanDetailsDTO.getToYear();

						Date from = new SimpleDateFormat("dd-MMM-yyyy").parse(forFromDate);
						Date to = retailProductionPlan.getPlanTo();

						int diffMonth = AppUtil.monthCountBetweenTwoDates(from, to);
						log.info("<--- diffMonth ---> " + diffMonth);

						if (diffMonth == 0) {
							AppUtil.addError("Societywise Plan is completed for Selected Product ");
							return;
						}

						planFromMonthYearList = AppUtil.calculateMonthYear(from, 1, 1);

						planToMonthYearList = AppUtil.calculateMonthYear(from, 1, diffMonth);

					} else {
						String msg = baseDTO.getErrorDescription();
						errorMap.notify(baseDTO.getStatusCode());
						log.info("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					}
				} else {
					processSocietyPlanFromDate(retailProductionPlan.getPlanFrom(), retailProductionPlan.getPlanTo());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Service not available --->");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Error in getvarityMasters() --->", e);
		}
	}

	public void generateSocietywiseproductionPlan() {
		log.info("<--- Inside generateSocietywiseproductionPlan() ---> ");

		Long productionPlanID = retailProductionPlan.getId();
		Long dnpOfficeId = entityMaster.getId();
		Long productVarietyId = productVarity.getId();
		String productCategoryCode = productCategory.getProductCatCode();

		RetailSocietyPlanDetailsDTO request = new RetailSocietyPlanDetailsDTO();
		request.setProductionPlanId(productionPlanID);
		request.setDnpOfficeId(dnpOfficeId);
		request.setProductId(productVarietyId);
		request.setProductCategoryCode(productCategoryCode);

		productFlag = productCategoryCode;

		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/generate";
		log.info("<--- generateSocietywiseproductionPlan() URL ---> " + URL);

		try {

			String selectedSocietyPlanFrom = 1 + "-" + societyPlanFromMonthYear;
			String selectedSocietyPlanTo = 1 + "-" + societyPlanToMonthYear;
			Set<String> monthSet = new LinkedHashSet<String>();

			Date societyPlanFrom = new SimpleDateFormat("dd-MMM-yyyy").parse(selectedSocietyPlanFrom);
			Date societyPlanTo = new SimpleDateFormat("dd-MMM-yyyy").parse(selectedSocietyPlanTo);

			int diffMonth = AppUtil.monthCountBetweenTwoDates(societyPlanFrom, societyPlanTo);
			log.info("<--- diffMonth ---> " + diffMonth);

			monthSet = AppUtil.calculateMonthYear(societyPlanFrom, 0, diffMonth + 1);
			log.info("<--- requestedList ---> " + monthSet);

			List<String> monthYearList = new ArrayList<String>(monthSet);

			request.setMonthYearList(monthYearList);

			BaseDTO baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailSocietyPlanDetailsDTO = mapper.readValue(jsonResponse, RetailSocietyPlanDetailsDTO.class);

					if (retailSocietyPlanDetailsDTO == null
							|| retailSocietyPlanDetailsDTO.getSocietyPlanDetailsMap() == null) {
						log.info(" retailSocietyPlanDetailsDTO is null ");
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						return;
					}

					societyPlanDetailsMap = retailSocietyPlanDetailsDTO.getSocietyPlanDetailsMap();

					for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap
							.entrySet()) {
						societyList = entry.getValue();

						Double productwiseTotalQty = societyList.stream()
								.mapToDouble(RetailSocietyPlanDetailsDTO::getPreviousYearSupplied).sum()
								+ societyList.stream()
										.mapToDouble(RetailSocietyPlanDetailsDTO::getCurrentYearRequirement).sum();

						for (RetailSocietyPlanDetailsDTO finalDto : societyList) {
							finalDto.setSocietywiseTotalQty(productwiseTotalQty);
						}
					}

					prepareMapForFooterTotalValueForPreviousYearSupplied(societyPlanDetailsMap);

					prepareMapForFooterTotalValueForCurrentYear(societyPlanDetailsMap);

				} else {
					log.info("<--- Empty Response ---> ");
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				log.info("<--- Internal Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("<--- Error in generateSocietywiseproductionPlan List --->", e);
		}
	}

	public String cancel() {
		log.info("<--- Inside cancel() ---> ");
		showRetailSocietyPlanList();

		return LIST_SOCIETY_WISE_PLAN_PAGE;
	}

	public String viewSocietyProcurementPlan() {
		log.info("<--- Inside viewSocietyProcuremnetPlan() action---> " + action);

		if (retailSocietyPlanListDTO == null) {
			log.info("<---Please Select any one Plan--->");
			Util.addWarn("Please Select any one Plan");
			return null;
		}
		Long societywisePlanId = retailSocietyPlanListDTO.getId();
		log.info("<--- viewSocietyProcuremnetPlan() societywise plan Id---> " + societywisePlanId);

		getRegionAndCategoryDetailsFromPlan(retailSocietyPlanListDTO.getRetailProductionPlanId());

		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/get/" + societywisePlanId;
		log.info("<--- viewSocietyProcuremnetPlan() URL ---> " + URL);

		try {

			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				retailSocietyPlanDetailsDTO = objectMapper.readValue(jsonResponse, RetailSocietyPlanDetailsDTO.class);

				productFlag = retailSocietyPlanDetailsDTO.getProductCategory().getProductCatCode();

				if (retailSocietyPlanDetailsDTO == null
						|| retailSocietyPlanDetailsDTO.getSocietyPlanDetailsMap() == null) {
					log.info(" retailSocietyPlanDetailsDTO is null ");
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
					return null;
				}

				societyPlanDetailsMap = retailSocietyPlanDetailsDTO.getSocietyPlanDetailsMap();

				for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap.entrySet()) {
					societyList = entry.getValue();

					if (productFlag.equalsIgnoreCase("A") || productFlag.equalsIgnoreCase("AJ")) {
						Double productwiseTotalValue = societyList.stream().filter(R -> R.getProductQuantity() != null)
								.mapToDouble(RetailSocietyPlanDetailsDTO::getProductValue).sum();

						for (RetailSocietyPlanDetailsDTO finalDto : societyList) {
							finalDto.setSocietywiseTotalValue(productwiseTotalValue);
						}
					} else {
						log.info("societyList size==> " + societyList.size());
						Double productwiseTotalQty = societyList.stream().filter(R -> R.getProductQuantity() != null)
								.mapToDouble(RetailSocietyPlanDetailsDTO::getProductQuantity).sum();

						for (RetailSocietyPlanDetailsDTO finalDto : societyList) {
							finalDto.setSocietywiseTotalQty(productwiseTotalQty);
						}
					}

				}

				/*
				 * To set the footer total value
				 */
				prepareMapForFooterTotalValueForPreviousYearSupplied(societyPlanDetailsMap);

				prepareMapForFooterTotalValueForCurrentYear(societyPlanDetailsMap);

			} else {
				log.info("<--- Response Not Found ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Error in view Societywise ProcuremnetPlan  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return VIEW_SOCIETY_WISE_PLAN_PAGE;
	}

	public String editSocietyProcurementPlan() {
		log.info("<--- Inside viewSocietyProcuremnetPlan() action---> " + action);

		growlFlag = null;

		if (retailSocietyPlanListDTO == null) {
			log.info("<---Please Select any one Plan--->");
			Util.addWarn("Please Select any one Plan");
			return null;
		}

		if (retailSocietyPlanListDTO.getStatus().equals(RetailSocietyPlanStatus.SUBMITTED)
				|| retailSocietyPlanListDTO.getStatus().equals(RetailSocietyPlanStatus.APPROVED)) {
			errorMap.notify(ErrorDescription.CANNOT_EDIT_PLAN.getCode());
			return null;
		}
		Long societywisePlanId = retailSocietyPlanListDTO.getId();
		retailSocietyPlanId = retailSocietyPlanListDTO.getId();
		log.info("<--- viewSocietyProcuremnetPlan() societywise plan Id---> " + societywisePlanId);

		getRegionAndCategoryDetailsFromPlan(retailSocietyPlanListDTO.getRetailProductionPlanId());

		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/get/" + societywisePlanId;
		log.info("<--- viewSocietyProcuremnetPlan() URL ---> " + URL);

		try {

			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				retailSocietyPlanDetailsDTO = objectMapper.readValue(jsonResponse, RetailSocietyPlanDetailsDTO.class);

				log.info("<--- retailSocietyPlanDetailsDTO---> " + retailSocietyPlanDetailsDTO.getId());

				loadRetailProductionPlanApprovedPlanList();

				retailProductionPlan = retailSocietyPlanDetailsDTO.getRetailProductionPlan();

				getListOfDnpOfficesForPlan(retailProductionPlan.getId());

				entityMaster = retailSocietyPlanDetailsDTO.getDanpOffice();

				getListOfDnpProducts(retailProductionPlan.getId(), entityMaster.getId());

				productCategory = retailSocietyPlanDetailsDTO.getProductCategory();

				productFlag = productCategory.getProductCatCode();

				onchangeCategory();

				productGroup = retailSocietyPlanDetailsDTO.getProductGroupMaster();

				onchangeGroup();

				productVarity = retailSocietyPlanDetailsDTO.getProductVarietyMaster();

				fromMonth = retailSocietyPlanDetailsDTO.getFromMonth();

				fromYear = retailSocietyPlanDetailsDTO.getFromYear();

				toMonth = retailSocietyPlanDetailsDTO.getToMonth();

				toYear = retailSocietyPlanDetailsDTO.getToYear();

				if (retailSocietyPlanDetailsDTO != null) {
					societyPlanDetailsMap = retailSocietyPlanDetailsDTO.getSocietyPlanDetailsMap();

					for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap
							.entrySet()) {
						societyList = entry.getValue();

						if (productFlag.equalsIgnoreCase("A") || productFlag.equalsIgnoreCase("AJ")) {
							Double productwiseTotalValue = societyList.stream()
									.mapToDouble(RetailSocietyPlanDetailsDTO::getProductValue).sum();

							for (RetailSocietyPlanDetailsDTO finalDto : societyList) {
								finalDto.setSocietywiseTotalValue(productwiseTotalValue);
							}
						} else {
							Double productwiseTotalQty = societyList.stream()
									.mapToDouble(RetailSocietyPlanDetailsDTO::getProductQuantity).sum();

							for (RetailSocietyPlanDetailsDTO finalDto : societyList) {
								finalDto.setSocietywiseTotalQty(productwiseTotalQty);
							}
						}
					}

					/*
					 * To set the footer total value
					 */
					prepareMapForFooterTotalValueForPreviousYearSupplied(societyPlanDetailsMap);

					prepareMapForFooterTotalValueForCurrentYear(societyPlanDetailsMap);

				}

			} else {
				log.info("<--- Response Not Found ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Error in view Societywise ProcuremnetPlan  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return CREATE_SOCIETY_WISE_PLAN_PAGE;
	}

	private void prepareMapForFooterTotalValueForPreviousYearSupplied(
			Map<String, List<RetailSocietyPlanDetailsDTO>> societyPlanDetailsMap) {

		List<RetailSocietyPlanDetailsDTO> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap.entrySet()) {
			for (RetailSocietyPlanDetailsDTO dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalFinalValueMapForPrevious = totalListFromMap.stream()
				.collect(Collectors.groupingBy(RetailSocietyPlanDetailsDTO::getMonthValue,
						Collectors.summingDouble(RetailSocietyPlanDetailsDTO::getPreviousYearSupplied)));

	}

	private String prepareMapForFooterTotalValueForCurrentYear(
			Map<String, List<RetailSocietyPlanDetailsDTO>> societyPlanDetailsMap) {

		List<RetailSocietyPlanDetailsDTO> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap.entrySet()) {
			for (RetailSocietyPlanDetailsDTO dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}
		totalFinalValueMapForCurrent = totalListFromMap.stream().filter(x->x.getCurrentYearRequirement()!=null)
				.collect(Collectors.groupingBy(RetailSocietyPlanDetailsDTO::getMonthValue,
						Collectors.summingDouble(RetailSocietyPlanDetailsDTO::getCurrentYearRequirement)));

		Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry = societyPlanDetailsMap.entrySet().iterator().next();
		for (RetailSocietyPlanDetailsDTO dto : entry.getValue()) {

			log.info("<--- totalFinalValueMapForCurrent.containsKey(dto.getMonthValue() ---> "
					+ totalFinalValueMapForCurrent.get(dto.getMonthValue()));
			log.info("<--- dto.getDnpMonthwiseTotalQty() ---> " + dto.getDnpMonthwiseTotalQty());

			String qtyValue = productFlag.equalsIgnoreCase("A") || productFlag.equalsIgnoreCase("AJ") ? "Value"
					: "Quantity";

			Double dnpMonthwiseDataBasedOnProductCategorySelection = productFlag.equalsIgnoreCase("A")
					|| productFlag.equalsIgnoreCase("AJ") ? dto.getDnpMonthwiseTotalValue()
							: dto.getDnpMonthwiseTotalQty();

			if (growlFlag != null && "GROWL_DISPLAY".equalsIgnoreCase(growlFlag)
					&& totalFinalValueMapForCurrent.containsKey(dto.getMonthValue()) && !totalFinalValueMapForCurrent
							.get(dto.getMonthValue()).equals(dnpMonthwiseDataBasedOnProductCategorySelection) && "SUBMITTED".equalsIgnoreCase(buttonNameFlag)) {

				log.info("<--- Given Value Should not Exceed ---> ");
				AppUtil.addError("Given" + qtyValue + " should be equal to the Month Wise D&P Plan total " + qtyValue
						+ " : '" + dnpMonthwiseDataBasedOnProductCategorySelection + "' for the Month - " + "'"
						+ dto.getMonthValue() + "'");
				return null;
			}

		}
		return "success";
	}

	public String editSocietywise(String keyData, RetailSocietyPlanDetailsDTO societyPlanDetailsObject, int index) {
		log.info("<--- Inside keyData() ---> " + keyData);
		log.info("<--- Inside societyPlanDetailsObject() ---> " + societyPlanDetailsObject);
		log.info("<--- Inside index() ---> " + index);

		growlFlag = "GROWL_DISPLAY";

		String society = keyData;

		for (Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry : societyPlanDetailsMap.entrySet()) {
			if (entry.getKey().equalsIgnoreCase(society)) {

				Double updatedValue = (societyPlanDetailsObject.getCurrentYearRequirement() != null
						? societyPlanDetailsObject.getCurrentYearRequirement()
						: 0D);

				entry.getValue().get(index).setCurrentYearRequirement(updatedValue);
			}
			Double societywiseTotal = entry.getValue().stream()
					.mapToDouble(RetailSocietyPlanDetailsDTO::getPreviousYearSupplied).sum()
					+ entry.getValue().stream().mapToDouble(RetailSocietyPlanDetailsDTO::getCurrentYearRequirement)
							.sum();

			for (RetailSocietyPlanDetailsDTO finalDto : entry.getValue()) {
				finalDto.setSocietywiseTotalQty(societywiseTotal);
			}
		}

		/*
		 * To set the footer total value
		 */
		prepareMapForFooterTotalValueForPreviousYearSupplied(societyPlanDetailsMap);

		prepareMapForFooterTotalValueForCurrentYear(societyPlanDetailsMap);

		return "success";
	}

	public String submitSocietyProcurementPlan(String buttonName) {
		log.info("<--- Inside submitSocietyProcurementPlan() ---> " + buttonName);

		growlFlag = "GROWL_DISPLAY";
		buttonNameFlag=buttonName;
		RetailSocietyPlanDetailsDTO request = new RetailSocietyPlanDetailsDTO();

		/*
		 * for(Map.Entry<String, List<RetailSocietyPlanDetailsDTO>> entry :
		 * societyPlanDetailsMap.entrySet()){ for(RetailSocietyPlanDetailsDTO dto :
		 * entry.getValue()){ if(editSocietywise(entry.getKey(), dto,
		 * entry.getValue().indexOf(dto))==null){ return null; } } }
		 */
		 if(societyPlanDetailsMap.isEmpty()) {
	        	AppUtil.addWarn("Without generating the data,Plan cannot be submitted");
	        	return null;
	      }
		 
		if (buttonName.equalsIgnoreCase("SUBMITTED")
				&& prepareMapForFooterTotalValueForCurrentYear(societyPlanDetailsMap) == null) {
			buttonNameFlag=null;
			return null;
		}

		request.setSocietyPlanDetailsMap(societyPlanDetailsMap);
		request.setProductionPlanId(retailProductionPlan.getId());
		request.setDnpOfficeId(entityMaster.getId());
		request.setProductId(productVarity.getId());
		request.setButtonName(buttonName);
		request.setFromMonth(societyPlanFromMonthYear);
		request.setToMonth(societyPlanToMonthYear);
		request.setProductCategory(productCategory);

		try {

			if (action.equalsIgnoreCase("ADD")) {

				String createPath = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/create";
				log.info("<--- submitSocietyProcurementPlan() create URL ---> " + createPath);

				BaseDTO baseDTO = httpService.post(createPath, request);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					if (buttonName.equalsIgnoreCase("INITIATED")) {
						log.info("<--- Retail Society Procuremnet Plan Saved Successfully");
						errorMap.notify(
								ErrorDescription.INFO_RETAIL_SALES_SOCIETYWISE_PROCUREMENT_PLAN_SAVED_SUCCESSFULLY
										.getCode());
					} else if (buttonName.equalsIgnoreCase("SUBMITTED")) {
						log.info("<--- Retail Society Procuremnet Plan Submitted Successfully");
						errorMap.notify(
								ErrorDescription.INFO_RETAIL_SALES_SOCIETYWISE_PROCUREMENT_PLAN_SUBMITTED_SUCCESSFULLY
										.getCode());
					}
					showRetailSocietyPlanList();
				} else {
					String msg = baseDTO.getErrorDescription();
					errorMap.notify(baseDTO.getStatusCode());
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					return null;
				}

			} else if (action.equalsIgnoreCase("EDIT")) {

				log.info("updateSocietyPlan() started execution");
				if (buttonName.equals("INITIATED")) {
					request.setRetailSocietyPlanStatus(RetailSocietyPlanStatus.INITIATED);
				} else {
					request.setRetailSocietyPlanStatus(RetailSocietyPlanStatus.SUBMITTED);
				}
				request.setId(retailSocietyPlanId);
				request.setFromMonth(fromMonth);
				request.setFromYear(fromYear);
				request.setToMonth(toMonth);
				request.setToYear(toYear);
				BaseDTO baseDTO = new BaseDTO();
				String url = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/update";
				if (buttonName.equals("INITIATED")) {
					retailSocietyPlanDetailsDTO.setRetailSocietyPlanStatus(RetailSocietyPlanStatus.INITIATED);
				} else {
					retailSocietyPlanDetailsDTO.setRetailSocietyPlanStatus(RetailSocietyPlanStatus.SUBMITTED);
				}

				baseDTO = httpService.put(url, request);

				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("Society Plan Updated successfully");
						errorMap.notify(ErrorDescription.SOCIETY_PLAN_UPDATED_SUCCESSFULLY.getCode());
					} else {
						String msg = baseDTO.getErrorDescription();
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
				}

			}

		} catch (Exception exception) {
			log.error("<--- Error in submitDnpProcuremnetPlan() ---> ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_SOCIETY_WISE_PLAN_PAGE;
	}

	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (retailSocietyPlanListDTO == null) {
			log.info("<---if(retailSocietyPlanListDTO == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			AppUtil.addWarn("Please Select any one Plan");
			return;
		} else {
			log.info("<---Retail Society Plan Id--->" + retailSocietyPlanListDTO.getId());
			if (!retailSocietyPlanListDTO.getStatus().toString().equals("INITIATED")
					&& !retailSocietyPlanListDTO.getStatus().toString().equals("REJECTED")) {
				log.info("Retail Society Plan Cannot be Deleted.");
				errorMap.notify(
						ErrorDescription.INFO_RETAIL_SALES_SOCIETYWISE_PROCUREMENT_PLAN_CANNOT_BE_DELETED.getCode());
				return;
			}
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called with Society Plan Id---> " + retailSocietyPlanListDTO.getId());
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/delete/"
				+ retailSocietyPlanListDTO.getId();

		BaseDTO baseDTO = httpService.delete(url);

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("<--- D & P Procuemner Plan has been Deleted Successfully ---> ");
				errorMap.notify(
						ErrorDescription.INFO_RETAIL_SALES_SOCIETYWISE_PROCUREMENT_PLAN_DELETED_SUCCESSFULLY.getCode());
				showRetailSocietyPlanList();
			} else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
		} else {
			log.info("<--- Internal Server Error ---> ");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_SOCIETY_WISE_PLAN_PAGE;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current D & P Procurement Plan Status ---> " + currentPlanStatus);
		log.info("<--- Societywise Plan Id ---> " + retailSocietyPlanDetailsDTO.getId());
		log.info("<--- Comments ---> " + approveComments);
		log.info("<--- Comments ---> " + rejectComments);

		BaseDTO baseDTO = null;

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			RetailSocietyPlanListDTO request = new RetailSocietyPlanListDTO(retailSocietyPlanDetailsDTO.getId(),
					currentPlanStatus, approveComments);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/approve";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			RetailSocietyPlanListDTO request = new RetailSocietyPlanListDTO(retailSocietyPlanDetailsDTO.getId(),
					currentPlanStatus, rejectComments);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/reject";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		approveComments = null;
		rejectComments = null;

		return LIST_SOCIETY_WISE_PLAN_PAGE;
	}

	public void changeCurrentStatus(String value) {
		log.info("<---changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;

		approveComments = null;
		rejectComments = null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		retailSocietyPlanListDTO = ((RetailSocietyPlanListDTO) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("RetailProductionPlanBean onPageLoad method started");
		addButtonFlag = false;
	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("RETAIL_SOCIETY_PLAN_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have a  privilege for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}
	
	public void clearDandPofficeDetails() {
		log.info("-----start clearDandPofficeDetails Method------");
		productCategory = new ProductCategory();
		productGroup = new ProductGroupMaster();
		productVarity = new ProductVarietyMaster();
		societyPlanFromMonthYear=null;
		societyPlanToMonthYear=null;
		log.info("-----End clearDandPofficeDetails Method------");
	}
}

