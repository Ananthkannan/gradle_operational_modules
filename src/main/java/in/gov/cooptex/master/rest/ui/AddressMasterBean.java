package in.gov.cooptex.master.rest.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.utilities.AppUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("addressMasterBean")
public class AddressMasterBean {

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private List<StateMaster> stateMasterList;

	@Getter
	@Setter
	private List<DistrictMaster> districtMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<TalukMaster> talukMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<CityMaster> cityMasterList = new ArrayList<>();

	@Getter
	@Setter
	private List<AreaMaster> areaMasterList = new ArrayList<>();

	@Getter
	@Setter
	AddressMaster addressMaster = new AddressMaster();
	
	@Getter
	@Setter
	private List<CountryMaster> listCountryMaster=new ArrayList<>();;
	

	@Autowired
	CommonBean commonBean;

	@Autowired
	TalukMasterBean talukMasterBean;

	public void openAddressDialog() {
		log.info("Open Address Dialog");

		stateMasterList = commonDataService.loadStateList();
		listCountryMaster=commonDataService.loadCountry();
		loadDistrict();
		loadTalukAndCity();

		
	}
	
	public void openAddressDialog1() {
		log.info("Open Address Dialog");
		stateMasterList = commonDataService.loadStateList();
		listCountryMaster=commonDataService.loadCountry();
		loadDistrict();
		loadTalukAndCity();

	}
	
	

	public void openAddressDialogWithAction(String action) {
		log.info("<<====  Address Master with Action======>>" + action);
		if (action.equalsIgnoreCase("add")) {
			log.info("inside  openAddressDialogWithAction after if condition");
			stateMasterList = commonDataService.loadStateList();
			loadDistrict();
			loadTalukAndCity();

		}

	}

	public void loadDistrict() {
		// log.info("loadDistrict [" + addressMaster.getStateMaster().getName() + "]");
//		if(addressMaster==null) {
//			 addressMaster = new AddressMaster();
//		}
//	
		if (addressMaster != null && addressMaster.getStateMaster() != null) {
			commonBean.loadActiveDistrictsByState(addressMaster.getStateMaster().getId());
			districtMasterList = commonBean.getDistrictMasterList();
			talukMasterList.clear();
			cityMasterList.clear();
		}
	}

	public void loadTaluk() {
		// log.info("loadTaluk [" + addressMaster.getDistrictMaster() + "]");
		if (addressMaster != null && addressMaster.getDistrictMaster() != null) {
			commonBean.loadActiveTaluksByDistrict(addressMaster.getDistrictMaster().getId());
			talukMasterList = commonBean.getTalukMasterList();
		}
	}

	public void loadCity() {
		// log.info("loadCity [" + addressMaster.getDistrictMaster() + "]");
		if (addressMaster != null && addressMaster.getDistrictMaster() != null) {
			cityMasterList = commonDataService.getCityByDistrictId(addressMaster.getDistrictMaster().getId());

		}
	}

	public void loadTalukAndCity() {
		// log.info("<<======= loadTalukAndCity [ " + addressMaster.getDistrictMaster()
		// + " ]");
		loadTaluk();
		loadCity();
	}

	public void loadArea() {
		log.info("loadArea [" + addressMaster.getCityMaster() + "]");
		if (addressMaster.getCityMaster() != null) {
			areaMasterList = commonDataService.loadActiveAreasByCity(addressMaster.getCityMaster().getId());
		}

	}

	public void loadCountry() {
		if (addressMaster != null && addressMaster.getCountryMaster() != null) {
			listCountryMaster=commonDataService.loadCountry();
		}
		
	}
	
	public void onchangeCountry(Long id) {
		log.info("<<=== onchangeCountry selected Country id::: " + id);
		try {

			if (addressMaster != null && addressMaster.getCountryMaster() != null) {
			stateMasterList=commonDataService.loadActiveStates(id);
			log.info("<<:::::::::::stateMaster List:::::::::::>>" + stateMasterList.size());
			districtMasterList = new ArrayList<>();
			talukMasterList = new ArrayList<>();
			cityMasterList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("<<==============  error in onchangeCountry  " + e);
			districtMasterList.clear();
			talukMasterList.clear();
			cityMasterList.clear();
		}
	}
	
	
	public String prepareAddress(AddressMaster adressMaster) {
		String addressEnglish = "";

		StringBuilder sb = new StringBuilder();

		if (adressMaster != null) {
			if (adressMaster.getAddressLineOne() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineOne())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineOne() + ",";
				sb.append(adressMaster.getAddressLineOne()).append(", ");
			}
			if (adressMaster.getAddressLineTwo() != null && StringUtils.isNotEmpty(adressMaster.getAddressLineTwo())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineTwo() + ",";
				sb.append(adressMaster.getAddressLineTwo()).append(", ");
			}
			if (adressMaster.getAddressLineThree() != null
					&& StringUtils.isNotEmpty(adressMaster.getAddressLineThree())) {
				// addressEnglish = addressEnglish + adressMaster.getAddressLineThree() + ",";
				sb.append(adressMaster.getAddressLineThree()).append(", ");
			}
			if (adressMaster.getLocAddressLineOne() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineOne())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineOne() + ",";
				sb.append(adressMaster.getLocAddressLineOne()).append(", ");
			}
			if (adressMaster.getLocAddressLineTwo() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineTwo())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineTwo() + ",";
				sb.append(adressMaster.getLocAddressLineTwo()).append(", ");
			}
			if (adressMaster.getLocAddressLineThree() != null
					&& StringUtils.isNotEmpty(adressMaster.getLocAddressLineThree())) {
				// addressEnglish = addressEnglish + adressMaster.getLocAddressLineThree() +
				// ",";
				sb.append(adressMaster.getLocAddressLineThree()).append(", ");
			}
			if (adressMaster.getLandmark() != null && StringUtils.isNotEmpty(adressMaster.getLandmark())) {
				// addressEnglish = addressEnglish + adressMaster.getLandmark() + ",";
				sb.append(adressMaster.getLandmark()).append(", ");
			}
			if (adressMaster.getCityMaster() != null && adressMaster.getCityMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getCityMaster().getName() +
				// ",";
				sb.append(adressMaster.getCityMaster().getName()).append(", ");
			}
			if (adressMaster.getCountryMaster()!= null && adressMaster.getCountryMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getCityMaster().getName() +
				// ",";
				sb.append(adressMaster.getCountryMaster().getName()).append(", ");
			}
			
			if (adressMaster.getTalukMaster() != null && adressMaster.getTalukMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getTalukMaster().getName() +
				// ",";
				sb.append(adressMaster.getTalukMaster().getName()).append(", ");
			}
			if (adressMaster.getDistrictMaster() != null && adressMaster.getDistrictMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getDistrictMaster().getName()
				// + ",";
				sb.append(adressMaster.getDistrictMaster().getName()).append(", ");
			}
			if (adressMaster.getStateMaster() != null && adressMaster.getStateMaster().getName() != null) {
				// addressEnglish = addressEnglish + adressMaster.getStateMaster().getName();
				sb.append(adressMaster.getStateMaster().getName()).append(", ");
			}
			if (StringUtils.isNotEmpty(adressMaster.getPostalCode())) {
				// addressEnglish = addressEnglish + adressMaster.getStateMaster().getName();
				sb.append(" - ").append(adressMaster.getPostalCode());
			}
			addressEnglish = sb.toString();
			// if (adressMaster.getPostalCode() != null &&
			// !adressMaster.getPostalCode().trim().isEmpty()) {
			// if (addressEnglish.isEmpty()) {
			// addressEnglish = adressMaster.getPostalCode();
			// } else {
			// addressEnglish = addressEnglish + "-" + adressMaster.getPostalCode();
			// }
			// }
		}
		return addressEnglish;
	}
}
