package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("customerTypeMasterBean")
@Scope("session")
@Log4j2
public class CustomerTypeMasterBean {

	final String LIST_PAGE = "/pages/masters/listCustomerTypeMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createCustomerTypeMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String CUSTOMER_TYPE_MASTER_URL = AppUtil.getPortalServerURL()+"/customertypemaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	CustomerTypeMaster customerTypeMaster,selectedCustomerTypeMaster;

	@Getter @Setter
	LazyDataModel<CustomerTypeMaster> lazyCustomerTypeMasterList;

	List<CustomerTypeMaster> customerTypeMasterList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	
	@Setter
	@Getter
	boolean cusCodeType = true;
	
	public String showCustomerTypeMasterListPage() {
		log.info("<==== Starts CustomerTypeMasterBean.showCustomerTypeMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		customerTypeMaster = new CustomerTypeMaster();
		selectedCustomerTypeMaster = new CustomerTypeMaster();
		loadLazyCustomerTypeMasterList();
		log.info("<==== Ends CustomerTypeMasterBean.showCustomerTypeMasterListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyCustomerTypeMasterList() {
		log.info("<===== Starts CustomerTypeMasterBean.loadLazyCustomerTypeMasterList ======>");

		lazyCustomerTypeMasterList = new LazyDataModel<CustomerTypeMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<CustomerTypeMaster> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = CUSTOMER_TYPE_MASTER_URL + "/getallcustomertypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						customerTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerTypeMaster>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return customerTypeMasterList;
			}

			@Override
			public Object getRowKey(CustomerTypeMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CustomerTypeMaster getRowData(String rowKey) {
				for (CustomerTypeMaster customerTypeMaster : customerTypeMasterList) {
					if (customerTypeMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedCustomerTypeMaster = customerTypeMaster;
						return customerTypeMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends CustomerTypeMasterBean.loadLazyCustomerTypeMasterList ======>");
	}


	public String showCustomerTypeMasterListPageAction() {
		log.info("<===== Starts CustomerTypeMasterBean.showCustomerTypeMasterListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedCustomerTypeMaster == null || selectedCustomerTypeMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_CUSTOMER_TYPE_MASTER.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create customerTypeMaster called..");
				customerTypeMaster = new CustomerTypeMaster();
				cusCodeType=false;
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				cusCodeType=true;
				log.info("edit customerTypeMaster called..");
				String url=CUSTOMER_TYPE_MASTER_URL+"/getbyid/"+selectedCustomerTypeMaster.getId();
				BaseDTO response = httpService.get(url);
				
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					customerTypeMaster = mapper.readValue(jsonResponse,new TypeReference<CustomerTypeMaster>(){
					});	
					
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete customerTypeMaster called..");
				AppUtil.addWarn("Customer Type Cannot be deleted");
				/*RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCustomerTypeMasterDelete').show();");*/
			}
		}catch(Exception e) {
			log.error("Exception occured in showCustomerTypeMasterListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends CustomerTypeMasterBean.showCustomerTypeMasterListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts CustomerTypeMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(CUSTOMER_TYPE_MASTER_URL+"/deletebyid/"+selectedCustomerTypeMaster.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCustomerTypeMasterDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete customerTypeMaster ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends CustomerTypeMasterBean.deleteConfirm ===========>");
		return  showCustomerTypeMasterListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts CustomerTypeMasterBean.saveOrUpdate =======>");
		try {
			if(customerTypeMaster.getCode() == null || customerTypeMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_CODE_EMPTY.getCode());
				return null;
			}
			if(customerTypeMaster.getName() == null || customerTypeMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_NAME_EMPTY.getCode());
				return null;
			}
			if(customerTypeMaster.getName().trim().length() < 3 || customerTypeMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(customerTypeMaster.getLocalName() == null || customerTypeMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_LNAME_EMPTY.getCode());
				return null;
			}
			if(customerTypeMaster.getLocalName().trim().length() < 3 || customerTypeMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_LNAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(customerTypeMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_STATUS_EMPTY.getCode());
				return null;
			}
			String url = CUSTOMER_TYPE_MASTER_URL+"/saveorupdate";
			BaseDTO response = httpService.post(url,customerTypeMaster);
			if(response != null && response.getStatusCode() == 0) {
				log.info("customerTypeMaster saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.CUSTOMER_TYPE_MASTER_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends CustomerTypeMasterBean.saveOrUpdate =======>");
		return showCustomerTypeMasterListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts CustomerTypeMasterBean.onRowSelect ========>"+event);
		selectedCustomerTypeMaster = ((CustomerTypeMaster) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends CustomerTypeMasterBean.onRowSelect ========>");
    }
	
}
