package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.LoanMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.weavers.model.WeaversBenefitScheme;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("weaversBenefitSchemeBean")
@Scope("session")
@Log4j2
public class WeaversBenefitSchemeBean {

	final String LIST_PAGE = "/pages/masters/weaversBenefitScheme/listWeaversBenefitScheme.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/weaversBenefitScheme/createWeaversBenefitScheme.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/masters/weaversBenefitScheme/viewWeaversBenefitScheme.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	WeaversBenefitScheme weaversBenefitScheme,selectedWeaversBenefitScheme;

	@Getter @Setter
	LazyDataModel<WeaversBenefitScheme> lazyWeaversBenefitSchemeList;

	List<WeaversBenefitScheme> weaversBenefitSchemeList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showWeaversBenefitSchemeListPage() {
		log.info("<==== Starts WeaversBenefitSchemeBean.showWeaversBenefitSchemeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag=true;
		weaversBenefitScheme = new WeaversBenefitScheme();
		selectedWeaversBenefitScheme = new WeaversBenefitScheme();
		loadLazyWeaversBenefitSchemeList();
		log.info("<==== Ends weaversBenefitSchemeBean.showWeaversBenefitSchemeListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyWeaversBenefitSchemeList() {
		log.info("<===== Starts weaversBenefitSchemeBean.loadLazySalesTypeList ======>");

		lazyWeaversBenefitSchemeList= new LazyDataModel<WeaversBenefitScheme>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -6613861142448851255L;

			@Override
			public List<WeaversBenefitScheme> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = SERVER_URL+appPreference.getOperationApiUrl()+"/weaversbenefitscheme/getallWeaversBenefitSchemelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						weaversBenefitSchemeList= mapper.readValue(jsonResponse, new TypeReference<List<WeaversBenefitScheme>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("weaversBenefitSchemeList size---------------->"+weaversBenefitSchemeList.size());
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return weaversBenefitSchemeList;
			}

			@Override
			public Object getRowKey(WeaversBenefitScheme res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public WeaversBenefitScheme getRowData(String rowKey) {
				for (WeaversBenefitScheme weaversBenefitScheme : weaversBenefitSchemeList) {
					if (weaversBenefitScheme .getId().equals(Long.valueOf(rowKey))) {
						selectedWeaversBenefitScheme= weaversBenefitScheme ;
						return weaversBenefitScheme;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  weaversBenefitSchemeBean.loadLazySalesTypeList  ======>");
	}


	public String showWeaversBenefitSchemeListPageAction() {
		log.info("<===== Starts weaversBenefitSchemeBean.showSalesTypeListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedWeaversBenefitScheme== null || selectedWeaversBenefitScheme.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if(action.equalsIgnoreCase("Create")) {
				log.info("create WeaversBenefitScheme called..");
				weaversBenefitScheme = new WeaversBenefitScheme();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit WeaversBenefitScheme called..");
				String url=SERVER_URL+appPreference.getOperationApiUrl()+"/weaversbenefitscheme/getbyid/"+selectedWeaversBenefitScheme.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					weaversBenefitScheme = mapper.readValue(jsonResponse,new TypeReference<WeaversBenefitScheme>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else if(action.equalsIgnoreCase("View")){
				log.info("View weaversBenefitScheme called..");
				
				String url=SERVER_URL+appPreference.getOperationApiUrl()+"/weaversbenefitscheme/getbyid/"+selectedWeaversBenefitScheme.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					weaversBenefitScheme = mapper.readValue(jsonResponse,new TypeReference<WeaversBenefitScheme>(){
					});	
					return VIEW_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}
			}else{
				log.info("delete weaversBenefitScheme called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmWeaversBenefitSchemeDelete').show()");
			}
		}catch(Exception e) {
			log.error("Exception occured in showWeaversBenefitSchemeListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends weaversBenefitSchemeBean.showWeaversBenefitSchemeListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts weaversBenefitSchemeBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL+appPreference.getOperationApiUrl()+"/weaversbenefitscheme/deletebyid/"+selectedWeaversBenefitScheme.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmWeaversBenefitSchemeDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete WeaversBenefitScheme ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends weaversBenefitSchemeBean.deleteConfirm ===========>");
		return  showWeaversBenefitSchemeListPage();
	}
	
	
	@Autowired
	AppPreference appPreference;
	
	
	
	public String saveOrUpdate(){
		log.info("<==== Starts weaversBenefitSchemeBean.saveOrUpdate =======>");
		try {
			if(weaversBenefitScheme.getCode() == null || weaversBenefitScheme.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_CODE_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getName() == null || weaversBenefitScheme.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_NAME_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getLocalName()== null || weaversBenefitScheme.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_LOC_NAME_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getEligibilityAmount()== null) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_ELIGIBILITY_AMOUNT_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getSchemeToDate()== null) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_PLAN_TO_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getSchemeFromDate()== null) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_PLAN_FROM_EMPTY.getCode());
				return null;
			}
			if(weaversBenefitScheme.getActiveStatus()== null) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_STATUS_EMPTY.getCode());
				return null;
			}
			
			
			if(weaversBenefitScheme.getCode().trim().length() < 3 || weaversBenefitScheme.getCode().trim().length() > 75) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_CODE_MIN_MAX_ERROR.getCode());
				return null;
			}
			if(weaversBenefitScheme.getName().trim().length() < 3 || weaversBenefitScheme.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			
			if(weaversBenefitScheme.getLocalName().trim().length() < 3 || weaversBenefitScheme.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.WEAVERSBENEFITS_LOC_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
			
			String url = SERVER_URL+appPreference.getOperationApiUrl()+"/weaversbenefitscheme/saveorupdate";
			BaseDTO response = httpService.post(url,weaversBenefitScheme);
			if(response != null && response.getStatusCode() == 0) {
				log.info("WeaversBenefitScheme saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.WEAVERSBENEFITS_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.WEAVERSBENEFITS_UPDATE_SUCCESS.getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends weaversBenefitSchemeBean.saveOrUpdate =======>");
		return showWeaversBenefitSchemeListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts weaversBenefitSchemeBean.onRowSelect ========>"+event);
		selectedWeaversBenefitScheme= ((WeaversBenefitScheme) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends weaversBenefitSchemeBean.onRowSelect ========>");
    }

	
	
}
