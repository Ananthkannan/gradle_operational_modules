package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.GlAccount;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.PayHeadCadreConfig;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.ExhibitionAddress;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("cityMasterBean")
public class CityMasterBean {

	private final String CREATE_CITY_MASTER_PAGE = "/pages/masters/createCityMaster.xhtml?faces-redirect=true;";
	private final String CITY_MASTER_LIST_URL = "/pages/masters/listCityMaster.xhtml?faces-redirect=true;";
	private final String CITY_MASTER_VIEW_URL = "/pages/masters/viewCityMaster.xhtml?faces-redirect=true;";
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	private String fileNames;

	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	CountryMaster countryMaster = new CountryMaster();

	@Getter
	@Setter
	StateMaster stateMaster = new StateMaster();

	@Getter
	@Setter
	DistrictMaster districtMaster = new DistrictMaster();

	@Getter
	@Setter
	CityMaster cityMaster = new CityMaster();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<CountryMaster> countryMasterList = new ArrayList<CountryMaster>();

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();

	@Getter
	@Setter
	List<DistrictMaster> districtMasterList = new ArrayList<DistrictMaster>();

	@Getter
	@Setter
	List<CityMaster> cityMasterList = new ArrayList<CityMaster>();

	@Getter
	@Setter
	LazyDataModel<CityMaster> cityMasterLazyList;

	@Getter
	@Setter
	CityMaster selectedCityMaster;

	@Getter
	@Setter
	private int cityMasterListSize;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	CommonBean commonBean;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	Long stateId = null;
	Long countryId = null;

	@PostConstruct
	public void init() {
		log.info(" CityMasterBean init --->" + SERVER_URL);
		clear();
		getAllCountryList();
//		getAllStateList();
//		getAllDistrictList();
		loadLazyList();
		// onCountryChange();
		// onStateChange();
	}

	public String createCityMaster() {
		init();
		addButtonFlag = false;
		return CREATE_CITY_MASTER_PAGE;
	}

	public void clear() {
		countryMaster = new CountryMaster();
		stateMaster = new StateMaster();
		districtMaster = new DistrictMaster();
		cityMaster = new CityMaster();
		countryMasterList = new ArrayList<CountryMaster>();
		stateMasterList = new ArrayList<StateMaster>();
		districtMasterList = new ArrayList<DistrictMaster>();
		cityMasterList = new ArrayList<CityMaster>();
		selectedCityMaster = new CityMaster();
	}

	public void getAllCountryList() {
		try {
			log.info(" START  getAllCountryList ");
			if (countryMaster != null) {
				countryMasterList = new ArrayList<CountryMaster>();
				log.info(" getAllCountryList ==>>> " + countryMaster.getId());
				countryMasterList = commonDataService.loadCountry();
			}
		} catch (Exception e) {
			log.error("Exception occured ....getAllCountryList.. ", e);
		}
		log.info(" END getAllCountryList ");
	}

	public void getAllStateList() {
		try {
			log.info(" START getAllStateList ");
			if (stateMaster != null) {
				stateMasterList = new ArrayList<StateMaster>();
				log.info(" getAllstateList ==>>> " + stateMaster);
				stateMasterList = commonDataService.loadSates();
			}
		} catch (Exception e) {
			log.error("Exception occured ....getAllStateList.. ", e);
		}
		log.info(" END getAllStateList ");
	}

	public void getAllDistrictList() {
		try {
			log.info(" START getAllDistrictList ");
			if (districtMaster != null) {
				districtMasterList = new ArrayList<DistrictMaster>();
				log.info(" getAllDistrictList ==>>> " + districtMaster);
				districtMasterList = commonDataService.getDistrictMasterList();
			}
		} catch (Exception e) {
			log.error("Exception occured ....getAllDistrictList.. ", e);
		}
		log.info(" END getAllDistrictList ");
	}

	public void onCountryChange() {
		log.info("onCountryChange [" + countryMaster.getId() + "]");
		if (countryMaster.getId() != null) {
			commonBean.loadActiveStatesByCountry(countryMaster.getId());
			stateMasterList = commonBean.getStateMasterList();
		}
	}

	public void onStateChange() {
		log.info("onStateChange [" + stateMaster.getId() + "]");
		if (stateMaster.getId() != null) {
			commonBean.loadActiveDistrictsByState(stateMaster.getId());
			districtMasterList = commonBean.getDistrictMasterList();
		}
	}

	public boolean checkDuplicate() {
		boolean valid=true;
		BaseDTO baseDTO = null;
		if(cityMaster.getName()==null || cityMaster.getName().length()==0){
			AppUtil.addError("please enter City Name  ");
			return false;
		}
		
		if(cityMaster.getId()!=null && cityMaster.getId()>0){
			return valid;
		}
		
		baseDTO = httpService.post(SERVER_URL + appPreference.getOperationApiUrl() + "/city/checkDuplicateCity",cityMaster);
		if(baseDTO.getStatusCode()==1){
			AppUtil.addError("City Name alredy exists ");
			return false;
		}
		return valid;
	}
	
	public String submit() {
		log.info("......  submit begin ....");
		try {
			if(checkDuplicate()){
			//cityMaster.setDistrictMaster(districtMaster);
			BaseDTO baseDTO = null;
			baseDTO = httpService.post(SERVER_URL + appPreference.getOperationApiUrl() + "/city/create",
					cityMaster);
			log.info("city Master" + cityMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode().equals(ErrorDescription.CITY_MASTER_SAVED_SUCCESSFUL.getCode())) {
					log.info(" saved successfully");
					init();
					errorMap.notify(ErrorDescription.CITY_MASTER_SAVED_SUCCESSFUL.getCode());
					return CITY_MASTER_LIST_URL;
				}else if(baseDTO.getStatusCode().equals(ErrorDescription.COMMUNITY_NAME_ALREADY_EXIST.getCode())) {
					AppUtil.addError("City Name already exists");
					return null;
				}else if(baseDTO.getStatusCode().equals(ErrorDescription.COMMUNITY_LNAME_ALREADY_EXIST.getCode())) {
					AppUtil.addError("City Name (In Tamil) already exists");
					return null;
				}else if(baseDTO.getStatusCode().equals(ErrorDescription.CITY_CODE_ALREADY_EXIST_DUPICATECITY.getCode())) {
					AppUtil.addError("City Code already exists");
					return null;
				}else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating city master" + e);
		}
		log.info("...... submit ended ....");

		return null;

	}

	public String cityMasterListPage() {
		log.info("============== cityMasterListPage =============");
		try {
			clear();
			addButtonFlag = false;
			cityMaster = new CityMaster();
			selectedCityMaster = new CityMaster();
			getAllCountryList();
			getAllStateList();
			getAllDistrictList();
		} catch (Exception e) {
			log.error("============== Exception cityMasterListPage()================" + e);
		}
		return CITY_MASTER_LIST_URL;
	}

	public void loadLazyList() {

		log.info("<===== Starts lazy List ======>");
		String CITY_MASTER_LIST_URL = SERVER_URL + appPreference.getOperationApiUrl() + "/city/getAllCity";

		cityMasterLazyList = new LazyDataModel<CityMaster>() {

			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<CityMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					BaseDTO response = httpService.post(CITY_MASTER_LIST_URL, paginationRequest);
					log.info("==========URL  List=============>" + CITY_MASTER_LIST_URL);
					log.info("Pagination request :::" + paginationRequest);

					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						cityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CityMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazy List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return cityMasterList;
			}

			@Override
			public Object getRowKey(CityMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CityMaster getRowData(String rowKey) {
				try {
					for (CityMaster cityMasterObj : cityMasterList) {
						if (cityMasterObj.getId().equals(Long.valueOf(rowKey))) {
							cityMaster = cityMasterObj;
							return cityMasterObj;
						}
					}
				}

				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};
		log.info("<===== Ends CityMaster.loadLazyCityMasterList ======>");
	}

	public String viewCityMaster() {
		log.info("<<<< ----------Start viewCityMaster() ------- >>>>");

		try {
			if (selectedCityMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/city/editCityById/"
					+ selectedCityMaster.getId();
			log.info(" selectedCityMaster ===>> view...URL.. " + url);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				cityMaster = mapper.readValue(jsonResponse, new TypeReference<CityMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		return CITY_MASTER_VIEW_URL;
	}

	public String editCityMaster() {

		log.info("<<<<< -------Start City Master EDIT called ------->>>>>> ");

		try {
			if (selectedCityMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/city/editCityById/"
					+ selectedCityMaster.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				cityMaster = mapper.readValue(jsonResponse, new TypeReference<CityMaster>() {
				});
				log.info("<<<<< -------End  EDIT  ------->>>>>> ");

				districtMaster = cityMaster.getDistrictMaster();
				stateMaster = cityMaster.getDistrictMaster().getStateMaster();
				countryMaster = cityMaster.getDistrictMaster().getStateMaster().getCountryMaster();
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return CREATE_CITY_MASTER_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		addButtonFlag = true;
		selectedCityMaster = ((CityMaster) event.getObject());
		cityMaster = selectedCityMaster;
		log.info("<===Ends .onRowSelect ========>");
	}

	public String deleteCity() {
		try {
			if (selectedCityMaster == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts cityMaster.deleteConfirm ---->");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/city/deleteCity/" + selectedCityMaster.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.CITY_MASTER_DELETE_SUCCESSFUL.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting cityMasterListPage....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends cityMasterListPage.deleteConfirm ---->");
		return cityMasterListPage();
	}

}
