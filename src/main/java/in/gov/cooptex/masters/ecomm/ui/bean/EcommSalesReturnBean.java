package in.gov.cooptex.masters.ecomm.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ItemQRCodeDetailsDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.ecommerce.EcommSalesReturnDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("ecommSalesReturnBean")
@Scope("session")
@Log4j2
public class EcommSalesReturnBean {

	final String LIST_PAGE = "/pages/ecommerce/admin/listEcommSalesReturn.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/ecommerce/admin/createEcommSalesReturn.xhtml?faces-redirect=true";

	final String ECOMM_SALES_RETURN_URL = AppUtil.getPortalServerURL() + "/ecommSalesReturnController";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	LazyDataModel<EcommSalesReturnDTO> lazyEcommSalesReturnDTOList;

	@Getter
	@Setter
	EcommSalesReturnDTO selectedEcommSalesReturnDTO;

	@Getter
	@Setter
	EcommSalesReturnDTO ecommSalesReturnDTO;

	List<EcommSalesReturnDTO> ecommSalesReturnList = null;

	public String showEcommSalesReturnListPage() {
		log.info("<==== Starts EcommSalesReturnBean.showEcommSalesReturnListPage =====>");
		mapper = new ObjectMapper();
		selectedEcommSalesReturnDTO = new EcommSalesReturnDTO();
		ecommSalesReturnList = new ArrayList<>();
		loadLazyEcommSalesReturnList();
		log.info("<==== Ends EcommSalesReturnBean.showEcommSalesReturnListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyEcommSalesReturnList() {
		log.info("<===== Starts EcommSalesReturnBean.loadLazyEcommSalesReturnList ======>");

		lazyEcommSalesReturnDTOList = new LazyDataModel<EcommSalesReturnDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EcommSalesReturnDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = ECOMM_SALES_RETURN_URL + "/getSalesReturnLazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						ecommSalesReturnList = mapper.readValue(jsonResponse,
								new TypeReference<List<EcommSalesReturnDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return ecommSalesReturnList;
			}

			@Override
			public Object getRowKey(EcommSalesReturnDTO res) {
				return res != null ? res.getSalesReturnId() : null;
			}

			@Override
			public EcommSalesReturnDTO getRowData(String rowKey) {
				for (EcommSalesReturnDTO ecommSalesReturn : ecommSalesReturnList) {
					if (ecommSalesReturn.getSalesReturnId().equals(Long.valueOf(rowKey))) {
						selectedEcommSalesReturnDTO = ecommSalesReturn;
						return ecommSalesReturn;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends EcommSalesReturnBean.loadLazyEcommSalesReturnList ======>");
	}

	/**
	 * return Qr Code Details
	 */
	public void searchQrCode() {
		log.info("<===== Starts EcommSalesReturnBean.searchQrCode ===========>");
		try {

			if (ecommSalesReturnDTO.getqRCode() != null) {
				if (AppUtil.isJSONValid(ecommSalesReturnDTO.getqRCode())) {
					ItemQRCodeDetailsDTO itemQRCodeDetailsDTO = new ItemQRCodeDetailsDTO();
					itemQRCodeDetailsDTO = new Gson().fromJson(ecommSalesReturnDTO.getqRCode(),
							ItemQRCodeDetailsDTO.class);
					if (itemQRCodeDetailsDTO != null) {
						String qrCode = itemQRCodeDetailsDTO.getQRCode();
						ecommSalesReturnDTO = getQrcodeDetails(qrCode);
					} else {
						log.info("itemQRCodeDetailsDTO is null");
					}
				} else {
					ecommSalesReturnDTO = getQrcodeDetails(ecommSalesReturnDTO.getqRCode());
				}
			}
			if (ecommSalesReturnDTO == null) {
				AppUtil.addWarn(" QR Code Not Available");
				return;
			}
			log.info("<===== Ends EcommSalesReturnBean.searchQrCode ===========>");
		} catch (Exception e) {
			log.error(" <<== Exception Occured on searchQrCode ==>>", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}

	}

	/**
	 * @param qrCode
	 * @return
	 */
	private EcommSalesReturnDTO getQrcodeDetails(String qrCode) {
		log.info("Start getItemQrcodeDetails Method======>" + qrCode);
		EcommSalesReturnDTO ecommSalesReturnDTO = null;
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = ECOMM_SALES_RETURN_URL + "/ecommSalesReturnController/getqrcodedetails/" + qrCode;
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Item Qr code details get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ecommSalesReturnDTO = mapper.readValue(jsonResponse, EcommSalesReturnDTO.class);

			}

		} catch (Exception ex) {
			log.error("Error in getQrcodeDetails Method", ex);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("Start getQrcodeDetails Method======>");
		return ecommSalesReturnDTO;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EcommSalesReturnBean.onRowSelect ========>" + event);
		selectedEcommSalesReturnDTO = ((EcommSalesReturnDTO) event.getObject());
		log.info("<===Ends EcommSalesReturnBean.onRowSelect ========>");
	}

	public String saveOrUpdate() {
		log.info("<==== Starts EcommSalesReturnBean.saveOrUpdate =======>");
		try {

			if (ecommSalesReturnDTO == null) {
				AppUtil.addWarn("Please select one record");
				// RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			String url = ECOMM_SALES_RETURN_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, ecommSalesReturnDTO);
			if (response != null && response.getStatusCode() == 0) {
				log.info("EcommSalesReturnBean saved successfully..........");
				AppUtil.addInfo(" Updated Successfully ");
				return showEcommSalesReturnListPage();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommSalesReturnBean.saveOrUpdate =======>");
		return null;
	}

	public String retrieveViewDetails() {

		log.info("<==== Starts EcommSalesReturnBean.retrieveViewDetails =======>");
		try {

			if (selectedEcommSalesReturnDTO == null) {
				AppUtil.addWarn("Please select one record");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			ecommSalesReturnDTO = new EcommSalesReturnDTO();
			ecommSalesReturnList = new ArrayList<>();

			String url = ECOMM_SALES_RETURN_URL + "/getView/" + selectedEcommSalesReturnDTO.getSalesReturnId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				log.info("Item Qr code details get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				ecommSalesReturnDTO = mapper.readValue(jsonResponse, EcommSalesReturnDTO.class);

				String jsonResponse1 = mapper.writeValueAsString(response.getResponseContents());
				ecommSalesReturnList = mapper.readValue(jsonResponse1, new TypeReference<List<EcommSalesReturnDTO>>() {
				});
				ecommSalesReturnDTO.setEcommSalesReturnDTOList(ecommSalesReturnList);

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommSalesReturnBean.saveOrUpdate =======>");

		return ADD_PAGE;
	}

	/**
	 * @param ecommSalesReturnDTO
	 */
	public void updateApprovedQnty(EcommSalesReturnDTO dto) {
		try {
			log.info("<==== Started EcommSalesReturnBean.updateApprovedQnty =======>");
			if (!(dto.getReqReturnQnty() >= dto.getApprovedReturnQnty())) {
				log.info("<==== Approved Qnty Should not more than Req Qnty =======>");
				AppUtil.addWarn(" Approved Qnty Should not more than Req Qnty");
				dto.setApprovedReturnQnty(0D);
				dto.setTotalReturnValue(0D);
				return;
			}

			dto.setTotalReturnValue(dto.getApprovedReturnQnty() * dto.getUnitRate());
//			Double totalReturnValue = ecommSalesReturnDTO.getEcommSalesReturnDTOList().stream()
//					.collect(Collectors.summingDouble(x -> x.getTotalReturnValue()));

//			ecommSalesReturnDTO
//					.setTotalReturnValue(totalReturnValue);

		} catch (Exception e) {
			log.error(" Exception occured on updateApprovedQnty   === " + e);
		}
	}
}
