package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.HoRoEntitytypeDTO;
import in.gov.cooptex.core.dto.IncrementDTO;
import in.gov.cooptex.core.dto.IncrementForwardDTO;
import in.gov.cooptex.core.dto.IncrementListDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.PostpondForwardDTO;
import in.gov.cooptex.core.dto.PostpondListDTO;
import in.gov.cooptex.core.dto.PostponedEmployeeDTO;
import in.gov.cooptex.core.dto.UpdatePostponedDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.enums.ConformationTypeEnum;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.EmpIncrementLog;
import in.gov.cooptex.core.model.EmpIncrementNote;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.personnel.hrms.EmpManualPostponed;
import in.gov.cooptex.personnel.hrms.EmpManualPostponedDetails;
import in.gov.cooptex.personnel.hrms.EmpManualPostponedLog;
import in.gov.cooptex.personnel.hrms.EmpManualPostponedNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("confirmationPostponeBean")
public class ConfirmationPostponeBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/personnelHR/createEmployeeConfirmationPostpone.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/personnelHR/viewEmployeeConfirmationPostpone.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/personnelHR/listEmployeeConfirmationPostpone.xhtml?faces-redirect=true";

	final String REJECT = "REJECT";

	final String APPROVED = "APPROVED";

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	List<String> conformationTypeList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster headRegionOffice = new EntityMaster();

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<PostponedEmployeeDTO> postponedEmployeeDTOList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();

	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	List<Long> selectedEmployeeIdList = new ArrayList<>();

	@Getter
	@Setter
	String pageAction, conformationType, postpondRemarks;

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	List<EntityMaster> entityList;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	LazyDataModel<PostpondListDTO> postpondListDTOLazyList;

	@Getter
	@Setter
	List<PostpondListDTO> postpondListDTOList = new ArrayList<>();

	@Getter
	@Setter
	PostpondListDTO selectedPostpondListDTO = new PostpondListDTO();

	@Getter
	@Setter
	EmpManualPostponed employeeManualPostponed = new EmpManualPostponed();

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	String headRegionOfficeName, entityTypeName;

	@Getter
	@Setter
	Boolean viewFlag = false, addEditFlag = false,disableFlag = false;

	@Getter
	@Setter
	Long noteId;

	@Getter
	@Setter
	List<EmpManualPostponedLog> empManualPostponedLogList = new ArrayList<>();
	
	String updateNote;

	@PostConstruct
	public void init() {
		log.info("ConfirmationPostponeBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadConfirmationPage();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public void loadConfirmationPage() {
		log.info("<=ConfirmationPostponeBean :: loadConfirmationPage==>");
		try {
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			//entityTypeList = commonDataService.getEntityTypeByStatus();
			entityTypeList = commonDataService.getAllEntityType();
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CONFIRMATION_POSTPONE");
			loadConformationType();
		} catch (Exception e) {
			log.error("Exception in loadIncrementInformation  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadConformationType() {
		log.info("<=ConfirmationPostponeBean :: loadConformationType==>");
		try {
			for (ConformationTypeEnum conformationTypeEnum : ConformationTypeEnum.values()) {
				log.info("loadConformationType :: conformationTypeEnum ==> " + conformationTypeEnum.name());
				conformationTypeList.add(conformationTypeEnum.name());
			}

		} catch (Exception e) {
			log.error("Exception in loadConformationType  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadEmployeeByEntity() {
		log.info("<=ConfirmationPostponeBean :: loadEmployeeByEntity==>");
		try {
			if (entityMaster == null || entityMaster.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
				return;
			}
			if (conformationType == null || conformationType.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.TYPE_NOT_FOUND).getCode());
				return;
			}
			log.info("loadEmployeeByEntity :: entityMaster id==> " + entityMaster.getId());
			url = SERVER_URL + "/confirmationpostpone/loademployeebyentityandtype/:entityMasterId/:type";
			url = url.replace(":entityMasterId", entityMaster.getId().toString());
			url = url.replace(":type",conformationType);
			log.info("loadEmployeeByEntity :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeMasterList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info("ConfirmationPostponeBean :: employeeMasterList==> " + employeeMasterList.size());
				} else {
					selectedEmployeeIdList = new ArrayList<>();
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_NOT_FOUND).getCode());
					return;
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadEmployeeByEntity  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadEntityList() {
		log.info("<=ConfirmationPostponeBean ::  loadEntityList=>");
		try {
			entityList=new ArrayList<>();
			if(headRegionOffice!=null && headRegionOffice.getEntityTypeMaster()!=null && headRegionOffice.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
				entityTypeMaster=headRegionOffice.getEntityTypeMaster();
				entityMaster=headRegionOffice;
				entityList.add(entityMaster);
				disableFlag=true;
				loadEmployeeByEntity();
				return;
			}
			if (headRegionOffice != null && headRegionOffice.getId() != null && entityTypeMaster != null
					&& entityTypeMaster.getId() != null) {
				disableFlag=false;
				Long regionId = headRegionOffice.getId();
				Long entityTypeId = entityTypeMaster.getId();
				log.info("loadEntityList :: regionId==> " + regionId);
				log.info("loadEntityList :: entityTypeId==> " + entityTypeId); 
				entityList = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
				if (entityList != null && !entityList.isEmpty()) {
					log.info("entity list size==> " + entityList.size());
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
					return;
				}
			} else {
				disableFlag=false;
				log.info("Head / Region office or entity type not selected");

			}
		} catch (Exception exp) {
			log.error("loadEntityList==> ", exp);
		}

	}

	public String callConfirmationPage() {
		log.info("<=ConfirmationPostponeBean :: callConfirmationPage==>");
		try {
			if (pageAction.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				addEditFlag = false;
				loadLazyConfirmationPostpondList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				addEditFlag = false;
				return ADD_PAGE;
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				addEditFlag = true;
				if (selectedPostpondListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return editPostpond();
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedPostpondListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				return viewPostpond();
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				if (selectedPostpondListDTO == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			}

		} catch (Exception e) {
			log.error("Exception in callConfirmationPage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editPostpond() {
		log.info("<==== Starts ConformationPostponeBean :: viewPostpond =====>");
		try {
			Long postpondId = selectedPostpondListDTO.getPostpondId();
			log.info("viewPostpond :: postpondId==> " + postpondId);
			String url = SERVER_URL + "/confirmationpostpone/getpostpondbyid/id/" + postpondId;
			log.info("viewPostpond :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("viewPostpond :: base dto error code ==> " + baseDTO.getStatusCode());
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				//selectedEmployeeIdList
				// --to get confirmation postponed value
				employeeManualPostponed = mapper.readValue(jsonResponse, EmpManualPostponed.class);
				if (employeeManualPostponed != null && employeeManualPostponed.getId() != null) {
					conformationType = employeeManualPostponed.getPostponeType();
					log.info("conformationType==>" + conformationType);
					entityMaster = employeeManualPostponed.getEntityMaster();
					log.info("entityMaster id==> " + entityMaster.getId());
					// --to load head office/Region office and entity type
					loadHeadRegionOfficeAndEntityType(entityMaster);
					// -- to load entity based on entity type and ho/ro
					loadEntityList();

					// --to load confirmation postponed details value
					loadConfirmationPostpondDetails(employeeManualPostponed);
					// -- to get confirmation note postponed details
					if (employeeManualPostponed.getEmpManualPostponedNoteList() != null
							&& !employeeManualPostponed.getEmpManualPostponedNoteList().isEmpty()) {
						for (EmpManualPostponedNote empManualPostponedNote : employeeManualPostponed
								.getEmpManualPostponedNoteList()) {
							noteId = empManualPostponedNote.getId();
							forwardToUser = empManualPostponedNote.getUserMaster();
							log.info("forwardToUser id=> " + forwardToUser.getId());
							forwardFor = empManualPostponedNote.getFinalApproval();
							log.info("forwardFor=> " + forwardFor);
							note = empManualPostponedNote.getNote();
							updateNote=note;
							log.info("note=> " + note);
						}
					} else {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_NOTE_NOT_FOUND).getCode());
						return null;
					}
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_NOT_FOUND).getCode());
					return null;
				}
				// headRegionOffice = empIncrement.getEntityMaster();

				// --to get increment log details
				if (employeeManualPostponed.getEmpManualPostponedLogList() != null
						&& !employeeManualPostponed.getEmpManualPostponedLogList().isEmpty()) {
					empManualPostponedLogList = employeeManualPostponed.getEmpManualPostponedLogList();
				} else {
					//log.error("Increment log not found");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_LOG_NOT_FOUND).getCode());
					return null;
				}

				return ADD_PAGE;

			}
		} catch (Exception e) {
			log.error("Exception in viewPostpond  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String viewPostpond() {
		log.info("<==== Starts ConformationPostponeBean :: viewPostpond =====>");
		try {
			Long postpondId = selectedPostpondListDTO.getPostpondId();
			log.info("viewPostpond :: postpondId==> " + postpondId);
			String url = SERVER_URL + "/confirmationpostpone/getpostpondbyid/id/" + postpondId;
			log.info("viewPostpond :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("viewPostpond :: base dto error code ==> " + baseDTO.getStatusCode());
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get confirmation postponed value
				employeeManualPostponed = mapper.readValue(jsonResponse, EmpManualPostponed.class);
				if (employeeManualPostponed != null && employeeManualPostponed.getId() != null) {
					conformationType = employeeManualPostponed.getPostponeType();
					log.info("conformationType==>" + conformationType);
					entityMaster = employeeManualPostponed.getEntityMaster();
					log.info("entityMaster id==> " + entityMaster.getId());
					// --to load head office/Region office and entity type
					loadHeadRegionOfficeAndEntityType(entityMaster);

					// --to load confirmation postponed details value
					loadConfirmationPostpondDetails(employeeManualPostponed);

					// -- to get confirmation note postponed details
					if (employeeManualPostponed.getEmpManualPostponedNoteList() != null
							&& !employeeManualPostponed.getEmpManualPostponedNoteList().isEmpty()) {
						for (EmpManualPostponedNote empManualPostponedNote : employeeManualPostponed
								.getEmpManualPostponedNoteList()) {
							noteId = empManualPostponedNote.getId();
							forwardToUser = empManualPostponedNote.getUserMaster();
							log.info("forwardToUser id=> " + forwardToUser.getId());
							forwardFor = empManualPostponedNote.getFinalApproval();
							log.info("forwardFor=> " + forwardFor);
							note = empManualPostponedNote.getNote();
							log.info("note=> " + note);
						}
						Long loginUserId = loginBean.getUserMaster().getId();
						Long noteUserId = forwardToUser.getId();

						log.info("loginUserId=> " + loginUserId);
						log.info("noteUserId=> " + noteUserId);
						if (loginUserId.equals(noteUserId)) {
							viewFlag = true;
						} else {
							viewFlag = false;
						}
					} else {
						log.error("Increment note not found");
					}
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_NOT_FOUND).getCode());
					return null;
				}
				// --to get increment log details
				if (employeeManualPostponed.getEmpManualPostponedLogList() != null
						&& !employeeManualPostponed.getEmpManualPostponedLogList().isEmpty()) {
					empManualPostponedLogList = employeeManualPostponed.getEmpManualPostponedLogList();
				} else {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_LOG_NOT_FOUND).getCode());
					return null;
				}

				return VIEW_PAGE;

			}
		} catch (Exception e) {
			log.error("Exception in viewPostpond  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadConfirmationPostpondDetails(EmpManualPostponed empManualPostponed) {
		log.info("<==== Starts ConfirmationPostponeBean.loadConfirmationPostpondDetails =====>");
		try {
			if(empManualPostponed.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_ID_FOUND).getCode());
				return;
			}
			log.info("postpond id==> " + empManualPostponed.getId());
			url = SERVER_URL + "/confirmationpostpone/loadconfirmationpostponddetails/id/:postpondid";
			url = url.replace(":postpondid", empManualPostponed.getId().toString());

			log.info("getEmployeeIncrementDetailsByIncrementId :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					postponedEmployeeDTOList = mapper.readValue(jsonValue,
							new TypeReference<List<PostponedEmployeeDTO>>() {
							});
					if(postponedEmployeeDTOList != null && !postponedEmployeeDTOList.isEmpty()) {
						for(PostponedEmployeeDTO postponedEmployeeDTO : postponedEmployeeDTOList) {
							log.info("emp id==> "+postponedEmployeeDTO.getEmployeeId());
							selectedEmployeeIdList.add(postponedEmployeeDTO.getEmployeeId());
						}
					}
				} else {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
					postponedEmployeeDTOList = new ArrayList<>();
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadConfirmationPostpondDetails  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadHeadRegionOfficeAndEntityType(EntityMaster entityMaster) {
		log.info("<==== Starts ConfirmationPostponeBean.loadHeadRegionOfficeAndEntityType =====>");
		try {
			if (entityMaster == null || entityMaster.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_MASTER_FOUND).getCode());
				return;
			}
			log.info("loadHeadRegionOfficeAndEntityType :: entityMaster id==> " + entityMaster.getId());
			String url = SERVER_URL + "/confirmationpostpone/loadheadofficeregionofficeandentitytype/id/"
					+ entityMaster.getId();
			log.info("loadHeadRegionOfficeAndEntityType :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("viewPostpond :: base dto error code ==> " + baseDTO.getStatusCode());
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				List<HoRoEntitytypeDTO> horoEntitytypeDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<HoRoEntitytypeDTO>>() {
						});
				if (horoEntitytypeDTOList != null && !horoEntitytypeDTOList.isEmpty()) {
					log.info("horoEntitytypeDTOList==> " + horoEntitytypeDTOList);
					Long headRegionOfficeId = horoEntitytypeDTOList.get(0).getHeadRegionOfficeId();
					log.info("headRegionOfficeId==> " + headRegionOfficeId);
					headRegionOfficeName = horoEntitytypeDTOList.get(0).getHeadRegionOfficeName();
					log.info("headRegionOfficeName==> " + headRegionOfficeName);

					headRegionOffice.setId(headRegionOfficeId);
					headRegionOffice.setName(headRegionOfficeName);

					Long entityTypeId = horoEntitytypeDTOList.get(0).getEntityTypeId();
					log.info("entityTypeId==> " + entityTypeId);
					entityTypeName = horoEntitytypeDTOList.get(0).getEntityTypeName();
					log.info("entityTypeName==> " + entityTypeName);
					entityTypeMaster.setId(entityTypeId);
					entityTypeMaster.setEntityName(entityTypeName);
				} else {
					log.error("Head / Region office and Entity type not found on : " + entityMaster.getId());
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadHeadRegionOfficeAndEntityType  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadLazyConfirmationPostpondList() {
		log.info("<==== Starts ConfirmationPostponeBean.loadLazyConfirmationPostpondList =====>");
		postpondListDTOLazyList = new LazyDataModel<PostpondListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<PostpondListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/confirmationpostpone/loadpospondlist";
					log.info("loadLazyIncrementList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						postpondListDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<PostpondListDTO>>() {
								});
						log.info("loadLazyConfirmationPostpondList :: postpondListDTOList size==> "
								+ postpondListDTOList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyConfirmationPostpondList :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return postpondListDTOList;
			}

			@Override
			public Object getRowKey(PostpondListDTO res) {
				return res != null ? res.getPostpondId() : null;
			}

			@Override
			public PostpondListDTO getRowData(String rowKey) {
				for (PostpondListDTO postpondListDTO : postpondListDTOList) {
					if (postpondListDTO.getPostpondId().equals(Long.valueOf(rowKey))) {
						selectedPostpondListDTO = postpondListDTO;
						return postpondListDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedPostpondListDTO = ((PostpondListDTO) event.getObject());
		log.info("selectedPostpondListDTO getStatus ==> " + selectedPostpondListDTO.getStatus());
		if (selectedPostpondListDTO.getStatus().equalsIgnoreCase("APPROVED") || selectedPostpondListDTO.getStatus().equalsIgnoreCase("FINAL-APPROVED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedPostpondListDTO.getStatus().equalsIgnoreCase("REJECTED")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = true;
			isDeleteButton = true;
		} else if (selectedPostpondListDTO.getStatus().equalsIgnoreCase("SUBMITTED")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = false;
			isDeleteButton = true;
		}
	}

	public void loadEmployeeDetails() {
		log.info("<=ConfirmationPostponeBean :: loadEmployeeDetails==>");
		try {
			if (conformationType == null || conformationType.isEmpty()) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.POSTPONE_TYPE_FOUND).getCode());
				return;
			}
			if (headRegionOffice == null || headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return;
			}
			if (entityTypeMaster == null || entityTypeMaster.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_TYPE_FOUND).getCode());
				return;
			}
			if (entityMaster == null || entityMaster.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
				return;
			}
			if (selectedEmployeeIdList == null && !selectedEmployeeIdList.isEmpty()) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.SELECTED_THE_EMPLOYEE).getCode());
				return;
			}
			log.info("selectedEmployeeIdList==> " + selectedEmployeeIdList.size());
			url = SERVER_URL + "/confirmationpostpone/loademployeedetailbyemployeeid";

			log.info("loadEmployeeByEntity :: url ==> " + url);
			BaseDTO baseDTO = httpService.post(url, selectedEmployeeIdList);
			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					postponedEmployeeDTOList = mapper.readValue(jsonValue,
							new TypeReference<List<PostponedEmployeeDTO>>() {
							});
					if (postponedEmployeeDTOList != null && !postponedEmployeeDTOList.isEmpty()) {
						log.info("ConfirmationPostponeBean :: postponedEmployeeDTOList==> "
								+ postponedEmployeeDTOList.size());
						for (PostponedEmployeeDTO postponedEmployeeDTO : postponedEmployeeDTOList) {
							log.info("conformationType ==> " + conformationType);
							postponedEmployeeDTO.setConformationType(conformationType);
							if (conformationType.equals(ConformationTypeEnum.Confirmation.toString())) {
								log.info("Confirmation date==> " + postponedEmployeeDTO.getDateOfConfirmation());
								postponedEmployeeDTO.setActualDate(postponedEmployeeDTO.getDateOfConfirmation());
							} else if (conformationType.equals(ConformationTypeEnum.Increment.toString())) {
								log.info("Increment date==> " + postponedEmployeeDTO.getNextIncrementDate());
								postponedEmployeeDTO.setActualDate(postponedEmployeeDTO.getNextIncrementDate());
							} else if (conformationType.equals(ConformationTypeEnum.Promotion.toString())) {
								log.info("Promotion date==> " + postponedEmployeeDTO.getNextPromotionDate());
								postponedEmployeeDTO.setActualDate(postponedEmployeeDTO.getNextPromotionDate());
							}
							log.info("Actual date==> " + postponedEmployeeDTO.getActualDate());
						}
					} else {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INFORMATION_NOT_FOUND).getCode());
						return;
					}

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}

		} catch (Exception e) {
			log.error("Exception in loadEmployeeDetails  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void clearEmployeeDetails() {

		postponedEmployeeDTOList = new ArrayList<>();
		employeeMasterList = new ArrayList<>();
		conformationType = null;
		headRegionOffice = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		selectedEmployeeIdList = new ArrayList<>();
		note = null;
	}

	public String cancalConfirmationPostpone() {
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		addEditFlag = true;
		postponedEmployeeDTOList = new ArrayList<>();
		employeeMasterList = new ArrayList<>();
		conformationType = null;
		headRegionOffice = new EntityMaster();
		selectedEmployeeIdList = new ArrayList<>();
		selectedPostpondListDTO = new PostpondListDTO();
		entityTypeMaster = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		forwardToUser = new UserMaster();
		forwardFor = null;
		note = null;
		return LIST_PAGE;
	}

	public String deleteConfirmationPostpone() {
		try {

			if (selectedPostpondListDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("Selected Employee  Id : : " + selectedPostpondListDTO.getPostpondId());
			String getUrl = SERVER_URL + "/confirmationpostpone/deletepostpondbyid/id/:id";
			url = getUrl.replace(":id", selectedPostpondListDTO.getPostpondId().toString());
			BaseDTO baseDTO = httpService.delete(url);
			log.info("deleteConfirmationPostpone :: url ==>" + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Postpond deleted successFully ");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_DELETED_SUCCESSFULLY).getCode());
				return null;
			} else {
				log.error("Postpond Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			isAddButton = true;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
			addEditFlag = false;
			cancalConfirmationPostpone();
			loadLazyConfirmationPostpondList();

		} catch (Exception e) {
			log.error("Exception Occured While postpond  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;
	}

	public String savePostpone() {
		log.info("<=ConfirmationPostponeBean :: savePostpone==>");
		try {
			if (conformationType == null || conformationType.isEmpty()) {

				errorMap.notify(ErrorDescription.getError(OperationErrorCode.POSTPONE_TYPE_FOUND).getCode());
				return null;
			}
			if (headRegionOffice == null || headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return null;
			}
			if (entityTypeMaster == null || entityTypeMaster.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_TYPE_FOUND).getCode());
				return null;
			}
			if (entityMaster == null || entityMaster.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.ENTITY_NOT_FOUND).getCode());
				return null;
			}
			if (forwardToUser == null || forwardToUser.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
				return null;
			}
			if (forwardFor == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_FOR_NOT_FOUND).getCode());
				return null;
			}
			if (pageAction.equals("EDIT")) {
				return updateConfirmationPostpone();
			} else {
				return saveConfirmationPostpone();
			}
		} catch (Exception e) {
			log.error("Exception Occured While savePostpone  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String updateConfirmationPostpone() {
		log.info("<=ConfirmationPostponeBean :: updateConfirmationPostpone==>");
		try {
			if (employeeManualPostponed == null && employeeManualPostponed.getId() == null) {
				
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_NOT_FOUND).getCode());
				return null;
			}
			UpdatePostponedDTO updatePostponedDTO  = new UpdatePostponedDTO();
			updatePostponedDTO.setEmployeeManualPostponed(employeeManualPostponed);
			updatePostponedDTO.setPostponedEmployeeDTO(postponedEmployeeDTOList);
			
			List<EmpManualPostponedNote> empManualPostponedNoteList = new ArrayList<>();
			EmpManualPostponedNote empManualPostponedNote = new EmpManualPostponedNote();
			log.info("saveConfirmationPostpone: note==> " + note);
			if (pageAction.equals("EDIT")) {
				empManualPostponedNote.setId(noteId);
			}
			if(!note.isEmpty()) {
			empManualPostponedNote.setNote(note);
			}else {
			 empManualPostponedNote.setNote(updateNote);
			}
			
			empManualPostponedNote.setEmpManualPostponed(employeeManualPostponed);
			log.info("saveConfirmationPostpone: forwardToUser id==> " + forwardToUser.getId());
			empManualPostponedNote.setUserMaster(forwardToUser);
			log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
			empManualPostponedNote.setFinalApproval(forwardFor);
			empManualPostponedNoteList.add(empManualPostponedNote);
			
			updatePostponedDTO.setEmpManualPostponedNoteList(empManualPostponedNoteList);
			
			List<EmpManualPostponedLog> empManualPostponedLogList = new ArrayList<>();
			EmpManualPostponedLog empManualPostponedLog = new EmpManualPostponedLog();
			empManualPostponedLog.setEmpManualPostponed(employeeManualPostponed);
			empManualPostponedLog.setStage(ApprovalStage.SUBMITTED);
			empManualPostponedLogList.add(empManualPostponedLog);
			
			updatePostponedDTO.setEmpManualPostponedLogList(empManualPostponedLogList);
			String url = SERVER_URL + "/confirmationpostpone/updateconfirmationpostpone";
			log.info("saveConfirmationPostpone :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, updatePostponedDTO);
			log.info("saveConfirmationPostpone Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			mapper.readValue(jsonResponse, EmpIncrement.class);
			if (baseDTO.getStatusCode() == 0) {
				cancalConfirmationPostpone();
				loadLazyConfirmationPostpondList();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_UPDATED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving Confirmation Postpone with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception Occured While postpond  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String saveConfirmationPostpone() {
		log.info("<=ConfirmationPostponeBean :: saveConfirmationPostpone==>");
		try {
			
			log.info("saveConfirmationPostpone: conformationType==> " + conformationType);
			log.info("saveConfirmationPostpone: entityMaster id==> " + entityMaster.getId());
			if (employeeManualPostponed == null && employeeManualPostponed.getId() == null) {
				employeeManualPostponed = new EmpManualPostponed();
			}
			employeeManualPostponed.setPostponeType(conformationType);
			employeeManualPostponed.setEntityMaster(entityMaster);
			List<EmpManualPostponedDetails> empManualPostponedDetailList = new ArrayList<>();
			if (postponedEmployeeDTOList != null && !postponedEmployeeDTOList.isEmpty()) {
				for (PostponedEmployeeDTO postponedEmployeeDTO : postponedEmployeeDTOList) {
					EmpManualPostponedDetails empManualPostponedDetails = new EmpManualPostponedDetails();
					EmployeeMaster employeeMaster = new EmployeeMaster();

					log.info("saveConfirmationPostpone: employee id==> " + postponedEmployeeDTO.getEmployeeId());
					log.info("saveConfirmationPostpone: employeeMaster id==> " + employeeMaster.getId());
					log.info("saveConfirmationPostpone: actual date==> " + postponedEmployeeDTO.getActualDate());
					log.info("saveConfirmationPostpone: postpond date==> " + postponedEmployeeDTO.getPostponedReason());
					log.info("saveConfirmationPostpone: reason==> " + postponedEmployeeDTO.getPostponedReason());

					employeeMaster.setId(postponedEmployeeDTO.getEmployeeId());
					empManualPostponedDetails.setEmpManualPostponed(employeeManualPostponed);
					empManualPostponedDetails.setEmpMaster(employeeMaster);
					empManualPostponedDetails.setActualDate(postponedEmployeeDTO.getActualDate());
					empManualPostponedDetails.setPostponedDate(postponedEmployeeDTO.getPostponeDate());
					empManualPostponedDetails.setReason(postponedEmployeeDTO.getPostponedReason());
					empManualPostponedDetailList.add(empManualPostponedDetails);
				}
				employeeManualPostponed.setEmpManualPostponedDetailsList(empManualPostponedDetailList);
			} else {

				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.EMPLOYEE_NOT_FOUND).getCode());
				return null;
			}
			List<EmpManualPostponedNote> empManualPostponedNoteList = new ArrayList<>();
			EmpManualPostponedNote empManualPostponedNote = new EmpManualPostponedNote();
			log.info("saveConfirmationPostpone: note==> " + note);
			if (pageAction.equals("EDIT")) {
				empManualPostponedNote.setId(noteId);
			}
			empManualPostponedNote.setNote(note);
			empManualPostponedNote.setEmpManualPostponed(employeeManualPostponed);
			log.info("saveConfirmationPostpone: forwardToUser id==> " + forwardToUser.getId());
			empManualPostponedNote.setUserMaster(forwardToUser);
			log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
			empManualPostponedNote.setFinalApproval(forwardFor);
			empManualPostponedNoteList.add(empManualPostponedNote);
			employeeManualPostponed.setEmpManualPostponedNoteList(empManualPostponedNoteList);

			List<EmpManualPostponedLog> empManualPostponedLogList = new ArrayList<>();
			EmpManualPostponedLog empManualPostponedLog = new EmpManualPostponedLog();
			empManualPostponedLog.setEmpManualPostponed(employeeManualPostponed);
			empManualPostponedLog.setStage(ApprovalStage.SUBMITTED);
			empManualPostponedLogList.add(empManualPostponedLog);
			employeeManualPostponed.setEmpManualPostponedLogList(empManualPostponedLogList);

			String url = SERVER_URL + "/confirmationpostpone/saveconfirmationpostpone";
			log.info("saveConfirmationPostpone :: url==> " + url);
			BaseDTO baseDTO = httpService.post(url, employeeManualPostponed);
			log.info("saveConfirmationPostpone Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			mapper.readValue(jsonResponse, EmpIncrement.class);
			if (baseDTO.getStatusCode() == 0) {
				cancalConfirmationPostpone();
				loadLazyConfirmationPostpondList();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_INSERTED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving Confirmation Postpone with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in saveConfirmationPostpone  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose : to save approved or rejected status in log table
	 * 
	 * @param status
	 * @return
	 */
	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> " + status);
			log.info("postpondRemarks==> " + postpondRemarks);
			log.info("logList id==> " + empManualPostponedLogList.get(0).getId());
			log.info("noteId==> " + noteId);
			log.info("forwardFor==> " + forwardFor);
			PostpondForwardDTO postpondForwardDTO = new PostpondForwardDTO();
			postpondForwardDTO.setLogList(empManualPostponedLogList);
			postpondForwardDTO.setForwardRemarks(postpondRemarks);
			postpondForwardDTO.setEmpManualPostponed(employeeManualPostponed);
			postpondForwardDTO.setNoteId(noteId);
			log.info("forward user id==> " + forwardToUser.getId());
			postpondForwardDTO.setForwardUserId(forwardToUser.getId());
			postpondForwardDTO.setForwardFor(forwardFor);
			if (status.equals(REJECT)) {
				postpondForwardDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals(APPROVED)) {
				postpondForwardDTO.setStatus(ApprovalStage.APPROVED);
			} else {
				postpondForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + "/confirmationpostpone/updatepostpondlog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, postpondForwardDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				cancalConfirmationPostpone();
				loadLazyConfirmationPostpondList();
				errorMap.notify(
						ErrorDescription.getError(OperationErrorCode.CONFIRMATION_POSTPONE_LOG_INSERTED_SUCCESSFULLY).getCode());
				return LIST_PAGE;
			} else {
				log.info("Error while saving ModernizationRequest with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_PAGE;
	}
	
	@Getter
	@Setter
	public List<UserMaster> autoEmployeeMasterList = new ArrayList<UserMaster>();

	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(loginBean.getServerURL(), query);
		return autoEmployeeMasterList;
	}

}
