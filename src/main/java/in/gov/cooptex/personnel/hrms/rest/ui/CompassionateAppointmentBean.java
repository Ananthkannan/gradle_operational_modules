package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.IncomingTapalStatus;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CompassionateAppointmentDTO;
import in.gov.cooptex.core.dto.CompassionateAppointmentResponseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.CompassionateAppointment;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.enums.CommonForwardStatus;
import in.gov.cooptex.personnel.hrms.dto.IncomingTapalResponseDto;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("compassionateAppointmentBean")
@Scope("session")
public class CompassionateAppointmentBean extends CommonBean implements Serializable {

	ObjectMapper mapper;
	String jsonResponse;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	long documentSize;

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Autowired
	CommonDataService commonDataService;
	@Getter
	@Setter
	String address = "";
	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;
	
	@Getter
	@Setter
	CompassionateAppointmentDTO compassionateAppointmentDTO;

	@Getter
	@Setter
	private StreamedContent currentlyWorkingFile, willingToWorkAnyPostFile, willingToRelocateFile,
			finalSettlementMadeFile, dethCerFile, legalHeirCerFile, noObjectionCerFile, birthCerFile, transferCerFile,
			requisitionLetterFromEmpFile, requisitionLetterFromAppliFile, outstandingDueFromEmpFile;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	CompassionateAppointmentResponseDTO selectedCompassionateAppointment;

	@Getter
	@Setter
	CompassionateAppointment compassionateAppointment;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String empCode;

	@Getter
	@Setter
	List<GenderMaster> genderList;

	@Getter
	@Setter
	List<RelationshipMaster> relationList;

	@Getter
	@Setter
	List<QualificationMaster> qualificationList;

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	LazyDataModel<CompassionateAppointmentResponseDTO> compassionateAppointmentLazyList;

	@Getter
	@Setter
	List<EntityMaster> locationList;

	@Getter
	@Setter
	List<MaritalStatus> maritalStatusList;
	@Autowired
	AddressMasterBean addressMasterBean = new AddressMasterBean();
	@Getter
	@Setter
	Date today;
	
	@Getter
	@Setter
	private List<UserMaster> userMasters = new ArrayList<UserMaster>();
	
	@Getter
	@Setter
	UserMaster selectedUserMaster = new UserMaster();

	@Getter
	@Setter
	private UploadedFile currentlyWorkingCer, willingToWorkAnyPostCer, willingToRelocateCer, finalSettlementMadeCer,
			dethCer, legalHeirCer, noObjectionCer, finIndigentCer, incomeCer, birthCer, transferCer,
			requisitionLetterFromEmpCer, requisitionLetterFromAppliCer, outstandingDueFromEmpCertificate,
			qualificationCer;

	@Setter
	@Getter
	String currentlyWorkingFileName, willingToWorkAnyPostFileName, willingToRelocateFileName,
			finalSettlementMadeFileName, dethFileName, legalHeirFileName, noObjectionFileName, finIndigentFileName,
			incomeFileName, birthFileName, transferFileName, requisitionLetterFromFileName,
			requisitionLetterFromAppliFileName, outstandingDueFromEmpCerFileName, fileName = "", qualificationFileName;

	@Getter
	@Setter
	String uploadFile;

	@Getter
	@Setter
	String docType;
	
	@Getter
	@Setter
	int additionalInfoIncrement = 6;
	
	@Getter
	@Setter
	private Boolean previousApproval = false;
	
	@Getter
	@Setter
	Boolean approvalFlag = false;
	
	@Getter
	@Setter
	Boolean finalapprovalFlag = false;
	
	@Getter
	@Setter
	Boolean hideFlag = false;
	
	@Getter
	@Setter
	Boolean rejectFlag = false;
	
	@Getter
	@Setter
	Boolean disabledMonth = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;
	
	@Getter
	@Setter
	Boolean buttonFlag = true;
	
	@Setter
	@Getter
	private StreamedContent file;
	
	@Getter
	@Setter
	List<CompassionateAppointmentResponseDTO> compassionateResponseList;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	@Getter
	@Setter
	EmployeeMaster loginEmployeeMaster;
	
	@Getter
	@Setter
	String documentPath ;
	
	@Getter
	@Setter
	UserMaster forwadtousermaster;

	@PostConstruct
	public void init() {
		log.info("CompassionateAppointmentBean Init is executed.....................");
		compassionateAppointmentDTO=new CompassionateAppointmentDTO();
		loadAppConfigValues();
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			documentSize = Long
					.valueOf(commonDataService.getAppKeyValue(AppConfigKey.COMPASSIONATE_APPOINTMENT_DOC_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}
	
	

	public String loadCompasionateAppointmentList() {
		log.info("<-----Loading compassionate appoinment list page----->");
		mapper = new ObjectMapper();
		action = "LIST";
		addButtonFlag = false;
		viewButtonFlag = true;
		deleteButtonFlag = true;
		editButtonFlag = true;
		approveButtonFlag = false;
		empCode = null;

		genderList = new ArrayList<GenderMaster>();
		relationList = new ArrayList<RelationshipMaster>();
		qualificationList = new ArrayList<QualificationMaster>();
		designationList = new ArrayList<Designation>();
		locationList = new ArrayList<EntityMaster>();
		maritalStatusList = new ArrayList<MaritalStatus>();
		selectedCompassionateAppointment = new CompassionateAppointmentResponseDTO();
		compassionateAppointment = new CompassionateAppointment();

		currentlyWorkingCer = willingToWorkAnyPostCer = willingToRelocateCer = finalSettlementMadeCer = dethCer = legalHeirCer = noObjectionCer = birthCer = transferCer = requisitionLetterFromEmpCer = requisitionLetterFromAppliCer = outstandingDueFromEmpCertificate = null;

		currentlyWorkingFileName = willingToWorkAnyPostFileName = willingToRelocateFileName = finalSettlementMadeFileName = dethFileName = legalHeirFileName = noObjectionFileName = finIndigentFileName = incomeFileName = birthFileName = transferFileName = requisitionLetterFromFileName = requisitionLetterFromAppliFileName = outstandingDueFromEmpCerFileName = fileName = "";

		today = new Date();
		/*
		 * loadMasterValues();
		 */
		loadLazyCompassionateList();
		return "/pages/personnelHR/compassionateAppointment/listCompassionateAppointment.xhtml?faces-redirect=true";
	}

	/*public void loadCompassionateAppointmentLazy() {
		compassionateAppointmentLazyList = new LazyDataModel<CompassionateAppointment>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<CompassionateAppointment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Response retrurn null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<CompassionateAppointment> compassionateAppointmentList = mapper.readValue(jsonResponse,
							new TypeReference<List<CompassionateAppointment>>() {
							});
					if (compassionateAppointmentList == null) {
						log.info(" Compassionate appointment list is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Compassionate appointment list search Successfully Completed");
					} else {
						log.error(" Compassionate appointment list search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return compassionateAppointmentList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Compassionate appointment :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(CompassionateAppointment res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CompassionateAppointment getRowData(String rowKey) {
				List<CompassionateAppointment> responseList = (List<CompassionateAppointment>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (CompassionateAppointment res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedCompassionateAppointment = res;
						return res;
					}
				}
				return null;
			}
		};
	}
	*/
	public void loadLazyCompassionateList() {

		log.info("<===== loadLazyIncomingTapalList() Starts ======>");

		compassionateAppointmentLazyList = new LazyDataModel<CompassionateAppointmentResponseDTO>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -206372230414855092L;

			@Override

			public List<CompassionateAppointmentResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url = SERVER_URL + "/compassionateappointment/getAllCompassionate";
					
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						compassionateResponseList = mapper.readValue(jsonResponse, new TypeReference<List<CompassionateAppointmentResponseDTO>>() {
						});
						log.info("list----------" + compassionateResponseList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = compassionateResponseList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyCompassionate List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return compassionateResponseList;
			}

			@Override
			public Object getRowKey(CompassionateAppointmentResponseDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CompassionateAppointmentResponseDTO getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<CompassionateAppointmentResponseDTO> responseList = (List<CompassionateAppointmentResponseDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (CompassionateAppointmentResponseDTO compassionate : responseList) {
					if (compassionate.getId().longValue() == value.longValue()) {
						selectedCompassionateAppointment = compassionate;
						return compassionate;
					}
				}
				return null;
			}
		};
		log.info("<===== loadLazyIncomingTapalList() Ends ======>");

	}

	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			CompassionateAppointment request = new CompassionateAppointment();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("referenceNumber")) {
					log.info("Reference Number: " + value);
					searchFilters.put("referenceNumber", value);
				}
				if (filter.getKey().equals("employee.personalInfoEmployment.headAndRegionOffice.name")) {
					log.info("Entity name : " + value);
					searchFilters.put("entityName", value);
				}
				if (filter.getKey().equals("legalHierName")) {
					log.info("legalHierName: " + value);
					searchFilters.put("legalHierName", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("Created Date: " + value);
					searchFilters.put("createdDate", value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status", value);
				}
			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/compassionateappointment/loadlazylist", request);

		} catch (Exception e) {
			log.error("Exception Occured in compassionate appoinment load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String compassionateAppointmentListAction() {
		try {
			locationList = commonDataService.loadRegionalOffice();
			userMasters = getForwardToUsersList();
			loginEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			if (action.equalsIgnoreCase("ADD")) {
				log.info("Loading compassionate appointment create page.....");
				compassionateAppointment = new CompassionateAppointment();
				addressMasterBean.setAddressMaster(new AddressMaster());
				setQualificationFileName("");
				compassionateAppointmentDTO.setNote("");
				address = "";
				loadMasterValues();
				return "/pages/personnelHR/compassionateAppointment/createCompassionateAppointment.xhtml?faces-redirect=true";
			} else {
				if (selectedCompassionateAppointment == null) {
					errorMap.notify(ErrorDescription.SELECT_COMPASSIONATE_APPOINTMENT.getErrorCode());
					return null;
				}

				if (action.equalsIgnoreCase("DELETE")) {
					if (selectedCompassionateAppointment.getStatus().equalsIgnoreCase("Approved")) {
						errorMap.notify(
								ErrorDescription.APPROVED_COMPASSIONATE_APPOINTMENT_CANNOT_DELETE.getErrorCode());
						return null;
					}
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmAppointmenrDelete').show();");
				}

				else {

					log.info("<----Loading  page.....---->" + selectedCompassionateAppointment.getId());
					compassionateAppointment = new CompassionateAppointment();
					BaseDTO response = httpService.get(SERVER_URL + "/compassionateappointment/get/id/"
							+ selectedCompassionateAppointment.getId());
					
					if (response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						compassionateAppointmentDTO = mapper.readValue(jsonResponse,
								new TypeReference<CompassionateAppointmentDTO>() {
								});
						compassionateAppointment=compassionateAppointmentDTO.getCompassionateAppointment();
						if(compassionateAppointment!=null && compassionateAppointment.getEmployee()!=null && compassionateAppointment.getEmployee().getEmpCode()!=null)
							empCode = compassionateAppointment.getEmployee().getEmpCode();
						fileNameSet();
						
						if (action.equalsIgnoreCase("VIEW")) {
							locationList = commonDataService.loadRegionalOffice();
							compassionateAppointmentDTO.setRemarks("");
							addressMasterBean.setAddressMaster(compassionateAppointment.getAddressMaster());
							address = addressMasterBean.prepareAddress(compassionateAppointment.getAddressMaster());
							log.info("forwardtoooo"+ compassionateAppointmentDTO.getForwardTo());
							if (compassionateAppointmentDTO.getForwardTo() != null) {
								previousApproval=compassionateAppointmentDTO.getForwardFor();
								log.info("<<=== PreviousApproval...."+previousApproval);
								if (loginBean.getUserMaster().getId().equals(compassionateAppointmentDTO.getForwardTo())) {
									
									approvalFlag=true;
								    hideFlag=true;
		
									if(compassionateAppointmentDTO.getForwardFor()) {
										buttonFlag=false;
									}else {
										buttonFlag=true;
									}
									log.info("forwarddd"+ compassionateAppointmentDTO.getForwardFor());
									if (CommonForwardStatus.REJECTED.toString().equals(compassionateAppointmentDTO.getStage()) || 
											CommonForwardStatus.FINAL_APPROVED.toString().equals(compassionateAppointmentDTO.getStage())) {
										approvalFlag = false;
										buttonFlag=false;
										rejectFlag=true;
									} 
									
									
									else {
										approvalFlag = true;
									}
								} else {
									approvalFlag = false;
								}
								if(compassionateAppointmentDTO.getForwardFor()==false) {
									finalapprovalFlag = false;	
									approvalFlag=false;
									rejectFlag=true;
								}
								if(compassionateAppointmentDTO.getForwardFor()==true && loginBean.getUserMaster().getId().equals(compassionateAppointmentDTO.getForwardTo())) {
									finalapprovalFlag = true;	
									  hideFlag=true;
									rejectFlag=true;
								}
								
								if(compassionateAppointmentDTO.getForwardFor()==true && !loginBean.getUserMaster().getId().equals(compassionateAppointmentDTO.getForwardTo())) {
									finalapprovalFlag = true;	
									  hideFlag=true;
									  rejectFlag=false;
								}
								
								else if(compassionateAppointmentDTO.getForwardFor()==false && loginBean.getUserMaster().getId().equals(compassionateAppointmentDTO.getForwardTo())) {
									finalapprovalFlag = false;	
									  hideFlag=true;
								}
								
								if (CommonForwardStatus.REJECTED.toString().equals(compassionateAppointmentDTO.getStage()) || 
										CommonForwardStatus.FINAL_APPROVED.toString().equals(compassionateAppointmentDTO.getStage())) {
									  rejectFlag=false;
								}
//								
//								else {
//									finalapprovalFlag = true;	
//									  hideFlag=false;
//								}
							
							}
						
							return "/pages/personnelHR/compassionateAppointment/viewCompassionateAppointment.xhtml?faces-redirect=true";
						} else if (action.equalsIgnoreCase("APPROVE")) {
							return "/pages/personnelHR/compassionateAppointment/approvalCompassionateAppointment.xhtml?faces-redirect=true";
						} else if (action.equalsIgnoreCase("EDIT")) {
							loadMasterValues();
							addressMasterBean.setAddressMaster(new AddressMaster());
							addressMasterBean.setAddressMaster(compassionateAppointment.getAddressMaster());
							address = addressMasterBean.prepareAddress(compassionateAppointment.getAddressMaster());
							/*if (compassionateAppointmentDTO.getForwardTo() != null) {
								if (loginBean.getUserMaster().getId().equals(compassionateAppointmentDTO.getForwardTo())) {
									approveButtonFlag=true;
									if (CommonForwardStatus.REJECTED.toString().equals(compassionateAppointmentDTO.getStage())) {
										approvalFlag = false;
									} else {
										approvalFlag = true;
									}
								} else {
									approvalFlag = false;
								}
							}*/
							
							return "/pages/personnelHR/compassionateAppointment/createCompassionateAppointment.xhtml?faces-redirect=true";
						}
					} else {
						errorMap.notify(response.getStatusCode());
						return null;
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured in compassionate appointment action::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void saveAddress() {
		
		compassionateAppointment.setAddressMaster(addressMasterBean.getAddressMaster());
		address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("compassionateform");

	}


	public String confirmTemporaryEngagement() {
		log.info("... delete compassionate appontment called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.delete(
					SERVER_URL + "/compassionateappointment/delete/" + selectedCompassionateAppointment.getId());
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadCompasionateAppointmentList();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedCompassionateAppointment = ((CompassionateAppointmentResponseDTO) event.getObject());
		addButtonFlag = true;
		approveButtonFlag=false;
		if (selectedCompassionateAppointment.getStatus().equalsIgnoreCase(CommonForwardStatus.REJECTED.toString())) {
			viewButtonFlag = false;
			editButtonFlag = false;
			deleteButtonFlag = false;
		}
		if(selectedCompassionateAppointment.getStatus().equalsIgnoreCase(CommonForwardStatus.FINAL_APPROVED.toString())) {
			viewButtonFlag = false;
			editButtonFlag = true;
			deleteButtonFlag = true;
			approveButtonFlag=true;
		} else if(!selectedCompassionateAppointment.getStatus().equalsIgnoreCase(CommonForwardStatus.REJECTED.toString())
				&& !selectedCompassionateAppointment.getStatus().equalsIgnoreCase(CommonForwardStatus.FINAL_APPROVED.toString())) {
			viewButtonFlag = false;
			deleteButtonFlag = true;
			editButtonFlag = true;
		} 
	}

	public void searchEmpployee() {
		log.info("Searching employee code--->" + empCode);
		CompassionateAppointment CompassionateAppt = new CompassionateAppointment();
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/compassionateappointment/inactiveEmp/" + empCode);
			if (response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				CompassionateAppt = mapper.readValue(jsonResponse, new TypeReference<CompassionateAppointment>() {
				});
				compassionateAppointment.setEmployee(CompassionateAppt.getEmployee());
				compassionateAppointment.setDateOfDeath(CompassionateAppt.getDateOfDeath());
			} else {
				errorMap.notify(response.getStatusCode());
				return;
			}
		} catch (Exception e) {
			log.error("Exception occured while loading employee....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}
	
	public void fileUploaderLisner(FileUploadEvent event) {
		String uploadFile = (String) event.getComponent().getAttributes().get("uploadName");
		log.info("uploadFile----->" + uploadFile);
		try {
        // documentPath =  commonDataService.getAppKeyValue("COMPANSIONATE_APPTMT_UPLOAD_PATH") +"/"+uploadFile;
			switch (uploadFile) {
			case "CURRENT_WORKING": {
				currentlyWorkingCer = event.getFile();
				long size = currentlyWorkingCer.getSize();
				log.info("currentlyWorkingCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!currentlyWorkingCer.getContentType().contains("pdf") ) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload PDF OR Image(PNG,JPG) File");
					return;
				}
				currentlyWorkingFileName = currentlyWorkingCer.getFileName();
				fileName = fileName + "," + currentlyWorkingFileName;
//				String docPath = Util.fileUpload(documentPath, currentlyWorkingCer);
//				compassionateAppointment.setCurrentlyWorkingCerPath(docPath);
				compassionateAppointment.setCurrentlyWorkingCer(currentlyWorkingCer.getContents());
				compassionateAppointment.setCurrentlyWorkingFileName(currentlyWorkingFileName);
				break;

			}
			case "WORK_ANY_POST": {
				willingToWorkAnyPostCer = event.getFile();
				long size = willingToWorkAnyPostCer.getSize();
				log.info("willingToWorkAnyPostCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!willingToWorkAnyPostCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				willingToWorkAnyPostFileName = willingToWorkAnyPostCer.getFileName();
				fileName = fileName + "," + willingToWorkAnyPostFileName;
//				String docPath = Util.fileUpload(documentPath, willingToWorkAnyPostCer);
//				compassionateAppointment.setWillingToWorkAnyPostCerPath(docPath);
				compassionateAppointment.setWillingToWorkAnyPostCer(willingToWorkAnyPostCer.getContents());
				compassionateAppointment.setWillingToWorkAnyPostFileName(willingToWorkAnyPostFileName);
				break;
			}

			case "RELOCATE": {
				willingToRelocateCer = event.getFile();
				long size = willingToRelocateCer.getSize();
				log.info("willingToRelocateCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!willingToRelocateCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				willingToRelocateFileName = willingToRelocateCer.getFileName();
				fileName = fileName + "," + willingToRelocateFileName;
//				String docPath = Util.fileUpload(documentPath, willingToRelocateCer);
//				compassionateAppointment.setWillingToRelocateCerPath(docPath);
				compassionateAppointment.setWillingToRelocateCer(willingToRelocateCer.getContents());
				compassionateAppointment.setWillingToRelocateFileName(willingToRelocateFileName);
				break;
			}

			case "FINAL_SETTLEMENT": {
				finalSettlementMadeCer = event.getFile();
				long size = finalSettlementMadeCer.getSize();
				log.info("finalSettlementMadeCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!finalSettlementMadeCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				finalSettlementMadeFileName = finalSettlementMadeCer.getFileName();
				fileName = fileName + "," + finalSettlementMadeFileName;
//				String docPath = Util.fileUpload(documentPath, finalSettlementMadeCer);
//				compassionateAppointment.setFinalSettlementMadeCerPAth(docPath);
				compassionateAppointment.setFinalSettlementMadeCer(finalSettlementMadeCer.getContents());
				compassionateAppointment.setFinalSettlementMadeFileName(finalSettlementMadeFileName);
				break;
			}

			case "DEATH_CERTIFICATE": {
				dethCer = event.getFile();
				long size = dethCer.getSize();
				log.info("dethCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!dethCer.getContentType().contains("pdf") ) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload PDF OR Image(PNG,JPG) File");
					return;
				} 
				
				dethFileName = dethCer.getFileName();
				fileName = fileName + "," + dethFileName;
//				String docPath = Util.fileUpload(documentPath, dethCer);
//				compassionateAppointment.setDethCerPath(docPath);
				compassionateAppointment.setDethCer(dethCer.getContents());
				compassionateAppointment.setDethFileName(dethFileName);
				break;
			}

			case "QUALIFICATION_CERTIFICATE": {
				qualificationCer = event.getFile();
				long size = qualificationCer.getSize();
				log.info("qualificationCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!qualificationCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				qualificationFileName = qualificationCer.getFileName();
				fileName = fileName + "," + qualificationFileName;
//				String docPath = Util.fileUpload(documentPath, qualificationCer);
//				compassionateAppointment.setQualificationCerPath(docPath);
				compassionateAppointment.setQualificationCer(qualificationCer.getContents());
				compassionateAppointment.setQualificationFileName(qualificationFileName);
				break;
			}

			case "LEGAL_HEIR": {
				legalHeirCer = event.getFile();
				long size = legalHeirCer.getSize();
				log.info("legalHeirCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!legalHeirCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				legalHeirFileName = legalHeirCer.getFileName();
				fileName = fileName + "," + legalHeirFileName;
//				String docPath = Util.fileUpload(documentPath, legalHeirCer);
//				compassionateAppointment.setLegalHeirCerPath(docPath);
				compassionateAppointment.setLegalHeirCer(legalHeirCer.getContents());
				compassionateAppointment.setLegalHeirFileName(legalHeirFileName);
				break;
			}
			case "NO_OBJECTION": {
				noObjectionCer = event.getFile();
				long size = noObjectionCer.getSize();
				log.info("noObjectionCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!noObjectionCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				noObjectionFileName = noObjectionCer.getFileName();
				fileName = fileName + "," + noObjectionFileName;
//				String docPath = Util.fileUpload(documentPath, noObjectionCer);
//				compassionateAppointment.setNoObjectionCerPath(docPath);
				compassionateAppointment.setNoObjectionCer(noObjectionCer.getContents());
				compassionateAppointment.setNoObjectionFileName(noObjectionFileName);
				break;
			}
			case "BIRTH_CERTIFICATE": {
				birthCer = event.getFile();
				long size = birthCer.getSize();
				log.info("birthCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!birthCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				birthFileName = birthCer.getFileName();
				fileName = fileName + "," + birthFileName;
//				String docPath = Util.fileUpload(documentPath, birthCer);
//				compassionateAppointment.setBirthCerPath(docPath);
				compassionateAppointment.setBirthCer(birthCer.getContents());
				compassionateAppointment.setBirthFileName(birthFileName);
				break;
			}
			case "TRANSFER_CERTIFICATE": {
				transferCer = event.getFile();
				long size = transferCer.getSize();
				log.info("transferCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!transferCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				transferFileName = transferCer.getFileName();
				fileName = fileName + "," + transferFileName;
//				String docPath = Util.fileUpload(documentPath, transferCer);
//				compassionateAppointment.setTransferCerPath(docPath);
				compassionateAppointment.setTransferCer(transferCer.getContents());
				compassionateAppointment.setTransferFileName(transferFileName);
				break;
			}
			case "REQUISTION_EMP": {
				requisitionLetterFromEmpCer = event.getFile();
				long size = requisitionLetterFromEmpCer.getSize();
				log.info("requisitionLetterFromEmpCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!requisitionLetterFromEmpCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				requisitionLetterFromFileName = requisitionLetterFromEmpCer.getFileName();
				fileName = fileName + "," + requisitionLetterFromFileName;
//				String docPath = Util.fileUpload(documentPath, requisitionLetterFromEmpCer);
//				compassionateAppointment.setRequisitionLetterFromEmpPath(docPath);
				compassionateAppointment.setRequisitionLetterFromEmpCer(requisitionLetterFromEmpCer.getContents());
				compassionateAppointment.setRequisitionLetterFromFileName(requisitionLetterFromFileName);
				break;
			}
			case "REQUISITION_APPL": {
				requisitionLetterFromAppliCer = event.getFile();
				long size = requisitionLetterFromAppliCer.getSize();
				log.info("requisitionLetterFromAppliCer size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!requisitionLetterFromAppliCer.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				requisitionLetterFromAppliFileName = requisitionLetterFromAppliCer.getFileName();
				fileName = fileName + "," + requisitionLetterFromAppliFileName;
//				String docPath = Util.fileUpload(documentPath, requisitionLetterFromAppliCer);
//				compassionateAppointment.setRequisitionLetterFromAppliPath(docPath);
				compassionateAppointment.setRequisitionLetterFromAppliCer(requisitionLetterFromAppliCer.getContents());
				compassionateAppointment.setRequisitionLetterFromAppliFileName(requisitionLetterFromAppliFileName);
				break;
			}
			case "OUT_DUE_CER": {
				outstandingDueFromEmpCertificate = event.getFile();
				long size = outstandingDueFromEmpCertificate.getSize();
				log.info("currentlyWorkingCertificate size==>" + size);
				if (size > documentSize * 1024) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					errorMap.notify(ErrorDescription.COMPASSIONATE_DOCUMENT_SIZE_EXCEED.getErrorCode());
					return;
				}
				if (!outstandingDueFromEmpCertificate.getContentType().contains("pdf")) {
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//					errorMap.notify(ErrorDescription.INVALID_FILE_TYPE.getErrorCode());
					AppUtil.addWarn("Please Upload Only PDF File");
					return;
				}
				outstandingDueFromEmpCerFileName = outstandingDueFromEmpCertificate.getFileName();
				fileName = fileName + "," + requisitionLetterFromAppliFileName;
//				String docPath = Util.fileUpload(documentPath, outstandingDueFromEmpCertificate);
//				compassionateAppointment.setCurrentlyWorkingCertificatePath(docPath);
				compassionateAppointment.setOutstandingDueFromEmpCer(outstandingDueFromEmpCertificate.getContents());
				compassionateAppointment.setOutstandingDueFromEmpCerFileName(outstandingDueFromEmpCerFileName);
				break;
			}
			default:
				System.out.println("INVALID FILE UPLOAD CODE");
			}
		} catch (Exception e) {
			log.error("found exception in file upload", e);
		}
	}
	
	public void fileNameSet() {
		log.info("<<=== Start file name set...");
		try {
			dethFileName=fileNameSubStr(compassionateAppointment.getDethCerPath());
			qualificationFileName=fileNameSubStr(compassionateAppointment.getQualificationCerPath());
			legalHeirFileName=fileNameSubStr(compassionateAppointment.getLegalHeirCerPath());
			noObjectionFileName=fileNameSubStr(compassionateAppointment.getNoObjectionCerPath());
			transferFileName=fileNameSubStr(compassionateAppointment.getTransferCerPath());
			requisitionLetterFromFileName=fileNameSubStr(compassionateAppointment.getRequisitionLetterFromEmpPath());
			requisitionLetterFromAppliFileName=fileNameSubStr(compassionateAppointment.getRequisitionLetterFromAppliPath());
			currentlyWorkingFileName=fileNameSubStr(compassionateAppointment.getCurrentlyWorkingCerPath());
			willingToWorkAnyPostFileName=fileNameSubStr(compassionateAppointment.getWillingToWorkAnyPostCerPath());
			willingToRelocateFileName=fileNameSubStr(compassionateAppointment.getWillingToRelocateCerPath());
			finalSettlementMadeFileName=fileNameSubStr(compassionateAppointment.getFinalSettlementMadeCerPAth());
			birthFileName=fileNameSubStr(compassionateAppointment.getBirthCerPath());
			outstandingDueFromEmpCerFileName=fileNameSubStr(compassionateAppointment.getCurrentlyWorkingCertificatePath());
			
		}catch(Exception e) {
			log.error("<<== Exception while file name set..."+e);
		}
	}
	
	public String fileNameSubStr(String fileName) {
		String name=null;
		String file[];
		try {
			if(fileName!=null) {
//				file=fileName.split("/");
//				if(file[5]!=null) {
//					log.info("<<== File name.."+file[5]);
//					name=file[5];
//				}
				name = fileName.substring(fileName.lastIndexOf("-")+1);
			}
		}catch(Exception e) {
			log.error("<<=== Exception while fileName substring ge..",e);
		}
		return name;
	}
	
	public void downloadFile(String downloadFile) {
		InputStream input = null;
		if(downloadFile!=null) {
			try {
				log.info("downloadFile downloadFile==> "+downloadFile);
				File files = new File(downloadFile);
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file=(new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
				log.error("exception in filedownload method------- " + files.getName());
				
				
			} catch (FileNotFoundException e) {
				log.error("exception in filedownload method------- " + e);
			}
			catch (Exception exp) {
				log.error("exception in filedownload method------- " + exp);
			}
		}else {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			AppUtil.addWarn("File was empty");
			return;
		}
	}

	public String addCompassionate() {
		//log.info("Submit called=====>" + compassionateAppointment);
		if (compassionateAppointment.getEmployee() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_EMP_REQ.getErrorCode());
			return null;
		}

		if (compassionateAppointment.getLegalHierName() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_NAME_REQ.getErrorCode());
			return null;
		}

		if (compassionateAppointment.getDateOfBirth() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_DATEOFBIRTH_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getRelationship() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_RELATIONSHIP_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getGender() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_GENDER_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getSpecialization() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_SPECIALIZATION_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getDesignation() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_POST_REQ.getErrorCode());
			return null;
		}
		/*if (compassionateAppointment.getLocation() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_LOACTION_REQ.getErrorCode());
			return null;
		}*/
		if (compassionateAppointment.getMaritalStatus() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_MARITAL_REQ.getErrorCode());
			return null;
		}

		if (compassionateAppointment.getQualification() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_QUALIFICATION_REQ.getErrorCode());
			return null;
		}

		if (compassionateAppointment.getCurrentlyWorking()
				&& compassionateAppointment.getCurrentlyWorkingCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_CUR_WORKING_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getWillingToRelocate()
				&& compassionateAppointment.getWillingToRelocateCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_WILLING_TO_RELOCATE_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getWillingToWorkAnyPost()
				&& compassionateAppointment.getWillingToWorkAnyPostCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_WILLING_TO_WORK_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getBirthCertificate() && compassionateAppointment.getBirthCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_BIRTH_CER_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getFinalSettleMentMade()
				&& compassionateAppointment.getFinalSettlementMadeCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_SETTLE_LEGAL_HEIR_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getDeathCertificate() && compassionateAppointment.getDethCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_DEATH_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getLegalHeirCertificate() && compassionateAppointment.getLegalHeirCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_LEGAL_HEIR_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getNoObjectionCertificate()
				&& compassionateAppointment.getNoObjectionCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_NO_OBJECTION_DOC_REQ.getErrorCode());
			return null;
		}

		if (compassionateAppointment.getTransferCertificate() && compassionateAppointment.getTransferCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_TRANSFER_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getRequisitionLetterFromEmp()
				&& compassionateAppointment.getRequisitionLetterFromEmpCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_REQUISITION_FROM_EMP_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getRequisitionLetterFromApplicant()
				&& compassionateAppointment.getRequisitionLetterFromAppliCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_REQUISITION_FROM_APPL_DOC_REQ.getErrorCode());
			return null;
		}
		if (compassionateAppointment.getOutstandingDueFromEmp()
				&& compassionateAppointment.getOutstandingDueFromEmpCer() == null) {
			errorMap.notify(ErrorDescription.COMPASSIONATE_OUTSTANDING_DUE_DOC_REQ.getErrorCode());
			return null;
		}
		if (dethFileName == null || dethFileName.isEmpty()) {
			AppUtil.addWarn("Please Upload Death Certificate");
			return null;
		}
		if (legalHeirFileName == null || legalHeirFileName.isEmpty()) {
			AppUtil.addWarn("Please Upload Legal Heir Certificate");
			return null;
		} 
		if (qualificationFileName == null || qualificationFileName.isEmpty()) {
			AppUtil.addWarn("Please Upload Qualification Certificate");
			return null;
		}
		
		if(compassionateAppointmentDTO.getNote().isEmpty() || compassionateAppointmentDTO.getNote()==null) {
			AppUtil.addWarn("Please Enter Note");
			return null;
		}

		try {
			compassionateAppointmentDTO.setCompassionateAppointment(compassionateAppointment);
			mapper = new ObjectMapper();
			if (compassionateAppointment.getId() == null) {
				compassionateAppointment.setCreatedDate(new Date());
				compassionateAppointment.setCreatedBy(loginBean.getUserDetailSession());
			} else {
				compassionateAppointment.setModifiedDate(new Date());
				compassionateAppointment.setModifiedBy(loginBean.getUserDetailSession());
			}
			
			if (compassionateAppointmentDTO.getForwardtouser() != null) {
				compassionateAppointmentDTO.setForwardTo(compassionateAppointmentDTO.getForwardtouser().getId());
			}

			BaseDTO response = httpService.post(SERVER_URL + "/compassionateappointment/save",
					compassionateAppointmentDTO);

			if (response != null && response.getStatusCode() == 0) {
				log.info("CompassionateAppointment saved successfully..........");
				if (action.equalsIgnoreCase("ADD")) {
					AppUtil.addInfo("CompassionateAppointment Details Added Successfully");
					return loadCompasionateAppointmentList();
				} else
					AppUtil.addInfo("CompassionateAppointment Details Updated Successfully");
				return loadCompasionateAppointmentList();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {

			log.error("Exception occured while saving compassionate engagement....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return null;
	}
	
	
	
	public void createDownloadFile() {
		try {
			log.info("docType------>" + docType);

			if (docType.equalsIgnoreCase("CWC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getCurrentlyWorkingCer());
				currentlyWorkingFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_CWC.pdf");
			} else if (docType.equalsIgnoreCase("WPC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getWillingToWorkAnyPostCer());
				willingToWorkAnyPostFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_WPC.pdf");
			} else if (docType.equalsIgnoreCase("WRC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getWillingToRelocateCer());
				willingToRelocateFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_WRC.pdf");
			} else if (docType.equalsIgnoreCase("FSC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getFinalSettlementMadeCer());
				finalSettlementMadeFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_FSC.pdf");
			} else if (docType.equalsIgnoreCase("DC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getDethCer());
				dethCerFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_DC.pdf");
			} else if (docType.equalsIgnoreCase("LHC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getLegalHeirCer());
				legalHeirCerFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_LHC.pdf");
			} else if (docType.equalsIgnoreCase("NOC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getNoObjectionCer());
				noObjectionCerFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_NOC.pdf");
			} else if (docType.equalsIgnoreCase("BC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getBirthCer());
				birthCerFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_BC.pdf");
			} else if (docType.equalsIgnoreCase("TC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(compassionateAppointment.getTransferCer());
				transferCerFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_TC.pdf");
			} else if (docType.equalsIgnoreCase("RLEC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getRequisitionLetterFromEmpCer());
				requisitionLetterFromEmpFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_RLEC.pdf");
			} else if (docType.equalsIgnoreCase("RLAC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getRequisitionLetterFromAppliCer());
				requisitionLetterFromAppliFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_RLAC.pdf");
			} else if (docType.equalsIgnoreCase("ODC")) {
				ByteArrayInputStream bais = new ByteArrayInputStream(
						compassionateAppointment.getOutstandingDueFromEmpCer());
				outstandingDueFromEmpFile = new DefaultStreamedContent(bais, "pdf",
						compassionateAppointment.getLegalHierName() + "_ODC.pdf");
			}

			/*
			 * Map<String, Object> session =
			 * FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			 * byte[] b = (byte[]) session.get("reportBytes"); if (b != null) { downloadFile
			 * = new DefaultStreamedContent(new ByteArrayInputStream(b), "application/pdf");
			 * }
			 * 
			 * 
			 * 
			 * ownloadFile = new DefaultStreamedContent(bais, "application/pdf");
			 * InputStream in = new
			 * ByteArrayInputStream(compassionateAppointment.getCurrentlyWorkingCer());
			 * downloadFile = new DefaultStreamedContent(in, "application/pdf");
			 * 
			 * 
			 * RequestContext context = RequestContext.getCurrentInstance();
			 * context.execute("PF('docPreview').show();");
			 */
		} catch (Exception e) {
			log.error("Exception--->", e);
		}
	}

	public void clearSearch() {
		empCode = null;
		employeeMaster = new EmployeeMaster();
		compassionateAppointment = new CompassionateAppointment();
	}

	public void loadMasterValues() {
		genderList = commonDataService.getAllGenderType();
		// relationList = commonDataService.getAllRelationShip();
		getRelationShips();
		loadDesignations();
		userMasters = getForwardToUsersList();
		qualificationList = commonDataService.loadQualifications();
		// designationList = commonDataService.loadDesignation();
		locationList = commonDataService.loadRegionalOffice();
		maritalStatusList = commonDataService.getAllMaritalStatus();
	}

	public List<RelationshipMaster> getRelationShips() {

		log.info("getAllRelationShip Start===>");
		try {
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/relationship/getrelationships";
			log.info("getAllRelationShip url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContents());
				relationList = mapper.readValue(jsonResponse, new TypeReference<List<RelationshipMaster>>() {
				});

			}
		} catch (Exception e) {
			log.error("getAllRelationShip error==>", e);
		}
		log.info("getAllRelationShip end===>");
		return relationList;
	}

	public List<Designation> loadDesignations() {

		log.info("loadDesignations Start===>");
		try {
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/designation/getdesignations";
			log.info("loadDesignations url==>" + url);
			relationShipBaseDTO = httpService.get(url);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContents());
				designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
				});
			}
		} catch (Exception e) {
			log.error("loadDesignations error==>", e);
		}
		log.info("loadDesignations end===>");
		return designationList;
	}
	
	 //Forward For approved functions 
    public String approveCompassionate() {
    	log.error("<----Start Compassionate Bean approval: ----->");
		BaseDTO baseDTO = null;
		try {
			if(previousApproval != true && compassionateAppointmentDTO.getForwardTo()==null) {
				errorMap.notify(ErrorDescription.FORWARD_TO_EMPTY.getErrorCode());
				log.info("Forward To is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if(compassionateAppointmentDTO.getRemarks()==null) {
				errorMap.notify(ErrorDescription.LOG_REMARKS_EMPTY.getErrorCode());
				log.info("Forward remarks is empty-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			log.info("compassionate id----------" + compassionateAppointmentDTO.getCompassionateAppointment());
			log.info("previousApproval ----------" + previousApproval);
			baseDTO = new BaseDTO();
			if (previousApproval == false && compassionateAppointmentDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Approved  ----->");
				compassionateAppointmentDTO.setStage(CommonForwardStatus.APPROVED.toString());
			} else if (previousApproval == true && compassionateAppointmentDTO.getForwardFor() == true) {
				log.error("<---- Approve Status change on Final Approved  ----->");
				Long userId=loginBean.getUserMaster().getId();
				compassionateAppointmentDTO.setForwardTo(userId);
				compassionateAppointmentDTO.setStage(CommonForwardStatus.FINAL_APPROVED.toString());
			} else {
				log.error("<---- Approve Status change on Approved  ----->");
				compassionateAppointmentDTO.setStage(CommonForwardStatus.APPROVED.toString());
			}
			log.info("approve remarks=========>" + compassionateAppointmentDTO.getRemarks());
			if (compassionateAppointmentDTO.getForwardtouser() != null) {
				compassionateAppointmentDTO.setForwardTo(compassionateAppointmentDTO.getForwardtouser().getId());
			}
			baseDTO = httpService.post(SERVER_URL + "/compassionateappointment/approved",compassionateAppointmentDTO);
					
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.COMPASSIONATE_APPROVED_SUCCESS.getErrorCode());
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadCompasionateAppointmentList();
			}
		} catch (Exception e) {
			log.error("approveInsurance method inside exception-------", e);
		}
		return null;
	}
    
    
    //Forward For rejected Compassionate functions 
	public String rejectCompassionate() {
		log.error("<----Start rejectCompassionate: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("Compassionate selected id----------" + compassionateAppointmentDTO.getId());
			baseDTO = new BaseDTO();
			Long userId=loginBean.getUserMaster().getId();
			compassionateAppointmentDTO.setForwardTo(userId);
			compassionateAppointmentDTO.setStage(CommonForwardStatus.REJECTED.toString());
			String url = SERVER_URL + "/compassionateappointment/reject";
			baseDTO = httpService.post(url, compassionateAppointmentDTO);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.COMPASSIONATE_REJECTED_SUCCESS.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadCompasionateAppointmentList();
			}
		} catch (Exception e) {
			log.error("Exception reject Compassionate method inside exception-------", e);
		}
		return null;
	}
	
	 //Forward For Users list based the designation
		public List<UserMaster> getForwardToUsersList() {
			log.error("<----Start getForwardToUsersList: ----->");
			BaseDTO baseDTO = null;
			List<UserMaster> userMasterList=new ArrayList<>();
			try {
				log.info("Compassionate selected id----------" + compassionateAppointmentDTO.getId());
				baseDTO = new BaseDTO();
				String url = SERVER_URL + "/compassionateappointment/forwardUsers";
				baseDTO = httpService.get(url);
				if (baseDTO.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					userMasterList = mapper.readValue(jsonResponse,
							new TypeReference<List<UserMaster>>() {
					});
					log.info("Users master list size..."+userMasterList);
				}
			} catch (Exception e) {
				log.error("Exception getForwardToUsersList Compassionate method inside exception-------", e);
			}
			return userMasterList;
		}
		
		
		public String approveCompassionateLocation() {
			log.error("<----Start approveCompassionateLocation: ----->");
			BaseDTO baseDTO = null;
			try {
				if(compassionateAppointment.getLocation()==null 
						|| compassionateAppointment.getLocation().getId()==null) {
					log.info("Location is Empty-----------------");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_WORKlOCATION_EMPTY).getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
				log.info("Compassionate selected id----------" + compassionateAppointment.getId());
				baseDTO = new BaseDTO();
				
				String url = SERVER_URL + "/compassionateappointment/approveLocation";
				baseDTO = httpService.post(url, compassionateAppointment);
				if (baseDTO.getStatusCode() != null) {
					errorMap.notify(baseDTO.getStatusCode());
					log.info("Successfully location updated-----------------");
					RequestContext.getCurrentInstance().update("growls");
					return loadCompasionateAppointmentList();
				}
			} catch (Exception e) {
				log.error("Exception reject Compassionate method inside exception-------", e);
			}
			return null;
		}
}
