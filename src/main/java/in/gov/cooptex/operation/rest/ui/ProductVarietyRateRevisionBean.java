package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.HelpDeskIssueReasonDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.ProductVarietyRevisedRateDTO;
import in.gov.cooptex.core.dto.ProductVarietyRevisedRateRequestDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountMovementType;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.finance.enums.CreditSalesDemandStatus;
import in.gov.cooptex.operation.model.ProductVarietyRateRevisionLog;
import in.gov.cooptex.operation.model.ProductVarietyRateRevisionNote;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("productVarietyRateRevisionBean")
public class ProductVarietyRateRevisionBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5928956397441906882L;
	private final String CREATE_RATE_REVISION_PAGE = "/pages/operation/yarn/createRevisedRetailPrice.xhtml?faces-redirect=true;";
	private final String LIST_RATE_REVISION_PAGE = "/pages/operation/yarn/listRevisedRetailPrice.xhtml?faces-redirect=true;";
	private final String VIEW_RATE_REVISION_PAGE = "/pages/operation/yarn/viewRevisedRetailPrice.xhtml?faces-redirect=true;";
	private final String VERIFY_RATE_REVISION_PAGE = "/pages/operation/yarn/verifiyrevisedraterp.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String PRODUCT_REVISED_RATE_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	ProductVarietyRevisedRateDTO productVarietyRevisedRateDTO;

	@Getter
	@Setter
	LazyDataModel<ProductVarietyRevisedRateDTO> lazyProductVarietyRevisedRateDTOList;

	List<ProductVarietyRevisedRateDTO> productVarietyRevisedRateDTOList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String receivedBy = "";

	@Getter
	@Setter
	String itemCode = "";

	@Getter
	@Setter
	Double unitRate = 0.00;;

	@Getter
	@Setter
	ProductVarietyRevisedRateDTO productVarietyRevisedRateResponseDTO = null;

	@Getter
	@Setter
	ProductVarietyRevisedRateDTO productVarietyRevisedRateResquestDTO = null;

	@Getter
	@Setter
	List<ProductVarietyRevisedRateDTO> responseList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductVarietyRevisedRateDTO> finalResponseList = new ArrayList<>();

	@Getter
	@Setter
	String note = "";

	@Getter
	@Setter
	String qrCode = "";

	@Getter
	@Setter
	UserMaster forwardTo = new UserMaster();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	ProductVarietyRateRevisionNote productVarietyRateRevisionNote = new ProductVarietyRateRevisionNote();

	@Getter
	@Setter
	ProductVarietyRateRevisionLog productVarietyRateRevisionLog = new ProductVarietyRateRevisionLog();

	@Getter
	@Setter
	boolean itemCodeAndUnitRateFlag = false;

	@Getter
	@Setter
	Boolean saveAndSubmitFlag;

	@Getter
	@Setter
	ProductVarietyRevisedRateDTO selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();

	@Getter
	@Setter
	ProductVarietyRevisedRateRequestDTO productVarietyRevisedRateRequestDTO = new ProductVarietyRevisedRateRequestDTO();

	@Getter
	@Setter
	ProductVarietyRevisedRateRequestDTO responseDTO = new ProductVarietyRevisedRateRequestDTO();

	@Getter
	@Setter
	List<ProductVarietyRevisedRateDTO> viewResponseList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductVarietyRevisedRateDTO> verifiedFinalApprovalResoponseList = new ArrayList<>();

	@Getter
	@Setter
	String remarks = "";

	@Setter
	@Getter
	private StreamedContent file;

	@Setter
	@Getter
	boolean loginCondition = true;

	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};

	@Getter
	@Setter
	String uomCode;

	public String showProductVarietyRevisedRateDTOListPage() {
		log.info("<==== Starts ProductVarietyRateRevisionBean.showAccountCategoryListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		productVarietyRevisedRateResponseDTO = new ProductVarietyRevisedRateDTO();
		productVarietyRevisedRateResquestDTO = new ProductVarietyRevisedRateDTO();
		productVarietyRevisedRateDTOList = new ArrayList<>();
		addButtonFlag = true;
		productVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
		selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
		loadLazyProductVarietyRevisedRateDTOList();
		log.info("<==== Ends ProductVarietyRateRevisionBean.showAccountCategoryListPage =====>");
		return LIST_RATE_REVISION_PAGE;
	}

	public void loadLazyProductVarietyRevisedRateDTOList() {
		log.info("<===== Starts ProductVarietyRateRevisionBean.loadLazyProductVarietyRevisedRateDTOList ======>");
		lazyProductVarietyRevisedRateDTOList = new LazyDataModel<ProductVarietyRevisedRateDTO>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<ProductVarietyRevisedRateDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/getproductrevisedratelazycode";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						productVarietyRevisedRateDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<ProductVarietyRevisedRateDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyProductVarietyRevisedRateDTOList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return productVarietyRevisedRateDTOList;
			}

			@Override
			public Object getRowKey(ProductVarietyRevisedRateDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductVarietyRevisedRateDTO getRowData(String rowKey) {
				try {
					for (ProductVarietyRevisedRateDTO productVarietyRevisedRateDTO : productVarietyRevisedRateDTOList) {
						if (productVarietyRevisedRateDTO.getId().equals(Long.valueOf(rowKey))) {
							selectedProductVarietyRevisedRateDTO = productVarietyRevisedRateDTO;
							return productVarietyRevisedRateDTO;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ProductVarietyRateRevisionBean.loadLazyProductVarietyRevisedRateDTOList ======>");
	}

	public String redirectSVInProgressPage() {
		if(loginBean.getStockVerificationIsInProgess()) {
			loginBean.setMessage("Revised retail price");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}
	
	public String showProductRevisedRatePageAction() {
		log.info(
				"<===== Starts ProductVarietyRateRevisionBean.showAccountCategoryListPageAction ===========>" + action);
		try {

			if (action.equalsIgnoreCase("CREATE") || action.equalsIgnoreCase("EDIT") || action.equalsIgnoreCase("VIEW")) {
				redirectSVInProgressPage();
			}
			
			
			if (action.equalsIgnoreCase("LIST")) {
				addButtonFlag = true;
				editButtonFlag = true;
				log.info("create ProductVarietyRevisedRateDTO called..");
				productVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResponseDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResquestDTO = new ProductVarietyRevisedRateDTO();
				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
				productVarietyRevisedRateDTOList = new ArrayList<>();
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				remarks=null;
				loadLazyProductVarietyRevisedRateDTOList();
				/*
				 * EmployeePersonalInfoEmployment empPersonalEmployeeMent =
				 * loginBean.getEmployee() .getPersonalInfoEmployment(); if
				 * (empPersonalEmployeeMent != null) { EntityMaster entityMaster =
				 * empPersonalEmployeeMent.getWorkLocation(); if (entityMaster != null) {
				 * EntityTypeMaster entityTypeMaster = entityMaster.getEntityTypeMaster(); if
				 * (entityTypeMaster != null &&
				 * entityTypeMaster.getEntityCode().equals("INSTITUTIONAL_SALES_SHOWROOM") ||
				 * entityTypeMaster.getEntityCode().equals("DISTRIBUTION_WAREHOUSE") ||
				 * entityTypeMaster.getEntityCode().equals("PRODUCT_WAREHOUSE")) {
				 * loginCondition = true; } else { loginCondition = false; } } else {
				 * AppUtil.addWarn("Entity not found"); return null; } } else {
				 * AppUtil.addWarn("Employee Information not found"); return null; }
				 */
                
				return LIST_RATE_REVISION_PAGE;
			}

			if (!action.equalsIgnoreCase("CREATE") && (selectedProductVarietyRevisedRateDTO == null
					|| selectedProductVarietyRevisedRateDTO.getId() == null)) {
				AppUtil.addWarn("Select atleast one record");
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				if (!productVarietyRevisedRateDTOList.isEmpty()) {
					boolean finalApprovedRecored = productVarietyRevisedRateDTOList.stream().anyMatch(
							i -> !i.getStatus().equals("FINAL-APPROVED") && !i.getStatus().equals("REJECTED"));
					boolean savedRecord = productVarietyRevisedRateDTOList.stream()
							.anyMatch(i -> i.getStatus().equals("SAVED"));
					if (finalApprovedRecored && selectedProductVarietyRevisedRateDTO == null && savedRecord) {
						AppUtil.addWarn("Please Select SAVED record then add again");
						return null;
					} else if (!savedRecord && finalApprovedRecored) {
						AppUtil.addWarn(
								"One Revised Rate process is already In-Progress. New Revised Rate can be started after completing existing process.");
						return null;

					}
				}
				log.info("create ProductVarietyRevisedRateDTO called..");
				productVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResponseDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResquestDTO = new ProductVarietyRevisedRateDTO();
				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
				productVarietyRevisedRateDTOList = new ArrayList<>();
				finalResponseList = new ArrayList<>();
				forwardTo = new UserMaster();
				setNote("");
				setQrCode("");
				setItemCode("");
				setUnitRate(0.00);
				productVarietyRateRevisionNote = new ProductVarietyRateRevisionNote();
				EmployeePersonalInfoEmployment empPersonalEmployeeMent = loginBean.getEmployee()
						.getPersonalInfoEmployment();
				if (empPersonalEmployeeMent != null) {
					EntityMaster entityMaster = empPersonalEmployeeMent.getWorkLocation();
					if (entityMaster != null) {
						EntityTypeMaster entityTypeMaster = entityMaster.getEntityTypeMaster();
						if (entityTypeMaster != null
								&& (entityTypeMaster.getEntityCode().equals(EntityType.INSTITUTIONAL_SALES_SHOWROOM)
										|| entityTypeMaster.getEntityCode().equals(EntityType.PRINTING_WARE_HOUSE))) {
							receivedBy = "ITEMCODE";
						} else {
							receivedBy = "QRCODE";
						}
					} else {
						AppUtil.addWarn("Entity not found");
						return null;
					}
				} else {
					AppUtil.addWarn("Employee Information not found");
					return null;
				}
				
				if (selectedProductVarietyRevisedRateDTO != null
						&& selectedProductVarietyRevisedRateDTO.getStatus().equals("SAVED")) {
					String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/get/"
							+ selectedProductVarietyRevisedRateDTO.getId();
					BaseDTO response = httpService.get(url);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						responseDTO = mapper.readValue(jsonResponse,
								new TypeReference<ProductVarietyRevisedRateRequestDTO>() {
								});
						finalResponseList = responseDTO.getProductVarietyRevisedRateDTOList();
						return CREATE_RATE_REVISION_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					}
				}
				return CREATE_RATE_REVISION_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				boolean finalApprovedRecored = productVarietyRevisedRateDTOList.stream()
						.anyMatch(i -> !i.getStatus().equals("FINAL-APPROVED") && !i.getStatus().equals("REJECTED"));
				if (finalApprovedRecored && selectedProductVarietyRevisedRateDTO != null
						&& selectedProductVarietyRevisedRateDTO.getStatus().equals("REJECTED")) {
					AppUtil.addWarn(
							"One Revised Rate process is already In-Progress. Cannot be edit after completing existing process.");
					return null;
				}
				
				EmployeePersonalInfoEmployment empPersonalEmployeeMent = loginBean.getEmployee()
						.getPersonalInfoEmployment();
				if (empPersonalEmployeeMent != null) {
					EntityMaster entityMaster = empPersonalEmployeeMent.getWorkLocation();
					if (entityMaster != null) {
						EntityTypeMaster entityTypeMaster = entityMaster.getEntityTypeMaster();
						if (entityTypeMaster != null
								&& (entityTypeMaster.getEntityCode().equals(EntityType.INSTITUTIONAL_SALES_SHOWROOM)
										|| entityTypeMaster.getEntityCode().equals(EntityType.PRINTING_WARE_HOUSE))) {
							receivedBy = "ITEMCODE";
						} else {
							receivedBy = "QRCODE";
						}
					} else {
						AppUtil.addWarn("Entity not found");
						return null;
					}
				} else {
					AppUtil.addWarn("Employee Information not found");
					return null;
				}
				productVarietyRevisedRateResponseDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResquestDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateDTOList = new ArrayList<>();
				log.info("edit ProductVarietyRevisedRateDTO called..");
				String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/get/"
						+ selectedProductVarietyRevisedRateDTO.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					responseDTO = mapper.readValue(jsonResponse,
							new TypeReference<ProductVarietyRevisedRateRequestDTO>() {
							});
					if (responseDTO.getProductVarietyRateRevisionNote() != null) {
						forwardTo = responseDTO.getProductVarietyRateRevisionNote().getUserMaster();
				
						loadAutoCompleteUserMaster(forwardTo.getUsername());
						if(autoEmployeeMasterList!=null) {
							autoEmployeeMasterList.forEach(leave -> {
								if (forwardTo.getId().equals(leave.getId())) {
									forwardTo.setUserDisplayName(leave.getUserDisplayName());
								}
							});
//						forwardTo=autoEmployeeMasterList.stream().findAny().filter(em->em.getId().equals(forwardTo.getId()));
						}
						productVarietyRateRevisionNote
								.setFinalApproval(responseDTO.getProductVarietyRateRevisionNote().getFinalApproval());
						note=responseDTO.getProductVarietyRateRevisionNote().getNote();
					}
					finalResponseList = responseDTO.getProductVarietyRevisedRateDTOList();
					return CREATE_RATE_REVISION_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("VIEW")) {
				productVarietyRevisedRateResponseDTO = new ProductVarietyRevisedRateDTO();
				productVarietyRevisedRateResquestDTO = new ProductVarietyRevisedRateDTO();
				forwardTo = new UserMaster();
				log.info("edit ProductVarietyRevisedRateDTO called..");
				String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/get/"
						+ selectedProductVarietyRevisedRateDTO.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					responseDTO = mapper.readValue(jsonResponse,
							new TypeReference<ProductVarietyRevisedRateRequestDTO>() {
							});
					commentAndLogList = response.getCommentAndLogList();
					if (responseDTO.getProductVarietyRateRevisionNote() == null) {
						responseDTO.setProductVarietyRateRevisionNote(new ProductVarietyRateRevisionNote());
					} else {
					//	forwardTo = responseDTO.getProductVarietyRateRevisionNote().getUserMaster();
					//	productVarietyRateRevisionNote
					//			.setFinalApproval(responseDTO.getProductVarietyRateRevisionNote().getFinalApproval());
						note=responseDTO.getProductVarietyRateRevisionNote().getNote();
					}
					viewResponseList = responseDTO.getProductVarietyRevisedRateDTOList();
					return VIEW_RATE_REVISION_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view ProductVarietyRevisedRateDTO called..");
				String viewurl = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/get/"
						+ selectedProductVarietyRevisedRateDTO.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					responseDTO = mapper.readValue(jsonResponse,
							new TypeReference<ProductVarietyRevisedRateRequestDTO>() {
							});
					viewResponseList = responseDTO.getProductVarietyRevisedRateDTOList();
					return VIEW_RATE_REVISION_PAGE;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends ProductVarietyRateRevisionBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}
	
	public void check (UserMaster user) {
		log.info("==================>>>>>>>>>>>>>User "+ user.getUsername() + user.getUserDisplayName());
		forwardTo=user;
		log.info("==================>>>>>>>>>>>>>forwardto "+ forwardTo.getUsername() + forwardTo.getUserDisplayName());
	}

	/**
	 * 
	 */
	public void getProductDetails() {
		log.info("<===== START ProductVarietyRateRevisionBean.getProductDetails ===========>");
		try {

			log.info("ItemCode: -1 " + itemCode);
			log.info("Unit Rate: " + unitRate);

			itemCode = itemCode != null ? itemCode.trim().toUpperCase() : null;

			log.info("ItemCode: -2 " + itemCode);
			log.info("QR code: -3 " + qrCode);
			if (receivedBy.equals("QRCODE")) {
				if (!StringUtils.isEmpty(qrCode)) {
					boolean isExists = finalResponseList.stream().anyMatch(i -> i.getQrCode().equals(qrCode));
					if (isExists) {
						AppUtil.addWarn("This QR code added already");
						RequestContext.getCurrentInstance().update("growls");
						return;
					}
				} else {
					AppUtil.addWarn("QR Code is required");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}
			} else {
				if (unitRate == null) {
					AppUtil.addWarn("Rate is required");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}

				if (!StringUtils.isEmpty(itemCode)) {
					boolean isExists = finalResponseList.stream().anyMatch(i -> (i.getItemCode().equals(itemCode))
							&& (i.getExistingRate().doubleValue() == unitRate.doubleValue()));
					if (isExists) {
						AppUtil.addWarn("This item is added already");
						RequestContext.getCurrentInstance().update("growls");
						return;
					}
				} else {
					AppUtil.addWarn("Item Code is required");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}
			}

			productVarietyRevisedRateResquestDTO.setItemCode(itemCode);
			productVarietyRevisedRateResquestDTO.setUnitRate(unitRate);
			productVarietyRevisedRateResquestDTO.setQrCode(qrCode);
			productVarietyRevisedRateResquestDTO.setItemAndQRFlag(receivedBy);
			log.info("=== Item Code == " + productVarietyRevisedRateResquestDTO.getItemCode());
			log.info("=== Unit Rate == " + productVarietyRevisedRateResquestDTO.getUnitRate());
			String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/getProductDetails";
			BaseDTO baseDTO = httpService.post(url, productVarietyRevisedRateResquestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				String response = mapper.writeValueAsString(baseDTO.getResponseContents());
				responseList = mapper.readValue(response, new TypeReference<List<ProductVarietyRevisedRateDTO>>() {
				});
				setQrCode("");
				setUnitRate(null);
				setItemCode("");
				if (responseList.isEmpty()) {
					itemCodeAndUnitRateFlag = false;
					AppUtil.addWarn("No record found");
					RequestContext.getCurrentInstance().update("growls");
				} else {
					if (receivedBy.equals("ITEMCODE")) {
						itemCodeAndUnitRateFlag = true;
						/*boolean isQrCodeEmpty = responseList.stream().anyMatch(o-> StringUtils.isEmpty(o.getQrCode()));
						if (isQrCodeEmpty) {
							AppUtil.addWarn("QR Code is required");
							RequestContext.getCurrentInstance().update("growls");
							return;
						}*/
					} else {
						itemCodeAndUnitRateFlag = false;
					}
				}
				String url1 = PRODUCT_REVISED_RATE_URL + "/uommaster/loaduombyproductid/"
						+ responseList.get(0).getProductId();
				BaseDTO baseDTO1 = httpService.get(url1);
				UomMaster uomMaster = new UomMaster();
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO1.getResponseContent());
				uomMaster = mapper.readValue(jsonResponse, UomMaster.class);
				uomCode = uomMaster.getCode();

				finalResponseList.addAll(responseList);
			} else {
				itemCodeAndUnitRateFlag = false;
				AppUtil.addWarn(baseDTO.getErrorDescription());
				RequestContext.getCurrentInstance().update("growls");
			}
		} catch (Exception e) {
			log.info("<===== Exception Occured in  ProductVarietyRateRevisionBean.getProductDetails ===========>" + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.getProductDetails ===========>");
	}

	public String saveOrUpdate() {
		log.info("<==== Starts ProductVarietyRateRevisionBean.saveOrUpdate =======>");
		try {
			if (saveAndSubmitFlag) {
				if (forwardTo == null || forwardTo.getId() == null) {
					AppUtil.addWarn("Forward To is required");
					return null;
				}
				if (productVarietyRateRevisionNote.getFinalApproval() == null) {
					AppUtil.addWarn("Forward For is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
				if (StringUtils.isEmpty(note)) {
					AppUtil.addWarn("Note is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			if (finalResponseList.isEmpty()) {
				AppUtil.addWarn("Revised Retail Price Details cannot be empty");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			/*if (receivedBy.equals("QRCODE")) {
				if (!StringUtils.isEmpty(qrCode)) {
					boolean isExists = finalResponseList.stream().anyMatch(i -> i.getQrCode().equals(qrCode));
					if (isExists) {
						AppUtil.addWarn("This QR Code added already");
						RequestContext.getCurrentInstance().update("growls");
						return null;
					}
				} else {
					AppUtil.addWarn("QR code is required");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}if(StringUtils.isEmpty(qrCode)) {
				AppUtil.addWarn("QR code is required");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}*/
			if (selectedProductVarietyRevisedRateDTO != null) {
				productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			} else {
				productVarietyRevisedRateRequestDTO.setId(null);
			}
			if (finalResponseList != null && !finalResponseList.isEmpty()) {
				List<ProductVarietyRevisedRateDTO> checkList = finalResponseList.stream().filter(
						i -> i.getRevisedRate() == null || i.getRevisedRate().equals(0.00d) || i.getRevisedRate() < 0)
						.collect(Collectors.toList());
				if (!checkList.isEmpty()) {
					AppUtil.addWarn("Revised Rate should be more than 0");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}

				List<ProductVarietyRevisedRateDTO> sameRateList = finalResponseList.stream()
						.filter(i -> i.getExistingRate() == null || i.getExistingRate().equals(i.getRevisedRate()))
						.collect(Collectors.toList());
				if (!sameRateList.isEmpty()) {
					AppUtil.addWarn("Revised rate should not be equal to existing rate");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
//this condition not needed said by helpdesk karthick
			//	List<ProductVarietyRevisedRateDTO> purchasePriceList = finalResponseList.stream()
			//			.filter(i -> i.getPurchasePrice() > i.getRevisedRate()).collect(Collectors.toList());
//				if (!purchasePriceList.isEmpty()) {
//					AppUtil.addWarn("Revised Rate should not be less than purchase price");
//					RequestContext.getCurrentInstance().update("growls");
//					return null;
//				}

				List<ProductVarietyRevisedRateDTO> checkqtytorevise = finalResponseList.stream().filter(
						i -> i.getQtyToRevise() == null || i.getQtyToRevise().equals(0.00d) || i.getQtyToRevise() < 0)
						.collect(Collectors.toList());
				if (!checkqtytorevise.isEmpty()) {
					AppUtil.addWarn("QTY to Revise should be more than 0");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}

				List<ProductVarietyRevisedRateDTO> greaterthanListcheck = finalResponseList.stream()
						.filter(i -> i.getQtyToRevise() > i.getAvailableQty()).collect(Collectors.toList());
				if (!greaterthanListcheck.isEmpty()) {
					AppUtil.addWarn("QTY to Revise should not be greater than available qty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}

			}
			productVarietyRevisedRateRequestDTO.setSaveAndSubmitFlag(saveAndSubmitFlag);
			productVarietyRevisedRateRequestDTO.setFinalApproval(productVarietyRateRevisionNote.getFinalApproval());
			productVarietyRevisedRateRequestDTO.setForwardTo(forwardTo);
			productVarietyRevisedRateRequestDTO.setNote(note);
			productVarietyRevisedRateRequestDTO.setProductVarietyRevisedRateDTOList(finalResponseList);
			String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/createproductvarietyraterevision";
			BaseDTO response = httpService.post(url, productVarietyRevisedRateRequestDTO);
			if (response != null && response.getStatusCode() == 0) {
				log.info("account category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE"))
					AppUtil.addInfo("Revised Retail Price Details added successfully");
				else
					AppUtil.addInfo("Revised Retail Price Details updated successfully");

				addButtonFlag = true;
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			//	productVarietyRevisedRateRequestDTO.setForwardTo(null);
			//	productVarietyRevisedRateRequestDTO.setFinalApproval(null);
				return LIST_RATE_REVISION_PAGE;
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
			
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends ProductVarietyRateRevisionBean.saveOrUpdate =======>");
		return showProductVarietyRevisedRateDTOListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ProductVarietyRateRevisionBean.onRowSelect ========>" + event);
		selectedProductVarietyRevisedRateDTO = ((ProductVarietyRevisedRateDTO) event.getObject());
		
		Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();
		if (selectedProductVarietyRevisedRateDTO != null
				&& (selectedProductVarietyRevisedRateDTO.getStatus().equals("REJECTED")
						|| selectedProductVarietyRevisedRateDTO.getStatus().equals("SAVED"))) {
			if (loginUserId != null && selectedProductVarietyRevisedRateDTO.getCreatedBy() != null
					&& selectedProductVarietyRevisedRateDTO.getCreatedBy().equals(loginUserId)) {
				editButtonFlag = true;
			} else {
				editButtonFlag = false;
			}
		} else {
			editButtonFlag = false;
		}
		/*if (selectedProductVarietyRevisedRateDTO.getStatus().equals("REJECTED")
				|| selectedProductVarietyRevisedRateDTO.getStatus().equals("SAVED")) {
			editButtonFlag = true;
		} else {
			editButtonFlag = false;
		}*/
		if (!selectedProductVarietyRevisedRateDTO.getStatus().equals("SAVED")) {
			addButtonFlag = false;
		} else {
			addButtonFlag = true;
		}
		deleteButtonFlag = true;
		log.info("<===Ends ProductVarietyRateRevisionBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts ProductVarietyRateRevisionBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(PRODUCT_REVISED_RATE_URL + "/delete/" + selectedProductVarietyRevisedRateDTO.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.GL_ACCOUNT_CATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAccountCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete account category ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends ProductVarietyRateRevisionBean.deleteConfirm ===========>");
		return showProductVarietyRevisedRateDTOListPage();
	}

	public String clearData() {
		log.info("<===== START ProductVarietyRateRevisionBean.clearData ===========>");
		try {
			responseList = new ArrayList<>();
			finalResponseList = new ArrayList<>();
			setItemCode("");
			setUnitRate(null);
			itemCodeAndUnitRateFlag = false;
			selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			setQrCode("");
			return CREATE_RATE_REVISION_PAGE;
		} catch (Exception e) {
			log.info("<===== Exception Occured in  ProductVarietyRateRevisionBean.clearData ===========>" + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.clearData ===========>");
		return null;
	}

	public String clear() {
		log.info("<===== START ProductVarietyRateRevisionBean.clear ===========>");
		try {
			selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			addButtonFlag = true;
			editButtonFlag = true;
			setAction("LIST");
			selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			return showProductRevisedRatePageAction();
		} catch (Exception e) {
			log.info("<===== Exception Occured in  ProductVarietyRateRevisionBean.clear ===========>" + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.clear ===========>");
		return null;
	}

	public String approveRevisedRate() {
		log.info("<===== START ProductVarietyRateRevisionBean.clear ===========>");
		try {
			if (forwardTo == null || forwardTo.getId() == null) {
				AppUtil.addWarn("Forward To is required");
				return null;

			}
			if (productVarietyRateRevisionNote.getFinalApproval() == null) {
				AppUtil.addWarn("Forward For is required");
				return null;
			}
			if(null == remarks || "".equalsIgnoreCase(remarks)){
				AppUtil.addWarn("Remarks is required");
				return null;
			}
			productVarietyRateRevisionLog.setRemarks(remarks);
			productVarietyRevisedRateRequestDTO.setProductVarietyRateRevisionLog(productVarietyRateRevisionLog);
			productVarietyRevisedRateRequestDTO.setForwardTo(forwardTo);
			productVarietyRevisedRateRequestDTO.setFinalApproval(productVarietyRateRevisionNote.getFinalApproval());
			if (StringUtils.isEmpty(note)) {
				productVarietyRevisedRateRequestDTO.setNote(responseDTO.getProductVarietyRateRevisionNote() != null
						&& responseDTO.getProductVarietyRateRevisionNote().getNote() != null
								? responseDTO.getProductVarietyRateRevisionNote().getNote()
								: "");
			} else {
				productVarietyRevisedRateRequestDTO.setNote(note);
			}

			productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			String approveUrl = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/approveRevisedRate";
			log.info("=== approveRevisedRate url is " + approveUrl);
			BaseDTO baseDTO = httpService.post(approveUrl, productVarietyRevisedRateRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				setAction("LIST");
				AppUtil.addInfo("Product variety Rate revision Approval Request Approved successfully");
				RequestContext.getCurrentInstance().update("growls");
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				return showProductRevisedRatePageAction();
			} else {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			}
			remarks=null;
		} catch (Exception e) {
			log.error("<===== Exception Occured in  ProductVarietyRateRevisionBean.clear ===========>   " + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.clear ===========>");
		return null;
	}

	public String rejectRevisedRate() {
		log.info("<===== START ProductVarietyRateRevisionBean.clear ===========>");
		try {
			if(null == remarks || "".equalsIgnoreCase(remarks)){
				AppUtil.addWarn("Remarks is required");
				return null;
			}
			productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			String sumbitForApprovalUrl = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/rejectRevisedRate";
			log.info("=== submitForApproval url is " + sumbitForApprovalUrl);
			productVarietyRateRevisionLog.setRemarks(remarks);
			productVarietyRevisedRateRequestDTO.setProductVarietyRateRevisionLog(productVarietyRateRevisionLog);
			productVarietyRevisedRateRequestDTO.setProductVarietyRevisedRateDTOList(verifiedFinalApprovalResoponseList);
			BaseDTO baseDTO = httpService.post(sumbitForApprovalUrl, productVarietyRevisedRateRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				setAction("LIST");
				AppUtil.addWarn("Product variety Rate revision Approval Request Rejected successfully");
				RequestContext.getCurrentInstance().update("growls");
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				return showProductRevisedRatePageAction();
			} else {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			}

		} catch (Exception e) {
			log.error("<===== Exception Occured in  ProductVarietyRateRevisionBean.clear ===========>   " + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.clear ===========>");
		return null;
	}

	public String getFinalApprovalVerifiedRecords() {
		log.info("<===== START ProductVarietyRateRevisionBean.getFinalApprovalVerifiedRecords ===========>");
		try {
			productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			/**
			 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/controller
			 * 
			 * ProductVarietyRateRevisionController.java
			 */
			
			String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/getverifiedrecords";
			log.info("=== getFinalApprovalVerifiedRecords url is " + url);
			//
			BaseDTO baseDTO = httpService.post(url, productVarietyRevisedRateRequestDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					verifiedFinalApprovalResoponseList = mapper.readValue(jsonResponse,
							new TypeReference<List<ProductVarietyRevisedRateDTO>>() {
							});
					if (verifiedFinalApprovalResoponseList.isEmpty()) {
						setAction("LIST");
						AppUtil.addWarn("No stock found to update revised rate.");
						RequestContext.getCurrentInstance().update("growls");
						selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
						return showProductRevisedRatePageAction();
					}
					return VERIFY_RATE_REVISION_PAGE;
				} else {

					errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
				}

			} else {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			}
		} catch (Exception e) {
			log.error(
					"<===== Exception Occured in  ProductVarietyRateRevisionBean.getFinalApprovalVerifiedRecords ===========>   "
							+ e);
		}
		return null;
	}

	public String saveVerifiedRecords() {
		log.info("<===== START ProductVarietyRateRevisionBean.saveVerifiedRecords ===========>");
		log.info("<===== END ProductVarietyRateRevisionBean.getFinalApprovalVerifiedRecords ===========>");
		try {
			/**
			 * 
			 */
			
			if(null == remarks || "".equalsIgnoreCase(remarks)){
				AppUtil.addWarn("Remarks is required");
				return null;
			}
			String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/saveApprovedRecords";
			log.info("=== saveVerifiedRecords url is " + url);
			productVarietyRateRevisionLog.setRemarks(remarks);
			productVarietyRevisedRateRequestDTO.setProductVarietyRateRevisionLog(productVarietyRateRevisionLog);
			productVarietyRevisedRateRequestDTO.setProductVarietyRevisedRateDTOList(verifiedFinalApprovalResoponseList);
			if (StringUtils.isEmpty(note)) {
				productVarietyRevisedRateRequestDTO.setNote(responseDTO.getProductVarietyRateRevisionNote() != null
						&& responseDTO.getProductVarietyRateRevisionNote().getNote() != null
								? responseDTO.getProductVarietyRateRevisionNote().getNote()
								: "");
			} else {
				productVarietyRevisedRateRequestDTO.setNote(note);
			}
			BaseDTO baseDTO = httpService.post(url, productVarietyRevisedRateRequestDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					setAction("LIST");
					AppUtil.addInfo("Product variety Rate revision Final Approved successfully");
					RequestContext.getCurrentInstance().update("growls");
					selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
					return showProductRevisedRatePageAction();
				} else if (baseDTO.getErrorDescription() != null && baseDTO.getErrorDescription().equals("FAILURE")) {
					AppUtil.addWarn(
							"Some of selected items are not available now to update revised rate. These items could have been moved out. Please click 'Final Approve' again now, to show available items only to update. ");
					return VIEW_RATE_REVISION_PAGE;

				}else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
			} else {
				AppUtil.addWarn("Invalid response from server");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			RequestContext.getCurrentInstance().update("growls");
			log.error("<=====Exception Occured in ProductVarietyRateRevisionBean.saveVerifiedRecords ===========>" + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.saveVerifiedRecords ===========>");
		return null;
	}

	public String submitForApproval() {
		log.info("<===== START ProductVarietyRateRevisionBean.submitForApproval ===========>");
		try {
			note=responseDTO.getProductVarietyRateRevisionNote().getNote();
			if (forwardTo == null || forwardTo.getId() == null) {
				AppUtil.addWarn("Forward To is required");
				return null;

			}
			if (productVarietyRateRevisionNote.getFinalApproval() == null) {
				AppUtil.addWarn("Forward For is required");
				return null;
			}
			if (StringUtils.isEmpty(note)) {
				AppUtil.addWarn("Note is required");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			productVarietyRevisedRateRequestDTO.setForwardTo(forwardTo);
			productVarietyRevisedRateRequestDTO.setFinalApproval(productVarietyRateRevisionNote.getFinalApproval());
			productVarietyRevisedRateRequestDTO.setNote(note);
			productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			String sumbitForApprovalUrl = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/submitForApproval";
			log.info("=== submitForApproval url is " + sumbitForApprovalUrl);
			productVarietyRevisedRateRequestDTO.setProductVarietyRevisedRateDTOList(verifiedFinalApprovalResoponseList);
			BaseDTO baseDTO = httpService.post(sumbitForApprovalUrl, productVarietyRevisedRateRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				setAction("LIST");
				AppUtil.addInfo("Product variety Rate revision Approval Request Added successfully");
				RequestContext.getCurrentInstance().update("growls");
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
				return showProductRevisedRatePageAction();
			} else {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
			}

		} catch (Exception e) {
			log.info("<===== Exception Occured in  ProductVarietyRateRevisionBean.submitForApproval ===========> " + e);
		}
		log.info("<===== END ProductVarietyRateRevisionBean.submitForApproval ===========>");
		return null;
	}

	public void generateQRCodeByCurrentStockStatus() {
		try {
			log.info("ProductVarietyRateRevisionBean :::  Start of generateQRCodeByCurrentStockStatus");
			InputStream input = null;
			productVarietyRevisedRateRequestDTO.setId(selectedProductVarietyRevisedRateDTO.getId());
			String url = PRODUCT_REVISED_RATE_URL + "/productvarietyraterevision/getQrCodeDetails";
			log.info("=== getFinalApprovalVerifiedRecords url is " + url);
			BaseDTO baseDTO = httpService.post(url, productVarietyRevisedRateRequestDTO);
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getMessage() != null) {
					String qrCodePdfGeneratePath = baseDTO.getMessage();
					File files = new File(qrCodePdfGeneratePath);
					input = new FileInputStream(files);
					ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
					file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
							files.getName()));
					log.error("exception in filedownload method------- " + files.getName());
				}
				AppUtil.addInfo("PDF generated successfully");
				RequestContext.getCurrentInstance().update("growls");
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			} else {
				AppUtil.addWarn("PDF Cannot be generated");
				RequestContext.getCurrentInstance().update("growls");
				selectedProductVarietyRevisedRateDTO = new ProductVarietyRevisedRateDTO();
			}
		} catch (Exception e) {
			log.error("Exception Occured in ProductVarietyRateRevisionBean.generateQRCodeByCurrentStockStatus  " + e);
		}
		log.info("ProductVarietyRateRevisionBean :::  End of generateQRCodeByCurrentStockStatus");
	}

	@Setter
	@Getter
	Integer deleteItemIndex = null;

	public String godeleteRevisedRate(Integer index) {
		deleteItemIndex = null;
		try {
			deleteItemIndex = index;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmRevisedRateDelete').show();");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public void deleteRevisedRate() {
		log.info("ProductVarietyRateRevisionBean :::  Start of deleteRevisedRate");
		try {
			int indexValue = deleteItemIndex.intValue();
			finalResponseList.remove(indexValue);
			RequestContext.getCurrentInstance().update("revisedRateTable");
		} catch (Exception e) {
			log.info("Exception Occurd in ProductVarietyRateRevisionBean :::  Start of deleteRevisedRate  " + e);
		}
		log.info("ProductVarietyRateRevisionBean :::  Start of deleteRevisedRate");
	}

	public String goRevisedRateFinalApproval() {
		try {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmRevisedRatefinalApproval').show();");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(loginBean.getServerURL(),query);
		return autoEmployeeMasterList;
	}

}
