package in.gov.cooptex.admin.training.ui;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.disciplinary.dto.EmpComplaintRegisterListResponseDTO;
import in.gov.cooptex.admin.disciplinary.dto.SuspensionDetailsResponseDto;
import in.gov.cooptex.admin.training.dto.EmpTrainingNoteDTO;
import in.gov.cooptex.admin.training.dto.EmployeeTraineesDTO;
import in.gov.cooptex.admin.training.dto.EmployeeTrainingRequestViewDTO;
import in.gov.cooptex.admin.training.dto.EmployeeTrainingResponseDTO;
import in.gov.cooptex.admin.training.dto.ExternalInternalTrainingRequestDTO;
import in.gov.cooptex.admin.training.enums.EmployeeTrainingStatus;
import in.gov.cooptex.admin.training.model.EmpTraining;
import in.gov.cooptex.admin.training.model.EmpTrainingLog;
import in.gov.cooptex.admin.training.model.EmpTrainingMembers;
import in.gov.cooptex.admin.training.model.EmpTrainingNote;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.ApproveRejectCommentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.TrainingTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.VenueMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("empTrainingBean")
@Scope("session")
public class EmpTrainingBean extends CommonBean implements Serializable {
	/**
	 * External Internal Training Bean
	 */
	private static final long serialVersionUID = -3692509443246631888L;

	private final String EMP_TRAINING_LIST_PAGE = "/pages/admin/training/listInternalExternalTraining.xhtml?faces-redirect=true;";
	private final String EMP_TRAINING_VIEW_PAGE = "/pages/admin/training/viewInternalExternalTrainingNominees.xhtml?faces-redirect=true;";
	private final String EMP_TRAINING_REPORT_PAGE = "/pages/admin/training/generateInternalExternalTrainingReport.xhtml?faces-redirect=true;";
	private final String EMP_TRAINING_CREATE_NOMINEE_PAGE = "/pages/admin/training/createInternalExternalTrainingNominees.xhtml?faces-redirect=true;";
	private final String EMP_TRAINING_CREATE_TRAINING_PAGE = "/pages/admin/training/createInternalExternalTrainingRequest.xhtml?faces-redirect=true;";

	private static final String EMP_TRAINING_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/employeetraining";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	private ObjectMapper mapper = new ObjectMapper();

	private String jsonResponse;

	@Autowired
	private HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	String action;

	@Setter
	@Getter
	String fileType;

	@Getter
	@Setter
	String empTrainingDocumentFileName;

	@Getter
	@Setter
	Boolean trainingReportFlag = false;

	@Getter
	@Setter
	Boolean trainingRequestFlag = true;

	@Getter
	@Setter
	Boolean createNomineesFlag = false;

	@Setter
	@Getter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean rejectButtonFlag = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	private Boolean isRowSelected = false;

	@Getter
	@Setter
	int totalRecords = 0;

	@Setter
	@Getter
	Boolean internalTypeFlag = false;

	@Setter
	@Getter
	Boolean externalTypeFlag = false;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Setter
	@Getter
	Boolean internalTypePanelNomineePageFlag = false;

	@Setter
	@Getter
	Boolean externalTypePanelNomineePageFlag = false;

	@Setter
	@Getter
	Boolean trainingTypeDropDownEditPageDisableFlag = true;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	private List<ApproveRejectCommentDTO> approveCommentsDTOList = new ArrayList<>();
	
	@Getter
	@Setter
	private List<ApproveRejectCommentDTO> rejectCommentsDTOList = new ArrayList<>();

	@Setter
	@Getter
	String selectedInstitute;

	private final String EXTERNAL = "External";
	private final String INTERNAL = "Internal";

	@Getter
	@Setter
	LazyDataModel<EmployeeTrainingResponseDTO> empTrainingsLazyDataModelListPAge;

	@Setter
	@Getter
	List<EmployeeTrainingResponseDTO> empTrainingDtoListPage;

	@Getter
	@Setter
	EmployeeTrainingResponseDTO selectedEmpTrainingRowDtoListPage = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	EmpTrainingNote lastEmpTrainingNote;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	EmpTrainingLog lastEmpTrainingLog;

	private SimpleDateFormat simpleDateFormatForDate = new SimpleDateFormat("dd-MMM-yyyy");

	private SimpleDateFormat simpleDateFormatForTime = new SimpleDateFormat("HH:mm a");

	private static DecimalFormat decimalFormat = new DecimalFormat(".##");

	@Getter
	@Setter
	private UserMaster forwardTo;

	@Setter
	@Getter
	private List trainingStatusValues;

	@Getter
	@Setter
	List<EmployeeTrainingRequestViewDTO> viewLogResponseList;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Setter
	@Getter
	ExternalInternalTrainingRequestDTO createTrainingRequestDTO;

	@Setter
	@Getter
	List<TrainingTypeMaster> trainingTypeMasterCreatePage;

	@Setter
	@Getter
	List<SupplierMaster> trainingInstitutionCreatePage;

	@Setter
	@Getter
	List<VenueMaster> venueMasterListCreatePage;

	@Setter
	@Getter
	List<Department> departmentListCreatePage;

	@Setter
	@Getter
	EmpTraining empTrainingNominationPage;

	@Setter
	@Getter
	List<EmpTrainingNoteDTO> empTrainingNotesViewPage;

	@Setter
	@Getter
	EmpTrainingNoteDTO lastNoteDto;

	@Setter
	@Getter
	List<EmpTrainingLog> empTrainingLogListViewPage;

	@Setter
	@Getter
	List<EmpTrainingMembers> empTrainingMemberListViewPage;

	@Setter
	@Getter
	EmployeeTraineesDTO addNomineeToTrainingDtoNominationPage;

	@Setter
	@Getter
	List<EmployeeMaster> employeeMasterListNominationPage;

	@Getter
	@Setter
	List<Long> selectedEmployeeMasterIdsListNominationPage;

	@Setter
	@Getter
	EmployeeTraineesDTO approveOrRejectTrainingRequestDtoViewPage;

	@Setter
	@Getter
	EmployeeTraineesDTO generateReportForTrainingRequestReportPage;

	@Setter
	@Getter
	List<EmpTrainingMembers> empTrainingMemberListAttendedTraining;

	@Setter
	@Getter
	private List<EmpTrainingMembers> selectedEmpAttendeesListViewPage;

	@Autowired
	LoginBean loginBean;

	@Setter
	@Getter
	List<Designation> designationList;
	
	@Setter
	@Getter
	private StreamedContent file;
	
		
	@Getter
	@Setter
	private String fileName;

	@Getter
	@Setter
	List<Long> designationIds;

	@Setter
	@Getter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	/**
	 * Set Max Date to Today Date
	 */
	public Date getToday() {
		return new Date();
	}

	public void getDateDiff() {
		long diff = this.createTrainingRequestDTO.getEndDate().getTime()
				- this.createTrainingRequestDTO.getStartDate().getTime();
		long noOfDays = diff / (24 * 60 * 60 * 1000);
		if (noOfDays > 0) {
			this.createTrainingRequestDTO.setNoOfdays(noOfDays + 1);
		} else {
			this.createTrainingRequestDTO.setNoOfdays((long) 1);
		}
	}

	public String getAmt(Double amt) {
		log.info("<===Starts EmpTrainingBean.parseDouble ========>" + amt);
		return amt == null ? "" : decimalFormat.format(amt);
	}

	@PostConstruct
	public void init() {
		log.info("<=== Starts EmpTrainingBean.init() ========>");
		showEmployeeTrainingListPage();
		viewLogResponseList = new ArrayList<EmployeeTrainingRequestViewDTO>();
		log.info("<=== Ends EmpTrainingBean.init() ========>");
	}

	/**
	 * List Page update=""
	 *
	 */

	public String showEmployeeTrainingListPage() {
		log.info("<=== Starts EmpTrainingBean.showEmployeeTrainingListPage() ========>");
		resetListPage();
		loadEmployeeTrainingListLazy();
		loadStatus();
		selectedEmpTrainingRowDtoListPage = null;
		log.info("<=== Ends EmpTrainingBean.showEmployeeTrainingListPage() ========>");
		return EMP_TRAINING_LIST_PAGE;
	}

	public void resetListPage() {
		log.info("<=== Starts EmpTrainingBean.resetListPage ========>");
		if (!isRowSelected) {
			clearEmployeeTrainingListPage();
		}
		isRowSelected = false;
		log.info("<=== Ends EmpTrainingBean.resetListPage ========>");
	}

	private void loadStatus() {
		log.info("<=== Starts employeeMaster.loadStatus() ========>");
		trainingStatusValues = new ArrayList();
		Object[] statusArray = EmployeeTrainingStatus.class.getEnumConstants();
		for (Object status : statusArray) {
			trainingStatusValues.add(status);
		}
		log.info("<=== Ends EmpTrainingBean.loadStatus() trainingStatusValues : " + trainingStatusValues);
	}

	public void loadEmployeeTrainingListLazy() {
		log.info("<=== Starts EmpTrainingBean.loadEmployeeTrainingListLazy() ========>");
		empTrainingsLazyDataModelListPAge = new LazyDataModel<EmployeeTrainingResponseDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<EmployeeTrainingResponseDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = EMP_TRAINING_URL + "/getalltraininglist";
					BaseDTO baseDTO = httpService.post(url, paginationRequest);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					if (baseDTO.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						empTrainingDtoListPage = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeTrainingResponseDTO>>() {
								});
						if (empTrainingDtoListPage == null) {
							log.info(" EmployeeTraining List is null");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" EmployeeTraining list search Successfully Completed");
					} else {
						log.error(
								" EmployeeTraining list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return empTrainingDtoListPage;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading EmployeeTraining list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeTrainingResponseDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeTrainingResponseDTO getRowData(String rowKey) {
				try {
					for (EmployeeTrainingResponseDTO empTraining : empTrainingDtoListPage) {
						if (empTraining.getId().equals(Long.valueOf(rowKey))) {
							selectedEmpTrainingRowDtoListPage = empTraining;
							return empTraining;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};
		log.info("<=== Ends EmpTrainingBean.loadEmployeeTrainingListLazy() ========>");
	}

	public String getDate(Date date) {
		log.info("<===Starts EmpTrainingBean.getDate ========>" + date);
		return date == null ? "" : simpleDateFormatForDate.format(date);
	}

	public String getTime(Date date) {
		log.info("<===Starts EmpTrainingBean.getDate ========>" + date);
		return date == null ? "" : simpleDateFormatForTime.format(date);
	}

	public void dateSelection() {
		// this.createTrainingRequestDTO.setEndDate(new
		// Date(this.createTrainingRequestDTO.getStartDate().getTime()));
		// getDateDiff();
	}

	/**
	 * Code for Row Select
	 */
	public void onRowSelectListPage(SelectEvent event) {
		log.info("<=== Starts EmpTrainingBean.onRowSelectListPage ========>" + event);
		selectedEmpTrainingRowDtoListPage = ((EmployeeTrainingResponseDTO) event.getObject());
		log.info("<=== Selected EmployeeTraining Object =====>", selectedEmpTrainingRowDtoListPage);
		if (!selectedEmpTrainingRowDtoListPage.getStatus().equalsIgnoreCase("Approved")
				&& !selectedEmpTrainingRowDtoListPage.getStatus().equalsIgnoreCase("FINAL_APPROVED")) {
			deleteButtonFlag = true;
			editButtonFlag = true;
			rejectButtonFlag = true;
			approveButtonFlag = true;
			createNomineesFlag = true;
			trainingRequestFlag = true;
		} else {
			deleteButtonFlag = false;
			editButtonFlag = false;
			rejectButtonFlag = false;
			approveButtonFlag = false;
			createNomineesFlag = false;
			trainingRequestFlag = false;
		}
		if ("FINAL_APPROVED".equalsIgnoreCase(selectedEmpTrainingRowDtoListPage.getStatus())) {
			trainingReportFlag = true;
		} else {
			trainingReportFlag = false;
		}
		viewButtonFlag = true;
		isRowSelected = true;
		log.info("<=== Ends EmpTrainingBean.onRowSelectListPage ========>");
	}

	public void clearEmployeeTrainingListPage() {
		log.info("<=== Starts EmpTrainingBean.clearEmployeeTrainingListPage ========>");
		viewButtonFlag = false;
		editButtonFlag = false;
		deleteButtonFlag = false;
		createNomineesFlag = false;
		trainingReportFlag = false;

		trainingRequestFlag = true;
		selectedEmpTrainingRowDtoListPage = null;
		log.info("<=== Ends EmpTrainingBean.clearEmployeeTrainingListPage ========>");
	}

	public String generateTrainingReportPage() {
		log.info("<=== Starts EmpTrainingBean.generateTrainingReportPage ========>");
		String response = null;
		if (selectedEmpTrainingRowDtoListPage != null && selectedEmpTrainingRowDtoListPage.getId() != null) {
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			EmpTraining empTraining = loadNomineeDataByTrainingId(selectedEmpTrainingRowDtoListPage.getId());
			if (empTraining != null) {
				log.info("Successfully loaded EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());

				if (empTrainingNotesViewPage == null) {
					log.info(
							"Failed to load EmpTrainingNote data for id :" + selectedEmpTrainingRowDtoListPage.getId());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				} else if (empTrainingMemberListViewPage == null) {
					log.info("Failed to load TrainingMemberList data for id :"
							+ selectedEmpTrainingRowDtoListPage.getId());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				} else {
					empTrainingNominationPage = empTraining;
					forwardToUsersList = commonDataService.loadForwardToUsersByFeature("INTERNAL_EXTERNAL_TRAINING");
					employeeMasterListNominationPage = loadAllActiveEmployeeMaster();

					if (employeeMasterListNominationPage == null) {
						log.info("Failed to load ActiveEmployeeMaster data");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					} else if (forwardToUsersList == null) {
						log.info("Failed to load ForwardToUsers data");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					// RequestObj
					generateReportForTrainingRequestReportPage = new EmployeeTraineesDTO();
					selectedEmployeeMasterIdsListNominationPage = new ArrayList<>();
					empTrainingMemberListAttendedTraining = new ArrayList<>();
					selectedEmpAttendeesListViewPage = new ArrayList<>();
					empTrainingMemberListViewPage.forEach(members -> {
						if (members.getTrainingAttended()) {
							selectedEmpAttendeesListViewPage.add(members);
						}
					});
					updateInternalOrExternalViewPanel();

					response = this.EMP_TRAINING_REPORT_PAGE;
				}
			} else {
				log.info("Failed to load EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());
			}
		} else {
			log.error("User have not selected any row");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends EmpTrainingBean.generateTrainingReportPage ========>");
		return response;
	}

	public String cancelGenerateReportTrainingRequest() {
		log.info("<=== Starts EmpTrainingBean.cancelGenerateReportTrainingRequest");
		String result = EMP_TRAINING_LIST_PAGE;
		nullifyingGenerateReportRequestObject();
		log.info("<=== Ends EmpTrainingBean.cancelGenerateReportTrainingRequest() ======>");
		return result;
	}

	private void nullifyingGenerateReportRequestObject() {
		log.info("<=== Starts EmpTrainingBean.nullifyingGenerateReportRequestObject");
		// nullifying objctes

		empTrainingNotesViewPage = null;
		empTrainingMemberListViewPage = null;
		empTrainingLogListViewPage = null;
		empTrainingNominationPage = null;
		selectedEmpAttendeesListViewPage = null;
		employeeMasterListNominationPage = null;
		empTrainingMemberListAttendedTraining = null;
		generateReportForTrainingRequestReportPage = null;
		selectedEmployeeMasterIdsListNominationPage = null;
		lastEmpTrainingNote = null;
		lastEmpTrainingLog = null;
		designationIds = null;
		employeeMasterList = null;
		log.info("<=== Ends EmpTrainingBean.nullifyingGenerateReportRequestObject");
	}

	public String submitAttendeesFromTrainingReportPage() {
		log.info("<=== Starts EmpTrainingBean.submitAttendeesFromTrainingReportPage");
		String result = null;
		try {
			generateReportForTrainingRequestReportPage.setEmpTraining(new EmpTraining());
			generateReportForTrainingRequestReportPage.setForwardToId(forwardTo != null ? forwardTo.getId() : null);
			generateReportForTrainingRequestReportPage.getEmpTraining().setId(empTrainingNominationPage.getId());
			generateReportForTrainingRequestReportPage.setEmpTrainingMemberList(selectedEmpAttendeesListViewPage);
			selectedEmpAttendeesListViewPage.forEach(empTrainingMembers -> {
				empTrainingMembers.setTrainingAttended(true);
				empTrainingMembers.setEmpTraining(generateReportForTrainingRequestReportPage.getEmpTraining());
			});
			if (generateReportForTrainingRequestReportPage.getEmployeeMastersList() != null) {
				for (EmployeeMaster e : generateReportForTrainingRequestReportPage.getEmployeeMastersList()) {

					boolean isPresent = false;
					for (EmpTrainingMembers m : generateReportForTrainingRequestReportPage.getEmpTrainingMemberList()) {
						if (m.getEmpMaster().getId().equals(e.getId())) {
							isPresent = true;
							break;
						}
					}
					if (!isPresent) {
						EmpTrainingMembers members = new EmpTrainingMembers();
						members.setEmpMaster(e);
						members.setTrainingAttended(false);
						members.setEmpTraining(generateReportForTrainingRequestReportPage.getEmpTraining());
						generateReportForTrainingRequestReportPage.getEmpTrainingMemberList().add(members);
					}
				}
			}
			generateReportForTrainingRequestReportPage.setEmployeeMastersList(new ArrayList<>());
			Long noteParentId = null;
			for (EmpTrainingNoteDTO note : empTrainingNotesViewPage) {
				if (noteParentId == null) {
					noteParentId = note.getId();
				} else {
					if (note.getId() > noteParentId) {
						noteParentId = note.getId();
					}
				}
			}

			generateReportForTrainingRequestReportPage.setNoteParentId(noteParentId);
			log.info(
					"<===  submitAttendeesFromTrainingReportPage data : " + generateReportForTrainingRequestReportPage);
			BaseDTO response = httpService.post(EMP_TRAINING_URL + "/savenotetrainee",
					generateReportForTrainingRequestReportPage);
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_REPORT_SUCCESS.getErrorCode());
				log.info("Training Report Saved Successful");
				nullifyingGenerateReportRequestObject();
				result = EMP_TRAINING_LIST_PAGE;
			} else {
				log.error("Training Report Saved Failed");
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_REPORT_FAILURE.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving EmpTrainingBean " + e.getMessage());
		}
		log.info("<=== Ends EmpTrainingBean.submitAttendeesFromTrainingReportPage() ======>");
		return result;
	}

	public void addAttendeesToTrainingReportPage() {
		log.info("<=== Starts EmpTrainingBean.addAttendeesToTrainingReportPage");
		generateReportForTrainingRequestReportPage.setEmployeeMastersList(new ArrayList<>());
		// removing already attented employee
		Iterator<Long> longIterator = selectedEmployeeMasterIdsListNominationPage.iterator();
		while (longIterator.hasNext()) {
			Long employeeId = longIterator.next();
			for (EmpTrainingMembers members : empTrainingMemberListViewPage) {
				if (members.getEmpMaster().getId().longValue() == employeeId.longValue()) {
					longIterator.remove();
					break;
				}
			}

		}
		for (EmployeeMaster employeeMaster : employeeMasterListNominationPage) {
			if (selectedEmployeeMasterIdsListNominationPage.contains(employeeMaster.getId())) {
				generateReportForTrainingRequestReportPage.getEmployeeMastersList().add(employeeMaster);
			}
		}
		log.info("<=== Ends EmpTrainingBean.addAttendeesToTrainingReportPage");
	}

	public void deleteAttendeesFromTrainingReportPage(Long id) {
		log.info("<=== Starts EmpTrainingBean.deleteAttendeesFromTrainingReportPage");
		if (id != null) {
			List<EmployeeMaster> employeeMasters = generateReportForTrainingRequestReportPage.getEmployeeMastersList();
			Iterator<EmployeeMaster> iterator = employeeMasters.iterator();
			while (iterator.hasNext()) {
				EmployeeMaster employee = iterator.next();
				if (id.equals(employee.getId())) {
					iterator.remove();
					break;
				}
			}
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.deleteAttendeesFromTrainingReportPage");
	}

	/**
	 * Open Create Nominee Page
	 */
	public String createNomineePage() {
		log.info("<=== Starts EmpTrainingBean.createNomineePage ========>");
		String response = null;
		loadEmployeeLoggedInUserDetails();
		if (selectedEmpTrainingRowDtoListPage != null && selectedEmpTrainingRowDtoListPage.getId() != null) {
			EmpTraining empTraining = loadNomineeDataByTrainingId(selectedEmpTrainingRowDtoListPage.getId());
			if (empTraining != null) {
				log.info("Successfully loaded EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());

				designationIds = null;
				employeeMasterList = new ArrayList<EmployeeMaster>();
				empTrainingNominationPage = empTraining;
				// employeeMasterListNominationPage = loadAllActiveEmployeeMaster();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("INTERNAL_EXTERNAL_TRAINING");
				addNomineeToTrainingDtoNominationPage = new EmployeeTraineesDTO();
				selectedEmployeeMasterIdsListNominationPage = new ArrayList<>();
				loadAllDesignationList();
				updateInternalOrExternalViewPanel();

				selectedEmpTrainingRowDtoListPage.setNotificationId(null);
				systemNotificationBean.loadTotalMessages();

				response = this.EMP_TRAINING_CREATE_NOMINEE_PAGE;
			} else {
				log.info("Failed to load EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());
			}
		} else {
			log.error("User have not selected any row");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends EmpTrainingBean.createNomineePage ========>");
		return response;
	}

	public void loadAllDesignationList() {
		log.info("... Load Designation List called ....");
		BaseDTO response = new BaseDTO();
		try {
			mapper = new ObjectMapper();
			if (empTrainingNominationPage != null && empTrainingNominationPage.getDepartment() != null
					&& empTrainingNominationPage.getDepartment().getId() != null)
				response = httpService.get(SERVER_URL + "/designation/getalldesignation/"
						+ empTrainingNominationPage.getDepartment().getId());
			else
				response = httpService.get(SERVER_URL + "/designation/getalldesignation");

			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	private void updateInternalOrExternalViewPanel() {
		log.info("<=== Starts EmpTrainingBean.updateInternalOrExternalViewPanel ========>");
		if (INTERNAL.equalsIgnoreCase(empTrainingNominationPage.getTrainingTypeMaster().getName())) {
			internalTypePanelNomineePageFlag = true;
				externalTypePanelNomineePageFlag = false;
		} else if (EXTERNAL.equalsIgnoreCase(empTrainingNominationPage.getTrainingTypeMaster().getName())) {
			internalTypePanelNomineePageFlag = false;
			externalTypePanelNomineePageFlag = true;
		}
		log.info("<=== Ends EmpTrainingBean.updateInternalOrExternalViewPanel ========>");
	}

	/**
	 * Returns EmpTraining Object by EmpTrainingId
	 */
	private EmpTraining loadNomineeDataByTrainingId(Long id) {
		log.info("<--- Starts EmpTrainingBean.loadNomineeDataByTrainingId() --->");
		EmpTraining empTraining = null;
		EmployeeTrainingRequestViewDTO viewdto = new EmployeeTrainingRequestViewDTO();
		viewLogResponseList = new ArrayList<EmployeeTrainingRequestViewDTO>();
		try {
			String requestPath = EMP_TRAINING_URL + "/getemptraining";
			Long notificationId = selectedEmpTrainingRowDtoListPage.getNotificationId();
			viewdto.setEmpTrainingId(id);
			viewdto.setNotificationId(notificationId);
			BaseDTO baseDTO = httpService.post(requestPath, viewdto);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					EmployeeTrainingRequestViewDTO viewDTO = mapper.readValue(jsonValue,
							new TypeReference<EmployeeTrainingRequestViewDTO>() {
							});

					// for log status Details
					// String jsonValues = mapper.writeValueAsString(baseDTO.getResponseContents());
					// log.info("Json File" + jsonValues);
					// viewLogResponseList = mapper.readValue(jsonValues,
					// new TypeReference<EmployeeTrainingRequestViewDTO>() {
					// });
					// log.info("Json Response List" + viewLogResponseList);
					viewLogResponseList = viewDTO.getEmployeeTrainingReqDTOList();
					approveCommentsDTOList = new ArrayList<>();
					rejectCommentsDTOList = new ArrayList<>();
					approveCommentsDTOList = viewDTO.getApproveCommentsDTOList();
					rejectCommentsDTOList = viewDTO.getApproveCommentsDTOList();
					empTraining = viewDTO.getEmpTraining();
					empTrainingNotesViewPage = viewDTO.getEmpTrainingNoteList();
					empTrainingMemberListViewPage = viewDTO.getEmpTrainingMemberList();
					empTrainingLogListViewPage = viewDTO.getEmpTrainingLogList();
					lastEmpTrainingNote = viewDTO.getEmpTrainingNote();
					lastEmpTrainingLog = viewDTO.getEmpTrainingLog();

					log.info("Loading empTrainingRequest Details success : " + viewDTO);

					selectedEmpTrainingRowDtoListPage.setNotificationId(null);
					systemNotificationBean.loadTotalMessages();
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Loading empTraining Failed Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ msg);
				}
			} else {
				log.error(" get EmployeeTraining Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("<--- Exception in loadNomineeDataByTrainingId() --->", exp);
		}
		log.info("<--- Ends EmpTrainingBean.loadNomineeDataByTrainingId() --->");
		return empTraining;
	}

	private List<EmployeeMaster> loadAllActiveEmployeeMaster() {
		log.info("<--- Starts EmpTrainingBean.loadAllActiveEmployeeMaster() --->");
		List<EmployeeMaster> employeeMasterList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getallactiveemployeemaster";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info("Loading employeeMasterList success : " + employeeMasterList);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get EmployeeMasterList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("<--- Exception in loadAllActiveEmployeeMaster() --->", exp);
		}
		log.info("<--- Ends EmpTrainingBean.loadAllActiveEmployeeMaster() --->");
		return employeeMasterList;
	}

	public List<String> institutionAutocomplete(String query) {
		log.info("Institute Autocomplete query==>" + query);
		List<String> results = new ArrayList<String>();
		trainingInstitutionCreatePage = loadTrainingInstitutionListByAutoComplete(query);
		if (trainingInstitutionCreatePage != null || !trainingInstitutionCreatePage.isEmpty()) {
			for (SupplierMaster sublist : trainingInstitutionCreatePage) {
				String supp = sublist.getName();
				results.add(supp);
			}
		}

		return results;
	}

	public void autoCompleteNoResultFount() {
		log.info("selected institute", selectedInstitute);

		for (SupplierMaster sublist : trainingInstitutionCreatePage) {
			if (sublist.getName().equals(selectedInstitute)) {
				Long id = sublist.getId();
				createTrainingRequestDTO.setInstitutionId(id);
				log.info("<============slected institute==========>" + createTrainingRequestDTO.getInstitutionId());
			} else {

			}

		}
	}

	// public void addNomineeToTrainingRequestPage() {
	// log.info("<=== Starts EmpTrainingBean.addNomineeToTrainingRequestPage");
	// addNomineeToTrainingDtoNominationPage.setEmployeeMastersList(new
	// ArrayList<>());
	// for (EmployeeMaster employeeMaster : employeeMasterListNominationPage) {
	// if
	// (selectedEmployeeMasterIdsListNominationPage.contains(employeeMaster.getId()))
	// {
	// addNomineeToTrainingDtoNominationPage.getEmployeeMastersList().add(employeeMaster);
	// }
	// }
	// log.info("<=== Ends EmpTrainingBean.addNomineeToTrainingRequestPage");
	// }

	public void addNomineeToTrainingRequestPage() {
		log.info("<=== Starts EmpTrainingBean.addNomineeToTrainingRequestPage");
		addNomineeToTrainingDtoNominationPage.setEmployeeMastersList(new ArrayList<>());
		for (EmployeeMaster employeeMaster : employeeMasterList) {
			if (selectedEmployeeMasterIdsListNominationPage.contains(employeeMaster.getId())) {
				addNomineeToTrainingDtoNominationPage.getEmployeeMastersList().add(employeeMaster);
			}
		}
		log.info("<=== Ends EmpTrainingBean.addNomineeToTrainingRequestPage");
	}

	public void deleteNomineeFromTrainingRequestPage(Long id) {
		log.info("<=== Starts EmpTrainingBean.deleteNomineeFromTrainingRequestPage");
		if (id != null) {
			List<EmployeeMaster> employeeMasters = addNomineeToTrainingDtoNominationPage.getEmployeeMastersList();
			Iterator<EmployeeMaster> iterator = employeeMasters.iterator();
			while (iterator.hasNext()) {
				EmployeeMaster employee = iterator.next();
				if (id.equals(employee.getId())) {
					iterator.remove();
					break;
				}
			}
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.deleteNomineeFromTrainingRequestPage");
	}

	public String cancelCreateNomineeRequest() {
		log.info("<=== Starts EmpTrainingBean.cancelCreateNomineeRequest");
		String result = EMP_TRAINING_LIST_PAGE;
		nullifyingCreateNomineeRequestObject();
		log.info("<=== Ends EmpTrainingBean.cancelCreateNomineeRequest() ======>");
		return result;
	}

	private void nullifyingCreateNomineeRequestObject() {
		log.info("<=== Starts EmpTrainingBean.nullifyingCreateNomineeRequestObject");
		// nullifying objctes
		empTrainingNotesViewPage = null;
		empTrainingMemberListViewPage = null;
		empTrainingLogListViewPage = null;
		empTrainingNominationPage = null;
		employeeMasterListNominationPage = null;
		addNomineeToTrainingDtoNominationPage = null;
		selectedEmployeeMasterIdsListNominationPage = null;
		log.info("<=== Ends EmpTrainingBean.nullifyingCreateNomineeRequestObject");
	}

	public String submitCreateNomineeRequest() {
		log.info("<=== Starts EmpTrainingBean.submitCreateNomineeRequest");
		String result = null;
		try {
			addNomineeToTrainingDtoNominationPage.setEmpTraining(new EmpTraining());
			addNomineeToTrainingDtoNominationPage.getEmpTraining().setId(empTrainingNominationPage.getId());
			Long noteParentId = null;
			for (EmpTrainingNoteDTO note : empTrainingNotesViewPage) {
				if (noteParentId == null) {
					noteParentId = note.getId();
				} else {
					if (note.getId() > noteParentId) {
						noteParentId = note.getId();
					}
				}
			}
			addNomineeToTrainingDtoNominationPage.setForwardToId(forwardTo != null ? forwardTo.getId() : null);
			addNomineeToTrainingDtoNominationPage.setNoteParentId(noteParentId);
			log.info("<===  submitCreateNomineeRequest data : " + addNomineeToTrainingDtoNominationPage);
			BaseDTO response = httpService.post(EMP_TRAINING_URL + "/savenotetrainee",
					addNomineeToTrainingDtoNominationPage);
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_SAVE_SUCCESS.getErrorCode());
				log.info("Training Request Saved Successful");
				nullifyingCreateNomineeRequestObject();
				result = EMP_TRAINING_LIST_PAGE;
			} else {
				log.error("Training Request Saved Failed");
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_SAVE_FAILURE.getErrorCode());
			}
			log.info("<=== Ends EmpTrainingBean.submitCreateNomineeRequest ========>");
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving EmpTrainingBean " + e.getMessage());
		}
		log.info("<=== Ends EmpTrainingBean.submitCreateNomineeRequest() ======>");
		return result;
	}

	/*
	 * Create Page
	 */
	public String createTrainingRequest() {
		log.info("<=== Starts EmpTrainingBean.createTrainingRequest ========>");
		createTrainingRequestDTO = new ExternalInternalTrainingRequestDTO();
		internalTypeFlag = false;
		externalTypeFlag = false;
		trainingTypeDropDownEditPageDisableFlag = false;
		loadAllCreateTrainingPageDropDown();
		log.info("<=== Ends EmpTrainingBean.createTrainingRequest ========>");
		return EMP_TRAINING_CREATE_TRAINING_PAGE;
	}

	public void loadAllCreateTrainingPageDropDown() {
		log.info("<=== Starts EmpTrainingBean.loadAllCreateTrainingPageDropDown() ========>");
		selectedEmpTrainingRowDtoListPage = null;
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("INTERNAL_EXTERNAL_TRAINING_REQUEST");
		trainingTypeMasterCreatePage = trainingTypeMasterList();
		trainingInstitutionCreatePage = loadTrainingInstitutionList();
		departmentListCreatePage = loadDepartmentList();
		loadEmployeeLoggedInUserDetails();
		log.info("<=== Ends EmpTrainingBean.loadAllCreateTrainingPageDropDown() ========>");
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		try {

			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();

			if (employeeMaster == null) {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}

		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public String selectTrainingType() {
		log.info("<=== Starts EmpTrainingBean.selectTrainingType ========>");
		String trainingTypeName = null;
		if (trainingTypeMasterCreatePage != null) {
			for (TrainingTypeMaster type : trainingTypeMasterCreatePage) {
				if (type.getId().equals(createTrainingRequestDTO.getTrainingTypeId())) {
					trainingTypeName = type.getName();
					break;
				}
			}
		}

		log.info("Selected trainingTypeName : " + trainingTypeName);
		if (trainingTypeName != null) {
			if (trainingTypeName.equalsIgnoreCase(INTERNAL)) {
				internalTypeFlag = true;
				externalTypeFlag = false;
			} else if (trainingTypeName.equalsIgnoreCase(EXTERNAL)) {
				externalTypeFlag = true;
				internalTypeFlag = false;
			}
			venueMasterListCreatePage = loadVenueMasterList(trainingTypeName);
		} else {
			internalTypeFlag = false;
			externalTypeFlag = false;
		}

		log.info("<=== Ends EmpTrainingBean.selectTrainingType ========>");
		return EMP_TRAINING_CREATE_TRAINING_PAGE;
	}

	public String cancelCreateTrainingRequest() {
		log.info("<=== Starts EmpTrainingBean.cancelCreateTrainingRequest");
		String result = EMP_TRAINING_LIST_PAGE;
		nullifyingCreateTrainingRequestObject();
		log.info("<=== Ends EmpTrainingBean.cancelCreateTrainingRequest() ======>");
		return EMP_TRAINING_LIST_PAGE;
	}

	private void nullifyingCreateTrainingRequestObject() {
		log.info("<=== Starts EmpTrainingBean.nullifyingCreateTrainingRequestObject");
		// nullifying objctes
		selectedEmpTrainingRowDtoListPage = null;
		trainingTypeMasterCreatePage = null;
		trainingInstitutionCreatePage = null;
		departmentListCreatePage = null;
		createTrainingRequestDTO = null;
		empTrainingDocumentFileName = null;
		selectedInstitute = null;
		fileType = null;
		log.info("<=== Ends EmpTrainingBean.nullifyingCreateTrainingRequestObject");
	}

	public String submitCreateTrainingRequest() {
		log.info("<=== Starts EmpTrainingBean.submitCreateTrainingRequest data : " + createTrainingRequestDTO);
		String result = null;

		log.info("::::::::::::::TraningTypeID:::::::::::::::::", createTrainingRequestDTO.getTrainingTypeId());
		List<TrainingTypeMaster> trainingType = commonDataService.loadTrainingType();
		TrainingTypeMaster selectedTrainingType = new TrainingTypeMaster();
		for (TrainingTypeMaster trainingtype : trainingType) {
			if (trainingtype.getId().equals(createTrainingRequestDTO.getTrainingTypeId())) {
				selectedTrainingType = trainingtype;
				break;
			}
		}
		try {
			if (createTrainingRequestDTO.getInstitutionId() == null
					&& selectedTrainingType.getName().equalsIgnoreCase("External")) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.SELECT_VALID_INSTITUTION_NAME).getErrorCode());
				return null;
			}
			createTrainingRequestDTO.setForwardToId(forwardTo != null ? forwardTo.getId() : null);
			BaseDTO response = httpService.post(EMP_TRAINING_URL + "/saveorupdate", createTrainingRequestDTO);
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_SAVE_SUCCESS.getErrorCode());
				nullifyingCreateTrainingRequestObject();
				result = EMP_TRAINING_LIST_PAGE;
			} else {
				errorMap.notify(response.getStatusCode());
			}
			log.info("<=== Ends EmpTrainingBean.submitCreateTrainingRequest ========>");
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving EmpTrainingBean " + e.getMessage());
		}
		log.info("<=== Ends EmpTrainingBean.submitCreateTrainingRequest() ======>");
		return result;
	}

	private List<TrainingTypeMaster> trainingTypeMasterList() {
		log.info("<=== Starts EmpTrainingBean.trainingTypeMasterList() ========>");
		List<TrainingTypeMaster> trainingTypeMasterList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getinternalexternaltrainingtype";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());

					trainingTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<TrainingTypeMaster>>() {
					});
					log.info("Total no. of Training Type : " + trainingTypeMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get TrainingTypeMasterList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in trainingTypeMasterList() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.trainingTypeMasterList() ========>");
		return trainingTypeMasterList;
	}

	private List<SupplierMaster> loadTrainingInstitutionList() {
		log.info("<=== Starts EmpTrainingBean.loadTrainingInstitutionList() ========>");
		List<SupplierMaster> supplierMasterList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getallactivetraininginstitute";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());

					supplierMasterList = mapper.readValue(jsonValue, new TypeReference<List<SupplierMaster>>() {
					});
					log.info("Total no. of Training Institution : " + supplierMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get SupplierMasterList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadTrainingInstitutionList() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.loadTrainingInstitutionList() ========>");
		return supplierMasterList;
	}

	public List<SupplierMaster> loadTrainingInstitutionListByAutoComplete(String name) {
		log.info("<=== Starts EmpTrainingBean.loadTrainingInstitutionListByAutoComplete() ========>");
		List<SupplierMaster> supplierMasterList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getallactivetraininginstituteForAutoComplete/" + name;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());

					supplierMasterList = mapper.readValue(jsonValue, new TypeReference<List<SupplierMaster>>() {
					});
					log.info("Total no. of Training Institution : " + supplierMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get SupplierMasterList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadTrainingInstitutionListByAutoComplete() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.loadTrainingInstitutionListByAutoComplete() ========>");
		return supplierMasterList;
	}

	private List<VenueMaster> loadVenueMasterList(String typeOfTraining) {
		log.info("<=== Starts EmpTrainingBean.loadVenueMasterList() ======>");
		List<VenueMaster> venueMasterList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getvenuebyenvironment/" + typeOfTraining;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());

					venueMasterList = mapper.readValue(jsonValue, new TypeReference<List<VenueMaster>>() {
					});
					log.info("Total no. of VenueMaster : " + venueMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get VenueMasterList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadVenueMasterList() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.loadVenueMasterList() ======>");
		return venueMasterList;
	}

	private List<Department> loadDepartmentList() {
		log.info("<=== Starts EmpTrainingBean.loadDepartmentList() ======>");
		List<Department> departmentList = null;
		try {
			String requestPath = EMP_TRAINING_URL + "/getallactivedepartment";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());

					departmentList = mapper.readValue(jsonValue, new TypeReference<List<Department>>() {
					});
					log.info("Total no. of Department : " + departmentList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			} else {
				log.error(" get DepartmentList Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadDepartmentList() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.loadDepartmentList() ======>");
		return departmentList;
	}

	public String editTraining() {
		log.info("<=== Starts EmpTrainingBean.editTraining ========>");
		String response = null;
		if (selectedEmpTrainingRowDtoListPage != null && selectedEmpTrainingRowDtoListPage.getId() != null) {
			EmpTraining empTraining = loadNomineeDataByTrainingId(selectedEmpTrainingRowDtoListPage.getId());
			if (empTraining != null) {
				log.info("Successfully loaded EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());

				loadAllCreateTrainingPageDropDown();

				createTrainingRequestDTO = new ExternalInternalTrainingRequestDTO();
				// common
				createTrainingRequestDTO.setId(empTraining.getId());
				createTrainingRequestDTO.setTrainingTypeId(empTraining.getTrainingTypeMaster().getId());
				createTrainingRequestDTO.setTrainingName(empTraining.getTrainingName());
				createTrainingRequestDTO.setVenueId(empTraining.getVenue().getId());
				createTrainingRequestDTO.setStartDate(empTraining.getScheduleStartDate());
				createTrainingRequestDTO.setEndDate(empTraining.getScheduleEndDate());
				createTrainingRequestDTO.setNoOfdays(empTraining.getNoOfDays());
				createTrainingRequestDTO.setReason(empTraining.getReason());

				if (empTrainingNotesViewPage != null) {
					EmpTrainingNoteDTO empTrainingNoteDTO = null;
					for (EmpTrainingNoteDTO note : empTrainingNotesViewPage) {
						if (empTrainingNoteDTO == null) {
							empTrainingNoteDTO = note;
						} else {
							if ((note.getId()) < empTrainingNoteDTO.getId()) {
								empTrainingNoteDTO = note;
							}
						}
					}
					createTrainingRequestDTO.setNote(empTrainingNoteDTO.getNote());
					createTrainingRequestDTO.setForwardToId(forwardTo != null ? forwardTo.getId() : null);
				}

				if (empTrainingLogListViewPage != null) {
					EmpTrainingLog empTrainingLog = null;
					for (EmpTrainingLog log : empTrainingLogListViewPage) {
						if (empTrainingLog == null) {
							empTrainingLog = log;
						} else {
							if ((log.getId()) < empTrainingLog.getId()) {
								empTrainingLog = log;
							}
						}
					}
					createTrainingRequestDTO.setComments(empTrainingLog.getRemarks());
				}

				if (empTraining.getTrainingTypeMaster().getName().equals(INTERNAL)) {
					internalTypeFlag = true;
					externalTypeFlag = false;
					venueMasterListCreatePage = loadVenueMasterList(empTraining.getTrainingTypeMaster().getName());
					createTrainingRequestDTO.setDepartmentId(empTraining.getDepartment().getId());
					createTrainingRequestDTO.setTopicOfTheTraining(empTraining.getTrainingTopic());
					createTrainingRequestDTO.setTrainerName(empTraining.getTrainerName());

				} else if (empTraining.getTrainingTypeMaster().getName().equals(EXTERNAL)) {
					externalTypeFlag = true;
					internalTypeFlag = false;
					venueMasterListCreatePage = loadVenueMasterList(empTraining.getTrainingTypeMaster().getName());
					createTrainingRequestDTO.setInstitutionId(empTraining.getTrainingInstitute().getId());
					createTrainingRequestDTO.setProgramFees(empTraining.getProgramFees());
					createTrainingRequestDTO.setReferenceNumber(empTraining.getReferenceNo());
					createTrainingRequestDTO.setReferenceDate(empTraining.getReferenceDate());
				}

				trainingTypeDropDownEditPageDisableFlag = true;
				response = this.EMP_TRAINING_CREATE_TRAINING_PAGE;
			} else {
				log.info("Failed to load EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());
			}
		} else {
			log.error("User have not selected any row");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		log.info("<=== Ends EmpTrainingBean.editTraining ========>");
		return response;
	}

	/**
	 * View Training Request Page
	 */
	public String viewTrainingRequestDetailsPage() {
		log.info("<=== Starts EmpTrainingBean.viewTrainingRequestDetailsPage ========>");
		String response = this.EMP_TRAINING_LIST_PAGE;
		if (selectedEmpTrainingRowDtoListPage != null && selectedEmpTrainingRowDtoListPage.getId() != null) {
			EmpTraining empTraining = loadNomineeDataByTrainingId(selectedEmpTrainingRowDtoListPage.getId());
			if (empTraining != null) {
				log.info("Successfully loaded EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());

				if (empTrainingNotesViewPage == null) {
					log.info(
							"Failed to load EmpTrainingNote data for id :" + selectedEmpTrainingRowDtoListPage.getId());
				} else {
					// getting last note
					for (EmpTrainingNoteDTO noteDTO : empTrainingNotesViewPage) {
						if (lastNoteDto == null) {
							lastNoteDto = noteDTO;
						} else {
							if (noteDTO.getId() > lastNoteDto.getId()) {
								lastNoteDto = noteDTO;
							}
						}
					}
					if(!CollectionUtils.isEmpty(empTrainingNotesViewPage)) {
						EmpTrainingNoteDTO noteDTO = empTrainingNotesViewPage.get(0);
						if(lastNoteDto != null) {
							lastNoteDto.setNote(noteDTO.getNote());
						}
					}
					approveOrRejectTrainingRequestDtoViewPage = new EmployeeTraineesDTO();
					if (lastEmpTrainingNote != null) {
						approveOrRejectTrainingRequestDtoViewPage
								.setForwardToId(lastEmpTrainingNote.getForwardTo().getId());
						approveOrRejectTrainingRequestDtoViewPage.setForwardFor(lastEmpTrainingNote.getFinalApproval());
						log.info("visibleStatus called....createdBy Id is......"
								+ lastEmpTrainingNote.getForwardTo().getId());
						log.info("loginUserID::" + loginBean.getUserDetailSession().getId());
						Long loginId = loginBean.getUserMaster().getId();
						previousApproval = lastEmpTrainingNote.getFinalApproval();
						// if (lastEmpTrainingNote.getForwardTo().getId().equals(loginId)) {
						// approvalFlag = true;
						// }else if(true == previousApproval &&
						// "APPROVED".equals(selectedEmpTrainingRowDtoListPage.getStatus())) {
						// buttonFlag = true;
						// approvalFlag = true;
						// }else {
						// approvalFlag = false;
						// }

						if (lastEmpTrainingNote.getForwardTo().getId().equals(loginId)) {
							approvalFlag = true;

							if (true == previousApproval
									&& "APPROVED".equals(selectedEmpTrainingRowDtoListPage.getStatus())) {
								buttonFlag = true;
							}

						} else {
							approvalFlag = false;
						}

					} else {
						log.info("lastEmpTrainingNote is null");
						approvalFlag = false;
					}

					empTrainingNominationPage = empTraining;
					forwardToUsersList = commonDataService.loadForwardToUsersByFeature("INTERNAL_EXTERNAL_TRAINING");
					updateInternalOrExternalViewPanel();
					response = this.EMP_TRAINING_VIEW_PAGE;
				}
			} else {
				log.info("Failed to load EmpTraining data for id :" + selectedEmpTrainingRowDtoListPage.getId());
			}
		} else {
			log.error("User have not selected any row");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends EmpTrainingBean.viewTrainingRequestDetailsPage ========>");
		return response;
	}

	public String cancelViewTrainingRequest() {
		log.info("<=== Starts EmpTrainingBean.cancelViewTrainingRequest");
		String result = "listInternalExternalTraining.xhtml";
		nullifyingViewTrainingRequestObject();
		log.info("<=== Ends EmpTrainingBean.cancelViewTrainingRequest() ======>");
		return EMP_TRAINING_LIST_PAGE;
	}

	private void nullifyingViewTrainingRequestObject() {
		log.info("<=== Starts EmpTrainingBean.nullifyingViewTrainingRequestObject() ======>");
		// nullifying objctes
		empTrainingMemberListViewPage = null;
		empTrainingLogListViewPage = null;
		empTrainingNominationPage = null;
		empTrainingNotesViewPage = null;
		approveOrRejectTrainingRequestDtoViewPage = null;
		log.info("<=== Ends EmpTrainingBean.nullifyingViewTrainingRequestObject() ======>");
	}
	
		public void getDownloadFile() {
			InputStream input = null;
			try {
				if(null != empTrainingNominationPage && null != empTrainingNominationPage.getFilePath()){
				File files = new File(empTrainingNominationPage.getFilePath());
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
				log.error("exception in filedownload method------- " + files.getName());
				}
			} catch (FileNotFoundException e) {
				log.error("exception in filedownload method------- " + e);
			} catch (Exception exp) {
				log.error("exception in filedownload method------- " + exp);
			}

		}
		
	/**
	 * Delete Create Nominee Page
	 */
	public String deleteTraining() {
		log.info("<=== Starts EmpTrainingBean.deleteTraining ========>");
		String result = this.EMP_TRAINING_LIST_PAGE;
		if (selectedEmpTrainingRowDtoListPage != null && selectedEmpTrainingRowDtoListPage.getId() != null) {
			String url = EMP_TRAINING_URL + "/delete/" + selectedEmpTrainingRowDtoListPage.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_DELETE_SUCCESS.getErrorCode());
				log.info("Training Request Saved Successful");
			} else {
				errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_DELETE_FAILURE.getErrorCode());
				log.info("Training Request Saved Failed");
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmPetitionRegisterDelete').hide();");
		} else {
			log.error("User have not selected any row");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		log.info("<=== Ends EmpTrainingBean.deleteTraining ========>");
		return EMP_TRAINING_LIST_PAGE;
	}

	public void selectApproveOrRejected(String status) {
		log.info("<=== Starts EmpTrainingBean.submitApprovalOrRejection =======>");
		approveOrRejectTrainingRequestDtoViewPage.setStage(status);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgComments1').show();");
		log.info("<=== Ends EmpTrainingBean.submitApprovalOrRejection =======>");
	}

	public String submitApprovalOrRejection() {
		log.info("<=== Starts EmpTrainingBean.submitApprovalOrRejection data : "
				+ approveOrRejectTrainingRequestDtoViewPage);
		String result = null;
		try {
			if (approveOrRejectTrainingRequestDtoViewPage.getRemarks() == null
					|| approveOrRejectTrainingRequestDtoViewPage.getRemarks().trim().isEmpty()) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlgComments1').show();");
			}

			if (approveOrRejectTrainingRequestDtoViewPage.getStage().equalsIgnoreCase("APPROVED")) {
				approveOrRejectTrainingRequestDtoViewPage.setForwardToId(forwardTo != null ? forwardTo.getId() : null);
			}
			
			approveOrRejectTrainingRequestDtoViewPage.setEmpTraining(new EmpTraining());
			approveOrRejectTrainingRequestDtoViewPage.getEmpTraining().setId(empTrainingNominationPage.getId());
			approveOrRejectTrainingRequestDtoViewPage.setNote(lastNoteDto.getNote());

			Long noteParentId = null;
			for (EmpTrainingNoteDTO note : empTrainingNotesViewPage) {
				if (noteParentId == null) {
					noteParentId = note.getId();
				} else {
					if (note.getId() > noteParentId) {
						noteParentId = note.getId();
					}
				}
			}
			approveOrRejectTrainingRequestDtoViewPage.setNoteParentId(noteParentId);
			BaseDTO response = httpService.post(EMP_TRAINING_URL + "/approvalorrejection",
					approveOrRejectTrainingRequestDtoViewPage);
			if (response.getStatusCode() == 0) {
				if (approveOrRejectTrainingRequestDtoViewPage.getStage().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_APPROVED_SUCCESSFULLY.getErrorCode());
				} else if (approveOrRejectTrainingRequestDtoViewPage.getStage().equalsIgnoreCase("FINAL_APPROVED")) {
					errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_FINAL_APPROVED_SUCCESSFULLY.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_REJECTED_SUCCESSFULLY.getErrorCode());
				}
				nullifyingViewTrainingRequestObject();
				result = EMP_TRAINING_LIST_PAGE;
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving EmpTrainingBean " + e.getMessage());
		}
		log.info("<=== Ends EmpTrainingBean.submitApprovalOrRejection() ======>");
		return result;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("<=== Starts EmpTrainingBean.uploadDocument() ======>");
		try {
			UploadedFile file = event.getFile();
			fileType = file.getFileName();
			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")
					&&!fileType.contains(".PDF") && !fileType.contains(".JPG") && !fileType.contains(".PNG")
					&& !fileType.contains(".JPEG") && !fileType.contains(".DOCX") && !fileType.contains(".DOC")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
			}
			long size = file.getSize();
			// size = size / (1024*1024);
			if ((size / (1024 * 1024)) > 2) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
			}
			if (size > 0) {
				String createEmpTrainingPathName = commonDataService.getAppKeyValue("EMPLOYEE_TRAINING_FILE_PATH");
				String filePath = Util.fileUpload(createEmpTrainingPathName, file);
				empTrainingDocumentFileName = filePath;
				createTrainingRequestDTO.setFileUploadPath(filePath);
				errorMap.notify(ErrorDescription.EMP_TRAINING_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Uploading Document is uploaded successfully");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("<<==  Error in file upload " + e.getMessage());
		}
		log.info("<=== Ends EmpTrainingBean.uploadDocument() ======>");
	}

	public void getEmployeesByDesignationIds() {
		log.info("Enter into EmpTrainingBean.getEmployeesByDesignationIds:::::");
		try {
			selectedEmployeeMasterIdsListNominationPage = null;
			employeeMasterList = new ArrayList<EmployeeMaster>();

			// String url = EMP_TRAINING_URL + "/getemployeebydesignationids";
			String url = EMP_TRAINING_URL + "/getemployeebydesignationandentity";
			log.info("getEmployeesByDesignationIds Url is================>>>>>>>" + url);

			if (!designationIds.isEmpty()) {
				log.info("getEmployeesByDesignationIds designationIds is================>>>>>>>"
						+ designationIds.toString());

				BaseDTO response = httpService.post(url, designationIds);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					employeeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
					log.info("employeeMasterList is================>>>>>>>" + employeeMasterList.toString());

				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				log.info("designationIds is Empty================>>>>>>>");
				Util.addWarn("Please Choose Designation Id to Dropdown");
			}
		} catch (Exception e) {
			log.info("Exception occured in EmpTrainingBean.getEmployeesByDesignationIds Exception is=>>>>>" + e);
		}
		log.info("Exit into EmpTrainingBean.getEmployeesByDesignationIds:::::");
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("EmpTrainingBean.showViewListPage() - START");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String requestId = "";
		String notificationId = "";
		String trainingId = "";
		String trainingReportId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			requestId = httpRequest.getParameter("requestId");
			notificationId = httpRequest.getParameter("notificationId");
			trainingId = httpRequest.getParameter("trainingId");
			trainingReportId =  httpRequest.getParameter("trainingReportId");
			log.info("EmpTrainingBean.showViewListPage() - " + notificationId);

		}
		if (StringUtils.isNotEmpty(requestId)) {
			selectedEmpTrainingRowDtoListPage = new EmployeeTrainingResponseDTO();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedEmpTrainingRowDtoListPage.setNotificationId(Long.parseLong(notificationId));
			}

			selectedEmpTrainingRowDtoListPage.setId(Long.parseLong(requestId));
			createNomineePage();
		}

		if (StringUtils.isNotEmpty(trainingId)) {
			selectedEmpTrainingRowDtoListPage = new EmployeeTrainingResponseDTO();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedEmpTrainingRowDtoListPage.setNotificationId(Long.parseLong(notificationId));
			}

			selectedEmpTrainingRowDtoListPage.setId(Long.parseLong(trainingId));
			viewTrainingRequestDetailsPage();
		}
		
		if (StringUtils.isNotEmpty(trainingReportId)) {
			selectedEmpTrainingRowDtoListPage = new EmployeeTrainingResponseDTO();
			if (!StringUtils.isEmpty(notificationId)) {
				selectedEmpTrainingRowDtoListPage.setNotificationId(Long.parseLong(notificationId));
			}

			selectedEmpTrainingRowDtoListPage.setId(Long.parseLong(trainingReportId));
			generateTrainingReportPage();
		}
		return null;
	}

}
