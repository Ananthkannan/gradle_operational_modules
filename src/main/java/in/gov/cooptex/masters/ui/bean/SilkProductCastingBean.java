package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.operation.model.ProductCostingMaster;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("silkProductCastingBean")
@Scope("session")
public class SilkProductCastingBean {
	
	
	
	private final String CREATE_PAGE = "/pages/masters/createSilkProductCostingMaster.xhtml?faces-redirect=true;";
	private final String List_Page = "/pages/masters/listSilkProductCostingMaster.xhtml?faces-redirect=true;";

	private final String View_Page = "/pages/masters/viewSilkProductCostingMaster.xhtml?faces-redirect=true;";
	private String serverURL = null;
	
	
	@Autowired
	AppPreference appPreference;
	
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	private String action;
	
	@Getter
	@Setter
	ProductCostingMaster productCostingMaster;
	
	@Getter
	@Setter
	ProductCostingMaster beforeEditObject;
	
	
	@Getter @Setter
	ProductCostingMaster selectedProductCostingMaster;
	
	@Setter @Getter
	LazyDataModel<ProductCostingMaster> productCostingMasterLazyList;
	
	@Setter @Getter
	List<ProductCostingMaster> productCostingMasterList;
	
	@Getter @Setter
	 boolean saveButton = true;
	
	@Getter @Setter
	 boolean updateButton = false;
	
	@Getter
	@Setter
	boolean disableAddButton = false;
	@Getter
	@Setter
	boolean disableSuplierMasterDropDown = false;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	
	
	@Autowired
	CommonDataService commonDataService;
	
	boolean forceLogin = false;

	ObjectMapper mapper;

	String jsonResponse;
	
	
	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList = new ArrayList<SupplierMaster>();
	
	@PostConstruct	
	public void init() {
		log.info("inside the init method of  of silkProductCosting");
		productCostingMaster = new ProductCostingMaster();
		disableSuplierMasterDropDown = false;
		loadValues();
		loadLazySilkProductCostingList();
		loadValuesForSuplierMasterDropDownS();
	}
	
	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}
	
	
	
	private void loadValuesForSuplierMasterDropDown() {
		supplierMasterList = commonDataService.loadAllSupplierMaster();
		log.info("list returened from data base size is"+supplierMasterList.size());
		
	}
	
	public List<SupplierMaster> getSupplierMasterCodeAndName(String query) {
		log.info("Institute Autocomplete query==>" + query);
		List<String> results = new ArrayList<String>();
		Long id=null;
		if (query != null && !StringUtils.isBlank(query)) {
			supplierMasterList = loadProductVarietyListByAutoComplete(query);
		}
		else {
			AppUtil.addWarn("Enter Supplier Code or Name");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		return supplierMasterList;
	}
	
	public List<SupplierMaster> loadProductVarietyListByAutoComplete(String name) {
		log.info("<=== Starts EmpTrainingBean.loadTrainingInstitutionListByAutoComplete() ========>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url =  serverURL+"/supplier/master/loadsupplierlistautocompleteByCodeOrName/"+name;
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
			}
		} catch (Exception exp) {
			log.error("<--- Exception in loadProductVarietyListByAutoComplete() --->", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=== Ends EmpTrainingBean.loadProductVarietyListByAutoComplete() ========>");
		return supplierMasterList;
	}
	
	
private void loadValuesForSuplierMasterDropDownS() {
		
		try {
			BaseDTO baseDTO = new BaseDTO();
			log.info("SuplierMasterDropDown"+AppUtil.getPortalServerURL());
			log.info("SuplierMasterDropDown"+AppUtil.getPortalServerURL());
			log.info("SuplierMasterDropDown"+serverURL);
			String url =  serverURL+"/supplier/master/getAllSuplierMaster";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.info("cause for error"+e.getMessage());
			log.error("Exception occured .... ", e);
		}

		
	}







public void checkTheDateOverlapsInProductCostingMasterForSupplierId() throws UIException {
	

		log.info("the object we are going to check for date overlaps is"+ " "+productCostingMaster);
		
		BaseDTO baseDTO = new BaseDTO();
		
		String url = serverURL + appPreference.getOperationApiUrl() + "/SilkProductCostingControler/checkTheDuplicateDateRangeForSilkCostingBySuplierId";
		baseDTO = httpService.post(url, productCostingMaster);
		
		if (baseDTO.getTotalRecords() > 0) {
			log.info("the totalRecords is "+baseDTO.getTotalRecords());
			log.info("the combination is Aleready Exist");	
			throw new UIException("The Date range   is Aleready Exist");
		}
		
	}


public boolean checkTheDateOverlapsInSocietyProductAppraisalBySuplierId()  {
	
	log.info("the object we are going to check for date overlaps is"+ " "+productCostingMaster);
	BaseDTO baseDTO = new BaseDTO();
	String url = serverURL + appPreference.getOperationApiUrl() + "/SilkProductCostingControler/checkTheDateOverlapsInSocietyProductAppraisalBySuplierId";
	baseDTO = httpService.post(url, productCostingMaster);
	if (baseDTO.getTotalRecords() > 0) {
		log.info("the totalRecords is "+baseDTO.getTotalRecords());
		log.info("the combination is Aleready Exist");	
		return false;
	}
	return true;
	
}

private void showClientError(String errorMessage) {
	log.info("inside the showClientError method");
	AppUtil.addError(errorMessage);
}

public String  save()  {
	log.info("the save  silkProductCasting is started");
	try {		
	validateInput();	
	checkTheDateOverlapsInProductCostingMasterForSupplierId();
			
			 	
					log.info("...... productCostingMaster submit begin ...."+productCostingMaster.toString());
					BaseDTO baseDTO = new BaseDTO() ;
					String SaveUrl = serverURL + appPreference.getOperationApiUrl() +"/SilkProductCostingControler/SaveOrUpdateSilkProductCasting";
					baseDTO = httpService.post(SaveUrl, productCostingMaster);
					
					log.info(" baseDTo return From Data Base"+baseDTO.toString());
					
					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {
							log.info("productCostingMaster is saved successfully");
							productCostingMaster = new ProductCostingMaster();	
							//errorMap.notify(baseDTO.getStatusCode());
							errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_COASTING_CREATED_SUCCESSFULLY).getErrorCode());
							return  List_Page;
						}
					}
			} catch (UIException e) {
				  log.info("Inside UIException");
					showClientError(e.getMessage());
					log.error("UIException at addItem() >>>> " + e.getMessage());
				
				}catch (Exception ex) {
					 log.info("Inside Exception");
					log.error("Exception at addItem() >>>> ", ex);
					
				}	
			
			
			return null;
			
		}



public String update() {
	
	log.info("the update silkProductCasting is started");
	try {
		validateInput();	
		
//		log.info("beforeEditObject"+beforeEditObject);
//		log.info("(productCostingMaster.getFromDate() == beforeEditObject.getFromDate()"+ productCostingMaster.getFromDate()+  beforeEditObject.getFromDate() + (productCostingMaster.getFromDate() == beforeEditObject.getFromDate()));
//		log.info("(productCostingMaster.getId() == beforeEditObject.getId()"+productCostingMaster.getToDate()+beforeEditObject.getToDate()+(productCostingMaster.getToDate() ==  beforeEditObject.getToDate()));
//		
//		if((productCostingMaster.getId() == beforeEditObject.getId()) &&  (productCostingMaster.getFromDate() == beforeEditObject.getFromDate() )  && (productCostingMaster.getToDate() ==  beforeEditObject.getToDate()) ) {
//			 log.info("From date and two date have not change");
//			 
//		}else { 
//			boolean  checkDateOverlapAfterEdit = checkTheDateOverlapsInSocietyProductAppraisalBySuplierId();
//			log.info("checkDateOverlapAfterEdit"+checkDateOverlapAfterEdit);
//			if(checkDateOverlapAfterEdit) {
//				checkTheDateOverlapsInProductCostingMasterForSupplierId();
//			}else {
//				throw new UIException("Not allowed to Edit This File");
//			}
//			
//		}
			
				
						log.info("...... productCostingMaster submit begin ...."+productCostingMaster.toString());
						BaseDTO baseDTO = new BaseDTO() ;
						String SaveUrl = serverURL + appPreference.getOperationApiUrl() +"/SilkProductCostingControler/SaveOrUpdateSilkProductCasting";
						baseDTO = httpService.post(SaveUrl, productCostingMaster);
						
						log.info(" baseDTo return From Data Base"+baseDTO.toString());
						
						if (baseDTO != null) {
							if (baseDTO.getStatusCode() == 0) {
								log.info("productCostingMaster is saved successfully");
								productCostingMaster = new ProductCostingMaster();	
								//errorMap.notify(baseDTO.getStatusCode());
								errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_COASTING_UPDATED_SUCCESSFULLY).getErrorCode());
								return  List_Page;
							}
						}
				} catch (UIException e) {
					  log.info("Inside UIException");
						showClientError(e.getMessage());
						log.error("UIException at addItem() >>>> " + e.getMessage());
					
					}catch (Exception ex) {
						 log.info("Inside Exception");
						log.error("Exception at addItem() >>>> ", ex);
						
					}	
				
				
				return null;
				
	
}

private void validateInput() throws UIException {
	log.info("productCostingMaster"+productCostingMaster);
		if (productCostingMaster == null) {
			throw new UIException("Invalid input");
		} else if (productCostingMaster.getSupplierMaster() == null) {
			throw new UIException("Please select SupplierMaster");
		} else if (productCostingMaster.getSilkCost() == null) {
			throw new UIException("Please Enter Silk Cost");
		} else if (productCostingMaster.getJariCost() == null) {
			throw new UIException("Please Enter Zari Cost");
		} else if (productCostingMaster.getTechJariCost() == null) {
			throw new UIException("Please Entere Tech Zari Cost");
		} else if (productCostingMaster.getFromDate() ==  null) {
			throw new UIException("Please Select From Date");
		} else if (productCostingMaster.getToDate() ==  null) {
			throw new UIException("Please Select To Date");
		}
	
	

}


	public String goToCreatePage() {
		productCostingMaster = new ProductCostingMaster();
		
		disableSuplierMasterDropDown = false;
			return CREATE_PAGE;
	}


	public String editSilkProductCosting() {
		updateButton = true;
		saveButton = false;
		log.info("<<<<< -------Start editProductVariety called ------->>>>>> ");
		
		try {
				
				if (productCostingMaster.getId() == null) {
					Util.addWarn("Please select one Silk Product Costing");
					return null;
				} else {
					disableAddButton = false;
					log.info("the suplier id is"+ productCostingMaster.getSupplierMaster().getId());
					log.info("the from_date is"+ productCostingMaster.getFromDate());
					log.info("the to_date is"+ productCostingMaster.getToDate());
					boolean  checkDateOverlap = checkTheDateOverlapsInSocietyProductAppraisalBySuplierId();
					log.info("check"+checkDateOverlap);
					if(checkDateOverlap) {
						beforeEditObject = new ProductCostingMaster();
						beforeEditObject = productCostingMaster;
						disableSuplierMasterDropDown = true;
						return CREATE_PAGE;
					}else {
						throw new UIException("Not allowed to Edit This File");
					}
				
				}
			
		}catch (UIException e) {
			  log.info("Inside UIException");
				showClientError(e.getMessage());
				log.error("UIException at addItem() >>>> " + e.getMessage());
			
			}catch (Exception ex) {
				 log.info("Inside Exception");
				log.error("Exception at addItem() >>>> ", ex);
				
			}	
		
			
		return null;	
		}
	



	public String gotoViewPage() {
		
		
	
		log.info("<----Redirecting to user view page---->");
		if (productCostingMaster.getId() == null) {
			Util.addWarn("Please select one Silk Product Costing");
			return null;
		}
		
		log.info("the selected  Silk Product Costing is"+productCostingMaster);
		disableAddButton = false;
		
		return View_Page;
		
	}



	public String deleteSilkProductCosting() {
		try {
			
			if (productCostingMaster.getId() == null) {
				Util.addWarn("Please select one Silk Product Costing");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSilkProductCostingDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}
	


	public String deletetSilkProduCtCosting() {
	
		try {
			log.info("Delete deletetSilkProduCtCosting started the id is [" +productCostingMaster.getId()  + "]");
			ProductCostingMaster productCostingMasterDelete = new ProductCostingMaster();
			productCostingMasterDelete.setId(productCostingMaster.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL  +appPreference.getOperationApiUrl()+ "/SilkProductCostingControler/deletetSilkProduCtCostingById";
			baseDTO = httpService.post(url, productCostingMasterDelete);
			
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("Country deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					productCostingMaster = new ProductCostingMaster();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;
	
					return forceLogin();
	
				} else {
					log.info("Error occured in Rest api ... ");
					//errorMap.notify(baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.PRODUCT_COASTING_DELETED_SUCCESSFULLY).getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting EmployeeOtConfirm  ....", e);
		}
		return null;
	}


	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}



	public String clear() {
		log.info("<---- Start productVarietyTax . clear ----->");
		init();
		selectedProductCostingMaster = new ProductCostingMaster();
		log.info("<---- End productVarietyTax . clear ----->");
		disableAddButton = false;
		disableSuplierMasterDropDown = false;
		return List_Page;
	
	}


	public void loadLazySilkProductCostingList() {
		disableAddButton = false;
		log.info("<===== Starts loadLazySilkProductCostingList======>");
		productCostingMasterLazyList = new LazyDataModel<ProductCostingMaster>() {
			
			private static final long serialVersionUID = -2007656114950592854L;
	
			@Override
			public List<ProductCostingMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL + appPreference.getOperationApiUrl()+"/SilkProductCostingControler/getSilkProductCastingListByLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get productVarietyTax list");
					log.info("ProductCostingMaster Total records"+ response.getTotalRecords());
					
					log.info("the object returnend from ProductCostingMasterControler"+response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						productCostingMasterList = mapper.readValue(jsonResponse,new TypeReference<List<ProductCostingMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} 
					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e.getCause());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"+ productCostingMasterList);
				log.info("Ends lazy load....");
				
				return productCostingMasterList ;
			}
	
			@Override
			public Object getRowKey(ProductCostingMaster res) {
				return res != null ? res.getId() : null;
			}
	
			@Override
			public ProductCostingMaster getRowData(String rowKey) {
				try {
				for (ProductCostingMaster productCostingMaster : productCostingMasterList) {
					if (productCostingMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedProductCostingMaster = productCostingMaster;
						return productCostingMaster;
					}
				}				
				}
				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
			
		};
		
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		productCostingMaster = ((ProductCostingMaster) event.getObject());
		disableAddButton = true;
	}
	


	
	
	
	
	

}
