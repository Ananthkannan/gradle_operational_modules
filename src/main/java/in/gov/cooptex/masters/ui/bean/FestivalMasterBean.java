package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.asset.model.AssetRegister;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.FestivalMaster;
import in.gov.cooptex.core.model.FestivalTypeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("festivalMasterBean")
public class FestivalMasterBean {
	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	FestivalMaster festivalMaster, selectedFestivalMaster;
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	@Setter
	@Getter
	List<FestivalTypeMaster> festivalTypeMasterList;
	
	@Setter
	@Getter
	List<FestivalMaster> festivalMasterList;
	@Getter
	@Setter
	String action;
	@Autowired
	ErrorMap errorMap;
	final String SERVER_URL = AppUtil.getPortalServerURL();
	@Autowired
	AppPreference appPreference;
	@Autowired
	HttpService httpService;
	ObjectMapper mapper;

	String jsonResponse;
	@Getter
	@Setter
	LazyDataModel<FestivalMaster> lazyFestivalMasterList;
	
	
	private final String CREATE_PAGE = "/pages/masters/createFestivalPeriodMaster.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listFestivalPeriodMaster.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE = "/pages/masters/viewFestivalPeriodMaster.xhtml?faces-redirect=true;";

	public String showFestivalPeriodListPageAction() {
		log.info("<===== Starts FestivalPeriodMasterBean.showFestivalPeriodListPageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedFestivalMaster == null || selectedFestivalMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create FestivalPeriod called..");
				festivalMaster = new FestivalMaster();
				loadAllFestivalTypes();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				loadAllFestivalTypes();
				log.info("edit FestivalPeriod called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/festivalmaster/get/"
						+ selectedFestivalMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					festivalMaster = mapper.readValue(jsonResponse, new TypeReference<FestivalMaster>() {
					});      
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View petitionType called..");

				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/festivalmaster/get/"
						+ selectedFestivalMaster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						festivalMaster = mapper.readValue(jsonResponse, new TypeReference<FestivalMaster>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete festivalmaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFestivalMasterDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showFestivalPeriodListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends FestivalPeriodMasterBean.showFestivalPeriodListPageAction ===========>" + action);
		return null;
	}
	public String deleteConfirm() {
		log.info("<===== Starts FestivalPeriodMasterBean.deleteConfirm ===========>" + selectedFestivalMaster.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/festivalmaster/deletebyid/" + selectedFestivalMaster.getId());
			
			if (response != null && response.getStatusCode() == 0) {
				AppUtil.addInfo("Festival Details Deleted Successfully");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmFestivalMasterDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete festivalmaster....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends FestivalPeriodMasterBean.deleteConfirm ===========>");
		return showfestivalMasterListPage();
	}
	public String saveOrUpdate() {
		log.info("<==== Starts FestivalPeriodMasterBean.saveOrUpdate =======>");
		try {
			

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/festivalmaster/saveorupdate";
			BaseDTO response = httpService.post(url, festivalMaster);

			if (response != null && response.getStatusCode() == 0) {
				log.info("FestivalPeriodMaster saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					AppUtil.addInfo("Festival Details Added Successfully");
				else
					AppUtil.addInfo("Festival Details Updated Successfully");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends FestivalPeriodMasterBean.saveOrUpdate =======>");
		return showfestivalMasterListPage();
	}
	public String showfestivalMasterListPage() {
		log.info("<==== Starts FestivalPeriodMasterBean.showfestivalMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		festivalMaster = new FestivalMaster();
		selectedFestivalMaster = new FestivalMaster();
		loadLazyFestivalMasterList();
		log.info("<==== Ends FestivalPeriodMasterBean.showfestivalMasterListPage =====>");
		return LIST_PAGE;
	}
	public List<FestivalTypeMaster> loadAllFestivalTypes() {
		log.info("<======== FestivalMasterBean.loadAllFestivalTypes calling ======>");
		festivalTypeMasterList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + "/festivaltypemaster/getActiveFestivalTypes";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				festivalTypeMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<FestivalTypeMaster>>() {
						});
			} else {
				festivalTypeMasterList = new ArrayList<>();
			}
			log.info("suppliermasterService organizationtypemasterList size" + festivalTypeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception suppliermasterService.loadAllOrganizationTypeMaster() ==========>", e);
		}
		return festivalTypeMasterList;
	}
	
	public void loadLazyFestivalMasterList() {
		log.info("<===== Starts FestivalMasterBean.loadLazyFestivalMasterList ======>");

		lazyFestivalMasterList = new LazyDataModel<FestivalMaster>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<FestivalMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/festivalmaster/getallfestivaltypemasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						festivalMasterList = mapper.readValue(jsonResponse, new TypeReference<List<FestivalMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("FestivalMasterBean size---------------->" + lazyFestivalMasterList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return festivalMasterList;
			}

			@Override
			public Object getRowKey(FestivalMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public FestivalMaster getRowData(String rowKey) {
				for (FestivalMaster festivalMaster : festivalMasterList) {
					if (festivalMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedFestivalMaster = festivalMaster;
						return festivalMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  FestivalMasterBean.loadLazyFestivalMasterList  ======>");
	}
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts FestivalMasterBean.onRowSelect ========>" + event);
		selectedFestivalMaster = ((FestivalMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends FestivalMasterBean.onRowSelect ========>" + selectedFestivalMaster.getId());
	}
}
