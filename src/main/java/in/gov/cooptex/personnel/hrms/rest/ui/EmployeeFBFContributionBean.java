package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.AppConfigRequest;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.personnel.hrms.model.EmpDeathRegister;
import in.gov.cooptex.personnel.hrms.model.EmpFbfContribution;
import in.gov.cooptex.personnel.hrms.model.NatureOfDeath;
import in.gov.cooptex.personnel.payroll.model.EmpPfSelfContribution;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("employeeFBFContributionBean")
@Scope("session")
@Log4j2
public class EmployeeFBFContributionBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8125282699784414088L;
	
	private static final String EMPLOYEE_FBF_CONTRIBUTION_CREATE_PAGE = "/pages/personnelHR/employeeProfile/createFBFContribution.xhtml?faces-redirect=true";
	
	private static final String EMPLOYEE_FBF_CONTRIBUTION_LIST_PAGE = "/pages/personnelHR/employeeProfile/listFBFContribution.xhtml?faces-redirect=true";
	
	private static final String EMPLOYEE_FBF_CONTRIBUTION_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewFBFContribution.xhtml?faces-redirect=true";
	
	private static final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Getter @Setter
	boolean addButtonFlag = true;
	
	@Getter @Setter
	LazyDataModel<EmpFbfContribution> employeeFBFContributionLazyList;
	
	@Getter @Setter
	String action;
	
	@Getter @Setter
	Map<String, Integer> monthNameMap;
	
	@Getter @Setter
	List<Integer> yearList;
	
	@Autowired
	ErrorMap errorMap;
	
	String jsonResponse;
	
	@Autowired
	HttpService httpService;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Getter @Setter
	Integer totalRecords;
	
	@Getter @Setter
	List<EmpDeathRegister> employeeList = new ArrayList<>();
	
	@Getter @Setter
	EmpFbfContribution empFbfContribution = new EmpFbfContribution();
	
	@Getter @Setter
	EmpFbfContribution selectedEmpFbfContribution = new EmpFbfContribution();
	
	public String showEmployeeFBFContributionListPage() {
		log.info("<<====== EmployeePfSelfContributionBean:showEmployeeFBFContributionListPage() ======>> ");
		try {
			loadEmployeeFBFContributionLazyList();
			getEmployeeListForFBFContribution();
			if(employeeList.size() == 0 || employeeList.isEmpty() || employeeList == null) {
				addButtonFlag = false;
			}else {
				addButtonFlag = true;
			}
			selectedEmpFbfContribution = new EmpFbfContribution();
		}catch(Exception e) {
			log.error("Exception at showEmployeeRequestListPage() - ",e);
		}
		return EMPLOYEE_FBF_CONTRIBUTION_LIST_PAGE;
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<<======= EmployeeFBFContributionBean:onRowSelect() Calling =======>>");
		try {
			addButtonFlag = false;
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:onRowSelect()",e);
		}
	}

	public void loadValues() {
		log.info("<<======= EmployeeFBFContributionBean:loadValues() Calling =======>>");
		try {
			monthNameMap = Util.getMonthIdsByDetailName();
			yearList = Util.getNextSixYearList();
			getEmployeeListForFBFContribution();
			getFbfContributionTotalAmtPlanned();
			getFbfContributionEmployerAmt();
			findCountForTotalPermenantEmployees();
			calculatePerEmployeeContributionAmount();
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:loadValues()",e);
		}
	}
	
	public void getFbfContributionTotalAmtPlanned() {
		log.info("<<======= EmployeeFBFContributionBean:getFbfContributionTotalAmtPlanned() Starts =======>>");
		AppConfigRequest appConfigRequest = null;
		try {
			appConfigRequest = new AppConfigRequest();
			appConfigRequest.setAppKey(AppConfigEnum.FBF_CONTRIBUTION_TOTAL_AMOUNT_PLANNED.toString());
			double totalPlannedAmount = getFbfContributionAmt(appConfigRequest);
			log.info("<<======== FbfContribution TotalPlannedAmount =======>> "+totalPlannedAmount);
			empFbfContribution.setTotalAmountPlanned(totalPlannedAmount);
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:getFbfContributionTotalAmtPlanned()",e);
		}
	}
	
	public void getFbfContributionEmployerAmt() {
		log.info("<<======= EmployeeFBFContributionBean:getFbfContributionEmployerAmt() Starts =======>>");
		AppConfigRequest appConfigRequest = null;
		try {
			appConfigRequest = new AppConfigRequest();
			appConfigRequest.setAppKey(AppConfigEnum.FBF_CONTRIBUTION_EMPLOYER_AMOUNT.toString());
			double employerAmount = getFbfContributionAmt(appConfigRequest);
			log.info("<<======== FbfContribution EmployerAmount =======>> "+employerAmount);
			empFbfContribution.setEmployerContribution(employerAmount);
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:getFbfContributionEmployerAmt()",e);
		}
	}
	
	public double getFbfContributionAmt(AppConfigRequest appConfigRequest) {
		log.info("<<======= EmployeeFBFContributionBean:getFbfContributionTotalAmtPlanned() Starts =======>> ");
		double amount = 0.00;
		try {
			String requestPath = SERVER_URL + "/appConfig/getvalue";
			BaseDTO baseDTO = httpService.post(requestPath,appConfigRequest);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					amount = mapper.readValue(jsonResponse, double.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in EmployeeFBFContributionBean:getFbfContributionTotalAmtPlanned() ", exp);
		}
		log.info("<<======= EmployeeFBFContributionBean:getFbfContributionTotalAmtPlanned() Ends =======>> ");
		return amount;
	}
	
	public void findCountForTotalPermenantEmployees() {
		log.info("<<======= EmployeeFBFContributionBean:findCountForTotalPermenantEmployees() Starts =======>> ");
		Long employeeCount = 0L;
		try {
			String requestPath = SERVER_URL + "/empfbfcontribution/findcountforpermenantemployees";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeCount = mapper.readValue(jsonResponse, Long.class);
					empFbfContribution.setTotalEmployees(employeeCount);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in EmployeeFBFContributionBean:getEmployeeListForFBFContribution() ", exp);
		}
		log.info("<<======= EmployeeFBFContributionBean:getEmployeeListForFBFContribution() Ends =======>> ");
	}
	
	public void calculatePerEmployeeContributionAmount() {
		log.info("<<======= EmployeeFBFContributionBean:calculatePerEmployeeContributionAmount() Starts =======>>");
		double totalEmployeeContributionAmount = 0.00;
		long totalPermenantEmployees = 0;
		double perEmployeeContributionAmount = 0.00;
		try {
			if(empFbfContribution.getTotalAmountPlanned() != null && empFbfContribution.getEmployerContribution() != null) {
				totalEmployeeContributionAmount =  empFbfContribution.getTotalAmountPlanned() - empFbfContribution.getEmployerContribution();
				log.info("totalEmployeeContributionAmount - "+totalEmployeeContributionAmount);
			}
			totalPermenantEmployees = empFbfContribution.getTotalEmployees();
			if(totalPermenantEmployees > 0) {
				perEmployeeContributionAmount = totalEmployeeContributionAmount / totalPermenantEmployees;
				log.info("perEmployeeContributionAmount - "+perEmployeeContributionAmount);
				empFbfContribution.setPerEmployeeContribution(Double.valueOf(AppUtil.DECIMAL_FORMAT.format(perEmployeeContributionAmount)));
			}
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:calculatePerEmployeeContributionAmount()",e);
		}
		log.info("<<======= EmployeeFBFContributionBean:calculatePerEmployeeContributionAmount() Ends =======>>");
	}
	
	
	public void getEmployeeListForFBFContribution() {
		log.info("<<======= EmployeeFBFContributionBean:getEmployeeListForFBFContribution() Starts =======>> ");
		try {
			String requestPath = SERVER_URL + "/empfbfcontribution/getemplistforfbfcontribution";
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmpDeathRegister>>() {
					});
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in EmployeeFBFContributionBean:getEmployeeListForFBFContribution() ", exp);
		}
		log.info("<<======= EmployeeFBFContributionBean:getEmployeeListForFBFContribution() Ends =======>> ");
	}
	
	public String employeeFBFContributionListAction() {
		log.info("<====== EmployeeFBFContributionBean:employeeFBFContributionListAction Starts =====>");
		try {
			if(action.equalsIgnoreCase("CREATE")) {
				log.info("<====== Employee FBF Contribution Create Page Calling =====>");
				loadValues();
				return EMPLOYEE_FBF_CONTRIBUTION_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception Occured in EmployeeFBFContributionBean:employeeFBFContributionListAction", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EmployeeFBFContributionBean:employeeFBFContributionListAction Ends====>");
		return null;
	}
	
	public String saveFBFContribution() {
		log.info("<<======= EmployeeFBFContributionBean:saveFBFContribution() ======>> ");
		BaseDTO baseDTO = null;
		try {
			String url = SERVER_URL + "/empfbfcontribution/create";
			baseDTO = httpService.post(url, empFbfContribution);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.FBF_CONTRIBUTION_SAVE_SUCCESS).getErrorCode());
				return showEmployeeFBFContributionListPage();
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
		}catch (Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:saveFBFContribution()", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}
	
	private void loadEmployeeFBFContributionLazyList() {
		log.info("<<============== EmployeeFBFContributionBean:loadEmployeeFBFContributionLazyList Strats ===========>> ");
		employeeFBFContributionLazyList = new LazyDataModel<EmpFbfContribution>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 2559910409973223615L;

			public List<EmpFbfContribution> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				String url = "";
				List<EmpFbfContribution> empFbfContribtuionList = null;
				try {
					empFbfContribtuionList = new ArrayList<>();
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					url = AppUtil.getPortalServerURL() + "/empfbfcontribution/loadLazylist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						empFbfContribtuionList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpFbfContribution>>() {
								});
						log.info("Employee FBF Contribtuion List Size - " + empFbfContribtuionList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadEmployeeFBFContributionLazyList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return empFbfContribtuionList;
			}

			@Override
			public Object getRowKey(EmpFbfContribution res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpFbfContribution getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<EmpFbfContribution> responseList = (List<EmpFbfContribution>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmpFbfContribution empFbfContributionObj : responseList) {
					if (empFbfContributionObj.getId().longValue() == value.longValue()) {
						selectedEmpFbfContribution = empFbfContributionObj;
						return empFbfContributionObj;
					}
				}
				return null;
			}
		};
	}
	
	public String gotoViewPage() {
		log.info("<<============== EmployeeFBFContributionBean:gotoViewPage() Strats ===========>> ");
		try {
			if (selectedEmpFbfContribution == null) {
				log.info("<---Please Select any one Record--->");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
		}catch(Exception e) {
			log.error("Exception at EmployeeFBFContributionBean:gotoViewPage()",e);
		}
		log.info("<<============== EmployeeFBFContributionBean:gotoViewPage() Ends ===========>> ");
		return EMPLOYEE_FBF_CONTRIBUTION_VIEW_PAGE;
	}

}
