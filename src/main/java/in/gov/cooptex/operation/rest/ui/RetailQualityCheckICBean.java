package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.PurchaseInvoice;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GovtSchemeQCDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.RejectReasonMaster;
import in.gov.cooptex.core.model.StockRejectDetails;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.ItemStatus;
import in.gov.cooptex.operation.enums.RetailQualityCheckStatus;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.GovtSchemeQC;
import in.gov.cooptex.operation.model.GovtSchemeQCItems;
import in.gov.cooptex.operation.model.QualityCheckProductWise;
import in.gov.cooptex.operation.model.RetailQualityCheck;
import in.gov.cooptex.operation.model.RetailQualityCheckDetails;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.RetailQualityCheckSearch;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailQualityCheckICBean")
@Scope("session")
public class RetailQualityCheckICBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String RETAIL_QUALITY_CHECK_LIST = "/pages/inspectionCenter/qc/listQualityCheck.xhtml?faces-redirect=true;";

	private final String RETAIL_QUALITY_CHECK_CREATE = "/pages/inspectionCenter/qc/createQualityCheck.xhtml?faces-redirect=true;";

	private final String RETAIL_QUALITY_CHECK_VIEW = "/pages/inspectionCenter/qc/viewQualityCheck.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Getter
	@Setter
	Long govtSchemeQCId;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String activeStatus;

	@Getter
	@Setter
	RetailQualityCheckSearch retailQualityCheckSearch;

	@Getter
	@Setter
	RetailQualityCheck retailQualityCheck;

	@Getter
	@Setter
	GovtSchemeQC govtSchemeQC;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	LazyDataModel<RetailQualityCheckSearch> retailQualityCheckSearchLazyList;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	@Getter
	@Setter
	QualityCheckProductWise selectedQualityCheckProductWise;

	@Getter
	@Setter
	Integer totalProducts;

	@Getter
	@Setter
	Integer qcCompleted;

	@Getter
	@Setter
	Integer qcInprogress;

	@Getter
	@Setter
	Integer qcYetToStart;

	@Getter
	@Setter
	Integer totalItems;

	@Getter
	@Setter
	Integer itemsAccepted;

	@Getter
	@Setter
	Integer itemsRejected;

	@Getter
	@Setter
	Integer itemsToBeCheck;

	@Getter
	@Setter
	Integer totalItemsForProduct;

	@Getter
	@Setter
	Integer itemsAcceptedForProduct;

	@Getter
	@Setter
	Integer itemsRejectedForProduct;

	@Getter
	@Setter
	Integer itemsToBeCheckForProduct;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	String errorMsg;

	@Getter
	@Setter
	Long loggedInWarehouseId;

	@Getter
	@Setter
	int index;

	@Getter
	@Setter
	ProductCategory productCategory;
	
	@Getter
	@Setter
	GovtSchemeQCDTO govtSchemeQCDTO=new GovtSchemeQCDTO();
	
	@Getter
	@Setter
	List<StockRejectDetails> stockRejectDetailsList=new ArrayList<>();
	
	@Getter
	@Setter
	List<StockRejectDetails> savedStockRejectDetailsList=new ArrayList<>();
	
	@Getter
	@Setter
	List<RejectReasonMaster> rejectReasonMasterList=new ArrayList<>();
	
	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsByQCList=new ArrayList<>();
	
	@Getter
	@Setter
	List<PurchaseInvoice> purchaseInvoiceList=new ArrayList<>();
	
	@Getter
	@Setter
	String purchaseInvoice=new String();

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		getProductWarehouseQualityCheckList();
		loadStatusValues();
		
	}

	public RetailQualityCheckICBean() {
		log.info("<#======Inside PurchaseOrderBean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	private void loadStatusValues() {

		Object[] statusArray = RetailQualityCheckStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public void loadLazyRetailQualityCheckList() {
		addButtonFlag = false;
		log.info("RetailQualityCheckICBean.loadLazyPurchaseOrderList method started");
		retailQualityCheckSearchLazyList = new LazyDataModel<RetailQualityCheckSearch>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailQualityCheckSearch> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<RetailQualityCheckSearch> data = new ArrayList<RetailQualityCheckSearch>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<RetailQualityCheckSearch>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(RetailQualityCheckSearch res) {
				return res != null ? res.getStockInwardNumber() : null;
			}

			@Override
			public RetailQualityCheckSearch getRowData(String rowKey) {
				List<RetailQualityCheckSearch> responseList = (List<RetailQualityCheckSearch>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RetailQualityCheckSearch res : responseList) {
					if (res.getId() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	private RetailQualityCheckSearch getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		RetailQualityCheckSearch request = new RetailQualityCheckSearch();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("stockInwardNumber")) {
				log.info("stock Inward Number : " + value);
				request.setStockInwardNumber(value);
			}

			if (entry.getKey().equals("senderCodeOrName")) {
				log.info("Sender Code Or Name : " + value);
				request.setSenderCodeOrName(value);
			}
			if (entry.getKey().equals("receiverCodeOrName")) {
				log.info("Receiver Code Or Name : " + value);
				request.setReceiverCodeOrName(value);
			}
			if (entry.getKey().equals("stockReceivedDate")) {
				log.info("stock Received Date : " + value);
				request.setStockReceivedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		request.setTransferType(StockTransferType.INSPECTION_INWARD);

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("RetailQualityCheckICBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		retailQualityCheckSearch = new RetailQualityCheckSearch();
		RetailQualityCheckSearch request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/quality/check/inspection/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("RetailQualityCheckICBean.getSearchData Method Complted");
		return baseDTO;
	}

	public String getProductWarehouseQualityCheckList() {
		retailQualityCheckSearch = new RetailQualityCheckSearch();
		loadLazyRetailQualityCheckList();
		return RETAIL_QUALITY_CHECK_LIST;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailQualityCheckICBean.onRowSelect method started");
		retailQualityCheckSearch = ((RetailQualityCheckSearch) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckICBean.onPageLoad method started");
		addButtonFlag = false;
	}

	public String getProductWarehouseQualityCheckCreate() {
		retailQualityCheck = new RetailQualityCheck();
		retailQualityCheck.setStockTransfer(new StockTransfer());
		loadRejectMaster();
		govtSchemeQC = new GovtSchemeQC();
		supplierMaster = new SupplierMaster();
		govtSchemeQCId = null;
		productCategory = new ProductCategory();
		retailQualityCheck.setQualityCheckProductWiseList(new ArrayList<>());
		totalProducts = null;
		qcCompleted = null;
		qcInprogress = null;
		qcYetToStart = null;
		totalItems = null;
		itemsAccepted = null;
		itemsRejected = null;
		itemsToBeCheck = null;
		setLoggedInWarehouse();
		getSocietyList();
		stockTransferList = new ArrayList<>();
		stockRejectDetailsList=new ArrayList<>();

		return RETAIL_QUALITY_CHECK_CREATE;
	}

	public String getEditPage() {

		retailQualityCheck = new RetailQualityCheck();
		stockRejectDetailsList=new ArrayList<>();
		addButtonFlag = false;
		setLoggedInWarehouse();
		getSocietyList();
		productCategory = new ProductCategory();

		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {

			return null;
		} else {
			if (retailQualityCheckSearch.getQualityCheckStatus().equals(RetailQualityCheckStatus.QC_COMPLETE)) {
				errorMap.notify(8517);
				return null;
			}
			govtSchemeQC = getRetailQualityCheck(retailQualityCheckSearch.getId());

			govtSchemeQCId = govtSchemeQC.getId();

			retailQualityCheck.setStockTransfer(govtSchemeQC.getStockTransfer());

			if (retailQualityCheck.getStockTransfer() != null
					&& retailQualityCheck.getStockTransfer().getSupplierMaster() != null) {
				supplierMaster = retailQualityCheck.getStockTransfer().getSupplierMaster();
				getStockTransfers();
				getProductCountDetails();
				getItemCountDetails();
			}
			return RETAIL_QUALITY_CHECK_CREATE;
		}

	}

	public String getViewPage() {

		getSocietyList();
		setLoggedInWarehouse();
		getStockTransfers();
		stockRejectDetailsList=new ArrayList<>();
		retailQualityCheck = new RetailQualityCheck();

		productCategory = new ProductCategory();

		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {

			return null;
		} else {
			govtSchemeQC = getRetailQualityCheck(retailQualityCheckSearch.getId());

			retailQualityCheck.setStockTransfer(govtSchemeQC.getStockTransfer());

			getProductCountDetails();
			getItemCountDetails();
			return RETAIL_QUALITY_CHECK_VIEW;
		}

	}

	public void processDelete() {
		log.info("RetailQualityCheckICBean.processDelete method started");
		addButtonFlag = false;
		if (retailQualityCheckSearch == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(24103);
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public void setLoggedInWarehouse() {
		if (loginBean.getUserProfile() != null && (EmployeeMaster) loginBean.getUserProfile() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
						.getWorkLocation() != null) {
			loggedInWarehouseId = ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
					.getWorkLocation().getId();
		}
	}

	public void getSocietyList() {
		supplierMasterList = commonDataService.findSupplierByProductWarehouse(loggedInWarehouseId,
				StockTransferStatus.SUBMITTED, StockTransferType.INSPECTION_INWARD);
	}

	public void getStockTransfers() {
		if (supplierMaster != null && supplierMaster.getId() != null)
			stockTransferList = commonDataService.findStockBySupplierAndQCNotCreated(supplierMaster.getId(),
					StockTransferStatus.SUBMITTED, StockTransferType.INSPECTION_INWARD);
	}

	public List<PurchaseInvoice> invoiceNumberBystockTransfer() {
		if(retailQualityCheck!=null && retailQualityCheck.getStockTransfer()!=null && retailQualityCheck.getStockTransfer().getId()!=null)
		{
			try {
			Long stockTransferId=retailQualityCheck.getStockTransfer().getId();
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/stock/transfer/getInvoiceNumber/" +stockTransferId ;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			purchaseInvoiceList = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseInvoice>>() {
			});
			if(purchaseInvoiceList!=null) {
				log.info("<<=== purchaseInvoiceList..."+purchaseInvoiceList.size());
				StringBuilder str=new StringBuilder();
				for(PurchaseInvoice list:purchaseInvoiceList) {
					String no=list.getInvoiceNumberPrefix();
					if(str!=null && purchaseInvoiceList.size()>1)
						str.append(",");
					
					str.append(no);
				}
				log.info("<<=== purchaseInvoce no..."+str);
				purchaseInvoice=str.toString();
			}
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	  }
		return purchaseInvoiceList;
	}
	
	public List<GovtSchemeQCItems> qcItemDetails(Long stockTransferId) {
		GovtSchemeQCDTO qcDetails = new GovtSchemeQCDTO();
		List<GovtSchemeQCItems> itemList = null;
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/inspection/get/qcitemdetailsList/" + stockTransferId;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			govtSchemeQCDTO = mapper.readValue(jsonResponse, new TypeReference<GovtSchemeQCDTO>() {
			});
			itemList=govtSchemeQCDTO.getGovtSchemeQCItemsList();
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return itemList;
	}

	public String getStockTransferData() {
		log.info("getStockTransferData started " + retailQualityCheck);
		/*
		 * if (retailQualityCheck == null || retailQualityCheck.getStockTransfer() ==
		 * null || retailQualityCheck.getStockTransfer().getId() == null) {
		 * errorMap.notify(24103); return null; }
		 */
		StockTransfer stockTransfer = getStockTransferById(retailQualityCheck.getStockTransfer().getId());
		if (stockTransfer != null) {
			List<GovtSchemeQCItems> productWiseList = qcItemDetails(stockTransfer.getId());
			govtSchemeQC.setGovtSchemeQCItemsList(productWiseList);
			
			getProductCountDetails();

			getItemCountDetails();

			return null;
		}
		return null;
	}
	
	public StockTransfer getStockTransferById(Long id) {
		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/stock/transfer/getTransferById/" + id;

		StockTransfer stockTransfer = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransfer = mapper.readValue(jsonValue, StockTransfer.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return stockTransfer;
	}

	public String delete() {
		log.info("RetailQualityCheckICBean.delete method started");
		if (retailQualityCheckSearch == null || retailQualityCheckSearch.getId() == null) {
			errorMap.notify(24103);
			return null;
		}
		if (retailQualityCheckSearch.getQualityCheckStatus().equals(RetailQualityCheckStatus.QC_PENDING)
				|| retailQualityCheckSearch.getQualityCheckStatus().equals(RetailQualityCheckStatus.QC_IN_PROGRESS)) {
			String url = serverURL + appPreference.getOperationApiUrl() + "/quality/check/inspection/delete/"
					+ retailQualityCheckSearch.getId();
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.delete(url);
			} catch (Exception exp) {
				log.error("Error in delete draft plan", exp);
				errorMap.notify(1);
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(24101);
				return getProductWarehouseQualityCheckList();
			} else {
				errorMap.notify(baseDTO.getStatusCode());
				return getProductWarehouseQualityCheckList();
			}
		} else {
			AppUtil.addError(errorMap.getMessage(24111));
			return null;
		}

	}

	public void selectProductWiseData(GovtSchemeQCItems qualityCheckProductWise, int idx) {
		index = idx;
		StockRejectDetails stockTransferDetails=new StockRejectDetails();
		
		StockTransferItems stockItems=new StockTransferItems();
		stockRejectDetailsList=new ArrayList<>();
		String stage=new String();
		loadRejectMaster();
		try {
			stockTransferDetails.setStockTransferItems(stockItems);
			StockTransferItems stockItemss=govtSchemeQCDTO.getStockTransferItemsList().get(index);
			stage=govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQualityCheckStatus().name();
			log.info("<<=== Selected stockTransfer Items...."+stockItemss);
			stockTransferDetails.getStockTransferItems().setId(stockItemss.getId());
			stockTransferDetails.setDescription(new String());
			stockTransferDetails.setRejectReasonMaster(new RejectReasonMaster());
		}catch(Exception e) {	
			log.info("<<<=== Exception while set StockReject..");
		}
		
		try {govtSchemeQCDTO.getStockTransferItemsList().get(index);
			Long stockTransferId=govtSchemeQCDTO.getStockTransferItemsList()!=null && govtSchemeQCDTO.getStockTransferItemsList().get(index)!=null
									&& govtSchemeQCDTO.getStockTransferItemsList().get(index).getId()!=null?govtSchemeQCDTO.getStockTransferItemsList().get(index).getId():null;
			List<StockRejectDetails> stockRejctList=new ArrayList<>();
			
			if(stockTransferId!=null)
				stockRejctList=loadStockTransferRejctListByItems(stockTransferId);
			
			if(stockRejctList!=null && stockRejctList.size()>0 && stage.equalsIgnoreCase("QC_COMPLETE")) {
				stockRejectDetailsList=stockRejctList;
				loadRejectMaster();
			}else if(!stage.equalsIgnoreCase("QC_COMPLETE")){
				stockRejectDetailsList.add(stockTransferDetails);
			}
		}catch(Exception e) {
			log.error("<<=== Exception while get check status...",e);
		}
		
		log.info("RetailQualityCheckICBean.selectProductWiseData method started");
	}
	
	public void loadRejectMaster() {
		log.info("<<<==== Start loadRejectMaster...");
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/get/getAllrejectReason";
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			rejectReasonMasterList = mapper.readValue(jsonResponse, new TypeReference<List<RejectReasonMaster>>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("<<<==== End loadRejectMaster...");
	}
	
	public List<StockRejectDetails> loadStockTransferRejctList(Long stockTransferId) {
		log.info("<<<==== Start loadStockTransferRejctList...");
		List<StockRejectDetails> stockRejctList=new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/inspection/get/getAllStockReason/"+stockTransferId;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			stockRejctList = mapper.readValue(jsonResponse, new TypeReference<List<StockRejectDetails>>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("<<<==== End loadStockTransferRejctList...");
		return stockRejctList;
	}
	
	public List<StockRejectDetails> loadStockTransferRejctListByItems(Long stockTransferItemsId) {
		log.info("<<<==== Start loadStockTransferRejctList...");
		List<StockRejectDetails> stockRejctList=new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/quality/check/inspection/get/getAllStockByItems/"+stockTransferItemsId;
			BaseDTO baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			stockRejctList = mapper.readValue(jsonResponse, new TypeReference<List<StockRejectDetails>>() {
			});
		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("<<<==== End loadStockTransferRejctList...");
		return stockRejctList;
	}

	public void getProductCountDetails() {
		try {
		if (govtSchemeQC.getGovtSchemeQCItemsList() == null) {
			totalProducts = 0;
			qcCompleted = 0;
			qcInprogress = 0;
			qcYetToStart = 0;
			return;
		}
		totalProducts = govtSchemeQC.getGovtSchemeQCItemsList().size();
		qcCompleted = calculateProductCounts(govtSchemeQC.getGovtSchemeQCItemsList(),
				RetailQualityCheckStatus.QC_COMPLETE);
		qcInprogress = calculateProductCounts(govtSchemeQC.getGovtSchemeQCItemsList(),
				RetailQualityCheckStatus.QC_IN_PROGRESS);
		qcYetToStart = calculateProductCounts(govtSchemeQC.getGovtSchemeQCItemsList(),
				RetailQualityCheckStatus.QC_PENDING);
		}catch(Exception e) {
			log.error("<<<==== Exception getProductCountDetails...."+e);
		}
	}

	private int calculateProductCounts(List<GovtSchemeQCItems> items, RetailQualityCheckStatus productStatus) {
		int count = 0;
		for (GovtSchemeQCItems item : items) {
			if (item.getQualityCheckStatus().equals(productStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	public void getItemCountDetails() {
		totalItems = 0;
		// if(govtSchemeQC.getGovtSchemeQCItemsList() == null) {
		itemsAccepted = 0;
		itemsRejected = 0;
		itemsToBeCheck = 0;
		// return;
		// }
		try {
		for (GovtSchemeQCItems item : govtSchemeQC.getGovtSchemeQCItemsList()) {
			for (GovtSchemeQCItems detail : item.getQcItemList()) {
				if (detail.getReceivedQuantity() != null) {
					totalItems = totalItems + detail.getReceivedQuantity().intValue();
				}
			}
		}

		for (GovtSchemeQCItems item : govtSchemeQC.getGovtSchemeQCItemsList()) {
			for (GovtSchemeQCItems detail : item.getQcItemList()) {
				if (detail.getAcceptedQuantity() != null) {
					itemsAccepted = itemsAccepted + detail.getAcceptedQuantity().intValue();
				}
			}
		}

		for (GovtSchemeQCItems item : govtSchemeQC.getGovtSchemeQCItemsList()) {
			for (GovtSchemeQCItems detail : item.getQcItemList()) {
				if (detail.getAcceptedQuantity() != null) {
					itemsRejected = itemsRejected + detail.getRejectedQuantity().intValue();
				}
			}
		}

		itemsToBeCheck = totalItems - (itemsAccepted + itemsRejected);
		}catch(Exception e) {
			log.error("<<==== Exception while getItemCountDetails...",e);
		}
		/*
		 * itemsAccepted = calculateItemCounts(govtSchemeQC.getGovtSchemeQCItemsList(),
		 * RetailQualityCheckStatus.QC_COMPLETE); itemsRejected =
		 * calculateItemCounts(govtSchemeQC.getGovtSchemeQCItemsList(),
		 * RetailQualityCheckStatus.QC_PENDING);
		 */

	}

	private int calculateItemCounts(List<GovtSchemeQCItems> items, RetailQualityCheckStatus itemStatus) {
		int count = 0;
		try {
		for (GovtSchemeQCItems item : items) {
			for (GovtSchemeQCItems detail : item.getQcItemList()) {
				if (detail.getQualityCheckStatus().equals(itemStatus)) {
					count = count + 1;
				}
			}
		}
		}catch(Exception e) {
			log.error("<<==== Exception while calculateItemCounts...",e);
		}
		return count;
	}

	private int calculateItemCountsForProduct(QualityCheckProductWise item, ItemStatus itemStatus) {
		int count = 0;
		try {
		for (RetailQualityCheckDetails detail : item.getRetailQualityCheckDetailList()) {
			if (detail.getItemStatus().equals(itemStatus)) {
				count = count + 1;
			}
		}
		}catch(Exception e) {
			log.error("<<==== Exception while calculateItemCountsForProduct...",e);
		}
		return count;
	}
	
	public void addStockRejctReason() {
		log.info("<<==== Start AddStockRejectReason....");
		StockRejectDetails stockTransferDetails=new StockRejectDetails();
		StockTransferItems stockItems=new StockTransferItems();
		try {
			stockTransferDetails.setStockTransferItems(stockItems);
			StockTransferItems stockItemss=govtSchemeQCDTO.getStockTransferItemsList().get(index);
			log.info("<<=== Selected stockTransfer Items...."+stockItemss);
			stockTransferDetails.getStockTransferItems().setId(stockItemss.getId());
			stockTransferDetails.setDescription(new String());
			stockTransferDetails.setRejectReasonMaster(new RejectReasonMaster());
		}catch(Exception e) {
			log.info("<<<=== Exception while set StockReject..");
		}
		stockRejectDetailsList.add(stockTransferDetails);
	}
	
	public String checkStockRejct(int idx, Double Value) {
		Double holeCount=0D;
		Double rejectQnty=0D;
		try {
			rejectQnty=govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).getRejectedQuantity();
			if(stockRejectDetailsList!=null) {
				holeCount = stockRejectDetailsList.stream()
						 .filter(stockRejectDetails ->stockRejectDetails!=null && 
								 stockRejectDetails.getRejectQuantity()!=null).mapToDouble(StockRejectDetails::getRejectQuantity).sum();
				log.info("<<== StockRejectCount..."+holeCount);
				if(rejectQnty<holeCount) {
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.REJECT_QUANTITY_IS_EXCEED).getErrorCode());
					return null;
				}
			}
		}catch(Exception e) {
			log.error("<<== Validation check for stockRejct...",e);
		}
		return null;
	}

	public String saveQualityCheckItems(String requestType) {
		log.info("Retail Quality Check save method started");
		addButtonFlag = false;
		if (requestType.equals(RetailQualityCheckStatus.QC_IN_PROGRESS.name())) {
			govtSchemeQC.getGovtSchemeQCItemsList().get(index)
					.setQualityCheckStatus(RetailQualityCheckStatus.QC_IN_PROGRESS);

			for (int ix = 0; ix < govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().size(); ix++) {
				// if(govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix).getAcceptedQuantity()
				// == null ) {
				govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix)
						.setQualityCheckStatus(RetailQualityCheckStatus.QC_IN_PROGRESS);
				// }
				
				Double rejectQnty=govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix).getRejectedQuantity();
				if(stockRejectDetailsList!=null) {
					Double holeCount = stockRejectDetailsList.stream()
							 .filter(stockRejectDetails ->stockRejectDetails!=null && 
									 stockRejectDetails.getRejectQuantity()!=null).mapToDouble(StockRejectDetails::getRejectQuantity).sum();
					log.info("<<== StockRejectCount..."+holeCount);
					if(rejectQnty<holeCount) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.REJECT_QUANTITY_IS_EXCEED).getErrorCode());
						return null;
					}
					
					for(StockRejectDetails list:stockRejectDetailsList) {
						if(list.getDescription()==null && list.getDescription().trim().equalsIgnoreCase("")) {
							errorMap.notify(ErrorDescription.ALL_RECORDS_NOT_VERIFIED.getCode());
							return null;
						}
					}
				}
			}
		} else {

			for (int ix = 0; ix < govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().size(); ix++) {
				if (govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix)
						.getAcceptedQuantity() == null) {
					errorMap.notify(ErrorDescription.ALL_RECORDS_NOT_VERIFIED.getCode());
					return null;
				}
			}

			for (int ix = 0; ix < govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().size(); ix++) {
				// if(govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix).getAcceptedQuantity()
				// == null ) {
				govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix)
						.setQualityCheckStatus(RetailQualityCheckStatus.QC_COMPLETE);
				// }
				
				Double rejectQnty=govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(ix).getRejectedQuantity();
				if(stockRejectDetailsList!=null) {
					Double holeCount = stockRejectDetailsList.stream()
							 .filter(stockRejectDetails ->stockRejectDetails!=null && 
									 stockRejectDetails.getRejectQuantity()!=null).mapToDouble(StockRejectDetails::getRejectQuantity).sum();
					log.info("<<== StockRejectCount..."+holeCount);
					if(rejectQnty<holeCount) {
						errorMap.notify(ErrorDescription.getError(OperationErrorCode.REJECT_QUANTITY_IS_EXCEED).getErrorCode());
						return null;
					}
				}
			}

			govtSchemeQC.getGovtSchemeQCItemsList().get(index)
					.setQualityCheckStatus(RetailQualityCheckStatus.QC_COMPLETE);
		}
		
		

		govtSchemeQC.setStockTransfer(retailQualityCheck.getStockTransfer());

		String url = "";
		if (govtSchemeQCId == null) {
			url = serverURL + appPreference.getOperationApiUrl() + "/quality/check/inspection/create";
		} else {
			url = serverURL + appPreference.getOperationApiUrl() + "/quality/check/inspection/update";
			govtSchemeQC.setId(govtSchemeQCId);
		}
		BaseDTO baseDTO = null;

		GovtSchemeQC govtSchemeQCCreatebj = null;

		/*
		 * for(GovtSchemeQCItems item :
		 * govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList()) {
		 * if(item.getQualityCheckStatus().equals(RetailQualityCheckStatus.QC_PENDING))
		 * { govtSchemeQC.getGovtSchemeQCItemsList().get(index).setQualityCheckStatus(
		 * RetailQualityCheckStatus.QC_PENDING); break; } else {
		 * govtSchemeQC.getGovtSchemeQCItemsList().get(index).setQualityCheckStatus(
		 * RetailQualityCheckStatus.QC_COMPLETE); } }
		 */

		try {
			govtSchemeQCDTO.setGovtSchemeQC(govtSchemeQC);
			govtSchemeQCDTO.setStockRejectDetailsList(stockRejectDetailsList);
			
			baseDTO = httpService.post(url, govtSchemeQCDTO);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					try {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						govtSchemeQCCreatebj = mapper.readValue(jsonValue, GovtSchemeQC.class);
						govtSchemeQCId = govtSchemeQCCreatebj.getId();

						getProductCountDetails();
						getItemCountDetails();

					} catch (Exception e) {
						log.error("Error in Retail Quality Check save", e);
						errorMap.notify(1);
						return RETAIL_QUALITY_CHECK_CREATE;
					}
					log.info("Retail Quality Check saved successfully");
					errorMap.notify(24104);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in Retail Quality Check save", exp);
			errorMap.notify(1);
			return RETAIL_QUALITY_CHECK_CREATE;
		}
		/*
		 * if(retailQualityCheck.getStockTransfer()!=null &&
		 * retailQualityCheck.getStockTransfer().getSupplierMaster()!=null) {
		 * supplierMaster = retailQualityCheck.getStockTransfer().getSupplierMaster();
		 * getStockTransfers(); getProductCountDetails(); getItemCountDetails(); }
		 */
		return RETAIL_QUALITY_CHECK_CREATE;

	}

	public GovtSchemeQC getRetailQualityCheck(Long id) {
		String URL = serverURL + appPreference.getOperationApiUrl() + "/quality/check/inspection/getbyid/" + id;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				govtSchemeQCDTO = mapper.readValue(jsonValue, GovtSchemeQCDTO.class);
			}
			
			govtSchemeQC=govtSchemeQCDTO.getGovtSchemeQC();
			stockRejectDetailsList=govtSchemeQCDTO.getStockRejectDetailsList();
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return govtSchemeQC;
	}
	
	

	public void updateLength(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setProductLength(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateAcceptedQuantity(int idx, Double Value) {
		if (govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx)
				.getReceivedQuantity() < govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx)
						.getAcceptedQuantity()) {
			errorMap.notify(ErrorDescription.ACCEPTED_NOT_MORE_THAN_RECEIVED.getCode());
			govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).setAcceptedQuantity(null);
		} else {
			govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).setAcceptedQuantity(Value);
			govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).setRejectedQuantity(
					govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).getReceivedQuantity()
							- govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx)
									.getAcceptedQuantity());
			govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx)
					.setQualityCheckStatus(RetailQualityCheckStatus.QC_COMPLETE);
		}
	}

	public void updateRejectedQuantity(int idx, Double Value) {
		govtSchemeQC.getGovtSchemeQCItemsList().get(index).getQcItemList().get(idx).setRejectedQuantity(Value);
	}

	public void updateEndsPerInch(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setEndsPerInch(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updatePicksPerInch(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setPicksPerInch(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateProductWeight(int idx, Double Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setProductWeight(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void updateStitchingStatus(int idx, boolean Value) {
		retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.setStitchingStatus(Value);
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public void validateData(int idx) {
		if (!validateATNumberWiseDetails(idx)) {
			Util.addError(errorMsg);
			return;
		}
	}

	public boolean validateATNumberWiseDetails(int idx) {

		log.info("validateATNumberWiseDetails called ");
		RetailQualityCheckDetails qualityCheckDetails = retailQualityCheck.getQualityCheckProductWiseList().get(index)
				.getRetailQualityCheckDetailList().get(idx);
		if (!qualityCheckDetails.isVerified()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.PENDING);
		}
		ProductVarietyMaster productVarietyMaster = retailQualityCheck.getQualityCheckProductWiseList().get(index)
				.getProductVarietyMaster();

		Double lowestLengthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getLengthActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getLengthTolerance());
		Double lowestWidthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getWidthActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getWidthTolerance());
		Double lowestPicksPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchTolerance());
		Double lowestEndsPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getEndspPerInchActual())
				- AppUtil.ifNullRetunZero(productVarietyMaster.getEndsPerInchTolerance());

		Double highestLengthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getLengthActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getLengthTolerance());
		Double highestWidthTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getWidthActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getWidthTolerance());
		Double highestPicksPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getPicksPerInchTolerance());
		Double highestEndsPerInchTolerance = AppUtil.ifNullRetunZero(productVarietyMaster.getEndspPerInchActual())
				+ AppUtil.ifNullRetunZero(productVarietyMaster.getEndsPerInchTolerance());

		lowestLengthTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestLengthTolerance);
		lowestWidthTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestWidthTolerance);
		lowestPicksPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestPicksPerInchTolerance);
		lowestEndsPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(lowestEndsPerInchTolerance);

		highestLengthTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestLengthTolerance);
		highestWidthTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestWidthTolerance);
		highestPicksPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestPicksPerInchTolerance);
		highestEndsPerInchTolerance = AppUtil.ifNegativeOrZeroRetunOne(highestEndsPerInchTolerance);

		if (qualityCheckDetails.getProductLength() != null
				&& (qualityCheckDetails.getProductLength() < lowestLengthTolerance
						|| qualityCheckDetails.getProductLength() > highestLengthTolerance)) {
			log.info("Length range should be : (" + lowestLengthTolerance + '-' + highestLengthTolerance + ')');
			errorMsg = "Length " + errorMap.getMessage(24109) + '(' + lowestLengthTolerance + '-'
					+ highestLengthTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			return false;
		}
		if (qualityCheckDetails.getProductWidth() != null
				&& (qualityCheckDetails.getProductWidth() < lowestWidthTolerance
						|| qualityCheckDetails.getProductWidth() > highestWidthTolerance)) {
			log.info("Width range should be : (" + lowestWidthTolerance + '-' + highestWidthTolerance + ')');
			errorMsg = "Width " + errorMap.getMessage(24109) + '(' + lowestWidthTolerance + '-' + highestWidthTolerance
					+ ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			return false;
		}

		if (qualityCheckDetails.getEndsPerInch() != null
				&& (qualityCheckDetails.getEndsPerInch() < lowestEndsPerInchTolerance
						|| qualityCheckDetails.getEndsPerInch() > highestEndsPerInchTolerance)) {
			log.info("Ends Per Inch range should be : (" + lowestEndsPerInchTolerance + '-'
					+ highestEndsPerInchTolerance + ')');
			errorMsg = "Ends Per Inch " + errorMap.getMessage(24109) + '(' + lowestEndsPerInchTolerance + '-'
					+ highestEndsPerInchTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			return false;
		}
		if (qualityCheckDetails.getPicksPerInch() != null
				&& (qualityCheckDetails.getPicksPerInch() < lowestPicksPerInchTolerance
						|| qualityCheckDetails.getPicksPerInch() > highestPicksPerInchTolerance)) {
			log.info("Picks Per Inch range should be : (" + lowestPicksPerInchTolerance + '-'
					+ highestPicksPerInchTolerance + ')');
			errorMsg = "Picks Per Inch " + errorMap.getMessage(24109) + '(' + lowestPicksPerInchTolerance + '-'
					+ highestPicksPerInchTolerance + ')';
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
			return false;
		}

		errorMsg = null;
		if (retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
				.isVerified()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.ACCEPTED);
		} else {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.PENDING);
		}
		if (!qualityCheckDetails.isStitchingStatus()) {
			retailQualityCheck.getQualityCheckProductWiseList().get(index).getRetailQualityCheckDetailList().get(idx)
					.setItemStatus(ItemStatus.REJECTED);
		}
		return true;

	}
	
	public void clearQualityCheckDetails() {
		log.info("======Start clearQualityCheckDetails()========");
		retailQualityCheck=new RetailQualityCheck();
		supplierMaster=new SupplierMaster();
		purchaseInvoice="";
		log.info("======End clearQualityCheckDetails()========");
	}

}