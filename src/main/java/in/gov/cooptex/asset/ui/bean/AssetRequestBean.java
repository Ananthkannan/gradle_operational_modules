package in.gov.cooptex.asset.ui.bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.asset.model.AssetAllocation;
import in.gov.cooptex.asset.model.AssetRequestLog;
import in.gov.cooptex.asset.model.AssetRequestNote;
import in.gov.cooptex.asset.model.dto.AssetRequestApproveDTO;
import in.gov.cooptex.asset.model.dto.AssetRequestListDTO;
import in.gov.cooptex.asset.model.dto.AssetRequestViewDTO;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.AssetRequestStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.hrms.dto.EmpIncomeTaxWorksheetDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("assetRequestBean")
@Scope("session")

public class AssetRequestBean extends CommonBean implements Serializable {

	final String LIST_PAGE = "/pages/assetManagement/listAssetRequest.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/assetManagement/createAssetRequest.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/assetManagement/viewAssetRequest.xhtml?faces-redirect=true";
	final String ALLOC_ADD_PAGE = "/pages/assetManagement/requestBaseAllocation.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;
	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Getter
	@Setter
	LazyDataModel<AssetRequestListDTO> lazyAssetRequestList;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean approveeditFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean forwdforflag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Getter
	@Setter
	Boolean baseAllocation = false;

	@Getter
	@Setter
	Boolean baseAllocationflag = false;

	@Getter
	@Setter
	Boolean baseAllocationViewPage = false;

	@Getter
	@Setter
	List<AssetRequestListDTO> assetList = null;

	@Getter
	@Setter
	AssetRequestListDTO selectedasset;

	@Getter
	@Setter
	Long assetrequestId;

	@Getter
	@Setter
	List<AssetRequestListDTO> assetRequestListDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<AssetRequestListDTO> removeassetRequestListDTOList = new ArrayList<>();

	@Getter
	@Setter
	String apprremarks;

	@Getter
	@Setter
	String rejtremark;

	@Getter
	@Setter
	Long forwardTo;

	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	AssetRequestApproveDTO assetRequestApproveDTO = new AssetRequestApproveDTO();

	@Getter
	@Setter
	AssetRequestLog assetRequestLog;

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	AssetRequestViewDTO viewassetdetails;

	@Getter
	@Setter
	AssetRequestListDTO assetRequestListDTO = new AssetRequestListDTO();

	@Getter
	@Setter
	List<AssetRequestListDTO> assetMasterList = new ArrayList<>();

	@Getter
	@Setter
	String pageAction, assetsInfo;

	@Getter
	@Setter
	Long quantity;

	@Getter
	@Setter
	String reason;

	@Getter
	@Setter
	AssetRequestListDTO asset = new AssetRequestListDTO();

	@Getter
	@Setter
	List<AssetRequestListDTO> assetsMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<AssetRequestListDTO> assetlist = new ArrayList<>();

	@Getter
	@Setter
	List<AssetRequestNote> createresponse = null;

	@Getter
	@Setter
	Long previousApprover = null;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	@Getter
	@Setter
	UserMaster forwadtousermaster;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	Double usedquantity;

	@Getter
	@Setter
	Boolean reqbaseAllocation = false;

	@Getter
	@Setter
	Long reqvalue = null;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@PostConstruct
	public String showViewListPage() {
		log.info("CircularBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("asset_request_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// init();
			selectedasset = new AssetRequestListDTO();
			action = "VIEW";
			// assetAllocation.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedasset.getId());
			selectedasset.setId(Long.parseLong(circularId));
			log.info("valueeee" + selectedasset.getId());
			selectedNotificationId = Long.parseLong(notificationId);

			viewDetails();
		}
		log.info("Asset Request showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public String showAssetList() {
//	userMasters	 = commonDataService.loadForwardToUsers();
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
		log.info("forward to user info" + userMasters);
		lazyLoadAssetslist();
		baseAllocationflag = false;
		editButtonFlag = true;
		viewButtonFlag = true;
		addButtonFlag = true;
		forwdforflag = false;
		setSelectedasset(null);
		return LIST_PAGE;
	}

	public String showAssetPageAction() {
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create Page  called..");
				assetRequestListDTOList = new ArrayList<>();
				assetRequestListDTO = new AssetRequestListDTO();
				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();
				quantity = null;
				reason = null;
				setAssetsInfo(null);
				setForwardFor(null);
				setForwardTo(null);
				forwadtousermaster = null;
				return ADD_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return null;
	}

	public void lazyLoadAssetslist() {
		log.info("<<<< ----------Starts lazyLoadAssetList------- >>>>");
		lazyAssetRequestList = new LazyDataModel<AssetRequestListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<AssetRequestListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/assetrequest/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						assetList = mapper.readValue(jsonResponse, new TypeReference<List<AssetRequestListDTO>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						assetList = new ArrayList<>();
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in meetingList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return assetList;
			}

			@Override
			public Object getRowKey(AssetRequestListDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AssetRequestListDTO getRowData(String rowKey) {
				try {
					for (AssetRequestListDTO assets : assetList) {
						if (assets.getId().equals(Long.valueOf(rowKey))) {
							selectedasset = assets;
							return assets;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<<<< ----------Ends lazyLoadAsset List------- >>>>");
	}

	public String createddate(Date d) {
		log.info("<<<< ----------Starts MeetingDate formate changing------ >>>>");
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		log.info("<<<< ----------Ends MeetingDate formate changing------ >>>>");
		return simpleDateFormat.format(d);
	}

	public String quantityCheck(AssetRequestListDTO item) {
		if (item.getAvailable_quantity() != null) {
			if (item.getApprove_quantity() != null || item.getApprove_quantity() > 0) {

				if (item.getAvailable_quantity() < item.getApprove_quantity()) {

					item.setApprove_quantity(0L);
					Util.addWarn("Please Enter Lesser Than Available Qty");
				}
				if (item.getAvailable_quantity() > item.getQuantity()
						&& item.getApprove_quantity() > item.getQuantity()) {

					item.setApprove_quantity(0L);
					Util.addWarn("Please Enter Lesser Than Request Qty");
				}
			}
		} else {
			item.setApprove_quantity(0L);
		}

		return "";
	}

	public String viewDetails() {
		log.info("<<<< ----------Starts viewDetails------ >>>>");

		log.info("selected view id", selectedasset);
		long assetid = selectedasset.getId();
		log.info("asset id" + assetid);
		try {

			viewassetdetails = new AssetRequestViewDTO();
			// if (action.equalsIgnoreCase("VIEW")) {

			log.info("view asset list called.." + reqbaseAllocation);

			if (reqbaseAllocation.equals(false)) {
				reqvalue = 0L;
			} else {
				reqvalue = 1L;
			}

			String url = SERVER_URL + "/assetrequest/getbyid/" + assetid + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0) + "/"
					+ (reqvalue != null ? reqvalue : 0);

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewassetdetails = mapper.readValue(jsonResponse, new TypeReference<AssetRequestViewDTO>() {
				});

				selectedasset.setAssetrequestnote(viewassetdetails.getAssetRequestNote());

				selectedasset.setAssetrequestlog(viewassetdetails.getAssetRequestLog());

				// List<AssetRequestViewDTO> assetRequestDtolist= mapper.readValue(jsonResponse,
				// new TypeReference<AssetRequestViewDTO>() {
				// });

				// if(!CollectionUtils.isEmpty(assetRequestDtolist)) {
				// AssetRequestViewDTO assetRequestViewDTO= assetRequestDtolist.get(0);
				// (assetRequestViewDTO != null) {
				// selectedasset.setAssetrequestnote(assetRequestViewDTO.getAssetRequestNote());

				// }
				// }

				String jsonResponse1 = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
				});

				assetRequestListDTOList = viewassetdetails.getAssetlist();
				if (viewassetdetails != null) {
					if (viewassetdetails.getAssetlist().size() > 0) {
						for (AssetRequestListDTO data : viewassetdetails.getAssetlist()) {
							if (data.getApprove_quantity() != null) {
								if (data.getQuantity() != null) {
									if (data.getQuantity() > 0 && data.getApprove_quantity() > 0) {
										baseAllocationViewPage = true;
									}
								}
							}
						}
					}
				}

			} else {

				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			if (selectedasset != null) {

				selectedasset.getAssetrequestlog().setRemarks("");

				// if(!CollectionUtils.isEmpty(assetRequestListDTOList)) {
				// AssetRequestViewDTO assetRequestViewDTO = assetRequestListDTOList.get(0);
				// }

				setPreviousApproval(selectedasset.getAssetrequestnote().getFinalApproval());
				if ("VIEW".equalsIgnoreCase(action)) {
					if (selectedasset.getAssetrequestnote().getForwardTo() != null) {
						if (loginBean.getUserMaster().getId()
								.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
							previousApprover = selectedasset.getAssetrequestnote().getForwardTo().getId();
							approveeditFlag = false;
							buttonFlag = false;

							if (AssetRequestStage.REJECTED.equals(selectedasset.getStatus())) {
								approvalFlag = false;
							} else {
								approvalFlag = true;
							}
						} else {
							approvalFlag = false;
						}
					}
					log.info("" + selectedasset.getAssetrequestlog().getStage());
					setFinalApproveFlag(selectedasset.getAssetrequestnote().getFinalApproval());
					if (AssetRequestStage.FINAL_APPROVED.equals(selectedasset.getStatus())) {
						buttonFlag = true;
						forwdforflag = true;

					}
					if (AssetRequestStage.FINAL_APPROVED.equals(selectedasset.getStatus())
							|| AssetRequestStage.APPROVED.equals(selectedasset.getStatus())) {

						baseAllocationflag = false;
					}
					if (AssetRequestStage.FINAL_APPROVED.equals(selectedasset.getStatus())) {
						baseAllocationflag = true;
						approvalFlag = false;
					}

					log.info("current statussss" + selectedasset.getStatus());
					if (AssetRequestStage.ALLOCATION_INITIATED.equals(selectedasset.getAssetrequestlog().getStage())) {
						baseAllocationflag = true;
						approvalFlag = true;
					}

					if (AssetRequestStage.ALLOCATION_INITIATED.equals(selectedasset.getAssetrequestlog().getStage())
							&& loginBean.getUserMaster().getId()
									.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						baseAllocationflag = true;
						approvalFlag = true;
					}
					if (AssetRequestStage.ALLOCATION_INITIATED.equals(selectedasset.getAssetrequestlog().getStage())
							&& !loginBean.getUserMaster().getId()
									.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						baseAllocationflag = true;
						approvalFlag = false;
					}

					if (AssetRequestStage.ALLOCATION_APPROVED.equals(selectedasset.getAssetrequestlog().getStage())
							&& loginBean.getUserMaster().getId()
									.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						baseAllocationflag = true;
						approvalFlag = true;
					}

					if (AssetRequestStage.ALLOCATION_APPROVED.equals(selectedasset.getAssetrequestlog().getStage())
							&& !loginBean.getUserMaster().getId()
									.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						baseAllocationflag = true;
						approvalFlag = false;
					}
					if (AssetRequestStage.ALLOCATION_FINAL_APPROVED.equals(selectedasset.getStatus())) {
						baseAllocationflag = true;
						approvalFlag = false;
					}
					if (AssetRequestStage.ALLOCATION_REJECTED.equals(selectedasset.getStatus())) {
						baseAllocationflag = true;
						approvalFlag = false;
					}

					if (loginBean.getUserMaster().getId()
							.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						setHideflag(true);
					}
					if (!loginBean.getUserMaster().getId()
							.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())) {
						setHideflag(false);
					}
					if (loginBean.getUserMaster().getId()
							.equals(selectedasset.getAssetrequestnote().getForwardTo().getId())
							&& selectedasset.getAssetrequestnote().getFinalApproval().equals(true)) {
						setHideflag(false);
					}

					if (AssetRequestStage.REJECTED.equals(selectedasset.getStatus())) {
						setHideflag(false);
					}
					if (AssetRequestStage.ALLOCATION_REJECTED.equals(selectedasset.getStatus())) {
						setHideflag(false);
						baseAllocationflag = true;
					}

					// selectedasset.getAssetrequestlog().setRemarks("");
					return VIEW_PAGE;
				}
			}
			// }
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends viewDetails------ >>>>");

		return null;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<<<< ----------Starts onRowSelect------ >>>>");
		selectedasset = ((AssetRequestListDTO) event.getObject());
		log.info("<<<< ----------assetrequestId ------ >>>>" + selectedasset.getId());
		assetrequestId = selectedasset.getId();
		addButtonFlag = false;
		viewButtonFlag = false;
		deleteButtonFlag = true;
		if (selectedasset.getStatus().equals(AssetRequestStage.REJECTED)) {
			log.info("<<<< ----------Status ------ >>>> REJECTED");
			editButtonFlag = true;
			approvalFlag = false;
		} else {
			editButtonFlag = true;
		}
		if (selectedasset.getStatus().equals(AssetRequestStage.APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.REQUEST_INITIATED)
				|| selectedasset.getStatus().equals(AssetRequestStage.REJECTED)
				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_FINAL_APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_REJECTED)) {
			log.info("<<<< ----------Status ------ >>>> APPROVED");
			baseAllocation = false;
		}

		if (selectedasset.getStatus().equals(AssetRequestStage.FINAL_APPROVED)) {
			log.info("<<<< ----------Status ------ >>>> FINAL APPROVED");
			baseAllocation = true;
		}

		if (selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_INITIATED)) {
			log.info("<<<< ----------Status ------ >>>> ALLOCATION_INITIATED");
			forwdforflag = true;
			baseAllocation = false;
		}

		if (selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_INITIATED)

				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_FINAL_APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.ALLOCATION_REJECTED)) {
			log.info("<<<< ----------Status ------ >>>> APPROVED");
			reqbaseAllocation = true;
		}
		if (selectedasset.getStatus().equals(AssetRequestStage.REQUEST_INITIATED)

				|| selectedasset.getStatus().equals(AssetRequestStage.APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.FINAL_APPROVED)
				|| selectedasset.getStatus().equals(AssetRequestStage.REJECTED)) {

			reqbaseAllocation = false;
		}
		log.info("<<<< ----------Ends onRowSelect------ >>>>");
	}

	public String submitApprovedStatus() {
		BaseDTO baseDTO = null;
		try {
			log.info(" Asset Request id----------" + selectedasset.getId());
			log.info("<<<< ----------Status ------ >>>> Approving");
			baseDTO = new BaseDTO();

			if (baseAllocationflag.equals(false)) {
				if (previousApproval == false && selectedasset.getAssetrequestnote().getFinalApproval() == true) {
					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.APPROVED);
					log.info("<<<< ----------Status ------ >>>> ALLOCATION_INITIATED");
				} else if (previousApproval == true && selectedasset.getAssetrequestnote().getFinalApproval() == true) {
					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.FINAL_APPROVED);
					log.info("<<<< ----------Status ------ >>>> FINAL_APPROVED");
				} else {

					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.APPROVED);
					log.info("<<<< ----------Status ------ >>>> APPROVED");
				}
			}
			if (baseAllocationflag.equals(true)) {
				if (previousApproval == false && selectedasset.getAssetrequestnote().getFinalApproval() == true) {
					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.ALLOCATION_APPROVED);
					log.info("<<<< ----------Status ------ >>>> ALLOCATION_INITIATED");
				} else if (previousApproval == true && selectedasset.getAssetrequestnote().getFinalApproval() == true) {
					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.ALLOCATION_FINAL_APPROVED);
					log.info("<<<< ----------Status ------ >>>> FINAL_APPROVED");
				} else {

					selectedasset.getAssetrequestlog().setStage(AssetRequestStage.ALLOCATION_APPROVED);
					log.info("<<<< ----------Status ------ >>>> APPROVED");
				}
			}
			log.info("approve remarks=========>" + selectedasset.getAssetrequestlog().getRemarks());

			if (selectedasset.getAssetrequestnote().getForwardTo().getId() == null) {
				selectedasset.getAssetrequestnote().getForwardTo().setId(previousApprover);
			}

			log.info("<<<< ----------HITTING SERVICE ------ >>>> ");
			String url = SERVER_URL + "/assetrequest/approveassets";

			baseDTO = httpService.post(url, selectedasset);
			if (baseDTO.getStatusCode() == 0) {
				if (baseAllocationflag.equals(false)) {
					errorMap.notify(ErrorDescription.ASSET_REQUEST_APPROVE.getErrorCode());
				} else if (baseAllocationflag.equals(true)) {
					errorMap.notify(ErrorDescription.ASSETALLOCATION_APPROVAL_APPROVED_SUCCESSFULLY.getErrorCode());
				}
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showAssetList();
			}
		} catch (Exception e) {
			log.error("submitApprovedStatus method inside exception-------", e);
		}
		return null;
	}

	public String submitRejectedStatus() {
		BaseDTO baseDTO = null;
		try {
			log.info("Asset Request id----------" + selectedasset.getId());
			baseDTO = new BaseDTO();
			log.info("<<<< ----------Setting Status ------ >>>> REJECTED");
			if (baseAllocationflag.equals(false)) {
				selectedasset.getAssetrequestlog().setStage(AssetRequestStage.REJECTED);
			} else if (baseAllocationflag.equals(true)) {
				selectedasset.getAssetrequestlog().setStage(AssetRequestStage.ALLOCATION_REJECTED);
			}
			String url = SERVER_URL + "/assetrequest/rejectassets";
			baseDTO = httpService.post(url, selectedasset);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ASSET_REQUEST_REJECT.getErrorCode());
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showAssetList();
			}
		} catch (Exception e) {
			log.error("rejecttaBill method inside exception-------", e);
		}
		return null;
	}

	public String deleteAssets() {
		log.info("<<<< ----------Starts deleteAssets----- >>>>");
		long assetid = selectedasset.getId();
		log.info("asset id", assetid);

		try {

			log.info("Delete Assets list called..");

			String url = SERVER_URL + "/assetrequest/delete/" + assetid;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				errorMap.notify(ErrorDescription.ASSET_REQUEST_DELETE_SUCCESS.getErrorCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends deleteAssets----- >>>>");
		return LIST_PAGE;

	}

	public List<String> assetAutocomplete(String query) {
		log.info("Asset Autocomplete query==>" + query);
		List<String> results = new ArrayList<String>();
		assetsMasterList = loadassetMasterList(query);
		if (assetsMasterList != null || !assetsMasterList.isEmpty()) {
			for (AssetRequestListDTO assetslst : assetsMasterList) {
				String supp = assetslst.getAsset();
				results.add(supp);
			}
		}
		return results;
	}

	public List<AssetRequestListDTO> loadassetMasterList(String query) {

		// List<AssetRequestListDTO> assetsMasterList;

		try {
			BaseDTO baseDTO = new BaseDTO();
			assetsMasterList = new ArrayList<AssetRequestListDTO>();
			log.info(".......TenderApplyBean loadAssetRequestListDTONoTender..............");
			String url = SERVER_URL + "/assetrequest/getassetslist/" + query;

			baseDTO = httpService.get(url);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				assetsMasterList = mapper.readValue(jsonResponse, new TypeReference<List<AssetRequestListDTO>>() {
				});
				log.info("assetsMasterList size==> " + assetsMasterList.size());
			} else {
				assetsMasterList = new ArrayList<>();
			}
			return assetsMasterList;
		} catch (Exception e) {
			log.error("Exception in loadDistTalukInfo  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void assetslistchange() {
		log.info("<===========ASSET LIST ==========>", assetsInfo);
	}

	public void autoCompleteNoResultFount() {
		log.info("selected assets", assetsInfo);

		for (AssetRequestListDTO intrdad : assetsMasterList) {
			if (intrdad.getAsset().equalsIgnoreCase(assetsInfo)) {
				selectedasset = intrdad;
				log.info("<============slected assets==========>" + selectedasset);
			} else {

			}

		}
	}

	public void addassetlist() {
		log.info("<===========requested ==========>" + reason);
		AssetRequestListDTO creatAssetList = new AssetRequestListDTO();
		creatAssetList.setId(selectedasset.getId());
		creatAssetList.setAsset(selectedasset.getAsset());
		creatAssetList.setAssetCategory(selectedasset.getAssetCategory());
		creatAssetList.setAssetSubCategory(selectedasset.getAssetSubCategory());
		creatAssetList.setQuantity(quantity);
		// Long usedqty =
		// assetRequestItemsRepository.getsumofitems(creatAssetList.getId());

		try {
			String url = SERVER_URL + "/assetrequest/getusedqtyofasset/" + creatAssetList.getId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("valueeee" + baseDTO.getResponseContent());

					usedquantity = mapper.readValue(jsonValue, new TypeReference<Double>() {
					});

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in getusedqtyofasset() --->", exp);
		}

		log.info("appp" + usedquantity);
		creatAssetList.setAvailable_quantity(usedquantity);
		creatAssetList.setAssetCategoryId(selectedasset.getAssetCategoryId());
		creatAssetList.setAssetSubCategoryId(selectedasset.getAssetSubCategoryId());
		creatAssetList.setReason(reason);
		log.info("idddd" + creatAssetList.getId());
		log.info("typingvalueeee" + selectedasset.getId());
//		if (assetRequestListDTOList.stream()
//				.anyMatch(e -> e.getAssetId()==(selectedasset != null ? selectedasset.getId() : 0))) {
//
//			AppUtil.addError("Asset Items Already Exist");
//			return;
//		}
//		else {

		boolean existFlag = assetRequestListDTOList.stream()
				.anyMatch(b -> b.getId().equals(creatAssetList.getId()))
				&& assetRequestListDTOList.stream()
						.anyMatch(b -> b.getAssetCategory().equals(creatAssetList.getAssetCategory()))
				&& assetRequestListDTOList.stream()
						.anyMatch(b -> b.getAssetSubCategory().equals(creatAssetList.getAssetSubCategory()));
		if (existFlag) {
			AppUtil.addError("This Asset Is Already Added");
			RequestContext.getCurrentInstance().update("growls");
			clear();
			return;
		}
		assetRequestListDTOList.add(creatAssetList);
		// }
		clear();
		log.info("<==============requested data ==============>" + assetRequestListDTOList);
	}

	public void clear() {
		log.info("<===========AssetRequestBean.clear() ==========>");
		selectedasset = new AssetRequestListDTO();
		setQuantity(null);
		setAssetsInfo(null);
		setReason(null);
		setForwardFor(null);
		setForwardTo(null);
	}

	public void deleteAssetList(AssetRequestListDTO creatAssetList) {

		try {
			if (assetRequestListDTOList.contains(creatAssetList)) {
				removeassetRequestListDTOList.add(creatAssetList);
				assetRequestListDTOList.remove(creatAssetList);
			}
		} catch (Exception exp) {
			log.info("Error In deleteAssetList Metod", exp);
		}

	}

	public String createAssetRequest() {

		log.info("<<<< ----------Start creatMereting ------- >>>>");
		log.info("meeting edit fonction is clled");
		log.info("create meeting id called", assetRequestListDTOList);
		if (assetRequestListDTOList.size() == 0) {
			errorMap.notify(ErrorDescription.ENTER_ATLEAST_ONE_DETAIL.getErrorCode());
			log.info("Inside Empty List");
			return null;
		} else {
			if (assetRequestListDTO.getNote() == "") {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED.getErrorCode());
				return null;
			}
			assetRequestListDTO.setRemoveAssetRequest(removeassetRequestListDTOList);
			assetRequestListDTO.setAddAssetRequest(assetRequestListDTOList);
			setForwardTo(forwadtousermaster.getId());
			assetRequestListDTO.setForwardTo(forwardTo);
			assetRequestListDTO.setForwardFor(forwardFor);
			log.info("create request list" + assetRequestListDTO);
			if (action.equals("ADD")) {
				try {
					BaseDTO baseDTO = new BaseDTO();

					String url = SERVER_URL + "/assetrequest/ceate";

					baseDTO = httpService.post(url, assetRequestListDTO);

					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {
							errorMap.notify(ErrorDescription.ASSET_REQUEST_SAVE_SUCCESS.getErrorCode());
						}
					}
					log.info("create response", createresponse);
					return showAssetList();
				} catch (Exception e) {
					log.error("found exception in onchangeEntityType: ", e);
				}
			} else {
				if (action.equals("EDIT")) {
					try {

						BaseDTO baseDTO = new BaseDTO();

						String url = SERVER_URL + "/assetrequest/update";

						baseDTO = httpService.post(url, assetRequestListDTO);

						if (baseDTO != null) {
							if (baseDTO.getStatusCode() == 0) {
								errorMap.notify(ErrorDescription.ASSET_REQUEST_UPDATE_SUCCESS.getErrorCode());
							}

						}
						log.info("create response", createresponse);
						return showAssetList();
						// assetRequestListDTO = new AssetRequestListDTO();

					} catch (Exception e) {
						log.error("found exception in onchangeEntityType: ", e);
					}
				}
			}
		}
		log.info("<<<< ----------End createMeetingType ------- >>>>");
		return LIST_PAGE;
	}

	public void createParams() {
		log.info("<<<< ----------Starts createNote------- >>>>");
		log.info("Create Note", assetRequestListDTO.getNote());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
		log.info("<<<< ----------Ends createNote------- >>>>");
	}

	public void cancelRequest() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
	}

	public String editAssetDetails() {

		log.info("<<<< ----------Starts viewDetails------ >>>>");

		log.info("selected view id", selectedasset);
		long assetid = selectedasset.getId();
		log.info("asset id", assetid);
		try {

			viewassetdetails = new AssetRequestViewDTO();
			if (action.equalsIgnoreCase("EDIT")) {

				log.info("view asset list called..");

				if (reqbaseAllocation.equals(false)) {
					reqvalue = 0L;
				} else {
					reqvalue = 1L;
				}

				String url = SERVER_URL + "/assetrequest/getbyid/" + assetid + "/"
						+ (selectedNotificationId != null ? selectedNotificationId : 0) + "/"
						+ (reqvalue != null ? reqvalue : 0);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					viewassetdetails = mapper.readValue(jsonResponse, new TypeReference<AssetRequestViewDTO>() {
					});

					assetRequestListDTO.setId(selectedasset.getId());
					assetRequestListDTOList = viewassetdetails.getAssetlist();
					setForwardTo(forwadtousermaster.getId());
					forwardTo = viewassetdetails.getForwardTo();
					forwardFor = viewassetdetails.getForwardFor();
					assetRequestListDTO.setNote(viewassetdetails.getNote());

				} else {

					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}

				return ADD_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends viewDetails------ >>>>");

		return null;

	}

	public String clearSession() {
		log.info("<<<< ----------Start Clearing Session ------- >>>>");
		log.info("<<<< ----------Setting Button Flags ------- >>>>");
		addButtonFlag = true;
		viewButtonFlag = true;
		deleteButtonFlag = true;
		editButtonFlag = true;
		baseAllocation = false;
		selectedasset = new AssetRequestListDTO();
		log.info("<<<< ----------Returning List Page ------- >>>>" + LIST_PAGE);
		return LIST_PAGE;
	}

	public String createAssetRequestAllocation() {

		log.info("<<<< ----------Start creatMereting ------- >>>>");
		log.info("meeting edit fonction is clled");
		log.info("create meeting id called", assetRequestListDTOList);
		if (assetRequestListDTOList.size() == 0) {
			errorMap.notify(ErrorDescription.ENTER_ATLEAST_ONE_DETAIL.getErrorCode());
			log.info("Inside Empty List");
			return null;
		} else {

			for (AssetRequestListDTO assetalloc : assetRequestListDTOList) {
				if (assetalloc.getApprove_quantity() == null || assetalloc.getApprove_quantity() <= 0) {
					AppUtil.addWarn("Please Enter Allocate Quantity");
					log.info("Inside Empty List");
					return null;
				}
			}
			assetRequestListDTO.setAddAssetRequest(assetRequestListDTOList);
			setForwardTo(forwadtousermaster.getId());
			assetRequestListDTO.setForwardTo(forwardTo);
			assetRequestListDTO.setForwardFor(forwardFor);
			log.info("create request list" + assetRequestListDTO);

			try {

				assetRequestListDTO.setId(assetrequestId);
				BaseDTO baseDTO = new BaseDTO();

				String url = SERVER_URL + "/assetrequest/updateallocation";

				baseDTO = httpService.post(url, assetRequestListDTO);

				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						errorMap.notify(ErrorDescription.ASSETALLOCATION_SAVE_SUCCESS.getErrorCode());
					}

				}
				log.info("create response", createresponse);
				return showAssetList();
				// assetRequestListDTO = new AssetRequestListDTO();

			} catch (Exception e) {
				log.error("found exception in onchangeEntityType: ", e);
			}
		}
		log.info("<<<< ----------End createMeetingType ------- >>>>");
		return LIST_PAGE;
	}

	public String showallocation() {
		log.info("<<<< ----------Starts showing allocation ------- >>>>");
		log.info("<<<< ----------Asset Request ID ------- >>>>" + selectedasset.getId());
		long assetid = selectedasset.getId();
		assetsInfo = null;
		quantity = null;
		try {

			createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();
			if (reqbaseAllocation.equals(false)) {
				reqvalue = 0L;
			} else {
				reqvalue = 1L;
			}
			log.info("<<<< ----------Retriving Details for Asset Request------- >>>>");
			String url = SERVER_URL + "/assetrequest/getbyid/" + assetid + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0) + "/"
					+ (reqvalue != null ? reqvalue : 0);
			log.info("URL-->" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewassetdetails = mapper.readValue(jsonResponse, new TypeReference<AssetRequestViewDTO>() {
				});

				log.info("<<<< ----------Setting Details for Asset Allocation------- >>>>");
				assetRequestListDTOList = viewassetdetails.getAssetlist();
				// setForwardTo(forwadtousermaster.getId());
				forwardTo = viewassetdetails.getForwardTo();
				forwardFor = viewassetdetails.getForwardFor();
				assetRequestListDTO.setNote(viewassetdetails.getNote());
				return ALLOC_ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("found exception in showallocation: ");
			e.printStackTrace();
		}
		return null;
	}

}
