/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyFieldVerificationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.weavers.model.SocietyEnrollment;
import in.gov.cooptex.weavers.model.SocietyFieldVerification;
import in.gov.cooptex.weavers.model.SocietyFieldVerificationEmp;
import in.gov.cooptex.weavers.model.SocietyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("societyFieldVerificationBean")
@Scope("session")
@Log4j2
public class SocietyFieldVerificationBean {
	@Autowired
	AppPreference appPreference;

	private final String CREATE_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/createFieldVerification.xhtml?faces-redirect=true;";
	private final String LIST_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/listFieldVerification.xhtml?faces-redirect=true;";
	private final String VIEW_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/viewFieldVerification.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	SocietyRegistrationRequest societyRegistrationRequest;
	@Getter
	@Setter
	List<SocietyRegistrationRequest> societyRegReqList = null;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;
	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = null;
	@Getter
	@Setter
	EntityMaster entityMaster;
	@Getter
	@Setter
	List<EntityMaster> entityMasterList = null;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = null;

	@Getter
	@Setter
	SocietyFieldVerification societyFieldVerification;
	@Getter
	@Setter
	SocietyEnrollment societyEnrollment;
	@Getter
	@Setter
	String societyAddress="";
	@Getter
	@Setter
	AddressMaster memberaddress = new AddressMaster();
	@Getter
	@Setter
	AddressMaster industryAddress = new AddressMaster();

	// ================Collecter list==========
	@Getter
	@Setter
	SocietyFieldVerificationEmp committeeMembers;

	@Getter
	@Setter
	List<SocietyFieldVerificationEmp> committeeMembersList = null;

	@Getter
	@Setter
	List<SocietyFieldVerificationEmp> delectedcommitteeMembersList = null;

	@Getter
	@Setter
	SocietyFieldVerification loomVerification;

	@Getter
	@Setter
	List<SocietyFieldVerification> loomVerificationList = null;

	@Getter
	@Setter
	List<SocietyFieldVerification> delectedloomVerificationList = null;

	// ==============================================

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	String addressfield;

	public String showSocietyFieldVerificationListPage() {
		log.info("<==== Starts societyFieldVerificationBean-showSocietyFieldVerificationListPage =====>");
		mapper = new ObjectMapper();
		selectedsocietyFieldVerification = new SocietyFieldVerification();

		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		action = "";
		loadLazySocietyFieldVerificationList();
		log.info("<==== Ends societyFieldVerificationBean-showSocietyFieldVerificationListPage =====>");
		return LIST_SOCIETY_PAGE;
	}

	public String societyFieldVerificationListPageAction() {
		log.info("<========inside societyFieldVerificationBean.societyFieldVerificationListPageAction=======>");
		societyFieldVerification = new SocietyFieldVerification();
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("create Society Field Verification called..");
				societyRegReqList = new ArrayList<>();
				entityTypeMasterList = new ArrayList<>();
				committeeMembersList = new ArrayList<>();
				loomVerificationList = new ArrayList<>();
				delectedloomVerificationList = new ArrayList<>();
				delectedcommitteeMembersList = new ArrayList<>();
				societyRegistrationRequest = new SocietyRegistrationRequest();
				societyFieldVerification = new SocietyFieldVerification();
				societyEnrollment=new SocietyEnrollment();
				societyAddress=null;
				societyRegReqList = commonDataService.getSubmittedsocietyEnrollment();
				entityTypeMasterList = commonDataService.loadEntityTypeList();
				log.info("=======societyregreq List size=============>" + societyRegReqList.size());
				return CREATE_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View Society Field Verificationcalled.."
						+ selectedsocietyFieldVerification.getSocietyRegistrationRequest().getId());
				getsocietyFieldVerificationbyId();
				return VIEW_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("Edit Society Field Verification called..");
				societyRegReqList = new ArrayList<>();
				entityTypeMasterList = new ArrayList<>();
				committeeMembersList = new ArrayList<>();
				loomVerificationList = new ArrayList<>();
				delectedloomVerificationList = new ArrayList<>();
				delectedcommitteeMembersList = new ArrayList<>();
				societyRegistrationRequest = new SocietyRegistrationRequest();
				societyFieldVerification = new SocietyFieldVerification();
				societyRegReqList = commonDataService.getSubmittedsocietyEnrollment();
				entityTypeMasterList = commonDataService.loadEntityTypeList();
				getsocietyFieldVerificationbyId();
				return CREATE_SOCIETY_PAGE;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void getsocietyFieldVerificationbyId() {
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyFieldVerification/getSocietyFieldbyReqID/"
					+ selectedsocietyFieldVerification.getSocietyRegistrationRequest().getId();
			log.info("after Url declared====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			SocietyFieldVerificationDTO societyFieldVerificationDTO = new SocietyFieldVerificationDTO();

			selectedsocietyFieldVerification = new SocietyFieldVerification();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyFieldVerificationDTO = mapper.readValue(jsonResponse,
						new TypeReference<SocietyFieldVerificationDTO>() {
						});

				societyRegistrationRequest = societyFieldVerificationDTO.getSocietyFieldVerification()
						.getSocietyRegistrationRequest();
				societyFieldVerification = societyFieldVerificationDTO.getSocietyFieldVerification();
				committeeMembersList = societyFieldVerificationDTO.getCommitteeMembersList();
				loomVerificationList = societyFieldVerificationDTO.getLoomVerificationList();

			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	@Getter
	@Setter
	LazyDataModel<SocietyFieldVerification> lazySocietyFieldVerificationList;
	@Getter
	@Setter
	SocietyFieldVerification selectedsocietyFieldVerification;

	@Getter
	@Setter
	List<SocietyFieldVerification> societyFieldVerificationList = null;

	String jsonResponse;

	public void loadLazySocietyFieldVerificationList() {
		log.info("<===== Starts loadLazySocietyFieldVerificationList ======>");

		String lazylistUrl = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/societyFieldVerification/getlazyGroupedSocietyFieldVerification";

		lazySocietyFieldVerificationList = new LazyDataModel<SocietyFieldVerification>() {

			private static final long serialVersionUID = -1881518467214125592L;

			@Override
			public List<SocietyFieldVerification> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for loadLazySocietyFieldVerificationList=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						societyFieldVerificationList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyFieldVerification>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazySocietyFieldVerificationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + societyFieldVerificationList.size());
				return societyFieldVerificationList;
			}

			@Override
			public Object getRowKey(SocietyFieldVerification res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyFieldVerification getRowData(String rowKey) {
				try {
					for (SocietyFieldVerification SocietyFieldVerification : societyFieldVerificationList) {
						if (SocietyFieldVerification.getId().equals(Long.valueOf(rowKey))) {
							selectedsocietyFieldVerification = SocietyFieldVerification;
							return SocietyFieldVerification;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.loadLazySocietyFieldVerification List ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts selectedsocietyFieldVerificationbean.onRowSelect ========>" + event);
		selectedsocietyFieldVerification = ((SocietyFieldVerification) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends selectedsocietyFieldVerificationBean.onRowSelect ========>"
				+ selectedsocietyFieldVerification.getSocietyRegistrationRequest().getId());
	}

	public void entityTypeChange() {
		try {
			log.info("=====entityTypeChange Called========>" + entityTypeMaster.getEntityName());
			entityMasterList = new ArrayList<>();
			entityMasterList = commonDataService.getEntityMaster(entityTypeMaster);

		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public void entityChange() {
		try {
			log.info("=====entityChange Called=========>" + entityMaster.getName());
			employeeMasterList = new ArrayList<>();
			employeeMasterList = commonDataService.getEmployeeMasterByEntityId(entityMaster.getId());
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public String adrressString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	public String subString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
			subStringAddress = subStringAddress.substring(0, 19);
			subStringAddress = subStringAddress + "........";
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		if (addressfield.equalsIgnoreCase("memberAddress")) {
			setMemberaddress(addressMasterBean.getAddressMaster());
		} else if (addressfield.equalsIgnoreCase("industryAddress")) {
			setIndustryAddress(addressMasterBean.getAddressMaster());
		}
		addressMasterBean.setAddressMaster(new AddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("createSocietyFieldForm");
	}

	public String saveOrUpdateSocietyFieldVerification() {
		log.info("inside saveOrUpdateSocietyFieldVerification()");
		try {
			if (committeeMembersList.size() == 0) {
				errorMap.notify(ErrorDescription.SOCIETY_FIELD_COMMITTEE_ADD_ATLEAST_ONE.getCode());
				return null;
			} else if (loomVerificationList.size() == 0) {
				errorMap.notify(ErrorDescription.SOCIETY_FIELD_LOOMLIST_ADD_ATLEAST_ONE.getCode());
				return null;
			}
			/*
			 * if(community.getCode() == null || community.getCode().trim().isEmpty()) {
			 * errorMap.notify(ErrorDescription.COMMUNITY_CODE_EMPTY.getCode()); return
			 * null; }
			 */
			SocietyFieldVerificationDTO societyFieldVerificationDTO = new SocietyFieldVerificationDTO();
			societyFieldVerification.setSocietyRegistrationRequest(societyRegistrationRequest);
			societyFieldVerificationDTO.setSocietyFieldVerification(societyFieldVerification);
			societyFieldVerificationDTO.setCommitteeMembersList(committeeMembersList);
			societyFieldVerificationDTO.setLoomVerificationList(loomVerificationList);
			societyFieldVerificationDTO.setAction(action);
			societyFieldVerificationDTO.setDeletedCommitteeMembersList(delectedcommitteeMembersList);
			societyFieldVerificationDTO.setDeletedLoomVerificationList(delectedloomVerificationList);
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyFieldVerification/saveOrUpdateSocietyFieldVerification";
			BaseDTO response = httpService.post(url, societyFieldVerificationDTO);
			if (response != null && response.getStatusCode() == 0) {
				log.info("selectedSocietyFieldVerification saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.SOCIETY_FIELD_INSERTED_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.SOCIETY_FIELD_UPDATE_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Error Occured ::", e);
		}
		return LIST_SOCIETY_PAGE;
	}

	public void removeloomlist() {
		log.info("=====removeloomlist===Called=======");
		try {
			loomVerificationList.removeAll(delectedloomVerificationList);
		} catch (Exception e) {
			log.info("removeloomlist Error :" + e);
		}
	}

	public void removeCommitteelist() {
		log.info("=====removeCommitteelist===Called=======");
		try {
			committeeMembersList.removeAll(delectedcommitteeMembersList);
		} catch (Exception e) {
			log.info("removeCommitteelist Error :" + e);
		}
	}

	public String committeeMembersListAdd() {
		log.info("=====committeeMembersListAdd===Called=======");
		try {
			committeeMembers = new SocietyFieldVerificationEmp();
			committeeMembers.setEntityMaster(entityMaster);
			committeeMembers.setEmpMaster(employeeMaster);
			committeeMembersList.add(committeeMembers);
			clear();
			// entityMaster = new EntityMaster();
			// employeeMaster = new EmployeeMaster();

		} catch (Exception e) {
			log.info("committeeMembersListAdd Error :" + e);
		}
		return null;
	}

	public void clear() {
		entityMaster = new EntityMaster();
		employeeMaster = new EmployeeMaster();
		entityTypeMaster = new EntityTypeMaster();
		entityMasterList = new ArrayList<>();
		employeeMasterList = new ArrayList<>();
//		societyFieldVerification.setDateOfVisit(null);
	}

	public String loomVerificationListAdd() {
		log.info("=====loomVerificationListAdd===Called=======");
		try {
			if(memberaddress == null || memberaddress.getAddressLineOne() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.MEMBER_ADDRESS_IS_NOT_EMPTY).getCode());
				return null;
			}
			if(industryAddress == null || industryAddress.getAddressLineOne() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.INDUSTRY_ADDRESS_IS_NOT_EMPTY).getCode());
				return null;
			}
			loomVerification = new SocietyFieldVerification();
			loomVerification.setMemberName(societyFieldVerification.getMemberName());
			loomVerification.setNumberOfLooms(societyFieldVerification.getNumberOfLooms());
			loomVerification.setAadharNumber(societyFieldVerification.getAadharNumber());
			loomVerification.setRationCardNumber(societyFieldVerification.getRationCardNumber());
			loomVerification.setMemberMaster(memberaddress);
			loomVerification.setMemberIndustryAddress(industryAddress);
			loomVerificationList.add(loomVerification);
			loomVerificationClear();

		} catch (Exception e) {
			log.info("loomVerificationListAdd Error :" + e);
		}
		return null;
	}

	public void loomVerificationClear() {
		societyFieldVerification.setMemberName("");
		societyFieldVerification.setNumberOfLooms(null);
		societyFieldVerification.setAadharNumber("");
		societyFieldVerification.setRationCardNumber("");
		memberaddress = new AddressMaster();
		industryAddress = new AddressMaster();
	}

	public void getSocietyAddressBySocietyRegistrationRequestId() {
		log.info("getSocietyAddressBySocietyRegistrationRequestId called");
		if (getSocietyRegistrationRequest() != null && getSocietyRegistrationRequest().getId() != null) {
			log.info("getSocietyRegistrationRequest======>" + getSocietyRegistrationRequest().getId());
			// SocietyEnrollment getSocietyEnrollmentbySocRegReqId(Long id);
			societyEnrollment = commonDataService
					.getSocietyEnrollmentbySocRegReqId(getSocietyRegistrationRequest().getId());
			log.info("societyEnrollment========>" + societyEnrollment.getSocietyAddress());
			AddressMaster address=societyEnrollment.getSocietyAddress();
			
			 societyAddress = address.getAddressLineOne() + ", " + address.getAddressLineTwo() + ", "
					+ address.getAddressLineThree() + ", " + address.getDistrictMaster().getName() + ", "
					+ address.getStateMaster().getName() + ", Pin: " + address.getPostalCode() + " Landmark: "
					+ address.getLandmark();
			log.info("societyAddress===>"+societyAddress);
		}

	}
}
