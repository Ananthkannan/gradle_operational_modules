package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EntityMasterDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.Region;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.operation.dto.RegionWiseSalesData;
import in.gov.cooptex.operation.dto.RoPlanDetailsDto;
import in.gov.cooptex.operation.dto.ShowroomPlanDetailsDto;
import in.gov.cooptex.operation.enums.RequestType;
import in.gov.cooptex.operation.enums.RetailProductionPlanStage;
import in.gov.cooptex.operation.enums.RetailProductionPlanStatus;
import in.gov.cooptex.operation.production.dto.ProductWiseProductionPlanDto;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("retailProductionRegionalOfficeBean")
@Scope("session")
public class RetailProductionRegionalOfficeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String REGIONALOFFICE_LIST_URL = "/pages/operation/retailproduction/ROProductionPlanList.xhtml?faces-redirect=true;";

	private final String REGIONALOFFICE_SHOWROOM_LIST_URL = "/pages/operation/retailproduction/listInitialProductionPlanRo.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	String originatedFrom;

	@Getter
	@Setter
	LazyDataModel<RetailProductionPlanResponse> retailProductionPlanResponseLazyList;

	@Getter
	@Setter
	LazyDataModel<RoPlanDetailsDto> showroomPlanDetailsDtoList;

	@Getter
	@Setter
	RoPlanDetailsDto roPlanDetailsDtoResponse;

	@Getter
	@Setter
	List<RoPlanDetailsDto> showRoomPdfList;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

	@Getter
	@Setter
	StringBuilder productCategoryValues = new StringBuilder();

	@Getter
	@Setter
	List<RetailProductionPlanResponse> regionsPdfList;

	@Getter
	@Setter
	Integer resultSize;

	@Getter
	@Setter
	String serverURL;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	ShowroomPlanDetailsDto showroomPlanDetailsDto;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Date stockAsOnDate;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> productWiseProductionPlanDtoList;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Region region;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	ShowroomPlanDetailsDto selectedShowroomPlanDetailsDto;

	@Getter
	@Setter
	ShowroomPlanDetailsDto selectedShowroomPlanDetailsDtoFilter;

	@Getter
	@Setter
	ShowroomPlanDetailsDto productWisePlanDetailsDTO;

	@Getter
	@Setter
	ShowroomPlanDetailsDto showroomPlanDetailsDtoFilterMap;

	@Getter
	@Setter
	Map<String, List<ProductWiseProductionPlanDto>> collect;

	@Getter
	@Setter
	RegionWiseSalesData regionWiseSalesData;

	@Getter
	@Setter
	RetailProductionPlanRegion retailProductionPlanRegion;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> selectedProductWiseProductionPlanDtoList;

	@Getter
	@Setter
	Map<String, Double> totalShowroomDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomProductValueMap;

	@Getter
	@Setter
	ShowroomPlanDetailsDto productWisePlanDetailsDTOForFilter;

	@Getter
	@Setter
	Map<String, Double> totalShowroomRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomFinalValueMap;

	@Getter
	@Setter
	List<ProductWiseProductionPlanDto> regionWiseProductDetails;

	@Getter
	@Setter
	Integer showroomPercentage;

	@Getter
	@Setter
	Integer notSubmittedShowrromCount;

	@Getter
	@Setter
	Integer totalShowroomCount;

	@Getter
	@Setter
	private String currentPlanStatus = "INITIATED";

	@Getter
	@Setter
	String comments;

	@Getter
	@Setter
	String commentsReject;

	@Getter
	@Setter
	String commentsApprove;

	@Getter
	@Setter
	BaseDTO baseDTO;

	@Getter
	@Setter
	Map<String, Double> totalDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMap;

	@Getter
	@Setter
	String searchText;

	@Getter
	@Setter
	List statusAndStageValues;

	@Autowired
	RetailProductionPlanLogBean retailProductionPlanLogBean;

	@Getter
	@Setter
	Map<String, Double> totalProductDraftValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductShowroomValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductRoValueMap;

	@Getter
	@Setter
	Map<String, Double> totalProductFinalValueMap;

	@Setter
	@Getter
	StreamedContent file;

	@Getter
	@Setter
	Map filterMap;

	@Getter
	@Setter
	boolean approveOrRejectEnable;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	Integer defaultRowSize;

	boolean originatedFromMenu;

	Long roPlanId;
	
	@Getter @Setter
	String action;

	public RetailProductionRegionalOfficeBean() {
		log.info("<#======Inside RetailProductionRegionalOffice Bean======#>");
		loadServerURL();
	}

	private void loadStatusValues() {
		Object[] statusArray = RetailProductionPlanStatus.class.getEnumConstants();
		Object[] stageArray = RetailProductionPlanStage.class.getEnumConstants();

		statusAndStageValues = new ArrayList<>();

		for (Object stage : stageArray) {
			for (Object status : statusArray) {
//				if (!stage.toString().equalsIgnoreCase("DRAFT")) {
					if (!(stage + "-" + status).equalsIgnoreCase("SHOWROOM-APPROVED")
							&& !(stage + "-" + status).equalsIgnoreCase("SHOWROOM-REJECTED")) {
						statusAndStageValues.add(stage + "-" + status);
					}
//				}
			}
		}

		log.info("statusAndStageValues " + statusAndStageValues);
	}

	private void loadServerURL() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	// used to get the list of RO wise regions under the select planId
	public String getRegionalOfficePlanList(Long planId) {
		if (originatedFrom == null) {
			originatedFrom = "RO";
		}
		originatedFromMenu = false;

		defaultRowSize = appPreference.getDefaultListSize();

		log.info(" inside getRegionalOfficePlanList");

		log.info(" planId  " + planId);
		roPlanId = planId;
		loadStatusValues();
		loadServerURL();
		loadLazyData();

		return REGIONALOFFICE_LIST_URL;
	}

	// loads Region wise data under the selected plan
	private void loadLazyData() {
		log.info(" Inside LoadLazyData");
		retailProductionPlanResponseLazyList = new LazyDataModel<RetailProductionPlanResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RetailProductionPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				log.info("load is called.");

				filterMap = filters;
				sortingOrder = sortOrder;
				sortingField = sortField;

				List<RetailProductionPlanResponse> data = new ArrayList<RetailProductionPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse,
							new TypeReference<List<RetailProductionPlanResponse>>() {
							});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RetailProductionPlanResponse response) {
				log.info("getRowKey is called.");
				return response != null ? response.getRegionId(): null;
			}

			@Override
			public RetailProductionPlanResponse getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RetailProductionPlanResponse> responseList = (List<RetailProductionPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RetailProductionPlanResponse response : responseList) {
					if (response.getRegionId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}

	public boolean filterMapDetails() {

		collect = showroomPlanDetailsDtoFilterMap.getProductWiseProductionPlanMap().entrySet().stream()
				.filter(map -> map.getKey().contains(searchText))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

		showroomPlanDetailsDto.setProductWiseProductionPlanMap(collect);

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
				.getProductWiseProductionPlanMap().entrySet()) {
			productWiseProductionPlanDtoList = entry.getValue();
		}
		return true;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		retailProductionPlanResponse = new RetailProductionPlanResponse();
		BaseDTO baseDTO = new BaseDTO();
		RetailProductionPlanRequest request = getRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			Long regionId = null;
			EntityMasterDTO entityMaster = null;
			EntityMaster workLocationEntity = null;
			EmployeePersonalInfoEmployment personalInfoEmployment = null;
			EmployeeMaster employeMaster = loginBean.getEmployee();
			if (employeMaster != null) {
				personalInfoEmployment = employeMaster.getPersonalInfoEmployment();
				if (personalInfoEmployment != null) {
					workLocationEntity = personalInfoEmployment.getWorkLocation();
				} else {
					log.error("EmployeePersonalInfoEmployment is empty");
				}
			} else {
				log.error("EmployeeMaster is empty");
			}
			if (workLocationEntity != null) {
				entityMaster = loadEntityByEntityId(workLocationEntity.getId());
				if (entityMaster.getEntityTypeMaster().getEntityCode().equals(EntityType.HEAD_OFFICE)
						|| entityMaster.getEntityTypeMaster().getEntityCode().equals(EntityType.REGIONAL_OFFICE)) {
					regionId = personalInfoEmployment.getWorkLocation().getId();
					request.setLoginUserEntityType(entityMaster.getEntityTypeMaster().getEntityCode());
					log.info("getSearchData login entityType --------->"+request.getLoginUserEntityType());

				} else {
					regionId = entityMaster.getEntityMasterRegion().getId();
				}
			}
			request.setLoginRegionId(regionId);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	public EntityMasterDTO loadEntityByEntityId(Long id) {
		EntityMasterDTO entityMaster = new EntityMasterDTO();
		try {
			String URL = AppUtil.getPortalServerURL() + "/entitymaster/getentitymasterbyentityId/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			entityMaster = mapper.readValue(jsonResponse, EntityMasterDTO.class);

			if (entityMaster == null) {
				log.info("EmployeeMasterBean loadEntityByEntityId value is null");
			}

			log.info("SupplyRateConfirmationBean entityMasterObject " + entityMaster.toString());
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
		return entityMaster;
	}

	/*
	 * 
	 * RO WISE Showroom deatils list
	 * 
	 */

	private BaseDTO getShowRoomSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		BaseDTO baseDTO = new BaseDTO();

		roPlanDetailsDtoResponse = new RoPlanDetailsDto();

		RetailProductionPlanRequest request = getShowroomRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {

			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/showroom/search";
			baseDTO = httpService.post(URL, request);
			return baseDTO;
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	/*
	 * RO WISE Showroom deatils list
	 * 
	 * 
	 * 
	 */
	private void loadShowRoomLazyData() {
		log.info(" Inside loadShowRoomLazyData");
		showroomPlanDetailsDtoList = new LazyDataModel<RoPlanDetailsDto>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<RoPlanDetailsDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				log.info("load is called.");

				filterMap = filters;
				sortingOrder = sortOrder;
				sortingField = sortField;

				List<RoPlanDetailsDto> data = new ArrayList<RoPlanDetailsDto>();
				try {
					BaseDTO baseDTO = getShowRoomSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					data = objectMapper.readValue(jsonResponse, new TypeReference<List<RoPlanDetailsDto>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						resultSize = baseDTO.getTotalRecords();
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(RoPlanDetailsDto response) {
				log.info("getRowKey is called.");
				return response != null ? response.getPlanId() : null;
			}

			@Override
			public RoPlanDetailsDto getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<RoPlanDetailsDto> responseList = (List<RoPlanDetailsDto>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (RoPlanDetailsDto response : responseList) {
					if (response.getShowroomId().longValue() == value.longValue()) {
						roPlanDetailsDtoResponse = response;
						return response;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Retail Monthwise Procurement Plan onRowSelect method started");
		retailProductionPlanResponse = ((RetailProductionPlanResponse) event.getObject());
	}
	
	public void onRowSelectbutton(RetailProductionPlanResponse retailObj) {
		log.info("Retail Monthwise Procurement Plan onRowSelect method started");
		retailProductionPlanResponse = retailObj;
	}
	public String viewRegionalOffice() {

		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			log.info("retailProductionPlanResponse >>>>>>>>>>>" + retailProductionPlanResponse);
			errorMap.notify(8525);
			return null;
		}
		retailProductionPlanLogBean.setRetailProductionPlanResponse(retailProductionPlanResponse);
		log.info("viewRegionalOffice plan id--------------->"+retailProductionPlanResponse.getPlanId());

		showroomPlanDetailsDto = new ShowroomPlanDetailsDto();
		totalShowroomCount = 0;
		searchText = null;
		getRegionalOfficePlanDetails();
		getRegionalDetails();
		getregionPlanDetails();
		getShowRoomCountDetails();
		return "/pages/operation/retailproduction/viewRoProductionPlan.xhtml?faces-redirect=true;\";";
	}

	public String getRegionalOfficePlanDetails() {
		log.info("Inside get region method input userid >>>>>>>>>>>" + retailProductionPlanResponse);
		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getPlanId() == null) {
			errorMap.notify(8525);
			return null;
		}
		try {
			/*String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/get/"
					+ retailProductionPlanResponse.getPlanId();*/
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getregionplandetails/"
					+ retailProductionPlanResponse.getPlanId();
			baseDTO = httpService.get(URL);
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			retailProductionPlan = objectMapper.readValue(jsonResponse, RetailProductionPlan.class);
			productCategoryValues.setLength(0);
			log.info("<--- retailProductionPlan.getProductCategoryList() ---> "
					+ retailProductionPlan.getProductCategoryList());
			for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
				if (categoryValue != null && categoryValue.getProductCatCode() != null
						&& categoryValue.getProductCatName() != null)
					productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
							.append(categoryValue.getProductCatName()).append(",<br/>");
			}
			productCategoryValues.deleteCharAt(productCategoryValues.length() - 6);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return null;
	}

	public void getRegionalDetails() {

		log.info("Inside get region method input userid >>>>>>>>>>>" + loginBean.getUserDetailSession().getId());
		Long userId = loginBean.getUserDetailSession().getId();
		String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/societyPlan/getRegion/" + userId;

		try {
			// baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			// region = mapper.readValue(jsonResponse, Region.class);
			if (retailProductionPlanResponse != null) {
				URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getregiondetails/"
						+ retailProductionPlanResponse.getRegionId() + "/" + retailProductionPlanResponse.getPlanId();
				baseDTO = httpService.get(URL);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailProductionPlanRegion = mapper.readValue(jsonResponse, RetailProductionPlanRegion.class);
			}

		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		currentPlanStatus = value;
		comments = null;
		commentsApprove = null;
		commentsReject = null;
	}

	public void getShowRoomCountDetails() {
		showroomPercentage = 0;
		approveOrRejectEnable = false;
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getshowroomcount/"
					+ retailProductionPlanResponse.getPlanId();
			baseDTO = httpService.get(URL);
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			regionWiseSalesData = objectMapper.readValue(jsonResponse, RegionWiseSalesData.class);
			if (regionWiseSalesData != null && regionWiseSalesData.getSubmittedShowroomCount() != null) {
				if (regionWiseSalesData.getSubmittedShowroomCount() != null) {
					showroomPercentage = regionWiseSalesData.getSubmittedShowroomCount() * 100 / totalShowroomCount;
				}
				notSubmittedShowrromCount = totalShowroomCount - regionWiseSalesData.getSubmittedShowroomCount();
			}
			if (showroomPercentage != null && showroomPercentage.equals(100)) {
				approveOrRejectEnable = true;
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public void getPlan(Long showroomId, Long planId) {

		log.info("Get Plan Method Called ==>");

		ShowroomPlanDetailsDto request = new ShowroomPlanDetailsDto();
		request.setPlanId(planId);
		request.setShowroomId(showroomId);
		request.setRequestType(RequestType.VIEW);

		selectedShowroomPlanDetailsDto = new ShowroomPlanDetailsDto();

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/showroom/getplandetails";

		BaseDTO baseDTO = httpService.post(url, request);

		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				selectedShowroomPlanDetailsDto = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				selectedShowroomPlanDetailsDtoFilter = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : selectedShowroomPlanDetailsDto
						.getProductWiseProductionPlanMap().entrySet()) {
					selectedProductWiseProductionPlanDtoList = entry.getValue();
				}
				log.info("showroomPlanDetailsDto == >>" + selectedShowroomPlanDetailsDto);
				calculateTotalShowroomValue();
			} catch (IOException e) {
				log.error(e);
			}
		}

	}

	public boolean filterProductDetails() {

		collect = selectedShowroomPlanDetailsDtoFilter.getProductWiseProductionPlanMap().entrySet().stream()
				.filter(map -> map.getKey().contains(searchText))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

		selectedShowroomPlanDetailsDto.setProductWiseProductionPlanMap(collect);

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : selectedShowroomPlanDetailsDto
				.getProductWiseProductionPlanMap().entrySet()) {
			selectedProductWiseProductionPlanDtoList = entry.getValue();
		}
		return true;
	}

	public void calculateTotalShowroomValue() {

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : selectedShowroomPlanDetailsDto
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalShowroomDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalShowroomProductValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalShowroomRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalShowroomFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));

	}

	public String clearROSearch() {
		retailProductionPlanResponse = new RetailProductionPlanResponse();
		return REGIONALOFFICE_LIST_URL;
	}

	public String clearROShowroomSearch() {
		roPlanDetailsDtoResponse = new RoPlanDetailsDto();
		return REGIONALOFFICE_SHOWROOM_LIST_URL;
	}

	public boolean filterByPrice(Object value, Object filter, Locale locale) {
		String filterText = (filter == null) ? null : filter.toString().trim();
		if (filterText == null || filterText.equals("")) {
			return true;
		}

		if (value == null) {
			return false;
		}

		return ((Comparable) value).compareTo(filterText) > 0;
	}

	public void getProductWisePlanDetails() {

		log.info("Get getProductWisePlanDetails Called ==>");

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getproductdetails/"
				+ retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				productWisePlanDetailsDTO = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				productWisePlanDetailsDTOForFilter = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : productWisePlanDetailsDTO
						.getProductWiseProductionPlanMap().entrySet()) {
					regionWiseProductDetails = entry.getValue();
				}
				calculateTotalProductValue();
				log.info("productWisePlanDetailsDTO == >>" + productWisePlanDetailsDTO);

			} catch (IOException e) {
				log.error(e);
			}
		}
		RequestContext.getCurrentInstance().execute("PF('dlg2').show();");

	}

	public boolean filterProductWiseDetails() {

		collect = productWisePlanDetailsDTOForFilter.getProductWiseProductionPlanMap().entrySet().stream()
				.filter(map -> map.getKey().contains(searchText))
				.collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

		productWisePlanDetailsDTO.setProductWiseProductionPlanMap(collect);

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : productWisePlanDetailsDTO
				.getProductWiseProductionPlanMap().entrySet()) {
			regionWiseProductDetails = entry.getValue();
		}
		return true;
	}

	public void calculateTotalProductValue() {

		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : productWisePlanDetailsDTO
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalProductDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalProductShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalProductRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalProductFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));

	}

	public void setSelectedColumn(Map.Entry<String, List<ProductWiseProductionPlanDto>> value) {
		log.info("Get Plan Method Called ==>" + value);
		String showroomCode = null;
		try {
			if (value != null && value.getKey() != null) {
				String[] keyList = value.getKey().split("/");
				showroomCode = keyList[0];
				String url = serverURL + "/entitymaster/getByCode/" + showroomCode;
				BaseDTO baseDTO = httpService.get(url);
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				EntityMaster entityMaster = mapper.readValue(jsonValue, EntityMaster.class);
				if (entityMaster != null) {
					getPlan(entityMaster.getId(), retailProductionPlanResponse.getPlanId());
				}
			}
		} catch (IOException e) {
			log.error(e);
		}
		RequestContext.getCurrentInstance().update("retailPlan:result");
		RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
	}

	public String statusUpdate() {
		log.info("Status Updated is called");
		log.info(currentPlanStatus);
		log.info(retailProductionPlan.getId());
		log.info(comments);

		String commentsGiven = "";

		if (currentPlanStatus.equals("APPROVED")) {
			commentsGiven = commentsApprove;
		} else {
			commentsGiven = commentsReject;
		}

		BaseDTO baseDTO = null;
		RetailProductionPlanRequest request = new RetailProductionPlanRequest(retailProductionPlan.getId(),
				currentPlanStatus, commentsGiven);
		request.setShowroomList(showroomPlanDetailsDto.getShowroomList());
		request.setRegionId(retailProductionPlanResponse.getRegionId());
		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/approve/ro";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/reject/ro";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return REGIONALOFFICE_LIST_URL;
	}

	public void getregionPlanDetails() {

		log.info("Get Plan Method Called ==>");
		totalShowroomCount = 0;

		if (retailProductionPlanResponse == null || retailProductionPlanResponse.getRegionId() == null) {
			errorMap.notify(8525);
			return;
		}
		
		log.info("getregionPlanDetails regionId----------->"+retailProductionPlanResponse.getRegionId());

		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/region/office/getplandetails/"
				+ retailProductionPlanResponse.getRegionId() + "/" + retailProductionPlanResponse.getPlanId();

		BaseDTO baseDTO = httpService.get(url);

		if (baseDTO != null) {
			try {

				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

				showroomPlanDetailsDto = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				showroomPlanDetailsDtoFilterMap = mapper.readValue(jsonValue, ShowroomPlanDetailsDto.class);

				for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
						.getProductWiseProductionPlanMap().entrySet()) {
					productWiseProductionPlanDtoList = entry.getValue();
					totalShowroomCount++;

				}
				calculateTotalValue();
				log.info("showroomPlanDetailsDto == >>" + showroomPlanDetailsDto);

			} catch (IOException e) {
				log.error(e);
			}
		}

	}

	public void calculateTotalValue() {
		List<ProductWiseProductionPlanDto> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<ProductWiseProductionPlanDto>> entry : showroomPlanDetailsDto
				.getProductWiseProductionPlanMap().entrySet()) {
			for (ProductWiseProductionPlanDto dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalDraftValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getDraftValue)));
		totalShowroomValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getShowroomValue)));
		totalRoValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getRoValue)));
		totalFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(ProductWiseProductionPlanDto::getMonth,
						Collectors.summingDouble(ProductWiseProductionPlanDto::getFinalValue)));

	}

	public void generateShowroomListPDF() {
		log.info("Inside retailProductionPlanRegionalOfficeBean generateShowroomListPDF ");
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/downloadshowroomlistpdf";

		try {

			RetailProductionPlanRequest request = getShowroomRequestObject(null, null, sortingField, sortingOrder,
					filterMap);
			log.info("Generate PDF [" + request + "]");
			InputStream inputStream = httpService.generatePDF(url, request);
			file = new DefaultStreamedContent(inputStream, "application/pdf", "RetailProductionPlanShowroomList.pdf");
			log.info("Generate PDF finished successfully....");

		} catch (ParseException e) {
			log.error("ParseException ", e);
		} catch (Exception ex) {
			log.error("Exception ", ex);
		}

	}

	public void generateRegionListPDF() {
		log.info("Inside retailProductionPlanRegionalOfficeBean generateRegionListPDF ");
		String url = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/downloadregionlistpdf";
		try {

			RetailProductionPlanRequest request = getRequestObject(null, null, sortingField, sortingOrder, filterMap);
			log.info("Generate PDF [" + request + "]");
			InputStream inputStream = httpService.generatePDF(url, request);
			file = new DefaultStreamedContent(inputStream, "application/pdf", "RetailProductionPlanRegionList.pdf");
			log.info("Generate PDF finished successfully....");

		} catch (ParseException e) {
			log.error("ParseException ", e);
		} catch (Exception ex) {
			log.error("Exception ", ex);
		}

	}

	public String getShowRoomList() {

		log.info(" inside getShowRoomList");

		if (retailProductionPlanResponse == null) {
			log.info("Select Atleast One Region ");
			Util.addWarn("Select One Record");
			return null;

		}

		loadStatusValues();
		loadServerURL();
		loadShowRoomLazyData();

		return REGIONALOFFICE_SHOWROOM_LIST_URL;
	}

	public String editPlan() {

		log.info("siva", retailProductionPlanResponse, roPlanDetailsDtoResponse);

		return null;

	}

	public RetailProductionPlanRequest getShowroomRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			if (entry.getKey().equals("showRoomCodeOrName")) {
				request.setShowRoomCodeOrName(value);
			}

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planTo")) {
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("receivedDate")) {
				request.setReceivedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dueDate")) {
				request.setDueDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("contactNumber")) {
				request.setContactNumber(value);
			}

			if (entry.getKey().equals("stageOrStatus")) {
				request.setStageOrStatus(value);
			}

		}

		log.info("response obj -- " + retailProductionPlanResponse);

		log.info("request obj retailProductionPlanResponse.getRegionId() -- "
				+ retailProductionPlanResponse.getRegionId());
		log.info("request obj retailProductionPlanResponse.getRegionId() -- "
				+ retailProductionPlanResponse.getPlanId());

		request.setRegionId(retailProductionPlanResponse.getRegionId());
		request.setId(retailProductionPlanResponse.getPlanId());

		log.info("request obj -- " + request);
		return request;
	}

	public RetailProductionPlanRequest getRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		RetailProductionPlanRequest request = new RetailProductionPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Filter Key [" + entry.getKey() + "] Filter Value [" + entry.getValue().toString() + "]");
			String value = entry.getValue().toString();

			if (entry.getKey().equals("regionCodeOrName")) {
				request.setRegionCodeOrName(value);
			}

			if (entry.getKey().equals("codeOrName")) {
				request.setCodeOrName(value);
			}

			if (entry.getKey().equals("planFrom")) {
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planTo")) {
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("receivedDate")) {
				request.setReceivedDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dueDate")) {
				request.setDueDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("stageOrStatus")) {
				request.setStageOrStatus(value);
			}

			if (entry.getKey().equals("netEligibilityValue")) {
				request.setNetEligibilityValue(Double.parseDouble(value));
			}
		}

		// passing the planId from UI
		request.setId(roPlanId);

		return request;
	}

}
