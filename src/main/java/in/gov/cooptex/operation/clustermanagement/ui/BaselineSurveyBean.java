package in.gov.cooptex.operation.clustermanagement.ui;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.context.FacesContext;

import org.hibernate.type.descriptor.sql.JdbcTypeFamilyInformation.Family;
import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.IdentificationType;
import in.gov.cooptex.core.model.LoomTypeMaster;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QualificationMaster;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.clustermanagement.model.WeaverRegister;
import in.gov.cooptex.operation.dto.BaseLineSurveyDTO;
import in.gov.cooptex.operation.enums.AverageProductionPerDayEnum;
import in.gov.cooptex.operation.enums.ClusterFamilyMembersEnum;
import in.gov.cooptex.operation.enums.EarningsPerMonthEnum;
import in.gov.cooptex.operation.enums.LoomActivityEnum;
import in.gov.cooptex.operation.enums.LoomDesignCapacityEnum;
import in.gov.cooptex.operation.enums.ProductDemandEnum;
import in.gov.cooptex.operation.enums.TrainingTypeEnum;
import in.gov.cooptex.operation.enums.WeaverEngagedYearsEnum;
import in.gov.cooptex.operation.enums.WeaversAncillaryWorkEnum;
import in.gov.cooptex.operation.enums.WeaversFamilyMembersEnum;
import in.gov.cooptex.operation.enums.WeaversVehicleDetailsEnum;
import in.gov.cooptex.operation.enums.WeaversWorkerEnum;
import in.gov.cooptex.operation.enums.WeaversWorkerTypeEnum;
import in.gov.cooptex.weavers.model.WeaversBenefitScheme;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("baselineSurveyBean")
public class BaselineSurveyBean {

	private static final String PORTAL_SERVER_URL = AppUtil.getPortalServerURL();
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	List<EntityMaster> regionsList;
	
	@Getter @Setter
	List<GenderMaster> gendersList;
	
	@Getter @Setter
	List<MaritalStatus> maritalStatusList;
	
	@Getter @Setter
	List<Religion> religionList;
	
	@Getter @Setter
	List<CasteMaster> casteList;
	
	@Getter @Setter
	List<QualificationMaster> qualificationList;
	
	@Getter @Setter
	List<BankMaster> bankList;
	
	@Getter @Setter
	List<BankBranchMaster> bankBranchList;
	
	@Getter @Setter
	List<ClusterFamilyMembersEnum> familyMembersList;
	
	@Getter @Setter
	List<WeaversFamilyMembersEnum> weaversFamilyMembersList;
	
	@Getter @Setter
	List<WeaversAncillaryWorkEnum> ancillaryWorkList;
	
	@Getter @Setter
	List<WeaversWorkerEnum> weaversWorkerList;
	
	@Getter @Setter
	List<WeaversVehicleDetailsEnum> weaversVehicleDetailsList;
	
	@Getter @Setter
	List<WeaversWorkerTypeEnum> weaversWorkerTypeList;
	
	@Getter @Setter
	List<WeaverEngagedYearsEnum> weaversEngagedYearsList;
	
	@Getter @Setter
	List<LoomDesignCapacityEnum> loomDesignCapacityEnumList;
	
	@Getter @Setter
	List<AverageProductionPerDayEnum> averageProductionPerDayList;
	
	@Getter @Setter
	List<EarningsPerMonthEnum> earningsPerMonthList;
	
	@Getter @Setter
	List<TrainingTypeEnum> trainingTypeList;
	
	@Getter @Setter
	List<ProductDemandEnum> productDemandList;
	
	@Getter @Setter
	List<LoomActivityEnum> loomActivityEnumList;
	
	@Getter @Setter
	List<IdentificationType> identificationList;
	
	@Getter @Setter
	List<StateMaster> stateList;
	
	@Getter @Setter
	List<DistrictMaster> districtList;
	
	@Getter @Setter
	List<CityMaster> cityList;
	
	@Getter @Setter
	List<AreaMaster> areaList;
	
	@Getter @Setter
	List<TalukMaster> talukList;
	
	@Getter @Setter
	List<LoomTypeMaster> loomTypeList;
	
	@Getter @Setter
	List<EmployeeMaster> reviewedEmployeeList;
	
	@Autowired
	ErrorMap errorMap;
	
	@Autowired
	HttpService httpService;
	
	ObjectMapper mapper=new ObjectMapper();
	
	String jsonValue;
	
	@Getter @Setter
	BankMaster bankMaster;
	
	@Getter @Setter
	StateMaster stateMaster;
	
	@Getter @Setter
	DistrictMaster districtMaster;
	
	@Getter @Setter
	CityMaster cityMaster;
	
	@Getter @Setter
	BaseLineSurveyDTO baseLineSurveyDTO;
	
	@Autowired
	LoginBean loginBean;
	
    @Getter @Setter
    List<UserMaster> forwardToUsersList;
    
    @Getter @Setter
    List<Designation> designationList;
    
    @Autowired
    AddressMasterBean addressMasterBean;
    
    @Autowired
    AppPreference appPreference;
	  
    @Getter @Setter
    List<ProductVarietyMaster> productList;
    
    @Getter @Setter
    List<WeaversBenefitScheme> weaverBenefitSchemeList;
    
	public void processData() {
		if (!FacesContext.getCurrentInstance().isPostback()) {
			log.info("<===== Start BaselineSurveyBean:processData() loadUIPage ======>");
			loadValues();
			loadWeaversEnumsList();
			bankMaster = new BankMaster();
			baseLineSurveyDTO = new BaseLineSurveyDTO();
			baseLineSurveyDTO.setAddressMaster(new AddressMaster());
			baseLineSurveyDTO.setEmployeeMaster(new EmployeeMaster());
			baseLineSurveyDTO.setWeaverRegister(new WeaverRegister());
			log.info("<===== End BaselineSurveyBean:processData loadUIPage ======>");
		}
	}
	
	public void loadValues() {
		log.info("<===== Start BaselineSurveyBean:loadValues() ======>");
		/* Load All Active Regions */
		regionsList = commonDataService.getSimpleRegionList(); 
		log.info("regionsList size" + regionsList.size());
		/* Load All Active Genders */
		gendersList = commonDataService.loadAllActiveGenders();
		log.info("gendersList size" + gendersList.size());
		/* Load All Active Marital Status */
		maritalStatusList = commonDataService.loadAllActiveMaritalStatus();
		log.info("maritalStatusList size" + maritalStatusList.size());
		/* Load All Active Religion */
		religionList = commonDataService.loadAllActiveReligions();
		log.info("religionList size" + religionList.size());
		/* Load All Active Castes */
		casteList = commonDataService.loadAllActiveCastes();
		log.info("casteList size" + casteList.size());
		/* Load All Active Qualifications */
		qualificationList = commonDataService.loadAllActiveQualifications();
		log.info("qualificationList size" + qualificationList.size());
		/* Load All Active Banks */
		bankList = commonDataService.loadAllActiveBanks();
		log.info("Bank Master size" + bankList.size());
		loadAllIdentificationType();
	}
	
	public void loadWeaversEnumsList() {
		log.info("<===== Start BaselineSurveyBean:loadWeaversEnumsList() ======>");
		familyMembersList=Arrays.asList(ClusterFamilyMembersEnum.values());
		log.info("<======== familyMembersList ==========> "+familyMembersList);
		weaversFamilyMembersList=Arrays.asList(WeaversFamilyMembersEnum.values());
		log.info("<======== weaversFamilyMembersList ==========> "+weaversFamilyMembersList);
		ancillaryWorkList=Arrays.asList(WeaversAncillaryWorkEnum.values());
		log.info("<======== ancillaryWorkList ===========>"+ancillaryWorkList);
		weaversWorkerList=Arrays.asList(WeaversWorkerEnum.values());
		log.info("<======== weaversWorkerList ===========>"+weaversWorkerList);
		weaversVehicleDetailsList=Arrays.asList(WeaversVehicleDetailsEnum.values());
		log.info("<========= weaversVehicleDetailsList ==========>"+weaversVehicleDetailsList);
		/* Weavers Professional Details Enum List */
		weaversWorkerTypeList=Arrays.asList(WeaversWorkerTypeEnum.values());
		log.info("<========= weaversWorkerTypeList ==========>"+weaversWorkerTypeList);
		weaversEngagedYearsList=Arrays.asList(WeaverEngagedYearsEnum.values());
		log.info("<========= weaversEngagedYearsList ==========>"+weaversEngagedYearsList);
		loomDesignCapacityEnumList=Arrays.asList(LoomDesignCapacityEnum.values());
		log.info("<========= loomDesignCapacityEnumList ==========>"+loomDesignCapacityEnumList);
		averageProductionPerDayList=Arrays.asList(AverageProductionPerDayEnum.values());
		log.info("<========= averageProductionPerDayList ==========>"+averageProductionPerDayList);
		earningsPerMonthList=Arrays.asList(EarningsPerMonthEnum.values());
		log.info("<========= earningsPerMonthList ==========>"+earningsPerMonthList);
		trainingTypeList=Arrays.asList(TrainingTypeEnum.values());
		log.info("<========= trainingTypeList ==========>"+trainingTypeList);
		productDemandList=Arrays.asList(ProductDemandEnum.values());
		log.info("<========= productDemandList ==========>"+productDemandList);
		loadAllLoomType();
		loadAllProductVarieties();
		/* Weavers Loom Activities */
		loomActivityEnumList=Arrays.asList(LoomActivityEnum.values());
		log.info("<========= loomActivityEnumList ==========>"+loomActivityEnumList);
		reviewedEmployeeList=new ArrayList<>();
		EmployeeMaster employeeMaster=loginBean.getEmployee();
		log.info("<===== login employeeMaster ====>"+employeeMaster.getId());
		Long entityId=employeeMaster.getPersonalInfoEmployment().getWorkLocation().getId();
		if(entityId!=null) {
			reviewedEmployeeList=commonDataService.getEmployeeMasterByEntityId(entityId);
			log.info("<===== login reviewedEmployeeList size ====>"+reviewedEmployeeList.size());
		}
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.CIRCULAR.toString());
		loadAllWeaverBenefitSchemes();
	}
	
	/* Load All Identification Type */
	public void loadAllIdentificationType() {
		log.info("<======== BaselineSurveyBean.loadAllIdentificationType() calling ======>");
		identificationList = new ArrayList<>();
		try {
			String url = PORTAL_SERVER_URL +"/identificationtype/getall";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				identificationList = mapper.readValue(jsonValue, new TypeReference<List<IdentificationType>>() {
				});
			}else {
				identificationList = new ArrayList<>();
			}
			log.info("identificationList size" + identificationList.size());
		} catch (Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadAllIdentificationType() ==========>" , e);
		}
	}
	
	/* Load All Weaver Benefit Schemes List */
	public void loadAllWeaverBenefitSchemes() {
		log.info("<======== BaselineSurveyBean.loadAllWeaverBenefitSchemes() calling ======>");
		weaverBenefitSchemeList = new ArrayList<>();
		try {
			String url = PORTAL_SERVER_URL +appPreference.getOperationApiUrl()+"/weaversbenefitscheme/getActiveWeaversBenefitScheme";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				weaverBenefitSchemeList = mapper.readValue(jsonValue, new TypeReference<List<WeaversBenefitScheme>>() {
				});
			}else {
				weaverBenefitSchemeList = new ArrayList<>();
			}
			log.info("weaverBenefitSchemeList size" + weaverBenefitSchemeList.size());
		} catch (Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadAllWeaverBenefitSchemes() ==========>" , e);
		}
	}
	
	
	/* Load All Product Variety Master List */
	public void loadAllProductVarieties() {
		log.info("<======== BaselineSurveyBean.loadAllProductVarieties() calling ======>");
		productList = new ArrayList<>();
		try {
			String url = PORTAL_SERVER_URL +"/productvariety/getallproductvarietys";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productList = mapper.readValue(jsonValue, new TypeReference<List<ProductVarietyMaster>>() {
				});
			}else {
				productList = new ArrayList<>();
			}
			log.info("productList size" + productList.size());
		} catch (Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadAllProductVarieties() ==========>" , e);
		}
	}
	
	/* Load All Loom Type */
	public void loadAllLoomType() {
		log.info("<======== BaselineSurveyBean.loadAllLoomType() calling ======>");
		loomTypeList = new ArrayList<>();
		try {
			String url = PORTAL_SERVER_URL +"/loomtype/getActiveLoomType";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				loomTypeList = mapper.readValue(jsonValue, new TypeReference<List<LoomTypeMaster>>() {
				});
			}else {
				loomTypeList = new ArrayList<>();
			}
			log.info("loomTypeList size" + loomTypeList.size());
		} catch (Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadAllLoomType() ==========>" , e);
		}
	}
	
	/* Load All Bank Branch By Bank Id */
	public void loadBankBranchByBankId() {
		log.info("<===== Start BaselineSurveyBean:loadBankBranchByBankId() ======>");
		bankBranchList=new ArrayList<>();
		try {
			Long bankId=bankMaster.getId();
			log.info("<======= bankId =========>"+bankId);
			if(bankId!=null) {
				bankBranchList=commonDataService.loadAllActiveBranchByBankId(bankId);
				log.info("bankBranchList size"+bankBranchList.size());
			}
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadBankBranchByBankId()==========>" , e);
		}
	}
	
	/* get Employee Designation By Employee Id */
	public void getEmpoyeeDesignation() {
		log.info("<======== BaselineSurveyBean.getEmpoyeeDesignation() calling ======>");
		try {
			Designation designation=baseLineSurveyDTO.getEmployeeMaster().getPersonalInfoEmployment().getDesignation();
			if(designation!=null) {
				baseLineSurveyDTO.setEmployeeDesignation(designation);
				log.info("Employee Designation=============>"+baseLineSurveyDTO.getEmployeeDesignation());
			}else {
				baseLineSurveyDTO.setEmployeeDesignation(null);
			}
		} catch (Exception e) {
			log.error("<========== Exception BaselineSurveyBean.getEmpoyeeDesignation() ==========>" , e);
		}
	}
	
	public String addAddress() {
		log.info("<===== Start BaselineSurveyBean:addAddress() ======>");
		try {
			stateList=commonDataService.getStateList();
			RequestContext.getCurrentInstance().execute("PF('addressdialog').show();");
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean:addAddress()==========>" , e);
		}
		return null;
	}

	/* load district By State Id */
	public void loadDistrictByState() {
		log.info("<===== Start BaselineSurveyBean:loadDistrictByState() ======>");
		districtList=new ArrayList<>();
		try {
			Long stateId=baseLineSurveyDTO.getAddressMaster().getStateMaster().getId();
			log.info("<======= stateId =========>"+stateId);
			if(stateId!=null) {
				districtList=commonDataService.loadDistrictByState(stateId);
				log.info("districtList size"+districtList.size());
			}
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadDistrictByState()==========>" , e);
		}
	}
	
	/* load city By District Id */
	public void loadCityByDistrict() {
		log.info("<===== Start BaselineSurveyBean:loadCityByDistrict() ======>");
		cityList=new ArrayList<>();talukList=new ArrayList<>();
		try {
			Long districtId=baseLineSurveyDTO.getAddressMaster().getDistrictMaster().getId();
			log.info("<======= districtId =========>"+districtId);
			if(districtId!=null) {
				cityList=commonDataService.loadActiveCitiesByDistrict(districtId);
				log.info("cityList size"+cityList.size());
				talukList=commonDataService.loadTalukByDistrict(districtId);
				log.info("talukList size"+talukList.size());
			}
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadCityByDistrict()==========>" , e);
		}
	}
	
	/* load area By City Id */
	public void loadActiveAreasByCity() {
		log.info("<===== Start BaselineSurveyBean:loadActiveAreasByCity() ======>");
		areaList=new ArrayList<>();
		try {
			Long cityId=baseLineSurveyDTO.getAddressMaster().getCityMaster().getId();
			log.info("<======= cityId =========>"+cityId);
			if(cityId!=null) {
				areaList=commonDataService.loadActiveAreasByCity(cityId);
				log.info("areaList size"+areaList.size());
			}
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.loadActiveAreasByCity()==========>" , e);
		}
	}
	
	public void saveAddress() {
		log.info("<===== Start BaselineSurveyBean:saveAddress() ======>");
		String address="";
		try {
			if(baseLineSurveyDTO.getAddressMaster()!=null) {
				address=addressMasterBean.prepareAddress(baseLineSurveyDTO.getAddressMaster());
				baseLineSurveyDTO.setWeaversAddress(address);
			}
			RequestContext.getCurrentInstance().execute("PF('addressdialog').hide();");	
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.saveAddress()==========>" , e);
		}
	}
	
	public String saveWeaverDetails() {
		log.info("<===== Start BaselineSurveyBean:saveWeaverPersonalDetails() ======>");
		try {
			if(baseLineSurveyDTO.getAddressMaster()!=null) {
				baseLineSurveyDTO.getWeaverRegister().setAddressMaster(baseLineSurveyDTO.getAddressMaster());
			}
			String url=PORTAL_SERVER_URL + appPreference.getOperationApiUrl()+ "/weaver/baselinesurvey/saveweaverdetails";
			BaseDTO response= httpService.post(url,baseLineSurveyDTO);
//			errorMap.notify(response.getStatusCode());
			/*if(response.getStatusCode()==0) {
				return null;
			}else {
				errorMap.notify(response.getStatusCode());
			}*/
			log.info("<=======Ends DepartmentBean.submit ======>");
		}catch(Exception e) {
			log.error("<========== Exception BaselineSurveyBean.saveWeaverPersonalDetails()==========>" , e);
		}
		return "";
	}
	
}
