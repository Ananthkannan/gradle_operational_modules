/**
 * 
 */
package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dnp.dto.DAndPProcurementPlanRequest;
import in.gov.cooptex.operation.dnp.dto.DAndPProcurementPlanResponse;
import in.gov.cooptex.operation.dnp.dto.DAndPProductVarietyFinalDTO;
import in.gov.cooptex.operation.dnp.model.DAndPProcurementPlan;
import in.gov.cooptex.operation.enums.DAndpProcurementPlanStatus;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import in.gov.cooptex.operation.production.model.RetailProductionPlanRegion;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Log4j2
@Service("dandPProcurementPlanBean")
@Scope("session")
public class DAndPProcurementPlanBean implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = -4534752375766357656L;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	DAndPProcurementPlan dnpProcurementPlan;

	@Getter
	@Setter
	List<RetailProductionPlan> retailProductionPlanFinalList;

	@Getter
	@Setter
	String planFromMonth = null;

	@Getter
	@Setter
	String planToMonth = null;

	@Getter
	@Setter
	Integer planFromYear = null;

	@Getter
	@Setter
	Integer planToYear = null;

	@Getter
	@Setter
	StringBuilder regionValues = null;

	@Getter
	@Setter
	StringBuilder productCategoryValues = null;

	@Getter
	@Setter
	Date planCreatedDate;

	@Getter
	@Setter
	String planCreatedBy;

	@Getter
	@Setter
	Date planApprovedDate;

	@Getter
	@Setter
	String planApprovedBy;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Setter
	@Getter
	List<EntityMaster> dnpOfficeList;

	@Setter
	@Getter
	String action = null;

	private final String CRATE_DANDP_WISE_PROCUREMENT_PLAN = "/pages/operation/retailproduction/dandp/createDnpProcurementPlan.xhtml?faces-redirect=true;";

	private final String LIST_DANDP_WISE_PROCUREMENT_PLAN = "/pages/operation/retailproduction/dandp/listDnpProcurementPlan.xhtml?faces-redirect=true;";

	private final String VIEW_DANDP_WISE_PROCUREMENT_PLAN = "/pages/operation/retailproduction/dandp/viewDnpProcurementPlan.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	DAndPProcurementPlanResponse dnpPlanResponse;

	@Getter
	@Setter
	LazyDataModel<DAndPProcurementPlanResponse> dnpPlanResponseList;

	@Getter
	@Setter
	Integer size = 0;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	String dnpInchargeName;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String currentPlanStatus;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	Map<String, List<DAndPProductVarietyFinalDTO>> dnpProductMap = new LinkedHashMap<String, List<DAndPProductVarietyFinalDTO>>();

	@Getter
	@Setter
	Map<String, Long> existingDnpProductMap = new LinkedHashMap<String, Long>();

	@Getter
	@Setter
	Map<String, Double> productPriceMap = new LinkedHashMap<>();

	@Getter
	@Setter
	List<DAndPProductVarietyFinalDTO> prodList = new ArrayList<DAndPProductVarietyFinalDTO>();

	@Getter
	@Setter
	Map<String, Double> totalFinalValueMap;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	boolean saveButtonFlag = false;

	@Getter
	@Setter
	boolean submitButtonFlag = false;

	@Getter
	@Setter
	Long dnpPlanId;

	Map<String, Double> productMap = new LinkedHashMap<>();

	public DAndPProcurementPlanBean() {
		log.info("<--- Inside DAndPMonthwiseRetailPlanBean() --->");
		loadValues();
	}

	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("<--- Exception at loadValues() ---> " + e.toString());
		}
	}

	public String createDnpMonthwiseForm() {
		saveButtonFlag = false;
		submitButtonFlag = false;
		log.info("<--- Inside createDnpMonthwiseForm() ---> " + action);
		retailProductionPlan = new RetailProductionPlan();
		planFromMonth = null;
		planFromYear = null;
		planToMonth = null;
		planToYear = null;
		planCreatedDate = null;
		planCreatedBy = null;
		planApprovedDate = null;
		planApprovedBy = null;

		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();

		dnpOfficeList = new ArrayList<>();
		dnpPlanResponse = null;
		dnpInchargeName = null;

		dnpProductMap = new LinkedHashMap<String, List<DAndPProductVarietyFinalDTO>>();
		prodList = new ArrayList<DAndPProductVarietyFinalDTO>();

		loadRetailProductionPlanFinalApprovedList();

		return CRATE_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public void loadRetailProductionPlanFinalApprovedList() {
		log.info("<--- Inside loadRetailProductionPlanFinalApprovedList() ---> ");

		// String requestPath = serverURL + appPreference.getOperationApiUrl() +
		// "/retail/plan/getfinalapprovedlist";
		String requestPath = serverURL + appPreference.getOperationApiUrl()
				+ "/retail/plan/getfinalapprovedlistwithoutdandpp";
		BaseDTO baseDTO = httpService.get(requestPath);
		try {
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailProductionPlanFinalList = (List<RetailProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<RetailProductionPlan>>() {
							});
					log.info("RetailProductionPlanList with Size of : " + retailProductionPlanFinalList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("<--- Exception in loadRetailProductionPlanFinalApprovedList() --->", e);
		}
	}

	public void onChangeProductionPlan() {
		log.info("<--- Inside onChangePropductionPlan() ---> ");

		dnpOfficeList = new ArrayList<>();
		entityMaster = null;
		dnpInchargeName = null;

		if (retailProductionPlan == null) {
			log.info("<--- if selected nothing clear all fields. Do this --->");
			retailProductionPlan = new RetailProductionPlan();
			planFromMonth = null;
			planFromYear = null;
			planToMonth = null;
			planToYear = null;
			regionValues = new StringBuilder();
			productCategoryValues = new StringBuilder();

			planCreatedDate = null;
			planCreatedBy = null;
			planApprovedDate = null;
			planApprovedBy = null;

			return;
		}
		getRegionandCategoryDetailsFromPlan(retailProductionPlan.getId());

		loadDnpOfficeListBasedOnPlan(retailProductionPlan.getId());
	}

	public void loadDnpOfficeListBasedOnPlan(Long planId) {
		log.info("<--- Inside loadDnpOfficeListBasedOnPlan() with plan Id---> " + planId);

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/getalldnpoffices/" + planId;
		log.info("<--- Get all available D & P offices for Plan URL ---> " + URL);
		try {
			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				dnpOfficeList = (List<EntityMaster>) mapper.readValue(jsonResponse,
						new TypeReference<List<EntityMaster>>() {
						});
				if (dnpOfficeList != null) {
					log.info("<--- dAndPOfficeList size ---> " + dnpOfficeList.size());
				} else {
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
				}
			}
		} catch (Exception e) {
			log.error("<--- Error in loadDnpOfficeListBasedOnPlan List --->", e);
		}
	}

	private void loadAllDnpOffices() {
		log.info("<--- Inside loadAllDnpOffices() with plan Id---> ");

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/get/getalldnpoffices";
		log.info("<--- Get all available D & P offices URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);
		try {
			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				dnpOfficeList = (List<EntityMaster>) mapper.readValue(jsonResponse,
						new TypeReference<List<EntityMaster>>() {
						});
				if (dnpOfficeList != null) {
					log.info("<--- dAndPOfficeList size ---> " + dnpOfficeList.size());
				} else {
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
				}
			}
		} catch (Exception e) {
			log.error("<--- Error in loadDnpOfficeListBasedOnPlan List --->", e);
		}
	}

	private void getRegionandCategoryDetailsFromPlan(Long planId) {
		log.info("<--- Inside getRegionandCategoryDetailsFromPlan() with plan ID---> " + planId);

		regionValues = new StringBuilder();
		productCategoryValues = new StringBuilder();

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/retail/plan/getplanbyid/" + planId;
			log.info("<--- getRegionandCategoryDetailsFromPlan() URL ---> " + URL);
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper objectMapper = new ObjectMapper();
			String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
			retailProductionPlan = objectMapper.readValue(jsonResponse, RetailProductionPlan.class);

			regionValues.setLength(0);
			log.info("<--- retailProductionPlan.getRetailProductionPlanRegionList() ---> "
					+ retailProductionPlan.getRetailProductionPlanRegionList());
			for (RetailProductionPlanRegion regionValue : retailProductionPlan.getRetailProductionPlanRegionList()) {
				if (regionValue != null && regionValue.getEntityMaster().getCode() != null
						&& regionValue.getEntityMaster().getName() != null)
					regionValues.append(regionValue.getEntityMaster().getCode()).append("/")
							.append(regionValue.getEntityMaster().getName()).append(",<br/>");
			}

			regionValues.deleteCharAt(regionValues.length() - 6);

			productCategoryValues.setLength(0);
			log.info("<--- retailProductionPlan.getProductCategoryList() ---> "
					+ retailProductionPlan.getProductCategoryList());
			for (ProductCategory categoryValue : retailProductionPlan.getProductCategoryList()) {
				if (categoryValue != null && categoryValue.getProductCatCode() != null
						&& categoryValue.getProductCatName() != null)
					productCategoryValues.append(categoryValue.getProductCatCode()).append("/")
							.append(categoryValue.getProductCatName()).append(",<br/>");
			}

			productCategoryValues.deleteCharAt(productCategoryValues.length() - 6);

			Date planForm = retailProductionPlan.getPlanFrom();
			Date planTo = retailProductionPlan.getPlanTo();

			planFromMonth = Util.getOnlyMonthFromDate(planForm);
			planToMonth = Util.getOnlyMonthFromDate(planTo);

			planFromYear = Util.getOnlyYearFromDate(planForm);
			planToYear = Util.getOnlyYearFromDate(planTo);

			planCreatedDate = retailProductionPlan.getCreatedDate();
			planCreatedBy = retailProductionPlan.getCreatedByName();
			planApprovedDate = retailProductionPlan.getApprovedDate();
			planApprovedBy = retailProductionPlan.getApprovedByEmployeeName();

		} catch (Exception e) {
			log.error("Exception at getRegionandCategoryDetailsFromPlan >>> ", e);
		}
	}

	private void getDnpOfficeById(Long dnpOfficeId) {
		log.info("<--- Inside getDnpOfficeById() with D&P Office ID---> " + dnpOfficeId);

		String URL = serverURL + "/entitymaster/get/" + dnpOfficeId;
		log.info("<--- generateProductVarietywiseProcuremnetPlan() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		try {
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					entityMaster = objectMapper.readValue(jsonResponse, EntityMaster.class);
					if (entityMaster.getEmployeeMaster() != null) {
						if (entityMaster.getEmployeeMaster().getLastName() != null) {
							dnpInchargeName = entityMaster.getEmployeeMaster().getFirstName() + " "
									+ entityMaster.getEmployeeMaster().getLastName();
						} else {
							dnpInchargeName = entityMaster.getEmployeeMaster().getFirstName();
						}
					}
					log.info(" entityMaster " + entityMaster);
					log.info(" dnpInchargeName " + dnpInchargeName);
				} else {
					log.error("<--- Entity Not Found ---> ");
					errorMap.notify(ErrorDescription.ERROR_ENTITY_NOTFOUND.getCode());
				}
			} else {
				log.error("<--- Internal Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("<--- Failure Response ---> ");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public void onChangeDnpOffice() {
		log.info("<--- Inside onChangeDnpOffice() ---> ");

		if (entityMaster == null) {
			log.info("<--- If D & P not selected do this ---> ");
			dnpInchargeName = null;
			return;
		}
		if (entityMaster != null && entityMaster.getEmployeeMaster() != null) {
			String empFirstName = entityMaster.getEmployeeMaster().getFirstName();
			String empLastName = entityMaster.getEmployeeMaster().getLastName();
			if (empLastName != null) {
				dnpInchargeName = empFirstName + " " + empLastName;
			} else {
				dnpInchargeName = empFirstName;
			}
			log.info("<---dnpInchargeName ---> " + dnpInchargeName);
		}
	}

	public void generateProductVarietywiseProcuremnetPlan() {
		log.info("<--- Inside generateProductVarietywiseProcuremnetPlan() ---> ");

		DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest();
		if (retailProductionPlan.getId() != null && entityMaster.getId() != null) {
			request.setProductionPlanId(retailProductionPlan.getId());
			request.setDnpOfficeId(entityMaster.getId());

			log.info("Plan ID : " + retailProductionPlan.getId());
			log.info("D & P Office ID : " + entityMaster.getId());
		}

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/generate";
		log.info("<--- generateProductVarietywiseProcuremnetPlan() URL ---> " + URL);

		try {
			BaseDTO baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					dnpPlanResponse = (DAndPProcurementPlanResponse) mapper.readValue(jsonResponse,
							DAndPProcurementPlanResponse.class);
					if (dnpPlanResponse != null && dnpPlanResponse.getDnpProductMap() != null
							&& dnpPlanResponse.getDnpProductMap().size() > 0) {

						dnpProductMap = dnpPlanResponse.getDnpProductMap();

						for (Map.Entry<String, List<DAndPProductVarietyFinalDTO>> entry : dnpProductMap.entrySet()) {
							prodList = entry.getValue();
							log.info("vavavav--------"+entry.getValue());
						}

						/*
						 * this map is for when the product is already created then get the sum of
						 * product_qty value formula : previous "product_qty" value from
						 * retail_production_plan_month_wise table - "sum(product_qty)" from
						 * retail_plan_dandp_month_wise
						 */
						/*
						 * if(dnpPlanResponse.getExistingDnpProductMap()!=null &&
						 * dnpPlanResponse.getExistingDnpProductMap().size()>0){ existingDnpProductMap =
						 * dnpPlanResponse.getExistingDnpProductMap(); }
						 */
						/*
						 * if any product which is available in multiple D & P offices we need give edit
						 * option to get the average price of the product
						 */
						/*for (String key : dnpProductMap.keySet()) {
							// here key contains productCode / productName / (true/false ie.to check whether
							// the product is eligible for edit or not)
							log.info(" Product key ::  " + key);
							if (key.split("@")[2].trim().equalsIgnoreCase("true")) {
								
							}
						}*/
						getRetailAveragePurhasePrice(dnpProductMap);
						/* to set the footer total value */
						prepareMapForFooterTotalValue(dnpProductMap);
					} else {
						log.info(
								"<--- No Mapping Products Found for the Selected Plan and Selected D & P Office ---> ");
						errorMap.notify(baseDTO.getStatusCode());
					}
				} else {
					log.info("<--- No Mapping Products Found for the Selected Plan and Selected D & P Office ---> ");
					errorMap.notify(baseDTO.getStatusCode());
				}
			} else {
				log.info("<--- Internal Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("<--- Error in generateProductVarietywiseProcuremnetPlan List --->", e);
		}
	}

	private void getRetailAveragePurhasePrice(Map<String, List<DAndPProductVarietyFinalDTO>> dnpProductMap) {

		DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest();
		List<String> productList = new ArrayList<>();

		Set<String> keySet = dnpProductMap.keySet();
		for (String key : keySet) {
			/*
			 * get the only the product which is eligible for to edit in the create page
			 * 
			 * productList is used to add all the products which are eligible for edit
			 * 
			 * based on product we will get <product(key),averageprice(value)>
			 */

			if (key.split("@")[2].trim().equalsIgnoreCase("true")) {
				productList.add(key.split("@")[0].trim());
			}
		}

		request.setProductList(productList);

		log.info("<--- product List  ---> " + productList);

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/getaverageprice";
		log.info("<--- getaverageprice URL ---> " + URL);
		BaseDTO baseDTO = httpService.post(URL, request);
		dnpPlanResponse = new DAndPProcurementPlanResponse();
		try {
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					dnpPlanResponse = (DAndPProcurementPlanResponse) mapper.readValue(jsonResponse,
							DAndPProcurementPlanResponse.class);
					if (dnpPlanResponse != null && dnpPlanResponse.getProductPriceMap() != null) {
						productPriceMap = dnpPlanResponse.getProductPriceMap();
						log.info("<--- Product Price Map From DB---> " + productPriceMap);
					} else {
						log.info("<--- Average Purchase Prise Not Available ---> ");
						errorMap.notify(ErrorDescription.ERROR_AVERAGE_PURCHASE_PRICE_NOT_AVAILABLE.getCode());
					}
				} else {
					log.info("<--- Average Purchase Prise Not Available ---> " + baseDTO.getStatusCode());
					errorMap.notify(ErrorDescription.ERROR_AVERAGE_PURCHASE_PRICE_NOT_AVAILABLE.getCode());
				}
			} else {
				log.info("<--- Internal Server Error ---> ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("<--- Error in generateProductVarietywiseProcuremnetPlan List --->", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	private void prepareMapForFooterTotalValue(Map<String, List<DAndPProductVarietyFinalDTO>> dnpProductMap) {

		List<DAndPProductVarietyFinalDTO> totalListFromMap = new ArrayList<>();

		for (Map.Entry<String, List<DAndPProductVarietyFinalDTO>> entry : dnpProductMap.entrySet()) {
			for (DAndPProductVarietyFinalDTO dto : entry.getValue()) {
				totalListFromMap.add(dto);
			}
		}

		totalFinalValueMap = totalListFromMap.stream()
				.collect(Collectors.groupingBy(DAndPProductVarietyFinalDTO::getMonth,
						Collectors.summingDouble(DAndPProductVarietyFinalDTO::getProductValue)));

	}

	public void editProductwiseQtyValue(String keyData, DAndPProductVarietyFinalDTO updatedProduct) {
		log.info("<--- Inside editProductwiseQtyValue()  keyData ---> " + keyData);
		saveButtonFlag = false;
		submitButtonFlag = false;
		Long existingDnpProductTotalQty = 0L;
		long updatedQuantity = 0;
		Double dnpwiseProductSum = 0D;
		if (dnpProductMap.containsKey(keyData)) {

			String productCode = keyData.split("@")[0].trim();

			if (existingDnpProductMap != null && existingDnpProductMap.size() > 0) {
				existingDnpProductTotalQty = existingDnpProductMap.get(productCode);
				log.info(
						"<--- existingDnpProductTotalQty from dnp_month_wise table ---> " + existingDnpProductTotalQty);
			}

			List<DAndPProductVarietyFinalDTO> updatedList = dnpProductMap.get(keyData);
			// previous total product quantity code/name/editable(true or false)/totalQty
			// from retail_production_plan_month_wise
			Double previousProductTotalQty = Double.valueOf(keyData.split("@")[3].trim());
			log.info("<--- previousPorductTotalQty ---> " + previousProductTotalQty);

			// user entered total product quantity
			if (updatedProduct != null && updatedProduct.getProductWiseTotalQty() != null) {
				updatedQuantity = updatedProduct.getProductWiseTotalQty().longValue();
				log.info("<--- updatedQuantity ---> " + updatedQuantity);
			} else {
				log.info("<--- Update Total Product Quantity With Valid Data ---> ");
				errorMap.notify(ErrorDescription.ERROR_UPDATED_TOTAL_PRODUCT_QUANTITY_WITH_VALID_DATA.getCode());
				return;
			}
			// existingDnpProductTotalQty-newchekqy+updatedQuantity >
			// previousPorductTotalQty

			if (productMap != null && productMap.size() > 0 && productMap.containsKey(keyData)) {
				dnpwiseProductSum = productMap.get(keyData);
			}
			// To check whether the Updated Quantity exceeding previous Quantity
			if ((existingDnpProductTotalQty - dnpwiseProductSum + updatedQuantity) > (previousProductTotalQty)) {
				saveButtonFlag = true;
				submitButtonFlag = true;
				log.info("<--- Updated Quantity Should not Exceed Previous Quantity. Available Qty is  ---> "
						+ ((previousProductTotalQty) - (existingDnpProductTotalQty - dnpwiseProductSum)));
				AppUtil.addError("Updated Quantity Should not Exceed Previous Quantity. Available Qty is :: "
						+ ((previousProductTotalQty) - (existingDnpProductTotalQty - dnpwiseProductSum)));
				return;
			}

			if (productPriceMap != null && productPriceMap.size() > 0) {
				Double averagePurchasePrice = productPriceMap.get(productCode);

				long updatedMonthwiseSum = 0;

				for (DAndPProductVarietyFinalDTO dto : updatedList) {

					dto.setProductQty(Math.round(updatedQuantity * (dto.getProcurementPercentage() / 100)));
					dto.setProductValue((dto.getProductQty()) * averagePurchasePrice);
					dto.setProductWiseTotalvalue(updatedQuantity * averagePurchasePrice);

					updatedMonthwiseSum = updatedMonthwiseSum + dto.getProductQty();
				}

				DAndPProductVarietyFinalDTO finaDto = updatedList.get(updatedList.size() - 1);

				log.info("<---  updatedMonthwiseSum ---> " + updatedMonthwiseSum);

				if (updatedQuantity > updatedMonthwiseSum) {
					log.info("<--- if(updatedMonthwiseSum > updatedQuantity) updatedList last month ---> ");

					updatedList.get(updatedList.size() - 1)
							.setProductQty(Math.round(updatedQuantity * finaDto.getProcurementPercentage() / 100)
									+ updatedQuantity - updatedMonthwiseSum);

					updatedList.get(updatedList.size() - 1)
							.setProductValue((finaDto.getProductQty()) * averagePurchasePrice);
					updatedList.get(updatedList.size() - 1)
							.setProductWiseTotalvalue(updatedQuantity * averagePurchasePrice);

				} else {
					log.info("<--- else ---> ");

					updatedList.get(updatedList.size() - 1)
							.setProductQty(Math.round(updatedQuantity * finaDto.getProcurementPercentage() / 100)
									- updatedMonthwiseSum - updatedQuantity);
					updatedList.get(updatedList.size() - 1)
							.setProductValue((finaDto.getProductQty()) * averagePurchasePrice);
					updatedList.get(updatedList.size() - 1)
							.setProductWiseTotalvalue(updatedQuantity * averagePurchasePrice);

				}

				dnpProductMap.put(keyData, updatedList);
			} else {
				log.info("<--- Average Purchase Prise Not Available ---> ");
				errorMap.notify(ErrorDescription.ERROR_AVERAGE_PURCHASE_PRICE_NOT_AVAILABLE.getCode());
			}

			/* to set the footer total value */
			prepareMapForFooterTotalValue(dnpProductMap);
		}

	}

	public String submitDnpProcurementPlan(String buttonName) {
		log.info("<--- Inside submitDnpProcuremnetPlan() ---> " + buttonName);
		log.info("<---Response ---> " + dnpPlanId);

		DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest();
		request.setDnpProductMap(dnpProductMap);
		request.setProductionPlanId(retailProductionPlan.getId());
		request.setDnpOfficeId(entityMaster.getId());
		request.setButtonName(buttonName);
		try {

			if (action.equalsIgnoreCase("ADD")) {

				String createPath = serverURL + appPreference.getOperationApiUrl() + "/dnp/create";
				log.info("<--- submitDnpProcuremnetPlan() create URL ---> " + createPath);

				BaseDTO baseDTO = httpService.post(createPath, request);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					if (buttonName.equalsIgnoreCase("INITIATED")) {
						log.info("<--- Retail Sales D & P Procuremnet Plan Saved Successfully");
						errorMap.notify(
								ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_SAVED_SUCCESSFULLY.getCode());
					} else if (buttonName.equalsIgnoreCase("SUBMITTED")) {
						log.info("<--- Retail Sales D & P Procuremnet Plan Submitted Successfully");
						errorMap.notify(ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_SUBMITTED_SUCCESSFULLY
								.getCode());
					}
					getDnpList();
				} else {
					String msg = baseDTO.getErrorDescription();
					errorMap.notify(baseDTO.getStatusCode());
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					return null;
				}

			} else if (action.equalsIgnoreCase("EDIT")) {

				request.setDnpId(dnpPlanId);
				// request.setDnpPlanStatus(dnpPlanResponse.getDnpPlanStatus().toString());

				String updatePath = serverURL + appPreference.getOperationApiUrl() + "/dnp/update";
				log.info("<--- submitDnpProcuremnetPlan() update URL ---> " + updatePath);

				BaseDTO baseDTO = httpService.post(updatePath, request);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {

					if (buttonName.equalsIgnoreCase("INITIATED")) {
						log.info("<--- Retail Sales D & P Procuremnet Plan Saved Successfully");
						errorMap.notify(
								ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_SAVED_SUCCESSFULLY.getCode());
					} else if (buttonName.equalsIgnoreCase("SUBMITTED")) {
						log.info("<--- Retail Sales D & P Procuremnet Plan Updated Successfully");
						errorMap.notify(
								ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_UPDATED_SUCCESSFULLY.getCode());
					}
					getDnpList();
				} else {
					String msg = baseDTO.getErrorDescription();
					errorMap.notify(baseDTO.getStatusCode());
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					return null;
				}

			}

		} catch (Exception exception) {
			log.error("<--- Error in submitDnpProcuremnetPlan() ---> ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public String editDnpProcurementPlanForm() {
		dnpPlanId = null;
		log.info("<--- Inside editDnpProcurementPlanForm()---> " + action);

		if (dnpPlanResponse == null) {
			log.info("<---Please Select any one Plan--->");
			Util.addWarn("Please Select any one Plan");
			return null;
		}

		if (!dnpPlanResponse.getDnpPlanStatus().toString().equals("INITIATED")
				&& !dnpPlanResponse.getDnpPlanStatus().toString().equals("REJECTED")) {
			log.info("D & P Procuemner Plan Cannot be Deleted.");

			String status = dnpPlanResponse.getDnpPlanStatus().toString().equalsIgnoreCase("APPROVED") ? "Approved"
					: "Submitted";

			Util.addWarn(status + " Plan Cannot be Edited");
			// errorMap.notify(ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_CANNOT_BE_DELETED.getCode());
			return null;
		}
		Long dnpId = dnpPlanResponse.getDnpId();
		Long productionPlanId = dnpPlanResponse.getProductionPlanId();
		Long dnpOfficeId = dnpPlanResponse.getDnpOfficeId();
		DAndpProcurementPlanStatus status = dnpPlanResponse.getDnpPlanStatus();

		log.info("<--- dnp Id---> " + dnpId);
		log.info("<--- Plan Id---> " + productionPlanId);
		log.info("<--- D&P Office Id---> " + dnpOfficeId);
		log.info("<--- D&P Statu---> " + dnpPlanResponse.getDnpPlanStatus());

		dnpPlanId = dnpId;

		getRegionandCategoryDetailsFromPlan(productionPlanId);
		loadRetailProductionPlanFinalApprovedList();

		getDnpOfficeById(dnpOfficeId);
		loadAllDnpOffices();

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/get/" + dnpId;
		log.info("<--- getByDnpId() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		try {

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				dnpPlanResponse = objectMapper.readValue(jsonResponse, DAndPProcurementPlanResponse.class);

				if (dnpPlanResponse != null && dnpPlanResponse.getDnpProductMap() != null
						&& dnpPlanResponse.getDnpProductMap().size() > 0) {

					dnpPlanResponse.setDnpId(dnpId);
					dnpPlanResponse.setDnpPlanStatus(status);
					dnpProductMap = dnpPlanResponse.getDnpProductMap();

					/*
					 * this list is to iterate the grid structure
					 */

					for (Map.Entry<String, List<DAndPProductVarietyFinalDTO>> entry : dnpProductMap.entrySet()) {
						prodList = entry.getValue();

						Double productwiseTotalQty = prodList.stream()
								.mapToDouble(DAndPProductVarietyFinalDTO::getProductQty).sum();
						Double productwiseTotalValue = prodList.stream()
								.mapToDouble(DAndPProductVarietyFinalDTO::getProductValue).sum();

						for (DAndPProductVarietyFinalDTO finalDto : prodList) {
							finalDto.setProductWiseTotalQty(productwiseTotalQty.longValue());
							finalDto.setProductWiseTotalvalue(productwiseTotalValue);
						}
						productMap.put(entry.getKey(), productwiseTotalQty);
					}

					if (dnpPlanResponse.getExistingDnpProductMap() != null
							&& dnpPlanResponse.getExistingDnpProductMap().size() > 0) {
						existingDnpProductMap = dnpPlanResponse.getExistingDnpProductMap();
					}

					/*
					 * if any product which is available in multiple D & P offices we need give edit
					 * option to get the average price of the product
					 */
					for (String key : dnpProductMap.keySet()) {
						// here key contains productCode / productName / (true/false ie.to check whether
						// the product is eligible for edit or not)
						if (key.split("@")[2].trim().equalsIgnoreCase("true")) {
							getRetailAveragePurhasePrice(dnpProductMap);
						}
					}

					/*
					 * To set the footer total value
					 */
					prepareMapForFooterTotalValue(dnpProductMap);

				}
			} else {
				log.info("<--- Retail Sales D & P Procurement Plan Not found ---> ");
				errorMap.notify(ErrorDescription.ERROR_RETAIL_SALES_DNP_PROCUREMENT_PLAN_NOT_FOUND.getCode());
			}
		} catch (Exception e) {
			log.error("Error in viewDnpProcuremnetPlan  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return CRATE_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public String viewDnpProcuremnetPlan() {
		log.info("<--- Inside viewDnpProcuremnetPlan() action---> " + action);

		if (dnpPlanResponse == null) {
			log.info("<---Please Select any one Plan--->");
			Util.addWarn("Please Select any one Plan");
			return null;
		}
		log.info("<--- Selected Dnp Plan Id---> " + dnpPlanResponse.getDnpId());
		log.info("<--- Selected Dnp Plan Status ---> " + dnpPlanResponse.getDnpPlanStatus());

		DAndpProcurementPlanStatus selectedPlanStatus = dnpPlanResponse.getDnpPlanStatus();
		Long dnpId = dnpPlanResponse.getDnpId();

		getRegionandCategoryDetailsFromPlan(dnpPlanResponse.getProductionPlanId());

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/get/" + dnpPlanResponse.getDnpId();
		log.info("<--- viewDnpProcuremnetPlan() URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);

		try {

			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				dnpPlanResponse = objectMapper.readValue(jsonResponse, DAndPProcurementPlanResponse.class);

				if (dnpPlanResponse != null && dnpPlanResponse.getDnpProductMap() != null
						&& dnpPlanResponse.getDnpProductMap().size() > 0) {

					dnpPlanResponse.setDnpPlanStatus(selectedPlanStatus);
					dnpPlanResponse.setDnpId(dnpId);

					dnpProductMap = dnpPlanResponse.getDnpProductMap();

					/*
					 * this list is to iterate the grid structure
					 */
					for (Map.Entry<String, List<DAndPProductVarietyFinalDTO>> entry : dnpProductMap.entrySet()) {
						prodList = entry.getValue();

						Double productwiseTotalQty = prodList.stream()
								.mapToDouble(DAndPProductVarietyFinalDTO::getProductQty).sum();
						Double productwiseTotalValue = prodList.stream()
								.mapToDouble(DAndPProductVarietyFinalDTO::getProductValue).sum();

						for (DAndPProductVarietyFinalDTO finalDto : prodList) {
							finalDto.setProductWiseTotalQty(productwiseTotalQty.longValue());
							finalDto.setProductWiseTotalvalue(productwiseTotalValue);
						}

					}

					/*
					 * To set the footer total value
					 */
					prepareMapForFooterTotalValue(dnpProductMap);

				}
			} else {
				log.info("<--- Retail Sales D & P Procurement Plan Not found ---> ");
				errorMap.notify(ErrorDescription.ERROR_RETAIL_SALES_DNP_PROCUREMENT_PLAN_NOT_FOUND.getCode());
			}
		} catch (Exception e) {
			log.error("Error in viewDnpProcuremnetPlan  ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return VIEW_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public String getDnpList() {
		log.info("<--- Inside getDnpMonthwisePlanList() ---> ");
		retailProductionPlan = new RetailProductionPlan();
		loadLazyDraftProductionList();
		loadStatusValues();
		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public void loadLazyDraftProductionList() {
		addButtonFlag = false;
		log.info("LoadLazyData");
		dnpPlanResponseList = new LazyDataModel<DAndPProcurementPlanResponse>() {

			private static final long serialVersionUID = 2784959485860775583L;

			@Override
			public List<DAndPProcurementPlanResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				List<DAndPProcurementPlanResponse> data = new ArrayList<DAndPProcurementPlanResponse>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<DAndPProcurementPlanResponse>>() {
					});
					log.info("data data>>>> " + data);
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
						log.info("<--- listSize --->  " + size);
					} else {
						size = 0;
						this.setRowCount(0);
						log.info("<--- No Records Found --->");
						errorMap.notify(baseDTO.getStatusCode());
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(DAndPProcurementPlanResponse res) {
				return res != null ? res.getDnpId() : null;
			}

			@Override
			public DAndPProcurementPlanResponse getRowData(String rowKey) {
				List<DAndPProcurementPlanResponse> responseList = (List<DAndPProcurementPlanResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (DAndPProcurementPlanResponse res : responseList) {
					if (res.getDnpId().longValue() == value.longValue()) {
						dnpPlanResponse = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();
		log.info("<---Inside search called--->");
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + ",  Value : " + entry.getValue().toString());
		}

		dnpPlanResponse = new DAndPProcurementPlanResponse();

		DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("productionPlanCodeOrName")) {
				log.info("productionPlanCodeOrName : " + value);
				request.setProductionPlanCodeOrName(value);
			}

			if (entry.getKey().equals("productionPlanFromDate")) {
				log.info("productionPlanFromDate : " + value);
				request.setProductionPlanFromDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("productionPlanToDate")) {
				log.info("productionPlanToDate : " + value);
				request.setProductionPlanToDate(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("dnpOfficeCodeOrName")) {
				log.info("dnpOfficeCodeOrName : " + value);
				request.setDnpOfficeCodeOrName(value);
			}

			if (entry.getKey().equals("dnpPlanStatus")) {
				log.info("dnpPlanStatus : " + value);
				request.setDnpPlanStatus(value);
			}
		}
		try {
			log.info("request " + request);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/search";
			log.info("Search URL " + URL);
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	private void loadStatusValues() {
		Object[] statusArray = DAndpProcurementPlanStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status);
		}

		log.info("<--- statusValues ---> " + statusValues);
	}

	public String clear() {
		log.info("<--- Inside clear() --->");
		dnpPlanResponse = new DAndPProcurementPlanResponse();
		getDnpList();
		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}
	
	public void addclear() {
		dnpOfficeList=new  ArrayList<EntityMaster>();
		dnpInchargeName=null;
	}

	public String cancel() {
		log.info("<--- Inside cancel() --->");
		getDnpList();
		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public void processDelete() {
		log.info("<---Inside processDelete()--->");
		if (dnpPlanResponse == null) {
			log.info("<---if(dnpPlanResponse == null)--->");
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			Util.addWarn("Please Select any one Plan");
			return;
		} else {
			log.info("<---D & P Procuremnet Plan Id--->" + dnpPlanResponse.getDnpId());
			if (!dnpPlanResponse.getDnpPlanStatus().toString().equals("INITIATED")
					&& !dnpPlanResponse.getDnpPlanStatus().toString().equals("REJECTED")) {
				log.info("D & P Procuemner Plan Cannot be Deleted.");

				String status = dnpPlanResponse.getDnpPlanStatus().toString().equalsIgnoreCase("APPROVED") ? "Approved"
						: "Submitted";

				Util.addWarn(status + " Plan Cannot be Deleted");

				// errorMap.notify(ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_CANNOT_BE_DELETED.getCode());
				return;
			}
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String confirmDelete() {
		log.info("<---Inside deleteAction called with Dnp Id---> " + dnpPlanResponse.getDnpId());
		String url = serverURL + appPreference.getOperationApiUrl() + "/dnp/delete/" + dnpPlanResponse.getDnpId();

		BaseDTO baseDTO = httpService.delete(url);

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("<--- D & P Procuemner Plan has been Deleted Successfully ---> ");
				errorMap.notify(ErrorDescription.INFO_RETAIL_SALES_DNP_PROCUREMENT_PLAN_DELETED_SUCCESSFULLY.getCode());
				getDnpList();
			} else {
				String msg = baseDTO.getErrorDescription();
				errorMap.notify(baseDTO.getStatusCode());
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				return null;
			}
		} else {
			log.info("<--- Internal Server Error ---> ");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current D & P Procurement Plan Status ---> " + currentPlanStatus);
		log.info("<--- D & P Id ---> " + dnpPlanResponse.getDnpId());
		log.info("<--- Comments ---> " + approveComments);
		log.info("<--- Comments ---> " + rejectComments);

		BaseDTO baseDTO = null;

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest(dnpPlanResponse.getDnpId(),
					currentPlanStatus, approveComments);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/approve";
			baseDTO = httpService.post(URL, request);
		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			DAndPProcurementPlanRequest request = new DAndPProcurementPlanRequest(dnpPlanResponse.getDnpId(),
					currentPlanStatus, rejectComments);
			String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/reject";
			baseDTO = httpService.post(URL, request);
		}

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}
		approveComments = null;
		rejectComments = null;

		return LIST_DANDP_WISE_PROCUREMENT_PLAN;
	}

	public void changeCurrentStatus(String value) {
		log.info("<---changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;

		approveComments = null;
		rejectComments = null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("D & P Procurement Plan onRowSelect method started");
		dnpPlanResponse = ((DAndPProcurementPlanResponse) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("D & P Procurement Plan onPageLoad method started");
		addButtonFlag = false;
	}

	public String authorization() throws IOException {
		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("RETAIL_DANDP_PROCUREMENT_PLAN_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have a  privilege for this page..................");
			FacesContext.getCurrentInstance().getExternalContext().redirect("/pages/dashboard.xhtml");
		}
		return null;
	}

}
