package in.gov.cooptex.operation.rest.util;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import lombok.Getter;
import lombok.Setter;

@Service("detectOSAndBrowser")
@Scope("session")
public class DetectOSAndBrowser {
	@Getter
	@Setter
	String osClass = null;

	@Getter
	@Setter
	String browserClass = null;

	@Getter
	@Setter
	String osBrowerClass = null;

	@Getter
	@Setter
	String windowsVersion = null;

	public void getOSAndBrowser() {
		HttpServletRequest httpServletRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
				.getExternalContext().getRequest();
		String browserDetails = httpServletRequest.getHeader("User-Agent");
		String userAgent = browserDetails;
		String user = userAgent.toLowerCase();
		String os = "";
		String browser = "";

		// =================OS=======================
		if (userAgent.toLowerCase().indexOf("windows") >= 0) {
			os = "windows";
			if (user.indexOf("windows nt 5.1") >= 0) {
				windowsVersion = "windows_xp";
			} else {
				windowsVersion = "windows_others";
			}
		} else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
			os = "mac";
		} else if (userAgent.toLowerCase().indexOf("x11") >= 0) {
			os = "unix";
		} else if (userAgent.toLowerCase().indexOf("android") >= 0) {
			os = "android";
		} else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
			os = "iphone";
		} else {
			os = "unknown";
		}

		// ===============Browser===========================
		if (user.contains("msie") || user.contains("edge")) {
			browser = "ie";

		} else if (user.contains("safari") && user.contains("version")) {
			browser = "safari";

		} else if (user.contains("opr") || user.contains("opera")) {
			browser = "opera";

		} else if (user.contains("chrome")) {
			browser = "chrome";

		} else if (user.indexOf("mozilla/7.0") > -1 || user.indexOf("netscape6") != -1
				|| user.indexOf("mozilla/4.7") != -1 || user.indexOf("mozilla/4.78") != -1
				|| user.indexOf("mozilla/4.08") != -1 || user.indexOf("mozilla/3") != -1) {

			browser = "netscape";

		} else if (user.contains("firefox")) {
			browser = "firefox";

		} else if (user.contains("rv")) {
			browser = "ie";
		} else {
			browser = "unknown";
		}

		osBrowerClass = os + "-" + browser;
		osClass = os;
		browserClass = browser;
	}

}
