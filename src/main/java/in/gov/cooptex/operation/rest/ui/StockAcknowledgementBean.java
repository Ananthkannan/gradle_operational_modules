package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.TaxTypes;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.SocietyProductAppraisal;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.TransportTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.ProductVarietyTax;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockacknowledgementbean")
@Scope("session")
public class StockAcknowledgementBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/productWarehouse/stockAcknowledgement/listStockAcknowledgement.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/productWarehouse/stockAcknowledgement/createStockAcknowledgement.xhtml?faces-redirect=true;";

	private final String PREVIEW_SOCIETY_STOCK_ACK_URL = "/pages/productWarehouse/stockAcknowledgement/previewStockAcknowledgement.xhtml?faces-redirect=true;";
	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	String pageaction;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer stockSize;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	private int taxCodeListSize;

	@PostConstruct
	private void init() {
		log.info("init is called.");
		stockAcknowledgementBeanListPage();
		List<String> taxCodeList = new ArrayList<>();
		Field[] interfaceFields = TaxTypes.class.getFields();
		for (Field taxCode : interfaceFields) {
			taxCodeList.add(taxCode.getName());
		}
		taxCodeListSize = taxCodeList == null ? 0 : taxCodeList.size();
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferResponse = new StockTransferResponse();
		stockAcknowledgementBeanListPage();
		addButtonFlag = false;
		editButtonFlag = true;
		taxApplicableFlag = true;
	}

	public String stockAcknowledgementBeanListPage() {
		log.info("stockAcknowledgementBeanListPage is called.");
		// SERVER_URL = stockTransferUtility.loadServerUrl();

		addButtonFlag = false;
		editButtonFlag = true;
		pageaction = "";
		taxApplicableFlag = true;
		stockTransferResponse = new StockTransferResponse();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		// stockTransferResponse = new StockTransferResponse();
		// addButtonFlag = false;

		loadLazyList();
		return LIST_PAGE_URL;
	}

	// List Page Method
	private void loadLazyList() {
		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();
				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {
				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}
		};
	}

	private BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stockacknowledgement/lazystockacksearchlist";
		baseDTO = httpService.post(URL, request);
		log.info("SocietyStockMovementBean.getSearchData Method Complted");

		return baseDTO;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("RetailProductionPlanBean onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		addButtonFlag = true;
		if (stockTransferResponse != null && stockTransferResponse.getStatus() != null
				&& stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
			editButtonFlag = false;
		} else if (stockTransferResponse != null && stockTransferResponse.getStatus() != null
				&& stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
			editButtonFlag = true;
		}
	}

	/**
	 * @return
	 */
	public String redirectSVInProgressPage() {
		try {
			Boolean stockVerificationIsInProgress = loginBean.getStockVerificationIsInProgess();
			if (stockVerificationIsInProgress != null && stockVerificationIsInProgress.booleanValue()) {
				loginBean.setMessage("Stock acknowledgement");
				loginBean.redirectSVInProgressPgae();
			}
		} catch (Exception ex) {
			log.error("Exception at redirectSVInProgressPage()", ex);
		}
		return null;
	}

	/**
	 * @return
	 */
	public String addStockAcknowledgement() {
		log.info("addStockAcknowledgement method start");
		redirectSVInProgressPage();
		clear();
		loadStockInwardFromList();
		log.info("addStockAcknowledgement method End");
		return ADD_PAGE_URL;
	}

	/**
	 * 
	 */
	private void clear() {
		log.info("clear method start");
		stockMovementNumberList = new ArrayList<StockTransfer>();
		stockInwardFromList = new ArrayList<SupplierMaster>();
		selectedStockInwardFrom = new SupplierMaster();
		stockTransferMovementList = new ArrayList<StockTransfer>();
		selectedStockMovementObj = new StockTransfer();
		stockTransfer = new StockTransfer();
		taxApplicableFlag = true;
	}

	// Stock Inward From
	@Getter
	@Setter
	List<SupplierMaster> stockInwardFromList;

	@Getter
	@Setter
	List<StockTransfer> stockMovementNumberList;

	@Getter
	@Setter
	SupplierMaster selectedStockInwardFrom;

	@Getter
	@Setter
	SocietyProductAppraisal societyProductAppraisal = new SocietyProductAppraisal();

	public void loadStockInwardFromList() {
		log.info("loadActiveProductwarehouseList method start");
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockacknowledgement/loadstockinwardfromlist";

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockInwardFromList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
				});
			} else {
				log.warn("loadActiveProductwarehouseList not success error in service side *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in loadActiveProductwarehouseList ", ee);
		}

	}

	@Autowired
	CommonDataService commonDataService;

	public void onChangeStockTransferFrom() {
		log.info("onChangeStockTransferFrom " + selectedStockInwardFrom);
		try {
			stockTransferMovementList = new ArrayList<>();
			selectedStockMovementObj = new StockTransfer();
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockacknowledgement/loadstockmovementlist/"
					+ (selectedStockInwardFrom != null ? selectedStockInwardFrom.getId() : 0);

			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				log.warn("entityMasterList Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferMovementList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
				});
			} else {
				log.warn("onChangeStockTransferFrom not success error in service side *** :");
			}
		} catch (Exception e) {
			log.error("onChangeStockTransferFrom exception----", e);
		}

	}

	@Getter
	@Setter
	StockTransfer selectedStockMovementObj;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsActualList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsPopupdisplayList;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Getter
	@Setter
	List<StockTransfer> stockTransferMovementList;

	@Getter
	@Setter
	Boolean disableBtnSearch;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsMovementList;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDlgDisplay;

	@Getter
	@Setter
	Boolean taxApplicableFlag = true;

	@Getter
	@Setter
	ProductVarietyMaster selectedProductVarietyMaster = new ProductVarietyMaster();

	public void onSelectStockTransfer() {
		if (selectedStockMovementObj != null) {
			disableBtnSearch = false;
		} else {
			disableBtnSearch = true;
		}
	}

	public String searchSocietyStockMovement() {
		log.info("start searchSocietyStockMovement.....");
		stockTransfer = new StockTransfer();
		StockTransfer stockTransferActual = new StockTransfer();
		stockTransferItemsActualList = new ArrayList<StockTransferItems>();
		taxApplicableFlag = true;
		try {

			if (selectedStockMovementObj == null || selectedStockMovementObj.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			stockTransferActual = commonDataService.getStockTransferById(selectedStockMovementObj.getId());
			if (stockTransferActual != null) {
				// SupplierMaster supplier = stockTransferActual.getSupplierMaster();
				// if(supplier != null) {
				// Boolean taxExemptedFlag = supplier.getTaxExempted();
				// taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
				// if(taxExemptedFlag) {
				// taxApplicableFlag = false;
				// } else {
				// taxApplicableFlag = true;
				// }
				// } else {
				// taxApplicableFlag = true;
				// }

				Boolean invoiceTaxExemptedFlag = stockTransferActual.getTaxExempted();
				invoiceTaxExemptedFlag = invoiceTaxExemptedFlag == null ? false : invoiceTaxExemptedFlag;
				if (invoiceTaxExemptedFlag) {
					taxApplicableFlag = false;
				} else {
					taxApplicableFlag = true;
				}

			}
			stockTransferItemsActualList = stockTransferActual != null ? stockTransferActual.getStockTransferItemsList()
					: null;
			stockTransfer = getStockTranferById(selectedStockMovementObj.getId());
			if (stockTransfer != null) {

				// stockTransferResponse = new StockTransferResponse();

				stockTransfer.setStockTransfer(new StockTransfer());
				stockTransfer.getStockTransfer().setId(stockTransfer.getId());
				stockTransfer.setId(null);
				stockTransfer.setReferenceNumber(null);
				stockTransfer.setDateReceived(new Date());
				stockTransfer.setIsReceived(true);
				stockTransfer.setStatus(StockTransferStatus.INITIATED);
				stockTransfer.setTransferType(StockTransferType.WAREHOUSE_INWARD);

				/*
				 * if (stockTransfer.getId() == null) {
				 * stockTransfer.setStockTransferItemsList(new ArrayList<>()); }
				 */

				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {

						// StockTransferItems stockTransferItemsObj = new StockTransferItems();
						/*
						 * stockTransferItemsObj
						 * .setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster()
						 * );
						 */

						if (stockTransferItemslocalObj.getUomMaster() == null) {
							Util.addWarn("UOM is Empty");
							// stockTransferItemsObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							log.info("UOM is not Empty" + stockTransferItemslocalObj.getUomMaster().getCode());
						}
						stockTransferItemslocalObj.setItemTotal(stockTransferItemslocalObj.getUnitRate()
								* stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						stockTransferItemslocalObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
						stockTransferItemslocalObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItems.setDescription(description);
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {

							Double cgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							stockTransferItemslocalObj.setTaxValue(totalTax);

							stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							loadProductVerietyTaxPercentage();
							stockTransferItemslocalObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									+ stockTransferItemslocalObj.getTaxValue());
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}

			stockTransferItemsActualList = calculateStockTranferItemList(stockTransferItemsActualList);
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "";
	}

	/*
	 * private double roundOffTaxValue(double taxV) { Double taxRoundOff =
	 * Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxV));
	 * log.info("tax round off value----" + taxRoundOff); String str =
	 * String.valueOf(taxRoundOff); String[] convert = str.split("\\."); int length
	 * = convert[1].length(); double taxValue = 0.0; if (length == 2) { int
	 * splitValue = Integer.valueOf(convert[1].substring(1));
	 * log.info("split up value----------" + splitValue); if (splitValue % 2 == 0) {
	 * log.info("splitValue is even-----"); taxValue = taxRoundOff; } else {
	 * log.info("splitValue is odd-----"); taxValue = taxRoundOff + 0.01; taxValue =
	 * Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue)); } } else { taxValue
	 * = taxRoundOff; } return taxValue; }
	 */

	public void loadProductVerietyTaxPercentage() {
		log.info("loadProductVerietyTaxPercentage :: start");
		try {

			if (societyProductAppraisal != null) {
				if (societyProductAppraisal.getProductCost() != null && stockTransferItems.getUnitRate() != null) {
					if (!societyProductAppraisal.getProductCost().equals(stockTransferItems.getUnitRate())) {
						stockTransferItems.setUnitRate(null);
						AppUtil.addError(" Product Cost And Unit Rate In Item Receive Details Should be Same");
						return;
					}
				}
			}

			Double totalTax = 0.0;
			if (selectedProductVarietyMaster.getId() == null) {
				// AppUtil.addError("Please select Product Variety Master");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_DETAILS_EMPTY).getCode());
				return;
			}
			if (stockTransferItems.getUnitRate() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.UNIT_RATE_SHOULD_NOT_EMPTY).getCode());
				return;
			}
			Long ProductId = selectedProductVarietyMaster.getId();
			Double unitRate = stockTransferItems.getUnitRate();

			log.info("loadProductVerietyTaxPercentage :: ProductId==> " + ProductId);
			log.info("loadProductVerietyTaxPercentage :: unitRate==> " + unitRate);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/productVarietyTaxControler/gettaxlistbyproductidunitrate/" + ProductId + "/" + unitRate;
			log.info("loadProductVerietyTaxPercentage :: url==> " + url);
			BaseDTO response = httpService.get(url);

			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());

				List<ProductVarietyTax> proTaxList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyTax>>() {
						});
				if (proTaxList != null && !proTaxList.isEmpty()) {
					log.info("proTaxList size==> " + proTaxList.size());
					for (ProductVarietyTax proTax : proTaxList) {
						log.info("tax==> " + proTax.getTaxPercent());
						if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("SGST")) {
							stockTransferItems.setSgstTaxPercent(proTax.getTaxPercent());
						} else if (proTax.getTaxMaster() != null
								&& proTax.getTaxMaster().getTaxCode().equalsIgnoreCase("CGST")) {
							stockTransferItems.setCgstTaxPercent(proTax.getTaxPercent());
						}

						totalTax = totalTax + proTax.getTaxPercent();

					}
				} else {
					totalTax = 0.0;
					log.error("proTaxList is null or empty");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
				}
				log.info("totalTax==> " + totalTax);
				stockTransferItems.setTaxPercent(totalTax);
			} else {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_TAX_IS_EMPTY).getCode());
			}

		} catch (Exception se) {
			log.error("Set PurchaseOrderUpdate Get Error " + se);
		}
	}

	public void viewItemDetailsDlg(StockTransferItems items) {
		stockTransferItemsDlgDisplay = items;
	}

	public Double getCgstValue() {
		Double cgstValue = 0.0;
		try {
			if (stockTransfer.getStockTransferItemsList() != null
					&& stockTransfer.getStockTransferItemsList().size() > 0) {

				for (StockTransferItems stockTransferItemsObj : stockTransfer.getStockTransferItemsList()) {
					Double cgstPercentage = stockTransferItemsObj.getCgstTaxPercent();
					Double total = stockTransferItemsObj.getItemTotal();
					total = total == null ? 0D : total;
					log.info("getCgstValue() - total: " + total);
					cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
					Double cgstValueTemp = total * (cgstPercentage / 100);
					cgstValueTemp = cgstValueTemp == null ? 0D : cgstValueTemp;

					/**
					 * Don't round off. Trim after two decimal.
					 */
					cgstValueTemp = AppUtil.getTrimmedValue(cgstValueTemp);
					log.info("getSgstValue() - cgstValueTemp: " + cgstValueTemp);

					cgstValue = cgstValue + cgstValueTemp;
				}
				log.info("getCgstValue() - cgstValue: " + cgstValue);

			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return cgstValue;
	}

	public Double getSgstValue() {
		Double sgstValue = 0.0;
		try {
			if (stockTransfer.getStockTransferItemsList() != null
					&& stockTransfer.getStockTransferItemsList().size() > 0) {
				for (StockTransferItems stockTransferItemsObj : stockTransfer.getStockTransferItemsList()) {
					Double sgstPercentage = stockTransferItemsObj.getSgstTaxPercent();
					Double total = stockTransferItemsObj.getItemTotal();
					total = total == null ? 0D : total;
					log.info("getSgstValue() - total: " + total);
					sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
					Double sgstValueTemp = total * (sgstPercentage / 100);
					sgstValueTemp = sgstValueTemp == null ? 0D : sgstValueTemp;

					/**
					 * Don't round off. Trim after two decimal.
					 */
					sgstValueTemp = AppUtil.getTrimmedValue(sgstValueTemp);
					log.info("getSgstValue() - sgstValueTemp: " + sgstValueTemp);

					sgstValue = sgstValue + sgstValueTemp;
				}
				log.info("getSgstValue() - sgstValue: " + sgstValue);
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return sgstValue;
	}

	public String saveStockTransfer() {
		log.info("saveStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		if (validateStockTransfer()) {
			// clearBundleDetails();
			return createstockackinward();
		} else {
			return null;
		}
	}

	public String submitStockTransfer() {
		log.info("submitStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		log.info("stocktransfer item list---------" + stockTransfer.getStockTransferItemsList().size());
		if (validateStockTransfer()) {
			// clearBundleDetails();
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			return null;
		} else {
			return null;
		}
	}

	private boolean validateStockTransfer() {

		if (stockTransfer.getStockTransferItemsList() == null || stockTransfer.getStockTransferItemsList().isEmpty()) {
			errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_ITEMS_REQUIRED.getCode());
			return false;
		}

		return true;
	}

	public String createstockackinward() {
		log.info("createstockackinward method is executing..");
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stockacknowledgement/createstockackinward";
		try {
			stockTransfer.setStockTransferItemsList(new ArrayList<StockTransferItems>());
			/*
			 * for(StockTransferItems stockTransferitems :stockTransferItemsActualList) {
			 * log.info(stockTransferitems.getAtNumber());
			 * log.info(stockTransferitems.getDispatchedQty());
			 * log.info(stockTransferitems.getReceivedQty());
			 * log.info(stockTransferitems.getProductVarietyMaster().getCode()); }
			 */
			if (stockTransferItemsActualList != null && stockTransferItemsActualList.size() > 0) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.00");
				for (StockTransferItems stockTransferItemslocalObj : stockTransferItemsActualList) {
					stockTransferItemslocalObj.setItemTotal(
							stockTransferItemslocalObj.getUnitRate() * stockTransferItemslocalObj.getReceivedQty());
					stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
					double total = stockTransferItemslocalObj.getItemTotal();
					if (stockTransferItemslocalObj.getDiscountPercent() != null
							&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
						double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
						stockTransferItemslocalObj.setDiscountValue(discountV);
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
								- stockTransferItemslocalObj.getDiscountValue());
					}
					if (stockTransferItemslocalObj.getTaxPercent() != null
							&& stockTransferItemslocalObj.getTaxPercent() > 0) {

						Double cgstPercentage = stockTransferItemslocalObj.getCgstTaxPercent();
						cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
						Double cgstValue = total * (cgstPercentage / 100);
						cgstValue = cgstValue == null ? 0D : cgstValue;
						/**
						 * Don't round off. Trim after two decimal.
						 */
						cgstValue = AppUtil.getTrimmedValue(cgstValue);

						Double sgstPercentage = stockTransferItemslocalObj.getSgstTaxPercent();
						sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
						Double sgstValue = total * (sgstPercentage / 100);
						sgstValue = sgstValue == null ? 0D : sgstValue;
						/**
						 * Don't round off. Trim after two decimal.
						 */
						sgstValue = AppUtil.getTrimmedValue(sgstValue);

						Double totalTax = Double.sum(cgstValue, sgstValue);
						stockTransferItemslocalObj.setTaxValue(totalTax);

						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
								+ stockTransferItemslocalObj.getTaxValue());
					}
				}
			}
			stockTransfer.setStockTransferItemsList(stockTransferItemsActualList);
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
					RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					RequestContext.getCurrentInstance().execute("PF('statusDialog').hide();");
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public String getStockAcknowledgementDetailBystockID() {
		log.info("getStockAcknowledgementDetailBystockID method start");
		clear();

		log.info("getStockAcknowledgementDetailBystockID method End");
		return ADD_PAGE_URL;
	}

	public String validateCurrentReceivedQty(StockTransferItems stockTransferItemsObj) {
		if (stockTransferItemsObj != null && stockTransferItemsObj.getDispatchedQty() != null
				&& stockTransferItemsObj.getReceivedQty() != null) {
			log.info("DispatchedQty() " + stockTransferItemsObj.getDispatchedQty());
			log.info("ReceivedQty() " + stockTransferItemsObj.getReceivedQty());
			if (stockTransferItemsObj.getReceivedQty() <= 0) {
				// errorMap.notify(OperationErrorCode.SOCIETY_STOCK_ACK_RECEIVED_ZERO);
				stockTransferItemsObj.setReceivedQty(0D);
			} else if (stockTransferItemsObj.getDispatchedQty() < stockTransferItemsObj.getReceivedQty()) {
				errorMap.notify(OperationErrorCode.SOCIETY_STOCK_ACK_RECEIVED_MORE_THAN_AVAILABLILITY);
				stockTransferItemsObj.setReceivedQty(null);
			}

			stockTransferItemsActualList.stream().forEach(items -> {
				if (items.getAtNumber().equalsIgnoreCase(stockTransferItemsObj.getAtNumber())) {
					items.setReceivedQty(stockTransferItemsObj.getReceivedQty());
				}
			});

			stockTransferItemsObj.setItemTotal(stockTransferItemsObj.getUnitRate()
					* (stockTransferItemsObj.getReceivedQty() != null ? stockTransferItemsObj.getReceivedQty() : 0));
			stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemTotal());
			// stockTransferItems.setDescription(description);
			Double total = stockTransferItemsObj.getItemTotal();
			if (stockTransferItemsObj.getDiscountPercent() != null && stockTransferItemsObj.getDiscountPercent() > 0
					&& total != null) {
				double discountV = (total * stockTransferItemsObj.getDiscountPercent()) / 100;
				stockTransferItemsObj.setDiscountValue(discountV);
				stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemNetTotal()
						- (stockTransferItemsObj.getDiscountValue() != null ? stockTransferItemsObj.getDiscountValue()
								: 0));
			}
			DecimalFormat decimalFormat = new DecimalFormat("#0.00");
			decimalFormat.setRoundingMode(RoundingMode.DOWN);
			if (stockTransferItemsObj.getTaxPercent() != null && stockTransferItemsObj.getTaxPercent() > 0
					&& total != null) {
				double tax = stockTransferItemsObj.getTaxPercent();
				double taxV = (total * tax) / 100;
				log.info("tax value----" + taxV);
				stockTransferItemsObj.setTaxValue(taxV);
				double taxValue = AppUtil.roundOffTaxValue(taxV);
				stockTransferItemsObj.setTaxValue(taxValue);
				// stockTransferItems.setTaxPercent(stockTransferItems.getTaxPercent());
				stockTransferItemsObj.setItemNetTotal(stockTransferItemsObj.getItemNetTotal()
						+ (stockTransferItemsObj.getTaxValue() != null ? stockTransferItemsObj.getTaxValue() : 0));

				if ((stockTransferItemsObj.getCgstTaxPercent() == null
						&& stockTransferItemsObj.getSgstTaxPercent() == null)
						|| (stockTransferItemsObj.getCgstTaxPercent() == 0.0
								&& stockTransferItemsObj.getSgstTaxPercent() == 0.0)) {
					stockTransferItems = new StockTransferItems();
					selectedProductVarietyMaster = (stockTransferItemsObj.getProductVarietyMaster());
					stockTransferItems.setUnitRate(stockTransferItemsObj.getUnitRate());
					societyProductAppraisal = new SocietyProductAppraisal();
					loadProductVerietyTaxPercentage();
					stockTransferItemsObj.setCgstTaxPercent(
							stockTransferItems.getCgstTaxPercent() != null ? stockTransferItems.getCgstTaxPercent()
									: 0);
					stockTransferItemsObj.setSgstTaxPercent(
							stockTransferItems.getSgstTaxPercent() != null ? stockTransferItems.getSgstTaxPercent()
									: 0);
				}
			}
		}
		return null;
	}

	@Getter
	@Setter
	TransportTypeMaster selectedTransportTypeMaster;

	@Getter
	@Setter
	String purchaseOrderNumber;

	@Getter
	@Setter
	Integer invoiceNumber;

	@Getter
	@Setter
	Date invoiceDate;

	public String viewSocietyStockAckInward() {
		log.info("start viewSocietyStockMovement:");
		stockTransfer = new StockTransfer();
		taxApplicableFlag = true;
		// pageaction = "VIEW";
		try {

			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			// stockTransfer =
			// commonDataService.getStockTransferById(stockTransferResponse.getId());
			stockTransfer = getStockTranferById(stockTransferResponse.getId());
			if (stockTransfer != null) {

				// SupplierMaster supplier = stockTransfer.getSupplierMaster();
				// if (supplier != null) {
				// Boolean taxExemptedFlag = supplier.getTaxExempted();
				// taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
				// if (taxExemptedFlag) {
				// taxApplicableFlag = false;
				// } else {
				// taxApplicableFlag = true;
				// }
				// } else {
				// taxApplicableFlag = true;
				// }

				Boolean invoiceTaxExemptedFlag = stockTransfer.getTaxExempted();
				invoiceTaxExemptedFlag = invoiceTaxExemptedFlag == null ? false : invoiceTaxExemptedFlag;
				if (invoiceTaxExemptedFlag) {
					taxApplicableFlag = false;
				} else {
					taxApplicableFlag = true;
				}

				stockTransferItemsMovementList = new ArrayList<>();
				stockTransferResponse = new StockTransferResponse();
				invoiceNumber = (stockTransfer.getPurchaseInvoiceNumber() != null
						? Integer.parseInt(stockTransfer.getPurchaseInvoiceNumber())
						: null);
				invoiceDate = stockTransfer.getInvoiceDate();
				selectedTransportTypeMaster = stockTransfer.getTransportMaster() != null
						? stockTransfer.getTransportMaster().getTransportTypeMaster()
						: null;

				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {
						stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getPurchasePrice());
						// StockTransferItems stockTransferItemsObj = new StockTransferItems();
						stockTransferItemslocalObj
								.setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster());

						if (stockTransferItemslocalObj.getUomMaster() != null) {
							stockTransferItemslocalObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							Util.addWarn("UOM is Empty");
						}
						stockTransferItemslocalObj.setItemTotal((stockTransferItemslocalObj.getUnitRate() != null
								? stockTransferItemslocalObj.getUnitRate()
								: 0)
								* (stockTransferItemslocalObj.getReceivedQty() != null
										? stockTransferItemslocalObj.getReceivedQty()
										: 0));
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						// stockTransferItemslocalObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getReceivedQty());
						// stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItemslocalObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItems.setDescription(description);
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {

							Double cgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							stockTransferItemslocalObj.setTaxValue(totalTax);

							stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							loadProductVerietyTaxPercentage();
							stockTransferItemslocalObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									+ stockTransferItemslocalObj.getTaxValue());
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return PREVIEW_SOCIETY_STOCK_ACK_URL;
	}

	public String deleteConfirmation() {
		log.info("starts deleteConfirmation");
		try {
			pageaction = "DELETE";
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('dlg1').show();");
			}
			log.info("ends deleteConfirmation");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteSocietyStockInward() {
		log.info("starts deleteSocietyStockInward");
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/delete/"
				+ stockTransferResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(URL);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode().equals(ErrorDescription.SUCCESS_RESPONSE.getCode())) {
					log.info("Deleted successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
					addButtonFlag = false;
					log.info("ends deleteSocietyStockOutward");
					return stockAcknowledgementBeanListPage();
				} else {
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
			RequestContext context = RequestContext.getCurrentInstance();
			// context.execute("PF('statusDialog').hide();");
			context.execute("PF('dlg1').hide();");
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		log.info("ends deleteSocietyStockInward");
		return null;
	}

	public String editSocietyStockAckInward() {
		log.info("start editSocietyStockAckInward:");
		stockTransfer = new StockTransfer();
		StockTransfer stockTransferActual = new StockTransfer();
		stockTransferItemsActualList = new ArrayList<StockTransferItems>();
		try {

			redirectSVInProgressPage();

			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			if (stockTransferResponse == null || stockTransferResponse.getStatus() == null
					|| stockTransferResponse.getStatus().equals(StockTransferStatus.SUBMITTED)) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			// stockTransfer =
			// commonDataService.getStockTransferById(stockTransferResponse.getId());
			stockTransferActual = commonDataService.getStockTransferById(stockTransferResponse.getId());

			if (stockTransferActual != null) {
				// SupplierMaster supplier = stockTransferActual.getSupplierMaster();
				// if (supplier != null) {
				// Boolean taxExemptedFlag = supplier.getTaxExempted();
				// taxExemptedFlag = taxExemptedFlag == null ? false : taxExemptedFlag;
				// if (taxExemptedFlag) {
				// taxApplicableFlag = false;
				// } else {
				// taxApplicableFlag = true;
				// }
				// } else {
				// taxApplicableFlag = true;
				// }

				Boolean invoiceTaxExemptedFlag = stockTransferActual.getTaxExempted();
				invoiceTaxExemptedFlag = invoiceTaxExemptedFlag == null ? false : invoiceTaxExemptedFlag;
				if (invoiceTaxExemptedFlag) {
					taxApplicableFlag = false;
				} else {
					taxApplicableFlag = true;
				}

			}
			stockTransferItemsActualList = stockTransferActual != null ? stockTransferActual.getStockTransferItemsList()
					: null;
			stockTransfer = getStockTranferById(stockTransferResponse.getId());
			if (stockTransfer != null) {
				stockTransferItemsMovementList = new ArrayList<>();
				stockTransferResponse = new StockTransferResponse();
				invoiceNumber = (stockTransfer.getPurchaseInvoiceNumber() != null
						? Integer.parseInt(stockTransfer.getPurchaseInvoiceNumber())
						: null);
				invoiceDate = stockTransfer.getInvoiceDate();
				selectedTransportTypeMaster = stockTransfer.getTransportMaster() != null
						? stockTransfer.getTransportMaster().getTransportTypeMaster()
						: null;
				DecimalFormat decimalFormat = new DecimalFormat("#0.00");
				decimalFormat.setRoundingMode(RoundingMode.DOWN);
				if (stockTransfer.getStockTransferItemsList() != null
						&& stockTransfer.getStockTransferItemsList().size() > 0) {
					for (StockTransferItems stockTransferItemslocalObj : stockTransfer.getStockTransferItemsList()) {

						// StockTransferItems stockTransferItemsObj = new StockTransferItems();
						stockTransferItemslocalObj
								.setProductVarietyMaster(stockTransferItemslocalObj.getProductVarietyMaster());

						if (stockTransferItemslocalObj.getUomMaster() != null) {
							stockTransferItemslocalObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
						} else {
							Util.addWarn("UOM is Empty");
						}
						stockTransferItemslocalObj.setItemTotal((stockTransferItemslocalObj.getUnitRate() != null
								? stockTransferItemslocalObj.getUnitRate()
								: 0)
								* (stockTransferItemslocalObj.getReceivedQty() != null
										? stockTransferItemslocalObj.getReceivedQty()
										: 0));
						stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
						// stockTransferItemslocalObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
						stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getReceivedQty());
						// stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItemslocalObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
						// stockTransferItems.setDescription(description);
						double total = stockTransferItemslocalObj.getItemTotal();
						if (stockTransferItemslocalObj.getDiscountPercent() != null
								&& stockTransferItemslocalObj.getDiscountPercent() > 0) {
							double discountV = (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
							stockTransferItemslocalObj.setDiscountValue(discountV);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									- stockTransferItemslocalObj.getDiscountValue());
						}

						if (stockTransferItemslocalObj.getTaxPercent() != null
								&& stockTransferItemslocalObj.getTaxPercent() > 0) {

							Double cgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = stockTransferItemslocalObj.getTaxPercent() / taxCodeListSize;
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							stockTransferItemslocalObj.setTaxValue(totalTax);

							stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.getTaxPercent());

							stockTransferItems = new StockTransferItems();
							selectedProductVarietyMaster = (stockTransferItemslocalObj.getProductVarietyMaster());
							stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
							societyProductAppraisal = new SocietyProductAppraisal();
							loadProductVerietyTaxPercentage();
							stockTransferItemslocalObj.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
									? stockTransferItems.getCgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
									? stockTransferItems.getSgstTaxPercent()
									: 0);
							stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemNetTotal()
									+ stockTransferItemslocalObj.getTaxValue());
						}
						/*
						 * index++; stockTransferItemsObj.setId(index);
						 */
						// stockTransferItemsMovementList.add(stockTransferItemsObj);
					}
				}
			}
		} catch (Exception e) {
			log.error("Exception in saveSateOutwardItem  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return ADD_PAGE_URL;
	}

	public String dateFormateChanged(Date date) {
		String returnDate = "";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			if (date != null) {
				returnDate = simpleDateFormat.format(date);
			}
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return returnDate;
	}

	private StockTransfer getStockTranferById(Long stockTrasnferId) {
		log.info("Start method getStockTranferById()");
		StockTransfer stockTransferLocal = new StockTransfer();
		try {
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/societystockmovement/getStockTransferbyId/"
					+ stockTrasnferId;
			log.info("::: Item Retrieved Details Controller calling :::");
			baseDTO = httpService.get(url);
			log.info("::: get Item Retrieved Details Response :");
			if (baseDTO.getStatusCode() == 0) {
				log.warn("Item Retrieved Details Successfully Completed ******");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferLocal = mapper.readValue(jsonResponse, new TypeReference<StockTransfer>() {
				});
				log.warn("Size of StockItemLis is--->:" + stockTransferLocal != null
						? stockTransferLocal.getStockTransferItemsList().size()
						: "null");
				// FacesContext.getCurrentInstance().getExternalContext().redirect(INPUT_FORM_VIEW_URL);
				return stockTransferLocal;
			} else {
				log.warn("Item Retrieved Details Error *** :");
				return null;
			}
		} catch (Exception ee) {
			log.error("Exception Occured in Item Retrieved Details load ", ee);
			return null;
		}
	}

	private List<StockTransferItems> calculateStockTranferItemList(
			List<StockTransferItems> stockTransferItemsActualList) {
		log.info("Start method calculateStockTranferItemList()");
		List<StockTransferItems> stockTransferItemsList = new ArrayList<StockTransferItems>();
		try {

			/*
			 * DecimalFormat decimalFormat = new DecimalFormat("#0.00");
			 * decimalFormat.setRoundingMode(RoundingMode.DOWN);
			 */
			if (stockTransferItemsActualList != null && stockTransferItemsActualList.size() > 0) {
				for (StockTransferItems stockTransferItemslocalObj : stockTransferItemsActualList) {
					if (stockTransferItemslocalObj.getUomMaster() == null) {
						Util.addWarn("UOM is Empty");
						// stockTransferItemsObj.setUomMaster(stockTransferItemslocalObj.getUomMaster());
					} else {
						log.info("UOM is not Empty" + stockTransferItemslocalObj.getUomMaster().getCode());
					}
					stockTransferItemslocalObj.setItemTotal(
							stockTransferItemslocalObj.getUnitRate() * stockTransferItemslocalObj.getDispatchedQty());
					stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.getItemTotal());
					stockTransferItemslocalObj.setDispatchedQty(stockTransferItemslocalObj.getDispatchedQty());
					stockTransferItemslocalObj.setReceivedQty(stockTransferItemslocalObj.getDispatchedQty());
					stockTransferItemslocalObj.setUnitRate(stockTransferItemslocalObj.getUnitRate());
					stockTransferItemslocalObj.setPurchasePrice(stockTransferItemslocalObj.getUnitRate());
					// stockTransferItems.setDescription(description);
					/*
					 * double total = stockTransferItemslocalObj.getItemTotal(); if
					 * (stockTransferItemslocalObj.getDiscountPercent() != null &&
					 * stockTransferItemslocalObj.getDiscountPercent() > 0) { double discountV =
					 * (total * stockTransferItemslocalObj.getDiscountPercent()) / 100;
					 * stockTransferItemslocalObj.setDiscountValue(discountV);
					 * stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.
					 * getItemNetTotal() - stockTransferItemslocalObj.getDiscountValue()); }
					 */

					/*
					 * if (stockTransferItemslocalObj.getTaxPercent() != null &&
					 * stockTransferItemslocalObj.getTaxPercent() > 0) { double tax =
					 * stockTransferItemslocalObj.getTaxPercent(); double taxV = (total * tax) /
					 * 100; log.info("tax value----" + taxV);
					 * stockTransferItemslocalObj.setTaxValue(taxV); double taxValue =
					 * roundOffTaxValue(decimalFormat, taxV);
					 * stockTransferItemslocalObj.setTaxValue(taxValue);
					 * stockTransferItemslocalObj.setTaxPercent(stockTransferItemslocalObj.
					 * getTaxPercent());
					 * 
					 * stockTransferItems = new StockTransferItems(); selectedProductVarietyMaster =
					 * (stockTransferItemslocalObj.getProductVarietyMaster());
					 * stockTransferItems.setUnitRate(stockTransferItemslocalObj.getUnitRate());
					 * societyProductAppraisal = new SocietyProductAppraisal();
					 * loadProductVerietyTaxPercentage();
					 * stockTransferItemslocalObj.setCgstTaxPercent(
					 * stockTransferItems.getCgstTaxPercent() != null ?
					 * stockTransferItems.getCgstTaxPercent() : 0);
					 * stockTransferItemslocalObj.setSgstTaxPercent(
					 * stockTransferItems.getSgstTaxPercent() != null ?
					 * stockTransferItems.getSgstTaxPercent() : 0);
					 * stockTransferItemslocalObj.setItemNetTotal(stockTransferItemslocalObj.
					 * getItemNetTotal() + stockTransferItemslocalObj.getTaxValue()); }
					 */
					/*
					 * index++; stockTransferItemsObj.setId(index);
					 */
					stockTransferItemsList.add(stockTransferItemslocalObj);
				}
			}

		} catch (Exception ee) {
			log.error("Exception Occured in calculateStockTranferItemList ", ee);
		}
		return stockTransferItemsList;
	}

	public void stockTransferItemsdetail(StockTransferItems stockTransferItems) {
		log.info("start stockTransferItemsdetail.....");
		stockTransferItemsPopupdisplayList = new ArrayList<>();
		try {
			Map<Long, List<StockTransferItems>> stocktransferitemsbyproductId = stockTransferItemsActualList.stream()
					.collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));
			if (stocktransferitemsbyproductId != null) {
				for (Map.Entry<Long, List<StockTransferItems>> entry : stocktransferitemsbyproductId.entrySet()) {
					if (entry.getKey().equals(stockTransferItems.getProductVarietyMaster().getId())) {

						Map<Double, List<StockTransferItems>> stockTransferItemsByAMount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));
						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAMount
								.entrySet()) {
							if (entrynew.getKey().equals(stockTransferItems.getUnitRate())) {
								stockTransferItemsPopupdisplayList = entrynew.getValue();
							}
						}
					}
				}
			}
			RequestContext.getCurrentInstance().execute("PF('prodvarietyitem').show();");
			RequestContext.getCurrentInstance().update("itemTablePanelDlg");
			log.info("stockTransferItems--->" + stockTransferItemsPopupdisplayList);
		} catch (Exception ee) {
			log.error("Exception Occured in stockTransferItemsdetail ", ee);
			return;
		}
	}

	public String validateCurrentReceivedQtyNew(StockTransferItems stockTransferItemsObj) {
		log.info("DispatchedQty() " + stockTransferItemsObj.getDispatchedQty());
		log.info("ReceivedQty() " + stockTransferItemsObj.getReceivedQty());
		try {
			if (stockTransferItemsObj != null && stockTransferItemsObj.getDispatchedQty() != null
					&& stockTransferItemsObj.getReceivedQty() != null) {
				if (stockTransferItemsObj.getReceivedQty() <= 0) {
					// errorMap.notify(OperationErrorCode.SOCIETY_STOCK_ACK_RECEIVED_ZERO);
					stockTransferItemsObj.setReceivedQty(0D);
				} else if (stockTransferItemsObj.getDispatchedQty() < stockTransferItemsObj.getReceivedQty()) {
					errorMap.notify(OperationErrorCode.SOCIETY_STOCK_ACK_RECEIVED_MORE_THAN_AVAILABLILITY);
					stockTransferItemsObj.setReceivedQty(null);
				}

				stockTransferItemsActualList.stream().forEach(items -> {
					if (items.getAtNumber().equalsIgnoreCase(stockTransferItemsObj.getAtNumber())) {
						items.setReceivedQty(stockTransferItemsObj.getReceivedQty());
					}
				});

				stockTransfer.getStockTransferItemsList().stream().forEach(stItemslist -> {
					if (stItemslist.getProductVarietyMaster().getCode()
							.equalsIgnoreCase(stockTransferItemsObj.getProductVarietyMaster().getCode())
							&& stItemslist.getUnitRate().equals(stockTransferItemsObj.getUnitRate())) {

						Double receivedQty = stockTransferItemsQty(
								stockTransferItemsObj.getProductVarietyMaster().getId(),
								stockTransferItemsObj.getUnitRate());
						stItemslist.setReceivedQty(receivedQty);
						stItemslist.setItemTotal(stItemslist.getUnitRate()
								* (stItemslist.getReceivedQty() != null ? stItemslist.getReceivedQty() : 0));

						stItemslist.setItemNetTotal(stItemslist.getItemTotal());
						// stockTransferItems.setDescription(description);
						Double total = stItemslist.getItemTotal();
						if (stItemslist.getDiscountPercent() != null && stItemslist.getDiscountPercent() > 0
								&& total != null) {
							double discountV = (total * stItemslist.getDiscountPercent()) / 100;
							stItemslist.setDiscountValue(discountV);
							stItemslist.setItemNetTotal(stItemslist.getItemNetTotal()
									- (stItemslist.getDiscountValue() != null ? stItemslist.getDiscountValue() : 0));
						}

						if (stItemslist.getTaxPercent() != null && stItemslist.getTaxPercent() > 0 && total != null) {

							total = total == null ? 0D : total;

							Double cgstPercentage = stItemslist.getCgstTaxPercent();
							cgstPercentage = cgstPercentage == null ? 0D : cgstPercentage;
							Double cgstValue = total * (cgstPercentage / 100);
							cgstValue = cgstValue == null ? 0D : cgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							cgstValue = AppUtil.getTrimmedValue(cgstValue);

							Double sgstPercentage = stItemslist.getSgstTaxPercent();
							sgstPercentage = sgstPercentage == null ? 0D : sgstPercentage;
							Double sgstValue = total * (sgstPercentage / 100);
							sgstValue = sgstValue == null ? 0D : sgstValue;
							/**
							 * Don't round off. Trim after two decimal.
							 */
							sgstValue = AppUtil.getTrimmedValue(sgstValue);

							Double totalTax = Double.sum(cgstValue, sgstValue);
							stItemslist.setTaxValue(totalTax);

							// stockTransferItems.setTaxPercent(stockTransferItems.getTaxPercent());
							stItemslist.setItemNetTotal(stItemslist.getItemNetTotal()
									+ (stItemslist.getTaxValue() != null ? stItemslist.getTaxValue() : 0));

							if ((stItemslist.getCgstTaxPercent() == null && stItemslist.getSgstTaxPercent() == null)
									|| (stItemslist.getCgstTaxPercent() == 0.0
											&& stItemslist.getSgstTaxPercent() == 0.0)) {
								stockTransferItems = new StockTransferItems();
								selectedProductVarietyMaster = (stItemslist.getProductVarietyMaster());
								stockTransferItems.setUnitRate(stItemslist.getUnitRate());
								societyProductAppraisal = new SocietyProductAppraisal();
								loadProductVerietyTaxPercentage();
								stItemslist.setCgstTaxPercent(stockTransferItems.getCgstTaxPercent() != null
										? stockTransferItems.getCgstTaxPercent()
										: 0);
								stItemslist.setSgstTaxPercent(stockTransferItems.getSgstTaxPercent() != null
										? stockTransferItems.getSgstTaxPercent()
										: 0);
							}
						}
					}
				});
			}
			RequestContext.getCurrentInstance().update("itemTablePanel");
			RequestContext.getCurrentInstance().update("totalReceivedQty");
		} catch (Exception e) {
			log.error("Exception Occured in stockTransferItemsdetail ", e);
		}
		return null;
	}

	public Double stockTransferItemsQty(Long productId, Double unitRate) {
		log.info("start stockTransferItemsQty.....");
		Double receivedQty = 0D;
		try {
			Map<Long, List<StockTransferItems>> stocktransferitemsbyproductId = stockTransferItemsActualList.stream()
					.collect(Collectors.groupingBy(i -> i.getProductVarietyMaster().getId()));
			if (stocktransferitemsbyproductId != null) {
				for (Map.Entry<Long, List<StockTransferItems>> entry : stocktransferitemsbyproductId.entrySet()) {
					if (entry.getKey().equals(productId)) {
						Map<Double, List<StockTransferItems>> stockTransferItemsByAmount = entry.getValue().stream()
								.filter(unit -> unit.getUnitRate() != null)
								.collect(Collectors.groupingBy(i -> i.getUnitRate()));
						for (Map.Entry<Double, List<StockTransferItems>> entrynew : stockTransferItemsByAmount
								.entrySet()) {
							if (entrynew.getKey().equals(unitRate)) {

								receivedQty = entrynew.getValue().stream().filter(i -> i.getReceivedQty() != null)
										.mapToDouble(q -> q.getReceivedQty()).sum();
							}
						}
					}
				}
			}
			log.info("stockTransferItems---receivedQty->" + receivedQty);
		} catch (Exception ee) {
			log.error("Exception Occured in stockTransferItemsdetail ", ee);
		}
		return receivedQty;
	}

}
