package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.ApproveRejectCommentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayscaleInformation;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.esr.dto.EmpPromotionDetailsDTO;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.hrms.model.EmpPromotionRequestDetails;
import in.gov.cooptex.personnel.hrms.dto.EmpPromotionRequestDto;
import in.gov.cooptex.personnel.hrms.dto.EmpPromotionResponseDto;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("empPromotionRequestBean")
public class EmpPromotionRequestBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1839609947276154749L;

	private final String PROMOTION_REQUEST_CREATE_PAGE = "/pages/admin/promotion/createPromotion.xhtml?faces-redirect=true;";
	private final String PROMOTION_REQUEST_LIST_URL = "/pages/admin/promotion/listPromotion.xhtml?faces-redirect=true;";
	private final String PROMOTION_REQUEST_VIEW_URL = "/pages/admin/promotion/viewPromo.xhtml?faces-redirect=true;";
	private final String PROMOTION_REQUEST_JOINDATE_URL = "/pages/admin/promotion/joinDatePromo.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EmpPromotionResponseDto empPromotionResponseDto = new EmpPromotionResponseDto();

	@Getter
	@Setter
	EmpPromotionRequestDetails empPromotionRequestDetails = new EmpPromotionRequestDetails();

	@Getter
	@Setter
	EmpPromotionRequestDto empPromotionRequestDto = new EmpPromotionRequestDto();

	@Getter
	@Setter
	EmpPromotionRequestDto viewEmpPromotionRequestDto = new EmpPromotionRequestDto();

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> approveCommentsDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> rejectCommentsDTOList = new ArrayList<>();

	@Getter
	@Setter
	EmpPromotionResponseDto selectedEmpPromotionResponseDto;

	@Getter
	@Setter
	LazyDataModel<EmpPromotionResponseDto> lazyEmployeePromotionResponseDtoList;

	@Getter
	@Setter
	List<EmpPromotionResponseDto> empPromotionResponseDtoList = new ArrayList<EmpPromotionResponseDto>();

	@Getter
	@Setter
	List<EmpPromotionResponseDto> createEmpPromotionResponseDtoList = new ArrayList<EmpPromotionResponseDto>();

	@Getter
	@Setter
	List<EmpPromotionResponseDto> viewEmpPromotionResponseDtoList = new ArrayList<EmpPromotionResponseDto>();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String remarks;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean joinDateButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EntityMaster> hoRoRegionList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList = new ArrayList<EntityTypeMaster>();

	@Getter
	@Setter
	List<PayScaleMaster> payScaleMasterList = new ArrayList<PayScaleMaster>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	List<EmployeeMaster> reportingTo = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	List<Designation> designationList = new ArrayList<Designation>();

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Long loanTenure = 0L;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	EntityMaster selectedHORORegion = new EntityMaster();

	@Getter
	@Setter
	EntityMaster selectedEntityMaster = new EntityMaster();

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster = new EntityTypeMaster();

	@Getter
	@Setter
	EmployeeMaster selectedEmployeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	EmployeePayscaleInformation employeePayscaleInformation = new EmployeePayscaleInformation();

	@Getter
	@Setter
	EmployeePersonalInfoEmployment employeePersonalInfoEmployment = new EmployeePersonalInfoEmployment();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<UserMaster>();

	@Getter
	@Setter
	List<EmpPromotionRequestDetails> empPromotionRequestDetailsList = new ArrayList<EmpPromotionRequestDetails>();

	@Getter
	@Setter
	List<EmpPromotionRequestDetails> empPromotionRequestDetailsListTotal = new ArrayList<EmpPromotionRequestDetails>();

	@Getter
	@Setter
	Double revisedBasicPay = 0.0;

	@Getter
	@Setter
	Boolean buttonShowsCondition = false;

	@Getter
	@Setter
	Boolean approveButtonAction = true;

	@Getter
	@Setter
	Boolean editOption = true;

	@Getter
	@Setter
	Department department = new Department();

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	String forwardFor;

	@Getter
	@Setter
	Boolean disableFlag = true;

	@Getter
	@Setter
	private Double collectedSecurityDeposit;

	@Getter
	@Setter
	private Double balanceSecurityDeposit;

	@Getter
	@Setter
	UserMaster userMaster;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	private Long notificationId = 0L;

	private Long systemNotificationId = 0L;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@PostConstruct
	public void init() {
		log.info("Promotion RequestBean init() Starts");
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";

		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			// selectedLeaveRequest = new LeaveRequest();
			// actionType = ActionTypeEnum.VIEW;
			// selectedLeaveRequest.setId(Long.parseLong(id));
			selectedEmpPromotionResponseDto = new EmpPromotionResponseDto();
			action = "VIEW";
			selectedEmpPromotionResponseDto.setId(Long.valueOf(id));
			systemNotificationId = Long.parseLong(notificationId);
			// showPage(ActionTypeEnum.VIEW);
			showEmployeePromotionData();
		}

	}

	public String showEmployeePromotionRequestListPage() {
		log.info("<==== Starts EmployeePromotionRequestBean.showLoanDisbursementListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		joinDateButtonFlag = true;
		loadLazyEmployeePromotionRequestList();
		log.info("<==== Ends EmployeePromotionRequestBean.showLoanDisbursementListPage =====>");
		return PROMOTION_REQUEST_LIST_URL;
	}

	public String showEmployeePromotionData() {
		log.info("<===== Starts EmployeePromotionRequestBean.showEmployeePromotionData ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedEmpPromotionResponseDto == null || selectedEmpPromotionResponseDto.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("<==== Starts EmployeePromotionRequestBean.showCreatePage =====>");
				editOption = true;
				empPromotionRequestDetails = new EmpPromotionRequestDetails();
				empPromotionRequestDetailsList = new ArrayList<EmpPromotionRequestDetails>();
				empPromotionRequestDto = new EmpPromotionRequestDto();
				viewEmpPromotionRequestDto = new EmpPromotionRequestDto();
				empPromotionRequestDetails = new EmpPromotionRequestDetails();
				selectedEmployeeMaster = new EmployeeMaster();
				departmentList = commonDataService.loadDepartmentList();
				hoRoRegionList = commonDataService.loadHeadAndRegionalOffice();
				payScaleMasterList = commonDataService.loadPayscale();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROMOTION.name());
				createEmpPromotionResponseDtoList = new ArrayList<EmpPromotionResponseDto>();
				collectedSecurityDeposit = 0D;
				balanceSecurityDeposit = 0D;
				department = new Department();
				userMaster = new UserMaster();
				employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
				clearSelectedDatas();
				clear();
				return PROMOTION_REQUEST_CREATE_PAGE;

			} else if (action.equalsIgnoreCase("EDIT")) {
				editOption = false;
				log.info("EmployeePromotionRequestBean editEmployeePromotion called..");
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROMOTION.name());
				String url = SERVER_URL + "/employeepromotionrequest/getbyid/" + selectedEmpPromotionResponseDto.getId()
						+ "/" + systemNotificationId;
				log.info("<=EmployeePromotionRequestBean :: editEmployeePromotion :: url=>" + url);
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					viewEmpPromotionRequestDto = mapper.readValue(jsonResponse,
							new TypeReference<EmpPromotionRequestDto>() {
							});
					empPromotionRequestDetailsList = viewEmpPromotionRequestDto.getEmpPromotionRequestDetailsList();
					return PROMOTION_REQUEST_CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("<=EmployeePromotionRequestBean :: viewEmployeePromotion=>");
				try {
					forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROMOTION.name());
					String url = SERVER_URL + "/employeepromotionrequest/getbyid/"
							+ selectedEmpPromotionResponseDto.getId() + "/" + systemNotificationId;
					log.info("<=EmployeePromotionRequestBean :: viewEmployeePromotion :: url=>" + url);
					BaseDTO baseDTO = httpService.get(url);
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						viewEmpPromotionRequestDto = mapper.readValue(jsonResponse,
								new TypeReference<EmpPromotionRequestDto>() {
								});

						Object[] approveRejectCommentData = baseDTO.getCommentAndLogList();
						setApproveCommentsDTOList((List<ApproveRejectCommentDTO>) approveRejectCommentData[0]);
						setRejectCommentsDTOList((List<ApproveRejectCommentDTO>) approveRejectCommentData[1]);

						empPromotionRequestDetailsList = viewEmpPromotionRequestDto.getEmpPromotionRequestDetailsList();
						if (loginBean.getUserDetailSession().getId().equals(
								viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getUserMaster().getId())) {
							buttonShowsCondition = true;
							approveButtonAction = true;
							disableFlag = false;
						} else {
							disableFlag = true;
						}
						if (viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getFinalApproval()) {
							approveButtonAction = false;
						}
						if (viewEmpPromotionRequestDto.getEmpPromotionRequestLog().getStage()
								.equals(ApprovalStage.FINAL_APPROVED)) {
							buttonShowsCondition = false;
						}
						empPromotionRequestDto
								.setNote(viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getNote());
						if (viewEmpPromotionRequestDto != null
								&& viewEmpPromotionRequestDto.getEmpPromotionRequestNote() != null) {
							empPromotionRequestDto.setUserId(
									viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getUserMaster().getId());
							empPromotionRequestDto.setFinalApproval(
									viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getFinalApproval());

						}
						userMaster = new UserMaster();
						return PROMOTION_REQUEST_VIEW_URL;
					}
				} catch (Exception e) {
					log.error(" Exception Occured While view Enquiry Officer :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			else if (action.equalsIgnoreCase("JOINDATE")) {

				log.info("<=EmployeePromotionRequestBean :: View join date=> /getemployeepromotion"
						+ selectedEmpPromotionResponseDto.getId());
				try {
					forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PROMOTION.name());
					String url = SERVER_URL + "/employeepromotionrequest/findallacceptemployee/"
							+ selectedEmpPromotionResponseDto.getId();
					log.info("<=EmployeePromotionRequestBean :: viewEmployeePromotion :: url=>" + url);
					BaseDTO baseDTO = httpService.get(url);
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						viewEmpPromotionRequestDto = mapper.readValue(jsonResponse,
								new TypeReference<EmpPromotionRequestDto>() {
								});
						empPromotionRequestDetailsListTotal = viewEmpPromotionRequestDto
								.getEmpPromotionRequestDetailsListTotal();

						int empPromotionRequestDetailsListTotalSize = empPromotionRequestDetailsListTotal != null
								? empPromotionRequestDetailsListTotal.size()
								: 0;
						log.info(
								"<=EmployeePromotionRequestBean :: viewEmployeePromotion :: empPromotionRequestDetailsListTotalSize: "
										+ empPromotionRequestDetailsListTotalSize);
						if (CollectionUtils.isEmpty(empPromotionRequestDetailsListTotal)) {
							errorMap.notify(ErrorDescription.NO_RECORD_FOUND.getErrorCode());
							return null;
						}

						if (loginBean.getUserDetailSession().getId().equals(
								viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getUserMaster().getId())) {
							buttonShowsCondition = true;
							approveButtonAction = true;
							disableFlag = false;
						} else {
							disableFlag = true;
						}
						if (viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getFinalApproval()) {
							approveButtonAction = false;
						}
						if (viewEmpPromotionRequestDto.getEmpPromotionRequestLog().getStage()
								.equals(ApprovalStage.FINAL_APPROVED)) {
							buttonShowsCondition = false;
						}
						empPromotionRequestDto
								.setNote(viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getNote());
						if (viewEmpPromotionRequestDto != null
								&& viewEmpPromotionRequestDto.getEmpPromotionRequestNote() != null) {
							empPromotionRequestDto.setUserId(
									viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getUserMaster().getId());
							empPromotionRequestDto.setFinalApproval(
									viewEmpPromotionRequestDto.getEmpPromotionRequestNote().getFinalApproval());

						}
						userMaster = new UserMaster();
						return PROMOTION_REQUEST_JOINDATE_URL;
					}
				} catch (Exception e) {
					log.error(" Exception Occured While view Enquiry Officer :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showEnquriyOfficerData ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EmployeePromotionRequestBean.showEmployeePromotionData ===========>" + action);
		return null;
	}

	@Getter
	@Setter
	Boolean hoRegiondisableFlag = true;

	public void loadEntityType() {
		log.info("<======== EmployeePromotionRequestBean.loadAllEntityByRegionId() calling ======>");
		try {
			entityMasterList = new ArrayList<>();
			entityTypeMasterList = new ArrayList<>();
			if (empPromotionRequestDetails != null && empPromotionRequestDetails.getNewRegion() != null
					&& empPromotionRequestDetails.getNewRegion().getEntityTypeMaster() != null
					&& empPromotionRequestDetails.getNewRegion().getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
				empPromotionRequestDetails
						.setNewEntityType(empPromotionRequestDetails.getNewRegion().getEntityTypeMaster());
				entityTypeMasterList.add(empPromotionRequestDetails.getNewRegion().getEntityTypeMaster());
				empPromotionRequestDetails.setNewWorkLocation(empPromotionRequestDetails.getNewRegion());
				entityMasterList.add(empPromotionRequestDetails.getNewRegion());
				hoRegiondisableFlag = true;
				loadEmployeeList(empPromotionRequestDetails.getNewRegion());
				return;
			} else {
				hoRegiondisableFlag = false;
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ empPromotionRequestDetails.getNewRegion().getId();
				log.info("url" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					entityTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
					});
				}
				log.info("regionalOfficegetEmployeeMasterByEntityIdEntityTypeList size" + entityTypeMasterList.size());
			}
		} catch (Exception e) {
			log.error("<========== Exception EmployeePromotionRequestBean.loadAllEntityTypesByRegionId()==========>",
					e);
		}
	}

	public void loadEntityMaster() {
		log.info("<======== EmployeePromotionRequestBean.loadEntityMaster() calling ======>");
		try {
			String url = AppUtil.getPortalServerURL() + "/entitymaster/getbyid/"
					+ empPromotionRequestDetails.getNewEntityType().getId();
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				entityMasterList = mapper.readValue(jsonValue, new TypeReference<List<EntityMaster>>() {
				});
			}
			log.info("EmployeePromotionRequestBean.loadEntityMaster size" + entityMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception EmployeePromotionRequestBean.loadEntityMaster()==========>", e);
		}

		log.info("<======== EmployeePromotionRequestBean.loadEntityMaster() END ======>");
	}

	public void loadEmployeeList() {
		log.info("<======== EmployeePromotionRequestBean.loadEmployeeList() calling ======>");
		employeeMasterList = new ArrayList<>();
		try {
			if (empPromotionRequestDto.getDesignationId() != null && !empPromotionRequestDto.getRegionId().isEmpty()) {
				String url = AppUtil.getPortalServerURL() + "/employeepromotionrequest/getpromotineligibleemployees";
				log.info("url" + url);
				BaseDTO baseDTO = httpService.post(url, empPromotionRequestDto);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
				}
				log.info("EmployeePromotionRequestBean.loadEntityMaster size" + employeeMasterList.size());
			}
		} catch (Exception e) {
			log.error("<========== Exception EmployeePromotionRequestBean.loadEntityMaster()==========>", e);
		}

		log.info("<======== EmployeePromotionRequestBean.loadEmployeeList() END ======>");
	}

	public void loadDesignationList() {
		log.info("... Load Designation List called ....");
		employeeMasterList = new ArrayList<>();
		try {
			log.info("DepartmentId is==>" + department.getId());
			if (department.getId() != null) {
				mapper = new ObjectMapper();
				BaseDTO response = httpService.get(SERVER_URL + "/designation/getalldesignation/" + department.getId());
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
				});
				log.info("designationList---->" + designationList.size());
			} else {
				designationList = new ArrayList<>();
			}
		} catch (Exception e) {
			log.error("Exception occured while loading Designation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void addEmployeePromotion() {
		log.info("<======== EmployeePromotionRequestBean.addEmployeePromotion() START ======>");
		try {

		} catch (Exception e) {
			log.info("<======== Exception Occured in.addEmployeePromotion() ======>");
			log.info("<======== Exception is ======>" + e);
		}
		log.info("<======== EmployeePromotionRequestBean.addEmployeePromotion() END ======>");

	}

	public String saveEmployeePromotionRequest() {
		log.info("<==== Starts EmployeePromotionRequestBean.saveLoanDisbursement =======>");
		try {

			if (empPromotionRequestDetailsList.isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.EMPLOYEE_PROMOTEDTABLE_DETAIL_CANNOT_EMPTY)
						.getErrorCode());
				return null;

			}

			if (empPromotionRequestDetailsList.isEmpty()) {
				log.info("====Promotion Details List is Empty Value=====");
				Util.addWarn("Promotion Details List is Cannot Be Empty");
				return null;
			}
			if (userMaster == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (empPromotionRequestDto.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
			if (StringUtils.isEmpty(note) || note == null || note.equalsIgnoreCase("")) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}

			String url = "";

			if (action.equalsIgnoreCase("CREATE")) {
				url = serverURL + "/employeepromotionrequest/savepromotedemployees";
			} else {
				url = serverURL + "/employeepromotionrequest/updatepromotedemployees";
				empPromotionRequestDto.setId(selectedEmpPromotionResponseDto.getId());
			}

			log.info("==========Save Employee Promotion request URL is=======" + url);
			empPromotionRequestDto.setEmpPromotionRequestDetailsList(empPromotionRequestDetailsList);
			empPromotionRequestDto.setNote(note);
			if (userMaster != null) {
				empPromotionRequestDto.setUserId(userMaster.getId());
			}
			BaseDTO response = httpService.post(url, empPromotionRequestDto);
			if (response != null && response.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("CREATE")) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.EMPLOYEES_PROMOTED_SUCCESS).getErrorCode());
				} else {
					/*
					 * errorMap.notify(ErrorDescription.getError(MastersErrorCode.
					 * EMPLOYEES_PROMOTED_UPDATED_SUCCESS) .getErrorCode());
					 */

					AppUtil.addInfo("Employee Promotion updated successfully");
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.update("growls");
				clearSelectedDatas();
				return showEmployeePromotionRequestListPage();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EmployeePromotionRequestBean.saveEmployeePromotionRequest =======>");
		return null;
	}

	public void loadLazyEmployeePromotionRequestList() {
		log.info("<===== Starts EmployeePromotionRequestBean.loadLazyEmployeePromotionRequestList ======>");
		lazyEmployeePromotionResponseDtoList = new LazyDataModel<EmpPromotionResponseDto>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -7079199200346366750L;

			@Override
			public List<EmpPromotionResponseDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/employeepromotionrequest/getemployeepromotion";
					log.info("Lazy load url is----------->>>>>" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						empPromotionResponseDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmpPromotionResponseDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("Total Record Count----------->>>>>" + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in empPromotionResponseDtoList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return empPromotionResponseDtoList;
			}

			@Override
			public Object getRowKey(EmpPromotionResponseDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpPromotionResponseDto getRowData(String rowKey) {
				try {
					for (EmpPromotionResponseDto empPromotionResponseDto : empPromotionResponseDtoList) {
						if (empPromotionResponseDto.getId().equals(Long.valueOf(rowKey))) {
							selectedEmpPromotionResponseDto = empPromotionResponseDto;
							return selectedEmpPromotionResponseDto;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends EmployeePromotionRequestBean.loadLazyEmployeePromotionRequestList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeePromotionRequestBean.onRowSelect ========>" + event);
		selectedEmpPromotionResponseDto = ((EmpPromotionResponseDto) event.getObject());
		if (selectedEmpPromotionResponseDto.getStatus().equals(ApprovalStage.FINAL_APPROVED)) {
			addButtonFlag = false;
			editButtonFlag = false;
			joinDateButtonFlag = true;

		} else if (selectedEmpPromotionResponseDto.getStatus().equals(ApprovalStage.REJECTED)) {
			addButtonFlag = false;
			editButtonFlag = true;
			joinDateButtonFlag = false;
		} else if (selectedEmpPromotionResponseDto.getStatus().equals(ApprovalStage.APPROVED)) {
			editButtonFlag = false;
			addButtonFlag = false;
			joinDateButtonFlag = false;
		} else {
			addButtonFlag = false;
			editButtonFlag = false;
			joinDateButtonFlag = false;
		}

		log.info("<===Ends EmployeePromotionRequestBean.onRowSelect ========>");
	}

	public String clear() {
		log.info("<===== Start EmployeePromotionRequestBean.clear ======>");
		empPromotionResponseDto = new EmpPromotionResponseDto();
		selectedEmpPromotionResponseDto = new EmpPromotionResponseDto();
		empPromotionResponseDtoList = new ArrayList<EmpPromotionResponseDto>();
		log.info("<===== Ends EmployeePromotionRequestBean.clear ======>");
		clearSelectedDatas();
		return showEmployeePromotionRequestListPage();
	}

	public void getSecurityDepositByEmpId(Long empId) {
		log.info("<<======= EmployeePromotionRequestBean .getSecurityDepositByEmpId() Starts =======>> ");
		try {
			String requestPath = SERVER_URL + "/joiningChecklist/getsecuritydeposit/" + empId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					collectedSecurityDeposit = mapper.readValue(jsonResponse, new TypeReference<Double>() {
					});
					collectedSecurityDeposit = collectedSecurityDeposit == null ? 0.00 : collectedSecurityDeposit;
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("Exception in EmployeePromotionRequestBean. getSecurityDepositByEmpId() ", exp);
		}
		log.info("<<======= EmployeePromotionRequestBean. getSecurityDepositByEmpId() Ends =======>> ");
	}

	public void showEmoloyee() {
		log.info("=========Start showEmoloyee =============");
		employeePayscaleInformation = new EmployeePayscaleInformation();
		EmployeeMaster employeeMaster = selectedEmployeeMaster;
		Long empId = selectedEmployeeMaster == null ? null : selectedEmployeeMaster.getId();
		if (empId == null) {
			log.info("Employee Master is Empty");
			return;
		}
		EmpPromotionDetailsDTO empPromotionDetailsDTO = new EmpPromotionDetailsDTO();
		boolean valid = true;
		String url = SERVER_URL + "/employeepromotionrequest/getdetailsbyemployeeid/" + selectedEmployeeMaster.getId()
				+ "/" + empPromotionRequestDto.getDesignationId();
		log.info("<=findPromotionEmployee URL=>" + url);
		BaseDTO baseDTO = httpService.get(url);
		if (baseDTO != null) {
			if (baseDTO.getResponseContent() != null) {
				try {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					empPromotionDetailsDTO = mapper.readValue(jsonResponse,
							new TypeReference<EmpPromotionDetailsDTO>() {
							});
					if (empPromotionDetailsDTO != null) {

						if (empPromotionDetailsDTO.getCreatedDate() != null) {
							int nmonth = AppUtil.monthCountBetweenTwoDates(empPromotionDetailsDTO.getCreatedDate(),
									new Date());
							if (nmonth < 12) {
								valid = false;
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}

		if (empPromotionDetailsDTO != null) {
			EmployeePersonalInfoEmployment personalInfoEmployment = empPromotionDetailsDTO.getPersonalInfoEmployment();
			log.info("findPromotionEmployee() - personalInfoEmployment: " + personalInfoEmployment);
			if (personalInfoEmployment != null) {
				selectedEmployeeMaster.setPersonalInfoEmployment(personalInfoEmployment);
				employeePersonalInfoEmployment = personalInfoEmployment;
			}
		}

		if (valid) {
			// Get Collected Security Deposit Amount from EmpJoiningCheckList Table based on
			// Appoint Order Detail Emp Id
			getSecurityDepositByEmpId(empId);
			log.info("Collected Security Deposit Amount : " + collectedSecurityDeposit);
			log.info("=========Selected EmployeeMaster =============" + selectedEmployeeMaster);

			if (selectedEmployeeMaster.getPersonalInfoEmployment() != null
					&& selectedEmployeeMaster.getPersonalInfoEmployment().getWorkLocation() != null
					&& selectedEmployeeMaster.getPersonalInfoEmployment().getWorkLocation().getId() != null) {
				log.info("Worklocation id: "
						+ selectedEmployeeMaster.getPersonalInfoEmployment().getWorkLocation().getId());
			} else {
				employeePersonalInfoEmployment = employeeMaster.getPersonalInfoEmployment();
				selectedEmployeeMaster.setPersonalInfoEmployment(employeePersonalInfoEmployment);
			}

			// selectedEmployeeMaster.getPersonalInfoEmployment().setReportingTo(empPromotionDetailsDTO.getReportBy());

			selectedEmployeeMaster.getPersonalInfoEmployment().setReportingTo(empPromotionDetailsDTO.getReportTo());

			log.info("Selected employee personalinfoemployment: " + selectedEmployeeMaster.getPersonalInfoEmployment());

			log.info("=========employeePersonalInfoEmployment =============" + employeePersonalInfoEmployment);
			if (!employeeMaster.getEmployeePayscaleInformationList().isEmpty()) {
				employeePayscaleInformation = employeeMaster.getEmployeePayscaleInformationList()
						.get(employeeMaster.getEmployeePayscaleInformationList().size() - 1);
			} else if (empPromotionDetailsDTO.getEmployeePayscaleInformation() != null) {
				employeePayscaleInformation = empPromotionDetailsDTO.getEmployeePayscaleInformation();

				if (StringUtils.isEmpty(employeePayscaleInformation.getRevisedBasicPay())) {
					AppUtil.addWarn("Current Basic Pay not found ");
				}
			}

		} else {
			selectedEmployeeMaster = new EmployeeMaster();
			employeePayscaleInformation = new EmployeePayscaleInformation();
			AppUtil.addWarn("Can Re Apply After 1 Year");
			log.warn("Can Re Apply After 1 Year");
		}

		log.info("=========END showEmoloyee =============");
	}

	public void checkPromotionEligibility() {
		try {
			log.info("<=findPromotionEmployee start=>");
			if (selectedEmployeeMaster != null && empPromotionRequestDto != null) {

				String url = SERVER_URL + "/employeepromotionrequest/getdetailsbyemployeeid/"
						+ selectedEmployeeMaster.getId() + "/" + empPromotionRequestDto.getDesignationId();
				log.info("<=findPromotionEmployee URL=>" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					if (baseDTO.getResponseContent() != null) {
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						EmpPromotionDetailsDTO empPromotionDetailsDTO = mapper.readValue(jsonResponse,
								new TypeReference<EmpPromotionDetailsDTO>() {
								});
						if (empPromotionDetailsDTO.getStatus() != null) {
							if (empPromotionDetailsDTO.getStatus().equalsIgnoreCase("REJECTED")) {
								if (empPromotionDetailsDTO != null) {
									int noOfMonth = AppUtil.monthCountBetweenTwoDates(
											empPromotionDetailsDTO.getDesignationCreatedDate(), new Date());
									if (noOfMonth <= 12) {
										AppUtil.addWarn("Can Re Apply After 1 Year");
										return;
									}
								}
							} else if (empPromotionDetailsDTO.getStatus().equalsIgnoreCase("FINAL-APPROVED")) {
								int noOfYear = getYearsBetweenDates(empPromotionDetailsDTO.getEmpPromotionLastDate(),
										new Date());
								if (noOfYear != empPromotionDetailsDTO.getPromotionPeriod()) {
									AppUtil.addWarn(" Can Re Apply After " + empPromotionDetailsDTO.getPromotionPeriod()
											+ " Year From The Last Promotion");
									return;
								}
							} else {
								int noOfYear = getYearsBetweenDates(empPromotionDetailsDTO.getDesignationCreatedDate(),
										new Date());
								if (noOfYear != empPromotionDetailsDTO.getPromotionPeriod()) {
									AppUtil.addWarn(" Can Apply After " + empPromotionDetailsDTO.getPromotionPeriod()
											+ " Year");
									return;
								}
							}
						}
					}
				}
			}
			log.info("<=checkPromotionEligibility end=>");

		} catch (Exception e) {
			log.error(":: Exception checkPromotionEligibility ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	private static int getYearsBetweenDates(Date first, Date second) {
		Calendar firstCal = GregorianCalendar.getInstance();
		Calendar secondCal = GregorianCalendar.getInstance();

		firstCal.setTime(first);
		secondCal.setTime(second);

		secondCal.add(Calendar.DAY_OF_YEAR, 1 - firstCal.get(Calendar.DAY_OF_YEAR));

		return secondCal.get(Calendar.YEAR) - firstCal.get(Calendar.YEAR);
	}

	public List<EmployeeMaster> loadEmployeeList(EntityMaster entityMaster) {
		log.info("<======== EmployeePromotionRequestBean.loadEmployeeList() calling ======>");
		try {
			reportingTo = new ArrayList<>();
			if (empPromotionRequestDetails.getNewWorkLocation() != null
					&& empPromotionRequestDetails.getNewWorkLocation().getId() != null) {
				String url = AppUtil.getPortalServerURL() + "/employee/getemployeebyentityandentitytypeid/"
						+ empPromotionRequestDetails.getNewWorkLocation().getId();
				log.info("loadEmployeeList url is" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					reportingTo = mapper.readValue(jsonValue, new TypeReference<List<EmployeeMaster>>() {
					});
				}
			}
			log.info("meetingentitytypelist size" + entityTypeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception EmployeePromotionRequestBean.loadEmployeeList()==========>", e);
		}
		return reportingTo;
	}

	public String loadPromotionEmployeeData() {
		log.info("<======== EmployeePromotionRequestBean.loadPromotionEmployeeData() START ======>");
		try {
			if (selectedEmployeeMaster.getId() != null) {
				if (employeePersonalInfoEmployment.getCurrentDesignation().getId() == null) {
					log.info("====Employee Current Designation is Empty Value=====");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CURRENT_DESIGNATION_EMPTY).getErrorCode());
					return null;
				}
				if (employeePayscaleInformation.getRevisedBasicPay() == null) {
					log.info("====Employee Current Basic Pay is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.CURRENT_BASIC_PAY_EMPTY).getErrorCode());
					return null;
				}

				if (employeePayscaleInformation.getRevisedPayScaleMaster() == null
						|| employeePayscaleInformation.getRevisedPayScaleMaster().getId() == null) {
					log.info("====Employee Current Payscale Master is Empty Value=====");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CURRENT_PAYSCALE_MASTER_EMPTY).getErrorCode());
					return null;
				}
				if (employeePersonalInfoEmployment.getWorkLocation().getId() == null) {
					log.info("====Employee Current Worklocation is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.CURRENT_ENTITY_EMPTY).getErrorCode());
					return null;
				}
				if (employeePersonalInfoEmployment.getWorkLocation().getEntityTypeMaster() == null
						|| employeePersonalInfoEmployment.getWorkLocation().getEntityTypeMaster().getId() == null) {
					log.info("====Employee Current Entity Type is Empty Value=====");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CURRENT_ENTITY_TYPE_EMPTY).getErrorCode());
					return null;
				}
				if (employeePersonalInfoEmployment.getWorkLocation() == null) {
					log.info("====Employee Current Region is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.CURRENT_REGION_EMPTY).getErrorCode());
					return null;
				}

				if (empPromotionRequestDetails.getNewWorkLocation() == null
						|| empPromotionRequestDetails.getNewWorkLocation().getId() == null) {
					log.info("====Employee New WorkLocation is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_ENTITY_EMPTY).getErrorCode());
					return null;
				}
				if (empPromotionRequestDetails.getNewRegion() == null
						|| empPromotionRequestDetails.getNewRegion().getId() == null) {
					log.info("====Employee New Region is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_REGION_EMPTY).getErrorCode());
					return null;
				}
				if (empPromotionRequestDetails.getNewEntityType() == null
						|| empPromotionRequestDetails.getNewEntityType().getId() == null) {
					log.info("====Employee New Entity Type is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_ENTITY_TYPE_EMPTY).getErrorCode());
					return null;
				}

				if (empPromotionRequestDetails.getRevisedPayScale() == null
						|| empPromotionRequestDetails.getRevisedPayScale().getId() == null) {
					log.info("====Employee New PayScale is Empty Value=====");
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.NEW_PAYSCALE_MASTER_EMPTY).getErrorCode());
					return null;
				}
				if (empPromotionRequestDetails.getRevisedBasicPay() == null) {
					log.info("====Employee Revised Basic Pay is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_BASIC_PAY_EMPTY).getErrorCode());
					return null;
				}
				if (empPromotionRequestDetails.getPromotedDesignation() == null) {
					log.info("====Employee New Designation is Empty Value=====");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_DESIGNATION_EMPTY).getErrorCode());
					return null;
				}
				if (empPromotionRequestDetails != null) {
					for (EmpPromotionRequestDetails empPromotionRequestDetails : empPromotionRequestDetailsList) {
						Long empId = empPromotionRequestDetails.getEmployee().getId();
						log.info("empId==> " + empId);
						if (empId == selectedEmployeeMaster.getId()) {
							errorMap.notify(
									ErrorDescription.getError(MastersErrorCode.EMP_ID_ALREADY_EXIST).getErrorCode());
							return null;
						}
					}
				}
				if (employeePayscaleInformation != null) {
					Double currentBasicPay = Double.parseDouble(employeePayscaleInformation.getRevisedBasicPay());
					empPromotionRequestDetails.setCurrentBasicPay(currentBasicPay);
					empPromotionRequestDetails
							.setCurrentPayScale(employeePayscaleInformation.getRevisedPayScaleMaster());

				}
				if (employeePersonalInfoEmployment != null) {
					empPromotionRequestDetails
							.setCurrentDesignation(employeePersonalInfoEmployment.getCurrentDesignation());
					empPromotionRequestDetails.setCurrentEntityType(employeePersonalInfoEmployment.getEntityType());
					empPromotionRequestDetails.setCurrentReportingTo(employeePersonalInfoEmployment.getReportingTo());
					empPromotionRequestDetails.setCurrentWorkLocation(employeePersonalInfoEmployment.getWorkLocation());

					if (employeePersonalInfoEmployment.getWorkLocation() != null
							&& employeePersonalInfoEmployment.getWorkLocation().getEntityMasterRegion() == null)
						empPromotionRequestDetails.setCurrentRegion(employeePersonalInfoEmployment.getWorkLocation());
					else if (employeePersonalInfoEmployment.getWorkLocation() != null)
						empPromotionRequestDetails.setCurrentRegion(
								employeePersonalInfoEmployment.getWorkLocation().getEntityMasterRegion());
				}

				empPromotionRequestDetails.setEmployee(selectedEmployeeMaster);
				if (action.equalsIgnoreCase("CREATE")) {
					empPromotionRequestDetailsList.add(empPromotionRequestDetails);
					collectedSecurityDeposit = null;
					empPromotionRequestDetails = new EmpPromotionRequestDetails();
				}
				clearSelectedDatas();
			}
		} catch (Exception e) {
			log.info("<========Exception occured EmployeePromotionRequestBean.loadPromotionEmployeeData() ======>");
			log.info("<========Exception is ======>", e);
		}
		log.info("<======== EmployeePromotionRequestBean.loadPromotionEmployeeData() END ======>");

		return null;

	}

	public void clearSelectedDatas() {
		empPromotionRequestDetails = new EmpPromotionRequestDetails();
		employeePayscaleInformation = new EmployeePayscaleInformation();
		employeePayscaleInformation = new EmployeePayscaleInformation();
		selectedEmployeeMaster = new EmployeeMaster();
		selectedHORORegion = new EntityMaster();
		selectedEntityMaster = new EntityMaster();
		selectedEntityTypeMaster = new EntityTypeMaster();
		empPromotionRequestDto.setRegionId(null);
		empPromotionRequestDto.setDesignationId(null);
		department = new Department();
		setNote(null);
	}

	public String approveReject() {
		log.info("<===Starts EmployeePromotionRequestBean.approveReject ========>");

		if (null != remarks && !remarks.equals("")) {
			empPromotionRequestDto.setNote(remarks);
		}

		if (empPromotionRequestDto.getNote().equals("")) {
			log.info("====Employee New Designation is Empty Value=====");
			errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
			return null;
		}

		if (empPromotionRequestDto.getApproveReject().equals("APPROVE")) {
			if (userMaster == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			} else {
				empPromotionRequestDto.setUserId(userMaster.getId());
			}

			if (empPromotionRequestDto.getUserId() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (empPromotionRequestDto.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
		}
		if (empPromotionRequestDto.getApproveReject().equals("REJECT")) {
			if (empPromotionRequestDto.getNote() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}
		}
		String url = SERVER_URL + "/employeepromotionrequest/approvereject";
		log.info("approveReject Url is==================>>>>>>>>>>>>" + url);
		empPromotionRequestDto.setId(selectedEmpPromotionResponseDto.getId());
		empPromotionRequestDto.setEmpPromotionRequestDetailsList(empPromotionRequestDetailsList);
		BaseDTO response = httpService.post(url, empPromotionRequestDto);
		if (response != null && response.getStatusCode() == 0) {
			if (empPromotionRequestDto.getApproveReject().equals("REJECT")) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.PROMOTED_EMPLOYEES_REJECTED_SUCCESS).getErrorCode());
			} else if (empPromotionRequestDto.getApproveReject().equalsIgnoreCase("FINAL-APPROVED")) {
				/*
				 * errorMap.notify(ErrorDescription.getError(MastersErrorCode.
				 * PROMOTED_EMPLOYEES_FINAL_APPROVED_SUCCESS) .getErrorCode());
				 */
				AppUtil.addInfo("Promoted Employees Final Approved Successfully");
			} else {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.PROMOTED_EMPLOYEES_APPROVED_SUCCESS).getErrorCode());
			}
			return clear();
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===End EmployeePromotionRequestBean.approveReject ========>");
		return null;
	}

	public void editAction(EmpPromotionRequestDetails selectedEmpPromotionRequestDetails) {
		log.info("<===Starts EmployeePromotionRequestBean.editAction ========>");
		empPromotionRequestDetails = selectedEmpPromotionRequestDetails;
		loadEntityType();
		loadEntityMaster();
		reportingTo = loadEmployeeList(empPromotionRequestDetails.getNewWorkLocation());
		departmentList = commonDataService.loadDepartmentList();
		hoRoRegionList = commonDataService.loadHeadAndRegionalOffice();
		payScaleMasterList = commonDataService.loadPayscale();
		designationList = commonDataService.loadAllDesignationList();
		employeePersonalInfoEmployment = empPromotionRequestDetails.getEmployee().getPersonalInfoEmployment();
		selectedEmployeeMaster = selectedEmpPromotionRequestDetails.getEmployee();
		selectedEmployeeMaster.getPersonalInfoEmployment()
				.setReportingTo(selectedEmpPromotionRequestDetails.getCurrentReportingTo());
		employeePayscaleInformation.setRevisedPayScaleMaster(selectedEmpPromotionRequestDetails.getCurrentPayScale());
		employeePayscaleInformation
				.setRevisedBasicPay(String.valueOf(selectedEmpPromotionRequestDetails.getCurrentBasicPay()));

		log.info("<===Starts EmployeePromotionRequestBean.editAction ========>");
	}

	public void employeeClear() {
		employeeMasterList = new ArrayList<EmployeeMaster>();
	}

	public void resivedPayScaleChange() {
		log.info("<<============ EmpPromotionRequestBean. resivedPayScaleChange ===============>> ");
		if (empPromotionRequestDetails != null && empPromotionRequestDetails.getRevisedPayScale() != null) {
			double revisedPayScaleAmount = empPromotionRequestDetails.getRevisedPayScale().getFromAmount();
			if (revisedPayScaleAmount <= collectedSecurityDeposit) {
				empPromotionRequestDetails.setBalanceSecurityDeposit(0D);
			} else {
				empPromotionRequestDetails.setBalanceSecurityDeposit(revisedPayScaleAmount - collectedSecurityDeposit);
			}
		}
	}

	public void valueDateChange() {
		log.info("===value change listener=====" + empPromotionRequestDetailsListTotal.get(0).getJoinDate());
		empPromotionRequestDetails.setJoinDate(empPromotionRequestDetailsListTotal.get(0).getJoinDate());
	}

	public String onChangeJoiningDate() {
		String url = serverURL + "/employeepromotionrequest/updateempjoiningdate";
		empPromotionRequestDto.setEmpPromotionRequestDetailsListTotal(empPromotionRequestDetailsListTotal);
		log.info("============" + empPromotionRequestDetails.getJoinDate());
		BaseDTO response = httpService.post(url, empPromotionRequestDto);
		if (response != null && response.getStatusCode() == 0) {
			selectedEmpPromotionResponseDto = new EmpPromotionResponseDto();
			errorMap.notify(ErrorDescription.EMP_EMPLOYEEMENT_DETAILS_UPDATED.getErrorCode());
			RequestContext context = RequestContext.getCurrentInstance();
			context.update("growls");
		}
		return PROMOTION_REQUEST_LIST_URL;
	}
}