package in.gov.cooptex.admin.vehicle.ui;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.tapal.model.TapalDeliveryType;
import in.gov.cooptex.admin.vehicle.model.VehicleArrival;
import in.gov.cooptex.admin.vehicle.model.VehicleDeparture;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.VehicleDepartureSearchRequest;
import in.gov.cooptex.core.dto.VehicleDepartureSearchResponse;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.VehicleRegister;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("vehicleManagementBean")
@Scope("session")
public class VehicleManagementBean {
	
	private final String VEHICLE_DEPARTURE_VIEW_PAGE = "/pages/admin/vehicleManagement/viewVehicleDepartureAndArrival.xhtml?faces-redirect=true;";
	private final String VEHICLE_DEPARTURE_CREATE_PAGE = "/pages/admin/vehicleManagement/createVehicleDeparture.xhtml?faces-redirect=true;";
	private final String VEHICLE_MANAGEMENT_LIST_PAGE = "/pages/admin/vehicleManagement/listVehicle.xhtml?faces-redirect=true;";
	private final String VEHICLE_ARRIVAL_CREATE_PAGE = "/pages/admin/vehicleManagement/createVehicleArrival.xhtml?faces-redirect=true;";
	
	private String serverURL = null;

	@Getter
	@Setter
	VehicleDepartureSearchResponse selectedvehicledepartureResponse;
	
	@Getter
	@Setter
	double totalkilometer;


	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	String action=null;
	
	
	
	@Getter
	@Setter
	EmployeeMaster empmaster;
	
	@Getter
	@Setter
	private VehicleDeparture vehicledeparture;
	
	@Getter
	@Setter
	private VehicleArrival vehicleArrival;

	@Getter
	@Setter
	private VehicleDeparture selectedvehicledeparture;

	@Getter
	@Setter
	private List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	private List<EmployeeMaster> driverList;

	@Getter
	@Setter
	private BaseDTO baseDTO = new BaseDTO();


	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	ObjectMapper mapper;
	
	boolean forceLogin = false;

	
	@Getter
	@Setter
	private String jsonResponse;

	@Getter
	@Setter
	private List<VehicleRegister> vehicleRegisterList;

	@Getter
	@Setter
	private Date startTime;

	@Getter
	@Setter
	private Date endTime;

	@Autowired
	LoginBean loginBean;

	int vehicleRegisterListsize;

	@Autowired
	CommonDataService commondataservice;

	@Getter
	@Setter
	List<VehicleDeparture> vehicleDepartureList;

	@Getter
	@Setter
	LazyDataModel<VehicleDepartureSearchResponse> vehicledepartureResponseLazyList;

	@Getter
	@Setter
	private String vehicle_make;

	@Getter
	@Setter
	private String vehicle_variant;

	@Getter
	@Setter
	int totalRecords;
	
	@Getter
	@Setter
	boolean transactionCheck;
	
	@Getter
	@Setter
	boolean addButton=true;
	
	@Getter
	@Setter
	boolean viewCheck;
	
	@Getter
	@Setter
	Double lastkilomaeter;

	
	 
	
	public String showList() {
		log.info("<---------- Loading TapalDeliveryTypeBean showList list page---------->");
		serverURL = AppUtil.getPortalServerURL();
		vehicledeparture = new VehicleDeparture();
		loadValues();
		loadLazyVehicleDepartureList();
		getAllEmployee();
		return VEHICLE_MANAGEMENT_LIST_PAGE;
	}

	private void loadValues() {
		try {

			loadVehicle();
			loadVehicleDriver();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
	}
	
	
	
	public String creatDeparture() {
		getAllEmployee();
		loadVehicle();
		vehicledeparture=new VehicleDeparture();
		return VEHICLE_DEPARTURE_CREATE_PAGE;
	}
	
	

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		vehicledeparture = new VehicleDeparture();
		return VEHICLE_MANAGEMENT_LIST_PAGE;

	}
	
	public String deleteVehicleDeparture() {
		try {
			action = "Delete";
			if (selectedvehicledepartureResponse == null) {
				Util.addWarn("Please Select One Vehicle Departure");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVehicleDepartureDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	

	public String deleteVehicleDepartureConfirm() {
		
		try {
			log.info("Delete confirmed for Vehicle Departure [" + selectedvehicledepartureResponse + "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/delete";
			baseDTO = httpService.post(url, selectedvehicledepartureResponse);
			showListPage();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Vehicle Departure deleted successfully....!!!");
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_DEPARTURE_DELETE_SUCCESSFULLY).getErrorCode());
					selectedvehicledepartureResponse = new VehicleDepartureSearchResponse();
				}
				else if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;
					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String showListPage() {
		log.info("<==== Starts ListPage =====>");
		mapper = new ObjectMapper();
		boolean editButtonFlag = true;
		deleteButtonFlag = true;
		addButton=true;
		vehicledeparture = new VehicleDeparture();
		selectedvehicledeparture = new VehicleDeparture();
		selectedvehicledepartureResponse=new VehicleDepartureSearchResponse();
		loadLazyVehicleDepartureList();
		log.info("<==== Ends ListPage =====>");

		return VEHICLE_MANAGEMENT_LIST_PAGE;
	}

	public void getAllEmployee() {
		log.info("... getAllEmployee called ....");
		employeeList = new ArrayList<EmployeeMaster>();
		try {

			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/vehiclemanagement/getallEmployee/";

			baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent()); 

			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while getting All Employee....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void loadVehicle() {
		log.info("<<<< ----------Start VehicleRegisterBean-loadVehicleTypeList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url=null;
			vehicleRegisterList = new ArrayList<VehicleRegister>();
			log.info("<<== action..."+action);
			Long vehicleRegisterId=0L;
			if(vehicledeparture!=null && vehicledeparture.getVehicleRegister()!=null && vehicledeparture.getVehicleRegister().getId()!=null) {
				log.info("<<== vehicle Register Id..."+vehicledeparture.getVehicleRegister().getId());
				vehicleRegisterId=vehicledeparture.getVehicleRegister().getId();
			}
			if(!action.equalsIgnoreCase("EDIT"))
				 url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadVehicle";
			else
				 url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadEditVehicle/"+vehicleRegisterId;
			
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleRegisterList = mapper.readValue(jsonResponse, new TypeReference<List<VehicleRegister>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicleRegisterList != null) {
			vehicleRegisterListsize = vehicleRegisterList.size();
		} else {
			vehicleRegisterListsize = 0;
		}
		log.info("<<<< ----------VehicleRegisterBean-vehicleTypeListSize ------- >>>> " + vehicleRegisterListsize);

	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	
	public List<EmployeeMaster> loadDriverAutoComplete(String query) {
		log.info("VehicleManagementBean. loadDriverAutoComplete() - START");
		List<EmployeeMaster> employeeList = new ArrayList<EmployeeMaster>();
		try {
			log.info("VehicleManagementBean. loadDriverAutoComplete() - query: "+query);
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadDriverAutoComplete/"+query;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
		} catch(Exception e) {
			log.error("Exception at loadDriverAutoComplete()", e);
		}
		log.info("VehicleManagementBean. loadDriverAutoComplete() - END");
		return employeeList;
	}

	/**
	 * 
	 * @param query
	 * @return
	 */
	public List<EmployeeMaster> loadAllEmployeeAutoComplete(String query) {
		log.info("VehicleManagementBean. loadAllEmployeeAutoComplete() - START");
		List<EmployeeMaster> employeeList = new ArrayList<EmployeeMaster>();
		try {
			log.info("VehicleManagementBean. loadAllEmployeeAutoComplete() - query: "+query);
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getAllEmployeeAutoComplete/"+query;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
		} catch(Exception e) {
			log.error("Exception at loadAllEmployeeAutoComplete()", e);
		}
		log.info("VehicleManagementBean. loadAllEmployeeAutoComplete() - END");
		return employeeList;
	}
	
	private void loadVehicleDriver() {
		log.info("<<<< ----------Start loadVehicleDriver ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			driverList = new ArrayList<EmployeeMaster>();

			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/loadDriverList";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				driverList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		log.info("<<<< ----------loadVehicleDriver-------end----- >>>> " + driverList);
	}

	public String submitDeparture() {
		log.info("...... vehicledeparture submit begin ....");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/create";
			vehicledeparture.setCreatedBy(loginBean.getUserDetailSession());
			vehicledeparture.setCreatedDate(new Date());
			empmaster=new EmployeeMaster();
			empmaster=loginBean.getEmployee();
			log.info("empmaster"+empmaster);
			vehicledeparture.setEntityMaster(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation());

			baseDTO = httpService.post(URL, vehicledeparture);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("vehicledeparture saved successfully");
					showListPage();
					//errorMap.notify(baseDTO.getStatusCode());
					if(action.equalsIgnoreCase("ADD"))
						errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_DEPARTURE_SAVE_SUCCESSFULLY).getErrorCode());
					else
						errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_DEPARTURE_UPDATE_SUCCESSFULLY).getErrorCode());
					
					RequestContext.getCurrentInstance().update("growls");
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating vehicledeparture" + e);
		}
		log.info("...... vehicledeparture submit ended ....");
		return VEHICLE_MANAGEMENT_LIST_PAGE;
	}
	
	
	public String submitArrival() {
		log.info("...... vehicleArrival submit begin ....");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = serverURL + appPreference.getOperationApiUrl() + "/vehiclearrival/create";
			vehicleArrival.setCreatedBy(loginBean.getUserDetailSession());
			vehicleArrival.setCreatedDate(new Date());
			vehicleArrival.setVehicleDeparture(vehicledeparture);
			
			if(vehicledeparture.getStartKilometre()>=vehicleArrival.getEndKilometer()) {
				log.info("End kilometer is less than start kilometer-----------------");
				Util.addWarn("Please Enter EndKilometer greater than Started Kilometer");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if(vehicledeparture.getDepartureDate().equals(vehicleArrival.getArrivalDate())) {
			if(vehicleArrival.getArrivalTime().before(vehicledeparture.getDepartureTime())) {
				Util.addWarn("Arrival Time should not be allowed for before departure  Time");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			}
		
			baseDTO = httpService.post(URL, vehicleArrival);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("vehicleArrival saved successfully");
					showListPage();
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_ARRIVAL_SAVE_SUCCESSFULLY).getErrorCode());
					RequestContext.getCurrentInstance().update("growls");
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating vehicleArrival" + e);
		}
		log.info("...... vehicleArrival submit ended ....");
		return VEHICLE_MANAGEMENT_LIST_PAGE;
	}


	public void loadLazyVehicleDepartureList() {

		log.info("< --  Start VehicleDeparture Lazy Load -- > ");
		vehicledepartureResponseLazyList = new LazyDataModel<VehicleDepartureSearchResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<VehicleDepartureSearchResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
			 			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						//mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						List<VehicleDepartureSearchResponse> data = mapper.readValue(jsonResponse,
								new TypeReference<List<VehicleDepartureSearchResponse>>() {
								});
						if (data == null) {
							log.info(" VehicleDepartureSearchResponse Failed to Deserialize");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}

						if (baseDTO.getStatusCode() == 0) {
							this.setRowCount(baseDTO.getTotalRecords());
							totalRecords = baseDTO.getTotalRecords();
							log.info(" Vehicle Departure Search  Successfully Completed");
						} else {
							log.error(
									" Vehicle Departure  Search Failed With status code :: " + baseDTO.getStatusCode());
							errorMap.notify(baseDTO.getStatusCode());
						}
						return data;
					}
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Vehicle Departure data :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(VehicleDepartureSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VehicleDepartureSearchResponse getRowData(String rowKey) {
				List<VehicleDepartureSearchResponse> responseList = (List<VehicleDepartureSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (VehicleDepartureSearchResponse res : responseList) {
					if (res.getId() == value.longValue()) {
						selectedvehicledepartureResponse = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	public void onRowSelect(SelectEvent event) {
		selectedvehicledepartureResponse = ((VehicleDepartureSearchResponse) event.getObject());
		addButton=false;
		if(selectedvehicledepartureResponse.isStatus()==true){
			transactionCheck=false;
			viewCheck=true;
			
			Util.addInfo("Vehicle Arrival is Completed");
		}else {
			transactionCheck=true;
			viewCheck=false;
		}
	

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			VehicleDepartureSearchRequest request = new VehicleDepartureSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("vehicleRegistrarNo")) {
					log.info("vehVEHICLE_DEPARTURE_VIEW_PAGEicleRegistrarNo : " + value);
					request.setVehicleRegistrarNo(value);
				}

				if (entry.getKey().equals("driverName")) {
					log.info("employeeName : " + value);
					request.setDriverName(value);
				}
				if (entry.getKey().equals("destination")) {
					log.info("destination : " + value);
					request.setDestination(value);
				}

				if (entry.getKey().equals("employeeName")) {
					log.info("employeeName : " + value);
					request.setEmployeeName(value);
				}
				
				
				
				if (entry.getKey().equals("departureDate")) {
					log.info("departureDate : " + value);
					request.setDepartureDate(AppUtil.serverDateFormat(value));
				}

			}
			log.info("Search request data" + request);
			String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/vehiclemanagement/search";
			baseDTO = httpService.post(URL, request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Vehicle Departure  search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		} 
		return baseDTO;
	}
	
	
	
	public String viewTapalDeliveryType() {
		try {
			action = "View";
			if (selectedvehicledepartureResponse == null) {
				Util.addWarn("Please Select One Tapal Delivery Type");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTapalDeliverTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	public String edit() {
		log.info("<<<< ----------Start VehicleManagement  edit() ------- >>>>");
		totalkilometer=0;
		if(selectedvehicledepartureResponse!=null) { 
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicledeparture=new VehicleDeparture();
			
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getvehicleDeparture";
			long id=selectedvehicledepartureResponse.getId();
			baseDTO = httpService.post(url, selectedvehicledepartureResponse);
			vehicledeparture = new VehicleDeparture();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicledeparture = mapper.readValue(jsonResponse, new TypeReference<VehicleDeparture>() {});
				
				if(transactionCheck)
					getarrival();
				
				loadVehicle();
				getAllEmployee();
				totalkilometer=vehicleArrival.getEndKilometer() -  vehicledeparture.getStartKilometre();
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicledeparture != null)
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);
		else
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);

		log.info("<<<< ----------End vehicleManagement  Edit ------- >>>>");
		return VEHICLE_DEPARTURE_CREATE_PAGE;
		}
		else {
			Util.addWarn("Please Select One Vehicle Transaction");
			return VEHICLE_MANAGEMENT_LIST_PAGE;
		}
	}
	

	
	public String getvehicleManagementView() {
		log.info("<<<< ----------Start TapalDeliveryTypeBean . getTapalDeliveryTypeView() ------- >>>>");
		totalkilometer=0;
		if(selectedvehicledepartureResponse!=null) { 
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicledeparture=new VehicleDeparture();
			
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getvehicleDeparture";
			long id=selectedvehicledepartureResponse.getId();
			baseDTO = httpService.post(url, selectedvehicledepartureResponse);
			vehicledeparture = new VehicleDeparture();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicledeparture = mapper.readValue(jsonResponse, new TypeReference<VehicleDeparture>() {});
				getarrival();
				getAllEmployee();
				if(vehicleArrival != null && vehicleArrival.getEndKilometer() != null && vehicledeparture != null && vehicledeparture.getStartKilometre() != null) {
					totalkilometer=vehicleArrival.getEndKilometer() -  vehicledeparture.getStartKilometre();
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicledeparture != null)
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);
		else
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);

		log.info("<<<< ----------End vehicleManagement . getvehicleManagementView ------- >>>>");
		return VEHICLE_DEPARTURE_VIEW_PAGE;
		}
		else {
			Util.addWarn("Please Select One Vehicle Transaction");
			return VEHICLE_MANAGEMENT_LIST_PAGE;
		}
		
	}
	
	
	public void getarrival() {
		log.info("<<<< ----------Start getArrival() ------- >>>>");
		if(selectedvehicledepartureResponse!=null) { 
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicleArrival=new VehicleArrival();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getvehicleArrival";
			long id=selectedvehicledepartureResponse.getId();
			baseDTO = httpService.post(url, selectedvehicledepartureResponse);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleArrival = mapper.readValue(jsonResponse, new TypeReference<VehicleArrival>() {});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
	}
		
	}

	public String createArival() {
		log.info("<<<< ----------Start createArival . createArival() ------- >>>>");
		vehicleArrival=new VehicleArrival();
		totalkilometer=0.0;
		if(selectedvehicledepartureResponse!=null) { 
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicledeparture=new VehicleDeparture();
			
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getvehicleDeparture";
			baseDTO = httpService.post(url, selectedvehicledepartureResponse);
			vehicledeparture = new VehicleDeparture();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicledeparture = mapper.readValue(jsonResponse, new TypeReference<VehicleDeparture>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicledeparture != null)
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);
		else
			log.info("vehicleManagement getvehicleManagementView" + vehicledeparture);

		log.info("<<<< ----------End vehicleManagement . getvehicleManagementView ------- >>>>");
		return VEHICLE_ARRIVAL_CREATE_PAGE;
		}
		
		else {
			Util.addWarn("Please Select Vehicle Departure");
			return VEHICLE_MANAGEMENT_LIST_PAGE;
		}
	}
	
	public void gettotalkilometers() {
		if(vehicledeparture.getStartKilometre()<vehicleArrival.getEndKilometer()) {
			//totalkilometer	=vehicledeparture.getStartKilometre()+vehicleArrival.getEndKilometer();
			totalkilometer	= vehicleArrival.getEndKilometer()-vehicledeparture.getStartKilometre();
			log.info("totalkilometer-----------------"+totalkilometer);
		}
		else {
			log.info("End kilometer is less than start kilometer-----------------");
			totalkilometer=0.0;
			vehicleArrival.setEndKilometer(0d);
			Util.addWarn("Please Enter EndKilometer greater than Started Kilometer");
			RequestContext.getCurrentInstance().update("growls");
		}
		
	}

	
public void getlastendKilometerofArrivalVehicle() {
		
		Long vehicleid= vehicledeparture.getVehicleRegister().getId();
		
		log.info("vehicleidddd"+ vehicleid);
		try {
			
			String url = serverURL + appPreference.getOperationApiUrl()
					+ "/vehicleregister/getLastKilometerofVehicleById/" +vehicleid;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("valueeee" + baseDTO.getResponseContent());

					lastkilomaeter = mapper.readValue(jsonValue, new TypeReference<Double>() {
					});
					vehicledeparture.setStartKilometre(lastkilomaeter);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception exp) {
			log.error("<--- Exception in getlastendKilometerofArrivalVehicle() --->", exp);
		}
		
		
	}
}
