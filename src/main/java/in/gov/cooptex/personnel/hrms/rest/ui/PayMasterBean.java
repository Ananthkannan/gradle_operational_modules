package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.hrms.dto.PayMasterDTO;
import in.gov.cooptex.personnel.hrms.rest.ui.EmployeeTransferDetailsBean.Resource;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("payMasterBean")
@Scope("session")
public class PayMasterBean {

	@Getter
	@Setter
	public Resource resource;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse, url;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	EntityMaster headOrRegionFrom;

	@Getter
	@Setter
	List<EntityMaster> headRegionalOfficeList;

	@Getter
	@Setter
	EntityMaster entity;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeMasterList;

	@Getter
	@Setter
	EntityTypeMaster entityTypeMaster;

	@Getter
	@Setter
	Department department;

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList;

	@Getter
	@Setter
	SectionMaster section;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<Long> yearList = new ArrayList<>();

	@Getter
	@Setter
	Map<Integer, String> monthList = new HashMap<>();

	@Getter
	@Setter
	Integer month;

	@Getter
	@Setter
	Long year;
	
	@Getter
	@Setter
	PayMasterDTO payMasterDTO = new PayMasterDTO();
	
	@Getter
	@Setter
	List<PayMasterDTO> payMasterDTOList = new ArrayList<>();

	@PostConstruct
	public void init() {
		clear();
		loadMasterValues();
		loadMonths();
		loadYearByEmployee();
	}

	public void loadMonths() {
		log.info("Server url...." + AppUtil.getPortalServerURL() + "/payroll/getmonths/");
		BaseDTO response = httpService.get(AppUtil.getPortalServerURL() + "/payroll/getmonths");
		if (response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
			return;
		}
		try {
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			monthList = mapper.readValue(jsonResponse, new TypeReference<Map<Integer, String>>() {
			});
			log.info("month list");
		} catch (JsonProcessingException jpEx) {
			log.info("Exception while parsing ...", jpEx);
		} catch (IOException e) {
			log.info("IOException occured ... ", e);
		}

	}

	public void loadYearByEmployee() {
		EmployeeMaster master = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
		log.info("Server url...." + AppUtil.getPortalServerURL() + "/payroll/getyearbyemp/" + master.getId());
		BaseDTO response = httpService.get(AppUtil.getPortalServerURL() + "/payroll/getyearbyemp/" + master.getId());
		if (response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
			return;
		}
		try {
			mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			yearList = mapper.readValue(jsonResponse, new TypeReference<List<Long>>() {
			});
			log.info("yearList..." + yearList);
		} catch (JsonProcessingException jpEx) {
			log.info("Exception while parsing ...", jpEx);
		} catch (IOException e) {
			log.info("IOException occured ... ", e);
		}

	}

	private void loadMasterValues() {
		headRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
		entityTypeMasterList = commonDataService.getAllEntityType();
		departmentList = commonDataService.getDepartment();

	}

	private void clear() {
		headRegionalOfficeList = new ArrayList<>();
		entityTypeMasterList = new ArrayList<>();
		departmentList = new ArrayList<>();
		employeeList = new ArrayList<>();
		sectionMasterList = new ArrayList<>();

		headOrRegionFrom = new EntityMaster();
		entity = new EntityMaster();
		entityTypeMaster = new EntityTypeMaster();
		department = new Department();
		section = new SectionMaster();
		employeeMaster = new EmployeeMaster();

	}

	public void onEntityOfficeChange() {

		entityTypeMaster = null;
		entity = null;
		if (headOrRegionFrom.getId() != null) {
			headOrRegionFrom.setEntityTypeMaster(commonDataService.getEntityType(headOrRegionFrom.getId()));
		}
		if (Objects.isNull(headOrRegionFrom)) {
			department = new Department();
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		} else if (headOrRegionFrom.getEntityTypeMaster() != null
				&& headOrRegionFrom.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
			log.info("Selected Head office code is :" + headOrRegionFrom.getEntityTypeMaster().getEntityCode());
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		} else {
			if (headOrRegionFrom.getEntityTypeMaster() != null) {
				log.info("Selected Regional office code is :" + headOrRegionFrom.getEntityTypeMaster().getEntityCode());
			}
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		}
	}

	public void onEntityTypeChange() {
		log.info(":: Entity type change ::");
		try {
			if (entityTypeMaster != null) {
				Long entityTypeId = entityTypeMaster.getId();
				String url = AppUtil.getPortalServerURL() + "/entitymaster/getbyid/" + entityTypeId;
				log.info("Get Entity Masters on Entity Type Change URL :: " + url);
				BaseDTO entityTypeDTO = httpService.get(url);
				if (entityTypeDTO == null) {
					log.error("Get Entity Masters on Entity Type Change Response null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				} else {
					jsonResponse = mapper.writeValueAsString(entityTypeDTO.getResponseContent());
					entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					if (entityMasterList == null) {
						log.info("Get Entity Masters on Entity Type Change List null :: ");
					} else {
						log.info("Get Entity Masters on Entity Type Change List Size :: {} ", entityMasterList.size());
					}
				}
			} else {
				log.error("Get Entity Masters on Entity Type Change Id not found");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error("Exception While Load Entity Master on Entity Type Change ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onEntityDepartmentChange() {
		BaseDTO baseDto = new BaseDTO();
		log.info("onchangeDepartment called..");
		try {
			if (department.getId() != null) {

				String resource = AppUtil.getPortalServerURL() + "/sectionmaster/getsectionbydepartmentid/"
						+ department.getId();
				log.info("onchangeDepartment Request URL ::::::::::" + resource);
				baseDto = httpService.get(resource);
				if (Objects.isNull(baseDto)) {
					log.info("Section baseDTO response is  :" + baseDto);
				}
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
				if (Objects.isNull(sectionMasterList) || sectionMasterList.isEmpty()) {
					log.info(":: sectionMaster list size is :" + sectionMasterList.size());
					sectionMasterList = new ArrayList<>();
				} else {
					log.info(":: sectionMaster list size is :" + sectionMasterList.size());
				}
			}
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}
	}

	public void onChangeSection() {

		log.info("onchangeDepartment called..");
		// Long entityId = null, departmentId = null, sectionID = null;
		try {
			employeeList = new ArrayList<EmployeeMaster>();

			// if (Objects.isNull(department.getId())) {
			// log.info("Selected department is :" + department);
			// } else {
			// departmentId = department.getId();
			// log.info("Selected department is :" + department);
			// }
			// if (Objects.isNull(section.getId())) {
			// log.info("Selected section is :" + section);
			// } else {
			// sectionID = section.getId();
			// log.info("Selected section is :" + section);
			// }
			if (entity.getId() != null && department.getId() != null && section.getId() != null) {

				employeeList = commonDataService.getEmployeeByEntityDeptSectionId(entityTypeMaster.getId(), department.getId(),
						section.getId());
				
//				employeeList = commonDataService.getEmployeeByEntityDeptSectionId(2l, 2l,
//						1l);
				
			}

			if (Objects.isNull(employeeList) || employeeList.isEmpty()) {
				log.info(":: Employee list size is :" + employeeList.size());
				employeeList = new ArrayList<>();
			} else {
				log.info(":: Employee list size is :" + employeeList.size());
			}
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}
	}

	public void generatePayMaster() {

		BaseDTO baseDto = new BaseDTO();
		log.info("generatePayMaster called..");
		try {
			String resource = AppUtil.getPortalServerURL() + "/paymastercontroller/getpaymasterdetails/"
					+ employeeMaster.getId() + "/" + month + "/" + year;
			log.info("onchangeDepartment Request URL ::::::::::" + resource);
			baseDto = httpService.get(resource);
			if (Objects.isNull(baseDto)) {
				log.info("Section baseDTO response is  :" + baseDto);
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
			payMasterDTO = mapper.readValue(jsonResponse, PayMasterDTO.class);
			
			if(payMasterDTO != null) {
				
			}
			
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}

	}

}
