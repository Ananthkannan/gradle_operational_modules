package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.MasterSearchDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("districtMasterBean")
public class DistrictMasterBean {

	private final String LIST_PAGE_URL = "/pages/masters/district/listDistrictMaster.xhtml?faces-redirect=true;";

	private final String CREATE_PAGE_URL = "/pages/masters/district/createDistrictMaster.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/district/viewDistrictMaster.xhtml?faces-redirect=true;";

	public String gotoPage(String URL) {

		log.info("Goto Page [" + URL + "]");

		if (URL.equalsIgnoreCase("LIST")) {
			return gotoListPage();
		} else if (URL.equalsIgnoreCase("ADD")) {
			return gotoAddPage();
		} else if (URL.equalsIgnoreCase("EDIT")) {
			return gotoEditPage();
		} else if (URL.equalsIgnoreCase("VIEW")) {
			return gotoViewPage();
		} else {
			return null;
		}

	}

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	boolean disableAddButton = false;
	
	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	LazyDataModel<MasterSearchDTO> districtMasterLazyList;

	@Getter
	@Setter
	MasterSearchDTO masterSearchDTO;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	Integer listSize;
	
	
	@Getter
	@Setter
	Boolean messageflag = true;

	private String gotoListPage() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	private String gotoAddPage() {

		log.info("gotoAddPage");
        messageflag=false;
		districtMaster = new DistrictMaster();
		districtMaster.setStatus(true);
		countryMaster = null;
		countryMasterList = commonDataService.loadActiveCountries();

		return CREATE_PAGE_URL;
	}

	private String gotoViewPage() {

		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_DISTRICT_MASTER.getCode());
			return null;
		}

		districtMaster = getDistrictMasterById(masterSearchDTO.getId());

		return VIEW_PAGE_URL;
	}

	private String gotoEditPage() {
		messageflag=true;
		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}

		districtMaster = getDistrictMasterById(masterSearchDTO.getId());
		if (districtMaster != null) {
			CountryMaster country = new CountryMaster();
			Integer countries = masterSearchDTO.getCountryCodeOrName().indexOf("/");
			String counryId = masterSearchDTO.getCountryCodeOrName().substring(countries + 1);
			country.setName(counryId);
			countryMaster = getCountryView(country);
			countryMasterList = commonDataService.loadActiveCountries();
		}
		stateMasterList = commonDataService.loadActiveStates(countryMaster.getId());

		return CREATE_PAGE_URL;

	}

	public DistrictMaster getDistrictMasterById(Long id) {
		String URL = appPreference.getPortalServerURL() + "/districtMaster/get/" + id;
		DistrictMaster districtMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				districtMaster = mapper.readValue(jsonValue, DistrictMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return districtMaster;
	}

	/*******************************************************************************************************/

	public void loadLazyList() {

		districtMasterLazyList = new LazyDataModel<MasterSearchDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<MasterSearchDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<MasterSearchDTO> data = new ArrayList<MasterSearchDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<MasterSearchDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(MasterSearchDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MasterSearchDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<MasterSearchDTO> responseList = (List<MasterSearchDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (MasterSearchDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		masterSearchDTO = ((MasterSearchDTO) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		MasterSearchDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = appPreference.getPortalServerURL() + "/districtMaster/list";

		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	private MasterSearchDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		MasterSearchDTO request = new MasterSearchDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("districtCodeOrName")) {
				log.info("districtCodeOrName : " + value);
				request.setDistrictCodeOrName(value);
			}

			if (entry.getKey().equals("stateCodeOrName")) {
				log.info("stateCodeOrName : " + value);
				request.setStateCodeOrName(value);
			}

			if (entry.getKey().equals("countryCodeOrName")) {
				log.info("countryCodeOrName : " + value);
				request.setCountryCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				log.info("status : " + value);
				request.setStatus(Boolean.valueOf(value));
			}

		}

		return request;
	}

	/*******************************************************************************************************/

	@Getter
	@Setter
	CountryMaster countryMaster = new CountryMaster();

	@Getter
	@Setter
	DistrictMaster districtMaster;

	@Getter
	@Setter
	List<CountryMaster> countryMasterList = new ArrayList<CountryMaster>();

	@Getter
	@Setter
	List<StateMaster> stateMasterList = new ArrayList<StateMaster>();

	@PostConstruct
	public void init() {
		log.info(" DistrictMasterBean Init");
	}

	public void onCountryChange() {
		log.info("onCountryChange() - called ");
		stateMasterList = commonDataService.loadActiveStates(countryMaster.getId());
	}

	public void onStateChange() {
		log.info("onStateChange() - called ");
	}
	
	
	private boolean checkDuplicateDistrict() {
		boolean valid=true;
//		if(districtMaster.getCode()!=null  && districtMaster.getCode().length()>0){
//			if(districtMaster.getCode().length()>=2 ) {
//				
//			}else {
//				AppUtil.addError("Code minimumm 2 characters");
//				return false;
//			}
//		}
		
		if(districtMaster.getName()!=null  && districtMaster.getName().length()>0){
			if(districtMaster.getName().length()>=5 ) {
				
			}else {
				AppUtil.addError("District Name 5 characters");
				return false;
			}
		}
		
		if(districtMaster.getLdistrictName()!=null  && districtMaster.getLdistrictName().length()>0){
			if(districtMaster.getLdistrictName().length()>=5 ) {
				
			}else {
				AppUtil.addError("District Name (In Tamil)  minimumm 5 characters");
				return false;
			}
		}

			if(districtMaster.getId()!=null && districtMaster.getId()>0){
				return valid;
			}
			
			String URL = appPreference.getPortalServerURL() + "/districtMaster/checkDuplicateDistrict";
			BaseDTO baseDTO = httpService.post(URL, districtMaster);
			if(baseDTO.getStatusCode()==1){
				AppUtil.addError("District Code alredy exists ");
				return false;
			}
	
			return valid;
	}

	public String create() { 

		log.info("create method is executing..");

		String URL = appPreference.getPortalServerURL() + "/districtMaster/create";

		try {
		if(checkDuplicateDistrict()){
			BaseDTO baseDTO = httpService.post(URL, districtMaster);

			if (baseDTO != null) {
				if (baseDTO.getMessage()!=null && baseDTO.getMessage().equalsIgnoreCase("SUCCESS")) {
					log.info("Saved successfully");
					if(messageflag.equals(false)) {
					errorMap.notify(ErrorDescription.DISTRICT_SAVED_SUCCESSFULLY.getCode());
					}
					if(messageflag.equals(true)) {
						errorMap.notify(ErrorDescription.DISTRICT_UPDATED_SUCCESSFULLY.getErrorCode());
						
					}
					return LIST_PAGE_URL;
				}else{
					errorMap.notify(ErrorDescription.DISTRICT_UPDATED_SUCCESSFULLY.getErrorCode());
					return LIST_PAGE_URL;
				// return showListQualificationPage();
				}
			}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return null;
	}

	public void processDelete() {
		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_DISTRICT_MASTER.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {

		String URL = appPreference.getPortalServerURL() + "/districtMaster/delete/" + masterSearchDTO.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.DISTRICT_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return gotoPage("LIST");
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	private CountryMaster getCountryView(CountryMaster countryMaster) {
		log.info("<<<< ----------Start countryMasterBean . getCountryView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + "/country/getcountry";
			baseDTO = httpService.post(url, countryMaster);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				countryMaster = mapper.readValue(jsonResponse, new TypeReference<CountryMaster>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (countryMaster != null)
			log.info("countrymaster getCountryView" + countryMaster);
		else
			log.info("countrymaster getCountryView" + countryMaster);

		log.info("<<<< ----------End CountryMasterBean . getCountryView ------- >>>>");
		return countryMaster;
	}

}
