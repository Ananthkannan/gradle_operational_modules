package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AppConfig;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.production.model.ProductDesignTarget;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class AppConfigBean {

	private final String CREATE_APPCONFIG_PAGE = "/pages/masters/config/createAppConfig.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_LIST_URL = "/pages/masters/config/listAppConfig.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_EDIT_URL = "/pages/masters/config/editAppConfig.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/masters/config/viewAppConfig.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	AppConfig appConfig;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<AppConfig> appConfigList = new ArrayList<AppConfig>();
	
	@Getter	@Setter
	LazyDataModel<AppConfig> appConfigLazyList;
	
	@Getter	@Setter
	AppConfig appConfigLazy;

	@Getter
	@Setter
	private int appConfigListSize;

	boolean forceLogin = false;
	
	@Getter	@Setter
	Boolean editButtonFlag;
	
	@Getter	@Setter
	Boolean addButtonFlag;

	@Getter	@Setter
	int size;
	
	@Getter	@Setter
	String secondPassword;
	
	@Getter	@Setter
	Boolean userAuthentication;
	
	@Getter	@Setter
	Boolean appConfigKeyVerifyFlag;
	@PostConstruct
	public void init() {
		log.info(" AppConfigBean init --->" + serverURL);
		editButtonFlag = false;
		addButtonFlag = true;
		userAuthentication = false;
		appConfigKeyVerifyFlag = false;
		loadValues();
		addAppConfig();
//		getAllAppConfigList();
		loadAppConfigLazyList();
		getAppConfigView(appConfig);
	}

	public String showList() {
		log.info("<---------- Loading AppConfigBean showList list page---------->");
		loadValues();
//		getAllAppConfigList();
		loadAppConfigLazyList();
		return INPUT_FORM_LIST_URL;
	}

	public String addAppConfig() {
		log.info("<---- Start AppConfigBean.addAppConfig ---->");
		try {
			action = "ADD";
			appConfig = new AppConfig();

			log.info("<---- End AppConfig.addAppConfig ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_APPCONFIG_PAGE;

	}

	private void getAllAppConfigList() {
		log.info("<<<< ----------Start appConfigBean . loadAllUserList ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/appConfig/getall";
			baseDTO = httpService.get(url);
			appConfig = new AppConfig();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				appConfigList = mapper.readValue(jsonResponse, new TypeReference<List<AppConfig>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (appConfigList != null) {
			appConfigListSize = appConfigList.size();
		} else {
			appConfigListSize = 0;
		}
		log.info("<<<< ----------End AppConfigBean . getAllAppConfigList ------- >>>>");
	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			appConfig = new AppConfig();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		appConfig = new AppConfig();
		editButtonFlag = false;
		addButtonFlag = true;
		return INPUT_FORM_LIST_URL;

	}

	public String submit() {
		log.info("...... AppConfigBean submit begin ....");
		try {
			verifyAppConfigKey();
			if(appConfigKeyVerifyFlag == false) {
//				errorMap.notify(ErrorDescription.APP_CONFIG_KEY_ALREADY_EXIST.getErrorCode());
				log.info("App key already exist");
			}else {
				BaseDTO baseDTO = null;
				baseDTO = httpService.post(serverURL + "/appConfig/create", appConfig);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("AppConfigBean saved successfully");
						showList();
						errorMap.notify(ErrorDescription.APP_CONFIG_SAVE_SUCCESS.getErrorCode());
						return INPUT_FORM_LIST_URL;
					} else {
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
								+ baseDTO.getErrorDescription());
						errorMap.notify(baseDTO.getStatusCode());
					   return null;
					}
				}
			}
		} catch (Exception e) {
			log.error("Error while creating AppConfig" + e);
		}
		log.info("...... AppConfigBean submit ended ....");
		
		return null;
	}
	
	public void verifyAppConfigKey() {
		try {
			log.info("...... AppConfigBean submit begin ...."+appConfig.getAppKey());
			BaseDTO baseDTO = null;
			AppConfig appConfigReturn;
			baseDTO = httpService.get(serverURL + "/appConfig/verifyAppConfigKey/"+appConfig.getAppKey());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("verifyAppConfigKey Success Response ");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					appConfigReturn = mapper.readValue(jsonResponse, new TypeReference<AppConfig>() {
					});
					if(appConfigReturn != null) {
						appConfigKeyVerifyFlag = false;
						errorMap.notify(ErrorDescription.APP_CONFIG_KEY_ALREADY_EXIST.getErrorCode());
						log.info("verifyAppConfigKey Already Exist");
					}else {
						appConfigKeyVerifyFlag = true;
						log.info("verifyAppConfigKey Not Available");
					}
					
				} else {
					log.warn("AppConfigKey Not Available fetch getStatusCode() Error");
				}
			}
		} catch (Exception e) {
			log.error("Error while verifyAppConfigKey" + e);
		}

	}
	//edit appConfig
	public String updateAppConfig() {
		log.info("...... AppConfigBean update begin ....");
		try {
			if(userAuthentication == true) {
				BaseDTO baseDTO = null;
				baseDTO = httpService.post(serverURL + "/appConfig/updateAppConfig", appConfig);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("AppConfigBean update successfully");
						errorMap.notify(ErrorDescription.APP_CONFIG_UPDATE_SUCCESS.getErrorCode());
						appConfig = new AppConfig();
						editButtonFlag = false;
						addButtonFlag = true;
						loadAppConfigLazyList();
						return INPUT_FORM_LIST_URL;
					} else {
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
								+ baseDTO.getErrorDescription());
						errorMap.notify(ErrorDescription.APP_CONFIG_UPDATE_FAILED.getErrorCode());
					   return null;
					}
				}
			}else {
				errorMap.notify(ErrorDescription.APP_CONFIG_SECOND_PWD_INVALID.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Error while creating AppConfig" + e);
		}
		log.info("...... AppConfigBean submit ended ....");
		
		return null;
	}

	public String editAppConfig() {
		log.info("<<<<< -------Start editAppConfig called ------->>>>>> ");
		action = "EDIT";

		if (appConfig == null) {
			Util.addWarn("Please select one appconfig");
			return null;
		}

		log.info("<<<<< -------End editAppConfig called ------->>>>>> ");

		return CREATE_APPCONFIG_PAGE;

	}

	public String deleteAppConfig() {
		try {
			action = "Delete";
			if (appConfig == null) {
				Util.addWarn("Please Select One appconfig");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAppConfigDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteAppConfigConfirm() {

		try {
			log.info("Delete confirmed for appconfig [" + appConfig.getAppKey() + "]");
			AppConfig appConfigDelete = new AppConfig();
			appConfigDelete.setAppKey(appConfig.getAppKey());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/appConfig/deleteAppConfig";
			baseDTO = httpService.post(url, appConfig);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("AppConfig deleted successfully....!!!");
					errorMap.notify(ErrorDescription.APP_CONFIG_DELETE_SUCCESS.getErrorCode());
					appConfig = new AppConfig(); 
					editButtonFlag = false;
					addButtonFlag = true;
					loadAppConfigLazyList();
					return INPUT_FORM_LIST_URL;
				}
//				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
//					log.info("Same user update invalidating session-------->");
//					forceLogin = true;
//					return forceLogin();
//				}
				else {
					log.info("Error occured in Delete AppConfig ... ");
					errorMap.notify(ErrorDescription.APP_CONFIG_DELETE_FAILED.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String clear() {
		log.info("<---- Start appConfigBean . clear ----->");
//		getAllAppConfigList();
		loadAppConfigLazyList();
		appConfig = new AppConfig();
		editButtonFlag = false;
		addButtonFlag = true;
		log.info("<---- End AppConfigBean . clear ----->");

		return INPUT_FORM_LIST_URL;

	}

	public String backPage() {

		showList();
		return INPUT_FORM_LIST_URL;
	}

	public String viewAppConfig() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (appConfig == null) {
			Util.addWarn("Please Select One AppConfig");
			return null;
		}
		return INPUT_FORM_VIEW_URL;

	}

	private void getAppConfigView(AppConfig appConfig) {
		log.info("<<<< ----------Start appConfigBean . getAppConfigView ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/appConfig/getappconfig";
			baseDTO = httpService.post(url, appConfig);
			appConfig = new AppConfig();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				appConfig = mapper.readValue(jsonResponse, new TypeReference<AppConfig>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (appConfig != null)
			log.info("appconfig getAppConfigView" + appConfig);
		else
			log.info("appconfig getAppConfigView" + appConfig);

		log.info("<<<< ----------End AppConfigBean . getAppConfigView ------- >>>>");
	}

	
	public void loadAppConfigLazyList() {
		log.info("<--- loadLazy productDesignAchievement RequirementList ---> ");
			userAuthentication = false;
			appConfigLazyList = new LazyDataModel<AppConfig>() {
			
				private static final long serialVersionUID = 1213111804540391535L;

			@Override
			public List<AppConfig> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<AppConfig> data = new ArrayList<AppConfig>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<AppConfig>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazy AppConfig()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(AppConfig res) {
				return res != null ? res.getAppKey() : null;
			}

			@Override
			public AppConfig getRowData(String rowKey) {
				List<AppConfig> responseList = (List<AppConfig>) getWrappedData();
				try {
//					Long value = Long.valueOf(rowKey);
					for (AppConfig res : responseList) {
						if (res.getAppKey().equals(rowKey)) {
							appConfigLazy = res;
							return res;
						}
					}
				}catch(Exception see) {}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		appConfigLazy = new AppConfig();

		BaseDTO baseDTO = new BaseDTO();
		

		AppConfig yarnRequirement = new AppConfig();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		yarnRequirement.setPaginationDTO(paginationDTO);

		yarnRequirement.setFilters(filters);

		try {
			String URL = serverURL +  "/appConfig/searchDataLazy";
			baseDTO = httpService.post(URL, yarnRequirement);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}
	
	public void onRowSelect(SelectEvent event) {
		editButtonFlag = true;
		addButtonFlag = false;
	}
	public String validateUserPassword() {
		log.info(":::<- validateUserPassword Called->:::"+secondPassword);
		BaseDTO baseDTO = new BaseDTO();
		userAuthentication = false;
		try {
			String url = serverURL + "/appConfig/validateUserConfigPassword/"+secondPassword;
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode()==0) {
				log.info("validateUserPassword Success");
				userAuthentication = true;
				return INPUT_FORM_EDIT_URL;
			}
			else{
				errorMap.notify(ErrorDescription.APP_CONFIG_SECOND_PWD_INVALID.getErrorCode());
				log.warn("validateUserPassword  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in validateUserPassword ", ee);
		}
		return null;
	}
	public String validateUserPasswordForDelete() {
		log.info(":::<- validateUserPassword Called->:::"+secondPassword);
		BaseDTO baseDTO = new BaseDTO();
		userAuthentication = false;
		try {
			String url = serverURL + "/appConfig/validateUserConfigPassword/"+secondPassword;
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode()==0) {
				log.info("validateUserPassword Success");
				userAuthentication = true;
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmAppConfigDelete').show();");
			}
			else{
				errorMap.notify(ErrorDescription.APP_CONFIG_SECOND_PWD_INVALID.getErrorCode());
				log.warn("validateUserPassword  Error ");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in validateUserPassword ", ee);
		}
		return null;
	}
}
