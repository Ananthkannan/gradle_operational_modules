/* ------------------------------------------------------------------------------
 *
 *  # Echarts - Column and Waterfall charts
 *
 *  Demo JS code for echarts_columns_waterfalls.html page
 *
 * ---------------------------------------------------------------------------- */


// Configure examples
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {

    // Define elements
    var columns_stacked_element = document.getElementById('columns_stacked');
    var columns_stacked_element_for_Showroom = document.getElementById('columns_stacked_showroom');
    var columns_stacked_for_product_wise_sales = document.getElementById('columns_stacked_product_wise_sales');
    
    var bars_basic_element = document.getElementById('bars_basic');
    var bars_basic_element_popup = document.getElementById('bars_basic_popup');
    
    var pie_basic_element = document.getElementById('pie_basic');
    var pie_donut_element = document.getElementById('pie_donut');
    
    var line_basic_element = document.getElementById('line_basic');
    var line_stacked_element = document.getElementById('line_stacked');
    var line_basic_element_popup = document.getElementById('line_basic_popup');
    var line_stacked_element_popup = document.getElementById('line_stacked_popup');
    
    var bars_basic_element_category_wise_Sale = document.getElementById('bars_basic_categorywise');
    var bars_basic_element_category_wise_Sale_popup = document.getElementById('bars_basic_categorywise_popup');



    //
    // Charts configuration
    //

    // Stacked columns chart
    if (columns_stacked_element) {

        // Initialize chart
        var columns_stacked = echarts.init(columns_stacked_element);


        //
        // Chart config
        //

        // Options
        var option = {
            color: ['#3398DB'],
            tooltip: {
                trigger: 'axis',
                padding: [5, 10],
                axisPointer: {
                    type: 'shadow'
                }
            },

            grid: {
                left: '4%',
                right: '4%',
                bottom: '4%',
                top: '2.5%',
                containLabel: true
            },
            xAxis: [
                {
                    name: 'All Regions',
                    nameTextStyle: {
                        lineHeight: 96,
                        fontSize: 14,   
                        color: '#2F6497',
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    nameLocation: 'center',
                    scale: true,
                    type: 'category',
                    boundaryGap: true,
                    data: ['Coimbatore', 'Cuddalore', 'Chennai', 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                    // data: ['Com', 'Cud', 'Chn', 'Mad', 'Sal', 'Tha', 'Tir',
					// 'Vel', 'Ban', 'Mum', 'Vij'],
                    axisTick: {
                        alignWithLabel: true
                    },
                    axisLabel: {
                        rotate: 45,
                        color: '#333'
                    }

                }
            ],
            yAxis: [
                {
                    name: 'Total sales in lakhs',
                    nameTextStyle: {
                        lineHeight: 136,
                        color: '#2F6497',
                        fontSize: 14,
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    axisLabel: {
                        color: '#333'
                    },
                    nameLocation: 'center',
                    type: 'value'
                }
            ],
            series: [
                {
                    name: 'Total Sales',
                    type: 'bar',
                    barWidth: '60%',
                    data: [
                        {
                            value: 100000,
                            itemStyle: {color: '#d87c7c'},
                        },
                        {
                            value: 520000,
                            itemStyle: {color: '#BF399E'},
                        },
                        {
                            value: 2000000,
                            itemStyle: {color: '#EE6868'},
                        },
                        {
                            value: 3340000,
                            itemStyle: {color: '#00897b'},
                        },
                        {
                            value: 3900000,
                            itemStyle: {color: '#2E86C1'},
                        },
                        {
                            value: 3300000,
                            itemStyle: {color: '#28B463'},
                        },
                        {
                            value: 2200000,
                            itemStyle: {color: '#ED4A7B'},
                        },
                        {
                            value: 520000,
                            itemStyle: {color: '#ff6361'},
                        },
                        {
                            value: 280000,
                            itemStyle: {color: '#f33334'},
                        },
                        {
                            value: 60000,
                            itemStyle: {color: '#58508d'},
                        },
                        {
                            value: 500000,
                            itemStyle: {color: '#003f5c'},
                        }
                    ],
                }
            ]
        };
        // use configuration item and data specified to show chart
        columns_stacked.setOption(option);
        columns_stacked.on('click', function (params) {
            $("#salesModelForShowroom").modal('show');
        });
        $('#salesModelForShowroom').on('shown.bs.modal', function () {
            if (columns_stacked_element_for_Showroom) {

                // Initialize chart
                var columns_stacked_showroom = echarts.init(columns_stacked_element_for_Showroom);


                //
                // Chart config
                //

                // Options
                var option = {
                    color: ['#3398DB'],
                    tooltip: {
                        trigger: 'axis',
                        padding: [5, 10],
                        axisPointer: {
                            type: 'shadow'
                        }
                    },

                    grid: {
                        left: '4%',
                        right: '4%',
                        bottom: '5.5%',
                        top: '4%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            interval: 0,
                            name: 'All Showrooms',
                            nameTextStyle: {
                                lineHeight: 260,
                                fontSize: 14,
                                rich: {
                                    a: {
                                        // `lineHeight` is not set, then it will
										// be 56
                                    }
                                }
                            },
                            nameLocation: 'center',
                            scale: true,
                            type: 'category',
                            boundaryGap: true,
                            // data: ['Coimbatore', 'Cuddalore', 'Chennai',
							// 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli',
							// 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                            data: ['Textiles', 'Cool Brands', 'SSK Silks And Sarees', 'The Store', 'Your Choice', 'Best Collections', 'Showroom 19', 'Adidas', 'Style and Style', 'Hifi', 'Vij',
                                'Com', 'Cud', 'Chn', 'Mad', 'Sal', 'Tha', 'Tir', 'Vel', 'Ban', 'Mum',],
                            axisTick: {
                                alignWithLabel: true
                            },
                            axisLabel: {
                                rotate: 45
                            }

                        }
                    ],
                    yAxis: [
                        {
                            name: 'Total sales in lakhs',
                            nameTextStyle: {
                                lineHeight: 126,
                                fontSize: 14,
                                rich: {
                                    a: {
                                        // `lineHeight` is not set, then it will
										// be 56
                                    }
                                }
                            },
                            nameLocation: 'center',
                            type: 'value'
                        }
                    ],
                    series: [
                        {
                            name: 'Total Sales',
                            type: 'bar',
                            barWidth: '60%',
                            data: [100000, 520000, 2000000, 3340000, 3900000, 3300000, 2200000, 520000, 280000, 60000, 500000, 2500000, 3500000, 5800000, 9800000, 5400000,
                                5400000, 6800000, 7400000, 6854545]
                        }
                    ]
                };
                // use configuration item and data specified to show chart
                columns_stacked_showroom.setOption(option);

            }
        });

    }

    // columns_stacked_for_category_wise_sales chart
    if (columns_stacked_for_product_wise_sales) {

        // Initialize chart
        var columns_stacked_product_wise_sales = echarts.init(columns_stacked_for_product_wise_sales);

        //
        // Chart config
        //

        // Options
        var option = {
            // legend: {
            // type: 'scroll'
            // },
            grid: {
                top: '3%',
                left: '8%',
                right: '4%',
                bottom: '8%',
                containLabel: true
            },
            tooltip: {
                trigger: 'axis',
                padding: [5, 10],
                axisPointer: {
                    type: 'shadow'
                }
            },
            xAxis: {
                type: 'category',
                name: 'All Regions',
                nameTextStyle: {
                    lineHeight: 46,
                    fontSize: 14,
                    color: '#7660a0',
                    rich: {
                        a: {
                            // `lineHeight` is not set, then it will be 56
                        }
                    }
                },
                nameLocation: 'center',
                data: ['Com', 'Cud', 'Chn', 'Mad', 'Sal', 'Tha', 'Tir', 'Vel', 'Ban', 'Mum', 'Vij'],
                axisTick: {
                    alignWithLabel: true
                }
            },
            yAxis: {
                name: 'Total sales in lakhs',
                nameTextStyle: {
                    lineHeight: 146,
                    fontSize: 14,
                    color: '#7660a0',
                    rich: {
                        a: {
                            // `lineHeight` is not set, then it will be 56
                        }
                    }
                },
                nameLocation: 'center',
            },
            // Declare several bar series, each will be mapped
            // to a column of dataset.source by default.
            series: [
                {
                    name: 'Total Sales',
                    type: 'bar',
                    barGap: 0,
                    barWidth: '60%',
                    data: [1000000, 520000, 2000000, 3340000, 3900000, 3300000, 2200000, 520000, 280000, 600000, 500000]
                },
                {
                    name: 'Total Products Sold',
                    type: 'bar',
                    barGap: 0,
                    barWidth: '20%',
                    data: [100000, 330000, 802000, 904000, 390500, 3302500, 228000, 525600, 280100, 500000, 500000]
                }
            ]
        };
        // use configuration item and data specified to show chart
        columns_stacked_product_wise_sales.setOption(option);
    }
        // Charts configuration

        if (bars_basic_element) {
            // Initialize chart
            var bars_basic = echarts.init(bars_basic_element);

            //
            // Chart config
            //
            var option = {

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80', '#606fa5', '#e67e22'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 40,
                    right: 10,
                    top: 55,
                    bottom: 22,
                    containLabel: true
                },

                // Add legend
                legend: {
                    type: 'scroll',
                    data: ['CASH', 'KNT', 'CREDIT', 'CONTRACT', 'BULK', 'PP', 'COMPLIEMENT'],
                    top: 10,
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    name: 'All Regions',
                    nameTextStyle: {
                        lineHeight: 120,
                        fontSize: 14,
                        color: '#606fa5',
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    nameLocation: 'center',
                    type: 'category',
                    data: ['Coimbatore', 'Cuddalore', 'Chennai', 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                    axisLabel: {
                        color: '#000'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#000'
                        }
                    },
                    axisLabel: {
                        rotate: 45
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    name: 'Total sales in lakhs',
                    nameTextStyle: {
                        lineHeight: 100,
                        fontSize: 14,
                        color: '#606fa5',
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    nameLocation: 'center',
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'CASH',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320, 410, 220, 320, 425]
                    },
                    {
                        name: 'KNT',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210, 140, 250, 650, 410]
                    },
                    {
                        name: 'CREDIT',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310, 235, 246, 544, 748]
                    },
                    {
                        name: 'CONTRACT',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410, 210, 480, 650, 458]
                    },
                    {
                        name: 'BULK',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570, 1542, 254, 658, 2947]
                    },
                    {
                        name: 'PP',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120, 1254, 999, 487, 698]
                    },
                    {
                        name: 'COMPLIEMENT',
                        type: 'bar',
                        stack: 'Television',
                        data: [120, 132, 101, 134, 290, 230, 220, 245, 254, 987, 458]
                    }
                ]
            };
            bars_basic.setOption(option);
            // use configuration item and data specified to show chart
            bars_basic.on('click', function (params) {
                $("#typeWiseSalesShowroom").modal('show');
            });
            $('#typeWiseSalesShowroom').on('shown.bs.modal', function () {
                // Basic bar chart popup
                if (bars_basic_element_popup) {

                    // Initialize chart
                    var bars_basic_popup = echarts.init(bars_basic_element_popup);

                    //
                    // Chart config
                    //
                    var option = {

                        // Define colors
                        color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80', '#606fa5', '#e67e22'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 40,
                            right: 10,
                            top: 55,
                            bottom: 18,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            type: 'scroll',
                            data: ['CASH', 'KNT', 'CREDIT', 'CONTRACT', 'BULK', 'PP', 'COMPLIEMENT'],
                            top: 10,
                            itemHeight: 8,
                            itemGap: 20
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            },
                            axisPointer: {
                                type: 'shadow',
                                shadowStyle: {
                                    color: 'rgba(0,0,0,0.025)'
                                }
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            name: 'All Showrooms',
                            nameTextStyle: {
                                lineHeight: 250,
                                fontSize: 14,
                                color: '#000',
                                rich: {
                                    a: {
                                        // `lineHeight` is not set, then it will
										// be 56
                                    }
                                }
                            },
                            nameLocation: 'center',
                            type: 'category',
                            data: ['Hifi', 'Style and Style', 'Adidas', 'Showroom 19', 'Best Collections', 'Your Choice', 'The Store', 'SSK Silks And Sarees', 'Cool Brands', 'Textiles', 'Vij', 'Example 19', 'Example Awesome'],
                            axisLabel: {
                                color: '#000'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#000'
                                }
                            },
                            axisLabel: {
                                rotate: 45
                            },
                            splitLine: {
                                show: true,
                                lineStyle: {
                                    color: '#eee',
                                    type: 'dashed'
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            name: 'Total sales in lakhs',
                            nameTextStyle: {
                                lineHeight: 100,
                                fontSize: 14,
                                color: '#000',
                                rich: {
                                    a: {
                                        // `lineHeight` is not set, then it will
										// be 56
                                    }
                                }
                            },
                            nameLocation: 'center',
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: '#eee'
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'CASH',
                                type: 'bar',
                                data: [320, 332, 301, 334, 390, 330, 320, 410, 220, 320, 425, 552, 245]
                            },
                            {
                                name: 'KNT',
                                type: 'bar',
                                stack: 'Advertising',
                                data: [120, 132, 101, 134, 90, 230, 210, 140, 250, 650, 410, 321, 145]
                            },
                            {
                                name: 'CREDIT',
                                type: 'bar',
                                stack: 'Advertising',
                                data: [220, 182, 191, 234, 290, 330, 310, 235, 246, 544, 748, 845, 125]
                            },
                            {
                                name: 'CONTRACT',
                                type: 'bar',
                                stack: 'Advertising',
                                data: [150, 232, 201, 154, 190, 330, 410, 210, 480, 650, 458, 541, 236]
                            },
                            {
                                name: 'BULK',
                                type: 'bar',
                                stack: 'Advertising',
                                data: [862, 1018, 964, 1026, 1679, 1600, 1570, 1542, 254, 658, 2947, 478, 598]
                            },
                            {
                                name: 'PP',
                                type: 'bar',
                                barWidth: 10,
                                stack: 'Television',
                                data: [620, 732, 701, 734, 1090, 1130, 1120, 1254, 999, 487, 698, 548, 356]
                            },
                            {
                                name: 'COMPLIEMENT',
                                type: 'bar',
                                stack: 'Television',
                                data: [120, 132, 101, 134, 290, 230, 220, 245, 254, 987, 458, 478, 258]
                            }
                        ]
                    };
                    // use configuration item and data specified to show chart
                    bars_basic_popup.setOption(option);
                }
            });
        }
        
        //
        // Charts configuration
        //

        // Basic pie chart
        if (pie_basic_element) {

            // Initialize chart
            var pie_basic = echarts.init(pie_basic_element);

            //
            // Chart config
            //

            // Options
            var option = {

                color: [
                    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: function (params) {

                        return `${params.seriesName}<br />
                              ${params.name}: ${params.data.value} (${params.percent}%)<br />
                              ${params.data.name1}: ${params.data.value1}`;
                    }
                },
                series: [{
                    name: 'Fast Moving Product',
                    type: 'pie',
                    barGap: 0,
                    barWidth: '60%',
                    data: [{
                        value: 3354,
                        name: 'Pant',
                        value1: 335,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1250,
                        name: 'Shirt',
                        value1: 35,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 2234,
                        name: 'Silk saree',
                        value1: 135,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 4135,
                        name: 'Cap',
                        value1: 835,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1548,
                        name: 'kurta',
                        value1: 535,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 4548,
                        name: 'tawel',
                        value1: 935,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 6548,
                        name: 'Shorts',
                        value1: 635,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1848,
                        name: 'Baniyan',
                        value1: 435,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1348,
                        name: 'Dhoti',
                        value1: 235,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1448,
                        name: 'Bag',
                        value1: 135,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1548,
                        name: 'Cotton Saree',
                        value1: 335,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1148,
                        name: 'Boxers',
                        value1: 115,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1618,
                        name: 'Joggers',
                        value1: 215,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1458,
                        name: 'Pullover',
                        value1: 535,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 7548,
                        name: 'T-shirt',
                        value1: 435,
                        name1: 'Total Products Sold'
                    }
                    ],
                }]
            };
            pie_basic.setOption(option);
        }

        // Basic donut chart
        if (pie_donut_element) {

            // Initialize chart
            var pie_donut = echarts.init(pie_donut_element);

            // Options
            var option = {

                // Colors
                color: [
                    '#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80',
                    '#8d98b3', '#e5cf0d', '#97b552', '#95706d', '#dc69aa',
                    '#07a2a4', '#9a7fd1', '#588dd5', '#f5994e', '#c05050',
                    '#59678c', '#c9ab00', '#7eb00a', '#6f5553', '#c14089'
                ],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    formatter: function (params) {

                        return `${params.seriesName}<br />
                              ${params.name}: ${params.data.value} (${params.percent}%)<br />
                              ${params.data.name1}: ${params.data.value1}`;
                    }
                },
                series: [{
                    name: 'Slow Moving Product',
                    type: 'pie',
                    barGap: 0,
                    barWidth: '60%',
                    radius: ['50%', '70%'],
                    center: ['50%', '47.5%'],
                    itemStyle: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#fff'
                        }
                    },
                    data: [{
                        value: 3354,
                        name: 'Formal Shirt',
                        value1: 335,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1250,
                        name: 'Formal Pant',
                        value1: 35,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 2234,
                        name: 'Casuals',
                        value1: 135,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 4135,
                        name: 'Western Suits',
                        value1: 835,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1548,
                        name: 'Skirts',
                        value1: 535,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 4548,
                        name: 'Tops',
                        value1: 935,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 6548,
                        name: 'Sweatshirt',
                        value1: 635,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1848,
                        name: 'Slim Fir Jeans',
                        value1: 435,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1348,
                        name: 'Socks',
                        value1: 235,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1448,
                        name: 'Long Sleeve T-shirt',
                        value1: 135,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1548,
                        name: 'Joggers',
                        value1: 335,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1148,
                        name: 'Boxers',
                        value1: 115,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1618,
                        name: 'Joggers',
                        value1: 215,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 1458,
                        name: 'Pullover',
                        value1: 535,
                        name1: 'Total Products Sold'
                    },
                    {
                        value: 7548,
                        name: 'T-shirt',
                        value1: 435,
                        name1: 'Total Products Sold'
                    }
                    ],
                }]
            };
            pie_donut.setOption(option);
        }
        
        // Basic line chart
        if (line_stacked_element) {

            // Initialize chart
            var line_stacked = echarts.init(line_stacked_element);


            //
            // Chart config
            //

            // Options
            var option = {

                // Define colors
                color: ['#EF5350', '#66BB6A'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 20,
                    top: 55,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Total Sales', 'Total Expenses'],
                    itemHeight: 8,
                    top: 10,
                    itemGap: 20,
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    // data: ['Manyavar', 'Divya textile showroom', 'Koskii',
					// 'Max Fashion India', 'Avirate Store', 'PSR silks and
					// saarees', 'AND Store', 'PPS Silks And Sarees',
                    // 'Indya', 'Tailorman'],
                    data: ['Coimbatore', 'Cuddalore', 'Chennai', 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                    axisLabel: {
                        rotate: 45,
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Total Sales',
                        type: 'line',
                        data: [11000, 150001, 501254, 1458523, 1254712, 6478913, 2154810, 4152110, 452001, 210020, 458795],
                        smooth: true,
                        symbolSize: 7,
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Average'
                            }]
                        },
                        itemStyle: {
                            normal: {
                                borderWidth: 2
                            }
                        }
                    },
                    {
                        name: 'Total Expenses',
                        type: 'line',
                        data: [15248, 325448, 365598, 784558, 365558, 147558, 325568, 45784, 698547, 254781, 32548],
                        smooth: true,
                        symbolSize: 7,
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Average'
                            }]
                        },
                        itemStyle: {
                            normal: {
                                borderWidth: 2
                            }
                        }
                    }
                ]
            };

            line_stacked.setOption(option);
            line_stacked.on('click', function (params) {
                $("#profitableshowroomsdrilldown").modal('show');
            });
            $('#profitableshowroomsdrilldown').on('shown.bs.modal', function () {
                // Basic bar chart popup
                if (line_basic_element_popup) {

                    // Initialize chart
                    var line_basic_popup = echarts.init(line_basic_element_popup);

                    //
                    // Chart config
                    //
                    // Options
                    var option = {

                        // Define colors
                        color: ['#EF5350', '#66BB6A'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 20,
                            top: 55,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Total Sales', 'Total Expenses'],
                            itemHeight: 8,
                            top: 10,
                            itemGap: 20,
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: ['Manyavar', 'Divya textile showroom', 'Koskii', 'Max Fashion India', 'Avirate Store', 'PSR silks and saarees', 'AND Store', 'PPS Silks And Sarees',
                                'Indya', 'Tailorman'],
                            // data: ['Coimbatore', 'Cuddalore', 'Chennai',
							// 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli',
							// 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                            axisLabel: {
                                rotate: 45,
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'Total Sales',
                                type: 'line',
                                data: [11000, 150001, 501254, 1458523, 1254712, 6478913, 2154810, 4152110, 452001, 210020, 458795],
                                smooth: true,
                                symbolSize: 7,
                                markLine: {
                                    data: [{
                                        type: 'average',
                                        name: 'Average'
                                    }]
                                },
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            },
                            {
                                name: 'Total Expenses',
                                type: 'line',
                                data: [15248, 325448, 365598, 784558, 365558, 147558, 325568, 45784, 698547, 254781, 32548],
                                smooth: true,
                                symbolSize: 7,
                                markLine: {
                                    data: [{
                                        type: 'average',
                                        name: 'Average'
                                    }]
                                },
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            }
                        ]
                    };
                    // use configuration item and data specified to show chart
                    line_basic_popup.setOption(option);
                }
            });
        }

        // Stacked lines chart
        if (line_basic_element) {

            // Initialize chart
            var line_basic = echarts.init(line_basic_element);


            //
            // Chart config
            //
            var option = {

                // Define colors
                color: ['#EF5350', '#66BB6A'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 0,
                    right: 20,
                    top: 55,
                    bottom: 0,
                    containLabel: true
                },

                // Add legend
                legend: {
                    data: ['Total Sales', 'Total Expenses'],
                    itemHeight: 8,
                    top: 10,
                    itemGap: 20,
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    }
                },

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    boundaryGap: false,
                    // data: ['Manyavar', 'Divya textile showroom', 'Koskii',
					// 'Max Fashion India', 'Avirate Store', 'PSR silks and
					// saarees', 'AND Store', 'PPS Silks And Sarees',
                    // 'Indya', 'Tailorman'],
                    data: ['Coimbatore', 'Cuddalore', 'Chennai', 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                    axisLabel: {
                        rotate: 45,
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: ['#eee']
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'Total Sales',
                        type: 'line',
                        data: [11000, 150001, 501254, 1458523, 1254712, 6478913, 2154810, 4152110, 452001, 210020, 458795],
                        smooth: true,
                        symbolSize: 7,
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Average'
                            }]
                        },
                        itemStyle: {
                            normal: {
                                borderWidth: 2
                            }
                        }
                    },
                    {
                        name: 'Total Expenses',
                        type: 'line',
                        data: [15248, 325448, 365598, 784558, 365558, 147558, 325568, 45784, 698547, 254781, 32548],
                        smooth: true,
                        symbolSize: 7,
                        markLine: {
                            data: [{
                                type: 'average',
                                name: 'Average'
                            }]
                        },
                        itemStyle: {
                            normal: {
                                borderWidth: 2
                            }
                        }
                    }
                ]
            };


            line_basic.setOption(option);
            line_basic.on('click', function (params) {
                $("#nonprofitableshowroomsdrilldown").modal('show');
            });
            $('#nonprofitableshowroomsdrilldown').on('shown.bs.modal', function () {
                // Basic bar chart popup
                if (line_stacked_element_popup) {

                    // Initialize chart
                    var line_stacked_popup = echarts.init(line_stacked_element_popup);

                    //
                    // Chart config
                    //
                    var option = {

                        // Define colors
                        color: ['#EF5350', '#66BB6A'],

                        // Global text styles
                        textStyle: {
                            fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                            fontSize: 13
                        },

                        // Chart animation duration
                        animationDuration: 750,

                        // Setup grid
                        grid: {
                            left: 0,
                            right: 20,
                            top: 55,
                            bottom: 0,
                            containLabel: true
                        },

                        // Add legend
                        legend: {
                            data: ['Total Sales', 'Total Expenses'],
                            itemHeight: 8,
                            top: 10,
                            itemGap: 20,
                        },

                        // Add tooltip
                        tooltip: {
                            trigger: 'axis',
                            backgroundColor: 'rgba(0,0,0,0.75)',
                            padding: [10, 15],
                            textStyle: {
                                fontSize: 13,
                                fontFamily: 'Roboto, sans-serif'
                            }
                        },

                        // Horizontal axis
                        xAxis: [{
                            type: 'category',
                            boundaryGap: false,
                            data: ['Hifi', 'Style and Style', 'Adidas', 'Showroom 19', 'Best Collections', 'Your Choice', 'The Store', 'SSK Silks And Sarees',
                                'Cool Brands', 'Textiles'],
                            // data: ['Coimbatore', 'Cuddalore', 'Chennai',
							// 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli',
							// 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                            axisLabel: {
                                rotate: 45,
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            }
                        }],

                        // Vertical axis
                        yAxis: [{
                            type: 'value',
                            axisLabel: {
                                color: '#333'
                            },
                            axisLine: {
                                lineStyle: {
                                    color: '#999'
                                }
                            },
                            splitLine: {
                                lineStyle: {
                                    color: ['#eee']
                                }
                            },
                            splitArea: {
                                show: true,
                                areaStyle: {
                                    color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                                }
                            }
                        }],

                        // Add series
                        series: [
                            {
                                name: 'Total Sales',
                                type: 'line',
                                data: [11000, 150001, 501254, 1458523, 1254712, 6478913, 2154810, 4152110, 452001, 210020, 458795],
                                smooth: true,
                                symbolSize: 7,
                                markLine: {
                                    data: [{
                                        type: 'average',
                                        name: 'Average'
                                    }]
                                },
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            },
                            {
                                name: 'Total Expenses',
                                type: 'line',
                                data: [15248, 325448, 365598, 784558, 365558, 147558, 325568, 45784, 698547, 254781, 32548],
                                smooth: true,
                                symbolSize: 7,
                                markLine: {
                                    data: [{
                                        type: 'average',
                                        name: 'Average'
                                    }]
                                },
                                itemStyle: {
                                    normal: {
                                        borderWidth: 2
                                    }
                                }
                            }
                        ]
                    };
                    // use configuration item and data specified to show chart
                    line_stacked_popup.setOption(option);
                }
            });
        }
        
        if (bars_basic_element_category_wise_Sale) {
            // Initialize chart
            var bars_basic_categorywise = echarts.init(bars_basic_element_category_wise_Sale);

            //
            // Chart config
            //
            var option = {

                // Define colors
                color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80', '#606fa5', '#e67e22'],

                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },

                // Chart animation duration
                animationDuration: 750,

                // Setup grid
                grid: {
                    left: 40,
                    right: 10,
                    top: 55,
                    bottom: 22,
                    containLabel: true
                },

                // Add legend
                legend: {
                    type: 'scroll',
                    data: ['A/Pure Silk', 'AJ/Half Silk', 'C/Cotton', 'D/Powerloom', 'F/Government', 'B/Polyster'],
                    top: 10,
                    itemHeight: 8,
                    itemGap: 20
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: [10, 15],
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer: {
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(0,0,0,0.025)'
                        }
                    }
                },

                // Horizontal axis
                xAxis: [{
                    name: 'All Regions',
                    nameTextStyle: {
                        lineHeight: 120,
                        fontSize: 14,
                        color: '#d67278',
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    nameLocation: 'center',
                    type: 'category',
                    data: ['Coimbatore', 'Cuddalore', 'Chennai', 'Madurai', 'Salem', 'Thanjavur', 'Tirunelveli', 'Vellore', 'Bangalore', 'Mumbai', 'Vijayawada'],
                    axisLabel: {
                        color: '#000'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#000'
                        }
                    },
                    axisLabel: {
                        rotate: 45
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: '#eee',
                            type: 'dashed'
                        }
                    }
                }],

                // Vertical axis
                yAxis: [{
                    name: 'Total sales in lakhs',
                    nameTextStyle: {
                        lineHeight: 100,
                        fontSize: 14,
                        color: '#d67278',
                        rich: {
                            a: {
                                // `lineHeight` is not set, then it will be 56
                            }
                        }
                    },
                    nameLocation: 'center',
                    type: 'value',
                    axisLabel: {
                        color: '#333'
                    },
                    axisLine: {
                        lineStyle: {
                            color: '#999'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: '#eee'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                        }
                    }
                }],

                // Add series
                series: [
                    {
                        name: 'A/Pure Silk',
                        type: 'bar',
                        data: [320, 332, 301, 334, 390, 330, 320, 410, 220, 320, 425]
                    },
                    {
                        name: 'AJ/Half Silk',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [120, 132, 101, 134, 90, 230, 210, 140, 250, 650, 410]
                    },
                    {
                        name: 'C/Cotton',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [220, 182, 191, 234, 290, 330, 310, 235, 246, 544, 748]
                    },
                    {
                        name: 'D/Powerloom',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [150, 232, 201, 154, 190, 330, 410, 210, 480, 650, 458]
                    },
                    {
                        name: 'F/Government',
                        type: 'bar',
                        stack: 'Advertising',
                        data: [862, 1018, 964, 1026, 1679, 1600, 1570, 1542, 254, 658, 2947]
                    },
                    {
                        name: 'B/Polyster',
                        type: 'bar',
                        barWidth: 10,
                        stack: 'Television',
                        data: [620, 732, 701, 734, 1090, 1130, 1120, 1254, 999, 487, 698]
                    }
                ]
            };
            bars_basic_categorywise.setOption(option);
            // use configuration item and data specified to show chart
            bars_basic_categorywise.on('click', function (params) {
               $("#categorywisesalesdrilldown").modal('show');
           });
           $('#categorywisesalesdrilldown').on('shown.bs.modal', function () {
               // Basic bar chart popup
               if (bars_basic_element_category_wise_Sale_popup) {

                   // Initialize chart
                   var bars_basic_categorywise_popup = echarts.init(bars_basic_element_category_wise_Sale_popup);

                   //
                   // Chart config
                   //
                   var option = {

                       // Define colors
                       color: ['#2ec7c9', '#b6a2de', '#5ab1ef', '#ffb980', '#d87a80', '#606fa5', '#e67e22'],

                       // Global text styles
                       textStyle: {
                           fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                           fontSize: 13
                       },

                       // Chart animation duration
                       animationDuration: 750,

                       // Setup grid
                       grid: {
                           left: 40,
                           right: 10,
                           top: 55,
                           bottom: 18,
                           containLabel: true
                       },

                       // Add legend
                       legend: {
                           type: 'scroll',
                           data: ['A/Pure Silk', 'AJ/Half Silk', 'C/Cotton', 'D/Powerloom', 'F/Government', 'B/Polyster'],
                           top: 10,
                           itemHeight: 8,
                           itemGap: 20
                       },

                       // Add tooltip
                       tooltip: {
                           trigger: 'axis',
                           backgroundColor: 'rgba(0,0,0,0.75)',
                           padding: [10, 15],
                           textStyle: {
                               fontSize: 13,
                               fontFamily: 'Roboto, sans-serif'
                           },
                           axisPointer: {
                               type: 'shadow',
                               shadowStyle: {
                                   color: 'rgba(0,0,0,0.025)'
                               }
                           }
                       },

                       // Horizontal axis
                       xAxis: [{
                           name: 'All Showrooms',
                           nameTextStyle: {
                               lineHeight: 250,
                               fontSize: 14,
                               color: '#000',
                               rich: {
                                   a: {
                                       // `lineHeight` is not set, then it will
										// be 56
                                   }
                               }
                           },
                           nameLocation: 'center',
                           type: 'category',
                           data: ['Hifi', 'Style and Style', 'Adidas', 'Showroom 19', 'Best Collections', 'Your Choice', 'The Store', 'SSK Silks And Sarees', 'Cool Brands', 'Textiles', 'Vij', 'Example 19', 'Example Awesome'],
                           axisLabel: {
                               color: '#000'
                           },
                           axisLine: {
                               lineStyle: {
                                   color: '#000'
                               }
                           },
                           axisLabel: {
                               rotate: 45
                           },
                           splitLine: {
                               show: true,
                               lineStyle: {
                                   color: '#eee',
                                   type: 'dashed'
                               }
                           }
                       }],

                       // Vertical axis
                       yAxis: [{
                           name: 'Total sales in lakhs',
                           nameTextStyle: {
                               lineHeight: 100,
                               fontSize: 14,
                               color: '#000',
                               rich: {
                                   a: {
                                       // `lineHeight` is not set, then it will
										// be 56
                                   }
                               }
                           },
                           nameLocation: 'center',
                           type: 'value',
                           axisLabel: {
                               color: '#333'
                           },
                           axisLine: {
                               lineStyle: {
                                   color: '#999'
                               }
                           },
                           splitLine: {
                               lineStyle: {
                                   color: '#eee'
                               }
                           },
                           splitArea: {
                               show: true,
                               areaStyle: {
                                   color: ['rgba(250,250,250,0.1)', 'rgba(0,0,0,0.01)']
                               }
                           }
                       }],

                       // Add series
                       series: [
                           {
                               name: 'A/Pure Silk',
                               type: 'bar',
                               data: [320, 332, 301, 334, 390, 330, 320, 410, 220, 320, 425, 552, 245]
                           },
                           {
                               name: 'AJ/Half Silk',
                               type: 'bar',
                               stack: 'Advertising',
                               data: [120, 132, 101, 134, 90, 230, 210, 140, 250, 650, 410, 321, 145]
                           },
                           {
                               name: 'C/Cotton',
                               type: 'bar',
                               stack: 'Advertising',
                               data: [220, 182, 191, 234, 290, 330, 310, 235, 246, 544, 748, 845, 125]
                           },
                           {
                               name: 'D/Powerloom',
                               type: 'bar',
                               stack: 'Advertising',
                               data: [150, 232, 201, 154, 190, 330, 410, 210, 480, 650, 458, 541, 236]
                           },
                           {
                               name: 'F/Government',
                               type: 'bar',
                               stack: 'Advertising',
                               data: [862, 1018, 964, 1026, 1679, 1600, 1570, 1542, 254, 658, 2947, 478, 598]
                           },
                           {
                               name: 'B/Polyster',
                               type: 'bar',
                               barWidth: 10,
                               stack: 'Television',
                               data: [620, 732, 701, 734, 1090, 1130, 1120, 1254, 999, 487, 698, 548, 356]
                           }
                       ]
                   };
                   // use configuration item and data specified to show chart
                   bars_basic_categorywise_popup.setOption(option);
               }
           });
        }
        
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        	  var target = $(e.target).attr("#bordered-justified-tab1") // activated
																		// tab
        	  switch (target) {
        	    case "#bordered-justified-tab1":
        	    	switch(target) {
        	    	case "#left-tab1":
        	    	columns_stacked.redraw();
        	    	$(window).trigger('resize');
        	    	break;
        	    	
        	    	case "#left-tab2":
        	    		bars_basic.redraw();
            	    	$(window).trigger('resize');
            	    	break;
        	    	}
        	    	break;
        	    case "#bordered-justified-tab2":
        	    	bars_basic.redraw();
        	      $(window).trigger('resize');
        	      break;
        	  }
        	});
});



