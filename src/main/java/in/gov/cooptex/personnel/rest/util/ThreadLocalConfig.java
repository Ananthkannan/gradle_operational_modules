package in.gov.cooptex.personnel.rest.util;

public class ThreadLocalConfig {

	public static final String TRACK_ID_HEADER = "trackId";

	private static final ThreadLocal<String> id = new ThreadLocal<String>();

	public static String getId() {
		return id.get();
	}

	public static void setId(String correlationId) {
		id.set(correlationId);
	}
}
