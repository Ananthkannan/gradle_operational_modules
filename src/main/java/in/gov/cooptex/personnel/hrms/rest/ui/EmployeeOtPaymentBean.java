package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.EmpOTPayment;
import in.gov.cooptex.admin.model.EmpOTRegister;
import in.gov.cooptex.admin.vehicle.model.FuelCoupon;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.FileMovementConfig;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.operation.fm.rest.ui.FileMovementConfigBean;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeeOtPaymentBean")
@Scope("session")
public class EmployeeOtPaymentBean {
	
	
	private static final long serialVersionUID = -7555120159391217305L;
	
	private final String CREATE_EmployeeOtPayment_MASTER_PAGE = "/pages/personnelHR/payRoll/createEmployeeOtPayment.xhtml?faces-redirect=true;";
	
	
	private final String INPUT_FORM_LIST_URL = "/pages/personnelHR/payRoll/listEmployeeOtPayment.xhtml?faces-redirect=true;";
	
	private final String INPUT_FORM_VIEW_URL = "/pages/personnelHR/payRoll/viewEmployeeOtPayment.xhtml?faces-redirect=true;";
	
	private String serverURL = null;
	
	
	@Autowired
	AppPreference appPreference;
	
	
	@Autowired
	LoginBean loginBean;
	
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	private String action;
	
	@Getter
	@Setter
	boolean disableAddButton = false;
	
	
	
	
	@Getter
	@Setter
	HashMap<String,Long> EmpOtRgisterMapByempNameAsKey=new HashMap<String,Long>(); 
	
	
	@Getter
	@Setter
	EmpOTPayment  empOTPayment;
	
	@Getter
	@Setter
	EmpOTPayment selectedEmpOTPayment;
	
	@Getter
	@Setter
	boolean  view = true;
	
	
	boolean forceLogin = false;

	
	@Getter
	@Setter
	int totalRecords = 0;

	ObjectMapper mapper;

	String jsonResponse;
	
	
	@Getter
	@Setter
	List<EmpOTPayment> employeeMasterListByUserMaster = new ArrayList<EmpOTPayment>();
	
	@Getter
	@Setter
	LazyDataModel<EmpOTPayment> empOTPaymentLazyList;
	
	
	@Getter
	@Setter
	List<EmpOTPayment> empOTPaymentList = new ArrayList<EmpOTPayment>();
	
	
	
	
	
	
	
	@PostConstruct	
	public void init() {
		log.info("the post construct method of EmployeeOtPayment is get Started");
		disableAddButton = false;
		log.info("Inside init()");
		empOTPayment = new EmpOTPayment(); 
		loadValues();
		loadValuesForCreateByUserId();
		loadLazyEmpOTPaymentList();
			
	}
	
	
	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			empOTPayment = new EmpOTPayment();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}
	
	
	private void loadValuesForCreateByUserId() {
		log.info("Inside loadValuesForCreate()");
		empOTPayment = new EmpOTPayment();
		Long empId=loginBean.getUserDetailSession().getId();
		
		UserMaster userMaster =  new UserMaster();
		userMaster.setId(empId);
		//employeeMasterListByUserMaster = commonDataService.loadActiveFileMovementAppFeatureList();
		
		try {
			log.info("Get  EmployeeMasterLList By UserMaster and userMaster Id is  [" +empId+ "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL+"/employeeOtPayment/getListOfEmployMasterByUserMasterId";
			log.info("loadValuesForCreateByUserId url==>"+url);
			baseDTO = httpService.post(url, userMaster);
			log.info(" baseDTo return From Data Base"+baseDTO.getResponseContents());
			employeeMasterListByUserMaster.clear();
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				employeeMasterListByUserMaster = mapper.readValue(jsonResponse,new TypeReference<List<EmpOTPayment>>() {
					
						});
				EmpOtRgisterMapByempNameAsKey.clear();
				for(EmpOTPayment   empOTPayment : employeeMasterListByUserMaster) {
					EmpOtRgisterMapByempNameAsKey.put(empOTPayment.getEmpMaster().getFirstName(), empOTPayment.getEmpOTRegister().getId());
				}
				
				log.info("EmpOtRgisterMapByempNameAsKey"+EmpOtRgisterMapByempNameAsKey.toString());
			} 
			else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			
			} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		} 
		
		log.info("AppconfigEnum"+AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE);
		if(AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE != null) {
			log.info("appconfigenum Enum value to string"+ AppConfigEnum.FILE_MOVEMENT_LEVEL_SIZE.toString() );
		}

}
	
	private void validateInputForEmployeeOtPayement() throws UIException {
		log.info("inside the valdation unit of EmployeOtPayement");
		if (empOTPayment == null) {
			throw new UIException("Invalid input");
		} else if (empOTPayment.getEmpMaster()== null) {
			throw new UIException("Please select Employee");
		} else if (empOTPayment.getOtDate()== null) {
			throw new UIException("Please select Date");
		} else if (empOTPayment.getOtHours() == null) {
			throw new UIException("Please Enter The Ot Hour");
		} else if (empOTPayment.getAmount() == null) {
			throw new UIException("Please Enter Amount");
		} 
	}
	
	
	public void loadLazyEmpOTPaymentList() {
		disableAddButton = false;
		log.info("<===== Starts EmpOTPaymentList Bean.loadLazyFuelCouponList======>");
		empOTPaymentLazyList = new LazyDataModel<EmpOTPayment>() {
			
			private static final long serialVersionUID = -2007656114950592854L;

			@Override
			public List<EmpOTPayment> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = serverURL + "/employeeOtPayment/getEmployeeOtPaymentBYLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get employeeOtPayment list");
					log.info("EmployeeOtPayment Total records"+ response.getTotalRecords());
					
					log.info("the object returnend from employeeOtPaymentControler"+response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						empOTPaymentList = mapper.readValue(jsonResponse,new TypeReference<List<EmpOTPayment>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} 
					else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyFuelCoupon List ...", e.getCause());
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"+ empOTPaymentLazyList.toString());
				log.info("Ends lazy load....");
				
				return empOTPaymentList ;
			}

			@Override
			public Object getRowKey(EmpOTPayment res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmpOTPayment getRowData(String rowKey) {
				try {
				for (EmpOTPayment empOTPayment : empOTPaymentList) {
					if (empOTPayment.getId().equals(Long.valueOf(rowKey))) {
						selectedEmpOTPayment = empOTPayment;
						return empOTPayment;
					}
				}				
				}
				catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
			
		};
		log.info("<===== Ends selectedFileMovementConfig Bean.loadLazyselectedFileMovementConfigList ======>");
	}

	private void showClientError(String errorMessage) {
		AppUtil.addError(errorMessage);
	}
	
	public String saveEmpOTPayment() {
		if(empOTPayment != null ) {
			
		log.info("the empOTPayment from page"+empOTPayment.toString());
		try {
			validateInputForEmployeeOtPayement();
			
		}catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
			return null;
		}
		catch (Exception e1) {
			log.error("Exception at addItem() >>>> ", e1);
		}
		try {
			EmpOTRegister empOTRegister = new  EmpOTRegister();
			
			empOTRegister.setId(EmpOtRgisterMapByempNameAsKey.get(empOTPayment.getEmpMaster().getFirstName()));
			empOTPayment.setEmpOTRegister(empOTRegister);
			empOTPayment.setActiveStatus(true);
			log.info("...... FuelCoupon submit begin ...."+empOTRegister.toString());
			BaseDTO baseDTO = null;
			String url = serverURL  +"/employeeOtPayment/createOrUpdateEmployeeOtPayment";
			baseDTO = httpService.post(url, empOTPayment);
			
			log.info(" baseDTo return From Data Base"+baseDTO.toString());
			
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("empOTPayment is saved successfully");
						
					errorMap.notify(baseDTO.getStatusCode());
					
				}
			}else {
				log.info("base dto is empty from data base for empOTPayment");
			}
		} catch (Exception e) {
			log.error("Error while creating CountryMaster" + e);
		}
	
		log.info("...... fileMovementConfiglist submit ended ....");
	
	}

		return INPUT_FORM_LIST_URL;
	}
	
	public String showEmployeeOtListPage() {
		
		loadLazyEmpOTPaymentList();
		
		return INPUT_FORM_LIST_URL;
	}
	
	
	public String goToEmployeeOtCreatePage() {
		
		loadValuesForCreateByUserId();
		return CREATE_EmployeeOtPayment_MASTER_PAGE;
	}
	
	
	public String gotoEmployeeOtViewPage() {
		
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (empOTPayment == null) {
			Util.addWarn("Please Select one Employee Data");
			return null;
		}
		disableAddButton = false;
		return INPUT_FORM_VIEW_URL;
		
	}
	
	
	public String editEmployeeOt() {
		log.info("<<<<< -------Start editEmployeeOt called ------->>>>>> ");
		action = "EDIT";

		if (empOTPayment == null) {
			Util.addWarn("Please select one Fuel Coupon");
			return null;
		}

		
		log.info("<<<<< -------End editEmployeeOt called ------->>>>>> ");
		disableAddButton = false;
		return CREATE_EmployeeOtPayment_MASTER_PAGE;

	}
	
	public String deletetEmployeeOt() {
		try {
			action = "Delete";
			if (empOTPayment == null) {
				Util.addWarn("Please Select one Employee Data");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEmployeeOtDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		disableAddButton = false;
		return null;
	}
	
	
	
	
	public String deletetEmployeeOtConfirm() {

		try {
			log.info("Delete deletetEmployeeOtConfirm started the id is [" +empOTPayment.getId()  + "]");
			EmpOTPayment empOTPaymentId = new EmpOTPayment();
			empOTPaymentId.setId(empOTPayment.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL  + "/employeeOtPayment/deleteEmployeeOtPaymentById";
			baseDTO = httpService.post(url, empOTPaymentId);
			
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("Country deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					empOTPayment = new EmpOTPayment();
				}
				if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;

					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting EmployeeOtConfirm  ....", e);
		}
		return null;
	}
	
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	
	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		empOTPayment = ((EmpOTPayment) event.getObject());
		disableAddButton = true;
	}
	
	
	public String clear() {
		log.info("<---- Start countryMasterBean . clear ----->");
		loadLazyEmpOTPaymentList();
		selectedEmpOTPayment = new EmpOTPayment();
		log.info("<---- End CountryMasterBean . clear ----->");
		disableAddButton = false;
		return INPUT_FORM_LIST_URL;

	}
	
	
	
}
