package in.gov.cooptex.personnel.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.filemovement.model.FileMovementFiles;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.TrainingMaterialDetails;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("trainingMaterialDetailsBean")
@Scope("session")
@Log4j2
public class TrainingMaterialDetailsBean {

	final String LIST_PAGE = "/pages/document/listTrainingMaterialDetails.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createTrainingMaterialDetails.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String TRAINING_MATERIAL_DETAILS_URL = AppUtil.getPortalServerURL() + "/trainingmaterialdetails";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	TrainingMaterialDetails trainingMaterialDetails, selectedTrainingMaterialDetails;

	@Getter
	@Setter
	LazyDataModel<TrainingMaterialDetails> lazyTrainingMaterialDetailsList;

	List<TrainingMaterialDetails> trainingMaterialDetailsList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Setter
	@Getter
	private StreamedContent file;

	public String showTrainingMaterialDetailsListPage() {
		log.info("<==== Starts TrainingMaterialDetailsBean.showTrainingMaterialDetailsListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		trainingMaterialDetails = new TrainingMaterialDetails();
		selectedTrainingMaterialDetails = new TrainingMaterialDetails();
		loadLazyTrainingMaterialDetailsList();
		log.info("<==== Ends TrainingMaterialDetailsBean.showTrainingMaterialDetailsListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyTrainingMaterialDetailsList() {
		log.info("<===== Starts TrainingMaterialDetailsBean.loadLazyTrainingMaterialDetailsList ======>");

		lazyTrainingMaterialDetailsList = new LazyDataModel<TrainingMaterialDetails>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<TrainingMaterialDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = TRAINING_MATERIAL_DETAILS_URL + "/getalltrainingmaterialdetailslistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						trainingMaterialDetailsList = mapper.readValue(jsonResponse,
								new TypeReference<List<TrainingMaterialDetails>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return trainingMaterialDetailsList;
			}

			@Override
			public Object getRowKey(TrainingMaterialDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TrainingMaterialDetails getRowData(String rowKey) {
				for (TrainingMaterialDetails trainingMaterialDetails : trainingMaterialDetailsList) {
					if (trainingMaterialDetails.getId().equals(Long.valueOf(rowKey))) {
						selectedTrainingMaterialDetails = trainingMaterialDetails;
						return trainingMaterialDetails;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends TrainingMaterialDetailsBean.loadLazyTrainingMaterialDetailsList ======>");
	}

/*	public String showTrainingMaterialDetailsListPageAction() {
		log.info("<===== Starts TrainingMaterialDetailsBean.showTrainingMaterialDetailsListPageAction ===========>"
				+ action);
		try {
//			if (!action.equalsIgnoreCase("create")
//					&& (selectedTrainingMaterialDetails == null || selectedTrainingMaterialDetails.getId() == null)) {
//				errorMap.notify(ErrorDescription.getError(SELECT_TRAINING_MATERIAL_DETAILS).getCode());
//				return null;
//			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create trainingMaterialDetails called..");
				trainingMaterialDetails = new TrainingMaterialDetails();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit trainingMaterialDetails called..");
				String url = TRAINING_MATERIAL_DETAILS_URL + "/getbyid/" + selectedTrainingMaterialDetails.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					trainingMaterialDetails = mapper.readValue(jsonResponse,
							new TypeReference<TrainingMaterialDetails>() {
							});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete trainingMaterialDetails called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTrainingMaterialDetailsDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showTrainingMaterialDetailsListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends TrainingMaterialDetailsBean.showTrainingMaterialDetailsListPageAction ===========>"
				+ action);
		return null;
	}*/


	/*public void onRowSelect(SelectEvent event) {
		log.info("<===Starts TrainingMaterialDetailsBean.onRowSelect ========>" + event);
		selectedTrainingMaterialDetails = ((TrainingMaterialDetails) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends TrainingMaterialDetailsBean.onRowSelect ========>");
	}*/
	



}
