/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.util.WeaversSocietyEnrollmentStatus;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.DNPManagerDTO;
import in.gov.cooptex.core.dto.EmployeeLogNoteSignatureDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SocietyFieldVerificationDTO;
import in.gov.cooptex.core.dto.SocietyRegistrationReqDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.LoomTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.finance.dto.StaffPPPermitSearchDTO;
import in.gov.cooptex.personnel.rest.util.Util;
import in.gov.cooptex.weavers.model.SocietyRegRequestLog;
import in.gov.cooptex.weavers.model.SocietyRegRequestNote;
import in.gov.cooptex.weavers.model.SocietyRegistrationRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author ftuser
 *
 */
@Service("societyRegistrationRequestBean")
@Scope("session")
@Log4j2
public class SocietyRegistrationRequestBean {
	@Autowired
	AppPreference appPreference;

	private final String CREATE_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/createRequestForSocietyEnrollment.xhtml?faces-redirect=true;";
	private final String LIST_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/listRequestForSocietyEnrollment.xhtml?faces-redirect=true;";
	private final String VIEW_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/viewRequestForSocietyEnrollment.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	SocietyRegistrationRequest societyRegistration, selectedSocietyRegistration;

	String jsonResponse;

	@Getter
	@Setter
	SocietyRegRequestNote societyRegReqNote;

	@Getter
	@Setter
	SocietyRegRequestLog societyRegReqLog, selectedSocietyRegistrationLog;

	@Getter
	@Setter
	LazyDataModel<SocietyRegRequestLog> lazySocietyRegistrationLogList;

	@Getter
	@Setter
	List<SocietyRegRequestLog> societyRegistrationLogList = null;

	@Getter
	@Setter
	List<LoomTypeMaster> loomTypeList = null;

	@Getter
	@Setter
	LoomTypeMaster selectedLoomType;

	@Getter
	@Setter
	List<CircleMaster> circleMasterlist = null;

	@Getter
	@Setter
	CircleMaster selectedCircleMaster;

	@Getter
	@Setter
	List<EntityMaster> entityMasterlist = null;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	private UploadedFile recommendationLetter;

	@Setter
	@Getter
	String requestLeterFileName;

	@Setter
	@Getter
	String remarkscomment;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	EntityMaster selectedEntityMaster;

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean approvalButtonFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	List<EmployeeLogNoteSignatureDTO> viewLogResponseList;

	@Setter
	@Getter
	String fileName;

	@Setter
	@Getter
	private StreamedContent downloadfile;

	@Getter
	@Setter
	EmployeeMaster loggedInEmployeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	Boolean hideflag = true;

	public String showSocietyRequestListPage() {
		log.info("<==== Starts societyRegistrationRequestBean-showSocietyRequestListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		societyRegistration = new SocietyRegistrationRequest();
		selectedSocietyRegistrationLog = new SocietyRegRequestLog();
		action = "";
		lazySocietyRegistrationList();

		log.info("<==== Ends societyRegistrationRequestBean-showSocietyRequestListPage =====>");
		return LIST_SOCIETY_PAGE;
	}

	@Getter
	@Setter
	Long notificationId = 0L;

	@PostConstruct
	public void showViewListPage() {
		log.info("societyRegistrationRequestBean. showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String societyregistrationid = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			societyregistrationid = httpRequest.getParameter("societyregistrationid");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(societyregistrationid)) {
			selectedSocietyRegistrationLog = new SocietyRegRequestLog();
			selectedSocietyRegistrationLog.setSocietyRegistrationRequest(new SocietyRegistrationRequest());
			selectedSocietyRegistrationLog.getSocietyRegistrationRequest().setId(Long.valueOf(societyregistrationid));
			setNotificationId(Long.valueOf(notificationId));
			action = "View";
			societyRegisterListPageAction();
		}
		return;
	}

	public String societyRegisterListPageAction() {
		log.info("<========inside societyRegistrationRequestBean.societyRegisterListPageAction=======>");
		try {
			societyRegistration = new SocietyRegistrationRequest();
			circleMasterlist = new ArrayList<>();
			forwardToUsersList = new ArrayList<>();
			selectedEntityMaster = new EntityMaster();
			selectedLoomType = new LoomTypeMaster();
			selectedCircleMaster = new CircleMaster();
			forwardTo = new UserMaster();
			selectedSocietyRegistration = new SocietyRegistrationRequest();
			if (action.equalsIgnoreCase("Create")) {
				log.info("create Society Registration called..");
				loadActiveLoomType();
				/* loadForwardToUsers(); */
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REQUEST_FOR_SOCIETY_ENROLLMENT");
				circleMasterlist = commonDataService.loadCircles();
				// entityMasterlist = commonDataService.loadShowRoomList();
				// Load DAndP Office EntityType Entity List
				entityMasterlist = commonDataService.loadProducingRegion();
				societyRegReqLog = new SocietyRegRequestLog();
				societyRegReqNote = new SocietyRegRequestNote();
				setRequestLeterFileName("");
				loadEmployeeLoggedInUserDetails();
				return CREATE_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("View")) {
				setApprovalFlag(false);
				setApprovalButtonFlag(false);
				setRemarkscomment("");
				log.info("view Society Registration called..");
				societyRegReqValuebyId();
				// loadForwardToUsers();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REQUEST_FOR_SOCIETY_ENROLLMENT");

				Long userId = loginBean.getUserDetailSession().getId();
				log.info("login userId: " + userId);

				if (societyRegReqLog != null) {
					String stage = societyRegReqLog.getStage();
					log.info("stage: " + stage);
					if (StringUtils.isNotEmpty(stage)) {
						if (stage.equals(WeaversSocietyEnrollmentStatus.SUBMITTED)
								|| stage.equals(WeaversSocietyEnrollmentStatus.APPROVED)
								|| stage.equals(WeaversSocietyEnrollmentStatus.FINAL_APPROVED)) {
							if (societyRegReqNote != null && societyRegReqNote.getUserMaster() != null
									&& societyRegReqNote.getUserMaster().getId().equals(userId)) {
								log.info("============approval flag==========true=====");
								setApprovalFlag(true);
								setApprovalButtonFlag(true);
								if (societyRegReqNote.getFinalApproval()) {
									setApprovalFlag(false);
								}
							}
						}
					}

					if (WeaversSocietyEnrollmentStatus.FINAL_APPROVED.equals(stage)
							|| WeaversSocietyEnrollmentStatus.REJECTED.equals(stage)) {
						setApprovalFlag(false);
						setApprovalButtonFlag(false);
					}
				}

				// else {
				// log.info("============approval flag==========false=====");
				// setApprovalFlag(false);
				// setApprovalButtonFlag(false);
				// }
				setFinalapprovalFlag(societyRegReqNote != null ? societyRegReqNote.getFinalApproval() : null);
				return VIEW_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("Edit Society Registration called..");
				loadActiveLoomType();
				// loadForwardToUsers();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("REQUEST_FOR_SOCIETY_ENROLLMENT");
				circleMasterlist = commonDataService.loadCircles();
				// entityMasterlist = commonDataService.loadShowRoomList();
				// Load DAndP Office EntityType Entity List
				entityMasterlist = commonDataService.loadProducingRegion();
				societyRegReqValuebyId();
				loadEmployeeLoggedInUserDetails();
				return CREATE_SOCIETY_PAGE;
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Society Registration called.." + selectedSocietyRegistrationLog.getId());

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmScoietyregreqDelete').show();");

			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void societyRegReqValuebyId() {
		log.info("<<<<<:::::::societyRegistrationRequestBean.societyRegReqValuebyId::::::::::>>>>>");
		try {
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/getbyReqIDNotifycationID/"
					+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId() + "/"
					+ (notificationId != null ? notificationId : 0);
			log.info("after Url declared====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			SocietyRegistrationReqDTO societyRegReqDTO = new SocietyRegistrationReqDTO();

			selectedSocietyRegistration = new SocietyRegistrationRequest();
			societyRegReqNote = new SocietyRegRequestNote();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				societyRegReqDTO = mapper.readValue(jsonResponse, new TypeReference<SocietyRegistrationReqDTO>() {
				});

				SocietyRegRequestNote societyRegRequestNote = societyRegReqDTO.getSocietyRegRequestNote();

				if (societyRegRequestNote != null) {
					previousApproval = societyRegRequestNote.getFinalApproval();
					selectedSocietyRegistration = societyRegReqDTO.getSocietyRegistrationRequest();
					societyRegReqNote = societyRegRequestNote;
				}

				societyRegReqLog = societyRegReqDTO.getSocietyRegRequestLog();
				selectedEntityMaster = selectedSocietyRegistration.getEntityMaster();

				onChangeDnpOffice();
				if (action.equalsIgnoreCase("Edit")) {
					setSelectedLoomType(selectedSocietyRegistration.getLoomTypeMaster());
					setSelectedCircleMaster(selectedSocietyRegistration.getCircleMaster());
					setSelectedEntityMaster(selectedSocietyRegistration.getEntityMaster());
					setForwardTo(societyRegReqNote.getUserMaster());
					setRequestLeterFileName(selectedSocietyRegistration.getRequestLeterFilePath()
							.substring(selectedSocietyRegistration.getRequestLeterFilePath().lastIndexOf("/")));
				} else {

					ObjectMapper mapperEmployee = new ObjectMapper();
					String employeeDatajsonResponses = mapperEmployee.writeValueAsString(baseDTO.getTotalListOfData());
					viewLogResponseList = mapperEmployee.readValue(employeeDatajsonResponses,
							new TypeReference<List<EmployeeLogNoteSignatureDTO>>() {
							});
					log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());

				}
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public void loadActiveLoomType() {
		log.info("Inside loadActiveLoomType Method");
		loomTypeList = new ArrayList<>();
		BaseDTO baseDTO = new BaseDTO();
		String url = SERVER_URL + "/loomtype/getActiveLoomType";
		try {
			log.info("URL===LOOM_TYPE_LIST_URL================>" + url);
			baseDTO = httpService.get(url);
			log.info("<=========== after URL =======>" + baseDTO);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				loomTypeList = mapper.readValue(jsonResponse, new TypeReference<List<LoomTypeMaster>>() {
				});
				log.info("=============activeloomTypeList=====================" + loomTypeList.size());
			}
		} catch (Exception e) {
			log.error("Error Occured ::", e);
		}
	}

	/*
	 * public void loadForwardToUsers() { log.info("Inside loadForwardToUsers() ");
	 * BaseDTO baseDTO = null; try { String url = appPreference.getPortalServerURL()
	 * + "/user/getallforwardtousers"; baseDTO = httpService.get(url); if (baseDTO
	 * != null) { ObjectMapper mapper = new ObjectMapper(); String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); forwardToUsersList =
	 * mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() { }); }
	 * } catch (Exception e) {
	 * log.error("Exception Occured While loadForwardToUsers() :: ", e); } }
	 */

	/*
	 * public void onChangeDnpOffice() {
	 * log.info("<--- Inside onChangeDnpOffice() ---> ");
	 * 
	 * if (entityMaster == null) {
	 * log.info("<--- If D & P not selected do this ---> "); dnpInchargeName = null;
	 * return; } if (entityMaster != null) { String empFirstName =
	 * entityMaster.getEmployeeMaster().getFirstName(); String empLastName =
	 * entityMaster.getEmployeeMaster().getLastName();
	 * 
	 * dnpInchargeName = empFirstName + " " + empLastName;
	 * 
	 * log.info("<---dnpInchargeName ---> " + dnpInchargeName); } }
	 */

	public void onChangeDnpOffice() {

		try {
			log.info("onChangeDnpOffice :: selectedEntityMaster id==> " + selectedEntityMaster.getId());
			String url = AppUtil.getPortalServerURL() + "/employee/getdnpmanageroffice/" + selectedEntityMaster.getId();
			log.info("Url onChangeDnpOffice====>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				List<DNPManagerDTO> dNPManagerDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<DNPManagerDTO>>() {
						});
				if (dNPManagerDTOList != null && !dNPManagerDTOList.isEmpty()) {
					log.info("dNPManagerDTOList size==> " + dNPManagerDTOList.size());
					DNPManagerDTO dNPManagerDTO = dNPManagerDTOList.get(0);
					log.info("dNPManagerDTO==> " + dNPManagerDTO);
					EmployeeMaster empMaster = new EmployeeMaster();
					empMaster.setId(dNPManagerDTO.getEmployeeId());
					empMaster.setEmpCode(dNPManagerDTO.getEmployeeCode());
					empMaster.setFirstName(dNPManagerDTO.getFirstName());
					empMaster.setLastName(dNPManagerDTO.getLastName());
					selectedEntityMaster.setEmployeeMaster(empMaster);
				}
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}

	}

	public String saveOrUpdateSocietyRegReq() {
		log.info("inside createSocietyRequest()" + selectedSocietyRegistration.getRequestLeterFilePath());
		try {
			if (societyRegReqNote.getNote() == null || societyRegReqNote.getNote().isEmpty()) {
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_REG_NOTE_EMPTY.getCode());
				return null;
			}
			FacesContext fc = FacesContext.getCurrentInstance();
			// --selectedEntityMaster.employeeMaster.firstName
			/*
			 * if (selectedEntityMaster.getEmployeeMaster() == null ||
			 * selectedEntityMaster.getEmployeeMaster().getFirstName() =="" ||
			 * selectedEntityMaster.getEmployeeMaster().getFirstName() == null) {
			 * fc.addMessage("managername", new FacesMessage("D&P Manager Name Required"));
			 * return null; }
			 */
			if (selectedSocietyRegistration.getRequestLeterFilePath() == ""
					|| selectedSocietyRegistration.getRequestLeterFilePath() == null) {
				fc.addMessage("recLetter", new FacesMessage("Recommendation Letter Required"));
				return null;
			}

			selectedSocietyRegistration.setCircleMaster(selectedCircleMaster);
			selectedSocietyRegistration.setEntityMaster(selectedEntityMaster);
			selectedSocietyRegistration.setLoomTypeMaster(selectedLoomType);

			societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.SUBMITTED);
			selectedSocietyRegistration.setSocietyRegRequestLog(societyRegReqLog);

			societyRegReqNote.setUserMaster(forwardTo);
			selectedSocietyRegistration.setSocietyRegRequestNote(societyRegReqNote);

			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/saveOrUpdateSocietyRegReq";
			BaseDTO response = httpService.post(url, selectedSocietyRegistration);
			if (response != null && response.getStatusCode() == 0) {
				log.info("selectedSocietyRegistration saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_INSERTED_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_UPDATE_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("saveOrUpdateSocietyRegReq :: Exception==> ", e);
		}
		return LIST_SOCIETY_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SocietyRegistrationReqbean.onRowSelect ========>" + event);
		selectedSocietyRegistrationLog = ((SocietyRegRequestLog) event.getObject());
		addButtonFlag = true;
		if (selectedSocietyRegistrationLog.getStage().equalsIgnoreCase("REJECTED")) {
			editButtonFlag = false;
			deleteButtonFlag = false;
		} else {
			editButtonFlag = true;
			deleteButtonFlag = true;
		}
		viewButtonFlag = false;
		log.info("<===Ends SocietyRegReqBean.onRowSelect ========>"
				+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
	}

	public void lazySocietyRegistrationList() {
		log.info("<===== Starts Marital Ststus Bean.lazySocietyRegistrationList List ======>");
		String lazylistUrl = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/societyregistrationrequest/getallSocietyRegistrationlazy";

		lazySocietyRegistrationLogList = new LazyDataModel<SocietyRegRequestLog>() {

			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<SocietyRegRequestLog> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for lazySocietyRegistrationList List=============>" + lazylistUrl);
					BaseDTO response = httpService.post(lazylistUrl, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						societyRegistrationLogList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyRegRequestLog>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazySocietyRegistrationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load...." + societyRegistrationLogList.size());
				return societyRegistrationLogList;
			}

			@Override
			public Object getRowKey(SocietyRegRequestLog res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyRegRequestLog getRowData(String rowKey) {
				try {
					for (SocietyRegRequestLog societyRegReqLog : societyRegistrationLogList) {
						if (societyRegReqLog.getId().equals(Long.valueOf(rowKey))) {
							selectedSocietyRegistrationLog = societyRegReqLog;
							return societyRegReqLog;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Bean.lazySocietyRegistrationList List ======>");
	}

	long documentSize;

	/*
	 * public void uploadRecommendation(FileUploadEvent event) {
	 * log.info(":::::::Attachment Upload is processed::::::::"); try {
	 * log.info("<====== Attachment Upload Event =====>" + event); if (event == null
	 * || event.getFile() == null) { log.error(" Attachment Upload is null ");
	 * return; } recommendationLetter = event.getFile(); long size =
	 * recommendationLetter.getSize(); log.info("uploadSignature size==>" + size);
	 * size = size / 1000; documentSize =
	 * Long.valueOf(commonDataService.getAppKeyValue(
	 * "REQUEST_FOR_SOCIETY_ENROLLMENT_DOC_FILE_SIZE")); if (size > documentSize) {
	 * FacesContext.getCurrentInstance().getExternalContext().getFlash().
	 * setKeepMessages(true);
	 * errorMap.notify(ErrorDescription.SOCIETY_REQUEST_RECOMMANDATION_LETTER_SIZE.
	 * getErrorCode()); return; } String docPathName =
	 * commonDataService.getAppKeyValue("REQUEST_FOR_SOCIETY_ENROLLMENT_FILE_PATH");
	 * 
	 * log.info("<<<<::::::::::Request for Society Enrollment Doc Path::::::>>>> " +
	 * docPathName); Timestamp timestamp = new
	 * Timestamp(System.currentTimeMillis()); String timeStamp =
	 * Long.toString(timestamp.getTime()); String fileDetail =
	 * Util.uploadDocument(docPathName, recommendationLetter, timeStamp);
	 * log.info("fileName with path...." + fileDetail); if
	 * (fileDetail.equalsIgnoreCase("Error")) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * log.error("Same file already exist on target path"); } else {
	 * selectedSocietyRegistration.setRequestLeterFilePath(fileDetail);
	 * requestLeterFileName = recommendationLetter.getFileName();
	 * log.info(":::::::file Name:::::"+selectedSocietyRegistration.
	 * getRequestLeterFilePath());
	 * log.info(":::::::requestLeterFileName:::::"+requestLeterFileName); }
	 * errorMap.notify(ErrorDescription.
	 * SOCIETY_REQUEST_RECOMMANDATION_LETTER_UPLOAD_SUCCESS.getErrorCode());
	 * log.info(" Attachment is uploaded successfully"); } catch (Exception exp) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
	 * log.error("found exception in file upload", exp); } }
	 */

	public void uploadRecommendation(FileUploadEvent event) {
		log.info(":::::::Attachment Upload is processed::::::::");
		try {
			log.info("<====== Attachment Upload Event =====>" + event);
			if (event == null || event.getFile() == null) {
				log.error(" Attachment Upload is null ");
				return;
			}
			recommendationLetter = event.getFile();
			String type = FilenameUtils.getExtension(recommendationLetter.getFileName());
			log.info(" File Type :: " + type);
			boolean validFileType = AppUtil.isValidFileType(type, new String[] { "doc", "docx", "pdf", "xlsx", "xls" });

			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DOC_PDF_XLS_FORMAT).getCode());
				return;
			}
			long size = recommendationLetter.getSize();
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			log.info("uploadRecommendation size==>" + size);
			// documentSize =
			// Long.valueOf(commonDataService.getAppKeyValue("REQUEST_FOR_SOCIETY_ENROLLMENT_DOC_FILE_SIZE"));
			// --5 MB
			documentSize = Long.valueOf(commonDataService.getAppKeyValue("EMP_CHECKLIST_DOC_SIZE_LIMIT_IN_MB"));
			log.info("documentSize ==> " + documentSize);
			if (size > documentSize) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_RECOMMANDATION_LETTER_SIZE.getErrorCode());
				return;
			}
			String docPathName = commonDataService.getAppKeyValue("REQUEST_FOR_SOCIETY_ENROLLMENT_FILE_PATH");

			log.info("<<<<::::::::::Request for Society Enrollment Doc Path::::::>>>> " + docPathName);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String timeStamp = Long.toString(timestamp.getTime());
			String fileDetail = Util.uploadDocument(docPathName, recommendationLetter, timeStamp);
			log.info("fileName with path...." + fileDetail);
			if (fileDetail.equalsIgnoreCase("Error")) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				log.error("Same file already exist on target path");
			} else {
				selectedSocietyRegistration.setRequestLeterFilePath(fileDetail);
				requestLeterFileName = recommendationLetter.getFileName();
				log.info(":::::::file Name:::::" + selectedSocietyRegistration.getRequestLeterFilePath());
				log.info(":::::::requestLeterFileName:::::" + requestLeterFileName);
			}
			errorMap.notify(ErrorDescription.SOCIETY_REQUEST_RECOMMANDATION_LETTER_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Attachment is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void clearNote() {
		societyRegReqNote.setNote(null);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').show();");
	}

	public String deleteConfirm() {
		log.info("<===== Starts showSocietyRequestbean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/deletebysocietyRegReqById/"
					+ selectedSocietyRegistrationLog.getSocietyRegistrationRequest().getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SOCIETY_REQUEST_DELETE_SUCCESSFULLY.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmScoietyregreqDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete community ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends showSocietyRequestbean.deleteConfirm ===========>");
		return showSocietyRequestListPage();
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		societyRegReqLog.setStage(value);
		setRemarkscomment("");
	}

	public String statusUpdate() {
		try {
			log.info("inside statusUdate socreqreg------->" + selectedSocietyRegistration.getId());
			SocietyRegistrationReqDTO socRegReqDTO = new SocietyRegistrationReqDTO();
			/*
			 * if (remarkscomment == null || remarkscomment == "") {
			 * AppUtil.addError("remarks is required"); return null; }
			 */
			// societyRegReqLog.setRemarks(remarkscomment);
			socRegReqDTO.setSocietyRegRequestNote(societyRegReqNote);
			socRegReqDTO.setSocietyRegRequestLog(societyRegReqLog);
			socRegReqDTO.setSocietyRegistrationRequest(selectedSocietyRegistration);
			if (societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.REJECTED)) {
				socRegReqDTO.getSocietyRegRequestNote().setUserMaster(forwardTo);
				societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.REJECTED);
			} else {
				if (societyRegReqNote.getFinalApproval() == true && previousApproval == true
						&& societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.APPROVED)) {
					societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.FINAL_APPROVED);
					socRegReqDTO.getSocietyRegRequestNote().setUserMaster(forwardTo);
				} else if (societyRegReqNote.getFinalApproval() == true || societyRegReqNote.getFinalApproval() == false
						&& previousApproval == false
						&& societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.APPROVED)) {
					societyRegReqLog.setStage(WeaversSocietyEnrollmentStatus.APPROVED);
					socRegReqDTO.getSocietyRegRequestNote().setUserMaster(forwardTo);
				}
			}
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/societyregistrationrequest/societyRegReqForwardStage";
			log.info(":::::statusUpdate::::::Url::::::::::::::" + url);
			BaseDTO response = httpService.post(url, socRegReqDTO);
			log.info(":::::statusUpdate::::::response::::::status::::::::" + response);
			if (response != null && response.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
				log.info("::::inside Success::response:::<<<<<::::Stage:::>>>>::" + societyRegReqLog.getStage());
				if (societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.FINAL_APPROVED)
						|| societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.APPROVED)) {
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_APPROVED_SUCCESSFULLY.getCode());
				} else if (societyRegReqLog.getStage().equalsIgnoreCase(WeaversSocietyEnrollmentStatus.REJECTED)) {
					errorMap.notify(ErrorDescription.SOCIETY_REQUEST_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				log.info("::::inside Failure::response:::::");
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		return showSocietyRequestListPage();
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("<<<<<<<<<<:::::Inside loadEmployeeLoggedInUserDetails():::::>>>>>>>>>");
		try {
			loggedInEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			if (loggedInEmployeeMaster == null) {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}
	
	public void downloadIncomingFile(String downloadFilePath) {
		InputStream input = null;
		try {
			File files = new File(downloadFilePath);
			log.info("filepathhh" + downloadFilePath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			downloadfile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
					files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public void handleClose(CloseEvent event) {
		societyRegReqNote.setNote(null);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
	}

	@Getter
	@Setter
	public List<UserMaster> autoEmployeeMasterList = new ArrayList<UserMaster>();

	public List<UserMaster> loadAutoCompleteUserMaster(String query) {
		log.info("Institute Autocomplete query==>" + query);
		autoEmployeeMasterList = commonDataService.loadAutoCompleteForwardToUser(loginBean.getServerURL(), query);
		return autoEmployeeMasterList;
	}

}
