package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CommunalRoster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.GenderMaster;
import in.gov.cooptex.core.model.PriorityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("communalRosterResisterBean")
public class CommunalRosterResisterBean {
	
	private final String CREATE_COMMUNAL_ROASTER_PAGE = "/pages/masters/communalRosterResister/createCommunalRosterResister.xhtml?faces-redirect=true;";
	private final String COMMUNAL_ROASTER_LIST_URL = "/pages/masters/communalRosterResister/listCommunalRosterResister.xhtml?faces-redirect=true;";
	private final String COMMUNAL_ROASTER_VIEW_URL = "/pages/masters/communalRosterResister/viewCommunalRosterResister.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;
	
    ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	List<GenderMaster> genderMasterList = null;

	@Getter
	@Setter
	GenderMaster genderMaster = new GenderMaster();

	@Getter
	@Setter
	List<PriorityMaster> priorityMasterList = null;

	@Getter
	@Setter
	PriorityMaster priorityMaster = new PriorityMaster();

	@Getter
	@Setter
	String pageAction;

	@Getter
	@Setter
	LazyDataModel<CommunalRoster> lazyCommunalRosterModel;

	@Getter
	@Setter
	CommunalRoster communalRoster = new CommunalRoster();

	@Getter
	@Setter
	CommunalRoster selectedCommunalRoster = new CommunalRoster();

	@Getter
	@Setter
	List<Community> communityMasterList;

	@Getter
	@Setter
	String communityCode;

	@Getter
	@Setter
	Community community = new Community();

	@Getter
	@Setter
	Long rosterPoint = 0l;
	
	@Getter
	@Setter
	List<CommunalRoster> communalRosterList = new ArrayList<CommunalRoster>();
	
	@Getter
	@Setter
	String action;

	
	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	@Getter
	@Setter
	Boolean viewButtonFlag = true;
	
	
	@Getter
	@Setter
	String communityName;
	
	@Getter
	@Setter
	List<GenderMaster> newGenderMasterList = new ArrayList<GenderMaster>();

	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	
	public String init() {
		log.info("communalRosterResisterBean Init is executed.....................");
		mapper = new ObjectMapper();
		loadCommunalRosterResisterValues();
		loadLazyCommunalRosterList();
		newGenderMasterList=new ArrayList<GenderMaster>();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return COMMUNAL_ROASTER_LIST_URL;
	}

	public void loadCommunalRosterResisterValues() {
		try {
			genderMasterList = commonDataService.loadGenderMasterList();
			newGenderMasterList=new ArrayList<GenderMaster>();
			Iterator<GenderMaster> genderMasterItr=genderMasterList.iterator();
			while(genderMasterItr.hasNext())
			{
				GenderMaster genderMaster=genderMasterItr.next();
				if(genderMaster.getName().equals("Male"))
				  genderMaster.setName("General");
				else if(genderMaster.getName().equals("Female"))
					 genderMaster.setName("Women");
				newGenderMasterList.add(genderMaster);
			}
			communityMasterList = commonDataService.getAllCommunity();
			rosterPoint = getRosterPointData();
		} catch (Exception e) {
			log.error("Exception in loadCommunalRosterResisterValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public Long getRosterPointData() {
		try {
			log.info("<=getRosterPointData=>");

			String apiURL = appPreference.getPortalServerURL() + "/communal/getmaxrosterpoint";
			BaseDTO baseDTO = httpService.get(apiURL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				Long rosterPointVal = mapper.readValue(jsonValue, Long.class);
				if (rosterPointVal == null) {
					rosterPoint = 1L;
				} else {
					rosterPoint = rosterPointVal + 1;
				}
				log.info("rosterPoint==>" + rosterPoint);			

			}
		} catch (Exception e) {
			log.error("Exception in getRosterPointData  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return rosterPoint;
	}

	public String loadCommunityCode() {
		try {
			log.info("loadCommunityCode name==>" + communityName);
			Community community = commonDataService.getCommunityByName(communityName);
			if (community != null) {
				communalRoster.setCommunityCode(community.getCode());
				log.info("loadCommunityCode communityCode==>" + communityCode);
			}
		} catch (Exception e) {
			log.error("Exception in loadCommunalRosterResisterValues  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return communityCode;
	}

	public String saveCommunalRoster() {
		try {
			log.info("<=call saveUpdateBiometric=>");
			log.info("<=communalRoster=>" + communalRoster);
			communalRoster.setRosterName(communityName);
			if (rosterPoint == null) {
				errorMap.notify(ErrorDescription.ROSTER_POINT_REQUIRED.getErrorCode());
				return null;
			}
			if (communalRoster.getRosterName() == null || communalRoster.getRosterName().isEmpty()) {
				errorMap.notify(ErrorDescription.ROSTER_NAME_REQUIRED.getErrorCode());
				return null;
			}
			if (communalRoster.getCommunityCode() == null || communalRoster.getCommunityCode().isEmpty()) {
				errorMap.notify(ErrorDescription.COMMUNITY_CODE_REQUIRED.getErrorCode());
				return null;
			}
			if (communalRoster.getGender() == null || communalRoster.getGender().getId() == null) {
				errorMap.notify(ErrorDescription.JOB_APPLICATION_GENDER_IS_EMPTY.getErrorCode());
				return null;
			}
			if(communalRoster.getCommunityCode().length()>3)
			{
				errorMap.notify(ErrorDescription.ROASTER_COMMUNITY_CODE_MIN_MAX_ERROR.getErrorCode());
				return null;	
			}
			if (communalRoster.getPriority() == null || communalRoster.getPriority().isEmpty()) {
				errorMap.notify(ErrorDescription.PRIORITY_EMPTY.getErrorCode());
				return null;
			}
			communalRoster.setRosterPoint(rosterPoint);
			String url = SERVER_URL + "/communal/create";
			BaseDTO baseDTO = httpService.put(url, communalRoster);
			log.info("Save Communal Roster : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			CommunalRoster bioReg = mapper.readValue(jsonResponse, CommunalRoster.class);
			log.info("bioReg==>" + bioReg);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.COMMUNAL_ROSTER_ADDED_SUCCESSFULLY.getErrorCode());
				clear();
				loadLazyCommunalRosterList();
				return "/pages/masters/communalRosterResister/listCommunalRosterResister.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving biometric with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception in saveUpdateBiometric  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void loadLazyCommunalRosterList() {

		log.info("<=CommunalRosterResisterBean :: loadLazyCommunalRosterList=>");
		lazyCommunalRosterModel = new LazyDataModel<CommunalRoster>() {

			private static final long serialVersionUID = 1L;

			

			@Override
			public List<CommunalRoster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				CommunalRoster request = new CommunalRoster();

				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);

					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);
					String url = SERVER_URL + "/communal/lazyload/search";

					log.info("loadLazyCommunalRosterList url==>" + url);

					BaseDTO baseDTO = httpService.post(url, request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					communalRosterList = mapper.readValue(jsonResponse, new TypeReference<List<CommunalRoster>>() {
					});
					if (communalRosterList == null) {
						log.info(" :: loadLazyCommunalRosterList Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {

						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: totalRecords ::" + totalRecords);
					} else {
						log.error(":: loadLazyCommunalRosterList Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return communalRosterList;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading loadLazyCommunalRosterList ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(CommunalRoster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CommunalRoster getRowData(String rowKey) {
				List<CommunalRoster> responseList = (List<CommunalRoster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (CommunalRoster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedCommunalRoster = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public void onRowSelect() {
		addButtonFlag = false;

	}

	public void clear() {
		communityCode = null;
		rosterPoint = getRosterPointData();
		communalRoster = new CommunalRoster();
		selectedCommunalRoster = new CommunalRoster();
	}
	
	public String clearPage() {
		communityCode = null;
		addButtonFlag = true;
		rosterPoint = getRosterPointData();
		communalRoster = new CommunalRoster();
		selectedCommunalRoster = new CommunalRoster();
		loadLazyCommunalRosterList();
		return COMMUNAL_ROASTER_LIST_URL;
	}
	
	public String showCommunalRoasterData() {
		log.info("<===== Starts CommunalRosterResisterBean.showCommunalRoasterData ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedCommunalRoster == null || selectedCommunalRoster.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create GlAccountCategory called..");
				Long lastCommunalData=getRosterPointData();
				communalRoster=new CommunalRoster();
				communalRoster.setRosterPoint(lastCommunalData);
				loadCommunalRosterResisterValues();
				
				return CREATE_COMMUNAL_ROASTER_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit GlAccountCategory called..");
				String url = AppUtil.getPortalServerURL() + "/communal/get/" + selectedCommunalRoster.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					communalRoster = mapper.readValue(jsonResponse, new TypeReference<CommunalRoster>() {
					});
					return CREATE_COMMUNAL_ROASTER_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete GlAccountCategory called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCommunalRoasterDelete').show();");
				
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("<=CommunalRosterResisterBean :: viewCommunalRoster=>");
				try {
					if (selectedCommunalRoster == null) {
						errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
						return null;
					}
					String apiURL = appPreference.getPortalServerURL() + "/communal/get/" + selectedCommunalRoster.getId();
					log.info("<=CommunalRosterResisterBean :: viewCommunalRoster :: url=>" + apiURL);
					BaseDTO baseDTO = httpService.get(apiURL);
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						communalRoster = mapper.readValue(jsonValue, CommunalRoster.class);
						return COMMUNAL_ROASTER_VIEW_URL;
					}

				} catch (Exception e) {
					log.error(" Exception Occured While viewCommunalRoster :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showCommunalRoasterData ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends CommunalRosterResisterBean.showCommunalRoasterData ===========>" + action);
		return null;
	}
	
	public String deleteConfirm() {
		try {

			if (selectedCommunalRoster == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			log.info("selected Communal Roster  Id : : " + selectedCommunalRoster.getId());
			String getUrl = SERVER_URL + "/communal/delete/id/:id";
			String url = getUrl.replace(":id", selectedCommunalRoster.getId().toString());
			log.info("deleteCommunalRoster Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("Roster Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.ROSTER_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			clear();
			init();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return COMMUNAL_ROASTER_LIST_URL;
	}
}
