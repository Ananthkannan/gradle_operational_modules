package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.AdditionalForwardDTO;
import in.gov.cooptex.core.dto.ApproveRejectCommentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.dto.GovtSocietyPlanResponseDTO;
import in.gov.cooptex.operation.enums.AdditionalProductionPlanStage;
import in.gov.cooptex.operation.model.AdditionalProductionPlan;
import in.gov.cooptex.operation.model.AdditionalProductionPlanDetails;
import in.gov.cooptex.operation.model.AdditionalProductionPlanLog;
import in.gov.cooptex.operation.model.AdditionalProductionPlanNote;
import in.gov.cooptex.operation.model.ExhibitionMaster;
import in.gov.cooptex.personnel.hrms.dto.DeathRegistrationDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("additionalProductionPlanBean")
@Scope("session")
public class AdditionalProductionPlanBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 4578136862113789205L;

	private final String ADDITIONAL_PLAN_LIST = "/pages/operation/productionplan/listAdditionalProductionPlan.xhtml?faces-redirect=true;";

	private final String ADDITIONAL_PLAN_CREATE = "/pages/operation/productionplan/createAdditionalProductionPlan.xhtml?faces-redirect=true;";

	private final String ADDITIONAL_PLAN_VIEW = "/pages/operation/productionplan/viewAdditionalProductionPlan.xhtml?faces-redirect=true;";

	@Setter
	@Getter
	StreamedContent pdfFile;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	boolean addButtonFlag = false, showRoomFlag = false, exFlag = false, ortherFlag = false;

	@Getter
	@Setter
	Integer size;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	AdditionalProductionPlan additionalProductionPlan;

	@Getter
	@Setter
	List<GovtSocietyPlanResponseDTO> viewNoteEmployeeDetails = new ArrayList<>();

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> approveCommentsDTOList = new ArrayList<>();

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> rejectCommentsDTOList = new ArrayList<>();

	ObjectMapper mapper;

	@Getter
	@Setter
	LazyDataModel<AdditionalProductionPlan> additionalProductionPlanLazyList;

	List<AdditionalProductionPlanDetails> addList = new ArrayList<>();

	List<Long> proVarieryMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<ExhibitionMaster> exhibitionMasterList = new ArrayList<>();

	@Getter
	@Setter
	AdditionalProductionPlan additionalProductionPlanSearch;

	@Getter
	@Setter
	List<String> statusValues;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<EntityMaster> regionList;

	@Getter
	@Setter
	List<EntityMaster> showroomList;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	EntityMaster region;

	@Getter
	@Setter
	AdditionalProductionPlanDetails additionalProductionPlanDetails;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster = new ProductVarietyMaster();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList = new ArrayList<>();

	@Getter
	@Setter
	Double totalItemQunatity;

	@Getter
	@Setter
	AdditionalProductionPlanNote additionalProductionPlanNote;

	@Getter
	@Setter
	AdditionalProductionPlanNote additionalProductionPlanLastNote = new AdditionalProductionPlanNote();

	@Getter
	@Setter
	AdditionalProductionPlanLog additionalProductionPlanLastLog = new AdditionalProductionPlanLog();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String comments, planProperty, uamCode;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	Double footerTotal;

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean dropdownFlag = true;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	String logRemarks;

	@Getter
	@Setter
	private Boolean editButtonFlag = false;

	@Getter
	@Setter
	private Boolean previousApproveFlag = false;

	@Getter
	@Setter
	private Boolean headerNameAddEdit = false;

	DecimalFormat formatter = new DecimalFormat("0.0000");

	private AdditionalProductionPlanDetails selectedAdditionalProductionPlanDetails;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		loadStatusValues();
		showViewListPage();

	}

	public AdditionalProductionPlanBean() {
		log.info("<#======Inside PurchaseOrderBean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public String onChangeDate() {
		/*
		 * if(additionalProductionPlan!= null &&
		 * additionalProductionPlan.getValidDate()!=null &&
		 * additionalProductionPlan.getNotLaterThan()!=null &&
		 * additionalProductionPlan.getValidDate().after(additionalProductionPlan.
		 * getNotLaterThan())) { log.info("Procurement To is required");
		 * errorMap.notify(ErrorDescription.PROCUREMENT_ORDER_VALID_DATE_NOT_LATER_THAN.
		 * getCode()); return null; }
		 */
		if ((additionalProductionPlan.getPlanTo() != null && additionalProductionPlan.getPlanFrom() != null)
				&& additionalProductionPlan.getPlanTo().before(additionalProductionPlan.getPlanFrom())) {
			additionalProductionPlan.setPlanTo(null);
		}
		return null;
	}

	private void loadStatusValues() {
		Object[] statusArray = AdditionalProductionPlanStage.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status.toString().replace("_", "-"));
		}
		log.info("<--- statusValues ---> " + statusValues);
	}

	public void changeProductCategory() {
		log.info("Inside changeProductCategory");
		additionalProductionPlanDetails = new AdditionalProductionPlanDetails();
		productGroupMaster = null;
		additionalProductionPlanDetails.setProductVarietyMaster(null);
		additionalProductionPlanDetails.setItemQunatity(null);

		if (productCategory != null) {
			log.info("Selected Product Category " + productCategory.getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(productCategory);
			productList = new ArrayList<ProductVarietyMaster>();
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void changeProductGroup() {
		log.info("Inside changeProductGroup");

		additionalProductionPlanDetails.setProductVarietyMaster(null);
		additionalProductionPlanDetails.setItemQunatity(null);

		if (productGroupMaster != null) {
			log.info("Selected Product Group " + productGroupMaster);
			productList = commonDataService.loadProductVarieties(productGroupMaster);
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void changesProductVariety() {
		log.info("Inside changesProductVariety");

		additionalProductionPlanDetails.setItemQunatity(null);

		if (productVarietyMaster != null) {
			uamCode = productVarietyMaster.getUomMaster().getCode();
			log.info("uamCode===>" + uamCode);
			log.info("Selected Product Variety " + productVarietyMaster.getCode());
		}
	}

	public String getCreatePage() {
		headerNameAddEdit = false;
		additionalProductionPlan = new AdditionalProductionPlan();
		region = new EntityMaster();
		uamCode = null;
		note = null;
		loadBasicData();
		loadEmployeeLoggedInUserDetails();
		proVarieryMasterList = new ArrayList<>();
		return ADDITIONAL_PLAN_CREATE;
	}

	public void loadBasicData() {
		try {
			additionalProductionPlanDetails = new AdditionalProductionPlanDetails();
			additionalProductionPlanDetails.setProductVarietyMaster(new ProductVarietyMaster());
			additionalProductionPlanDetails.getProductVarietyMaster().setUomMaster(new UomMaster());
			additionalProductionPlanNote = new AdditionalProductionPlanNote();
			regionList = commonDataService.loadRegionalOffice();
			// employeeList = commonDataService.getAllEmployee();
			employeeList = commonDataService.getActiveEmployeeNameOrderList();
			// forwardToUsersList = commonDataService.loadForwardToUsers();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("ADDITIONAL_PRODUCTION_PLAN");
			productCategoryList = commonDataService.loadProductCategories();
			exhibitionMasterList = commonDataService.loadExhibitionCodeandName();
			productGroupList = new ArrayList<>();
			productList = new ArrayList<>();
			productGroupMaster = new ProductGroupMaster();
			productCategory = new ProductCategory();
		} catch (Exception e) {
			log.error(" Exception Occured While loadBasicData  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String getEditPage() {

		log.info("<=getEditPage call=>");
		try {
			headerNameAddEdit = true;
			if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
				log.info("Please Select Any One");
				AppUtil.addWarn("Please select any one plan");
				return null;
			}
			uamCode = null;
			action = "EDIT";
			loadBasicData();

			getViewPage();

			if (additionalProductionPlan.getAdditionalProductionPlanLogList() != null
					&& additionalProductionPlan.getAdditionalProductionPlanLogList().size() > 0
					&& additionalProductionPlan.getAdditionalProductionPlanLogList().get(0) != null
					&& additionalProductionPlan.getAdditionalProductionPlanLogList().get(0).getStage() != null
					&& !additionalProductionPlan.getAdditionalProductionPlanLogList().get(0).getStage().name()
							.equals(AdditionalProductionPlanStage.INITIATED.name())) {
				errorMap.notify(ErrorDescription.ADDITIONAL_PLAN_CANNOT_BE_MODIFIED.getCode());
				return null;
			}

			calculateTotalQuantity();

			if (additionalProductionPlan.getEntityMaster() != null) {
				region = additionalProductionPlan.getEntityMaster().getEntityMasterRegion();
				regionList = commonDataService.loadRegionalOffice();
				if (region != null && region.getId() != null) {
					showroomList = commonDataService.loadShowroomListForRegion(region.getId());

				} else {
					showroomList = commonDataService.loadShowRoomList();
				}
			}

			additionalProductionPlanNote = new AdditionalProductionPlanNote();
			if (additionalProductionPlan.getAdditionalProductionPlanNoteList() != null
					&& additionalProductionPlan.getAdditionalProductionPlanNoteList().size() > 0) {
				additionalProductionPlanNote = additionalProductionPlan.getAdditionalProductionPlanNoteList().get(0);
			}

			log.info("getPlanFor==>" + additionalProductionPlan.getPlanFor());
			note = additionalProductionPlanNote.getNote();
			if (additionalProductionPlan.getPlanFor().equals("Showroom")) {
				showRoomFlag = true;
				exFlag = false;
				ortherFlag = false;
			} else if (additionalProductionPlan.getPlanFor().equals("Exhibition")) {
				exFlag = true;
				showRoomFlag = false;
				ortherFlag = false;
			} else if (additionalProductionPlan.getPlanFor().equals("Others")) {
				ortherFlag = true;
				exFlag = false;
				showRoomFlag = false;
			}

		} catch (Exception e) {
			log.error(" Exception Occured While loadBasicData  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		// additionalProductionPlan.planFor

		return ADDITIONAL_PLAN_CREATE;
	}

	public String cancel() {
		log.info("<--- Inside cancel() ---> ");
		loadLazyAdditionalPlanList();
		return ADDITIONAL_PLAN_LIST;
	}

	public String getViewPage() {
		log.info("Inside getViewPage Action " + action);

		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			log.info("Please Select Any One");
			AppUtil.addWarn("Please select any one plan");
			return null;
		}
		log.info("additionalProductionPlan Id " + additionalProductionPlan.getId());
		BaseDTO baseDTO = null;
		try {
			// loadEmployeeLoggedInUserDetails();
			// forwardToUsersList =
			// commonDataService.loadForwardToUsersByFeature("ADDITIONAL_PRODUCTION_PLAN");
			String additionalProductionPlanViewPath = appPreference.getPortalServerURL()
					+ appPreference.getOperationApiUrl() + "/productionplan/retail/additional/get";
			log.info(" additionalProductionPlanViewPath " + additionalProductionPlanViewPath);

			baseDTO = httpService.post(additionalProductionPlanViewPath, additionalProductionPlan);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				additionalProductionPlan = objectMapper.readValue(jsonResponse,
						new TypeReference<AdditionalProductionPlan>() {
						});

				String jsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = mapper.readValue(jsonResponses,
						new TypeReference<List<DeathRegistrationDTO>>() {
						});

				approveCommentsDTOList = additionalProductionPlan.getApproveCommentsDTOList();
				rejectCommentsDTOList = additionalProductionPlan.getRejectCommentsDTOList();

				if (additionalProductionPlan.getNotificationId() != null) {
					systemNotificationBean.loadTotalMessages();
				}

				if (additionalProductionPlan != null
						&& additionalProductionPlan.getAdditionalProductionPlanNoteList() != null
						&& additionalProductionPlan.getAdditionalProductionPlanNoteList().size() > 0) {
					additionalProductionPlanLastNote = additionalProductionPlan.getAdditionalProductionPlanNoteList()
							.get(0);
					previousApproveFlag = additionalProductionPlanLastNote.getFinalApproval();
					additionalProductionPlanLastLog = additionalProductionPlan.getAdditionalProductionPlanLogList()
							.get(0);
				}
				if (additionalProductionPlanLastNote != null && additionalProductionPlanLastNote.getForwardTo() != null
						&& additionalProductionPlanLastNote.getForwardTo().getId() != null) {
					if (additionalProductionPlanLastNote.getForwardTo().getId().longValue() == loginBean.getUserMaster()
							.getId().longValue()) {
						enableFlag = true;
					}
				}
				if (additionalProductionPlanLastNote.getForwardTo() != null && !additionalProductionPlanLastLog
						.getStage().equals(AdditionalProductionPlanStage.INITIATED)) {
					log.info("login usermaster id==>" + loginBean.getUserMaster().getId());
					log.info("forward to usermaster id==>" + additionalProductionPlanLastNote.getForwardTo().getId());
					if (loginBean.getUserMaster().getId()
							.equals(additionalProductionPlanLastNote.getForwardTo().getId())) {

						if (AdditionalProductionPlanStage.REJECTED.equals(additionalProductionPlanLastLog.getStage())
								|| AdditionalProductionPlanStage.FINAL_APPROVED
										.equals(additionalProductionPlanLastLog.getStage())) {
							approvalFlag = false;
						} else {
							approvalFlag = true;
						}

						if (additionalProductionPlanLastNote.getFinalApproval()) {
							dropdownFlag = false;
						}

					} else {
						approvalFlag = false;
					}
				}
				setFinalApproveFlag(additionalProductionPlanLastNote.getFinalApproval());
				if (AdditionalProductionPlanStage.FINAL_APPROVED.equals(additionalProductionPlanLastLog.getStage())) {
					buttonFlag = true;
				}
				if (additionalProductionPlan.getEntityMaster() != null) {
					EntityMaster em = commonDataService
							.findRegionByShowroomId(additionalProductionPlan.getEntityMaster().getId());
					additionalProductionPlan.getEntityMaster().setEntityMasterRegion(em);
					region = additionalProductionPlan.getEntityMaster().getEntityMasterRegion();
				}
				calculateTotalQuantity();
				footerTotal = additionalProductionPlan.getAdditionalProductionPlanDetailList().stream()
						.mapToDouble(AdditionalProductionPlanDetails::getItemQunatity).sum();

			} else {
				log.error("Service Not Avalible");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}

		} catch (Exception e) {
			log.error("Error in getViewPage ", e);
		}

		return ADDITIONAL_PLAN_VIEW;
	}

	public String approveRejComments(String status) {
		try {
			log.info("status==>" + status);
			log.info("logRemarks==>" + logRemarks);
			log.info("additionalProductionPlanLastLog==>" + additionalProductionPlanLastLog.getId());
			AdditionalForwardDTO requestModForwardDTO = new AdditionalForwardDTO();
			requestModForwardDTO.setLogList(additionalProductionPlan.getAdditionalProductionPlanLogList());
			requestModForwardDTO.setForwardRemarks(logRemarks);
			requestModForwardDTO.setAdditionalProductionPlan(additionalProductionPlan);
			requestModForwardDTO.setAddProPlanLastNote(additionalProductionPlanLastNote);

			if (status.equals("reject")) {
				requestModForwardDTO.setStatus(ApprovalStage.REJECTED);
			} else {
				if (additionalProductionPlanLastNote.getFinalApproval() == true && previousApproveFlag) {
					requestModForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
				} else {
					requestModForwardDTO.setStatus(ApprovalStage.APPROVED);
				}
			}
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/productionplan/retail/additional/updatelog";
			BaseDTO baseDTO = httpService.post(url, requestModForwardDTO);
			log.info("Save Modernization Request : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				// clear();
				loadLazyAdditionalPlanList();
				if (status.equals("reject")) {
					errorMap.notify(ErrorDescription
							.getError(OperationErrorCode.ADDITIONAL_PRODUCTION_PLAN_REJECTED_SUCCESSFULLY)
							.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription
							.getError(OperationErrorCode.ADDITIONAL_PRODUCTION_PLAN_APPROVED_SUCCESSFULLY)
							.getErrorCode());
				}

				return ADDITIONAL_PLAN_LIST;
			} else {
				log.info("Error while saving ModernizationRequest with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return null;
	}

	public String processDelete() {

		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			log.info("Please Select Any One");
			AppUtil.addWarn("Please select any one plan");
			return null;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");
		}
		return "";
		// return ADDITIONAL_PLAN_CREATE;
	}

	public String deleteProductionPlan() {
		try {

			if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
				errorMap.notify(ErrorDescription.ADDITIONAL_PLAN_SELECT_ONE_RECORD.getCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + additionalProductionPlan.getId());
			/*
			 * String getUrl = JOB_SERVER_URL+ "/delete/id/:id"; String url =
			 * getUrl.replace(":id", additionalProductionPlan.getId().toString());
			 */
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/productionplan/retail/additional/delete/" + additionalProductionPlan.getId();
			log.info("deleteProductionPlan Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EMPLOYEE_DELETED_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

			loadLazyAdditionalPlanList();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return ADDITIONAL_PLAN_LIST;
	}

	public void onChangeRegion() {
		if (region != null) {
			showroomList = commonDataService.loadActiveShowroomsByRegionId(region.getId());
		}
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + note);
		// log.info("note >>>>>>>>> " + note);
		additionalProductionPlanNote.setNote(note);
		log.info("=========================================");
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public String clearItems() {
		log.info("clear items starts------");
		uamCode = null;
		additionalProductionPlanDetails = new AdditionalProductionPlanDetails();
		productGroupMaster = null;
		productCategory = null;
		productVarietyMaster = null;
		productList = new ArrayList<>();
		productGroupList = new ArrayList<>();
		additionalProductionPlanDetails.setItemQunatity(null);
		return null;
	}

	public void setQuantity() {
		additionalProductionPlanDetails.getItemQunatity();
	}

	public void addProduct() {
		if (productCategory == null || productCategory.getId() == null) {
			errorMap.notify(ErrorDescription.ERRO_PRODUCT_CATEGORY_REQ.getCode());
			return;
		}
		if (productGroupMaster == null || productGroupMaster.getId() == null) {
			errorMap.notify(ErrorDescription.PRODUCT_GROUP_EMPTY.getCode());
			return;
		}
		if (productVarietyMaster == null || productVarietyMaster.getId() == null) {
			errorMap.notify(ErrorDescription.PRODUCT_VARIETY_EMPTY.getCode());
			return;
		}
		if (uamCode == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.UOM_NAME_REQUIRED).getCode());
			return;
		}
		Double itemQuantity = additionalProductionPlanDetails.getItemQunatity();
		if (itemQuantity == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_QTY_REQUIRED).getCode());
			return;
		}
		if (itemQuantity != null && itemQuantity <= 0D) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.QUANTITY_ZERO_NOT_ALLOWED).getCode());
			return;
		}
		if (additionalProductionPlan.getAdditionalProductionPlanDetailList() == null) {
			additionalProductionPlan.setAdditionalProductionPlanDetailList(new ArrayList<>());
		}
		if (productVarietyMaster != null) {
			if (checkProductExist(productVarietyMaster)) {
				AdditionalProductionPlanDetails additionalProductionPlanDetailsObj = new AdditionalProductionPlanDetails();
				additionalProductionPlanDetailsObj.setProductVarietyMaster(productVarietyMaster);
				additionalProductionPlanDetailsObj.setItemQunatity(itemQuantity);
				additionalProductionPlan.getAdditionalProductionPlanDetailList()
						.add(additionalProductionPlanDetailsObj);
			}
			calculateTotalQuantity();
			clearProductPlanDetails();

		}

	}

	public void clearProductPlanDetails() {
		uamCode = null;
		productGroupMaster = null;
		productCategory = null;
		productVarietyMaster = new ProductVarietyMaster();
		productGroupMaster = new ProductGroupMaster();
		productCategory = new ProductCategory();
		additionalProductionPlanDetails.setItemQunatity(null);
	}

	private boolean checkProductExist(ProductVarietyMaster productVarietyMaster) {
		try {
			log.info("variety master name ==> " + productVarietyMaster.getName());
			log.info("variety master id ==> " + productVarietyMaster.getId());
			totalItemQunatity = 0.0;
			if (proVarieryMasterList.contains(productVarietyMaster.getId())) {
				log.info("<=Product Exist=>");
				for (AdditionalProductionPlanDetails detail : additionalProductionPlan
						.getAdditionalProductionPlanDetailList()) {
					log.info(productVarietyMaster.getId() + "<==ID==>" + detail.getProductVarietyMaster().getId());
					if (productVarietyMaster.getId() == detail.getProductVarietyMaster().getId()) {
						totalItemQunatity = detail.getItemQunatity()
								+ additionalProductionPlanDetails.getItemQunatity();
						// additionalProductionPlanDetails.setItemQunatity(totalItemQunatity);
						detail.setItemQunatity(totalItemQunatity);
					}

				}
				return false;
			} else {
				proVarieryMasterList.add(productVarietyMaster.getId());
				return true;
			}

		} catch (Exception e) {
			log.error("checkProductExist==>", e);
		}
		return false;
	}

	public String saveAdditionalProductionPlan(String requestType) {
		log.info("saveAdditionalProductionPlan method started");

		log.info("getNote====>" + additionalProductionPlanNote.getNote());
		if (additionalProductionPlan.getPlanName() == null) {
			// errorMap.notify(AdditionalProductionErrorDescription.PLAN_NAME_REQUIRED.getErrorCode());
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLAN_NAME_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getPlanFrom() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLAN_FROM_DATE_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getPlanTo() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLAN_TO_DATE_REQUIRED).getErrorCode());
			return null;
		}
		Date fromDate = additionalProductionPlan.getPlanFrom();
		Date toDate = additionalProductionPlan.getPlanTo();
		log.info("compate date==>" + fromDate.compareTo(toDate));

		if (fromDate.compareTo(toDate) == 1) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.TO_DATE_GRATER_THAN_FROM_DATE).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getPlanFor() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLAN_FOR_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getPlanDueDate() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.PLAN_DUE_DATE_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getRequestFromEmployee() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.REQUEST_FROM_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getRequestedDate() == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.REQUEST_DATE_REQUIRED).getErrorCode());
			return null;
		}
		if (additionalProductionPlan.getAdditionalProductionPlanDetailList() == null
				|| additionalProductionPlan.getAdditionalProductionPlanDetailList().isEmpty()) {
			if (productCategory == null) {
				errorMap.notify(ErrorDescription.ERRO_PRODUCT_CATEGORY_REQ.getCode());
				return null;
			}
			if (productGroupMaster == null) {
				errorMap.notify(ErrorDescription.PRODUCT_GROUP_EMPTY.getCode());
				return null;
			}
			if (productVarietyMaster == null) {
				errorMap.notify(ErrorDescription.PRODUCT_VARIETY_EMPTY.getCode());
				return null;
			}
			if (uamCode == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.UOM_NAME_REQUIRED).getCode());
				return null;
			}
			if (additionalProductionPlanDetails.getItemQunatity() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_QTY_REQUIRED).getCode());
				return null;
			}
		}

		if (additionalProductionPlanNote.getForwardTo() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getCode());
			return null;
		}
		if (additionalProductionPlanNote.getForwardTo() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getCode());
			return null;
		}
		if (additionalProductionPlanNote.getNote() == null) {
			errorMap.notify(ErrorDescription.NEW_INVESTMENT_NOTE_NULL.getCode());
			return null;
		}

		additionalProductionPlanNote.setNote(additionalProductionPlanNote.getNote());
		additionalProductionPlan.setAdditionalProductionPlanNoteList(new ArrayList<>());
		additionalProductionPlan.getAdditionalProductionPlanNoteList().add(additionalProductionPlanNote);

		AdditionalProductionPlanLog additionalProductionPlanLog = new AdditionalProductionPlanLog();

		if (requestType.equals(AdditionalProductionPlanStage.INITIATED.name())) {
			additionalProductionPlanLog.setStage(AdditionalProductionPlanStage.INITIATED);
		} else {
			additionalProductionPlanLog.setStage(AdditionalProductionPlanStage.SUBMITTED);
		}
		if (additionalProductionPlan.getAdditionalProductionPlanLogList() == null) {
			additionalProductionPlan.setAdditionalProductionPlanLogList(new ArrayList<>());
		}
		additionalProductionPlan.getAdditionalProductionPlanLogList().add(additionalProductionPlanLog);

		String url = serverURL + appPreference.getOperationApiUrl() + "/productionplan/retail/additional/create";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, additionalProductionPlan);
		} catch (Exception exp) {
			log.error("Error in Additional Production Plan ", exp);
			errorMap.notify(1);
			return null;
		}

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Additional Production Plan saved successfully");
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.ADDITION_PRODUCTION_PLAN_SAVE_SUCCESSFULLY).getErrorCode());
				if (requestType.equals(AdditionalProductionPlanStage.INITIATED.name())) {
					AppUtil.addInfo("Additional Production Plan saved successfully");
				} else {
					AppUtil.addInfo("Additional Production Plan submitted successfully");
				}
				// errorMap.notify(92107);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}
		return ADDITIONAL_PLAN_LIST;
	}

	/*
	 * public boolean checkProductExist() {
	 * log.info("getAdditionalProductionPlanDetailList==>"+additionalProductionPlan.
	 * getAdditionalProductionPlanDetailList());
	 * if(additionalProductionPlan.getAdditionalProductionPlanDetailList()!=null) {
	 * for(AdditionalProductionPlanDetails details :
	 * additionalProductionPlan.getAdditionalProductionPlanDetailList()) {
	 * if(details.getProductVarietyMaster().getId().equals(details.
	 * getProductVarietyMaster().getId())) {
	 * details.setItemQunatity(AppUtil.ifNullRetunZero(details.getItemQunatity())
	 * +AppUtil.ifNullRetunZero(additionalProductionPlanDetails.getItemQunatity()));
	 * return true; } } } return false; }
	 */

	public void calculateTotalQuantity() {
		totalItemQunatity = 0.0;
		if (additionalProductionPlan != null) {
			for (AdditionalProductionPlanDetails detail : additionalProductionPlan
					.getAdditionalProductionPlanDetailList()) {
				totalItemQunatity = totalItemQunatity + AppUtil.ifNullRetunZero(detail.getItemQunatity());
				detail.setTotalItemQunatity(totalItemQunatity);
			}
		}
		log.info("calculateTotalQuantity totalItemQunatity==>" + totalItemQunatity);
		// RequestContext context = RequestContext.getCurrentInstance();
		// context.update("tablePanel ");
	}

	public void confirmRemoveProduct(AdditionalProductionPlanDetails additionalProductionPlanDetails) {
		selectedAdditionalProductionPlanDetails = new AdditionalProductionPlanDetails();
		selectedAdditionalProductionPlanDetails = additionalProductionPlanDetails;
		RequestContext.getCurrentInstance().execute("PF('confirmAddlnPlanDelete').show();");
	}

	public String removeProduct() {
		proVarieryMasterList.remove(selectedAdditionalProductionPlanDetails.getProductVarietyMaster().getId());
		// proVarieryMasterList.remove(additionalProductionPlanDetails.getProductVarietyMaster().getName());
		additionalProductionPlan.getAdditionalProductionPlanDetailList()
				.remove(selectedAdditionalProductionPlanDetails);
		calculateTotalQuantity();
		return ADDITIONAL_PLAN_CREATE;
	}

	public void loadLazyAdditionalPlanList() {
		addButtonFlag = false;
		dropdownFlag = true;
		log.info("AdditionalProductionPlan.loadLazyAdditionalPlanList method started");
		additionalProductionPlanLazyList = new LazyDataModel<AdditionalProductionPlan>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<AdditionalProductionPlan> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<AdditionalProductionPlan> data = new ArrayList<AdditionalProductionPlan>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<AdditionalProductionPlan>>() {
					});
					// if (data != null) {
					this.setRowCount(baseDTO.getTotalRecords());
					size = baseDTO.getTotalRecords();
					// }
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(AdditionalProductionPlan res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AdditionalProductionPlan getRowData(String rowKey) {
				List<AdditionalProductionPlan> responseList = (List<AdditionalProductionPlan>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (AdditionalProductionPlan res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						additionalProductionPlan = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	private AdditionalProductionPlan getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		AdditionalProductionPlan request = new AdditionalProductionPlan();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("planCodeOrName")) {
				log.info("plan Code Or Name : " + value);
				request.setPlanCodeOrName(value);
			}
			if (entry.getKey().equals("planFrom")) {
				log.info("planFromSearch : " + value);
				request.setPlanFrom(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("planTo")) {
				log.info("planToSearch : " + value);
				request.setPlanTo(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("planDueDate")) {
				log.info("planDueDateSearch : " + value);
				request.setPlanDueDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("stage")) {
				log.info("selectedStage : " + value);
				request.setStage(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("AdditionalProductionPlan.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		additionalProductionPlanSearch = new AdditionalProductionPlan();
		AdditionalProductionPlan request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/productionplan/retail/additional/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("AdditionalProductionPlan.getSearchData Method Complted");
		return baseDTO;
	}

	public void onPageLoad() {
		log.info("AdditionalProductionPlan onPageLoad method started");
		addButtonFlag = false;
		editButtonFlag = false;
		dropdownFlag = true;
		previousApproveFlag = false;
	}

	public String getAdditionalPlanListPage() {
		additionalProductionPlanSearch = new AdditionalProductionPlan();
		additionalProductionPlan = new AdditionalProductionPlan();
		loadLazyAdditionalPlanList();
		dropdownFlag = true;
		return ADDITIONAL_PLAN_LIST;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("AdditionalProductionPlan.onRowSelect method started");
		AdditionalProductionPlan additionPP = new AdditionalProductionPlan();
		additionPP = ((AdditionalProductionPlan) event.getObject());
		editButtonFlag = true;

		UserMaster currentUser = loginBean.getUserMaster();
		Long loginUserId = currentUser == null ? null : currentUser.getId();
		log.info("AdditionalProductionPlan.onRowSelect() - loginUserId: " + loginUserId);

		UserMaster createdBy = additionPP.getCreatedBy();
		Long createdById = createdBy == null ? null : createdBy.getId();
		log.info("AdditionalProductionPlan.onRowSelect() - createdById: " + createdById);

		// if (additionPP.getStage().equalsIgnoreCase("SUBMITTED") ||
		// additionPP.getStage().equalsIgnoreCase("APPROVED")) {
		if (additionPP.getStage().equalsIgnoreCase("INITIATED")) {
			if (loginUserId != null && createdById != null && createdById.longValue() == loginUserId) {
				editButtonFlag = false;
			} else {
				editButtonFlag = true;
			}
		} else {
			editButtonFlag = true;
		}
		addButtonFlag = true;
	}

	public void onChangeProperty() {

		log.info("==>" + additionalProductionPlan.getPlanFor());
		if (additionalProductionPlan.getPlanFor().equals("Showroom")) {
			showRoomFlag = true;
			exFlag = false;
			ortherFlag = false;
		} else if (additionalProductionPlan.getPlanFor().equals("Exhibition")) {
			exFlag = true;
			showRoomFlag = false;
			ortherFlag = false;
		} else if (additionalProductionPlan.getPlanFor().equals("Others")) {
			ortherFlag = true;
			exFlag = false;
			showRoomFlag = false;
		}

	}

	public void onRowEdit() {
		calculateTotalQuantity();

	}

	public void clear() {
		additionalProductionPlan = new AdditionalProductionPlan();
		additionalProductionPlanSearch = new AdditionalProductionPlan();
		loadLazyAdditionalPlanList();
	}

	public String showViewListPage() {
		log.info("<==== Starts showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String additionalPlanId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			additionalPlanId = httpRequest.getParameter("additionalPlanId");

			notificationId = httpRequest.getParameter("notificationId");
			log.info("==== Selected Notification Id ====" + notificationId);

		}
		additionalProductionPlan = new AdditionalProductionPlan();
		if (!StringUtils.isEmpty(notificationId)) {
			additionalProductionPlan.setNotificationId(Long.parseLong(notificationId));
		}
		if (!StringUtils.isEmpty(additionalPlanId)) {
			additionalProductionPlan.setId(Long.parseLong(additionalPlanId));
			action = "VIEW";
			getViewPage();
		}
		return ADDITIONAL_PLAN_VIEW;
	}

}
