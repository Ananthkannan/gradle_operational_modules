package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SearchDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ManualAttendanceDetails;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.dto.ProductQRCodeItemDetailsDTO;
import in.gov.cooptex.operation.dto.ScanProductDTO;
import in.gov.cooptex.operation.enums.ItemStatus;
import in.gov.cooptex.operation.enums.ProductQRCodeStatus;
import in.gov.cooptex.operation.enums.RegionType;
import in.gov.cooptex.operation.enums.RetailQualityCheckStatus;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.enums.YesNo;
import in.gov.cooptex.operation.model.ProductQRCode;
import in.gov.cooptex.operation.model.ProductQRCodeItem;
import in.gov.cooptex.operation.model.ProductQRCodeItemDetails;
import in.gov.cooptex.operation.model.QualityCheckProductWise;
import in.gov.cooptex.operation.model.RetailQualityCheck;
import in.gov.cooptex.operation.model.RetailQualityCheckDetails;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.ProductQRCodeDetailsDTO;
import in.gov.cooptex.operation.production.dto.ProductQRCodeRequest;
import in.gov.cooptex.operation.production.dto.ProductQRCodeResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("productQRCodeBean")
@Scope("session")
public class ProductQRCodeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/productWarehouse/qrcode/listQrCodeGeneration.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/productWarehouse/qrcode/createQrCodeGeneration.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/productWarehouse/qrcode/viewQRCodeGeneration.xhtml?faces-redirect=true;";

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	Integer totalProductVarities;

	@Getter
	@Setter
	Integer totalQRCodeGeneratedVarities;

	@Getter
	@Setter
	Integer totalQRCodeYetToGeneratedVarities;

	@Getter
	@Setter
	Integer totalProductItemWiseCount;

	@Getter
	@Setter
	Integer totalProductItemWiseQRCodeCount;

	@Setter
	@Getter
	StreamedContent file;

	@Getter
	@Setter
	Integer totalProductItemWiseQRCodeToBeGenerateCount;

	@Getter
	@Setter
	Integer totalProductItems;

	@Getter
	@Setter
	Integer totalQRCodeGeneratedItems;

	@Getter
	@Setter
	Integer totalQRCodeYetToGeneratedItems;

	@Getter
	@Setter
	Integer totalQRCodeYetToGenerateAtNumberWiseItems;

	@Getter
	@Setter
	Integer totalQRCodeGeneratedAtNumberWiseItems;

	@Getter
	@Setter
	Integer totalIntraRegionItems;

	@Getter
	@Setter
	Integer totalInterRegionItems;

	@Getter
	@Setter
	String[] senderAddress;

	@Getter
	@Setter
	String[] receiverAddress;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	String societyCode;

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	String atNumberValue;

	@Getter
	@Setter
	RegionType regionType;

	@Getter
	@Setter
	Long stockTransferId;

	@Getter
	@Setter
	String productQRCodePath;

	@Getter
	@Setter
	boolean disableQRCodeButton;

	@Getter
	@Setter
	boolean disableRegionSubmit;

	@Getter
	@Setter
	ProductQRCodeDetailsDTO productQRCodeDetailsDTO;

	@Getter
	@Setter
	List<Object> regionTypeValues;

	@Getter
	@Setter
	LazyDataModel<ProductQRCodeResponse> productQRCodeResponseLazyList;

	@Getter
	@Setter
	ProductQRCodeResponse productQRCodeResponse;

	@Getter
	@Setter
	ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO;

	@Getter
	@Setter
	List<ProductQRCodeItemDetailsDTO> productQRCodeItemDetailsDTOList;

	@Getter
	@Setter
	String barCodeValue;

	@Getter
	@Setter
	ProductQRCode productQRCode;

	@Getter
	@Setter
	ProductQRCodeItemDetailsDTO singleProductItem;

	@Getter
	@Setter
	boolean regionTypeDisable;

	@Getter
	@Setter
	SupplierMaster supplierId;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	String[] societyAddress;

	@Getter
	@Setter
	Integer index;

	@Getter
	@Setter
	String[] productWarehouseAddress;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	ProductQRCodeItem selectedProductQRCodeItem;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Long qualityCheckId = 0L;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer size;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	StreamedContent downloadFile;

	@Setter
	@Getter
	StreamedContent downloadInwardPdfFile;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	boolean editButtonFlag = true;

	@Getter
	@Setter
	boolean generateButtonFlag = true;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<String> barCodeList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductQRCodeItemDetails> selectedProductQRCodeItemList = new ArrayList<>();

	@Getter
	@Setter
	List<ProductQRCodeItemDetailsDTO> multipleProductItemList = new ArrayList<>();

	@Getter
	@Setter
	Double totalPurchasePrice = 0.0;

	@Getter
	@Setter
	Double totalRetailPrice = 0.0;

	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {

			String loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();

			if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)) {
				selectedProductQRCodeItemList = new ArrayList<>();
				if (page.equals("ADD")) {
					return getAddPage(loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId());

				} else if (page.equals("EDIT")) {
					if (productQRCodeResponse != null) {
						if (productQRCodeResponse.getStatus().toString().equalsIgnoreCase("QR_COMPLETE")) {
							errorMap.notify(ErrorDescription
									.getError(OperationErrorCode.CAN_NOT_EDIT_QR_CODE_COMPLETED_STATUS).getErrorCode());
						} else {
							return editQRCode(loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId());
						}
					} else {
						AppUtil.addWarn("Select One Record");
					}

				} else {
					return null;
				}
			} else {
				errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_PRODUCT_WAREHOUSE.getCode());
			}
		}

		return null;
	}

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		loadUrl();
		loadStatusValues();
		loadLazyList();
		productQRCodePath = commonDataService.getAppKeyValue("PRODUCT_QR_CODE_PATH");
		addButtonFlag = false;
		generateButtonFlag = true;
	}

	private void loadStatusValues() {

		Object[] statusArray = ProductQRCodeStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	private void loadRegionTypeValues() {

		Object[] regionTypeArray = RegionType.class.getEnumConstants();
		regionTypeValues = new ArrayList<>();
		for (Object regionTypeValue : regionTypeArray) {
			regionTypeValues.add(regionTypeValue);
		}
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	public String clearSearch() {
		productQRCodeResponse = new ProductQRCodeResponse();
		addButtonFlag = false;
		generateButtonFlag = true;
		selectedProductQRCodeItemList = new ArrayList<>();
		return getListPage();
	}

	public String getListPage() {
		log.info("Going to list page");
		productQRCodeResponse = new ProductQRCodeResponse();
		selectedProductQRCodeItemList = new ArrayList<>();
		generateButtonFlag = true;
		return LIST_PAGE_URL;
	}

	public void onClickQRCodeModalOpen(ProductQRCodeItem productQRCodeItem, int idx) {
		log.info("onClickQRCodeModalOpen");
		index = idx;
		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			if (productQRCodeItemDetails.getRegionType() == null) {
				disableQRCodeButton = true;
				break;
			} else {
				disableQRCodeButton = false;
			}
		}

		if (productQRCodeItem.getProductStatus().equals(ProductQRCodeStatus.QR_COMPLETE)) {
			disableQRCodeButton = true;
		} else {
			disableQRCodeButton = false;
		}

		selectProductWiseData(productQRCodeItem, idx, YesNo.NO);
	}

	public void disableRegionType() {
		log.info("disableRegionType");
		if (regionType != null) {
			regionTypeDisable = true;
		}
	}

	/**
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	private ProductQRCodeRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {

		ProductQRCodeRequest request = new ProductQRCodeRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("ackReferenceNumber")) {
				log.info("Ack Reference Number : " + value);
				request.setAckReferenceNumber(value);
			}

			if (entry.getKey().equals("stockInwardNumber")) {
				log.info("stock Inward Number : " + value);
				request.setStockInwardNumber(value);
			}

			if (entry.getKey().equals("senderCodeOrName")) {
				log.info("Sender Code Or Name : " + value);
				request.setSenderCodeOrName(value);
			}
			if (entry.getKey().equals("receiverCodeOrName")) {
				log.info("Receiver Code Or Name : " + value);
				request.setReceiverCodeOrName(value);
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
			if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null
					&& loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
				request.setLoginUserEntityId(
						loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
			}
		}

		return request;
	}

	/**
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		// for (Map.Entry<String, Object> entry : filters.entrySet()) {
		// log.info("Key : " + entry.getKey() + " Value : " +
		// entry.getValue().toString());
		// }

		productQRCodeResponse = new ProductQRCodeResponse();
		//
		ProductQRCodeRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		//
		if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null
				&& loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
			request.setLoginUserEntityId(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
		}
		/**
		 * /api/v1/operation/product/qr/code/search
		 * 
		 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller/ProductQRCodeController.java
		 * 
		 */
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	/**
	 * @param productQRCodeItem
	 * @param idx
	 * @param buttonType
	 * @return
	 */
	public String selectProductWiseData(ProductQRCodeItem productQRCodeItem, int idx, YesNo buttonType) {
		selectedProductQRCodeItemList = new ArrayList<>();
		index = idx;
		barCodeList = new ArrayList<>();
		loadRegionTypeValues();
		if (productQRCode.getProductQRCodeItemList().get(index).getProductStatus()
				.equals(ProductQRCodeStatus.QR_COMPLETE)) {
			disableRegionSubmit = true;
		} else {
			disableRegionSubmit = false;
		}

		totalQRCodeYetToGeneratedItems = 0;
		totalQRCodeYetToGenerateAtNumberWiseItems = 0;
		totalQRCodeGeneratedAtNumberWiseItems = 0;
		totalInterRegionItems = 0;
		totalIntraRegionItems = 0;

		log.info("productQRCodeBean.selectProductWiseData method started");

		if (productQRCodeItem != null) {
			selectedProductQRCodeItem = productQRCodeItem;
		}
		regionType = null;
		ScanProductDTO scanProductDTO = new ScanProductDTO();
		scanProductDTO.setProductId(selectedProductQRCodeItem.getProductVarietyMaster().getId());
		scanProductDTO.setQualityCheckId(qualityCheckId);

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/getproductqrcodeitemdetails/";
		BaseDTO baseDTO = null;
		int productQRCodeItemDetailsDTOListSize = 0;
		try {
			baseDTO = httpService.post(url, scanProductDTO);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productQRCodeDetailsDTO = mapper.readValue(jsonResponse, ProductQRCodeDetailsDTO.class);

			productQRCodeItemDetailsDTOList = productQRCodeDetailsDTO.getProductQRCodeItemDetailsDTOList();
			productQRCodeItemDetailsDTOListSize = productQRCodeItemDetailsDTOList != null ? productQRCodeItemDetailsDTOList.size() : 0;
			log.info("productQRCodeItemDetailsDTOList size==> "+ productQRCodeItemDetailsDTOListSize);

			/*
			 * for (ProductQRCodeItemDetails productQRCodeItemDetails :
			 * productQRCode.getProductQRCodeItemList().get(index)
			 * .getProductQRCodeItemDetailsList()) { if (action.equals("ADD") &&
			 * buttonType.equals(YesNo.YES) && productQRCode.getId() == null) {
			 * productQRCodeItemDetails.setRegionType(null); } productQRCodeItemDetails
			 * .setLength(getLength(productQRCodeItemDetailsDTOList,
			 * productQRCodeItemDetails.getAtNumber())); productQRCodeItemDetails
			 * .setWidth(getWidth(productQRCodeItemDetailsDTOList,
			 * productQRCodeItemDetails.getAtNumber()));
			 * productQRCodeItemDetails.setEndsperInch(
			 * getEndsPerInch(productQRCodeItemDetailsDTOList,
			 * productQRCodeItemDetails.getAtNumber()));
			 * productQRCodeItemDetails.setPicksperInch(
			 * getPicksPerInch(productQRCodeItemDetailsDTOList,
			 * productQRCodeItemDetails.getAtNumber())); }
			 */
			totalProductItems = productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList()
					.size() - totalQRCodeGeneratedAtNumberWiseItems;
			totalQRCodeGeneratedItems = calculateProductItemCounts(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList(),
					ProductQRCodeStatus.QR_COMPLETE);
			totalQRCodeYetToGeneratedItems = calculateProductItemCounts(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList(),
					ProductQRCodeStatus.QR_PENDING);
			totalQRCodeGeneratedAtNumberWiseItems = calculateProductItemCounts(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList(),
					ProductQRCodeStatus.QR_COMPLETE);
			totalQRCodeYetToGenerateAtNumberWiseItems = productQRCode.getProductQRCodeItemList().get(index)
					.getProductQRCodeItemDetailsList().size() - totalQRCodeYetToGeneratedItems;
			log.info("productQRCodeBean.selectProductWiseData method started", productQRCode);
			updateRegionCount();
			regionTypeDisable = false;
			updateProductQRCodeItemDetails();
			calculateTotalRetailandPurchasePrice();
		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
		}
		log.info("list value" + productQRCodeItemDetailsDTOListSize);
		return null;
	}

	private void updateProductQRCodeItemDetails() {
		int tempIntex = index;
		for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
				.get(index).getProductQRCodeItemDetailsList()) {
			productQRCodeItemDetailsValue.setLength(getLength(productQRCodeItemDetailsDTOList,
					productQRCodeItemDetailsDTOList.get(tempIntex).getAtNumber()));
			productQRCodeItemDetailsValue.setWidth(getWidth(productQRCodeItemDetailsDTOList,
					productQRCodeItemDetailsDTOList.get(tempIntex).getAtNumber()));
			productQRCodeItemDetailsValue.setPicksperInch(getPicksPerInch(productQRCodeItemDetailsDTOList,
					productQRCodeItemDetailsDTOList.get(tempIntex).getAtNumber()));
			productQRCodeItemDetailsValue.setEndsperInch(getEndsPerInch(productQRCodeItemDetailsDTOList,
					productQRCodeItemDetailsDTOList.get(tempIntex).getAtNumber()));
			++tempIntex;
		}

	}

	public String submitQRCode() {

		log.info("submitQRCode method started");
		// productQRCode.setProductQRCodeStatus(ProductQRCodeStatus.QR_PENDING);

		productQRCode.setProductQRCodeStatus(ProductQRCodeStatus.QR_IN_PROGRESS);

		if (barCodeList != null && !barCodeList.isEmpty()) {
			for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
					.getProductQRCodeItemDetailsList()) {
				productQRCodeItemDetails.setProductStatus(ProductQRCodeStatus.QR_IN_PROGRESS);
				if (productQRCodeItemDetails.getRegionType() == null
						&& productQRCodeItemDetails.getAtNumber().equals(barCodeList.get(index))) {
					errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
					return null;
				}
			}
		}
		selectedProductQRCodeItem.setProductStatus(ProductQRCodeStatus.QR_IN_PROGRESS);

		try {
//			ObjectMapper map = new ObjectMapper();
//			String jsonResponses = map.writeValueAsString(productQRCode);
//
//			log.info("submitQRCode()-jsonResponses - " + jsonResponses);

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/create";
			BaseDTO baseDTO = null;

			baseDTO = httpService.post(url, productQRCode);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ProductQRCode productQR = mapper.readValue(jsonResponse, ProductQRCode.class);

				String urll = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/get/"
						+ productQR.getId();

				BaseDTO baseDto = null;

				baseDto = httpService.get(urll);
				if (baseDto != null && baseDto.getStatusCode() == 0) {
					ObjectMapper mapperr = new ObjectMapper();
					String jsonResponsee = mapperr.writeValueAsString(baseDto.getResponseContent());
					productQRCode = mapperr.readValue(jsonResponsee, ProductQRCode.class);
				}

				log.info("QRCode Added successfully");
				errorMap.notify(ErrorDescription.PRODCUT_QR_CODE_SUBMITTED_SUCCESSFULLY.getCode());
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
			}

		} catch (Exception exp) {
			log.info("Exception in submitQRCode method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String completeQRCode() {

		// log.info("completeQRCode method started");
		// productQRCode.setProductQRCodeStatus(ProductQRCodeStatus.QR_PENDING);
		//
		// String url = SERVER_URL + appPreference.getOperationApiUrl() +
		// "/product/qr/code/updateqrcodefinalstatus";
		// BaseDTO baseDTO = null;
		// try {
		// baseDTO = httpService.post(url, productQRCode);
		// return LIST_PAGE_URL;
		// } catch (Exception exp) {
		// log.info("Exception in completeQRCode method");
		// errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		// }
		// if (baseDTO != null) {
		// if (baseDTO.getStatusCode() == 0) {
		// log.info("QRCode Added successfully");
		// errorMap.notify(ErrorDescription.PRODCUT_QR_CODE_SUBMITTED_SUCCESSFULLY.getCode());
		// } else {
		// String msg = baseDTO.getErrorDescription();
		// log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " +
		// msg);
		// }
		// } else {
		// log.info("failiure in completeQRCode method ");
		// }
		// return null;

		return LIST_PAGE_URL;
	}

	public void getSupplierList() {

		log.info("getSupplierList method started");

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/getsupplierlist";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			supplierMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
			});

		} catch (Exception exp) {
			log.info("Exception in submitQRCode method");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

	}

	public Double getLength(List<ProductQRCodeItemDetailsDTO> productQRCodeItemDetailsDTOList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : productQRCodeItemDetailsDTOList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getProductLength();
			}
		}
		return null;
	}

	public Double getWidth(List<ProductQRCodeItemDetailsDTO> productQRCodeItemDetailsDTOList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : productQRCodeItemDetailsDTOList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getProductWidth();
			}
		}
		return null;
	}

	public Double getPicksPerInch(List<ProductQRCodeItemDetailsDTO> productQRCodeItemDetailsDTOList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : productQRCodeItemDetailsDTOList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getPicksPerInch();
			}
		}
		return null;
	}

	public Double getEndsPerInch(List<ProductQRCodeItemDetailsDTO> productQRCodeItemDetailsDTOList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : productQRCodeItemDetailsDTOList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getEndsPerInch();
			}
		}
		return null;
	}

	public String editQRCode(Long workLocationId) {
		totalQRCodeGeneratedVarities = 0;
		stockTransferList = new ArrayList<>();

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/get/"
				+ productQRCodeResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productQRCode = mapper.readValue(jsonResponse, ProductQRCode.class);
			supplierMasterList = commonDataService.findSupplierByStatusAndTransferType(StockTransferStatus.SUBMITTED,
					StockTransferType.WAREHOUSE_INWARD, workLocationId);
			// supplierId = productQRCode.getStockTransfer().getSupplierMaster().getId();
			supplierId = productQRCode.getStockTransfer().getSupplierMaster();
			// getStockTransferListByValue();
			if (productQRCode.getStockTransfer().getReferenceNumber() != null
					&& productQRCode.getStockTransfer().getDateTransferred() != null) {
				productQRCode.getStockTransfer().setCreatedByName(productQRCode.getStockTransfer().getReferenceNumber()
						+ " / " + AppUtil.DATE_FORMAT.format(productQRCode.getStockTransfer().getDateTransferred()));
			} else {
				productQRCode.getStockTransfer()
						.setCreatedByName(productQRCode.getStockTransfer().getReferenceNumber());
			}
			stockTransferList.add(productQRCode.getStockTransfer());

			String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/product/qr/code/getqcbystocktransferid/" + productQRCode.getStockTransfer().getId();

			RetailQualityCheck retailQualityCheck = null;
			try {
				baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailQualityCheck = mapper.readValue(jsonValue, RetailQualityCheck.class);
					if (retailQualityCheck != null) {
						qualityCheckId = retailQualityCheck.getId();
						log.info("qualityCheckId==>" + qualityCheckId);
					}
				}
			} catch (Exception exception) {
				log.error("Exception ", exception);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
			totalQRCodeGeneratedVarities = calculateProductCounts(productQRCode.getProductQRCodeItemList(),
					ProductQRCodeStatus.QR_COMPLETE);
			getProductItemCounts();
			log.info("ProductQRCode Edit Method", productQRCode);
		} catch (Exception exp) {
			log.info("Exception in editQRCode method");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return ADD_PAGE_URL;
	}

	public String addBarCodeValue() {
		log.info("addBarCodeValue - ");
		if (regionType == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
			return null;
		}
		if (atNumberValue == null
				&& (selectedProductQRCodeItemList == null || selectedProductQRCodeItemList.size() == 0)) {
			errorMap.notify(ErrorDescription.PLEASE_INPUT_AT_NUMBER.getCode());
			return null;
		}
		barCodeValue = atNumberValue == null ? null : new String(atNumberValue);
		onChangeBarCodeValue();
		return null;
	}

	public String onChangeBarCodeValue() {
		log.info("productQRCodeBean.onChangeBarCodeValue method started", barCodeValue);
		String barCode = null;
		barCodeList = new ArrayList<>();
		if (barCodeValue != null) {
			barCode = barCodeValue.trim();
			atNumberValue = new String(barCode);
		}

		if (regionType == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
			barCodeValue = null;
			selectedProductQRCodeItemList = new ArrayList<>();
			return null;
		}
		ScanProductDTO scanProductDTO = new ScanProductDTO();
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/getproductpricedetails/";
		if (StringUtils.isNotEmpty(barCode)) {
			barCodeList.add(barCode);
			scanProductDTO.setAtNumberList(barCodeList);
			scanProductDTO.setAtNumber(barCode);
		} else {
			if (selectedProductQRCodeItemList != null && !selectedProductQRCodeItemList.isEmpty()) {
				barCodeList = new ArrayList<>();
				for (ProductQRCodeItemDetails productQRCodeItemDetailsObj : selectedProductQRCodeItemList) {
					String barCodeValue = productQRCodeItemDetailsObj.getAtNumber().trim();
					barCodeList.add(new String(barCodeValue));
				}
			}
		}
		scanProductDTO.setAtNumberList(barCodeList);
		scanProductDTO.setRegionType(regionType);
		scanProductDTO.setProductId(selectedProductQRCodeItem.getProductVarietyMaster().getId());
		scanProductDTO.setStockInwardId(productQRCode.getStockTransfer().getId());
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, scanProductDTO);
			ObjectMapper mapper = new ObjectMapper();
			/*
			 * String jsonResponse =
			 * mapper.writeValueAsString(baseDTO.getResponseContent()); singleProductItem =
			 * mapper.readValue(jsonResponse, ProductQRCodeItemDetailsDTO.class);
			 */

			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			multipleProductItemList = mapper.readValue(jsonResponse,
					new TypeReference<List<ProductQRCodeItemDetailsDTO>>() {
					});

			/*
			 * if (singleProductItem == null) {
			 * log.info("onChangeBarCodeValue Price Details Not available" +
			 * singleProductItem);
			 * errorMap.notify(ErrorDescription.PRICE_NOT_AVAILABLE.getCode()); barCodeValue
			 * = null; return null; }
			 */
			if (multipleProductItemList == null) {
				log.info("onChangeBarCodeValue Price Details Not available" + singleProductItem);
				if (baseDTO.getStatusCode().equals(
						ErrorDescription.getError(OperationErrorCode.PURCHASE_PRICE_NOT_AVAILABLE).getErrorCode())) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.PURCHASE_PRICE_NOT_AVAILABLE).getCode());
				} else if (baseDTO.getStatusCode().equals(ErrorDescription
						.getError(OperationErrorCode.PROFIT_MARGIN_MASTER_NOT_AVAILABLE).getErrorCode())) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.PROFIT_MARGIN_MASTER_NOT_AVAILABLE).getCode());
				} else {
					errorMap.notify(ErrorDescription.PRICE_NOT_AVAILABLE.getCode());
				}
				barCodeValue = null;
				barCodeList = new ArrayList<>();
				return null;

			}
			if (multipleProductItemList != null && multipleProductItemList.size() > 0
					&& !multipleProductItemList.isEmpty()) {
				for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTOObj : multipleProductItemList) {
					updateMultipleProductItem(productQRCodeItemDetailsDTOObj);
				}
			}

			calculateTotalRetailandPurchasePrice();

			log.info("productQRCodeBean.onChangeBarCodeValue method started", singleProductItem);
		} catch (Exception exp) {
			log.info("Exception  in getting Price Details " + exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		// upateProductItem(barCode);
		log.info("list value" + productQRCodeItemDetailsDTOList);

		/*
		 * if (regionType.equals(RegionType.INNER_STATE)) { totalIntraRegionItems =
		 * totalProductItems - totalInterRegionItems; } if
		 * (regionType.equals(RegionType.OUTER_STATE)) { totalInterRegionItems =
		 * totalProductItems - totalIntraRegionItems; }
		 */

		updateRegionCount();

		barCodeValue = null;
		barCodeList = new ArrayList<>();

		return null;
	}

	public void calculateTotalRetailandPurchasePriceOld() {
		totalPurchasePrice = 0.0;
		totalRetailPrice = 0.0;

		if (productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList() != null
				&& !productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList().isEmpty()) {
			for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
					.getProductQRCodeItemDetailsList()) {
				if (productQRCodeItemDetails.getPurchasePrice() != null) {
					log.info("getPurchasePrice==> " + productQRCodeItemDetails.getPurchasePrice());
					totalPurchasePrice = totalPurchasePrice + productQRCodeItemDetails.getPurchasePrice();
				}
				if (productQRCodeItemDetails.getRetailPrice() != null) {
					log.info("getRetailPrice==> " + productQRCodeItemDetails.getRetailPrice());
					totalRetailPrice = totalRetailPrice + productQRCodeItemDetails.getRetailPrice();
				}

			}
		}
		log.info("totalPurchasePrice==> " + totalPurchasePrice);
		log.info("totalRetailPrice==> " + totalRetailPrice);
	}
	
	public void calculateTotalRetailandPurchasePrice() {
		totalPurchasePrice = 0D;
		totalRetailPrice = 0D;

		List<ProductQRCodeItemDetails> productQRCodeItemDetailsList = productQRCode.getProductQRCodeItemList(
				).get(index).getProductQRCodeItemDetailsList();
		if (!CollectionUtils.isEmpty(productQRCodeItemDetailsList)) {
			
			totalPurchasePrice = productQRCodeItemDetailsList.stream().filter(o->o.getPurchasePrice() != null).collect(Collectors.summingDouble
					(ProductQRCodeItemDetails::getPurchasePrice));
			
			totalRetailPrice = productQRCodeItemDetailsList.stream().filter(o->o.getRetailPrice() != null).collect(Collectors.summingDouble
					(ProductQRCodeItemDetails::getRetailPrice));
			
		}
		log.info("totalPurchasePrice==> " + totalPurchasePrice);
		log.info("totalRetailPrice==> " + totalRetailPrice);
	}

	public void updateRegionCount() {

		/*
		 * totalInterRegionItems = calculateProductRegionTypeCount(
		 * productQRCode.getProductQRCodeItemList().get(index).
		 * getProductQRCodeItemDetailsList(), RegionType.INNER_STATE);
		 * 
		 * log.info("totalInterRegionItems", totalInterRegionItems);
		 * 
		 * totalIntraRegionItems = calculateProductRegionTypeCount(
		 * productQRCode.getProductQRCodeItemList().get(index).
		 * getProductQRCodeItemDetailsList(), RegionType.OUTER_STATE);
		 */

		log.info("totalIntraRegionItems", totalIntraRegionItems);

	}

	public String completeScan() {

		Integer completedCount = 0;

		for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
				.get(index).getProductQRCodeItemDetailsList()) {
			if (productQRCodeItemDetailsValue.getRegionType() != null) {
				completedCount = completedCount + 1;
			}
		}

		if (completedCount == productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList()
				.size()) {
			errorMap.notify(ErrorDescription.ALL_SCAN_COMPLETED.getCode());
			return null;
		}

		if (regionType == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
			return null;
		}

		StringBuilder atNumber = new StringBuilder();

		ProductQRCodeDetailsDTO productQRCodeDetailsDTO = new ProductQRCodeDetailsDTO();
		List<ProductQRCodeItemDetailsDTO> productDetailDTOList = null;

		for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
				.get(index).getProductQRCodeItemDetailsList()) {
			if (productQRCodeItemDetailsValue.getRegionType() == null) {
				barCodeValue = productQRCodeItemDetailsValue.getAtNumber();
				atNumber.append(productQRCodeItemDetailsValue.getAtNumber()).append(",");
			}
			productQRCodeItemDetailsValue.setRegionType(regionType);
		}

		/*
		 * if (regionType.equals(RegionType.INNER_STATE)) {
		 * productQRCodeDetailsDTO.setRegionType(RegionType.OUTER_STATE); } else {
		 * productQRCodeDetailsDTO.setRegionType(RegionType.INNER_STATE); }
		 */

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/getallpricedetails";
		productQRCodeDetailsDTO.setAtNumber(atNumber);
		productQRCodeDetailsDTO.setStockInwardId(productQRCode.getStockTransfer().getId());
		productQRCodeDetailsDTO.setRegionType(regionType);
		productQRCodeDetailsDTO
				.setProductId(productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, productQRCodeDetailsDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productDetailDTOList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductQRCodeItemDetailsDTO>>() {
						});
			}

		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		try {
			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				if (productQRCodeItemDetailsValue.getRegionType() == null) {
					barCodeValue = productQRCodeItemDetailsValue.getAtNumber();
					/*
					 * if (regionType.equals(RegionType.INNER_STATE)) {
					 * productQRCodeItemDetailsValue.setRegionType(RegionType.OUTER_STATE); } else {
					 * productQRCodeItemDetailsValue.setRegionType(RegionType.INNER_STATE); }
					 */
					productQRCodeItemDetailsValue.setPurchasePrice(
							getPurchasePrice(productDetailDTOList, productQRCodeItemDetailsValue.getAtNumber()));
					productQRCodeItemDetailsValue.setRetailPrice(
							getRetailPrice(productDetailDTOList, productQRCodeItemDetailsValue.getAtNumber()));
				}
			}
		} catch (Exception exp) {
			log.info("Exception In Complete Scan Method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		updateRegionCount();
		return null;
	}

	public String deleteProductItem(int indexValue) {

		if (productQRCode.getProductQRCodeItemList().get(index).getProductStatus()
				.equals(ProductQRCodeStatus.QR_COMPLETE)) {
			errorMap.notify(ErrorDescription.PRODUCT_QR_CODE_COMPLETED.getCode());
			return null;
		}

		productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList().get(indexValue)
				.setRegionType(null);
		productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList().get(indexValue)
				.setPurchasePrice(null);
		productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList().get(indexValue)
				.setRetailPrice(null);

		updateRegionCount();

		return null;
	}

	public String clearValues() {
		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			productQRCodeItemDetails.setRegionType(null);
			productQRCodeItemDetails.setPurchasePrice(null);
			productQRCodeItemDetails.setRetailPrice(null);
		}
		return null;
	}

	public void upateProductItem(String value) {
		try {
			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				if (productQRCodeItemDetailsValue.getAtNumber().equalsIgnoreCase(value)) {
					productQRCodeItemDetailsValue.setRegionType(regionType);
					productQRCodeItemDetailsValue.setPurchasePrice(singleProductItem.getPurchasePrice());
					productQRCodeItemDetailsValue.setRetailPrice(singleProductItem.getRetailPrice());
				}
			}
		} catch (Exception exp) {
			log.info("Exception In upateProductItem Method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void updateMultipleProductItem(ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTOObj) {
		try {
			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				if (productQRCodeItemDetailsValue.getAtNumber()
						.equalsIgnoreCase(productQRCodeItemDetailsDTOObj.getAtNumber().trim())) {
					productQRCodeItemDetailsValue.setRegionType(regionType);
					productQRCodeItemDetailsValue.setPurchasePrice(productQRCodeItemDetailsDTOObj.getPurchasePrice());
					productQRCodeItemDetailsValue.setRetailPrice(productQRCodeItemDetailsDTOObj.getRetailPrice());
				}
			}
		} catch (Exception exp) {
			log.info("Exception In upateProductItem Method", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public Double getPurchasePrice(List<ProductQRCodeItemDetailsDTO> purchasePriceList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : purchasePriceList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getPurchasePrice();
			}
		}
		return null;
	}

	public Double getRetailPrice(List<ProductQRCodeItemDetailsDTO> purchasePriceList, String atNumber) {
		for (ProductQRCodeItemDetailsDTO productQRCodeItemDetailsDTO : purchasePriceList) {
			if (productQRCodeItemDetailsDTO.getAtNumber().equals(atNumber)) {
				return productQRCodeItemDetailsDTO.getRetailPrice();
			}
		}
		return null;
	}

	public void loadLazyList() {

		productQRCodeResponseLazyList = new LazyDataModel<ProductQRCodeResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<ProductQRCodeResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<ProductQRCodeResponse> data = new ArrayList<ProductQRCodeResponse>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<ProductQRCodeResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(ProductQRCodeResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ProductQRCodeResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<ProductQRCodeResponse> responseList = (List<ProductQRCodeResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (ProductQRCodeResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void processDelete() {
		log.info("productQRCodeBean processDelete method started");
		if (productQRCodeResponse == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE_QR_CODE.getCode());
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		log.info("productQRCodeBean delete method started");
		if (productQRCodeResponse == null || productQRCodeResponse.getId() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE_QR_CODE.getCode());
			return null;
		}

		if (productQRCodeResponse == null
				|| productQRCodeResponse.getStatus().equals(ProductQRCodeStatus.QR_COMPLETE)) {
			errorMap.notify(ErrorDescription.QR_COMPLETED_CANNOT_BE_DELETED.getCode());
			return null;
		}

		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/delete/"
				+ productQRCodeResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.PRODCUT_QR_CODE_DELETED_SUCCESSFULLY.getCode());
			return getListPage();
		} else {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return getListPage();

	}

	/**
	 * This method is generating the PDF based on the search result.
	 */

	public void generatePDF() {

		log.info("Generate PDF started....");

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/pdf/download";

		try {

			ProductQRCodeRequest request = getSearchRequestObject(null, null, sortingField, sortingOrder, filtersMap);

			log.info("Generate PDF [" + request + "]");

			InputStream inputStream = httpService.generatePDF(URL, request);

			downloadFile = new DefaultStreamedContent(inputStream, "application/pdf", "ProductQRCodeList.pdf");

			log.info("Generate PDF finished successfully....");

		} catch (Exception e) {
			log.error("Exception ", e);
		}

	}

	/**
	 * This method is generating the PDF For QRCode Images
	 */

	/*
	 * public void downloadQRCodeZip() {
	 * 
	 * log.info("downloadQRCodeZip started....");
	 * 
	 * String URL = SERVER_URL + appPreference.getOperationApiUrl() +
	 * "/product/qr/code/createqrcodezip";
	 * 
	 * try {
	 * 
	 * ProductQRCodeDetailsDTO request = new ProductQRCodeDetailsDTO();
	 * 
	 * request.setProductQRCodeItemDetailsEntityList(
	 * productQRCode.getProductQRCodeItemList().get(index).
	 * getProductQRCodeItemDetailsList());
	 * 
	 * request.setQrCodeDirectoryPath(productQRCode.getProductQRCodeItemList().get(
	 * index).getQrZipFilePath());
	 * 
	 * log.info("downloadQRCodeZip [" + request + "]");
	 * 
	 * InputStream inputStream = httpService.generatePDF(URL, request);
	 * 
	 * downloadFile = new DefaultStreamedContent(inputStream, "application/zip",
	 * productQRCode.getStockTransfer().getReferenceNumber() + "_" +
	 * productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster()
	 * .getCode() + "_" +
	 * productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster()
	 * .getName() + ".zip", Charsets.UTF_8.name());
	 * 
	 * log.info("downloadQRCodeZip finished successfully....");
	 * 
	 * } catch (Exception e) { log.error("Exception ", e); }
	 * 
	 * }
	 */

	public String generateQRCodeByTable(int idx) {
		log.info(" <<< --- Start the generateQRCodeByTable() --->>> ");
		log.info("<<<--- ProductQrCode Item List .. Index Position [ " + idx + "]");
		index = idx;
		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			if (productQRCodeItemDetails.getRegionType() == null) {
				errorMap.notify(ErrorDescription.PLEASE_COMPLETE_REGION_CATEGORIZATION.getCode());
				return null;
			}
		}

		/*
		 * if (productQRCode.getProductQRCodeItemList().get(index).getProductStatus()
		 * .equals(ProductQRCodeStatus.QR_COMPLETE)) {
		 * errorMap.notify(ErrorDescription.PRODUCT_QR_CODE_COMPLETED.getCode()); return
		 * null; }
		 */

		generateQRCode();
		log.info(" <<< --- End the generateQRCodeByTable() --->>> ");
		return null;
	}

	/*
	 * Generate QR Code Method For product
	 */
	public String generateQRCode() {

		log.info(".....generateQRCode started....");

		generateQRCodeInAsynchMode();

		return null;
	}

	private String generateQRCodeInAsynchMode() {

		log.info(".....generateQRCode started....");

		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			productQRCodeItemDetails.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);
			if (productQRCodeItemDetails.getRegionType() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
				return null;
			}
		}
		StringBuilder atNumbers = new StringBuilder();

		BaseDTO baseDTO = new BaseDTO();

		/**
		 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller/
		 * 
		 * ProductQRCodeController.java
		 */
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/asyncgenerateqrcodeforitem";

		try {

			ProductQRCodeDetailsDTO request = new ProductQRCodeDetailsDTO();

			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				atNumbers.append("'").append(productQRCodeItemDetailsValue.getAtNumber()).append("'").append(",");
			}

			if (atNumbers.length() > 0) {
				atNumbers.setLength(atNumbers.length() - 1);
			}

			request.setAtNumber(atNumbers);

			request.setProductQRCodeItemDetailsEntityList(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

			request.setInwardNumber(productQRCode.getStockTransfer().getReferenceNumber());

			request.setWareHouseCode(String.valueOf(productQRCode.getToEntity().getCode()));

			request.setProductQRCodeId(productQRCode.getId());
			request.setProductVarietyCode(
					productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getCode());
			request.setProductId(productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
//			request.setQrCodeDirectoryPath(productQRCode.getProductQRCodeItemList().get(index).getQrZipFilePath());
			// Newly added.
			// request.setIndex(index);
			request.setProductItemId(productQRCode.getProductQRCodeItemList().get(index).getId());
			// request.setProductQRCode(productQRCode);
			request.setStockInwardId(productQRCode.getStockTransfer().getId());
			log.info("generateQRCode [" + request + "]");
			/*
			 * This HttpRequest to generate QR Code and store the generated QR Code for
			 * 
			 * @ProductQrCodeItemsDetails
			 * 
			 * @StockTransferItems
			 * 
			 * @InventoryItems
			 */
			baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {

					String message = baseDTO.getMessage();

					log.info("QRCode Generated successfully. Message: " + message);

					if (StringUtils.isNotEmpty(message)) {
						AppUtil.addInfo(message);
					} else {
						AppUtil.addError("No response from server. Please contact support.");
					}

				} else {
					String errorMsg = baseDTO.getMessage();
					if (StringUtils.isEmpty(errorMsg)) {
						errorMsg = "Unknown error. Qr code generation not completed. Please contact support.";
					}

					log.info("QRCode Generation - Error Message: " + errorMsg);

					AppUtil.addError(errorMsg);
				}
			} else {
				log.error("Empty response from server. Please contact support.");
				AppUtil.addError("Empty response from server. Please contact support.");
			}

			log.info("generateQRCode finished successfully....");

		} catch (Exception e) {
			log.error("Exception at generateQRCode()", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	@SuppressWarnings("unused")
	private String generateQRCodeInSynchMode() {

		log.info(".....generateQRCode started....");

		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			productQRCodeItemDetails.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);
			if (productQRCodeItemDetails.getRegionType() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
				return null;
			}
		}
		StringBuilder atNumbers = new StringBuilder();

		BaseDTO baseDTO = new BaseDTO();

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/generateqrcodeforitem";

		try {

			ProductQRCodeDetailsDTO request = new ProductQRCodeDetailsDTO();

			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				atNumbers.append("'").append(productQRCodeItemDetailsValue.getAtNumber()).append("'").append(",");
			}

			if (atNumbers.length() > 0) {
				atNumbers.setLength(atNumbers.length() - 1);
			}

			request.setAtNumber(atNumbers);

			request.setProductQRCodeItemDetailsEntityList(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

			request.setInwardNumber(productQRCode.getStockTransfer().getReferenceNumber());

			request.setWareHouseCode(String.valueOf(productQRCode.getToEntity().getCode()));

			request.setProductQRCodeId(productQRCode.getId());
			request.setProductVarietyCode(
					productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getCode());
			request.setProductId(productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
//			request.setQrCodeDirectoryPath(productQRCode.getProductQRCodeItemList().get(index).getQrZipFilePath());
			// Newly added.
			// request.setIndex(index);
			request.setProductItemId(productQRCode.getProductQRCodeItemList().get(index).getId());
			// request.setProductQRCode(productQRCode);
			request.setStockInwardId(productQRCode.getStockTransfer().getId());
			log.info("generateQRCode [" + request + "]");
			/*
			 * This HttpRequest to generate QR Code and store the generated QR Code for
			 * 
			 * @ProductQrCodeItemsDetails
			 * 
			 * @StockTransferItems
			 * 
			 * @InventoryItems
			 */
			baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("QRCode Generated successfully");
					String message = baseDTO.getMessage();

					if (StringUtils.isNotEmpty(message)) {
						AppUtil.addInfo(message);
					} else {
						errorMap.notify(ErrorDescription.PRODUCT_QR_CODE_GENERATED_SUCCESSFULLY.getCode());
					}

					// ProductQRCodeItemDetails set product status to QR_COMPLETE
					for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode
							.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList()) {
						productQRCodeItemDetailsValue.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);
					}
					// ProductQRCodeItem set product status to QR_COMPLETE
					productQRCode.getProductQRCodeItemList().get(index)
							.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);

					totalQRCodeGeneratedAtNumberWiseItems = calculateProductItemCounts(
							productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList(),
							ProductQRCodeStatus.QR_COMPLETE);

					totalQRCodeYetToGenerateAtNumberWiseItems = productQRCode.getProductQRCodeItemList().get(index)
							.getProductQRCodeItemDetailsList().size() - totalQRCodeGeneratedAtNumberWiseItems;

					totalQRCodeGeneratedVarities = calculateProductCounts(productQRCode.getProductQRCodeItemList(),
							ProductQRCodeStatus.QR_COMPLETE);

					getProductItemCounts();

				} else {
					String errorMsg = baseDTO.getMessage();
					if (StringUtils.isEmpty(errorMsg)) {
						errorMsg = "Unknown error. Qr code generation not completed. Please contact support.";
					}
					AppUtil.addError(errorMsg);
				}
			} else {
				log.info("failiure in generateQRCode method ");
			}

			log.info("generateQRCode finished successfully....");

		} catch (Exception e) {
			log.error("Exception at generateQRCode()", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	/*
	 * Generate QR Code Method For product
	 */
	public String generateQRCodeOld() {

		log.info(".....generateQRCode started....");

		for (ProductQRCodeItemDetails productQRCodeItemDetails : productQRCode.getProductQRCodeItemList().get(index)
				.getProductQRCodeItemDetailsList()) {
			productQRCodeItemDetails.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);
			if (productQRCodeItemDetails.getRegionType() == null) {
				errorMap.notify(ErrorDescription.PLEASE_SELECT_REGION_TYPE.getCode());
				return null;
			}
		}
		StringBuilder atNumbers = new StringBuilder();

		BaseDTO baseDTO = new BaseDTO();

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/generateqrcodeforitem";

		try {

			ProductQRCodeDetailsDTO request = new ProductQRCodeDetailsDTO();

			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				atNumbers.append("'").append(productQRCodeItemDetailsValue.getAtNumber()).append("'").append(",");
			}

			if (atNumbers.length() > 0) {
				atNumbers.setLength(atNumbers.length() - 1);
			}

			request.setAtNumber(atNumbers);

			request.setProductQRCodeItemDetailsEntityList(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

			request.setInwardNumber(productQRCode.getStockTransfer().getReferenceNumber());

			request.setWareHouseCode(String.valueOf(productQRCode.getToEntity().getCode()));

			request.setProductQRCodeId(productQRCode.getId());
			request.setProductVarietyCode(
					productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getCode());
			request.setProductId(productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
//			request.setQrCodeDirectoryPath(productQRCode.getProductQRCodeItemList().get(index).getQrZipFilePath());

			log.info("generateQRCode [" + request + "]");

			baseDTO = httpService.post(URL, request);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("QRCode Generated successfully");
					errorMap.notify(ErrorDescription.PRODUCT_QR_CODE_GENERATED_SUCCESSFULLY.getCode());

					for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode
							.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList()) {
						productQRCodeItemDetailsValue.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);
					}

					productQRCode.getProductQRCodeItemList().get(index)
							.setProductStatus(ProductQRCodeStatus.QR_COMPLETE);

					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/product/qr/code/updateqrcodestatus";

					request.setProductItemId(productQRCode.getProductQRCodeItemList().get(index).getId());

					request.setProductQRCodeItemDetailsEntityList(
							productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

					baseDTO = httpService.post(url, request);

					totalQRCodeGeneratedAtNumberWiseItems = calculateProductItemCounts(
							productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList(),
							ProductQRCodeStatus.QR_COMPLETE);

					totalQRCodeYetToGenerateAtNumberWiseItems = productQRCode.getProductQRCodeItemList().get(index)
							.getProductQRCodeItemDetailsList().size() - totalQRCodeGeneratedAtNumberWiseItems;

					totalQRCodeGeneratedVarities = calculateProductCounts(productQRCode.getProductQRCodeItemList(),
							ProductQRCodeStatus.QR_COMPLETE);

					getProductItemCounts();
					completeQRCode();

				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
				}
			} else {
				log.info("failiure in generateQRCode method ");
			}

			log.info("generateQRCode finished successfully....");

		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		productQRCodeResponse = ((ProductQRCodeResponse) event.getObject());
		addButtonFlag = true;
		if (productQRCodeResponse.getStatus().equals(ProductQRCodeStatus.QR_COMPLETE_PAR_NOT_GENERATED)) {
			editButtonFlag = true;
			generateButtonFlag = false;
		} else {
			editButtonFlag = false;
			generateButtonFlag = true;
		}
	}

	public void onPageLoad() {
		log.info("PurchaseOrderBean.onPageLoad method started");
		addButtonFlag = false;
	}

	public String getAddPage(Long workLocationId) {
		log.info("Add Button is pressed....");
		productQRCode = new ProductQRCode();
		productQRCode.setStockTransfer(new StockTransfer());
		productQRCode.setProductQRCodeItemList(new ArrayList<>());
		totalProductVarities = 0;
		totalQRCodeGeneratedVarities = 0;
		totalQRCodeYetToGeneratedVarities = 0;
		totalProductItems = 0;
		totalQRCodeGeneratedItems = 0;
		totalQRCodeYetToGeneratedItems = 0;
		totalIntraRegionItems = 0;
		totalInterRegionItems = 0;
		totalQRCodeGeneratedAtNumberWiseItems = 0;
		totalProductItemWiseCount = 0;
		totalProductItemWiseQRCodeCount = 0;
		totalProductItemWiseQRCodeToBeGenerateCount = 0;
		supplierId = null;
		barCodeValue = null;
		barCodeList = new ArrayList<>();
		atNumberValue = null;
		index = null;
		// supplierMasterList =
		// commonDataService.findSupplierByStatusAndTransferType(StockTransferStatus.SUBMITTED,
		// StockTransferType.WAREHOUSE_INWARD, workLocationId);
		loadRegionTypeValues();
		return ADD_PAGE_URL;
	}

	public void onSelectSupplierMaster() {
		log.info(" onSelectSupplierMaster called");

		stockTransferList = new ArrayList<>();
		productQRCode.setStockTransfer(null);

		if (productQRCode.getFromEntity() != null) {
			stockTransferList = commonDataService.findStockBySupplierAndStatusAndTransferType(
					productQRCode.getFromEntity().getId(), StockTransferStatus.SUBMITTED,
					StockTransferType.SOCIETY_OUTWARD, null);

			for (StockTransfer st : stockTransferList) {
				String dateTransferred = AppUtil.DATE_FORMAT.format(st.getDateTransferred());
				st.setCreatedByName(st.getReferenceNumber() + " / " + dateTransferred);
			}

		}
	}

	public String getViewPage() {
		log.info("View Button is pressed....");
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/get/"
				+ productQRCodeResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productQRCode = mapper.readValue(jsonResponse, ProductQRCode.class);

			String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/product/qr/code/getqcbystocktransferid/" + productQRCode.getStockTransfer().getId();

			RetailQualityCheck retailQualityCheck = null;
			try {
				baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					retailQualityCheck = mapper.readValue(jsonValue, RetailQualityCheck.class);
					if (retailQualityCheck != null) {
						qualityCheckId = retailQualityCheck.getId();
						log.info("qualityCheckId==>" + qualityCheckId);
					}
				}
			} catch (Exception exception) {
				log.error("Exception ", exception);
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
			totalQRCodeGeneratedVarities = calculateProductCounts(productQRCode.getProductQRCodeItemList(),
					ProductQRCodeStatus.QR_COMPLETE);
			getProductItemCounts();
			log.info("ProductQRCode Edit Method", productQRCode);
		} catch (Exception exp) {
			log.info("Exception in editQRCode method");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		try {
			if (productQRCodeResponse == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			loadRegionTypeValues();
			getQRCodeById();
			if (productQRCode != null) {
				log.info("productQRCode==>" + productQRCode);
				if (productQRCode.getFromEntity() != null) {
					senderAddress = AppUtil.prepareAddress(productQRCode.getFromEntity().getAddressMaster());
				}
				if (productQRCode.getToEntity() != null) {
					receiverAddress = AppUtil.prepareAddress(productQRCode.getToEntity().getAddressMaster());
				}
				if (productQRCode.getProductQRCodeItemList() != null) {
					totalQRCodeGeneratedVarities = calculateProductCounts(productQRCode.getProductQRCodeItemList(),
							ProductQRCodeStatus.QR_COMPLETE);
				}
			}
			getProductItemCounts();
		} catch (Exception e) {
			e.printStackTrace();
			log.error("productQRCodeBean :: getViewPage==>", e);
		}
		return VIEW_PAGE_URL;
	}

	public void getStockTransferListByValue() {
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/product/qr/code/getstocktransferlist";
			SearchDTO dto = new SearchDTO();
			dto.setSupplierId(supplierId.getId());
			BaseDTO baseDTO = httpService.post(url, dto);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			stockTransferList = mapper.readValue(jsonResponse, new TypeReference<List<StockTransfer>>() {
			});

			if (stockTransferList != null) {
				stockTransferList.stream().forEach(st -> {
					String formatTransferDate = st.getDateTransferred() == null ? ""
							: " / " + AppUtil.DATE_FORMAT.format(st.getDateTransferred());
					st.setCreatedByName(st.getReferenceNumber() + formatTransferDate);
				});
			}

		} catch (JsonProcessingException e) {
			log.error("JsonProcessingException ", e);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
	}

	public String getProductQRCodeItemData() {
		log.info("getStockTransferData started " + productQRCode);

		if (supplierId == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_SOCIETY.getCode());
			return null;
		}

		if (productQRCode == null || productQRCode.getStockTransfer() == null
				|| productQRCode.getStockTransfer().getReferenceNumber() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_STOCK_TRANSFER_NUMBER.getCode());
			return null;
		}

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/product/qr/code/getqcbystocktransferid/" + productQRCode.getStockTransfer().getId();

		RetailQualityCheck retailQualityCheck = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				retailQualityCheck = mapper.readValue(jsonValue, RetailQualityCheck.class);
				qualityCheckId = retailQualityCheck.getId();
			} else {
				log.error("Exception ", baseDTO.getErrorDescription());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
		}

		if (retailQualityCheck != null) {

			List<ProductQRCodeItem> productQRCodeItemList = new ArrayList<>();

			for (QualityCheckProductWise item : retailQualityCheck.getQualityCheckProductWiseList()) {
				ProductQRCodeItem productQRCodeItem = new ProductQRCodeItem();
				productQRCodeItem.setDispatchedQuantity(item.getDispatchedQuantity());
				productQRCodeItem.setReceivedQuantity(item.getReceivedQuantity());
				productQRCodeItem.setProductVarietyMaster(item.getProductVarietyMaster());
				productQRCodeItem.setProductStatus(ProductQRCodeStatus.QR_PENDING);
				productQRCodeItem.setProductQRCodeItemDetailsList(
						getItemDetailsList(item.getRetailQualityCheckDetailList(), item.getProductVarietyMaster()));
				productQRCodeItemList.add(productQRCodeItem);
			}
			totalProductVarities = productQRCodeItemList.size();
			totalQRCodeGeneratedVarities = calculateProductCounts(productQRCodeItemList,
					ProductQRCodeStatus.QR_COMPLETE);
			totalQRCodeYetToGeneratedVarities = calculateProductCounts(productQRCodeItemList,
					ProductQRCodeStatus.QR_PENDING);
			productQRCode.setProductQRCodeItemList(productQRCodeItemList);
			productQRCode.setFromEntity(retailQualityCheck.getFromEntity());
			productQRCode.setToEntity(retailQualityCheck.getToEntity());
			productQRCode.setStockTransfer(retailQualityCheck.getStockTransfer());
			// societyAddress =
			// AppUtil.prepareAddress(retailQualityCheck.getFromEntity().getAddressMaster());
			// productWarehouseAddress =
			// AppUtil.prepareAddress(retailQualityCheck.getToEntity().getAddressMaster());
			getProductItemCounts();
			return null;
		}
		return null;
	}

	public List<ProductQRCodeItemDetails> getItemDetailsList(
			List<RetailQualityCheckDetails> retailQualityCheckDetailsList, ProductVarietyMaster productVarietyMaster) {

		List<ProductQRCodeItemDetails> productQRCodeItemDetailsList = new ArrayList<>();

		for (RetailQualityCheckDetails detailsValue : retailQualityCheckDetailsList) {
			if (detailsValue.getItemStatus().equals(ItemStatus.ACCEPTED)) {
				ProductQRCodeItemDetails productQRCodeItemDetails = new ProductQRCodeItemDetails();
				productQRCodeItemDetails.setAtNumber(detailsValue.getAtNumber());
				productQRCodeItemDetails.setProductVarietyMaster(productVarietyMaster);
				productQRCodeItemDetails.setProductStatus(ProductQRCodeStatus.QR_PENDING);
				productQRCodeItemDetailsList.add(productQRCodeItemDetails);
			}
		}
		return productQRCodeItemDetailsList;
	}

	public String getProductItemCounts() {
		ProductQRCodeDetailsDTO productQRCodeDetailsDTO = new ProductQRCodeDetailsDTO();

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/product/qr/code/getproductitemcountdetails";

		productQRCodeDetailsDTO.setQualityCheckId(qualityCheckId);
		Double recivedQuantity = null;
		if (index != null) {
			productQRCodeDetailsDTO.setProductId(
					productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
			recivedQuantity = productQRCode.getProductQRCodeItemList().get(index).getReceivedQuantity();
			log.info("<<=== recivedQuantity....." + recivedQuantity);
		}

		if (productQRCode.getId() != null) {
			productQRCodeDetailsDTO.setProductQRCodeId(productQRCode.getId());
		}

		try {
			BaseDTO baseDTO = httpService.post(URL, productQRCodeDetailsDTO);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				productQRCodeDetailsDTO = mapper.readValue(jsonValue, ProductQRCodeDetailsDTO.class);
				totalProductItemWiseCount = 0;
				if (productQRCodeDetailsDTO.getTotalProductItemCount() != null
						&& productQRCodeDetailsDTO.getTotalProductItemCount().intValue() > 0) {
					totalProductItemWiseCount = productQRCodeDetailsDTO.getTotalProductItemCount();
				} else if (recivedQuantity != null) {
					Integer val = (int) Math.round(recivedQuantity);
					totalProductItemWiseCount = val;
				}
				// totalProductItemWiseCount =
				// productQRCodeDetailsDTO.getTotalProductItemCount():recivedQuantity.intValue();
				totalProductItemWiseQRCodeCount = productQRCodeDetailsDTO.getTotalQRCodeGeneratedCount();
				if (totalProductItemWiseQRCodeCount == null) {
					totalProductItemWiseQRCodeCount = 0;
				}
				if (totalProductItemWiseCount != null)
					totalProductItemWiseQRCodeToBeGenerateCount = Math
							.abs(totalProductItemWiseCount - totalProductItemWiseQRCodeCount);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	private int calculateProductCounts(List<ProductQRCodeItem> items, ProductQRCodeStatus productStatus) {
		int count = 0;
		for (ProductQRCodeItem item : items) {
			if (item.getProductStatus().equals(productStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	private int calculateProductItemCounts(List<ProductQRCodeItemDetails> items, ProductQRCodeStatus productStatus) {
		int count = 0;
		for (ProductQRCodeItemDetails item : items) {
			if (item.getProductStatus().equals(productStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	private int calculateProductRegionTypeCount(List<ProductQRCodeItemDetails> items, RegionType regionStatus) {
		int count = 0;
		for (ProductQRCodeItemDetails item : items) {
			if (item.getRegionType() != null && item.getRegionType().equals(regionStatus)) {
				count = count + 1;
			}
		}
		return count;
	}

	public String getQRCodeById() {
		log.info("retailQRCOdeBean.getQRCodeById method started");
		if (productQRCodeResponse == null || productQRCodeResponse.getId() == null) {
			log.info("#==--> productQRCode is Not Available <--==#");
			errorMap.notify(ErrorDescription.PRODCUT_QR_CODE_NOT_EMPTY.getCode());
			return null;
		}
		String url = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/get/"
				+ productQRCodeResponse.getId();
		log.info("getQRCodeById url==>" + url);
		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
		} catch (Exception e) {
			log.error("Error in viewQRCode", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				productQRCode = mapper.readValue(jsonValue, ProductQRCode.class);
				log.info("getQRCodeById productQRCode==>" + productQRCode);
			} catch (IOException e) {
				log.error(e);
			}
		}
		if (productQRCode == null) {
			log.info("#==--> productQRCode is Not Available <--==#");
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return null;
	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("QR_CODE_PRICE_FIXATION_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}

	// to download all same product qrcodes into a single pdf file which is already
	// generated while generate qrcode..
	public void downloadQrCodePdf() {

		log.info("downloadQrCodePdf started....");

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/downloadqrcode";

		try {
			StringBuilder atNumbers = new StringBuilder();

			ProductQRCodeDetailsDTO request = new ProductQRCodeDetailsDTO();

			for (ProductQRCodeItemDetails productQRCodeItemDetailsValue : productQRCode.getProductQRCodeItemList()
					.get(index).getProductQRCodeItemDetailsList()) {
				atNumbers.append("'").append(productQRCodeItemDetailsValue.getAtNumber()).append("'").append(",");
			}

			if (atNumbers.length() > 0) {
				atNumbers.setLength(atNumbers.length() - 1);
			}

			request.setAtNumber(atNumbers);

			request.setProductQRCodeItemDetailsEntityList(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

			request.setInwardNumber(productQRCode.getStockTransfer().getReferenceNumber());

			request.setWareHouseCode(String.valueOf(productQRCode.getToEntity().getCode()));

			request.setProductQRCodeId(productQRCode.getId());
			request.setProductVarietyCode(
					productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getCode());
			request.setProductId(productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getId());
//			request.setQrCodeDirectoryPath(productQRCode.getProductQRCodeItemList().get(index).getQrZipFilePath());
			request.setProductQRCodeItemDetailsEntityList(
					productQRCode.getProductQRCodeItemList().get(index).getProductQRCodeItemDetailsList());

			request.setProductItemId(productQRCode.getProductQRCodeItemList().get(index).getId());

			log.info("<<--- Pdf File path :: >> " + request.getQrCodeDirectoryPath());

			log.info("downloadQRCodeZip [" + request + "]");

			InputStream inputStream = httpService.generatePDF(URL, request);

			downloadFile = new DefaultStreamedContent(inputStream, "application/pdf",
					productQRCode.getStockTransfer().getReferenceNumber() + "_"
							+ productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster().getCode()
							+ "_" + productQRCode.getProductQRCodeItemList().get(index).getProductVarietyMaster()
									.getName().replaceAll(" ", "_")
							+ ".pdf");

			log.info("downloadQrCodePdf finished successfully....");

		} catch (Exception e) {
			log.error("Exception ", e);
		}

	}

	public void selectAutoSearch() {
		if (supplierId != null) {
			getStockTransferListByValue();
		} else {
			AppUtil.addWarn("Please Enter Society Code and Name ");
		}

	}

	public List<SupplierMaster> supplierAutocomplete(String query) {
		log.info("supplierAutocomplete Autocomplete query==>" + query);
		if (query != null) {
			String supplierCode = query;
			// supplierMasterList =
			// commonDataService.findSupplierByProductWarehouseAutoComplete(loggedInWarehouseId,
			// StockTransferStatus.SUBMITTED, StockTransferType.WAREHOUSE_INWARD,
			// supplierCode);
			supplierMasterList = commonDataService.findSupplierByStatusAndTransferTypeAutoComplete(
					RetailQualityCheckStatus.QC_COMPLETE, StockTransferType.WAREHOUSE_INWARD,
					loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId(), supplierCode);

		} else {
			AppUtil.addWarn("====");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

		return supplierMasterList;
	}

	public String generatePAR() {
		log.info("<-----generatePAR method starts---->");
		String msg = "";
		try {
			log.info("generatePAR productQRCodeResponse obj---->" + productQRCodeResponse);
			if (productQRCodeResponse != null && productQRCodeResponse.getStockInwardNumber() != null) {
				/**
				 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller/
				 * 
				 * ProductQRCodeController.java
				 */
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/generatepar/"
						+ productQRCodeResponse.getStockInwardNumber();
				//
				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						msg = "PAR Generated Successfully";
						AppUtil.addInfo(msg);
					} else if (baseDTO.getStatusCode().equals(ErrorDescription.PLAN_ALREADY_EXISTS.getCode())) {
						msg = "PAR Already Generated";
						AppUtil.addWarn(msg);
					} else {
						errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
					}
					productQRCodeResponse = new ProductQRCodeResponse();
					addButtonFlag = false;
					generateButtonFlag = true;
					selectedProductQRCodeItemList = new ArrayList<>();
				} else {
					errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				}
			}
		} catch (Exception e) {
			log.error("generatePAR method exception---->", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("<-----generatePAR method ends---->");
		return LIST_PAGE_URL;
	}

	public String downloadInwardQrCode() {
		log.info("ProductQRCodeBean. downloadInwardQrCode() STARTED");
		String pdfFilePath = null;
		InputStream input = null;
		downloadInwardPdfFile = null;
		try {
			if (productQRCodeResponse == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String inwardNumber = productQRCodeResponse.getStockInwardNumber();
			if (StringUtils.isEmpty(inwardNumber)) {
				AppUtil.addError("Stock transfer id is empty");
				return null;
			}

			inwardNumber = inwardNumber.trim();
			String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/product/qr/code/downloadqrcodebyinwardid/"
					+ inwardNumber;
			BaseDTO baseDTO = httpService.get(URL);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				pdfFilePath = mapper.readValue(jsonValue, String.class);
			}

			if (StringUtils.isEmpty(pdfFilePath)) {
				AppUtil.addWarn("QR Code not yet generated for all items in this inward.");
				return null;
			}
			File files = new File(pdfFilePath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			downloadInwardPdfFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
					files.getName()));
		} catch (RestException re) {
			log.error("RestException at ProductQRCodeBean. downloadInwardQrCode()", re);
			AppUtil.addWarn(re.getMessage());
			return null;
		} catch (Exception e) {
			log.error("Exception at ProductQRCodeBean. downloadInwardQrCode() ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}

		log.info("ProductQRCodeBean. downloadInwardQrCode() COMPLETED");
		return null;
	}

}
