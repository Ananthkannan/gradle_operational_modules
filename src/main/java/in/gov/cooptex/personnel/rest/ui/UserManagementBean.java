package in.gov.cooptex.personnel.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.time.DateUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GroupDTO;
import in.gov.cooptex.core.dto.UserMasterDTO;
import in.gov.cooptex.core.enums.UserType;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.Region;
import in.gov.cooptex.core.model.RoleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
@Scope("session")
public class UserManagementBean implements Serializable {/**
 * 
 */
	private static final long serialVersionUID = 1L;

	private String serverURL = null;

	@Getter
	@Setter
	UserMasterDTO userMaster;

	@Getter
	@Setter
	private String confirmPassword;

	@Getter
	@Setter
	LazyDataModel<UserMaster> userMasterLazyList;

	@Getter
	@Setter
	private String action;


	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<GroupDTO> groupList=new ArrayList<GroupDTO>();

	@Getter
	@Setter
	List<GroupDTO> selectedGroupList=new ArrayList<GroupDTO>();

	@Getter
	@Setter
	List<EntityMaster> regionList=new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<Region> selectedRegionList=new ArrayList<Region>();

	@Getter
	@Setter
	List<RoleMaster> selectedRoleList=new ArrayList<RoleMaster>();

	@Getter
	@Setter
	List<RoleMaster> roleList=new ArrayList<RoleMaster>();
	
	@Getter
	@Setter
	String userType;


	@Autowired
	LoginBean loginBean;



	@Getter
	@Setter
	private Integer userMasterListSize;


	@Getter
	@Setter
	List<UserMaster> userMasterList=new ArrayList<UserMaster>();

	@Getter
	@Setter
	UserMaster selectedUser;

	@Setter @Getter
	UserMasterDTO selectedUserMasterDTO=new UserMasterDTO();

	@Setter @Getter
	String regionNames="";

	@Setter @Getter
	String roleMasterNames="";

	@Setter @Getter
	String groupNames="";
	
	@Getter
	@Setter
	List<String> userTypeList = new ArrayList<>();
	
	
	boolean forceLogin=false;
	
	@PostConstruct
	public void init() {
		loadValues();
		//loadAllUserList();
	}
	
	public String showList()
	{
		log.info("<---------- Loading user list page---------->");
		loadValues();
		loadAllUserList();
		return "/pages/userManagement/listUser.xhtml?faces-redirect=true";
	}
	private void loadValues() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " , e);
		}
		log.info("Server url--->"+serverURL);
	}

	public String addUser() {
		log.info("<---- Start UsermanagementBean.addUser ---->");
		try {
			userMaster = new UserMasterDTO();
			userMaster.setStatus(true);
			action = "ADD";
			getAllGroup();
			getAllRegion();
			getAllRoles();
			loadUserType();
			regionNames="";
			groupNames="";
			roleMasterNames="";
			log.info("<---- End UsermanagementBean.addUser ---->");
		}catch(Exception e)
		{
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>",e);
		}
		return "/pages/userManagement/createUser.xhtml?faces-redirect=true";

	}
	
	public void loadUserType() {
		log.info("<=UsermanagementBean :: loadUserType==>");
		try {
			for (UserType userType : UserType.values()) {
				log.info("loadUserType :: UserType ==> " + userType.name());
				userTypeList.add(userType.name());
			}

		} catch (Exception e) {
			log.error("Exception in loadUserType  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public String showEditForm() {
		log.info("<---- Start UsermanagementBean .showEditForm  ---->");
		if(selectedUser == null) {
			log.info("Selected user Emtpy");
			Util.addWarn("Please Select One User");
			return null;
		}
		action="EDIT";
		try {
			loadUserType();
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/user/get";
			baseDTO = httpService.post(url, selectedUser);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			String jsonResponse;
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			userMaster = mapper.readValue(jsonResponse, new TypeReference<UserMasterDTO>() {});
			if(userMaster != null) {
				log.info("edit userType==> "+userMaster.getUserType().name());
				userType = userMaster.getUserType().name();
			}
			getAllGroup();
			getAllRegion();
			getAllRoles();
			changeRoles();
			changeGroup();
			changeRegions();
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... " , jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured ... " , ioEx);
		}catch (Exception e) {
			log.error("Exception occured while redirecting to edit page ... " , e);
		}

		log.info("<---- End UsermanagementBean .showEditForm  ---->");
		log.info("<---- Redirecting to Edit User form  ---->");
		return "/pages/userManagement/createUser.xhtml?faces-redirect=true";

	}


	public String cancelUser() {
		log.info("<----Redirecting to user List page---->");
		userType = null;
		userMaster = new UserMasterDTO();
		selectedUser = new UserMaster();
		loadAllUserList();
		return "/pages/userManagement/listUser.xhtml?faces-redirect=true";

	}
	public String deleteUser() {
		try {
			if(selectedUser == null) {
				Util.addWarn("Please Select One User");
			}
			else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
		} catch(Exception e)
		{
			log.error("Exception occured .... " , e);
		}
		return null;
	}

	public String deleteUserConfirm() {

		try {
			log.info("Delete confirmed for user ["+selectedUser.getUsername()+"]");
			UserMaster userMaster = new UserMaster();
			userMaster.setId(selectedUser.getId());
			userMaster.setModifiedBy(loginBean.getUserDetailSession().getId());
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/user/delete";
			baseDTO = httpService.post(url, userMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1019) {
					log.info("User deleted successfully....!!!");
					errorMap.notify(baseDTO.getStatusCode());
					loadAllUserList();
					selectedUser = new UserMaster();
				} if(baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin=true;
					return forceLogin();
					
				}
				/*else {
					log.info("Error occured in Rest api ... ");
					errorMap.notify(baseDTO.getStatusCode());
				}*/
			}
		} catch(Exception e)
		{
			log.error("Exception occured while deleting user ...." , e);
		}
		return null;
	}

	public String submit() {
		log.info("<<<<<< -------- Start UsermanagementBean.submit Save or update User------ >>>>>>");
		try {
		UserMaster user=loginBean.getUserDetailSession();

		if(userMaster==null)
		{
			log.warn("<<<< ......User is empty.....>>>>");
			Util.addWarn("User must not be empty");
			return null;
		}
		if(userMaster.getUsername()==null || userMaster.getUsername().isEmpty())
		{
			log.warn("<<<< ......User name is empty.....>>>>");
			Util.addWarn("User name should not be empty");
			return null;
		}
		if(action.equals("ADD") && (userMaster.getPassword()==null || userMaster.getUsername().isEmpty()))
		{
			log.warn("<<<< ......password is empty.....>>>>");
			Util.addWarn("password should not be empty");
			return null;
		}
		if(action.equals("ADD") && (confirmPassword==null || confirmPassword.isEmpty()))
		{
			log.warn("<<<< ......confirm password is empty.....>>>>");
			Util.addWarn("confirm password should not be empty");
			return null;
		}
		if(action.equals("ADD") && !userMaster.getPassword().equals(confirmPassword))
		{
			log.warn("<<<< ......password and confirm password mismatched is empty.....>>>>");
			Util.addWarn("password should match confirm password");
			return null;
		}
		if(userMaster.getRoleMaster()==null || userMaster.getRoleMaster().size()==0)
		{
			Util.addWarn("Role is mandatory");
			return null;
		}
		log.info("userType==> "+userType);
		userMaster.setUserType(UserType.valueOf(userType));
		if(action.equals("ADD") && userMaster.getId()==null)
		{

			log.info("Saving user............");
			userMaster.setCreatedBy(user.getId());
			//userMaster.setUserType(UserType.ADMIN_USER);
			
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/user/add";
			baseDTO = httpService.post(url, userMaster);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 1010) {
					log.info(url + "<--- baseDTO.getStatusCode() == 1010 --->");
//					Util.addInfo("User Saved Successfully");
					errorMap.notify(baseDTO.getStatusCode());
					FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				}

				if (baseDTO.getStatusCode() == 1012) {
					log.info(url + "<--- baseDTO.getStatusCode() == 1012 --->");
					errorMap.notify(baseDTO.getStatusCode());
//					Util.addWarn("Duplicate user name");
					return null;
				}
				
				if (baseDTO.getStatusCode() == 1071) {
					log.info(url + "<--- baseDTO.getStatusCode() == 1071 --->");
					errorMap.notify(baseDTO.getStatusCode());
//					Util.addWarn("Password must be alphanumeric and length must be 6 to 15");
					return null;
				}
			}
		}else
			if(action.equals("EDIT") && userMaster.getId()!=null)
			{
				log.info("Updating user............");
				userMaster.setModifiedBy(user.getId());
				BaseDTO baseDTO=new BaseDTO();
				String url = serverURL + "/user/update";
				baseDTO = httpService.post(url, userMaster);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 1017) {
						log.info(url + "<--- baseDTO.getStatusCode() == 0 --->");
						errorMap.notify(baseDTO.getStatusCode());
//						Util.addInfo("User updated Successfully");
						FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
					}if(baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
						log.info("Same user update invalidating session-------->");
						forceLogin=true;
						RequestContext context = RequestContext.getCurrentInstance();
						context.execute("PF('forceLogoutDialog').show();");
						return null;
						
					}
					if (baseDTO.getStatusCode() == 1012) {
						log.info(url + "<--- baseDTO.getStatusCode() == 1012 --->");
//						Util.addError("Duplicate user name");
						errorMap.notify(baseDTO.getStatusCode());
						return null;
					}
					if (baseDTO.getStatusCode() == 10009) {
						log.info(url + "<--- baseDTO.getStatusCode() == 10009 --->");
						errorMap.notify(baseDTO.getStatusCode());
//						Util.addError("Data alreday updated");
						return null;
					}
				}
			}
		selectedUser = new UserMaster();
		loadAllUserList();
		userType = null;
		}catch(Exception e)
		{
			log.error("Exception occured while savin user ..... " , e);
		}
		log.info("<<<<<< -------- Start UsermanagementBean.submit Save or update User ------ >>>>>>");
		return "/pages/userManagement/listUser.xhtml?faces-redirect=true";
	}

	public String forceLogin() {
		log.info("force login called--->"+forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	public void  getAllGroup(){
		log.info("<<<<< ------- Start UsermanagementBean .get All Group Called -------- >>>>>>>>>");
		try {
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/group/getallactivegroups";

			baseDTO = httpService.get(url);
			if(baseDTO!=null)
			{
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				groupList = mapper.readValue(jsonResponse, new TypeReference<List<GroupDTO>>() {});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json Processing Exception occured While converting .... " , jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... " , ioEx);
		} catch (Exception e)
		{
			log.error("Exception occured while fetching group.... " , e);
		}
		log.info("<<<<< -------End  UsermanagementBean.get All Group Called -------- >>>>>>>>>");

	}

	public void  getAllRegion(){
		log.info("<<<< ------ Start Usermanagement.getAllRegion ---- --- >>>>>>");
		try {
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/entitymaster/region/getall";
			baseDTO = httpService.get(url);
			if(baseDTO!=null)
			{
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				regionList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {});
			}
			log.info("user amster:::"+regionList.size());
		} catch (JsonProcessingException e) {
			log.error("Json processing exception occured while converting ... " , e);
		} catch (IOException e) {
			log.error("IO Exception occured ... "+e);
		}catch(Exception e)
		{
			log.error("Exception occured while fetching Regions ... " , e);
		}
		log.info("<<<< ------ End Usermanagement.getAllRegion ---- --- >>>>>>");

	}
	public void  getAllRoles(){
		log.info("<<<<<<< ---------- Start Usermanagementbean.getAllRoles ---------- >>>>>>>");
		try {
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/role/getallactive";
			baseDTO = httpService.get(url);
			if(baseDTO!=null)
			{
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				roleList = mapper.readValue(jsonResponse, new TypeReference<List<RoleMaster>>() {});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... " , jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... " , ioEx);
		} catch (Exception e)
		{
			log.error("Exception occured .... " , e);
		}

	}
	public void loadAllUserList() {
		log.info("<<<< ----------Start UsermangementBean . loadAllUserList ------- >>>>");
		try {
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/user/getall";
			baseDTO = httpService.get(url);
			selectedUser=new UserMaster();
			if(baseDTO!=null)
			{
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				userMasterList = mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() {});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... " , jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... " , ioEx);
		} catch (Exception e)
		{
			log.error("Exception occured .... ", e);
		}

		if(userMasterList!=null)
			userMasterListSize=userMasterList.size();
		else
			userMasterListSize=0;
		log.info("<<<< ----------End UsermangementBean . loadAllUserList ------- >>>>");
	}
	public String viewUser() {
		log.info("<----Redirecting to user view page---->");
		if(selectedUser == null) {
			Util.addWarn("Please Select One User");
			return null;
		}
		getSelectedUserDetails();
		return "/pages/userManagement/viewUser.xhtml?faces-redirect=true";

	}

	public String getSelectedUserDetails() {
		log.info("<<<<<< ------ Start Usermanagementbean.getSelectedUserDetails -------->>>>>>>>>>>");
		try {
			regionNames="";
			roleMasterNames="";
			groupNames="";
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/user/get";
			baseDTO = httpService.post(url, selectedUser);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			if(baseDTO!=null)
			{
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				selectedUserMasterDTO = mapper.readValue(jsonResponse, new TypeReference<UserMasterDTO>() {});
			}

			log.info("view usertype==> "+selectedUserMasterDTO.getUserType().name());
			userType = selectedUserMasterDTO.getUserType().name();

			List<EntityMaster> regionList=selectedUserMasterDTO.getRegion();
			List<RoleMaster> roleMasterList=selectedUserMasterDTO.getRoleMaster();
			List<GroupDTO> groupList=selectedUserMasterDTO.getGroupList();

			if(selectedUserMasterDTO.getUsername()==null||selectedUserMasterDTO.getUsername().isEmpty()){
				selectedUserMasterDTO.setUsername("UserName Not Found");
			}
			if(selectedUserMasterDTO.getPassword()==null||selectedUserMasterDTO.getPassword().isEmpty()){
				selectedUserMasterDTO.setPassword("Password Not Found");
			}

			if(regionList!=null){
				for(EntityMaster regionName:regionList ){
					if (regionNames != "") {
						regionNames = regionNames + ",";
					}
					regionNames = regionNames + regionName.getName();
				}
			}
			if(roleMasterList!=null){
				for(RoleMaster roleMasterName:roleMasterList ){
					if (roleMasterNames != "") {
						roleMasterNames = roleMasterNames + ",";
					}
					roleMasterNames = roleMasterNames + roleMasterName.getRoleName();
				}
			}
			if(groupList!=null){
				for(GroupDTO groupName:groupList ){
					if (groupNames != "") {
						groupNames = groupNames + ",";
					}
					groupNames = groupNames + groupName.getName();
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting ... " , jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occurred ..... " , ioEx);
		} catch (Exception e)
		{
			log.error("Exception occured while fetching selected user details .... " , e);
		}
		log.info("<<<<<< ------ End Usermanagementbean.getSelectedUserDetails -------->>>>>>>>>>>");
		return "/pages/userManagement/viewUser.xhtml?faces-redirect=true";
	}

	public String clear() {
		log.info("<---- Start UsermanagementBean . clear ----->");
		loadAllUserList();
		log.info("<---- End UsermanagementBean . clear ----->");
		
		return "/pages/userManagement/listUser.xhtml?faces-redirect=true";

	}

	
	
	public void changeRoles() {
		roleMasterNames = "";
		if(userMaster.getRoleMaster()!=null)
		for (RoleMaster region : userMaster.getRoleMaster()) {
			if (roleMasterNames != "") {
				roleMasterNames = roleMasterNames + ",";
			}
			roleMasterNames = roleMasterNames + region.getRoleName();
		}
	}
	public void changeRegions() {
		regionNames = "";
		if(userMaster.getRegion()!=null)
		for (EntityMaster region : userMaster.getRegion()) {
			if (regionNames != "") {
				regionNames = regionNames + ",";
			}
			regionNames = regionNames + region.getName();
		}
	}
	public void changeGroup() {
		groupNames = "";
		if(userMaster.getGroupList()!=null)
		for (GroupDTO group : userMaster.getGroupList()) {
			if (groupNames != "") {
				groupNames = groupNames + ",";
			}
			groupNames = groupNames + group.getName();
		}
	}

	public boolean  filterDate(Object value, Object filter, Locale locale) {
		if( filter == null ) {
            return true;
        }

        if( value == null ) {
            return false;
        }
      return  DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);

	}
	
	public void validatePassword(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validatePassword method");
		
		if(!Validate.validateAlphaNumericPassword(value)) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage("Password must contain both number and alphabets and the length must be between 6 and 15");
			context.addMessage(comp.getClientId(context), message);
		}
	}
}
