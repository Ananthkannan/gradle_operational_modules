package in.gov.cooptex.masters.ui.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.model.TemplateDetails;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("templateDetailsBean")
public class TemplateDetailsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_TEMPLATEDETAILS_MASTER_PAGE = "/pages/masters/listTemplateDetails.xhtml?faces-redirect=true;";
	private final String CREATE_TEMPLATEDETAILS_MASTER_PAGE = "/pages/masters/createTemplateDetails.xhtml?faces-redirect=true;";
	private final String VIEW_TEMPLATEDETAILS_MASTER_PAGE = "/pages/masters/viewTemplateDetails.xhtml?faces-redirect=true;";

	Map<String, String> templateCodesMap = null;
	@Getter
	@Setter
	int totalRecords = 0;

	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;
	ObjectMapper mapper;

	String jsonResponse;
	@Getter
	@Setter
	Boolean addButtonFlag = true;;

	@Getter
	@Setter
	Boolean editButtonFlag = true;;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	private TemplateCode templateCode;
	@Getter
	@Setter
	LazyDataModel<TemplateDetails> lazyTemplateDetailsList;
	@Getter
	@Setter
	TemplateDetails selectedTemplateDetails = new TemplateDetails();

	@Getter
	@Setter
	private List<TemplateCode> templateCodeList;

	@Getter
	@Setter
	List<TemplateDetails> templateDetailsList = new ArrayList<TemplateDetails>();

	@Getter
	@Setter
	private int templateDetailsListSize;

	@Getter
	@Setter
	TemplateDetails templateDetails;

	boolean forceLogin = false;

	@PostConstruct
	public void init() {
		log.info(" TemplateDetailsBean init --->");
		getAllTemplateCode();
		loadValues();
		addTemplateDetails();
		getAllTemplateDetailsList();
		loadLazytemplatedetailsList();

	}

	public List<TemplateCode> getAllTemplateCode() {
		templateCodesMap = new HashMap<>();
		TemplateCode[] listTemplateCode;
		BaseDTO baseDTO = new BaseDTO();
		listTemplateCode = TemplateCode.values();

		for (TemplateCode listTemplateCodes : listTemplateCode) {
			templateCodesMap.put(listTemplateCodes.toString(), listTemplateCodes.toString());
			templateCodeList = new ArrayList(templateCodesMap.values());
			baseDTO.setResponseContents(templateCodeList);
			try {
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					templateCodeList = mapper.readValue(jsonResponse, new TypeReference<List<TemplateCode>>() {
					});
				}
			} catch (Exception e) {
				baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				log.error("found exception in getall templateCode", e);
			}

		}

		return templateCodeList;

	}

	public String cancel() {
		log.info("<----Redirecting to user List page---->");
		templateDetails = new TemplateDetails();
		return LIST_TEMPLATEDETAILS_MASTER_PAGE;

	}

	public String clear() {
		addButtonFlag = true;
		log.info("<---- Start TapalDeliveryTypeBean . clear ----->");
		getAllTemplateDetailsList();
		templateDetails = new TemplateDetails();
		log.info("<---- End TapalDeliveryTypeBean . clear ----->");

		return LIST_TEMPLATEDETAILS_MASTER_PAGE;

	}

	private void loadValues() {
		try {
			serverURL = appPreference.getPortalServerURL();
			templateDetails = new TemplateDetails();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> ", e);
		}
		log.info("Server url--->" + serverURL);
	}

	public String backPage() {
		showTemplateDetailsList();
		return LIST_TEMPLATEDETAILS_MASTER_PAGE;
	}

	public String showTemplateDetailsList() {
		log.info("<---------- Loading TemplateDetsilsBean showList list page---------->");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		loadValues();
		getAllTemplateDetailsList();
		loadLazytemplatedetailsList();
		return LIST_TEMPLATEDETAILS_MASTER_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		selectedTemplateDetails = ((TemplateDetails) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Starts TemplateDetails.onRowSelect ========>" + event);

		log.info("<===Ends TemplateDetails.onRowSelect ========>");
	}

	public String addTemplateDetails() {
		log.info("<---- Start TemplateDetailsBean.addTemplateDetails ---->");
		try {
			action = "ADD";
			templateDetails = new TemplateDetails();
			templateDetails.setActiveStatus(true);

			log.info("<---- End TemplateDetails.addtemplateDetails ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_TEMPLATEDETAILS_MASTER_PAGE;

	}

	public String deleteTemplateDetails() {
		try {
			action = "Delete";
			if (templateDetails == null) {
				Util.addWarn("Please Select One Templeate Details");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTemplateDetailsDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String editTemplateDetails() throws IOException {
		log.info("<<<<< -------Start TemplateDetails called ------->>>>>> ");
		action = "EDIT";

		if (templateDetails == null) {
			Util.addWarn("Please select one TemplateDetails");
			return null;
		}

		String url = serverURL + appPreference.getOperationApiUrl() + "/templateDetail/get/"
				+ selectedTemplateDetails.getId();
		BaseDTO response = httpService.get(url);
		if (response != null && response.getStatusCode() == 0) {
			ObjectMapper mapper = new ObjectMapper();
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			templateDetails = mapper.readValue(jsonResponse, new TypeReference<TemplateDetails>() {
			});
		}
		if (templateDetails.getActiveStatus().equals(true)) {
			templateDetails.setActiveStatus(templateDetails.getActiveStatus());
		} else {
			templateDetails.setActiveStatus(templateDetails.getActiveStatus());
		}
		log.info("<<<<< -------End TemplateDetails called ------->>>>>> ");

		return CREATE_TEMPLATEDETAILS_MASTER_PAGE;

	}

	public String viewTemplateDetails() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (templateDetails == null) {
			Util.addWarn("Please Select One TemplateDetails");
			return null;
		}
		return VIEW_TEMPLATEDETAILS_MASTER_PAGE;

	}

	public String deleteTemplateDetailsConfirm() {

		try {
			log.info("Delete confirmed for Template Details [" + templateDetails.getTemplateType() + "]");
			BaseDTO baseDTO = new BaseDTO();

			String url = serverURL + appPreference.getOperationApiUrl() + "/templateDetail/delete/"
					+ selectedTemplateDetails.getId();
			baseDTO = httpService.delete(url);
			showTemplateDetailsList();

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(MastersErrorCode.TEMPLATEDETAILS_DELETE_SUCCESS);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTemplateDetailsDelete').hide();");
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return LIST_TEMPLATEDETAILS_MASTER_PAGE;
	}

	public void getTemplateDetailsviews(TemplateDetails templateDetails) {
		log.info("<<<< ----------Start templateDetailsBean . getTemplateDetailsView() ------- >>>>");

		try {
			BaseDTO baseDTO = new BaseDTO();

			String url = serverURL + appPreference.getOperationApiUrl() + "/templateDetail/gettemplateDetails";
			baseDTO = httpService.post(url, templateDetails);
			templateDetails = new TemplateDetails();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				templateDetails = mapper.readValue(jsonResponse, new TypeReference<TemplateDetails>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (templateDetails != null)
			log.info("TemplateDetailsBean getTemplateDetailsView" + templateDetails);
		else
			log.info("TemplateDetailsBean getTemplateDetailsView" + templateDetails);

		log.info("<<<< ----------End TemplateDetailsBean . getTemplateDetailsView ------- >>>>");
	}

	private void getAllTemplateDetailsList() {
		log.info("<<<< ----------Start TemplateDetails-getAllTemplateDetailsList() ------- >>>>");
		try {
			templateDetailsList = new ArrayList<TemplateDetails>();
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/templateDetail/getActiveTemplateDetails";
			baseDTO = httpService.get(url);
			log.info("getvalue" + baseDTO);
			templateDetails = new TemplateDetails();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				templateDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<TemplateDetails>>() {
				});
				// this.setRowCount(baseDTO.getTotalRecords());
				totalRecords = baseDTO.getTotalRecords();
				log.info("===templateDetailsList.size() first======" + templateDetailsList.size());

			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (templateDetailsList != null) {
			templateDetailsListSize = templateDetailsList.size();
			log.info("===templateDetailsList.size()  second======" + templateDetailsList.size());
		} else {
			templateDetailsListSize = 0;
		}
		log.info("<<<< ----------End TemplateDetails . getAllTemplateDetailsList ------- >>>>");
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String submit() {
		log.info("<=======Starts  TemplateDetailsBean.submit ======>" + templateDetails);
		if (templateDetails.getTemplateCode() == null) {
			errorMap.notify(ErrorDescription.TEMPLATEDETAILS_TYPE_EMPTY.getErrorCode());
			return null;
		}
		if (templateDetails.getSubject() == null || templateDetails.getSubject().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.TEMPLATEDETAILS_SUBJECT_EMPTY.getErrorCode());
			return null;
		}
		if (templateDetails.getDescription() == null || templateDetails.getDescription().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.TEMPLATEDETAILS_DESCRIPTION_EMPTY.getErrorCode());
			return null;
		}
		if (templateDetails.getLocalDescription() == null || templateDetails.getLocalDescription().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.TEMPLATEDETAILS_LDESCRIPTION_EMPTY.getErrorCode());
			return null;
		}

		templateDetails.setCreatedBy(loginBean.getUserDetailSession());
		String url = serverURL + appPreference.getOperationApiUrl() + "/templateDetail/create";
		log.info("<===Controller url====>" + url);
		BaseDTO response = httpService.post(url, templateDetails);
		if (response != null && response.getStatusCode() == 0) {
			log.info("templateDetail saved successfully..........");
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.TEMPLATEDETAILS_INSERTED_SUCCESSFULLY.getCode());
			else
				errorMap.notify(MastersErrorCode.TEMPLATEDETAILS_UPDATE_SUCCESS);
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
			return null;
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<=======Ends TemplateDetailsBean.submit ======>");
		return LIST_TEMPLATEDETAILS_MASTER_PAGE;
	}

	public void loadLazytemplatedetailsList() {
		log.info("<===== Starts TemplateDetailsBean.loadLazypetitionTypeList ======>");

		lazyTemplateDetailsList = new LazyDataModel<TemplateDetails>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<TemplateDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + appPreference.getOperationApiUrl()
							+ "/templateDetail/getalltemplatelistlazy";
					log.info("===url templateDetail/getalltemplatelistlazy ====" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("===url templateDetail/getalltemplatelistlazy  responseafter httpService.post ===="
							+ response);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						// mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						templateDetailsList = mapper.readValue(jsonResponse,
								new TypeReference<List<TemplateDetails>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("TemplateDetailsBean size---------------->" + lazyTemplateDetailsList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return templateDetailsList;
			}

			@Override
			public Object getRowKey(TemplateDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public TemplateDetails getRowData(String rowKey) {
				for (TemplateDetails templateDetails : templateDetailsList) {
					if (templateDetails.getId().equals(Long.valueOf(rowKey))) {
						selectedTemplateDetails = templateDetails;
						return templateDetails;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  TemplateDetailsBean.loadLazySalesTypeList  ======>");
	}

}
