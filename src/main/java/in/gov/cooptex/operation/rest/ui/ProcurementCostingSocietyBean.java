package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import in.gov.cooptex.operation.dto.ProcurementCostingRequestDto;
import in.gov.cooptex.operation.dto.ProcurementCostingResponseDto;
import in.gov.cooptex.operation.model.ProcurementCosting;
import in.gov.cooptex.operation.model.ProcurementCostingNote;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("procurementCostingSocietyBean")
@Scope("session")
@Data
public class ProcurementCostingSocietyBean implements Serializable {
	/**
		 * 
		 */
	private static final long serialVersionUID = 8295605454734837955L;

	private final String CREATE_PROCUREMENT_COSTING_SOCIETY_PAGE = "/pages/weavers/societyEnrollment/createProcurementCostingSociety.xhtml?faces-redirect=true;";
	private final String PROCUREMENT_COSTING_SOCIETY_LIST_URL = "/pages/weavers/societyEnrollment/listProcurementCostingSociety.xhtml?faces-redirect=true;";
	private final String PROCUREMENT_COSTING_SOCIETY_VIEW_URL = "/pages/operation/procurement/viewProcurementCostingSociety.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	@Getter
	@Setter
	String jsonResponse;

	@Getter
	@Setter
	LazyDataModel<ProcurementCosting> procurementCostingLazyList;

	@Getter
	@Setter
	List<ProcurementCosting> procurementCostingList = new ArrayList<ProcurementCosting>();

	@Getter
	@Setter
	private ProcurementCosting procurementCosting = new ProcurementCosting();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList = new ArrayList<ProductGroupMaster>();

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList = new ArrayList<ProductDesignMaster>();

	@Getter
	@Setter
	ProcurementCostingResponseDto selectedProcurementCostingResponseDto;

	@Getter
	@Setter
	ProcurementCostingResponseDto procurementCostingResponseDto;

	@Getter
	@Setter
	LazyDataModel<ProcurementCostingResponseDto> lazyProcurementCostingList;

	@Getter
	@Setter
	List<ProcurementCostingResponseDto> procurementCostingResponseDtoList = new ArrayList<ProcurementCostingResponseDto>();

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Long productGroupCode;

	@Getter
	@Setter
	private ProductVarietyMaster productVarietyCode;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	Long productDesignCode;

	@Getter
	@Setter
	ProductVarietyMaster productVariety;

	@Getter
	@Setter
	ProductCategory productCategoryCode;

	@Getter
	@Setter
	Long forwardTo;

	@Getter
	@Setter
	ProcurementCostingRequestDto procurementCostingRequestDto = new ProcurementCostingRequestDto();

	@Getter
	@Setter
	ProcurementCostingNote procurementCostingNote;

	@Getter
	@Setter
	private Double warpWastagePercetnage = 0.0, weftWastagePercentage = 0.0, warpYarnRate = 0.0, weftYarnRate = 0.0,
			numberOfUnits = 0.0, weightPerUnit = 0.0, warpYarnRequired = 0.0, weftYarnRequired = 0.0,
			costWarpYarn = 0.0, costWeftYarn = 0.0, warpWastageAmount = 0.0, weftWastageAmount = 0.0,
			weavingWages, profitPercetage , totalRate = 0.0, totalRateTemp = 0.0, costWarpYarnTemp = 0.0,
			costWeftYarnTemp = 0.0, ratePerUnit = 0.0, preparatoryCharges, sticker, knotting,
			totalPurchasePrice = 0.0, box, polythin_cover, otherChargesAmount;

	@Getter
	@Setter
	String otherChargesName;

	@Getter
	@Setter
	private Date periodFrom;

	@Getter
	@Setter
	private Date periodTo;

	@Getter
	@Setter
	private Long version;

	@Getter
	@Setter
	private List<UserMaster> userMasters = new ArrayList<UserMaster>();

	@Getter
	@Setter
	private Double warpWastageAmountTemp;

	@Getter
	@Setter
	private Double warpWastageTemp;

	@Getter
	@Setter
	private Double ratePerUnitTemp;

	@Getter
	@Setter
	private Double weftWastageAmountTemp;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	boolean diseabledForwardTo;

	@Getter
	@Setter
	boolean renderforwardfor;

	@Getter
	@Setter
	boolean diseabledForwardFor;

	@Getter
	@Setter
	boolean approveRender = true;

	@Getter
	@Setter
	boolean rejectRender = true;

	@Getter
	@Setter
	boolean diseabledCatgory = false;

	@Getter
	@Setter
	boolean diseabledGroup = false;

	@Getter
	@Setter
	boolean diseabledVariety = false;

	@Getter
	@Setter
	boolean diseabledDesign = false;

	@Getter
	@Setter
	boolean forwardForRender = true;

	@Getter
	@Setter
	boolean forwardToRender = true;

	@Getter
	@Setter
	boolean renderedForwardForAndTo = true;

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingDtoList = new ArrayList<>();

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingResponseDtoListCharges = new ArrayList<>();

	@Getter
	@Setter
	List<ProcurementCostingRequestDto> procurementCostingResponseDtoListremove = new ArrayList<>();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<ProcurementCostingResponseDto> viewLogResponseList;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@PostConstruct
	public void init() {
		procurementCostingRequestDto = new ProcurementCostingRequestDto();
		productGroupMasterList = null;
		productCategoryList = null;
		productVarietyMasterList = null;
		productDesignMasterList = null;
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		productCategoryCode = null;
		productVarietyCode = null;
		procurementCosting = new ProcurementCosting();
		costWarpYarn = 0.0;
		costWeftYarn = 0.0;
		warpWastageAmount = 0.0;
		weftWastageAmount = 0.0;
		//weavingWages;
		//profitPercetage = 0.0;
		totalRate = 0.0;
		totalRateTemp = 0.0;
		costWarpYarnTemp = 0.0;
		costWeftYarnTemp = 0.0;
		ratePerUnit = 0.0;
		//preparatoryCharges = 0.0;
		//sticker = 0.0;
		//knotting = 0.0;
		totalPurchasePrice = 0.0;
		procurementCostingDtoList = new ArrayList<>();
		procurementCostingResponseDtoListremove = new ArrayList<>();
		procurementCostingResponseDto = new ProcurementCostingResponseDto();

	}

	public String showProcurementCostingSocietyListPage() {

		log.info("<==== Starts ProcurementCostingSocietyBean.showProcurementCostingListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		diseabledCatgory = false;
		diseabledGroup = false;
		diseabledVariety = false;
		diseabledDesign = false;
		productCategoryList = commonDataService.loadProductCategories();
		loadLazyProcurementCostingSocietyList();
		log.info("<==== Ends ProcurementCostingSocietyBean.showProcurementCostingListPage =====>");
		return PROCUREMENT_COSTING_SOCIETY_LIST_URL;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ProcurementCostingBean.onRowSelect ========>" + event);
		selectedProcurementCostingResponseDto = ((ProcurementCostingResponseDto) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends ProcurementCostingBean.onRowSelect ========>");
	}

	public String addProcurementCostingPage() {

		log.info("<==== Starts ProcurementCostingBean.addProcurementCostingListPage =====>");
		mapper = new ObjectMapper();
		init();
		procurementCostingRequestDto = new ProcurementCostingRequestDto();
		procurementCostingResponseDto = new ProcurementCostingResponseDto();
		productCategoryList = commonDataService.loadProductCategories();
		log.info("<==== Ends ProcurementCostingBean.showProcurementCostingListPage =====>");
		return CREATE_PROCUREMENT_COSTING_SOCIETY_PAGE;

	}

	public void loadProductVarietyList() {

		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList =====>");
		try {
			String requestPath = SERVER_URL + "/productvariety/getproductvarietybygroupid/"
					+ procurementCostingRequestDto.getProductGroupCode();
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productVarietyMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductVarietyMaster>>() {
				});
				log.info("VariteyList with Size of : " + productVarietyMasterList.size());

			}
		} catch (Exception e) {
			log.error("Error Product Group List --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList  =====>");

	}

	public void loadProductDesignList() {
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList =====>");
		try {
			String url = SERVER_URL + "/productDesign/getdesign/" + productVarietyCode.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignMaster>>() {
						});
			}
			log.info("Product Design Master List Size" + productDesignMasterList.size());

		} catch (Exception e) {
			log.error("Error Product Design List --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList  =====>");
	}

	public void loadProductGroupList() {
		log.info("<==== Starts ProcurementCostingBean.loadProductGroupList =====>");
		try {

			String requestPath = SERVER_URL + "/group/getproductgroupbycategoryid/" + productCategoryCode.getId();
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productGroupMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductGroupMaster>>() {
				});
				log.info("GroupList with Size of : " + productGroupMasterList.size());

			}
		} catch (Exception e) {
			log.error("Error Product Group List --==>", e);
		}
		log.info(
				"<userMasters = commonDataService.loadForwardToUsers();==== Starts ProcurementCostingBean.loadProductGroupList =====>");

	}

	public String showProcurementCostingSocietyListPageAction() {
		log.info("<===== Starts ProcurementCostingBean.showProcurementCostingSocietyListPageAction ===========>"
				+ action);

		try {
			if(selectedProcurementCostingResponseDto==null) {
				log.info("please select one record");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit ProcurementCosting called..");

				String url = SERVER_URL + "/procurementcostsociety/getbyid/"
						+ selectedProcurementCostingResponseDto.getProcurementId();
				BaseDTO response = httpService.get(url);

				if (response != null && response.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					procurementCostingRequestDto = new ProcurementCostingRequestDto();
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					procurementCostingResponseDto = mapper.readValue(jsonResponse,
							new TypeReference<ProcurementCostingResponseDto>() {
							});

					callOtherChargesDetails(selectedProcurementCostingResponseDto.getProcurementId());

					diseabledCatgory = true;
					diseabledGroup = true;
					diseabledVariety = true;
					diseabledDesign = true;
					setProcurementCosting(procurementCostingResponseDto.getProcurementCosting());
					setProductVarietyCode(procurementCostingResponseDto.getProductVarietyMaster());
					setProductCategoryCode(procurementCostingResponseDto.getProductCategory());
					procurementCostingRequestDto
							.setProductDesignCode(procurementCostingResponseDto.getProductDesignMaster().getId());

					procurementCostingRequestDto
							.setProductGroupCode(procurementCostingResponseDto.getProductGroupMaster().getId());
					setCostWeftYarn(procurementCostingResponseDto.getProcurementCosting().getCostWeftYarn());
					setWarpWastageAmount(procurementCostingResponseDto.getProcurementCosting().getWarpWastageAmount());
					setWeftWastageAmount(procurementCostingResponseDto.getProcurementCosting().getWeftWastageAmount());
					setWeavingWages(procurementCostingResponseDto.getProcurementCosting().getWeavingWages());
					setProfitPercetage(procurementCostingResponseDto.getProcurementCosting().getProfitPercetage());
					setTotalRate(procurementCostingResponseDto.getProcurementCosting().getTotalRate());
					setRatePerUnit(procurementCostingResponseDto.getProcurementCosting().getRatePerUnit());
					setPreparatoryCharges(
							procurementCostingResponseDto.getProcurementCosting().getPreparatoryCharges());
					setSticker(procurementCostingResponseDto.getProcurementCosting().getSticker());
					setKnotting(procurementCostingResponseDto.getProcurementCosting().getKnotting());
					setTotalPurchasePrice(
							procurementCostingResponseDto.getProcurementCosting().getTotalPurchasePrice());
					loadAllSelectOneMenuBox();
					log.info(
							"<===== Ends ProcurementCostingBean.showProcurementCostingListPageAction Edit  ===========>");
					return CREATE_PROCUREMENT_COSTING_SOCIETY_PAGE;

				} else {
					errorMap.notify(ErrorDescription.PROCUREMENT_CAN_NOT_EDIT.getCode());
				}

			} else {
				productVarietyCode = new ProductVarietyMaster();
				productDesignMaster = new ProductDesignMaster();
				log.info("view ProcurementCosting called..");
				String viewurl = SERVER_URL + "/procurementcostsociety/getbyid/"
						+ selectedProcurementCostingResponseDto.getProcurementId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					procurementCostingResponseDto = mapper.readValue(jsonResponse,
							new TypeReference<ProcurementCostingResponseDto>() {
							});
					callOtherChargesDetails(selectedProcurementCostingResponseDto.getProcurementId());

					return PROCUREMENT_COSTING_SOCIETY_VIEW_URL;
				}

			}

		} catch (Exception e) {
			log.error("<========  Exception occured in showProcurementCostingListPageAction ..<=====   ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends ProcurementCostingBean.showProcurementCostingListPageAction ===========>" + action);
		return null;
	}

	public ProcurementCostingResponseDto callOtherChargesDetails(Long procurementId) {
		log.info("<===== Start ProcurementCostingSocietyBean.callOtherChargesDetails ===========>");
		String viewurl = SERVER_URL + "/procurementcostsociety/getchargesbycostingid/"
				+ selectedProcurementCostingResponseDto.getProcurementId();

		try {
			BaseDTO viewresponse = httpService.get(viewurl);
			if (viewresponse != null && viewresponse.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String employeeDatajsonResponses = mapper.writeValueAsString(viewresponse.getResponseContents());

				procurementCostingDtoList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<ProcurementCostingRequestDto>>() {
						});
				if(procurementCostingDtoList != null && !procurementCostingDtoList.isEmpty()) {
					log.info("procurementCostingDtoList size==> "+procurementCostingDtoList.size());
					procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
				}else {
					log.error("procurementCostingDtoList is null");
				}
			}
			log.info("<===== End ProcurementCostingSocietyBean.callOtherChargesDetails ===========>");
		} catch (Exception e) {
			log.error("<===== === Exception occured in callOtherChargesDetails ...<========= ", e);

		}
		return null;
	}

	public void loadAllSelectOneMenuBox() {
		log.info("<====== Inside loadAllSelectOneMenuBox For Edit Options  ===>");
		productCategoryList = commonDataService.loadProductCategories();
		loadProductGroupList();
		loadProductVarietyList();
		loadProductDesignList();
		log.error("<========  Exit loadAllSelectOneMenuBox ..<=====   ");
	}

	public void loadLazyProcurementCostingSocietyList() {

		try {
			log.info("<===== Starts ProcurementCostingBean.loadLazyProcurementCostingSocietyList ======>");
			lazyProcurementCostingList = new LazyDataModel<ProcurementCostingResponseDto>() {
				private static final long serialVersionUID = 1L;

				@Override
				public List<ProcurementCostingResponseDto> load(int first, int pageSize, String sortField,
						SortOrder sortOrder, Map<String, Object> filters) {
					try {
						PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
								sortOrder.toString(), filters);
						log.info("Pagination request :::" + paginationRequest);
						String url = SERVER_URL + "/procurementcostsociety/getprocurementcostingsociety";
						BaseDTO response = httpService.post(url, paginationRequest);
						if (response != null && response.getStatusCode() == 0) {
							jsonResponse = mapper.writeValueAsString(response.getResponseContents());
							procurementCostingResponseDtoList = mapper.readValue(jsonResponse,
									new TypeReference<List<ProcurementCostingResponseDto>>() {
									});
							this.setRowCount(response.getTotalRecords());
							totalRecords = response.getTotalRecords();
							log.info("Totalrecords======="+totalRecords);
						} else {
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}
					} catch (Exception e) {
						log.error("Exception occured in loadLazyProcurementCostingSocietyList ...", e);
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					log.info("Ends lazy load....");
					return procurementCostingResponseDtoList;
				}

				@Override
				public Object getRowKey(ProcurementCostingResponseDto res) {
					return res != null ? res.getProcurementId() : null;
				}

				@Override
				public ProcurementCostingResponseDto getRowData(String rowKey) {
					try {
						for (ProcurementCostingResponseDto glAccountCategory : procurementCostingResponseDtoList) {
							if (glAccountCategory.getProcurementId().equals(Long.valueOf(rowKey))) {
								selectedProcurementCostingResponseDto = glAccountCategory;
								return glAccountCategory;
							}
						}
					} catch (Exception e) {
						log.error("Exception occured in getRowData ...", e);
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					return null;
				}

			};
		} catch (Exception e) {
			log.info("<===== Ends ProcurementCostingBean.loadLazyProcurementCostingSocietyList ======>" , e);
		}
		log.info("<===== Ends ProcurementCostingBean.loadLazyProcurementCostingSocietyList ======>");
	}

	public String saveProcurementCostingSociety() {
		log.info("<===== Starts ProcurementCostingBean.saveProcurementCosting ======>" + procurementCostingRequestDto);
		try {
			if (procurementCostingDtoList != null && !procurementCostingDtoList.isEmpty()) {
				procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
			}else {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.OTHER_CHARGES_NAME_AND_AMOUNT_EMPTY).getCode());
				return null;
			}
			log.info("rate per unit==> "+procurementCosting.getRatePerUnit());
			procurementCostingRequestDto.setProcurementCosting(procurementCosting);
			procurementCostingRequestDto.setProudctVarietyMaster(productVarietyCode);
			procurementCostingRequestDto.setUserId(loginBean.getUserDetailSession().getId());
			String urlfortotalpriceCheck = SERVER_URL + "/procurementcostsociety/gettotalprice";

			BaseDTO response = httpService.post(urlfortotalpriceCheck, procurementCostingRequestDto);
			if (response != null && response.getStatusCode() == 0) {
				log.info(
						"<===== Starts ProcurementCostingSocietyBean.saveProcurementCosting FOR ProcurementCostingSociety======>");
				String url = SERVER_URL + "/procurementcostsociety/savecostingsociety";
				log.info("saveProcurementCostingSociety ::  url==> "+url);
				BaseDTO response1 = httpService.post(url, procurementCostingRequestDto);
				if (response1 != null && response1.getStatusCode() == 0) {
					log.info(
							"<===== Inside ProcurementCostingSocietyBean.saveProcurementCostingSociety Reponse Clause  ======>");
					if("CREATE".equalsIgnoreCase(action)) {
						errorMap.notify(ErrorDescription.PROCUREMENT_SAVED_SUCCESS.getCode());
					}else if("EDIT".equalsIgnoreCase(action)) {
						//errorMap.notify(ErrorDescription.getError(PersonnalErrorCode.PROCUREMENT_UPDATE_SUCCESS).getErrorCode());
						selectedProcurementCostingResponseDto = null;
					}
				} else {
					//AppUtil.addError("Internal Error");
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.PROCUREMENT_COSTING_NOT_EMPTY).getCode());	
					return null;
				}
				return PROCUREMENT_COSTING_SOCIETY_LIST_URL;
			} else {
				//AppUtil.addError("Total Purchase Price already Stored");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.TOTAL_PURCHASE_PRICE_ALREDY_STORED).getCode());
				return null;
			}

		} catch (Exception e) {
			log.error("<===== Exception Occured in ProcurementCostingBean.saveProcurementCosting ======>" , e);

		}
		log.info("<===== Ends ProcurementCostingBean.saveProcurementCosting ======>");
		return null;

	}

	public void calculateFormula() {

		try {

			log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

			costWarpYarn = 0.0;
			costWeftYarn = 0.0;
			warpWastageAmount = 0.0;
			weftWastageAmount = 0.0;
			totalRate = 0.0;
			ratePerUnit = 0.0;
			totalPurchasePrice = 0.0;
			preparatoryCharges = 0.0;
			sticker = 0.0;
			knotting = 0.0;
			box = 0.0;
			polythin_cover = 0.0;

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRate() > 0
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() > 0) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula costWarpYarn =====>");
				costWarpYarn = costWarpYarn + (double) Math
						.round(procurementCosting.getWarpYarnRate() * procurementCosting.getWarpYarnRequired());
				costWarpYarnTemp = costWarpYarn;
				procurementCosting.setCostWarpYarn(costWarpYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWarpYarn =====>" + costWarpYarn);

			}
			if (procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRate() > 0
					&& procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftYarnRequired() > 0) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula costWeftYarn =====>");
				costWeftYarn = costWeftYarn + (double) Math
						.round(procurementCosting.getWeftYarnRate() * procurementCosting.getWeftYarnRequired());
				costWeftYarnTemp = costWeftYarn;
				procurementCosting.setCostWeftYarn(costWeftYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWeftYarn =====>" + costWeftYarn);
			}
			if (procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula warpWastageAmount =====>");
				warpWastageAmount = warpWastageAmount + (double) Math.round(
						(procurementCosting.getWarpYarnRequired() * procurementCosting.getWarpWastagePercetnage()) / 100
								* procurementCosting.getWarpYarnRate());
				warpWastageAmountTemp = warpWastageAmount;
				procurementCosting.setWarpWastageAmount(warpWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula warpWastageAmount =====>"
						+ warpWastageAmount);
			}

			if (procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula weftWastageAmount =====>");
				weftWastageAmount = weftWastageAmount + (double) Math.round(
						(procurementCosting.getWeftYarnRequired() * procurementCosting.getWeftWastagePercentage()) / 100
								* procurementCosting.getWeftYarnRate());
				weftWastageAmountTemp = weftWastageAmount;
				procurementCosting.setWeftWastageAmount(weftWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula weftWastageAmount =====>"
						+ weftWastageAmount);
			}

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null
					&& profitPercetage != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula totalRate =====>");
				totalRate = totalRate + (double) Math
						.round(((costWarpYarnTemp + costWeftYarnTemp + totalRate + weftWastageAmountTemp + weavingWages)
								* (profitPercetage) / 100 + costWarpYarnTemp + costWeftYarnTemp + warpWastageAmountTemp
								+ weftWastageAmountTemp + weavingWages));
				totalRateTemp = totalRate;
				procurementCosting.setTotalRate(totalRate);
				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalRate);
			}
			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null && profitPercetage != null
					&& procurementCosting.getNumberOfUnits() != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula ratePerUnit =====>");
				ratePerUnit = ratePerUnit + (double) Math.round(totalRateTemp / procurementCosting.getNumberOfUnits());
				ratePerUnitTemp = ratePerUnit;
				procurementCosting.setRatePerUnit(ratePerUnit);
				procurementCosting.setWeavingWages((double) Math.round(weavingWages));
				procurementCosting.setProfitPercetage(profitPercetage);
				log.info("<==== END ProcurementCostingBean.calculateFormula ratePerUnit =====>" + totalRate);
			}
			if (preparatoryCharges != null && sticker != null && knotting != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula totalPurchasePrice =====>");
				totalPurchasePrice = totalPurchasePrice + (double) Math
						.round(ratePerUnit + preparatoryCharges + sticker + knotting + box + polythin_cover);
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				procurementCosting.setPreparatoryCharges((double) Math.round(preparatoryCharges));
				procurementCosting.setSticker((double) Math.round(sticker));
				procurementCosting.setKnotting((double) Math.round(knotting));

				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalPurchasePrice);
			}

		} catch (Exception e) {
			log.error("Error Ajax Calculation --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

	}

	public void addOtherCharges() {

		ProcurementCostingRequestDto procurementcostingdto = new ProcurementCostingRequestDto();
		try {
			if (otherChargesName != null && otherChargesAmount != 0) {
				log.info("<==== Starts ProcurementCostingBean.addOtherCharges Inside If =====>" + otherChargesName
						+ otherChargesAmount);
				procurementcostingdto.setOtherChargesName(otherChargesName);
				procurementcostingdto.setOtherChargesAmount(otherChargesAmount);
				procurementCostingDtoList.add(procurementcostingdto);
				procurementCostingRequestDto.setProcurementCostingRequestDtolist(procurementCostingDtoList);
				totalPurchasePrice = totalPurchasePrice + otherChargesAmount;
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				setOtherChargesAmount(null);
				setOtherChargesName(null);
				log.info("<==== END ProcurementCostingBean.addOtherCharges =============>");

			}
		} catch (Exception e) {
			log.info("<==== Exception Occured In ProcurementCostingBean.addOtherCharges Inside Catch=============>");
		}

	}

	public void deleteRow(ProcurementCostingRequestDto procurementcostingDTO) {

		try {
			if (procurementCostingDtoList.contains(procurementcostingDTO)) {
				log.info("<==== Starts ProcurementCostingBean.deleteRow Inside If =====>" + procurementcostingDTO);
				procurementCostingDtoList.remove(procurementcostingDTO);
				procurementCostingRequestDto.getProcurementCostingRequestDtolist().remove(procurementcostingDTO);
				totalPurchasePrice = totalPurchasePrice - procurementcostingDTO.getOtherChargesAmount();
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				log.info("<==== END ProcurementCostingBean.deleteRow =====>");
			}
		} catch (Exception exp) {
			log.info("Error In deleteRow Metod", exp);
		}

	}
	
	public String clear() {
		setSelectedProcurementCostingResponseDto(null);
		return showProcurementCostingSocietyListPage();
		
	}

}
