package in.gov.cooptex.operation.rest.ui;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GSTSummaryDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.ProductSelectionDTO;
import in.gov.cooptex.core.dto.ProductSelectionDetailsDTO;
import in.gov.cooptex.core.dto.ProductSelectionVaritiesDTO;
import in.gov.cooptex.core.dto.TaxSummaryDTO;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.QuotationProductSelection;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.QuotationStatus;
import in.gov.cooptex.operation.model.ProcurementCosting;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.QuotationRequest;
import in.gov.cooptex.operation.production.dto.QuotationResponse;
import in.gov.cooptex.operation.production.dto.QuotationSelectionRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanResponse;
import in.gov.cooptex.operation.production.dto.SelectedQuotationObject;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import in.gov.cooptex.operation.production.model.RetailProductionPlan;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("supplyRateConfirmationBean")
@Scope("session")
public class SupplyRateConfirmationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String SUPPLY_RATE_ADD_URL = "/pages/operation/retailproduction/rateconfirmation/createSupplyRateConfirmation.xhtml?faces-redirect=true;";

	private final String QUOTATION_LIST_URL = "/pages/operation/retailproduction/rateconfirmation/listSupplyRateConfirmation.xhtml?faces-redirect=true;";

	private final String QUOTATION_PREVIEW_URL = "/pages/operation/retailproduction/rateconfirmation/viewQuotation.xhtml?faces-redirect=true;";
	
	private final String PREVIEW_SUPPLY_RATE_CONFIRMATION_URL = "/pages/operation/retailproduction/rateconfirmation/previewSupplyRateConfirmation.xhtml?faces-redirect=true;";

	private final Integer SOCIETY_INFO_NOT_AVAILABLE = 78476;

	private final Integer ENTITY_MASTER_INFO_NOT_AVAILABLE = 12017;

	private final Integer PRODCUT_WAREHOUSE_INFO_NOT_AVAILABLE = 12018;

	private final Integer PRODCUT_VARIETIES_LIST_IS_EMPTY = 12019;

	private final Integer QUOTATION_ADDED_SUCCESSFULLY = 12020;

	private final Integer UNABLE_TO_ADD_QUOTATION = 12021;

	private final Integer QUOTATION_DELETED_SUCCESSFULLY = 12095;

	private final Integer PLEASE_SELECT_DANP_OFFICE = 12023;

	private final Integer PLEASE_SELECT_PRODUCT_SELECTED_PERSONS = 12024;

	private final Integer PLEASE_SELECT_QUOTATION_VALID_DATE = 12025;

	private final Integer PLEASE_SELECT_ANY_ONE_QUOTATION = 12026;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Getter
	@Setter
	String productSelectedPersonNames;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyCode;

	@Getter
	@Setter
	ItemDataWithGstDTO quotationItemWithGstDTO;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	Quotation quotation;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<QuotationResponse> quotationPdfList;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String personSelected;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	String productCategoryNames;

	@Getter
	@Setter
	String regionNames;

	@Getter
	@Setter
	List<EntityMaster> dandpOfficeDropDownList;

	@Getter
	@Setter
	SelectedQuotationObject selectedQuotationObject;

	@Getter
	@Setter
	Long danpOfficeObjectId;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	Long productDesignCode;

	@Getter
	@Setter
	List<String> planToMonthYearList;

	@Getter
	@Setter
	RetailProductionPlanResponse retailProductionPlanResponse;

	@Getter
	@Setter
	RetailProductionPlanRequest retailProductionPlanRequest;

	@Getter
	@Setter
	List<GSTSummaryDTO> GSTSummaryDTOList;

	@Getter
	@Setter
	String productWarehouseAddress;

	@Getter
	@Setter
	String productWarehouseDeliveryAddress;

	@Getter
	@Setter
	String currentPlanStatus;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Getter
	@Setter
	LazyDataModel<QuotationResponse> quotationResponseLazyList;

	@Getter
	@Setter
	List<ProductSelectionDTO> productSelectedByList;

	@Getter
	@Setter
	List<TaxSummaryDTO> taxSummaryDTOList;

	@Getter
	@Setter
	List<ProductSelectionDTO> productSelectedPersonsList;

	@Getter
	@Setter
	QuotationResponse quotationResponse;

	@Getter
	@Setter
	RetailProductionPlan retailProductionPlan;

	@Getter
	@Setter
	SupplierMaster societyObject;

	@Getter
	@Setter
	Double totalValueProductVariety;

	@Getter
	@Setter
	boolean taxTableView = false;

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	List<QuotationItem> quotationItemListValues;

	@Getter
	@Setter
	Double totalCgstAmount;

	@Getter
	@Setter
	Double totalSgstAmount;

	@Getter
	@Setter
	Double totalTaxAmount;

	@Getter
	@Setter
	Double totalTaxValue;

	@Getter
	@Setter
	Double cgstTaxValue;

	@Getter
	@Setter
	Double sgstTaxValue;

	@Getter
	@Setter
	Double totalCgstSgstTaxValue;

	@Getter
	@Setter
	String quotationNumberprefix;

	@Getter
	@Setter
	List<String> productIdsList;

	@Getter
	@Setter
	String danpOfficeAddress;

	@Getter
	@Setter
	Integer quotationNumber;

	@Getter
	@Setter
	EntityMaster productWarehouseObject;

	@Getter
	@Setter
	EntityMaster entityMasterObject;
	
	@Getter
	@Setter
	SupplierMaster supplierMasterObject;

	@Getter
	@Setter
	EntityMaster headOfficeObject;

	@Getter
	@Setter
	String headOfficeAddress;

	@Getter
	@Setter
	EntityMaster danpOfficeObject;

	@Getter
	@Setter
	TaxSummaryDTO taxSummaryDTO;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	QuotationSelectionRequest quotationSelectionRequest;

	@Getter
	@Setter
	ProductSelectionVaritiesDTO selectedProductSelectionVaritiesDTO;

	@Getter
	@Setter
	List<ProductSelectionVaritiesDTO> productSelectionVaritiesDTOList;

	@Getter
	@Setter
	List<RetailProductionPlanResponse> retailProductionPlanPdfList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	String societyAddress;

	@Getter
	@Setter
	List<Object> statusValues;

	@Setter
	@Getter
	StreamedContent file;

	@Setter
	@Getter
	List<ProductSelectionDetailsDTO> productDetails;

	@Getter
	@Setter
	Double totalTaxCgstSgstAmount;

	@Getter
	@Setter
	Double totalTaxMaterialAmount;

	@Getter
	@Setter
	Map<String, Object> filterMap;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	String societyInformationAddress;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	Date quotationEditedDate;

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList = new ArrayList<ProductDesignMaster>();

	@Getter
	@Setter
	private ProcurementCosting procurementCosting = new ProcurementCosting();

	@Getter
	@Setter
	List<ProcurementCosting> procurementCostingList = new ArrayList<ProcurementCosting>();

	@Getter
	@Setter
	Long productVarietyId;

	@Getter
	@Setter
	private Double warpWastagePercetnage = 0.0, weftWastagePercentage = 0.0, warpYarnRate = 0.0, weftYarnRate = 0.0,
			numberOfUnits = 0.0, weightPerUnit = 0.0, warpYarnRequired = 0.0, weftYarnRequired = 0.0,
			costWarpYarn = 0.0, costWeftYarn = 0.0, warpWastageAmount = 0.0, weftWastageAmount = 0.0,
			weavingWages = 0.0, profitPercetage = 0.0, totalRate = 0.0, totalRateTemp = 0.0, costWarpYarnTemp = 0.0,
			costWeftYarnTemp = 0.0, ratePerUnit = 0.0, preparatoryCharges = 0.0, sticker = 0.0, knotting = 0.0,
			totalPurchasePrice = 0.0, box = 0.0, polythin_cover = 0.0, otherChargesAmount = 0.0, printcharges = 0.0,
			covercharges = 0.0;

	@Getter
	@Setter
	private Double warpWastageAmountTemp, warpWastageTemp, ratePerUnitTemp, weftWastageAmountTemp;
	
	List<Long> retailProductSelectionIdList=new ArrayList<Long>();
	
	@Getter
	@Setter
	EntityMaster selectedProductWarehouseObject=new EntityMaster();
	
	@Getter
	@Setter
	List<EntityMaster> productWarehouseObjectList=new ArrayList<EntityMaster>();
	
	
	
	@Getter
	@Setter
	Boolean selectBoolean=true;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		loadUrl();
		loadStatusValues();
		getQuotationList();
		procurementCosting = new ProcurementCosting();
		procurementCostingList = new ArrayList<>();
	}

	public SupplyRateConfirmationBean() {
		log.info("<#======Inside SupplyRateConfirmationBean ======#>");
	}

	private void loadUrl() {
		try {
			serverURL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	private void loadStatusValues() {

		Object[] statusArray = QuotationStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("SupplyRateConfirmationBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		quotationResponse = new QuotationResponse();

		QuotationRequest request = new QuotationRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());

			if (key.equals("quotationNumberPrefixOrNumber")) {
				log.info("quotationNumber : " + value);
				request.setQuotationNumberPrefixOrNumber(value);
			}
			if (key.equals("societyCodeOrName")) {
				log.info("societyCodeOrName : " + value);
				request.setSocietyCodeOrName(value);
			}
			if (key.equals("danpOfficeCodeOrName")) {
				log.info("danpOfficeCodeOrName : " + value);
				request.setDanpOfficeCodeOrName(value);
			}
			if (key.equals("productWarehouseCodeorName")) {
				log.info("productWarehouseCodeorName : " + value);
				request.setProductWarehouseCodeorName(value);
			}
			if (key.equals("productSelectedBy")) {
				log.info("productSelectedBy : " + value);
				request.setProductSelectedBy(value);
			}
			if (key.equals("status")) {
				log.info("status : " + value);
				request.setStatus(value);
			}
		}
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("SupplyRateConfirmationBean.getSearchData Method Complted");
		return baseDTO;
	}

	private void loadAllDnpOffices() {
		log.info("<--- Inside loadAllDnpOffices() ---> ");

		String URL = serverURL + appPreference.getOperationApiUrl() + "/dnp/get/getalldnpoffices";
		log.info("<--- Get all available D & P offices URL ---> " + URL);
		BaseDTO baseDTO = httpService.get(URL);
		try {
			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				dandpOfficeDropDownList = (List<EntityMaster>) mapper.readValue(jsonResponse,
						new TypeReference<List<EntityMaster>>() {
						});

				log.info("<--- dAndPOfficeList size ---> " + dandpOfficeDropDownList.size());
			}
		} catch (Exception e) {
			log.error("<--- Error in dandpOfficeDropDownList List --->", e);
		}
	}

	public void loadLazyDraftProductionList() {
		addButtonFlag = false;
		log.info("SupplyRateConfirmationBean loadLazyDraftProductionList method started");
		quotationResponseLazyList = new LazyDataModel<QuotationResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<QuotationResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<QuotationResponse> data = new ArrayList<QuotationResponse>();
				try {

					filterMap = filters;
					sortingOrder = sortOrder;
					sortingField = sortField;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<QuotationResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(QuotationResponse res) {
				return res != null ? res.getId() : null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public QuotationResponse getRowData(String rowKey) {
				List<QuotationResponse> responseList = (List<QuotationResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (QuotationResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void convertData() {
		log.info("SupplyRateConfirmationBean convertData method started");
		quotationSelectionRequest = new QuotationSelectionRequest();
		quotationSelectionRequest.setDanpOfficeId(danpOfficeObjectId);
		quotationSelectionRequest.setQuotationNumberPrefix(quotation.getQuotationNumberPrefix());
		quotationSelectionRequest.setQuotationNumber(quotation.getQuotationNumber());
		quotationSelectionRequest.setProductSelectedByList(productSelectedByList);
		quotationSelectionRequest.setSupplierId(societyObject.getId());
		quotationSelectionRequest.setStatus(QuotationStatus.INITIATED);
		quotationSelectionRequest.setValidDate(selectedQuotationObject.getValidDate());
		quotationSelectionRequest.setProductVaritiesList(productSelectionVaritiesDTOList);
		quotationSelectionRequest.setBillingEntityId(headOfficeObject.getId());
//		quotationSelectionRequest.setBuyerEntityId(societyObject.getId());
		quotationSelectionRequest.setBuyerEntityId(danpOfficeObjectId);
		quotationSelectionRequest.setDeliveryEntityId(productWarehouseObject.getId());
		quotationSelectionRequest.setGstTaxDetailsList(gstPercentageWiseDTOList);
		quotationSelectionRequest.setTaxDetailsList(GSTSummaryDTOList);

	}

	public String getSupplyRateCreateForm() {
		selectedQuotationObject = new SelectedQuotationObject();
		dataToggle = "collapse in";
		productSelectedPersonNames = null;
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();
		productSelectedByList = new ArrayList<ProductSelectionDTO>();
		gstPercentageWiseDTOList = new ArrayList<>();
		societyObject = null;
		danpOfficeObjectId = null;
		gstWiseList = new ArrayList<>();
		productDetails = new ArrayList<>();
		productWarehouseAddress = null;
		cgstTaxValue = null;
		totalTaxMaterialAmount = null;
		sgstTaxValue = null;
		totalCgstSgstTaxValue = null;
		productWarehouseObject = null;
		totalValueProductVariety = null;
		procurementCosting = new ProcurementCosting();
		procurementCostingList = new ArrayList<>();
		productDesignCode = null;
		productVarietyCode = new ProductVarietyMaster();
		productVarietyId = null;
		personSelected = productSelectedByList.size() + " Person Selected";
		loadDANDPOfficeList();
		loadSocietyDetailsByLoggedinUser();
		getProdutSelectedPersonsList();
		//getAllQuotationProductSelectionList();
		loadPreviewDetails();
		loadHeadOffice();
		return SUPPLY_RATE_ADD_URL;
	}

	public void changeProductSelectedBy() {
		log.info("SupplyRateConfirmationBean changeProductSelectedBy method started" + productSelectedByList);

		if (productSelectedByList.size() > 1) {
			personSelected = productSelectedByList.size() + " Persons Selected";
		} else {
			personSelected = productSelectedByList.size() + " Person Selected";
		}

		productSelectedPersonNames = "";
		for (ProductSelectionDTO productSelected : productSelectedByList) {
			if (productSelectedPersonNames != "") {
				productSelectedPersonNames = productSelectedPersonNames + ",";
			}
			productSelectedPersonNames = productSelectedPersonNames + productSelected.getProductSelectedBy() + "-"
					+ productSelected.getRegionCode() + "/" + productSelected.getRegionName();
		}

		log.info("SupplyRateConfirmationBean changeProductSelectedBy method completed : " + productSelectedPersonNames);

	}

	public void choosePrint() {
		Map<String, Object> options = new HashMap<String, Object>();
		options.put("resizable", false);
		options.put("draggable", false);
		options.put("modal", true);
		RequestContext.getCurrentInstance().openDialog("viewSupplyRateConfirmation2.xhtml", options, null);
	}

	public void loadDANDPOfficeList() {
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getdanpofficelistbyusersupplierid";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			dandpOfficeDropDownList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
	}

	public void loadSocietyDetailsByLoggedinUser() {
		log.info("Start method loadSocietyDetailsByLoggedinUser");
		societyObject=new SupplierMaster();
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getsocietybasedonlogin";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			societyObject = mapper.readValue(jsonValue, SupplierMaster.class);

			if (societyObject == null) {
				log.info("SupplyRateConfirmationBean societyObject value is null");
				errorMap.notify(SOCIETY_INFO_NOT_AVAILABLE);
			}

			String societyAddressArray[] = AppUtil.prepareAddress(societyObject.getAddressId());

			if(societyAddressArray != null) {
				if (languageBean.getLocaleCode().equals("en_IN")) {
					societyAddress = societyAddressArray[0];
					societyInformationAddress = societyAddressArray[0];
				} else {
					societyInformationAddress = societyAddressArray[1];
				}
			}
			log.info("SupplyRateConfirmationBean societyObject " + societyObject);
		} catch (Exception e) {
			log.error("Exception in getSocietyBasedOnLogin ", e);
		}
	}

	public String editQuotation() {
		log.info("SupplyRateConfirmationBean editQuotation method started");

		if (quotationResponse == null || quotationResponse.getId() == null) {
			errorMap.notify(PLEASE_SELECT_ANY_ONE_QUOTATION);
			return null;
		}

		if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.SUBMITTED)) {
			errorMap.notify(ErrorDescription.SUBMITTED_QUOTATION_CANNOT_EDITED.getCode());
			return null;
		}
		if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.APPROVED)) {
			errorMap.notify(ErrorDescription.APPROVED_QUOTATION_CANNOT_EDITED.getCode());
			return null;
		}
		/*if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.REJECTED)) {
			errorMap.notify(ErrorDescription.APPROVED_QUOTATION_CANNOT_EDITED.getCode());
			return null;
		}*/

		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/get/" + quotationResponse.getId();

		BaseDTO baseDTO = new BaseDTO();

		try {

			baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();

			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

			quotation = mapper.readValue(jsonValue, Quotation.class);

			quotationNumberprefix = quotation.getQuotationNumberPrefix();

			quotationNumber = quotation.getQuotationNumber();

			selectedQuotationObject = new SelectedQuotationObject();

			quotationItemListValues = quotation.getQuotationItemList();

			quotationEditedDate = new Date(quotation.getValidDate().getTime());

			societyObject = getSupplierMasterById(quotation.getSupplier().getId());
			

			String societyAddressArray[] = AppUtil.prepareAddress(societyObject.getAddressMaster());

			if(societyAddressArray != null) {
				if (languageBean.getLocaleCode().equals("en_IN")) {
					societyAddress = societyAddressArray[0];
					societyInformationAddress = societyAddressArray[0];
				} else {
					societyInformationAddress = societyAddressArray[1];
				}
			}

			//loadAllDnpOffices();
			loadDANDPOfficeList();
			danpOfficeObjectId = quotation.getDandpOffice().getId();
			// loadSocietyDetailsByLoggedinUser();

			productSelectedByList = new ArrayList<>();
			log.info("product selection quotation list-----------"+quotation.getQuotationProductSelctionIdList().size());
			if(quotation.getQuotationProductSelctionIdList() != null) {
				for (Long selectionId : quotation.getQuotationProductSelctionIdList()) {
					ProductSelectionDTO productSelectionDTO = new ProductSelectionDTO();
					productSelectionDTO.setProductSelectionId(selectionId);
					productSelectedByList.add(productSelectionDTO);
				}
			}
			//getProdutSelectedPersonsList();
			loadProductSelectedByList();
			changeProductSelectedBy();
			loadPreviewDetails();
			loadHeadOffice();
			getProdutWarehouseDetails();
			getProductVaritiesList();
			getProductVaritiesDetailsList();
			selectedQuotationObject.setValidDate(quotationEditedDate);
			dataToggle = "collapse in";

		} catch (Exception e) {
			log.error("Exception ", e);
		}

		return SUPPLY_RATE_ADD_URL;

	}

	public String viewQuotation() {
		log.info("SupplyRateConfirmationBean viewPlan method started");
		if (quotationResponse == null || quotationResponse.getId() == null) {
			errorMap.notify(PLEASE_SELECT_ANY_ONE_QUOTATION);
			return null;
		}

		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/get/" + quotationResponse.getId();

		BaseDTO baseDTO = new BaseDTO();

		try {

			baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();

			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

			quotation = mapper.readValue(jsonValue, Quotation.class);

			danpOfficeObjectId = quotation.getDandpOffice().getId();

			danpOfficeObject = quotation.getDandpOffice();

			selectedQuotationObject = new SelectedQuotationObject();

			selectedQuotationObject.setValidDate(quotation.getValidDate());

			societyObject = getSupplierMasterById(quotation.getSupplier().getId());

			String societyAddressArray[] = AppUtil.prepareAddress(societyObject.getAddressMaster());

				if (languageBean.getLocaleCode().equals("en_IN") && societyAddressArray!=null) {
					societyAddress = societyAddressArray[0];
					societyInformationAddress = societyAddressArray[0];
				} else if(societyAddressArray !=null) {
					societyInformationAddress = societyAddressArray[1];
				}

			productSelectedByList = new ArrayList<>();

			for (Long quotationProductSelection : quotation.getQuotationProductSelctionIdList()) {
				ProductSelectionDTO productSelectionDTO = new ProductSelectionDTO();
				productSelectionDTO
						.setProductSelectionId(quotationProductSelection);
				productSelectedByList.add(productSelectionDTO);
			}

			loadDANDPOfficeList();
			//getProdutSelectedPersonsList();
			loadProductSelectedByList();
			
			changeProductSelectedBy();
			// loadSocietyDetailsByLoggedinUser();
			loadPreviewDetails();
			loadHeadOffice();
			getProdutWarehouseDetails();
			getProductVaritiesList();
			getProductVaritiesDetailsList();
			log.info("SupplyRateConfirmationBean viewPlan method started" + danpOfficeObject);

		} catch (Exception e) {
			log.error("Exception ", e);
		}

		return QUOTATION_PREVIEW_URL;

	}

	public EntityMaster getEnitiyMasterById(Long id) {
		try {
			String URL = serverURL + "/entitymaster/getentitymasterbyid/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			entityMasterObject = mapper.readValue(jsonResponse, EntityMaster.class);

			if (entityMasterObject == null) {
				log.info("SupplyRateConfirmationBean entityMasterObject value is null");
				errorMap.notify(ENTITY_MASTER_INFO_NOT_AVAILABLE);
			}

			log.info("SupplyRateConfirmationBean entityMasterObject " + entityMasterObject);
		} catch (Exception e) {
			log.error("Exception in getDANDPOfficeList ", e);
		}
		return entityMasterObject;
	}

	public SupplierMaster getSupplierMasterById(Long id) {
		log.info("<--Starts SupplyRateConfirmationBean .getSupplierMasterById()-->");
		try {
			supplierMasterObject=new SupplierMaster();
			String URL = serverURL + appPreference.getOperationApiUrl()+ "/supply/rate/getsuppliermasterbyid/" + id;
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			supplierMasterObject = mapper.readValue(jsonResponse, SupplierMaster.class);

			if (supplierMasterObject == null) {
				log.info("SupplyRateConfirmationBean supplierMasterObject value is null");
				errorMap.notify(SOCIETY_INFO_NOT_AVAILABLE);
			}

			log.info("SupplyRateConfirmationBean supplierMasterObject " + supplierMasterObject);
		} catch (Exception e) {
			log.error("Exception in getSupplierMasterById ", e);
		}
		return supplierMasterObject;
	}
	
	public void getProdutWarehouseDetails() {
		try {
			log.info("danpOfficeObjectId --------------->"+danpOfficeObjectId);
			danpOfficeObject = getEnitiyMasterById(danpOfficeObjectId);

			String danpOfficeAddressArray[] = AppUtil.prepareAddress(danpOfficeObject.getAddressMaster());

			if (languageBean.getLocaleCode().equals("en_IN") && danpOfficeAddressArray !=null) {
				danpOfficeAddress = danpOfficeAddressArray[0];
			} else if(danpOfficeAddressArray !=null){
				danpOfficeAddress = danpOfficeAddressArray[1];
			}
			// if (danpOfficeObject.getWarehhouse() != null) {
			// productWarehouseObject =
			// getEnitiyMasterById(danpOfficeObject.getWarehhouse().getId());
			// }
			if (danpOfficeObject.getWarehhouse() == null) {
				productWarehouseObject = getEnitiyMasterById(danpOfficeObject.getId());
			} else {
				productWarehouseObject = getEnitiyMasterById(danpOfficeObject.getWarehhouse().getId());
			}

			if (productWarehouseObject == null) {
				log.info("SupplyRateConfirmationBean productWarehouseObject value is null");
				errorMap.notify(PRODCUT_WAREHOUSE_INFO_NOT_AVAILABLE);
				AppUtil.addWarn("Product Warehouse Not available");
			}else
			{

				if(danpOfficeObjectId==null)
				{
					productWarehouseDeliveryAddress ="";
					productWarehouseAddress="";
					productWarehouseObjectList=new ArrayList<EntityMaster>();
					return;
				}
				//String url=serverURL + "/entitymaster/getdnpofficebyentityid/" + productWarehouseObject.getId();
				String url=serverURL + "/entitymaster/getdnpofficebyentityid/" + danpOfficeObjectId;
				log.info("====Url is==="+url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					productWarehouseObjectList = mapper.readValue(jsonResponse,
							new TypeReference<List<EntityMaster>>() {
							});
					
					if(!productWarehouseObjectList.isEmpty())
					{
						if(productWarehouseObjectList.get(0).getAddressMaster()!=null)
						{
							String productWarehouseAddressArray[] = AppUtil.prepareAddress(productWarehouseObjectList.get(0).getAddressMaster());
							if(productWarehouseAddressArray !=null) {
								if (languageBean.getLocaleCode().equals("en_IN")) {
									productWarehouseAddress = productWarehouseAddressArray[0];
									productWarehouseDeliveryAddress = productWarehouseAddressArray[0];
								} else {
									productWarehouseAddress = productWarehouseAddressArray[1];
									productWarehouseDeliveryAddress = productWarehouseAddressArray[1];
								}
							}
						}
					}
				}
			}

			log.info("SupplyRateConfirmationBean productWarehouseObject " + productWarehouseObject);
		} catch (Exception e) {
			log.error("Exception in productWarehouseObject ", e);
		}
	}
	
	public void setAddressMaster()
	{
		try {
			if(selectedProductWarehouseObject.getAddressMaster()!=null)
			{
				String productWarehouseAddressArray[] = AppUtil.prepareAddress(selectedProductWarehouseObject.getAddressMaster());
				if(productWarehouseAddressArray !=null) {
					if (languageBean.getLocaleCode().equals("en_IN")) {
						productWarehouseAddress = productWarehouseAddressArray[0];
						productWarehouseDeliveryAddress = productWarehouseAddressArray[0];
					} else {
						productWarehouseAddress = productWarehouseAddressArray[1];
						productWarehouseDeliveryAddress = productWarehouseAddressArray[1];
					}
				}
			}
		}catch (Exception e) {
			log.error("=== Exception is ==" +e.getMessage());
		}
	}

	public void getProdutSelectedPersonsList() {
		try {
			/*String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getproductselectedpersonslist/"
					+ societyObject.getId();*/
			
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getProdutSelectedPersonsListBysupplierID/"+ societyObject.getId();
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productSelectedPersonsList = mapper.readValue(jsonResponse, new TypeReference<List<ProductSelectionDTO>>() {
			});
			
			log.info("productSelectedPersonsList====>"+productSelectedPersonsList.size());
			List<ProductSelectionDTO> productSelectedPersonsLocalList=new ArrayList<>();
			for(ProductSelectionDTO productSelectionDTO : productSelectedPersonsList) {
				log.info(":::: productSelectionDTO ::::"+productSelectionDTO);
			if (productSelectedPersonsLocalList.stream().anyMatch(t -> t.getRegionCode().equalsIgnoreCase(productSelectionDTO.getRegionCode()))) {
				log.info("Already in the list");
				int index=productSelectedPersonsLocalList.indexOf(productSelectedPersonsLocalList.stream().anyMatch(t -> t.getRegionCode().equalsIgnoreCase(productSelectionDTO.getRegionCode())));
				log.info(":: index ::"+(index+1));
				List<Long> longList= productSelectedPersonsLocalList.get((index+1)).getProductSelectionIdList();
				productSelectionDTO.setProductSelectionIdList(new ArrayList<>());
				productSelectionDTO.getProductSelectionIdList().addAll(longList);
				productSelectionDTO.getProductSelectionIdList().add(productSelectionDTO.getProductSelectionId());
				productSelectedPersonsLocalList.set((index+1),productSelectionDTO);
			}
			else {
				log.info("Not in the list");	
				productSelectionDTO.setProductSelectionIdList(new ArrayList<>());
				productSelectionDTO.getProductSelectionIdList().add(productSelectionDTO.getProductSelectionId());
				productSelectedPersonsLocalList.add(productSelectionDTO);
			}
				log.info(":::: productSelectionDTO ::::"+productSelectionDTO.getProductSelectionId()); 
				
			}
			productSelectedPersonsList.clear();
			log.info("product selection local list-------"+productSelectedPersonsLocalList.size());
			log.info("product selection id list-----------"+productSelectedPersonsLocalList.get(0).getProductSelectionIdList().size());
			productSelectedPersonsList.addAll(productSelectedPersonsLocalList);
			
		} catch (Exception e) {
			log.error("Exception in getProdutSelectedPersonsList ", e);
		}
	}

	public void loadHeadOffice() {
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getheadoffice";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			headOfficeObject = mapper.readValue(jsonResponse, EntityMaster.class);

			if (headOfficeObject == null) {
				errorMap.notify(ENTITY_MASTER_INFO_NOT_AVAILABLE);
			}
			if(headOfficeObject.getAddressMaster() != null) {
				String headOfficeAddressArray[] = AppUtil.prepareAddress(headOfficeObject.getAddressMaster());
				if(headOfficeAddressArray.length > 0) {
					if (languageBean.getLocaleCode().equals("en_IN")) {
						headOfficeAddress = headOfficeAddressArray[0].replace(",", "<br/>");
					} else {
						headOfficeAddress = headOfficeAddressArray[1].replace(",", "<br/>");
					}
				}
			}else {
				headOfficeAddress = "";
			}

		} catch (Exception e) {
			log.error("Exception in loadHeadOffice ", e);
		}
	}

	public String loadProductSelectedByList() {
		try {

			quotationSelectionRequest = new QuotationSelectionRequest();
			quotationSelectionRequest.setProductSelectedByList(productSelectedByList);

			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/loadproductselectedlist";
			BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productSelectedByList = mapper.readValue(jsonResponse, new TypeReference<List<ProductSelectionDTO>>() {
			});

		} catch (Exception e) {
			log.error("Exception in getProdutVaritiesList ", e);
		}
		return null;
	}

	public String getProductVaritiesList() {
		try {

			if (danpOfficeObjectId == null && action.equals("ADD")) {
				errorMap.notify(PLEASE_SELECT_DANP_OFFICE);
				return null;
			}
			if (productSelectedByList.size() == 0 && action.equals("ADD")) {
				errorMap.notify(PLEASE_SELECT_PRODUCT_SELECTED_PERSONS);
				return null;
			}
			if (selectedQuotationObject.getValidDate() == null && action.equals("ADD")) {
				errorMap.notify(PLEASE_SELECT_QUOTATION_VALID_DATE);
				return null;
			}

			productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();
			quotationSelectionRequest = new QuotationSelectionRequest();
			log.info("productSelectedByList size------------"+productSelectedByList.size());
			quotationSelectionRequest.setProductSelectedByList(productSelectedByList);

			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getproductvaritieslist";
			BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productSelectionVaritiesDTOList = mapper.readValue(jsonResponse,
					new TypeReference<List<ProductSelectionVaritiesDTO>>() {
					});
			calculateTotal();
			gstCalculation();
			getProductVaritiesDetailsList();

		} catch (Exception e) {
			log.error("Exception in getProdutVaritiesList ", e);
		}
		return null;
	}

	public void calculateTotal() {
		totalValueProductVariety = productSelectionVaritiesDTOList.stream()
				.collect(Collectors.summingDouble(ProductSelectionVaritiesDTO::getUnitAmount));
	}

	public void getProductVaritiesDetailsList() {
		try {
			productDetails = new ArrayList<ProductSelectionDetailsDTO>();
			quotationSelectionRequest = new QuotationSelectionRequest();
			quotationSelectionRequest.setProductSelectedByList(productSelectedByList);

			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/getproductvaritiesdetailslist";
			BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);

			if (baseDTO == null) {
				log.info("SupplyRateConfirmationBean getProductVaritiesDetailsList value is empty");
				errorMap.notify(PRODCUT_VARIETIES_LIST_IS_EMPTY);
			}

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			productDetails = mapper.readValue(jsonResponse, new TypeReference<List<ProductSelectionDetailsDTO>>() {
			});

		} catch (Exception e) {
			log.error("Exception in getProductVaritiesDetailsList ", e);
		}
	}

	public void gstCalculation() {
		GSTSummaryDTOList = new ArrayList<GSTSummaryDTO>();
		productIdsList = new ArrayList<>();
		for (ProductSelectionVaritiesDTO productSelectionVaritiesDTO : productSelectionVaritiesDTOList) {
			String productID = new String();
			productID = productSelectionVaritiesDTO.getProductVarietyId().toString();
			productIdsList.add(productID);
		}

		quotationSelectionRequest = new QuotationSelectionRequest();
		quotationSelectionRequest.setProductIdsList(productIdsList);
		quotationSelectionRequest.setProductVaritiesList(productSelectionVaritiesDTOList);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/loadgstsummary";
			BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotationItemWithGstDTO = mapper.readValue(jsonResponse, ItemDataWithGstDTO.class);
			gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
			gstWiseList = quotationItemWithGstDTO.getGstWiseList();
			totalTaxCgstSgstAmount = calculateTotalTaxAmount(gstPercentageWiseDTOList);
			totalTaxMaterialAmount = totalTaxCgstSgstAmount + totalValueProductVariety;
			calculateTotalTaxSummary();

			amountInWords = AmountToWordConverter.converter(String.valueOf(totalTaxMaterialAmount));
			dataToggle = "collapse";

		} catch (Exception e) {
			log.error("Exception in gstCalculation ", e);
		}

	}

	public void calculateTotalTaxSummary() {
		if (gstPercentageWiseDTOList.size() != 0) {
			cgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			sgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalCgstSgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
		}
	}

	public Double calculateTotalTaxAmount(List<HsnCodeTaxPercentageWiseDTO> gstList) {
		Double total = 0.0;
		for (HsnCodeTaxPercentageWiseDTO gst : gstList) {
			if (gst.getTotalTax() != null) {
				total = total + gst.getTotalTax();
			}
		}
		return total;
	}

	public Date getCurrentDate() {
		return new Date();
	}

	public String loadPreviewDetails() {
		String quotationNumberString = "";
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/loadpreviewdetails/"
					+ societyObject.getCode();
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotation = mapper.readValue(jsonValue, Quotation.class);
		} catch (Exception e) {
			log.error("Exception in generateQuotationNumber ", e);
		}

		return quotationNumberString;
	}

	public String saveProcurementCosting() {
		procurementCosting.setProductVarietyId(productVarietyId);

		if (productDesignCode != null) {
			procurementCosting.setProductDesignId(productDesignCode);
		}
		int count = -1;
		for (ProcurementCosting costing : procurementCostingList) {
			if (costing.getProductVarietyId() == procurementCosting.getProductVarietyId()) {
				count++;
				break;
			}
		}
		if (count >= 0) {
			procurementCostingList.remove(count);
		}
		procurementCostingList.add(procurementCosting);

		return SUPPLY_RATE_ADD_URL;

	}

	public void getSelectedProductVarietyMaster() {

		try {
			productVarietyCode = new ProductVarietyMaster();
			String url = serverURL + "/productvariety/getselectedProductVarity/" + productVarietyId;
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productVarietyCode = mapper.readValue(jsonResponse, ProductVarietyMaster.class);
			}
		} catch (Exception e) {
			log.error("Exception Occured whiule retrieveing product variety information");
		}
	}

	public void productVarietySelect(ProductSelectionVaritiesDTO selectedProductSelectionVaritiesDTO, int idx) {
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList =====>");
		productVarietyId = selectedProductSelectionVaritiesDTO.getProductVarietyId();
		productVarietyCode = new ProductVarietyMaster();
		try {
			productDesignMasterList = new ArrayList<>();
			String url = serverURL + "/productDesign/getdesign/"
					+ selectedProductSelectionVaritiesDTO.getProductVarietyId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductDesignMaster>>() {
						});
			}
			log.info("Product Design Master List Size" + productDesignMasterList.size());

		} catch (Exception e) {
			log.error("Error Product Design List --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.loadProductVarietyList  =====>");
	}

	public String saveQuotation(String requestType) {
		log.info("SupplyRateConfirmationBean saveQuotation method started");

		convertData();
		if (requestType.equals(QuotationStatus.INITIATED.name())) {
			quotationSelectionRequest.setStatus(QuotationStatus.INITIATED);
		} else {
			quotationSelectionRequest.setStatus(QuotationStatus.SUBMITTED);
		}
		quotationSelectionRequest.setProcurementCostingList(procurementCostingList);
		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/create";
		if(selectedProductWarehouseObject.getId()!=null)
		{
			quotationSelectionRequest.setBuyerEntityId(selectedProductWarehouseObject.getId());
		}
		BaseDTO baseDTO = httpService.post(url, quotationSelectionRequest);
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Quotation Added successfully");
				errorMap.notify(QUOTATION_ADDED_SUCCESSFULLY);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} else {
			log.info("SupplyRateConfirmationBean failiure in saveQuotation method ");
		}
		return getQuotationList();
	}

	public String submitQuotation(String requestType) {
		log.info("SupplyRateConfirmationBean submitQuotation method started");

		convertData();
		quotationSelectionRequest.setStatus(QuotationStatus.SUBMITTED);

		if (requestType.equals(QuotationStatus.INITIATED.name())) {
			quotationSelectionRequest.setStatus(QuotationStatus.INITIATED);
		} else {
			quotationSelectionRequest.setStatus(QuotationStatus.SUBMITTED);
		}
		quotationSelectionRequest.setQuotationItemList(quotationItemListValues);
		quotationSelectionRequest.setId(quotationResponse.getId());
		quotationSelectionRequest.setQuotationNumberPrefix(quotationNumberprefix);
		quotationSelectionRequest.setQuotationNumber(quotationNumber);
		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/update";
		BaseDTO baseDTO = httpService.post(url, quotationSelectionRequest);
		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Quotation Added successfully");
				errorMap.notify(QUOTATION_ADDED_SUCCESSFULLY);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} else {
			log.info("SupplyRateConfirmationBean failiure in submitQuotation method ");
		}
		return getQuotationList();
	}

	public String getQuotationList() {
		log.info("SupplyRateConfirmationBean getQuotationList method started");
		dataToggle = "collapse in";
		quotationResponse = new QuotationResponse();
		loadLazyDraftProductionList();
		return QUOTATION_LIST_URL;

	}

	public String delete() {
		log.info("SupplyRateConfirmationBean delete method started");
		if (quotationResponse == null || quotationResponse.getId() == null) {
			errorMap.notify(PLEASE_SELECT_ANY_ONE_QUOTATION);
			return null;
		}
		if (!quotationResponse.getStatus().equals(QuotationStatus.INITIATED)) {
			errorMap.notify(ErrorDescription.CANNOT_DELETE_QUOTATION.getCode());
			return getQuotationList();
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/delete/"
				+ quotationResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(QUOTATION_DELETED_SUCCESSFULLY);
			return getQuotationList();
		} else {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return getQuotationList();

	}

	public void generatePDF() {
		log.info("Inside SupplyRateConfirmationBean generatePDF ");
		String url = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/downloadpdf";

		try {
			RestTemplate restTemplate = new RestTemplate();
			QuotationRequest quotationRequest = new QuotationRequest();

			restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

			BaseDTO baseDTO = getSearchData(null, null, sortingField, sortingOrder, filterMap);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotationPdfList = mapper.readValue(jsonResponse, new TypeReference<List<QuotationResponse>>() {
			});

			quotationRequest.setQuotationPdfList(quotationPdfList);

			HttpEntity<QuotationRequest> httpRequestEntity = new HttpEntity<QuotationRequest>(quotationRequest,
					loginBean.headers);

			ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.POST, httpRequestEntity,
					byte[].class);

			InputStream targetStream = new ByteArrayInputStream(response.getBody());

			file = new DefaultStreamedContent(targetStream, "application/pdf", "SupplyRateConfirmationList.pdf");

		} catch (Exception exp) {
			log.error("Error in Pdf Download", exp);
			errorMap.notify(1);
		}

	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current Quotation Status ---> " + currentPlanStatus);
		log.info("<--- Comments ---> " + approveComments);
		log.info("<--- Comments ---> " + rejectComments);

		if (currentPlanStatus.equals("APPROVED") && (approveComments == null || approveComments.equals(""))) {
			log.error("Please Enter Approve Comments");
			errorMap.notify(ErrorDescription.PLEASE_ENTER_APPROVE_COMMENTS.getCode());
			return null;
		}

		if (currentPlanStatus.equals("REJECTED") && (rejectComments == null || rejectComments.equals(""))) {
			log.error("Please Enter Reject Comments");
			errorMap.notify(ErrorDescription.PLEASE_ENTER_REJECT_COMMENTS.getCode());
			return null;
		}

		BaseDTO baseDTO = null;

		try {
			if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
				QuotationSelectionRequest request = new QuotationSelectionRequest(quotationResponse.getId());
				request.setComments(approveComments);
				String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/approve";
				baseDTO = httpService.post(URL, request);
			} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
				QuotationSelectionRequest request = new QuotationSelectionRequest(quotationResponse.getId());
				request.setComments(rejectComments);
				String URL = serverURL + appPreference.getOperationApiUrl() + "/supply/rate/reject";
				baseDTO = httpService.post(URL, request);
			}

			if (baseDTO != null) {
				errorMap.notify(baseDTO.getStatusCode());
			}
			approveComments = null;
			rejectComments = null;

		} catch (Exception exp) {
			log.error("Error in Approving quotation", exp);
			errorMap.notify(1);
		}

		return QUOTATION_LIST_URL;
	}

	public void changeCurrentStatus(String value) {
		log.info("<---changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;

		approveComments = null;
		rejectComments = null;
	}

	public void processDelete() {
		log.info("SupplyRateConfirmationBean processDelete method started");
		if (quotationResponse == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(PLEASE_SELECT_ANY_ONE_QUOTATION);
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("SupplyRateConfirmationBean onRowSelect method started");
		quotationResponse = ((QuotationResponse) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("SupplyRateConfirmationBean onPageLoad method started");
		addButtonFlag = false;
	}

	// public String getPreviewSupplyRateConfirmation() {
	// try {
	// productDetails = new ArrayList<ProductSelectionDetailsDTO>();
	// quotationSelectionRequest = new QuotationSelectionRequest();
	// quotationSelectionRequest.setProductSelectedByList(productSelectedByList);
	//
	// String URL = serverURL + appPreference.getOperationApiUrl() +
	// "/supply/rate/getproductvaritiesdetailslist";
	// BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);
	//
	// if (baseDTO == null) {
	// log.info("SupplyRateConfirmationBean getProductVaritiesDetailsList value is
	// empty");
	// errorMap.notify(PRODCUT_VARIETIES_LIST_IS_EMPTY);
	// }
	//
	// ObjectMapper mapper = new ObjectMapper();
	// String jsonResponse =
	// mapper.writeValueAsString(baseDTO.getResponseContent());
	// productDetails = mapper.readValue(jsonResponse, new
	// TypeReference<List<ProductSelectionDetailsDTO>>() {
	// });
	//
	// } catch (Exception e) {
	// log.error("Exception in getProductVaritiesDetailsList ", e);
	// }
	// }

	public void calculateFormula() {

		try {

			log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

			costWarpYarn = 0.0;
			costWeftYarn = 0.0;
			warpWastageAmount = 0.0;
			weftWastageAmount = 0.0;
			totalRate = 0.0;
			ratePerUnit = 0.0;
			totalPurchasePrice = 0.0;

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRate() > 0
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() > 0) {
				log.info(
						"<==== Inside ProcurementCostingBean.calculateFormula costWarpYarn =====  WarpYarnRate * WarpYarnRequired   >");

				costWarpYarn = costWarpYarn + (double) Math
						.round(procurementCosting.getWarpYarnRate() * procurementCosting.getWarpYarnRequired());
				costWarpYarnTemp = costWarpYarn;
				procurementCosting.setCostWarpYarn(costWarpYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWarpYarn =====>" + costWarpYarn);

			}
			if (procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRate() > 0
					&& procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftYarnRequired() > 0) {
				log.info(
						"<==== Inside ProcurementCostingBean.calculateFormula costWeftYarn ===== WeftYarnRate * WeftYarnRequired ");
				costWeftYarn = costWeftYarn + (double) Math
						.round(procurementCosting.getWeftYarnRate() * procurementCosting.getWeftYarnRequired());
				costWeftYarnTemp = costWeftYarn;
				procurementCosting.setCostWeftYarn(costWeftYarn);
				log.info("<==== END ProcurementCostingBean.calculateFormula costWeftYarn =====>" + costWeftYarn);
			}
			if (procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null) {
				log.info(
						"<==== Inside ProcurementCostingBean.calculateFormula warpWastageAmount =====  WarpYarnRequired * WarpWastagePercetnage / 100 * WarpYarnRate ");
				warpWastageAmount = warpWastageAmount + (double) Math.round(
						(procurementCosting.getWarpYarnRequired() * procurementCosting.getWarpWastagePercetnage()) / 100
								* procurementCosting.getWarpYarnRate());
				warpWastageAmountTemp = warpWastageAmount;
				procurementCosting.setWarpWastageAmount(warpWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula warpWastageAmount =====>"
						+ warpWastageAmount);
			}

			if (procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null) {
				log.info(
						"<==== Inside ProcurementCostingBean.calculateFormula weftWastageAmount =====  WeftYarnRequired * WeftWastagePercentage / 100 * WeftYarnRate ");
				weftWastageAmount = weftWastageAmount + (double) Math.round(
						(procurementCosting.getWeftYarnRequired() * procurementCosting.getWeftWastagePercentage()) / 100
								* procurementCosting.getWeftYarnRate());
				weftWastageAmountTemp = weftWastageAmount;
				procurementCosting.setWeftWastageAmount(weftWastageAmount);
				log.info("<==== END ProcurementCostingBean.calculateFormula weftWastageAmount =====>"
						+ weftWastageAmount);
			}

			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null
					&& profitPercetage != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula "
						+ " totalRate =====  (costWarpYarn + costWeftYarn + totalRate + weftWastageAmount + weavingWages) * profitPercetage / 100 +  "
						+ " costWarpYarn + costWeftYarn + warpWastageAmount + weftWastageAmount + weavingWages");

				totalRate = totalRate + (double) Math
						.round(((costWarpYarnTemp + costWeftYarnTemp + totalRate + weftWastageAmountTemp + weavingWages)
								* (profitPercetage) / 100 + costWarpYarnTemp + costWeftYarnTemp + warpWastageAmountTemp
								+ weftWastageAmountTemp + weavingWages));
				totalRateTemp = totalRate;
				procurementCosting.setTotalRate(totalRate);
				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalRate);
			}
			if (procurementCosting.getWarpYarnRate() != null && procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWeftYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWarpYarnRequired() != null
					&& procurementCosting.getWarpWastagePercetnage() != null
					&& procurementCosting.getWarpYarnRate() != null && procurementCosting.getWeftYarnRequired() != null
					&& procurementCosting.getWeftWastagePercentage() != null
					&& procurementCosting.getWeftYarnRate() != null && weavingWages != null && profitPercetage != null
					&& procurementCosting.getNumberOfUnits() != null) {
				log.info(
						"<==== Inside ProcurementCostingBean.calculateFormula ratePerUnit =====  totalRate / NumberOfUnits ");
				ratePerUnit = ratePerUnit + (double) Math.round(totalRateTemp / procurementCosting.getNumberOfUnits());
				ratePerUnitTemp = ratePerUnit;
				procurementCosting.setRatePerUnit(ratePerUnit);
				procurementCosting.setWeavingWages((double) Math.round(weavingWages));
				procurementCosting.setProfitPercetage(profitPercetage);
				log.info("<==== END ProcurementCostingBean.calculateFormula ratePerUnit =====>" + totalRate);
			}
			if (preparatoryCharges != null && sticker != null && knotting != null) {
				log.info("<==== Inside ProcurementCostingBean.calculateFormula "
						+ " totalPurchasePrice =====  ratePerUnit + preparatoryCharges + sticker + knotting + box + polythin_cover");
				totalPurchasePrice = totalPurchasePrice + (double) Math
						.round(ratePerUnit + preparatoryCharges + sticker + knotting + box + polythin_cover);
				procurementCosting.setTotalPurchasePrice(totalPurchasePrice);
				procurementCosting.setPreparatoryCharges((double) Math.round(preparatoryCharges));
				procurementCosting.setSticker((double) Math.round(sticker));
				procurementCosting.setKnotting((double) Math.round(knotting));

				log.info("<==== END ProcurementCostingBean.calculateFormula totalRate =====>" + totalPurchasePrice);
			}

		} catch (Exception e) {
			log.error("Error Ajax Calculation --==>", e);
		}
		log.info("<==== Starts ProcurementCostingBean.calculateFormula =====>");

	}
	
	public void getAllQuotationProductSelectionList() {
		log.info("getProductSelectionList Method Start====>");
		try {
			BaseDTO baseDTO=new BaseDTO();
			String URL = serverURL +"/quotation/getAllQuotationProductSelection";
			baseDTO = httpService.get(URL);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				retailProductSelectionIdList = mapper.readValue(jsonResponse,
						new TypeReference<List<Long>>() {
						});
				
				log.info("retailProductSelectionIdList size="+retailProductSelectionIdList.size());
				List<ProductSelectionDTO> productSelectionDTOList=new ArrayList<ProductSelectionDTO>();
				for (ProductSelectionDTO productSelectionDTO : productSelectedPersonsList) {
					for (Long rpsId : retailProductSelectionIdList) {
						if(productSelectionDTO.getProductSelectionId().equals(rpsId)) {
							productSelectionDTOList.add(productSelectionDTO);
						}
						
					}
					
				}
				if(productSelectionDTOList!=null) {
				productSelectedPersonsList.removeAll(productSelectionDTOList);
				}
				log.info("remove List size="+productSelectedPersonsList.size());
				log.info("productSelectedPersonsList size="+productSelectedPersonsList.size());
			}
			
		}
		catch(Exception ex) {
			log.error("Error in getProductSelectionList Method ====",ex);
		}
		log.info("getProductSelectionList Method End====>");
	}
	
	public String redirectPreviewPage() {
		log.info("-------Start redirectPreviewPage method -------");
		if(productSelectionVaritiesDTOList==null || productSelectionVaritiesDTOList.isEmpty()) {
			AppUtil.addWarn("Add Product Details");
			return null;
		}
		if(gstPercentageWiseDTOList==null || gstPercentageWiseDTOList.isEmpty()) {
			AppUtil.addWarn("Add GST Summary Details");
			return null;
		}
		log.info("-------End redirectPreviewPage method -------");
		return PREVIEW_SUPPLY_RATE_CONFIRMATION_URL;
	}

}