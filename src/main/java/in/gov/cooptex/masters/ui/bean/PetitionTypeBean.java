package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("petitionTypeBean")
public class PetitionTypeBean {
	@Getter
	@Setter
	PetitionType petitionType, selectedPetitionType;

	@Getter
	@Setter
	LazyDataModel<PetitionType> lazyPetitionTypeList;

	List<PetitionType> petitionTypeList = null;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	String action;

	@Autowired
	AppPreference appPreference;

	final String SERVER_URL = AppUtil.getPortalServerURL();
	final String PETITION_TYPE_URL = AppUtil.getPortalServerURL() + "/petitiontype";

	ObjectMapper mapper;

	String jsonResponse;
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;
	/*
	 * @Getter
	 * 
	 * @Setter PetitionType selectedPetitionTypeMaster = new PetitionType();
	 */

	private final String CREATE_PAGE = "/pages/masters/petitionType/createPetitionType.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/petitionType/listPetitionType.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE = "/pages/masters/petitionType/viewPetitionType.xhtml?faces-redirect=true;";

	public String showPetitionListPageAction() {
		log.info("<===== Starts petitionType.showpetitionTypePageAction ===========>" + action);
		try {

			if (!action.equalsIgnoreCase("create")
					&& (selectedPetitionType == null || selectedPetitionType.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("Create")) {
				log.info("create petitionType called..");
				petitionType = new PetitionType();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit petitionType called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/petitiontype/get/"
						+ selectedPetitionType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					petitionType = mapper.readValue(jsonResponse, new TypeReference<PetitionType>() {
					});
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View petitionType called..");

				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/petitiontype/get/"
						+ selectedPetitionType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						petitionType = mapper.readValue(jsonResponse, new TypeReference<PetitionType>() {
						});
						return VIEW_PAGE;
					} else if (response != null && response.getStatusCode() != 0) {
						errorMap.notify(response.getStatusCode());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
					}
				}
			} else {
				log.info("delete petitionType called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPetitionTypeDelete').show()");
			}
		} catch (Exception e) {
			log.error("Exception occured in showpetitionTypeListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends petitionTypeBean.showpetitionTypeListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts PetitionTypeBean.deleteConfirm ===========>" + selectedPetitionType.getId());
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/petitiontype/deletebyid/" + selectedPetitionType.getId());
			// BaseDTO response =
			// httpService.delete(PETITION_TYPE_URL+"/deletebyid/"+selectedPetitionType.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPetitionTypeDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete petitionType....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends PetitionTypeBean.deleteConfirm ===========>");
		return showpetitionTypeListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts petitionTypeBean.saveOrUpdate =======>" + petitionType.getLocName());
		try {
			if (petitionType.getCode() == null || petitionType.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_CODE_EMPTY.getCode());
				return null;
			}
			if (petitionType.getName() == null || petitionType.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_NAME_EMPTY.getCode());
				return null;
			}
			if (petitionType.getLocName() == null || petitionType.getLocName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_LOC_NAME_EMPTY.getCode());
				return null;
			}

			if (petitionType.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_STATUS_EMPTY.getCode());
				return null;
			}

			if (petitionType.getCode().trim().length() < 3 || petitionType.getCode().trim().length() > 75) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_CODE_MIN_MAX_ERROR.getCode());
				return null;
			}
			if (petitionType.getName().trim().length() < 3 || petitionType.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}

			if (petitionType.getLocName().trim().length() < 3 || petitionType.getLocName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.PETITIONTYPE_LOC_NAME_MIN_MAX_ERROR.getCode());
				return null;
			}
//			
//			if (petitionType.getSlaTimeLimit()==null && petitionType.getSlaTimeLimit() > 0) {
//				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SLA_TIME_LIMIT_REQUIRED).getErrorCode());
//				return null;
//			}'
			
//			if (petitionType.getSlaTimeLimit() ==null) {
//				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SLA_TIME_LIMIT_REQUIRED).getErrorCode());
//				return null;
//			}

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/petitiontype/saveorupdate";
			BaseDTO response = httpService.post(url, petitionType);

			if (response != null && response.getStatusCode() == 0) {
				log.info("petitiontype saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.PETITIONTYPE_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.PETITIONTYPE_UPDATE_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends petitiontype.saveOrUpdate =======>");
		return showpetitionTypeListPage();
	}

	public String showpetitionTypeListPage() {
		log.info("<==== Starts petitiontype.showpetitiontypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		petitionType = new PetitionType();
		selectedPetitionType = new PetitionType();
		loadLazypetitionTypeList();
		log.info("<==== Ends petitiontypeBean.showpetitiontypeListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazypetitionTypeList() {
		log.info("<===== Starts petitiontypeBean.loadLazypetitionTypeList ======>");

		lazyPetitionTypeList = new LazyDataModel<PetitionType>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = -4894434595510290699L;

			@Override
			public List<PetitionType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/petitiontype/getallpetitiontypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						petitionTypeList = mapper.readValue(jsonResponse, new TypeReference<List<PetitionType>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("petitionTypeBean size---------------->" + lazyPetitionTypeList.getPageSize());
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return petitionTypeList;
			}

			@Override
			public Object getRowKey(PetitionType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PetitionType getRowData(String rowKey) {
				for (PetitionType petitionType : petitionTypeList) {
					if (petitionType.getId().equals(Long.valueOf(rowKey))) {
						selectedPetitionType = petitionType;
						return petitionType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends  petitionTypeBean.loadLazySalesTypeList  ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PetitionTypeBean.onRowSelect ========>" + event);
		selectedPetitionType = ((PetitionType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends PetitionTypeBean.onRowSelect ========>" + selectedPetitionType.getId());
	}

}
