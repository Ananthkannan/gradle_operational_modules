package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.KNTPlanDenominationMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("kntPlanDenominationMasterBean")
public class KNTPlanDenominationMasterBean {

	private final String CREATE_PAGE = "/pages/masters/createKNTPlanDenomination.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/masters/listKNTPlanDenomination.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/masters/viewKNTPlanDenomination.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String KNT_PLAN_DENOMINATION_URL = AppUtil.getPortalServerURL() + "/kntplandenominationmaster";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	KNTPlanDenominationMaster kntPlanDenomination;
	
	@Getter
	@Setter
	List<KNTPlanDenominationMaster> kntPlanDenominationList = new ArrayList<KNTPlanDenominationMaster>();
	
	@Getter
	@Setter
	List<KNTPlanDenominationMaster> allKntPlanDenominationList = new ArrayList<KNTPlanDenominationMaster>();

	@Getter
	@Setter
	KNTPlanDenominationMaster selectedKntPlanDenomination;

	@Getter
	@Setter
	LazyDataModel<KNTPlanDenominationMaster> lazyKntPlanDenominationList;
	

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	private String pageHead = "";
	@Autowired
	AppPreference appPreference;

	@Autowired
	LoginBean loginBean;

	public String showKntPlanDenominationListPage() {
		log.info("<==== Starts showKntPlanDenominationListPage Bean.showKntPlanDenominationListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		viewButtonFlag = true;
		kntPlanDenominationList = new ArrayList<>();
		kntPlanDenomination = new KNTPlanDenominationMaster();
		selectedKntPlanDenomination = new KNTPlanDenominationMaster();
		loadLazyKntPlanDenominationList();

		log.info("<==== Ends showKntPlanDenominationListPage Bean.showKntPlanDenominationListPage =====>");
		return LIST_PAGE;
	}

	

	public void loadLazyKntPlanDenominationList() {
		log.info("<===== Starts Knt Plan Denomination Bean.load AssetCategory List ======>");
		lazyKntPlanDenominationList = new LazyDataModel<KNTPlanDenominationMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<KNTPlanDenominationMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = KNT_PLAN_DENOMINATION_URL + "/list";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get asset category list");
					if (response != null && response.getStatusCode() == 0) {
						log.info("get asset category list" + response.getTotalRecords());
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						kntPlanDenominationList = mapper.readValue(jsonResponse,
								new TypeReference<List<KNTPlanDenominationMaster>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyKntPlanDenominationList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return kntPlanDenominationList;
			}

			@Override
			public Object getRowKey(KNTPlanDenominationMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public KNTPlanDenominationMaster getRowData(String rowKey) {
				try {
					for (KNTPlanDenominationMaster kntDenomination : kntPlanDenominationList) {
						if (kntDenomination.getId().equals(Long.valueOf(rowKey))) {
							selectedKntPlanDenomination = kntDenomination;
							return kntDenomination;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends Knt Plan Denomination Bean.loadLazykntPlanDenominationList List ======>");
	}

	public String showKntPlanDenominationPageAction() {
		log.info("<===== Starts Knt Plan Denomination Bean.showKntPlanDenominationListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE")
					&& (selectedKntPlanDenomination == null || selectedKntPlanDenomination.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create Knt Plan Denomination called..");
				statusButtonFlag = false;
				pageHead = "Add";
				kntPlanDenomination = new KNTPlanDenominationMaster();
				return CREATE_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit Knt Plan Denomination called..");
				statusButtonFlag = true;
				pageHead = "Edit";
				String url = KNT_PLAN_DENOMINATION_URL + "/get/" + selectedKntPlanDenomination.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					kntPlanDenomination = mapper.readValue(jsonResponse, new TypeReference<KNTPlanDenominationMaster>() {
					});
					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Knt Plan Denomination called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDeleteDlg').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view Knt Plan Denomination called..");
				String viewurl = KNT_PLAN_DENOMINATION_URL + "/get/" + selectedKntPlanDenomination.getId();
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					kntPlanDenomination = mapper.readValue(jsonResponse, new TypeReference<KNTPlanDenominationMaster>() {
					});
					return VIEW_PAGE;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showKntPlanDenominationListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends Knt Plan Denomination Bean.showKntPlanDenominationListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {
		log.info("<===== Starts Save Knt Plan Denomination Bean.saveOrUpdate ===========>");
		try {
			boolean valid=formValidate(true);
			log.info("<<--- Form Validation is........."+valid);
			if(valid==true) {
				if (!action.equalsIgnoreCase("CREATE")
						&& (selectedKntPlanDenomination == null || selectedKntPlanDenomination.getId() == null)) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
					return null;
				}
				if (action.equalsIgnoreCase("CREATE")) {
					kntPlanDenomination.setCreatedBy(loginBean.getUserDetailSession());
					kntPlanDenomination.setCreatedDate(new Date());
					if (kntPlanDenomination.getActiveStatus() == null) {
						kntPlanDenomination.setActiveStatus(true);
					}
				} else if (action.equalsIgnoreCase("EDIT")) {
					kntPlanDenomination.setModifiedBy(loginBean.getUserDetailSession());
					kntPlanDenomination.setModifiedDate(new Date());
					if (kntPlanDenomination.getActiveStatus() == null) {
						errorMap.notify(ErrorDescription.ACTIVE_STATUS_EMPTY.getCode());
						RequestContext.getCurrentInstance().update("growls");
						return null;
					}
				}
					try {
						String url = KNT_PLAN_DENOMINATION_URL + "/create";
						BaseDTO response = httpService.post(url, kntPlanDenomination);
						if (response != null && response.getStatusCode() == 0) {
							log.info("Knt Plan Denomination saved successfully..........");
							if (action.equalsIgnoreCase("CREATE"))
								errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_SAVE_SUCCESS).getErrorCode());
							else
								errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_UPDATE_SUCCESS).getErrorCode());
							
							return showKntPlanDenominationListPage();
						} else if (response != null && response.getStatusCode() != 0) {
							errorMap.notify(response.getStatusCode());
							return null;
						} else {
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
						}
					}catch(Exception e) {
						log.error("<<<--- Exception while saving time in knt-->");
					}
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends Knt Plan Denomination Bean.saveOrUpdate =======>");
		return null;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts Knt Plan Denomination Bean.onRowSelect ========>" + event);
		kntPlanDenomination = ((KNTPlanDenominationMaster) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends Knt Plan Denomination Bean.onRowSelect ========>");
	}
	
	public void getAllKntPlan() {
		log.info("<===== Starts Knt Plan Denomination Bean.getAll ===========>");
		try {
			log.info("<<-- select the id for getAll ---->");
			BaseDTO response = httpService.get(KNT_PLAN_DENOMINATION_URL + "/getAll");
			if (response != null && response.getStatusCode() == 0) {
				log.info("<<<--- successfully getting the knt plan denomination--->");
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					allKntPlanDenominationList = mapper.readValue(jsonResponse, new TypeReference<List<KNTPlanDenominationMaster>>() {
				});
					log.info("<<--- allKntPlanDenominationList size..."+allKntPlanDenominationList.size());
			} else {
				log.info("<<<=== some error on get All knt plan denominatino ===>>");
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Knt Plan Denomination ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends Knt Plan Denomination Bean.getAll ===========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts Knt Plan Denomination Bean.deleteConfirm ===========>");
		try {
			log.info("<<-- select the id for delete ---->"+selectedKntPlanDenomination.getId());
			BaseDTO response = httpService.delete(KNT_PLAN_DENOMINATION_URL + "/delete/" + selectedKntPlanDenomination.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDeleteDlg').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Knt Plan Denomination ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends Knt Plan Denomination Bean.deleteConfirm ===========>");
		return showKntPlanDenominationListPage();
	}
	
	

	public boolean formValidate(boolean validate) {
		log.info("<===== Starts Knt Plan Denomination Bean.FromValidation ===========>");
		validate=true;
		if(kntPlanDenomination==null) {
			log.info("<===== kntPlanDenomination Object is null ===========>");
			validate=false;
		}
		else if(kntPlanDenomination.getFromAmount()==null || kntPlanDenomination.getFromAmount()<=0) {
			log.info("<===== kntPlanDenomination From Amount is null ===========>");
			validate=false;
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_FROMAMT_REQUIRED).getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
		}
		else if(kntPlanDenomination.getToAmount()==null || kntPlanDenomination.getToAmount()<=0) {
			log.info("<===== kntPlanDenomination To Amount is null ===========>");
			validate=false;
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_TOAMT_REQUIRED).getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
		}
		else if(kntPlanDenomination.getDenomination()==null || kntPlanDenomination.getDenomination()<=0) {
			log.info("<===== kntPlanDenomination Denomination is null ===========>");
			validate=false;
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_DENOMAMT_REQUIRED).getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
		}
		else if(kntPlanDenomination.getFromAmount() >= kntPlanDenomination.getToAmount()) {
			log.info("<===== kntPlanDenomination To Amount is greater than to To Amount ===========>");
			validate=false;
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.TO_AMT_IS_GREATER_THAN_FROM_AMT).getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
		}
		else if(kntPlanDenomination.getFromAmount()!=null || kntPlanDenomination.getDenomination()>0) {
			getAllKntPlan();
			if(allKntPlanDenominationList!=null) {
				log.info("<<<---- allKntPlanDenominationList size()........"+allKntPlanDenominationList.size());
				
				for(KNTPlanDenominationMaster denom:allKntPlanDenominationList) {
					Double fromAmt=denom.getFromAmount();
					Double toAmt=denom.getToAmount();
					Double denomination=denom.getDenomination();
					Long id = denom.getId();
					if(kntPlanDenomination.getId()!=null && id!=null && action.equalsIgnoreCase("EDIT") && kntPlanDenomination.getId().equals(id)){
						log.info("<===== kntPlanDenomination Edit, and comming is same id so no need to validate ===========>");
					}else {
						if(kntPlanDenomination.getFromAmount().equals(fromAmt)) {
							log.info("<===== kntPlanDenomination From Amount is already exist ===========>");
							validate=false;
							errorMap.notify(ErrorDescription.getError(MastersErrorCode.FROM_AMT_IS_ALREADY_EXIST).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							break;
						}
						else if(kntPlanDenomination.getToAmount().equals(toAmt)) {
							log.info("<===== kntPlanDenomination To Amount is already exist ===========>");
							validate=false;
							errorMap.notify(ErrorDescription.getError(MastersErrorCode.TO_AMT_IS_ALREADY_EXIST).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							break;
						}
						else if(kntPlanDenomination.getDenomination().equals(denomination)) {
							log.info("<===== kntPlanDenomination Denomination is already exist ===========>");
							validate=false;
							errorMap.notify(ErrorDescription.getError(MastersErrorCode.DENOM_IS_ALREADY_EXIST).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							break;
						}
						else if((fromAmt >= kntPlanDenomination.getFromAmount() && fromAmt <= kntPlanDenomination.getToAmount() && toAmt >= kntPlanDenomination.getToAmount()) 
								|| (fromAmt <= kntPlanDenomination.getFromAmount() && toAmt >= kntPlanDenomination.getFromAmount())
								 || (fromAmt <= kntPlanDenomination.getToAmount() && toAmt >= kntPlanDenomination.getToAmount())) {
							log.info("<===== kntPlanDenomination From Amount is over lapped ===========>");
							validate=false;
							errorMap.notify(ErrorDescription.getError(MastersErrorCode.KNT_PLAN_DENOM_AMT_IS_OVERLAP).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							break;
						}
					}
				}
			}
		}
		return validate;
	}
}
