package in.gov.cooptex.operation.rest.ui;

import java.util.*;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;

import in.gov.cooptex.operation.dto.YarnRequirementDetailsListDataDTO;
import in.gov.cooptex.operation.enums.PetitionRegisterStatus;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.mobile.dto.SupplierDTO;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.dto.YarnRequirementDTO;
import in.gov.cooptex.operation.dto.YarnRequirementDetailDto;
import in.gov.cooptex.operation.dto.YarnRequirementDetailsListDataDTO;
import in.gov.cooptex.operation.enums.YarnRequirementStatus;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.YarnRequirement;
import in.gov.cooptex.operation.model.YarnRequirementDetails;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("yarnRequirementBean")
@Scope("session")
public class YarnRequirementBean {

	private static final String LIST_YARN_REQUIREMENT = "/pages/operation/yarn/listYarnRequirement.xhtml?faces-redirect=true;";

	private static final String CREATE_YARN_REQUIREMENT = "/pages/operation/yarn/createYarnRequirement.xhtml?faces-redirect=true;";

	private static final String VIEW_YARN_REQUIREMENT = "/pages/operation/yarn/viewYarnRequirement.xhtml?faces-redirect=true;";

	@Getter
	@Setter
	LazyDataModel<YarnRequirement> yarnRequirementLazyList;

	@Getter
	@Setter
	List<UserMaster> forwardToList;
	
	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	YarnRequirement yarnRequirement,selectedYarnRequirement;

	@Getter
	@Setter
	int size,totalRecords=0;

	@Getter
	@Setter
	String action;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	EntityMaster entityMaster;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<CircleMaster> circleMasterList;

	@Getter
	@Setter
	CircleMaster circleMaster;

	@Getter
	@Setter
	List<String> fromMonthYear;

	@Getter
	@Setter
	String fromDate,note;

	@Getter
	@Setter
	List<String> toMonthYear;

	@Getter
	@Setter
	String toDate;

	@Getter
	@Setter
	List<String> socityCodeNameList;

	ObjectMapper mapper;

	String jsonResponse;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true,approveStatus;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;
	
	
	@Getter
	@Setter
	List statusValues;
	
	@Getter
	@Setter
	List<YarnRequirementDetailsListDataDTO> dataList = new ArrayList<YarnRequirementDetailsListDataDTO>();

	@Getter
	@Setter
	List<String> productVarietyLst = new ArrayList<String>();

	/*@Getter
	@Setter
	LazyDataModel<YarnRequirementBean> yarnRequirementDetailsList;

	@Getter
	@Setter
	private List<YarnRequirementBean> beanList;
*/
	
	@Getter
	@Setter
	List<YarnRequirement> yarnRequirementList;
	
	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;
	
	@Getter
	@Setter
	List<SupplierDTO> supplierDtoList;
	
	@Getter
	@Setter
	private String societyCodeName;

	@Getter
	@Setter
	private String yarnUnitName;

	@Getter
	@Setter
	private String yarnValues;

	@Getter
	@Setter
	private long index;
	
	@Getter
	@Setter
	EmployeeMaster employee;
	
	String YARN_REQUIREMENT_URL="";
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<String> monthList;
	
	@Getter
	@Setter
	List<Long> fromYearList,toYearList;
	
	
	@Getter
	@Setter
	List<YarnRequirementDetailDto> yarnProductDtoList;
	
	@Getter
	@Setter
	List<ProductVarietyMaster> productList;
	
	@Getter
	@Setter
	List<YarnRequirementDetails> yarnRequirementDetailsList;
	
	@Getter
	@Setter
    List<Object> requirementDetailDtoList;

    @Setter
    @Getter
    private List yarnStatusValues;

	@Setter
	@Getter
	private Boolean isRowSelected = false;

	@Setter
	@Getter
	List<DetailsDto> detailsDto;

	@Setter
	@Getter
	ArrayList<String> headers;

	@Setter
	@Getter
	HashMap<String, Object> totalAmounts;

	@Setter
	@Getter
	YarnRequirementDTO approveOrRejectYarnRequirementDTOViewPage;
	
	@PostConstruct
    public void init() {
		log.info("init  method called...");
		employee = commonDataService.loadEmployeeByUser(loginBean.getUserDetailSession().getId());
		loadToYearList();
		loadFromYearList();
        loadStatus();
        showYarnRequirementListPage();
    }

    private void loadStatus() {
        log.info("<=== Starts yarnRequirementBean.loadStatus() ========>");
        yarnStatusValues = new ArrayList();
        Object[] statusArray = PetitionRegisterStatus.class.getEnumConstants();
        for (Object status : statusArray) {
            yarnStatusValues.add(status);
        }
        log.info("<=== Ends yarnRequirementBean.loadStatus() yarnStatusValues : " + yarnStatusValues);
    }


	public String showYarnRequirementListPage() {
		log.info("<==== Starts yarnRequirementBean-showYarnRequirementListPage =====>");
		if (!isRowSelected) {
			editButtonFlag = false;
			deleteButtonFlag = false;
			addButtonFlag = true;
			viewButtonFlag = false;
		}
		isRowSelected = false;

		mapper = new ObjectMapper();
		action = "";
		supplierMasterList = new ArrayList<>();
		yarnRequirementDetailsList = new ArrayList<>();
		yarnProductDtoList = new ArrayList<>();
		monthList = AppUtil.getAllMonthNames();
		selectedYarnRequirement = new YarnRequirement();
		circleMasterList = commonDataService.loadAllCircles();
		YARN_REQUIREMENT_URL = appPreference.getPortalServerURL()+appPreference.getOperationApiUrl()+"/yarnrequirement";
		loadYarnProductList();
		loadLazyYarnRequirementList();
		log.info("<==== Ends yarnRequirementBean-showYarnRequirementListPage =====>");
		return LIST_YARN_REQUIREMENT;
	}
	
	public void  loadLazyYarnRequirementList() {
		log.info("<===== Starts yarnRequirementBean.loadLazyYarnRequirementList ======>");

		yarnRequirementLazyList = new LazyDataModel<YarnRequirement>() {
			private static final long serialVersionUID = 5221554353084853593L;

			@Override
			public List<YarnRequirement> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = YARN_REQUIREMENT_URL+"/getlazyyarnrequirementlist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						yarnRequirementList = mapper.readValue(jsonResponse, new TypeReference<List<YarnRequirement>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return yarnRequirementList;
			}

			@Override
			public Object getRowKey(YarnRequirement res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public YarnRequirement getRowData(String rowKey) {
				for (YarnRequirement yarnRequirement : yarnRequirementList) {
					if (yarnRequirement.getId().equals(Long.valueOf(rowKey))) {
						selectedYarnRequirement = yarnRequirement;
						return yarnRequirement;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends yarnRequirementBean.loadLazyYarnRequirementList ======>");
	}


	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts yarnRequirementBean.onRowSelect ========>"+event);
		selectedYarnRequirement = ((YarnRequirement) event.getObject());
		if (!selectedYarnRequirement.getStatusForLog().equalsIgnoreCase("Approved")) {
			deleteButtonFlag = true;
		} else {
			deleteButtonFlag = false;
		}
		addButtonFlag=false;
		editButtonFlag=true;
		isRowSelected = true;
		viewButtonFlag = true;
		log.info("<===Ends yarnRequirementBean.onRowSelect ========>");
	}

	public String yarnRequirementListPageAction() {
		log.info("<========Starts yarnRequirementBean.yarnRequirementListPageAction=======>");
		try {
			if(!action.equalsIgnoreCase("create") && (selectedYarnRequirement == null || selectedYarnRequirement.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}
			
			if (action.equalsIgnoreCase("Create")) {
				log.info("create SocietyProductAppraisal called..");
				yarnRequirement = new YarnRequirement();
				yarnRequirement.setEntityMaster(employee.getPersonalInfoEmployment().getWorkLocation());
				loadForwardUser();
				dataList.clear();
				productVarietyLst.clear();
				approveStatus=null;
				forwardTo = new UserMaster();
				return CREATE_YARN_REQUIREMENT;
			} else if (action.equalsIgnoreCase("View") || action.equalsIgnoreCase("Edit")) {
				log.info("view/edit SocietyProductAppraisal called..");
				loadForwardUser();
				supplierMasterList = new ArrayList<>();
				String url = YARN_REQUIREMENT_URL+"/get/"+selectedYarnRequirement.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					yarnRequirement = mapper.readValue(jsonResponse,new TypeReference<YarnRequirement>(){
					});
					if(yarnRequirement != null) {
						for(YarnRequirementDetails d :yarnRequirement.getYarnRequirementDetails()) {
							supplierMasterList.add(d.getSupplierMaster());
						}
						yarnRequirementDetailsList = yarnRequirement.getYarnRequirementDetails();
						process(yarnRequirementDetailsList);
					}
					 
					if (action.equalsIgnoreCase("Edit"))
						return CREATE_YARN_REQUIREMENT;
					else
						return VIEW_YARN_REQUIREMENT;
					
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Society SocietyProductAppraisal called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmYarnRequirementDelete').show();");
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		log.info("<========Ends yarnRequirementBean.yarnRequirementListPageAction=======>");
		return null;
	}

	public String yarnRequirementViewPageAction() {
		log.info("<========Starts yarnRequirementBean.yarnRequirementListPageAction=======>");
		try {
			String url = YARN_REQUIREMENT_URL + "/get/" + selectedYarnRequirement.getId();
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				yarnRequirement = mapper.readValue(jsonResponse, new TypeReference<YarnRequirement>() {
				});
				if (yarnRequirement != null) {
					for (YarnRequirementDetails d : yarnRequirement.getYarnRequirementDetails()) {
						supplierMasterList.add(d.getSupplierMaster());
					}
					yarnRequirementDetailsList = yarnRequirement.getYarnRequirementDetails();

					return VIEW_YARN_REQUIREMENT;
				}

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		log.info("<========Ends yarnRequirementBean.yarnRequirementListPageAction=======>");
		return null;
	}

	private void process(List<YarnRequirementDetails> details) {
		detailsDto = new ArrayList<>();
		headers = new ArrayList<>();
		totalAmounts = new HashMap<>();
		approveOrRejectYarnRequirementDTOViewPage = new YarnRequirementDTO();
		approveOrRejectYarnRequirementDTOViewPage.setForwardTo(new UserMaster());
		HashSet<String> hashSets = new HashSet<>();

		String societyCodeNameKey = "Society Code / Name";
		String yarnUnitCodeNameKey = "Yarn Unit Code / Name";
		totalAmounts.put(yarnUnitCodeNameKey, "Total");

		HashMap<String, DetailsDto> tempMap = new HashMap<>();

		for (YarnRequirementDetails requirementDetails : details) {
			//adding unique headers
			String itemName = requirementDetails.getItem().getName();
			hashSets.add(itemName);
			if (totalAmounts.containsKey(itemName)) {
				totalAmounts.put(itemName, (Double) totalAmounts.get(itemName) + requirementDetails.getRequiredQuantity());
			} else {
				totalAmounts.put(itemName, requirementDetails.getRequiredQuantity());
			}

			//adding values for that headers

			String societyValueCode = requirementDetails.getSupplierMaster().getCode() + " / "
					+ requirementDetails.getSupplierMaster().getName();

			String yarnValueCode = requirementDetails.getEntityMaster().getCode() + " / "
					+ requirementDetails.getEntityMaster().getName();

			String uniqueRowKey = societyValueCode.concat(yarnValueCode);

			if (tempMap.containsKey(uniqueRowKey)) {
				DetailsDto dto = tempMap.get(uniqueRowKey);
				Double qty = (Double) dto.data(itemName);
				if (qty == null) {
					dto.setData(itemName, requirementDetails.getRequiredQuantity().toString());
				} else {
					dto.setData(itemName, (requirementDetails.getRequiredQuantity() + qty));
				}
			} else {
				DetailsDto dto = new DetailsDto();
				dto.setData(societyCodeNameKey, societyValueCode);
				dto.setData(yarnUnitCodeNameKey, yarnValueCode);
				dto.setData(itemName, requirementDetails.getRequiredQuantity().toString());
				tempMap.put(uniqueRowKey, dto);
			}
		}
		HashMap<String, Double> total = new HashMap<>();
		final String totalKeySuffix = "Total";
		for (Map.Entry<String, DetailsDto> entry : tempMap.entrySet()) {
			DetailsDto value = entry.getValue();
			detailsDto.add(value);
		}

		headers.add(societyCodeNameKey);
		headers.add(yarnUnitCodeNameKey);
		headers.addAll(hashSets);
	}
	
	public void generate() {
		log.info("<========Ends yarnRequirementBean.yarnRequirementListPageAction=======>");
		try {

			if(!checkAvailability()) {
				errorMap.notify(ErrorDescription.YARN_REQUIREMENT_ALREADY_PRESENT.getErrorCode());


            }else {

                log.info("circleMaster.getId() ......."+yarnRequirement.getCircleMaster().getId());
				String url = YARN_REQUIREMENT_URL+"/getRequirementTable/"+yarnRequirement.getCircleMaster().getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					dataList = mapper.readValue(jsonResponse,new TypeReference<List<YarnRequirementDetailsListDataDTO>>(){
                    });
					if(dataList!=null && dataList.size()>0)
						productVarietyLst = dataList.get(0).getProductVarietyNameList();
				}
			}



			/*yarnRequirementDetailsList = new ArrayList<>();
			log.info("circleMaster.getId() ......."+yarnRequirement.getCircleMaster().getId());
			String url = YARN_REQUIREMENT_URL+"/getallsupplymaster/"+yarnRequirement.getCircleMaster().getId();
			BaseDTO response = httpService.get(url);
			if(response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				log.info("response.getResponseContents() :::::"+response.getResponseContents());
				log.info("json Response :::::"+jsonResponse);
				supplierMasterList = mapper.readValue(jsonResponse,new TypeReference<List<SupplierMaster>>(){
				});	
				if(supplierMasterList != null)
					log.info("supplierMasterList size is "+supplierMasterList.size());
				
				for( SupplierMaster s: supplierMasterList) {
					for(ProductVarietyMaster p : productList) {
						YarnRequirementDetails details = new YarnRequirementDetails();
						details.setSupplierMaster(s);
						details.setItem(p);
						details.setDerviedQuantity(0.0);
						details.setRequiredQuantity(0.0);
						yarnRequirementDetailsList.add(details);
					}
				}
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
			}else {
				log.info("Response :::::::::"+response);
			}*/
		}catch(RestException re) {
			log.error("Exception occured in generate method.,.....",re);
			errorMap.notify(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in generate method.,.....",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<========Ends yarnRequirementBean.yarnRequirementListPageAction=======>");
	}
	
	public String saveOrUpdate() {
		log.info("<== Starts yarnRequirementBean.saveorupdate.===>");
		try {
			BaseDTO response = null;
			
			log.info("<<======  map:::: "+dataList);


            if(dataList==null || dataList.size()<1) {
				errorMap.notify(ErrorDescription.YARN_REQUIREMENT_IS_EMPTY.getErrorCode());
				return null;
			}
			if(forwardTo==null || forwardTo.getId()<1) {
				errorMap.notify(ErrorDescription.FORWARD_TO_USER_EMPTY.getErrorCode());
				return null;
			}
			if(note.isEmpty()) {
				errorMap.notify(ErrorDescription.NOTE_IS_EMPTY.getErrorCode());
				return null;
			}
			if(approveStatus==null) {
				errorMap.notify(ErrorDescription.FINAL_APPROVAL_EMPTY.getErrorCode());
				return null;
			}

            YarnRequirementDTO request = new YarnRequirementDTO();
			request.setCircleMaster(circleMaster);
			request.setFromMonth(yarnRequirement.getFromMonth());
			request.setFromYear(yarnRequirement.getFromYear());
			request.setToMonth(yarnRequirement.getToMonth());
			request.setToYear(yarnRequirement.getToYear());
			request.setCircleMaster(yarnRequirement.getCircleMaster());
			request.setDnpOffice(yarnRequirement.getEntityMaster());
			request.setNote(note);
			request.setForwardTo(forwardTo);
			request.setFinalApproval(approveStatus);
			request.setDetailsList(dataList);
			log.info("<<=====  REQUEST::: "+request);
			String url = YARN_REQUIREMENT_URL+"/create";
			response = httpService.post(url,request);
			if(response != null && response.getStatusCode() == 0) {
				log.info("<<========  CREATED SUCCESS FULLY");
				errorMap.notify(ErrorDescription.YARN_REQUIREMENT_CREATE_SUCCESS.getErrorCode());
				return showYarnRequirementListPage();
			}



			/*Validate.objectNotNull(societyProductAppraisal.getSociety(), ErrorDescription.SOCIETY_REQUIRED);
			Validate.objectNotNull(societyProductAppraisal.getProductVariety(), ErrorDescription.PRODUCT_REQUIRED);
			Validate.objectNotNull(societyProductAppraisal.getWeaversName(), ErrorDescription.WEAVERS_NAME_REQUIRED);
			Validate.objectNotNull(societyProductAppraisal.getAppraisedBy(), ErrorDescription.APPRAISAL_BY_REQUIRED);
			Validate.objectNotNull(societyProductAppraisal.getProductCost(), ErrorDescription.PRODUCT_COST_REQUIRED);*/
			
			/*String url = YARN_REQUIREMENT_URL+"/saveorupdate";
			yarnRequirement.setYarnRequirementDetails(yarnRequirementDetailsList);
			if(action.equalsIgnoreCase("Create")) {
				response = httpService.post(url, yarnRequirement);
				if(response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_CREATE_SUCCESS.getCode());						
					return showYarnRequirementListPage();
				}else if(response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}else {
					log.error("Response............"+response);
				}
			}else if(action.equalsIgnoreCase("Edit")) {
				response = httpService.post(url, yarnRequirement);
				if(response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_UPDATE_SUCCESS.getCode());						
					return showYarnRequirementListPage();
				}else if(response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}else {
					log.error("Response............"+response);
				}
			}*/
		}catch(RestException restException) {
			errorMap.notify(restException.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in saveorupdate method...........",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<== Ends yarnRequirementBean.saveorupdate.===>");
		return null;
	}
	
	
	public String gotoCreatePage() {
		fromDate = null;
		toDate = null;
		circleMaster = null;
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		log.info("Logged in User Employee Details " + employeeMaster.getFirstName() + " / "
				+ employeeMaster.getLastName());

		entityMaster = commonDataService.getEntityInfoByLoggedInUser();
		log.info("Logged in User Entity Details " + entityMaster.getCode() + " / " + entityMaster.getName());

		circleMasterList = commonDataService.loadCircles();

		fromMonthYear = AppUtil.prepareFromMonthYearAndToMonthYear(1);

		toMonthYear = AppUtil.prepareFromMonthYearAndToMonthYear(11);

		return CREATE_YARN_REQUIREMENT;
	}

	public void clear() {
		fromDate = null;
		toDate = null;
		circleMaster = null;
	}

	public String gotoViewPage() {

		return VIEW_YARN_REQUIREMENT;
	}

	/*public String getList() {
		log.info("Inside getList() ");

		loadStatusValues();
		loadLazyYarnRequirementList();

		return LIST_YARN_REQUIREMENT;
	}*/

	private void loadStatusValues() {
		Object[] statusArray = YarnRequirementStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status.toString());
		}

		log.info("<--- statusValues ---> " + statusValues);
	}

	public void onPageLoad() {
		log.info("Yarn Requirement onPageLoad method started");
		addButtonFlag = false;
	}


    public void yarnCalculation(ValueChangeEvent event) {
		log.info("new value---------" + event.getNewValue());

		
	}
	
	public void loadYarnProductList() {
		log.info("<========Ends yarnRequirementBean.loadYarnProductList=======>");
		try {
			String url = YARN_REQUIREMENT_URL+"/getallyarnproductvarietymaster";
			BaseDTO response = httpService.get(url);
			yarnRequirementDetailsList = new ArrayList<>();
			yarnProductDtoList.clear();
			if(response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productList = mapper.readValue(jsonResponse,new TypeReference<List<ProductVarietyMaster>>(){
				});	
				if(productList != null)
					log.info("productList size is "+productList.size());
				for(ProductVarietyMaster p : productList) {
					YarnRequirementDetails details = new YarnRequirementDetails();
					details.setItem(p);
					details.setDerviedQuantity(0.0);
					yarnRequirementDetailsList.add(details);
				}
				
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
			}else {
				log.info("Response :::::::::"+response);
			}
		}catch(RestException re) {
			log.error("Exception occured in loadYarnProductList method.,.....",re);
			errorMap.notify(re.getStatusCode());
		}catch(Exception e) {
			log.error("Exception occured in loadYarnProductList method.,.....",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<========Ends yarnRequirementBean.loadYarnProductList=======>");
	}
		
	
	
	public List<Long> loadFromYearList(){
		log.info("loadFromYearList list called..");
		fromYearList = new ArrayList<>();
		Long year = (long) Calendar.getInstance().get(Calendar.YEAR);
		log.info("Year ::"+year);
		for(int i=0;i<3;i++) {
			fromYearList.add(year-i);	
		}
		Collections.sort(fromYearList);
		return fromYearList; 
	}
	
	public List<Long> loadToYearList(){
		log.info("loadToYearList list called..");
		toYearList = new ArrayList<>();
		Long year = (long) Calendar.getInstance().get(Calendar.YEAR);
		log.info("Year ::"+year);
		for(int i=0;i<2;i++) {
			toYearList.add(year+i);	
		}
		return toYearList; 
	}

	
	/*public void formYarnRequirementDetailDto() {
		log.info("<=== Starts formYarnRequirementDetailDto===>");
		Map<String, List<YarnRequirementDetails>> requirementDetails = salesInvoiceItemsList.stream()
				.collect(Collectors.groupingBy(s -> s.getSalesInvoiceItem().getProductVarietyMaster().getHsnCode()));
		List<YarnRequirementDetailDto> requirementDetailsList = new ArrayList<>();
		for()
		log.info("<=== Ends formYarnRequirementDetailDto===>");
		
	}*/
	
	public void loadForwardUser() {
		log.info("<<===============    yarnRequirementBean ---   doForward  =====##STARTS");
//		forwardToList = commonDataService.loadForwardToUsers();
		forwardToList = commonDataService.loadForwardToUsersByFeature("YARN_REQUIREMENT");
		log.info("<<===============    yarnRequirementBean ---   doForward  =====##ENDS");
	}
	
	public void updateTable() {
		log.info("Starts updateTable ......");
		log.info("Ends updateTable ......");
	}

    public Boolean checkAvailability() {
		log.info("<<=======  yarnRequirementBean -----  checkAvailability  =======##STARTS");
		String url = YARN_REQUIREMENT_URL+"/checkForAvailibility";
		BaseDTO response = httpService.post(url,yarnRequirement);
		if(response != null && response.getStatusCode() == 0) {
			log.info("<<========  Available::: ");
			return true;
		}

        log.info("<<=======  yarnRequirementBean -----  checkAvailability  =======##ENDS");
		return false;
	}

    public void clearNote() {
		log.info("<<======  CLEAR NOTE ::: ");
		note="";
	}

	public void selectApproveOrRejected(String status) {
		log.info("<=== Starts YarnRequirementBean.submitApprovalOrRejection =======>");
		approveOrRejectYarnRequirementDTOViewPage.setStage(status);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlgComments1').show();");
		log.info("<=== Ends YarnRequirementBean.submitApprovalOrRejection =======>");
	}

	public String submitApprovalOrRejection() {
		log.info("<=== Starts YarnRequirementBean.submitApprovalOrRejection data : "
				+ approveOrRejectYarnRequirementDTOViewPage);
		String result = null;
		try {
			approveOrRejectYarnRequirementDTOViewPage.setDnpOfficeId(selectedYarnRequirement.getId());

			BaseDTO response = httpService.post(YARN_REQUIREMENT_URL + "/approvalorrejection",
					approveOrRejectYarnRequirementDTOViewPage);
			if (response.getStatusCode() == 0) {
				if (approveOrRejectYarnRequirementDTOViewPage.getStage().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.YARN_REQUIREMENT_APPROVED_SUCCESSFULLY.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription.YARN_REQUIREMENT_REJECTED_SUCCESSFULLY.getErrorCode());
				}
				nullifyingViewYarnRequirementObject();
				result = LIST_YARN_REQUIREMENT;
			} else {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			e.printStackTrace();
			log.info("<<==  Error in Saving YarnRequirementBean " + e.getMessage());
		}
		log.info("<=== Ends YarnRequirementBean.submitApprovalOrRejection() ======>");
		return result;
	}

	public String cancelViewYarnRequirement() {
		log.info("<=== Starts YarnRequirementBean.cancelViewYarnRequirement");
		String result = "listYarnRequirement.xhtml";
		nullifyingViewYarnRequirementObject();
		log.info("<=== Ends YarnRequirementBean.cancelViewYarnRequirement() ======>");
		return result;
	}

	private void nullifyingViewYarnRequirementObject() {
		log.info("<=== Starts YarnRequirementBean.nullifyingViewYarnRequirementObject() ======>");
		//nullifying objctes
		detailsDto = null;
		headers = null;
		totalAmounts = null;
		approveOrRejectYarnRequirementDTOViewPage = null;
		log.info("<=== Ends YarnRequirementBean.nullifyingViewYarnRequirementObject() ======>");
	}

	public class DetailsDto {
		private Map<String, Object> data = new HashMap<>();

		public void setData(String key, Object value) {
			this.data.put(key, value);
		}

		public Object data(String key) {
			if (this.data.containsKey(key)) {
				return data.get(key);
			} else {
				return 0.0;
			}
		}
	}
}
