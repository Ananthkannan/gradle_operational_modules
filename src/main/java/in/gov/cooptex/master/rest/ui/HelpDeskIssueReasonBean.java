package in.gov.cooptex.master.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.HelpDeskIssueReasonDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class HelpDeskIssueReasonBean {
	private final String INPUT_FORM_LIST_URL = "/pages/masters/helpDeskIssueReason/listHelpDeskIssueReason.xhtml?faces-redirect=true;";
	private final String CREATE_HELPDESK_ISSUE_REASON_PAGE = "/pages/masters/helpDeskIssueReason/createHelpDeskIssueReason.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/masters/helpDeskIssueReason/viewHelpDeskIssueReason.xhtml?faces-redirect=true;";
	private String serverURL = null;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	private int helpdeskissueListSize;

	boolean forceLogin = false;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	LazyDataModel<HelpDeskIssueReasonDTO> helpdeskissueLazyList = null;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	HelpDeskIssueReasonDTO helpDeskIssueReasonDTO = new HelpDeskIssueReasonDTO();

	@PostConstruct
	public void init() {
		log.info(" helpDeskIssueReasonBean init --->" + serverURL);
		serverURL = appPreference.getPortalServerURL();
		// addhelptask();
		loadLazyList();
	}

	public String addhelptask() {
		log.info("<---- Start helpDeskIssueReasonBean.addhelptask ---->");
		try {
			action = "ADD";
			helpDeskIssueReasonDTO = new HelpDeskIssueReasonDTO();
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		log.info("<---- End helpDeskIssueReasonBean.addhelptask ---->");
		return CREATE_HELPDESK_ISSUE_REASON_PAGE;
	}

	public String showList() {
		log.info("<---------- Loading helpDeskIssueReasonBean showList list page---------->");
		return INPUT_FORM_LIST_URL;
	}

	public boolean validation() {
		boolean  valid =true;

		if(helpDeskIssueReasonDTO.getCode()!=null  && helpDeskIssueReasonDTO.getCode().length()>0){
			if(helpDeskIssueReasonDTO.getCode().length()>=2 ) {
				
			}else {
				AppUtil.addError("Code minimumm 2 characters");
				return false;
			}
		}
		
		if(helpDeskIssueReasonDTO.getName()!=null  && helpDeskIssueReasonDTO.getName().length()>0){
			if(helpDeskIssueReasonDTO.getName().length()>=2 ) {
				
			}else {
				AppUtil.addError("Code minimumm 2 characters");
				return false;
			}
		}
		
		if(helpDeskIssueReasonDTO.getLhelpDeshIssueName()!=null  && helpDeskIssueReasonDTO.getLhelpDeshIssueName().length()>0){
			if(helpDeskIssueReasonDTO.getLhelpDeshIssueName().length()>=2 ) {
				
			}else {
				AppUtil.addError("Local Name (in Tamil) minimumm 2 characters");
				return false;
			}
		}

		
		return valid;
	}
	
	public String submit() {
		log.info("...... helpDeskIssueReasonBean submit begin ....");
		try {
			if(validation()){

			log.info("object::" + helpDeskIssueReasonDTO.getCode());
			BaseDTO baseDTO = null;
			String url = serverURL + "/helpDeskIssueReason/createhelpdeskissuereason";
			log.info("...... Url::" + url);
			baseDTO = httpService.post(url, helpDeskIssueReasonDTO);

			if (baseDTO != null) {
				if (baseDTO.getMessage()!=null && baseDTO.getMessage().equalsIgnoreCase("SUCCESS")) {
					log.info("helpDeskIssueReasonBean saved successfully");
					showList();

					if (action == "ADD") {
						// AppUtil.addInfo("Helpdesk Issue Reason Created Successfully");
						errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_REASON_ADDED_SUCCESSFULLY);
					} else {
						// AppUtil.addInfo("Helpdesk Issue Reason Updated Successfully");
						errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_REASON_UPDATED_SUCCESSFULLY);
					}
					clear();
					return INPUT_FORM_LIST_URL;

				} else if (baseDTO.getStatusCode() == 223445842) {
					errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_CODE_ALREADY_EXISTS);
					return null;
				} else if (baseDTO.getStatusCode() == 223445843) {
					errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_NAME_ALREADY_EXISTS);
					return null;
				} else if (baseDTO.getStatusCode() == 223445844) {
					errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_LOCAL_NAME_ALREADY_EXISTS);
					return null;
				} else {
					log.error("Help Desk Issue Reason code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
		}
		} catch (Exception e) {
			log.error("Error while creating helpDeskIssueReason" + e);
		}
		log.info("...... helpDeskIssueReasonBean submit ended ....");
		
		return null;
	}

	public String clear() {
		log.info("<---- Start helpDeskIssueReasonBean . clear ----->");
		helpDeskIssueReasonDTO = new HelpDeskIssueReasonDTO();
		log.info("<---- End helpDeskIssueReasonBean . clear ----->");

		return INPUT_FORM_LIST_URL;
	}

	public void loadLazyList() {
		helpdeskissueLazyList = new LazyDataModel<HelpDeskIssueReasonDTO>() {

			private static final long serialVersionUID = -5731311643435459480L;

			@Override
			public List<HelpDeskIssueReasonDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<HelpDeskIssueReasonDTO> data = new ArrayList<HelpDeskIssueReasonDTO>();

				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<HelpDeskIssueReasonDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(HelpDeskIssueReasonDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public HelpDeskIssueReasonDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<HelpDeskIssueReasonDTO> responseList = (List<HelpDeskIssueReasonDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (HelpDeskIssueReasonDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	protected BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO basadtoObj = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		HelpDeskIssueReasonDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = appPreference.getPortalServerURL() + "/helpDeskIssueReason/list";

		basadtoObj = httpService.post(URL, request);

		return basadtoObj;
	}

	private HelpDeskIssueReasonDTO getSearchRequestObject(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		HelpDeskIssueReasonDTO request = new HelpDeskIssueReasonDTO();
		try {
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();

				if (entry.getKey().equals("code")) {
					log.info("code : " + value);
					request.setCode(value);
				}

				if (entry.getKey().equals("name")) {
					log.info("name : " + value);
					request.setName(value);
				}

				if (entry.getKey().equals("lhelpDeshIssueName")) {
					log.info("lhelpDeshIssueName : " + value);
					request.setLhelpDeshIssueName(value);
				}
				if (entry.getKey().equals("createdDate")) {
					/*
					 * DateFormat dateFormat = new SimpleDateFormat("yyyy-MMM-dd"); String strDate =
					 * dateFormat.format(value);
					 */
					log.info("createdDate : " + value);
					// Date date1 = new SimpleDateFormat("dd/MMM/yyyy").parse(value);
					request.setCreatedDate((Date) entry.getValue());
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return request;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		helpDeskIssueReasonDTO = ((HelpDeskIssueReasonDTO) event.getObject());

	}

	public String backHelpDeskIssueReason() {
		showList();
		return INPUT_FORM_LIST_URL;
	}

	public String editHelpDeskIssueReasonn() {
		log.info("<<<<< -------Start editHelpDeskIssueReason called ------->>>>>> ");
		action = "EDIT";
		if (helpDeskIssueReasonDTO == null) {
			Util.addWarn("Please select one Help Desk Issue Reason");
			return null;
		}

		log.info("<<<<< -------End editHelpDeskIssueReason called ------->>>>>> ");

		return CREATE_HELPDESK_ISSUE_REASON_PAGE;

	}

	public String viewHelpDeskIssueReason() {
		action = "View";

		log.info("<----Redirecting to user view page---->");
		if (helpDeskIssueReasonDTO == null) {
			Util.addWarn("Please Select One Help Desk Issue Reason");
			return null;
		}
		return INPUT_FORM_VIEW_URL;

	}

	public String deleteHelpDeskIssueReason() {
		try {
			action = "Delete";
			if (helpDeskIssueReasonDTO == null) {
				Util.addWarn("Please Select One Help Desk Issue Reason");
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmHelpDeskIssueReasonDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteHelpDeskIssueReasonConfirm() {

		try {
			log.info("Delete confirmed for Help Desk Issue Reason [" + helpDeskIssueReasonDTO.getName() + "]");
			HelpDeskIssueReasonDTO helpDeskIssuersnDelete = new HelpDeskIssueReasonDTO();
			helpDeskIssuersnDelete.setId(helpDeskIssueReasonDTO.getId());
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/helpDeskIssueReason/delete";
			baseDTO = httpService.post(url, helpDeskIssueReasonDTO);
			showList();
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Help Desk Issue Reason deleted successfully....!!!");
					errorMap.notify(MastersErrorCode.HELP_DESK_ISSUE_REASON_DELETED_SUCCESSFULLY);
					// AppUtil.addInfo("Helpdesk Issue Reason Deleted Successfully");
					helpDeskIssueReasonDTO = new HelpDeskIssueReasonDTO();
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}

	
	public String clearHelpDeskIssueReason() {
		log.info("<---- Start helpDeskIssueReasonBean . clear ----->");
		helpDeskIssueReasonDTO = new HelpDeskIssueReasonDTO();
		log.info("<---- End helpDeskIssueReasonBean . clear ----->");

		return INPUT_FORM_LIST_URL;

	}

}
