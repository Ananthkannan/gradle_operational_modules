/**
 * 
 */
package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.GroupDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AllowanceGrade;
import in.gov.cooptex.core.model.AllowanceGradeCity;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.hrms.dto.GradewiseCityConfigDto;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author user
 *
 */

@Log4j2
@Scope("session")
@Service("gradewiseCityConfigBean")
public class GradewiseCityConfigBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private final String LIST_GRADEWISE_CITY_CONFIG_PAGE = "/pages/masters/listGradewiseCityConfig.xhtml?faces-redirect=true;";
	private final String CREATE_GRADEWISE_CITY_CONFIG_PAGE = "/pages/masters/createGradewiseCityConfig.xhtml?faces-redirect=true;";
	private final String VIEW_GRADEWISE_CITY_CONFIG_PAGE = "/pages/masters/viewGradewiseCityConfig.xhtml?faces-redirect=true;";
	
	
	final String SERVER_URL = AppUtil.getPortalServerURL();
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	String action;
	
	@Getter
	@Setter
	GradewiseCityConfigDto gradewiseCityConfigDto;
	
	@Getter
	@Setter
	String cityNames;
	
	@Getter
	@Setter
	int totalRecords = 0;
	
	@Getter
	@Setter
	AllowanceGradeCity selectedGradeWiseCity;
	
	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;
	
	@Getter
	@Setter
	Boolean editFlagForCitySingleSelect = true;
	
	@Getter
	@Setter
	Boolean editFlagForCityMultiSelect = true;
	
	
	@Getter
	@Setter
	List<AllowanceGrade> allowanceGradeNameList;
	
	@Getter
	@Setter
	List<CityMaster> cityList;
	
	@Getter
	@Setter
	List<AllowanceGradeCity> allowanceGradeCityList;
	
	@Getter
	@Setter
	AllowanceGradeCity allowanceGradeCity;
	
	
	@Getter
	@Setter
	LazyDataModel<AllowanceGradeCity> allowanceGradeCityLazyList;
	
	@PostConstruct
	private void init() {
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		clear();	
		loadLazyGradewiseCityConfigList();	
	}
	
	public void clear() {
		allowanceGradeNameList = new ArrayList<AllowanceGrade>();
		cityList = new ArrayList<CityMaster>();
		allowanceGradeCity = new AllowanceGradeCity();
		selectedGradeWiseCity = new AllowanceGradeCity();
		
	}
	
	public String showGradewiseCityConfigListPage() {
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		allowanceGradeCity = new AllowanceGradeCity();
		selectedGradeWiseCity = new AllowanceGradeCity();
		loadLazyGradewiseCityConfigList();	
		return LIST_GRADEWISE_CITY_CONFIG_PAGE;
	}
	
	public String gradeWiseCityConfigListPageAction() {
		try {
			if (action.equalsIgnoreCase("Create")) {
				log.info("called add gradewise city page");
				clear();
				fetchGradeList();	
				fetchCityList();
				gradewiseCityConfigDto = new GradewiseCityConfigDto();
				editFlagForCitySingleSelect = false;
				editFlagForCityMultiSelect = true;
				return CREATE_GRADEWISE_CITY_CONFIG_PAGE;
				
			} else if (action.equalsIgnoreCase("View")) {
				log.info("called view gradewise city  page");
				editFlagForCitySingleSelect = false;
				editFlagForCityMultiSelect = false;
				editGradewiseCityConfig();
				return VIEW_GRADEWISE_CITY_CONFIG_PAGE;
				
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("called Edit gradewise city page");
				fetchGradeList();	
				fetchCityList();
				editGradewiseCityConfig();
				editFlagForCitySingleSelect = true;
				editFlagForCityMultiSelect = false;
				return CREATE_GRADEWISE_CITY_CONFIG_PAGE;			

			} else if (action.equalsIgnoreCase("Delete")) {
				log.info("delete gradewise city called.." + allowanceGradeCity.getId());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmGradewiseCityDelete').show();");
				
			} else if (action.equalsIgnoreCase("Clear")) {
				log.info("called Clear Quotation page");
				return showGradewiseCityConfigListPage();
				
			}
		} catch (Exception e) {
			log.error(" Exception at quotationListPageAction " + e);
		}

		return null;
	}
	
	/*** GradewiseCityConfig OnRow Select Here ***/
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts AllowanceGradeCity.onRowSelect ========>" + event);
		selectedGradeWiseCity = ((AllowanceGradeCity) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		log.info("<===Ends AllowanceGradeCity.onRowSelect ========>");
	}

	
	public void fetchGradeList() {
			
		try {
			allowanceGradeNameList = new ArrayList<AllowanceGrade>();
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/gradeWiseCityConfig/getAllGradeName";
			log.info("<-Inside GradewiseCityConfigBean starts fetchGradeList method to fetch grade name from allowance_grade table->" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				allowanceGradeNameList = mapper.readValue(jsonResponse, new TypeReference<List<AllowanceGrade>>() {
				});
			
			}
		}
		catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Inside GradewiseCityConfigBean Exception occured in fetchGradeList method ", e);
		}
		
	}
	
	public void fetchCityList() {
		
		try {
			cityList = new ArrayList<CityMaster>();
			cityNames = "";
			BaseDTO baseDTO = new BaseDTO();
			String url = SERVER_URL + "/gradeWiseCityConfig/getAllCity";
			log.info("<-Inside GradewiseCityConfigBean starts fetchCityList method to fetch city name from city_master table->" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				cityList = mapper.readValue(jsonResponse, new TypeReference<List<CityMaster>>() {
				});
			}

		/*if(cityList!=null){
			for(CityMaster city:cityList ){
				if (cityNames != "") {
					cityNames = cityNames + ",";
				}
				cityNames = cityNames + city.getName();
			}
		}*/
		}
		catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Inside GradewiseCityConfigBean Exception occured in fetchCityList method ", e);
		}
		
	}
	
	public String saveGradewiseCity() {
		
		BaseDTO response = new BaseDTO();
		
		try {
		String url = SERVER_URL + "/gradeWiseCityConfig/saveGradewiseCity";
		log.info(" Inside GradewiseCityConfigBean starts saveGradewiseCity method" + url);
		response = httpService.post(url, gradewiseCityConfigDto);
		
		if (response != null && response.getStatusCode() == 0) {
			
			if(action.equalsIgnoreCase("Create")) {
				log.info(" Inside GradewiseCityConfigBean ==> Save Successfull");
				errorMap.notify(ErrorDescription.GRADEWISE_CITY_CONFIG_SAVE_SUCCESS.getErrorCode());
			}else {
				log.info(" Inside GradewiseCityConfigBean ==> Update Successfull");
				errorMap.notify(ErrorDescription.GRADEWISE_CITY_CONFIG_UPDATE_SUCCESS.getErrorCode());
			}
		}else if (response != null && response.getStatusCode() != 0 && response.getStatusCode() != ErrorDescription.CITY_CODE_ALREADY_EXIST_DUPICATECITY.getErrorCode()){
			if(action.equalsIgnoreCase("Create")) {			
				log.info(" Inside GradewiseCityConfigBean ==> Save Unsuccessfull");
				errorMap.notify(ErrorDescription.CITY_CODE_ALREADY_EXIST_DUPICATECITY.getErrorCode());
			}else {
				log.info(" Inside GradewiseCityConfigBean ==> Update Unsuccessfull");
				errorMap.notify(ErrorDescription.CITY_CODE_ALREADY_EXIST_DUPICATECITY.getErrorCode());
			}
			return null;
		}else {
			log.info(" Inside GradewiseCityConfigBean ==> Save failed");
			errorMap.notify(response.getStatusCode());
			return null;
		}
				
	}catch (Exception e) {
		log.error("Error at saveOrUpdateGradewiseCityConfig " + e);
		response.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
	}
		return showGradewiseCityConfigListPage();
	}
	
	
	public void loadLazyGradewiseCityConfigList() {
		
		log.info("Inside GradewiseCityConfigBean starts loadLazyGradewiseCityConfigList() method");
		String GRADEWISE_CITY_LIST_URL = SERVER_URL + "/gradeWiseCityConfig/getallGradewiseCitylistlazy";

		allowanceGradeCityLazyList = new LazyDataModel<AllowanceGradeCity>() {

			
			private static final long serialVersionUID = 6709377140398085375L;

			@Override
			public List<AllowanceGradeCity> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					log.info("==========URL for AllowanceGradeCity List=============>" + GRADEWISE_CITY_LIST_URL);
					BaseDTO response = httpService.post(GRADEWISE_CITY_LIST_URL, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					allowanceGradeCityList = mapper.readValue(jsonResponse, new TypeReference<List<AllowanceGradeCity>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyGradewiseCityConfigList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return allowanceGradeCityList;
			}

			@Override
			public Object getRowKey(AllowanceGradeCity res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public AllowanceGradeCity getRowData(String rowKey) {
				try {
					for (AllowanceGradeCity allowanceGradeCityObj : allowanceGradeCityList) {
						if (allowanceGradeCityObj.getId().equals(Long.valueOf(rowKey))) {
							allowanceGradeCity = allowanceGradeCityObj;
							return allowanceGradeCityObj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends GradewiseCityConfigBean.loadLazyGradewiseCityConfigList ======>");
	}
	
	public void editGradewiseCityConfig() {
		
		try {
			log.info("Inside GradewiseCityConfigBean starts editGradewiseCityConfig method");
			String url = SERVER_URL + "/gradeWiseCityConfig/allowanceGradeCityId/" + selectedGradeWiseCity.getId();
			log.info("editGradewiseCityConfig by allowance grade city table id ==> URL ====>>>  " + url);
			BaseDTO baseDTO = httpService.get(url);
			allowanceGradeCity = new AllowanceGradeCity();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				allowanceGradeCity = mapper.readValue(jsonResponse, new TypeReference<AllowanceGradeCity>() {
				});
				if(allowanceGradeCity != null) {
					gradewiseCityConfigDto = new GradewiseCityConfigDto();
					gradewiseCityConfigDto.setAllowanceGradeCity(allowanceGradeCity);
				}
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		}catch (Exception e) {
			log.error(" Error Occured In editGradewiseCityConfig() method ", e);
		}
	}
	
	public String deleteGradewiseCityConfig() {
		
		Long allowanceGradeCityId = null;
		allowanceGradeCityId = selectedGradeWiseCity.getId();
		try {
			if(allowanceGradeCityId != null) {
			log.info("Inside GradewiseCityConfigBean starts deleteGradewiseCityConfig method" + allowanceGradeCityId);
			BaseDTO response = httpService.delete(SERVER_URL + "/gradeWiseCityConfig/deleteByAllowancegradeCityId/" + allowanceGradeCityId);
			if (response != null && response.getStatusCode() == 0) {
				log.info(" AllowanceGradeCity Details deleted successfully");
				errorMap.notify(ErrorDescription.ALLOWANCE_GRADE_CITY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmQuotationDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting allowance grade city", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends GradewiseCityConfigBean.deleteGradewiseCityConfig =====>");
		return showGradewiseCityConfigListPage();
	}
	
	
}
