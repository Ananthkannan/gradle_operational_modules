package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.UserType;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TaxCalculationUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.OrderStatus;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.PurchaseOrderType;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.DnpDetailsDto;
import in.gov.cooptex.operation.production.dto.PurchaseOrderSearch;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("purchaseOrderBean")
@Scope("session")
public class PurchaseOrderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String PURCHASE_ORDER_LIST = "/pages/operation/retailproduction/purchase/listOrderForm.xhtml?faces-redirect=true;";

	private final String PURCHASE_ORDER_CREATE = "/pages/operation/retailproduction/purchase/createPurchaseOrder.xhtml?faces-redirect=true;";

	private final String PURCHASE_ORDER_PREVIEW = "/pages/operation/retailproduction/purchase/previewOrderForm.xhtml?faces-redirect=true;";

	private final String PURCHASE_ORDER_VIEW = "/pages/operation/retailproduction/purchase/viewPurchaseOrder.xhtml?faces-redirect=true;";

	// the below constants using for product warehouse started
	private final String PRODUCT_WAREHOUSE_STOCK_INWARD_LIST = "/pages/productWarehouse/listStockInwardPWH.xhtml?faces-redirect=true;";

	public String getProductWarehouseStockInwardList() {
		return PRODUCT_WAREHOUSE_STOCK_INWARD_LIST;
	}

	private final String PRODUCT_WAREHOUSE_STOCK_INWARD_CREATE = "/pages/productWarehouse/createStockInwardPWH.xhtml?faces-redirect=true;";

	public String getProductWarehouseStockInwardCreate() {

		return PRODUCT_WAREHOUSE_STOCK_INWARD_CREATE;
	}

	private final String PRODUCT_WAREHOUSE_QR_CODE_LIST = "/pages/productWarehouse/listQrCodeGeneration.xhtml?faces-redirect=true;";

	public String getProductWarehouseQrCodeList() {

		return PRODUCT_WAREHOUSE_QR_CODE_LIST;
	}

	private final String PRODUCT_WAREHOUSE_QR_CODE_CREATE = "/pages/productWarehouse/createQrCodeGeneration.xhtml?faces-redirect=true;";

	public String getProductWarehouseQrCodeCreate() {

		return PRODUCT_WAREHOUSE_QR_CODE_CREATE;
	}

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST = "/pages/productWarehouse/listStockOutwardPWH.xhtml?faces-redirect=true;";

	public String getProductWarehouseStockOutwardList() {

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

	private final String PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE = "/pages/productWarehouse/createStockOutwardPWH.xhtml?faces-redirect=true;";

	public String getProductWarehouseStockOutwardCreate() {

		return PRODUCT_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	private final String DISTRIBUTION_WAREHOUSE_STOCK_INWARD_CREATE = "/pages/distributionWarehouse/createStockInwardDWH.xhtml?faces-redirect=true;";

	public String getDistributionWarehouseStockInwardCreate() {

		return DISTRIBUTION_WAREHOUSE_STOCK_INWARD_CREATE;
	}

	private final String DISTRIBUTION_WAREHOUSE_STOCK_OUTWARD_LIST = "/pages/distributionWarehouse/listStockOutwardDWH.xhtml?faces-redirect=true;";

	public String getDistributionWarehouseStockOutwardList() {

		return DISTRIBUTION_WAREHOUSE_STOCK_OUTWARD_LIST;
	}

	private final String DISTRIBUTION_WAREHOUSE_STOCK_OUTWARD_CREATE = "/pages/distributionWarehouse/createStockOutwardDWH.xhtml?faces-redirect=true;";

	public String getDistributionWarehouseStockOutwardCreate() {

		return DISTRIBUTION_WAREHOUSE_STOCK_OUTWARD_CREATE;
	}

	private final String WEAVERS_SOCIETY_INVOCE_LIST = "/pages/weavers/listSocietyInvoice.xhtml?faces-redirect=true;";

	public String getWeaversSocietyInvoiceList() {

		return WEAVERS_SOCIETY_INVOCE_LIST;
	}

	private final String WEAVERS_SOCIETY_INVOCE_CREATE = "/pages/weavers/createSocietyInvoice.xhtml?faces-redirect=true;";

	public String getWeaversSocietyInvoiceCreate() {

		return WEAVERS_SOCIETY_INVOCE_CREATE;
	}

	private final String WEAVERS_SOCIETY_INVOCE_PREVIEW = "/pages/weavers/previewInvoice.xhtml?faces-redirect=true;";

	public String getWeaversSocietyInvoicePreview() {
		return WEAVERS_SOCIETY_INVOCE_PREVIEW;
	}
	// the above constants using for product warehouse ends here

	@Setter
	@Getter
	StreamedContent pdfFile;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String activeStatus;

	@Getter
	@Setter
	Set<SupplierMaster> societyUnderdnpList = new HashSet<>();

	@Getter
	@Setter
	PurchaseOrderSearch purchaseOrderSearch;

	@Getter
	@Setter
	PurchaseOrder purchaseOrder;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	LazyDataModel<PurchaseOrderSearch> purchaseOrderSearchLazyList;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	List<Quotation> quotationList;

	@Getter
	@Setter
	String[] societyAddress;

	@Getter
	@Setter
	String[] warehouseAddress;

	@Getter
	@Setter
	String[] headOfficeAddress;

	@Getter
	@Setter
	List<EntityMaster> warehouseList;

	@Getter
	@Setter
	List<PurchaseOrderType> purchaseOrderTypeList;

	@Getter
	@Setter
	List<PurchaseOrderItems> orderItemsList;

	@Getter
	@Setter
	List<PurchaseOrderItems> selectedOrderItemsList;

	@Getter
	@Setter
	Double materialValue;

	@Getter
	@Setter
	String[] dnpAddress;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Double totalCgstAmount;

	@Getter
	@Setter
	Double totalSgstAmount;

	@Getter
	@Setter
	Double totalTaxAmount;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	boolean purchaseOrderItemEditable;

	@Getter
	@Setter
	String dataToggle = "collapse in";
	
	@Getter
	@Setter
	private Date validDateLimit;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		getPurchaseOrderListPage();
		// Purchase Order Items Editable flag : will come from application.properties
		// file
		purchaseOrderItemEditable = appPreference.getPurchaseOrderItemEditable();
		loadStatusValues();
	}

	public PurchaseOrderBean() {
		log.info("<#======Inside PurchaseOrderBean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	private void loadStatusValues() {

		Object[] statusArray = OrderStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public void loadLazyPurchaseOrderList() {
		addButtonFlag = false;
		log.info("PurchaseOrderBean.loadLazyPurchaseOrderList method started");
		purchaseOrderSearchLazyList = new LazyDataModel<PurchaseOrderSearch>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<PurchaseOrderSearch> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<PurchaseOrderSearch> data = new ArrayList<PurchaseOrderSearch>();
				try {

					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if(baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						data = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrderSearch>>() {
						});
						if (data != null) {
							this.setRowCount(baseDTO.getTotalRecords());
							size = baseDTO.getTotalRecords();
						}
						if(baseDTO.getStatusCode() != 0) {
							errorMap.notify(baseDTO.getStatusCode());
						}
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(PurchaseOrderSearch res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PurchaseOrderSearch getRowData(String rowKey) {
				List<PurchaseOrderSearch> responseList = (List<PurchaseOrderSearch>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (PurchaseOrderSearch res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	private PurchaseOrderSearch getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {

		PurchaseOrderSearch request = new PurchaseOrderSearch();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("poNumber")) {
				log.info("PoNumber : " + value);
				request.setPoNumber(value);
			}

			if (entry.getKey().equals("planCodeOrName")) {
				log.info("plan Code Or Name : " + value);
				request.setPlanCodeOrName(value);
			}
			if (entry.getKey().equals("procurementOrderNumber")) {
				log.info("Procurement Order Number : " + value);
				request.setProcurementOrderNumber(value);
			}
			if (entry.getKey().equals("societyCodeOrName")) {
				log.info("SocietyCode Or Name : " + value);
				request.setSocietyCodeOrName(value);
			}
			if (entry.getKey().equals("supplyRateConfirmationNo")) {
				log.info("supplyRateConfirmationNo : " + value);
				request.setSupplyRateConfirmationNo(value);
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		log.info("PurchaseOrderBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		purchaseOrderSearch = new PurchaseOrderSearch();
		
		PurchaseOrderSearch request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("PurchaseOrderBean.getSearchData Method Complted");
		return baseDTO;
	}

	public void processDelete() {
		log.info("PurchaseOrderBean.processDelete method started");
		if (purchaseOrderSearch == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(24001);
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		log.info("PurchaseOrderBean.delete method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(24001);
			return null;
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/delete/"
				+ purchaseOrderSearch.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete draft plan", exp);
			errorMap.notify(1);
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(24002);
			return getPurchaseOrderListPage();
		} else {
			errorMap.notify(baseDTO.getStatusCode());
			return getPurchaseOrderListPage();
		}

	}

	public String getPurchaseOrderListPage() {
		purchaseOrderSearch = new PurchaseOrderSearch();
		loadLazyPurchaseOrderList();
		return PURCHASE_ORDER_LIST;
	}

	public String getCreatePage() {
		action = "CREATE";
		dataToggle = "collapse in";
		purchaseOrder = new PurchaseOrder();
		orderItemsList = new ArrayList<>();
		quotationList = new ArrayList<>();
		societyAddress = null;
		clearTableData();
		DnpDetailsDto dnpDetailsDto = loadBasicData();
		if (dnpDetailsDto != null) {
			// dnpOffice
			purchaseOrder.setEntityMaster(dnpDetailsDto.getDnpOffice());
			dnpAddress = AppUtil.prepareAddress(dnpDetailsDto.getDnpOffice().getAddressMaster());

			// society List Under dnp
			societyUnderdnpList = new HashSet<>(dnpDetailsDto.getSocietyList());

			// WareHouse List Under dnp
			warehouseList = dnpDetailsDto.getWarehouseList();

			// HeadOffice
			EntityMaster headOffice = dnpDetailsDto.getHeadOffice();
			if (headOffice != null) {
				purchaseOrder.setBillingEntityMaster(dnpDetailsDto.getHeadOffice());
				headOfficeAddress = AppUtil.prepareAddress(headOffice.getAddressMaster());
			}
		}
		return PURCHASE_ORDER_CREATE;
	}

	public void changeWarehouse(EntityMaster warehouse) {
		warehouseAddress = null;
		if (warehouse != null && warehouse.getId() != null) {
			warehouseAddress = AppUtil.prepareAddress(warehouse.getAddressMaster());
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("PurchaseOrderBean.onRowSelect method started");
		purchaseOrderSearch = ((PurchaseOrderSearch) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("PurchaseOrderBean.onPageLoad method started");
		addButtonFlag = false;
	}

	public void changeSociety(SupplierMaster society) {
		societyAddress = null;
		if (society != null) {
			purchaseOrder.setSupplierMaster(society);
			societyAddress = AppUtil.prepareAddress(society.getAddressMaster());
			if(societyAddress==null) {
				society.setAddressMaster(society.getAddressId());
				societyAddress=AppUtil.prepareAddress(society.getAddressMaster());
			}
			quotationList = commonDataService.getQuotationListForSociety(society.getId());
			purchaseOrder.setQuotation(null);
			gstPercentageWiseDTOList = new ArrayList<>();
			orderItemsList = new ArrayList<>();
			selectedOrderItemsList = new ArrayList<>();
			gstWiseList = new ArrayList<>();
			log.info("PurchaseOrderBean.changeSociety method started");
		} else {
			log.warn("Society is empty");
		}
	}

	public void generate(Quotation quotation) {
		dataToggle = "collapse in";
		if (quotation == null || quotation.getId() == null) {
			return;
		}
		log.info("PurchaseOrderBean.changeQuotation method started :" + quotation.getQuotationNumber());
		purchaseOrder.setQuotation(quotation);
		ItemDataWithGstDTO quotationItemWithGstDTO = commonDataService.getAllOrderItemsByQuotation(quotation.getId());
		orderItemsList = new ArrayList<>();
		if (quotationItemWithGstDTO != null) {
			gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
			orderItemsList = quotationItemWithGstDTO.getPurchaseOrderItemList();
			gstWiseList = quotationItemWithGstDTO.getGstWiseList();
			if (orderItemsList != null && !orderItemsList.isEmpty()) {
				dataToggle = "collapse";
			}
		}
		selectedOrderItemsList.addAll(orderItemsList);
		
		calculateTotalValues(orderItemsList);
		log.info("PurchaseOrderBean.changeQuotation method completed");
	}

	public void clearTableData() {
		selectedOrderItemsList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		totalCgstAmount = 0.0;
		totalSgstAmount = 0.0;
		totalTaxAmount = 0.0;
		materialValue = 0.0;
		//societyAddress = null;
		warehouseAddress = null;
	}

	public String previewPurchaseOrder() {
		if (orderItemsList == null || orderItemsList.size() == 0) {
			errorMap.notify(24006);
			return null;
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/preview";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Purchase Order preview successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in preview Purchase Order", exp);
			errorMap.notify(1);
			return null;
		}

		if (selectedOrderItemsList != null && selectedOrderItemsList.size() > 0) {
			purchaseOrder.setPurchaseOrderItemsList(selectedOrderItemsList);
		} else {
			purchaseOrder.setPurchaseOrderItemsList(orderItemsList);
		}
		//calculateTax(purchaseOrder.getPurchaseOrderItemsList());
		try {
			amountInWords = AmountToWordConverter.converter(
					String.valueOf(AppUtil.ifNullRetunZero(materialValue) + AppUtil.ifNullRetunZero(totalTaxAmount)));
		} catch (Exception e) {
			log.error("Error while amountInWords", e);
			errorMap.notify(1);
			return null;
		}
		return PURCHASE_ORDER_PREVIEW;
	}

	public DnpDetailsDto loadBasicData() {
		DnpDetailsDto dnpDetailsDto = commonDataService.getDnpDetails();

		if (dnpDetailsDto == null) {
			log.info("D & P data is not Availiable");
			return null;
		}
		// society List Under dnp
		societyUnderdnpList = new HashSet<>(dnpDetailsDto.getSocietyList());

		// WareHouse List Under dnp
		warehouseList = dnpDetailsDto.getWarehouseList();

		// Purchase Order Type List
		purchaseOrderTypeList = commonDataService.getPurchaseOrderTypeList();

		return dnpDetailsDto;
	}

	public void get() {
		log.info("PurchaseOrderBean.getPurchaseOrder method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(24011);
		}
		String url = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/get/"
				+ purchaseOrderSearch.getId();

		BaseDTO baseDTO = null;

		try {
			baseDTO = httpService.get(url);
		} catch (Exception e) {
			log.error("Error in viewPlan", e);
			errorMap.notify(1);
		}

		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);
			} catch (IOException e) {
				log.error(e);
			}
		}
		if (purchaseOrder == null) {
			log.info("#==--> PurchaseOrder is Not Available <--==#");
			errorMap.notify(9612);
		} else {
			quotationList = commonDataService.getQuotationListForSociety(purchaseOrder.getSupplierMaster().getId());
			orderItemsList = purchaseOrder.getPurchaseOrderItemsList();
			selectedOrderItemsList = orderItemsList;
			calculateTax(purchaseOrder.getPurchaseOrderItemsList());
		}

	}

	public String getEditPage() {
		action = "EDIT";
		orderItemsList = new ArrayList<>();
		dataToggle = "collapse in";
		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		if (!purchaseOrderSearch.getStatus().equals(OrderStatus.INITIATED.name())) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_CANNOT_EDIT.getCode());
			return null;
		}
		get();
		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		loadBasicData();
		if (purchaseOrder.getEntityMaster() != null) {
			dnpAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getDeliveryEntityMaster() != null) {
			warehouseAddress = AppUtil.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}
		return PURCHASE_ORDER_CREATE;
	}

	public String getViewPage() {
		action = "VIEW";
		orderItemsList = new ArrayList<>();
		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		get();
		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		if (purchaseOrder.getEntityMaster() != null) {
			dnpAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getDeliveryEntityMaster() != null) {
			warehouseAddress = AppUtil.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}
		return PURCHASE_ORDER_VIEW;
	}

	public String savePurchaseOrder(String requestType) {
		log.info("PurchaseOrderBean.savePurchaseOrder method started");
		if (requestType.equals(OrderStatus.INITIATED.name())) {
			purchaseOrder.setStatus(OrderStatus.INITIATED);
		} else {
			purchaseOrder.setStatus(OrderStatus.SUBMITTED);
		}
		purchaseOrder.setMaterialValue(materialValue);
		purchaseOrder.setTotalValue(materialValue);
		purchaseOrder.setNetValue(materialValue);

		String url = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/create";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
		} catch (Exception exp) {
			log.error("Error in Purchase Order create ", exp);
			// Error : JSF1095: The response was already committed by the
			// time we tried to set the outgoing cookie for the flash.
			// Any values stored to the flash will not be available on the
			// next request.
			// Note : Plan is updated successfully ,so that we are
			// redirecting to list page with success message
			log.info(
					"Purchase Order is created successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(24010);
			return PURCHASE_ORDER_LIST;
		}

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Purchase Order saved successfully");
				errorMap.notify(24010);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}
		return PURCHASE_ORDER_LIST;
	}

	public void changeUnit(List<PurchaseOrderItems> itemsList) {
		for (PurchaseOrderItems item : itemsList) {
			if (item.getItemQty() != null && item.getUnitRate() != null) {
				item.setItemAmount(item.getItemQty() * item.getUnitRate());
				Double taxValue = item.getItemAmount() * (item.getTaxPercent().doubleValue() / 100);
				item.setTaxValue(taxValue);
				item.setItemTotal(taxValue + item.getItemAmount());
			}
		}
		calculateTax(itemsList);
	}

	public void calculateTax(List<PurchaseOrderItems> itemsList) {
		if (itemsList != null && itemsList.size() > 0) {

			/*List<GstSummeryDTO> gstList = new ArrayList<>();
			for (PurchaseOrderItems item : itemsList) {
				if (item.getProductVarietyMaster() != null) {
					StringBuilder productId = new StringBuilder(item.getProductVarietyMaster().getId().toString());
					gstList.addAll(commonDataService.loadTaxForProduct(productId,
							AppUtil.ifNullRetunZero(item.getItemAmount())));
				}
			}*/
			String url = serverURL + appPreference.getOperationApiUrl();
			ItemDataWithGstDTO quotationItemWithGstDTO = new ItemDataWithGstDTO();
			quotationItemWithGstDTO = commonDataService.getTaxDetails(itemsList,url);
			gstWiseList = quotationItemWithGstDTO.getGstWiseList();
			gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();

			calculateTotalValues(itemsList);
		}
	}

	public void calculateTotalValues(List<PurchaseOrderItems> itemsList) {
		materialValue = TaxCalculationUtil.calculatePurchaseOrderMaterialValue(itemsList);
		totalCgstAmount = TaxCalculationUtil.calculateTotalCgstAmount(gstPercentageWiseDTOList);
		totalSgstAmount = TaxCalculationUtil.calculateTotalSgstAmount(gstPercentageWiseDTOList);
		totalTaxAmount = TaxCalculationUtil.calculateTotalTaxAmount(gstPercentageWiseDTOList);
	}

	/**
	 * This method is generating the PDF based on the search result.
	 */

	public void generatePDF() {

		log.info("Generate PDF started....");

		String URL = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/pdf/download";

		try {

			PurchaseOrderSearch request = getSearchRequestObject(null, null, sortingField, sortingOrder, filtersMap);

			log.info("Generate PDF [" + request + "]");

			InputStream inputStream = httpService.generatePDF(URL, request);

			pdfFile = new DefaultStreamedContent(inputStream, "application/pdf", "PurchaseOrderList.pdf");

			log.info("Generate PDF finished successfully....");

		} catch (Exception e) {
			log.error("Exception ", e);
		}

	}

	public void clearItemData() {
		orderItemsList = new ArrayList<>();
		clearTableData();
		Long quotationId = null;
		String url = "";
		BaseDTO baseDTO = null;
		validDateLimit = null;
		try {
			if(purchaseOrder.getQuotation() != null && purchaseOrder.getQuotation().getId() != null) {
				quotationId = purchaseOrder.getQuotation().getId();
				log.info("quotationId------------"+quotationId);
				url = serverURL + appPreference.getOperationApiUrl() + "/purchase/order/checkValidDate/"+quotationId;
				baseDTO = httpService.get(url);
				if(baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					validDateLimit = mapper.readValue(jsonValue, Date.class);
				}
				log.info("valid date------------"+validDateLimit);
				if(validDateLimit == null) {
					validDateLimit = new Date();
				}
			}
		}catch (Exception e) {
			log.error("clearItemData getvalidate date exception----",e);
		}
	}

}