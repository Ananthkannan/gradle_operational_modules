package in.gov.cooptex.master.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.BusinessTypeMaster;
import in.gov.cooptex.core.model.CircleMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.LoomTypeMaster;
import in.gov.cooptex.core.model.Nationality;
import in.gov.cooptex.core.model.OrganizationTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.model.SupplierTypeMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("supplierMasterBean")
public class SupplierMasterBean {

	private final String CREATE_SUPPLIER_MASTER = "/pages/masters/createSupplierMaster.xhtml?faces-redirect=true;";

	private final String LIST_SUPPLIER_MASTER = "/pages/masters/listSupplierMaster.xhtml?faces-redirect=true;";

	private final String VIEW_SUPPLIER_MASTER = "/pages/masters/viewSupplierMaster.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;
	ObjectMapper mapper;

	String jsonResponse;
	@Autowired
	AddressMasterBean addressMasterBean = new AddressMasterBean();

	@Getter
	@Setter
	private String action;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	SupplierMaster supplierMaster, selectedSupplierMaster;

	/*
	 * @Getter @Setter List<SupplierMaster> supplierMasterList = new
	 * ArrayList<SupplierMaster>();
	 */

	@Getter
	@Setter
	private int supplierMasterListSize;

	boolean forceLogin = false;

	@Getter
	@Setter
	List<AddressMaster> addressMasterList = new ArrayList<AddressMaster>();

	@Getter
	@Setter
	private int addressMasterListSize;

	@Getter
	@Setter
	AddressMaster addressMaster = new AddressMaster();

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	List<DistrictMaster> districtList;

	@Getter
	@Setter
	List<TalukMaster> talukList;

	@Getter
	@Setter
	List<CityMaster> cityList;

	@Getter
	@Setter
	List<AreaMaster> areaList;

	@Getter
	@Setter
	private String address = "";

	@Getter
	@Setter
	private int stateMasterListSize;

	@Getter
	@Setter
	private int districtListSize;

	@Getter
	@Setter
	private int talukListSize;
	@Setter
	@Getter
	List<OrganizationTypeMaster> organizationtypemasterList;
	@Getter
	@Setter
	private int cityListSize;

	@Getter
	@Setter
	private int areaListSize;

	@Getter
	@Setter
	private int bankMasterListSize;

	@Getter
	@Setter
	List<SupplierTypeMaster> supplierTypeMasterList;

	@Getter
	@Setter
	SupplierTypeMaster supplierTypeMaster;
	
	@Getter
	@Setter
	private List<CountryMaster> listCountry;

	@Getter
	@Setter
	List<BusinessTypeMaster> businessTypeMasterList;

	@Getter
	@Setter
	List<LoomTypeMaster> loonTypeList;

	@Getter
	@Setter
	List<CircleMaster> circleList;

	@Getter
	@Setter
	List<DistrictMaster> districList;

	@Getter
	@Setter
	BusinessTypeMaster businessTypeMaster;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<BankMaster> bankMasterList;

	@Getter
	@Setter
	BankMaster bankMaster;
	
	@Getter
	@Setter
	CircleMaster circleMaster;
	
	@Getter
	@Setter
	List<Nationality> nationalityList;

	@Getter
	@Setter
	List<BankBranchMaster> branchList;

	@Getter
	@Setter
	BankBranchMaster branchMaster;

	@Getter
	@Setter
	String ifscCode;

	@Getter
	@Setter
	LazyDataModel<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Autowired
	LanguageBean languageBean;

	public String gotoCreateSupplierForm() {
		log.info("<---- Inside gotoCreateSupplierForm ---->");
		addressMasterBean.setAddressMaster(new AddressMaster());

		supplierMaster = new SupplierMaster();
		
		
		supplierTypeMasterList = commonDataService.loadSupplierTypeMaster();

		businessTypeMasterList = commonDataService.loadBusinessTypeList();
		circleList=commonDataService.loadCircles();
		districList=commonDataService.getDistrictMasterList();

		bankMasterList = commonDataService.getBankDetails();
		listCountry=commonDataService.getCountryMasterList();

		stateList = commonDataService.loadStateList();
		nationalityList = commonDataService.getAllNationality();
		loadAllOrganizationTypeMaster();
		getLoomType();

		bankMaster = null;

		ifscCode = null;
		addressMasterBean.setAddressMaster(new AddressMaster());
		address = "";
		return CREATE_SUPPLIER_MASTER;
	}

	
	
	public void processBranches() {
		log.info("<---- Inside processBranches ---->");

		if (bankMaster != null) {

			Long bankId = bankMaster.getId();
			log.info("<---- Inside bankId ----> " + bankId);

			branchList = commonDataService.getBranchDetails(bankId);
			if (branchList != null) {
				log.info("<---- branchList ----> " + branchList.size());
			}

		} else {
			branchList = new ArrayList<>();
			branchMaster = new BankBranchMaster();
			ifscCode = null;
		}
	}

	public void updateIfscCode() {
		log.info("<---- Inside updateIfscCode ----> ");

		if (supplierMaster.getBranchMaster() != null) {
			ifscCode = supplierMaster.getBranchMaster().getIfscCode();
		} else {
			ifscCode = null;
		}
	}

	public String cancel() {
		log.info("<----cancel---->");
		supplierMaster = new SupplierMaster();
		addressMaster = new AddressMaster();

		loadLazySupplierList();
		listCountry=new ArrayList<>();
		return LIST_SUPPLIER_MASTER;

	}

	public String showList() {
		log.info("<--- Loading supplierMasterBean showList list page --->");

		loadLazySupplierList();

		return LIST_SUPPLIER_MASTER;
	}

	public List<LoomTypeMaster> getLoomType() {

		BaseDTO baseDTO = null;
		try {
			String URL = appPreference.getPortalServerURL() + "/loomtype/getActiveLoomType/";
			baseDTO = httpService.get(URL);
			mapper = new ObjectMapper();
			if (baseDTO != null) {
				loonTypeList = new ArrayList<LoomTypeMaster>();
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				loonTypeList = mapper.readValue(jsonResponse, new TypeReference<List<LoomTypeMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception in getallshowroomforregion ", e);
		}
		return loonTypeList;
	}

	public void loadLazySupplierList() {
		log.info("<--- loadLazySupplierList ---> ");

		supplierMasterList = new LazyDataModel<SupplierMaster>() {

			private static final long serialVersionUID = -1540942419672760421L;

			@Override
			public List<SupplierMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<SupplierMaster> data = new ArrayList<SupplierMaster>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<SupplierMaster>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazySupplierList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(SupplierMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SupplierMaster getRowData(String rowKey) {
				List<SupplierMaster> responseList = (List<SupplierMaster>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (SupplierMaster res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						supplierMaster = res;
						return res;
					}
				}
				return null;
			}

		};
	}
	
	

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		supplierMaster = new SupplierMaster();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		SupplierMaster supplierMaster = new SupplierMaster();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		supplierMaster.setPaginationDTO(paginationDTO);

		log.info("supplierMaster  " + supplierMaster);
		supplierMaster.setFilters(filters);

		try {
			String supplierMasterSearchUrl = appPreference.getPortalServerURL() + "/supplier/master/search";
			log.info("Supplier Master Search Url " + supplierMasterSearchUrl);
			baseDTO = httpService.post(supplierMasterSearchUrl, supplierMaster);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("FDS Plan onRowSelect method started");
		supplierMaster = ((SupplierMaster) event.getObject());
		addButtonFlag = true;
	}

	public void onPageLoad() {
		log.info("FDS Plan onPageLoad method started");
		addButtonFlag = false;
	}

	public String submitSupplierMaster() {
		log.info("...... supplierMasterBean submit begin ....");
		BaseDTO baseDTO = null;
		try {

			log.info("GSTIN number=====>" + supplierMaster.getGstNumber());
			// log.info("address state gstnumber=====>"
			// + supplierMaster.getAddressMaster().getStateMaster().getGstStateCode());

			if (checkValidation(true)) {

				log.info("gstinNumber is correct");
				
				
				baseDTO = httpService.post(appPreference.getPortalServerURL() + "/supplier/master/create",
						supplierMaster);
					
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("supplierMasterBean saved successfully");
						AppUtil.addInfo("Supplier Master saved successfully");
						return showList();
					} else {
						log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
								+ baseDTO.getErrorDescription());

						if (baseDTO.getStatusCode()
								.equals(MastersErrorCode.SUPPLIER_MASTER_GST_NO_SHOULD_NOT_BE_DUPLICATE)) {
							AppUtil.addWarn("GST No Should not be Duplicate");
							return null;
						}else if(baseDTO.getStatusCode().equals(ErrorDescription.SUPPLIER_MASTER_CODE_SHOULD_NOT_BE_DUPLICATE.getErrorCode())) {
							AppUtil.addWarn("Supplier Code  Should not be Duplicate");
							return null;
						}
						
						else {
							AppUtil.addWarn("Unable to create supplier.");
							return null;
						}

					}
				} else {
					AppUtil.addWarn("Invalid response from server.");
					return null;
				}
				
			}

		} catch (Exception e) {
			log.error("Error while creating supplierMaster" + e);
		}
		return null;
	}

	public boolean checkValidation(boolean valid) {

		try {
			
			if (supplierMaster.getGstNumber().length() >= 15) {

				// String subString = supplierMaster.getGstNumber().substring(0,
				// supplierMaster.getAddressMaster().getStateMaster().getGstStateCode().length());
				String gstinNumber = supplierMaster.getGstNumber();
				String alphabets = gstinNumber.substring(2, 7);
				String numeric = gstinNumber.substring(7, 11);
				String oneAlphabets = String.valueOf(gstinNumber.charAt(11));
				String oneAlphanum = String.valueOf(gstinNumber.charAt(12));
				String oneAlphabets1 = String.valueOf(gstinNumber.charAt(13));
				String oneAlphanum1 = String.valueOf(gstinNumber.charAt(14));

				// if (!Pattern.matches("[0-9]{1,2}", subString)) {
				// AppUtil.addWarn("Enter Valid GST");
				// return false;
				// }

				if (!Pattern.matches("[a-zA-Z]{1,5}", alphabets)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				if (!Pattern.matches("[0-9]{1,4}", numeric)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				if (!Pattern.matches("[a-zA-Z]{1}", oneAlphabets)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				if (!Pattern.matches("[a-zA-Z0-9]{1}", oneAlphanum)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				if (!Pattern.matches("[a-zA-Z]{1}", oneAlphabets1)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				if (!Pattern.matches("[a-zA-Z0-9]{1}", oneAlphanum1)) {
					AppUtil.addWarn("Enter Valid GST");
					return false;
				}
				
				
			}

		} catch (Exception e) {
			log.info("Exception Occured In checkValidation" + e);
		}
		return valid;

	}

	public String updateSupplierMaster() {
		log.info("...... supplierMasterBean update begin ....");
		BaseDTO baseDTO = null;
		try {
//			String GstinNumber = supplierMaster != null ? supplierMaster.getGstNumber() : null;
//			String GstStateCode = supplierMaster != null
//					? (supplierMaster.getAddressMaster().getStateMaster().getGstStateCode())
//					: null;
					

//			if (GstinNumber != null && GstStateCode != null) {

				// String subString = GstinNumber.substring(0, GstStateCode.length());
				if (checkValidation(true)) {
					log.info("gstinNumber is correct");
					baseDTO = httpService.post(appPreference.getPortalServerURL() + "/supplier/master/update",
							supplierMaster);

					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {

							log.info("supplierMasterBean updated successfully");
				 			AppUtil.addInfo("Supplier Master Updated successfully");

							// errorMap.notify(ErrorDescription.SUPPLIER_MASTER_UPDATED.getCode());
							return showList();
						} else {
							log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
									+ baseDTO.getErrorDescription());
							errorMap.notify(baseDTO.getStatusCode());
							return CREATE_SUPPLIER_MASTER;
						}
					}
				 else {
					log.info("gstinNumber is worng");
					// AppUtil.addError("Please Enter Valid GSTIN number");
					return CREATE_SUPPLIER_MASTER;
				}
			}
		} catch (Exception e) {
			log.error("Error while update supplierMaster", e);
		}
		return LIST_SUPPLIER_MASTER;
	}

	public String delete() {
		log.info("...... supplierMasterBean delete begin ....");

		if (supplierMaster == null) {
			log.info("Please Any One");
			AppUtil.addWarn("Please Any One");
			return null;
		}

		if (supplierMaster != null && supplierMaster.getActiveStatus()) {
			log.info("Approved Supplier Cannot be Deleted");
			AppUtil.addWarn("Approved Supplier Cannot be Deleted");
			return null;
		}

		BaseDTO baseDTO = null;
		try {

			String deletePath = appPreference.getPortalServerURL() + "/supplier/master/delete/"
					+ supplierMaster.getId();
			log.info(" deletePath " + deletePath);

			baseDTO = httpService.get(deletePath);

			if (baseDTO != null) {
				log.info("supplierMasterBean Deleted successfully");
				AppUtil.addInfo("Supplier Master Deleted successfully");
				showList();
				// errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error("Error while Deleting supplierMaster" + e);
		}
		return LIST_SUPPLIER_MASTER;
	}

	public String getForm() {
		log.info(" Inside gotoView()");

		BaseDTO baseDTO = null;
		address = null;

		if (supplierMaster == null) {
			log.info("Please Any One");
			AppUtil.addWarn("Please Any One");
			return null;
		}
		supplierTypeMasterList = commonDataService.loadSupplierTypeMaster();
		businessTypeMasterList = commonDataService.loadBusinessTypeList();
		bankMasterList = commonDataService.getBankDetails();
		stateList = commonDataService.loadStateList();
		nationalityList = commonDataService.getAllNationality();
		circleList=commonDataService.loadCircles();
		listCountry=commonDataService.loadCountry();
		districList=commonDataService.getDistrictMasterList();
		loadAllOrganizationTypeMaster();
		getLoomType();
		try {

			String getByIdPath = appPreference.getPortalServerURL() + "/supplier/master/get/" + supplierMaster.getId();
			log.info(" getByIdPath " + getByIdPath);

			baseDTO = httpService.get(getByIdPath);

			ObjectMapper mapper = new ObjectMapper();

			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			supplierMaster = mapper.readValue(jsonResponse, new TypeReference<SupplierMaster>() {
			});
			BankBranchMaster bankBranch = supplierMaster.getBranchMaster();
			if (bankBranch != null) {
				bankMaster = bankBranch.getBankMaster();
				ifscCode = bankBranch.getIfscCode();
				processBranches();
			}
			addressMasterBean.setAddressMaster(supplierMaster.getAddressMaster());
			// addressMasterBean.setAddressMaster( new AddressMaster());
			address = addressMasterBean.prepareAddress(supplierMaster.getAddressMaster());
		}

		catch (Exception e) {
			log.error("Error while gotoView supplierMaster", e);
		}

		if ("EDIT".equalsIgnoreCase(action)) {
			addressMasterBean.setAddressMaster(new AddressMaster());
			addressMasterBean.setAddressMaster(supplierMaster.getAddressMaster());
			if (addressMasterBean.getAddressMaster() == null) {
				addressMasterBean.setAddressMaster(new AddressMaster());
			}

			return CREATE_SUPPLIER_MASTER;
		}
		address = addressMasterBean.prepareAddress(supplierMaster.getAddressMaster());
		return VIEW_SUPPLIER_MASTER;
	}
	
	

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());

		supplierMaster.setAddressMaster(addressMasterBean.getAddressMaster());

		address = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("suppForm");
	}

	public void getAllDistrictByState() {
		log.info(" Inside  getAllDistrictByState() ");

		districtList = new ArrayList<>();

		if (addressMaster != null && addressMaster.getStateMaster() != null
				&& addressMaster.getStateMaster().getId() != null) {
			Long stateId = addressMaster.getStateMaster().getId();
			log.info("stateId : " + stateId);

			districtList = commonDataService.loadActiveDistrictsByState(stateId);
		} else {

			log.info(" getAllDistrictByState() addressMaster.getStateMaster().getId() ==null");

			districtList = new ArrayList<>();
			talukList = new ArrayList<TalukMaster>();
			cityList = new ArrayList<CityMaster>();
			areaList = new ArrayList<AreaMaster>();

			addressMaster.setDistrictMaster(new DistrictMaster());
			addressMaster.setTalukMaster(new TalukMaster());
			addressMaster.setCityMaster(new CityMaster());
			addressMaster.setAreaMaster(new AreaMaster());
		}

		log.info("customerDistrictList ===>>>>" + districtList.size());
	}

	public void getAllTalukByDistrict() {
		log.info("Inside getAllTalukByDistrict ");

		talukList = new ArrayList<>();

		if (addressMaster != null && addressMaster.getDistrictMaster() != null
				&& addressMaster.getDistrictMaster().getId() != null) {
			Long districtId = addressMaster.getDistrictMaster().getId();
			log.info("districtId : " + districtId);

			talukList = commonDataService.loadActiveTaluksByDistrict(districtId);
		} else {

			log.info(" getAllTalukByDistrict() addressMaster.getDistrictMaster().getId() ==null");

			talukList = new ArrayList<>();
			cityList = new ArrayList<CityMaster>();
			areaList = new ArrayList<AreaMaster>();

			addressMaster.setTalukMaster(new TalukMaster());
			addressMaster.setCityMaster(new CityMaster());
			addressMaster.setAreaMaster(new AreaMaster());
		}

		log.info("talukList ===>>>>" + talukList.size());
	}

	public void getAllCityByTaluk() {
		log.info(" Inside  " + addressMaster);
		cityList = new ArrayList<CityMaster>();

		if (addressMaster != null && addressMaster.getTalukMaster() != null
				&& addressMaster.getTalukMaster().getId() != null) {
			Long districtId = addressMaster.getDistrictMaster().getId();
			log.info("districtId : " + districtId);
			cityList = commonDataService.loadActiveCitiesByDistrict(districtId);
		} else {
			cityList = new ArrayList<CityMaster>();
			areaList = new ArrayList<AreaMaster>();

			addressMaster.setCityMaster(new CityMaster());
			addressMaster.setAreaMaster(new AreaMaster());
		}

		log.info("cityList ===>>>>" + cityList.size());

	}

	public void getAllAreaListByTaluk() {
		log.info("<< == Auto Complete method to getAreaList == >> " + addressMaster);
		areaList = new ArrayList<AreaMaster>();

		if (addressMaster != null && addressMaster.getCityMaster() != null
				&& addressMaster.getCityMaster().getId() != null) {
			Long cityId = addressMaster.getCityMaster().getId();
			log.info("cityId : " + cityId);

			areaList = commonDataService.loadActiveAreasByCity(cityId);
		} else {
			areaList = new ArrayList<AreaMaster>();

			addressMaster.setAreaMaster(new AreaMaster());
		}

		log.info("areaList ===>>>>" + areaList.size());
	}

	public List<OrganizationTypeMaster> loadAllOrganizationTypeMaster() {
		log.info("<======== suppliermasterService.loadAllOrganizationTypeMaster() calling ======>");
		organizationtypemasterList = new ArrayList<>();
		try {
			String url = AppUtil.getPortalServerURL() + "/organizationtypemaster/getActiveOrganizationTypes";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				organizationtypemasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<OrganizationTypeMaster>>() {
						});
			} else {
				organizationtypemasterList = new ArrayList<>();
			}
			log.info("suppliermasterService organizationtypemasterList size" + organizationtypemasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception suppliermasterService.loadAllOrganizationTypeMaster() ==========>", e);
		}
		return organizationtypemasterList;
	}

}
