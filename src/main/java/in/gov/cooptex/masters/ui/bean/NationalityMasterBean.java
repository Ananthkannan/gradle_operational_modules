package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Nationality;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;


@Service("nationalityMasterBean")
@Scope("session")
@Log4j2
public class NationalityMasterBean {

	final String LIST_PAGE = "/pages/masters/listNationalityMaster.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createNationalityMaster.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String NATIONALITY_MASTER_URL = AppUtil.getPortalServerURL()+"/nationality";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter @Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter @Setter
	Nationality nationalityMaster,selectedNationalityMaster;

	@Getter @Setter
	LazyDataModel<Nationality> lazyNationalityMasterList;

	List<Nationality> nationalityMasterList = null;

	@Getter
    @Setter
    int totalRecords = 0;
	
	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;
	
	public String showNationalityMasterListPage() {
		log.info("<==== Starts NationalityMasterBean.showNationalityMasterListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		nationalityMaster = new Nationality();
		selectedNationalityMaster = new Nationality();
		loadLazyNationalityMasterList();
		log.info("<==== Ends NationalityMasterBean.showNationalityMasterListPage =====>");
		return LIST_PAGE;
	}


	public void  loadLazyNationalityMasterList() {
		log.info("<===== Starts NationalityMasterBean.loadLazyNationalityMasterList ======>");

		lazyNationalityMasterList = new LazyDataModel<Nationality>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<Nationality> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					log.info("Pagination request :::"+paginationRequest);
					String url = NATIONALITY_MASTER_URL + "/getallnationalitymasterlistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						nationalityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<Nationality>>() {});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return nationalityMasterList;
			}

			@Override
			public Object getRowKey(Nationality res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Nationality getRowData(String rowKey) {
				for (Nationality nationalityMaster : nationalityMasterList) {
					if (nationalityMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedNationalityMaster = nationalityMaster;
						return nationalityMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends NationalityMasterBean.loadLazyNationalityMasterList ======>");
	}


	public String showNationalityMasterListPageAction() {
		log.info("<===== Starts NationalityMasterBean.showNationalityMasterListPageAction ===========>"+action);
		try{
			if(!action.equalsIgnoreCase("create") && (selectedNationalityMaster == null || selectedNationalityMaster.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.SELECT_NATIONALITY_MASTER).getCode());
				return null;
			}
			if(action.equalsIgnoreCase("create")) {
				log.info("create nationalityMaster called..");
				nationalityMaster = new Nationality();
				return ADD_PAGE;
			}else if(action.equalsIgnoreCase("Edit")) {
				log.info("edit nationalityMaster called..");
				String url=NATIONALITY_MASTER_URL+"/getbyid/"+selectedNationalityMaster.getId();
				BaseDTO response = httpService.get(url);
				if(response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					nationalityMaster = mapper.readValue(jsonResponse,new TypeReference<Nationality>(){
					});	
					return ADD_PAGE;
				}else if(response != null && response.getStatusCode() != 0){
					errorMap.notify(response.getStatusCode());
				}else{
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			}else{
				log.info("delete nationalityMaster called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmNationalityMasterDelete').show();");
			}
		}catch(Exception e) {
			log.error("Exception occured in showNationalityMasterListPageAction ..",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends NationalityMasterBean.showNationalityMasterListPageAction ===========>"+action);
		return null;
	}


	public String deleteConfirm() {
		log.info("<===== Starts NationalityMasterBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(NATIONALITY_MASTER_URL+"/deletebyid/"+selectedNationalityMaster.getId());
			if(response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmNationalityMasterDelete').hide();");
			}else if(response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while delete nationalityMaster ....",e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends NationalityMasterBean.deleteConfirm ===========>");
		return  showNationalityMasterListPage();
	}
	
	public String saveOrUpdate(){
		log.info("<==== Starts NationalityMasterBean.saveOrUpdate =======>");
		try {
			if(nationalityMaster.getCode() == null || nationalityMaster.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_CODE_EMPTY).getCode());
				return null;
			}
			if(nationalityMaster.getName() == null || nationalityMaster.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_NAME_EMPTY).getCode());
				return null;
			}
			if(nationalityMaster.getName().trim().length() < 3 || nationalityMaster.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if(nationalityMaster.getLocalName() == null || nationalityMaster.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_LNAME_EMPTY).getCode());
				return null;
			}
			if(nationalityMaster.getLocalName().trim().length() < 3 || nationalityMaster.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_LNAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if(nationalityMaster.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_STATUS_EMPTY).getCode());
				return null;
			}
			String url = NATIONALITY_MASTER_URL+"/saveorupdate";
			BaseDTO response = httpService.post(url,nationalityMaster);
			if(response != null && response.getStatusCode() == 0) {
				log.info("nationalityMaster saved successfully..........");
				if(action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.getError(MastersErrorCode.NATIONALITY_MASTER_UPDATE_SUCCESS).getCode());
			}else if(response != null && response.getStatusCode() != 0){
				errorMap.notify(response.getStatusCode());
				return null;
			}else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());	
			}
		}catch(Exception e) {
			log.error("Exception occured while save or update .......",e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends NationalityMasterBean.saveOrUpdate =======>");
		return showNationalityMasterListPage();
	}
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts NationalityMasterBean.onRowSelect ========>"+event);
		selectedNationalityMaster = ((Nationality) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		log.info("<===Ends NationalityMasterBean.onRowSelect ========>");
    }
	
}
