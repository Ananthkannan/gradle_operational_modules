package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.inventory.dto.InventoryConfigDTO;
import in.gov.cooptex.core.inventory.model.InventoryConfig;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.model.SalesOrderItems;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("inventoryConfigBean")
public class InventoryConfigBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String SERVER_URL = AppUtil.getPortalServerURL();
	private final String LIST_INVENTORY_CONFIG_MASTER_PAGE = "/pages/masters/listInventoryControl.xhtml?faces-redirect=true;";
	private final String CREATE_INVENTORY_CONFIG_MASTER_PAGE = "/pages/masters/createInventoryControl.xhtml?faces-redirect=true;";
	private final String VIEW_INVENTORY_CONFIG_MASTER_PAGE = "/pages/masters/viewInventoryControl.xhtml?faces-redirect=true;";

	@Autowired
	LoginBean loginBean;
	@Autowired
	AppPreference appPreference;
	@Autowired
	HttpService httpService;
	String jsonResponse;
	ObjectMapper mapper;
	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	int totalRecords = 0;
	@Getter
	@Setter
	Boolean addButtonFlag = false;
	@Getter
	@Setter
	Boolean editButtonFlag;
	@Getter
	@Setter
	Boolean deleteButtonFlag;
	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;
	@Getter
	@Setter
	InventoryConfig inventoryConfig;
	@Getter
	@Setter
	InventoryConfigDTO inventoryConfigDTOAdd;
	@Getter
	@Setter
	List<InventoryConfigDTO> inventoryConfigDTOAddList = new ArrayList<>();;
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	InventoryConfigDTO selectedInventoryConfigDTO;
	@Getter
	@Setter
	InventoryConfigDTO inventoryConfigDTO;
	@Getter
	@Setter
	LazyDataModel<InventoryConfig> lazyInventoryConfigList;
	@Getter
	@Setter
	InventoryConfig selectedInventoryConfig;
	@Getter
	@Setter
	List<InventoryConfigDTO> inventoryConfigDTOList = new ArrayList<InventoryConfigDTO>();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;
	@Getter
	@Setter
	ProductCategory selectedProductCategory;
	@Getter
	@Setter
	ProductGroupMaster selectedProductGroupMaster;
	@Getter
	@Setter
	private List<EntityMaster> showRoomListEntity;
	@Getter
	@Setter
	private List<Long> showRoomList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;
	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;
	@Getter
	@Setter
	private List<ProductCategory> productCategoryListEntity;
	@Setter
	@Getter
	List<ProductVarietyMaster> productVarietyMasterList = new ArrayList<ProductVarietyMaster>();

	@Getter
	@Setter
	LazyDataModel<InventoryConfigDTO> lazyInventoryConfigDTOList;
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<InventoryConfig> inventoryConfigItemsList;
	@Getter
	@Setter
	boolean inventoryConfigFlag;
	@Getter
	@Setter
	boolean regionShowroomFlag = false;
	
	@Getter
	@Setter
	Boolean salesItemMinQty=false;

	public String showInventoryConfigListPage() {
		log.info("<==== Starts InventoryConfigBean.showInventoryConfigListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = false;
		regionShowroomFlag = false;
		inventoryConfig = new InventoryConfig();
		inventoryConfigDTO = new InventoryConfigDTO();
		selectedInventoryConfigDTO = new InventoryConfigDTO();
		loadLazyInventoryConfigList();
		selectedInventoryConfig = new InventoryConfig();
		log.info("<==== Ends InventoryConfigBean.showInventoryConfigListPage =====>");
		return LIST_INVENTORY_CONFIG_MASTER_PAGE;
	}

	private void loadLazyInventoryConfigList() {

		log.info("<===== Starts InventoryConfig.loadLazyInventoryConfigList ======>");
		lazyInventoryConfigDTOList = new LazyDataModel<InventoryConfigDTO>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<InventoryConfigDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/inventoryconfig/getinventoryconfiglist";
					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						inventoryConfigDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<InventoryConfigDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyInventoryConfigList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return inventoryConfigDTOList;
			}

			@Override
			public Object getRowKey(InventoryConfigDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public InventoryConfigDTO getRowData(String rowKey) {
				try {
					for (InventoryConfigDTO inventoryConfig : inventoryConfigDTOList) {
						if (inventoryConfig.getId().equals(Long.valueOf(rowKey))) {
							selectedInventoryConfigDTO = inventoryConfig;
							return inventoryConfig;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends InventoryConfig.loadLazyInventoryConfigList ======>");

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts InventoryConfig.onRowSelect ========>" + event);
		selectedInventoryConfigDTO = ((InventoryConfigDTO) event.getObject());

		log.info("selectedInventoryConfig id" + selectedInventoryConfigDTO.getId());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		log.info("<===Ends InventoryConfig.onRowSelect ========>");
	}

	public String addInventoryConfig() {
		log.info("<--- Start InventoryConfigBean.addInventoryConfig ---->");
		try {
			action = "ADD";
			inventoryConfig = new InventoryConfig();
			loadAllRegionList();
			loadProductCategory();
			inventoryConfigDTOAddList = new ArrayList<>();
			log.info("<---- End InventoryConfigBean.addInventoryConfig ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_INVENTORY_CONFIG_MASTER_PAGE;
	}

	public void loadProductCategory() {
		productCategoryList = commonDataService.loadProductCategories();
		
	}

	public void loadProductGroupByProductCategory() {
		productGroupList = commonDataService.loadProductGroups(inventoryConfigDTO.selectedProductCategory);
		loadProductItemMinQuantity();
	}

	public void loadProductVarietiesByProductGroup() {
		productVarietyMasterList = commonDataService.loadProductVarieties(inventoryConfigDTO.selectedProductGroup);
	}

	public void loadAllRegionList() {
		try {
			log.info("================call bean loadAllRegionList()============================");
			String url = SERVER_URL + "/entitymaster/simpleregionlist";
			BaseDTO response = httpService.get(url);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});
				log.info("Entity master list size" + entityMasterList.size());
			}
		} catch (Exception e) {
			log.error("Load loadAllRegionList funtion", e);
		}
	}
	
	public void loadProductItemMinQuantity() {
		try {
			log.info("================call bean loadProductItemMinQuantity()============================");
			inventoryConfigDTO.setMinStock(0L);
			salesItemMinQty=false;
			if(inventoryConfigDTO!=null && inventoryConfigDTO.getSelectedProductCategory()!=null 
				&& inventoryConfigDTO.getSelectedProductCategory().getId()!=null) {
				if(inventoryConfigDTO.getSelectedProductCategory().getProductCatCode().equalsIgnoreCase("C")
						|| inventoryConfigDTO.getSelectedProductCategory().getProductCatCode().equalsIgnoreCase("D")) {
					String url = SERVER_URL + appPreference.getOperationApiUrl() + "/inventoryconfig/getProductItemMinQuantity" ;
					BaseDTO response = httpService.post(url,inventoryConfigDTO);
					Long salesItemsList=null;
				
					if (response != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						salesItemsList = mapper.readValue(jsonResponse, Long.class);
						log.info("salesOrderItem list size..." + salesItemsList);
						
						if(salesItemsList!=null && salesItemsList>0) {
								//Double qty=Math.floor((salesItemsList/3));
								Double qty=Math.floor((salesItemsList/3)/12);
								if(qty>0) {
									log.info("<<< salesQty...."+qty);
									Long salesQty=qty.longValue();
									inventoryConfigDTO.setMinStock(salesQty);
									salesItemMinQty=true;
								}
						}
					}
			    }
			}
			} catch (Exception e) {
				log.error("<<== Exception Load salesOrderItem funtion", e);
			}
	}

	public void loadShowRoomList() {
		showRoomList = null;
		log.info("<===== Start InventoryConfigBean.loadShowRoomList ======>");
		String requestPath = SERVER_URL + "/entitymaster/getallshowroomforregion/"
				+ inventoryConfigDTO.getSelectedRegion().getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					showRoomListEntity = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					log.info("showRoomListt.size " + (showRoomList == null ? 0 : showRoomList.size()));
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("<--- Exception in loadshowRoomList() --->", e);
		}
		log.info("<===== End InventoryConfigBean.loadShowRoomList ======>");
	}

	public void loadAllProductGroupList() {
		productGroupList = null;
		try {
			log.info("================call bean loadAllProductGroupList()============================");
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/inventoryconfig/getproductgrouplist";
			BaseDTO response = httpService.get(url);
			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productGroupList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
				log.info("product Group List size" + productGroupList.size());
			}
		} catch (Exception e) {
			log.error("Load loadAllProductGroupList funtion", e);
		}
	}

	public void onProductVarietySelection() {

		log.info("<====== InventoryConfigBean.onProductVarietySelection Starts====> - selectedProductCategory"
				+ selectedProductCategory);
		selectedProductCategory = new ProductCategory();
		selectedProductGroupMaster = new ProductGroupMaster();
		Long ProductCategoryId = inventoryConfigDTO.selectedProductCategory.getId();
		Long productGroupId = inventoryConfigDTO.selectedProductGroup.getId();
		productVarietyMasterList = loadProductVarietyByProductcatAndProductGroup(ProductCategoryId, productGroupId);
		log.info("<====== InventoryConfigBean.onProductVarietySelection Ends====> ");
	}

	public List<ProductVarietyMaster> loadProductVarietyByProductcatAndProductGroup(Long ProductCategoryId,
			Long productGroupId) {
		log.info("Inside loadProductVarietyByProductcatAndProductGroup()");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			String Url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/inventoryconfig/getproductvarbyproductcatandproductgroup/" + ProductCategoryId + "/"
					+ productGroupId;
			log.info("URL request for entity :: " + Url);
			BaseDTO response = httpService.get(Url);

			if (response != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
				log.info("Total No. Of ProductVarietyMaster by productcat or Regional office and productGroup:["
						+ productVarietyMasterList.size() + "]");
			}
		} catch (Exception e) {
			log.info("Error in getting productVarietyMasterList() ::: " + e);
		}
		return productVarietyMasterList;
	}

	public void inventoryConfigAddNewInward() {
		log.info("InventotoryConfig add new received :");
		inventoryConfigDTOAdd = new InventoryConfigDTO();
		try {
			if (inventoryConfigDTO.getSelectedRegion() == null) {
				log.info("Invalid Region quantity:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_REGION.getErrorCode());
			} else if (inventoryConfigDTO.getSelectedShowRoom() == null) {
				log.info("Invalid ShowRoomCode and Name:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_SHOWROOM.getErrorCode());
			} else if (inventoryConfigDTO.getSelectedProductCategory() == null) {
				log.info("Invalid ProductCategory and Name:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTCATEGORY.getErrorCode());
			} else if (inventoryConfigDTO.getSelectedProductGroup() == null) {
				log.info("Invalid ProductCategory and Name:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTGROUP.getErrorCode());
			} else if (inventoryConfigDTO.getSelectedProductVariety() == null) {
				log.info("Invalid Productvar code and Name:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTVARIETY.getErrorCode());
			} else if (inventoryConfigDTO.getStockMaintanenceType().equals("")) {
				log.info("Invalid StockMaintanenceType quantity:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_STOCKMAINTANENCETYPE.getErrorCode());
			} else if (inventoryConfigDTO.getMinStock() == null || inventoryConfigDTO.getMinStock() <= 0) {
				log.info("Invalid MinStock quantity:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_MINSTOCK.getErrorCode());
			} else if (inventoryConfigDTO.getMaxStock() == null || inventoryConfigDTO.getMaxStock() <= 0) {
				log.info("Invalid MaxStock quantity:");
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_MAXSTOCK.getErrorCode());
			} else if (inventoryConfigDTO.getMinStock() > inventoryConfigDTO.getMaxStock()) {
				log.info("Minmum Value is More than Maximum value error");
				Util.addWarn("Minimum Stock must be less than Maximum Stock Value");
			} else {
				inventoryConfigDTOAdd.setShowRoom(inventoryConfigDTO.selectedShowRoom);
				inventoryConfigDTOAdd.setProductCategory(inventoryConfigDTO.selectedProductCategory);
				inventoryConfigDTOAdd.setProductGroup(inventoryConfigDTO.selectedProductGroup);
				inventoryConfigDTOAdd.setProductVariety(inventoryConfigDTO.selectedProductVariety);
				inventoryConfigDTOAdd.setMaxStock(inventoryConfigDTO.maxStock);
				inventoryConfigDTOAdd.setMinStock(inventoryConfigDTO.minStock);
				inventoryConfigDTOAdd.setStockMaintanenceType(inventoryConfigDTO.getStockMaintanenceType());
				inventoryConfigDTOAddList.add(inventoryConfigDTOAdd);
				// inventoryConfigDTO.setSelectedShowRoom(null);
				// inventoryConfigDTO.setSelectedRegion(null);
				inventoryConfigDTO.setSelectedProductCategory(null);
				inventoryConfigDTO.setSelectedProductGroup(null);
				inventoryConfigDTO.setMaxStock(null);
				inventoryConfigDTO.setMinStock(null);
				inventoryConfigDTO.setStockMaintanenceType(null);
				inventoryConfigDTO.setSelectedProductVariety(null);
				regionShowroomFlag = true;
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_ADD_SUCCESS.getErrorCode());
				log.info("InventotoryConfig add Success :");
				inventoryConfig = new InventoryConfig();

			}
		} catch (Exception e) {
			log.error("InventotoryConfig Collection Add Error :" + e);
			errorMap.notify(ErrorDescription.INVENTORY_CONFIG_ADD_FAILED.getErrorCode());
		}

	}

	public String clearInventoryConfigList() {
		log.info("clear InventotoryConfig list called :");
		// inventoryConfigDTO.setSelectedShowRoom(null);
		// inventoryConfigDTO.setSelectedRegion(null);
		inventoryConfigDTO.setSelectedProductCategory(null);
		inventoryConfigDTO.setSelectedProductGroup(null);
		inventoryConfigDTO.setMaxStock(null);
		inventoryConfigDTO.setMinStock(null);
		inventoryConfigDTO.setStockMaintanenceType(null);
		inventoryConfigDTO.setSelectedProductVariety(null);
		regionShowroomFlag = false;
		return CREATE_INVENTORY_CONFIG_MASTER_PAGE;

	}

	public String saveOrUpdate() {
		log.info("<==== Starts InventoryConfigBean.saveOrUpdate =======>");
		try {

			inventoryConfigDTO.setInventoryConfigList(new ArrayList<>());
			inventoryConfigDTO.setIntenventoryConfigDTOList(inventoryConfigDTOAddList);
			if (action.equalsIgnoreCase("EDIT")) {
				if (inventoryConfigDTO.getSelectedRegion() == null) {
					log.info("Invalid Region quantity:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_REGION.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getSelectedShowRoom() == null) {
					log.info("Invalid ShowRoomCode and Name:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_SHOWROOM.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getSelectedProductCategory() == null) {
					log.info("Invalid ProductCategory and Name:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTCATEGORY.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getSelectedProductGroup() == null) {
					log.info("Invalid ProductCategory and Name:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTGROUP.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getSelectedProductVariety() == null) {
					log.info("Invalid Productvar code and Name:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_PRODUCTVARIETY.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getStockMaintanenceType().equals("")) {
					log.info("Invalid StockMaintanenceType quantity:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_STOCKMAINTANENCETYPE.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getMinStock() == null || inventoryConfigDTO.getMinStock() <= 0) {
					log.info("Invalid MinStock quantity:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_MINSTOCK.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getMaxStock() == null || inventoryConfigDTO.getMaxStock() <= 0) {
					log.info("Invalid MaxStock quantity:");
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_INVALID_MAXSTOCK.getErrorCode());
					return null;
				} else if (inventoryConfigDTO.getMinStock() > inventoryConfigDTO.getMaxStock()) {
					log.info("Minmum Value is More than Maximum value error");
					Util.addWarn("Minimum Stock must be less than Maximum Stock Value");
					return null;
				}

			}
			if (action.equalsIgnoreCase("ADD") && inventoryConfigDTOAddList.size() == 0) {
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_ITEMS_NULL.getErrorCode());
				return null;
			}
			BaseDTO response = httpService.post(
					SERVER_URL + appPreference.getOperationApiUrl() + "/inventoryconfig/create", inventoryConfigDTO);
			
			if (response != null && response.getStatusCode() == 0) {
				log.info("Inventory Config  saved successfully..........");
				if (action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.INVENTORY_CONFIG_UPDATED_SUCCESS.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}

		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends InventoryConfigBean.saveOrUpdate =======>");
		return showInventoryConfigListPage();
	}

	public void updateMaxValue() {

		if (inventoryConfigDTO.getMinStock() != null && inventoryConfigDTO.getMaxStock() != null) {
			Long maxQnty = inventoryConfigDTO.getMinStock() * inventoryConfigDTO.getMinStock() * inventoryConfigDTO.getMinStock();
			if (!(inventoryConfigDTO.getMaxStock() >= maxQnty)) {
				AppUtil.addWarn("Max Stock QnTY Should Be THREE TIMES of the Minimum Quantity");
				inventoryConfigDTO.setMaxStock((long) 0);
			}
		}

	}

	public String deleteInventoryConfig() {
		try {
			action = "Delete";
			if (selectedInventoryConfigDTO == null || selectedInventoryConfigDTO.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_INVENTORY_CONFIG.getErrorCode());
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInventoryConfigDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteInventoryConfigConfirm() {
		log.info("<===== Starts InventoryConfigBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/inventoryconfig/delete/" + selectedInventoryConfigDTO.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INVENTORY_CONFIG_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmInventoryConfigDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete InventoryConfig ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends InventoryConfigBean.deleteConfirm ===========>");
		return showInventoryConfigListPage();
	}

	public String clear() {
		log.info("<---- Start InventoryConfigBean . clear ----->");
		showInventoryConfigListPage();

		log.info("<---- End InventoryConfigBean.clear ----->");
		inventoryConfigDTO.setSelectedProductVariety(null);
		return LIST_INVENTORY_CONFIG_MASTER_PAGE;

	}

	public String viewInventoryConfig() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedInventoryConfigDTO == null || selectedInventoryConfigDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_INVENTORY_CONFIG.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedInventoryConfigDTO.getId());

		selectedInventoryConfigDTO = getInventoryConfigDetails(selectedInventoryConfigDTO.getId());

		log.info("selectedInventoryConfig Object:::" + selectedInventoryConfig);
		return VIEW_INVENTORY_CONFIG_MASTER_PAGE;
	}

	public InventoryConfigDTO getInventoryConfigDetails(Long id) {

		log.info("<====== InventoryConfigBean.getInventoryConfigDetails starts====>" + id);
		inventoryConfigDTO = new InventoryConfigDTO();
		try {
			if (selectedInventoryConfigDTO == null || selectedInventoryConfigDTO.getId() == null) {
				log.error(" No InventoryConfig selected ");
				errorMap.notify(ErrorDescription.SELECT_INVENTORY_CONFIG.getErrorCode());
			}
			log.info("Selected InventoryConfigDTO  Id : : " + selectedInventoryConfigDTO.getId());
			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/inventoryconfig/get/"
					+ selectedInventoryConfigDTO.getId();

			log.info("InventoryConfig Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			log.info("view response" + baseDTO.getResponseContent());
			inventoryConfigDTO = mapper.readValue(jsonResponse, InventoryConfigDTO.class);
			log.info("product variety" + inventoryConfigDTO.getProductVarietyMaster());

			if (baseDTO.getStatusCode() == 0) {
				log.info(" InventoryConfig Retrived  SuccessFully ");
			} else {
				log.error(" InventoryConfig Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return inventoryConfigDTO;
		} catch (Exception e) {
			log.error("Exception Ocured while get InventoryConfig Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String editInventoryConfigDetails() {
		try {
			if (selectedInventoryConfigDTO == null || selectedInventoryConfigDTO.getId() == null) {
				log.info("----- selectedInventoryConfigDTO-------");
				errorMap.notify(ErrorDescription.SELECT_INVENTORY_CONFIG.getCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		inventoryConfigDTOAddList = new ArrayList<>();
		inventoryConfig = new InventoryConfig();

		inventoryConfigDTO = getInventoryConfigDetails(selectedInventoryConfigDTO.getId());
		loadAllRegionList();
		loadShowRoomList();
		loadAllProductGroupList();
		loadProductCategory();
		loadProductVarietyByProductcatAndProductGroup(inventoryConfigDTO.selectedProductCategory.getId(),
				inventoryConfigDTO.getSelectedProductGroup().getId());

		return CREATE_INVENTORY_CONFIG_MASTER_PAGE;

	}

}
