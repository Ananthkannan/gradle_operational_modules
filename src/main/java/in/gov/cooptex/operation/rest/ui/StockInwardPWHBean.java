package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransfer;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.model.StockTransferItemsBundles;
import in.gov.cooptex.core.ui.EntityType;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.enums.StockTransferStatus;
import in.gov.cooptex.operation.enums.StockTransferType;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.StockTransferRequest;
import in.gov.cooptex.operation.production.dto.StockTransferResponse;
import in.gov.cooptex.operation.rest.ui.service.StockTransferUtility;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("stockInwardPWHBean")
@Scope("session")
public class StockInwardPWHBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/productWarehouse/inward/listStockInwardPWH.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/productWarehouse/inward/createStockInwardPWH.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/productWarehouse/inward/viewStockInwardPWH.xhtml?faces-redirect=true;";

	private StockTransferUtility stockTransferUtility = new StockTransferUtility();

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	boolean disableBtnSearch = true;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	private String action;

	@Getter
	@Setter
	String SERVER_URL = AppUtil.getPortalServerURL();

	@Getter
	@Setter
	LazyDataModel<StockTransferResponse> stockTransferResponseLazyList;

	@Getter
	@Setter
	StockTransferResponse stockTransferResponse;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Integer stockSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	List<StockTransfer> stockTransferList;

	@Getter
	@Setter
	StockTransfer stockTransfer;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	List<String> stockTransferFromList;

	@Getter
	@Setter
	private String stockTransferFrom;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	public EntityMaster getLoginEmployeeWorkLocation() {
		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {
			return loginEmployee.getPersonalInfoEmployment().getWorkLocation();
		}
	}

	public String gotoPage(String page) {
		log.info("GOTO [" + page + "]");

		loginEmployee = (EmployeeMaster) loginBean.getUserProfile();

		if (loginEmployee == null || loginEmployee.getPersonalInfoEmployment() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster() == null
				|| loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
						.getEntityCode() == null) {

			errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_DETAILS_REQUIRED.getCode());
			return null;

		} else {

			String loginEntityCode = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getEntityTypeMaster()
					.getEntityCode();

			if (loginEntityCode.equals(EntityType.PRODUCT_WAREHOUSE)) {
				log.info("page is---------" + page);
				stockTransferFromList = new ArrayList<>();
				stockTransferFromList.add("Entity");
				stockTransferFromList.add("Supplier");

				if ("ADD".equals(page) || "EDIT".equals(page)) {
					redirectSVInProgressPage();
				}
				
				if (page.equals("ADD")) {
					return gotoAddPage(loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId());
				} else if (page.equals("EDIT")) {
					return gotoEditPage();
				} else if (page.equals("VIEW")) {
					return gotoViewPage();
				} else if (page.equals("LIST")) {
					return getListPage();
				} else {
					return null;
				}
			} else {
				errorMap.notify(ErrorDescription.LOGIN_EMPLOYEE_LOCATION_IS_NOT_PRODUCT_WAREHOUSE.getCode());
			}
		}

		return null;
	}

	public String redirectSVInProgressPage() {
		if(loginBean.getStockVerificationIsInProgess()) {
			loginBean.setMessage("Inward");
			loginBean.redirectSVInProgressPgae();
		}
		return null;
	}
	
	public String getListPage() {
		log.info("Going to list page");

		
		// SERVER_URL = stockTransferUtility.loadServerUrl();
		statusValues = stockTransferUtility.loadStockTransferStatusList();
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
		loadLazyList();

		return LIST_PAGE_URL;
	}

	private StockTransferRequest getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		StockTransferRequest request = new StockTransferRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("referenceNumber")) {
				request.setReferenceNumber(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				request.setSocietyCodeOrName(value);
			}

			if (entry.getKey().equals("wareHouseCodeOrName")) {
				request.setWareHouseCodeOrName(value);
			}

			if (entry.getKey().equals("dateReceived")) {
				request.setDateReceived(AppUtil.serverDateFormat(value));
			}

			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		StockTransferRequest request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);
		if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null
				&& loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation() != null) {
			request.setLoginUserId(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId());
		}
		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/inward/search";
		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	public void loadLazyList() {

		stockTransferResponseLazyList = new LazyDataModel<StockTransferResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<StockTransferResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<StockTransferResponse> data = new ArrayList<StockTransferResponse>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<StockTransferResponse>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						stockSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(StockTransferResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public StockTransferResponse getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<StockTransferResponse> responseList = (List<StockTransferResponse>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (StockTransferResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		stockTransferResponse = ((StockTransferResponse) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		log.info("RetailQualityCheckBean.onPageLoad method started");
		stockTransferResponse = new StockTransferResponse();
		disableAddButton = false;
	}

	/***************************************************************************************************/

	/*
	 * This method is called when Edit Button is pressed
	 */

	public String gotoEditPage() {
		try {
			if (stockTransferFromList == null) {
				stockTransferFromList = new ArrayList<>();
				stockTransferFromList.add("Entity");
				stockTransferFromList.add("Supplier");
			}
			if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
				return null;
			}

			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {

				stockTransfer = getStockTransferById(stockTransferResponse.getId());
				log.info("gotoEditPage() stockTransfer id-------------" + stockTransfer.getId());

				purchaseOrderList = new ArrayList<PurchaseOrder>();
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/checkpurchaseorder/"
						+ stockTransfer.getId();

				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					purchaseOrderList = mapper.readValue(jsonValue, new TypeReference<List<PurchaseOrder>>() {
					});
				}
				log.info("Purchase order list size----------" + purchaseOrderList.size());

				if (purchaseOrderList.size() == 0) {
					purchaseorderFlag = true;
				} else {
					purchaseorderFlag = false;
				}

				if (stockTransfer.getStockTransfer() != null) {
					stockTransfer.getStockTransfer().setCreatedByName(
							stockTransfer.getStockTransfer().getReferenceNumber() + " / " + AppUtil.DATE_FORMAT
									.format(stockTransfer.getStockTransfer().getDateTransferred()));
					prepareReceiveProduct();
				}

				bundleNumbersAdded = new HashSet<>();

				stockTransferItemsDisplayList = new ArrayList<>();
				for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
					addItemToDisplayList(items);
				}

				disableBtnSearch = false;
				displayRemoveButtonItemDetails = true;

				return ADD_PAGE_URL;
			} else {
				errorMap.notify(ErrorDescription.STOCK_TRANS_CANNOT_BE_EDITTED.getCode());
				return null;
			}
		} catch (Exception e) {
			log.info("gotoEditPage exception-------", e);
			return null;
		}
	}

	public String gotoViewPage() {

		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
			return null;
		}

		stockTransfer = getStockTransferById(stockTransferResponse.getId());

		bundleNumbersAdded = new HashSet<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		for (StockTransferItems items : stockTransfer.getStockTransferItemsList()) {
			addItemToDisplayList(items);
		}

		displayRemoveButtonItemDetails = false;

		return VIEW_PAGE_URL;

	}

	/*
	 * This method is called when the Delete button is pressed
	 */
	public void processDelete() {

		// Step 1: Check the object is selected from the list
		if (stockTransferResponse == null || stockTransferResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.STOCK_TRANS_SELECT_ONE_RECORD.getCode());
		} else {

			// Step 2: Find the Object is removable
			if (stockTransferResponse.getStatus().equals(StockTransferStatus.INITIATED)) {
				RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
			} else {
				errorMap.notify(ErrorDescription.RECORED_CAN_NOT_BE_DELETED.getCode());
			}
		}
	}

	/*
	 * This method is called from inside the confirmation box to delete an object
	 */
	public String delete() {

		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/delete/"
				+ stockTransferResponse.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.STOCK_TRANS_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return getListPage();
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	/*********************************************************************************************************************************/

	public String gotoAddPage(Long workLocationId) {
		log.info("Add Button is pressed....");
		disableBtnSearch = true;
		stockTransfer = new StockTransfer();
		stockTransferItems = new StockTransferItems();
		stockTransfer.setStockTransfer(null);
		stockTransfer.setSupplierMaster(null);
		stockTransferList = null;
		stockTransferItemsDisplayListTemp = new ArrayList<>();
		stockTransferItemsDisplayList = new ArrayList<>();
		transferredBundleNumbers = null;
		transferredBundleWeight = null;
		transferredTotalBundles = null;

		bundleNumbersAdded = new HashSet<String>();
		displayRemoveButtonItemDetails = true;

		setStockTransferFrom(null);
		purchaseorderFlag = true;
		entityMasterList = new ArrayList<>();
		supplierMasterList = new ArrayList<>();
		return ADD_PAGE_URL;
	}

	public void onChangeStockTransferFrom() {
		log.info("onChangeStockTransferFrom " + stockTransferFrom);
		try {
			Long workLocationId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();

			if (stockTransferFrom.equals("Supplier")) {
				supplierMasterList = commonDataService.findSupplierByStatusAndTransferType(
						StockTransferStatus.SUBMITTED, StockTransferType.SOCIETY_OUTWARD, workLocationId);
				log.info("supplier master list----------" + supplierMasterList.size());
				entityMasterList = new ArrayList<>();

			} else if (stockTransferFrom.equals("Entity")) {
				entityMasterList = commonDataService.getFromEntityOnStocktransfer(workLocationId);
				log.info("entity master list----------" + entityMasterList.size());
				supplierMasterList = new ArrayList<>();
			}
			stockTransferList = new ArrayList<>();
			stockTransfer.setSupplierMaster(null);
			stockTransfer.setFromEntityMaster(null);
			stockTransfer.setStockTransfer(null);
		} catch (Exception e) {
			log.error("onChangeStockTransferFrom exception----", e);
		}

	}

	public void onChangeFromEntityMaster() {

		log.info("onChangeFromEntityMaster called");

		stockTransferList = new ArrayList<>();
		stockTransfer.setStockTransfer(null);

		Long workLocationId = loginEmployee.getPersonalInfoEmployment().getWorkLocation().getId();

		if (stockTransfer.getFromEntityMaster() != null) {

			stockTransferList = commonDataService.getStockTransferByWorklocation(workLocationId,
					stockTransfer.getFromEntityMaster().getId(), StockTransferType.PRODUCT_WAREHOUSE);

			if (stockTransferList != null) {

				stockTransferList.stream().forEach(st -> {

					st.setCreatedByName(
							st.getReferenceNumber() + " / " + AppUtil.DATE_FORMAT.format(st.getDateTransferred()));

				});

			}

		}
	}

	public void onSelectSupplierMaster() {
		log.info("onSelectSupplierMaster called");

		stockTransferList = new ArrayList<>();
		stockTransfer.setStockTransfer(null);

		EntityMaster employeeWorkLocation = getLoginEmployeeWorkLocation();

		if (stockTransfer.getSupplierMaster() != null && employeeWorkLocation != null) {
			stockTransferList = commonDataService.findStockBySupplierAndStatusAndTransferType(
					stockTransfer.getSupplierMaster().getId(), StockTransferStatus.SUBMITTED,
					StockTransferType.SOCIETY_OUTWARD, employeeWorkLocation.getId());

			if (stockTransferList != null) {
				stockTransferList.stream().forEach(st -> {
					st.setCreatedByName(
							st.getReferenceNumber() + " / " + AppUtil.DATE_FORMAT.format(st.getDateTransferred()));
				});
			}
		}
	}

	public void onSelectStockTransfer() {
		if (stockTransfer.getStockTransfer() != null) {
			disableBtnSearch = false;
		} else {
			disableBtnSearch = true;
		}
	}

	/***************************************************************************************************************************/

	@Getter
	@Setter
	String transferredBundleNumbers;

	@Getter
	@Setter
	Integer transferredTotalBundles;

	@Getter
	@Setter
	Double transferredBundleWeight;

	@Getter
	@Setter
	List<PurchaseOrder> purchaseOrderList = null;

	@Getter
	@Setter
	private Boolean purchaseorderFlag = true;

	public void searchStock() {
		log.info("Seach Stock Button is pressed.....");
		try {
			if (stockTransfer.getStockTransfer() != null) {
				log.info("Stock Transfer ID : [" + stockTransfer.getStockTransfer().getId() + "]");

				purchaseOrderList = new ArrayList<PurchaseOrder>();
				String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/checkpurchaseorder/"
						+ stockTransfer.getStockTransfer().getId();

				BaseDTO baseDTO = httpService.get(URL);
				if (baseDTO.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					purchaseOrderList = mapper.readValue(jsonValue, new TypeReference<List<PurchaseOrder>>() {
					});
				}
				log.info("Purchase order list size----------" + purchaseOrderList != null ? purchaseOrderList.size() : 0);

				stockTransfer.setStockTransfer(getStockTransferById(stockTransfer.getStockTransfer().getId()));

				log.info("stock transferitem list-----------"
						+ stockTransfer.getStockTransfer().getStockTransferItemsList() != null
								? stockTransfer.getStockTransfer().getStockTransferItemsList().size()
								: "StockTransferItem null");

				transferredBundleNumbers = stockTransfer.getStockTransfer().getBundleNumbers();
				transferredBundleWeight = stockTransfer.getStockTransfer().getBundleWeight();
				transferredTotalBundles = stockTransfer.getStockTransfer().getTotalBundles();

				prepareStockTransfer(stockTransfer.getStockTransfer(), purchaseOrderList);

			} else {
				log.info("Fields are required.");
			}
		} catch (Exception exception) {
			log.error("Seach Stock Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
	}

	public void prepareStockTransfer(StockTransfer selectedStockTransfer, List<PurchaseOrder> purchaseOrderList) {

		// stockTransfer.setFromEntityMaster(selectedStockTransfer.getFromEntityMaster());
		stockTransfer.setToEntityMaster(selectedStockTransfer.getToEntityMaster());

		stockTransfer.setTransportMaster(selectedStockTransfer.getTransportMaster());
		stockTransfer.setWaybillAvailable(selectedStockTransfer.getWaybillAvailable());
		stockTransfer.setWaybillNumber(selectedStockTransfer.getWaybillNumber());
		stockTransfer.setTransportChargeAvailable(selectedStockTransfer.getTransportChargeAvailable());
		stockTransfer.setTransportChargeType(selectedStockTransfer.getTransportChargeType());
		stockTransfer.setTransportChargeAmount(selectedStockTransfer.getTransportChargeAmount());

		stockTransfer.setDateTransferred(selectedStockTransfer.getDateTransferred());
		stockTransfer.setDateReceived(new Date());
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		stockTransfer.setTransferType(StockTransferType.WAREHOUSE_INWARD);

		if (stockTransfer.getId() == null) {
			stockTransfer.setStockTransferItemsList(new ArrayList<>());
		}
		stockTransferItemsDisplayList = new ArrayList<>();

		if (purchaseOrderList.size() == 0) {
			if (stockTransferFrom.equals("Entity")) {
				purchaseorderFlag = false;
			} else {
				purchaseorderFlag = true;
			}
			prepareReceiveProduct();
		} else {
			purchaseorderFlag = false;

			if (stockTransfer.getFromEntityMaster() != null) {
				addStockTransferItemsFromEntity(selectedStockTransfer);
			} else if (stockTransfer.getSupplierMaster() != null) {
				addStockTransferItemsFromSupplier(selectedStockTransfer);
			}
		}

	}

	public void prepareReceiveProduct() {
		log.info("gotoBundleDetails is called ", stockTransfer.getStockTransfer());

		bundleNumberSet = new HashSet<>();
		stockTransferItems = new StockTransferItems();

		if (stockTransfer.getStockTransfer() != null
				&& stockTransfer.getStockTransfer().getStockTransferItemsList() != null) {

			for (StockTransferItems sti : stockTransfer.getStockTransfer().getStockTransferItemsList()) {
				if (sti.getStockTransferItemsBundlesList() != null
						&& !sti.getStockTransferItemsBundlesList().isEmpty()) {
					for (StockTransferItemsBundles stockTransferItemsBundles : sti.getStockTransferItemsBundlesList()) {
						bundleNumberSet.add(stockTransferItemsBundles.getBundleNumber());
					}
				}
			}
		}
		stockTransferItemsListTemp = new ArrayList<>(stockTransfer.getStockTransfer().getStockTransferItemsList());
		log.info("stockTransferItemsListTemp size()===========>" + stockTransferItemsListTemp.size()
				+ stockTransferItemsListTemp);
		log.info("Number of Bundle Numbers " + bundleNumberSet);
	}

	private void addStockTransferItemsFromSupplier(StockTransfer selectedStockTransfer) {
		stockTransfer.setStockTransferItemsList(new ArrayList<>());
		int count = 0;

		stockTransfer.setBundleNumbers("");
		if (selectedStockTransfer.getStockTransferItemsList() != null
				&& !selectedStockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : selectedStockTransfer.getStockTransferItemsList()) {
				log.info("supllier bundle list-------" + items.getStockTransferItemsBundlesList().size());

				if (items.getStockTransferItemsBundlesList() != null
						&& !items.getStockTransferItemsBundlesList().isEmpty()) {
					for (StockTransferItemsBundles bundles : items.getStockTransferItemsBundlesList()) {
						StockTransferItems newItems = new StockTransferItems(items);
						newItems.setReceivedQty(bundles.getQuantity());
						newItems.setBundleNumber(bundles.getBundleNumber());
						newItems.setItemNetTotal(items.getItemNetTotal());
						newItems.setDispatchedQty(bundles.getQuantity());

						if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
							stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
						}

						if (bundles.getBundleNumber() != null) {
							stockTransfer.setBundleNumbers(
									stockTransfer.getBundleNumbers() + String.valueOf(bundles.getBundleNumber()));
						}

						count += 1;
						stockTransfer.setTotalBundles(count);
						stockTransfer.getStockTransferItemsList().add(newItems);
					}
				}
			}
			stockTransferItemsDisplayList.addAll(stockTransfer.getStockTransferItemsList());
			log.info("stockTransferItemsDisplayList size-----" + stockTransferItemsDisplayList.size());
		}
	}

	private void addStockTransferItemsFromEntity(StockTransfer selectedStockTransfer) {

		stockTransfer.setStockTransferItemsList(new ArrayList<>());

		bundleNumberSet = new HashSet<>();

		stockTransfer.setBundleNumbers("");
		if (selectedStockTransfer.getStockTransferItemsList() != null
				&& !selectedStockTransfer.getStockTransferItemsList().isEmpty()) {
			for (StockTransferItems items : selectedStockTransfer.getStockTransferItemsList()) {
				StockTransferItems newItems = new StockTransferItems(items);
				newItems.setReceivedQty(items.getDispatchedQty());
				newItems.setBundleNumber(items.getBundleNumber());
				newItems.setItemNetTotal(AppUtil.ifNullRetunZero(items.getItemNetTotal()));
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}

				if (items.getBundleNumber() != null) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() == null ? ""
							: stockTransfer.getBundleNumbers() + String.valueOf(items.getBundleNumber()));
					bundleNumberSet.add(items.getBundleNumber());
				}

				stockTransfer.getStockTransferItemsList().add(newItems);
			}
		}

		stockTransfer.setTotalBundles(bundleNumberSet.size());
		stockTransferItemsDisplayList.addAll(stockTransfer.getStockTransferItemsList());
		log.info("stockTransferItemsDisplayList size-----" + stockTransferItemsDisplayList.size());
	}

	/*************************************************************************************************************************************/

	public void clearBundleDetails() {
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList().stream().forEach(items -> {
				items.getStockTransferItemsBundlesList().clear();
			});
		}
	}

	public String saveStockTransfer() {
		log.info("saveStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.INITIATED);
		if (validateStockTransfer()) {
			clearBundleDetails();
			return create();
		} else {
			return null;
		}
	}

	public String submitStockTransfer() {
		log.info("submitStockTransfer is called " + stockTransfer);
		stockTransfer.setStatus(StockTransferStatus.SUBMITTED);
		log.info("stocktransfer item list---------" + stockTransfer.getStockTransferItemsList().size());
		if (validateStockTransfer()) {
			clearBundleDetails();
			return create();
		} else {
			return null;
		}
	}

	private boolean validateStockTransfer() {

		if (stockTransfer.getStockTransferItemsList() == null || stockTransfer.getStockTransferItemsList().isEmpty()) {
			errorMap.notify(ErrorDescription.STOCK_TRANSFER_STOCK_ITEMS_REQUIRED.getCode());
			return false;
		}

		return true;
	}

	/*************************************************************************************************************************************/

	@Getter
	@Setter
	Set<String> bundleNumberSet = new HashSet<>();

	@Getter
	@Setter
	StockTransferItems stockTransferItems = new StockTransferItems();

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsListTemp;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayList;

	@Getter
	@Setter
	List<StockTransferItems> stockTransferItemsDisplayListTemp;

	@Getter
	@Setter
	StockTransferItems stockTransferItemsDisplay;

	@Getter
	@Setter
	Set<String> bundleNumbersAdded;

	@Getter
	@Setter
	boolean displayRemoveButtonItemDetails = false;

	public StockTransferItems searchStockTransferItemsByProductVariety(StockTransferItems stockTransferItems) {

		if (stockTransferItemsListTemp != null && !stockTransferItemsListTemp.isEmpty()) {
			for (StockTransferItems items : stockTransferItemsListTemp) {
				if (items.getAtNumber() != null) {
					log.info("<======== Inside At number not null in stock transfer items ===========>"
							+ items.getAtNumber());
					if (items.getProductVarietyMaster().getId()
							.equals(stockTransferItems.getProductVarietyMaster().getId())
							&& items.getAtNumber().equals(stockTransferItems.getAtNumber())) {
						StockTransferItems newItem = new StockTransferItems(items);
						newItem.setBundleNumber(stockTransferItems.getBundleNumber());
						newItem.setAtNumber(stockTransferItems.getAtNumber());
						return newItem;
					}
				} else {
					log.info("<======== Inside At number null in stock transfer items ===========>");
					if (items.getProductVarietyMaster().getId()
							.equals(stockTransferItems.getProductVarietyMaster().getId())) {
						StockTransferItems newItem = new StockTransferItems(items);
						newItem.setBundleNumber(stockTransferItems.getBundleNumber());
						newItem.setAtNumber(stockTransferItems.getAtNumber());
						return newItem;
					}
				}
			}
		}

		return null;
	}

	public void viewItemDetails(StockTransferItems items) {
		stockTransferItemsDisplay = items;
	}

	public void changeBandleNumber() {
		log.info("changeBandleNumber " + stockTransferItems);
	}

	public String scanOrAddReceiveProduct() {
		log.info("scanReceiveProduct is called");
		if (stockTransferItems != null && stockTransferItems.getAtNumber() != null) {

			if (stockTransferUtility.isAtNumberDuplicated(stockTransferItems.getAtNumber(),
					stockTransfer.getStockTransferItemsList())) {
				errorMap.notify(ErrorDescription.STOCK_TRANS_AT_NUMBER_DUPLICATED.getCode());
				String bundleNumber = stockTransferItems.getBundleNumber();
				stockTransferItems = new StockTransferItems();
				stockTransferItems.setBundleNumber(bundleNumber);
				return null;
			}
			log.info("<============ stockTransferItemsListTemp size ==========>" + stockTransferItemsListTemp.size());
			ProductVarietyMaster productVarietyMaster = stockTransferUtility
					.getProductByAtNumber(stockTransferItems.getAtNumber(), stockTransferItemsListTemp);
			log.info("productVarietyMaster id" + productVarietyMaster);
			if (productVarietyMaster == null) {

				productVarietyMaster = findProductByAtNumberAndSupplier(stockTransferItems.getAtNumber(),
						stockTransfer.getSupplierMaster().getId());

				if (productVarietyMaster == null || productVarietyMaster.getSelectable()) {
					errorMap.notify(ErrorDescription.STOCK_TRANSFER_ATNUMBER_NOT_FOUND_IN_INWARD.getCode());
					String bundleNumber = stockTransferItems.getBundleNumber();
					stockTransferItems = new StockTransferItems();
					stockTransferItems.setBundleNumber(bundleNumber);
					return null;
				}
			}
			stockTransferItems.setProductVarietyMaster(productVarietyMaster);
			addReceiveProduct();

			return null;
		}
		return null;
	}

	public String addReceiveProduct() {
		log.info("addReceiveProduct is called");

		if (stockTransferItems != null && stockTransferItems.getAtNumber() != null
				&& stockTransferItems.getProductVarietyMaster() != null) {

			StockTransferItems items = searchStockTransferItemsByProductVariety(stockTransferItems);
			if (items != null) {
				items.setSupplierMaster(stockTransfer.getSupplierMaster());
				addItemToDisplayList(items);
				items.setReceivedQty(AppUtil.ifNullRetunZero(items.getDispatchedQty()));
				stockTransfer.getStockTransferItemsList().add(items);
				String bundleNumber = stockTransferItems.getBundleNumber();
				stockTransferItems = new StockTransferItems();
				stockTransferItems.setBundleNumber(bundleNumber);
			} else {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_ATNUMBER_NOT_FOUND_IN_INWARD.getCode());
				String bundleNumber = stockTransferItems.getBundleNumber();
				stockTransferItems = new StockTransferItems();
				stockTransferItems.setBundleNumber(bundleNumber);
				return null;
			}
		}

		return null;
	}

	public void removeReceiveProduct(StockTransferItemsBundles bundles) {
		log.info("removeReceiveProduct ", bundles);
		if (stockTransfer.getStockTransferItemsList() != null && !stockTransfer.getStockTransferItemsList().isEmpty()) {
			stockTransfer.getStockTransferItemsList()
					.removeIf(item -> item.getAtNumber().equals(bundles.getAtNumber()));
			removeItemFromDisplayList(bundles);
		}
	}

	public void removeItemFromDisplayList(StockTransferItemsBundles bundles) {
		if (bundleNumbersAdded != null && !bundleNumbersAdded.isEmpty()) {
			bundleNumbersAdded.removeIf(bundleNumber -> {
				if (bundles.getBundleNumber() != null)
					return bundleNumber.equals(bundles.getBundleNumber());
				else
					return false;
			});
			updateBundleNumbers(bundleNumbersAdded);
		}

		if (stockTransferItemsDisplayList != null && !stockTransferItemsDisplayList.isEmpty()) {
			StockTransferItems itemToRemove = null;
			boolean removeItemFlag = false;
			for (StockTransferItems items : stockTransferItemsDisplayList) {
				boolean isBundleRemoved = false;
				if (items.getProductVarietyMaster().getId()
						.equals(bundles.getStockTransferItems().getProductVarietyMaster().getId())) {

					if (items.getStockTransferItemsBundlesList() != null
							&& !items.getStockTransferItemsBundlesList().isEmpty()
							&& items.getStockTransferItemsBundlesList()
									.removeIf(b -> b.getAtNumber().equals(bundles.getAtNumber()))) {
						isBundleRemoved = true;
					}
					if (isBundleRemoved) {
						items.setReceivedQty(items.getReceivedQty() - 1);
						items.setItemNetTotal(items.getItemNetTotal() - bundles.getItemNetTotal());
						if (items.getReceivedQty() == 0) {
							removeItemFlag = true;
							itemToRemove = items;
						}
					}
				}
			}
			if (removeItemFlag) {
				stockTransferItemsDisplayList.remove(itemToRemove);
			}
		}

	}

	public void addItemToDisplayList(StockTransferItems items) {

		StockTransferItems stockItems = new StockTransferItems(items);

		StockTransferItemsBundles bundles = new StockTransferItemsBundles();
		bundles.setStockTransferItems(stockItems);
		bundles.setAtNumber(stockItems.getAtNumber());
		bundles.setQuantity(AppUtil.ifNullRetunZero(stockItems.getDispatchedQty()));
		bundles.setItemNetTotal(AppUtil.ifNullRetunZero(stockItems.getItemNetTotal()));
		if (stockItems.getBundleNumber() != null) {
			bundles.setBundleNumber(stockItems.getBundleNumber());
			bundleNumbersAdded.add(stockItems.getBundleNumber());
			updateBundleNumbers(bundleNumbersAdded);
		}

		if (stockTransferItemsDisplayList == null) {
			stockTransferItemsDisplayList = new ArrayList<>();
		}

		Boolean currentATnumberAddFlag = false;
		if (!stockTransferItemsDisplayList.isEmpty()) {
			stockTransferItemsDisplayList = new ArrayList<>();
			for (StockTransferItems sti : stockTransferItemsDisplayListTemp) {
				if (sti.getProductVarietyMaster().getId().longValue() == stockItems.getProductVarietyMaster().getId()
						.longValue()) {
					sti.setReceivedQty(sti.getReceivedQty() + AppUtil.ifNullRetunZero(stockItems.getDispatchedQty()));
					sti.setItemNetTotal(sti.getItemNetTotal() + AppUtil.ifNullRetunZero(bundles.getItemNetTotal()));
					sti.getStockTransferItemsBundlesList().add(bundles);
					currentATnumberAddFlag = true;
				}
				stockTransferItemsDisplayList.add(sti);
			}
			if (!currentATnumberAddFlag) {
				stockItems.getStockTransferItemsBundlesList().add(bundles);
				stockItems.setReceivedQty(AppUtil.ifNullRetunZero(stockItems.getDispatchedQty()));
				stockItems.setDispatchedQty(stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems,
						stockTransferItemsListTemp));
				stockTransferItemsDisplayList.add(stockItems);
			}
		} else {
			stockItems.getStockTransferItemsBundlesList().add(bundles);
			stockItems.setReceivedQty(AppUtil.ifNullRetunZero(stockItems.getDispatchedQty()));
			stockItems.setDispatchedQty(
					stockTransferUtility.sumOfDispatchedQtyByProductVariety(stockItems, stockTransferItemsListTemp));
			stockTransferItemsDisplayList.add(stockItems);
		}

		stockTransferItemsDisplayListTemp = new ArrayList<>(stockTransferItemsDisplayList);
	}

	public void updateBundleNumbers(Set<String> setOfBundleNumbers) {
		if (setOfBundleNumbers != null && !setOfBundleNumbers.isEmpty()) {
			stockTransfer.setBundleNumbers("");

			setOfBundleNumbers.stream().forEach(bundleNumber -> {
				if (stockTransfer.getBundleNumbers() != null && stockTransfer.getBundleNumbers().length() > 0) {
					stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + ", ");
				}
				stockTransfer.setBundleNumbers(stockTransfer.getBundleNumbers() + String.valueOf(bundleNumber));
			});

			stockTransfer.setTotalBundles(setOfBundleNumbers.size());

		}
	}

	/********************************************************************************************************************************************/

	public StockTransfer getStockTransferById(Long id) {
		/*
		 * 
		 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/
		 * controller/StockTransferController.java
		 */
		String URL = SERVER_URL + appPreference.getOperationApiUrl() + "/stock/transfer/get/" + id;
		log.info("getStockTransferById url==>" + URL);
		StockTransfer stockTransfer = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransfer = mapper.readValue(jsonValue, StockTransfer.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return stockTransfer;
	}

	public String create() {
		log.info("create method is executing..");

		String URL = SERVER_URL + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/inward/create";
		try {
			BaseDTO baseDTO = httpService.post(URL, stockTransfer);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Stock Transfer saved successfully");
					errorMap.notify(ErrorDescription.STOCK_TRANS_SAVED_SUCCESSFULLY.getCode());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public ProductVarietyMaster findProductByAtNumberAndSupplier(String atNumber, Long supplierId) {

		String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/stock/transfer/product/warehouse/product/selection/" + atNumber + "/" + supplierId;
		ProductVarietyMaster productVarietyMaster = null;
		try {

			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				if (baseDTO.getResponseContent() != null) {
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					productVarietyMaster = mapper.readValue(jsonResponse, ProductVarietyMaster.class);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("Exception", e);
		}
		return productVarietyMaster;
	}

	public String authorization() throws IOException {

		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpSession session = (HttpSession) externalContext.getSession(true);
		String contextPath = session.getServletContext().getContextPath();

		log.info("authorization called........" + loginBean.getUserFeatures());
		log.info(
				"ajax request............" + FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest());
		if (!loginBean.getUserFeatures().containsKey("PWH_STOCK_INWARD_ADD")
				&& FacesContext.getCurrentInstance().getPartialViewContext() != null
				&& !FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) {
			log.info("User dont have feature for this page..................");
			externalContext.redirect(AppUtil.getUnAuthorizedPageUrl(contextPath));
		}
		return null;
	}

}
