package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.VenueType;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.VenueMasterDTO;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.VenueMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("venueBean")
public class VenueMasterBean {
	private final String CREATE_PAGE = "/pages/masters/createVenue.xhtml?faces-redirect=true";

	private final String LIST_PAGE_URL = "/pages/masters/listVenue.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/viewVenue.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL() + "/venuemaster";

	String serverURL;
	String jsonResponse;
	ObjectMapper mapper = new ObjectMapper();
	BaseDTO baseDTO = new BaseDTO();
	@Autowired
	CommonDataService commonDataService;
	@Getter
	@Setter
	VenueMaster venueMaster;

	@Getter
	@Setter
	VenueMaster selectedVenueMaster = new VenueMaster();

	@Getter
	@Setter
	List<EntityMaster> regionList = new ArrayList<EntityMaster>();
	@Getter
	@Setter
	List<VenueType> venueTypeList = new ArrayList<VenueType>();

	@Setter
	@Getter
	List<EntityMaster> entityTypeList;
	// entityTypeList
	@Getter
	@Setter
	private String addAddressTextArea;

	@Getter
	@Setter
	VenueMasterDTO venueMasterDTO;

	@Getter
	@Setter
	private List<EntityTypeMaster> listEntityTypeMaster = new ArrayList<EntityTypeMaster>();

	@Setter
	@Getter
	List<EntityMaster> listEntityMaster = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	LazyDataModel<VenueMaster> lazyVenueMasterList;

	List<VenueMaster> venueMasterList = null;

	

	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	LazyDataModel<VenueMasterDTO> venueMasterLazyList;

	@Getter
	@Setter
	private List<StateMaster> listStateMaster = new ArrayList<StateMaster>();
	@Getter
	@Setter
	private List<DistrictMaster> listDistrictMaster = new ArrayList<DistrictMaster>();
	@Getter
	@Setter
	private List<TalukMaster> listTalukMaster = new ArrayList<TalukMaster>();
	@Getter
	@Setter
	private List<CityMaster> listCityMaster;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	AddressMaster memberaddress = new AddressMaster();

	@Getter
	@Setter
	String venueMasterAddress;

	@Getter
	@Setter
	Boolean venueMasterAddressFlag = false;

	@Getter
	@Setter
	String addressfield;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Integer listSize;
	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String gotoListPage() {
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		venueMaster = new VenueMaster();
		// disableAddButton = false;
		selectedVenueMaster = new VenueMaster();
		loadLazyList();
		return LIST_PAGE_URL;
	}

	private void loadLazyList() {
		log.info("<===== Starts VenueMasterBean.loadLazyVenueMasterList ======>");

		lazyVenueMasterList = new LazyDataModel<VenueMaster>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<VenueMaster> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/list";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						venueMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VenueMaster>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return venueMasterList;
			}

			@Override
			public Object getRowKey(VenueMaster res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VenueMaster getRowData(String rowKey) {
				for (VenueMaster venueMaster : venueMasterList) {
					if (venueMaster.getId().equals(Long.valueOf(rowKey))) {
						selectedVenueMaster = venueMaster;
						return venueMaster;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends VenueMasterBean.loadLazyVenueMasterList ======>");
	}

	public String clear() {
		log.info("<---- Start VenueMasterBean . clear ----->");
		gotoListPage();

		log.info("<---- End VenueMasterBean.clear ----->");

		return LIST_PAGE_URL;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts VenueMaster.onRowSelect ========>" + event);
		selectedVenueMaster = ((VenueMaster) event.getObject());
		log.info("selectedVenueMaster id" + selectedVenueMaster.getId());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends VenueMaster.onRowSelect ========>");
	}

	public String addVenuMaster() {
		log.info("<---- Start VenueMasterBean.addVenuMaster ---->");
		try {
			action = "Create";
			setVenueMasterAddress(null);
			venueMaster = new VenueMaster();
			venueMaster.setActiveStatus(true);
			
			regionList = commonDataService.loadRegionWiseEntityMaster();
			venueTypeList = commonDataService.getActiveVenueTypes();
			log.info("====venueTypeList========"+venueTypeList.size());
			
			// loadProductCategory();

		} catch (Exception e) {
			log.error("Error occured while redirectin", e);
		}
		return CREATE_PAGE;
	}

	public String adrressString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	public String subString(AddressMaster addressMaster) {
		String subStringAddress = "";
		try {
			log.info("======addressMaster Called========>" + addressMaster.getId());
			subStringAddress = addressMasterBean.prepareAddress(addressMaster);
			subStringAddress = subStringAddress.substring(0, 19);
			subStringAddress = subStringAddress + "........";
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return subStringAddress;
	}

	// test code

	/*
	 * public void environmentChangeListener(){ String environment =
	 * venueMaster.getEnvironment(); if(environment.equals("Internal")) {
	 * setDisplayName(true); }else { setDisplayName(false); } }
	 */

	public void createAddress() {
		log.info("<<===  selecte address button    ===>> ");

		String addressForTextArea = memberaddress.getAddressLineOne() + ", " + memberaddress.getAddressLineTwo() + ", "
				+ memberaddress.getAddressLineThree() + ", " + memberaddress.getDistrictMaster().getName() + ", "
				+ memberaddress.getStateMaster().getName() + ", Pin: " + memberaddress.getPostalCode() + " Landmark: "
				+ memberaddress.getLandmark();
		addAddressTextArea = addressForTextArea;

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('statusDialog').hide();");
	}

	/*
	 * public void addAddressfunction() { loadStateMaster(); }
	 */

	public boolean checkValidation() {
		if (venueMaster != null) {
			if (venueMaster.getCode() == null) {
				log.info("<<=======   code variable null");
				errorMap.notify(ErrorDescription.CUST_BUSINESS_TYPE_REQUIRED.getErrorCode());
				return false;
			}
			if (venueMaster.getName() == null) {
				log.info("<<=======   name variable null");
				errorMap.notify(ErrorDescription.CUST_BUSINESS_TYPE_REQUIRED.getErrorCode());
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/*
	 * public void clear() { venueMaster.setCode(""); venueMaster.setName("");
	 * venueMaster.setLname(""); addAddressTextArea = "";
	 * venueMaster.setContactName(""); venueMaster.setContactNumber("");
	 * venueMaster.setMobileNumber(""); venueMaster.setFaxNumber("");
	 * venueMaster.setEmailId(""); venueMaster.setEnvironment(""); displayName =
	 * false; listEntityMaster.clear();
	 * 
	 * addressMaster.setAddressLineOne(""); addressMaster.setAddressLineTwo("");
	 * addressMaster.setAddressLineThree("");
	 * addressMaster.setLocAddressLineOne("");
	 * addressMaster.setLocAddressLineTwo("");
	 * addressMaster.setLocAddressLineThree(""); addressMaster.setPostalCode("");
	 * addressMaster.setLandmark(""); listStateMaster.clear();
	 * listDistrictMaster.clear(); listTalukMaster.clear(); listCityMaster.clear();
	 * 
	 * }
	 */
	public void loadEntityTypeMaster() {

		try {
			/*
			 * listEntityTypeMaster = commonDataService.loadEntityTypes();
			 * listEntityMaster.clear();
			 */

			String url = serverURL + "/entitytypemaster/loadAll";
			log.info("<<=== get District URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listEntityTypeMaster = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
			});
			listEntityMaster.clear();
		} catch (Exception e) {
			log.error("<<==============  error in loadStateMaster  " + e);
		}
	}

	public void onchangeEntityMaster() {
		log.info("onchangeEntityType called..");

		try {
			log.info("Test " + venueMaster.getEntityMaster().getId());

			if (venueMaster.getEntityMaster().getName() != null) {
				Long entityTypeId = venueMaster.getEntityMaster().getId();

				String url = serverURL + "/entitymaster/getbyEntityid/" + entityTypeId;
				log.info("<<=== get District URL " + url);
				baseDTO = httpService.get(url);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				listEntityMaster = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
				});

			} else {
				log.info("entity not found.");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_ENTITY_TYPE_NOT_SELECTED.getErrorCode());
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
	}

	public void loadStateMaster() {
		try {
			String url = serverURL + "/stateMaster/getAllStates";
			log.info("URL " + url);
			baseDTO = httpService.get(url);

			log.info("check " + baseDTO);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listStateMaster = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
			});
			listDistrictMaster.clear();
			listTalukMaster.clear();
		} catch (Exception e) {
			log.error("<<==============  error in loadStateMaster  " + e);
		}
	}

	/*
	 * public void onchangeState() { long id =
	 * addressMaster.getStateMaster().getId(); try {
	 * log.info("<<===  selected state ::: " + id); String url = serverURL +
	 * "/districtMaster/getalldistrictbystate/" + id;
	 * log.info("<<=== get District URL " + url); baseDTO = httpService.get(url);
	 * jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * listDistrictMaster = mapper.readValue(jsonResponse, new
	 * TypeReference<List<DistrictMaster>>() { }); listTalukMaster.clear();
	 * log.info("<<====  Districts::: " + listDistrictMaster.size()); } catch
	 * (Exception e) { log.error("<<==============  error in onchangeState  " + e);
	 * listDistrictMaster.clear(); listTalukMaster.clear(); listCityMaster.clear();
	 * } }
	 */

	/*
	 * public void onchangeDistrict(Long id) {
	 * log.info("<<===  selected district ::: " + id); if (id == null) {
	 * listTalukMaster.clear(); listCityMaster.clear(); } else { try { String url =
	 * serverURL + "/city/getactivecitiesbydistrict/" + id;
	 * log.info("<<=== get District URL " + url); baseDTO = httpService.get(url);
	 * jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
	 * listCityMaster = mapper.readValue(jsonResponse, new
	 * TypeReference<List<CityMaster>>() { }); url = serverURL +
	 * "/taluk/getbydistrict/" + id; baseDTO = httpService.get(url); jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContents()); listTalukMaster =
	 * mapper.readValue(jsonResponse, new TypeReference<List<TalukMaster>>() { });
	 * log.info("<<====  Cisties::: " + listCityMaster.size());
	 * log.info("<<====  Taluks::: " + listTalukMaster.size()); } catch (Exception
	 * e) { log.error("<<==============  error in onchangeDistrict  " + e);
	 * listTalukMaster.clear(); listCityMaster.clear(); } } }
	 */
	public String viewVenuMaster() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedVenueMaster == null || selectedVenueMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_VENUE_MASTER.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedVenueMaster.getId());

		selectedVenueMaster = getVenueMasterDetails(selectedVenueMaster.getId());

		log.info("selected VenuMaster Object:::" + selectedVenueMaster);
		return VIEW_PAGE_URL;
	}

	public String editVenuMaster() {

		action = "Edit";
		log.info("<----Redirecting to user view page---->");
		if (selectedVenueMaster == null || selectedVenueMaster.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_VENUE_MASTER.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedVenueMaster.getId());
		regionList = commonDataService.loadRegionWiseEntityMaster();
		venueTypeList = commonDataService.getActiveVenueTypes();

		selectedVenueMaster = getVenueMasterDetails(selectedVenueMaster.getId());
		if (selectedVenueMaster != null) {
			venueMaster = selectedVenueMaster;
			venueMasterAddress = addressMasterBean.prepareAddress(selectedVenueMaster.getAddressId());
		}

		log.info("selected VenuMaster Object:::" + selectedVenueMaster);
		return CREATE_PAGE;
	}

	private VenueMaster getVenueMasterDetails(Long id) {
		log.info("<====== VenueMasterBean.getVenueMasterBeanDetails starts====>" + id);
		try {
			if (selectedVenueMaster == null || selectedVenueMaster.getId() == null) {
				log.error(" No VenueMaster selected ");
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICATION_NO_SELECTED.getErrorCode());
			}
			log.info("Selected VenueMaster  Id : : " + selectedVenueMaster.getId());
			String url = SERVER_URL + "/get/" + selectedVenueMaster.getId();

			log.info("VenueMaster Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			VenueMaster venueMasterDetail = mapper.readValue(jsonResponse, VenueMaster.class);

			if (venueMasterDetail != null) {
				venueMaster = venueMasterDetail;
				addAddressTextArea = addressMasterBean.prepareAddress(venueMaster.getAddressId());
			} else {
				log.error("VenueMaster object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" VenueMaster Retrived  SuccessFully ");
			} else {
				log.error(" VenueMaster Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return venueMaster;
		} catch (Exception e) {
			log.error("Exception Ocured while get VenueMaster Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String deleteVenueMaster() {
		try {
			action = "DELETE";
			if (selectedVenueMaster == null || selectedVenueMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_VENUE_MASTER.getErrorCode());
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteVenueMasterConfirm() {
		log.info("<===== Starts VenueMasterBean.deleteVenueMasterConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + "/delete/" + selectedVenueMaster.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.VENUE_MASTER_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete VenueMaster ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends VenueMasterBean.deleteVenueMasterConfirm ===========>");
		return gotoListPage();
	}

	public String editVenueMaster() {
		log.info("<<<<< -------Start editVenueMaster called ------->>>>>> ");

		try {
			if (selectedVenueMaster == null || selectedVenueMaster.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_TAPAL_FOR.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("Edit")) {
				log.info("edit VenueMaster called..");
				String url = SERVER_URL + "/get/" + selectedVenueMaster.getId();

				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					venueMaster = mapper.readValue(jsonResponse, new TypeReference<VenueMaster>() {
					});

					return CREATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in editVenueMaster ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends VenueMaster.editVenueMaster ===========>" + action);
		return null;
	}

	public String backPage() {

		gotoListPage();
		return LIST_PAGE_URL;
	}

	public void openVenueMasterAddress() {
		log.info(" Inside openVenueMasterAddress " + addressMasterBean.getAddressMaster());
		if (venueMaster.getAddressId() != null && venueMaster.getAddressId().getStateMaster() != null) {
			addressMasterBean.openAddressDialog();
			addressMasterBean.setAddressMaster(venueMaster.getAddressId());
			addressMasterBean.loadDistrict();
			addressMasterBean.loadTaluk();
			addressMasterBean.loadCity();

		} else {
			addressMasterBean.getAddressMaster().setAddressLineOne(null);
			addressMasterBean.getAddressMaster().setAddressLineTwo(null);
			addressMasterBean.getAddressMaster().setAddressLineThree(null);
			addressMasterBean.getAddressMaster().setLocAddressLineOne(null);
			addressMasterBean.getAddressMaster().setLocAddressLineTwo(null);
			addressMasterBean.getAddressMaster().setLocAddressLineThree(null);
			addressMasterBean.getAddressMaster().setLandmark(null);
			addressMasterBean.getAddressMaster().setPostalCode(null);
			addressMasterBean.getAddressMaster().setCountryMaster(null);
			addressMasterBean.getAddressMaster().setStateMaster(null);
			addressMasterBean.getAddressMaster().setDistrictMaster(null);
			addressMasterBean.getAddressMaster().setCityMaster(null);
			addressMasterBean.getAddressMaster().setTalukMaster(null);
			addressMasterBean.openAddressDialog();
		}
		venueMasterAddressFlag = true;

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').show();");
		context.update("addressDialogForm");
	}

	public void saveAddress() {
		log.info(" Inside saveAddress " + addressMasterBean.getAddressMaster());
		log.info("venueMasterAddressFlag ::\t" + venueMasterAddressFlag);
		if (venueMasterAddressFlag) {
			venueMaster.setAddressId(addressMasterBean.getAddressMaster());
			venueMasterAddress = addressMasterBean.prepareAddress(addressMasterBean.getAddressMaster());
		}

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addressDialog').hide();");
		context.update("addForm");
		venueMasterAddressFlag = false;
	}

	public String submit() {
		log.info("<=======Starts  VenueMasterBean.submit ======>");

		if (venueMaster.getAddressId() == null || venueMaster.getCode().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.VENUE_MASTER_ADDRESS_NULL.getErrorCode());
			return null;
		}
		if(StringUtils.isNotEmpty(venueMaster.getEmailId())) {
			if (!EmployeeValidator.validateEmail(venueMaster.getEmailId())) {
				log.error("  Invalid Email Id ");
				errorMap.notify(ErrorDescription.INAVALID_SUPPORT_EMAIL.getErrorCode());
				return null;
			}
		}
		

		if (action.equalsIgnoreCase("Create")) {
			venueMaster.setCreatedBy(loginBean.getUserDetailSession());
			EmployeeMaster employeeMaster = loginBean.getEmployee();
			log.info("entity id--------------" + employeeMaster);
			venueMaster.setEntityMaster(employeeMaster.getPersonalInfoEmployment().getWorkLocation());
			log.info("entity id-------------" + venueMaster.getEntityMaster().getId());
		} else if (action.equalsIgnoreCase("Edit")) {
			venueMaster.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response = httpService.post(SERVER_URL + "/create", venueMaster);
		// errorMap.notify(response.getStatusCode());
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("Create"))
				errorMap.notify(ErrorDescription.VENUE_MASTER_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.VENUE_MASTER_UPDATE_SUCCESS.getErrorCode());
			// return showListQualificationPage();
			return gotoListPage();
		} else if (response.getStatusCode() == ErrorDescription.SALESTYPE_NAME_ALREADY_EXIST.getErrorCode()) {
			errorMap.notify(ErrorDescription.SALESTYPE_NAME_ALREADY_EXIST.getErrorCode());
		}

		else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends VenueMasterBean.submit ======>");
		return null;
	}

}
