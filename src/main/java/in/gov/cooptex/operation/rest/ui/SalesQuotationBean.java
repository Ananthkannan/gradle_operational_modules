package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.model.ModeOfEnquiry;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CustomerQuotationRequestDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SalesQuotationDetailsDTO;
import in.gov.cooptex.core.dto.SalesQuotationProductDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.PlanStage;
import in.gov.cooptex.operation.enums.QuotationStatus;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.QuotationLog;
import in.gov.cooptex.operation.model.QuotationNote;
import in.gov.cooptex.operation.production.dto.QuotationRequest;
import in.gov.cooptex.operation.production.dto.QuotationResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("salesQuotationBean")
@Scope("session")
public class SalesQuotationBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String LIST_PAGE_URL = "/pages/operation/productionplan/sales/listSalesQuotation.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/productionplan/sales/createSalesQuotation.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/productionplan/sales/viewSalesQuotation.xhtml?faces-redirect=true;";

	private final String PREVIEW_PAGE_URL = "/pages/operation/productionplan/sales/previewSalesQuotation.xhtml?faces-redirect=true;";

	private final String PREVIEW_VIEW_PAGE_URL = "/pages/operation/previewViewQuotation.xhtml?faces-redirect=true;";

	@Autowired
	LanguageBean languageBean;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String approveComments;

	@Getter
	@Setter
	String rejectComments;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	EntityMaster headOffice;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	Quotation quotation = new Quotation();

	@Getter
	@Setter
	QuotationResponse quotationResponse;

	@Getter
	@Setter
	String[] headOfficeAddress;

	@Getter
	@Setter
	String[] customerAddress;

	@Getter
	@Setter
	String[] deliveryAddress;

	@Getter
	@Setter
	String action;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<CustomerMaster> customerMasterList;

	@Getter
	@Setter
	CustomerTypeMaster customerTypeMaster;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<SalesQuotationProductDTO> tempList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	LazyDataModel<QuotationResponse> quotationResponseLazyList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	Double cgstTaxValue;

	@Getter
	@Setter
	Double sgstTaxValue;

	@Getter
	@Setter
	Double totalCgstSgstTaxValue;

	@Getter
	@Setter
	Double totalValueProductVariety;

	@Getter
	@Setter
	Double totalTaxMaterialAmount;

	@Getter
	@Setter
	Double totalTaxRoundAmount;

	@Getter
	@Setter
	Double totalTaxRoundMaterialAmount;

	@Getter
	@Setter
	Double totalUnitPriceSum;

	@Getter
	@Setter
	Double discountSum;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	QuotationNote quotationNote = new QuotationNote();

	@Getter
	@Setter
	QuotationNote lastNote = new QuotationNote();

	@Getter
	@Setter
	List<QuotationNote> noteList;

	@Getter
	@Setter
	List<ModeOfEnquiry> modeOfEnquiryList;

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	private String currentPlanStatus = "SUBMITTED";

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Map<String, Object> filterMap;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	CustomerMaster customerMaster = new CustomerMaster();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	Double profitMargin;

	@Getter
	@Setter
	EntityMaster loggedInEntity;

	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeMasterList;

	@Getter
	@Setter
	CustomerQuotationRequestDTO customerQuotationRequest;

	@Getter
	@Setter
	Double purchasePrice;

	@Getter
	@Setter
	Double retailPrice;

	@Getter
	@Setter
	Double supplierRate;

	@Getter
	@Setter
	List<Double> supplierValueList;

	@Getter
	@Setter
	Double discountValue;

	@Getter
	@Setter
	int indexId;

	@Getter
	@Setter
	Boolean editFlag = true, viewFlag = true, deleteFlag = true;

	public SalesQuotationBean() {
		log.info("Inside FreeDistributionSystemBean()>>>>>>>>>");
	}

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>");
		loadStatusValues();
		addButtonFlag = true;
		editFlag = true;
		viewFlag = true;
		deleteFlag = true;
	}

	@Getter
	@Setter
	List statusValues;

	private void loadStatusValues() {
		Object[] statusArray = QuotationStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status);
		}

		log.info("<--- statusValues ---> " + statusValues);
	}

	public String getAddPage() {
		customerQuotationRequest = new CustomerQuotationRequestDTO();
		tempList = new ArrayList<>();
		supplierValueList = new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		totalTaxMaterialAmount = null;
		totalValueProductVariety = null;
		totalTaxRoundAmount = null;
		totalTaxRoundMaterialAmount = null;
		discountSum = null;
		totalUnitPriceSum = null;
		quotation = new Quotation();
		quotationNote = new QuotationNote();
		getAllCustomer();
		// loadForwardToUsers();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.SALES_QUOTATION.toString());
		setLoggedInUser();
		loadEmployeeLoggedInUserDetails();
		loadAllModeOfEnquiry();
		discountValue = null;
		supplierRate = null;
		retailPrice = null;
		purchasePrice = null;
		profitMargin = null;
		productCategoryList = commonDataService.loadProductCategories();
		return ADD_PAGE_URL;
	}

	public void getAllCustomer() {

		log.info("getAllCustomer ()");

		customerTypeMasterList = commonDataService.getAllCustomerTypes();
		// loadAllCustomerType();

		log.info("getAllCustomer ()", customerTypeMasterList);

	}

	public void getNote() {
		log.info("==================test======================= " + quotationNote.getNote());
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + quotationNote.getNote());
		log.info("=========================================");
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;

		try {

			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadAllModeOfEnquiry() {
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/getmodeofenquiry";
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();

			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

			modeOfEnquiryList = mapper.readValue(jsonResponse, new TypeReference<List<ModeOfEnquiry>>() {
			});

		} catch (Exception e) {
			log.error("Exception in loadHeadOffice ", e);
		}
	}

	private List<CustomerTypeMaster> loadAllCustomerType() {

		log.info("Loading all customer types............");

		List<CustomerTypeMaster> customerTypeMasterList = null;

		try {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/contract/plan/customertype";

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerTypeMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerTypeMaster>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerTypeMasterList;

	}

	public Date getCurrentDate() {
		return new Date();
	}

	public void setLoggedInUser() {
		BaseDTO baseDTO = new BaseDTO();

		if (loginBean.getUserProfile() != null && (EmployeeMaster) loginBean.getUserProfile() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment() != null
				&& ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
						.getWorkLocation() != null) {
			loggedInEntity = ((EmployeeMaster) loginBean.getUserProfile()).getPersonalInfoEmployment()
					.getWorkLocation();
		}

		if (loggedInEntity == null) {

			try {
				String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/sales/quotation/getentitybyuser/" + loginBean.getUserMaster().getId();
				baseDTO = httpService.get(URL);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					loggedInEntity = mapper.readValue(jsonValue, EntityMaster.class);
				}
			} catch (Exception e) {

				log.error("Exception occured  setLoggedInUser.... ", e);

			}
		}

	}

	public String getListPage() {

		loadStatusValues();

		loadLazyList();
		addButtonFlag = true;
		return LIST_PAGE_URL;
	}

	public void onSelectCustomerTypeMaster() {
		log.info("SalesQuotationBean :: onSelectCustomerTypeMaster");
		try {
			if (customerQuotationRequest == null && customerQuotationRequest.getCustomerTypeMaster() == null
					&& customerQuotationRequest.getCustomerTypeMaster().getId() == null) {
				AppUtil.addError("Customer type master not found");
				return;
			}
			customerMasterList = new ArrayList<>();
			log.info("customer type master id==> " + customerQuotationRequest.getCustomerTypeMaster().getId());
			/*
			 * customerMasterList = loadAllCustomerByCustomerType(
			 * customerQuotationRequest.getCustomerTypeMaster().getId());
			 */
			onSelectCustomerMaster();
			/*
			 * if (customerMasterList != null && !customerMasterList.isEmpty()) {
			 * log.info("Customer master list id==> " + customerMasterList.size());
			 * 
			 * } else { AppUtil.addError("Customer master not found"); return; }
			 */
		} catch (Exception e) {
			log.error("SalesQuotationBean :: onSelectCustomerTypeMaster exception ==> ", e);
		}
	}

	public void onSelectCustomerMaster() {
		log.info("SalesQuotationBean :: onSelectCustomerMaster");
		try {
			if (customerQuotationRequest == null && customerQuotationRequest.getCustomerMaster() == null) {
				AppUtil.addError("Customer master not found");
				return;
			}
			log.info("customer master id==> " + customerQuotationRequest.getCustomerMaster().getId());
			// CustomerMaster customerMaster=new CustomerMaster();
			if (customerQuotationRequest.getCustomerMaster() != null
					&& customerQuotationRequest.getCustomerMaster().getId() != null) {
				customerMaster = getCustomerDetails(customerQuotationRequest.getCustomerMaster().getId());
			}
			customerQuotationRequest.setCustomerMaster(customerMaster);
			quotation.setCustomer(customerMaster);
		} catch (Exception e) {
			log.error("SalesQuotationBean :: onSelectCustomerMaster exception ==> ", e);
		}
	}

	private List<CustomerMaster> loadAllCustomerByCustomerType(Long customerTypeId) {

		log.info("Loading all customer ............");

		List<CustomerMaster> customerMasterList = null;

		try {

			/*
			 * String url = appPreference.getPortalServerURL() +
			 * appPreference.getOperationApiUrl() + "/contract/plan/customer/" +
			 * customerTypeId;
			 */
			// String url = appPreference.getPortalServerURL() +
			// appPreference.getOperationApiUrl()
			// + "/customerMaster/customerlist/" + customerTypeId;

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/loadcustomerlist/" + customerTypeId;
			log.info(" =====> Post Request To controller inside Method loadAllCustomerByCustomerType==== >>>>");
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				log.info(" =====> Loaded all customer data === > Converting to json == >>>>");
				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
				});
				if (customerMasterList != null && !customerMasterList.isEmpty()) {
					log.info("customerMasterList size==> " + customerMasterList.size());
				}
				log.info(" =====> Exit loadAllCustomerByCustomerType ==== >>>>");

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerMasterList;

	}

	public void processProductGroups() {
		log.info("Inside processProductGroups");
		log.info("Selected Product Category " + customerQuotationRequest.getProductCategory().getProductCatCode());
		productGroupList = commonDataService.loadProductGroups(customerQuotationRequest.getProductCategory());
		log.info("productGroupList " + productGroupList);
	}

	public void processProductGroup() {
		log.info("Inside processProductGroup");

		customerQuotationRequest.setProductVarietyMaster(null);
		customerQuotationRequest.setQuantity(null);

		if (customerQuotationRequest != null && customerQuotationRequest.getProductGroupMaster() != null) {
			log.info("Selected Product Group " + customerQuotationRequest.getProductGroupMaster().getCode());
			productList = commonDataService.loadProductVarieties(customerQuotationRequest.getProductGroupMaster());
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public String loadPreviewDetails() {
		String quotationNumberString = "";
		Quotation quotationObj = new Quotation();
		;
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/loadpreviewdetails/" + loggedInEntity.getCode();
			BaseDTO baseDTO = httpService.get(URL);

			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotationObj = mapper.readValue(jsonValue, Quotation.class);
			quotation.setQuotationNumber(quotationObj.getQuotationNumber());
			quotation.setQuotationNumberPrefix(quotationObj.getQuotationNumberPrefix());
			quotation.setEmployeeName(quotationObj.getEmployeeName());
		} catch (Exception e) {
			log.error("Exception in generateQuotationNumber ", e);
		}

		return quotationNumberString;
	}

	public void clear() {
		log.info("clear method starts----------");
		customerQuotationRequest = new CustomerQuotationRequestDTO();
		addButtonFlag = true;
		editFlag = true;
		viewFlag = true;
		deleteFlag = true;
		productCategoryList = new ArrayList<>();
		productCategoryList = commonDataService.loadProductCategories();
		productGroupList = new ArrayList<>();
		productList = new ArrayList<>();
		discountValue = null;
		supplierRate = null;
		setSupplierRate(null);
		retailPrice = null;
		purchasePrice = null;
		profitMargin = null;
		log.info("clear method ends----------");
	}

	public String editQuotation() {
		log.info("SupplyRateConfirmationBean editQuotation method started");
		customerQuotationRequest = new CustomerQuotationRequestDTO();
		supplierValueList = new ArrayList<>();
		getAllCustomer();
		loadEmployeeLoggedInUserDetails();
		loadAllModeOfEnquiry();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.SALES_QUOTATION.toString());
		productCategoryList = commonDataService.loadProductCategories();

		if (quotationResponse == null || quotationResponse.getId() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE.getCode());
			return null;
		}

		if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.SUBMITTED)) {
			errorMap.notify(ErrorDescription.SUBMITTED_QUOTATION_CANNOT_EDITED.getCode());
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.APPROVED)) {
			errorMap.notify(ErrorDescription.APPROVED_QUOTATION_CANNOT_EDITED.getCode());
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		if (quotationResponse == null || quotationResponse.getStatus().equals(QuotationStatus.REJECTED)) {
			errorMap.notify(ErrorDescription.APPROVED_QUOTATION_CANNOT_EDITED.getCode());
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/sales/quotation/get/"
				+ quotationResponse.getId();

		BaseDTO baseDTO = new BaseDTO();

		try {

			baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();

			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

			quotation = mapper.readValue(jsonValue, Quotation.class);

			/*
			 * if(quotation.getQuotationLogList()!=null) { Long quotationNoteId=0L;
			 * List<QuotationNote> quotationNoteList=quotation.getQuotationNoteList();
			 * List<Long> quotationIdList=new ArrayList(); if(quotationNoteList!=null) {
			 * for(QuotationNote qu:quotationNoteList) { quotationIdList.add(qu.getId()); }
			 * quotationNoteId=Collections.max(quotationIdList);
			 * log.info("maximum selected quotationNoteID is:"+quotationNoteId);
			 * for(QuotationNote qu:quotationNoteList) { Long quoId=qu.getId();
			 * if(quoId==quotationNoteId) { quotationNote.setUserMaster(qu.getUserMaster());
			 * quotationNote.setFinalApproval(qu.getFinalApproval());
			 * quotationNote.setNote(qu.getNote());
			 * log.info("selected finalApproval:"+qu.getFinalApproval()); } } }
			 * 
			 * }
			 */

			if (quotation.getQuotationNoteList() != null) {
				QuotationNote lastNote = quotation.getQuotationNoteList()
						.get(quotation.getQuotationNoteList().size() - 1);
				log.info("lastNote ====>" + lastNote.getNote());
				quotationNote.setUserMaster(lastNote.getUserMaster());
				quotationNote.setFinalApproval(lastNote.getFinalApproval());
				quotationNote.setNote(lastNote.getNote());
			}

			for (QuotationItem item : quotation.getQuotationItemList()) {
				item.setTotalUnitPrice(item.getUnitRate() * item.getItemQuantity());
				supplierValueList.add(item.getUnitRate() * item.getItemQuantity());
			}

			customerQuotationRequest.setCustomerTypeMaster(quotation.getCustomer().getCustomerTypeMaster());

			customerQuotationRequest.setQuotationItemList(quotation.getQuotationItemList());
			customerQuotationRequest.setCustomerMaster(quotation.getCustomer());

			onSelectCustomerTypeMaster();

			loadTaxDetails(customerQuotationRequest);

		} catch (Exception e) {
			log.error("Exception ", e);
		}

		return ADD_PAGE_URL;

	}

	public String processDelete() {
		log.info("SupplyRateConfirmationBean processDelete method started");
		if (quotationResponse == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE.getCode());
			return null;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
		return null;
	}

	public String viewQuotation() {
		log.info("SupplyRateConfirmationBean viewQuotation method started");
		customerQuotationRequest = new CustomerQuotationRequestDTO();

		if (quotationResponse == null || quotationResponse.getId() == null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_ANY_ONE.getCode());
			return null;
		}

		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/sales/quotation/get/"
				+ quotationResponse.getId();

		BaseDTO baseDTO = new BaseDTO();
		supplierValueList = new ArrayList<>();
		try {

			baseDTO = httpService.get(url);

			ObjectMapper mapper = new ObjectMapper();

			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());

			quotation = mapper.readValue(jsonValue, Quotation.class);

			forwardToUsersList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.SALES_QUOTATION.toString());

			for (QuotationItem item : quotation.getQuotationItemList()) {
				item.setTotalUnitPrice(item.getUnitRate() * item.getItemQuantity());
				Double supplierValue = (item.getTotalUnitPrice() - item.getDiscountValue());
				supplierValueList.add(supplierValue);
			}

			customerQuotationRequest.setCustomerMaster(quotation.getCustomer());

			customerQuotationRequest.setCustomerTypeMaster(quotation.getCustomer().getCustomerTypeMaster());

			customerQuotationRequest.setQuotationItemList(quotation.getQuotationItemList());

			onSelectCustomerTypeMaster();
			loadTaxDetails(customerQuotationRequest);

			noteList = quotation.getQuotationNoteList();

			Collections.sort(noteList, new ComparatorId());

			lastNote = noteList.get(noteList.size() - 1);

			quotationNote.setFinalApproval(lastNote.getFinalApproval());

			quotationNote.setNote(lastNote.getNote());

			if (lastNote.getUserMaster() != null
					&& lastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId().longValue()) {
				enableFlag = true;
			}

			eventLogList = new ArrayList<>();

			approvedEventLogList = new ArrayList<>();

			rejectedEventLogList = new ArrayList<>();

			if (quotation.getQuotationLogList() != null && !quotation.getQuotationLogList().isEmpty()) {
				for (QuotationLog planLog : quotation.getQuotationLogList()) {
					EventLog eventLog = new EventLog("", planLog.getCreatedByName(), planLog.getCreatedByDesignation(),
							planLog.getCreatedDate(), planLog.getRemarks());
					if (planLog.getStage().equals(PlanStage.INITIATED.toString())
							|| planLog.getStage().equals(PlanStage.SUBMITTED.toString())) {
						eventLog.setTitle("Created By");
					} else if (planLog.getStage().equals(PlanStage.APPROVED.toString())) {
						eventLog.setTitle("Approved By");
						approvedEventLogList.add(eventLog);
					} else if (planLog.getStage().equals(PlanStage.REJECTED.toString())) {
						eventLog.setTitle("Rejected By");
						rejectedEventLogList.add(eventLog);
					} else if (planLog.getStage().equals(QuotationStatus.FINAL_APPROVED.toString())) {
						eventLog.setTitle("Final Approved By");
						approvedEventLogList.add(eventLog);
					}
					eventLogList.add(eventLog);
				}
			}

			log.info("eventLogList", eventLogList);

			Collections.sort(eventLogList, new Comparator<EventLog>() {
				public int compare(EventLog m1, EventLog m2) {
					return m1.getTitle().compareTo(m2.getTitle());
				}
			});

			if (quotation.getStatus().equals(QuotationStatus.FINAL_APPROVED)
					|| quotation.getStatus().equals(QuotationStatus.REJECTED)) {
				enableFlag = false;
			}

		} catch (Exception e) {
			log.error("Exception ", e);
		}

		return VIEW_PAGE_URL;

	}

	static class ComparatorId implements Comparator<QuotationNote> {
		@Override
		public int compare(QuotationNote obj1, QuotationNote obj2) {
			return obj1.getId().compareTo(obj2.getId());
		}
	}

	public void onRowSelect(SelectEvent event) {
		log.info("SupplyRateConfirmationBean onRowSelect method started");
		quotationResponse = ((QuotationResponse) event.getObject());
		log.info("quotationResponse.getStatus==> " + quotationResponse.getStatus());

		if (quotationResponse.getStatus().equals(QuotationStatus.INITIATED)) {
			addButtonFlag = true;
			editFlag = true;
			viewFlag = true;
			deleteFlag = true;
		} else if (quotationResponse.getStatus().equals(QuotationStatus.SUBMITTED)) {
			addButtonFlag = true;
			editFlag = false;
			viewFlag = true;
			deleteFlag = true;
		} else if (quotationResponse.getStatus().equals(QuotationStatus.APPROVED)) {
			addButtonFlag = true;
			editFlag = false;
			viewFlag = true;
			deleteFlag = false;
		} else if (quotationResponse.getStatus().equals(QuotationStatus.REJECTED)) {
			addButtonFlag = true;
			editFlag = true;
			viewFlag = true;
			deleteFlag = true;
		} else if (quotationResponse.getStatus().equals(QuotationStatus.FINAL_APPROVED)) {
			addButtonFlag = true;
			editFlag = false;
			viewFlag = true;
			deleteFlag = false;
		}

	}

	public String gotoPreview() {
		log.info("note =====> " + quotationNote.getNote());
		if (quotation.getValidDate() == null) {
			log.info("Quotation valid date is Null");
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_VALID_DATE_NOT_NULL.getCode());
			return null;
		}
		if (quotationNote.getNote() == null) {
			errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		if (quotation.getQuotationItemList() == null || quotation.getQuotationItemList().size() == 0) {
			AppUtil.addWarn("Please enter the product details");
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		setLoggedInUser();
		loadPreviewDetails();

		if (headOffice != null) {
			headOfficeAddress = AppUtil.prepareAddress(headOffice.getAddressMaster());

		}
		if (customerQuotationRequest.getCustomerMaster() != null
				&& customerQuotationRequest.getCustomerMaster().getBillingAddressMaster() != null) {
			customerAddress = AppUtil
					.prepareAddress(customerQuotationRequest.getCustomerMaster().getBillingAddressMaster());

		}
		if (customerQuotationRequest.getCustomerMaster() != null
				&& customerQuotationRequest.getCustomerMaster().getShippingAddressMaster() != null) {
			deliveryAddress = AppUtil
					.prepareAddress(customerQuotationRequest.getCustomerMaster().getShippingAddressMaster());

		}

		return PREVIEW_PAGE_URL;
	}

	public String gotoPreviewView() {

		loadPreviewDetails();

		headOfficeAddress = AppUtil.prepareAddress(headOffice.getAddressMaster());

		customerAddress = AppUtil.prepareAddress(quotation.getCustomer().getBillingAddressMaster());

		deliveryAddress = AppUtil.prepareAddress(quotation.getCustomer().getShippingAddressMaster());

		return PREVIEW_VIEW_PAGE_URL;
	}

	public void loadLazyList() {

		log.info("SupplyRateConfirmationBean loadLazyDraftProductionList method started");
		quotationResponseLazyList = new LazyDataModel<QuotationResponse>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<QuotationResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<QuotationResponse> data = new ArrayList<QuotationResponse>();
				try {

					filterMap = filters;
					sortingOrder = sortOrder;
					sortingField = sortField;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<QuotationResponse>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(QuotationResponse res) {
				return res != null ? res.getId() : null;
			}

			@SuppressWarnings("unchecked")
			@Override
			public QuotationResponse getRowData(String rowKey) {
				List<QuotationResponse> responseList = (List<QuotationResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (QuotationResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/user/getallforwardtousers";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				forwardToUsersList = mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("SupplyRateConfirmationBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		quotationResponse = new QuotationResponse();

		QuotationRequest request = new QuotationRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue().toString();

			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());

			if (key.equals("quotationNumberPrefixOrNumber")) {
				log.info("quotationNumber : " + value);
				request.setQuotationNumberPrefixOrNumber(value);
			}
			if (key.equals("customerTypeName")) {
				log.info("customerTypeName : " + value);
				request.setCustomerCodeOrType(value);
			}
			if (key.equals("customerName")) {
				log.info("customerName : " + value);
				request.setCustomerName(value);
			}
			if (key.equals("netTotal")) {
				log.info("netTotal : " + value);
				request.setNetTotal(value);
			}
			if (key.equals("validDate")) {
				log.info("validDate : " + value);
				request.setValidDate(AppUtil.serverDateFormat(value));
			}
			if (key.equals("status")) {
				log.info("status : " + value);
				request.setStatus(value);
			}
		}
		try {
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("SupplyRateConfirmationBean.getSearchData Method Complted");
		return baseDTO;
	}

	public String addProduct() {

		if (checkProductExists(quotation.getQuotationItemList(),
				customerQuotationRequest.getProductVarietyMaster().getId())) {
			log.info("Product Already Exists ");
			errorMap.notify(ErrorDescription.PRODUCT_ALREADY_EXISTS.getCode());
			return null;
		}

		tempList = new ArrayList<>();
		if (supplierValueList == null) {
			supplierValueList = new ArrayList<>();
		}

		SalesQuotationProductDTO salesQuotationProductDTOTemp = new SalesQuotationProductDTO();

		salesQuotationProductDTOTemp.setProductVarietyId(customerQuotationRequest.getProductVarietyMaster().getId());

		salesQuotationProductDTOTemp.setUnit(customerQuotationRequest.getQuantity());
		tempList.add(salesQuotationProductDTOTemp);

		customerQuotationRequest.setProductList(tempList);

		SalesQuotationDetailsDTO salesQuotationDetailsDTO = new SalesQuotationDetailsDTO();
		SalesQuotationProductDTO salesQuotationProductDTO = new SalesQuotationProductDTO();

		if (customerQuotationRequest != null && customerQuotationRequest.getProductVarietyMaster() != null) {

			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/getproductdetails";
			BaseDTO baseDTO = null;
			try {
				baseDTO = httpService.post(url, customerQuotationRequest);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					salesQuotationDetailsDTO = mapper.readValue(jsonValue, SalesQuotationDetailsDTO.class);
					salesQuotationProductDTO = salesQuotationDetailsDTO.getSalesQuotationProductDTO();
				}

				if (salesQuotationProductDTO.getHsnCode() == null) {
					log.info("Hsn Code Null");
					errorMap.notify(ErrorDescription.HSN_CODE_NOT_CONFIGURED.getCode());
					return null;
				}
				if (salesQuotationProductDTO.getUnitPrice() == null) {
					log.info("Unit Price Null");
					errorMap.notify(ErrorDescription.PRODUCT_PRICE_NOT_CONFIGURED.getCode());
					return null;
				}
				if (salesQuotationProductDTO.getTaxPercent() == null) {
					log.info("Tax Percent Null");
					errorMap.notify(ErrorDescription.PRODUCT_TAX_DETAILS_NOT_CONFIGURED.getCode());
					return null;
				}

			} catch (Exception exp) {
				log.error("Error in addProduct create ", exp);
				errorMap.notify(24010);
			}

			QuotationItem item = new QuotationItem();
			try {
				item.setProductVarietyMaster(customerQuotationRequest.getProductVarietyMaster());
				item.getProductVarietyMaster().setHsnCode(salesQuotationProductDTO.getHsnCode());
				item.setItemQuantity(customerQuotationRequest.getQuantity());
				item.setUnitRate(supplierRate);
				item.setItemAmount(retailPrice);
				item.setQuotation(new Quotation());
				item.setTotalUnitPrice(item.getItemQuantity() * item.getItemAmount());
				Double disPer = Math.floor(customerQuotationRequest.getDiscountPercent());
				item.setDiscountPercent(disPer);
				// Double discountValue = Math.ceil((item.getTotalUnitPrice() *
				// item.getDiscountPercent()) / 100);
				Double discountValue = (item.getItemAmount() - item.getUnitRate());
				item.setDiscountValue(discountValue);
				item.setTaxPercent(salesQuotationProductDTO.getTaxPercent());
				item.setTaxValue(item.getTotalUnitPrice() * item.getTaxPercent() / 100);
				// item.setTotalAmount((item.getTaxValue() + item.getTotalUnitPrice()) -
				// item.getDiscountValue());
				// Double supplierValue = (item.getTotalUnitPrice() - item.getDiscountValue());
				Double supplierValue = (item.getUnitRate() * item.getItemQuantity());
				item.setTotalAmount(supplierValue + item.getTaxValue());
				supplierValueList.add(supplierValue);
				log.info("item quotation..." + item);
			} catch (Exception e) {
				log.info("Exception while set Quotation Item....." + e);
			}
			quotation.getQuotationItemList().add(item);

			customerQuotationRequest.setQuotationItemList(quotation.getQuotationItemList());

			quotation.setCustomer(customerQuotationRequest.getCustomerMaster());

			loadTaxDetails(customerQuotationRequest);

			/*
			 * ProductCategory prodCat=new ProductCategory(); ProductGroupMaster prodGrp=new
			 * ProductGroupMaster(); ProductVarietyMaster prodVar=new
			 * ProductVarietyMaster(); Integer qty=0; Double per=0D;
			 */

			customerQuotationRequest.setProductCategory(null);
			customerQuotationRequest.setProductVarietyMaster(null);
			customerQuotationRequest.setProductGroupMaster(null);
			customerQuotationRequest.setQuantity(null);
			customerQuotationRequest.setDiscountPercent(null);
			productCategoryList = new ArrayList<>();
			productCategoryList = commonDataService.loadProductCategories();
			productGroupList = new ArrayList<>();
			productList = new ArrayList<>();
			discountValue = null;
			supplierRate = null;
			retailPrice = null;
			purchasePrice = null;
			profitMargin = null;
		}
		return null;
	}

	public String formatNumber(Double number) {
		if (number != null) {
			NumberFormat formatter = new DecimalFormat("#0.00");
			return formatter.format(number);
		}
		return null;
	}

	public void calculateTotalTaxSummary() {
		if (gstPercentageWiseDTOList.size() != 0) {
			cgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			sgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalCgstSgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
		} else {
			cgstTaxValue = null;
			sgstTaxValue = null;
			totalCgstSgstTaxValue = null;
		}
	}

	public String delete() {
		log.info("SalesQuotationBean delete method started");
		if (quotationResponse == null || quotationResponse.getId() == null) {
			return null;
		}
		if (!quotationResponse.getStatus().equals(QuotationStatus.INITIATED)) {
			errorMap.notify(ErrorDescription.CANNOT_DELETE_QUOTATION.getCode());
			return LIST_PAGE_URL;
		}
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/sales/quotation/delete/" + quotationResponse.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete quotation", exp);
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.SALES_QUOTATION_DELETED_SUCCESSFULLY.getCode());
			return LIST_PAGE_URL;
		} else {
			errorMap.notify(baseDTO.getStatusCode());
		}
		return LIST_PAGE_URL;

	}

	public void loadTaxDetails(CustomerQuotationRequestDTO customerQuotationRequestDTO) {
		String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/sales/quotation/gettaxdetails";
		BaseDTO baseDTO = null;
		ItemDataWithGstDTO quotationItemWithGstDTO = new ItemDataWithGstDTO();
		try {
			baseDTO = httpService.post(url, customerQuotationRequest);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				quotationItemWithGstDTO = mapper.readValue(jsonValue, ItemDataWithGstDTO.class);
				log.info("quotationItemWithGstDTO" + quotationItemWithGstDTO);
				gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
				gstWiseList = quotationItemWithGstDTO.getGstWiseList();
				headOffice = quotationItemWithGstDTO.getHeadOffice();
				calculateTotalTaxSummary();

				if (quotation.getQuotationItemList().size() != 0) {
					totalTaxMaterialAmount = quotation.getQuotationItemList().stream()
							.mapToDouble(QuotationItem::getTotalAmount).sum();

					totalValueProductVariety = quotation.getQuotationItemList().stream()
							.collect(Collectors.summingDouble(QuotationItem::getTotalUnitPrice));

					totalUnitPriceSum = quotation.getQuotationItemList().stream()
							.collect(Collectors.summingDouble(QuotationItem::getTotalUnitPrice));

					discountSum = quotation.getQuotationItemList().stream()
							.collect(Collectors.summingDouble(QuotationItem::getDiscountValue));

					log.info("totalTaxMaterialAmount....." + totalTaxMaterialAmount);

					if (totalTaxMaterialAmount != null) {
						totalTaxRoundMaterialAmount = (double) Math.round(totalTaxMaterialAmount);
						totalTaxRoundAmount = totalTaxRoundMaterialAmount - totalTaxMaterialAmount;
					}

					quotation.setRoundOff(totalTaxRoundAmount);
					quotation.setGrandTotal(totalTaxMaterialAmount);
					quotation.setNetTotal(totalTaxRoundMaterialAmount);

					amountInWords = AmountToWordConverter
							.converter(String.valueOf(AppUtil.ifNullRetunZero(totalTaxRoundMaterialAmount)));
				} else {
					totalTaxMaterialAmount = null;

					totalValueProductVariety = null;

					totalUnitPriceSum = null;

					discountSum = null;

					totalTaxRoundMaterialAmount = null;

					totalTaxRoundAmount = null;

					// quotation.setNetTotal(totalTaxMaterialAmount);

					quotation.setRoundOff(totalTaxRoundAmount);
					quotation.setGrandTotal(totalTaxMaterialAmount);
					quotation.setNetTotal(totalTaxRoundMaterialAmount);

					amountInWords = null;
				}

			}
		} catch (Exception exp) {
			log.error("Error in loadTaxDetails  ", exp);
		}
	}

	public boolean checkProductExists(List<QuotationItem> quotationItemList, Long productId) {
		boolean idExists = quotationItemList.stream()
				.anyMatch(t -> t.getProductVarietyMaster().getId().equals(productId));
		return idExists;
	}

	public String editRow(int index) {
		log.info("salesQuotationBean.editRow method started" + customerQuotationRequest);

		quotation.getQuotationItemList().get(index)
				.setTotalUnitPrice(quotation.getQuotationItemList().get(index).getUnitRate()
						* quotation.getQuotationItemList().get(index).getItemQuantity());

		quotation.getQuotationItemList().get(index)
				.setDiscountValue(quotation.getQuotationItemList().get(index).getTotalUnitPrice()
						* quotation.getQuotationItemList().get(index).getDiscountPercent() / 100);

		quotation.getQuotationItemList().get(index)
				.setTaxValue(quotation.getQuotationItemList().get(index).getTotalUnitPrice()
						* quotation.getQuotationItemList().get(index).getTaxPercent() / 100);

		quotation.getQuotationItemList().get(index)
				.setTotalAmount(quotation.getQuotationItemList().get(index).getTaxValue()
						+ quotation.getQuotationItemList().get(index).getTotalUnitPrice()
						- quotation.getQuotationItemList().get(index).getDiscountValue());

		totalValueProductVariety = quotation.getQuotationItemList().stream()
				.collect(Collectors.summingDouble(QuotationItem::getTotalUnitPrice));

		customerQuotationRequest.setQuotationItemList(quotation.getQuotationItemList());
		loadTaxDetails(customerQuotationRequest);
		customerQuotationRequest = new CustomerQuotationRequestDTO();
		return null;
	}

	public String deleteItem(int index) {
		log.info("salesQuotationBean.editRow method started");
		quotation.getQuotationItemList().remove(index);
		customerQuotationRequest.setQuotationItemList(quotation.getQuotationItemList());
		loadTaxDetails(customerQuotationRequest);
		AppUtil.addInfo("Product Deleted Successfully");
		return null;
	}

	public void onChangeDiscoutPercent() {
		if (customerQuotationRequest.getDiscountPercent() > 100) {
			log.error("Invalid Discount ");
			errorMap.notify(ErrorDescription.DISCONT_INVALID.getCode());
			customerQuotationRequest.setDiscountPercent(null);
		}
	}

	public String saveCustomerQuotation(String requestType) {
		log.info("salesQuotationBean.saveCustomerQuotation method started");
		if (requestType != null && requestType.equals(QuotationStatus.INITIATED.name())) {
			quotation.setStatus(QuotationStatus.INITIATED);
		} else {
			log.info("qutotaion..." + quotation);
			quotation.setStatus(QuotationStatus.SUBMITTED);
		}

		try {
			quotation.setNote(quotationNote.getNote());

			quotation.setForwardToId(quotationNote.getUserMaster().getId());

			quotation.setIsFinalApprove(quotationNote.getFinalApproval());
		} catch (Exception e) {
			log.info("Exception while quotationNote add to quotation:" + e);
		}
		String url = "";

		if (action.equals("ADD")) {
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/sales/quotation/create";
		} else if (action.equals("EDIT")) {
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl() + "/sales/quotation/update";
		}

		String urlStatus = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/sales/quotation/updatestatus";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, quotation);
			ObjectMapper mapper = new ObjectMapper();
			String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotation = mapper.readValue(jsonValue, Quotation.class);
			baseDTO = httpService.post(urlStatus, quotation);
		} catch (Exception exp) {
			log.error("Error in saveCustomerQuotation create ", exp);
			return null;
		}

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
//				errorMap.notify(ErrorDescription.SALES_QUOTATION_ADDES_SUCCESSFULLY.getCode());
				if (requestType.equals(QuotationStatus.INITIATED.name())) {
					AppUtil.addInfo("Sales Quotation saved successfully");
				} else {
					AppUtil.addInfo("Sales Quotation submitted successfully");
				}
				addButtonFlag = true;
				editFlag = true;
				viewFlag = true;
				deleteFlag = true;
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}
		return LIST_PAGE_URL;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current FDS Plan Status ---> " + currentPlanStatus);
		log.info("<--- approveComments ---> " + approveComments);
		log.info("<--- rejectComments ---> " + rejectComments);

		BaseDTO baseDTO = null;

		if (currentPlanStatus != null && currentPlanStatus.equals("APPROVED")) {
			Quotation request = new Quotation(quotation.getId(), QuotationStatus.APPROVED, approveComments);

			if (quotationNote.getUserMaster() != null) {
				request.setForwardToId(quotationNote.getUserMaster().getId());
			}
			request.setIsFinalApprove(quotationNote.getFinalApproval());
			request.setNote(quotationNote.getNote());

			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/approve";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				AppUtil.addInfo("Quotation Approved Successfully");
			}

		} else if (currentPlanStatus != null && currentPlanStatus.equals("REJECTED")) {
			Quotation request = new Quotation(quotation.getId(), QuotationStatus.REJECTED, rejectComments);
			String URL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/sales/quotation/reject";
			baseDTO = httpService.post(URL, request);
			if (baseDTO != null) {
				AppUtil.addInfo("Quotation Rejected Successfully");
			}

		}

		/*
		 * if (baseDTO != null) { errorMap.notify(baseDTO.getStatusCode()); }
		 */
		approveComments = null;
		rejectComments = null;
		addButtonFlag = true;
		editFlag = true;
		viewFlag = true;
		deleteFlag = true;
		return LIST_PAGE_URL;
	}

	public void changeCurrentStatus(String value) {

		log.info("Changing the status to " + value);

		currentPlanStatus = value;
		approveComments = null;
		rejectComments = null;
	}

	public void processProductVariety() {
		log.info("Inside processProductGroup");

		customerQuotationRequest.setQuantity(null);

		getPrices();
	}

	public void getPrices() {
		log.info("get retail and purchase details");
		BaseDTO baseDTO = null;
		CustomerQuotationRequestDTO quotationRate = new CustomerQuotationRequestDTO();
		if (customerQuotationRequest != null && customerQuotationRequest.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + customerQuotationRequest.getProductVarietyMaster().getCode());
			Long prodId = customerQuotationRequest.getProductVarietyMaster().getId();
			Long catId = customerQuotationRequest.getProductCategory().getId();
			try {
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/sales/quotation/findUnitRateByProductId/" + prodId + "/" + catId;
				baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					quotationRate = mapper.readValue(jsonValue, CustomerQuotationRequestDTO.class);

					purchasePrice = quotationRate.getPurchasePrice();
					log.info("Purchase price is...:" + purchasePrice);
					setPurchasePrice(purchasePrice != null ? purchasePrice : 0.0);
					try {
						if (customerQuotationRequest != null && customerQuotationRequest.getCustomerMaster() != null
								&& customerQuotationRequest.getCustomerMaster().getAddressMaster() != null
								&& customerQuotationRequest.getCustomerMaster().getAddressMaster()
										.getStateMaster() != null
								&& customerQuotationRequest.getCustomerMaster().getAddressMaster().getStateMaster()
										.getCode().equalsIgnoreCase("TN")) {
							retailPrice = quotationRate.getMarginInnerState();
						} else {
							retailPrice = quotationRate.getMarginOuterState();
						}
						setRetailPrice(retailPrice != null ? retailPrice : 0.0);
						retailPrice = purchasePrice + ((purchasePrice * retailPrice) / 100);
						setRetailPrice(retailPrice != null ? retailPrice : 0.0);
						log.info("retail price is...:" + retailPrice);
						customerQuotationRequest.setRetailPrice(retailPrice);
					} catch (Exception e) {
						log.info("Exception while setting margin price" + e);
					}
				} else if (baseDTO.getStatusCode().equals(ErrorDescription
						.getError(OperationErrorCode.ERROR_SALES_QUOTATION_PRODUCT_PUCHASE_PRICE).getCode())) {
					// errorMap.notify(baseDTO.getStatusCode());
					AppUtil.addWarn("Procurement costing not available");
					return;
				}
			} catch (Exception e) {
				log.info("Exception getPrice from category" + e);
			}
		}
	}

	public boolean amountValidation() {
		boolean validate = true;
		try {
			log.info("purchase price--------------" + purchasePrice);
			log.info("supplier rate--------------" + supplierRate);

			Double supplier5PercentageValue = (double) (purchasePrice / 100 * 5);
			log.info("supplier5PercentageValue-----------------" + supplier5PercentageValue);

			if (supplierRate >= Math.round(purchasePrice + supplier5PercentageValue)) {
				log.info("----------inside warning------");
				AppUtil.addWarn(" Supplier Rate Should be  Purchase Price + 5 % ");
			}

			FacesContext fc = FacesContext.getCurrentInstance();
			if (purchasePrice >= supplierRate) {
				fc.addMessage("supplierRate", new FacesMessage(ErrorCodeDescription.getDescription(290697)));
				validate = false;
			}
		} catch (Exception e) {
			log.error("exception in amountValidation------------>", e);
		}
		return validate;
	}

	public void discountRateCalculate() {
		discountValue = (retailPrice - supplierRate);
		discountValue = discountValue > 0 ? discountValue : 0;
		if (discountValue != null) {
			Double discountPercent = Math.ceil(discountValue * 100) / retailPrice;
			discountPercent = discountPercent > 0 ? discountPercent : 0;
			profitMargin = Math.ceil((supplierRate - purchasePrice) / purchasePrice);
			profitMargin = profitMargin > 0 ? profitMargin : 0;
			log.info("profitMargin " + profitMargin);
			log.info("discountPercent... " + discountPercent);
			customerQuotationRequest.setDiscountPercent(discountPercent);
		}
	}

	public Double getSalseValue(int i) {
		Double val = 0D;
		try {
			if (supplierValueList != null && supplierValueList.get(i) > 0)
				val = supplierValueList.get(i);
		} catch (Exception e) {
			log.info("Excepion getSalesValue...." + e);
		}
		return val;
	}

	public void showDeleteConfirmationProductDetails(int index) {
		log.info("============>Start showDeleteConfirmationProductDetails Method=========>");
		try {
			indexId = index;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('deletedialog').show();");
		} catch (Exception ex) {
			log.error("Error in showDeleteConfirmationProductDetails Method", ex);
		}
		log.info("============>End showDeleteConfirmationProductDetails Method=========>");
	}

	public List<CustomerMaster> loadCustomerByName(String query) {
		log.info("Loading all customer ............");

		// List<CustomerMaster> customerMasterList = null;

		try {
			/*
			 * CustomerMasterController.java - Method: getAllCustomersByType()
			 */
			if (customerQuotationRequest != null) {
				if (customerQuotationRequest.getCustomerTypeMaster() != null) {
					String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
							+ "/customerMaster/customerlistbynameorcodeandtype/" + query + "/"
							+ customerQuotationRequest.getCustomerTypeMaster().getId();

					BaseDTO baseDTO = httpService.get(url);

					if (baseDTO != null) {

						ObjectMapper mapper = new ObjectMapper();

						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

						String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

						customerMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
						});

					}
				} else {
					AppUtil.addWarn("Please Select Customer Type First");
				}
			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerMasterList;
	}

	public CustomerMaster getCustomerDetails(Long custId) {
		log.info("=============>Start getCustomerBillingAndShippingAddress Method==========>" + custId);
		CustomerMaster customerMaster = new CustomerMaster();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/customerMaster/getcustomerDetails/" + custId;
			baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerMaster = mapper.readValue(jsonResponse, new TypeReference<CustomerMaster>() {
				});

			}

		} catch (Exception ex) {
			log.error("Error in getCustomerBillingAndShippingAddress", ex);
		}
		log.info("=============>End getCustomerBillingAndShippingAddress Method==========>");
		return customerMaster;
	}
}
