package in.gov.cooptex.admin.petition.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.petition.dto.PetitionRegisterNoteResponseDTO;
import in.gov.cooptex.admin.petition.dto.PetitionRegisterRequestDTO;
import in.gov.cooptex.admin.petition.dto.PetitionResponseDTO;
import in.gov.cooptex.admin.petition.model.PetitionRegister;
import in.gov.cooptex.admin.petition.model.PetitionRegisterNote;
import in.gov.cooptex.admin.petition.model.PetitionSource;
import in.gov.cooptex.admin.petition.model.PetitionType;
import in.gov.cooptex.admin.petition.model.PetitionerType;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.InstitutionType;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("petitionRequestBean")
@Scope("session")
public class PetitionRequestBean extends CommonBean implements Serializable {
	/**
	*
	*/
	private static final long serialVersionUID = -3692509443246631999L;

	private final String PETITION_LIST_PAGE = "/pages/admin/petition/listPetition.xhtml?faces-redirect=true;";
	private final String PETITION_ADD_PAGE = "/pages/admin/petition/createPetition.xhtml?faces-redirect=true;";
	private final String PETITION_VIEW_PAGE = "/pages/admin/petition/viewPetition.xhtml?faces-redirect=true;";
	private final String PETITION_APPROVE_PAGE = "/pages/admin/petition/approvalPetition.xhtml?faces-redirect=true;";
	public static final String PETITION_REGISTER_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/petitionregister";
	public static final String PETITION_TYPE_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/petitiontype";
	public static final String PETITION_SOURCE_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/petitionsource";
	public static final String PETITIONER_TYPE_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/petitionertype";


	private ObjectMapper mapper; 

	private String jsonResponse;

	@Autowired
	private HttpService httpService;

	@Setter
	@Getter
	String requestLaterFileName;

	@Getter
	@Setter
	Boolean previousApproval = false;
 
	@Getter
	@Setter
	PetitionResponseDTO petitionResponseDTO = new PetitionResponseDTO();

	@Getter
	@Setter
	PetitionResponseDTO selectedPetitionRegisterDto = new PetitionResponseDTO();
	
	@Getter
	@Setter
	Long selectedNotificationId=null;

	@Getter
	@Setter
	PetitionRegister viewPetitionRegisterObj = new PetitionRegister();

    @Getter
    @Setter
    List<PetitionRegisterNoteResponseDTO> viewPetitionRegisterNoteListObj = new ArrayList<>();

    @Getter
    @Setter
    List<PetitionRegisterNoteResponseDTO> displayPetitionRegisterNoteListObj = new ArrayList<>();

	@Getter
	@Setter
	PetitionRegister createPetitionRegisterRequestObj = new PetitionRegister();

	@Setter
	@Getter
	PetitionRegisterNote createPetitionRegisterNote;

	@Setter
	@Getter
	PetitionRegisterRequestDTO createPetitionRegisterNoteInfo;

	@Getter
	@Setter
	LazyDataModel<PetitionResponseDTO> petitionRegisterLazyDataModel;

	@Setter
	@Getter
	List statusValues;

	@Getter
	@Setter
	String note;

	@Setter
	@Getter
	List<String> typesOfPetitionValues;

	@Setter
	@Getter
	List<PetitionResponseDTO> petitionResponseDTOList;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> headOrRegionalOffice = new ArrayList<>();

	@Getter
	@Setter
	List<PetitionSource> petitionSourceList = new ArrayList<>();

	@Getter
	@Setter
	List<PetitionerType> petitionerTypeList = new ArrayList<>();

	@Getter
	@Setter
	List<PetitionType> petitionTypeList = new ArrayList<>();

	@Getter
	@Setter
	private UploadedFile petitionDocumentFile = null;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String petitionDocumentFileName;

	@Setter
	@Getter
	String fileType;

    @Setter
    @Getter
    Boolean isRowSelectedFlag = false;

	@Setter
	@Getter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

    @Getter
    @Setter
    Boolean viewStatusButtonFlag = false;

	@Getter
	@Setter
	String remarkscomment;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	@Getter
	@Setter
	private String Url = null;

	@Setter
	@Getter
	List<InstitutionType> institutionTypeList;

	@Setter
	@Getter
	InstitutionType institutionType;

	@Getter
	@Setter
	private Map<String, Object> filterMap;

	@Getter
	@Setter
	private SortOrder sortingOrder;

	@Getter
	@Setter
	private String sortingField;

	@Getter
	@Setter
	private Integer resultSize;

	@Getter
	@Setter
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

    @Getter
    @Setter
    long noOfDays;

    @Getter
    @Setter
    Long registerNoteId;

	@Getter
	@Setter
	private Date startTime, endTime;

	@Getter
	@Setter
	UserMaster forwardTo;

    @Getter
    @Setter
    String forwardFor;

    @Getter
	@Setter
	EmployeeMaster employeeMaster;
    
    @Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

    @Setter
    @Getter
    private StreamedContent file;

	private SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("dd-MMM-yyyy");
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	List<UserMaster> userMasters;
	
	@Autowired
	SystemNotificationBean systemNotificationBean;


	public void init() {
		showListPetitionRequestPage();
	}

	public String showListPetitionRequestPage() {
		log.info("<=== Starts PetitionRequestBean.showListPetitionRequestPage() ======>");
		mapper = new ObjectMapper();
        if (isRowSelectedFlag) {
            addButtonFlag = false;
        } else {
            addButtonFlag = true;
        }
        isRowSelectedFlag = false;
        selectedPetitionRegisterDto = new PetitionResponseDTO();
		petitionResponseDTO = new PetitionResponseDTO();
		loadStatus();
		loadPetitionType();
		loadPetitionListLazy();
		log.info("<=== Ends PetitionRequestBean.showListPetitionRequestPage() ======>");
		return PETITION_LIST_PAGE;
	}

	public void loadAllCreatePageDropDown() {
		log.info("<=== Starts PetitionRequestBean.loadAllCreatePageDropDown() ======>");
		loadSourceOFPetition();
		loadPetitionTypeListObj();
		loadPetitionerType();
		forwardToUsersList = new ArrayList<>();
		//forwardToUsersList = commonDataService.loadForwardToUsers();
		employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PETITION");
		headOrRegionalOffice = commonDataService.loadHeadAndRegionalOffice();

		createPetitionRegisterRequestObj =  new PetitionRegister();
		createPetitionRegisterRequestObj.setPetitionerType(new PetitionerType());
		createPetitionRegisterRequestObj.setPetitionType(new PetitionType());
		createPetitionRegisterRequestObj.setPetitionSource(new PetitionSource());
		createPetitionRegisterRequestObj.setEntityMaster(new EntityMaster());
		createPetitionRegisterNote = new PetitionRegisterNote();
		createPetitionRegisterNote.setForwardTo(new UserMaster());
		log.info("<=== Ends PetitionRequestBean.loadAllCreatePageDropDown() ======>");
	}


	private void loadStatus() {
		log.info("<=== Starts PetitionRequestBean.loadStatus() ======>");
		statusValues = new ArrayList<>();
        Object[] statusArray = {"SUBMITTED","FORWARDED", "CANCEL", "CLOSED","VIEWED"};
		for (Object status : statusArray) {
			statusValues.add(status);
		}
		log.info("<=== Ends PetitionRequestBean.loadStatus() =====> Status ::"+statusValues);
	}

	private void loadSourceOFPetition() {
		log.info("<=== Starts PetitionRequestBean.loadSourceOFPetition() ======>");
		try {
			petitionSourceList = new ArrayList<>();
			String url = PETITION_SOURCE_URL + "/getpetitionsourcedropdown";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<PetitionSource> petitionSources = mapper.readValue(jsonResponse,
							new TypeReference<List<PetitionSource>>() { });
					petitionSourceList = petitionSources;
					log.info("Loading PetitionSource success : " + petitionSources);
				} else {
					log.info(" Petition Source List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Petition Source Type list Successfully Completed petitionSourceList : "+petitionSourceList);
			} else {
				log.error("Petition Source Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Petition Source Type list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends PetitionRequestBean.loadSourceOFPetition() ======>");
	}
	private void loadPetitionerType() {
		log.info("<=== Starts PetitionRequestBean.loadPetitionerType() ======>");
		try {
			petitionerTypeList = new ArrayList<>();
			String url = PETITIONER_TYPE_URL + "/getpetitionertypedropdown";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<PetitionerType> petitionerTypes = mapper.readValue(jsonResponse,
							new TypeReference<List<PetitionerType>>() { });
					petitionerTypeList = petitionerTypes;
					log.info("Loading PetitionerType success : " + petitionerTypes);
				} else {
					log.info(" Petitioner Type List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info(" Petitioner Type list Successfully Completed");
			} else {
				log.error(" Petitioner Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Petitioner Type list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends PetitionRequestBean.loadPetitionerType() ======>");
	}

	private void loadPetitionType() {
		log.info("<=== Starts PetitionRequestBean.loadPetitionType() ======>");
		try {
			typesOfPetitionValues = new ArrayList<>();
			String url = PETITION_TYPE_URL + "/getpetitiontypedropdown";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}

			if (baseDTO.getStatusCode() == 0) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<PetitionType> petitionTypes = mapper.readValue(jsonResponse,
							new TypeReference<List<PetitionType>>() { });
					for(PetitionType petitionType : petitionTypes){
						typesOfPetitionValues.add(petitionType.getName());
					}
					log.info("Loading typesOfPetitionValues Successfully Completed  : " + typesOfPetitionValues);
				} else {
					log.info(" Petition Request List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
			} else {
				log.error(" Petition Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Petition Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends PetitionRequestBean.loadPetitionType() ======>");
	}

	private void loadPetitionTypeListObj() {
		log.info("<=== Starts PetitionRequestBean.loadPetitionTypeListObj() ======>");
		try {
			petitionTypeList = new ArrayList<>();
			String url = PETITION_TYPE_URL + "/getpetitiontypedropdown";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			List<PetitionType> petitionTypes = mapper.readValue(jsonResponse,
					new TypeReference<List<PetitionType>>() {
					});
			if (petitionTypes == null) {
				log.info(" Petition Request List is null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			if (baseDTO.getStatusCode() == 0) {
				petitionTypeList = petitionTypes;
				log.info(" Petition Type list Successfully Completed");
			} else {
				log.error(" Petition Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Petition Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<=== Ends PetitionRequestBean.loadPetitionTypeListObj() ======>");
	}

	public void loadPetitionListLazy() {
		log.info("<=== Starts PetitionRequestBean.loadPetitionListLazy() ======>");
		petitionRegisterLazyDataModel = new LazyDataModel<PetitionResponseDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<PetitionResponseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = PETITION_REGISTER_URL + "/getallpetitionregisterlist";
					BaseDTO baseDTO = httpService.post(url, paginationRequest);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					petitionResponseDTOList = mapper.readValue(jsonResponse,
							new TypeReference<List<PetitionResponseDTO>>() {
							});
					if (petitionResponseDTOList == null) {
						log.info(" Petition Request List is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Petition Request list search Successfully Completed");
					} else {
						log.error(" Petition Request list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return petitionResponseDTOList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Petition Request list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(PetitionResponseDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PetitionResponseDTO getRowData(String rowKey) {
				try {
					for (PetitionResponseDTO petition : petitionResponseDTOList) {
						if (petition.getId().equals(Long.valueOf(rowKey))) {
							selectedPetitionRegisterDto = petition;
							return petition;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}
		};

		log.info("<=== Ends PetitionRequestBean.loadPetitionListLazy() ======>");
	}

	public String getDate(Date date){
		return simpleDateFormat.format(date);
	}

    public Date getToday() {
        return new Date();
    }

    public String getFileName(String path) {
        return new File(path).getName();
    }

	public String petitionRegisterListAction() {
		log.info("<====== PetitionRequestBean.petitionRegisterListAction() Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				setFileType(null);
				log.info("<====== PetitionRequest add page called.......====>");
				selectedPetitionRegisterDto = new PetitionResponseDTO();
				PetitionRegister createPetitionRegisterRequestObj = new PetitionRegister();
				loadAllCreatePageDropDown();
				return PETITION_ADD_PAGE;
				
			}
			if (selectedPetitionRegisterDto == null) {
				errorMap.notify(ErrorDescription.SELECT_PETITION.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmPetitionRegisterDelete').show();");
			}

		} catch (Exception e) {
			log.error("Exception Occured in PetitionRequestBean.petitionRegisterListAction()::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== Ends PetitionRequestBean.petitionRegisterListAction() ====>");
		return null;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("<=== Starts PetitionRequestBean.uploadDocument() ======>");
		try {
			UploadedFile file  = event.getFile();
			
//			fileType = FilenameUtils.getExtension(file.getFileName());
//			log.info(" File Type :: " + fileType);
//			boolean validFileType = AppUtil.isValidFileType(fileType,
//					new String[] { "png", "jpg", "JPG", "doc", "jpeg", "gif", "docx", "pdf", "xlsx", "xls" });
//			if (!validFileType) {
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
//				return;
//			}
			fileType = file.getFileName();
			
			if(!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg")
					&& !fileType.contains(".docx")
					&& !fileType.contains(".doc")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
				return;
			}
			long size = file.getSize();
			
			log.info("filesizeeee"+ size);
			//			size = size /  (1024*1024);
			
			
//			if((size /  (1024*1024)) >2) {
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
//				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
//				log.info("<<====  Large size file uploaded");
//			}
//			if (size >= 2000 ) {
//				String createRaiseComplaintPathName = commonDataService.getAppKeyValue("PETITION_REGISTER_FILE_PATH");
//				String filePath = Util.fileUpload(createRaiseComplaintPathName, file);
//				petitionDocumentFileName = filePath;
//				errorMap.notify(ErrorDescription.PETITION_REGISTER_FILE_UPLOAD_SUCCESS.getErrorCode());
//				log.info(" Uploading Document is uploaded successfully");
//			}
			
			if (size > 0 ) {
				String createRaiseComplaintPathName = commonDataService.getAppKeyValue("PETITION_REGISTER_FILE_PATH");
				String filePath = Util.fileUpload(createRaiseComplaintPathName, file);
				petitionDocumentFileName = filePath;
				errorMap.notify(ErrorDescription.PETITION_REGISTER_FILE_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Uploading Document is uploaded successfully");
			}
//			else {
//				Util.addWarn("File size should be less than 2 MB");
//				return ;
//			}
			
//			else {
//				Util.addWarn("File size should be less than 100 MB");
//				return ;
//			} 

		}catch(Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("<<==  Error in file upload "+e.getMessage());
		}
		log.info("<=== Ends PetitionRequestBean.uploadDocument() ======>");
	}

	public String submit() {
		try {
			log.info("<=======Starts PetitionRegisterBean.submit ======>" + createPetitionRegisterRequestObj);
			log.info("noteeeee"+ createPetitionRegisterNote.getNote());
			
			if(createPetitionRegisterNote.getNote()==null) {
				 Util.addWarn("Note is Required");
				return null;
			}
			else if(createPetitionRegisterNote.getForwardTo()==null) {
			 Util.addWarn("Forward To is Required");
			
		     return null;		 
		  }
				
						
			else {
			createPetitionRegisterRequestObj.setCreatedDate(new Date());
			createPetitionRegisterRequestObj.setFilePath(petitionDocumentFileName);
			PetitionRegisterRequestDTO petitionRegisterRequestDTO = new PetitionRegisterRequestDTO();
			petitionRegisterRequestDTO.setPetitionRegister(createPetitionRegisterRequestObj);
			petitionRegisterRequestDTO.setForwardTo(new UserMaster());
			petitionRegisterRequestDTO.getForwardTo().setId(createPetitionRegisterNote.getForwardTo().getId());
			petitionRegisterRequestDTO.setNote(createPetitionRegisterNote.getNote());
           log.info("noteee"+createPetitionRegisterNote.getNote() + createPetitionRegisterNote.getForwardTo().getId() );
			BaseDTO response = httpService.post(PETITION_REGISTER_URL + "/saveorupdate", petitionRegisterRequestDTO);
			log.info("status code - " + response.getStatusCode());
			if (response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.PETITION_REGISTER_SAVE_SUCCESS.getErrorCode());
				return showListPetitionRequestPage();
			} else {
				errorMap.notify(response.getStatusCode());
			}
		}
			log.info("<=======Ends PetitionRegisterBean.submit ======>");
		}catch (Exception e){
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("<=======found exception in file save petition : "+e.getMessage());
			return null;
		}
		return null;
	}

	/**
	 * Code for Row Select
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts PetitionRequestBean.onRowSelect ========>"+event);
		selectedPetitionRegisterDto = ((PetitionResponseDTO) event.getObject());
		log.info("<==== SelectedPeitionRegister Object =====>",selectedPetitionRegisterDto);
        isRowSelectedFlag = true;
		addButtonFlag=false;
		if(selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("CLOSED")) {
			//viewButtonFlag=false;
			finalapprovalFlag = false;
		}else {
			finalapprovalFlag = true;
			//viewButtonFlag=true;
		}
		viewButtonFlag=true;
		editButtonFlag=false;
		deleteButtonFlag=true;
        //forwardToUsersList = commonDataService.loadForwardToUsers();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PETITION");
		log.info("<===Ends PetitionRequestBean.onRowSelect ========>");
	}

	public String clearPetitionRegisterSelections(){
	    viewButtonFlag=false;
	    deleteButtonFlag=false;
        selectedPetitionRegisterDto = new PetitionResponseDTO();
		return PETITION_LIST_PAGE;
	}

	/**
	 * View Page Functionalities
	 */
	
	public String viewPetitionRegisterInfo() {
		log.info("<===== Starts viewPetitionRegisterInfo ======>");
		log.info("filepathhhh" +viewPetitionRegisterObj.getFilePath()); 
		
		try { 
			
		
			String url = PETITION_REGISTER_URL + "/getpetitionbyid/"+selectedPetitionRegisterDto.getId()+"/"+(selectedNotificationId!=null?selectedNotificationId:0);
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				viewStatusButtonFlag=true;
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				viewPetitionRegisterNoteListObj  = mapper.readValue(jsonResponse,
						new TypeReference<List<PetitionRegisterNoteResponseDTO>>() {
						});
				userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PETITION_REGISTER.toString());

				employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
                registerNoteId = viewPetitionRegisterNoteListObj.get(viewPetitionRegisterNoteListObj.size()-1).getId();
                forwardTo= viewPetitionRegisterNoteListObj.get(viewPetitionRegisterNoteListObj.size()-1).getSelectedForwardTo();
                String note=viewPetitionRegisterNoteListObj.get(viewPetitionRegisterNoteListObj.size()-1).getNote();
                displayPetitionRegisterNoteListObj = viewPetitionRegisterNoteListObj;
                displayPetitionRegisterNoteListObj.remove(0);
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				viewPetitionRegisterObj = mapper.readValue(jsonResponse,
						new TypeReference<PetitionRegister>() {
						});
				

				if(viewPetitionRegisterObj != null){
					log.info("PetitionRegisterNoteObj sizeee ::::::::::::::::"+ viewPetitionRegisterNoteListObj.size());
                    log.info("PetitionRegisterObj size ::::::::::::::::"+ viewPetitionRegisterObj);
                    createPetitionRegisterNoteInfo = new PetitionRegisterRequestDTO();
                    createPetitionRegisterNoteInfo.setForwardTo(forwardTo);
                    createPetitionRegisterNoteInfo.setNote(note);
                    createPetitionRegisterNoteInfo.setPetitionRegister(viewPetitionRegisterObj);
                    forwardFor = viewPetitionRegisterObj.getPetitionStatus();
                    
                    if( forwardTo.getId().equals(loginBean.getUserDetailSession().getId()) ) {
                		viewStatusButtonFlag=true;
                		finalapprovalFlag=true;
        			}
//                
                	if (selectedNotificationId != null) {
    					systemNotificationBean.loadTotalMessages();
    				}
                    if(selectedNotificationId == null && ! selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("CLOSED") ) {
                    if(forwardTo.getId().equals(loginBean.getUserDetailSession().getId())) {
                    	
                 
                    changeViewStatusByForwardTo(selectedPetitionRegisterDto.getId());
                 
                    finalapprovalFlag=true;
                   
                    
                    }
                    }
				
//				if(! selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("CLOSED")) {
//					
//				}
//                
                    
                 	if(selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("CLOSED") || selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("CANCEL") ) {
                 		finalapprovalFlag=false;
                 		viewStatusButtonFlag=false;
        			}
                 	
                 	if(! forwardTo.getId().equals(loginBean.getUserDetailSession().getId())) {
                		viewStatusButtonFlag=false;
        			}
                 	if(! forwardTo.getId().equals(loginBean.getUserDetailSession().getId()) && selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("FORWARDED") ) {
                		viewStatusButtonFlag=false;
                		finalapprovalFlag=false;
        			}
                 	
                	
                 	
                 	if(loginBean.getUserMaster().getUsername().equals("hosuper")) {
                 		finalapprovalFlag=false;
                 		viewStatusButtonFlag=false;
                 	}
                 	
                	if(! forwardTo.getId().equals(loginBean.getUserDetailSession().getId()) && selectedPetitionRegisterDto.getPetitionStatus().equalsIgnoreCase("SUBMITTED") ) {
                		viewStatusButtonFlag=false;
                		finalapprovalFlag=false;
        			}
                
                	if (selectedNotificationId != null) {
    					systemNotificationBean.loadTotalMessages();
    				}
        			  
                    userMaster = new UserMaster();
                    return PETITION_VIEW_PAGE;
				}
				else {
					errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getErrorCode());
					return null;
				}

			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			}
		} catch (Exception e) {
				log.error("Exception occured in viewPetitionRegisterInfo ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends viewPetitionRegisterInfo ======>");
		return null;
	}

	public void createParams(){
		log.info("<===== Starts PetitionRequestBean.createParams =====>",note);
		createPetitionRegisterNoteInfo.setNote(note);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
		log.info("<===== Ends PetitionRequestBean.createParams =====>",note);
	}
	public String submitNoteInformation(){
		log.info("<===== Starts PetitionRequestBean.submitNoteInformation =====>");
		createPetitionRegisterNoteInfo.getPetitionRegister().setId(viewPetitionRegisterObj.getId());
        createPetitionRegisterNoteInfo.getPetitionRegister().setPetitionStatus(forwardFor);
        createPetitionRegisterNoteInfo.setForwardTo(userMaster);
        createPetitionRegisterNoteInfo.setPetitionRegisterNoteId(registerNoteId);
        //if (!forwardFor.equalsIgnoreCase("SUBMITTED")) {
        if (!forwardFor.equalsIgnoreCase("FORWARDED")) {
            createPetitionRegisterNoteInfo.setPetitionRegisterNoteId(null);
            createPetitionRegisterNoteInfo.setForwardTo(null);
            createPetitionRegisterNoteInfo.setNote(null);
        }
		log.info("createPetitionRegisterNoteInfo",createPetitionRegisterNoteInfo);
		BaseDTO response = httpService.post(PETITION_REGISTER_URL + "/saveorupdatenote", createPetitionRegisterNoteInfo);
		if (response.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.PETITION_REGISTER_SAVE_SUCCESS.getErrorCode());
			return showListPetitionRequestPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===== Ends PetitionRequestBean.submitNoteInformation =====>");
		return null;
	}

	public String deletePetitionRegister(){
		log.info("<===== Starts PetitionRequestBean.deletePetitionRegister =====>");
		log.info("<==== SelectedPeitionRegister Object =====>",selectedPetitionRegisterDto);
		String url = PETITION_REGISTER_URL + "/delete/"+selectedPetitionRegisterDto.getId();
		BaseDTO response = httpService.get(url);
		if (response != null && response.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.PETITION_REGISTER_DELETE_SUCCESS.getErrorCode());
		}
		else{
			errorMap.notify(ErrorDescription.PETITION_REGISTER_DELETE_FAILURE.getErrorCode());
		}
		RequestContext context = RequestContext.getCurrentInstance();

		context.execute("PF('confirmPetitionRegisterDelete').hide();");
		log.info("<===== Ends PetitionRequestBean.deletePetitionRegister =====>");
		return PETITION_LIST_PAGE;
	}

    public void downloadFile() {
        InputStream input = null;
        try {
        	log.info("filepathhh" + viewPetitionRegisterObj.getFilePath()) ;
            File file = new File(viewPetitionRegisterObj.getFilePath());
            input = new FileInputStream(file);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            this.setFile(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in filedownload method------- " + e);
        } catch (Exception e) {
            log.error("exception in filedownload method------- " + e);
        }
    }

    public void viewPageSelectStatus() {
       // if (forwardFor.equalsIgnoreCase("SUBMITTED")) {
    	
    	
    	log.info("status" + forwardFor);
    	 if (forwardFor.equalsIgnoreCase("FORWARDED")) {
            viewStatusButtonFlag = true;
        } else {
            viewStatusButtonFlag = false;
        }
    	 

    }
    
   public String changeViewStatusByForwardTo(Long petitionId) {
	   log.info("<===== Starts PetitionRequestBean.changeViewStatusByForwardTo =====>");
	   try {
		   String url = PETITION_REGISTER_URL + "/viewstatus/"+petitionId;
			BaseDTO response = httpService.get(url);
			if (response.getStatusCode() == 0) {
				  log.info("<=====foward user viewed petition=====>");
			}
	   }
	   catch(Exception ex) {
		   log.error("exception in changeViewStatusByForwardTo method------- " + ex);
	   }
	   log.info("<===== End PetitionRequestBean.changeViewStatusByForwardTo =====>");
	   return null;
   }
   @PostConstruct
	public String showViewListPage() {
		log.info("PetitionBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String petitionId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			petitionId = httpRequest.getParameter("petitionId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(petitionId)) {
			selectedPetitionRegisterDto = new PetitionResponseDTO();
			action = "VIEW";
			selectedPetitionRegisterDto.setId(Long.parseLong(petitionId));
			selectedNotificationId = Long.parseLong(notificationId);
			viewPetitionRegisterInfo();
		}
		log.info("PetitionBean showViewListPage() Ends");
		addButtonFlag = true;
		return PETITION_VIEW_PAGE;
	}
   
   
}
