package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.ByteArrayInputStream;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.dto.EmployeeAdminPromotionDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.LoginDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.model.AdvanceTypeMaster;
import in.gov.cooptex.core.model.BloodGroupMaster;
import in.gov.cooptex.core.model.CasteMaster;
import in.gov.cooptex.core.model.Community;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmpTransferDetailsLog;
import in.gov.cooptex.core.model.EmpTransferDetailsNote;
import in.gov.cooptex.core.model.EmployeeContactDetails;
import in.gov.cooptex.core.model.EmployeeEducationQualification;
import in.gov.cooptex.core.model.EmployeeInterchangeDetails;
import in.gov.cooptex.core.model.EmployeeLoanAndAdvanceDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EmployeePersonalInfoGeneral;
import in.gov.cooptex.core.model.EmployeePromotionDetails;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.ExternalTrainingTypeMaster;
import in.gov.cooptex.core.model.InternalTrainingTypeMaster;
import in.gov.cooptex.core.model.LoanMaster;
import in.gov.cooptex.core.model.MaritalStatus;
import in.gov.cooptex.core.model.Nationality;
import in.gov.cooptex.core.model.Religion;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.Training;
import in.gov.cooptex.core.model.TrainingTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.hrms.model.EmpPromotionRequestDetails;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.personnel.hrms.dto.EmpPromotionRequestDto;
import in.gov.cooptex.personnel.hrms.dto.EmpPromotionResponseDto;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeProfileBean")
public class EmployeeProfileBean extends CommonBean {

	String url;

	ObjectMapper mapper;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0, interTotalRecord = 0, trainingTotalRecord = 0, promotionTotalRecord = 0;

	@Autowired
	LoginBean login;

	@Getter
	@Setter
	String action, suspensionFlag = null, pageName = "create", fileName, trainName = "internal", traingingFileName,
			pageAction;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Getter
	@Setter
	Training training = new Training();

	@Getter
	@Setter
	Training selectedTraining = new Training();

	@Getter
	@Setter
	EmployeePromotionDetails selectedEmployeePromotionDetails = new EmployeePromotionDetails();

	@Getter
	@Setter
	EmployeeAdminPromotionDTO employeeAdminPromotionDTO = new EmployeeAdminPromotionDTO();

	@Getter
	@Setter
	EmployeePromotionDetails employeePromotionDetails = new EmployeePromotionDetails();

	@Getter
	@Setter
	List<EmployeePromotionDetails> employeePromotionList = new ArrayList<>();

	@Getter
	@Setter
	Boolean isAddButton, isApproveButton, isEditButton, isDeleteButton, addLoan, editLoan, viewPromotion = true,
			dinPromotion = false, accPromotion = false, isContact = true;

	@Getter
	@Setter
	LazyDataModel<EmployeeTransferDetails> lazyApplicationModel;

	@Getter
	@Setter
	Boolean takenStatus = false;

	@Getter
	@Setter
	LazyDataModel<EmployeeInterchangeDetails> lazyInterchangeModel;

	@Getter
	@Setter
	LazyDataModel<EmployeePromotionDetails> lazyPromotion;

	@Getter
	@Setter
	LazyDataModel<Training> lazyTraining;

	@Setter
	@Getter
	EmployeeInterchangeDetails selectedEmployeeInterchangeDetails = new EmployeeInterchangeDetails();

	@Getter
	@Setter
	EmployeeInterchangeDetails employeeInterchangeDetail = new EmployeeInterchangeDetails();

	@Getter
	@Setter
	List<EmployeeEducationQualification> educationQualificationList = new ArrayList<EmployeeEducationQualification>();

	@Getter
	@Setter
	EmployeeTransferDetails employeeTransferDetail = new EmployeeTransferDetails();

	@Getter
	@Setter
	List<EmployeeTransferDetails> employeeTransferList = new ArrayList<>();

	@Setter
	@Getter
	EmployeeTransferDetails selectedEmployeeTransferDetails = new EmployeeTransferDetails();

	@Getter
	@Setter
	EmployeePersonalInfoEmployment employeePersonalInfo = new EmployeePersonalInfoEmployment();

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterAllList = new ArrayList<SectionMaster>();

	@Getter
	@Setter
	List<EntityMaster> headAndRegionalOfficeList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EntityMaster> entityMasterToList = new ArrayList<EntityMaster>();

	@Setter
	@Getter
	List<Designation> designationList;

	@Setter
	@Getter
	List<TrainingTypeMaster> trainingTypeList;

	@Setter
	@Getter
	List<InternalTrainingTypeMaster> internalTypeList;

	@Setter
	@Getter
	List<ExternalTrainingTypeMaster> externalTypeList;

	@Setter
	@Getter
	List<Department> departmentList;

	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList = new ArrayList<EntityTypeMaster>();

	@Getter
	@Setter
	EmployeeContactDetails employeeContactDetails = new EmployeeContactDetails();

	@Getter
	@Setter
	String transferType;

	@Autowired
	HttpService httpService;

	String jsonResponse;

	@Getter
	@Setter
	String textarea;

	long employeePhotoSize;

	@Getter
	@Setter
	LoanMaster loanMaster;

	@Getter
	@Setter
	AdvanceTypeMaster advanceType;

	@Getter
	@Setter
	List<LoanMaster> loanMasterList;

	@Getter
	@Setter
	List<AdvanceTypeMaster> advanceTypeMasterList;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();

	@Getter
	@Setter
	String loanType = "loan";

	@Getter
	@Setter
	List<EntityMaster> loadRegionList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	private UploadedFile relievingLetterFile;

	@Getter
	@Setter
	int totalInterchangeRecords = 0;

	@Getter
	@Setter
	String loanOrAdvanceTypeName;

	@Getter
	@Setter
	int totalLoanSize;

	@Getter
	@Setter
	LazyDataModel<EmployeeLoanAndAdvanceDetails> loanDetailsList;

	@Getter
	@Setter
	EmployeeLoanAndAdvanceDetails selectedEmployeeLoanAndAdvance;

	@Getter
	@Setter
	String newPassword, confirmPassword, oldPassword = "", oldProfilePassword = "";

	@Getter
	@Setter
	String newProfilePassword, confirmProfilePassword;

	@Getter
	@Setter
	List<EmpPromotionRequestDetails> empPromotionRequestDetailsList = new ArrayList<EmpPromotionRequestDetails>();

	@Getter
	@Setter
	EmpPromotionRequestDetails selectedEmpPromotionRequestDetails = new EmpPromotionRequestDetails();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Boolean viewButton = true;

	@Getter
	@Setter
	String acceptOption = "";

	@Getter
	@Setter
	Boolean acceptOrRejectStatus = false;

	@Getter
	@Setter
	EmpPromotionRequestDto empPromotionRequestDto = new EmpPromotionRequestDto();

	@Getter
	@Setter
	List<String> optionTakenList = new ArrayList<String>();

	@Getter
	@Setter
	Boolean acceptReject = false;

	@Getter
	@Setter
	SupplierMaster supplierMaster = new SupplierMaster();

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	public static final String EMP_SERVER_URL = AppUtil.getPortalServerURL() + "/employeeprofile";

	public static final String SUPPLIER_SERVER_URL = AppUtil.getPortalServerURL() + "/supplier/master";

	@Autowired
	AddressMasterBean addressMasterBean = new AddressMasterBean();

	@Getter
	@Setter
	String address = "";

	@Getter
	@Setter
	UserDTO userDTO = null;

	private Long notificationId = 0L;

	private Long systemNotificationId = 0L;

	@PostConstruct
	public void init() {
		log.info("Init is executed.....................");
		employeeMaster = new EmployeeMaster();
		loadSearchListOfValues();
		loadAppConfigValues();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		newPassword = "";
		confirmPassword = "";

		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";

		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedEmpPromotionRequestDetails = new EmpPromotionRequestDetails();
			action = "VIEW";
			selectedEmpPromotionRequestDetails.setId(Long.valueOf(id));
			systemNotificationId = Long.parseLong(notificationId);
			viewemployeeDetails();
		}

	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			employeePhotoSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public void loadSearchListOfValues() {
		log.info("start loadSearchListOfValues ");
		try {
			designationList = commonDataService.loadDesignation();
			departmentList = commonDataService.getDepartment();
			sectionMasterAllList = commonDataService.getSectionMaster();
			// headAndRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
			entityTypeList = commonDataService.getEntityTypeByStatus();
			loanMasterList = commonDataService.loadAllLoan();
			advanceTypeMasterList = commonDataService.loadAllAdvanceTypeLoan();
			loadRegionList = commonDataService.loadEntityRegion();
			trainingTypeList = commonDataService.loadTrainingType();
			internalTypeList = commonDataService.loadInternalTrainingType();
			externalTypeList = commonDataService.loadExternalTrainingType();
			log.info("externalTypeList==>" + externalTypeList.size());
			log.info("internalTypeList==>" + internalTypeList.size());
		} catch (Exception exp) {
			log.error(" Exception Occured in while loading masters :: ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String showEmpTransfer() {
		try {
			log.info("Show Employee Form Called with Action " + action);
			if (pageAction.equalsIgnoreCase("ADD")) {
				employeeTransferDetail.setEmpTransferDetailsNote(new EmpTransferDetailsNote());
				employeeTransferDetail.setEmpTransferDetailslog(new EmpTransferDetailsLog());
				EmployeeMaster employeeMaster = loginBean.getEmployee();
				if (employeeMaster != null) {
					String url = EMP_SERVER_URL + "/getemployeetransferstatusbyempId/" + employeeMaster.getId();
					BaseDTO baseDTO = httpService.get(url);
					if (baseDTO != null && baseDTO.getStatusCode() == 0) {
						Boolean responseValue = (Boolean) baseDTO.getResponseContent();
						if (responseValue) {

							employeeMaster = getByEmployeeId();
							if (employeeMaster != null) {
								employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
							}
							return "/pages/personnelHR/employeeProfile/createTransferDetails.xhtml?faces-redirect=true";
						} else {
							Util.addWarn("You are not Eligible to apply Transfer");
							return null;
						}
					}
				} else {
					Util.addWarn("This feature is only for Employee");
					return null;
				}
				return null;
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedEmployeeTransferDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				pageName = "Edit";
				log.info("ID-->" + selectedEmployeeTransferDetails.getId());
				employeeMaster = getByEmployeeId();
				if (employeeMaster != null) {
					employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
				}
				employeeTransferDetail = getEmpTransferDetails(selectedEmployeeTransferDetails.getId());
				onchangeRegionAndEntityType();
				return "/pages/personnelHR/employeeProfile/createTransferDetails.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedEmployeeTransferDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->" + selectedEmployeeTransferDetails.getId());
				employeeMaster = getByEmployeeId();
				if (employeeMaster != null) {
					employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
				}
				employeeTransferDetail = getEmpTransferDetails(selectedEmployeeTransferDetails.getId());
				return "/pages/personnelHR/employeeProfile/viewTransferDetails.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: Transfer Delete Response :: ");
				if (selectedEmployeeTransferDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedEmployeeInterchangeDetails.getStatus() != null
						&& selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading employee profile bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private EmployeeInterchangeDetails getInterchangeById(Long interchangeId) {

		try {
			if (selectedEmployeeInterchangeDetails == null || selectedEmployeeInterchangeDetails.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  Id : : " + selectedEmployeeInterchangeDetails.getId());
			String getUrl = EMP_SERVER_URL + "/interchangeget/id/:id";
			url = getUrl.replace(":id", selectedEmployeeInterchangeDetails.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			// log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeInterchangeDetails empInterchange = mapper.readValue(jsonResponse,
					EmployeeInterchangeDetails.class);

			if (empInterchange != null) {
				employeeInterchangeDetail = empInterchange;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeInterchangeDetail;
		} catch (Exception e) {
			log.error("ExceptionException Ocured while get Emp Interchange Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	private EmployeeTransferDetails getEmpTransferDetails(Long id) {
		try {
			if (selectedEmployeeTransferDetails == null || selectedEmployeeTransferDetails.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  Id : : " + selectedEmployeeTransferDetails.getId());
			String getUrl = EMP_SERVER_URL + "/get/id/:id";
			url = getUrl.replace(":id", selectedEmployeeTransferDetails.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			// log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeTransferDetails empTransfer = mapper.readValue(jsonResponse, EmployeeTransferDetails.class);

			if (empTransfer != null) {
				transferType = empTransfer.getTransferType();
				log.info("transferType=> " + transferType);
				employeeTransferDetail = empTransfer;
				getHeadAndRegionalOfficeByTransferType();
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeTransferDetail;
		} catch (Exception e) {
			log.error("ExceptionException Ocured while get Emp Transfer Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String loadEmployeeProfile() {
		try {
			employeeMaster = getByEmployeeId();
			if (employeeMaster != null) {
				// EmployeePersonalInfoGeneral personaInfoGeneral
				if (employeeMaster.getPersonaInfoGeneral() == null) {
					EmployeePersonalInfoGeneral personaInfoGeneral = new EmployeePersonalInfoGeneral();
					BloodGroupMaster bloodGroup = new BloodGroupMaster();
					CasteMaster caste = new CasteMaster();
					Community community = new Community();
					Religion religion = new Religion();
					Nationality nationality = new Nationality();
					MaritalStatus maritalStatus = new MaritalStatus();

					personaInfoGeneral.setBloodGroup(bloodGroup);
					personaInfoGeneral.setCaste(caste);
					personaInfoGeneral.setCommunity(community);
					personaInfoGeneral.setReligion(religion);
					personaInfoGeneral.setNationality(nationality);
					personaInfoGeneral.setMaritalStatus(maritalStatus);

					employeeMaster.setPersonaInfoGeneral(personaInfoGeneral);
				}
				if (employeeMaster.getPersonalInfoEmployment() == null) {

					EmployeePersonalInfoEmployment personalInfoEmployment = new EmployeePersonalInfoEmployment();
					employeeMaster.setPersonalInfoEmployment(personalInfoEmployment);
				} else {
					employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
				}
				employeeContactDetails = employeeMaster.getContactDetails();
				if (employeeContactDetails == null) {
					isContact = false;
				}
				educationQualificationList = employeeMaster.getEducationList();

			} else {
				UserMaster user = login.getUserDetailSession();
				String url = SUPPLIER_SERVER_URL + "/getByUsrId/" + user.getId();
				log.info("====Url is===" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper objectMapper = new ObjectMapper();
					String response = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMaster = objectMapper.readValue(response, new TypeReference<SupplierMaster>() {
					});
					if (supplierMaster != null) {
						address = addressMasterBean.prepareAddress(supplierMaster.getAddressId());
					}
				}
				return "/pages/personnelHR/employeeProfile/supplierProfile.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading employee profile bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/employeeProfile/employeeGeneralInformation.xhtml?faces-redirect=true";
	}

	public EmployeeMaster getByEmployeeId() {
		try {
			UserMaster user = login.getUserDetailSession();
			mapper = new ObjectMapper();
			log.info("user==>" + user);
			log.info("getId==>" + user.getId());
			if (user == null || user.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			url = EMP_SERVER_URL + "/get/employee/" + user.getId();

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);

			/*
			 * if (baseDTO == null || baseDTO.getResponseContent() == null) {
			 * log.error("Base DTO Returned Null Value");
			 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); return null;
			 * }
			 */
			EmployeeMaster employee = null;
			if (baseDTO != null && baseDTO.getResponseContent() != null) {
				log.info("condent==>" + baseDTO.getResponseContent());
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
			}

			if (employee != null) {
				employeeMaster = employee;
			} else {
				log.error("employee object failed to desearlize");
				// errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return employeeMaster;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public StreamedContent getUploadedImage() {
		try {
			if (employeeMaster == null || employeeMaster.getPhoto() == null) {
				FacesContext context = FacesContext.getCurrentInstance();
				ExternalContext externalContext = context.getExternalContext();
				return new DefaultStreamedContent(
						externalContext.getResourceAsStream("/assets/images/profile-photo.png"), "image/png");
			}
			log.info("getUploadedImage Photo==>" + employeeMaster.getPhoto());
			return new DefaultStreamedContent(new ByteArrayInputStream(employeeMaster.getPhoto()), "image/png");
		} catch (Exception e) {
			log.error(" Error in while getting uploaded employee Image", e);
			errorMap.notify(ErrorDescription.EMP_PHOTO_DISPLAY_FAILED.getErrorCode());
			return null;
		}
	}

	public void onchangeRegionAndEntityType() {
		log.info("onchangeEntityType called..");
		try {

			if (employeeTransferDetail.getHeadRegOffice() != null
					&& employeeTransferDetail.getHeadRegOffice().getEntityTypeMaster() != null
					&& "HEAD_OFFICE".equalsIgnoreCase(
							employeeTransferDetail.getHeadRegOffice().getEntityTypeMaster().getEntityCode())) {
				employeeTransferDetail.setTransferTo(employeeTransferDetail.getHeadRegOffice());
				employeeTransferDetail.setEntityTo(employeeTransferDetail.getHeadRegOffice().getEntityTypeMaster());
				suspensionFlag = "DISABLEDENTITY";

			} else {
				suspensionFlag = null;
			}

			if (employeeTransferDetail != null && employeeTransferDetail.getHeadRegOffice() != null
					&& employeeTransferDetail.getHeadRegOffice().getId() != null
					&& employeeTransferDetail.getEntityTo() != null
					&& employeeTransferDetail.getEntityTo().getId() != null) {
				Long regionId = employeeTransferDetail.getHeadRegOffice().getId();
				Long entityTypeId = employeeTransferDetail.getEntityTo().getId();
				entityMasterToList = commonDataService.loadActiveEntityByregionOrentityTypeId(regionId, entityTypeId);
			} else {
				log.info("entity not found.");
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_ENTITY_TYPE_NOT_SELECTED.getErrorCode());
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
	}

	public String saveUpdateTransfer() {
		log.info("<---- : saveUpdateTransfer :------>" + employeeMaster);
		try {

			if (employeeTransferDetail.getEmpTransferDetailsNote() == null
					|| employeeTransferDetail.getEmpTransferDetailsNote().getUserMaster() == null
					|| employeeTransferDetail.getEmpTransferDetailsNote().getUserMaster().getId() == null) {
				AppUtil.addWarn("Forward To is Required");
				return null;
			}
			if (employeeTransferDetail.getEmpTransferDetailsNote() == null
					|| employeeTransferDetail.getEmpTransferDetailsNote().getFinalApproval() == null) {
				AppUtil.addWarn("Forward For is Required");
				return null;
			}
			UserMaster user = login.getUserDetailSession();

			log.info("saveUpdateTransfer :: getId==>" + user.getId());
			log.info("saveUpdateTransfer :: transferType==>" + transferType);
			if (employeePersonalInfo.getEntityType() != null) {
				log.info("saveUpdateTransfer :: entity type==>" + employeePersonalInfo.getEntityType().getEntityName());
			}
			if (employeePersonalInfo.getWorkLocation() != null) {
				log.info("saveUpdateTransfer :: worklocation==>" + employeePersonalInfo.getWorkLocation().getName());
			}
			if (employeePersonalInfo.getSectionMaster() != null) {
				log.info("saveUpdateTransfer :: section master==>" + employeePersonalInfo.getSectionMaster().getName());
			}
			employeeTransferDetail.setCategory("Transfer");
			employeeTransferDetail.setTransferType(transferType);
			employeeTransferDetail.setEntityFrom(employeePersonalInfo.getEntityType());
			employeeTransferDetail.setTransferFrom(employeePersonalInfo.getWorkLocation());
			employeeTransferDetail.setSectionFrom(employeePersonalInfo.getCurrentSection());
			employeeTransferDetail.setUserId(user.getId());
			employeeTransferDetail.setEmpId(loginBean.getEmployee().getId());
			log.info("employeeTransferDetail==>" + employeeTransferDetail);
			url = EMP_SERVER_URL + "/savetransfer";
			BaseDTO baseDTO = httpService.put(url, employeeTransferDetail);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeTransferDetails emp = mapper.readValue(jsonResponse, EmployeeTransferDetails.class);
			log.info("emp==>" + emp);
			if (emp != null) {
				employeeTransferDetail = emp;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (pageAction.equals("EDIT")) {
					errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_UPDATED.getErrorCode());
				} else {
					errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_CREATED.getErrorCode());
				}
				clearTransfer();
				return "/pages/personnelHR/employeeProfile/listTransferDetails.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving employee transfer request with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * purpose : to uploaded the image
	 * 
	 * @author krishnakumar
	 * @param event
	 * @return
	 */
	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			long size = relievingLetterFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				employeeInterchangeDetail.setSupportingDocuments(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			log.info("uploadSignature filePath===>" + filePath);

			employeeInterchangeDetail.setSupportingDocuments(filePath);

			fileName = relievingLetterFile.getFileName();
			errorMap.notify(ErrorDescription.EMP_PROFILE_UPLOADED_SUCCESSFULLY.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void uploadTraingDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			relievingLetterFile = event.getFile();
			long size = relievingLetterFile.getSize();
			log.info("uploadSignature size==>" + size);
			size = size / 1000;
			if (size > employeePhotoSize) {
				training.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String photoPathName = commonDataService.getAppKeyValue("JOBAPPLICATION_PHOTO_UPLOAD_PATH");
			String filePath = Util.fileUpload(photoPathName, relievingLetterFile);
			log.info("uploadSignature filePath===>" + filePath);

			training.setDocumentPath(filePath);

			traingingFileName = relievingLetterFile.getFileName();
			errorMap.notify(ErrorDescription.EMP_PROFILE_UPLOADED_SUCCESSFULLY.getErrorCode());
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String loadTrainingRequest() {
		try {
			if (pageAction.equalsIgnoreCase("ADD")) {
				training = new Training();
				loadSearchListOfValues();
				traingingFileName = "";
				return "/pages/personnelHR/employeeProfile/createTrainingRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedTraining == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				Long trainingId = selectedTraining.getId();
				training = getTrainingById(trainingId);
				if (training.getDocumentPath() != null) {
					String filePathValue[] = training.getDocumentPath().split("uploaded/");
					log.info("train 0==>" + filePathValue[0]);
					log.info("train 1==>" + filePathValue[1]);
					traingingFileName = filePathValue[1];
				}
				pageName = "Edit";
				onchangeInternalType();
				return "/pages/personnelHR/employeeProfile/createTrainingRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedTraining == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				traingingFileName = null;
				trainName = null;
				onchangeInternalType();
				Long trainingId = selectedTraining.getId();
				training = getTrainingById(trainingId);
				if (training.getDocumentPath() != null) {
					String filePathValue[] = training.getDocumentPath().split("uploaded/");
					log.info("Edu 0==>" + filePathValue[0]);
					log.info("Edu 1==>" + filePathValue[1]);
					traingingFileName = filePathValue[1];
				}
				return "/pages/personnelHR/employeeProfile/viewTrainingRequest.xhtml?faces-redirect=true";

			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: Interchange Delete Response :: ");
				if (selectedTraining == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} /*
					 * else if(selectedTraining.getStatus().equalsIgnoreCase("Approved")){
					 * errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode())
					 * ; return null; }
					 */else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			} else if (pageAction.equalsIgnoreCase("CANCEL") || pageAction.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				selectedTraining = new Training();
				lazyDataTraining();
				return "/pages/personnelHR/employeeProfile/listTrainingRequest.xhtml?faces-redirect=true";
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", e);
		}
		return null;
	}

	public Training getTrainingById(Long id) {
		try {
			if (selectedTraining == null || selectedTraining.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			log.info("Selected Employee  Id : : " + selectedTraining.getId());
			String getUrl = EMP_SERVER_URL + "/traingingget/id/:id";
			url = getUrl.replace(":id", selectedTraining.getId().toString());

			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			// log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			Training trainingDetails = mapper.readValue(jsonResponse, Training.class);

			if (trainingDetails != null) {
				training = trainingDetails;
			} else {
				log.error("employee object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Retrived  SuccessFully ");
			} else {
				log.error(" Employee Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", e);
		}
		return training;
	}

	public void onchangeInternalType() {
		try {
			if (training.getTrainingTypeMaster() != null) {
				log.info("traingType==>" + training.getTrainingTypeMaster().getName());
				if (training.getTrainingTypeMaster().getName().equalsIgnoreCase("Internal")) {
					trainName = "internal";

				} else if (training.getTrainingTypeMaster().getName().equalsIgnoreCase("External")) {
					trainName = "external";

				}
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", e);
		}
	}

	public String saveUpdateTraining() {
		log.info("<---- : saveUpdateTraining :------>" + training);
		try {

			if (training.getStartDate().after(training.getEndDate())) {
				Util.addWarn("End date Should greater than start date");
				return null;
			}
			UserMaster user = login.getUserDetailSession();
			training.setUserId(user.getId());
			log.info("getId==>" + user.getId());
			url = EMP_SERVER_URL + "/savetraining";
			BaseDTO baseDTO = httpService.put(url, training);
			log.info("Save saveUpdateInterchange Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			Training emp = mapper.readValue(jsonResponse, Training.class);
			log.info("emp==>" + emp);
			if (emp != null) {
				training = emp;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (!pageAction.equals("EDIT")) {
					errorMap.notify(ErrorDescription.EMPLOYEE_TRAINING_SAVE_SUCCESS.getErrorCode());
				} else {
					AppUtil.addInfo("Employee Training Request Updated Successfully");
				}
				return "/pages/personnelHR/employeeProfile/listTrainingRequest.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving employee transfer request with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	/**
	 * Loan And Advance
	 * 
	 * @author Praveen Kunwar
	 * @return
	 */

	public void loadLazyLoanandAdvanceDetails() {
		loanDetailsList = new LazyDataModel<EmployeeLoanAndAdvanceDetails>() {

			private static final long serialVersionUID = -9122090322809059767L;

			List<EmployeeLoanAndAdvanceDetails> dataList = new ArrayList<EmployeeLoanAndAdvanceDetails>();

			@Override
			public List<EmployeeLoanAndAdvanceDetails> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				EmployeeLoanAndAdvanceDetails request = new EmployeeLoanAndAdvanceDetails();

				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					UserMaster user = login.getUserDetailSession();

					log.info("getId==>" + user.getId());
					request.setUserId(user.getId());
					request.setEmployeeMaster(null);
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(EMP_SERVER_URL + "/loaneandadvance/lazyload/search", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					dataList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeLoanAndAdvanceDetails>>() {
					});
					if (dataList == null) {
						log.info(" :: Employee Loan and Advance Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {

						dataList.forEach(loan -> {
							if (loan.getLoanMaster() != null && loan.getLoanMaster().getId() != null) {
								loan.setLoanType("Loan");
								loan.setLoanOrAdvanceTypeName(loan.getLoanMaster().getName());
								loan.setLoanOrAdvanceAmount(loan.getLoanAmountRequired());
							} else if (loan.getAdvanceType() != null && loan.getAdvanceType().getId() != null) {
								loan.setLoanType("Advance");
								loan.setLoanOrAdvanceTypeName(loan.getAdvanceType().getName());
								loan.setLoanOrAdvanceAmount(loan.getAdvanceAmountRequired());
							}

						});

						this.setRowCount(baseDTO.getTotalRecords());
						totalLoanSize = baseDTO.getTotalRecords();
						log.info(":: Loan and Advance search Result size ::" + totalLoanSize);
					} else {
						log.error(":: Employee Loan and Advance Search Failed With status code :: "
								+ baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return dataList;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Employee Loan and Advance data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeLoanAndAdvanceDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeLoanAndAdvanceDetails getRowData(String rowKey) {
				List<EmployeeLoanAndAdvanceDetails> responseList = (List<EmployeeLoanAndAdvanceDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeLoanAndAdvanceDetails res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeLoanAndAdvance = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String showLoanAndAdvance() {
		log.info("inside showLoanAndAdvance Action::::::" + action);
		if (action.equalsIgnoreCase("add")) {
			loanType = "Loan";
			loanMaster = new LoanMaster();
			advanceType = new AdvanceTypeMaster();
			selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
			employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
			return "/pages/personnelHR/employeeProfile/createLoansAdvance.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("edit")) {
			if (selectedEmployeeLoanAndAdvance == null || selectedEmployeeLoanAndAdvance.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			loanType = selectedEmployeeLoanAndAdvance.getLoanType();
			prepareLoanDetails();

			return "/pages/personnelHR/employeeProfile/createLoansAdvance.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("view")) {
			if (selectedEmployeeLoanAndAdvance == null || selectedEmployeeLoanAndAdvance.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;

			}
			employeeLoanAndAdvanceDetails = getLoanAndAdvanceById(selectedEmployeeLoanAndAdvance.getId());
			return "/pages/personnelHR/employeeProfile/viewLoansAdvance.xhtml?faces-redirect=true";
		} else if (action.equalsIgnoreCase("cancel") || action.equalsIgnoreCase("list")) {
			loadLazyLoanandAdvanceDetails();
			selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
			return "/pages/personnelHR/employeeProfile/listLoansAdvance.xhtml?faces-redirect=true";
		}

		return null;
	}

	public void prepareLoanDetails() {
		if (selectedEmployeeLoanAndAdvance.getId() != null) {
			employeeLoanAndAdvanceDetails = getLoanAndAdvanceById(selectedEmployeeLoanAndAdvance.getId());

			if (loanType.equals("Loan")) {
				loanMasterList = commonDataService.loadAllLoan();
				loanMasterList.forEach(loan -> {
					if (loan.getName().equals(employeeLoanAndAdvanceDetails.getLoanMaster().getName())) {
						loanMaster = employeeLoanAndAdvanceDetails.getLoanMaster();
					}
				});
			} else if (loanType.equals("Advance")) {
				advanceTypeMasterList = commonDataService.loadAllAdvanceTypeLoan();
				advanceTypeMasterList.forEach(advance -> {
					if (advance.getName().equals(employeeLoanAndAdvanceDetails.getAdvanceType().getName())) {
					}
					advanceType = employeeLoanAndAdvanceDetails.getAdvanceType();
				});
			}
		} else {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("error found while selecting loan and advance recor..");
		}
	}

	public EmployeeLoanAndAdvanceDetails getLoanAndAdvanceById(Long id) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = EMP_SERVER_URL + "/loaneandadvance/id/" + id;
			log.info("Requested URL : ==>" + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeLoanAndAdvanceDetails = mapper.readValue(jsonResponse,
					new TypeReference<EmployeeLoanAndAdvanceDetails>() {
					});
			log.info("designation Object==> " + employeeLoanAndAdvanceDetails);
		} catch (Exception e) {
			log.error("Exception Occured in loadDesignationById  ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return employeeLoanAndAdvanceDetails;
	}

	public void onchangeLoan() {
		log.info("loan type is change :::" + loanType);
		try {
			if (loanType.equalsIgnoreCase("loan") || loanType.equalsIgnoreCase("advance")) {
				loanMaster = new LoanMaster();
				advanceType = new AdvanceTypeMaster();
				employeeLoanAndAdvanceDetails = new EmployeeLoanAndAdvanceDetails();
			}
		} catch (Exception e) {
			log.error("error found in change loan:::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public String deletLoadAndAdvance() {

		try {
			if (selectedEmployeeLoanAndAdvance == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else if (selectedEmployeeLoanAndAdvance.getStatus().equals("APPROVED")) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else if (selectedEmployeeLoanAndAdvance.getStatus().equals("REJECTED")) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Loan and Advance  :: ", e);
		}
		return null;
	}

	public String deleteLoanAndAdvanceConfirm() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("Selected OralTestMark  Id : : " + selectedEmployeeLoanAndAdvance.getId());
			String url = EMP_SERVER_URL + "/loaneandadvance/delete/" + selectedEmployeeLoanAndAdvance.getId();
			log.info("Loan and Advance Delete URL called : - " + url);
			baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDTO.getStatusCode() == 0) {
					log.info(" Loan and Advance Deleted SuccessFully ");
					errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_DELETED_SUCCESSFULLY.getErrorCode());
					selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
					return null;
				} else {
					log.error(" Loan and Advance Delete Operation Failed with status code " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Loan and Advance  :: ", e);
			errorMap.notify(baseDTO.getStatusCode());
		}
		return null;
	}

	public String clearLoanAndAdvance() {
		addLoan = false;
		editLoan = false;
		selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
		return "/pages/personnelHR/employeeProfile/listLoansAdvance.xhtml?faces-redirect=true";

	}

	public boolean vlidateLoanAndAdvanceDetails(EmployeeLoanAndAdvanceDetails employeeLoanAndAdvanceDetails) {

		if (loanMaster != null && loanType.equalsIgnoreCase("Loan")) {

			employeeLoanAndAdvanceDetails.setLoanMaster(loanMaster);

			Double loanEligibility = employeeLoanAndAdvanceDetails.getLoanMaster().getLoanEligiblilityAmount()
					.doubleValue();
			Double loanAmountRequired = employeeLoanAndAdvanceDetails.getLoanAmountRequired().doubleValue();

			Double maxLoanAmount = employeeLoanAndAdvanceDetails.getLoanMaster().getMaxLoanAmount() == null ? 0.0
					: employeeLoanAndAdvanceDetails.getLoanMaster().getMaxLoanAmount().doubleValue();

			if (loanEligibility != null && loanAmountRequired != null
					&& (loanAmountRequired > loanEligibility || (loanAmountRequired > maxLoanAmount))) {
				log.info("you are not eligible for required amount");
				errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_NOT_ELIGIBLE.getErrorCode());
				return false;
			}
			employeeLoanAndAdvanceDetails.setAdvanceType(null);
			employeeLoanAndAdvanceDetails.setAdvanceAmountRequired(null);
		} else if (advanceType != null && loanType.equalsIgnoreCase("Advance")) {
			employeeLoanAndAdvanceDetails.setAdvanceType(advanceType);

			Double advLoanEligibility = employeeLoanAndAdvanceDetails.getAdvanceType().getAdvanceEligiblilityAmount()
					.doubleValue();
			Double advLoanAmountRequired = employeeLoanAndAdvanceDetails.getAdvanceAmountRequired().doubleValue();
			Double maxAdvLoanAmount = employeeLoanAndAdvanceDetails.getAdvanceType().getMaxAdvanceAmount()
					.doubleValue();

			if (advLoanEligibility != null && advLoanAmountRequired != null
					&& (advLoanAmountRequired > advLoanEligibility) || (advLoanAmountRequired > maxAdvLoanAmount)) {
				log.info("you are not eligible for required amount");
				errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_NOT_ELIGIBLE.getErrorCode());
				return false;
			}
			employeeLoanAndAdvanceDetails.setLoanMaster(null);
			employeeLoanAndAdvanceDetails.setLoanAmountRequired(null);
		}
		return true;
	}

	public String createLoanAndAdvance() {
		log.info("start createLoanAndAdvance ");
		BaseDTO baseDto = new BaseDTO();
		if (employeeLoanAndAdvanceDetails == null) {
			log.info("::::::Loan and Advanced type object is null ::::::");
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}

		try {
			if (vlidateLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails)) {

				employeeMaster = getByEmployeeId();

				employeeLoanAndAdvanceDetails.setUserId(employeeMaster.getUserId());

				url = EMP_SERVER_URL + "/loanrequest";
				log.info("Loan and Advanced type Request URL ::::::" + url);
				employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
				baseDto = httpService.post(url, employeeLoanAndAdvanceDetails);
				if (baseDto == null) {
					log.info("::LoanAndAdvanceType details response null. ::" + baseDto);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				} else {
					if (baseDto.getStatusCode() == 0) {
						loadLazyLoanandAdvanceDetails();
						selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
						log.info("::LoanAndAdvanceType details saved successfully. ::" + baseDto.getResponseContent());
						errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_SAVED_SUCCESSFULLY.getErrorCode());
						return "/pages/personnelHR/employeeProfile/listLoansAdvance.xhtml?faces-redirect=true";
					} else {
						log.info("::LoanAndAdvanceType details failed to save. ::" + baseDto);
						errorMap.notify(baseDto.getStatusCode());
						return null;
					}
				}
			} else {
				log.error("validation failed while creating loan and advance .. ");
				return null;
			}

		} catch (Exception e) {
			log.error(":: Exception in createLoanAndAdvance", e);
			errorMap.notify(baseDto.getStatusCode());
		}

		return null;
	}

	public String updateLoanAndAdvance() {
		log.info("update createLoanAndAdvance ");
		BaseDTO baseDto = new BaseDTO();
		try {
			String url = EMP_SERVER_URL + "/loaneandadvance/update";
			log.info("Loan and Advance Requested URL : ==>" + url);
			if (vlidateLoanAndAdvanceDetails(employeeLoanAndAdvanceDetails)) {
				employeeMaster = getByEmployeeId();
				employeeLoanAndAdvanceDetails.setEmployeeMaster(null);
				employeeLoanAndAdvanceDetails.setUserId(employeeMaster.getUserId());
				baseDto = httpService.put(url, employeeLoanAndAdvanceDetails);
				if (baseDto == null) {
					log.info("::LoanAndAdvanceType details response null. ::" + baseDto);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				} else {
					if (baseDto.getStatusCode() == 0) {
						loadLazyLoanandAdvanceDetails();
						addLoan = false;
						selectedEmployeeLoanAndAdvance = new EmployeeLoanAndAdvanceDetails();
						log.info(
								"::LoanAndAdvanceType details updated successfully. ::" + baseDto.getResponseContent());
						errorMap.notify(ErrorDescription.EMP_LOAN_AND_ADVANCE_UPDATED_SUCCESSFULLY.getErrorCode());
						return "/pages/personnelHR/employeeProfile/listLoansAdvance.xhtml?faces-redirect=true";
					} else {
						log.info("::LoanAndAdvanceType details failed to updated. ::" + baseDto);
						errorMap.notify(baseDto.getStatusCode());
						return null;
					}
				}

			}

		} catch (Exception e) {
			log.error("Exception Occured in updating loan and advance  ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	public String saveOrUpdate() {
		String response = null;
		if (selectedEmployeeLoanAndAdvance != null && selectedEmployeeLoanAndAdvance.getId() != null) {
			response = updateLoanAndAdvance();
		} else {
			response = createLoanAndAdvance();
		}
		return response;
	}

	public String loadEmployeeInterchange() {
		try {
			employeeMaster = getByEmployeeId();
			employeeInterchangeDetail = new EmployeeInterchangeDetails();
			if (employeeMaster != null) {
				employeePersonalInfo = employeeMaster.getPersonalInfoEmployment();
			}
			if (pageAction.equalsIgnoreCase("ADD")) {
				return "/pages/personnelHR/employeeProfile/createInterchangeRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedEmployeeInterchangeDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				Long interchangeId = selectedEmployeeInterchangeDetails.getId();
				employeeInterchangeDetail = getInterchangeById(interchangeId);
				if (employeeInterchangeDetail.getSupportingDocuments() != null) {
					String filePathValue[] = employeeInterchangeDetail.getSupportingDocuments().split("uploaded/");
					log.info("Edu 0==>" + filePathValue[0]);
					log.info("Edu 1==>" + filePathValue[1]);
					fileName = filePathValue[1];
				}
				pageName = "update";
				return "/pages/personnelHR/employeeProfile/createInterchangeRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("CANCEL") || pageAction.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isApproveButton = true;
				isEditButton = true;
				isDeleteButton = true;
				interchangeClear();
				lazyDataInterchange();
				return "/pages/personnelHR/employeeProfile/listInterchangeRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				fileName = null;
				if (selectedEmployeeInterchangeDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				Long interchangeId = selectedEmployeeInterchangeDetails.getId();
				employeeInterchangeDetail = getInterchangeById(interchangeId);
				if (employeeInterchangeDetail.getSupportingDocuments() != null) {
					String filePathValue[] = employeeInterchangeDetail.getSupportingDocuments().split("uploaded/");
					log.info("Edu 0==>" + filePathValue[0]);
					log.info("Edu 1==>" + filePathValue[1]);
					fileName = filePathValue[1];
				}
				return "/pages/personnelHR/employeeProfile/viewInterchangeRequest.xhtml?faces-redirect=true";
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				log.info(":: Interchange Delete Response :: ");
				if (selectedEmployeeInterchangeDetails == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				} else if (selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("Approved")) {
					errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
					return null;
				} else {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmUserDelete').show();");
				}
				return "";
			}

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading employee profile bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void onRowInterSelect(SelectEvent event) {

		selectedEmployeeInterchangeDetails = ((EmployeeInterchangeDetails) event.getObject());
		if (selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("Approved")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("Rejected")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = true;
		} else if (selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("InProgress")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public String loadEmployeeTransfer() {
		try {
			isAddButton = true;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
			clear();
			loadLazyList();

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading employee profile bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/employeeProfile/listTransferDetails.xhtml?faces-redirect=true";

	}

	public void onRowSelect(SelectEvent event) {
		selectedEmpPromotionRequestDetails = ((EmpPromotionRequestDetails) event.getObject());
		/*
		 * if (selectedEmpPromotionRequestDetails.getAcceptStatus()==null) {
		 * viewButton=true; }else { viewButton=false; }
		 */
	}

	public String viewemployeeDetails() {
		log.info("======Employee Profile Bean viewemployeeDetails START=====");
		String promotionOptinvalue = commonDataService
				.getAppKeyValue(AppConfigEnum.PROMOTION_ACCEPTENCE_OPTIONS.toString());
		optionTakenList = Arrays.asList(promotionOptinvalue.split("\\s*,\\s*"));
		log.info("======Employee Profile Bean viewemployeeDetails END=====");
		if (selectedEmpPromotionRequestDetails.getAcceptStatus() != null) {
			acceptReject = true;
		}
		try {
			EmployeeMaster employeMaster = loginBean.getEmployee();
			log.info("Employee id==>" + employeMaster.getId());
			String url = SERVER_URL + "/employeepromotionrequest/getemployeedetailsbyid/" + employeMaster.getId();
			log.info("<=findPromotionEmployee URL=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					empPromotionRequestDetailsList = mapper.readValue(jsonResponse,
							new TypeReference<List<EmpPromotionRequestDetails>>() {
							});
					log.info("EmployeePromotionList with Size of : " + empPromotionRequestDetailsList.size());

					for (EmpPromotionRequestDetails empPromotionRequestDetails : empPromotionRequestDetailsList) {
						selectedEmpPromotionRequestDetails = empPromotionRequestDetails;
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.info("Exception in view ", e);
		}

		return "/pages/personnelHR/employeeProfile/viewPromotion.xhtml?faces-redirect=true";
	}

	public String employeeClear() {
		log.info("======Employee Profile Bean employeeClear START=====");
		selectedEmpPromotionRequestDetails = new EmpPromotionRequestDetails();
		log.info("======Employee Profile Bean employeeClear END=====");
		return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true";
	}

	public String clear() {
		log.info("Transfer application clear method called..");
		selectedEmployeeTransferDetails = new EmployeeTransferDetails();
		employeeTransferDetail = new EmployeeTransferDetails();
		headAndRegionalOfficeList = new ArrayList<EntityMaster>();
		transferType = null;
		loadLazyList();
		// disableButton = false;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		viewButton = true;
		return "/pages/personnelHR/employeeProfile/listTransferDetails.xhtml?faces-redirect=true";
	}

	public void clearTransfer() {
		log.info("clearTransfer call...");
		selectedEmployeeTransferDetails = new EmployeeTransferDetails();
		employeeTransferDetail = new EmployeeTransferDetails();
		headAndRegionalOfficeList = new ArrayList<EntityMaster>();
		transferType = null;
		loadLazyList();
		// disableButton = false;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		viewButton = true;

	}

	public String saveUpdateInterchange() {
		log.info("<---- : saveUpdateInterchange :------>" + employeeMaster);
		try {

			UserMaster user = login.getUserDetailSession();

			if (employeePersonalInfo.getEmployeeMaster() != null
					&& validateInterchangedEmployee(employeePersonalInfo.getEmployeeMaster().getId())) {
				AppUtil.addWarn("Already Given Request for interchange");
				return null;
			}

			log.info("getId==>" + user.getId());
			employeeInterchangeDetail.setEntityType(employeePersonalInfo.getEntityType());
			employeeInterchangeDetail.setEntity(employeePersonalInfo.getWorkLocation());
			employeeInterchangeDetail.setCurrentDesignation(employeePersonalInfo.getCurrentDesignation());
			employeeInterchangeDetail.setCurrentDepartment(employeePersonalInfo.getCurrentDepartment());
			employeeInterchangeDetail.setUserId(user.getId());
			employeeInterchangeDetail.setDateOfJoining(employeePersonalInfo.getDateOfJoining());
			employeeInterchangeDetail.setStatus("INPROGRESS");

			log.info("employeeInterchangeDetail==>" + employeeInterchangeDetail);
			url = EMP_SERVER_URL + "/saveinterchange";
			BaseDTO baseDTO = httpService.put(url, employeeInterchangeDetail);
			log.info("Save saveUpdateInterchange Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else if (baseDTO != null && baseDTO.getStatusCode() == 2000) {
				AppUtil.addWarn("Reporting Manager Not Available for you");
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeInterchangeDetails emp = mapper.readValue(jsonResponse, EmployeeInterchangeDetails.class);
			log.info("emp==>" + emp);
			if (emp != null) {
				employeeInterchangeDetail = emp;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (pageAction.equals("ADD")) {
					errorMap.notify(ErrorDescription.INTERCHANGE_SAVED.getErrorCode());

				} else {
					errorMap.notify(ErrorDescription.INTERCHANGE_UPDATE.getErrorCode());
				}
				interchangeClear();
				return "/pages/personnelHR/employeeProfile/listInterchangeRequest.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving employee transfer request with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;

	}

	@Getter
	@Setter
	EntityMaster headOrRegionFrom;

	public void getHeadAndRegionalOfficeByTransferType() {
		log.info("getHeadAndRegionalOfficeByTransferType Method start=====>");
		try {
			headAndRegionalOfficeList = new ArrayList<EntityMaster>();
			headOrRegionFrom = loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation();
			if (transferType.equalsIgnoreCase("IntraRegion")) {
				headAndRegionalOfficeList.add(headOrRegionFrom);
			} else {
				Integer entityCode = employeePersonalInfo.getWorkLocation().getCode();
				headAndRegionalOfficeList = getHeadRegionalOfficeList(transferType, entityCode);
			}
			/*
			 * if (transferType.equalsIgnoreCase("IntraRegion")) { headAndRegionalOfficeList
			 * = commonDataService.getPreExamMasterList(); } else if
			 * (transferType.equalsIgnoreCase("InterRegion")) { headAndRegionalOfficeList =
			 * commonDataService.loadHeadAndRegionalOffice(); List<EntityMaster>
			 * tnRegionalOfficeList = commonDataService.getPreExamMasterList();
			 * headAndRegionalOfficeList.removeAll(tnRegionalOfficeList); } else {
			 * headAndRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
			 * 
			 * }
			 */

		} catch (Exception ex) {
			log.info("error found in getHeadAndRegionalOfficeByTransferType Method", ex);
		}
		log.info("getHeadAndRegionalOfficeByTransferType Method End=====>");
	}

	public List<EntityMaster> getHeadRegionalOfficeList(String transferType, Integer entityCode) {
		log.info("<--- Inside getHeadRegionalOfficeList() --->");
		List<EntityMaster> entityMaster = null;
		try {
			if (transferType.equalsIgnoreCase("InterRegion")) {
				log.info("-----InterRegion------");
			} else if (transferType.equalsIgnoreCase("Section")) {
				entityCode = 0;
			}
			String requestPath = AppUtil.getPortalServerURL() + "/entitymaster/getHeadRegionalOfficeList/" + entityCode;
			BaseDTO baseDTO = httpService.get(requestPath);
			log.info("baseDTO==>" + baseDTO);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					entityMaster = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					log.info("getHeadRegionalOfficeList : " + entityMaster.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("<--- Exception in getHeadRegionalOfficeList() --->", e);
		}
		return entityMaster;
	}

	public String deleteTransfer() {
		try {

			if (selectedEmployeeTransferDetails == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedEmployeeTransferDetails.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + selectedEmployeeTransferDetails.getId());
			String getUrl = EMP_SERVER_URL + "/transferdelete/id/:id";
			url = getUrl.replace(":id", selectedEmployeeTransferDetails.getId().toString());
			log.info("deleteJobApplication Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);
			log.info("EmployeeType Master Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EMP_TRANSFER_DELETE_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

			loadLazyList();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/employeeProfile/listTransferDetails.xhtml?faces-redirect=true";
	}

	public void loadLazyList() {
		lazyApplicationModel = new LazyDataModel<EmployeeTransferDetails>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<EmployeeTransferDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getTransferSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<EmployeeTransferDetails> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeeTransferDetails>>() {
							});
					if (data == null) {
						log.info(" :: Job Advertisement Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(":: Job Advertisement Search Successfully Completed ::");
					} else {
						log.error(":: Job Advertisement Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeTransferDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeTransferDetails getRowData(String rowKey) {
				List<EmployeeTransferDetails> responseList = (List<EmployeeTransferDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeTransferDetails res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeTransferDetails = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public BaseDTO getTransferSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first, pageSize,
					sortField, sortOrder, filters);
			EmployeeTransferDetails request = new EmployeeTransferDetails();
			UserMaster user = login.getUserDetailSession();

			log.info("getId==>" + user.getId());
			request.setUserId(user.getId());
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			request.setFilters(filters);
			log.info("Search request data" + request);
			baseDTO = httpService.post(EMP_SERVER_URL + "/loadlazylist", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in employee search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public void lazyDataInterchange() {

		lazyInterchangeModel = new LazyDataModel<EmployeeInterchangeDetails>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<EmployeeInterchangeDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					EmployeeInterchangeDetails request = new EmployeeInterchangeDetails();
					UserMaster user = login.getUserDetailSession();

					log.info("getId==>" + user.getId());
					request.setUserId(user.getId());
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(EMP_SERVER_URL + "/interchange/lazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<EmployeeInterchangeDetails> data = mapper.readValue(jsonResponse,
							new TypeReference<List<EmployeeInterchangeDetails>>() {
							});
					if (data == null) {
						log.info(" :: Employee Interchange Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						interTotalRecord = baseDTO.getTotalRecords();
						log.info(":: Job Advertisement Search Successfully Completed ::");
					} else {
						log.error(":: Job Advertisement Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeInterchangeDetails res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeInterchangeDetails getRowData(String rowKey) {
				List<EmployeeInterchangeDetails> responseList = (List<EmployeeInterchangeDetails>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (EmployeeInterchangeDetails res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedEmployeeInterchangeDetails = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String interchangeClear() {
		log.info("Interchange application clear method called..");
		selectedEmployeeInterchangeDetails = new EmployeeInterchangeDetails();
		lazyDataInterchange();
		fileName = null;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		return "/pages/personnelHR/employeeProfile/listInterchangeRequest.xhtml?faces-redirect=true";
	}

	public String deleteInterchange() {
		try {

			if (selectedEmployeeInterchangeDetails == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedEmployeeInterchangeDetails.getStatus().equalsIgnoreCase("Approved")) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			}

			log.info("Selected Employee  Id : : " + selectedEmployeeInterchangeDetails.getId());
			String getUrl = EMP_SERVER_URL + "/delete/id/:id";
			url = getUrl.replace(":id", selectedEmployeeInterchangeDetails.getId().toString());
			log.info("delete employee interchange Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EMP_INTERCHANGE_DELETE_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

			loadLazyList();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/employeeProfile/listInterchangeRequest.xhtml?faces-redirect=true";
	}

	public void onTrainingRowSelect(SelectEvent event) {
		selectedTraining = ((Training) event.getObject());
		if (selectedTraining.getStatus().equalsIgnoreCase("Approved")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = false;
		} else if (selectedTraining.getStatus().equalsIgnoreCase("Rejected")) {
			isAddButton = false;
			isApproveButton = false;
			isEditButton = false;
			isDeleteButton = true;
		} else if (selectedTraining.getStatus().equalsIgnoreCase("InProgress")) {
			isAddButton = false;
			isApproveButton = true;
			isEditButton = true;
			isDeleteButton = true;
		}
	}

	public void lazyDataTraining() {

		lazyTraining = new LazyDataModel<Training>() {

			private static final long serialVersionUID = -560456931230099423L;

			@Override
			public List<Training> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {

					log.info("First : {}, Page Size : {}, Sort Field : {}, Sort Order : {}, Map Filters : {}", first,
							pageSize, sortField, sortOrder, filters);
					Training request = new Training();
					UserMaster user = login.getUserDetailSession();

					log.info("getId==>" + user.getId());
					request.setUserId(user.getId());
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					log.info("Search request data" + request);

					BaseDTO baseDTO = httpService.post(EMP_SERVER_URL + "/training/lazylist", request);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}

					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<Training> data = mapper.readValue(jsonResponse, new TypeReference<List<Training>>() {
					});
					if (data == null) {
						log.info(" :: Employee Interchange Failed to Deserialize ::");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						trainingTotalRecord = baseDTO.getTotalRecords();
						log.info(":: Training Search Successfully Completed ::");
					} else {
						log.error(":: Training Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error(":: Exception Exception Ocured while Loading Job Advertisement data ::", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(Training res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Training getRowData(String rowKey) {
				List<Training> responseList = (List<Training>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (Training res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedTraining = res;
						return res;
					}
				}
				return null;
			}
		};
	}

	public String deleteTraining() {
		try {

			if (selectedTraining == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			/*
			 * if(selectedTraining.getStatus().equalsIgnoreCase("Approved")) {
			 * errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode())
			 * ; return null; }
			 */

			log.info("Selected Employee  Id : : " + selectedTraining.getId());
			String getUrl = EMP_SERVER_URL + "/trainingdelete/id/:id";
			url = getUrl.replace(":id", selectedTraining.getId().toString());
			log.info("delete employee interchange Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.delete(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Employee Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.EMP_TRAINING_DELETE_SUCCESSFULLY.getErrorCode());
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

			lazyDataTraining();

		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/employeeProfile/listTrainingRequest.xhtml?faces-redirect=true";
	}

	public String clearTraining() {
		log.info("Training clear method called..");
		selectedTraining = new Training();
		lazyDataTraining();
		// disableButton = false;
		isAddButton = true;
		isApproveButton = true;
		isEditButton = true;
		isDeleteButton = true;
		return "/pages/personnelHR/employeeProfile/listTrainingRequest.xhtml?faces-redirect=true";
	}

	public String viewInterchange() {
		return "/pages/personnelHR/employeeProfile/viewInterchangeRequest.xhtml?faces-redirect=true";
	}

	public void onRowLoanSelect(SelectEvent event) {
		log.info(" onRowSelect select Loan and advance : ");
		addLoan = true;
		if (selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("APPROVED")
				|| selectedEmployeeLoanAndAdvance.getStatus().equalsIgnoreCase("REJECTED")) {
			editLoan = true;
		} else {
			editLoan = false;
		}

	}

	public String findPromotionEmployee() {
		try {
			log.info("<=findPromotionEmployee start=>");
			EmployeeMaster employeMaster = loginBean.getEmployee();
			log.info("Employee id==>" + employeMaster.getId());
			String url = SERVER_URL + "/employeepromotionrequest/getemployeedetailsbyid/" + employeMaster.getId();
			log.info("<=findPromotionEmployee URL=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					empPromotionRequestDetailsList = mapper.readValue(jsonResponse,
							new TypeReference<List<EmpPromotionRequestDetails>>() {
							});
					log.info("EmployeePromotionList with Size of : " + empPromotionRequestDetailsList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
			log.info("<=findPromotionEmployee end=>");
			return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true";

		} catch (Exception e) {
			log.error(":: Exception findPromotionEmployee ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public void showEmpPromotion() {
		try {
			log.info("<=showEmpPromotion start=>");

			getPromotion();
			viewPromotion = false;
			dinPromotion = false;
			accPromotion = false;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('emppopup').show();");
			log.info("<=showEmpPromotion end=>");

		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading find Promotion Employee ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void getPromotion() {
		try {
			log.info("getPromotionId==>" + selectedEmployeePromotionDetails.getPromotionId());

			String requestPath = EMP_SERVER_URL + "/selectedemppromotion/"
					+ selectedEmployeePromotionDetails.getPromotionId();
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				if (baseDTO.getResponseContent() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeAdminPromotionDTO = mapper.readValue(jsonResponse,
							new TypeReference<EmployeeAdminPromotionDTO>() {
							});
					log.info("employeeAdminPromotionDTO==>" + employeeAdminPromotionDTO);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}

		} catch (Exception e) {
			log.error(":: Exception Exception Ocured while Loading find Promotion Employee ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void declineEmpPromotion() {
		getPromotion();
		viewPromotion = true;
		dinPromotion = true;
		accPromotion = false;
		employeeAdminPromotionDTO.setEmpStatus("Rejected");
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('emppopup').show();");
	}

	public void accEmpPromotion() {
		getPromotion();
		viewPromotion = true;
		dinPromotion = true;
		accPromotion = true;
		employeeAdminPromotionDTO.setEmpStatus("Approved");
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('emppopup').show();");
	}

	public String updateEmployeePromotion() {

		log.info("<---- : updateEmployeePromotion :------>" + employeeAdminPromotionDTO);
		try {
			if (employeeAdminPromotionDTO.getEmpStatus().equalsIgnoreCase("Approved")) {
				log.info("exe==>" + employeeAdminPromotionDTO.getOptionExercised());
				if (employeeAdminPromotionDTO.getOptionExercised() == null
						|| employeeAdminPromotionDTO.getOptionExercised().equals("null")
						|| employeeAdminPromotionDTO.getOptionExercised().isEmpty()) {
					log.error("Selected The option exercised");
					Util.addWarn("Selected The option exercised");
					return null;
				}
				log.info("taken==>" + employeeAdminPromotionDTO.getOptionsTaken());
				if (employeeAdminPromotionDTO.getOptionExercised() != null
						&& employeeAdminPromotionDTO.getOptionExercised().equals("true")
						&& employeeAdminPromotionDTO.getOptionsTaken() == null
						|| employeeAdminPromotionDTO.getOptionsTaken().equals("null")
						|| employeeAdminPromotionDTO.getOptionsTaken().isEmpty()) {

					log.error("Selected The Options Taken");
					Util.addWarn("Selected The Options Taken");
					return null;
				}
			}
			if (employeeAdminPromotionDTO.getProComments() == null
					|| employeeAdminPromotionDTO.getProComments().equals("null")
					|| employeeAdminPromotionDTO.getProComments().isEmpty()) {
				log.error("Enter The comments");
				Util.addWarn("Enter The comments");
				return null;
			}
			url = EMP_SERVER_URL + "/updatepromotionstatus";
			BaseDTO baseDTO = httpService.put(url, employeeAdminPromotionDTO);
			log.info("Save Job Application Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeePromotionDetails empPro = mapper.readValue(jsonResponse, EmployeePromotionDetails.class);

			if (baseDTO.getStatusCode() == 0) {
				findPromotionEmployee();
				errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('emppopup').hide();");
				return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true";
			} else {
				log.info("Error while saving employee transfer request with error code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String empPromtion() {
		findPromotionEmployee();
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('emppopup').hide();");
		return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true";
	}

	public void loadOptionTaken(String name) {
		if (name.equals("true")) {
			takenStatus = true;
		} else {
			takenStatus = false;
		}
	}

	/**
	 * 
	 */
	public void changePasswordAction() {
		log.info("<========== Inside changePasswordAction() ===============>");
		newPassword = "";
		confirmPassword = "";
	}

	/**
	 * ChangeProfilePassword
	 */
	public void changeProfilePasswordAction() {
		log.info("<========== Inside changeProfilePasswordAction() ===============>");
		newProfilePassword = "";
		confirmProfilePassword = "";
	}

	// This pattern accept atleast one special character and atleast one lower case
	// and atleast one upper case maximum (6 -15)

	public String changePassword() {
		log.info("<========== Inside changePassword() ===============>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (StringUtils.isNotEmpty(newPassword)) {
				if (!Validate.validateAlphaNumericPassword(newPassword)) {
					errorMap.notify(ErrorDescription.PASSWORD_MUST_BE_ALPHANUMERIC.getErrorCode());
					return null;
				}
			}
			if (StringUtils.isEmpty(oldPassword)) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.OLD_PASSSWORD_REQUIRED).getErrorCode());
				return null;
			} else if (StringUtils.isEmpty(newPassword)) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_PASSWORD_REQUIRED).getErrorCode());
				return null;
			} else if (StringUtils.isEmpty(confirmPassword)) {
				errorMap.notify(ErrorDescription.getError(ErrorCode.CONFIRM_PASSWORD_REQUIRED).getErrorCode());
				return null;
			} else {
				if (newPassword.equals(confirmPassword)) {
					userDTO = new UserDTO();
					userDTO.setExistingPassword(oldPassword);
					userDTO.setNewPassword(newPassword);
					String url = EMP_SERVER_URL + "/changePassword";
					log.info("changePassword()" + url);
					baseDTO = httpService.post(url, userDTO);
					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {
							log.info("Password changed successfully");
							RequestContext.getCurrentInstance().execute("PF('changePassword').hide();");
							RequestContext.getCurrentInstance().execute("PF('changePasswordSuccess').show();");
						} else {
							errorMap.notify(baseDTO.getStatusCode());
							return null;
						}
					}
				} else {
					errorMap.notify(ErrorDescription.getError(ErrorCode.CONFIRM_PASSWORD_MISMATCH).getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("<========= Inside Exception ===========>", e);
		}
		return null;
	}

	public String changePasswordSuccessAction() {
		log.info("<========== Inside changePasswordSuccessAction() ===============>");
		try {
			login.logout();
			log.info("logout successfully");
		} catch (Exception e) {
			log.error("<========= Inside changePasswordSuccessAction:Exception ===========>", e);
		}
		return "/login.xhtml?faces-redirect=true?faces-redirect=true";
	}

	
	/**
	 * @param password
	 * @return
	 */
	public static int calculatePasswordStrength(String password) {

		int iPasswordScore = 0;

		if (password.length() < 8) {
			return 0;
		} else if (password.length() >= 10) {
			iPasswordScore += 2;
		} else {
			iPasswordScore += 1;
		}

		/*
		 * if password contains 2 digits, add 2 to score. if contains 1 digit add 1 to
		 * score
		 */
		if (password.matches("(?=.*[0-9].*[0-9]).*")) {
			iPasswordScore += 2;
		} else if (password.matches("(?=.*[0-9]).*")) {
			iPasswordScore += 1;
		}

		// if password contains 1 lower case letter, add 2 to score
		if (password.matches("(?=.*[a-z]).*")) {
			iPasswordScore += 2;
		}

		/*
		 * if password contains 2 upper case letters, add 2 to score. if contains only 1
		 * then add 1 to score.
		 */
		if (password.matches("(?=.*[A-Z].*[A-Z]).*")) {
			iPasswordScore += 2;
		} else if (password.matches("(?=.*[A-Z]).*")) {
			iPasswordScore += 1;
		}

		/*
		 * if password contains 2 special characters, add 2 to score. if contains only 1
		 * special character then add 1 to score.
		 */
		if (password.matches("(?=.*[~!@#$%^&*()_-].*[~!@#$%^&*()_-]).*")) {
			iPasswordScore += 2;
		} else if (password.matches("(?=.*[~!@#$%^&*()_-]).*")) {
			iPasswordScore += 1;
		}

		return iPasswordScore;

	}
	/*
	 * Change Profile Password
	 */

	public String changeProfilePassword() {
		log.info("<========== Inside changeProfilePassword() ===============>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			if (StringUtils.isNotEmpty(newProfilePassword)) {
				if (!Validate.validateAlphaNumericPassword(newProfilePassword)) {
					errorMap.notify(ErrorDescription.PASSWORD_MUST_BE_ALPHANUMERIC.getErrorCode());
					return null;
				}
			}
			if (StringUtils.isEmpty(oldProfilePassword)) {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.EXISTING_PROFILE_PASSWORD_REQUIRED).getErrorCode());
				return null;
			} else if (newProfilePassword.equals("")) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEW_PASSWORD_REQUIRED).getErrorCode());
				return null;
			} else if (confirmProfilePassword.equals("")) {
				errorMap.notify(ErrorDescription.getError(ErrorCode.CONFIRM_PASSWORD_REQUIRED).getErrorCode());
				return null;
			} else {
				if (newProfilePassword.equals(confirmProfilePassword)) {
					/*
					 * Controller: EmployeeProfileController.java
					 */
					userDTO = new UserDTO();
					userDTO.setExistingPassword(oldProfilePassword);
					userDTO.setNewPassword(newProfilePassword);
					String url = EMP_SERVER_URL + "/changeprofilepassword";
					log.info("changePassword()" + url);
					baseDTO = httpService.post(url, userDTO);
					if (baseDTO != null) {
						if (baseDTO.getStatusCode() == 0) {
							log.info("Password changed successfully");
							RequestContext.getCurrentInstance().execute("PF('changeProfilePassword').hide();");
							RequestContext.getCurrentInstance().execute("PF('changePasswordSuccess').show();");
						} else {
							errorMap.notify(baseDTO.getStatusCode());
							return null;
						}
					}
				} else {
					errorMap.notify(ErrorDescription.getError(ErrorCode.CONFIRM_PASSWORD_MISMATCH).getErrorCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error("<========= Inside changeProfilePassword()-Exception ===========>", e);
		}
		return null;
	}

	public String changeProfilePasswordSuccessAction() {
		log.info("<========== Inside changeProfilePasswordSuccessAction() ===============>");
		try {
			login.logout();
			log.info("logout successfully");
		} catch (Exception e) {
			log.error("<========= Inside changeProfilePasswordSuccessAction:Exception ===========>", e);
		}
		return "/login.xhtml?faces-redirect=true?faces-redirect=true";
	}

	public String showDetails() {
		log.info("<========== EmployeeProfileBean.showDetails() START===============>");
		try {
			if (acceptOption.equals("ACCEPT")) {
				acceptOrRejectStatus = true;
				if (selectedEmpPromotionRequestDetails.getPromotionOption() == null) {
					log.info(
							"<========== EmployeeProfileBean.showDetails() selectedEmpPromotionRequestDetails.getPromotionOption() is Empty===============>");
					Util.addWarn("Please Choose Whether Option Exercised");
					return null;
				}
				if (selectedEmpPromotionRequestDetails.getOptionTaken().equals("null")) {
					log.info(
							"<========== EmployeeProfileBean.showDetails() selectedEmpPromotionRequestDetails.getOptionTaken() is Empty===============>");
					Util.addWarn("Please Choose Options Taken");
					return null;
				}

			}
			if (acceptOption.equals("ACCEPT"))
				selectedEmpPromotionRequestDetails.setAcceptStatus(true);
			else
				selectedEmpPromotionRequestDetails.setAcceptStatus(false);

			empPromotionRequestDto.setEmpPromotionRequestDetails(selectedEmpPromotionRequestDetails);
			String url = AppUtil.getPortalServerURL() + "/employeepromotionrequest/promotionaccept";
			log.info("url" + url);
			BaseDTO baseDTO = httpService.post(url, empPromotionRequestDto);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				return findPromotionEmployee();
			}
			errorMap.notify(baseDTO.getStatusCode());
		} catch (Exception e) {
			log.info("<========== Exception Occured in EmployeeProfileBean.showDetails()===============>");
			log.info("<========== Exception is===============>" + e);
		}
		log.info("<========== EmployeeProfileBean.showDetails() END===============>");
		return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true";

	}

	public String clearPage() {
		log.info("<========== EmployeeProfileBean.clearPage() START===============>");
		selectedEmpPromotionRequestDetails = new EmpPromotionRequestDetails();
		log.info("<========== EmployeeProfileBean.clearPage() END===============>");
		return "/pages/personnelHR/employeeProfile/listpromotion.xhtml?faces-redirect=true;";

	}

	public void daysCaluclation()

	{
		log.info("<========== EmployeeProfileBean.daysCaluclation() START===============>");
		Date startDate = training.getStartDate();
		Date endDate = training.getEndDate();
		if (startDate != null && endDate != null) {
			Integer noOfDays = (int) ChronoUnit.DAYS.between(startDate.toInstant(), endDate.toInstant());
			training.setNoOfDays(noOfDays + 1);
		}

		log.info("<========== EmployeeProfileBean.daysCaluclation() END===============>");
	}

	public void validatePassword(FacesContext context, UIComponent comp, Object value) {
		log.info("inside validatePassword method");
		String text = (String) value;
		Pattern pattern;
		Matcher matcher;
		final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{6,15})";
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(text);
		boolean matchFound = matcher.matches();
		boolean valid = true;
		if (text == null || (text != null && text.length() < 6 || text.length() > 15)) {
			valid = false;
		} else if (text.length() >= 6 && text.length() <= 15) {
			if (!matchFound) {
				valid = false;
			}
		}
		if (!valid) {
			((UIInput) comp).setValid(false);
			FacesMessage message = new FacesMessage(
					"Password must contain both number and alphabets and the length must be between 6 and 15");
			context.addMessage(comp.getClientId(context), message);
		}
	}

	private boolean validateInterchangedEmployee(Long empId) {
		log.info("====START validateInterchangedEmployee Method==========");
		boolean isEmployee = false;
		try {
			EmployeeInterchangeDetails employeeInterchangeDetails = new EmployeeInterchangeDetails();
			String url = SERVER_URL + "/employeeprofile/getEmployee/" + empId;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeInterchangeDetails = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeInterchangeDetails>() {
						});
			}
			if (employeeInterchangeDetails != null && employeeInterchangeDetails.getId() != null) {
				isEmployee = true;
			}
		} catch (Exception e) {
			log.error("====Exception===", e);
		}
		log.info("====End validateInterchangedEmployee Method==========");
		return isEmployee;
	}
}
