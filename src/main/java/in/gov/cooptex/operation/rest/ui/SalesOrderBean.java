package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.apache.commons.lang.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.ApproveRejectCommentDTO;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CustomerQuotationRequestDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.AddressMaster;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.CustomerMaster;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.QuotationItem;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TaxCalculationUtil;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.master.rest.ui.AddressMasterBean;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.OrderStatus;
import in.gov.cooptex.operation.enums.SalesOrderStage;
import in.gov.cooptex.operation.model.Quotation;
import in.gov.cooptex.operation.model.SalesOrder;
import in.gov.cooptex.operation.model.SalesOrderAddress;
import in.gov.cooptex.operation.model.SalesOrderItems;
import in.gov.cooptex.operation.model.SalesOrderLog;
import in.gov.cooptex.operation.model.SalesOrderNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("salesOrderBean")
@Scope("session")
public class SalesOrderBean extends CommonBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String SALES_ORDER_LIST = "/pages/operation/productionplan/contract/listSalesOrder.xhtml?faces-redirect=true;";

	private final String SALES_ORDER_CREATE = "/pages/operation/productionplan/contract/createSalesOrder.xhtml?faces-redirect=true;";

	private final String SALES_ORDER_VIEW = "/pages/operation/productionplan/contract/viewSalesOrder.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AddressMasterBean addressMasterBean;

	@Getter
	@Setter
	String action = null, orderType = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	String activeStatus, billingAddressString = "", shippingAddressString = "";

	@Getter
	@Setter
	SalesOrder salesOrder, selectedSalesOrder;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	LazyDataModel<SalesOrder> salesOrderList;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	Integer size;

	@Getter
	@Setter
	List<Quotation> quotationList;

	@Getter
	@Setter
	Double materialValue = 0.0;

	@Getter
	@Setter
	Double totalCgstAmount = 0.0;

	@Getter
	@Setter
	Double totalSgstAmount = 0.0;

	@Getter
	@Setter
	Double totalCgstPercent = 0.0, totalSgstPercent = 0.0;

	@Getter
	@Setter
	Double totalTaxAmount = 0.0;

	@Getter
	@Setter
	Double totalDiscountAmount = 0.0;

	@Getter
	@Setter
	Double roundOffValue = 0.0;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	String amountInWords, customerName;

	@Getter
	@Setter
	List<CustomerMaster> customerMasterList;

	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeMasterList;

	@Getter
	@Setter
	CustomerTypeMaster customerTypeMaster;

	@Getter
	@Setter
	private String currentPlanStatus;

	@Getter
	@Setter
	String comments;

	@Getter
	@Setter
	SalesOrderNote salesOrderNote = new SalesOrderNote();

	@Getter
	@Setter
	SalesOrderNote salesOrderLastNote = new SalesOrderNote();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	Double footerTotal;

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	String billingAddressDetails;

	@Getter
	@Setter
	String shippingAddressDetails;

	@Getter
	@Setter
	List statusValues;

	@Getter
	@Setter
	SalesOrderItems salesOrderItems = new SalesOrderItems();

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productList;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<QuotationItem> quotationItemList;

	@Getter
	@Setter
	AddressMaster billingAddress = new AddressMaster();

	@Getter
	@Setter
	AddressMaster shippingAddress;

	@Getter
	@Setter
	List<AddressMaster> shippingAddressList;

	@Getter
	@Setter
	List<StateMaster> billingStateList;

	@Getter
	@Setter
	List<DistrictMaster> billingDistrictList;

	@Getter
	@Setter
	List<TalukMaster> billingTalukList;

	@Getter
	@Setter
	List<CityMaster> billingCityList;

	@Getter
	@Setter
	List<AreaMaster> billingAreaList;

	@Getter
	@Setter
	List<StateMaster> shippingStateList;

	@Getter
	@Setter
	List<DistrictMaster> shippingDistrictList;

	@Getter
	@Setter
	List<TalukMaster> shippingTalukList;

	@Getter
	@Setter
	List<CityMaster> shippingCityList;

	@Getter
	@Setter
	List<AreaMaster> shippingAreaList;

	@Getter
	@Setter
	List<StateMaster> stateList;

	@Getter
	@Setter
	private String[] billingAddressArray;

	@Getter
	@Setter
	private String[] shippingAddressArray;

	@Getter
	@Setter
	Double purchasePrice;

	@Getter
	@Setter
	Double retailPrice;

	@Getter
	@Setter
	Double supplierRate;

	@Getter
	@Setter
	Double profitMargin;

	@Getter
	@Setter
	List<Double> supplierValueList = new ArrayList<>();

	@Getter
	@Setter
	Double discountValue;

	@Getter
	@Setter
	SalesOrderItems selectedSalesOrderItems = new SalesOrderItems();

	@Getter
	@Setter
	SalesOrderAddress salesOrderAddress = new SalesOrderAddress();

	@Getter
	@Setter
	List<SalesOrderAddress> salesOrderAddressList = new ArrayList<SalesOrderAddress>();

	@Getter
	@Setter
	AddressMaster changeAddress = new AddressMaster();

	@Getter
	@Setter
	private String[] changeAddressArray;

	@Getter
	@Setter
	private String changeAddressValue;

	@Getter
	@Setter
	Boolean deliveryQuantityCheck = false;

	@Getter
	@Setter
	Boolean shippingAddressCheck = false;

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> approveCommentsDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<ApproveRejectCommentDTO> rejectCommentsDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<Map<String, Object>> employeeData = new ArrayList<>();

	@Getter
	@Setter
	String customerAddCountryName = null;

	@Getter
	final String countryName = "India";

	@Setter
	@Getter
	List<CountryMaster> countryMasterList;

	@Getter
	@Setter
	Double itemQuantity = 0.0;

	@Getter
	@Setter
	boolean disableFlag = false;

	@Getter
	@Setter
	List<Double> unitRateList = new ArrayList<>();

	@PostConstruct
	public void init() {
		log.info("SalesOrderBean. init() Calling");
		loadBasicData();
	}

	public SalesOrderBean() {
		log.info("<#======Inside SalesOrderBean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public void getAllBillingDistrictByState() {
		log.info(" Inside  getAllBillingDistrictByState() ");

		billingDistrictList = new ArrayList<>();

		if (billingAddress != null && billingAddress.getStateMaster() != null
				&& billingAddress.getStateMaster().getId() != null) {
			Long stateId = billingAddress.getStateMaster().getId();
			log.info("stateId : " + stateId);

			billingDistrictList = commonDataService.loadActiveDistrictsByState(stateId);
		} else {

			log.info(" getAllDistrictByState() addressMaster.getStateMaster().getId() ==null");

			billingDistrictList = new ArrayList<>();
			billingTalukList = new ArrayList<TalukMaster>();
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setDistrictMaster(new DistrictMaster());
			billingAddress.setTalukMaster(new TalukMaster());
			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllShippingDistrictByState() {
		log.info(" Inside  getAllShippingDistrictByState() ");

		shippingDistrictList = new ArrayList<>();

		if (shippingAddress != null && shippingAddress.getStateMaster() != null
				&& shippingAddress.getStateMaster().getId() != null) {
			Long stateId = shippingAddress.getStateMaster().getId();
			log.info("stateId : " + stateId);

			shippingDistrictList = commonDataService.loadActiveDistrictsByState(stateId);
		} else {

			log.info(" getAllDistrictByState() addressMaster.getStateMaster().getId() ==null");

			shippingDistrictList = new ArrayList<>();
			shippingTalukList = new ArrayList<TalukMaster>();
			shippingCityList = new ArrayList<CityMaster>();
			shippingAreaList = new ArrayList<AreaMaster>();

			shippingAddress.setDistrictMaster(new DistrictMaster());
			shippingAddress.setTalukMaster(new TalukMaster());
			shippingAddress.setCityMaster(new CityMaster());
			shippingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllBillingTalukByDistrict() {
		log.info("Inside getAllBillingTalukByDistrict ");

		billingTalukList = new ArrayList<>();

		if (billingAddress != null && billingAddress.getDistrictMaster() != null
				&& billingAddress.getDistrictMaster().getId() != null) {
			Long districtId = billingAddress.getDistrictMaster().getId();
			log.info("districtId : " + districtId);

			billingTalukList = commonDataService.loadActiveTaluksByDistrict(districtId);
		} else {

			log.info(" getAllTalukByDistrict() addressMaster.getDistrictMaster().getId() ==null");

			billingTalukList = new ArrayList<>();
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setTalukMaster(new TalukMaster());
			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllShippingTalukByDistrict() {
		log.info("Inside getAllShippingTalukByDistrict ");

		shippingTalukList = new ArrayList<>();

		if (shippingAddress != null && shippingAddress.getDistrictMaster() != null
				&& shippingAddress.getDistrictMaster().getId() != null) {
			Long districtId = shippingAddress.getDistrictMaster().getId();
			log.info("districtId : " + districtId);

			shippingTalukList = commonDataService.loadActiveTaluksByDistrict(districtId);
		} else {

			log.info(" getAllTalukByDistrict() addressMaster.getDistrictMaster().getId() ==null");

			shippingTalukList = new ArrayList<>();
			shippingCityList = new ArrayList<CityMaster>();
			shippingAreaList = new ArrayList<AreaMaster>();

			shippingAddress.setTalukMaster(new TalukMaster());
			shippingAddress.setCityMaster(new CityMaster());
			shippingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllBillingCityByTaluk() {
		log.info(" Inside  " + billingAddress);
		billingCityList = new ArrayList<CityMaster>();

		if (billingAddress != null && billingAddress.getTalukMaster() != null
				&& billingAddress.getTalukMaster().getId() != null) {
			Long districtId = billingAddress.getDistrictMaster().getId();
			Long talukId = billingAddress.getTalukMaster().getId();
			log.info("districtId : " + districtId);
			log.info("talukId : " + talukId);

			billingCityList = commonDataService.loadActiveCitiesByDistrict(districtId);

		} else {
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllShippingCityByTaluk() {
		log.info(" Inside  " + shippingAddress);
		shippingCityList = new ArrayList<CityMaster>();

		if (shippingAddress != null && shippingAddress.getTalukMaster() != null
				&& shippingAddress.getTalukMaster().getId() != null) {
			Long districtId = shippingAddress.getDistrictMaster().getId();
			Long talukId = shippingAddress.getTalukMaster().getId();
			log.info("districtId : " + districtId);
			log.info("talukId : " + talukId);

			shippingCityList = commonDataService.loadActiveCitiesByDistrict(districtId);

		} else {
			shippingCityList = new ArrayList<CityMaster>();
			shippingAreaList = new ArrayList<AreaMaster>();

			shippingAddress.setCityMaster(new CityMaster());
			shippingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public String getEditPage() {
		action = "EDIT";
		orderType = null;
		loadBasicData();
		if (salesOrder == null || salesOrder.getId() == null) {
			errorMap.notify(ErrorDescription.SALES_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		try {

			getViewPage();

			if ("SUBMITTED".equalsIgnoreCase(selectedSalesOrder.getSalesOrderStatus())
					|| "FINAL_APPROVED".equalsIgnoreCase(selectedSalesOrder.getSalesOrderStatus())
					|| "APPROVED".equalsIgnoreCase(selectedSalesOrder.getSalesOrderStatus())) {
				errorMap.notify(ErrorDescription.SALES_ORDER_CANNOT_BE_MODIFIED.getCode());
				return null;
			}

			// calculateTax(salesOrder.getSalesOrderItemsList());

			if (salesOrder.getCustomerMaster() != null) {
				customerTypeMaster = salesOrder.getCustomerMaster().getCustomerTypeMaster();
			}
			/*
			 * if (customerTypeMaster != null && customerTypeMaster.getId() != null) {
			 * customerMasterList =
			 * commonDataService.getCustomerListByType(customerTypeMaster.getId()); }
			 */

			if (salesOrder.getQuotation() != null && salesOrder.getQuotation().getId() != null) {
				Quotation quotation = new Quotation(salesOrder.getQuotation().getId(),
						salesOrder.getQuotation().getQuotationNumber(),
						salesOrder.getQuotation().getQuotationNumberPrefix());
				salesOrder.setQuotation(quotation);
				setOrderType("withQuotation");
				if (salesOrder.getCustomerMaster() != null && salesOrder.getCustomerMaster().getId() != null) {
					quotationList = commonDataService
							.getQuotationListForCustomer(salesOrder.getCustomerMaster().getId());
				}
			} else {
				setOrderType("withoutQuotation");
			}
			if (salesOrder.getBillingAddress() != null) {
				billingAddress = salesOrder.getBillingAddress();
				billingAddressArray = AppUtil.prepareAddress(salesOrder.getBillingAddress());
			}
			if (salesOrder.getShippingAddress() != null) {
				shippingAddress = salesOrder.getShippingAddress();
				shippingAddressArray = AppUtil.prepareAddress(salesOrder.getShippingAddress());
			}
			salesOrderNote = new SalesOrderNote();
			if (salesOrder.getSalesOrderNoteList() != null && salesOrder.getSalesOrderNoteList().size() > 0) {
				salesOrderNote = salesOrder.getSalesOrderNoteList().get(0);
			}

			/*
			 * if (salesOrder.getSalesOrderItemsList() != null) { supplierValueList = new
			 * ArrayList<>(); List<SalesOrderItems> salesOrderItemList =
			 * salesOrder.getSalesOrderItemsList(); for (SalesOrderItems item :
			 * salesOrderItemList) { Double supplierValue = (item.getItemTotal() -
			 * item.getDiscountValue()); supplierValueList.add(supplierValue); } }
			 */
		} catch (Exception e) {
			log.info("Exception while get edit page....." + e);
		}

		return SALES_ORDER_CREATE;
	}

	public void getAllBillingAreaListByTaluk() {
		log.info("<< == Auto Complete method to getAllBillingAreaListByTaluk == >> " + billingAddress);
		billingAreaList = new ArrayList<AreaMaster>();

		if (billingAddress != null && billingAddress.getCityMaster() != null
				&& billingAddress.getCityMaster().getId() != null) {
			Long cityId = billingAddress.getCityMaster().getId();
			log.info("cityId : " + cityId);

			billingAreaList = commonDataService.loadActiveAreasByCity(cityId);
		} else {
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void saveBillingAddress() {
		log.info(" Inside saveBillingAddress " + billingAddress);
		if (billingAddress == null) {
			billingAddress = new AddressMaster();
		}
		billingAddressArray = AppUtil.prepareAddress(billingAddress);
		changeAddress = new AddressMaster();
		changeAddress.setStateMaster(new StateMaster());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('billingaddrdialog').hide();");
	}

	public void saveShippingAddress() {
		log.info(" Inside saveShippingAddress " + shippingAddress);
		if (shippingAddress == null) {
			shippingAddress = new AddressMaster();
		}
		shippingAddressList.add(shippingAddress);
		shippingAddress = new AddressMaster();
		shippingAddressArray = AppUtil.prepareAddress(shippingAddress);
	}

	public void getAllShippingAreaListByTaluk() {
		log.info("<< == Auto Complete method to getAllShippingAreaListByTaluk == >> " + shippingAddress);
		billingAreaList = new ArrayList<AreaMaster>();

		if (shippingAddress != null && shippingAddress.getCityMaster() != null
				&& shippingAddress.getCityMaster().getId() != null) {
			Long cityId = shippingAddress.getCityMaster().getId();
			log.info("cityId : " + cityId);

			shippingAreaList = commonDataService.loadActiveAreasByCity(cityId);
		} else {
			shippingAreaList = new ArrayList<AreaMaster>();

			shippingAddress.setAreaMaster(new AreaMaster());
		}

		log.info("areaList ===>>>>" + shippingAreaList.size());
	}

	public void clearSalesOrderItems() {
		log.info("start clearSalesOrderItems ");
		salesOrderItems = new SalesOrderItems();
		// productGroupMaster = null;
		// productCategory = null;
		// productList = new ArrayList<>();
		// productGroupList = new ArrayList<>();
		salesOrderItems.setProductVarietyMaster(null);
		salesOrderItems.setItemQty(null);
		// productCategory = null;
		// productGroupMaster = null;
		// salesOrderItems.setProductVarietyMaster(null);
		retailPrice = null;
		setRetailPrice(null);
		purchasePrice = null;
		supplierRate = null;
		setSupplierRate(null);
		// salesOrderItems.setItemQty(null);
		profitMargin = null;
		discountValue = null;
		salesOrderItems = new SalesOrderItems();
		log.info("end clearSalesOrderItems ");
	}

	public void onSelectCustomerTypeMaster() {
		log.info("onSelectCustomerTypeMaster [" + salesOrder + "]");

		salesOrder.setCustomerMaster(null);
		salesOrder.setPoRefNumber(null);
		salesOrder.setQuotation(null);
		salesOrder.setSalesOrderItemsList(new ArrayList<>());
		gstPercentageWiseDTOList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		amountInWords = "";
		customerAddCountryName = null;
		if (salesOrder != null) {
			salesOrder.setCustomerMaster(null);

			if (customerTypeMaster != null && customerTypeMaster.getId() != null) {
				// customerMasterList =
				// commonDataService.getCustomerListByType(customerTypeMaster.getId());
				customerMasterList = loadAllCustomerByCustomerType(customerTypeMaster.getId());
			}
		}
	}

	private List<CustomerMaster> loadAllCustomerByCustomerType(Long customerTypeId) {

		log.info("Loading all customer ............");

		List<CustomerMaster> customerMasterList = null;

		try {
			/*
			 * CustomerMasterController.java - Method: getAllCustomersByType()
			 */
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/customerMaster/customerlist/" + customerTypeId;

			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
				});

			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerMasterList;

	}

	public void onSelectCustomerMaster() {
		log.info("onSelectCustomerMaster [" + salesOrder + "]");

		salesOrder.setPoRefNumber(null);
		salesOrder.setQuotation(null);
		gstPercentageWiseDTOList = new ArrayList<>();
		salesOrder.setSalesOrderItemsList(new ArrayList<>());
		gstWiseList = new ArrayList<>();
		amountInWords = "";

		if (salesOrder != null) {

			if (salesOrder.getCustomerMaster() != null && salesOrder.getCustomerMaster().getId() != null) {
				quotationList = commonDataService.getQuotationListForCustomer(salesOrder.getCustomerMaster().getId());
			}
		}
		CustomerMaster customerMaster = new CustomerMaster();
		if (salesOrder.getCustomerMaster() != null && salesOrder.getCustomerMaster().getId() != null) {
			customerMaster = getCustomerDetails(salesOrder.getCustomerMaster().getId());
		}
		if (customerMaster != null) {
			salesOrder.setCustomerMaster(customerMaster);
			if (customerMaster.getBillingAddressMaster() != null) {
				billingAddressArray = AppUtil.prepareAddress(customerMaster.getBillingAddressMaster());
				salesOrder.setBillingAddress(customerMaster.getBillingAddressMaster());
				billingAddress = customerMaster.getBillingAddressMaster();
				billingAddressString = addressMasterBean.prepareAddress(customerMaster.getBillingAddressMaster());
			}
			if (customerMaster.getShippingAddressMaster() != null) {
				changeAddressArray = AppUtil.prepareAddress(customerMaster.getBillingAddressMaster());
				salesOrder.setShippingAddress(customerMaster.getShippingAddressMaster());
				shippingAddress = customerMaster.getShippingAddressMaster();
				shippingAddressString = addressMasterBean.prepareAddress(customerMaster.getShippingAddressMaster());
			}

			if (customerMaster.getAddressMaster() != null) {
				CountryMaster country = customerMaster.getAddressMaster().getCountryMaster();
				if (country != null) {
					customerAddCountryName = country != null ? country.getName() : null;
				} else {
					errorMap.notify(ErrorDescription.COUNTRY_MASTER_SHOULD_NOT_BE_NULL.getCode());
					return;
				}

			} else {
				errorMap.notify(ErrorDescription.CUST_ADDRESS_REQUIRED.getCode());
				return;
			}

		}
	}

	public void onSelectQuotation() {
		log.info("onSelectQuotation [" + salesOrder + "]");
		salesOrder.setSalesOrderItemsList(new ArrayList<>());
		gstPercentageWiseDTOList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		amountInWords = "";
		if (salesOrder != null) {
			if (salesOrder.getQuotation() != null && salesOrder.getQuotation().getId() != null) {
				quotationItemList = commonDataService.getQuotationItemListById(salesOrder.getQuotation().getId());
			}
		}

	}

	public String saveSalesOrder(String requestType) {
		log.info("SalesOrderBean.saveSalesOrder method started");
		if (action.equals("EDIT")) {
			Iterator<SalesOrderAddress> salesOrderAddressItr = salesOrderAddressList.iterator();
			while (salesOrderAddressItr.hasNext()) {
				SalesOrderAddress salesOrderObject = salesOrderAddressItr.next();
				Integer count = (int) salesOrderAddressList.stream().filter(
						i -> i.getProductVarietyCodeAndName().equals(salesOrderObject.getProductVarietyCodeAndName()))
						.count();
				for (SalesOrderAddress salesOrderAddressObjects : salesOrderAddressList) {
					if (salesOrderAddressObjects.getProductVarietyCodeAndName()
							.equals(salesOrderObject.getProductVarietyCodeAndName())
							&& salesOrderAddressObjects.getDelievryAddress()
									.equals(salesOrderObject.getDelievryAddress())
							&& count > 1) {
						Util.addWarn("Delivery Address Cannot be Same for Same Product");
						return null;
					}

				}
				Double editDeliverQuantity = salesOrderAddressList.stream().filter(
						i -> i.getProductVarietyCodeAndName().equals(salesOrderObject.getProductVarietyCodeAndName()))
						.mapToDouble(SalesOrderAddress::getQtyToDeliver).sum();
				Double totalOrderQuantity = salesOrderAddressList.stream().filter(
						i -> i.getProductVarietyCodeAndName().equals(salesOrderObject.getProductVarietyCodeAndName()))
						.mapToDouble(SalesOrderAddress::getOrderQty).sum();
				if (totalOrderQuantity < editDeliverQuantity) {
					Util.addWarn("Deliver Quantity Must be less than Order Quantity");
					return null;
				}
			}
		}
		if (salesOrder.getValidityDate() != null && salesOrder.getExpectedDeliveryDate() != null
				&& salesOrder.getValidityDate().getTime() > salesOrder.getExpectedDeliveryDate().getTime()) {
			errorMap.notify(ErrorDescription.SALES_ORDER_DATE.getCode());
			return null;
		}

		if (billingAddress == null || billingAddress.getAddressLineOne() == null) {
			errorMap.notify(ErrorDescription.SALES_ORDER_BILLING_ADDRESS_NOT_NULL.getCode());
			return null;
		}

		if (salesOrderAddressList.isEmpty()) {
			errorMap.notify(ErrorDescription.SALES_ORDER_SHIPPING_ADDRESS_NOT_NULL.getCode());
			return null;
		}

		if (salesOrderNote == null || salesOrderNote.getNote() == null) {
			errorMap.notify(ErrorDescription.SALES_ORDER_NOTE_NOT_NULL.getCode());
			return null;
		}
		List<SalesOrderItems> itemLists = salesOrder.getSalesOrderItemsList();
		int itemListsSize = itemLists != null ? itemLists.size() : 0;

		if (itemListsSize > 0) {

			for (SalesOrderItems items : itemLists) {
				double orderQty = items.getItemQty();
				double deliveryQty = salesOrderAddressList.stream().filter(o -> o.getProductVarietyId() != null
						&& o.getUnitRate() != null && items.getUnitRate() != null
						&& items.getProductVarietyMaster().getId() != null
						&& o.getProductVarietyId().longValue() == items.getProductVarietyMaster().getId().longValue()
						&& o.getUnitRate().doubleValue() == items.getUnitRate().doubleValue())
						.mapToDouble(SalesOrderAddress::getQtyToDeliver).sum();
				// double deliveryQty =
				// salesOrderAddressList.stream().filter(o-> o.getProductVarietyId() != null &&
				// o.getUnitRate() != null &&
				// items.getUnitRate() != null && items.getProductVarietyMaster().getId() !=
				// null &&
				// o.getProductVarietyId() == items.getProductVarietyMaster().getId() &&
				// o.getUnitRate().doubleValue() == items.getUnitRate().doubleValue()).
				// collect(Collectors.summingDouble(SalesOrderAddress::getQtyToDeliver));

				if (orderQty == deliveryQty) {
					log.info("Order and Delivery qty is equal.");
				} else {
					Util.addWarn("Deliver Quantity Must be equal to Order Quantity");
					return null;
				}
			}
		}
		salesOrder.setMaterialValue(materialValue);

		double totalValue = (materialValue - totalDiscountAmount) + totalTaxAmount;
		double roundedValue = Math.round(totalValue);
		salesOrder.setTotalValue(totalValue);
		salesOrder.setRoundOffValue(roundedValue - totalValue);
		salesOrder.setDiscountValue(totalDiscountAmount);
		salesOrder.setNetValue(roundedValue);
		salesOrder.setBillingAddress(billingAddress);
		salesOrder.setShippingAddress(shippingAddress);
		salesOrder.setSalesOrderNoteList(new ArrayList<>());
		salesOrder.getSalesOrderNoteList().add(salesOrderNote);
		salesOrder.setSalesOrderAddressList(salesOrderAddressList);
		salesOrder.setSyncStatus(false);

		SalesOrderLog salesOrderLog = new SalesOrderLog();

		if (requestType.equals(SalesOrderStage.INITIATED.name())) {
			salesOrderLog.setStage(SalesOrderStage.INITIATED);
		} else {
			salesOrderLog.setStage(SalesOrderStage.SUBMITTED);
		}
		if (salesOrder.getSalesOrderLogList() == null) {
			salesOrder.setSalesOrderItemsList(new ArrayList<>());
		}
		salesOrder.getSalesOrderLogList().add(salesOrderLog);

		String url = serverURL + appPreference.getOperationApiUrl() + "/salesorder/create";
		log.info("<<===  URL:: " + url);

		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, salesOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Sales Order saved or submitted successfully");
					if (requestType.equals(SalesOrderStage.INITIATED.name())) {
						errorMap.notify(ErrorDescription.SALES_ORDER_SAVE_SUCCESS.getCode());
					} else {
						errorMap.notify(ErrorDescription.SALES_ORDER_SUBMITTED.getCode());
					}
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in Sales Order create :: saveSalesOrder==> ", exp);
			errorMap.notify(1);
			return null;
		}
		return SALES_ORDER_LIST;
	}

	public void changeUnit(List<SalesOrderItems> itemsList) {
		for (SalesOrderItems item : itemsList) {
			if (item.getItemQty() != null && item.getUnitRate() != null) {
				item.setItemAmount(item.getItemQty() * item.getUnitRate());
				Double taxValue = item.getItemAmount() * (item.getTaxPercent().doubleValue() / 100);
				item.setTaxValue(taxValue);
				if (item.getDiscountPercent() != null && item.getItemAmount() != null) {
					item.setDiscountValue(item.getItemAmount() * (item.getDiscountPercent().doubleValue() / 100));
				}
				item.setItemTotal(item.getItemAmount());
				item.setSupplierValue(item.getItemQty() * item.getUnitRate());
			}
			String productVarietyCodeAndName = item.getProductVarietyMaster().getCode() + " / "
					+ item.getProductVarietyMaster().getName();
			if (salesOrderAddressList.stream()
					.anyMatch(soa -> soa.getProductVarietyCodeAndName().equalsIgnoreCase(productVarietyCodeAndName))) {
				salesOrderAddressList.stream().forEach(sc -> {
					if (productVarietyCodeAndName.equalsIgnoreCase(sc.getProductVarietyCodeAndName())) {
						sc.setOrderQty(item.getItemQty());
						sc.setQtyToDeliver(item.getItemQty());
					}
				});
			}
		}
		calculateTax(itemsList);
	}

	public void calculateTax(List<SalesOrderItems> itemsList) {
		if (itemsList != null && itemsList.size() > 0) {
			List<GstSummeryDTO> gstList = new ArrayList<>();
			for (SalesOrderItems item : itemsList) {
				if (item != null && item.getProductVarietyMaster() != null) {
					StringBuilder productId = new StringBuilder(item.getProductVarietyMaster().getId().toString());
					// gstList.addAll(commonDataService.loadTaxForProduct(productId,
					// AppUtil.ifNullRetunZero(item.getItemAmount())));

//					gstList.addAll(commonDataService.loadTaxForProduct(productId,
//							AppUtil.ifNullRetunZero(item.getUnitRate())));

					Double discountVal = (item.getUnitRate() * (item.getDiscountPercent() / 100));
					Double unitrateForSingleQty = item.getUnitRate() - discountVal;
					gstList.addAll(commonDataService.loadTaxForProduct1(productId, unitrateForSingleQty));
				}
			}
			ItemDataWithGstDTO itemDataWithGstDTO = new ItemDataWithGstDTO();
			String appValue = commonDataService.getAppKeyValue(AppConfigKey.CALCULATE_GST_FOR_FREE_SAMPLE);
			boolean calculateGstForFreeSampleFlag = "true".equals(appValue) ? true : false;
			itemDataWithGstDTO.setCalculateGstFreeSampleFlag(calculateGstForFreeSampleFlag);
			itemDataWithGstDTO.setSalesOrderType(orderType);
			itemDataWithGstDTO = TaxCalculationUtil.calculateSalesOrderTax1(gstList, itemsList, itemDataWithGstDTO);
			gstWiseList = itemDataWithGstDTO.getGstWiseList();
			gstPercentageWiseDTOList = itemDataWithGstDTO.getGstPercentageWiseDTOList();
			calculateTotalValues(itemsList);
		}
	}

	public void clearSearchData() {
		salesOrder = new SalesOrder();
		customerTypeMaster = null;
		gstPercentageWiseDTOList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		totalDiscountAmount = 0.0;
		materialValue = 0.0;
		totalCgstAmount = 0.0;
		totalSgstAmount = 0.0;
		totalTaxAmount = 0.0;
		billingAddress = new AddressMaster();
		shippingAddress = new AddressMaster();
		salesOrderNote = new SalesOrderNote();
		billingAddressArray = null;
		shippingAddressArray = null;
		amountInWords = null;
		retailPrice = null;
		purchasePrice = null;
		supplierRate = null;
		profitMargin = null;
		discountValue = null;
		supplierValueList = new ArrayList<>();
	}

	public Double getSalseValue(int i) {
		Double val = 0D;
		try {
			if (supplierValueList != null && supplierValueList.get(i) > 0)
				val = supplierValueList.get(i);
		} catch (Exception e) {
			log.info("Excepion getSalesValue...." + e);
		}
		return val;
	}

	public String searchItemData() {
		disableFlag = true;
		log.info("order type is :::" + orderType);
		if (orderType.equalsIgnoreCase("freeSample")) {
			supplierRate = 0D;
		}
		if (billingAddress == null) {
			billingAddress = new AddressMaster();
			billingAddress.setStateMaster(new StateMaster());
		}
		changeAddress = new AddressMaster();
		changeAddress.setStateMaster(new StateMaster());
		if (orderType.equalsIgnoreCase("withoutQuotation") || orderType == null) {
			log.info("order type is null or withoutQuotation , so get quotationItemList function will not call");
			return null;
		}

		if (salesOrder.getValidityDate() != null && salesOrder.getExpectedDeliveryDate() != null
				&& salesOrder.getValidityDate().getTime() > salesOrder.getExpectedDeliveryDate().getTime()) {
			errorMap.notify(ErrorDescription.SALES_ORDER_DATE.getCode());
			return null;
		}
		List<SalesOrderItems> salesOrderItemsList = new ArrayList<>();
		salesOrder.setSalesOrderItemsList(new ArrayList<>());
		if (quotationItemList == null) {
			if (salesOrder.getQuotation() != null && salesOrder.getQuotation().getId() != null) {
				quotationItemList = commonDataService.getQuotationItemListById(salesOrder.getQuotation().getId());
			}
		}
		if (quotationItemList != null) {
			for (QuotationItem quotationItem : quotationItemList) {
				SalesOrderItems soItem = new SalesOrderItems();
				soItem.setProductVarietyMaster(quotationItem.getProductVarietyMaster());
				if (quotationItem.getProductVarietyMaster() != null) {
					soItem.setUomMaster(quotationItem.getProductVarietyMaster().getUomMaster());
				} else {
					soItem.setUomMaster(quotationItem.getUom());
				}
				soItem.setItemQty(quotationItem.getItemQuantity());
				soItem.setUnitRate(AppUtil.ifNullRetunZero(quotationItem.getUnitRate()));
				soItem.setDiscountPercent(AppUtil.ifNullRetunZero(quotationItem.getDiscountPercent()));
				soItem.setDiscountValue(AppUtil.ifNullRetunZero(quotationItem.getDiscountValue()));
				soItem.setTaxPercent(AppUtil.ifNullRetunZero(quotationItem.getTaxPercent().doubleValue()));
				/*
				 * if(salesOrder.getCustomerMaster().getAddressMaster().getCountryMaster().
				 * getName().equalsIgnoreCase("India") &&
				 * orderType.equalsIgnoreCase("freeSample")) {
				 * soItem.setTaxPercent(AppUtil.ifNullRetunZero(quotationItem.getTaxPercent().
				 * doubleValue())); }else { soItem.setTaxPercent(0D); }
				 */
				soItem.setTaxValue(AppUtil.ifNullRetunZero(quotationItem.getTaxValue()));
				soItem.setItemTotal(AppUtil.ifNullRetunZero(quotationItem.getItemQuantity() * soItem.getUnitRate()));
				soItem.setItemAmount(AppUtil.ifNullRetunZero(quotationItem.getItemAmount()));
				salesOrderItemsList.add(soItem);
			}
		}
		salesOrder.setSalesOrderItemsList(salesOrderItemsList);
		calculateTax(salesOrder.getSalesOrderItemsList());
		salesOrderItems = new SalesOrderItems();
		try {
			if (countryName.equals(customerAddCountryName)) {
				Double amountWord = AppUtil.ifNullRetunZero(totalTaxAmount)
						+ (AppUtil.ifNullRetunZero(materialValue) - AppUtil.ifNullRetunZero(totalDiscountAmount));
				if (amountWord >= 0) {
					amountInWords = AmountToWordConverter.converter(String.valueOf(AppUtil
							.ifNullRetunZero(totalTaxAmount)
							+ (AppUtil.ifNullRetunZero(materialValue) - AppUtil.ifNullRetunZero(totalDiscountAmount))));
				}
			} else {
				Double amountWord = AppUtil.ifNullRetunZero(materialValue)
						- AppUtil.ifNullRetunZero(totalDiscountAmount);
				if (amountWord >= 0) {
					amountInWords = AmountToWordConverter.converter(String.valueOf(
							AppUtil.ifNullRetunZero(materialValue) - AppUtil.ifNullRetunZero(totalDiscountAmount)));
				}
			}

			// Double amountWord = AppUtil.ifNullRetunZero(totalTaxAmount)
			// + (AppUtil.ifNullRetunZero(materialValue) -
			// AppUtil.ifNullRetunZero(totalDiscountAmount));
			// if (amountWord >= 0) {
			// amountInWords =
			// AmountToWordConverter.converter(String.valueOf(AppUtil.ifNullRetunZero(totalTaxAmount)
			// + (AppUtil.ifNullRetunZero(materialValue) -
			// AppUtil.ifNullRetunZero(totalDiscountAmount))));
			// }
		} catch (Exception e) {
			log.error("Error while amountInWords", e);
			errorMap.notify(1);
			return null;
		}

		return null;

	}

	public void changeProductCategory() {
		log.info("Inside changeProductCategory");
		salesOrderItems = new SalesOrderItems();
		productGroupMaster = null;
		salesOrderItems.setProductVarietyMaster(null);
		salesOrderItems.setItemQty(null);

		if (productCategory != null) {
			log.info("Selected Product Category " + productCategory.getProductCatCode());
			productGroupList = commonDataService.loadProductGroups(productCategory);
		} else {
			productGroupList = new ArrayList<ProductGroupMaster>();
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void changeProductGroup() {
		log.info("Inside changeProductGroup");

		salesOrderItems.setProductVarietyMaster(null);
		salesOrderItems.setItemQty(null);

		if (productGroupMaster != null) {
			log.info("Selected Product Group " + productGroupMaster);
			productList = commonDataService.loadProductVarieties(productGroupMaster);
		} else {
			productList = new ArrayList<ProductVarietyMaster>();
		}
	}

	public void changesProductVariety() {
		log.info("Inside changesProductVariety");

		salesOrderItems.setItemQty(null);

		if (salesOrderItems != null && salesOrderItems.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + salesOrderItems.getProductVarietyMaster());
			getPrices();
		}
	}

	public void getPrices() {
		log.info("get retail and purchase details");
		BaseDTO baseDTO = null;
		CustomerQuotationRequestDTO quotationRate = new CustomerQuotationRequestDTO();
		if (salesOrderItems != null && salesOrderItems.getProductVarietyMaster() != null) {
			log.info("Selected Product Variety " + salesOrderItems.getProductVarietyMaster().getCode());
			Long prodId = salesOrderItems.getProductVarietyMaster().getId();
			Long catId = productCategory.getId();
			try {
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/sales/quotation/findUnitRateByProductId/" + prodId + "/" + catId;
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					quotationRate = mapper.readValue(jsonValue, CustomerQuotationRequestDTO.class);

					purchasePrice = quotationRate.getPurchasePrice();
					log.info("Purchase price is...:" + purchasePrice);
					retailPrice = quotationRate.getRetailPrice();
					/*
					 * try { if (salesOrder.getCustomerMaster() != null &&
					 * salesOrder.getCustomerMaster().getAddressMaster() != null &&
					 * salesOrder.getCustomerMaster().getAddressMaster().getStateMaster().getCode()
					 * .equalsIgnoreCase("TN")) { if (quotationRate.getMarginInnerState() != null) {
					 * retailPrice = quotationRate.getMarginInnerState(); } else { retailPrice =
					 * quotationRate.getMarginFixedInnerValue(); } } else { if
					 * (quotationRate.getMarginOuterState() != null) { retailPrice =
					 * quotationRate.getMarginOuterState(); } else { retailPrice =
					 * quotationRate.getMarginFixedOuterValue(); } } retailPrice = purchasePrice +
					 * ((purchasePrice * retailPrice) / 100); log.info("retail price is...:" +
					 * retailPrice); } catch (Exception e) {
					 * log.info("Exception while setting margin price" + e); }
					 */
				}
			} catch (Exception e) {
				log.info("Exception getPrice from category" + e);
			}
		}
	}

	public boolean amountValidation() {
		boolean validate = true;
		FacesContext fc = FacesContext.getCurrentInstance();
		if (purchasePrice != null && supplierRate != null && purchasePrice >= supplierRate) {
			fc.addMessage("supplierRate", new FacesMessage(ErrorCodeDescription.getDescription(290697)));
			validate = false;
		}
		return validate;
	}

	private Double formatDouble(Double inputValue) {
		Double formattedValue = null;
		try {
			inputValue = inputValue == null ? 0D : inputValue;
			if (inputValue > 0D) {
				DecimalFormat decimalFormat = new DecimalFormat("#0.00");
				formattedValue = decimalFormat.parse(decimalFormat.format(inputValue)).doubleValue();
			} else {
				formattedValue = inputValue;
			}
		} catch (Exception ex) {
			formattedValue = inputValue;
			log.error("Exception at formatDouble", ex);
		}
		return formattedValue;
	}

	public void discountRateCalculate() {
		Double discountPercent = 0D;
		if (retailPrice != null) {
			/*
			 * if(orderType.equalsIgnoreCase("freeSample")) { supplierRate=0D; }
			 */
			retailPrice = formatDouble(retailPrice);
			supplierRate = formatDouble(supplierRate);
			// supplierRate = supplierRate = supplierRate == null ? 0 : supplierRate;
			discountValue = (retailPrice - supplierRate);
			discountValue = discountValue > 0 ? discountValue : 0;
		}

		/*
		 * if(supplierRate > retailPrice) {
		 * log.info("supplierRate Is Not Greater than the retailrate... " );
		 * AppUtil.addWarn("Supplier Rate should not be greater than Retail Price"); }
		 */
		Double discountValueUpd = 0D;
		if (discountValue != null) {
			// discountPercent = Math.ceil(discountValue * 100) / retailPrice;
			discountPercent = (discountValue * 100) / retailPrice;
			discountPercent = truncateDecimal(discountPercent, 2);
			discountValueUpd = retailPrice * (discountPercent / 100);
//			discountValueUpd = truncateDecimal(discountValueUpd,2);
			discountPercent = discountPercent > 0 ? discountPercent : 0;
			purchasePrice = purchasePrice == null ? 0 : purchasePrice;
			// profitMargin = Math.ceil((supplierRate - purchasePrice) / purchasePrice);
			profitMargin = ((supplierRate - purchasePrice) / purchasePrice);
			profitMargin = profitMargin > 0D ? profitMargin : 0D;
			if (profitMargin.isInfinite()) {
				profitMargin = null;
			}
			log.info("profitMargin " + profitMargin);
			log.info("discountPercent... " + discountPercent);
			// -------------------------------------------------------
			log.info("discountValue: " + discountValue);
			log.info("discountValueUpd: " + discountValueUpd);
			// -------------------------------------------------------
			salesOrderItems.setDiscountPercent(discountPercent);
			discountValue = (double) (Math.ceil(discountValueUpd * 100) / 100);

//			salesOrderItems.setDiscountValue(discountValue);
		}
	}

	private static Double truncateDecimal(Double value, int numberofDecimals) {
		BigDecimal truncateValue = new BigDecimal(String.valueOf(value)).setScale(numberofDecimals,
				BigDecimal.ROUND_FLOOR);
		return truncateValue.doubleValue();
	}

	public void addProduct() {
		try {

			/*
			 * if(supplierRate > retailPrice) {
			 * log.info("supplierRate Is Not Greater than the retailrate... " );
			 * AppUtil.addWarn("SupplierRate is Not Greater than RetailRate"); return ; }
			 */
			// salesOrderItems = new SalesOrderItems();
			if (!orderType.equalsIgnoreCase("freeSample") && supplierRate == 0D) {
				AppUtil.addWarn("SupplierRate Should Be Greater than Zero");
				return;
			}

			if (salesOrder.getSalesOrderItemsList() == null) {
				salesOrder.setSalesOrderItemsList(new ArrayList<>());
			}

			if (salesOrderItems == null || salesOrderItems.getItemQty() == null) {
				log.info("supplierRate Is Not Greater than the retailrate... ");
				AppUtil.addWarn("Pleae Enter Quantity");
				return;
			}

			if (salesOrderItems != null && salesOrderItems.getProductVarietyMaster() != null
					&& salesOrderItems.getItemQty() != null) {
				salesOrderItems.setUnitRate(retailPrice);
				// salesOrderItems.setItemAmount(supplierRate);
				if (orderType.equalsIgnoreCase("freeSample")) {
					salesOrderItems.setItemAmount(0D);
				} else {
					salesOrderItems.setItemAmount(supplierRate);
				}
				try {

					getProductDetails();

					// Set Supplier Value
					log.info(" ---------------salesOrderItems.getItemQty()------------- : "
							+ salesOrderItems.getItemQty());
					double itemQty = salesOrderItems.getItemQty() == null ? 0D : salesOrderItems.getItemQty();
					double roundedSupplierValue = (itemQty * salesOrderItems.getItemAmount());
					salesOrderItems.setSupplierValue(roundedSupplierValue);

					Double taxValue = 0.0;
					if (orderType.equalsIgnoreCase("freeSample")) {
						taxValue = (((salesOrderItems.getUnitRate() * salesOrderItems.getTaxPercent()) / 100)
								* salesOrderItems.getItemQty());
						taxValue = taxValue != null ? Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue)) : 0D;
						salesOrderItems.setTaxValue(roundOffTaxValue(taxValue));
					} else {

//						taxValue = (((salesOrderItems.getItemAmount() * salesOrderItems.getTaxPercent()) / 100)
//								* salesOrderItems.getItemQty());
						taxValue = salesOrderItems.getTaxValue();
						taxValue = taxValue != null ? Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue)) : 0D;
						salesOrderItems.setTaxValue(roundOffTaxValue(taxValue));
					}

					if (!checkProductExist()) {
						List<SalesOrderItems> itemList = new ArrayList<>();
						itemList.add(salesOrderItems);
						salesOrder.getSalesOrderItemsList().addAll(itemList);
					}

					Double supplierValue = (salesOrderItems.getItemTotal() - salesOrderItems.getDiscountValue());
					supplierValueList.add(supplierValue);
					calculateTax(salesOrder.getSalesOrderItemsList());

					log.info(
							"============================= addproduct Method Complete ===============================");

				} catch (Exception e) {
					log.info("Exception while addProduct....", e);
				}
			}
		} catch (Exception e) {
			log.info("Exception while addProduct...." + e);
		}
		// productCategory = null;
		// productGroupMaster = null;
		// salesOrderItems.setProductVarietyMaster(null);
		retailPrice = null;
		setRetailPrice(null);
		purchasePrice = null;
		supplierRate = null;
		setSupplierRate(null);
		// salesOrderItems.setItemQty(null);
		profitMargin = null;
		discountValue = null;
		salesOrderItems = new SalesOrderItems();
		if (billingAddress == null) {
			billingAddress = new AddressMaster();
			billingAddress.setStateMaster(new StateMaster());
		}
		changeAddress = new AddressMaster();
		changeAddress.setStateMaster(new StateMaster());
		clearSalesOrderItems();

	}

	public boolean checkProductExist() {
		for (SalesOrderItems item : salesOrder.getSalesOrderItemsList()) {
			if (item.getProductVarietyMaster().getId().equals(salesOrderItems.getProductVarietyMaster().getId())
					&& item.getDiscountPercent().equals(salesOrderItems.getDiscountPercent())
					&& item.getUnitRate().equals(salesOrderItems.getUnitRate())) {
				item.setItemQty(item.getItemQty() + salesOrderItems.getItemQty());
				item.setDiscountValue(item.getDiscountValue() + salesOrderItems.getDiscountValue());
				item.setTaxPercent(item.getTaxPercent() + salesOrderItems.getTaxPercent());
				item.setTaxValue(item.getTaxValue() + salesOrderItems.getTaxValue());
				item.setItemTotal(item.getItemTotal() + salesOrderItems.getItemTotal());
				item.setItemAmount(item.getItemAmount() + salesOrderItems.getItemAmount());
				return true;
			}
		}
		return false;
	}

	@Setter
	@Getter
	SalesOrderItems deleteProductVarietyDetail = null;

	public String godeleteRecord(SalesOrderItems salesOrderItems) {
		deleteProductVarietyDetail = null;
		try {
			deleteProductVarietyDetail = salesOrderItems;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmSalesorderDelete').show();");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public void removeProduct() {
		try {
			// Long
			// productId=salesOrderItems.getProductVarietyMaster()!=null?salesOrderItems.getProductVarietyMaster().getId():null;
			Long productId = deleteProductVarietyDetail.getProductVarietyMaster() != null
					? deleteProductVarietyDetail.getProductVarietyMaster().getId()
					: null;
			salesOrder.getSalesOrderItemsList().remove(deleteProductVarietyDetail);
			salesOrderAddressList.removeIf(
					(SalesOrderAddress soa) -> productId.toString().equals(soa.getProductVarietyId().toString()));
			if (salesOrder.getSalesOrderItemsList() != null && !salesOrder.getSalesOrderItemsList().isEmpty()) {
				calculateTax(salesOrder.getSalesOrderItemsList());
			} else {
				gstWiseList = new ArrayList<GstSummeryDTO>();
				gstPercentageWiseDTOList = new ArrayList<HsnCodeTaxPercentageWiseDTO>();
				materialValue = 0.0;
				totalCgstAmount = 0.0;
				totalSgstAmount = 0.0;
				totalTaxAmount = 0.0;
				totalDiscountAmount = 0.0;
				amountInWords = "";
				itemQuantity = 0.0;
				salesOrderAddress.setQtyToDeliver(null);
				changeAddressArray = null;
			}
		} catch (Exception ex) {
			log.info("Exception--", ex);
		}
	}

	public void getProductDetails() {
		log.info("Inside getProductDetails() ");
		String url = serverURL + appPreference.getOperationApiUrl() + "/salesorder/getproductdetails";
		BaseDTO baseDTO = null;
		try {
			salesOrderItems.setDiscountValue(discountValue);
			salesOrderItems.setSalesOrderType(orderType);
			baseDTO = httpService.post(url, salesOrderItems);
			if (baseDTO != null) {
				log.info(" ---------------salesOrderItems.getItemQty()------------- : " + salesOrderItems.getItemQty());
				String errorMessage = baseDTO.getMessage();
				if (StringUtils.isNotEmpty(errorMessage)) {
					AppUtil.addWarn(errorMessage);
					return;
				}

				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				salesOrderItems = mapper.readValue(jsonResponse, SalesOrderItems.class);
				log.info(" ---------------salesOrderItems.getItemQty()------------- : " + salesOrderItems.getItemQty());
				if (salesOrderItems != null) {
					log.info(" ---------------salesOrderItems.getItemQty()------------- :  ");
					double taxV = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(salesOrderItems.getTaxValue()));
					double taxValue = roundOffTaxValue(taxV);
					salesOrderItems.setTaxValue(taxValue);
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While getProductDetails() :: ", e);
		}
	}

	private double roundOffTaxValue(double taxV) {
		Double taxRoundOff = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxV));
		log.info("tax round off value----" + taxRoundOff);
		String str = String.valueOf(taxRoundOff);
		String[] convert = str.split("\\.");
		int length = convert[1].length();
		double taxValue = 0.0;
		if (length == 2) {
			int splitValue = Integer.valueOf(convert[1].substring(1));
			log.info("split up value----------" + splitValue);
			if (splitValue % 2 == 0) {
				log.info("splitValue is even-----");
				taxValue = taxRoundOff;
			} else {
				log.info("splitValue is odd-----");
				taxValue = taxRoundOff + 0.01;
				taxValue = Double.valueOf(AppUtil.DECIMAL_FORMAT.format(taxValue));
			}
		} else {
			taxValue = taxRoundOff;
		}
		return taxValue;
	}

	// Achyu code ends here

	public String getSalesOrderLazyList() {
		log.info("<--- Inside getSalesOrderList --->");
		loadStatusValues();
		loadLazySalesOrderList();
		billingAddressString = "";
		shippingAddressString = "";
		return SALES_ORDER_LIST;
	}

	public void loadLazySalesOrderList() {
		log.info("<--- loadLazySalesOrderList ---> ");
		salesOrderList = new LazyDataModel<SalesOrder>() {

			private static final long serialVersionUID = -525109335592548450L;

			@Override
			public List<SalesOrder> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<SalesOrder> data = new ArrayList<SalesOrder>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					data = mapper.readValue(jsonResponse, new TypeReference<List<SalesOrder>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("<--- Sales Order List Count --->  " + baseDTO.getTotalRecords());
						size = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error in loadLazySalesOrderList()  ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(SalesOrder res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SalesOrder getRowData(String rowKey) {
				List<SalesOrder> responseList = (List<SalesOrder>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (SalesOrder res : responseList) {
					if (res.getId().equals(value)) {
						salesOrder = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("SalesOrderBean.onRowSelect method started");
		selectedSalesOrder = (SalesOrder) event.getObject();
		addButtonFlag = true;
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		salesOrder = new SalesOrder();

		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		SalesOrder salesOrder = new SalesOrder();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		salesOrder.setPaginationDTO(paginationDTO);

		log.info("Sales Order Request  " + salesOrder);
		salesOrder.setFilters(filters);

		try {
			String salesOrderSearchURL = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/salesorder/search";
			log.info("Sales Order Search URL " + salesOrderSearchURL);
			baseDTO = httpService.post(salesOrderSearchURL, salesOrder);
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("Exception in getSearchData() ", e);
		}

		return baseDTO;
	}

	public void onPageLoad() {
		log.info("Sales Order onPageLoad method started");
		addButtonFlag = false;
	}

	@SuppressWarnings("unchecked")
	public String getViewPage() {
		log.info("SalesOrderBean. getViewPage()  Starts");
		try {
			customerAddCountryName = null;
			if (salesOrder == null) {
				log.info("Please Select Any One");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			String salesOrderViewPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/salesorder/getById/" + salesOrder.getId();
			log.info(" salesOrderViewPath " + salesOrderViewPath);

			BaseDTO baseDTO = httpService.get(salesOrderViewPath);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {

				ObjectMapper objectMapper = new ObjectMapper();
				String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
				salesOrder = objectMapper.readValue(jsonResponse, SalesOrder.class);
				if (salesOrder != null) {
					salesOrderAddressList = new ArrayList<SalesOrderAddress>();
					if (!salesOrder.getSalesOrderItemsList().isEmpty()) {
						Iterator<SalesOrderItems> salesOrderAddressListItr = salesOrder.getSalesOrderItemsList()
								.iterator();
						while (salesOrderAddressListItr.hasNext()) {
							SalesOrderItems salesOrderItems = salesOrderAddressListItr.next();
							for (SalesOrderAddress object : salesOrder.getSalesOrderAddressList()) {
								if (object.getSalesOrderItems().getId().equals(salesOrderItems.getId())) {
									object.setProductVarietyCodeAndName(
											salesOrderItems.getProductVarietyMaster().getCode() + " / "
													+ salesOrderItems.getProductVarietyMaster().getName());
									if (salesOrderItems.getProductVarietyMaster() != null) {
										object.setProductVarietyId(salesOrderItems.getProductVarietyMaster().getId());
										object.setUnitRate(salesOrderItems.getUnitRate());
									}
									salesOrderAddressList.add(object);
								}
							}
						}
					}
					orderType = null;
					if (salesOrder.getQuotation() != null && salesOrder.getQuotation().getId() != null)
						orderType = "withQuotation";
					salesOrderLastNote = salesOrder.getSalesOrderNoteList()
							.get(salesOrder.getSalesOrderNoteList().size() - 1);

					if (salesOrderLastNote.getUserMaster().getId().longValue() == loginBean.getUserMaster().getId()
							.longValue()) {
						enableFlag = true;
					}

					footerTotal = salesOrder.getSalesOrderItemsList().stream()
							.mapToDouble(SalesOrderItems::getItemTotal).sum();

					if (salesOrder.getBillingAddress() != null) {
						billingAddress = salesOrder.getBillingAddress();
						billingAddressDetails = prepareBillingAndShippingAddress(salesOrder.getBillingAddress());
					}

					CustomerMaster customer = salesOrder.getCustomerMaster();
					if (customer != null) {
						AddressMaster address = customer.getAddressMaster();
						if (address != null) {
							CountryMaster country = address.getCountryMaster();
							if (country != null) {
								customerAddCountryName = country.getName();
							} else {
								customerAddCountryName = null;
								log.info("Country not found.");
							}
						} else {
							customerAddCountryName = null;
							log.info("Customer Address not found.");
						}
					} else {
						customerAddCountryName = null;
						log.info("Customer not found.");
					}

					List<SalesOrderItems> itemsList = salesOrder.getSalesOrderItemsList();
					if (itemsList != null) {
						calculateTax(itemsList);
					}
					if (StringUtils.isEmpty(salesOrder.getPoRefNumber())) {
						salesOrder.setPoRefNumber("-");
					}

				}

				/*
				 * shippingAddressDetails = prepareBillingAndShippingAddress(
				 * salesOrder.getCustomerMaster().getShippingAddressMaster());
				 */
				Object[] approveRejectCommentData = baseDTO.getCommentAndLogList();
				setApproveCommentsDTOList((List<ApproveRejectCommentDTO>) approveRejectCommentData[0]);
				setRejectCommentsDTOList((List<ApproveRejectCommentDTO>) approveRejectCommentData[1]);
				setEmployeeData((List<Map<String, Object>>) approveRejectCommentData[2]);

			} else {
				log.error("Service Not Avalible");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			}
		} catch (Exception e) {
			log.error("Exception at SalesOrderBean. getViewPage() ", e);
		}
		log.info("SalesOrderBean. getViewPage()  Ends");
		return SALES_ORDER_VIEW;
	}

	public void changeCurrentStatus(String value) {
		log.info("<--- changeCurrentStatus() ---> " + value);
		currentPlanStatus = value;
		comments = null;
	}

	public String statusUpdate() {
		log.info("<--- Inside statusUpdate() ---> ");
		log.info("<--- Current Sales Order Status ---> " + currentPlanStatus);
		log.info("<--- Sales ORder Id ---> " + salesOrder.getId());
		log.info("<--- comments ---> " + comments);

		BaseDTO baseDTO = null;

		String updateStatusPath = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/salesorder/updatestatus";
		log.info("<--- updateStatusPath ---> " + updateStatusPath);

		SalesOrder request = new SalesOrder(salesOrder.getId(), currentPlanStatus, comments);
		if (currentPlanStatus != null && "APPROVED".equalsIgnoreCase(currentPlanStatus)) {
			request.setForwardToId(salesOrderNote.getUserMaster().getId());
			request.setIsFinalApprove(salesOrderNote.getFinalApproval());
			request.setNote(salesOrderNote.getNote());
		}
		baseDTO = httpService.post(updateStatusPath, request);

		if (baseDTO != null) {
			errorMap.notify(baseDTO.getStatusCode());
		}

		comments = null;

		return SALES_ORDER_LIST;
	}

	public void submitNote(String note) {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + salesOrderNote.getNote());
		log.info("=========================================");
	}

	public String prepareBillingAndShippingAddress(AddressMaster addressMaster) {
		String addressArray[] = AppUtil.prepareAddress(addressMaster);
		String addressDetails = "";

		if (languageBean.getLocaleCode().equals("en_IN")) {
			addressDetails = addressArray[0];
		} else {
			addressDetails = addressArray[1];
		}

		return addressDetails;
	}

	public String cancel() {
		log.info("<--- Inside cancel() ---> ");

		getSalesOrderLazyList();

		return SALES_ORDER_LIST;
	}

	private void loadStatusValues() {
		Object[] statusArray = OrderStatus.class.getEnumConstants();

		statusValues = new ArrayList<>();

		for (Object status : statusArray) {
			statusValues.add(status.toString().replace("_", "-"));
		}
		statusValues.remove("PENDING");
		statusValues.remove("PARTIALLY-PENDING");
		statusValues.remove("COMPLETED");
		log.info("<--- statusValues ---> " + statusValues);
	}

	public void sampleData() {
		log.info("salesOrderItems#############" + salesOrderItems);
	}

	public void changeProductVarietyList() {
		log.info("========START SalesOrderBean.changeProductVarietyList=======");
		// salesOrderAddress.setOrderQty(selectedSalesOrderItems.getItemQty());
		salesOrderAddress.setQtyToDeliver(null);
		Long productId = selectedSalesOrderItems.getProductVarietyMaster().getId();
		salesOrderAddress.setProductVarietyId(productId);
		List<SalesOrderItems> itemList = salesOrder.getSalesOrderItemsList();
		if (productId != null) {
			unitRateList = itemList.stream()
					.filter(o -> o.getProductVarietyMaster().getId() != null
							&& o.getProductVarietyMaster().getId() == productId)
					.map(o -> o.getUnitRate()).collect(Collectors.toList());
		}
		salesOrderAddress.setSalesOrderItems(selectedSalesOrderItems);
		itemQuantity = selectedSalesOrderItems.getItemQty();
		if (salesOrderAddressList != null && !salesOrderAddressList.isEmpty()) {
			Double deliverQty = salesOrderAddressList.stream().filter(dq -> dq.getProductVarietyId() != null)
					.filter(dq -> dq.getProductVarietyId().toString()
							.equals(selectedSalesOrderItems.getProductVarietyMaster().getId().toString()))
					.filter(dq -> dq.getSalesOrderItems().getUnitRate() != null)
					.filter(dq -> dq.getSalesOrderItems().getUnitRate().equals(selectedSalesOrderItems.getUnitRate()))
					.filter(dq -> dq.getSalesOrderItems().getItemAmount() != null)
					.filter(dq -> dq.getSalesOrderItems().getItemAmount()
							.equals(selectedSalesOrderItems.getItemAmount()))
					.collect(Collectors.summingDouble(dq -> dq.getQtyToDeliver()));

			itemQuantity = selectedSalesOrderItems.getItemQty() - deliverQty;
		}
		/*
		 * if(selectedSalesOrderItems.getProductVarietyMaster()!=null) {
		 * salesOrderAddress.setProductVarietyCodeAndName(selectedSalesOrderItems.
		 * getProductVarietyMaster().getCode()+" / "+selectedSalesOrderItems.
		 * getProductVarietyMaster().getName()); }
		 */
		log.info("========End SalesOrderBean.changeProductVarietyList=======");
	}

	@Setter
	@Getter
	SalesOrderAddress deleteProductAddress = null;

	public String godeleteProductAddress(SalesOrderAddress salesOrderAddress) {
		deleteProductAddress = null;
		try {
			deleteProductAddress = salesOrderAddress;
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmproductDelete').show();");
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public void removeAddressProduct() {
		salesOrderAddressList.remove(deleteProductAddress);

	}

	public void addAddress() {
		try {
			if (itemQuantity == null || itemQuantity <= 0) {
				AppUtil.addWarn("Please add quantity for this Product");
				return;
			}
			salesOrderAddress.setOrderQty(itemQuantity);
			if (selectedSalesOrderItems.getProductVarietyMaster() != null) {
				salesOrderAddress
						.setProductVarietyCodeAndName(selectedSalesOrderItems.getProductVarietyMaster().getCode()
								+ " / " + selectedSalesOrderItems.getProductVarietyMaster().getName());
				salesOrderAddress.setUnitRate(selectedSalesOrderItems.getUnitRate());
				// salesOrderAddress.setProductVarietyId(selectedSalesOrderItems.getProductVarietyMaster().getId());
			}

			if (changeAddressArray == null || changeAddressArray.length == 0) {
				Util.addWarn("Delivery Address is Required");
				return;
			}

			if (selectedSalesOrderItems.getProductVarietyMaster().getId() != null) {
				String addressArray = "ta_IN".equals(languageBean.getLocaleCode()) ? changeAddressArray[1]
						: changeAddressArray[0];

				boolean isExists = salesOrderAddressList.stream()
						.anyMatch(i -> (i.getProductVarietyCodeAndName().split(" / ")[0])
								.equals(selectedSalesOrderItems.getProductVarietyMaster().getCode())
								&& (i.getDelievryAddress().equals(addressArray)) && (selectedSalesOrderItems
										.getUnitRate().doubleValue() == (i.getUnitRate().doubleValue())));

				if (isExists) {
					AppUtil.addWarn("This Delivery Address added already");
					RequestContext.getCurrentInstance().update("growls");
					return;
				}
			}

			salesOrderAddress.setDelievryAddress(changeAddressArray[0]);
			log.info("========START SalesOrderBean.addAddress=======");
			if ((salesOrderAddress.getQtyToDeliver() != null ? salesOrderAddress.getQtyToDeliver()
					: 0.0) > (itemQuantity != null ? itemQuantity : 0.0)) {
				Util.addWarn("Delivery Quantity Value must be less than or equal to Ordered Quantity");
				return;
			}
			if (salesOrderAddress.getQtyToDeliver() == null) {
				AppUtil.addWarn("Enter Delivery Quantity");
				return;
			}
			log.info("order qty--------------" + salesOrderAddress.getOrderQty());
			log.info("deliver qty-----------" + salesOrderAddress.getQtyToDeliver());
			log.info("product varity code ----" + salesOrderAddress.getProductVarietyCodeAndName());
			salesOrderAddressList.add(salesOrderAddress);
			/*
			 * salesOrderAddress.setOrderQty(null); salesOrderAddress.setQtyToDeliver(null);
			 * salesOrderAddress.setProductVarietyCodeAndName(null);
			 */
			selectedSalesOrderItems = new SalesOrderItems();
			salesOrderAddress = new SalesOrderAddress();
			itemQuantity = null;
			// / changeAddressArray=null;
		} catch (Exception e) {
			log.error("add method exception -----", e);
		}
		log.info("========END SalesOrderBean.addAddress=======");
	}

	public void saveChangeBillingAddress() {
		log.info(" Inside saveBillingAddress " + changeAddress);
		if (changeAddress == null) {
			changeAddress = new AddressMaster();
		}
		changeAddressArray = AppUtil.prepareAddress(changeAddress);
		if (changeAddressArray != null) {
			String addressOne = changeAddressArray[0];
			String addressTwo = changeAddressArray[1];
			changeAddressValue = addressOne + addressTwo;
			salesOrderAddress.setDelievryAddress(changeAddressValue);
		}
		changeAddress = new AddressMaster();
		changeAddress.setStateMaster(new StateMaster());
		changeAddress.setDistrictMaster(new DistrictMaster());
		changeAddress.setTalukMaster(new TalukMaster());
		changeAddress.setCityMaster(new CityMaster());
		changeAddress.setAreaMaster(new AreaMaster());
		log.info("changeAddressArray is" + changeAddressArray);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('shippingaddrdialog').hide();");
	}

	public void checkDeliverQuantity(SalesOrderAddress salesOrderAddress) {
		log.info("========START SalesOrderBean.checkDeliverQuantity======");
		Double editDeliverQuantity = salesOrderAddressList.stream()
				.filter(i -> i.getProductVarietyCodeAndName().equals(salesOrderAddress.getProductVarietyCodeAndName()))
				.mapToDouble(SalesOrderAddress::getQtyToDeliver).sum();
		Double totalOrderQuantity = salesOrderAddressList.stream()
				.filter(i -> i.getProductVarietyCodeAndName().equals(salesOrderAddress.getProductVarietyCodeAndName()))
				.mapToDouble(SalesOrderAddress::getOrderQty).sum();
		if (totalOrderQuantity < editDeliverQuantity) {
			deliveryQuantityCheck = true;
		} else {
			deliveryQuantityCheck = false;
		}
		log.info("========END SalesOrderBean.checkDeliverQuantity======");
	}

	public void checkShippingAddress(SalesOrderAddress salesOrderAddress) {
		log.info("========START SalesOrderBean.checkShippingAddress======");
		Iterator<SalesOrderAddress> salesOrderAddressItr = salesOrderAddressList.iterator();
		while (salesOrderAddressItr.hasNext()) {
			SalesOrderAddress salesOrderObject = salesOrderAddressItr.next();
			if (salesOrderObject.getProductVarietyCodeAndName().equals(salesOrderAddress.getProductVarietyCodeAndName())
					&& salesOrderAddress.getDelievryAddress().equals(salesOrderObject.getDelievryAddress())) {
				shippingAddressCheck = true;
			} else {
				shippingAddressCheck = false;
			}
		}
		log.info("========END SalesOrderBean.checkShippingAddress======");
	}

	public CustomerMaster getCustomerDetails(Long custId) {
		log.info("=============>Start getCustomerBillingAndShippingAddress Method==========>" + custId);
		CustomerMaster customerMaster = new CustomerMaster();
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/customerMaster/customerDetails/" + custId;
			baseDTO = httpService.get(url);

			if (baseDTO != null) {

				ObjectMapper mapper = new ObjectMapper();

				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

				customerMaster = mapper.readValue(jsonResponse, new TypeReference<CustomerMaster>() {
				});

			}

		} catch (Exception ex) {
			log.error("Error in getCustomerBillingAndShippingAddress", ex);
		}
		log.info("=============>End getCustomerBillingAndShippingAddress Method==========>");
		return customerMaster;
	}

	public List<CustomerMaster> loadCustomerByName(String query) {
		log.info("Loading all customer ............");

		// List<CustomerMaster> customerMasterList = null;

		try {

			if (customerTypeMaster != null && customerTypeMaster.getId() != null) {
				/*
				 * CustomerMasterController.java - Method: getAllCustomersByType()
				 */
				String url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
						+ "/customerMaster/customerlistbynameorcodeandtype/" + query + "/" + customerTypeMaster.getId();

				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {

					ObjectMapper mapper = new ObjectMapper();

					mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());

					customerMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerMaster>>() {
					});

				}
			} else {
				AppUtil.addWarn("Customer type is required");
			}

		} catch (Exception e) {

			log.error("Exception occured .... ", e);

		}

		return customerMasterList;
	}

	public void getAllDeliveryDistrictByState() {
		log.info(" Inside  getAllDeliveryDistrictByState() ");

		billingDistrictList = new ArrayList<>();

		if (changeAddress != null && changeAddress.getStateMaster() != null
				&& changeAddress.getStateMaster().getId() != null) {
			Long stateId = changeAddress.getStateMaster().getId();
			log.info("stateId : " + stateId);

			billingDistrictList = commonDataService.loadActiveDistrictsByState(stateId);
		} else {

			log.info(" getAllDeliveryDistrictByState() addressMaster.getStateMaster().getId() ==null");

			billingDistrictList = new ArrayList<>();
			billingTalukList = new ArrayList<TalukMaster>();
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setDistrictMaster(new DistrictMaster());
			billingAddress.setTalukMaster(new TalukMaster());
			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllDeliveryTalukByDistrict() {
		log.info("Inside getAllDeliveryTalukByDistrict ");

		billingTalukList = new ArrayList<>();

		if (changeAddress != null && changeAddress.getDistrictMaster() != null
				&& changeAddress.getDistrictMaster().getId() != null) {
			Long districtId = changeAddress.getDistrictMaster().getId();
			log.info("districtId : " + districtId);

			billingTalukList = commonDataService.loadActiveTaluksByDistrict(districtId);
		} else {

			log.info(" getAllDeliveryTalukByDistrict() addressMaster.getDistrictMaster().getId() ==null");

			billingTalukList = new ArrayList<>();
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setTalukMaster(new TalukMaster());
			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllDeliveryCityByTaluk() {
		log.info(" Inside  " + changeAddress);
		billingCityList = new ArrayList<CityMaster>();

		if (changeAddress != null && changeAddress.getTalukMaster() != null
				&& changeAddress.getTalukMaster().getId() != null) {
			Long districtId = changeAddress.getDistrictMaster().getId();
			Long talukId = changeAddress.getTalukMaster().getId();
			log.info("districtId : " + districtId);
			log.info("talukId : " + talukId);

			billingCityList = commonDataService.loadActiveCitiesByDistrict(districtId);

		} else {
			billingCityList = new ArrayList<CityMaster>();
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setCityMaster(new CityMaster());
			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public void getAllDeliveryAreaListByTaluk() {
		log.info("<< == Auto Complete method to getAllBillingAreaListByTaluk == >> " + changeAddress);
		billingAreaList = new ArrayList<AreaMaster>();

		if (changeAddress != null && changeAddress.getCityMaster() != null
				&& changeAddress.getCityMaster().getId() != null) {
			Long cityId = changeAddress.getCityMaster().getId();
			log.info("cityId : " + cityId);

			billingAreaList = commonDataService.loadActiveAreasByCity(cityId);
		} else {
			billingAreaList = new ArrayList<AreaMaster>();

			billingAddress.setAreaMaster(new AreaMaster());
		}

	}

	public String salesOrderCancel() {
		log.info("SalesOrderBean. salesOrderCancel() Starts ");
		salesOrder = null;
		return SALES_ORDER_LIST;
	}

	public String getCreatePage() {
		log.info("SalesOrderBean. getCreatePage() Starts ");
		try {
			action = "CREATE";
			salesOrder = null;
			customerAddCountryName = null;
			salesOrderItems = new SalesOrderItems();
			loadBasicData();
			orderType = null;
			loadEmployeeLoggedInUserDetails();
			disableFlag = false;
		} catch (Exception e) {
			log.error("Exception at SalesOrderBean. getCreatePage() ", e);
			;
		}
		log.info("SalesOrderBean. getCreatePage() Ends ");
		return SALES_ORDER_CREATE;
	}

	private void loadBasicData() {
		log.info("SalesOrderBean. loadBasicData() Starts ");
		try {
			if (salesOrder == null) {
				salesOrder = new SalesOrder();
			}
			customerTypeMaster = new CustomerTypeMaster();
			customerTypeMasterList = commonDataService.getAllCustomerTypes();
			productCategoryList = commonDataService.loadProductCategories();
			salesOrderItems = new SalesOrderItems();
			productGroupMaster = new ProductGroupMaster();
			productCategory = new ProductCategory();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.SALES_ORDER.toString());
			gstPercentageWiseDTOList = new ArrayList<>();
			gstWiseList = new ArrayList<>();
			totalDiscountAmount = 0D;
			billingAddress = new AddressMaster();
			billingAddress.setStateMaster(new StateMaster());
			changeAddress = new AddressMaster();
			changeAddress.setStateMaster(new StateMaster());
			shippingAddress = new AddressMaster();
			salesOrderNote = new SalesOrderNote();
			billingAddressArray = null;
			shippingAddressArray = null;
			countryMasterList = commonDataService.loadCountry();
			// stateList = commonDataService.loadStateList();
			amountInWords = null;
			supplierValueList = new ArrayList<>();
			salesOrderAddressList = new ArrayList<SalesOrderAddress>();
			selectedSalesOrderItems = new SalesOrderItems();
			salesOrderAddress = new SalesOrderAddress();
			changeAddressValue = new String();
			materialValue = 0D;
			changeAddressArray = null;
		} catch (Exception e) {
			log.error("Exception at SalesOrderBean. loadBasicData() ", e);
			;
		}
		log.info("SalesOrderBean. loadBasicData() Ends ");
	}

	private void calculateTotalValues(List<SalesOrderItems> itemsList) {
		log.info("SalesOrderBean. calculateTotalValues() Starts ");
		double totalAmount = 0D;
		try {
			amountInWords = null;
			materialValue = TaxCalculationUtil.calculateSalesOrderMaterialValue(itemsList);
			totalCgstAmount = gstPercentageWiseDTOList.stream().filter(o -> o.getCgstAmount() != null)
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			totalSgstAmount = gstPercentageWiseDTOList.stream().filter(o -> o.getSgstAmount() != null)
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalTaxAmount = gstPercentageWiseDTOList.stream().filter(o -> o.getTotalTax() != null)
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
			totalDiscountAmount = itemsList.stream().filter(o -> o.getItemAmount() != null)
					.collect(Collectors.summingDouble(SalesOrderItems::getDiscountValue));

			if (countryName.equals(customerAddCountryName)) {
				totalAmount = (AppUtil.ifNullRetunZero(materialValue) - AppUtil.ifNullRetunZero(totalDiscountAmount))
						+ AppUtil.ifNullRetunZero(totalTaxAmount);
			} else {
				totalAmount = (AppUtil.ifNullRetunZero(materialValue) - AppUtil.ifNullRetunZero(totalDiscountAmount));
			}

			// totalAmount = (AppUtil.ifNullRetunZero(materialValue) -
			// AppUtil.ifNullRetunZero(totalDiscountAmount)) +
			// AppUtil.ifNullRetunZero(totalTaxAmount);
			amountInWords = AmountToWordConverter.converter(String.valueOf(AppUtil.DECIMAL_FORMAT.format(totalAmount)));
		} catch (Exception e) {
			log.error("Exception at SalesOrderBean. calculateTotalValues()", e);
		}
		log.info("SalesOrderBean. calculateTotalValues() Ends ");
	}

	private void loadEmployeeLoggedInUserDetails() {
		log.info("SalesOrderBean. loadEmployeeLoggedInUserDetails() Starts ");
		Long userId = null;
		try {
			userId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuserdetails/" + userId;

			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + userId);
				return;
			}
		} catch (Exception e) {
			log.error("Exception at SalesOrderBean. loadEmployeeLoggedInUserDetails()", e);
		}
		log.info("SalesOrderBean. loadEmployeeLoggedInUserDetails() Ends ");
	}

	public void getBillingAddressState() {
		log.info("--Start getBillingAddressState method--");
		try {
			if (billingAddress.getCountryMaster() != null) {
				log.info("country id: " + billingAddress.getCountryMaster().getId());
				stateList = commonDataService.loadActiveStates(billingAddress.getCountryMaster().getId());
				log.info("state list size::" + stateList != null ? stateList.size() : "null");
			}
		} catch (Exception ex) {
			log.info("---Exception--", ex);
		}
		log.info("--End getBillingAddressState method--");
	}

	public void getDeliverAddressStateByCountryId() {
		log.info("--Start getBillingAddressState method--");
		try {
			if (changeAddress.getCountryMaster() != null) {
				log.info("country id: " + changeAddress.getCountryMaster().getId());
				stateList = commonDataService.loadActiveStates(changeAddress.getCountryMaster().getId());
				log.info("state list size::" + stateList != null ? stateList.size() : "null");
			}
		} catch (Exception ex) {
			log.info("---Exception--", ex);
		}
		log.info("--End getBillingAddressState method--");
	}

	public void clearDeliveryDetails() {
		selectedSalesOrderItems = new SalesOrderItems();
		salesOrderAddress.setQtyToDeliver(null);
		changeAddressArray = null;
		itemQuantity = 0D;
	}

}