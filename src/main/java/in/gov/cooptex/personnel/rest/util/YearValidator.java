package in.gov.cooptex.personnel.rest.util;

import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@FacesValidator("yearValidator")
public class YearValidator implements Validator{
	
	@Getter @Setter
	String action;
	

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		Calendar c = Calendar.getInstance();
		int year = c.get(Calendar.YEAR);
		log.info("[ Currunt Year Value :]"+year);
		log.info("[ Request Value :]"+value.toString());
		
		int reqYear = Integer.valueOf(value.toString());
		if(reqYear > year || value.toString().trim().length()<4) {
			FacesMessage message =	new FacesMessage();
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			message.setSummary("Year is not valid.");
			message.setDetail("Year is not valid.");
			context.addMessage("educationForm:educationPanel", message);
			throw new ValidatorException(message);
			/*if(action.equals("ADD")) {
				action = "ADD";
			}else {
				action = "EDIT";
			}*/
		}
	}

}
