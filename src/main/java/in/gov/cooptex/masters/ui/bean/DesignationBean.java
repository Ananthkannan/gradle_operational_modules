package in.gov.cooptex.masters.ui.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.CadreMaster;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("designationBean")
@Scope("session")
public class DesignationBean {/**
	 * 
	 */
		private static final long serialVersionUID = 1L;
		private final String DESIGNATION_LIST_PAGE = "/pages/masters/designation/listDesignation.xhtml?faces-redirect=true;";
		private final String DESIGNATION_ADD_PAGE = "/pages/masters/designation/addDesignation.xhtml?faces-redirect=true;";
		private final String DESIGNATION_VIEW_PAGE = "/pages/masters/designation/viewDesignation.xhtml?faces-redirect=true;";

		public static final String SERVER_URL = AppUtil.getPortalServerURL();

		ObjectMapper mapper;

		String jsonResponse;

		@Autowired
		HttpService httpService;

		@Getter @Setter
		Designation selectedDesignation = new Designation();

		@Getter @Setter
		Designation designation = new Designation();

		@Getter @Setter
		private RestTemplate restTemplate;

		@Getter @Setter
		private String Url = null;

		@Getter @Setter
		LazyDataModel<Designation> designationlazyLoadList;

		@Getter @Setter
		private Map<String, Object> filterMap;

		@Getter @Setter
		private SortOrder sortingOrder;

		@Getter @Setter
		private String sortingField;

		@Getter @Setter
		private Integer resultSize;

		@Getter @Setter 
		private Integer defaultRowSize;

		@Autowired
		ErrorMap errorMap;

		@Getter
		@Setter
		int totalRecords = 0;

		@Getter
		@Setter
		String action;

		@Getter
		@Setter
		Boolean addButtonFlag = true;
		
		@Getter
		@Setter
		Boolean editButtonFlag = true;
		
		@Getter
		@Setter
		Boolean deleteButtonFlag = true;

		@Autowired
		LoginBean loginBean;
		
		@Autowired
		CommonDataService commonDataService;
		
		@Getter
		@Setter
		List<Designation> designationList = new ArrayList<>();
		
		@Getter
		@Setter
		List<Department> departmentList = new ArrayList<>();
		
		@Getter
		@Setter
		List<CadreMaster> cadreList = new ArrayList<>();
		
		public String showListDesignationPage() {
			log.info("showListDesignationPage called..");
			mapper = new ObjectMapper();
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = true;
			selectedDesignation = new Designation();
			designationList = commonDataService.loadDesignation();
			departmentList = commonDataService.getDepartment();
			cadreList = commonDataService.loadCadreMaster();
			loadDesignationListLazy();
			return DESIGNATION_LIST_PAGE;
		}


		public void loadDesignationListLazy() {

			log.info("<-----Designation list lazy load starts------>");
			designationlazyLoadList = new LazyDataModel<Designation>() {

				private static final long serialVersionUID = 1L;

				@Override
				public List<Designation> load(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters){
					try {
						BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
						if (baseDTO == null) {
							log.error(" Base DTO returned Null Values ");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
							return null;
						}
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
						List<Designation> designationList = mapper.readValue(jsonResponse,
								new TypeReference<List<Designation>>() {
						});
						if (designationList == null) {
							log.info(" DesignationList is null");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}

						if (baseDTO.getStatusCode() == 0) {
							this.setRowCount(baseDTO.getTotalRecords());
							totalRecords = baseDTO.getTotalRecords();
							log.info(" Designation list search Successfully Completed");
						} else {
							log.error(" Designation list search Failed With status code :: " + baseDTO.getStatusCode());
							errorMap.notify(baseDTO.getStatusCode());
						}
						return designationList;

					}catch (Exception e) {
						log.error("ExceptionException Ocured while Loading Designation list :: ", e);
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					return null;
				}



				@Override
				public Object getRowKey(Designation res) {
					return res != null ? res.getId() : null;
				}

				@Override
				public Designation getRowData(String rowKey) {
					List<Designation> responseList = (List<Designation>) getWrappedData();
					Long value = Long.valueOf(rowKey);
					for (Designation res : responseList) {
						if (res.getId().longValue() == value.longValue()) {
							selectedDesignation = res;
							return res;
						}
					}
					return null;
				}
			};
		}


		public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters) {
			BaseDTO dataList = new BaseDTO();
			try {
				log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
						+ sortOrder + "] " + "sortField:[" + sortField + "]");
				Designation request = new Designation();
				request.setFirst(first);
				request.setPagesize(pageSize);
				request.setSortField(sortField);
				request.setSortOrder(sortOrder.toString());
				Map<String, Object> searchFilters = new HashMap<>();
				for (Map.Entry<String, Object> filter : filters.entrySet()) {
					String value = filter.getValue().toString();
					log.info("Key : " + filter.getKey() + " Value : " + value);

					if (filter.getKey().equals("name")) {
						log.info("designationName : " + value);
						searchFilters.put("designationName", value);
					}
					if (filter.getKey().equals("lname")) {
						log.info("designationName in tamil : " + value);
						searchFilters.put("designationNameTamil", value);
					}
					if (filter.getKey().equals("department.name")) {
						log.info("designationName in tamil : " + value);
						searchFilters.put("departmentName", value);
					}
					if (filter.getKey().equals("cadreMaster.name")) {
						log.info("cadreMaster name : " + value);
						searchFilters.put("cadreName", value);
					}
					if (filter.getKey().equals("createdDate")) {
						log.info("createdDate : " + value);
						searchFilters.put("createdDate",value);
					}
					if (filter.getKey().equals("status")) {
						log.info("Status : " + value);
						searchFilters.put("status",value);
					}	
					if (filter.getKey().equals("hierarchyOrder")) {
						log.info("hierarchyOrder : " + value);
						searchFilters.put("hierarchyOrder",value);
					}

				}
				request.setFilters(searchFilters);
				log.info("Search request data" + request);
				dataList = httpService.post(SERVER_URL + "/designation/getalldesignationlistlazy", request);
				log.info("Search request response " + dataList);

			}catch(Exception e) {
				log.error("Exception Occured in Designation load ::", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			return dataList;
		}

		public String designationListAction() {
			log.info("<====== DesignationBean.designationListAction Starts====> action   :::"+action);
			try {
				if(action.equalsIgnoreCase("ADD")) {
					log.info("<====== designation add page called.......====>");
					selectedDesignation = new Designation();
					designation = new Designation();
					designation.setDepartment(new Department());
					designation.setCadreMaster(new CadreMaster());
					return DESIGNATION_ADD_PAGE;
				}
				if(selectedDesignation==null) {
					errorMap.notify(ErrorDescription.SELECT_DESIGNATION.getErrorCode());
					return null;
				}
				if(action.equalsIgnoreCase("DELETE")) {
					RequestContext context = RequestContext.getCurrentInstance();
					context.execute("PF('confirmDesignationDelete').show();");
				}else {
					log.info("<----Loading Designation Edit Or View page.....---->" + selectedDesignation.getId());
					designation = new Designation();
					BaseDTO response = httpService
							.get(SERVER_URL + "/designation/getdesignation/" + selectedDesignation.getId());
					if (response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						designation = mapper.readValue(jsonResponse, new TypeReference<Designation>() {
						});
						log.info("<====== DesignationBean.designationListAction Ends====>");
						if(action.equalsIgnoreCase("EDIT")) {
						return DESIGNATION_ADD_PAGE;
						} else {
						return DESIGNATION_VIEW_PAGE;	
						}
					} else {
						errorMap.notify(response.getStatusCode());
						log.info("<====== DesignationBean.designationListAction Ends====>");
						return null;
					}
				}

			} catch (Exception e) {
				log.error("Exception Occured in DesignationBean.designationListAction::", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			log.info("<====== DesignationBean.designationListAction Ends====>");
			return null;
		}

		public String deleteConfirm() {
			log.info("<--- Starts DesignationBean.deleteConfirm ---->");
			try {
				BaseDTO response = httpService
						.delete(SERVER_URL + "/designation/delete/" + selectedDesignation.getId());
				if(response.getStatusCode() == 0)
					errorMap.notify(ErrorDescription.DESIGNATION_DELETE_SUCCESS.getErrorCode());
				else
					errorMap.notify(response.getStatusCode());	
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			} catch (Exception e) {
				log.error("Exception occured while Deleting designation....", e);
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
			}
			log.info("<--- Ends DesignationBean.deleteConfirm ---->");
			return showListDesignationPage();
		}

		
		public void onRowSelect(SelectEvent event) {
			log.info("<===Starts DesignationBean.onRowSelect ========>"+event);
			selectedDesignation = ((Designation) event.getObject());
			addButtonFlag=false;
			editButtonFlag=true;
			deleteButtonFlag=true;
			/*if(selectedDesignation.getStatus() == true ){
				deleteButtonFlag=true;
			}else if(selectedDesignation.getStatus() == false){
				deleteButtonFlag=false;
			}*/
			log.info("<===Ends DesignationBean.onRowSelect ========>");
	    }

		
		public String submit() {
			log.info("<=======Starts  DesignationBean.submit ======>"+designation);
			
			if(designation.getDepartment() == null || designation.getDepartment().getId() == null ) {
				errorMap.notify(ErrorDescription.DEPARTMENT_NAME_EMPTY.getErrorCode());
				return null;
			}
			if(designation.getCadreMaster() == null || designation.getCadreMaster().getId() == null ) {
				errorMap.notify(ErrorDescription.CADRE_NAME_REQUIRED.getErrorCode());
				return null;
			}
			if(designation.getName() == null || designation.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.DESIGNATION_NAME_EMPTY.getErrorCode());
				return null;
			}
			if(designation.getName().trim().length()<3  || designation.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.DESIGNATION_NAME_MIN_MAX_ERROR.getErrorCode());
				return null;
			}
			if(designation.getLname() == null || designation.getLname().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.DESIGNATION_LNAME_EMPTY.getErrorCode());
				return null;
			}
			if(designation.getLname().trim().length()<3  || designation.getLname().trim().length() > 75) {
				errorMap.notify(ErrorDescription.DESIGNATION_LNAME_MIN_MAX_ERROR.getErrorCode());
				return null;
			}
			if(designation.getStatus() == null) {
				errorMap.notify(ErrorDescription.DESIGNATION_STATUS_EMPTY.getErrorCode());
				return null;
			}
			if(designation.getPromotionPeriod()<1  || designation.getPromotionPeriod() > 11) {
				errorMap.notify(ErrorDescription.DESIGNATION_PROMOTION_PERIOD_BETWEEN_ONE_TO_TEN_YEARS.getErrorCode());
				return null;
			}
			if(designation.getHierarchyOrder() == null) {				
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.DESIGNATION_HIERARCHY_ORDER_EMPTY).getCode());
				return null;
			}
			
			if(action.equalsIgnoreCase("ADD")) {
				designation.setCreatedBy(loginBean.getUserDetailSession());
			}else if (action.equalsIgnoreCase("EDIT")) {
				designation.setModifiedBy(loginBean.getUserDetailSession());
			}
			BaseDTO response= httpService.post(SERVER_URL + "/designation/save",designation);
//			errorMap.notify(response.getStatusCode());
			if(response.getStatusCode()==0) {
				if(action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.DESIGNATION_SAVE_SUCCESS.getErrorCode());
				else 
					errorMap.notify(ErrorDescription.DESIGNATION_UPDATE_SUCCESS.getErrorCode());
				return showListDesignationPage();
			}else {
				errorMap.notify(response.getStatusCode());
			}
			log.info("<=======Ends DesignationBean.submit ======>");
			return null;
		}
		


}
