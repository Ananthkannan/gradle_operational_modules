package in.gov.cooptex.operational.intend.ui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.IntendDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.intend.model.IntendConsolidation;
import in.gov.cooptex.operation.intend.model.IntendConsolidationLog;
import in.gov.cooptex.operation.intend.model.IntendDetails;
import in.gov.cooptex.operation.intend.model.IntendRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("intendConsolidatebean")
@Scope("session")
@Log4j2
public class IntendConsolidatebean {

	final String LIST_PAGE = "/pages/printingAndStationary/listConsolidateRequirements.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/printingAndStationary/createConsolidateRequirements.xhtml?faces-redirect=true";
	final String View_PAGE = "/pages/printingAndStationary/viewConsolidateRequirements.xhtml?faces-redirect=true";
	@Autowired
	AppPreference appPreference;

	final String CONSOLIDATE_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;
	
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	IntendRequest intendRequest, selectedIntendRequest;

	@Getter
	@Setter
	IntendConsolidationLog intendConsolidationlog,selectedintendConsolidationlog;
	
	@Getter
	@Setter
	LazyDataModel<IntendConsolidationLog> lazyIntendConsolidationlogList;

	@Getter
	@Setter
	List<IntendConsolidationLog> intendConsolidationlogList = null;
	
	@Getter
	@Setter
	List<IntendRequest> intendRequestList = null;
	@Getter
	@Setter
	List<IntendDetails> intendDetailsList = null;
	
	@Getter
	@Setter
	List<IntendConsolidation>  intendConsolidationList=null;
	

	@Getter
	@Setter
	String requirementCodeName = "";

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean searchContent = false;


	@Getter
	@Setter
	List<String> sectionList;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	@Getter
	@Setter
	Boolean forwardFor;

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	IntendDTO intenddto;
	
	@Getter
    @Setter
    int totalRecords = 0;
	@Getter
    @Setter
	List<SectionMaster> allSectionList;
	
	@Getter
	@Setter
	String currentPlanStatus;
	
	@Getter
	@Setter
	String comments;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();
	
	@Getter
	@Setter
	EmployeeMaster employeeMaster;
	
	@Getter
	@Setter
	boolean approveRejectRender = true;
	
	@Autowired
	LoginBean loginBean;
	
	public String consolidateListPage() {
		log.info("<==== Starts consolidateListPage =====>");
		mapper = new ObjectMapper();
		addButtonFlag = false;
		viewButtonFlag = true;
		intendConsolidationlog = new IntendConsolidationLog();
		selectedintendConsolidationlog= new IntendConsolidationLog();
		loadLazyConsolidationList();
		log.info("<==== Ends consolidateListPage=====>");
		return LIST_PAGE;
	}
	
	public void  loadLazyConsolidationList() {
		log.info("<===== Starts CommunityMasterBean.loadLazyCommunityList ======>");

		lazyIntendConsolidationlogList= new LazyDataModel<IntendConsolidationLog>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<IntendConsolidationLog> load(int first ,int pageSize,String sortField,SortOrder sortOrder,Map<String,Object> filters){
				try {
					IntendConsolidationLog paginationIntendconsolidatelog=new IntendConsolidationLog();
					
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,sortOrder.toString(), filters);
					paginationIntendconsolidatelog.setPaginationDTO(paginationRequest);
					paginationIntendconsolidatelog.setFilters(filters);
					
					log.info("Pagination request :::"+paginationIntendconsolidatelog);
					String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl() + "/intenddetails/getlazyAllConsolidate";
					BaseDTO response = httpService.post(url, paginationIntendconsolidatelog);
					if(response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						intendConsolidationlogList = mapper.readValue(jsonResponse, new TypeReference<List<IntendConsolidationLog>>(){});
						log.info("=======intendConsolidationLoglist================>"+intendConsolidationlogList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					}else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				}catch(Exception e) {
					log.error("Exception occured in lazyload ...",e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return intendConsolidationlogList;
			}

			@Override
			public Object getRowKey(IntendConsolidationLog res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public IntendConsolidationLog getRowData(String rowKey) {
				for (IntendConsolidationLog consolidationlog: intendConsolidationlogList) {
					if (consolidationlog.getId().equals(Long.valueOf(rowKey))) {
						selectedintendConsolidationlog= consolidationlog;
						return consolidationlog;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends IntendConsolidatebean.loadLazyConsolidationList ======>");
	}
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts CommunityMasterBean.onRowSelect ========>"+event);
		selectedintendConsolidationlog= ((IntendConsolidationLog) event.getObject());
		addButtonFlag=true;
		viewButtonFlag=false;
		log.info("<===Ends CommunityMasterBean.onRowSelect ========>");
    }
	@JsonIgnoreProperties(ignoreUnknown = true)
	public String consolidateListPageAction() {
		log.info("<========inside ConsolidateRequirement.resource=======>");
		try {
			setNote("");
		if (action.equalsIgnoreCase("create")) {
			setSearchContent(false);
			selectedIntendRequest = new IntendRequest();
			log.info("create ConsolidateRequirement called..");
			setRequirementCodeName("");
			getAllRequirementName();
//			loadForwardToUsers();
			//forwardToUsersList=commonDataService.loadForwardToUsers();
			employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CONSOLIDATE_REQUIREMENT");
			
			return ADD_PAGE;
		}else if(action.equalsIgnoreCase("View")) {
			intendConsolidationList=new ArrayList<>();
			allSectionList=new ArrayList<>();
			Set<String> templist = new HashSet<>();
			setSectionList(new ArrayList<>());
			log.info("edit ConsolidateRequirement called..");
			String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl()+ "/intenddetails/getViewConsolidateRecords/"
					+selectedintendConsolidationlog.getIntendRequest().getId();
			BaseDTO responsecon = httpService.get(url);
			
			intenddto=new IntendDTO();
			if (responsecon != null ) {
				log.info("inside responsecon======>");
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponsecon = mapper.writeValueAsString(responsecon.getResponseContent());
				intenddto=mapper.readValue(jsonResponsecon, new TypeReference<IntendDTO>() {
				});
				intendConsolidationList.addAll(intenddto.getIntendConsolidationlist());
				for (IntendConsolidation intendConsolidation : intendConsolidationList) {
					if(intendConsolidation.getSectionMaster()!=null) {
					sectionList.add(intendConsolidation.getSectionMaster().getName());
					}
				}
				log.info("===============>"+intenddto.getIntendConsolidationLogList().get(0).getStage());
				templist.addAll(sectionList);
				sectionList.clear();
				sectionList.addAll(templist);
//				loadForwardToUsers();
				//forwardToUsersList=commonDataService.loadForwardToUsers();
				employeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CONSOLIDATE_REQUIREMENT");
				setForwardTo(intenddto.getIntendConsolidationNoteList().get(0).getUserMaster());
				setForwardFor(intenddto.getIntendConsolidationNoteList().get(0).getFinalApproval());
				log.info("=============>"+intenddto.getIntendConsolidationNoteList().get(0).getFinalApproval());
				setNote(intenddto.getIntendConsolidationNoteList().get(0).getNote());
				if(intenddto.getIntendConsolidationLogList() !=null) {
					
					 if(intenddto.getIntendConsolidationNoteList().get(0).getUserMaster().getId().toString().equalsIgnoreCase(loginBean.getUserDetailSession().getId().toString())) {
						 if(intenddto.getIntendConsolidationLogList().get(0).getStage().equalsIgnoreCase("FINAL_APPROVED")) {
								approveRejectRender = false;
							}else {
								approveRejectRender = true;
							}
					 }else {
						 approveRejectRender = false; 
					 }
					
					
				}
				allSectionList=commonDataService.getActiveSectionMasters();
				return View_PAGE;
			}
			else{
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		}
		
		}catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return null;
	}

	public void getAllRequirementName() {
		BaseDTO baseDTO = new BaseDTO();
		log.info("<=== get All RequirementName====>");
		String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl() + "/intendrequest/getConsolidateRequirement";
		intendRequestList = new ArrayList<>();
		try {
			baseDTO = httpService.get(url);
			log.info("<=========== after URL =======>" + baseDTO);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				intendRequestList = mapper.readValue(jsonResponse, new TypeReference<List<IntendRequest>>() {
				});
				log.info("=============intendRequest=====================" + intendRequestList.size());
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public void search() {
		setSearchContent(false);
		setForwardTo(null);
		setForwardFor(null);
		setNote("");
		Set<String> templist = new HashSet<>();
		BaseDTO baseDTO = new BaseDTO();
		log.info("===value entity===>" + selectedIntendRequest.getEntityMaster().getEntityTypeMaster().getEntityName());
		String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl() + "/intenddetails/getdetailsbyRequestId/"
				+ selectedIntendRequest.getId() + "/"
				+ selectedIntendRequest.getEntityMaster().getEntityTypeMaster().getEntityName();
		setSectionList(new ArrayList<>());
		try {
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				intendDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<IntendDetails>>() {
				});
			}
			if (intendDetailsList.size()>0) {
				setSearchContent(true);
			
			for (IntendDetails intendDetails : intendDetailsList) {
				sectionList.add(intendDetails.getSectionMaster().getName());
			}
			templist.addAll(sectionList);
			sectionList.clear();
			sectionList.addAll(templist);
			
			allSectionList=commonDataService.getActiveSectionMasters();
			
			}
			else {
				errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_EMPTY_REQUIREMENT.getCode());

			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
	}

	public Double quantityValue(IntendDetails intendDetails, String sectionName) {
		Double quantity = 0.0;
		try {
			if (intendDetails.getSectionMaster().getName().equalsIgnoreCase(sectionName)) {
				quantity = intendDetails.getRequiredQuantity();
			} else {
				quantity = 0.0;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return quantity;
	}

	public Double quantityValue(IntendConsolidation intendConsolidation, String sectionName) {
		Double quantity = 0.0;
		try {
			if (intendConsolidation.getSectionMaster().getName().equalsIgnoreCase(sectionName)) {
				quantity = intendConsolidation.getRequiredQuantity();
			} else {
				quantity = 0.0;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return quantity;
	}

	public Double priceValue(IntendDetails intendDetails, String sectionName) {
		Double price = 0.0;
		try {
			if (intendDetails.getSectionMaster().getName().equalsIgnoreCase(sectionName)) {
				price = intendDetails.getPurchasePrice();
			} else {
				price = 0.0;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return price;
	}
	public Double priceValue(IntendConsolidation intendConsolidation, String sectionName) {
		Double price = 0.0;
		try {
			if (intendConsolidation.getSectionMaster().getName().equalsIgnoreCase(sectionName)) {
				price = intendConsolidation.getPurchasePrice();
			} else {
				price = 0.0;
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		return price;
	}

/*	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/user/getallforwardtousers";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				forwardToUsersList = mapper.readValue(jsonResponse, new TypeReference<List<UserMaster>>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}*/

	public String createConsolidate() {
		log.info("<==== Starts IntendConsolidate.create =======>");
		try {
			if (forwardTo==null) {
				errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_REQUIREMENT_FORWARDTO_REQUIRED.getCode());
				return null;
			}
			
			intenddto = new IntendDTO();
			intenddto.setIntendDetailsList(intendDetailsList);
			intenddto.setIntendRequest(selectedIntendRequest);
			intenddto.setUserMaster(forwardTo);
			intenddto.setNote(note);
			intenddto.setForwardFor(forwardFor);
			intenddto.setStatus("SUBMITTED");
			String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl()
					+ "/intenddetails/createConsolidateRequest";
			BaseDTO response = httpService.post(url, intenddto);

			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_INSERTED_SUCCESSFULLY.getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends IntendConsolidate.createConsolidate =======>");
		return LIST_PAGE;
	}
	
	public String statusUpdate() {
		try {
			IntendDTO intend = new IntendDTO();
			intend.setIntendConsolidationNoteList(intenddto.getIntendConsolidationNoteList());
			intend.setIntendRequest(intenddto.getIntendRequest());
			intend.setComments(comments);
			intend.setUserMaster(forwardTo);
			intend.setForwardFor(forwardFor);
			if (forwardFor==true && currentPlanStatus.equalsIgnoreCase("APPROVED")) {
				currentPlanStatus="FINAL_APPROVED";
			}
			intend.setCurrentPlanStatus(currentPlanStatus);
			String url = CONSOLIDATE_URL + appPreference.getOperationApiUrl()
					+ "/intenddetails/forwardStage";
			BaseDTO response = httpService.post(url, intend);
			if (response != null && response.getStatusCode() == 0) {
				if (currentPlanStatus.equalsIgnoreCase("FINAL_APPROVED") && currentPlanStatus.equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_APPROVED_SUCCESSFULLY.getCode());
				}
				else if (currentPlanStatus.equalsIgnoreCase("REJECTED") ) {
					errorMap.notify(ErrorDescription.INTEND_CONSOLIDATION_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
	return LIST_PAGE;
	}
	
	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		currentPlanStatus = value;
		comments = null;
	}

}
