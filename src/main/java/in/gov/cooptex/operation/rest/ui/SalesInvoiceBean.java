/**
 * 
 */
package in.gov.cooptex.operation.rest.ui;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.SalesInvoiceDTO;
import in.gov.cooptex.core.model.CustomerTypeMaster;
import in.gov.cooptex.core.model.SaleTypeMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("salesInvoiceBean")
@Scope("session")
public class SalesInvoiceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String HEAD_OFFICE_ADD_URL = "/pages/operation/retailproduction/retailSalesProductionPlanRequest.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_VIEW_URL = "/pages/operation/retailproduction/viewSalesProductionPlanRequest.xhtml?faces-redirect=true;";

	private final String HEAD_OFFICE_LIST_URL = "/pages/operation/sales/listSalesInvoice.xhtml?faces-redirect=true;";

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<SaleTypeMaster> salesTypeList;

	@Getter
	@Setter
	List<CustomerTypeMaster> customerTypeList;

	@Getter
	@Setter
	String action = null;

	@Getter
	@Setter
	String serverURL = null;

	@Getter
	@Setter
	Integer planSize;

	@Getter
	@Setter
	SalesInvoiceDTO salesInvoiceDTO;

	@Getter
	@Setter
	LazyDataModel<SalesInvoiceDTO> lazySalesInvoiceDTOList;

	@Autowired
	AppPreference appPreference;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>>>>>>>>>>");
		getSalesInvoiceTypes();
		getAllCustomerTypes();
		loadLazySalesInvoiceList();
	}

	public SalesInvoiceBean() {
		log.info("<#======Inside RetailProduction Plan Bean======#>");
		loadUrl();
	}

	private void loadUrl() {
		try {
			serverURL = AppUtil.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadValues() >>>> " + e.toString());
			log.error("Exception ", e);
		}
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();
		log.info("<---Inside search called--->");
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		if (sortField != null && sortField.equals("customerName")) {
			sortField = "customerAlias.name";
		}
		if (sortField != null && sortField.equals("customerType")) {
			sortField = "customerTypeAlias.name";
		}

		SalesInvoiceDTO request = new SalesInvoiceDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("invoiceNumber")) {
				request.setInvoiceNumber(Long.parseLong(value));
			}
			if (entry.getKey().equals("saleType")) {
				request.setSaleTypeId(Long.parseLong(value));
			}
			if (entry.getKey().equals("customerName")) {
				request.setCustomerName(value);
			}
			if (entry.getKey().equals("customerType")) {
				request.setCustomerTypeId(Long.parseLong(value));
			}
			if (entry.getKey().equals("invoiceDate")) {
				request.setInvoiceDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("netTotal")) {
				request.setNetTotal(Long.parseLong(value));
			}

		}
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/salesinvoice/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return baseDTO;
	}

	public void loadLazySalesInvoiceList() {

		log.info("starts loadLazySalesInvoiceList====>");
		lazySalesInvoiceDTOList = new LazyDataModel<SalesInvoiceDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<SalesInvoiceDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<SalesInvoiceDTO> data = new ArrayList<SalesInvoiceDTO>();
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<SalesInvoiceDTO>>() {
					});
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						planSize = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(SalesInvoiceDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SalesInvoiceDTO getRowData(String rowKey) {
				List<SalesInvoiceDTO> responseList = (List<SalesInvoiceDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (SalesInvoiceDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						salesInvoiceDTO = res;
						return res;
					}
				}
				return null;
			}

		};
	}

	public String editPlan() {
		return HEAD_OFFICE_ADD_URL;
	}

	public String viewPlan() {
		log.info("<<<<< SalesInvoicePlanBean-viewPlan() >>>>> 1");
		return HEAD_OFFICE_VIEW_URL;
	}

	public List<SaleTypeMaster> getSalesInvoiceTypes() {
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/salesinvoice/getallsalestypes";
			BaseDTO baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			salesTypeList = mapper.readValue(jsonResponse, new TypeReference<List<SaleTypeMaster>>() {
			});
			log.info("<<<<< salesTypeList >>>>> 1" + salesTypeList);
		} catch (Exception e) {
			log.error("Error in getSalesInvoiceTypes List --==>", e);
		}
		return null;
	}

	public List<SaleTypeMaster> getAllCustomerTypes() {
		try {
			String URL = serverURL + appPreference.getOperationApiUrl() + "/customermaster/getallcustomertypes";
			BaseDTO baseDTO = httpService.get(URL);
			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			customerTypeList = mapper.readValue(jsonResponse, new TypeReference<List<CustomerTypeMaster>>() {
			});

		} catch (Exception e) {
			log.error("Error in getSalesInvoiceTypes List --==>", e);
		}
		return null;
	}

	public String getSalesInvoiceList() {
		salesInvoiceDTO = new SalesInvoiceDTO();
		getSalesInvoiceTypes();
		getAllCustomerTypes();
		loadLazySalesInvoiceList();
		return HEAD_OFFICE_LIST_URL;

	}

}