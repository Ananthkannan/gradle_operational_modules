package in.gov.cooptex.operation.rest.ui;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.YarnConsolidationRequirementViewDTO;
import in.gov.cooptex.operation.dto.YarnRequestConsilidateListDataDTO;
import in.gov.cooptex.operation.dto.YarnRequirementConsolidateSearchDTO;
import in.gov.cooptex.operation.dto.YarnRequirementConsolidationDTO;
import in.gov.cooptex.operation.model.YarnConsolidateReqNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service
public class YarnRequirementConsolidationBean {

	private static final String LIST_PAGE= "/pages/operation/yarn/listYarnRequirementConsolidate.xhtml?faces-redirect=true;";
	private static final String ADD_PAGE= "/pages/operation/yarn/createYarnRequirementConsolidate.xhtml?faces-redirect=true;";
	private static final String VIEW_PAGE= "/pages/operation/yarn/viewYarnRequirementConsolidate.xhtml?faces-redirect=true;";
	
	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;
	
	private String serverURL;

	private BaseDTO baseDTO;

	private String jsonResponse;

	ObjectMapper mapper = new ObjectMapper();
	
	@Getter
	@Setter
	YarnRequirementConsolidationDTO dto;
	
	@Getter
	@Setter
	List<YarnRequestConsilidateListDataDTO> resultList = new ArrayList<YarnRequestConsilidateListDataDTO>();
	
	@Getter
	@Setter
	private String fromMonth,toMonth,note,remarks,action,pageHead;
	
	@Getter
	@Setter
	private Long fromYear,toYear;
	
	@Getter
	private String[] monthList = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
	
	@Getter
	private Long[] yearList = {2008l,2009l,2010l,2011l,2012l,2013l,2014l,2015l,2016l,2017l,2018l,2019l,2020l,2021l,2022l,2023l,2024l,2025l,2026l,2027l,2028l};
	
	@Getter
	private List<String> columnNameList = new ArrayList<String>();
	
	@Getter
	@Setter
	UserMaster forwardTo;
	
	@Getter
	@Setter
	List<UserMaster> forwardToList;
	
	@Getter
	@Setter
	YarnRequirementConsolidateSearchDTO responseDto;
	
	@Getter
	@Setter
	LazyDataModel<YarnRequirementConsolidateSearchDTO> lazyList;
	
	@Getter
	@Setter
	Boolean approveStatus;
	
	@Getter
	@Setter
	Integer totalRecords;
	
	@Getter
	@Setter
	YarnConsolidateReqNote noteEntity = new YarnConsolidateReqNote();
	
	@Getter
	@Setter
	Boolean addBtnFlag=true,editBtnFlag = false,viewBtnFlag=false;
	
	@Autowired
	CommonDataService commonDataService;
	
	public String showListPage() {
		action="LIST";
		serverURL = AppUtil.getPortalServerURL();
		listData();
		viewBtnFlag=editBtnFlag = false;
		addBtnFlag=true;
		return LIST_PAGE;
	}
	
	public String showCreatePage() {
		action="ADD";
		pageHead = "Create";
		serverURL = AppUtil.getPortalServerURL();
		dto =  new YarnRequirementConsolidationDTO();
		fromMonth = toMonth ="";
		fromYear = toYear =0L;
		note = "";
		forwardTo = new UserMaster();
		approveStatus=null;
		columnNameList.clear();
		resultList.clear();
		loadForwardUser();
		return ADD_PAGE;
	}
	
	public String showViewPage() {
		if(responseDto==null || responseDto.getId()<1) {
			errorMap.notify(ErrorDescription.SELECT_TICKET_FAQ.getErrorCode());
			return null;
		}
		action="VIEW";
		loadForwardUser();
		viewConsolidation();
		addBtnFlag=editBtnFlag = false;
		viewBtnFlag=true;
		return VIEW_PAGE;
	}
	
	public String cancel() {
		fromMonth = toMonth ="";
		fromYear = toYear =0L;
		return ADD_PAGE;
	}
	public String validateAndGenerate() {
		log.info("<<====  validateAndGenerate :: ");
		if(fromMonth==null || fromMonth.isEmpty()) {
			errorMap.notify(ErrorDescription.FROM_MONTH_REQUIRED.getErrorCode());
			log.info("<<======  fromMonth  NULL");
			return null;
		}
		if(fromYear==null || fromYear<1) {
			errorMap.notify(ErrorDescription.FROM_YEAR_REQUIRED.getErrorCode());
			log.info("<<======  fromYear  NULL");
			return null;
		}
		if(toMonth==null || toMonth.isEmpty()) {
			errorMap.notify(ErrorDescription.TO_MONTH_REQUIRED.getErrorCode());
			log.info("<<======  toMonth  NULL");
			return null;
		}
		if(toYear==null || toYear<1) {
			errorMap.notify(ErrorDescription.TO_YEAR_REQUIRED.getErrorCode());
			log.info("<<======  toYear  NULL");
			return null;
		}
		dto.setFromMonth(fromMonth);
		dto.setFromYear(fromYear);
		dto.setToMonth(toMonth);
		dto.setToYear(toYear);
		fetchDataMap();
		
		return ADD_PAGE;
	}
	
	private void fetchDataMap() {
		log.info("<<========  YarnRequirementConsolidationBean --- fetchDataMap ====##STARTS");
		try {
			String url = serverURL  + "/yarnrequirementConsolidation/generate";
			log.info("<<======  URL  :: "+url);
			baseDTO = httpService.post(url,dto);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				resultList = mapper.readValue(jsonResponse, new TypeReference<List<YarnRequestConsilidateListDataDTO>>() {
				});
				log.info("<<====  LIST:: "+resultList);
				if(resultList!=null && resultList.size()>0) {
//					columnList.clear();
					for(YarnRequestConsilidateListDataDTO data: resultList){
						log.info("<<=======  name:: "+data.getEntityName());
						log.info("<<====  code:: "+data.getEntityCode());
						log.info("<<======unit names:: "+data.getProductVarietyNameList());
						log.info("<===== requirements:: "+data.getTotalRequiredQuantityList());
						log.info("<<=====  Column MAp:: "+data.getColumnMap());
//						data.setColumnList(columnList);
						columnNameList = data.getProductVarietyNameList();
					}
					
				}
					
			}
		}catch(Exception e) {
			log.error("<<=====  ERROR::: ", e);
			}
		log.info("<<========  YarnRequirementConsolidationBean --- fetchDataMap ====##ENDS");
	}
	
	public void prepareData() {
		log.info("<<========  YarnRequirementConsolidationBean --- prepareData ====##STARTS");
		try {
			if(resultList!=null) {
				
			}
		}catch(Exception e) {
			log.error("<<=====  ERROR::: ", e);
			}
		log.info("<<========  YarnRequirementConsolidationBean --- prepareData ====##ENDS");
	}
	
	public void loadForwardUser() {
		log.info("<<===============    AdvertisementBean ---   doForward  =====##STARTS");
		forwardToList = commonDataService.loadForwardToUsers();
		log.info("<<===============    AdvertisementBean ---   doForward  =====##ENDS");
	}
	
	public String createConsolidation() {
		log.info("<<========  YarnRequirementConsolidationBean --- createConsolidation ====##STARTS");
		
		if(forwardTo==null || forwardTo.getId()<1) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
			return null;
		}
		if(note==null || note.isEmpty()) {
			errorMap.notify(ErrorDescription.NOTE_REQUIRED.getErrorCode());
			return null;
		}
		if(approveStatus==null) {
			errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
			return null;
		}
			
		
		dto.setForwardTo(forwardTo);
		dto.setNote(note);
		dto.setApprovalStatus(approveStatus);
		try {
			Validate.notNull(resultList);
			
			dto.setConsolidateList(resultList);
			String url = serverURL  + "/yarnrequirementConsolidation/create";
			log.info("<<======  URL  :: "+url);
			baseDTO = httpService.post(url,dto);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				errorMap.notify(ErrorDescription.YARN_REQUIREMENT_CONSOLIDATION_CREATE_SUCCESS.getErrorCode());
				log.info("<<==== Yarn requirement Consolidation CREATED:: ");
				
			}
		}catch(Exception e) {
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			log.error("<<=====  ERROR::: ", e);
			}
		log.info("<<========  YarnRequirementConsolidationBean --- createConsolidation ====##ENDS");
		return LIST_PAGE;
	}
	
	
	public String viewConsolidation() {
		log.info("<<===============  YarnRequirementConsolidationBean ----- viewConsolidation  ====##STARTS");
		try {
			Validate.notNull(responseDto);
			String url = serverURL  + "/yarnrequirementConsolidation/view/"+responseDto.getId();
			log.info("<<======  URL  :: "+url);
			baseDTO = httpService.get(url);
			if(baseDTO!=null && baseDTO.getStatusCode()==0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				resultList = mapper.readValue(jsonResponse, new TypeReference<List<YarnRequestConsilidateListDataDTO>>() {
				});
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				noteEntity = mapper.readValue(jsonResponse,YarnConsolidateReqNote.class);
				log.info("<<====  VIEW DTO :::: "+noteEntity);
				forwardTo = noteEntity.getForwardTo();
				note = noteEntity.getNote();
				approveStatus = noteEntity.getFinalApproval();
				log.info("<<====  LIST:: "+resultList);
				if(resultList!=null && resultList.size()>0) {
//					columnList.clear();
					for(YarnRequestConsilidateListDataDTO data: resultList){
						log.info("<<=======  name:: "+data.getEntityName());
						log.info("<<====  code:: "+data.getEntityCode());
						log.info("<<======unit names:: "+data.getProductVarietyNameList());
						log.info("<===== requirements:: "+data.getTotalRequiredQuantityList());
						log.info("<<=====  Column MAp:: "+data.getColumnMap());
//						data.setColumnList(columnList);
						columnNameList = data.getProductVarietyNameList();
					}
					
				}
			}
			
		}catch(Exception e) {
			log.error("<<============  ERRRO IN VIEW::: ",e);
			}
		log.info("<<===============  YarnRequirementConsolidationBean ----- viewConsolidation  ====##ENDSS");
		return VIEW_PAGE;
	}
	
	public void listData() {
		log.info("<<======  YarnRequirementConsolidationBean --- listData ==##STARTS ");
		lazyList =  new LazyDataModel<YarnRequirementConsolidateSearchDTO>() {


			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<YarnRequirementConsolidateSearchDTO> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				List<YarnRequirementConsolidateSearchDTO> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse,
								new TypeReference<List<YarnRequirementConsolidateSearchDTO>>() {
								});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(YarnRequirementConsolidateSearchDTO res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public YarnRequirementConsolidateSearchDTO getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<YarnRequirementConsolidateSearchDTO> responseList = (List<YarnRequirementConsolidateSearchDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (YarnRequirementConsolidateSearchDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		
		};
		
		log.info("<<======  YarnRequirementConsolidationBean --- listData ==##ENDS ");
	}
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		responseDto = new YarnRequirementConsolidateSearchDTO();

		BaseDTO baseDTO = new BaseDTO();

		YarnRequirementConsolidateSearchDTO request = new YarnRequirementConsolidateSearchDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("fromMonth")) {
				request.setFromMonth(value);
			} else if (key.equals("fromYear")) {
				request.setFromYear(Long.parseLong(value));
			} else if (key.equals("toMonth")) {
				request.setToMonth(value);
			} else if (key.equals("toYear")) {
				request.setToYear(Long.parseLong(value));
			} else if (key.equals("stage")) {
				request.setStage(value);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {

			String url = serverURL + "/yarnrequirementConsolidation/search";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;

	}

	public void onRowSelect(SelectEvent event) {
		log.info("Yarn Consolidate onRowSelect method started");
		responseDto = ((YarnRequirementConsolidateSearchDTO) event.getObject());
		viewBtnFlag=editBtnFlag = true;
		addBtnFlag=false;
	}
	
	
	
}
