package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

import org.apache.http.impl.client.HttpClients;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.accounts.model.BankBranchAddress;
import in.gov.cooptex.core.accounts.model.BankBranchMaster;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.UserDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.enums.ErrorMessageType;
import in.gov.cooptex.core.model.AreaMaster;
import in.gov.cooptex.core.model.BankMaster;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.ErrorMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.dto.BankBranchMasterDTO;
import in.gov.cooptex.operation.enums.BankBranchStatus;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("bankBranchMasterBean")
@Scope("session")
public class BankBranchMasterBean implements Serializable {
	private final String BANK_BRANCH_LIST_PAGE = "/pages/masters/listBankBranch.xhtml?faces-redirect=true;";
	private final String BANK_BRANCH_CREATE_PAGE = "/pages/masters/createBankBranchMaster.xhtml?faces-redirect=true;";
	private final String BANK_BRANCH_VIEW_PAGE = "/pages/masters/viewBankBranchMaster.xhtml?faces-redirect=true;";
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	private String branchCode;
	@Getter
	@Setter
	private String branchName;
	@Getter
	@Setter
	private String activeStatusMsg = null;
	@Getter
	@Setter
	private boolean activeStatus = false;
	@Getter
	@Setter
	private String branchLName;
	@Getter
	@Setter
	private UserDTO userDto;
	@Autowired
	private LoginBean loginBean;
	@Autowired
	LanguageBean languageBean;
	@Autowired
	HttpService httpService;
	@Getter
	@Setter
	BankBranchMasterDTO bankBranchMasterDTO;

	@Getter
	@Setter
	BankBranchMaster bankBranchMaster;

	@Getter
	@Setter
	private RestTemplate restTemplate;
	@Getter
	@Setter
	private String Url = null;
	@Getter
	@Setter
	private List<BankBranchMasterDTO> bankBranchMasterDtoList;
	@Getter
	@Setter
	private WrapperDTO wrapperDto;
	@Getter
	@Setter
	private ArrayList<BankBranchMasterDTO> bankBranchMasterList;
	/**
	 * For getting which button is clicked
	 */
	@Getter
	@Setter
	private String action;
	@Getter
	@Setter
	private boolean disable;
	@Autowired
	private ErrorMap errorMap;
	
	@Getter
	@Setter
	private List<Object> statusValues;
	@Getter
	@Setter
	private Integer numberOfRecords = 0;

	@Getter
	@Setter
	LazyDataModel<BankBranchMasterDTO> bankBranchMasterlazyLoadList;
	@Getter
	@Setter
	private Map<String, Object> filterMap;
	@Getter
	@Setter
	private SortOrder sortingOrder;
	@Getter
	@Setter
	private String sortingField;
	@Getter
	@Setter
	private Integer resultSize;
	@Getter
	@Setter
	private Integer defaultRowSize;
	@Getter
	@Setter
	boolean addButtonFlag=true;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	BaseDTO baseDTO;

	@Getter
	@Setter
	private String pageHead;

	@Getter
	@Setter
	private List<BankMaster> listBankMaster;

	@Getter
	@Setter
	private BankMaster bankMaster;

	@Getter
	@Setter
	private List<StateMaster> listStateMaster;

	@Getter
	@Setter
	private StateMaster stateMaster;

	@Getter
	@Setter
	private List<DistrictMaster> listDistrictMaster;

	@Getter
	@Setter
	private DistrictMaster districtMaster;

	@Getter
	@Setter
	private List<TalukMaster> listTalukMaster;

	@Getter
	@Setter
	private TalukMaster talukMaster;

	@Getter
	@Setter
	private List<CityMaster> listCityMaster;

	@Getter
	@Setter
	private CityMaster cityMaster;

	@Getter
	@Setter
	private List<AreaMaster> listAreaMaster;

	@Getter
	@Setter
	private AreaMaster areaMaster;

	@Getter
	@Setter
	private String addressTextArea;
	
	@Autowired
	AppPreference appPreference;

	@PostConstruct
	public void init() {
		loadStatusValues();
		lazyLoadBankBranchList();
	}

	public BankBranchMasterBean() {

		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
				HttpClients.createDefault());
		restTemplate = new RestTemplate(requestFactory);
		loadUrl();
		log.info("SERVER URL----" + Url);

		listBankMaster = new ArrayList<BankMaster>();
		listStateMaster = new ArrayList<StateMaster>();
		listDistrictMaster = new ArrayList<DistrictMaster>();
		listTalukMaster = new ArrayList<TalukMaster>();
		listCityMaster = new ArrayList<CityMaster>();
		listAreaMaster = new ArrayList<AreaMaster>();

	}

	private void loadUrl() {
		try {
			Url = AppUtil.getPortalServerURL();

		} catch (Exception e) {
			log.fatal("Exception at loadUrl() >>>> " + e.toString());
			e.printStackTrace();
		}
	}

	public void loadBankMaster() {
		try {
			String url = Url + "/bankmaster/getAllBanks";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listBankMaster = mapper.readValue(jsonResponse, new TypeReference<List<BankMaster>>() {
			});
		} catch (Exception e) {
			log.error("<<==============  error in loadBankMaster ", e);
		}
	}

	public void loadStateMaster() {
		try {
			String url = Url + "/stateMaster/getAllStates";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listStateMaster = mapper.readValue(jsonResponse, new TypeReference<List<StateMaster>>() {
			});
			listDistrictMaster.clear();
			listCityMaster.clear();
			listTalukMaster.clear();
			listAreaMaster.clear();
		} catch (Exception e) {
			log.error("<<==============  error in loadStateMaster  ", e);
		}
	}

	public void onchangeState() {
		log.info("<<===  selected state ::: " + stateMaster.getId());
		try {
			String url = Url + "/districtMaster/getalldistrictbystate/" + stateMaster.getId();
			log.info("<<=== get District URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listDistrictMaster = mapper.readValue(jsonResponse, new TypeReference<List<DistrictMaster>>() {
			});
			listCityMaster.clear();
			listTalukMaster.clear();
			listAreaMaster.clear();
			log.info("<<====  Districts::: " + listDistrictMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in onchangeState  ", e);
		}
	}

	public void loadBankBranch() {
		log.info("<<===  Bank Branch ::: " + bankBranchMasterDTO.getId());
		try {
			String url = Url + "/bankBranchMaster/getbybranchid/" + bankBranchMasterDTO.getId();
			log.info("<<=== get Bank Branch URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			bankBranchMaster = mapper.readValue(jsonResponse, new TypeReference<BankBranchMaster>() {
			});
			if (bankBranchMaster != null) {
				getAddress();
			}
			log.info("<<====  bankBranchMaster::: " + bankBranchMaster);
		} catch (Exception e) {
			log.error("<<==============  error in loadBankBranch  ", e);
		}
	}

	public void onchangeDistrict() {
		log.info("<<===  selected district ::: " + districtMaster);
		try {
			String url = Url + appPreference.getOperationApiUrl() +"/city/getactivecitiesbydistrict/" + districtMaster.getId();
			log.info("<<=== get District URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listCityMaster = mapper.readValue(jsonResponse, new TypeReference<List<CityMaster>>() {
			});
			url = Url + "/taluk/getbydistrict/" + districtMaster.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listTalukMaster = mapper.readValue(jsonResponse, new TypeReference<List<TalukMaster>>() {
			});
			listAreaMaster.clear();
			log.info("<<====  Cisties::: " + listCityMaster.size());
			log.info("<<====  Taluks::: " + listTalukMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in onchangeDistrict  ", e);
		}
	}
	public void onchangeCity() {
		log.info("<<===  selected state ::: " + bankBranchMaster.getBankBranchAddress().getCityMaster().getId());
		try {
			String url = Url + "/area/getactiveareasbycity/"
					+ bankBranchMaster.getBankBranchAddress().getCityMaster().getId();
			log.info("<<=== get area URL " + url);
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listAreaMaster = mapper.readValue(jsonResponse, new TypeReference<List<AreaMaster>>() {
			});
			log.info("<<====  listAreaMaster::: " + listAreaMaster.size());
		} catch (Exception e) {
			log.error("<<==============  error in onchangeCity  ", e);
		}
	}

	public void getAddress() {
		log.info("<<====  getAddress  =========>>");

		addressTextArea = "";
		
		if(bankBranchMaster != null && bankBranchMaster.getBankBranchAddress() != null) {
			if (bankBranchMaster.getBankBranchAddress().getLineOne() != null
					&& !bankBranchMaster.getBankBranchAddress().getLineOne().isEmpty())
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineOne() + ", ";
			if (bankBranchMaster.getBankBranchAddress().getLineTwo() != null
					&& !bankBranchMaster.getBankBranchAddress().getLineTwo().isEmpty())
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineTwo() + ", ";
			if (bankBranchMaster.getBankBranchAddress().getLineThree() != null
					&& !bankBranchMaster.getBankBranchAddress().getLineThree().isEmpty())
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineThree() + ", ";
			if (bankBranchMaster.getBankBranchAddress().getAreaMaster() != null)
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getAreaMaster().getName()
						+ ", ";
			if (bankBranchMaster.getBankBranchAddress().getTalukMaster() != null)
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getTalukMaster().getName()
						+ ", ";
			if (bankBranchMaster.getBankBranchAddress().getCityMaster() != null)
				addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getCityMaster().getName()
						+ ", ";
			if (districtMaster != null)
				addressTextArea = addressTextArea + districtMaster.getName() + ", ";
			if (stateMaster != null)
				addressTextArea = addressTextArea + stateMaster.getName() + ", ";
			if (bankBranchMaster.getBankBranchAddress().getPostalCode() != null
					&& !bankBranchMaster.getBankBranchAddress().getPostalCode().isEmpty())
				addressTextArea = addressTextArea + " Pin: " + bankBranchMaster.getBankBranchAddress().getPostalCode()
						+ ", ";
			if (bankBranchMaster.getBankBranchAddress().getLandMark() != null
					&& !bankBranchMaster.getBankBranchAddress().getLandMark().isEmpty())
				addressTextArea = addressTextArea + " Landmark: " + bankBranchMaster.getBankBranchAddress().getLandMark()
						+ ", ";
		}
	}

	public void createAddress() {
		log.info("<<====  createAddress  =========>>");

		addressTextArea = "";
		if (bankBranchMaster.getBankBranchAddress().getLineOne() != null
				&& !bankBranchMaster.getBankBranchAddress().getLineOne().isEmpty())
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineOne() + ", ";
		if (bankBranchMaster.getBankBranchAddress().getLineTwo() != null
				&& !bankBranchMaster.getBankBranchAddress().getLineTwo().isEmpty())
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineTwo() + ", ";
		if (bankBranchMaster.getBankBranchAddress().getLineThree() != null
				&& !bankBranchMaster.getBankBranchAddress().getLineThree().isEmpty())
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getLineThree() + ", ";
		if (bankBranchMaster.getBankBranchAddress().getAreaMaster() != null)
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getAreaMaster().getName()
					+ ", ";
		if (bankBranchMaster.getBankBranchAddress().getTalukMaster() != null)
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getTalukMaster().getName()
					+ ", ";
		if (bankBranchMaster.getBankBranchAddress().getCityMaster() != null)
			addressTextArea = addressTextArea + bankBranchMaster.getBankBranchAddress().getCityMaster().getName()
					+ ", ";
		if (districtMaster != null)
			addressTextArea = addressTextArea + districtMaster.getName() + ", ";
		if (stateMaster != null)
			addressTextArea = addressTextArea + stateMaster.getName() + ", ";
		if (bankBranchMaster.getBankBranchAddress().getPostalCode() != null
				&& !bankBranchMaster.getBankBranchAddress().getPostalCode().isEmpty())
			addressTextArea = addressTextArea + " Pin: " + bankBranchMaster.getBankBranchAddress().getPostalCode()
					+ ", ";
		if (bankBranchMaster.getBankBranchAddress().getLandMark() != null
				&& !bankBranchMaster.getBankBranchAddress().getLandMark().isEmpty())
			addressTextArea = addressTextArea + " Landmark: " + bankBranchMaster.getBankBranchAddress().getLandMark()
					+ ", ";

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('addresspanel').hide();");
		context.update("addForm:textarea");
	}

	/**
	 * This Method Insert The Product Category In Db via REST API
	 * Table(product_category)
	 * 
	 * @return
	 */
	public String createBankBranch() {
		log.info("<<====Inside createBankBranch()======>>" + bankBranchMaster);
		UserMaster user = new UserMaster();
		user.setId(loginBean.getUserDetailSession().getId());

		try {
			BaseDTO baseDTO = null;
			if (action.equals("ADD")) {
				baseDTO = httpService.post(Url + "/bankBranchMaster/addBankBranch", bankBranchMaster);
				if (baseDTO != null) {
					log.info("Status code:----->" + baseDTO.getStatusCode() );
					if(baseDTO.getStatusCode().equals(ErrorDescription.BANK_BRANCH_MASTER_BRANCH_CODE_ALREADY_EXISTS.getErrorCode())) {
						AppUtil.addWarn("Branch Code Already Exists");
						return null;
					}else if(baseDTO.getStatusCode().equals(MastersErrorCode.BRANCH_NAME_ALREADY_EXISTS)) {
						AppUtil.addWarn("Branch Name Already Exists");
						return null;
					}else if(baseDTO.getStatusCode().equals(MastersErrorCode.BRANCH_IFSC_CODE_ALREADY_EXISTS)) {
						AppUtil.addWarn("IFSC Code Already Exists");
						return null;
					}else {
						errorMap.notify(baseDTO.getStatusCode());
					}
				} 
			} else if (action.equals("EDIT")) {
				baseDTO = httpService.put(Url + "/bankBranchMaster/updateBankBranch", bankBranchMaster);
				if (baseDTO != null) {
					log.info("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					ErrorMaster error = errorMap.getErrorMaster(baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					if (error != null && (error.getMessageType().equals(ErrorMessageType.ERROR)
							|| error.getMessageType().equals(ErrorMessageType.WARN))) {
						return null;
					}
				}
			} else {
				log.info(" ");
			}
		} catch (RestException e) {
			log.error("Error while saving Product category", e);
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		}
		addButtonFlag = true;
		lazyLoadBankBranchList();
		bankBranchMasterDTO = null;
		return BANK_BRANCH_LIST_PAGE;
	}

	public String showCreateBankBranchPage() {
		log.info("inside BankBranchMasterBean  showCreateBankBranchPage ======");
		log.info("Button Pressed " + action);
		loadBankMaster();
		loadStateMaster();

		if (action.equals("EDIT")) {
			if(bankBranchMasterDTO==null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
				
			}
			pageHead = "Edit";
			log.info("Inside showCreateBankBranchPage()>>> IF >>>>");
			loadBankBranch();
			if (bankBranchMaster != null && bankBranchMaster.getBankBranchAddress() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster().getDistrictMaster() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster().getDistrictMaster()
							.getStateMaster() != null) {
				stateMaster = bankBranchMaster.getBankBranchAddress().getCityMaster().getDistrictMaster()
						.getStateMaster();
				onchangeState();
			}
			if (bankBranchMaster != null && bankBranchMaster.getBankBranchAddress() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster().getDistrictMaster() != null) {
				districtMaster = bankBranchMaster.getBankBranchAddress().getCityMaster().getDistrictMaster();
				onchangeDistrict();
			}
			if (bankBranchMaster != null && bankBranchMaster.getBankBranchAddress() != null
					&& bankBranchMaster.getBankBranchAddress().getCityMaster() != null) {
				onchangeCity();
			}
		} else {
			pageHead = "Create";
			branchCode = null;
			branchName = null;
			branchLName = null;
			stateMaster = null;
			districtMaster = null;
			addressTextArea = "";
			bankBranchMaster = new BankBranchMaster();
			// bankBranchMaster.setBankBranchAddress(new BankBranchAddress());
			// bankBranchAddress = new BankBranchAddress();
		}
		if (action == null) {
			return BANK_BRANCH_LIST_PAGE;
		}
		log.info("showCreateBankBranchPage()>>>> outside IF>>>");
		return BANK_BRANCH_CREATE_PAGE;
	}

	public void openPopUp() {
		log.info("Inside BankBranchBean>>>> openPopUp()");
		if (action.equals("ADD")) {
			bankBranchMaster.setBankBranchAddress(new BankBranchAddress());
		}

	}

	public String showListBankBrancgPage() {
		log.info("Inside BankBranchBean>>>> showListBankBranchPage()");
		addButtonFlag = true;
		lazyLoadBankBranchList();
		return BANK_BRANCH_LIST_PAGE;
	}
	
	public String showdelete() {
		try {
			action = "Delete";
			if (bankBranchMasterDTO == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmBankBranchDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteBankBranch() {
		log.info("Inside BankBranchMasterBean>>>>>> deleteBankBranch>>>>> ");
		log.info(" >>>>>>>>>>>> " + bankBranchMasterDTO);
		if (action.equals("DELETE") && bankBranchMasterDTO == null) {

			log.info("inside deleteBankBranch>>> IF  >>>>>>>>>");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return BANK_BRANCH_LIST_PAGE;
		}
		BaseDTO baseDTO = httpService.post(Url + "/bankBranchMaster/deleteBankBranch", bankBranchMasterDTO);
		log.info("-deleted-BaseDto Object--  " + baseDTO);
		if (baseDTO != null) {
			ErrorMaster error = errorMap.getErrorMaster(baseDTO.getStatusCode());
			errorMap.notify(baseDTO.getStatusCode());
			if (error != null && (error.getMessageType().equals(ErrorMessageType.ERROR)
					|| error.getMessageType().equals(ErrorMessageType.WARN))) {
				return null;
			}
		}

		bankBranchMaster = null;
		lazyLoadBankBranchList();
		log.info("BankBranch deleteBankBranch ===============#End");
		return BANK_BRANCH_LIST_PAGE;
	}

	public String showBankBranchViewPage() {
		log.info("BankBranchBean>>>>>>>>> showBankBranchViewPage() ");
		if (action.equals("VIEW") && bankBranchMasterDTO == null) {
			action = null;
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		loadBankBranch();
		return BANK_BRANCH_VIEW_PAGE;
	}

	public String back() {
		bankBranchMasterDTO = null;
		return BANK_BRANCH_LIST_PAGE;
	}

	public String clear() {
		log.info("BankBranch clear===============#Start");
		bankBranchMasterDTO = null;
		addButtonFlag = true;
		lazyLoadBankBranchList();
		log.info("BankBranch clear===============#End");
		return BANK_BRANCH_LIST_PAGE;
	}

	private void loadStatusValues() {
		Object[] statusArray = BankBranchStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);

		}

		log.info("Load Status Values" + statusValues.get(1));
	}

	public void lazyLoadBankBranchList() {
		log.info("====== lazyLoadBankBranchList =======");
		bankBranchMasterlazyLoadList = new LazyDataModel<BankBranchMasterDTO>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public List<BankBranchMasterDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				log.info("====== load() Called =======");
				filterMap = filters;
				sortingOrder = sortOrder;
				sortingField = sortField;
				List<BankBranchMasterDTO> data = new ArrayList<BankBranchMasterDTO>();
				try {

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);
					ObjectMapper objectMapper = new ObjectMapper();
					String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
					log.info("=== jsonResponse ====" + jsonResponse);
					data = objectMapper.readValue(jsonResponse, new TypeReference<List<BankBranchMasterDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						log.info("=== DATA ===" + data);
						resultSize = baseDTO.getTotalRecords();
						log.info("=== resultSize ====" + resultSize);
					}

				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;
			}

			@Override
			public Object getRowKey(BankBranchMasterDTO response) {
				log.info("getRowKey is called.");
				return response != null ? response.getId() : null;
			}

			@Override
			public BankBranchMasterDTO getRowData(String rowKey) {
				log.info("getRowData is called.");
				@SuppressWarnings("unchecked")
				List<BankBranchMasterDTO> responseList = (List<BankBranchMasterDTO>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (BankBranchMasterDTO response : responseList) {
					if (response.getId().longValue() == value.longValue()) {
						return response;
					}
				}
				return null;
			}

		};
	}
	
	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts .onRowSelect ========>" + event);
		addButtonFlag = false;
		bankBranchMasterDTO = ((BankBranchMasterDTO) event.getObject());
		log.info("<===Ends .onRowSelect ========>");
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		log.info("BankBranchMasterBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}
		BankBranchMasterDTO request = getRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			baseDTO = httpService.post(Url + "/bankBranchMaster/search", request);
			return baseDTO;

		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("BankBranchMasterBean.getSearchData Method Completed");
		return baseDTO;
	}

	/**
	 * 
	 * @param first
	 * @param pageSize
	 * @param sortField
	 * @param sortOrder
	 * @param filters
	 * @return
	 * @throws ParseException
	 */
	private BankBranchMasterDTO getRequestObject(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BankBranchMasterDTO request = new BankBranchMasterDTO();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("bankName")) {
				request.setBankName(value);
			}

			if (entry.getKey().equals("branchName")) {
				log.info("branchName : " + value);
				request.setBranchName(value);
			}
			if (entry.getKey().equals("ifscCode")) {
				log.info("ifscCode : " + value);
				request.setIfscCode(value);
			}
			if (entry.getKey().equals("contactName")) {
				log.info("contactName : " + value);
				request.setContactName(value);
			}
			if (entry.getKey().equals("contactNumber")) {
				log.info("contactNumber : " + value);
				request.setContactNumber(value);
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
			if (entry.getKey().equals("activeStatus")) {
				log.info("active Status : " + value);
				request.setActiveStatus(Boolean.parseBoolean(value));
			}
		}

		return request;
	}
}
