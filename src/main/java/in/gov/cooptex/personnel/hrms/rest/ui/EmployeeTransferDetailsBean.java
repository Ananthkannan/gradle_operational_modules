package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeTransferDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AdditionalChargeStage;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmpTransferDetailsLog;
import in.gov.cooptex.core.model.EmpTransferDetailsNote;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeeTransferDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeTransferDetailsBean")
public class EmployeeTransferDetailsBean extends CommonBean {

	@Getter
	@Setter
	public Resource resource;

	@Autowired
	ErrorMap errorMap;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse, url;

	@Getter
	@Setter
	private String transferDocumentFileName, reJoinFileName;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeTransferDetails employeeTransfer = new EmployeeTransferDetails();

	@Getter
	@Setter
	List<EntityMaster> headRegionalOfficeList, headOrRegionalOfficeFrom, headOrRegionalOfficeTo;

	@Getter
	@Setter
	EmployeeTransferDTO selectedEmployeeTransfer;

	@Getter
	@Setter
	List<EmployeeTransferDTO> employeeTransferList;

	@Getter
	@Setter
	private List<EntityTypeMaster> entityTypeMasterList, entityTypeFromList, entityTypeTo;

	@Getter
	@Setter
	LazyDataModel<EmployeeTransferDTO> lazyEmployeeTransferModel;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean add, approve, delete, edit, buttonFlag, disableHooRo, entityVisible, transferVisible, showEntityPanel,
			deputationFlag;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList;

	@Getter
	@Setter
	String employeeId;

	@Getter
	@Setter
	String byType = "byEmployeeId", selected;

	@Getter
	@Setter
	EntityMaster headOrRegionFrom, entityFrom;

	@Getter
	@Setter
	EntityTypeMaster entityTypeFrom;

	@Getter
	@Setter
	List<EntityMaster> headOrRegionalOfficeList;
	@Getter
	@Setter
	List<EntityTypeMaster> entityTypeList;
	@Getter
	@Setter
	List<EntityMaster> entityMasterList, entityMasterFrom, entityMasterTo;

	@Getter
	@Setter
	List<SectionMaster> sectionMastersFrom, sectionMastersTo;

	@Getter
	@Setter
	SectionMaster fromSection, toSection;

	@Getter
	@Setter
	private UploadedFile transferDocument, reJoinDocument;
	@Getter
	@Setter
	private List<Department> departmentList;

	@Getter
	@Setter
	Department department, departmentTo;

	@Getter
	@Setter
	SectionMaster section;

	@Getter
	@Setter
	EmployeeTransferDTO employeeTransferObj = new EmployeeTransferDTO();

	@Getter
	@Setter
	private String note, transferComment, approveRemark, rejeckRemark;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	EmpTransferDetailsNote empTransferDetailsNote = new EmpTransferDetailsNote();

	@Getter
	@Setter
	EmpTransferDetailsLog empTransferDetailsLog = new EmpTransferDetailsLog();

	@Getter
	@Setter
	private UserMaster userMaster;

	@Getter
	@Setter
	private Boolean finalApproval, transferAvailability, previousApproval, otherRegionflag, joinedonflag;

	@Getter
	@Setter
	private Date availabilityFromDate, joiningDate, joinedOn, relivingDate;

	@Getter
	@Setter
	private Integer availabilityPosition;

	@Setter
	@Getter
	private StreamedContent file, reJoinFile;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean rejectButtonFlag = false;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	String pfNumber;

	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	List<EmployeeTransferDTO> viewNoteEmployeeDetails = new ArrayList<>();

	public String showPage(Resource view) {
		log.info("Page name is {}.xhtml", view);

		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		if (view.equals(Resource.ADD)) {
			resource = Resource.ADD;
			// entityMasterList=new ArrayList<EntityMaster>();
			employeeMaster = new EmployeeMaster();
			employeeTransfer = new EmployeeTransferDetails();
			employeeId = null;
			toSection = new SectionMaster();
			loadMasterData();
			transferDocumentFileName = "";
			byType = "byEmployeeId";
			selected = "byEmployee";
			entityTypeFrom = new EntityTypeMaster();
			headOrRegionFrom = new EntityMaster();
			department = new Department();
			note = null;
			finalApproval = null;
			userMaster = null;
			pfNumber = null;
			return "/pages/admin/transferRequest/createTransferRequestAdmin.xhtml?faces-redirect=true;";

		} else if (view.equals(Resource.JOINING) || view.toString().equalsIgnoreCase("RELIVE")) {
			if (view.toString().equalsIgnoreCase("JOINING")) {
				resource = Resource.JOINING;
			} else {
				resource = Resource.RELIVE;
			}
			otherRegionflag = false;
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			viewTransferDetail();
			return "/pages/admin/transferRequest/viewJoiningAdmin.xhtml?faces-redirect=true;";

		} else if (view.equals(Resource.EDIT)) {
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			resource = Resource.EDIT;
			loadMasterData();
			viewTransferDetail();

			return "/pages/admin/transferRequest/createTransferRequestAdmin.xhtml?faces-redirect=true;";
		} else if (view.equals(Resource.VIEW)) {
			resource = Resource.VIEW;
			otherRegionflag = false;
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			viewTransferDetail();
			return "/pages/admin/transferRequest/viewTransferRequestAdmin.xhtml?faces-redirect=true;";
		} else if (view.equals(Resource.APPROVE)) {
			resource = Resource.APPROVE;
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			viewTransferDetail();
			return "/pages/admin/transferRequest/transferRequestAdminApproval.xhtml?faces-redirect=true;";
		} else if (view.equals(Resource.REJOINING)) {
			resource = Resource.REJOINING;
			otherRegionflag = false;
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			viewTransferDetail();
			return "/pages/admin/transferRequest/viewRejoiningAdmin.xhtml?faces-redirect=true;";
		} else if (view.equals(Resource.DEL)) {
			resource = Resource.DEL;
			if (Objects.isNull(selectedEmployeeTransfer)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			} else if (selectedEmployeeTransfer.getStatus() != null
					&& selectedEmployeeTransfer.getStatus().equalsIgnoreCase(ApprovalStage.APPROVED)) {
				errorMap.notify(ErrorDescription.APPROVED_RECORD_CANOT_DELETE.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}
			return "";
		} else {
			loadLazyModel();
			headRegionalOfficeList = commonDataService.loadHeadAndRegionalOffice();
			entityTypeMasterList = commonDataService.getAllEntityType();
			selectedEmployeeTransfer = new EmployeeTransferDTO();
			add = true;
			delete = true;
			approve = true;
			edit = true;
			joinedOn = null;
			deputationFlag = false;
			return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
		}
	}

	public boolean viewTransferDetail() {
		try {
			if (Objects.isNull(selectedEmployeeTransfer)) {
				log.info(" Selected employee transfer id is null : " + selectedEmployeeTransfer);
				return false;
			}

			employeeTransfer = selectedRecordId();
			
			if (employeeTransfer != null) {
				setNote(employeeTransfer.getNote());
				employeeMaster = employeeTransfer.getEmpMaster();

				if (resource.equals(Resource.VIEW)) {
					if (employeeTransfer.getDocumentPath() != null) {
						File file = new File(employeeTransfer.getDocumentPath());
						transferDocumentFileName = file.getName();
					}
					forwardToUsersList = commonDataService
							.loadForwardToUsersByFeature(AppFeatureEnum.EMP_TRANSFER_DETAILS.name());
					getTransferDetailsNoteByTransferId();
					if (empTransferDetailsNote.getUserMaster() != null) {
						if (empTransferDetailsNote.getUserMaster().getId().equals(loginBean.getUserMaster().getId())) {
							approveButtonFlag = true;
							rejectButtonFlag = true;
						} else {
							approveButtonFlag = false;
							rejectButtonFlag = false;
						}
					}
					if (employeeTransfer.getTransferType().equalsIgnoreCase("InterRegion")
							&& empTransferDetailsNote.getFinalApproval()) {
						otherRegionflag = true;
					}
				}
				if (resource.equals(Resource.JOINING) || resource.equals(Resource.RELIVE)) {
					if (employeeTransfer.getDocumentPath() != null) {
						File file = new File(employeeTransfer.getDocumentPath());
						transferDocumentFileName = file.getName();
					}
					forwardToUsersList = commonDataService
							.loadForwardToUsersByFeature(AppFeatureEnum.EMP_TRANSFER_DETAILS.name());
					getTransferDetailsNoteByTransferId();
					if (empTransferDetailsNote.getUserMaster() != null) {
						if (empTransferDetailsNote.getUserMaster().getId().equals(loginBean.getUserMaster().getId())) {
							approveButtonFlag = true;
							rejectButtonFlag = true;
						} else {
							approveButtonFlag = false;
							rejectButtonFlag = false;
						}
					}
					if (employeeTransfer.getTransferType().equalsIgnoreCase("InterRegion")
							&& empTransferDetailsNote.getFinalApproval()) {
						otherRegionflag = true;
					}
				}
				if (resource.equals(Resource.APPROVE)) {
					forwardToUsersList = commonDataService
							.loadForwardToUsersByFeature(AppFeatureEnum.EMP_TRANSFER_DETAILS.name());
					getTransferDetailsNoteByTransferId();
				}

				if (resource.equals(Resource.EDIT)) {
					headOrRegionFrom = new EntityMaster();
					section = new SectionMaster();
					selected = "byEntity";
					headOrRegionFrom = employeeTransfer.getTransferFrom();
					
					//headOrRegionalOfficeTo = 
					// headOrRegionFrom = employeeTransfer.getHeadRegOffice();
					section = employeeTransfer.getSectionFrom();
					if (employeeTransfer.getSectionTo() != null) {
						departmentTo = employeeTransfer.getSectionTo().getDepartment();
					}
					if (employeeTransfer.getSectionFrom() != null) {
						department = employeeTransfer.getSectionFrom().getDepartment();
					}
					onEntityDepartmentChange();
					onChangeSection();
					getHeadAndRegionalOfficeByTransferType();
					Long hoRoId = employeeTransfer.getHeadRegOffice().getId();
					Long entityTypeId = employeeTransfer.getEntityTo().getId();
					entityMasterTo = commonDataService.loadEntityByregionOrentityTypeId(hoRoId, entityTypeId);
					getTransferDetailsNoteByTransferId();
				}
			}
		} catch (Exception e) {
			log.error(":: Exception while get employee transfer details :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return true;
	}

	private EmployeeTransferDetails selectedRecordId() {
		log.info(":: Selected leave request ::");
		EmployeeTransferDetails employeeTransfer = null;

		try {
			if (Objects.isNull(selectedEmployeeTransfer.getId())) {
				log.info(" Selected emplyee transfer id is null : " + selectedEmployeeTransfer.getId());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_ID_ISNULL.getErrorCode());
				return null;
			}

			log.info(":: Selected emplyee transfer URI is ::" + Resource.GET.URI + selectedEmployeeTransfer.getId());
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/get/" + selectedEmployeeTransfer.getId()
					+ "/" + (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO baseDTO = httpService.get(url);
			if (Objects.isNull(baseDTO)) {
				log.info(":: Selected emplyee transfer BaseDTO response is null ::" + baseDTO);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				log.info(":: Selected emplyee transfer success response is ::" + baseDTO.getStatusCode());
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeTransfer = mapper.readValue(jsonResponse, EmployeeTransferDetails.class);

				String jsonResponses = mapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewNoteEmployeeDetails = mapper.readValue(jsonResponses,
						new TypeReference<List<EmployeeTransferDTO>>() {
						});

			}
		} catch (Exception e) {
			log.error(":: Exception while getting selected emplyee transfer ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		return employeeTransfer;
	}

	public enum Resource {

		CRETAE(AppUtil.getPortalServerURL() + "/employee/transfer/create"),
		GET(AppUtil.getPortalServerURL() + "/employee/transfer/get/"),
		UPDATE(AppUtil.getPortalServerURL() + "/employee/transfer/update"),
		DELETE(AppUtil.getPortalServerURL() + "/employee/transfer/delete/"),
		LAZY_LIST(AppUtil.getPortalServerURL() + "/employee/transfer/lazylist"),
		APPROVAL(AppUtil.getPortalServerURL() + "/employee/transfer/approvalstatus"),
		EMPURL(AppUtil.getPortalServerURL() + "/employee"),

		APPROVE("APPROVE"), REJECT("REJECT"), ADD("Create"), EDIT("Edit"), VIEW("View"), DEL("Delete"), LIST("List"),
		RELIVE("RELIVE"), JOINING("JOINING"), REJOINING("REJOINING");

		private final String URI;

		Resource(final String URI) {
			this.URI = URI;
		}

		@Override
		public String toString() {
			return URI;
		}
	}

	public void loadLazyModel() {

		log.info(":: Lazy load employee transfer details. :: ");

		lazyEmployeeTransferModel = new LazyDataModel<EmployeeTransferDTO>() {

			private static final long serialVersionUID = 7699834864652177050L;

			@Override
			public List<EmployeeTransferDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);

					log.info(":: Lazy load employee transfer pagination request. :: " + paginationRequest);

					log.info(":: Lazy load employee transfer pagination URL :: " + Resource.LAZY_LIST.URI);

					BaseDTO lazyDTO = httpService.post(Resource.LAZY_LIST.URI, paginationRequest);

					if (lazyDTO != null && lazyDTO.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(lazyDTO.getResponseContent());
						employeeTransferList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeTransferDTO>>() {
								});
						this.setRowCount(lazyDTO.getTotalRecords());
						totalRecords = lazyDTO.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error(":: Exception while  Lazy load employee transfer pagination :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return employeeTransferList;
			}

			@Override
			public Object getRowKey(EmployeeTransferDTO res) {
				log.info(":: Selected employee transfer id is :: " + res.getId());
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeTransferDTO getRowData(String rowKey) {
				log.info(":: Employee transfer selected row key is :: " + rowKey);
				for (EmployeeTransferDTO transfer : employeeTransferList) {
					if (transfer.getId().equals(Long.valueOf(rowKey))) {
						selectedEmployeeTransfer = transfer;
						return transfer;
					}
				}
				return null;
			}
		};
	}

	public String addCheckAvailability() {
		log.info(":: Employee transfer approval status :: ");
		try {
			setCheckTransferAvailability();
			if (Objects.isNull(employeeTransfer)) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("REQUEST_FINAL_APPROVED")) {
				if (StringUtils.isEmpty(userMaster.getEmpName()) && userMaster.getEmpName() == null) {
					Util.addWarn("Forward To is empty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("AVAILABILITY_FINAL_APPROVED")) {
				if (StringUtils.isEmpty(userMaster.getEmpName()) && userMaster.getEmpName() == null) {
					Util.addWarn("Forward To is empty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			if (availabilityPosition.equals(0)) {
				Util.addWarn("Please enter No. of Availability Position Value is greater than zero.");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			log.info(":: Employee transfer approval by id ::" + employeeTransfer.getId());
			log.info(":: Employee transfer approval by id URL ::" + Resource.APPROVAL.URI);

			if (employeeTransfer != null && employeeTransfer.getTransferAvailability())
				empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_SUBMITTED);
			else
				empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_REJECTED);

			employeeTransfer.setEmpTransferDetailslog(empTransferDetailsLog);
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/checkavilability";
			BaseDTO baseDto = httpService.put(url, employeeTransfer);
			log.info(":: Employee transfer approve by id BaseDTO Response ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Employee transfer approve by id BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			if (baseDto.getStatusCode() == 0) {
				log.info(":: Employee transfer record check Availability approved successfully ::");
				errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_APPROVED.getErrorCode());
			}
			loadLazyModel();
			add = true;
			deputationFlag = false;
		} catch (Exception e) {
			log.error(":: Exception while  check Availability approved employee transfer :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		selectedEmployeeTransfer = new EmployeeTransferDTO();
		// return
		// "/pages/personnelHR/employeeProfile/listTransferRequestAdmin.xhtml?faces-redirect=true;";
		return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void onRowSelect(SelectEvent event) {
		log.info(":: Employee transfer onRowSelect :: ");
		selectedEmployeeTransfer = ((EmployeeTransferDTO) event.getObject());
		try {
			getTransferDetailsNoteByTransferId();
			Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();

			if (selectedEmployeeTransfer != null && selectedEmployeeTransfer.getStatus() != null) {
				if (selectedEmployeeTransfer.getStatus()
						.equalsIgnoreCase(AdditionalChargeStage.TRANSFER_REQUEST_SUBMITTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.AVAILABILITY_SUBMITTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.RELIVING_SUBMITTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.JOINING_SUBMITTED)) {
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						edit = true;
						approve = false;
					} else {
						edit = false;
						add = false;
						approve = true;
					}
				} else if (selectedEmployeeTransfer.getStatus()
						.equalsIgnoreCase(AdditionalChargeStage.TRANSFER_REQUEST_APPROVED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.AVAILABILITY_APPROVED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.RELIVING_APPROVED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.JOINING_APPROVED)) {
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						approve = false;
						edit = false;
					} else {
						add = false;
						approve = false;
						edit = false;
					}
				} else if (selectedEmployeeTransfer.getStatus()
						.equalsIgnoreCase(AdditionalChargeStage.TRANSFER_REJECTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.AVAILABILITY_REJECTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.RELIVING_REJECTED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.JOINING_REJECTED)) {
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						edit = true;
						approve = false;
					} else {
						add = false;
						approve = false;
						edit = false;
					}
				} else if (selectedEmployeeTransfer.getStatus()
						.equalsIgnoreCase(AdditionalChargeStage.RELIVING_FINAL_APPROVED)) {
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						approve = false;
						edit = false;
					} else {
						add = false;
						approve = false;
						edit = false;
					}
				}
				joinedonflag = false;
				/*
				 * if((selectedEmployeeTransfer.getStatus().equalsIgnoreCase("JOINING_APPROVED")
				 * ||
				 * selectedEmployeeTransfer.getStatus().equalsIgnoreCase("JOINING_SUBMITTED"))
				 * &&
				 * empTransferDetailsNote.getUserMaster().getId().equals(loginBean.getUserMaster
				 * ().getId()) && empTransferDetailsNote.getFinalApproval()) {
				 * joinedonflag=true; }
				 */
				// As Discussed with BA rejoin button should enable in JOINING_FINAL_APPROVED
				// stage
				if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase(AdditionalChargeStage.JOINING_FINAL_APPROVED)
						|| selectedEmployeeTransfer.getStatus()
								.equalsIgnoreCase(AdditionalChargeStage.JOINING_ON_FINAL_APPROVED)) {
					if ((selectedEmployeeTransfer.getStatus()
							.equalsIgnoreCase(AdditionalChargeStage.JOINING_FINAL_APPROVED)
							|| selectedEmployeeTransfer.getStatus()
									.equalsIgnoreCase(AdditionalChargeStage.JOINING_ON_FINAL_APPROVED))
							&& empTransferDetailsNote.getUserMaster().getId().equals(loginBean.getUserMaster().getId())
							&& empTransferDetailsNote.getFinalApproval()) {
						joinedonflag = true;
						if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
								&& selectedEmployeeTransfer.getCreatedBy().getId() != null
								&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
							add = false;
							approve = false;
							edit = false;
						} else {
							add = false;
							approve = false;
							edit = false;
						}
					} else {
						if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
								&& selectedEmployeeTransfer.getCreatedBy().getId() != null
								&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
							add = false;
							approve = false;
							edit = false;
						} else {
							add = false;
							approve = false;
							edit = false;
						}
					}
				}
				deputationFlag = false;
				if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("AVAILABILITY_FINAL_APPROVED")) {
					deputationFlag = true;
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						approve = false;
						edit = false;
					} else {
						add = false;
						approve = false;
						edit = false;
					}
				} else if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("REQUEST_FINAL_APPROVED")
						&& selectedEmployeeTransfer.getCategory().equalsIgnoreCase("Deputation")) {
					deputationFlag = true;
					if (loginUserId != null && selectedEmployeeTransfer.getCreatedBy() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId() != null
							&& selectedEmployeeTransfer.getCreatedBy().getId().equals(loginUserId)) {
						add = false;
						approve = false;
						edit = false;
					} else {
						add = false;
						approve = false;
						edit = false;
					}
				}
			}
		} catch (Exception ex) {
			log.error(":: error in  onRowSelect Method:: ", ex);
		}
	}

	public String deleteRecord() {
		log.info(":: Employee transfer delete record by id ::");
		try {
			if (selectedEmployeeTransfer == null) {
				log.info(":: Employee transfer id not found ::");
				errorMap.notify(ErrorDescription.SELECT_RECORD.getErrorCode());
				return null;
			}
			log.info(":: Employee transfer selected record id ::" + selectedEmployeeTransfer.getId());
			log.info(":: Employee transfer delete by id URL ::" + Resource.DELETE.URI
					+ selectedEmployeeTransfer.getId());
			BaseDTO baseDto = httpService.delete(Resource.DELETE.URI + selectedEmployeeTransfer.getId());
			log.info(":: Employee transfer delete by id BaseDTO Response ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Employee transfer delete by id BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			if (baseDto.getStatusCode() == 0) {
				log.info(":: Employee transfer record deleted successfully ::");
				errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_DELETED.getErrorCode());
			} else {
				log.info(":: Employee transfer failed to delete ::");
				return null;
			}
		} catch (Exception e) {
			log.error(":: Exception while delete employee transfer by id ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		add = true;
		// return
		// "/pages/personnelHR/employeeProfile/listTransferRequestAdmin.xhtml?faces-redirect=true;";
		return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void loadMasterData() {
		log.info("loadMasterData Method called ===========>");

		try {
			// headOrRegionalOfficeTo = commonDataService.loadHeadAndRegionalOffice();
			entityTypeTo = commonDataService.getActiveEntityType();

			sectionMastersTo = new ArrayList<>();
			sectionMastersTo = getAllSection();
			headOrRegionalOfficeFrom = commonDataService.loadHeadAndRegionalOffice();
			// headOrRegionalOfficeTo = commonDataService.loadHeadAndRegionalOffice();

			// entityTypeFromList = commonDataService.getEntityTypeByStatus();

			url = AppUtil.getPortalServerURL() + "/loanandadvance/loadEntityTypeMaster";
			BaseDTO response = httpService.get(url);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				entityTypeFromList = mapper.readValue(jsonResponse, new TypeReference<List<EntityTypeMaster>>() {
				});

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

			sectionMastersTo = getAllSection();
			departmentList = commonDataService.getDepartment();
			forwardToUsersList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.EMP_TRANSFER_DETAILS.name());
			// createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();
		} catch (Exception e) {
			log.info("loadMasterData Method Exception ===========>", e);
		}
		log.info("loadMasterData Method End ===========>");
	}

	public void onHeadOfficeChangeFrom() {
		log.info("onHeadOfficeChange called ::::::::::::" + headOrRegionFrom);
		try {
			if (Objects.isNull(headOrRegionFrom)) {
				employeeTransfer.setEntityFrom(null);
				employeeTransfer.setTransferFrom(null);
				department = new Department();
			}

			if (headOrRegionFrom != null && headOrRegionFrom.getCode() == 847367) {
				showEntityPanel = true;
				entityTypeFrom = new EntityTypeMaster();
				entityFrom = new EntityMaster();
			} else {
				showEntityPanel = false;

				if (headOrRegionFrom != null && employeeTransfer.getEntityFrom() != null) {
					onchangeRegionAndEntityType();
				}
			}
		} catch (Exception e) {
			log.info("error found in onHeadOfficeChange::::::::::: ", e);
		}
	}

	public void onEntityChange() {
		entityFrom = entityFrom;
		// employeeTransfer.setTransferFrom(entityFrom);
	}

	public void onEmployeeChange() {
		employeeMaster = employeeMaster;
		employeeTransfer.setEmployeeMaster(employeeMaster);
		log.info("Employee Name is :" + employeeTransfer.getEmployeeMaster().getFirstName());
	}

	public void onchangeDepartment() {
		BaseDTO baseDto = new BaseDTO();
		log.info("onchangeDepartment called..");
		Long entityId = null, departmentId = null;
		try {

			employeeList = new ArrayList<>();

			if (!Objects.isNull(headOrRegionFrom) && headOrRegionFrom.getCode() == 847367) {
				entityId = headOrRegionFrom.getEntityTypeMaster().getId();
				log.info("Employee Entity from id is : " + entityId + " Name is "
						+ headOrRegionFrom.getEntityTypeMaster().getEntityName());
			}
			if (!Objects.isNull(employeeTransfer.getEntityFrom())
					&& !Objects.isNull(employeeTransfer.getEntityFrom().getId())) {
				entityId = employeeTransfer.getEntityFrom().getId();
				log.info("Employee Entity from id is : " + entityId + " Name is "
						+ employeeTransfer.getEntityFrom().getEntityName());
			}

			if (Objects.isNull(department.getId())) {
				log.info("Selected department is :" + department);
			} else {
				departmentId = department.getId();
				log.info("Selected department is :" + department);
			}

			String resource = AppUtil.getPortalServerURL() + "/leaverequest/employeelist/" + entityId + "/"
					+ departmentId;
			log.info("onchangeDepartment Request URL ::::::::::" + resource);
			baseDto = httpService.get(resource);
			if (Objects.isNull(baseDto)) {
				log.info("Employee baseDTO response is  :" + baseDto);
			}
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
			});
			if (Objects.isNull(employeeList) || employeeList.isEmpty()) {
				log.info(":: Employee list size is :" + employeeList.size());
				employeeList = new ArrayList<>();
			} else {
				log.info(":: Employee list size is :" + employeeList.size());
				employeeTransfer.setEmployeeMaster(employeeMaster);
			}

			/*
			 * if (entityFrom != null && entityFrom != null && department != null &&
			 * department.getId() != null) {
			 * 
			 * 
			 * if (headOrRegionFrom != null && headOrRegionFrom.getCode() == 847367)
			 * entityId = headOrRegionFrom.getEntityTypeMaster().getId(); else // entityId =
			 * entityFrom.getEntityTypeMaster().getId(); entityId =
			 * employeeTransfer.getEntityFrom().getId(); Long departmentId =
			 * department.getId(); log.info(":: Entity ::::" + entityId +
			 * ":::: department Id ::::" + department.getId());
			 * 
			 * String resource = AppUtil.getPortalServerURL() + "/leaverequest" +
			 * "/employeelist/" + entityId + "/" + departmentId;
			 * log.info("onchangeDepartment Request URL ::::::::::" + resource); baseDto =
			 * httpService.get(resource); jsonResponse =
			 * mapper.writeValueAsString(baseDto.getResponseContents()); employeeList =
			 * mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {});
			 * log.info(":: Employee list size is :" + employeeList.size()); }
			 */
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}
	}

	public void onchangeRegionAndEntityType() {
		log.info("onchangeEntityType called..");
		try {
			if (byType.equals("byEmployeeId")) {
				prepareEntity();
			} else {
				prepareEntity();
				if (headOrRegionFrom != null && headOrRegionFrom.getCode() == 847367) {
					onchangeDepartment();
				} else {
					if (headOrRegionFrom != null && headOrRegionFrom.getId() != null
							&& employeeTransfer.getEntityFrom() != null
							&& employeeTransfer.getEntityFrom().getId() != null) {
						Long hoRoId = headOrRegionFrom.getId();
						Long entityTypeId = employeeTransfer.getEntityFrom().getId();
						entityMasterList = commonDataService.loadActiveEntityByregionOrentityTypeId(hoRoId,
								entityTypeId);
						employeeTransfer.setEmployeeMaster(employeeMaster);
					}
				}
			}
		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}
	}

	public void prepareEntity() {
		if (employeeTransfer != null && employeeTransfer.getHeadRegOffice() != null
				&& employeeTransfer.getHeadRegOffice().getCode() != null
				&& employeeTransfer.getHeadRegOffice().getCode() == 847367) {
			employeeTransfer.setHeadRegOffice(employeeTransfer.getHeadRegOffice());
			buttonFlag = true;
			employeeTransfer.setEntityTo(null);
		}
		if (employeeTransfer != null && employeeTransfer.getHeadRegOffice() != null
				&& employeeTransfer.getHeadRegOffice().getId() != null && employeeTransfer.getEntityTo() != null
				&& employeeTransfer.getEntityTo().getId() != null) {
			buttonFlag = false;
			Long hoRoId = employeeTransfer.getHeadRegOffice().getId();
			Long entityTypeId = employeeTransfer.getEntityTo().getId();
			entityMasterTo = commonDataService.loadEntityByregionOrentityTypeId(hoRoId, entityTypeId);
		}
	}

	public List<SectionMaster> getAllSection() {
		sectionMastersFrom = new ArrayList<>();
		BaseDTO baseDto = new BaseDTO();
		try {
			url = AppUtil.getPortalServerURL() + "/sectionmaster/getAll";
			log.info("getall section request url :::" + url);
			baseDto = httpService.get(url);
			log.info("getall section response object:::" + baseDto);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			sectionMastersFrom = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
			});

		} catch (Exception e) {
			log.info("found errror in getall section = :::", e);
		}
		return sectionMastersFrom;
	}

	public String createTransferRequest() {
		log.info("createTransferRequest start .... ");
		BaseDTO baseDto = new BaseDTO();

		String requestURL = AppUtil.getPortalServerURL() + "/employee/transfer/create";
		String updateURL = AppUtil.getPortalServerURL() + "/employee/transfer/update";
		try {

			if (StringUtils.isEmpty(note)) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				return null;
			}
			if (employeeTransfer != null && employeeMaster != null) {

				if (Objects.isNull(employeeTransfer.getId())) {
					log.info("Create Transfer Request URL :" + Resource.CRETAE.URI);
					employeeTransfer.setEmployeeMaster(employeeMaster);
					employeeTransfer.setEmpId(employeeMaster.getId());
					if (userMaster != null) {
						empTransferDetailsNote.setUserMaster(userMaster);
					}
					empTransferDetailsNote.setFinalApproval(finalApproval);
					employeeTransfer.setEmpTransferDetailsNote(empTransferDetailsNote);
					if (selected.equalsIgnoreCase("byEmployee")) {
						employeeTransfer.setSectionFrom(employeeMaster.getPersonalInfoEmployment().getSectionMaster());
						if (employeeMaster.getPersonalInfoEmployment() != null
								&& employeeMaster.getPersonalInfoEmployment().getWorkLocation() != null
								&& "HEAD_OFFICE".equalsIgnoreCase(employeeMaster.getPersonalInfoEmployment()
										.getWorkLocation().getEntityTypeMaster().getEntityCode())) {
							employeeTransfer
									.setTransferFrom(employeeMaster.getPersonalInfoEmployment().getWorkLocation());
						}
					} else {
						employeeTransfer.setSectionFrom(section);

						if (headOrRegionFrom != null && "HEAD_OFFICE"
								.equalsIgnoreCase(headOrRegionFrom.getEntityTypeMaster().getEntityCode())) {
							employeeTransfer.setTransferFrom(headOrRegionFrom);
						}
					}

					employeeTransfer.setTransferReferenceDate(new Date());
					/*
					 * if(headOrRegionFrom!=null &&
					 * "HEAD_OFFICE".equalsIgnoreCase(headOrRegionFrom.getEntityTypeMaster().
					 * getEntityCode())) { employeeTransfer.setTransferFrom(headOrRegionFrom); }
					 */
					log.info("Transfer request url :::" + requestURL);
					baseDto = httpService.post(requestURL, employeeTransfer);
				} else {
					log.info("Update Transfer Request URL :" + Resource.UPDATE.URI);
					employeeTransfer.setEmployeeMaster(employeeMaster);
					employeeTransfer.setEmpId(employeeMaster.getId());
					empTransferDetailsNote.setUserMaster(userMaster);
					empTransferDetailsNote.setFinalApproval(finalApproval);
					employeeTransfer.setEmpTransferDetailsNote(empTransferDetailsNote);
					employeeTransfer.setSectionFrom(section);

					log.info("Transfer request url :::" + updateURL);
					baseDto = httpService.put(updateURL, employeeTransfer);
				}

				log.info("Transfer Request BaseDTO response:" + baseDto);

				if (baseDto.getStatusCode() == 0) {
					log.info("::Transfer Request  created successfully. ::" + baseDto);
					if (resource.equals(Resource.ADD)) {
						errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_CREATED.getErrorCode());
						cancel();
					} else {
						errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_UPDATED.getErrorCode());
						cancel();
					}
					selectedEmployeeTransfer = new EmployeeTransferDTO();
					loadLazyModel();
					add = true;
					// return
					// "/pages/personnelHR/employeeProfile/listTransferRequestAdmin.xhtml?faces-redirect=true";
					return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
				} else {
					log.info("::Transfer Request creation failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}

		} catch (Exception e) {
			log.info("::Transfer Request creation failed to save. ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	public void onchangeTranferType() {
		try {
			log.info("onchangeTranferType called .. " + employeeTransfer);

			if (employeeTransfer != null && employeeTransfer.getTransferType() != null
					&& employeeTransfer.getTransferType().equalsIgnoreCase("IntraRegion")
					&& employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice() != null) {
				if (employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice().getCode() == 847367) {
					employeeTransfer
							.setHeadRegOffice(employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice());
					employeeTransfer.setEntityTo(null);
					employeeTransfer.setTransferTo(null);

					transferVisible = true;
				} else {
					transferVisible = false;
					// headOrRegionalOfficeTo = commonDataService.loadHeadAndRegionalOffice();
					// employeeTransfer.setEntityTo(null);
					// employeeTransfer.setTransferTo(null);
					RequestContext.getCurrentInstance().update("transferForm:transferPanel");
				}

			}
		} catch (Exception e) {
			log.info("found error in onchangeTranferType ::::", e);
		}
	}

	public void searchEmployeeCode() {
		log.info(" Search Employee code ");
		try {
			if (Objects.isNull(employeeMaster.getEmpCode())) {
				log.info("Search Employee code is :" + employeeMaster.getEmpCode());
			}
			employeeMaster = getByEmployeeId(pfNumber);
			if (Objects.isNull(employeeMaster)) {
				log.info("Selected Employee is :" + employeeMaster);
				employeeMaster = new EmployeeMaster();
				errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
			}
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("found error while fetching employee code ::::", e);
		}
	}

	public String searchByEmployeeId() {
		log.info("search by employee ID start.." + employeeMaster.getEmpCode());
		try {
			if (employeeMaster.getEmpCode() != null) {
				employeeMaster = getByEmployeeId(employeeMaster.getEmpCode());

				if (Objects.isNull(employeeMaster)) {
					errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
					return null;
				}

				if (employeeMaster.getPersonalInfoEmployment() == null) {
					log.error("Not valid employee ..");
					errorMap.notify(ErrorDescription.EMP_TRANSFER_REQUEST_EMPLOYEE_NOT_FOUND.getErrorCode());
					return null;
				}

				if (employeeMaster.getId() != null)
					employeeTransfer.setEmployeeMaster(employeeMaster);

				if (employeeMaster != null && employeeMaster.getPersonalInfoEmployment() != null) {

					if (employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice() != null
							&& employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice().getCode() != null
							&& employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice()
									.getCode() == 847367) {
						employeeMaster.getPersonalInfoEmployment().setHeadAndRegionOffice(
								employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice());
						employeeMaster.getPersonalInfoEmployment().setEntityType(null);
						employeeMaster.getPersonalInfoEmployment().setWorkLocation(null);

					} else {
						if (employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice() == null) {
							errorMap.notify(
									ErrorDescription.EMP_TRANSFER_REQUEST_REGION_OFFICE_NOT_FOUND.getErrorCode());
						}

						if (employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice() != null) {
							employeeTransfer.setHeadRegOffice(
									employeeMaster.getPersonalInfoEmployment().getHeadAndRegionOffice());
						}
						if (employeeMaster.getPersonalInfoEmployment().getEntityType() != null) {
							employeeTransfer.setEntityFrom(employeeMaster.getPersonalInfoEmployment().getEntityType());
						}
						if (employeeMaster.getPersonalInfoEmployment().getWorkLocation() != null) {
							employeeTransfer
									.setTransferFrom(employeeMaster.getPersonalInfoEmployment().getWorkLocation());

							sectionMastersFrom = getAllSection();

							sectionMastersFrom.forEach(section -> {
								if (employeeMaster.getPersonalInfoEmployment().getWorkLocation().getId() == section
										.getId()) {
									employeeTransfer.setSectionFrom(section);
								}

							});
						}

					}

				}
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("found error while fetching employee by id ::::", e);
		}
		return null;
	}

	public void clearField() {
		log.info("clear employee Transfer called:::");
		employeeMaster = new EmployeeMaster();
		employeeTransfer = new EmployeeTransferDetails();
		employeeId = "";
		pfNumber = null;
	}

	public void onChangeEmployee() {
		log.info("change employee type:::" + byType);
		RequestContext.getCurrentInstance().update("transferForm:transferPanel");
	}

	public void onSelected() {
		if (selected.equals("byEmployee")) {
			employeeTransfer.setCategory(null);
			employeeTransfer.setTransferType(null);
			employeeTransfer.setHeadRegOffice(null);
			employeeTransfer.setEntityTo(null);
			employeeTransfer.setTransferTo(null);
			log.info("change employee type:::" + selected);
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		}
		if (selected.equals("byEntity")) {
			employeeTransfer.setCategory(null);
			employeeTransfer.setTransferType(null);
			employeeTransfer.setHeadRegOffice(null);
			employeeTransfer.setEntityTo(null);
			employeeTransfer.setTransferTo(null);
			log.info("change employee type:::" + selected);
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		}
	}

	public EmployeeMaster getByEmployeeId(String employeeID) {
		log.info("getByEmployeeId start ==>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/employee/getEmp/employeepfNo/" + pfNumber;
			log.info("Employee Get By Id  URL: - " + url);
			baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);

			/*
			 * if (baseDTO == null || baseDTO.getResponseContent() == null) {
			 * log.error("Base DTO Returned Null Value");
			 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode()); return null;
			 * }
			 */
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);

			return employeeMaster;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public void confirmStatus() {
		if (employeeTransfer == null) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return;
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmStatus').show();");
		}
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			transferDocument = event.getFile();
			String type = FilenameUtils.getExtension(transferDocument.getFileName());
			log.info(" File Type :: " + type);
			boolean validFileType = AppUtil.isValidFileType(type, new String[] { "png", "doc", "jpeg", "pdf", "PNG" });
			if (!validFileType) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				Util.addWarn("Upload only jpeg,png,pdf,doc files");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			long size = transferDocument.getSize();
			log.info("Employee transfer request document size is : " + size);
			size = (size / AppUtil.ONE_MB_IN_KB);
			log.info("file size---------------" + size);
			if (size > 2000) {
				Util.addWarn("Upload File Size Within 2 MB");
				RequestContext.getCurrentInstance().update("growls");
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_TRANSFER_REQUEST_UPLOAD_PATH");
			// File file = new File(uploadPath);
			// uploadPath = file.getAbsolutePath() + "/";
			// Path path = Paths.get(uploadPath);
			/*
			 * if (Files.notExists(path)) { log.info(" Path Doesn't exixts");
			 * Files.createDirectories(path); }
			 */
			// log.info(" Uploaded document oath is :" + uploadPath);
			transferDocumentFileName = transferDocument.getFileName();
			// String append = new Date().toGMTString() + transferDocumentFileName;
			String docPath = Util.fileUpload(uploadPath, transferDocument);
			employeeTransfer.setDocumentPath(docPath);
			errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_TRANSFER_DOCUMENT_UPLOADED.getErrorCode());
			// inputStream = transferDocument.getInputstream();
			/*
			 * log.info(" Path Doesn't exixts" + append); outputStream = new
			 * FileOutputStream(uploadPath + append); byte[] buffer = new byte[(int)
			 * transferDocument.getSize()]; int bytesRead = 0; while ((bytesRead =
			 * inputStream.read(buffer)) != -1) { outputStream.write(buffer, 0, bytesRead);
			 * } log.info(" upload Path is >>>" + uploadPath);
			 * log.info(" Leave Document is uploaded successfully"); if (outputStream !=
			 * null) { outputStream.close(); } if (inputStream != null) {
			 * inputStream.close(); }
			 */
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void searchEmployeeByCode() {
		try {
			if (Objects.isNull(employeeTransfer.getEmployeeMaster())
					&& Objects.isNull(employeeTransfer.getEmployeeMaster().getEmpCode())) {
				log.info("Employee code is :" + employeeTransfer.getEmployeeMaster().getEmpCode());
			}
			/*
			 * if (!StringUtils.isEmpty(pfNumber) && pfNumber!=null) {
			 * log.info("Employee code is :" + pfNumber);
			 */
			String employeeCode = employeeTransfer.getEmployeeMaster().getEmpCode();
			// String pfnumber =
			// employeeTransfer.getEmployeeMaster().getPersonalInfoEmployment().getPfNumber();

			log.info("Employee code is :" + employeeCode);
			String url = AppUtil.getPortalServerURL() + "/employee/getEmp/employeepfNo/" + pfNumber;
			log.info("Employee code url is :" + url);
			BaseDTO baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			if (Objects.isNull(employeeMaster)) {
				log.info("Selected Employee is :" + employeeMaster);
				employeeMaster = new EmployeeMaster();
				employeeTransfer.setEmployeeMaster(employeeMaster);
				errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
			} else {
				log.info("Selected Employee is :" + employeeMaster.getFirstName());
				employeeTransfer.setEmployeeMaster(employeeMaster);
				if (selected.equalsIgnoreCase("byEmployee")) {
					employeeTransfer.setEntityFrom(employeeMaster.getPersonalInfoEmployment().getEntityType());
					employeeTransfer.setTransferFrom(employeeMaster.getPersonalInfoEmployment().getWorkLocation());
					employeeTransfer.setSectionFrom(employeeMaster.getPersonalInfoEmployment().getSectionMaster());
				}
			}
			// }
		} catch (Exception e) {

		}
	}

	public void onTransferOfficeChange() {
		try {
			employeeTransfer.setEntityTo(null);
			employeeTransfer.setTransferTo(null);
			entityMasterTo = new ArrayList<>();
			if (Objects.isNull(employeeTransfer.getHeadRegOffice())) {
				transferVisible = true;
				employeeTransfer.setEntityTo(null);
				employeeTransfer.setTransferTo(null);
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else if (employeeTransfer.getHeadRegOffice() != null
					&& employeeTransfer.getHeadRegOffice().getEntityTypeMaster() != null
					&& employeeTransfer.getHeadRegOffice().getEntityTypeMaster().getEntityCode() != null
					&& employeeTransfer.getHeadRegOffice().getEntityTypeMaster().getEntityCode()
							.equals("HEAD_OFFICE")) {
				log.info("Selected Head office code is :"
						+ employeeTransfer.getHeadRegOffice().getEntityTypeMaster().getEntityCode());
				transferVisible = true;
				employeeTransfer.setEntityTo(null);
				employeeTransfer.setTransferTo(null);
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else {
				transferVisible = false;
				employeeTransfer.setEntityTo(null);
				employeeTransfer.setTransferTo(null);
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			}
		} catch (Exception e) {
			log.error("onTransferOfficeChange :: Exception ==> ", e);
		}
	}

	public void onEntityOfficeChange() {

		try {
			if (headOrRegionFrom.getId() != null) {
				headOrRegionFrom.setEntityTypeMaster(commonDataService.getEntityType(headOrRegionFrom.getId()));
			}
			if (Objects.isNull(headOrRegionFrom)) {
				entityVisible = true;
				employeeTransfer.setEntityFrom(null);
				employeeTransfer.setTransferFrom(null);
				department = new Department();
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else if (headOrRegionFrom.getEntityTypeMaster() != null
					&& headOrRegionFrom.getEntityTypeMaster().getEntityCode().equals("HEAD_OFFICE")) {
				log.info("Selected Head office code is :" + headOrRegionFrom.getEntityTypeMaster().getEntityCode());
				entityVisible = true;
				employeeTransfer.setEntityFrom(null);
				employeeTransfer.setTransferFrom(null);
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			} else {
				if (headOrRegionFrom.getEntityTypeMaster() != null) {

					url = AppUtil.getPortalServerURL() + "/loanandadvance/loadEntityTypeMaster";
					BaseDTO response = httpService.get(url);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						entityTypeFromList = mapper.readValue(jsonResponse,
								new TypeReference<List<EntityTypeMaster>>() {
								});

					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
					log.info("Selected Regional office code is :"
							+ headOrRegionFrom.getEntityTypeMaster().getEntityCode());
				}
				entityVisible = false;
				employeeTransfer.setEntityFrom(null);
				employeeTransfer.setTransferFrom(null);
				RequestContext.getCurrentInstance().update("transferForm:transferPanel");
			}
		} catch (Exception e) {

		}
	}

	public void onEntityTypeChange() {
		log.info(":: Entity type change ::");
		try {

			if (Objects.isNull(headOrRegionFrom) || Objects.isNull(employeeTransfer.getEntityFrom())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}

			if (Objects.isNull(headOrRegionFrom.getId()) && Objects.isNull(employeeTransfer.getEntityFrom().getId())) {
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
				return;
			}
			Long entityId = headOrRegionFrom.getId();
			Long entiyTypeId = employeeTransfer.getEntityFrom().getId();
			String resource = AppUtil.getPortalServerURL() + "/entitymaster/getall/entity/" + entityId + "/"
					+ entiyTypeId;
			log.info("URL request for entity :: " + resource);
			BaseDTO baseDto = httpService.get(resource);
			jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
			entityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
			});
			if (entityMasterList != null) {
				log.info("Entity location list size is : " + entityMasterList.size());
			}
			RequestContext.getCurrentInstance().update("transferForm:transferPanel");
		} catch (Exception e) {
			log.error(":: Exception while Entity type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public void onEntityDepartmentChange() {
		BaseDTO baseDto = new BaseDTO();
		log.info("onchangeDepartment called..");
		try {
			if (department.getId() != null) {

				String resource = AppUtil.getPortalServerURL() + "/sectionmaster/getsectionbydepartmentid/"
						+ department.getId();
				log.info("onchangeDepartment Request URL ::::::::::" + resource);
				baseDto = httpService.get(resource);
				if (Objects.isNull(baseDto)) {
					log.info("Section baseDTO response is  :" + baseDto);
				}
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
				if (Objects.isNull(sectionMasterList) || sectionMasterList.isEmpty()) {
					log.info(":: sectionMaster list size is :" + sectionMasterList.size());
					sectionMasterList = new ArrayList<>();
				} else {
					log.info(":: sectionMaster list size is :" + sectionMasterList.size());
				}
			} else if (departmentTo != null) {
				String resource = AppUtil.getPortalServerURL() + "/sectionmaster/getsectionbydepartmentid/"
						+ departmentTo.getId();
				log.info("onchangeDepartment Request URL ::::::::::" + resource);
				baseDto = httpService.get(resource);
				if (Objects.isNull(baseDto)) {
					log.info("Section baseDTO response is  :" + baseDto);
				}
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
				sectionMastersTo = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
			}
		} catch (

		Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}
	}

	public void onChangeSection() {
		log.info("onchangeDepartment called..");
		Long entityId = null, departmentId = null, sectionID = null;
		try {
			employeeList = new ArrayList<EmployeeMaster>();
			if (!Objects.isNull(headOrRegionFrom) && headOrRegionFrom.getName().equals("HEAD OFFICE")) {
				entityId = headOrRegionFrom.getEntityTypeMaster().getId();
				log.info("Employee Entity from id is : " + entityId + " Name is "
						+ headOrRegionFrom.getEntityTypeMaster().getEntityName());
			}
			if (!Objects.isNull(employeeTransfer.getTransferFrom())
					&& !Objects.isNull(employeeTransfer.getTransferFrom().getId())) {
				if (employeeTransfer.getTransferFrom().getEntityTypeMaster() != null
						&& employeeTransfer.getTransferFrom().getEntityTypeMaster().getId() != null) {
					entityId = employeeTransfer.getTransferFrom().getEntityTypeMaster().getId();
					log.info("Employee Entity from id is : " + entityId + " Name is "
							+ employeeTransfer.getEntityFrom().getEntityName());
				}
			}

			if (Objects.isNull(department.getId())) {
				log.info("Selected department is :" + department);
			} else {
				departmentId = department.getId();
				log.info("Selected department is :" + department);
			}
			if (Objects.isNull(section.getId())) {
				log.info("Selected section is :" + section);
			} else {
				sectionID = section.getId();
				log.info("Selected section is :" + section);
			}
			if (entityId != null && departmentId != null && sectionID != null) {
				employeeList = commonDataService.getEmployeeByEntityDeptSectionId(entityId, departmentId, sectionID);
			}

			if (Objects.isNull(employeeList) || employeeList.isEmpty()) {
				log.info(":: Employee list size is :" + employeeList.size());
				employeeList = new ArrayList<>();
			} else {
				log.info(":: Employee list size is :" + employeeList.size());
			}
		} catch (Exception e) {
			log.info("error found in onchangeDepartment::::::", e);
		}
	}

	public void getHeadAndRegionalOfficeByTransferType() {
		log.info("getHeadAndRegionalOfficeByTransferType Method start=====>");
		try {
			headOrRegionalOfficeTo = new ArrayList<EntityMaster>();
			if (selected.equalsIgnoreCase("byEmployee")) {
				if (employeeTransfer != null && employeeTransfer.getTransferType() != null
						&& !employeeTransfer.getTransferType().isEmpty() && employeeTransfer.getEmployeeMaster() != null
						&& employeeTransfer.getEmployeeMaster().getPersonalInfoEmployment() != null && employeeTransfer
								.getEmployeeMaster().getPersonalInfoEmployment().getHeadAndRegionOffice() != null) {
					if (employeeTransfer.getTransferType().equalsIgnoreCase("IntraRegion")) {
						headOrRegionalOfficeTo.add(employeeTransfer.getEmployeeMaster().getPersonalInfoEmployment()
								.getHeadAndRegionOffice());
					} else {
						Integer entityCode = employeeTransfer.getEmployeeMaster().getPersonalInfoEmployment()
								.getHeadAndRegionOffice().getCode();
						headOrRegionalOfficeTo = getHeadRegionalOfficeList(employeeTransfer.getTransferType(),
								entityCode);
					}
				} else {
					log.info("---NullPointer Exception--");
				}
			} else {
				if (headOrRegionFrom != null && employeeTransfer.getTransferType() != null
						&& !employeeTransfer.getTransferType().isEmpty()) {
					if (employeeTransfer.getTransferType().equalsIgnoreCase("IntraRegion")) {
						headOrRegionalOfficeTo.add(headOrRegionFrom);
					} else {
						Integer entityCode = headOrRegionFrom.getCode();
						headOrRegionalOfficeTo = getHeadRegionalOfficeList(employeeTransfer.getTransferType(),
								entityCode);
					}
				} else {
					log.info("---NullPointer Exception--");
				}
			}

		} catch (Exception ex) { 
			log.info("error found in getHeadAndRegionalOfficeByTransferType Method", ex);
		}
		log.info("getHeadAndRegionalOfficeByTransferType Method End=====>");
	}

	public List<EntityMaster> getHeadRegionalOfficeList(String transferType, Integer entityCode) {
		log.info("<--- Inside getHeadRegionalOfficeList() --->");
		List<EntityMaster> entityMaster = null;
		try {
			if (transferType.equalsIgnoreCase("InterRegion")) {
				log.info("-----InterRegion------");
			} else if (transferType.equalsIgnoreCase("Section")) {
				entityCode = 0;
			}
			String requestPath = AppUtil.getPortalServerURL() + "/entitymaster/getHeadRegionalOfficeList/" + entityCode;
			BaseDTO baseDTO = httpService.get(requestPath);
			log.info("baseDTO==>" + baseDTO);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					entityMaster = mapper.readValue(jsonResponse, new TypeReference<List<EntityMaster>>() {
					});
					log.info("getHeadRegionalOfficeList : " + entityMaster.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("<--- Exception in getHeadRegionalOfficeList() --->", e);
		}
		return entityMaster;
	}

	public void valueSetToNote() {
		log.info("valueSetToNote Method Start=====>");
		if (note != null) {
			empTransferDetailsNote.setNote(note);
		}
		log.info("valueSetToNote Method End=====>");
	}

	public void getTransferDetailsNoteByTransferId() {
		log.info("getTransferDetailsNoteByTransferId Method Start=====>");
		BaseDTO baseDTO = new BaseDTO();
		try {
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/getnote/"
					+ selectedEmployeeTransfer.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empTransferDetailsNote = mapper.readValue(jsonResponse, new TypeReference<EmpTransferDetailsNote>() {
				});
			}
			if (empTransferDetailsNote != null) {
				note = empTransferDetailsNote.getNote();
				finalApproval = empTransferDetailsNote.getFinalApproval();
				userMaster = empTransferDetailsNote.getUserMaster();
				previousApproval = empTransferDetailsNote.getFinalApproval();
			}
		} catch (Exception exp) {
			log.info("Error in getTransferDetailsNoteByTransferId Method =====>", exp);
		}
		log.info("getTransferDetailsNoteByTransferId Method End=====>");
	}

	public void cancel() {
		employeeMaster = new EmployeeMaster();
		employeeTransfer = new EmployeeTransferDetails();
		employeeId = null;
		toSection = new SectionMaster();
		transferDocumentFileName = "";
		byType = "byEmployeeId";
		selected = "byEmployee";
		entityTypeFrom = new EntityTypeMaster();
		headOrRegionFrom = new EntityMaster();
		department = new Department();
		departmentTo = null;
		empTransferDetailsNote = new EmpTransferDetailsNote();
		employeeList = new ArrayList<EmployeeMaster>();
		userMaster = null;
		finalApproval = null;
		section = null;
		note = "";
	}

	public void setCheckTransferAvailability() {
		log.info("setCheckTransferAvailability Method Start=====>");
		try {
			employeeTransfer.setTransferAvailability(transferAvailability);
			// employeeTransfer.setJoiningDate(joiningDate);
			employeeTransfer.setAvailabilityFromDate(availabilityFromDate);
			employeeTransfer.setAvailabilityPosition(availabilityPosition);
			employeeTransfer.setTransferComment(transferComment);
			empTransferDetailsNote.setUserMaster(userMaster);
			empTransferDetailsNote.setFinalApproval(finalApproval);
			employeeTransfer.setEmpTransferDetailsNote(empTransferDetailsNote);
			// errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
		} catch (Exception ex) {
			log.info("Error in setCheckTransferAvailability Method =====>", ex);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
		log.info("setCheckTransferAvailability Method End=====>");

	}

	public void downloadfile() {
		InputStream input = null;
		try {
			if (employeeTransfer.getDocumentPath() != null) {
				File files = new File(employeeTransfer.getDocumentPath());
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
				log.error("exception in filedownload method------- " + files.getName());
			}

		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	public String approveEmpTransferDetails() {
		log.error(" approveEmpTransferDetails method Start------- ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			/*
			 * if(StringUtils.isEmpty(userMaster.getEmpName()) &&
			 * userMaster.getEmpName()==null) { Util.addWarn("Forward To is empty");
			 * RequestContext.getCurrentInstance().update("growls"); return null; }
			 * if(finalApproval==null) { Util.addWarn("Forward for is empty");
			 * RequestContext.getCurrentInstance().update("growls"); return null; }
			 */
			/*
			 * if(StringUtils.isEmpty(approveRemark)) { Util.addWarn("Remarks is empty");
			 * RequestContext.getCurrentInstance().update("growls"); return null; }
			 */
		
			// employeeTransfer.setJoinedOn(joinedOn);
			String status = null;
			status = null != selectedEmployeeTransfer ? selectedEmployeeTransfer.getStatus(): null;
			if(null == status){
				if(null != viewNoteEmployeeDetails && !viewNoteEmployeeDetails.isEmpty())
					status = viewNoteEmployeeDetails.get(0).getStatus();
				previousApproval = null != empTransferDetailsNote ? empTransferDetailsNote.getFinalApproval():false;
			}
			empTransferDetailsNote.setUserMaster(userMaster);
			empTransferDetailsNote.setFinalApproval(finalApproval);
			employeeTransfer.setEmpTransferDetailsNote(empTransferDetailsNote);

			if (null != status && (status.equalsIgnoreCase("REQUEST_SUBMITTED") || status.equalsIgnoreCase("REQUEST_APPROVED")
					|| status.equalsIgnoreCase("REQUEST_FINAL_APPROVED"))) {
				if (previousApproval == false
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.TRANSFER_REQUEST_APPROVED);
				} else if (previousApproval == true
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.TRANSFER_FINAL_APPROVED);
				} else {
					empTransferDetailsLog.setStage(AdditionalChargeStage.TRANSFER_REQUEST_APPROVED);
				}
			}

			if (status.equalsIgnoreCase("AVAILABILITY_SUBMITTED") || status.equalsIgnoreCase("AVAILABILITY_APPROVED")
					|| status.equalsIgnoreCase("AVAILABILITY_FINAL_APPROVED")) {
				if (previousApproval == false
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_APPROVED);
				} else if (previousApproval == true
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_FINAL_APPROVED);
				} else {
					empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_APPROVED);
				}
			}

			if (status.equalsIgnoreCase("JOINING_SUBMITTED") || status.equalsIgnoreCase("JOINING_APPROVED")
					|| status.equalsIgnoreCase("JOINING_FINAL_APPROVED")) {
				if (previousApproval == false
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.JOINING_APPROVED);
				} else if (previousApproval == true
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.JOINING_FINAL_APPROVED);
				} else {
					empTransferDetailsLog.setStage(AdditionalChargeStage.JOINING_APPROVED);
				}
			}

			if (status.equalsIgnoreCase("RELIEVING_SUBMITTED") || status.equalsIgnoreCase("RELIEVING_APPROVED")
					|| status.equalsIgnoreCase("RELIEVING_FINAL_APPROVED")) {
				if (previousApproval == false
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.RELIVING_APPROVED);
				} else if (previousApproval == true
						&& employeeTransfer.getEmpTransferDetailsNote().getFinalApproval() == true) {
					empTransferDetailsLog.setStage(AdditionalChargeStage.RELIVING_FINAL_APPROVED);
				} else {
					empTransferDetailsLog.setStage(AdditionalChargeStage.RELIVING_APPROVED);
				}
			}

			empTransferDetailsLog.setRemarks(approveRemark);
			employeeTransfer.setEmpTransferDetailslog(empTransferDetailsLog);
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/approve";
			baseDTO = httpService.post(url, employeeTransfer);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_APPROVED.getErrorCode());
				loadLazyModel();
				selectedEmployeeTransfer = new EmployeeTransferDTO();
				return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
			}

		} catch (Exception exp) {
			log.error("Error in approveEmpTransferDetails method------- ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.error(" approveEmpTransferDetails method End------- ");
		return null;
	}

	public String rejectEmpTransferDetails() {
		log.error(" rejectEmpTransferDetails method Start------- ");
		BaseDTO baseDTO = new BaseDTO();
		try {
			/*
			 * if(StringUtils.isEmpty(rejeckRemark) && rejeckRemark==null) {
			 * Util.addWarn("Remarks is empty");
			 * RequestContext.getCurrentInstance().update("growls"); return null; }
			 */
			empTransferDetailsLog.setRemarks(rejeckRemark);
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("REQUEST_SUBMITTED")
					|| selectedEmployeeTransfer.getStatus().equalsIgnoreCase("REQUEST_APPROVED")) {
				empTransferDetailsLog.setStage(AdditionalChargeStage.TRANSFER_REJECTED);
			} else if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("AVAILABILITY_SUBMITTED")
					|| selectedEmployeeTransfer.getStatus().equalsIgnoreCase("AVAILABILITY_APPROVED")) {
				empTransferDetailsLog.setStage(AdditionalChargeStage.AVAILABILITY_REJECTED);
			} else if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("RELIEVING_APPROVED")
					|| selectedEmployeeTransfer.getStatus().equalsIgnoreCase("RELIEVING_SUBMITTED")) {
				empTransferDetailsLog.setStage(AdditionalChargeStage.RELIVING_REJECTED);
			} else if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("JOINING_SUBMITTED")
					|| selectedEmployeeTransfer.getStatus().equalsIgnoreCase("JOINING_APPROVED")) {
				empTransferDetailsLog.setStage(AdditionalChargeStage.JOINING_REJECTED);
			}
			employeeTransfer.setEmpTransferDetailslog(empTransferDetailsLog);
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/reject";
			baseDTO = httpService.post(url, employeeTransfer);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_REJECTED.getErrorCode());
				loadLazyModel();
				selectedEmployeeTransfer = new EmployeeTransferDTO();
				return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
			}
		} catch (Exception exp) {
			log.error("Exception in rejectEmpTransferDetails method End-- ", exp);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.error(" rejectEmpTransferDetails method End------- ");
		return null;
	}

	public void showDialogBox(String dlgName) {
		log.error(" showDialogBox method Start------- ");
		try {

			RequestContext context = RequestContext.getCurrentInstance();
			if (dlgName.equalsIgnoreCase("APPROVEDIALOG")) {
				context.execute("PF('dlgComments2').show();");
			} else {
				context.execute("PF('dlgComments3').show();");
			}
		} catch (Exception ex) {
			log.error("Exception in showDialogBox method End-- ", ex);
		}
		log.error(" showDialogBox method End------- ");
	}

	public String addjoining() {
		log.info(":: Employee transfer approval status :: ");
		try {
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("REQUEST_FINAL_APPROVED")) {
				if (StringUtils.isEmpty(userMaster.getEmpName()) && userMaster.getEmpName() == null) {
					Util.addWarn("Forward To is empty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("AVAILABILITY_FINAL_APPROVED")) {
				if (StringUtils.isEmpty(userMaster.getEmpName()) && userMaster.getEmpName() == null) {
					Util.addWarn("Forward To is empty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			if (selectedEmployeeTransfer.getStatus().equalsIgnoreCase("RELIEVING_FINAL_APPROVED")) {
				if (StringUtils.isEmpty(userMaster.getEmpName()) && userMaster.getEmpName() == null) {
					Util.addWarn("Forward To is empty");
					RequestContext.getCurrentInstance().update("growls");
					return null;
				}
			}
			setCheckTransferAvailability();
			if (Objects.isNull(employeeTransfer)) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			log.info(":: Employee transfer approval by id ::" + employeeTransfer.getId());
			log.info(":: Employee transfer approval by id URL ::" + Resource.APPROVAL.URI);

			if (resource.toString().equalsIgnoreCase("RELIVE")) {
				employeeTransfer.setJoiningDate(null);
				employeeTransfer.setRelievedOn(relivingDate);
				empTransferDetailsLog.setStage(AdditionalChargeStage.RELIVING_SUBMITTED);
			} else {
				employeeTransfer.setJoiningDate(joiningDate);
				empTransferDetailsLog.setStage(AdditionalChargeStage.JOINING_SUBMITTED);
			}

			employeeTransfer.setEmpTransferDetailslog(empTransferDetailsLog);
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/joining";
			BaseDTO baseDto = httpService.put(url, employeeTransfer);
			log.info(":: Employee transfer approve by id BaseDTO Response ::" + baseDto);
			if (baseDto == null) {
				log.info(":: Employee transfer approve by id BaseDTO Response null::");
				errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
			}
			if (baseDto.getStatusCode() == 0) {
				log.info(":: Employee transfer record check Availability approved successfully ::");
				errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_DETAILS_APPROVED.getErrorCode());
			}
			loadLazyModel();
			selectedEmployeeTransfer = new EmployeeTransferDTO();
			add = true;
		} catch (Exception e) {
			log.error(":: Exception while  check Availability approved employee transfer :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		selectedEmployeeTransfer = new EmployeeTransferDTO();
		// return
		// "/pages/personnelHR/employeeProfile/listTransferRequestAdmin.xhtml?faces-redirect=true;";
		return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
	}

	public void rejoinFileUpload(FileUploadEvent event) {

		log.info("Rejoin Upload button is pressed..");
		try {

			if (event == null || event.getFile() == null) {
				log.error("Rejoin Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			reJoinDocument = event.getFile();
			long size = reJoinDocument.getSize();
			log.info("Rejoin Document document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				employeeTransfer.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_TRANSFER_REQUEST_REJOIN_UPLOAD_PATH");

			reJoinFileName = reJoinDocument.getFileName();

			String docPath = Util.fileUpload(uploadPath, reJoinDocument);
			employeeTransfer.setRejoinDocumentPath(docPath);
			errorMap.notify(ErrorDescription.INFO_EMP_TRANSFER_TRANSFER_DOCUMENT_UPLOADED.getErrorCode());

		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public String saveRejoin() {
		log.info("Start saveRejoin Method");
		BaseDTO baseDTO = new BaseDTO();
		try {
			employeeTransfer.setId(selectedEmployeeTransfer.getId());
			empTransferDetailsNote.setUserMaster(loginBean.getUserMaster());
			empTransferDetailsNote.setFinalApproval(true);
			empTransferDetailsNote.setNote("ok");
			employeeTransfer.setEmpTransferDetailsNote(empTransferDetailsNote);
			employeeTransfer.setJoinedOn(joinedOn);
			String url = AppUtil.getPortalServerURL() + "/employee/transfer/rejoin";
			baseDTO = httpService.post(url, employeeTransfer);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(
						ErrorDescription.getError(AdminErrorCode.EMP_TRANSFER_REJOIN_SUBMIT_SUCCESSFULLY).getCode());
				return "/pages/admin/transferRequest/listTransferRequestAdmin.xhtml?faces-redirect=true;";
			}
		} catch (Exception ex) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("End saveRejoin Method");
		return null;
	}

	public void downloadRejoinfile() {
		InputStream input = null;
		try {
			if (employeeTransfer.getRejoinDocumentPath() != null) {
				File files = new File(employeeTransfer.getRejoinDocumentPath());
				input = new FileInputStream(files);
				ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
				reJoinFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
						files.getName()));
				log.error("exception in filedownload method------- " + files.getName());
			}

		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	@PostConstruct
	public String showViewListPage() {
		log.info("EmployeeTransferDetailsBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String employeeTransferId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			employeeTransferId = httpRequest.getParameter("empTransferId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(employeeTransferId)) {
			selectedEmployeeTransfer = new EmployeeTransferDTO();
			selectedEmployeeTransfer.setId(Long.parseLong(employeeTransferId));
			resource = Resource.VIEW;
			selectedNotificationId = Long.parseLong(notificationId);
//			getEmpTransferDetails(selectedEmployeeTransferDetails);
			viewTransferDetail();
		}
		log.info("EmployeeTransferDetailsBean showViewListPage() Ends");
		return "/pages/admin/transferRequest/viewTransferRequestAdmin.xhtml?faces-redirect=true";
	}
}
