package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeLogNoteSignatureDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AdditionalChargeStage;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeAdditionalCharge;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.hrms.dto.EmployeeAdditionalChargeRequestDto;
import in.gov.cooptex.personnel.hrms.EmployeeAdditionalChargeLog;
import in.gov.cooptex.personnel.hrms.EmployeeAdditionalChargeNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("employeeAdditionalChargeBean")
@Scope("session")
@Log4j2
public class EmployeeAdditionalChargeBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6451874855114911920L;

	private final String CREATE__PAGE = "/pages/personnelHR/createAdditionalCharge.xhtml?faces-redirect=true;";
	private final String LIST_PAGE = "/pages/personnelHR/listAdditionalCharge.xhtml?faces-redirect=true;";
	private final String VIEW_PAGE = "/pages/personnelHR/viewAdditionalCharge.xhtml?faces-redirect=true;";
	private final String RELEIVE_PAGE = "/pages/personnelHR/relievingAdditionalCharge.xhtml?faces-redirect=true;";

	private final String EMP_LIST_PAGE = "/pages/personnelHR/employeeProfile/listAdditionalCharge.xhtml?faces-redirect=true;";
	private final String EMP_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewAdditionalCharge.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	boolean deptvisibility = false;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();

	@Getter
	@Setter
	List<EntityMaster> entityMasterList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<EntityMaster> hoRoRegionList = new ArrayList<EntityMaster>();

	@Getter
	@Setter
	List<Designation> designationList;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	EmployeeAdditionalChargeRequestDto employeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();

	@Getter
	@Setter
	EmployeeAdditionalChargeRequestDto selectedemployeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	LazyDataModel<EmployeeAdditionalChargeRequestDto> lazyemployeeAdditionalChargeList;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	List<UserMaster> userMasters = new ArrayList<UserMaster>();

	@Autowired
	LoginBean loginBean;

	@Autowired
	RestTemplate restTemplate;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private EmployeeAdditionalCharge employeeAdditionalCharge = new EmployeeAdditionalCharge();

	@Getter
	@Setter
	private EmployeeAdditionalCharge relieveAdditionalCharge = new EmployeeAdditionalCharge();

	@Getter
	@Setter
	private EmployeeAdditionalChargeLog employeeAdditionalChargeLog = new EmployeeAdditionalChargeLog();

	@Getter
	@Setter
	private EmployeeAdditionalChargeNote employeeAdditionalChargeNote = new EmployeeAdditionalChargeNote();

	@Getter
	@Setter
	List<EmployeeAdditionalChargeRequestDto> employeeAdditionalChargeRequestDtoList = new ArrayList<EmployeeAdditionalChargeRequestDto>();

	@Getter
	@Setter
	EmployeeAdditionalChargeRequestDto viewResponseDTO = new EmployeeAdditionalChargeRequestDto();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean createdByflag = false;

	@Getter
	@Setter
	Boolean relieveFlag = true;

	@Getter
	@Setter
	private Boolean viewButtonFlag = false;

	@Getter
	@Setter
	private Boolean approveeditFlag = true;

	@Getter
	@Setter
	int forwardfor;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	List<EntityMaster> headOfficeAndRegionEntityList;

	@Getter
	@Setter
	EntityMaster headOfficeAndRegionEntity = new EntityMaster();

	@Getter
	@Setter
	EntityTypeMaster regionalOfficeEntityType = new EntityTypeMaster();

	@Getter
	@Setter
	List<EntityTypeMaster> regionalOfficeEntityTypeList;

	@Getter
	@Setter
	EntityMaster entityMaster = new EntityMaster();

	@Getter
	@Setter
	Department department = new Department();

	@Getter
	@Setter
	Date dateOfAdditionalCharge;

	/* it is define for maximum date of addtional charge Date can be given */
	@Getter
	@Setter
	Date forFromDate;

	@Getter
	@Setter
	Long totalDays;

	@Getter
	@Setter
	int fromDateCount;

	@Getter
	@Setter
	Double absentCount;

	@Getter
	@Setter
	EmployeeAdditionalChargeRequestDto countResponseDTO = new EmployeeAdditionalChargeRequestDto();

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	List<EmployeeLogNoteSignatureDTO> viewLogResponseList = new ArrayList<EmployeeLogNoteSignatureDTO>();

	@Getter
	@Setter
	Long loggedInuserEmpId;

	@Getter
	@Setter
	Boolean generateflag = false;

	@Autowired
	AppPreference appPreference;

	public void init() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		generateflag = false;
		forwardToUsersList = new ArrayList<>();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ADDITIONAL_CHARGE.name());
		// designationList = commonDataService.loadDesignation();
		departmentList = commonDataService.loadDepartmentList();
		loadEmployeeLoggedInUserDetails();
		String fromDateAppKeyValue = commonDataService.getAppKeyValue(AppConfigKey.ADDITIONAL_CHARGE_LIMIT_DAYS_COUNT);
		if (fromDateAppKeyValue != null) {
			fromDateCount = Integer.parseInt(fromDateAppKeyValue);
		} else {
			log.info("<=====Appconfig |ADDITIONAL_CHARGE_LIMIT_DAYS_COUNT| value not found");
		}
		forFromDate = Util.addDate(new Date(), fromDateCount);
		if (loginBean.getEmployee() != null) {
			loggedInuserEmpId = loginBean.getEmployee().getId();
			log.info("===========loggedInuserEmpId================" + loggedInuserEmpId);
		}
	}

	public String showEmployeeAdditionalChargeListPage() {
		log.info("<==== Starts EmployeeAdditionalChargeBean.showEmployeeAdditionalChargeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		relieveFlag = true;
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ADDITIONAL_CHARGE.name());
		// loadLazyLeaveEligibilityMasterList();
		log.info("<==== Ends EmployeeAdditionalChargeBean.showEmployeeAdditionalChargeListPage =====>");
		return LIST_PAGE;
	}

	/* Load All Entity Type By Region Id */
	public void loadAllEntityTypesByRegionId() {
		log.info("<======== EmployeeAdditionalChargeBean.loadAllEntityByRegionId() calling ======>");
		regionalOfficeEntityTypeList = new ArrayList<>();
		regionalOfficeEntityType = new EntityTypeMaster();
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<>();
		employeeAdditionalChargeRequestDto.setEmployeeMaster(new EmployeeMaster());
		employeeMasterList = new ArrayList<EmployeeMaster>();
		try {
			String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
					+ headOfficeAndRegionEntity.getId();
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				regionalOfficeEntityTypeList = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
				});
			}
			log.info("regionalOfficegetEmployeeMasterByEntityIdEntityTypeList size"
					+ regionalOfficeEntityTypeList.size());
		} catch (Exception e) {
			log.error("<========== Exception EmployeeAdditionalChargeBean.loadAllEntityTypesByRegionId()==========>",
					e);
		}
	}

	public void headandRegional() {

		regionalOfficeEntityType = new EntityTypeMaster();
		regionalOfficeEntityTypeList = new ArrayList<EntityTypeMaster>();
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<EntityMaster>();
		department = new Department();
		employeeAdditionalChargeRequestDto.setSelectedEmployee(null);
		employeeMasterList = new ArrayList<EmployeeMaster>();

		if (headOfficeAndRegionEntity != null) {
			log.info("<====== EmployeeAdditionalChargeBean.headandRegional Starts====>entity master",
					headOfficeAndRegionEntity.getId());
			try {
				if (headOfficeAndRegionEntity.getId() != null && headOfficeAndRegionEntity.getEntityTypeMaster() != null
						&& headOfficeAndRegionEntity.getEntityTypeMaster().getEntityCode()
								.equalsIgnoreCase("HEAD_OFFICE")) {
					visibility = true;
					deptvisibility = false;
					entityMaster.setId(headOfficeAndRegionEntity.getId());
				} else {
					visibility = false;
					deptvisibility = true;
					String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
							+ headOfficeAndRegionEntity.getId();
					BaseDTO baseDTO = httpService.get(url);

					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						regionalOfficeEntityTypeList = mapper.readValue(jsonValue,
								new TypeReference<List<EntityTypeMaster>>() {
								});
						log.info("entitytypelistByHeadAndRegionalId list size is "
								+ regionalOfficeEntityTypeList.size());
					}
				}
				employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(
						headOfficeAndRegionEntity, regionalOfficeEntityType, entityMaster, department,
						new SectionMaster());
			} catch (Exception exp) {
				log.error("found exception in onchangeEntityType: ", exp);
			}
			log.info("<======= EmployeeAdditionalChargeBean.headandRegiona Ends =========>");

		} else {
			deptvisibility = false;
		}
	}

	/* Load All Entity By Region Id and Entity Type */
	public void loadAllEntityByRegionIdandEntityTypeId() {
		log.info("<======== EmployeeAdditionalChargeBean.loadAllEntityByRegionIdandEntityTypeId() calling ======>");
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<EntityMaster>();
		department = new Department();
		employeeAdditionalChargeRequestDto.setSelectedEmployee(null);
		employeeMasterList = new ArrayList<EmployeeMaster>();

		Long regionId = 0l;
		Long entityTypeId = 0l;
		try {
			if (headOfficeAndRegionEntity != null) {
				regionId = headOfficeAndRegionEntity.getId();
			}
			if (regionalOfficeEntityType != null) {
				entityTypeId = regionalOfficeEntityType.getId();
				if (regionalOfficeEntityType.getEntityCode() != null) {
					entityMasterList = commonDataService.loadEntityByregionOrentityTypeId(
							headOfficeAndRegionEntity.getId(), regionalOfficeEntityType.getId());
					if (regionalOfficeEntityType.getEntityCode().equalsIgnoreCase("SHOW_ROOM")) {
						deptvisibility = true;
						// designationList = loadDesignation();
						// designationList = commonDataService.loadActiveDesignations();
					} else {
						deptvisibility = false;
					}

				}
			}
			if (regionId != null && entityTypeId != null) {
				employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(
						headOfficeAndRegionEntity, regionalOfficeEntityType, entityMaster, department,
						new SectionMaster());
				log.info("loadAllEntityByRegionIdandEntityTypeId size" + entityMasterList.size());
			}
		} catch (Exception e) {
			log.error(
					"<========== Exception EmployeeAdditionalChargeBean.loadAllEntityByRegionIdandEntityTypeId()==========>",
					e);
		}
	}

	public void clearEntities() {
		headOfficeAndRegionEntity = new EntityMaster();
		regionalOfficeEntityType = new EntityTypeMaster();
		regionalOfficeEntityTypeList = new ArrayList<EntityTypeMaster>();
		entityMaster = new EntityMaster();
		entityMasterList = new ArrayList<EntityMaster>();
		department = new Department();
		employeeAdditionalChargeRequestDto.setSelectedEmployee(null);
		employeeMasterList = new ArrayList<EmployeeMaster>();
		employeeAdditionalCharge.setCurrentDesignation(new Designation());
		generateflag = false;
	}

	// public void onchangeDepartmentOrEntity() {
	//
	// try {
	//
	// employeeAdditionalChargeRequestDto.setSelectedEmployee(null);
	// employeeMasterList = new ArrayList<EmployeeMaster>();
	//
	// if (regionalOfficeEntityType != null) {
	// if (regionalOfficeEntityType.getEntityCode() != null) {
	// if (regionalOfficeEntityType.getEntityCode().equalsIgnoreCase("SHOW_ROOM")) {
	// if (entityMaster != null) {
	// department = new Department();
	// employeeMasterList = new ArrayList<EmployeeMaster>();
	// if (entityMaster.getId() != null) {
	// employeeMasterList = commonDataService
	// .getEmployeeMasterByEntityId(entityMaster.getId());
	// }
	// }
	//
	// } else {
	// if (department != null) {
	// if (department.getId() != null) {
	// String url = serverURL + "/employeepersonalinfoemployment/getbydepartment/"
	// + department.getId();
	// log.info("loadEmployeeList url is" + url);
	// BaseDTO baseDTO = httpService.get(url);
	// if (baseDTO != null) {
	// String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
	// employeeMasterList = mapper.readValue(jsonValue,new
	// TypeReference<List<EmployeeMaster>>() {});
	// log.error("<========== employeeMasterList==========>",
	// employeeMasterList.toString());
	//
	// }
	// }
	// }
	// }
	//
	// }
	// }
	// log.info("loadEmployeeList size" + employeeMasterList.size());
	// } catch (Exception e) {
	// log.error("<========== Exception
	// EmployeeAdditionalChargeBean.loadEmployeeList()==========>", e);
	// }
	// }

	public void onchangeDepartmentOrEntity() {

		try {

			employeeAdditionalChargeRequestDto.setSelectedEmployee(null);
			employeeMasterList = new ArrayList<EmployeeMaster>();
			employeeAdditionalCharge.setCurrentDesignation(null);
			designationList = new ArrayList<>();

			log.info("Entity Type Name=========> " + headOfficeAndRegionEntity.getName().toString());
			employeeMasterList = commonDataService.loadEmployeesByHoroAndAllPossibleQueries(headOfficeAndRegionEntity,
					regionalOfficeEntityType, entityMaster, department, new SectionMaster());
			log.info("loadEmployeeList size" + employeeMasterList.size());
		} catch (Exception e) {
			log.error("<========== Exception EmployeeAdditionalChargeBean.onchangeDepartmentOrEntity()==========>", e);
		}
	}

	public void onchangeEmployee() {

		try {
			String url = serverURL + "/employee/get/id/"
					+ employeeAdditionalChargeRequestDto.getSelectedEmployee().getId();
			log.info("loadEmployeeList url is" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonValue, new TypeReference<EmployeeMaster>() {
				});
			}
			employeeAdditionalCharge
					.setCurrentDesignation(employeeMaster.getPersonalInfoEmployment().getCurrentDesignation());
			loadDesignationByhierarchyOrder();
			generateflag = true;
		} catch (Exception e) {
			log.error("<========== Exception EmployeeAdditionalChargeBean.loadEmployeeList()==========>", e);
		}
	}

	/* Load Designation Type By designation Id and hierarchy_order value */
	public void loadDesignationByhierarchyOrder() {
		log.info("<======== employeeAdditionalChargeBean.loadDesignationByhierarchyOrder() calling ======>");
		designationList = new ArrayList<>();
		try {
			if (employeeAdditionalCharge != null && employeeAdditionalCharge.getCurrentDesignation() == null
					|| employeeAdditionalCharge.getCurrentDesignation().getHierarchyOrder() == null) {
				employeeAdditionalCharge.getCurrentDesignation().setHierarchyOrder(0);
			}
			String url = AppUtil.getPortalServerURL() + "/designation/getdesignationbyhierarchyorder/"
					+ employeeAdditionalCharge.getCurrentDesignation().getId() + "/"
					+ employeeAdditionalCharge.getCurrentDesignation().getHierarchyOrder();
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				designationList = mapper.readValue(jsonValue, new TypeReference<List<Designation>>() {
				});
			}
			log.info("loadDesignationByhierarchyOrder======designationList size" + designationList.size());
		} catch (Exception e) {
			log.error("<========== Exception employeeAdditionalChargeBean.loadDesignationByhierarchyOrder()==========>",
					e);
		}
	}

	// Load Designation
	public List<Designation> loadDesignation() {
		log.info("Inside loadDesignation()");
		designationList = new ArrayList<Designation>();
		String Url = AppUtil.getPortalServerURL() + "/designation/getalldesignation";
		try {
			HttpEntity<BaseDTO> responseDTO = restTemplate.exchange(Url, HttpMethod.GET, loginBean.requestEntity,
					BaseDTO.class);
			BaseDTO baseDto = responseDTO.getBody();
			if (baseDto != null) {
				// designationList = (List<Designation>)
				// baseDto.getResponseContent();
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse;
				jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
				designationList = mapper.readValue(jsonResponse, new TypeReference<List<Designation>>() {
				});

			}

		} catch (Exception e) {
			log.info("Error in getting Designation List() ::: " + e);
		}
		return designationList;
	}

	public String submit() {
		if (StringUtils.isEmpty(employeeAdditionalChargeNote.getNote())) {
			errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
			return null;
		}

		if (StringUtils.isNotEmpty(employeeAdditionalChargeNote.getNote())) {
			String note = employeeAdditionalChargeNote.getNote();
			note = note.replaceAll("\\<.*?\\>", "");
			note = note.replaceAll("\\\\s", " ").trim();
			if (StringUtils.isEmpty(note)) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				return null;
			}
		}

		if (employeeAdditionalCharge.getCurrentDesignation() == null
				|| employeeAdditionalCharge.getCurrentDesignation().getName() == null) {
			AppUtil.addWarn("Generate Designation");
			return null;
		}

		employeeAdditionalCharge.setDateOfAditionaalCharge(dateOfAdditionalCharge);
		if (regionalOfficeEntityType != null) {
			if (regionalOfficeEntityType.getId() != null) {
				employeeAdditionalCharge.setEntityTypeMaster(regionalOfficeEntityType);
				employeeAdditionalChargeRequestDto.setSelectedEntityTypeMaster(regionalOfficeEntityType);
			}
		}
		if (entityMaster != null) {
			if (entityMaster.getId() != null) {
				employeeAdditionalCharge.setEntityMaster(entityMaster);
				employeeAdditionalChargeRequestDto.setSelectdEntityMaster(entityMaster);
			}
		}
		if (employeeAdditionalChargeRequestDto.getSelectedEmployee() != null) {
			employeeAdditionalCharge.setEmployeeMaster(employeeAdditionalChargeRequestDto.getSelectedEmployee());
		}
		if (headOfficeAndRegionEntity != null && regionalOfficeEntityType != null && entityMaster != null) {
			if (headOfficeAndRegionEntity.getId() != null && regionalOfficeEntityType.getId() == null
					&& entityMaster.getId() == null) {
				employeeAdditionalChargeRequestDto.setSelectdEntityMaster(headOfficeAndRegionEntity);
				employeeAdditionalChargeRequestDto
						.setSelectedEntityTypeMaster(headOfficeAndRegionEntity.getEntityTypeMaster());
			}
		}

		// employeeAdditionalCharge.setEntity(entityMaster);
		// employeeAdditionalCharge.setEmployeeMaster(employeeMaster);
		employeeAdditionalChargeRequestDto.setEmployeeAdditionalChargeNote(employeeAdditionalChargeNote);
		employeeAdditionalChargeRequestDto.setEmployeeAdditionalChargeLog(employeeAdditionalChargeLog);

		employeeAdditionalChargeRequestDto.setEmployeeAdditionalCharge(employeeAdditionalCharge);

		log.info("<=======Starts employeeAdditionalChargeBean.submit ======>" + employeeAdditionalCharge);
		String url = serverURL + "/employeeAdditionalCharge/create";
		BaseDTO response = httpService.post(url, employeeAdditionalChargeRequestDto);
		if (response.getStatusCode() == 0) {
			if (action.equals("ADD")) {
				errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_CREATED);
			} else {
				errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_UPDATED);
			}
			return showListPage();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends employeeAdditionalChargeBean.submit ======>");
		return null;
	}

	public void lazyemployeeAdditionalChargeListResponse() {
		log.info("<===== Starts employeeAdditionalChargeBean.lazyemployeeAdditionalChargeList ======>");
		employeeAdditionalChargeRequestDtoList = new ArrayList<>();
		lazyemployeeAdditionalChargeList = new LazyDataModel<EmployeeAdditionalChargeRequestDto>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EmployeeAdditionalChargeRequestDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/employeeAdditionalCharge/getall";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeeAdditionalChargeRequestDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeAdditionalChargeRequestDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info(totalRecords + "=======employeeAdditional List=========="
								+ employeeAdditionalChargeRequestDtoList.size());
						/*
						 * for (EmployeeAdditionalChargeRequestDto p :
						 * employeeAdditionalChargeRequestDtoList) { log.info("p.getId ::::::::::" + p);
						 * }
						 */
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in employeeAdditionalChargeBean.lazyemployeeAdditionalChargeList ...",
							e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeeAdditionalChargeRequestDtoList;
			}

			@Override
			public Object getRowKey(EmployeeAdditionalChargeRequestDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeAdditionalChargeRequestDto getRowData(String rowKey) {
				try {
					for (EmployeeAdditionalChargeRequestDto benefit : employeeAdditionalChargeRequestDtoList) {
						if (benefit.getId().equals(Long.valueOf(rowKey))) {
							selectedemployeeAdditionalChargeRequestDto = benefit;
							return benefit;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends employeeAdditionalChargeBean.lazyemployeeAdditionalChargeList ======>");
	}

	public String showListPage() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		relieveFlag = true;
		selectedemployeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();
		lazyemployeeAdditionalChargeListResponse();
		return LIST_PAGE;
	}

	public String clearButton() {
		editButtonFlag = true;
		addButtonFlag = false;
		viewButtonFlag = true;
		relieveFlag = true;
		selectedemployeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();
		lazyemployeeAdditionalChargeListResponse();
		return LIST_PAGE;
	}

	public String addPageAction() {
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("create  Employee Addition Charge called..");
				entityMasterList = new ArrayList<>();
				employeeMasterList = new ArrayList<>();
				// employeeAdditionalChargeRequestDto = new
				// EmployeeAdditionalChargeRequestDto();
				headOfficeAndRegionEntity = new EntityMaster();
				regionalOfficeEntityType = null;
				entityMaster = null;
				employeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();
				employeeAdditionalChargeLog = new EmployeeAdditionalChargeLog();
				employeeAdditionalChargeNote = new EmployeeAdditionalChargeNote();
				department = new Department();
				headOfficeAndRegionEntityList = new ArrayList<>();
				headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
				// hoRoRegionList = commonDataService.loadHeadAndRegionalOffice();
				departmentList = commonDataService.loadDepartmentList();
				// designationList = loadDesignation();
				// designationList = commonDataService.loadActiveDesignations();
				dateOfAdditionalCharge = null;
				employeeAdditionalCharge = new EmployeeAdditionalCharge();
				userMasters = commonDataService.loadForwardToUsers();
				fromDateCount = Integer
						.parseInt(commonDataService.getAppKeyValue(AppConfigKey.ADDITIONAL_CHARGE_LIMIT_DAYS_COUNT));
				forFromDate = Util.addDate(new Date(), fromDateCount);
				return CREATE__PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return null;
	}

	/* Load Designation Type By Department Id */
	public void loadDesignationByDept() {
		log.info("<======== employeeAdditionalChargeBean.loadDesignationByDept() calling ======>");
		try {
			String url = AppUtil.getPortalServerURL() + "/designation/getalldesignation/" + department.getId();
			log.info("url" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				designationList = mapper.readValue(jsonValue, new TypeReference<List<Designation>>() {
				});
			}
			log.info("designationList size" + designationList.size());
		} catch (Exception e) {
			log.error("<========== Exception employeeAdditionalChargeBean.loadDesignationByDept()==========>", e);
		}
	}

	public void onRowSelect(SelectEvent event) {
		addButtonFlag = true;
		viewButtonFlag = false;
		relieveFlag = true;
		selectedemployeeAdditionalChargeRequestDto = (EmployeeAdditionalChargeRequestDto) event.getObject();
		log.info("selectedBenefit id-------" + selectedemployeeAdditionalChargeRequestDto.getId());
		log.info("status----------" + selectedemployeeAdditionalChargeRequestDto.getStatus());

		if (AdditionalChargeStage.CHARGE_REJECTED.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
			editButtonFlag = false;
			approvalFlag = false;
		} else {
			editButtonFlag = true;
		}
		if (AdditionalChargeStage.EMPLOYEE_ACCEPTED.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())
				|| AdditionalChargeStage.RELIEVE_REJECTED
						.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
			relieveFlag = false;
		}
	}

	public String viewemployeeAdditionalCharge() {

		getViewemployeeAdditionalCharge();
		buttonFlag = false;
		approvalFlag = false;
		finalApproveFlag = false;
		approveeditFlag = true;
		if (selectedemployeeAdditionalChargeRequestDto != null) {

			setPreviousApproval(
					selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote().getFinalApproval());

			if ("VIEW".equalsIgnoreCase(action)) {

				if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
						.getUserMaster() != null) {
					if (loginBean.getUserMaster().getId().equals(selectedemployeeAdditionalChargeRequestDto
							.getEmployeeAdditionalChargeNote().getUserMaster().getId())) {
						approveeditFlag = false;

						if (AdditionalChargeStage.CHARGE_REJECTED
								.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())
								|| AdditionalChargeStage.EMPLOYEE_DECLINED
										.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
							approvalFlag = false;
							finalApproveFlag = true;
						} else {
							approvalFlag = true;
							finalApproveFlag = false;
							// selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							// .setUserMaster(null);
						}
					} else {
						approvalFlag = false;
						finalApproveFlag = true;
					}
				}
				if (AdditionalChargeStage.EMPLOYEE_ACCEPT_PENDING
						.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())
						|| AdditionalChargeStage.RELIEVE_FINAL_APPROVED
								.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())
						|| AdditionalChargeStage.EMPLOYEE_ACCEPTED
								.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
					buttonFlag = true;
					setFinalApproveFlag(selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval());
				}
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().setRemarks(null);
				return VIEW_PAGE;
			}
		}
		return null;
	}

	public void getViewemployeeAdditionalCharge() {
		log.info("Inside getViewJV() ");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeAdditionalChargeRequestDto();
		viewLogResponseList = new ArrayList<EmployeeLogNoteSignatureDTO>();
		try {
			String url = serverURL + "/employeeAdditionalCharge/getdetailsbyid/"
					+ selectedemployeeAdditionalChargeRequestDto.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeAdditionalChargeRequestDto>() {
						});

				ObjectMapper empLogmapper = new ObjectMapper();
				String employeeDatajsonResponses = empLogmapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<EmployeeLogNoteSignatureDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());

				if (selectedNotificationId != null) {
					systemNotificationBean.loadTotalMessages();
				}

				if (viewResponseDTO != null) {
					employeeAdditionalChargeRequestDto = viewResponseDTO;
				}

				if (viewResponseDTO.getEmployeeAdditionalCharge().getEntityMaster() != null) {
					employeeAdditionalChargeRequestDto
							.setSelectdEntityMaster(viewResponseDTO.getEmployeeAdditionalCharge().getEntityMaster());
				}
				if (employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityTypeMaster() != null) {
					employeeAdditionalChargeRequestDto.setSelectedEntityTypeMaster(
							employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityTypeMaster());
				}

				if (employeeAdditionalChargeRequestDto.getEmployeeMaster() != null) {

				}
				// designation=viewResponseDTO.getEmployeeAdditionalCharge().getInterchangeDesignation();
				// department=viewResponseDTO.getEmployeeAdditionalCharge().getInterDepartment();
				// regionalOfficeEntityType=viewResponseDTO.getEmployeeAdditionalCharge().getEntityType();
				if (viewResponseDTO.getEmployeeAdditionalChargeLog() != null) {
					employeeAdditionalChargeLog = viewResponseDTO.getEmployeeAdditionalChargeLog();
				}
				if (viewResponseDTO.getEmployeeAdditionalChargeNote() != null) {
					employeeAdditionalChargeNote = viewResponseDTO.getEmployeeAdditionalChargeNote();
				}

				selectedemployeeAdditionalChargeRequestDto
						.setEmployeeAdditionalChargeNote(employeeAdditionalChargeNote);
				selectedemployeeAdditionalChargeRequestDto.setEmployeeAdditionalChargeLog(employeeAdditionalChargeLog);
				if (viewResponseDTO.getEmployeeAdditionalCharge() != null) {
					employeeAdditionalCharge = viewResponseDTO.getEmployeeAdditionalCharge();
					relieveAdditionalCharge = viewResponseDTO.getEmployeeAdditionalCharge();

					if (loginBean.getUserMaster().getId().equals(employeeAdditionalCharge.getCreatedBy().getId())) {
						createdByflag = true;
					}
				}
				// entityMaster=viewResponseDTO.getEmployeeAdditionalCharge().getEntity();
				// selectedemployeeAdditionalChargeRequestDto
				// .setEmployeeAdditionalChargeLog(viewResponseDTO.getEmployeeAdditionalChargeLog());
				// headOfficeAndRegionEntity=viewResponseDTO.getHeadOfficeRegionOffice();
				if (relieveAdditionalCharge.getRelievingDate() != null) {
					/*
					 * totalDays =
					 * getDifferenceDays(relieveAdditionalCharge.getDateOfAditionaalCharge(),
					 * relieveAdditionalCharge.getRelievingDate()) + 2;
					 */
					totalDays = employeeAdditionalChargeRequestDto.getTotalWorkingDays().longValue();
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While getViewJV() :: ", e);
		}
	}

	public String editemployeeAdditionalCharge() {
		loadeditemployeeAdditionalCharge();
		return CREATE__PAGE;
	}

	public void loadeditemployeeAdditionalCharge() {
		log.info("Inside loadeditTaBill() ");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeAdditionalChargeRequestDto();

		headOfficeAndRegionEntity = null;
		regionalOfficeEntityType = null;
		entityMaster = null;

		try {
			String url = serverURL + "/employeeAdditionalCharge/getdetailsbyid/"
					+ selectedemployeeAdditionalChargeRequestDto.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeAdditionalChargeRequestDto>() {
						});

				if (viewResponseDTO != null) {
					employeeAdditionalChargeRequestDto = viewResponseDTO;

					// if
					// (viewResponseDTO.getEmployeeAdditionalCharge().getEntityTypeMaster().getEntityCode()
					// .equals("HEAD_OFFICE")) {
					// headOfficeAndRegionEntity =
					// viewResponseDTO.getEmployeeAdditionalCharge().getEntityMaster();
					// regionalOfficeEntityType = null;
					// entityMaster = null;
					// } else {
					// headOfficeAndRegionEntity = viewResponseDTO.getHoRoRegion();
					// }

					headOfficeAndRegionEntity = viewResponseDTO.getHoRoRegion();
					headOfficeAndRegionEntityList = new ArrayList<>();
					headOfficeAndRegionEntityList = commonDataService.loadHeadAndRegionalOffice();
					headandRegional();
					if (employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge()
							.getEntityTypeMaster() != null) {
						employeeAdditionalChargeRequestDto.setSelectedEntityTypeMaster(
								employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityTypeMaster());
						regionalOfficeEntityType = employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge()
								.getEntityTypeMaster();
					}
					loadAllEntityByRegionIdandEntityTypeId();

					if (employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityMaster() != null) {
						employeeAdditionalChargeRequestDto.setSelectdEntityMaster(
								employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityMaster());
						entityMaster = employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge()
								.getEntityMaster();
					}
					// onchangeDepartmentOrEntity();
					if (viewResponseDTO.getEmployeeMaster() != null) {
						department = viewResponseDTO.getEmployeeMaster().getPersonalInfoEmployment()
								.getCurrentDepartment();
					}
					onchangeDepartmentOrEntity();
					if (viewResponseDTO.getEmployeeMaster() != null) {
						employeeAdditionalChargeRequestDto.setSelectedEmployee(viewResponseDTO.getEmployeeMaster());
						employeeMasterList.add(viewResponseDTO.getEmployeeMaster());
					}
					if (viewResponseDTO.getEmployeeAdditionalChargeLog() != null) {
						employeeAdditionalChargeLog = viewResponseDTO.getEmployeeAdditionalChargeLog();
					}
					if (viewResponseDTO.getEmployeeAdditionalChargeNote() != null) {
						employeeAdditionalChargeNote = viewResponseDTO.getEmployeeAdditionalChargeNote();
					}
					if (viewResponseDTO.getEmployeeAdditionalCharge() != null) {
						employeeAdditionalCharge = viewResponseDTO.getEmployeeAdditionalCharge();
						dateOfAdditionalCharge = employeeAdditionalCharge.getDateOfAditionaalCharge();
					}
					onchangeEmployee();
					departmentList = commonDataService.loadDepartmentList();
					// designationList = loadDesignation();
					if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getUserMaster() != null) {
						if (loginBean.getUserMaster().getId().equals(selectedemployeeAdditionalChargeRequestDto
								.getEmployeeAdditionalChargeNote().getUserMaster().getId())) {
							employeeAdditionalChargeNote.setUserMaster(null);
						}
					}

				}
			}

			fromDateCount = Integer
					.parseInt(commonDataService.getAppKeyValue(AppConfigKey.ADDITIONAL_CHARGE_LIMIT_DAYS_COUNT));
			forFromDate = Util.addDate(new Date(), fromDateCount);
		} catch (Exception e) {
			log.error("Exception Occured While loadeditemployeeAdditionalCharge() :: ", e);
		}
	}

	public String approveemployeeAdditionalCharge() {
		BaseDTO baseDTO = null;
		try {
			log.info(" approveemployeeAdditionalCharge id----------"
					+ selectedemployeeAdditionalChargeRequestDto.getId());
			baseDTO = new BaseDTO();

			if (previousApproval == false
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.CHARGE_APPROVED.equals(
							selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.CHARGE_APPROVED);
			} else if (previousApproval == true
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.CHARGE_APPROVED.equals(
							selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.EMPLOYEE_ACCEPT_PENDING);
			}

			else if (previousApproval == false
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.CHARGE_SUBMITTED.equals(
							selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.CHARGE_APPROVED);
			}

			else if (previousApproval == true
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.CHARGE_SUBMITTED.equals(
							selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.EMPLOYEE_ACCEPT_PENDING);
			}

			if (previousApproval == false
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.RELIEVE_APPROVED
							.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.RELIEVE_APPROVED);
			} else if (previousApproval == true
					&& selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
							.getFinalApproval() == true
					&& AdditionalChargeStage.RELIEVE_APPROVED
							.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.RELIEVE_FINAL_APPROVED);
			}

			else if (AdditionalChargeStage.RELIEVE_SUBMITTED
					.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.RELIEVE_APPROVED);
			}

			log.info("approve remarks=========>"
					+ selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getRemarks());

			selectedemployeeAdditionalChargeRequestDto.setEmployeeAdditionalCharge(employeeAdditionalCharge);

			if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote() != null
					&& viewResponseDTO.getEmployeeAdditionalChargeNote() != null) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeNote()
						.setNote(viewResponseDTO.getEmployeeAdditionalChargeNote().getNote());
			}
			String url = serverURL + "/employeeAdditionalCharge/approveemployeeadditionalcharge";

			baseDTO = httpService.post(url, selectedemployeeAdditionalChargeRequestDto);
			if (baseDTO.getStatusCode() == 0) {

				if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
						.equals(AdditionalChargeStage.CHARGE_APPROVED)
						|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
								.equals(AdditionalChargeStage.RELIEVE_APPROVED)) {
					errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_APPROVED);
				}

				else {
					errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_FINAL_APPROVED);
				}
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("approveemployeeAdditionalCharge method inside exception-------", e);
		}
		return null;
	}

	public String rejectemployeeAdditionalCharge() {
		BaseDTO baseDTO = null;
		try {
			log.info("amount transfer id----------" + selectedemployeeAdditionalChargeRequestDto.getId());
			baseDTO = new BaseDTO();

			if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
					.equals(AdditionalChargeStage.CHARGE_SUBMITTED)
					|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
							.equals(AdditionalChargeStage.CHARGE_APPROVED)
					|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
							.equals(AdditionalChargeStage.CHARGE_FINAL_APPROVED)) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.CHARGE_REJECTED);
			}

			if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
					.equals(AdditionalChargeStage.RELIEVE_SUBMITTED)
					|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
							.equals(AdditionalChargeStage.RELIEVE_APPROVED)
					|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
							.equals(AdditionalChargeStage.RELIEVE_FINAL_APPROVED)) {
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
						.setStage(AdditionalChargeStage.RELIEVE_REJECTED);
			}

			String url = serverURL + "/employeeAdditionalCharge/rejectemployeeadditionalcharge";
			baseDTO = httpService.post(url, selectedemployeeAdditionalChargeRequestDto);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_REJECTED);
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showListPage();
			}
		} catch (Exception e) {
			log.error("rejectemployeeAdditionalCharge method inside exception-------", e);
		}
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts employeeAdditionalChargeBean.deleteConfirm ===========>"
				+ selectedemployeeAdditionalChargeRequestDto.getId());
		try {
			BaseDTO response = httpService.delete(serverURL + "/employeeAdditionalCharge/deletebyid/"
					+ selectedemployeeAdditionalChargeRequestDto.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_DELETED);
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEmpTransferDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete employeeAdditionalCharge....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends employeeAdditionalChargeBean.deleteConfirm ===========>");
		return showListPage();
	}

	public String showrelievePage() {
		BaseDTO baseDTO = null;
		relieveAdditionalCharge = new EmployeeAdditionalCharge();
		employeeAdditionalChargeLog = new EmployeeAdditionalChargeLog();
		employeeAdditionalChargeNote = new EmployeeAdditionalChargeNote();
		viewLogResponseList.clear();
		totalDays = 0L;
		absentCount = 0.0;
		relieveAdditionalCharge.setTotalPresentDays(0.0);
		try {
			if (selectedemployeeAdditionalChargeRequestDto == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			log.info("<=====  showrelievePage Started for Selected ID===========>"
					+ selectedemployeeAdditionalChargeRequestDto.getId());
			if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
					.equalsIgnoreCase(AdditionalChargeStage.EMPLOYEE_ACCEPTED)
					|| selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
							.equalsIgnoreCase(AdditionalChargeStage.RELIEVE_REJECTED)) {

				String url = serverURL + "/employeeAdditionalCharge/getdetailsbyid/"
						+ selectedemployeeAdditionalChargeRequestDto.getId() + "/"
						+ (selectedNotificationId != null ? selectedNotificationId : 0);
				baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeAdditionalChargeRequestDto = mapper.readValue(jsonResponse,
							new TypeReference<EmployeeAdditionalChargeRequestDto>() {
							});
				}
				ObjectMapper empLogmapper = new ObjectMapper();
				String employeeDatajsonResponses = empLogmapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<EmployeeLogNoteSignatureDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());

				relieveAdditionalCharge = employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge();

				return RELEIVE_PAGE;
			}
		} catch (Exception e) {
			return null;
		}
		errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		return null;
	}

	public void dateCalculate() {
		BaseDTO baseDTO = null;
		totalDays = getDifferenceDays(relieveAdditionalCharge.getDateOfAditionaalCharge(),
				relieveAdditionalCharge.getRelievingDate());
		log.info("<=====  showrelievePage Started for Selected ID===========>" + totalDays);
		countResponseDTO = new EmployeeAdditionalChargeRequestDto();
		try {
			employeeAdditionalChargeRequestDto.setFromDate(relieveAdditionalCharge.getDateOfAditionaalCharge());
			employeeAdditionalChargeRequestDto.setToDate(relieveAdditionalCharge.getRelievingDate());
			String url = serverURL + "/employeeAdditionalCharge/getpresentcount";
			baseDTO = httpService.post(url, employeeAdditionalChargeRequestDto);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				countResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeAdditionalChargeRequestDto>() {
						});

				relieveAdditionalCharge.setTotalPresentDays(countResponseDTO.getPresentDays());
				absentCount = countResponseDTO.getAbsentDays();
				totalDays = countResponseDTO.getTotalWorkingDays().longValue();
			}

		} catch (Exception e) {
			log.info("<=====  Date Calculation Ended With Error======================>", e);
		}

	}

	public static long getDifferenceDays(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}

	public String relievecharge() {
		try {
			if (employeeAdditionalChargeNote.getNote() == null
					&& employeeAdditionalChargeNote.getNote().trim().equalsIgnoreCase("")) {
				errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
				return null;
			}
			if (StringUtils.isNotEmpty(employeeAdditionalChargeNote.getNote())) {
				String note = employeeAdditionalChargeNote.getNote();
				note = note.replaceAll("\\<.*?\\>", "");
				note = note.replaceAll("\\\\s", " ").trim();
				if (StringUtils.isEmpty(note)) {
					errorMap.notify(ErrorDescription.NOTE_REQUIRED_ERROR.getErrorCode());
					return null;
				}
			}

			if (selectedemployeeAdditionalChargeRequestDto != null) {
				employeeAdditionalChargeRequestDto.setEmployeeAdditionalChargeNote(employeeAdditionalChargeNote);
				employeeAdditionalChargeRequestDto.setEmployeeAdditionalChargeLog(employeeAdditionalChargeLog);
				employeeAdditionalChargeRequestDto.setEmployeeAdditionalCharge(relieveAdditionalCharge);
				employeeAdditionalChargeRequestDto.setId(selectedemployeeAdditionalChargeRequestDto.getId());
				employeeAdditionalChargeRequestDto.setTotalWorkingDays(relieveAdditionalCharge.getTotalPresentDays());
				log.info("<=======Starts employeeAdditionalChargeBean.submit ======>" + employeeAdditionalCharge);
				String url = serverURL + "/employeeAdditionalCharge/createrelievecharge";
				BaseDTO response = httpService.post(url, employeeAdditionalChargeRequestDto);
				if (response.getStatusCode() == 0) {
					if (action.equals("ADD")) {
						errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_CREATED);
					} else {
						errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_UPDATED);
					}
				} else {
					errorMap.notify(response.getStatusCode());
				}
				log.info("<=======Ends employeeAdditionalChargeBean.submit ======>");
				return showListPage();
			}
		} catch (Exception e) {
			log.info("<=======employeeAdditionalChargeBean.submit Ends with *******Error****** ======>" + e);

		}
		return showListPage();
	}

	@Getter
	@Setter
	EmployeeMaster loggedInEmployeeMaster;

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		try {
			//loggedInEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
			loggedInEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetailsList();
			
			if (loggedInEmployeeMaster == null) {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	// new Methods

	public String showEmpSelfChargeListPage() {
		log.info("<==== Starts EmployeeAdditionalChargeBean.showEmpAdditionalChargeListPage =====>");
		mapper = new ObjectMapper();
		viewButtonFlag = true;
		selectedemployeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();
		lazyempSelfServiceChargeListResponse();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ADDITIONAL_CHARGE.name());
		log.info("<==== Ends EmployeeAdditionalChargeBean.showEmpAdditionalChargeListPage =====>");
		return EMP_LIST_PAGE;
	}

	public void lazyempSelfServiceChargeListResponse() {
		log.info("<===== Starts employeeAdditionalChargeBean.lazyempSelfServiceChargeListResponse ======>");
		employeeAdditionalChargeRequestDtoList = new ArrayList<>();
		lazyemployeeAdditionalChargeList = new LazyDataModel<EmployeeAdditionalChargeRequestDto>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 5259910690478317460L;

			@Override
			public List<EmployeeAdditionalChargeRequestDto> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/employeeAdditionalCharge/lazyemployeechargeselfservice/"
							+ loggedInuserEmpId;
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeeAdditionalChargeRequestDtoList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeeAdditionalChargeRequestDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info(totalRecords + "=======employeeAdditional List=========="
								+ employeeAdditionalChargeRequestDtoList.size());
						/*
						 * for (EmployeeAdditionalChargeRequestDto p :
						 * employeeAdditionalChargeRequestDtoList) { log.info("p.getId ::::::::::" + p);
						 * }
						 */
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error(
							"Exception occured in employeeAdditionalChargeBean.lazyempSelfServiceChargeListResponse ...",
							e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeeAdditionalChargeRequestDtoList;
			}

			@Override
			public Object getRowKey(EmployeeAdditionalChargeRequestDto res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EmployeeAdditionalChargeRequestDto getRowData(String rowKey) {
				try {
					for (EmployeeAdditionalChargeRequestDto benefit : employeeAdditionalChargeRequestDtoList) {
						if (benefit.getId().equals(Long.valueOf(rowKey))) {
							selectedemployeeAdditionalChargeRequestDto = benefit;
							return benefit;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends employeeAdditionalChargeBean.lazyemployeeAdditionalChargeList ======>");
	}

	public String viewEmpselfAdditionalCharge() {
		getViewemployeeAdditionalCharge();
		buttonFlag = false;
		if (selectedemployeeAdditionalChargeRequestDto != null) {
			if ("VIEW".equalsIgnoreCase(action)) {
				if (AdditionalChargeStage.EMPLOYEE_ACCEPTED
						.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())
						|| AdditionalChargeStage.EMPLOYEE_DECLINED
								.equals(selectedemployeeAdditionalChargeRequestDto.getStatus())) {
					buttonFlag = true;
				}
				selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().setRemarks(null);
				return EMP_VIEW_PAGE;
			}
		}
		return null;
	}

	public void getviewEmpselfAdditionalCharge() {
		log.info("Inside viewEmpselfAdditionalCharge() ");
		BaseDTO baseDTO = null;
		viewResponseDTO = new EmployeeAdditionalChargeRequestDto();
		viewLogResponseList.clear();
		try {
			String url = serverURL + "/employeeAdditionalCharge/getdetailsbyid/"
					+ selectedemployeeAdditionalChargeRequestDto.getId();
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmployeeAdditionalChargeRequestDto>() {
						});
				ObjectMapper empLogmapper = new ObjectMapper();
				String employeeDatajsonResponses = empLogmapper.writeValueAsString(baseDTO.getTotalListOfData());
				viewLogResponseList = mapper.readValue(employeeDatajsonResponses,
						new TypeReference<List<EmployeeLogNoteSignatureDTO>>() {
						});
				log.info("<======= view Note Employee Details List ==========>" + viewLogResponseList.size());

				if (viewResponseDTO != null) {
					employeeAdditionalChargeRequestDto = viewResponseDTO;
				}
				if (viewResponseDTO.getEmployeeAdditionalCharge().getEntityMaster() != null) {
					employeeAdditionalChargeRequestDto
							.setSelectdEntityMaster(viewResponseDTO.getEmployeeAdditionalCharge().getEntityMaster());
				}
				if (employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityTypeMaster() != null) {
					employeeAdditionalChargeRequestDto.setSelectedEntityTypeMaster(
							employeeAdditionalChargeRequestDto.getEmployeeAdditionalCharge().getEntityTypeMaster());
				}
				if (relieveAdditionalCharge.getRelievingDate() != null) {
					totalDays = getDifferenceDays(relieveAdditionalCharge.getDateOfAditionaalCharge(),
							relieveAdditionalCharge.getRelievingDate()) + 2;
				}
			}
		} catch (Exception e) {
			log.error("Exception Occured While getViewJV() :: ", e);
		}
	}

	public String acceptEmployeeAdditionalCharge() {
		BaseDTO baseDTO = null;
		try {
			log.info(" acceptEmployeeAdditionalCharge id----------"
					+ selectedemployeeAdditionalChargeRequestDto.getId());
			baseDTO = new BaseDTO();

			selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
					.setStage(AdditionalChargeStage.EMPLOYEE_ACCEPTED);

			log.info("accept remarks=========>"
					+ selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getRemarks());

			selectedemployeeAdditionalChargeRequestDto.setEmployeeAdditionalCharge(employeeAdditionalCharge);

			String url = serverURL + "/employeeAdditionalCharge/approveemployeeadditionalcharge";

			baseDTO = httpService.post(url, selectedemployeeAdditionalChargeRequestDto);
			if (baseDTO.getStatusCode() == 0) {
				if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
						.equals(AdditionalChargeStage.EMPLOYEE_ACCEPTED)) {
					errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_ACCEPTED);
				}
				log.info("Successfully accepted-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showEmpSelfChargeListPage();
			}
		} catch (Exception e) {
			log.error("acceptEmployeeAdditionalCharge method inside exception-------", e);
		}
		return null;
	}

	public String declineEmployeeAdditionalCharge() {
		BaseDTO baseDTO = null;
		try {
			log.info(" acceptEmployeeAdditionalCharge id----------"
					+ selectedemployeeAdditionalChargeRequestDto.getId());
			baseDTO = new BaseDTO();

			selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog()
					.setStage(AdditionalChargeStage.EMPLOYEE_DECLINED);

			log.info("decline remarks=========>"
					+ selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getRemarks());

			selectedemployeeAdditionalChargeRequestDto.setEmployeeAdditionalCharge(employeeAdditionalCharge);

			String url = serverURL + "/employeeAdditionalCharge/approveemployeeadditionalcharge";

			baseDTO = httpService.post(url, selectedemployeeAdditionalChargeRequestDto);
			if (baseDTO.getStatusCode() == 0) {
				if (selectedemployeeAdditionalChargeRequestDto.getEmployeeAdditionalChargeLog().getStage()
						.equals(AdditionalChargeStage.EMPLOYEE_DECLINED)) {
					errorMap.notify(OperationErrorCode.EMPLOYEE_ADDITIONAL_CHARGE_DECLINED);
				}
				log.info("Successfully decline-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return showEmpSelfChargeListPage();
			}
		} catch (Exception e) {
			log.error("acceptEmployeeAdditionalCharge method inside exception-------", e);
		}
		return null;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("FuelFillingRegisterBean showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String employeeAdditionalChargeId = "";
		String notificationId = "";
		String stage = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			employeeAdditionalChargeId = httpRequest.getParameter("employeeAdditionalChargeId");
			notificationId = httpRequest.getParameter("notificationId");
			stage = httpRequest.getParameter("stage");
			log.info("Selected Notification Id :" + notificationId);
		}

		if (StringUtils.isNotEmpty(employeeAdditionalChargeId)) {
			init();
			selectedemployeeAdditionalChargeRequestDto = new EmployeeAdditionalChargeRequestDto();
			action = "VIEW";
			selectedemployeeAdditionalChargeRequestDto.setId(Long.parseLong(employeeAdditionalChargeId));

			selectedNotificationId = Long.parseLong(notificationId);
			if (stage != null && (stage.equals("EMPLOYEE_ACCEPT_PENDING"))) {
				viewEmpselfAdditionalCharge();
			} else {
				viewemployeeAdditionalCharge();

			}

		}
		log.info("FuelFillingRegisterBean showViewListPage() Ends");
		addButtonFlag = true;
		return null;
	}
}
