package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.mis.hrms.dto.EmpGeneralInfoDto;
import in.gov.cooptex.operation.hrms.dto.EmpRetirementListDto;
import in.gov.cooptex.operation.hrms.dto.EmpRetirementRequestDTO;
import in.gov.cooptex.operation.hrms.model.EmpRetirementRequest;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement;
import in.gov.cooptex.operation.hrms.model.EmployeeRetirement.RetirementType;
import in.gov.cooptex.personnel.hrms.model.EmpRetirementRequestNote;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("empRetirementRequestBean")
@Scope("session")
public class EmpRetirementRequestBean extends CommonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2609026927926625284L;

	final String NORMAL_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listNormRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_CREATE_PAGE = "/pages/admin/retirement/requestNormalRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_VIEW_PAGE = "/pages/admin/retirement/viewNormalRetirement.xhtml?faces-redirect=true";
	final String NORMAL_RETIREMENT_APPROVAL_PAGE = "/pages/admin/retirement/approveNormalRetirement.xhtml?faces-redirect=true";

	final String VOLUNTARY_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listVoluntaryRetirement.xhtml?faces-redirect=true";
	final String VOLUNTARY_RETIREMENT_CREATE_PAGE = "/pages/admin/retirement/requestVoluntaryRetirement.xhtml?faces-redirect=true";
	final String VOLUNTARY_RETIREMENT_APPROVE_PAGE = "/pages/admin/retirement/approveVoluntaryRetirement.xhtml?faces-redirect=true";

	final String COMPULSORY_RETIREMENT_LIST_PAGE = "/pages/admin/retirement/listCompulsoryRetirement.xhtml?faces-redirect=true";
	final String COMPULSORY_RETIREMENT_CREATE_PAGE = "";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String action, focusProperty;

	@Getter
	@Setter
	int totalRecords = 0, normalRetirementTotalRecords = 0, voluntaryRetirementTotalRecords,
			compulsoryRetirementTotalRecords;

	@Getter
	@Setter
	EmpRetirementRequest normalEmployeeRetirement, voluntaryEmployeeRetirement, compulsoryEmployeeRetirement,
			selectedNormalRetirement, selectedVoluntaryRetirement, voluntaryRetirement, selectedCompulsoryRetirement;

	@Getter
	@Setter
	List<EmpRetirementRequest> employeeRetirementList;

	@Getter
	@Setter
	List<EmpRetirementRequest> selectedNormalEmployeeRetirementList;

	@Getter
	@Setter
	LazyDataModel<EmpRetirementRequest> voluntaryEmployeeRetirementListLazy, compulsoryEmployeeRetirementListLazy;

	@Getter
	@Setter
	LazyDataModel<EmpRetirementListDto> normalEmployeeRetirementListLazy;

	@Getter
	@Setter
	List<EmpRetirementListDto> empList;

	@Getter
	@Setter
	List<EntityMaster> selectedEntityList = new ArrayList<>();

	@Autowired
	HttpService httpService;

	@Autowired
	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	String entitytip = "No regions selected";

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	Boolean addButtonFlag = true, viewButtonFlag = true, approveButtonFlag = true, finalApproval;

	@Getter
	@Setter
	private String documentFileName;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	List<Long> selectedEntityTypeList = new ArrayList<>();

	@Getter
	@Setter
	EmpRetirementRequestDTO empRetirementRequestDTO = new EmpRetirementRequestDTO();

	@Getter
	@Setter
	private Date startDate;

	@Getter
	@Setter
	UserMaster forwardTo;

	@Getter
	@Setter
	private Date endDate;

	@Getter
	@Setter
	EmpRetirementRequestNote empRetirementRequestNote = new EmpRetirementRequestNote();

	@Getter
	@Setter
	List<EmployeeMaster> empRetirementList;

	@Getter
	@Setter
	EmpRetirementListDto selectedNormalRetirementDto = new EmpRetirementListDto();

	@Getter
	@Setter
	Boolean buttonShowsCondition = false;

	@Getter
	@Setter
	Boolean approveButtonAction = true;

	@Getter
	@Setter
	Boolean editOption = true;

	@Getter
	@Setter
	EmpRetirementRequestDTO viewEmpRetirementRequestDTO = new EmpRetirementRequestDTO();
	
	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	Date maxDate;

	@Getter
	@Setter
	Boolean noteButtonAction = false;

	@Getter
	@Setter
	EmpGeneralInfoDto empGeneralInfoDto = new EmpGeneralInfoDto();

	@Getter
	@Setter
	List<EmpRetirementRequestDTO> empRetirementRequestDtoList = new ArrayList<EmpRetirementRequestDTO>();

	@Getter
	@Setter
	Long employeeId;

	@Getter
	@Setter
	EmployeeRetirement compulsoryRetirement = new EmployeeRetirement();

	@Getter
	@Setter
	EmployeeMaster employee = new EmployeeMaster();

	@Getter
	@Setter
	Boolean createEmployeePage = false;

	@Getter
	@Setter
	String remarks = "";
	
	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = null;
	
	public String showRetirementListPage() {
		log.info("<===== Starts EmployeeRetirementBean.showRetirementListPage ============>" + action);
		addButtonFlag = true;
		selectedNormalRetirementDto = new EmpRetirementListDto();
		selectedNormalRetirement = new EmpRetirementRequest();
		empRetirementList = new ArrayList<>();
		selectedEntityTypeList = new ArrayList<>();
		totalRecords = 0;
		loadNormalRetirementLazyList();
		if (action.equals("VOLUNTARY_RETIREMENT_LIST")) {
			return VOLUNTARY_RETIREMENT_LIST_PAGE;
		} else if (action.equals("COMPULSORY_RETIREMENT_LIST")) {
			return COMPULSORY_RETIREMENT_LIST_PAGE;
		} else {
			return NORMAL_RETIREMENT_LIST_PAGE;
		}
	}

	public String retirementAction() {
		log.info("<===== Starts EmployeeRetirementBean.retirementAction ============>" + action);
		employeeMaster = new EmployeeMaster();
		employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
		if (action.equalsIgnoreCase("NORMAL_RETIREMENT_CREATE")) {
			entityMasterList = commonDataService.loadHeadAndRegionalOffice();
			normalEmployeeRetirement = new EmpRetirementRequest();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			selectedEntityTypeList = new ArrayList<>();
			empRetirementRequestDtoList = new ArrayList<EmpRetirementRequestDTO>();
			totalRecords = 0;
			startDate = null;
			endDate = null;
			empRetirementRequestNote.setForwardTo(null);
			empRetirementRequestNote.setFinalApproval(null);
			empRetirementRequestNote.setNote(null);
			//forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.NORMAL_RETIREMENT.name());
			return NORMAL_RETIREMENT_CREATE_PAGE;
		}
		if (action.equalsIgnoreCase("VOLUNTARY_RETIREMENT_CREATE")) {
			voluntaryEmployeeRetirement = new EmpRetirementRequest();
			employeeRetirementList = new ArrayList<>();
			selectedEntityList = new ArrayList<>();
			totalRecords = 0;
			return VOLUNTARY_RETIREMENT_CREATE_PAGE;
		} else if (action.equalsIgnoreCase("NORMAL_RETIREMENT_VIEW")
				|| action.equalsIgnoreCase("NORMAL_RETIREMENT_APPROVAL")) {
			if (selectedNormalRetirement == null || selectedNormalRetirement.getId() == null) {
				errorMap.notify(ErrorDescription.EMP_RETIREMENT_NORAML_SELECT_EMPTY.getCode());
				return null;
			}
		}
		log.info("<===== Ends EmployeeRetirementBean.retirementAction ============>");
		return null;
	}

	public void changeEntity() {
		entitytip = "";
		for (EntityMaster entity : entityMasterList) {
			if (selectedEntityTypeList.contains(entity.getId())) {
				if (entitytip != "") {
					entitytip = entitytip + ", ";
				}
				entitytip = entitytip + entity.getName();
			}
		}
	}

	public void searchEmployeeList() {
		log.info("<===== Starts EmployeeRetirementBean.searchEmployeeList ============>" + action);
		log.info("EmpRetirementRequestDTO > " + empRetirementRequestDTO);
		try {
			log.info("Entity List - " + selectedEntityTypeList.size());
			validateNormalRetirementSearch();
			if (startDate == null) {
				log.info("===========Start Date is Empty========");
				errorMap.notify(ErrorDescription.STARTDATE_EMPTY.getErrorCode());
			}
			if (endDate == null) {
				log.info("===========End Date is Empty========");
				errorMap.notify(ErrorDescription.ENDDATE_EMPTY.getErrorCode());
			}
			if (startDate != null && endDate != null) {
				empRetirementRequestDTO.setStartDate(startDate);
				empRetirementRequestDTO.setEndDate(endDate);
				empRetirementRequestDTO.setRegionId(selectedEntityTypeList);
				BaseDTO response = httpService.post(SERVER_URL + "/employee/retirementrequest/generate/list",
						empRetirementRequestDTO);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					empRetirementRequestDtoList = mapper.readValue(jsonResponse,
							new TypeReference<List<EmpRetirementRequestDTO>>() {
							});

					if (!empRetirementList.isEmpty())
						totalRecords = empRetirementList.size();
					log.info("::EmployeeRetirementSize::" + totalRecords);

				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					log.error("response :::" + response);
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while search ......" + e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<===== Ends EmployeeRetirementBean.searchEmployeeList ============>");
	}

	public void validateNormalRetirementSearch() {

		if (selectedEntityTypeList == null && selectedEntityTypeList.size() == 0) {
			log.info("Regions list empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_REGION_LIST_EMPTY.getCode());
			return;
		}
		log.info("Entity List - " + selectedEntityTypeList.size());
		if (startDate == null) {
			log.info("from date is empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_FROMDATE_NULL.getCode());
			return;
		}
		if (endDate == null) {
			log.info("todate is empty.");
			errorMap.notify(ErrorDescription.RETIREMENT_TODATE_NULL.getCode());
			return;
		}

	}

	public void loadNormalRetirementLazyList() {
		normalEmployeeRetirementListLazy = new LazyDataModel<EmpRetirementListDto>() {

			private static final long serialVersionUID = 2849181192334586867L;

			@Override
			public List<EmpRetirementListDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				PaginationDTO paginationRequestDto = new PaginationDTO(first / pageSize, pageSize, sortField,
						sortOrder.toString(), filters);
				try {
					paginationRequestDto.setRetirementType(RetirementType.NORMAL);
					log.info("Pagination request is ........" + paginationRequestDto);
					BaseDTO responseDto = new BaseDTO();
					if (action.equals("VOLUNTARY_RETIREMENT_LIST")) {
						responseDto = httpService.post(SERVER_URL + "/employee/retirementrequest/vrslazyload",
								paginationRequestDto);
					} else if (action.equals("COMPULSORY_RETIREMENT_LIST")) {
						responseDto = httpService.post(SERVER_URL + "/employee/retirementrequest/compulsorylazyload",
								paginationRequestDto);
					} else {
						responseDto = httpService.post(SERVER_URL + "/employee/retirementrequest/lazyload",
								paginationRequestDto);
					}

					if (responseDto != null && responseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(responseDto.getResponseContents());
						empList = mapper.readValue(jsonResponse, new TypeReference<List<EmpRetirementListDto>>() {
						});
						if(empList!=null && !empList.isEmpty()) {
						log.info("emplist" + empList.get(0).toString());
						}
						this.setRowCount(responseDto.getTotalRecords());
						if (action.equals("VOLUNTARY_RETIREMENT_LIST")) {
							voluntaryRetirementTotalRecords = responseDto.getTotalRecords();
						} else if (action.equals("COMPULSORY_RETIREMENT_LIST")) {
							compulsoryRetirementTotalRecords = responseDto.getTotalRecords();
						} else {
							normalRetirementTotalRecords = responseDto.getTotalRecords();
						}

						normalRetirementTotalRecords = responseDto.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception in normalEmployeeRetirementListLazy ..", e);
				}
				return empList;
			}

			@Override
			public Object getRowKey(EmpRetirementListDto object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmpRetirementListDto getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmpRetirementListDto emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	public String submit() {
		log.info("<===== Starts EmployeeRetirementRequestBean.submit ============>" + action);
		try {
			if (action.equalsIgnoreCase("NORMAL_RETIREMENT")) {
				if (empRetirementRequestDtoList == null || empRetirementRequestDtoList.size() == 0) {
					log.info("Employee list is empty");
					errorMap.notify(ErrorDescription.RETIREMENT_EMP_LIST_EMPTY.getCode());
					return null;
				}
				if(CollectionUtils.isNotEmpty(empRetirementRequestDtoList)) {
					boolean isSelected = empRetirementRequestDtoList.stream()
							.anyMatch(t -> t.getRetirementEmployees().equals(true));
					if(!isSelected) {
						errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
						return null;
					}
				}
				if (empRetirementRequestNote.getForwardTo() == null) {
					log.info("====Employee New Designation is Empty Value=====");
					errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
					return null;
				}

				if (empRetirementRequestNote.getFinalApproval() == null) {
					log.info("====Employee New Designation is Empty Value=====");
					errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
					return null;
				}
				if (empRetirementRequestNote.getNote() == null) {
					log.info("====Employee New Designation is Empty Value=====");
					errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
					return null;
				}

				empRetirementRequestDTO.setEmpRetirementRequestNote(empRetirementRequestNote);
				empRetirementRequestDTO.setEmployeeRetirementList(empRetirementList);
				empRetirementRequestDTO.setEmpRetirementRequestDTOList(empRetirementRequestDtoList);
				log.info("::employereteriementrequest list::" + empRetirementList);
				BaseDTO response = httpService.post(SERVER_URL + "/employee/retirementrequest/create/list",
						empRetirementRequestDTO);
				if (response != null && response.getStatusCode() == 0) {
					errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_SAVED_SUCCESSFULLY.getCode());
					action = "NORMAL_RETIREMENT_LIST";
					return showRetirementListPage();
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					log.info("Response ......" + response);
				}
				return null;
			} else if (action.equalsIgnoreCase("VOLUNTARY_RETIREMENT")) {
				voluntaryEmployeeRetirement = new EmpRetirementRequest();
				return VOLUNTARY_RETIREMENT_CREATE_PAGE;
			} else if (action.equalsIgnoreCase("COMPULSORY_RETIREMENT")) {
				compulsoryEmployeeRetirement = new EmpRetirementRequest();
				return COMPULSORY_RETIREMENT_CREATE_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured in submit....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<===== Ends EmployeeRetirementBean.submit ============>");
		return null;
	}

	public void updateTodate() {
		normalEmployeeRetirement.setEndDate(null);
		Date newDate = DateUtils.addMonths(startDate, 1);
		maxDate = newDate;
	}

	public String clear() {
		log.info("<===Starts EmpRetirementRequestBean.clear ========>");
		selectedNormalRetirementDto = new EmpRetirementListDto();
		log.info("<===Starts EmpRetirementRequestBean.clear ========>");
		return showRetirementListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmpRetirementRequestBean.onRowSelect ========>" + event);
		selectedNormalRetirementDto = ((EmpRetirementListDto) event.getObject());
		if (selectedNormalRetirementDto.getStatus().equals(ApprovalStage.FINAL_APPROVED)) {
			addButtonFlag = false;
		} else if (selectedNormalRetirementDto.getStatus().equals(ApprovalStage.REJECTED)) {
			addButtonFlag = false;

		} else if (selectedNormalRetirementDto.getStatus().equals(ApprovalStage.APPROVED)) {

			addButtonFlag = false;
		} else {
			addButtonFlag = false;
		}
		log.info("<===Ends EmpRetirementRequestBean.onRowSelect ========>");
	}

	public String viewStatus() {
		log.info("<===Starts EmpRetirementRequestBean.viewStatus ========>");
		try {

			if (selectedNormalRetirementDto == null) {
				log.info("==============Selected Retirement Dto value is Empty===========");
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			//forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.NORMAL_RETIREMENT.name());
			String url = SERVER_URL + "/employee/retirementrequest/getbyid/" + selectedNormalRetirementDto.getId()+"/"+(selectedNotificationId!=null?selectedNotificationId:0);
			log.info("<=EmpRetirementRequestBean :: viewEmployeePromotion :: url=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewEmpRetirementRequestDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmpRetirementRequestDTO>() {
						});
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo() != null) {
					if (loginBean.getUserDetailSession().getId()
							.equals(viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo().getId())) {
						buttonShowsCondition = true;
						approveButtonAction = true;
					}else {
						buttonShowsCondition = false;
						approveButtonAction = false;
					}
				}
				if (selectedNormalRetirementDto.getStatus().equals(ApprovalStage.SUBMITTED)) {
					noteButtonAction = true;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo() == null) {
					buttonShowsCondition = true;
					approveButtonAction = true;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getFinalApproval() != null
						&& viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getFinalApproval()) {
					approveButtonAction = false;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestLog().getStage()
						.equals(ApprovalStage.FINAL_APPROVED)) {
					buttonShowsCondition = false;
				}
				empRetirementRequestDTO.setNote(viewEmpRetirementRequestDTO.getNote());
				empRetirementRequestDTO.setCommencementNoticePeriod(viewEmpRetirementRequestDTO.getCommencementNoticePeriod());
				empRetirementRequestDTO.setRetirementRemarks(viewEmpRetirementRequestDTO.getRetirementRemarks());
				empRetirementRequestDTO.setOtherDues(viewEmpRetirementRequestDTO.getOtherDues());
				empRetirementRequestDTO.setViewLogList(viewEmpRetirementRequestDTO.getViewLogList());
				empRetirementRequestDTO.setUserMaster(new UserMaster());
				systemNotificationBean.loadTotalMessages();
				
				if (action.equals("VOLUNTARY_RETIREMENT_LIST")) {
					if (!viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().isEmpty()) {
						getEmpGeneralInfo(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().get(0)
								.getEmployee().getId());
					}
					return "/pages/admin/retirement/viewVoluntaryRetirement.xhtml?faces-redirect=true;";
				} else if (action.equals("COMPULSORY_RETIREMENT_LIST")) {
					if (!viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().isEmpty()) {
						getEmpGeneralInfo(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().get(0)
								.getEmployee().getId());
					}
					return "/pages/admin/retirement/viewCompulsoryRetirement.xhtml?faces-redirect=true;";
				} else {
					return "/pages/admin/retirement/viewNormRetirement.xhtml?faces-redirect=true;";
				}

			}
		} catch (Exception e) {
			log.error(" Exception Occured While EmpRetirementRequestBean.viewStatus :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String approveReject() {
		log.info("<===Starts EmpRetirementRequestBean.approveReject ========>");
		if (empRetirementRequestDTO.getApproveReject().equals("APPROVE")) {
			if (empRetirementRequestDTO.getUserMaster() == null 
					&& empRetirementRequestDTO.getUserMaster().getId() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (empRetirementRequestDTO.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
		}
		if (empRetirementRequestDTO.getApproveReject().equals("REJECT")) {
			if (empRetirementRequestDTO.getNote() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}
		}
		String url = SERVER_URL + "/employee/retirementrequest/approvereject";
		log.info("approveReject Url is==================>>>>>>>>>>>>" + url);
		empRetirementRequestDTO.setNote(empRetirementRequestDTO.getNote());
		empRetirementRequestDTO.setRemarks(remarks);
		empRetirementRequestDTO.setId(selectedNormalRetirementDto.getId());
		empRetirementRequestDTO
				.setEmpRetirementRequestDetailsList(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList());
		if(empRetirementRequestDTO.getUserMaster() != null && empRetirementRequestDTO.getUserMaster().getId() != null) {
			empRetirementRequestDTO.setUserId(empRetirementRequestDTO.getUserMaster().getId());
		} else {
			empRetirementRequestDTO.setUserId(null);
		}
		BaseDTO response = httpService.post(url, empRetirementRequestDTO);
		if (response != null && response.getStatusCode() == 0) {
			if (empRetirementRequestDTO.getApproveReject().equals("REJECT")) {
				errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_REJECTED_SUCCESSFULLY.getErrorCode());
			} else {
				errorMap.notify(ErrorDescription.RETIREMENT_NORMAL_APPROVED_SUCCESSFULLY.getErrorCode());
			}
			String retirementType = response.getGeneralContent();
			if(!StringUtils.isEmpty(retirementType)) {
				if(in.gov.cooptex.core.enums.RetirementType.VRS.equals(retirementType)) {
					action = "VOLUNTARY_RETIREMENT_LIST";
				} else if(in.gov.cooptex.core.enums.RetirementType.COR.equals(retirementType)) {
					action = "COMPULSORY_RETIREMENT_LIST";
				} else {
					action = "NORMAL_RETIREMENT_LIST";
				}
			}
			return clear();
		} else if (response != null && response.getStatusCode() != 0) {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===End EmpRetirementRequestBean.approveReject ========>");
		return null;
	}

	public String clearPage() {
		log.info("<===Start EmpRetirementRequestBean.clearPage ========>");
		selectedNormalRetirementDto = new EmpRetirementListDto();
		log.info("<===End EmpRetirementRequestBean.clearPage ========>");
		return showRetirementListPage();
	}

	public String compulsoryClearPage() {
		log.info("<===Start EmpRetirementRequestBean.clearPage ========>");
		selectedNormalRetirementDto = new EmpRetirementListDto();
		action = "COMPULSORY_RETIREMENT_LIST";
		log.info("<===End EmpRetirementRequestBean.clearPage ========>");
		return showRetirementListPage();
	}

	public void getEmpGeneralInfo(Long employeeId) {
		log.info("<===Starts EmpRetirementRequestBean.getEmpGeneralInfo ========>");
		try {
			String url = SERVER_URL + "/employee/retirementrequest/employeegeneralinfo/" + employeeId;
			log.info("<=EmpRetirementRequestBean :: getEmpGeneralInfo :: url=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empGeneralInfoDto = mapper.readValue(jsonResponse, new TypeReference<EmpGeneralInfoDto>() {
				});
			}
		} catch (Exception e) {
			log.info("<===Exception Occured in EmpRetirementRequestBean.getEmpGeneralInfo ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.getEmpGeneralInfo ========>");
	}
	

	public void getEmpGeneralInfoForVoluntary(Long employeeId) {
		log.info("<===Starts EmpRetirementRequestBean.getEmpGeneralInfo ========>");
		try {
			String url = SERVER_URL + "/employee/retirementrequest/employeegeneralinfovoluntary/" + employeeId;
			log.info("<=EmpRetirementRequestBean :: getEmpGeneralInfo :: url=>" + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				empGeneralInfoDto = mapper.readValue(jsonResponse, new TypeReference<EmpGeneralInfoDto>() {
				});
			}
		} catch (Exception e) {
			log.info("<===Exception Occured in EmpRetirementRequestBean.getEmpGeneralInfo ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.getEmpGeneralInfo ========>");
	}

	public String showEmployeeGeneralInformation() {

		if (selectedNormalRetirementDto == null) {
			log.info("==============Selected Retirement Dto value is Empty===========");
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			return null;
		}
		log.info("<===Starts EmpRetirementRequestBean.showEmployeeGeneralInformation ========>");
		try {

			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.NORMAL_RETIREMENT.name());
			String url = SERVER_URL + "/employee/retirementrequest/getbyid/" + selectedNormalRetirementDto.getId();
			log.info("<=EmpRetirementRequestBean :: viewEmployeePromotion :: url=>" + url);
			
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				viewEmpRetirementRequestDTO = mapper.readValue(jsonResponse,
						new TypeReference<EmpRetirementRequestDTO>() {
						});
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo() != null) {
					if (loginBean.getUserDetailSession().getId()
							.equals(viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo().getId())) {
						buttonShowsCondition = true;
						approveButtonAction = true;
					}
				}
				if (selectedNormalRetirementDto.getStatus().equals(ApprovalStage.SUBMITTED)) {
					noteButtonAction = true;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getForwardTo() == null) {
					buttonShowsCondition = true;
					approveButtonAction = true;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getFinalApproval() != null
						&& viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getFinalApproval()) {
					approveButtonAction = false;
				}
				if (viewEmpRetirementRequestDTO.getEmpRetirementRequestLog().getStage()
						.equals(ApprovalStage.FINAL_APPROVED)) {
					buttonShowsCondition = false;
				}
				empRetirementRequestDTO.setNote(viewEmpRetirementRequestDTO.getEmpRetirementRequestNote().getNote());
				if (action.equals("VOLUNTARY_RETIREMENT_LIST")) {
					if (!viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().isEmpty()) {
						getEmpGeneralInfo(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().get(0)
								.getEmployee().getId());
					}
					return "/pages/admin/retirement/viewVoluntaryRetirement.xhtml?faces-redirect=true;";
				} else if (action.equals("COMPULSORY_RETIREMENT_LIST")) {
					if (!viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().isEmpty()) {
						getEmpGeneralInfo(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().get(0)
								.getEmployee().getId());
					}
					return "/pages/admin/retirement/viewCompulsoryRetirement.xhtml?faces-redirect=true;";
				} else if (action.equals("VOLUNTARY_GENERAL_INFO")) {
					getEmpGeneralInfoForVoluntary(viewEmpRetirementRequestDTO.getEmpRetirementRequestDetailsList().get(0)
							.getEmployee().getId());
					return VOLUNTARY_RETIREMENT_APPROVE_PAGE;
				} else {
					return "/pages/admin/retirement/viewNormRetirement.xhtml?faces-redirect=true;";
				}
			}

		} catch (Exception e) {
			log.info("<===  Exception Occured In showEmployeeGeneralInformation ========>", e);
		}

		// viewStatus();
		log.info("<===End EmpRetirementRequestBean.showEmployeeGeneralInformation ========>");
		return null;

	}

	public String showCompulsoryRetirement() {
		log.info("<===Starts EmpRetirementRequestBean.showCompulsoryRetirement ========>");
		try {

		} catch (Exception e) {
			log.info("<===Excepton Occured in EmpRetirementRequestBean.showCompulsoryRetirement ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.showCompulsoryRetirement ========>");
		return COMPULSORY_RETIREMENT_CREATE_PAGE;
	}

	public void getEmployeeDetails(String search) {
		log.info("[ Get employee details ]");
		try {

			BaseDTO baseDTO = null;
			// For Employee Compulsory Retirement
			if (search.equals(RetirementType.COMPULSORY.name())) {
				if (Objects.isNull(compulsoryRetirement.getEmployee())
						&& StringUtils.isEmpty(compulsoryRetirement.getEmployee().getEmpCode())) {
					log.error("[ Selected employee code is ]" + compulsoryRetirement.getEmployee());
					return;
				}
				log.info("[ Selected employee code is : ]" + compulsoryRetirement.getEmployee().getEmpCode());
				log.info("=======GET URL is ======" + SERVER_URL + "/employee/retirement/get/employee/");
				baseDTO = httpService.get(SERVER_URL + "/employee/retirement/get/employeebypfno/"
						+ compulsoryRetirement.getEmployee().getEmpCode());
			}

			if (Objects.isNull(baseDTO)) {
				log.info("[ Employee details response dto is : ]" + baseDTO);
				return;
			} else {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employee = mapper.readValue(jsonResponse, EmployeeMaster.class);
				if(employee==null)
				{
					empGeneralInfoDto = new EmpGeneralInfoDto();
					Util.addWarn("Employee not found");
				}
				log.info("[ Employee details response is : ]" + employee);
				if (employee!=null && employee.getId() != null) {
					employeeId = employee.getId();
					getEmpGeneralInfo(employeeId);
				}
			}
		} catch (Exception e) {
			log.error("[ Exception while get employee details ]", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public String createCompulsory() {
		log.info("<===Starts EmpRetirementRequestBean.createCompulsory ========>");
		try {
			empGeneralInfoDto = new EmpGeneralInfoDto();
			compulsoryRetirement = new EmployeeRetirement();
			empRetirementRequestNote = new EmpRetirementRequestNote();
			employeeMaster = new EmployeeMaster();
			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			//forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.NORMAL_RETIREMENT.name());
		} catch (Exception e) {
			log.info("<===Excetion Occured in EmpRetirementRequestBean.createCompulsory ========>");
			log.info("<===Excetion is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.createCompulsory ========>");
		return "/pages/admin/retirement/createCompulsoryRetirement.xhtml?faces-redirect=true";
	}

	public String saveEmployeeCompulsoryRetirement() {
		log.info("<===Starts EmpRetirementRequestBean.saveEmployeeCompulsoryRetirement ========>");
		try {
			if (empRetirementRequestNote.getForwardTo() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_TO.getErrorCode());
				return null;
			}

			if (empRetirementRequestNote.getFinalApproval() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.PLEASE_SELECT_FORWARD_FOR.getErrorCode());
				return null;
			}
			if (empRetirementRequestNote.getNote() == null) {
				log.info("====Employee New Designation is Empty Value=====");
				errorMap.notify(ErrorDescription.BOARD_APPROVAL_REG_NOTE_EMPTY.getErrorCode());
				return null;
			}
			if (employee == null) {
				log.info("====Employee is Empty =====");
				errorMap.notify(ErrorDescription.EMP_NOT_FOUND.getErrorCode());
				return null;
			}
			empRetirementRequestDTO.setEmpRetirementRequestNote(empRetirementRequestNote);
			empRetirementRequestDTO.setEmployeeMaster(employee);
			String url = SERVER_URL + "/employee/retirementrequest/savecompulsory";
			log.info("<=EmpRetirementRequestBean :: saveEmployeeCompulsoryRetirement :: url=>" + url);
			BaseDTO baseDTO = httpService.post(url, empRetirementRequestDTO);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.RETIREMENT_COMPULSORY_SAVED_SUCCESSFULLY.getCode());
				return "/pages/admin/retirement/listCompulsoryRetirement.xhtml?faces-redirect=true";
			}
		} catch (Exception e) {
			log.info("<===Exception Occured in EmpRetirementRequestBean.saveEmployeeCompulsoryRetirement ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===Starts EmpRetirementRequestBean.saveEmployeeCompulsoryRetirement ========>");
		return null;
	}

	public void showNormalRetirementEmployeeGeneralInfo(Long empId) {
		log.info("<===Starts EmpRetirementRequestBean.showNormalRetirementEmployeeGeneralInfo ========>");
		log.info("<===Employee Master is ========>" + empId);
		try {
			if (empId != null) {
				getEmpGeneralInfo(empId);
			}
		} catch (Exception e) {
			log.info(
					"<===Exception Occured in EmpRetirementRequestBean.showNormalRetirementEmployeeGeneralInfo ========>");
			log.info("<===Exception is ========>" + e);
		}
		log.info("<===End EmpRetirementRequestBean.showNormalRetirementEmployeeGeneralInfo ========>");
	}
	

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts EmployeeIncrement-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String retirementId = "";
		String status = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			retirementId = httpRequest.getParameter("retirementid");
			status = httpRequest.getParameter("status");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (retirementId != null) {
			log.info("incrementId----------------->"+retirementId);
			Long fileId = Long.parseLong(retirementId);
			selectedNormalRetirementDto = new EmpRetirementListDto();
			selectedNormalRetirementDto.setId(fileId);
			selectedNormalRetirementDto.setStatus(status);
			selectedNotificationId = Long.parseLong(notificationId);
			action = "VOLUNTARY_RETIREMENT_LIST";
			viewStatus();
		}
		return null;
	}

}
