package in.gov.cooptex.operation.rest.ui;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EventLog;
import in.gov.cooptex.core.dto.GSTSummaryDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.ProductSelectionVaritiesDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.UomMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LanguageBean;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AmountToWordConverter;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.TaxCalculationUtil;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.operation.dto.GstSummeryDTO;
import in.gov.cooptex.operation.dto.HsnCodeTaxPercentageWiseDTO;
import in.gov.cooptex.operation.dto.ItemDataWithGstDTO;
import in.gov.cooptex.operation.enums.OrderStatus;
import in.gov.cooptex.operation.enums.ProcurementOrderStatus;
import in.gov.cooptex.operation.model.AdditionalProductionPlan;
import in.gov.cooptex.operation.model.ContractExportPlan;
import in.gov.cooptex.operation.model.GovtSchemePlan;
import in.gov.cooptex.operation.model.GovtSocietyPlan;
import in.gov.cooptex.operation.model.PurchaseOrder;
import in.gov.cooptex.operation.model.PurchaseOrderItems;
import in.gov.cooptex.operation.model.PurchaseOrderLog;
import in.gov.cooptex.operation.model.PurchaseOrderNote;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.dto.PurchaseOrderSearch;
import in.gov.cooptex.operation.production.dto.QuotationSelectionRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
@Scope("session")
public class GovtPurchaseOrderBean extends CommonBean {

	private final String LIST_PAGE_URL = "/pages/operation/retailproduction/purchase/listSalesPurchaseOrder.xhtml?faces-redirect=true;";

	private final String ADD_PAGE_URL = "/pages/operation/retailproduction/purchase/createSalesPurchaseOrder.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/operation/retailproduction/purchase/viewSalesPurchaseOrder.xhtml?faces-redirect=true;";

	private final String PREVIEW_PAGE_URL = "/pages/operation/retailproduction/purchase/previewSalesPurchaseOrder.xhtml?faces-redirect=true;";

	/*
	 * @Autowired LanguageBean languageBean;
	 */

	@Autowired
	LoginBean loginBean;

	@Autowired
	HttpService httpService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	List<GovtSchemePlan> govtProductionPlanList;

	/*
	 * @Getter @Setter GovtSchemePlan govtSchemePlan;
	 */

	@Getter
	@Setter
	String SERVER_URL;

	@Getter
	@Setter
	String govtProductionPlan;

	/*
	 * @Getter @Setter EntityMaster entityMaster;
	 * 
	 * @Getter @Setter List<EntityMaster> entityMasterList;
	 */

	/*
	 * @Getter @Setter EntityMaster shippingEntity;
	 */

	/*
	 * @Getter @Setter EntityMaster billingEntity;
	 */

	@Getter
	@Setter
	List<EntityMaster> shippingEntityList;

	@Getter
	@Setter
	List<EntityMaster> billingEntityList;

	/*
	 * @Getter @Setter ProductVarietyMaster productVarietyMaster;
	 * 
	 * @Getter @Setter List<ProductVarietyMaster> productVarietyMasterList;
	 */

	@Getter
	@Setter
	List<PurchaseOrderItems> orderItemsList;

	@Getter
	@Setter
	List<PurchaseOrderItems> selectedOrderItemsList;

	@Getter
	@Setter
	PurchaseOrderItems purchaseOrderItems;

	@Setter
	@Getter
	UomMaster uomMaster;

	@Setter
	@Getter
	List<UomMaster> uomMasterList;

	/*
	 * @Getter @Setter GovtSchemePlanItems govtSchemePlanItems;
	 */
	/*
	 * @Getter @Setter SupplierMaster supplierMaster;
	 */

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	private Double totalMaterialValue = 0.00;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	PurchaseOrderNote planNote = new PurchaseOrderNote();

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	GovtSocietyPlan govtSocietyPlan;

	/*
	 * @Getter @Setter List statusValues;
	 */

	@Getter
	@Setter
	int size;

	@Getter
	@Setter
	String action = null, planTypeName = "gov";

	@Getter
	@Setter
	List<EventLog> eventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> rejectedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EventLog> approvedEventLogList = new ArrayList<>();

	@Getter
	@Setter
	private Boolean editFlag = false;

	@Getter
	@Setter
	private Date validityDate, deliveryDate;

	@Getter
	@Setter
	List<ProductSelectionVaritiesDTO> productSelectionVaritiesDTOList;

	@Getter
	@Setter
	List<GSTSummaryDTO> gstSummaryDTOList;

	@Getter
	@Setter
	List<String> productIdsList;

	@Getter
	@Setter
	QuotationSelectionRequest quotationSelectionRequest;

	@Getter
	@Setter
	ItemDataWithGstDTO quotationItemWithGstDTO;

	@Getter
	@Setter
	List<HsnCodeTaxPercentageWiseDTO> gstPercentageWiseDTOList;

	@Getter
	@Setter
	List<GstSummeryDTO> gstWiseList;

	@Getter
	@Setter
	Double totalTaxCgstSgstAmount;

	@Getter
	@Setter
	Double totalTaxMaterialAmount;

	@Getter
	@Setter
	String amountInWords;

	@Getter
	@Setter
	String dataToggle = "collapse in";

	@Getter
	@Setter
	Double cgstTaxValue;

	@Getter
	@Setter
	Double sgstTaxValue;

	@Getter
	@Setter
	Double totalCgstSgstTaxValue;

	@Getter
	@Setter
	PurchaseOrder purchaseOrder;

	@Getter
	@Setter
	Double materialValue;

	@Getter
	@Setter
	String[] dnpAddress;

	@Getter
	@Setter
	String[] societyAddress;

	@Getter
	@Setter
	String[] warehouseAddress;

	@Getter
	@Setter
	String[] headOfficeAddress;

	@Getter
	@Setter
	List<Object> statusValues;

	@Getter
	@Setter
	Double totalCgstAmount;

	@Getter
	@Setter
	Double totalSgstAmount;

	@Getter
	@Setter
	Double totalTaxAmount;

	@Autowired
	LanguageBean languageBean;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	LazyDataModel<PurchaseOrderSearch> purchaseOrderSearchLazyList;

	@Getter
	@Setter
	PurchaseOrderSearch purchaseOrderSearch;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	@Getter
	@Setter
	ContractExportPlan contractExportPlan = new ContractExportPlan();

	@Getter
	@Setter
	List<ContractExportPlan> contractExportPlanList = new ArrayList<>();

	@Getter
	@Setter
	List<AdditionalProductionPlan> additionalProductionPlanList = new ArrayList<>();

	@Getter
	@Setter
	AdditionalProductionPlan additionalProductionPlan = new AdditionalProductionPlan();

	@Getter
	@Setter
	List<UserMaster> userMasters;
	@Getter
	@Setter
	Double totalAmount;
	@Getter
	@Setter
	Integer unit;

	@Getter
	@Setter
	private Boolean validateFlag = false;

	@PostConstruct
	public void init() {
		log.info("Inside init()>>>>>>>>>");
		govtProductionPlan = null;
		// govtSchemePlan = null;
		editFlag = false;
		loadUrl();
		loadStatusValues();
		// loadForwardToUsers();
		userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.PURCHASE_ORDER.toString());
		log.info("userMasters size" + userMasters.size());
		loadEmployeeLoggedInUserDetails();
		loadBillingAddressEntityList();
	}

	private void loadUrl() {
		try {
			SERVER_URL = appPreference.getPortalServerURL();
		} catch (Exception e) {
			log.fatal("Exception at loadUrl() ", e);
		}
	}

	private void loadStatusValues() {
		Object[] statusArray = OrderStatus.class.getEnumConstants();
		statusValues = new ArrayList<>();
		for (Object status : statusArray) {
			statusValues.add(status);
		}
	}

	public void loadForwardToUsers() {
		log.info("Inside loadForwardToUsers() ");
		try {
			forwardToUsersList = commonDataService.loadForwardToUsersByFeature("PURCHASE_ORDER");
			if (forwardToUsersList != null && !forwardToUsersList.isEmpty()) {
				log.info("forwardToUsersList size==> " + forwardToUsersList.size());
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadEmployeeLoggedInUserDetails() {
		log.info("Inside loadEmployeeLoggedInUserDetails()>>>>>>>>> ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ loginBean.getUserMaster().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + loginBean.getUserMaster().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void loadGovtProductionPlanApprovedPlanList() {
		log.info("loadGovtProductionPlanApprovedPlanList :: govtProductionPlan==>" + govtProductionPlan);
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			loadGovtSchemePlanApprovedPlanList();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Contract")) {
			planTypeName = "contract";
			loadContractPlanList(govtProductionPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			planTypeName = "export";
			loadContractPlanList(govtProductionPlan);
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			planTypeName = "additional";
			loadAdditionalPlanList();
		} else {
			govtProductionPlanList = new ArrayList<GovtSchemePlan>();
		}
		shippingEntityList = new ArrayList<>();
		loadShippingAddressEntityList(govtProductionPlan);
	}

	@SuppressWarnings("unchecked")
	public void loadAdditionalPlanList() {
		log.info("<--- Inside loadAdditionalList() ---> ");
		try {

			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			supplierMasterList = new ArrayList<SupplierMaster>();
			String url = SERVER_URL + "/govt/societyPlan/getcontactadditionalplanlist";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					additionalProductionPlanList = (List<AdditionalProductionPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<AdditionalProductionPlan>>() {
							});
					if (additionalProductionPlanList != null) {
						log.info("AdditionalProductionPlanList with Size of : " + additionalProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadAdditionalList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					additionalProductionPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadContractPlanList(String planType) {
		log.info("<--- Inside loadContactExportPlanApprovedPlanList() ---> ");
		try {

			uomMaster = null;
			uomMasterList = new ArrayList<UomMaster>();
			supplierMasterList = new ArrayList<SupplierMaster>();
			String url = SERVER_URL + "/govt/societyPlan/getcontactexportplanlist/" + planType;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContents() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					contractExportPlanList = (List<ContractExportPlan>) mapper.readValue(jsonValue,
							new TypeReference<List<ContractExportPlan>>() {
							});
					if (contractExportPlanList != null) {
						log.info("contractExportPlanList with Size of : " + contractExportPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" loadContactExportPlanApprovedPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					contractExportPlanList = new ArrayList<>();
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				contractExportPlanList = new ArrayList<>();
				log.info("<--- Requested Service Error in loadContactExportPlanApprovedPlanList() ---> ");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadContactExportPlanApprovedPlanList() --->", e);
		}
	}

	/*
	 * public void loadContractPlanList() { try { sss uomMaster = null;
	 * uomMasterList = new ArrayList<UomMaster>(); supplierMasterList = new
	 * ArrayList<SupplierMaster>(); if (action.equals("CREATE")) {
	 * totalMaterialValue = 0.00; } String requestPath = SERVER_URL +
	 * "/govt/societyPlan/getGovtproductionplanlist"; }catch(Exception e) {
	 * log.error("loadContractPlanList exception==>",e); } }
	 */

	@SuppressWarnings("unchecked")
	public void loadGovtSchemePlanApprovedPlanList() {
		log.info("<--- Inside loadRetailProductionPlanFinalApprovedList() ---> ");
		uomMaster = null;
		uomMasterList = new ArrayList<UomMaster>();
		supplierMasterList = new ArrayList<SupplierMaster>();
		if (action.equals("CREATE"))
			totalMaterialValue = 0.00;
		String requestPath = SERVER_URL + "/govt/societyPlan/getGovtproductionplanlist";

		try {
			planTypeName = "gov";
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					govtProductionPlanList = (List<GovtSchemePlan>) mapper.readValue(jsonValue,
							new TypeReference<List<GovtSchemePlan>>() {
							});

					if (govtProductionPlanList != null) {
						log.info("govtProductionPlanList with Size of : " + govtProductionPlanList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" govtProductionPlanList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadRetailProductionPlanFinalApprovedList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadRetailProductionPlanFinalApprovedList() --->", e);
		}
	}

	public void loadSocietyList() {

	}

	@SuppressWarnings("unchecked")
	public void loadShippingAddressEntityList(String planType) {
		log.info("<--- Inside loadShippingAddressEntityList() ---> ");

		try {
			if (planType != null) {
				log.info("plantype----------->" + planType);
				String requestPath = SERVER_URL + "/govtPurchaseOrder/getShippingAddressEntityList/" + planType + "/"
						+ loginBean.getUserMaster().getId();
				log.info("requestPath : " + requestPath);
				BaseDTO baseDTO = httpService.get(requestPath);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContent() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						shippingEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
								new TypeReference<List<EntityMaster>>() {
								});

						if (shippingEntityList != null) {
							log.info("shippingEntityList with Size of : " + shippingEntityList.size());
						} else {
							errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
							log.info(" shippingEntityList is Empty ");
						}

					} else {
						errorMap.notify(baseDTO.getStatusCode());
						log.info(" Error Message: " + baseDTO.getErrorDescription());
					}
				}
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadShippingAddressEntityList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadBillingAddressEntityList() {
		log.info("<--- Inside loadBillingAddressEntityList() ---> ");
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getBillingAddressEntityList";

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					billingEntityList = (List<EntityMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<EntityMaster>>() {
							});

					if (billingEntityList != null) {
						log.info("billingEntityList with Size of : " + billingEntityList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" billingEntityList is Empty ");
					}

				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in loadBillingAddressEntityList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in loadBillingAddressEntityList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getSuppliers() {
		log.info("<--- Inside getSuppliers() ---> ");
		supplierMasterList = new ArrayList<SupplierMaster>();
		if (purchaseOrder.getGovtSchemePlan() == null || purchaseOrder.getGovtSchemePlan().getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getSuppliers/" + purchaseOrder.getGovtSchemePlan().getId()
				+ "/" + loginBean.getUserMaster().getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMasterList = (List<SupplierMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<SupplierMaster>>() {
							});

					if (supplierMasterList != null) {
						log.info("supplierMasterList size : " + supplierMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" supplierMasterList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getSuppliers() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getSuppliers() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getAddSuppliers() {
		log.info("<--- Inside getSuppliers() ---> ");
		supplierMasterList = new ArrayList<SupplierMaster>();
		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getaddsuppliers/" + additionalProductionPlan.getId() + "/"
				+ loginBean.getUserMaster().getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMasterList = (List<SupplierMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<SupplierMaster>>() {
							});

					if (supplierMasterList != null) {
						log.info("supplierMasterList size : " + supplierMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" supplierMasterList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getSuppliers() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getSuppliers() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getConSuppliers() {
		log.info("<--- Inside getSuppliers() ---> ");
		supplierMasterList = new ArrayList<SupplierMaster>();
		if (contractExportPlan == null || contractExportPlan.getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getconsuppliers/" + contractExportPlan.getId() + "/"
				+ loginBean.getUserMaster().getId();

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					supplierMasterList = (List<SupplierMaster>) mapper.readValue(jsonValue,
							new TypeReference<List<SupplierMaster>>() {
							});

					if (supplierMasterList != null) {
						log.info("supplierMasterList size : " + supplierMasterList.size());
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						log.info(" supplierMasterList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getSuppliers() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getSuppliers() --->", e);
		}
	}

	/*
	 * @SuppressWarnings("unchecked") public void getPurchaseOrdrItemsList(){
	 * log.info("<--- Inside getPurchaseOrderItemsList() ---> ");
	 * purchaseOrderItemsList = new ArrayList<PurchaseOrderItems>();
	 * if(govtSchemePlan == null || govtSchemePlan.getId() == null) {
	 * AppUtil.addWarn("Please Select Plan"); return; } if(supplierMaster == null ||
	 * supplierMaster.getId() == null) { AppUtil.addWarn("Please Select Plan");
	 * return; } String requestPath = SERVER_URL+
	 * "/govtPurchaseOrder/getPurchaseOrderItemsList/"+govtSchemePlan.getId()+"/"+
	 * supplierMaster.getId();
	 * 
	 * try { log.info("requestPath : "+requestPath); BaseDTO baseDTO =
	 * httpService.get(requestPath);
	 * 
	 * if (baseDTO != null) { ObjectMapper mapper = new ObjectMapper(); if
	 * (baseDTO.getResponseContent() != null) { String jsonValue =
	 * mapper.writeValueAsString(baseDTO.getResponseContent());
	 * purchaseOrderItemsList = (List<PurchaseOrderItems>)
	 * mapper.readValue(jsonValue, new TypeReference<List<PurchaseOrderItems>>()
	 * {});
	 * 
	 * if(purchaseOrderItemsList!=null ){ log.info("purchaseOrderItemsList size : "
	 * + purchaseOrderItemsList.size()); }else{
	 * errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
	 * log.info(" purchaseOrderItemsList is Empty "); } } else {
	 * errorMap.notify(baseDTO.getStatusCode()); log.info(" Error Message: " +
	 * baseDTO.getErrorDescription()); } } else {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
	 * log.info("<--- Requested Service Error in getPurchaseOrderItemsList() ---> "
	 * ); } } catch (Exception e) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
	 * log.error("<--- Exception in getPurchaseOrderItemsList() --->", e); } }
	 */

	public void getProductVaritiesList() {
		log.info("getProductVaritiesList  started");
		if (govtProductionPlan != null && !govtProductionPlan.equals("") && govtProductionPlan.equals("Govt_Scheme")) {
			getProductVaritiesGovList();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Contract")) {
			getProductVaritiesConList();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Export")) {
			getProductVaritiesConList();
		} else if (govtProductionPlan != null && !govtProductionPlan.equals("")
				&& govtProductionPlan.equals("Additional_Production_Plan")) {
			getProductVaritiesAddList();
		}
	}

	@SuppressWarnings("unchecked")
	public void getProductVaritiesAddList() {
		log.info("<--- Inside getProductVaritiesList() ---> ");
		orderItemsList = new ArrayList<PurchaseOrderItems>();
		if (additionalProductionPlan == null || additionalProductionPlan.getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		if (purchaseOrder == null || purchaseOrder.getSupplierMaster() == null
				|| purchaseOrder.getSupplierMaster().getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		log.info("action::::::::::" + action);
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getAddPurchaseOrderItemsList/"
				+ additionalProductionPlan.getId() + "/" + purchaseOrder.getSupplierMaster().getId() + "/" + action;

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productSelectionVaritiesDTOList = (List<ProductSelectionVaritiesDTO>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductSelectionVaritiesDTO>>() {
							});

					if (productSelectionVaritiesDTOList != null) {
						log.info("productSelectionVaritiesDTOList size : " + productSelectionVaritiesDTOList.size());
						if (action != null && !action.equals("EDIT")) {
							calculateTotalMaterialValue();
						} else {

							for (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
								if (dto != null && dto.getUnit() != null && dto.getUnitPrice() != null
										&& dto.getTaxPercent() != null) {
									for (PurchaseOrderItems itms : purchaseOrder.getPurchaseOrderItemsList()) {
										if (itms.getProductVarietyMaster().getId().equals(dto.getProductVarietyId())) {
											dto.setUnit(itms.getItemQty());
											dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
											dto.setTaxAmount(
													((dto.getTaxPercent()) * ((dto.getUnit()) * (dto.getUnitPrice())))
															/ 100);
										}
									}
									// totalMaterialValue = totalMaterialValue + (dto.getUnit() *
									// dto.getUnitPrice());
								}
							}
							calculateTotalMaterialValueForEdit();
						}
						// gstCalculation();
						// getProductVaritiesDetailsList();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						AppUtil.addWarn("No product Variety available for this Society");
						log.info(" productSelectionVaritiesDTOList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getProductVaritiesList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getProductVaritiesList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getProductVaritiesConList() {
		log.info("<--- Inside getProductVaritiesList() ---> ");
		orderItemsList = new ArrayList<PurchaseOrderItems>();
		if (contractExportPlan == null || contractExportPlan.getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		if (purchaseOrder == null || purchaseOrder.getSupplierMaster() == null
				|| purchaseOrder.getSupplierMaster().getId() == null) {
			AppUtil.addWarn("Please Select Society");
			return;
		}
		log.info("action::::::::::" + action);
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getConPurchaseOrderItemsList/"
				+ contractExportPlan.getId() + "/" + purchaseOrder.getSupplierMaster().getId() + "/" + action;

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productSelectionVaritiesDTOList = (List<ProductSelectionVaritiesDTO>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductSelectionVaritiesDTO>>() {
							});

					if (productSelectionVaritiesDTOList != null) {
						log.info("productSelectionVaritiesDTOList size : " + productSelectionVaritiesDTOList.size());
						if (action != null && !action.equals("EDIT")) {
							calculateTotalMaterialValue();
						} else {
							for (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
								if (dto != null && dto.getUnit() != null && dto.getUnitPrice() != null
										&& dto.getTaxPercent() != null) {
									for (PurchaseOrderItems itms : purchaseOrder.getPurchaseOrderItemsList()) {
										if (itms.getProductVarietyMaster().getId().equals(dto.getProductVarietyId())) {
											dto.setUnit(itms.getItemQty());
											dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
											dto.setTaxAmount(
													((dto.getTaxPercent()) * ((dto.getUnit()) * (dto.getUnitPrice())))
															/ 100);
										}
									}
									// totalMaterialValue = totalMaterialValue + (dto.getUnit() *
									// dto.getUnitPrice());
								}
							}
							calculateTotalMaterialValueForEdit();
						}
						// gstCalculation();
						// getProductVaritiesDetailsList();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						AppUtil.addWarn("No product Variety available for this Society");
						log.info(" productSelectionVaritiesDTOList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getProductVaritiesList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getProductVaritiesList() --->", e);
		}
	}

	@SuppressWarnings("unchecked")
	public void getProductVaritiesGovList() {
		log.info("<--- Inside getProductVaritiesList() ---> ");
		orderItemsList = new ArrayList<PurchaseOrderItems>();
		if (purchaseOrder.getGovtSchemePlan() == null || purchaseOrder.getGovtSchemePlan().getId() == null) {
			AppUtil.addWarn("Please Select Plan");
			return;
		}
		if (purchaseOrder == null || purchaseOrder.getSupplierMaster() == null
				|| purchaseOrder.getSupplierMaster().getId() == null) {
			AppUtil.addWarn("Please Select Society");
			return;
		}
		log.info("action::::::::::" + action);
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();
		String requestPath = SERVER_URL + "/govtPurchaseOrder/getPurchaseOrderItemsList/"
				+ purchaseOrder.getGovtSchemePlan().getId() + "/" + purchaseOrder.getSupplierMaster().getId() + "/"
				+ action;

		try {
			log.info("requestPath : " + requestPath);
			BaseDTO baseDTO = httpService.get(requestPath);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					productSelectionVaritiesDTOList = (List<ProductSelectionVaritiesDTO>) mapper.readValue(jsonValue,
							new TypeReference<List<ProductSelectionVaritiesDTO>>() {
							});

					if (productSelectionVaritiesDTOList != null) {
						log.info("productSelectionVaritiesDTOList size : " + productSelectionVaritiesDTOList.size());
						if (action != null && !action.equals("EDIT")) {
							calculateTotalMaterialValue();
						} else {
							/*
							 * Map<Object, List<ProductSelectionVaritiesDTO>> productSelectionVaritiesDTOLis
							 * = productSelectionVaritiesDTOList .stream().collect(Collectors.groupingBy(a
							 * -> a.getProductVarietyId()));
							 * log.info("productSelectionVaritiesDTOLis size"+productSelectionVaritiesDTOLis
							 * .size());
							 * 
							 * 
							 * for (Map.Entry<Object, List<ProductSelectionVaritiesDTO>> dto :
							 * productSelectionVaritiesDTOLis.entrySet()) { ProductSelectionVaritiesDTO
							 * dtoObj=new ProductSelectionVaritiesDTO();
							 * 
							 * //productSelectionVaritiesDTOList.add(dtoObj); }
							 * List<ProductSelectionVaritiesDTO> dtoList =
							 * (List<ProductSelectionVaritiesDTO>) productSelectionVaritiesDTOList.stream()
							 * .collect(Collectors.groupingBy(a -> a.getProductVarietyId()));
							 * log.info("dtoList size"+dtoList.size()); for (ProductSelectionVaritiesDTO dto
							 * : dtoList) {
							 * 
							 * productSelectionVaritiesDTOList.add(dto); }
							 */

							for (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
								if (dto != null && dto.getUnit() != null && dto.getUnitPrice() != null
										&& dto.getTaxPercent() != null) {
									for (PurchaseOrderItems itms : purchaseOrder.getPurchaseOrderItemsList()) {
										if (itms.getProductVarietyMaster().getId().equals(dto.getProductVarietyId())) {
											dto.setUnit(itms.getItemQty());
											dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
											dto.setTaxAmount(
													((dto.getTaxPercent()) * ((dto.getUnit()) * (dto.getUnitPrice())))
															/ 100);
										}
									}
									// totalMaterialValue = totalMaterialValue + (dto.getUnit() *
									// dto.getUnitPrice());
								}
							}
							calculateTotalMaterialValueForEdit();
						}
						// gstCalculation();
						// getProductVaritiesDetailsList();
					} else {
						errorMap.notify(ErrorDescription.ERROR_EMPTY_LIST.getCode());
						AppUtil.addWarn("No product Variety available for this Society");
						log.info(" productSelectionVaritiesDTOList is Empty ");
					}
				} else {
					errorMap.notify(baseDTO.getStatusCode());
					log.info(" Error Message: " + baseDTO.getErrorDescription());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				log.info("<--- Requested Service Error in getProductVaritiesList() ---> ");
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<--- Exception in getProductVaritiesList() --->", e);
		}
	}

	public void calculateTotalMaterialValueValidate(ProductSelectionVaritiesDTO productSelectionVaritiesDTO) {
		log.info("calculateTotalMaterialValueValidate method Start======>");
		log.info("item :::::" + productSelectionVaritiesDTO);
		try {
			if (productSelectionVaritiesDTO.getPlanedUnits() < productSelectionVaritiesDTO.getUnit()) {
				AppUtil.addWarn("Current Unit Should be greater than Planned Unit ");
				validateFlag = true;
			} else {
				validateFlag = false;
				calculateTotalMaterialValue();
			}
		} catch (Exception ex) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.info("Error in calculateTotalMaterialValueValidate method=====>", ex);
		}
		log.info("calculateTotalMaterialValueValidate method End======>");
	}

	public void calculateTotalMaterialValue() {
		totalMaterialValue = 0.00;
		List<ProductSelectionVaritiesDTO> dtoList = new ArrayList<ProductSelectionVaritiesDTO>();
		;
		orderItemsList = new ArrayList<PurchaseOrderItems>();
		PurchaseOrderItems itms = null;
		for (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
			// log.info("dto :"+dto);
			if (dto != null && dto.getUnit() != null && dto.getUnitPrice() != null && dto.getTaxPercent() != null) {
				// dtoList = new ArrayList<ProductSelectionVaritiesDTO>();
				/*
				 * if (dto.getUnit() > (dto.getPlanedUnits() - dto.getAlreadyIssuedUnits())) {
				 * dto.setUnit(null); AppUtil.addWarn("Current unit should not greater than " +
				 * (dto.getPlanedUnits() - dto.getAlreadyIssuedUnits())); return; }
				 */
				dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
				dto.setTaxAmount(((dto.getTaxPercent()) * ((dto.getUnit()) * (dto.getUnitPrice()))) / 100);
				dto.setBalanceUnit(dto.getPlanedUnits() - dto.getUnit() + dto.getAlreadyIssuedUnits());
				dtoList.add(dto);

				itms = new PurchaseOrderItems();
				ProductVarietyMaster prd = new ProductVarietyMaster();
//				prd.setCode(dto.getProductVarietyCode());
//				prd.setName(dto.getProductVarietyName());
				prd.setId(dto.getProductVarietyId());
//				prd.setHsnCode(dto.getHsnCode());
				UomMaster uom = new UomMaster();
				uom.setId(dto.getUomId());
//				uom.setCode(dto.getUomCode());
				itms.setProductVarietyMaster(prd);
				itms.setUomMaster(uom);
				itms.setItemQty(dto.getUnit());
				itms.setItemAmount(dto.getUnitAmount());
				itms.setUnitRate(dto.getUnitPrice());
				itms.setTaxPercent(Double.valueOf(dto.getTaxPercent()));
				itms.setTaxValue(dto.getTaxAmount());
				itms.setItemTotal((dto.getUnitAmount() + dto.getTaxAmount()));

				orderItemsList.add(itms);

				totalMaterialValue = totalMaterialValue + (dto.getUnit() * dto.getUnitPrice());
			}
			/*
			 * if(dto != null && dto.getUnit() != null && dto.getUnitPrice() != null)
			 * totalMaterialValue = totalMaterialValue + (dto.getUnit() *
			 * dto.getUnitPrice());
			 */
		}
		if (dtoList != null && !dtoList.isEmpty() && dtoList.size() == productSelectionVaritiesDTOList.size())
			productSelectionVaritiesDTOList = dtoList;
		if (orderItemsList != null && !orderItemsList.isEmpty()
				&& productSelectionVaritiesDTOList.size() == orderItemsList.size())
			purchaseOrder.setPurchaseOrderItemsList(orderItemsList);
		if (dtoList.size() == productSelectionVaritiesDTOList.size()) {
			gstCalculation();
		}
		/*
		 * for(ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
		 * log.info("dto :"+dto); if(dto != null && dto.getUnit() != null &&
		 * dto.getUnitPrice() != null) totalMaterialValue = totalMaterialValue +
		 * (dto.getUnit() * dto.getUnitPrice()); }
		 */
	}

	public void gstCalculation() {
		gstSummaryDTOList = new ArrayList<GSTSummaryDTO>();
		productIdsList = new ArrayList<>();
		for (ProductSelectionVaritiesDTO productSelectionVaritiesDTO : productSelectionVaritiesDTOList) {
			String productID = new String();
			productID = productSelectionVaritiesDTO.getProductVarietyId().toString();
			productIdsList.add(productID);
		}

		quotationSelectionRequest = new QuotationSelectionRequest();
		quotationSelectionRequest.setProductIdsList(productIdsList);
		quotationSelectionRequest.setProductVaritiesList(productSelectionVaritiesDTOList);

		try {
			String URL = SERVER_URL + "/govtPurchaseOrder/loadgstsummary";
			BaseDTO baseDTO = httpService.post(URL, quotationSelectionRequest);

			ObjectMapper mapper = new ObjectMapper();
			String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			quotationItemWithGstDTO = mapper.readValue(jsonResponse, ItemDataWithGstDTO.class);
			gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
			log.info("gstPercentageWiseDTOList size" + gstPercentageWiseDTOList.get(0).getUnit());

			gstWiseList = quotationItemWithGstDTO.getGstWiseList();
			totalTaxCgstSgstAmount = calculateTotalTaxAmount(gstPercentageWiseDTOList);
			log.info("totalTaxCgstSgstAmount==>" + totalTaxCgstSgstAmount);
			log.info("totalMaterialValue==>" + totalMaterialValue);

			totalTaxMaterialAmount = totalTaxCgstSgstAmount + totalMaterialValue;

			log.info("totalTaxMaterialAmount==>" + totalTaxMaterialAmount);
			calculateTotalTaxSummary();

			// DecimalFormat df = new DecimalFormat(".##");
			NumberFormat numberFormat = new DecimalFormat("#0.00");
			String totTaxMaterialAmount = numberFormat.format(totalTaxMaterialAmount);

			log.info(totalTaxMaterialAmount + "<==>" + totTaxMaterialAmount);

			amountInWords = AmountToWordConverter.converter(String.valueOf(totTaxMaterialAmount));
			dataToggle = "collapse";

		} catch (Exception e) {
			log.error("Exception in gstCalculation ", e);
		}

	}

	public Double calculateTotalTaxAmount(List<HsnCodeTaxPercentageWiseDTO> gstList) {
		Double total = 0.0;
		for (HsnCodeTaxPercentageWiseDTO gst : gstList) {
			if (gst.getTotalTax() != null) {
				log.info("calculateTotalTaxAmount gst.getTotalTax() ==> " + gst.getTotalTax());
				total = total + truncateDecimal(gst.getTotalTax(), 2).doubleValue();
				log.info("calculateTotalTaxAmount total==> " + total);
			}
		}
		return total;
	}

	private BigDecimal truncateDecimal(double x, int numberofDecimals) {
		if (x > 0) {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
		} else {
			return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
		}
	}

	public void calculateTotalTaxSummary() {
		if (gstPercentageWiseDTOList.size() != 0) {
			cgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getCgstAmount));
			sgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getSgstAmount));
			totalCgstSgstTaxValue = gstPercentageWiseDTOList.stream()
					.collect(Collectors.summingDouble(HsnCodeTaxPercentageWiseDTO::getTotalTax));
		}
	}

	public void test() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");
	}

	public void submitNote() {
		log.info("=========================================");
		log.info("Note Text >>>>>>>>> " + planNote.getNote());
		log.info("=========================================");
	}

	public String getCreatePage() {
		action = "CREATE";
		dataToggle = "collapse in";
		purchaseOrder = new PurchaseOrder();
		orderItemsList = new ArrayList<>();
		clearTableData();

		govtProductionPlan = null;
		// govtSchemePlan = null;
		govtProductionPlanList = new ArrayList<GovtSchemePlan>();
		supplierMasterList = new ArrayList<SupplierMaster>();
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();

		contractExportPlan = new ContractExportPlan();
		contractExportPlanList = new ArrayList<>();

		additionalProductionPlanList = new ArrayList<>();
		additionalProductionPlan = new AdditionalProductionPlan();

		cgstTaxValue = 0.00;
		sgstTaxValue = 0.00;
		totalCgstSgstTaxValue = 0.00;
		totalTaxMaterialAmount = 0.00;

		eventLogList = new ArrayList<>();
		approvedEventLogList = new ArrayList<>();
		rejectedEventLogList = new ArrayList<>();

		editFlag = false;
		shippingEntityList = new ArrayList<>();

		return ADD_PAGE_URL;
	}

	public void clearTableData() {
		// selectedOrderItemsList = new ArrayList<>();
		gstWiseList = new ArrayList<>();
		gstPercentageWiseDTOList = new ArrayList<>();
		totalCgstAmount = 0.0;
		totalSgstAmount = 0.0;
		totalTaxAmount = 0.0;
		materialValue = 0.0;
		societyAddress = null;
		warehouseAddress = null;
	}

	public String previewPurchaseOrder() {
		/*
		 * if (orderItemsList==null || orderItemsList.size()==0) {
		 * errorMap.notify(24006); return null; }
		 */

		log.info("purchaseOrder==>" + purchaseOrder);

		/*
		 * if(govtProductionPlan == null || govtProductionPlan.isEmpty()) {
		 * Util.addWarn("Please Select Plan Type"); return null; }
		 * if(govtProductionPlan.equals("Govt_Scheme")) {
		 * if(purchaseOrder.getGovtSchemePlan() == null) {
		 * Util.addWarn("Please Select Plan Code / Name"); return null; }
		 * 
		 * }else if(govtProductionPlan.equals("Contract")) { if(contractExportPlan ==
		 * null) { Util.addWarn("Please Select Plan Code / Name"); return null; } }else
		 * if(govtProductionPlan.equals("Export")) { if(contractExportPlan == null) {
		 * Util.addWarn("Please Select Plan Code / Name"); return null; } }else
		 * if(govtProductionPlan.equals("Additional_Production_Plan")) {
		 * if(additionalProductionPlan == null) {
		 * Util.addWarn("Please Select Plan Code / Name"); return null; } }
		 * if(purchaseOrder.getSupplierMaster() == null) {
		 * Util.addWarn("Please Select Society Code / Name"); return null; }
		 * if(purchaseOrder.getDeliveryEntityMaster() == null) {
		 * Util.addWarn("Please Select Shipping Address"); return null; }
		 * if(purchaseOrder.getBillingEntityMaster() == null) {
		 * Util.addWarn("Please Select Billing Address"); return null; }
		 * if(purchaseOrder.getValidDate() == null) {
		 * Util.addWarn("Purchase Order Validity Date is Required"); return null; }
		 * if(purchaseOrder.getExpectedDeliveryDate() == null) {
		 * Util.addWarn("Expected Date of Devievery is Required"); return null; }
		 * if(productSelectionVaritiesDTOList == null ||
		 * productSelectionVaritiesDTOList.isEmpty()) {
		 * Util.addWarn("Product Variety Details is Required"); return null; }
		 * if(purchaseOrder.getTermsConditions() == null) {
		 * Util.addWarn("Terms & Conditions is Required"); return null; }
		 */
		/*
		 * if (planNote.getUserMaster() == null) {
		 * Util.addWarn("Please Select Forward To"); return null; } if
		 * (planNote.getFinalApproval() == null) {
		 * Util.addWarn("Please Select Forward For"); return null; }
		 */

		String url = SERVER_URL + "/govtPurchaseOrder/preview";
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Purchase Order preview successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);

					if (purchaseOrder == null) {
						return null;
					}
					log.info("getPurchaseOrderItemsList==> " + purchaseOrder.getPurchaseOrderItemsList());
					log.info("getValidDate==> " + purchaseOrder.getValidDate());
					log.info("getBillingEntityMaster==> " + purchaseOrder.getBillingEntityMaster());

					if (purchaseOrder.getEntityMaster() != null) {
						dnpAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
					}
					if (purchaseOrder.getDeliveryEntityMaster() != null) {
						warehouseAddress = AppUtil
								.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
					}
					if (purchaseOrder.getBillingEntityMaster() != null) {
						headOfficeAddress = AppUtil
								.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
					}
					if (purchaseOrder.getSupplierMaster() != null) {
						societyAddress = AppUtil.prepareAddress(purchaseOrder.getSupplierMaster().getAddressMaster());
					}
					log.info(":::::::::::::::" + purchaseOrder.getPurchaseOrderItemsList().size());
					// calculateTax(purchaseOrder.getPurchaseOrderItemsList());
					calculateTotalValues(purchaseOrder.getPurchaseOrderItemsList());
					try {

						log.info("materialValue==>" + materialValue);
						log.info("totalTaxAmount==>" + totalTaxAmount);

						Double totalValue = AppUtil.ifNullRetunZero(materialValue)
								+ AppUtil.ifNullRetunZero(totalTaxAmount);
						NumberFormat numberFormat = new DecimalFormat("#0.00");
						String totAmount = numberFormat.format(totalValue);

						log.info("totAmount==>" + totAmount);
						amountInWords = AmountToWordConverter.converter(String.valueOf(totAmount));

						// getProductVaritiesList();

						// gstCalculation();
						/*
						 * amountInWords = AmountToWordConverter.converter(String.valueOf(
						 * AppUtil.ifNullRetunZero(materialValue) +
						 * AppUtil.ifNullRetunZero(totalTaxAmount)));
						 */
					} catch (Exception e) {
						log.error("Error while amountInWords", e);
						errorMap.notify(1);
						return null;
					}

				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
		} catch (Exception exp) {
			log.error("Error in preview Purchase Order", exp);
			errorMap.notify(1);
			return null;
		}

		/*
		 * if (selectedOrderItemsList != null && selectedOrderItemsList.size() > 0) {
		 * purchaseOrder.setPurchaseOrderItemsList(selectedOrderItemsList); } else {
		 * purchaseOrder.setPurchaseOrderItemsList(orderItemsList); }
		 */

		return PREVIEW_PAGE_URL;
	}

	public void calculateTax(List<PurchaseOrderItems> itemsList) {
		// log.info("itemsList.size() :::::::: "+itemsList.size());
		if (itemsList != null && itemsList.size() > 0) {
			List<GstSummeryDTO> gstList = new ArrayList<>();
			for (PurchaseOrderItems item : itemsList) {
				// log.info("item :::::::::: "+item);
				if (item.getProductVarietyMaster() != null) {
					StringBuilder productId = new StringBuilder(item.getProductVarietyMaster().getId().toString());
					gstList.addAll(commonDataService.loadTaxForProduct(productId,
							AppUtil.ifNullRetunZero(item.getItemAmount())));
				}
			}

			ItemDataWithGstDTO quotationItemWithGstDTO = new ItemDataWithGstDTO();
			quotationItemWithGstDTO = TaxCalculationUtil.calculatePurchaseOrderTax(gstList, itemsList,
					quotationItemWithGstDTO);
			gstWiseList = quotationItemWithGstDTO.getGstWiseList();
			gstPercentageWiseDTOList = quotationItemWithGstDTO.getGstPercentageWiseDTOList();
			calculateTotalValues(itemsList);
		}
	}

	public void calculateTotalValues(List<PurchaseOrderItems> itemsList) {
		materialValue = TaxCalculationUtil.calculatePurchaseOrderMaterialValue(itemsList);
		totalCgstAmount = TaxCalculationUtil.calculateTotalCgstAmount(gstPercentageWiseDTOList);
		totalSgstAmount = TaxCalculationUtil.calculateTotalSgstAmount(gstPercentageWiseDTOList);
		totalTaxAmount = TaxCalculationUtil.calculateTotalTaxAmount(gstPercentageWiseDTOList);
	}

	public String savePurchaseOrder(String requestType) {
		log.info("PurchaseOrderBean.savePurchaseOrder method started {requestType} : " + requestType);
		if (requestType.equals(OrderStatus.INITIATED.name())) {
			purchaseOrder.setStatus(OrderStatus.INITIATED);
		} else if (requestType.equals(OrderStatus.SUBMITTED.name())) {
			purchaseOrder.setStatus(OrderStatus.SUBMITTED);
		} else if (requestType.equals(OrderStatus.APPROVED.name())) {
			purchaseOrder.setStatus(OrderStatus.APPROVED);
		} else if (requestType.equals(OrderStatus.REJECTED.name())) {
			purchaseOrder.setStatus(OrderStatus.REJECTED);
		}
		purchaseOrder.setMaterialValue(materialValue);
		purchaseOrder.setTotalValue(materialValue);
		purchaseOrder.setNetValue(materialValue);

		if (contractExportPlan != null && contractExportPlan.getPlanName() != null) {
			purchaseOrder.setContractExportPlan(contractExportPlan);
		}
		if (additionalProductionPlan != null && additionalProductionPlan.getPlanName() != null) {
			purchaseOrder.setAdditionalProductionPlan(additionalProductionPlan);
		}

		if (purchaseOrder == null || purchaseOrder.getPurchaseOrderItemsList() == null
				|| purchaseOrder.getPurchaseOrderItemsList().isEmpty()) {
			AppUtil.addWarn("Purchase Order Items List is empty");
			return null;
		}

		log.info("planNote : " + planNote);
		/*
		 * if (planNote == null || planNote.getNote() == null ||
		 * planNote.getNote().equals("") || planNote.getUserMaster() == null ||
		 * planNote.getUserMaster().getId() == null || planNote.getFinalApproval() ==
		 * null || planNote.getFinalApproval().equals("")) {
		 * AppUtil.addWarn("Please add proper Plan Note Details"); return null; }
		 */
		purchaseOrder.setPurchaseOrderNoteList(new ArrayList<PurchaseOrderNote>());
		purchaseOrder.getPurchaseOrderNoteList().add(planNote);
		String url = "";
		if (action != null && (action.equals("VIEW") || action.equals("EDIT"))) {
			url = SERVER_URL + "/govtPurchaseOrder/approveRejectPurchaseOrder";
		} else {
			url = SERVER_URL + "/govtPurchaseOrder/create";
		}
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.post(url, purchaseOrder);
		} catch (Exception exp) {
			log.error("Error in Purchase Order create ", exp);
			// Error : JSF1095: The response was already committed by the
			// time we tried to set the outgoing cookie for the flash.
			// Any values stored to the flash will not be available on the
			// next request.
			// Note : Plan is updated successfully ,so that we are
			// redirecting to list page with success message
			log.info(
					"Purchase Order is created successfully ,so that we are redirecting to list page with success message");
			errorMap.notify(24010);
			return LIST_PAGE_URL;
		}

		if (baseDTO != null) {
			if (baseDTO.getStatusCode() == 0) {
				log.info("Purchase Order saved successfully");
				errorMap.notify(24010);
			} else {
				String msg = baseDTO.getErrorDescription();
				log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		}
		return LIST_PAGE_URL;
	}

	public void loadLazyPurchaseOrderList() {
		editFlag = false;
		log.info("PurchaseOrderBean.loadLazyPurchaseOrderList method started");
		purchaseOrderSearchLazyList = new LazyDataModel<PurchaseOrderSearch>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<PurchaseOrderSearch> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<PurchaseOrderSearch> data = new ArrayList<PurchaseOrderSearch>();
				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<PurchaseOrderSearch>>() {
					});
					// if (data != null) {
					this.setRowCount(baseDTO.getTotalRecords());
					size = baseDTO.getTotalRecords();
					// }
				} catch (Exception e) {
					log.error("Error ", e);
				}
				return data;
			}

			@Override
			public Object getRowKey(PurchaseOrderSearch res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public PurchaseOrderSearch getRowData(String rowKey) {
				List<PurchaseOrderSearch> responseList = (List<PurchaseOrderSearch>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (PurchaseOrderSearch res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {

		log.info("PurchaseOrderBean.getSearchData Method Started");
		BaseDTO baseDTO = new BaseDTO();
		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");
		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		purchaseOrderSearch = new PurchaseOrderSearch();
		PurchaseOrderSearch request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		try {
			String URL = SERVER_URL + "/govtPurchaseOrder/search";
			baseDTO = httpService.post(URL, request);
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		log.info("PurchaseOrderBean.getSearchData Method Complted");
		return baseDTO;
	}

	private PurchaseOrderSearch getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {

		PurchaseOrderSearch request = new PurchaseOrderSearch();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("poNumber")) {
				log.info("PoNumber : " + value);
				request.setPoNumber(value);
			}

			if (entry.getKey().equals("societyCodeOrName")) {
				log.info("SocietyCode Or Name : " + value);
				request.setSocietyCodeOrName(value);
			}
			if (entry.getKey().equals("createdDate")) {
				log.info("createdDate : " + value);
				log.info("createdDate 2 : " + entry.getValue());
				request.setCreatedDate((Date) entry.getValue());
			}
			if (entry.getKey().equals("status")) {
				request.setStatus(value);
			}
		}

		return request;
	}

	public String getEditPage() {
		action = "EDIT";
		editFlag = true;
		orderItemsList = new ArrayList<>();
		dataToggle = "collapse in";
		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		if (!purchaseOrderSearch.getStatus().equals(OrderStatus.INITIATED.name())) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_CANNOT_EDIT.getCode());
			return null;
		}
		get();
		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		getProductVaritiesList();
		if (purchaseOrder.getEntityMaster() != null) {
			dnpAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getDeliveryEntityMaster() != null) {
			warehouseAddress = AppUtil.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}

		loadShippingAddressEntityList(govtProductionPlan);
		return ADD_PAGE_URL;
	}

	public String getViewPage() {
		action = "VIEW";
		orderItemsList = new ArrayList<>();
		clearTableData();
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(ErrorDescription.PURCHASE_ORDER_SELECT_ONE_RECORD.getCode());
			return null;
		}
		get();
		if (purchaseOrder == null || purchaseOrder.getId() == null) {
			return null;
		}
		if (purchaseOrder.getEntityMaster() != null) {
			dnpAddress = AppUtil.prepareAddress(purchaseOrder.getEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getDeliveryEntityMaster() != null) {
			warehouseAddress = AppUtil.prepareAddress(purchaseOrder.getDeliveryEntityMaster().getAddressMaster());
		}
		if (purchaseOrder.getBillingEntityMaster() != null) {
			headOfficeAddress = AppUtil.prepareAddress(purchaseOrder.getBillingEntityMaster().getAddressMaster());
		}
		return VIEW_PAGE_URL;
	}

	@SuppressWarnings("unlikely-arg-type")
	public void get() {
		log.info("GovtPurchaseOrderBean.getPurchaseOrder method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(24011);
		}
		String url = SERVER_URL + "/govtPurchaseOrder/get/" + purchaseOrderSearch.getId();

		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.get(url);
			log.info(":::::::::::::::::::");
		} catch (Exception e) {
			log.error("Error in viewPlan", e);
			errorMap.notify(1);
		}

		if (baseDTO != null) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				purchaseOrder = mapper.readValue(jsonValue, PurchaseOrder.class);
			} catch (IOException e) {
				log.error(e);
			}
		}
		if (purchaseOrder == null) {
			log.info("#==--> PurchaseOrder is Not Available <--==#");
			errorMap.notify(9612);
		} else {
			// quotationList =
			// commonDataService.getQuotationListForSociety(purchaseOrder.getSupplierMaster().getId());
			govtProductionPlanList = new ArrayList<GovtSchemePlan>();
			supplierMasterList = new ArrayList<SupplierMaster>();

			if (purchaseOrder.getGovtSchemePlan() != null && purchaseOrder.getGovtSchemePlan().getPlanName() != null) {
				log.info("gov plan==>" + purchaseOrder.getGovtSchemePlan().getPlanName());
				govtProductionPlan = "Govt_Scheme";
				loadGovtSchemePlanApprovedPlanList();
				getSuppliers();
			}

			if (purchaseOrder.getContractExportPlan() != null
					&& purchaseOrder.getContractExportPlan().getPlanType().name().equals("Contract")) {
				log.info("con plan==>" + purchaseOrder.getContractExportPlan().getPlanType());
				govtProductionPlan = "Contract";
				contractExportPlan = purchaseOrder.getContractExportPlan();
				loadGovtProductionPlanApprovedPlanList();
				getConSuppliers();
			}

			if (purchaseOrder.getContractExportPlan() != null
					&& purchaseOrder.getContractExportPlan().getPlanType().name().equals("Export")) {
				govtProductionPlan = "Export";
				contractExportPlan = purchaseOrder.getContractExportPlan();
				loadGovtProductionPlanApprovedPlanList();
				getConSuppliers();
			}

			if (purchaseOrder.getAdditionalProductionPlan() != null
					&& purchaseOrder.getAdditionalProductionPlan().getPlanName() != null) {
				log.info("add plan==>" + purchaseOrder.getAdditionalProductionPlan().getPlanName());
				govtProductionPlan = "Additional_Production_Plan";
				additionalProductionPlan = purchaseOrder.getAdditionalProductionPlan();
				log.info("additionalProductionPlan==>" + additionalProductionPlan.getPlanName());
				loadGovtProductionPlanApprovedPlanList();
				getAddSuppliers();
			}

			orderItemsList = purchaseOrder.getPurchaseOrderItemsList();
			selectedOrderItemsList = orderItemsList;
			calculateTax(purchaseOrder.getPurchaseOrderItemsList());

			planNote = purchaseOrder.getPurchaseOrderNoteList().get(0);
			getEmployeeDetailsByUserId();
			loadForwardToUsers();

			eventLogList = new ArrayList<>();

			approvedEventLogList = new ArrayList<>();

			rejectedEventLogList = new ArrayList<>();

			if (purchaseOrder.getPurchaseOrderLogList() != null && !purchaseOrder.getPurchaseOrderLogList().isEmpty()) {
				EmployeeMaster empDetails = null;
				for (PurchaseOrderLog purchaseLog : purchaseOrder.getPurchaseOrderLogList()) {
					String loggedUserUrl = appPreference.getPortalServerURL()
							+ "/employee/findempdetailsbyloggedinuser/" + purchaseLog.getCreatedBy().getId();

					BaseDTO dto = httpService.get(loggedUserUrl);
					if (dto != null && dto.getStatusCode() == 0) {
						ObjectMapper maper = new ObjectMapper();
						try {
							String jsonResp = maper.writeValueAsString(dto.getResponseContent());
							empDetails = maper.readValue(jsonResp, EmployeeMaster.class);
						} catch (Exception e) {
							log.error(e);
						}
					} else {
						AppUtil.addError(" Employee Details Not Found for the User ");
						return;
					}

					EventLog eventLog = new EventLog("", purchaseLog.getCreatedByName(),
							empDetails.getPersonalInfoEmployment().getDesignation().getName(),
							purchaseLog.getCreatedDate(), purchaseLog.getRemarks());
					if (purchaseOrder.getStatus().equals(ProcurementOrderStatus.INITIATED.toString())
							|| purchaseOrder.getStatus().equals(ProcurementOrderStatus.SUBMITTED.toString())) {
						eventLog.setTitle("Created By");
					} else if (purchaseOrder.getStatus().equals(ProcurementOrderStatus.APPROVED.toString())) {
						eventLog.setTitle("Approved By");
						approvedEventLogList.add(eventLog);
					} else if (purchaseOrder.getStatus().equals(ProcurementOrderStatus.REJECTED.toString())) {
						eventLog.setTitle("Rejected By");
						rejectedEventLogList.add(eventLog);
					}
					eventLogList.add(eventLog);
				}
			}

			Collections.sort(eventLogList, new Comparator<EventLog>() {
				public int compare(EventLog m1, EventLog m2) {
					return m1.getTitle().compareTo(m2.getTitle());
				}
			});
		}
	}

	public void calculateTotalMaterialValueForEdit() {
		totalMaterialValue = 0.00;
		List<ProductSelectionVaritiesDTO> dtoList = new ArrayList<ProductSelectionVaritiesDTO>();
		;
		// orderItemsList = new ArrayList<PurchaseOrderItems>();
		// productSelectionVaritiesDTOList
		// PurchaseOrderItems itms = null;
		for (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
			// log.info("dto :"+dto);
			if (dto != null && dto.getUnit() != null && dto.getUnitPrice() != null && dto.getTaxPercent() != null) {
				// dtoList = new ArrayList<ProductSelectionVaritiesDTO>();
				if (dto.getUnit() > (dto.getPlanedUnits() - dto.getAlreadyIssuedUnits())) {
					dto.setUnit(null);
					AppUtil.addWarn("Current unit should be greater than "
							+ (dto.getPlanedUnits() - dto.getAlreadyIssuedUnits()));
					return;
				}

				dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
				dto.setTaxAmount(((dto.getTaxPercent()) * ((dto.getUnit()) * (dto.getUnitPrice()))) / 100);
				dto.setBalanceUnit(dto.getUnit() + dto.getAlreadyIssuedUnits() - dto.getPlanedUnits());
				dtoList.add(dto);

				/*
				 * itms = new PurchaseOrderItems(); ProductVarietyMaster prd = new
				 * ProductVarietyMaster(); prd.setId(dto.getProductVarietyId()); UomMaster uom =
				 * new UomMaster(); uom.setId(dto.getUomId());
				 * 
				 * itms.setProductVarietyMaster(prd); itms.setUomMaster(uom);
				 * itms.setItemQty(dto.getUnit()); itms.setItemAmount(dto.getUnitAmount());
				 * itms.setUnitRate(dto.getUnitPrice());
				 * itms.setTaxPercent(Double.valueOf(dto.getTaxPercent()));
				 * itms.setTaxValue(dto.getTaxAmount());
				 * itms.setItemTotal((dto.getUnitAmount()+dto.getTaxAmount()));
				 * 
				 * orderItemsList.add(itms);
				 */

				for (PurchaseOrderItems itms : purchaseOrder.getPurchaseOrderItemsList()) {
					if (itms.getProductVarietyMaster().getId().equals(dto.getProductVarietyId())) {
						itms.setItemQty(dto.getUnit());
						itms.setItemAmount(dto.getUnitAmount());
						// itms.setUnitRate(dto.getUnitPrice());
						// itms.setTaxPercent(Double.valueOf(dto.getTaxPercent()));
						itms.setTaxValue(dto.getTaxAmount());
						itms.setItemTotal((dto.getUnitAmount() + dto.getTaxAmount()));
					}
				}

				totalMaterialValue = totalMaterialValue + (dto.getUnit() * dto.getUnitPrice());
			}
			/*
			 * if(dto != null && dto.getUnit() != null && dto.getUnitPrice() != null)
			 * totalMaterialValue = totalMaterialValue + (dto.getUnit() *
			 * dto.getUnitPrice());
			 */
		}
		if (dtoList != null && !dtoList.isEmpty() && dtoList.size() == productSelectionVaritiesDTOList.size())
			productSelectionVaritiesDTOList = dtoList;
		/*
		 * if(orderItemsList != null && !orderItemsList.isEmpty() &&
		 * productSelectionVaritiesDTOList.size() == orderItemsList.size())
		 * purchaseOrder.setPurchaseOrderItemsList(orderItemsList);
		 */
		if (dtoList.size() == productSelectionVaritiesDTOList.size()) {
			gstCalculation();
		}
		/*
		 * for(ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) {
		 * log.info("dto :"+dto); if(dto != null && dto.getUnit() != null &&
		 * dto.getUnitPrice() != null) totalMaterialValue = totalMaterialValue +
		 * (dto.getUnit() * dto.getUnitPrice()); }
		 */
	}

	public void getEmployeeDetailsByUserId() {
		log.info("Inside getEmployeeDetailsByUserId()>>>>>>>>> ");
		BaseDTO baseDTO = null;
		try {
			String url = appPreference.getPortalServerURL() + "/employee/findempdetailsbyloggedinuser/"
					+ planNote.getCreatedBy().getId();

			baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				employeeMaster = mapper.readValue(jsonResponse, EmployeeMaster.class);
			} else {
				AppUtil.addError(" Employee Details Not Found for the User " + planNote.getCreatedBy().getId());
				return;
			}
		} catch (Exception e) {
			log.error("Exception Occured While loadForwardToUsers() :: ", e);
		}
	}

	public void processDelete() {
		log.info("PurchaseOrderBean.processDelete method started");
		if (purchaseOrderSearch == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(24001);
			return;
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		log.info("PurchaseOrderBean.delete method started");
		if (purchaseOrderSearch == null || purchaseOrderSearch.getId() == null) {
			errorMap.notify(24001);
			return null;
		}
		String url = SERVER_URL + "/govtPurchaseOrder/deletePurchaseOrder/" + purchaseOrderSearch.getId();
		BaseDTO baseDTO = null;
		try {
			baseDTO = httpService.delete(url);
		} catch (Exception exp) {
			log.error("Error in delete draft plan", exp);
			errorMap.notify(1);
			return null;
		}
		if (baseDTO.getStatusCode() == 0) {
			errorMap.notify(24002);
			return getPurchaseOrderListPage();
		} else {
			errorMap.notify(baseDTO.getStatusCode());
			return getPurchaseOrderListPage();
		}
	}

	public String getPurchaseOrderListPage() {
		purchaseOrderSearch = new PurchaseOrderSearch();
		loadLazyPurchaseOrderList();
		return LIST_PAGE_URL;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("PurchaseOrderBean.onRowSelect method started");
		purchaseOrderSearch = ((PurchaseOrderSearch) event.getObject());
		// addButtonFlag = true;
	}

	public String clearPurchaseOrder() {

		action = "CREATE";
		dataToggle = "collapse in";
		purchaseOrder = new PurchaseOrder();
		orderItemsList = new ArrayList<>();
		clearTableData();

		govtProductionPlan = null;
		// govtSchemePlan = null;
		govtProductionPlanList = new ArrayList<GovtSchemePlan>();
		supplierMasterList = new ArrayList<SupplierMaster>();
		productSelectionVaritiesDTOList = new ArrayList<ProductSelectionVaritiesDTO>();

		cgstTaxValue = 0.00;
		sgstTaxValue = 0.00;
		totalCgstSgstTaxValue = 0.00;
		totalTaxMaterialAmount = 0.00;

		eventLogList = new ArrayList<>();
		approvedEventLogList = new ArrayList<>();
		rejectedEventLogList = new ArrayList<>();

		contractExportPlan = new ContractExportPlan();
		contractExportPlanList = new ArrayList<>();

		additionalProductionPlanList = new ArrayList<>();
		additionalProductionPlan = new AdditionalProductionPlan();

		editFlag = false;
		return ADD_PAGE_URL;
	}

	/*
	 * public void valueChange(ProductSelectionVaritiesDTO
	 * productSelectionVaritiesDTO) {
	 * log.info("================= amtChange() =====================");
	 * 
	 * try { log.info(productSelectionVaritiesDTO.getHsnCode());
	 * log.info(productSelectionVaritiesDTO.getTaxPercent());
	 * log.info(productSelectionVaritiesDTO.getUnit());
	 * log.info(productSelectionVaritiesDTO.getUnitPrice());
	 * if(productSelectionVaritiesDTO.getUnit()!=null) {
	 * 
	 * productSelectionVaritiesDTO.setTaxAmount(((productSelectionVaritiesDTO.
	 * getTaxPercent()) ((productSelectionVaritiesDTO.getUnit()) *
	 * (productSelectionVaritiesDTO.getUnitPrice()))) / 100);
	 * 
	 * Integer units=productSelectionVaritiesDTO.getUnit(); setUnit(units);
	 * setTotalAmount(((productSelectionVaritiesDTO.getUnit())*(
	 * productSelectionVaritiesDTO.getUnitPrice()))+(productSelectionVaritiesDTO.
	 * getTaxAmount())); log.info(" TotalAmount "+getTotalAmount());
	 * productSelectionVaritiesDTO.setUnitAmount(
	 * ((productSelectionVaritiesDTO.getUnit()) *
	 * (productSelectionVaritiesDTO.getUnitPrice())));
	 * List<ProductSelectionVaritiesDTO> dtoList = new
	 * ArrayList<ProductSelectionVaritiesDTO>(); orderItemsList = new
	 * ArrayList<PurchaseOrderItems>(); PurchaseOrderItems itms = null; for
	 * (ProductSelectionVaritiesDTO dto : productSelectionVaritiesDTOList) { //
	 * log.info("dto :"+dto); if (dto != null && dto.getUnit() != null &&
	 * dto.getUnitPrice() != null && dto.getTaxPercent() != null) { // dtoList = new
	 * ArrayList<ProductSelectionVaritiesDTO>();
	 * 
	 * dto.setUnitAmount(((dto.getUnit()) * (dto.getUnitPrice())));
	 * dto.setTaxAmount(((dto.getTaxPercent()) * ((dto.getUnit()) *
	 * (dto.getUnitPrice()))) / 100); dtoList.add(dto);
	 * 
	 * itms = new PurchaseOrderItems(); ProductVarietyMaster prd = new
	 * ProductVarietyMaster(); prd.setId(dto.getProductVarietyId()); UomMaster uom =
	 * new UomMaster(); uom.setId(dto.getUomId());
	 * 
	 * itms.setProductVarietyMaster(prd); itms.setUomMaster(uom);
	 * itms.setItemQty(dto.getUnit()); itms.setItemAmount(dto.getUnitAmount());
	 * itms.setUnitRate(dto.getUnitPrice());
	 * itms.setTaxPercent(Double.valueOf(dto.getTaxPercent()));
	 * itms.setTaxValue(dto.getTaxAmount()); itms.setItemTotal((dto.getUnitAmount()
	 * + dto.getTaxAmount()));
	 * 
	 * orderItemsList.add(itms);
	 * 
	 * totalMaterialValue = totalMaterialValue + (dto.getUnit() *
	 * dto.getUnitPrice()); }
	 * 
	 * if(dto != null && dto.getUnit() != null && dto.getUnitPrice() != null)
	 * totalMaterialValue = totalMaterialValue + (dto.getUnit() *
	 * dto.getUnitPrice());
	 * 
	 * } }
	 * 
	 * gstCalculation(); } catch (Exception e) { log.error("Exception  ", e); } }
	 */
}
