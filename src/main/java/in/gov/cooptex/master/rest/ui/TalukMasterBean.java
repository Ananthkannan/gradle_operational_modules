package in.gov.cooptex.master.rest.ui;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.MasterSearchDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CityMaster;
import in.gov.cooptex.core.model.CountryMaster;
import in.gov.cooptex.core.model.DistrictMaster;
import in.gov.cooptex.core.model.StateMaster;
import in.gov.cooptex.core.model.TalukMaster;
import in.gov.cooptex.core.model.TemplateDetails;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("talukMasterBean")
public class TalukMasterBean {

	private final String LIST_PAGE_URL = "/pages/masters/taluk/listTalukMaster.xhtml?faces-redirect=true;";

	private final String CREATE_PAGE_URL = "/pages/masters/taluk/createTalukMaster.xhtml?faces-redirect=true;";

	private final String VIEW_PAGE_URL = "/pages/masters/taluk/viewTalukMaster.xhtml?faces-redirect=true;";

	public String gotoPage(String URL) {

		log.info("Goto Page [" + URL + "]");

		if (URL.equalsIgnoreCase("LIST")) {
			return gotoListPage();
		} else if (URL.equalsIgnoreCase("ADD")) {
			return gotoAddPage();
		} else if (URL.equalsIgnoreCase("EDIT")) {
			return gotoEditPage();
		} else if (URL.equalsIgnoreCase("VIEW")) {
			return gotoViewPage();
		} else {
			return null;
		}

	}

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;
	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<MasterSearchDTO> talukMasterLazyList;

	@Getter
	@Setter
	MasterSearchDTO masterSearchDTO;

	@Getter
	@Setter
	String sortingField;

	@Getter
	@Setter
	SortOrder sortingOrder;

	@Getter
	@Setter
	Map<String, Object> filtersMap;

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	Integer listSize;

	@Getter
	@Setter
	List<CityMaster> cityMasterList = new ArrayList<CityMaster>();
	@Getter
	@Setter
	TemplateDetails selectedTemplateDetails = new TemplateDetails();
	final String SERVER_URL = AppUtil.getPortalServerURL();

	private String gotoListPage() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
		loadLazyList();
		return LIST_PAGE_URL;
	}

	@Autowired
	CommonBean commonBean;

	private String gotoAddPage() {

		log.info("gotoAddPage");

		talukMaster = new TalukMaster();
		talukMaster.setStatus(true);
		countryMaster = null;
		stateMaster = null;
		districtMaster = null;
		cityMaster = null;
		commonBean.loadActiveCountriesList();

		return CREATE_PAGE_URL;
	}

	private String gotoViewPage() {

		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_TALUK_MASTER.getCode());
			return null;
		}

		talukMaster = getTalukMasterById(masterSearchDTO.getId());

		return VIEW_PAGE_URL;
	}

	private String gotoEditPage() {

		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_TALUK_MASTER.getCode());
			return null;

		}

		talukMaster = getTalukMasterById(masterSearchDTO.getId());

		if (talukMaster != null) {

			commonBean.loadActiveCountriesList();

			if (talukMaster.getDistrictMaster() != null && talukMaster.getDistrictMaster().getStateMaster() != null
					&& talukMaster.getDistrictMaster().getStateMaster().getCountryMaster() != null) {

				countryMaster = talukMaster.getDistrictMaster().getStateMaster().getCountryMaster();
				stateMaster = talukMaster.getDistrictMaster().getStateMaster();

				commonBean.loadActiveStatesByCountry(
						talukMaster.getDistrictMaster().getStateMaster().getCountryMaster().getId());

				commonBean.loadActiveDistrictsByState(talukMaster.getDistrictMaster().getStateMaster().getId());

				commonBean.loadActiveCitiesByDistrict(talukMaster.getDistrictMaster().getId());
			}

		}

		return CREATE_PAGE_URL;

	}

	public TalukMaster getTalukMasterById(Long id) {
		String URL = appPreference.getPortalServerURL() + "/taluk/get/" + id;
		TalukMaster talukMaster = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				talukMaster = mapper.readValue(jsonValue, TalukMaster.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return talukMaster;
	}

	/**
	 * 
	 */
	public void loadLazyList() {

		talukMasterLazyList = new LazyDataModel<MasterSearchDTO>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<MasterSearchDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				List<MasterSearchDTO> data = new ArrayList<MasterSearchDTO>();

				try {
					sortingField = sortField;
					sortingOrder = sortOrder;
					filtersMap = filters;

					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					data = mapper.readValue(jsonResponse, new TypeReference<List<MasterSearchDTO>>() {
					});

					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						listSize = baseDTO.getTotalRecords();

					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				log.info(data);
				return data;
			}

			@Override
			public Object getRowKey(MasterSearchDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MasterSearchDTO getRowData(String rowKey) {

				@SuppressWarnings("unchecked")
				List<MasterSearchDTO> responseList = (List<MasterSearchDTO>) getWrappedData();

				Long value = Long.valueOf(rowKey);

				for (MasterSearchDTO res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						return res;
					}
				}
				return null;
			}

		};
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		masterSearchDTO = ((MasterSearchDTO) event.getObject());
		disableAddButton = true;
	}

	public void onPageLoad() {
		masterSearchDTO = new MasterSearchDTO();
		disableAddButton = false;
	}

	public BaseDTO getSearchData(Integer first, Integer pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {

		BaseDTO baseDTO = new BaseDTO();

		log.info("page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:[" + sortOrder + "] "
				+ "sortField:[" + sortField + "]");

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			log.info("Key : " + entry.getKey() + " Value : " + entry.getValue().toString());
		}

		MasterSearchDTO request = getSearchRequestObject(first, pageSize, sortField, sortOrder, filters);

		String URL = appPreference.getPortalServerURL() + "/taluk/list";

		baseDTO = httpService.post(URL, request);

		return baseDTO;
	}

	private MasterSearchDTO getSearchRequestObject(Integer first, Integer pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) throws ParseException {
		MasterSearchDTO request = new MasterSearchDTO();

		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();

			if (entry.getKey().equals("talukCodeOrName")) {
				log.info("talukCodeOrName : " + value);
				request.setTalukCodeOrName(value);
			}

			if (entry.getKey().equals("cityCodeOrName")) {
				log.info("cityCodeOrName : " + value);
				request.setCityCodeOrName(value);
			}

			if (entry.getKey().equals("districtCodeOrName")) {
				log.info("districtCodeOrName : " + value);
				request.setDistrictCodeOrName(value);
			}

			if (entry.getKey().equals("stateCodeOrName")) {
				log.info("stateCodeOrName : " + value);
				request.setStateCodeOrName(value);
			}

			if (entry.getKey().equals("countryCodeOrName")) {
				log.info("countryCodeOrName : " + value);
				request.setCountryCodeOrName(value);
			}

			if (entry.getKey().equals("status")) {
				log.info("status : " + value);
				request.setStatus(Boolean.valueOf(value));
			}

		}

		return request;
	}

	/*******************************************************************************************************/

	@Getter
	@Setter
	CountryMaster countryMaster = new CountryMaster();

	@Getter
	@Setter
	TalukMaster talukMaster;

	@Getter
	@Setter
	StateMaster stateMaster;

	@Getter
	@Setter
	DistrictMaster districtMaster;

	@Getter
	@Setter
	CityMaster cityMaster;

	@PostConstruct
	public void init() {
		log.info(" DistrictMasterBean Init");
	}

	public void onCountryChange() {
		log.info("onCountryChange() - called ");
	}

	public void onStateChange() {
		log.info("onStateChange() - called ");
	}

	public String create() {

		log.info("create method is executing..");

		String URL = appPreference.getPortalServerURL() + "/taluk/create";

		try {
			BaseDTO baseDTO = httpService.post(URL, talukMaster);

			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				if (action.equalsIgnoreCase("ADD"))
					errorMap.notify(ErrorDescription.TALUK_ADDED_SUCCESSFULLY.getCode());
				else
					errorMap.notify(ErrorDescription.TALUK_UPDATED_SUCCESSFULLY.getCode());
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return LIST_PAGE_URL;
	}

	public void processDelete() {
		if (masterSearchDTO == null || masterSearchDTO.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_TALUK_MASTER.getCode());
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {

		String URL = appPreference.getPortalServerURL() + "/taluk/delete/" + masterSearchDTO.getId();

		try {
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				errorMap.notify(ErrorDescription.TALUK_DELETED_SUCCESSFULLY.getCode());
				disableAddButton = false;
				return gotoPage("LIST");
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
			return null;
		}

		return null;
	}

	public void getCityByDistrictId() {

		log.info("<--- Inside getCityByDistrictId---> ");

		int listSize = 0;
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/city/getdistrict/"
					+ talukMaster.getDistrictMaster().getId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				if (baseDTO.getResponseContents() != null) {
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					cityMasterList = mapper.readValue(jsonResponse, new TypeReference<List<CityMaster>>() {
					});
					listSize = cityMasterList == null ? 0 : cityMasterList.size();
					log.info("Active Citys with Size of : " + listSize);
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("<--- Exception in loadActiveCities() --->", e);
		}
	}

}
