package in.gov.cooptex.masters.ui.bean;

import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.model.VenueType;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("venueTypeBean")
@Scope("session")
@Log4j2
public class VenueTypeBean {

	final String LIST_PAGE = "/pages/masters/listVenueType.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/masters/createVenueType.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String VENUE_TYPE_URL = AppUtil.getPortalServerURL() + "/venuetype";

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	VenueType venueType, selectedVenueType;

	@Getter
	@Setter
	LazyDataModel<VenueType> lazyVenueTypeList;

	List<VenueType> venueTypeList = null;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	public String showVenueTypeListPage() {
		log.info("<==== Starts VenueTypeBean.showVenueTypeListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		venueType = new VenueType();
		selectedVenueType = new VenueType();
		loadLazyVenueTypeList();
		log.info("<==== Ends VenueTypeBean.showVenueTypeListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyVenueTypeList() {
		log.info("<===== Starts VenueTypeBean.loadLazyVenueTypeList ======>");

		lazyVenueTypeList = new LazyDataModel<VenueType>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<VenueType> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = VENUE_TYPE_URL + "/getallvenuetypelistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						venueTypeList = mapper.readValue(jsonResponse, new TypeReference<List<VenueType>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return venueTypeList;
			}

			@Override
			public Object getRowKey(VenueType res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VenueType getRowData(String rowKey) {
				for (VenueType venueType : venueTypeList) {
					if (venueType.getId().equals(Long.valueOf(rowKey))) {
						selectedVenueType = venueType;
						return venueType;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends VenueTypeBean.loadLazyVenueTypeList ======>");
	}

	public String showVenueTypeListPageAction() {
		log.info("<===== Starts VenueTypeBean.showVenueTypeListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("create")
					&& (selectedVenueType == null || selectedVenueType.getId() == null)) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.SELECT_VENUE_TYPE).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("create")) {
				log.info("create venueType called..");
				venueType = new VenueType();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit venueType called..");
				String url = VENUE_TYPE_URL + "/getbyid/" + selectedVenueType.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					venueType = mapper.readValue(jsonResponse, new TypeReference<VenueType>() {
					});
					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else {
				log.info("delete venueType called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVenueTypeDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured in showVenueTypeListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends VenueTypeBean.showVenueTypeListPageAction ===========>" + action);
		return null;
	}

	public String deleteConfirm() {
		log.info("<===== Starts VenueTypeBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(VENUE_TYPE_URL + "/deletebyid/" + selectedVenueType.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_DELETE_SUCCESS).getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVenueTypeDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete venueType ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends VenueTypeBean.deleteConfirm ===========>");
		return showVenueTypeListPage();
	}

	public String saveOrUpdate() {
		log.info("<==== Starts VenueTypeBean.saveOrUpdate =======>");
		try {
			if (venueType.getCode() == null || venueType.getCode().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_CODE_EMPTY).getCode());
				return null;
			}
			if (venueType.getName() == null || venueType.getName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_NAME_EMPTY).getCode());
				return null;
			}
			if (venueType.getName().trim().length() < 3 || venueType.getName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_NAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (venueType.getLocalName() == null || venueType.getLocalName().trim().isEmpty()) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_LNAME_EMPTY).getCode());
				return null;
			}
			if (venueType.getLocalName().trim().length() < 3 || venueType.getLocalName().trim().length() > 75) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_LNAME_MIN_MAX_ERROR).getCode());
				return null;
			}
			if (venueType.getActiveStatus() == null) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_STATUS_EMPTY).getCode());
				return null;
			}
			String url = VENUE_TYPE_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, venueType);
			if (response != null && response.getStatusCode() == 0) {
				log.info("venueType saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_SAVE_SUCCESS).getCode());
				else
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VENUE_TYPE_UPDATE_SUCCESS).getCode());
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends VenueTypeBean.saveOrUpdate =======>");
		return showVenueTypeListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts VenueTypeBean.onRowSelect ========>" + event);
		selectedVenueType = ((VenueType) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends VenueTypeBean.onRowSelect ========>");
	}

}
