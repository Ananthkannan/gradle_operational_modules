package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStatus;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.RosterReservation;
import in.gov.cooptex.core.model.StaffEligibilityStatement;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author saranya
 *
 */
@Log4j2
@Scope("session")
@Service("rosterReservationBean")
public class RosterReservationBean extends CommonBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;

	String jsonResponse;

	ObjectMapper mapper;

	@Getter
	@Setter
	RosterReservation rosterReservation;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	/*
	 * @Getter @Setter List<RosterReservation> rosterList;
	 */

	@Getter
	@Setter
	LazyDataModel<RosterReservation> rosterList;

	@Getter
	@Setter
	RosterReservation selectedRosterReservation;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	List<Department> departmentList;

	@Autowired
	StaffEligibilityBean staffEligibilityBean;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteFlag = false;

	@Getter
	@Setter
	Boolean generateFlag = false;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	List<RosterReservation> roasterReservationList = new ArrayList<RosterReservation>();

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String approveRejectAction;

	final String APPROVING_PAGE = "/pages/personnelHR/ApprovalRosterReservation.xhtml?faces-redirect=true";

	/**
	 * purpose:Initial data once roster is generated based on from and to point
	 * 
	 * @return redirect to create roster page
	 */
	public String loadInitialData() {
		mapper = new ObjectMapper();
		log.info("RosterReservationBean.loadInitialData");
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());
		try {
			if (action.equals("GENERATE")) {
				if (staffEligibilityBean.getSelectedStaffEligibilityStmt() != null
						&& staffEligibilityBean.getSelectedStaffEligibilityStmt().getRecruitmentForPost() != null
						&& staffEligibilityBean.getSelectedStaffEligibilityStmt().getRecruitmentForPost()
								.getId() != null) {
					String url = SERVER_URL + "/rosterreservation/getInProgressData/"
							+ staffEligibilityBean.getSelectedStaffEligibilityStmt().getRecruitmentForPost().getId();
					log.info("======Url is======" + url);
					BaseDTO baseDto = httpService.get(url);
					if (baseDto != null && baseDto.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(baseDto.getResponseContents());
						roasterReservationList = mapper.readValue(jsonResponse,
								new TypeReference<List<RosterReservation>>() {
								});
						if (!roasterReservationList.isEmpty()) {
							log.info("=======roasterReservationList size=======" + roasterReservationList.size());
							Util.addWarn("Previous Roster Points Approval/Rejection is Pending");
							return null;
						}
					}
				}

			}
			action = "INITIAL";
			rosterReservation = new RosterReservation();
			if (staffEligibilityBean.selectedStaffEligibilityStmt == null) {
				log.warn("selected staff eligibility statement empty");
				Util.addWarn("Please select one staff eligibility");
				return null;
			}
			if (staffEligibilityBean.selectedStaffEligibilityStmt.getStatus().equalsIgnoreCase("InProgress")) {
				errorMap.notify(ErrorDescription.STAFF_ELIGIBILITY_NOT_YET_APPROVED.getErrorCode());
				return null;
			}
			if (staffEligibilityBean.selectedStaffEligibilityStmt.getStatus().equalsIgnoreCase("Rejected")) {
				errorMap.notify(ErrorDescription.CANNOT_GENERATE_ROSTER_NOT_APPROVED.getErrorCode());
				return null;
			}
			log.info("Server url...." + SERVER_URL + "/rosterreservation/getintial/"
					+ staffEligibilityBean.selectedStaffEligibilityStmt.getId());
			BaseDTO response = httpService.get(SERVER_URL + "/rosterreservation/getintial/"
					+ staffEligibilityBean.selectedStaffEligibilityStmt.getId());

			log.info("basedto..." + response);
			if (response.getStatusCode() != 5119) {
				errorMap.notify(response.getStatusCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			rosterReservation = mapper.readValue(jsonResponse, new TypeReference<RosterReservation>() {
			});

		} catch (Exception e) {

			log.error("Exception occured while fetching initial data...", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

		return "/pages/personnelHR/createRosterReservation.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:post roster reservation details generated to server
	 * 
	 * @return redirect to list page or notify any errors if occured
	 */
	public String saveRoster() {
		log.info("RosterReservationBean.saveRoster called ");
		try {
			mapper = new ObjectMapper();
			UserMaster userrMaster = loginBean.getUserDetailSession();
			if (rosterReservation == null) {
				AppUtil.addWarn("Roster is empty");
				return null;
			}
			rosterReservation.setCreatedBy(userrMaster);

			if (userMaster != null) {
				rosterReservation.setForwardToId(userMaster.getId());

			}
			log.info("roster reservation remarks...." + rosterReservation.getRemarks());
			BaseDTO response = httpService.post(SERVER_URL + "/rosterreservation/add", rosterReservation);
			if (response != null) {
				log.info("Response recieved from RestAPI ..." + response);
				errorMap.notify(response.getStatusCode());
				loadReservationList();
			} else {
				log.info("Response from Rest API is null....");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("Exception occured while saving roster .... ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return "/pages/personnelHR/listRosterReservation.xhtml?faces-redirect=true";
	}

	/**
	 * purpose : load roster reservation list
	 * 
	 * @return redirect to list page
	 */
	public String loadReservationList() {
		log.info("RosterReservationBean.loadReservationList called");
		try {

			forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.ASSET_REQUEST.name());

			selectedRosterReservation = new RosterReservation();
			loadLazyRosterList();
			loadDepartment();
		} catch (Exception e) {
			log.error("Exception Occured while fetching rosterList ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/listRosterReservation.xhtml?faces-redirect=true";
	}

	/**
	 * purpose : load department master
	 * 
	 */
	public void loadDepartment() {
		log.info("... Load department called ....");
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.get(SERVER_URL + "/department/load");
			jsonResponse = mapper.writeValueAsString(response.getResponseContents());
			departmentList = mapper.readValue(jsonResponse, new TypeReference<List<Department>>() {
			});
		} catch (Exception e) {
			log.error("Exception occured while loading department....", e);
		}
	}

	/**
	 * purpose:get selected roster reservation details and redirect to view page or
	 * notify any error occurs
	 * 
	 * @return
	 */
	public String viewRoster() {
		try {
			if (selectedRosterReservation == null) {
				AppUtil.addWarn("Please select one roster");
				return null;
			}
			action = "VIEW";
			rosterReservation = selectedRosterReservation;
			log.info("Server url...." + SERVER_URL + "/rosterreservation/get/" + selectedRosterReservation.getId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0));
			BaseDTO response = httpService
					.get(SERVER_URL + "/rosterreservation/get/" + selectedRosterReservation.getId() + "/"
							+ (selectedNotificationId != null ? selectedNotificationId : 0));

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			rosterReservation = mapper.readValue(jsonResponse, new TypeReference<RosterReservation>() {
			});

			log.info("Roster child list...");
			long sum = rosterReservation.getRosterReservationList().stream()
					.filter(o -> o.getTotalCount() != null && o.getTotalCount() >= 0).mapToLong(o -> o.getTotalCount())
					.sum();
			rosterReservation.setTotalCount(sum);
			if (rosterReservation.getStatus() != null && rosterReservation.getStatus().equals("InProgress")) {
				String diffDays = Util.diffDaysBetweenTwoDates(rosterReservation.getCreatedDate(), new Date());
				rosterReservation.setNoOfDaysForApprove(diffDays);
			}

			if (selectedRosterReservation != null) {
				setPreviousApproval(selectedRosterReservation.getFinalApprove());
				if ("VIEW".equalsIgnoreCase(action)) {
					if (selectedRosterReservation.getForwardToId() != null) {
						if (loginBean.getUserMaster().getId().equals(selectedRosterReservation.getForwardToId())) {
							if (ApprovalStatus.REJECTED.equals(selectedRosterReservation.getFinalstage())) {
								approvalFlag = false;
							} else {
								approvalFlag = true;
							}
						} else {
							approvalFlag = false;
						}
					}
					setFinalApproveFlag(selectedRosterReservation.getFinalApprove());
					if (selectedRosterReservation.getFinalApprove()
							&& loginBean.getUserMaster().getId().equals(selectedRosterReservation.getForwardToId())
							&& !selectedRosterReservation.getStatus()
									.equals(ApprovalStatus.FINAL_APPROVED.toString())) {

						buttonFlag = true;

						return APPROVING_PAGE;
					}
					selectedRosterReservation.setApprovalRemarks(null);
				}
			}

		} catch (Exception e) {
			log.error("Exception occured while viewing roster....", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return "/pages/personnelHR/viewRosterReservation.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:redirect from create,view pages to list page and reload the list
	 * 
	 * @return
	 */
	public String cancel() {
		action = "";
		selectedRosterReservation = new RosterReservation();
		return loadReservationList();
	}

	/**
	 * purpose:clear the fields in search and reload the list
	 * 
	 * @return
	 */
	public String clear() {
		action = "";
		selectedRosterReservation = new RosterReservation();
		rosterReservation = new RosterReservation();
		return loadReservationList();
	}

	/**
	 * purpose : will display pop up when delete clicked in list page
	 * 
	 * @return
	 */
	public String deleteRoster() {
		try {
			if (selectedRosterReservation == null) {
				AppUtil.addWarn("Please select one roster");
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmRosterDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured while deleting .... ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose:will delete the selected record if confirmed in dialog and redirect
	 * to list page
	 * 
	 * @return
	 */
	public String confirmDeleteRoster() {
		log.info("... delete Roster called ....");

		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService
					.get(SERVER_URL + "/rosterreservation/delete/" + selectedRosterReservation.getId());
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while delete confirm....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadReservationList();
	}

	/**
	 * purpose: get selected roster and details will be displayed in approval page
	 * 
	 * @return to roster approval page
	 */
	public String approveRoster() {
		try {
			log.info("Approve roster...... called");
			if (selectedRosterReservation == null) {
				AppUtil.addWarn("Please select one roster");
				return null;
			}
			if (selectedRosterReservation.getStatus().equals("Approved")) {
				AppUtil.addWarn("Roster already approved");
				return null;
			}
			if (selectedRosterReservation.getStatus().equals("Rejected")) {
				AppUtil.addWarn("Roster rejected once cannot be approved");
				return null;
			}
			rosterReservation = selectedRosterReservation;
			log.info("Server url...." + SERVER_URL + "/rosterreservation/get/" + selectedRosterReservation.getId());
			BaseDTO response = httpService
					.get(SERVER_URL + "/rosterreservation/get/" + selectedRosterReservation.getId());

			log.info("basedto..." + response);
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}

			jsonResponse = mapper.writeValueAsString(response.getResponseContent());
			rosterReservation = mapper.readValue(jsonResponse, new TypeReference<RosterReservation>() {
			});

			long sum = rosterReservation.getRosterReservationList().stream().filter(o -> o.getTotalCount() >= 0)
					.mapToLong(o -> o.getTotalCount()).sum();
			rosterReservation.setTotalCount(sum);
			if (rosterReservation.getStatus() != null && rosterReservation.getStatus().equals("InProgress")) {
				String diffDays = Util.diffDaysBetweenTwoDates(rosterReservation.getCreatedDate(), new Date());
				rosterReservation.setNoOfDaysForApprove(diffDays);
			}
		} catch (Exception e) {
			log.error("Exception occured while approving roster", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return "/pages/personnelHR/ApprovalRosterReservation.xhtml?faces-redirect=true";
	}

	/**
	 * purpose:display confirmation pop up before approval
	 * 
	 * @return
	 */
	public String beforeRosterReject() {
		log.info("before roster reservation...." + selectedRosterReservation.getId());
		try {
			if (selectedRosterReservation == null) {
				Util.addWarn("Please select one roster");
				return null;
			} else {
				approveRejectAction = ApprovalStatus.REJECTED.toString();
				rosterReservation.setStatus(ApprovalStatus.REJECTED.toString());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmRosterApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/**
	 * purpose:display confirmation pop up before approval
	 * 
	 * @return
	 */
	public String beforeRosterFinalApproval() {
		log.info("before roster reservation...." + selectedRosterReservation.getId());
		try {
			if (selectedRosterReservation == null) {
				Util.addWarn("Please select one roster");
				return null;
			} else {
				approveRejectAction = ApprovalStatus.APPROVED.toString();
				rosterReservation.setStatus(ApprovalStatus.FINAL_APPROVED.toString());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmRosterApprove').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	/**
	 * purpose : roster will be approved once confirmation is given
	 * 
	 * @return redirect to roster list page
	 */
	public String confirmApproveRoster() {
		log.info("confirm approval....." + rosterReservation.getStatus());
		log.info("confirm approval.selected...." + selectedRosterReservation.getStatus());
		try {
			mapper = new ObjectMapper();
			BaseDTO response = httpService.put(SERVER_URL + "/rosterreservation/approveorreject", rosterReservation);
			errorMap.notify(response.getStatusCode());
		} catch (Exception e) {
			log.error("Exception occured while loading department....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return loadReservationList();
	}

	/**
	 * purpose:load roster list using lazy datamodel
	 * 
	 */
	public void loadLazyRosterList() {

		log.info("< --  Start Employee Lazy Load -- > ");
		rosterList = new LazyDataModel<RosterReservation>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<RosterReservation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<RosterReservation> data = mapper.readValue(jsonResponse,
							new TypeReference<List<RosterReservation>>() {
							});
					log.info(" jsonResponsejsonResponsejsonResponsejsonResponse" + jsonResponse);

					if (data == null) {
						log.info(" Roster search response Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Roster search Successfully Completed");
					} else {
						log.error(" Roster search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading roster data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(RosterReservation res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public RosterReservation getRowData(String rowKey) {
				List<RosterReservation> responseList = (List<RosterReservation>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (RosterReservation res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedRosterReservation = res;
						return res;
					}
				}
				return null;
			}

		};

	}

	/**
	 * purpose will call server when search filtered
	 * 
	 * @param first     - page number
	 * @param pageSize  - how many records need to be fetched
	 * @param sortField - which filed to be sorted
	 * @param sortOrder - sort order that to be followed (ascending or descending)
	 * @param filters   - search filters
	 * @return BaseDTO with list of roster reservation based filters and pages size
	 */
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			RosterReservation request = new RosterReservation();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("referenceNumber")) {
					log.info("Reference NUmber : " + value);
					request.setReferenceNumber(value);
				}

				if (entry.getKey().equals("recruitmentforPost")) {
					log.info("recruitment for Post : " + value);
					request.setRecruitmentforPost(value);
				}

				if (entry.getKey().equals("recruitmentYear")) {
					log.info("Reference NUmber : " + value);
					request.setRecruitmentYear(Integer.valueOf(value));
				}

				if (entry.getKey().equals("referenceNumber")) {
					log.info("Reference NUmber : " + value);
					request.setReferenceNumber(value);
				}
				if (entry.getKey().contains("staffEligibility")) {
					StaffEligibilityStatement stmt = new StaffEligibilityStatement();
					if (entry.getKey().equals("staffEligibility.recruitmentForPost.name")) {
						log.info("recruitment for post : " + value);
						Designation des = new Designation();
						des.setName(value);
						stmt.setRecruitmentForPost(des);

					}

					if (entry.getKey().equals("staffEligibility.recruitmentYear")) {
						log.info("Recruitment year : " + value);

						stmt.setRecruitmentYear(Integer.valueOf(entry.getValue().toString()));

					}
					/*
					 * if (entry.getKey().equals("staffEligibility.department.id")) {
					 * log.info("Department name : " + value); Department dep = new Department();
					 * dep.setName(entry.getValue().toString()); stmt.setDepartment(dep);
					 * 
					 * }
					 */

					request.setStaffEligibility(stmt);
				}
				if (entry.getKey().equals("vacanciesToDistribute")) {
					log.info("VacanciesToDistribute : " + value);
					request.setVacanciesToDistribute(Long.valueOf(value));
				}
				if (entry.getKey().equals("previousRosterPoint")) {
					log.info("PreviousRosterPoint : " + value);
					request.setPreviousRosterPoint(Long.valueOf(value));
				}

				if (entry.getKey().equals("status")) {
					log.info("Ptatus : " + value);
					request.setStatus(value);
				}
				if (entry.getKey().equals("departmentId")) {
					log.info("Ptatus : " + value);
					request.setDepartmentId(Long.valueOf(value));
				}

			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(SERVER_URL + "/rosterreservation/getalllazy", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Roster search generate data ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	/**
	 * purpose:each time row selected in list page this method will be called to
	 * enable,disable buttons in list page
	 * 
	 * @param event
	 */
	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedRosterReservation = ((RosterReservation) event.getObject());

		addButtonFlag = true;
		if (selectedRosterReservation.getStatus().equalsIgnoreCase(ApprovalStatus.FINAL_APPROVED.toString())) {
			deleteFlag = true;
			approveButtonFlag = true;
			generateFlag = false;
			approveButtonFlag = true;

		} else if (selectedRosterReservation.getStatus().equalsIgnoreCase(ApprovalStatus.REJECTED.toString())) {
			approveButtonFlag = true;
			generateFlag = true;
			deleteFlag = false;
		} else if (selectedRosterReservation.getStatus().equalsIgnoreCase(ApprovalStatus.INPROGRESS.toString())) {
			generateFlag = true;
			approveButtonFlag = false;
			deleteFlag = false;
		}
	}

	public String approveFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info(" approveFLow RosterReservation id----------" + selectedRosterReservation.getId());
			baseDTO = new BaseDTO();
			if (previousApproval == false && selectedRosterReservation.getFinalApprove() == true) {
				selectedRosterReservation.setFinalstage(ApprovalStatus.APPROVED.toString());
			} else if (previousApproval == true && selectedRosterReservation.getFinalApprove() == true) {
				selectedRosterReservation.setFinalstage(ApprovalStatus.FINAL_APPROVED.toString());
			} else {
				selectedRosterReservation.setFinalstage(ApprovalStatus.APPROVED.toString());
			}
			log.info("approve remarks=========>" + selectedRosterReservation.getApprovalRemarks());
			String url = SERVER_URL + "/rosterreservation/approveFlow";
			baseDTO = httpService.post(url, selectedRosterReservation);
			if (baseDTO.getStatusCode() == 0) {
				Util.addInfo("Roster Reservation Approved Successfully");
				log.info("Successfully Approved-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadReservationList();
			}
		} catch (Exception e) {
			log.error("approveFLow method inside exception-------", e);
		}
		return null;
	}

	public String rejectFlow() {
		BaseDTO baseDTO = null;
		try {
			log.info("approveFLow RosterReservation id----------" + selectedRosterReservation.getId());
			baseDTO = new BaseDTO();
			selectedRosterReservation.setFinalstage(ApprovalStatus.REJECTED.toString());
			String url = SERVER_URL + "/rosterreservation/rejectFlow";
			baseDTO = httpService.post(url, selectedRosterReservation);
			if (baseDTO.getStatusCode() == 0) {
				Util.addWarn("StaffEligigbility Rejected Successfully");
				log.info("Successfully Rejected-----------------");
				RequestContext.getCurrentInstance().update("growls");
				return loadReservationList();
			}
		} catch (Exception e) {
			log.error("rejectjv method inside exception-------", e);
		}
		return null;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("Claims showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String rosterReservationId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			rosterReservationId = httpRequest.getParameter("rosterReservationId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("staffEligibilityStmtId" + rosterReservationId);
		}
		if (StringUtils.isNotEmpty(rosterReservationId)) {
			selectedRosterReservation = new RosterReservation();
			action = "VIEW";
			selectedRosterReservation.setId(Long.parseLong(rosterReservationId));
			selectedNotificationId = Long.parseLong(notificationId);

			viewRoster();
		}
		log.info(" showViewListPage() Ends");

		return null;
	}

}
