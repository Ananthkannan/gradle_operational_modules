package in.gov.cooptex.masters.ecomm.ui.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

//import org.hamcrest.text.IsEmptyString;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.ItemQRCodeDetailsDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePersonalInfoEmployment;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.StockTransferItems;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.ecomm.model.EcommFilterCategory;
import in.gov.cooptex.ecomm.model.EcommFilterColor;
import in.gov.cooptex.ecomm.model.EcommFilterMaterial;
import in.gov.cooptex.ecomm.model.EcommFilterOcassion;
import in.gov.cooptex.ecomm.model.EcommFilterWork;
import in.gov.cooptex.ecomm.model.EcommMainMenu;
import in.gov.cooptex.ecomm.model.EcommProductConfig;
import in.gov.cooptex.exceptions.EcommerceErrorCode;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.operation.dto.EcommProductConfigDTO;
import in.gov.cooptex.operation.dto.EcommProductConfigListDTO;
import in.gov.cooptex.operation.dto.EcommProductConfigViewDTO;
import in.gov.cooptex.operation.dto.EcommRateConfigDTO;
import in.gov.cooptex.operation.dto.ProductGroupMasterDTO;
import in.gov.cooptex.operation.dto.ProductVarietyMasterDTO;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.operation.rest.ui.StockOutwardPWHBean;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("ecommProductConfigBean")
@Scope("session")
@Log4j2
public class EcommProductConfigBean {

	final String LIST_PAGE = "/pages/ecommerce/admin/listEcommerceProductConfiguration.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/ecommerce/admin/createEcommProdConfig.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/ecommerce/admin/viewEcommerceProductConfiguration.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ECOMM_PRODUCT_CONFIG_URL = AppUtil.getPortalServerURL() + "/ecommProductConfig";

	ObjectMapper mapper;

	@Getter
	@Setter
	private EntityMaster loginEntityMaster = null;

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	EcommProductConfig ecommProductConfig, selectedEcommProductConfig;

	@Getter
	@Setter
	EcommProductConfigListDTO selectedEcommProductConfigList;

	@Getter
	@Setter
	LazyDataModel<EcommProductConfigListDTO> lazyEcommProductConfigList;

	List<EcommProductConfigListDTO> ecommProductConfigList = null;

	@Autowired
	StockOutwardPWHBean stockOutwardPWHBean = new StockOutwardPWHBean();

	@Setter
	@Getter
	EcommProductConfigDTO ecommProductConfig1 = new EcommProductConfigDTO();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false, disableQRCodeButton;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean bulkOrderFlag = false;

	@Getter
	@Setter
	ProductCategory productCategoryDto;

	@Getter
	@Setter
	ProductGroupMaster productGroupDto;

	@Getter
	@Setter
	List<ProductCategory> productCategoryDtoList;

	@Getter
	@Setter
	List<EcommFilterCategory> ecomFilterCategoryList;

	@Getter
	@Setter
	EcommFilterCategory ecomFilterCategory;

	@Getter
	@Setter
	List<EcommFilterMaterial> ecomFilterMaterialList;

	@Getter
	@Setter
	EcommFilterMaterial ecomFilterMaterail;

	@Getter
	@Setter
	List<EcommFilterColor> ecomFilterColorList;

	@Getter
	@Setter
	EcommFilterColor ecomFilterColor;

	@Getter
	@Setter
	List<EcommFilterWork> ecomFilterWorkList;

	@Getter
	@Setter
	EcommFilterWork ecomFilterWork;

	@Getter
	@Setter
	List<EcommFilterOcassion> ecomFilterOcassionList;

	@Getter
	@Setter
	EcommFilterOcassion ecomFilterOcassion;

	@Getter
	@Setter
	List<EcommMainMenu> ecomMainMenuList;

	@Getter
	@Setter
	EcommMainMenu ecomMainMenus;

	@Getter
	@Setter
	List<EcommMainMenu> ecomSubMenuList;

	@Getter
	@Setter
	List<EcommMainMenu> ecomChildMenuList;

	@Getter
	@Setter
	EcommMainMenu ecomSubMenu;

	@Getter
	@Setter
	List<ProductGroupMasterDTO> productGroupDtoList;

	@Getter
	@Setter
	ProductVarietyMasterDTO productVarietyMasterDTO = new ProductVarietyMasterDTO();

	@Getter
	@Setter
	List<ProductVarietyMasterDTO> productVarietyMasterDTOList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupMasterList;

	@Getter
	@Setter
	ProductGroupMaster productGroupMaster;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	StockTransferItems stockTransferItems;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String code;

	@Getter
	@Setter
	String name;

	@Getter
	@Setter
	String lname;

	@Getter
	@Setter
	String atNumber;

	@Setter
	@Getter
	String fileName;

	@Getter
	@Setter
	EcommProductConfigDTO ecommProductConfigDto;

	@Getter
	@Setter
	EcommRateConfigDTO ecommRateConfigDTO = new EcommRateConfigDTO();

	@Getter
	@Setter
	EcommProductConfigViewDTO ecommProductConfigViewDto;

	private long largeImageSize;

	private long mediumImageSize;

	private long smallImageSize;

	private long thumpImageSize;

	@Getter
	@Setter
	private InputStream largeInputStream;

	@Getter
	@Setter
	private InputStream mediumInputStream;

	@Getter
	@Setter
	private InputStream smallInputStream;

	@Getter
	@Setter
	private InputStream thumpInputStream;

	@Getter
	@Setter
	private UploadedFile uploadedFile;

	@Getter
	@Setter
	private UploadedFile largeUploadedFile;

	@Getter
	@Setter
	private UploadedFile mediumUploadedFile;

	@Getter
	@Setter
	private UploadedFile smallUploadedFile;

	@Getter
	@Setter
	private UploadedFile thumUploadedFile;

	@Getter
	@Setter
	private String largeUploadedFilePath;

	@Getter
	@Setter
	private String mediumUploadedFilePath;

	@Getter
	@Setter
	private String smallUploadedFilePath;

	@Getter
	@Setter
	private String thumUploadedFilePath;

	@Getter
	@Setter
	private String largeTimeStamp;

	@Getter
	@Setter
	private String mediumTimeStamp;

	@Getter
	@Setter
	private String smallTimeStamp;

	@Getter
	@Setter
	private String thumTimeStamp;

	long letterSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String selectedFileType;

	@Setter
	@Getter
	private StreamedContent file;

	private String LARGE_IMAGE = "";
	private String MEDIUM_IMAGE = "";
	private String SMALL_IMAGE = "";
	private String THUMP_IMAGE = "";

	final int SMALL_IMAGE_WIDTH = 80;
	final int SMALL_IMAGE_HEIGHT = 100;

	final int MED_IMAGE_WIDTH = 320;
	final int MED_IMAGE_HEIGHT = 520;

	final int LARGE_IMAGE_WIDTH = 1024;
	final int LARGE_IMAGE_HEIGHT = 520;

	final int THUMP_IMAGE_WIDTH = 80;
	final int THUMP_IMAGE_HEIGHT = 100;

	String parentPath = "";

	@Setter
	@Getter
	StreamedContent downloadFile;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	boolean disablethFild;

	@PostConstruct
	public void init() {
		LARGE_IMAGE = commonDataService.getAppKeyValue("ECOMM_PRODUCT_LARGE_IMAGES");
		MEDIUM_IMAGE = commonDataService.getAppKeyValue("ECOMM_PRODUCT_MEDIUM_IMAGES");
		SMALL_IMAGE = commonDataService.getAppKeyValue("ECOMM_PRODUCT_SMALL_IMAGES");
		THUMP_IMAGE = commonDataService.getAppKeyValue("ECOMM_PRODUCT_THUMBUNAIL_IMAGES");
		showEcommProductConfigListPage();
		ecommProductConfigDto = new EcommProductConfigDTO();
	}

	public String showEcommProductConfigListPage() {
		log.info("<==== Starts EcommFilterCategoryBean.showEcommFilterCategoryListPage =====>");
		mapper = new ObjectMapper();
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		bulkOrderFlag = false;
		disableQRCodeButton = false;
		ecommProductConfig = new EcommProductConfig();
		ecommRateConfigDTO = new EcommRateConfigDTO();
		selectedEcommProductConfigList = new EcommProductConfigListDTO();
		disablethFild = false;
		loadLazyEcommProductConfigList();
		log.info("<==== Ends EcommFilterCategoryBean.showEcommFilterCategoryListPage =====>");
		return LIST_PAGE;
	}

	public void loadLazyEcommProductConfigList() {
		log.info("<===== Starts EcommProductConfigBean.loadLazyEcommProductConfigList ======>");

		lazyEcommProductConfigList = new LazyDataModel<EcommProductConfigListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EcommProductConfigListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = ECOMM_PRODUCT_CONFIG_URL + "/getConfigLazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						ecommProductConfigList = mapper.readValue(jsonResponse,
								new TypeReference<List<EcommProductConfigListDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return ecommProductConfigList;
			}

			@Override
			public Object getRowKey(EcommProductConfigListDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public EcommProductConfigListDTO getRowData(String rowKey) {
				for (EcommProductConfigListDTO ecommProductConfig : ecommProductConfigList) {
					if (ecommProductConfig.getId().equals(Long.valueOf(rowKey))) {
						selectedEcommProductConfigList = ecommProductConfig;
						return ecommProductConfig;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends EcommProductConfigBean.loadLazyEcommProductConfigList ======>");
	}

	public String showEcommProductConfigPageAction() {
		log.info("<===== Starts EcommProductConfigBean.showEcommProductConfigListPageAction ===========>" + action);
		try {
			if (action.equalsIgnoreCase("create")) {
				log.info("create ecommProductConfig called..");
				ecommProductConfigDto = new EcommProductConfigDTO();
				ecommProductConfig1 = new EcommProductConfigDTO();
				ecommProductConfigViewDto = new EcommProductConfigViewDTO();
				bulkOrderFlag = false;
				getCategoryMasterList();
				getFilterCategoryList();
				getFilterMaterialList();
				getFilterColorList();
				getFilterOcassionList();
				getFilterWorkList();
				getAllecomMainMenuList();
				getEcomSubMenuList();
				return ADD_PAGE;
			} else if (action.equalsIgnoreCase("Edit")) {
				log.info("edit ecommProductConfig called..");
				String url = ECOMM_PRODUCT_CONFIG_URL + "/getEdit/" + selectedEcommProductConfigList.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					ecommProductConfigDto = mapper.readValue(jsonResponse, new TypeReference<EcommProductConfigDTO>() {
					});

//					if (ecommProductConfigDto != null) {
//						getCategoryMasterList();
//						onProductCategoryChange();
//						onProductGroupChange();
//						onProductVarietyChange();
//						getFilterCategoryList();
//						getFilterMaterialList();
//						getFilterColorList();
//						getFilterWorkList();
//						getFilterOcassionList();
//						getAllecomMainMenuList();
//						if (ecommProductConfigDto.getProductVarityMaster().getBulkorderAllowed() != null
//								&& !ecommProductConfigDto.getProductVarityMaster().getBulkorderAllowed()) {
//							onChangeMainMenu();
//							onChangeSubMenu();
//							ecommPriceCalculate(ecommProductConfigDto.getActualPrice(),
//									ecommProductConfigDto.getPriceIncrease());
//						}
//					}

					return ADD_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("View")) {
				log.info("View ecommProductConfig called....");
				bulkOrderFlag = false;
				ecommProductConfigViewDto = new EcommProductConfigViewDTO();
				String url = ECOMM_PRODUCT_CONFIG_URL + "/getView/" + selectedEcommProductConfigList.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					ecommProductConfigViewDto = mapper.readValue(jsonResponse,
							new TypeReference<EcommProductConfigViewDTO>() {
							});

					if (ecommProductConfigViewDto.getMainMenuCode() != null
							&& !ecommProductConfigViewDto.getMainMenuCode().equals(""))
						ecommPriceCalculate(ecommProductConfigDto.getActualPrice(),
								ecommProductConfigDto.getPriceIncrease());
					else
						bulkOrderFlag = true;

					return VIEW_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}

			} else if (action.equalsIgnoreCase("Delete")) {
				log.info("delete ecommProductConfig called.....");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEcommProductConfigDelete').show();");
			} else {
				return LIST_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception occured in showEcommProductConfigListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends EcommProductConfigBean.showEcommProductConfigListPageAction ===========>" + action);
		return null;
	}

	public void getCategoryMasterList() {
		log.info("===getCategoryMasterList===   Start");
		BaseDTO baseDTO;
		try {
			String url = ECOMM_PRODUCT_CONFIG_URL + "/getProductCategory";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
			});
			log.info("<<==== getCategoryMasterList List Sise::: " + productCategoryDtoList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void onProductCategoryChange() {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		if (ecommProductConfigDto.getProductCategory() != null
				&& ecommProductConfigDto.getProductCategory().getId() != null) {
			Long id = ecommProductConfigDto.getProductCategory().getId();
			productGroupList(id);
		}
	}

	public void productGroupList(Long categoryId) {
		log.info("EcommProductConfigBean onProductCategoryChange------#Start");
		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductGroup/" + categoryId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productGroupMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductGroupMaster>>() {
				});
				log.info("GroupList with Size of : " + productGroupMasterList.size());

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getGropByCategory() --->", exp);
		}
	}

	public void onProductGroupChange() {
		log.info("EcommProductConfigBean onProductGroupChange------#Start");
		if (ecommProductConfigDto.getProductGroupMaster() != null
				&& ecommProductConfigDto.getProductGroupMaster().getId() != null) {
			Long id = ecommProductConfigDto.getProductGroupMaster().getId();
			productVarietyList(id);
		}
	}

	public void productVarietyList(Long groupId) {
		log.info("---getProductVarietyList-----");
		productVarietyMasterDTOList = new ArrayList<ProductVarietyMasterDTO>();
		try {
			String requestPath = ECOMM_PRODUCT_CONFIG_URL + "/getProductVariety/" + groupId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				productVarietyMasterList = mapper.readValue(jsonValue, new TypeReference<List<ProductVarietyMaster>>() {
				});
				log.info("VariteyList with Size of : " + productVarietyMasterDTOList.size());

			}
		} catch (Exception exp) {
			log.error("<--- Exception in getCategoryVarity() --->", exp);
		}
	}

	public void onProductVarietyChange() {
		log.info("EcommProductConfigBean onProductVarietyChange------#Start");
		bulkOrderFlag = false;
		if (ecommProductConfigDto.getProductVarityMaster() != null
				&& ecommProductConfigDto.getProductVarityMaster().getId() != null) {
			Long id = ecommProductConfigDto.getProductVarityMaster().getId();
			log.info("Bulk order is..." + ecommProductConfigDto.getProductVarityMaster().getBulkorderAllowed());
			if (ecommProductConfigDto.getProductVarityMaster().getBulkorderAllowed() != null
					&& ecommProductConfigDto.getProductVarityMaster().getBulkorderAllowed()) {
				log.info("bulk order flag is change true...");
				bulkOrderFlag = true;
			}

			getProductStockItems(id);
		}
	}
	
	public List<ProductVarietyMaster> productAutoComplete(String productCodeOrName) {
		log.info("Received productVarietyMasterList ");
		productVarietyMasterList = new ArrayList<ProductVarietyMaster>();
		try {
			//notEligibleFoSsave = true;
			BaseDTO baseDTO = null;
			String url = SERVER_URL + appPreference.getOperationApiUrl()
					+ "/stockItemInwardPNS/getAllProductVarietyMasterList/" + ecommProductConfigDto.getProductGroupMaster().getId()
					+ "/" + productCodeOrName;
			baseDTO = httpService.get(url);
			log.info("::: Retrieved productVarietyMasterList :");
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else {
				log.warn("productVarietyMasterList Error *** :");
			}
		} catch (Exception ee) {
			log.error("Exception Occured in productVarietyMasterList load ", ee);
		}
		return productVarietyMasterList;
	}

	public void getProductStockItems(Long varityId) {
		log.info("-EcommProductConfigBean getStockItems-----");
		ecommProductConfigDto.setStockTransferItems(new StockTransferItems());
		try {
			String requestPath = SERVER_URL + "/ecommProductConfig/getStockTransferItem/" + varityId;
			BaseDTO baseDTO = httpService.get(requestPath);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();

				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				stockTransferItems = mapper.readValue(jsonValue, new TypeReference<StockTransferItems>() {
				});
				ecommProductConfigDto.setStockTransferItems(stockTransferItems);
				log.info("stockTransferItems of : " + stockTransferItems);

			}
		} catch (Exception exp) {
			log.error("<--- Exception in onProductVarietyChange() --->", exp);
		}
	}

	public void getFilterCategoryList() {
		log.info("===EcommProductConfigBean getFilterCategoryList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommfiltercategory/getAll";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			ecomFilterCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<EcommFilterCategory>>() {
			});
			log.info("<<==== getFilterCategoryList List Sise::: " + ecomFilterCategoryList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void getFilterMaterialList() {
		log.info("===EcommProductConfigBean getFilterMaterialList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getFilterMaterailList";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomFilterMaterialList = mapper.readValue(jsonResponse, new TypeReference<List<EcommFilterMaterial>>() {
			});
			log.info("<<==== getFilterMaterialList List Sise::: " + ecomFilterMaterialList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void getFilterColorList() {
		log.info("===EcommProductConfigBean getFilterMaterialList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getFilterColorList";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomFilterColorList = mapper.readValue(jsonResponse, new TypeReference<List<EcommFilterColor>>() {
			});
			log.info("<<==== getFilterColorList List Sise::: " + ecomFilterColorList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void getFilterWorkList() {
		log.info("===EcommProductConfigBean getFilterWorkList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getFilterWorkList";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomFilterWorkList = mapper.readValue(jsonResponse, new TypeReference<List<EcommFilterWork>>() {
			});
			log.info("<<==== getFilterColorList List Sise::: " + ecomFilterColorList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void getFilterOcassionList() {
		log.info("===EcommProductConfigBean getFilterMaterialList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getFilterOcassionList";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomFilterOcassionList = mapper.readValue(jsonResponse, new TypeReference<List<EcommFilterOcassion>>() {
			});
			log.info("<<==== getFilterOcassionList List Sise::: " + ecomFilterOcassionList.size());
		} catch (Exception e) {
			log.error("<<==============  error in loadBusinessTypeMaster" + e);
		}
	}

	public void getAllecomMainMenuList() {
		log.info("===EcommProductConfigBean getEcommMainMenuList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getEcommMainMenuList";
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomMainMenuList = mapper.readValue(jsonResponse, new TypeReference<List<EcommMainMenu>>() {
			});
			log.info("<<==== getEcomMainMenuList List Sise::: " + ecomMainMenuList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getEcomMainMenuList" + e);
		}
	}

	public void onChangeMainMenu() {
		log.info("onchange of main menu");
		if (ecommProductConfigDto.getEcommMainMenu() != null
				&& ecommProductConfigDto.getEcommMainMenu().getId() != null) {
			Long id = ecommProductConfigDto.getEcommMainMenu().getId();
			getEcomSubMenuList(id);
		}
	}

	public void getEcomSubMenuList(Long parentId) {
		log.info("===EcommProductConfigBean getEcommMainMenuList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getEcommSubMenuList/" + parentId;
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomSubMenuList = mapper.readValue(jsonResponse, new TypeReference<List<EcommMainMenu>>() {
			});
			log.info("<<==== getEcomSubMenuList List Sise::: " + ecomSubMenuList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getEcomSubMenuList" + e);
		}
	}

	public void onChangeSubMenu() {
		log.info("onchange of Sub menu");
		if (ecommProductConfigDto.getEcommSubMenu() != null
				&& ecommProductConfigDto.getEcommSubMenu().getId() != null) {
			Long id = ecommProductConfigDto.getEcommSubMenu().getId();
			getEcomChildMenuList(id);
		}
	}

	public void getEcomChildMenuList(Long parentId) {
		log.info("===EcommProductConfigBean getEcommMainMenuList===   Start");
		BaseDTO baseDTO;
		try {
			String url = SERVER_URL + "/ecommProductConfig/getEcommSubMenuList/" + parentId;
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			ecomChildMenuList = mapper.readValue(jsonResponse, new TypeReference<List<EcommMainMenu>>() {
			});
			log.info("<<==== getEcomSubMenuList List Sise::: " + ecomSubMenuList.size());
		} catch (Exception e) {
			log.error("<<==============  error in getEcomSubMenuList" + e);
		}
	}

	public void getEcommPrice() {
		ecommPriceCalculate(ecommProductConfigDto.getActualPrice(), ecommProductConfigDto.getPriceIncrease());
	}

	public void ecommPriceCalculate(Double acPrice, Double priceInc) {
		log.info("Start ecommPriceCalculate:");
		if (ecommProductConfigDto != null) {
			log.info("Product config not null calculate");
			Double ecomPrice = 0.0;
			if (acPrice != null && priceInc != null) {
				log.info("Actual price:" + acPrice);
				log.info("Discount Percentage:" + priceInc);

				Double price = ((acPrice / 100) * priceInc);
				ecomPrice = acPrice + price;
				log.info("Caluclated ecommPrice is:" + ecomPrice);
				ecommProductConfigDto.setEcommPrice(ecomPrice);
				ecommProductConfigViewDto.setEcommPrice(ecomPrice);
			}
		}
	}

	public String saveOrUpdate() {
		log.info("<==== Starts EcommFilterCategoryBean.saveOrUpdate =======>");
		try {

			if (ecommProductConfigDto.getqRCode() == null) {
				errorMap.notify(ErrorDescription.STOCK_TRANSFER_QR_CODE_INVALID.getErrorCode());
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			if (ecommProductConfigDto.getEcommPrice() == null || ecommProductConfigDto.getEcommPrice() == 0) {
				AppUtil.addError("E-Commerce Price is required");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}

			if (bulkOrderFlag)
				ecommProductConfigDto.setAvailableQty(0D);

//			UploadAllImages();

			String url = ECOMM_PRODUCT_CONFIG_URL + "/saveorupdate";
			BaseDTO response = httpService.post(url, ecommProductConfigDto);
			if (response != null && response.getStatusCode() == 0) {
				log.info("ecommFilterCategory saved successfully..........");
				if (action.equalsIgnoreCase("Create"))
					errorMap.notify(ErrorDescription.ECOMM_PRODUCT_CONFIG_SAVE_SUCCESS.getCode());
				else
					errorMap.notify(ErrorDescription.ECOMM_PRODUCT_CONFIG_UPDATE_SUCCESS.getCode());

				return showEcommProductConfigListPage();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends EcommFilterCategoryBean.saveOrUpdate =======>");
		return showEcommProductConfigListPage();
	}

	private void UploadAllImages() throws Exception {

		parentPath = commonDataService.getAppKeyValue("ECOMM_IMAGE_PARENT_DIR");
		if (parentPath == null && parentPath.isEmpty()) {
			AppUtil.addWarn("Parent Directory Path Not Added For >>> ECOMM_IMAGE_PARENT_DIR (App_config) ");
			return;
		}
		log.info(" LargerPath details >>>  ParentPath = " + parentPath + " largeUploadedFilePath = "
				+ largeUploadedFilePath + " largeUploadedFile = " + largeUploadedFile + " largeTimeStamp= "
				+ largeTimeStamp);

		String largePath = uploadDocument(parentPath, largeUploadedFilePath, largeUploadedFile, largeTimeStamp,
				largeImageSize, largeInputStream);
		log.info("LargeImage Path ===>  " + largePath);
//		if (largePath.isEmpty() || largePath.equalsIgnoreCase("")) {
//			AppUtil.addWarn("Failed To Upload File To Server");
//			log.error("==> Failed To Upload File To Server");
//			throw new Exception();
//		}
//		ecommProductConfigDto.setLargImagePath(largePath);

		if (largePath != null && !largePath.isEmpty()) {
			ecommProductConfigDto.setLargImagePath(largePath);
		}

		String mediumPath = uploadDocument(parentPath, mediumUploadedFilePath, mediumUploadedFile, mediumTimeStamp,
				mediumImageSize, mediumInputStream);
		log.info("mediumPath Path ===>  " + mediumPath);

//		if (mediumPath.isEmpty() || mediumPath.equalsIgnoreCase("")) {
//			AppUtil.addWarn("Failed To Upload File To Server");
//			log.error("==> Failed To Upload File To Server");
//			throw new Exception();
//		}
//
//		ecommProductConfigDto.setMediumImagePath(mediumPath);

		if (mediumPath != null && !mediumPath.isEmpty()) {
			ecommProductConfigDto.setMediumImagePath(mediumPath);
		}

		String smallPath = uploadDocument(parentPath, smallUploadedFilePath, smallUploadedFile, smallTimeStamp,
				smallImageSize, smallInputStream);
		log.info("smallPath Path ===>  " + smallPath);
//		if (smallPath.isEmpty() || smallPath.equalsIgnoreCase("")) {
//			AppUtil.addWarn("Failed To Upload File To Server");
//			log.error("==> Failed To Upload File To Server");
//			throw new Exception();
//		}
//
//		ecommProductConfigDto.setSmallImagePath(smallPath);

		if (smallPath != null && !smallPath.isEmpty()) {
			ecommProductConfigDto.setSmallImagePath(smallPath);
		}

		String thumPath = uploadDocument(parentPath, thumUploadedFilePath, thumUploadedFile, thumTimeStamp,
				thumpImageSize, thumpInputStream);
		log.info("ThumPath Path ===>  " + thumPath);

//		if (thumPath.isEmpty() || thumPath.equalsIgnoreCase("")) {
//			AppUtil.addWarn("Failed To Upload File To Server");
//			log.error("==> Failed To Upload File To Server");
//			throw new Exception();
//		}
//
//		ecommProductConfigDto.setThumbnailImagePath(thumPath);

		if (thumPath != null && !thumPath.isEmpty()) {
			ecommProductConfigDto.setThumbnailImagePath(thumPath);
		}

	}

	public boolean productValidate(boolean valid) {
		valid = true;
		FacesContext fc = FacesContext.getCurrentInstance();
		if (ecommProductConfigDto.getAvailableQty() == null || ecommProductConfigDto.getAvailableQty() <= 0) {
			fc.addMessage("receivedate", new FacesMessage(ErrorCodeDescription.getDescription(35001)));
			valid = false;
		}
		if (ecommProductConfigDto.getAvailableQty() == null || ecommProductConfigDto.getAvailableQty() <= 0) {
			fc.addMessage("receivedate", new FacesMessage(ErrorCodeDescription.getDescription(35001)));
			valid = false;
		}
		return valid;
	}

	public void largeImageUpload(FileUploadEvent event) {
		String filePath = "";
		try {
			filePath = fileUpload(event, LARGE_IMAGE);
			largeUploadedFilePath = LARGE_IMAGE;
			log.info(" LARGE_IMAGE Image Path ==== >> :" + LARGE_IMAGE);

			largeUploadedFile = event.getFile();
			largeImageSize = largeUploadedFile.getSize();
			largeInputStream = largeUploadedFile.getInputstream();
			log.info(" LARGE_IMAGE Image Size ==== >> :" + largeImageSize);

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			largeTimeStamp = Long.toString(timestamp.getTime());

		} catch (UIException e) {
			log.error("large image uploaded UIException.......");
		} catch (IOException e) {
			log.error("large image uploaded IOException.......");
		}
		log.info("image set from large image path is:" + filePath);
		ecommProductConfigDto.setLargImagePath(filePath);
	}

	public void smallImageUpload(FileUploadEvent event) {
		String filePath = "";
		try {
			filePath = fileUpload(event, SMALL_IMAGE);
			smallUploadedFilePath = SMALL_IMAGE;
			log.info(" SMALL_IMAGE Image Path ==== >> :" + SMALL_IMAGE);
			smallUploadedFile = event.getFile();
			smallImageSize = smallUploadedFile.getSize();
			smallInputStream = smallUploadedFile.getInputstream();
			log.info(" SMALL_IMAGE Image Size ==== >> :" + smallImageSize);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			smallTimeStamp = Long.toString(timestamp.getTime());
		} catch (UIException e) {
			log.error("small image uploaded exception.......");
		} catch (IOException e) {
			log.error("large image uploaded IOException.......");
		}

		log.info("image set from small image path is:" + filePath);
		ecommProductConfigDto.setSmallImagePath(filePath);
	}

	public void mediumImageUpload(FileUploadEvent event) {
		String filePath = "";
		try {
			filePath = fileUpload(event, MEDIUM_IMAGE);
			mediumUploadedFilePath = MEDIUM_IMAGE;
			log.info(" MEDIUM_IMAGE Image Path ==== >> :" + MEDIUM_IMAGE);
			mediumUploadedFile = event.getFile();
			mediumImageSize = mediumUploadedFile.getSize();
			mediumInputStream = mediumUploadedFile.getInputstream();
			log.info(" MEDIUM_IMAGE Image Size ==== >> :" + mediumImageSize);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			mediumTimeStamp = Long.toString(timestamp.getTime());
		} catch (UIException e) {
			log.error("small image uploaded exception.......");
		} catch (IOException e) {
			log.error("large image uploaded IOException.......");
		}
		log.info("image set from medium image path is:" + filePath);
		ecommProductConfigDto.setMediumImagePath(filePath);
	}

	public void thumpImageUpload(FileUploadEvent event) {
		String filePath = "";
		try {
			filePath = fileUpload(event, THUMP_IMAGE);
			thumUploadedFilePath = THUMP_IMAGE;
			log.info(" THUMP_IMAGE Image Path ==== >> :" + THUMP_IMAGE);
			thumUploadedFile = event.getFile();
			thumpImageSize = thumUploadedFile.getSize();
			thumpInputStream = thumUploadedFile.getInputstream();
			log.info(" THUMP Image Size ==== >> :" + thumpImageSize);
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			thumTimeStamp = Long.toString(timestamp.getTime());
		} catch (UIException e) {
			log.error("thump image uploaded exception.......");
		} catch (IOException e) {
			log.error("large image uploaded IOException.......");
		}
		log.info("image set from thump image path is:" + filePath);
		ecommProductConfigDto.setThumbnailImagePath(filePath);
	}

	public String fileUpload(FileUploadEvent event, String folder) throws UIException {
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return null;
		}

		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();

		log.info("uploadSignature size==>" + size);
		size = size / 1000;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("ECOMM_PRODUCT_IMAGES_FILE_SIZE"));
		if (size >= letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return null;
		}

		log.info("fileUpload selected file type:" + folder);
		String docPathName = commonDataService.getAppKeyValue("ECOMM_IMAGE_PARENT_DIR") + folder;
		File dir = new File(docPathName);

		if (!dir.exists()) {
			// dir.mkdirs();
			String errorMsg = errorMap
					.msg(ErrorDescription.getError(EcommerceErrorCode.FILE_DIRECTORY_NOT_FOUND).getErrorCode());
			AppUtil.addError(docPathName + ". " + errorMsg);
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}

//		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//		String timeStamp=Long.toString(timestamp.getTime());

		// String filePath =
		// Util.uploadDocument(parentPath,subPath,uploadedFile,timeStamp);
		setFileName(uploadedFile.getFileName());
		errorMap.notify(ErrorDescription.ECOMM_PRODUCT_CONFIG_FILEUPLOAD_SUCCESS.getErrorCode());
		log.info(" tapalFileUpload  is uploaded successfully");

		return uploadedFile.getFileName();
	}

	public void downloadfile(String path) {
		InputStream input = null;
		try {
			log.info("given Path:" + path);
			File file = new File(path);
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public String getFileSize(String selectedFileType) {
		int reqHeight = 0;
		int reqWidth = 0;
		String size = "";
		if (selectedFileType != null) {
			log.info("Selected image type is:" + selectedFileType);
			switch (selectedFileType) {
			case "LARGE_IMAGE":
				log.info("image set from large  :");
				reqHeight = LARGE_IMAGE_HEIGHT;
				reqWidth = LARGE_IMAGE_WIDTH;
				break;

			case "MEDIUM_IMAGE":
				log.info("image set from medium  :");
				reqHeight = LARGE_IMAGE_HEIGHT;
				reqWidth = LARGE_IMAGE_WIDTH;
				break;

			case "SMALL_IMAGE":
				log.info("image set from small :");
				reqHeight = LARGE_IMAGE_HEIGHT;
				reqWidth = LARGE_IMAGE_WIDTH;
				break;

			case "THUMP_IMAGE":
				log.info("image set from thump :");
				reqHeight = LARGE_IMAGE_HEIGHT;
				reqWidth = LARGE_IMAGE_WIDTH;
				break;
			}
		}
		size = reqHeight + "-" + reqWidth;
		return size;
	}

	public String deleteConfirm() {
		log.info("<===== Starts EcommProductConfigBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(ECOMM_PRODUCT_CONFIG_URL + "/deletebyid/" + selectedEcommProductConfigList.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.ECOMM_FILTER_CATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmEcommFilterCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete ecommFilterCategory ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends EcommFilterCategoryBean.deleteConfirm ===========>");
		return showEcommProductConfigListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EcommProductConfigBean.onRowSelect ========>" + event);
		selectedEcommProductConfigList = ((EcommProductConfigListDTO) event.getObject());
		addButtonFlag = true;
		editButtonFlag = false;
		deleteButtonFlag = false;
		viewButtonFlag = false;
		disableQRCodeButton = true;
		log.info("<===Ends EcommProductConfigBean.onRowSelect ========>");
	}

	public void searchQrCode() {
		log.info("<===== Starts EcommProductConfigBean.searchQrCode ===========>");
		try {

			if (ecommProductConfigDto.getqRCode() != null) {
				if (AppUtil.isJSONValid(ecommProductConfigDto.getqRCode())) {
					ItemQRCodeDetailsDTO itemQRCodeDetailsDTO1 = new ItemQRCodeDetailsDTO();
					itemQRCodeDetailsDTO1 = new Gson().fromJson(ecommProductConfigDto.getqRCode(),
							ItemQRCodeDetailsDTO.class);
					if (itemQRCodeDetailsDTO1 != null) {
						String qrCode = itemQRCodeDetailsDTO1.getQRCode();
						ecommProductConfig1 = getItemQrcodeDetails(qrCode);
					} else {
						log.info("itemQRCodeDetailsDTO is null");
					}
				} else {
					ecommProductConfig1 = getItemQrcodeDetails(ecommProductConfigDto.getqRCode());
				}
			}
			if (ecommProductConfig1 == null) {
//				AppUtil.addWarn(" QR Code Not Available");
				return;
			}
			if (ecommProductConfig1.getProductVarityMaster() == null) {
//				AppUtil.addWarn("Ecomm Rate Config Not Found"); 
				AppUtil.addWarn("Product not found");
				ecommProductConfigDto.setqRCode(null);
				ecommProductConfigDto.setQnty(null);
				ecommProductConfigDto.setAvailableQty(null);
				ecommProductConfigDto.setActualPrice(null);
				ecommProductConfigDto.setEcommPrice(null);
				disablethFild = false;
				return;
			}
			ecommProductConfigDto.setActualPrice(ecommProductConfig1.getActualPrice());
			if (ecommProductConfig1.getEcommPrice() == null) {
				AppUtil.addWarn("Ecom Price Not available for the QR Code");
				return;
			}
			ecommProductConfigDto.setInventoryId(ecommProductConfig1.getInventoryId());
//			ecommProductConfigDto.setEcommPrice(ecommProductConfig1.getEcommPrice());
			ecommProductConfigDto.setAvailableQty(ecommProductConfig1.getAvailableQty());
			if (ecommProductConfig1.getAvailableQty() > 0) {
				ecommProductConfigDto.setQnty(1D);
			}

			ecommProductConfigDto.setAtNumber(ecommProductConfig1.getAtNumber());
			ecommProductConfigDto.setqRCode(ecommProductConfig1.getqRCode());
			ecommProductConfigDto.setProductVarityMaster(ecommProductConfig1.getProductVarityMaster());
			ecommProductConfigDto.setProductGroupMaster(ecommProductConfig1.getProductGroupMaster());
				ecommProductConfigDto.setProductCategory(ecommProductConfig1.getProductCategory());
				System.out.println(ecommProductConfig1.getProductCategory().getProductCatCode());
				System.out.println(ecommProductConfig1.getProductCategory().getProductCatName());

			disablethFild = true;

			log.info("<===== Ends EcommProductConfigBean.searchQrCode ===========>");
		} catch (Exception e) {
			log.error(" <<== Exception Occured on searchQrCode ==>>", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}

	}

	public String clearData() {
		ecommProductConfigDto.setqRCode(null);
		ecommProductConfigDto.setQnty(null);
		ecommProductConfigDto.setAvailableQty(null);
		ecommProductConfigDto.setActualPrice(null);
		ecommProductConfigDto.setEcommPrice(null);
		ecommProductConfigDto.setProductCategory(null);
		ecommProductConfigDto.setProductGroupMaster(null);
		ecommProductConfigDto.setProductVarityMaster(null);
		disablethFild = false;
		return ADD_PAGE;
	}

	public void calculateEcommPrice() {
		try {
			ecommRateConfigDTO = new EcommRateConfigDTO();
			boolean valid = true;
			String qrCode = "";
			if (ecommProductConfigDto.getqRCode() == null || ecommProductConfigDto.getqRCode().isEmpty()) {
				AppUtil.addWarn("Please enter QR Code");
				valid = false;
			} else {
				qrCode = ecommProductConfigDto.getqRCode();
				if (ecommProductConfigDto.getActualPrice() == null) {
					AppUtil.addWarn("Quantity and Actual Price is Required");
					valid = false;
				}
			}

			if (valid == true) {
				log.info("Start getItemQrcodeDetails Method======>" + qrCode);
				EcommProductConfigDTO ecommProductConfigDTO = null;

				BaseDTO baseDTO = new BaseDTO();
				String URL = SERVER_URL + "/ecommProductConfig/calculateEcommPrice/" + qrCode;
				baseDTO = httpService.get(URL);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("Item Qr code details get successfully");
					ObjectMapper mapper = new ObjectMapper();
					String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					EcommRateConfigDTO data = mapper.readValue(jsonResponse, EcommRateConfigDTO.class);
					if (data != null) {
						ecommRateConfigDTO = data;
						ecommProductConfigDto.setEcommPrice(data.getGross_value());
					}

				}
			}

		} catch (Exception ex) {
			log.error("Error in getItemQrcodeDetails Method", ex);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("Start getItemQrcodeDetails Method======>");

	}

	/**
	 * @param qrCode
	 * @return
	 */
	private EcommProductConfigDTO getItemQrcodeDetails(String qrCode) {
		log.info("Start getItemQrcodeDetails Method======>" + qrCode);
		EcommProductConfigDTO ecommProductConfigDTO = null;
		BaseDTO baseDTO = new BaseDTO();
		try {

			/**
			 * COOPTEX-OPERATION-RESTAPI/src/main/java/in/gov/cooptex/operation/production/controller
			 * 
			 * StockTransferPWHController.java
			 */
			String URL = SERVER_URL + "/ecommProductConfig/getqrcodedetails/" + qrCode;
			baseDTO = httpService.get(URL);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				log.info("Item Qr code details get successfully");
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ecommProductConfigDTO = mapper.readValue(jsonResponse, EcommProductConfigDTO.class);

			}

		} catch (Exception ex) {
			log.error("Error in getItemQrcodeDetails Method", ex);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("Start getItemQrcodeDetails Method======>");

		if (ecommProductConfigDTO == null) {
			AppUtil.addWarn(baseDTO.getErrorDescription());
		}
		return ecommProductConfigDTO;
	}

	public void calculate() {
		try {
			if (ecommProductConfigDto.getAvailableQty() != null && ecommProductConfigDto.getQnty() != null) {

				if (ecommProductConfigDto.getAvailableQty() < ecommProductConfigDto.getQnty()) {
					AppUtil.addWarn(" Qnty Should Be lesser than Avail Qnty ");
					ecommProductConfigDto.setQnty(0D);
				}

			}

		} catch (Exception e) {
			log.error(" <== Exception Occured on calculate == > ", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
	}

	/**
	 * 
	 * @param parentpath,subPath,uploadFile,fileName
	 * @param uploadedFile
	 * @return
	 */

	public static String uploadDocument(String parentPath, String subPath, UploadedFile uploadFile, String fileName,
			long fileSize, InputStream inputStream) {
		String photoPath = null;
		String fileStatus = new String();
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStamp = Long.toString(timestamp.getTime());
		String supFolderFile = new String();
		try {
			OutputStream outputStream = null;

			String contentType = uploadFile.getContentType();
			log.info("Content Tyupe==>" + contentType);
			String fileName1 = uploadFile.getFileName();
			log.info("File Name==>" + fileName1);

			log.info("size==>" + fileSize);
			log.info(" ====>   inputStream ==>" + inputStream);

			if (fileSize > 0) {
				log.info("patent path==>" + parentPath);
				log.info("subfolder path==>" + subPath);
				log.info("fileName==>" + fileName);
				if (fileName == null || fileName.equals(""))
					fileName = timeStamp;

				photoPath = parentPath + subPath + '/' + fileName + "_" + uploadFile.getFileName();
				supFolderFile = subPath + '/' + fileName + "_" + uploadFile.getFileName();// Get subFolder with file
																							// name
				File checkFile = new File(photoPath);
				if (checkFile.exists()) {
					log.error("The same file name already exist in this directory");
					fileStatus = "Error";
					return fileStatus;
				}
				log.info("photoPath==>" + photoPath);

				Path pathName = Paths.get(parentPath);
				File outputFile = new File(photoPath);
				if (Files.notExists(pathName)) {
					Files.createDirectories(pathName);
				}
				outputStream = new FileOutputStream(outputFile);
				byte[] buffer = new byte[(int) fileSize];
				int bytesRead = 0;
				while ((bytesRead = inputStream.read(buffer)) != -1) {
					outputStream.write(buffer, 0, bytesRead);
				}
				if (outputStream != null) {
					outputStream.close();
				}
				if (inputStream != null) {
					inputStream.close();
				}
				log.info("photoPath full Path==>" + photoPath);
				log.info("photoPath sub folder path==>" + supFolderFile);
				fileStatus = photoPath;
			}
		} catch (Exception e) {
			log.error("fileUpload error==>" + e);
		}
		return supFolderFile;
	}

	// to download all same product qrcodes into a single pdf file which is already
	// generated while generate qrcode..
	public void downloadQrCodePdf() {

		log.info("downloadQrCodePdf started....");

		EmployeeMaster employeeMaster = loginBean.getEmployee();
		if (employeeMaster == null) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.USER_NOT_LINKED_EMPLOYEE).getErrorCode());
			return;
		}
		EmployeePersonalInfoEmployment personalInfoEmployment = employeeMaster.getPersonalInfoEmployment();
		if (personalInfoEmployment == null) {
			errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_PERSONAL_INFO_EMPLOYMENT_EMPTY)
					.getErrorCode());
			return;
		}
		loginEntityMaster = personalInfoEmployment.getWorkLocation();
		try {

			if (selectedEcommProductConfigList.getqRCode() == null) {
				AppUtil.addWarn(" QR Code Not Generated ");
				log.error(" QR Code Not Generated ");
				throw new RestException(" QR Code Not Generated ");
			}

			String URL = SERVER_URL + "/ecommProductConfig/downloadqrcode";
			log.info("downloadQRCodeZip [" + URL + "]");

			InputStream inputStream = httpService.generatePDF(URL, selectedEcommProductConfigList);
			if (inputStream != null) {
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				largeTimeStamp = Long.toString(timestamp.getTime());

				downloadFile = new DefaultStreamedContent(inputStream, "application/pdf",
						loginEntityMaster.getCode() + "_" + selectedEcommProductConfigList.getAtNumber() + "_"
								+ selectedEcommProductConfigList.getProductVarietyCode() + "_"
								+ selectedEcommProductConfigList.getProductVarietyName() + "_"
								+ largeTimeStamp.replaceAll(" ", "_") + ".pdf");

				log.info("downloadQrCodePdf finished successfully....");

			}

		} catch (Exception e) {
			log.error("Exception ", e);
		}

	}
}
