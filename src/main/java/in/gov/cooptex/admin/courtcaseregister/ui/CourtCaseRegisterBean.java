package in.gov.cooptex.admin.courtcaseregister.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.joda.time.DateTime;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultUploadedFile;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.courtcase.dto.CourtCaseDTO;
import in.gov.cooptex.admin.courtcase.dto.CourtCaseRegisterDTO;
import in.gov.cooptex.admin.disciplinary.model.EmpComplaintRegister;
import in.gov.cooptex.admin.model.FileName;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CourtCaseDetails;
import in.gov.cooptex.core.model.CourtCaseFiles;
import in.gov.cooptex.core.model.CourtCaseHearings;
import in.gov.cooptex.core.model.CourtCaseRegister;
import in.gov.cooptex.core.model.CourtCaseType;
import in.gov.cooptex.core.model.CourtMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.enums.CourtCaseStatus;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("courtCaseRegisterBean")
@Scope("session")
public class CourtCaseRegisterBean {

    final String LIST_PAGE = "/pages/admin/raisecomplaint/listCourtCase.xhtml?faces-redirect=true";
    final String ADD_PAGE = "/pages/admin/raisecomplaint/createCourtCase.xhtml?faces-redirect=true";
    final String VIEW_PAGE = "/pages/admin/raisecomplaint/viewCourtCase.xhtml?faces-redirect=true";
    final String UPDATE_HEARING_PAGE = "/pages/admin/raisecomplaint/updateHearingCourtCase.xhtml?faces-redirect=true";

    public static final String COURT_TYPE_URL = AppUtil.getPortalServerURL() + "/courtmaster";
    final String SERVER_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/courtcaseregister";

    String jsonResponse;

    @Autowired
    HttpService httpService;

    @Getter
    @Setter
    LazyDataModel <CourtCaseDTO> lazyCourtCaseDTOList;

    ObjectMapper mapper = new ObjectMapper();

    @Getter
    @Setter
    List <CourtCaseDTO> courtCaseDTOListPage = new ArrayList <>();

    @Getter
    @Setter
    int totalRecords = 0;

    @Autowired
    ErrorMap errorMap;

    @Getter
    @Setter
    CourtCaseDTO selectedCourtCaseDTOPage;

    @Setter
    @Getter
    List <CourtCaseType> courtCaseTypeValuesList = new ArrayList <>();

    @Setter
    @Getter
    List <CourtMaster> typesOfCourtValues = new ArrayList <>();

    @Setter
    @Getter
    List <EntityMaster> headOrRegionalOfficeList;

    @Setter
    @Getter
    List <EntityMaster> headOrRegionalList = new ArrayList <>();

    @Autowired
    CommonDataService commonDataService;

    @Setter
    @Getter
    private StreamedContent file;

    @Setter
    @Getter
    List <EntityMaster> headandregionalofficelist;

    @Getter
    @Setter
    List statusValues;

    @Getter
    @Setter
    Boolean addButtonFlag = false;

    @Getter
    @Setter
    Boolean editButtonFlag = false;

    @Getter
    @Setter
    Boolean deleteButtonFlag = false;

    @Getter
    @Setter
    Boolean viewButtonFlag = false;

    @Getter
    @Setter
    Boolean hearingButtonFlag = false;

    @Getter
    @Setter
    Boolean createCaseRefPanelFlag = false;

    @Getter
    @Setter
    String action;

    @Getter
    @Setter
    List <String> petitionerNameList = new ArrayList <>();

    @Getter
    @Setter
    List <String> respondentNameList = new ArrayList <>();

    @Getter
    @Setter
    String petitionerName;

    @Getter
    @Setter
    String respondentName;

    @Getter
    @Setter
    CourtCaseRegisterDTO courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();

    @Getter
    @Setter
    List<CourtCaseDetails> courtCaseDetailsListEditPage;


    @Getter
    @Setter
    EmpComplaintRegister empComplaintRegister = new EmpComplaintRegister();

    @Setter
    @Getter
    List <EmpComplaintRegister> empComplaintRegisterList = new ArrayList <>();

    @Setter
    @Getter
    String fileType;

    @Setter
    @Getter
    List <String> courtCaseDocumentFilePath = new ArrayList <>();

    @Setter
    @Getter
    List <String> courtCaseDocumentFileName = new ArrayList <>();

    @Getter
    @Setter
    List <UploadedFile> uploadedFilesList = new ArrayList <>();

    @Getter
    @Setter
    List <FileList> uploadedFilesListViewPage;

    @Getter
    @Setter
    List <CourtCaseFiles> courtCaseFilesListEditPage = new ArrayList <>();


    @Getter
    @Setter
    UploadedFile uploadedFile;

    @Getter
    @Setter
    List <CourtCaseHearings> updateHearingsListViewPage;
    
    @Getter
    @Setter
    Date lastHearingDateVal;
    
    @Getter
    @Setter
    Date nextHearingDateVal;

    @Getter
    @Setter
    CourtCaseHearings courtCaseHearings = new CourtCaseHearings();

    @Getter
    @Setter
    String complaintRegistered = "";

    @Getter
    @Setter
    String counterAffidavit="";

    @Getter
    @Setter
    String vakalathFiled="";

    @Getter
    @Setter
    Boolean display=false;
    
    @Getter
    @Setter
    Date newHearingDateVal;
    
    @Getter
    @Setter
    Boolean caseNoFlag=false;


    public String showListCourtCaseRegisterPage() {
        log.info( "<==========START CourtCaseRegisterBean.showListCourtCaseRegisterPage ==========>" );
        mapper = new ObjectMapper();
        clearCourtCaseRegisterSelections();
        selectedCourtCaseDTOPage = new CourtCaseDTO();
        courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
        respondentNameList = new ArrayList <>();
        petitionerNameList = new ArrayList <>();
        uploadedFilesList = new ArrayList <>();
        courtCaseDocumentFilePath = new ArrayList <>();
        empComplaintRegister = new EmpComplaintRegister();
        loadStatus();
        loadheadandregionaloffice();
        loadCourtType();
        loadCourtCaseType();
        lazyCourtCaseRegisterResponse();
        log.info( "<========== END CourtCaseRegisterBean.showListCourtCaseRegisterPage==========>" );
        return LIST_PAGE;
    }

    public void selectComplaintRegisteredCreatePage() {
        log.info( "<==========START CourtCaseRegisterBean.selectComplaintRegisteredCreatePage ==========>" );
        if (courtCaseRegisterRequestDTO.getComplaintRegistered()) {
            createCaseRefPanelFlag = true;
        } else {
            createCaseRefPanelFlag = false;
        }
        log.info( "<==========END CourtCaseRegisterBean.selectComplaintRegisteredCreatePage ==========>" );
    }

    public String clearCourtCaseRegisterSelections(){
        log.info( "<==========START CourtCaseRegisterBean.clearCourtCaseRegisterSelections ==========>" );
        courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
        respondentNameList = new ArrayList <>();
        petitionerNameList = new ArrayList <>();
        uploadedFilesList = new ArrayList <>();
        courtCaseDocumentFilePath = new ArrayList <>();
        empComplaintRegister = new EmpComplaintRegister();
        selectedCourtCaseDTOPage = new CourtCaseDTO(); 
        fileType = null;
        addButtonFlag=true;
        viewButtonFlag = false;
        editButtonFlag = false;
        deleteButtonFlag = false;
        hearingButtonFlag = false;
        loadStatus();
        loadheadandregionaloffice();
        loadCourtType();
        loadCourtCaseType();
        lazyCourtCaseRegisterResponse();
        log.info( "<==========END CourtCaseRegisterBean.clearCourtCaseRegisterSelections ==========>" );
        return LIST_PAGE;
    }
    private Date todayDate = new Date();

    public Date getTodayDate() {
        return todayDate;
    }
    /**
     * Code for Row Select
     */
    public void onRowSelect(SelectEvent event) {
        log.info("<===START CourtCaseRegisterBean.onRowSelect ========>" + event);
        selectedCourtCaseDTOPage = ((CourtCaseDTO) event.getObject());
        log.info("<==== selectedCourtCaseDTOPage Object =====>", selectedCourtCaseDTOPage);
        addButtonFlag = false;
        viewButtonFlag = true;
        editButtonFlag = true;
        deleteButtonFlag = true;
        hearingButtonFlag = true;
        if (!selectedCourtCaseDTOPage.getStatus().equals("CLOSED")) {
            addButtonFlag = false;
            editButtonFlag = false;
            deleteButtonFlag = false;
            hearingButtonFlag = true;
        } else {
            addButtonFlag = false;
            editButtonFlag = false;
            deleteButtonFlag = false;
            hearingButtonFlag = false;

        }
        log.info( "<===END CourtCaseRegisterBean.onRowSelect ========>" );
    }

    public String courtCaseRegisterListAction() {
        log.info( "<====== CourtCaseRegisterBean.courtCaseRegisterListAction() ====> action   :::" + action );
        try {
            if (action.equalsIgnoreCase( "ADD" ) || action.equalsIgnoreCase( "EDIT" )) {
                log.info( "<====== court Case Register add  page called.......====>" );
                selectedCourtCaseDTOPage = new CourtCaseDTO();
                courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
                loadAllCreatePageDropDown();
                uploadedFilesList = new ArrayList <>();
                petitionerName = null;
                respondentName = null;
                log.info( "<====== court Case Register add page ended.......====>" );
                return ADD_PAGE;
            }
            if (selectedCourtCaseDTOPage == null) {
                errorMap.notify( ErrorDescription.COURT_CASE_REGISTER_EMPTY.getErrorCode() );
                return null;
            }
            if (action.equalsIgnoreCase( "DELETE" )) {
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute( "PF('confirmCourtCaseRegisterDelete').show();" );

            }
            if (action.equalsIgnoreCase( "VIEW" )) {
                courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
                return VIEW_PAGE;
            }
            if (action.equalsIgnoreCase( "HEARING" )) {
                courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
                return UPDATE_HEARING_PAGE;
            }

        } catch (Exception e) {
            log.error( "Exception Occured in CourtCaseRegisterBean.courtCaseRegisterListAction()::", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        log.info( "<====== END CourtCaseRegisterBean.courtCaseRegisterListAction() ====>" );
        return null;
    }

    public void loadAllCreatePageDropDown() {
        log.info( "<====== START CourtCaseRegisterBean.loadAllCreatePageDropDown() ====>");
        loadheadandregionaloffice();
        loadCourtType();
        loadCourtCaseType();
        loadEmpComplaintRegister();
        log.info( "<====== END CourtCaseRegisterBean.loadAllCreatePageDropDown() ====>");
    }

	public void lazyCourtCaseRegisterResponse() {
        log.info( "<===== START CourtCaseRegisterBean.lazyCourtCaseRegisterResponse ======>" );
        lazyCourtCaseDTOList = new LazyDataModel <CourtCaseDTO>() {
            private static final long serialVersionUID = 8422543223567350599L;

            @Override
            public List <CourtCaseDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
                                            Map <String, Object> filters) {
                try {
                    PaginationDTO paginationRequest = new PaginationDTO( first / pageSize, pageSize, sortField,
                            sortOrder.toString(), filters );
                    log.info( "Court Case Register request :::" + paginationRequest );
                    String url = SERVER_URL + "/getallcourtcaseregister";
                    BaseDTO response = httpService.post( url, paginationRequest );
                    if (response != null && response.getStatusCode() == 0) {
                        jsonResponse = mapper.writeValueAsString( response.getResponseContents() );
                        courtCaseDTOListPage = mapper.readValue( jsonResponse,
                                new TypeReference <List <CourtCaseDTO>>() {
                                } );
                        this.setRowCount( response.getTotalRecords() );
                        totalRecords = response.getTotalRecords();
                    } else {
                        errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                    }
                } catch (Exception e){
                    log.error( "Exception occured in lazyCourtCaseRegisterResponse ...", e );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                log.info( "Ends lazy load...." );
                return courtCaseDTOListPage;
            }

            @Override
            public Object getRowKey(CourtCaseDTO res) {
                return res != null ? res.getId() : null;
            }

            @Override
            public CourtCaseDTO getRowData(String rowKey) {
                try {
                    for (CourtCaseDTO courtCaseDTO : courtCaseDTOListPage) {
                        if (courtCaseDTO.getId().equals( Long.valueOf( rowKey ) )) {
                            selectedCourtCaseDTOPage = courtCaseDTO;
                            return courtCaseDTO;
                        }
                    }
                } catch (Exception e) {
                    log.error( "Exception occured in getRowData ...", e );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                return null;
            }

        };
        log.info( "<===== END CourtCaseRegisterBean.lazyCourtCaseRegisterResponse() ======>" );
    }

    public String submit() {
        List <CourtCaseDetails> courtCaseDetailsList = new ArrayList <>();
        List <CourtCaseFiles> courtCaseFilesList = new ArrayList <>();
        List <UploadedFile> saveUploadFilesList = new ArrayList <>();
        try {
        	if(caseNoFlag)
        	{
        		Util.addWarn("Court caseNo already Exist");
        		return null;
        	}
            log.info( "<=======START CourtCaseRegisterBean.submit ======>" + courtCaseRegisterRequestDTO );
            //delete existing files
            if (uploadedFilesList != null && uploadedFilesList.size()>0) {

                if (courtCaseFilesListEditPage != null && courtCaseFilesListEditPage.size() > 0){

                    for(CourtCaseFiles caseFiles : courtCaseFilesListEditPage){
                        String name = caseFiles.getDocumentName();
                        if(name != null){
                            boolean isFound = false;
                            for(UploadedFile uploaded : uploadedFilesList) {
                                if (name.equalsIgnoreCase( uploaded.getFileName() )) {
                                    isFound = true;
                                    break;
                                }
                            }
                            if(!isFound){
                                courtCaseFilesList.add( caseFiles );
                            }
                        }
                    }

                    //removing
                    Iterator<CourtCaseFiles> iterator = courtCaseFilesListEditPage.iterator();
                    while (iterator.hasNext()){
                        CourtCaseFiles courtCaseFiles = iterator.next();
                        if(courtCaseFiles.getDocumentName() == null){
                            continue;
                        }

                        Iterator<UploadedFile> uploadedFileIterator = uploadedFilesList.iterator();
                        while (uploadedFileIterator.hasNext()){
                            UploadedFile file = uploadedFileIterator.next();

                            if(courtCaseFiles.getDocumentName().equalsIgnoreCase(file.getFileName() )){
                                uploadedFileIterator.remove();
                            }
                        }
                    }

                }

                //Delete files.
                if(courtCaseFilesList.size()>0){
                    Iterator<CourtCaseFiles> courtCaseFilesIterator = courtCaseFilesList.iterator();
                    while (courtCaseFilesIterator.hasNext()){
                        CourtCaseFiles caseFiles = courtCaseFilesIterator.next();
                        File file = new File(caseFiles.getDocumentPath());
                        if(file.exists()){
                            file.delete();
                        }
                    }
                }

                for (UploadedFile upload : uploadedFilesList) {
                    String filePath = uploadDocument(upload);
                    if (filePath == null){
                        continue;
                    }
                    CourtCaseFiles courtCaseFiles = new CourtCaseFiles();
                    courtCaseFiles.setDocumentName( upload.getFileName() );
                    courtCaseFiles.setDocumentPath( filePath);
                    courtCaseFilesList.add( courtCaseFiles );
                }
                courtCaseRegisterRequestDTO.setCourtCaseFilesList( courtCaseFilesList );
            }

            if (respondentNameList!=null) {

                if (courtCaseDetailsListEditPage != null && courtCaseDetailsListEditPage.size() > 0){
                    for(CourtCaseDetails caseDetail : courtCaseDetailsListEditPage){
                        if(caseDetail.getPetitionerName()== null && caseDetail.getRespondentName()!=null){
                            if(!respondentNameList.contains( caseDetail.getRespondentName())){
                                courtCaseDetailsList.add( caseDetail );
                            }
                        }
                    }

                    courtCaseDetailsListEditPage.forEach( courtCaseDetails ->  {
                        respondentNameList.remove( courtCaseDetails.getRespondentName());
                    });

                }

                for (String str : respondentNameList) {
                    CourtCaseDetails courtCaseDetails = new CourtCaseDetails();
                    courtCaseDetails.setRespondentName( str );
                    courtCaseDetailsList.add( courtCaseDetails );
                }

            }
            if (petitionerNameList!=null) {

                if (courtCaseDetailsListEditPage != null && courtCaseDetailsListEditPage.size() > 0){
                    for(CourtCaseDetails caseDetail : courtCaseDetailsListEditPage){
                        if(caseDetail.getRespondentName()== null && caseDetail.getPetitionerName()!=null) {
                            if (!petitionerNameList.contains( caseDetail.getPetitionerName() )) {
                                courtCaseDetailsList.add( caseDetail );
                            }
                        }
                    }

                    courtCaseDetailsListEditPage.forEach( courtCaseDetails ->  {
                        petitionerNameList.remove( courtCaseDetails.getPetitionerName());
                    });

                }

                for (String str : petitionerNameList) {
                    CourtCaseDetails courtCaseDetails = new CourtCaseDetails();
                    courtCaseDetails.setPetitionerName( str );
                    courtCaseDetailsList.add( courtCaseDetails );
                }

            }

            courtCaseRegisterRequestDTO.setCourtCaseDetailsList( courtCaseDetailsList );
            courtCaseRegisterRequestDTO.setEmpComplaintRegisterId( empComplaintRegister.getId() );
            if(courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null) {
            	 log.info("==================Uploaded Document List================="+courtCaseRegisterRequestDTO.getCourtCaseFilesList().toString());
            }
           
            BaseDTO response = httpService.post( SERVER_URL + "/createorupdate", courtCaseRegisterRequestDTO );
            if (response.getStatusCode() == 0) {
                errorMap.notify( ErrorDescription.COURT_CASE_SAVE_SUCCESS.getErrorCode() );
                return showListCourtCaseRegisterPage();
            } else {
                errorMap.notify( response.getStatusCode() );
            }
            log.info( "<=======END CourtCaseRegisterBean.submit ======>" );
        } catch (Exception e) {
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
            log.info( "<=======found exception in file save Court case register : " + e.getMessage() );
            return null;
        }
        return null;
    }

    public String loadCourtRegisterById() { 
        try {
            log.info( "<===== START CourtCaseRegisterBean.loadCourtRegisterById() ======>" );
            petitionerNameList = new ArrayList <String>();
            respondentNameList = new ArrayList <String>();
            courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
            courtCaseDocumentFileName = new ArrayList <>();
            courtCaseHearings = new CourtCaseHearings();
            String url = SERVER_URL + "/get/" + selectedCourtCaseDTOPage.getId();
            BaseDTO baseDTO = httpService.get( url );

            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContent() );
                courtCaseRegisterRequestDTO = mapper.readValue( jsonResponse,
                        new TypeReference <CourtCaseRegisterDTO>() {
                        } );
                if (courtCaseRegisterRequestDTO == null) {
                    log.info( " court Case register  is empty" );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                if (courtCaseRegisterRequestDTO.getComplaintRegistered() != null){
                    if (courtCaseRegisterRequestDTO.getComplaintRegistered()){
                        complaintRegistered = "yes";

                    } else {
                        complaintRegistered = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getCounterAffidavit() != null){
                    if (courtCaseRegisterRequestDTO.getCounterAffidavit()){
                        counterAffidavit = "yes";

                    } else {
                        counterAffidavit = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getVakalathFiled() != null){
                    if (courtCaseRegisterRequestDTO.getVakalathFiled()){
                        vakalathFiled = "yes";

                    } else {
                        vakalathFiled = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseDetailsList() != null && courtCaseRegisterRequestDTO.getCourtCaseDetailsList().size() > 0){
                    for (CourtCaseDetails courtCaseDetails : courtCaseRegisterRequestDTO.getCourtCaseDetailsList()) {
                        if (courtCaseDetails.getPetitionerName() != null) {
                            petitionerNameList.add( courtCaseDetails.getPetitionerName() );
                        }
                        if (courtCaseDetails.getRespondentName() != null) {
                            respondentNameList.add( courtCaseDetails.getRespondentName() );
                        }

                    }
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null && courtCaseRegisterRequestDTO.getCourtCaseFilesList().size() > 0){
                    for (CourtCaseFiles courtCaseFiles : courtCaseRegisterRequestDTO.getCourtCaseFilesList()) {
                        if (courtCaseFiles.getDocumentName() != null) {
                            courtCaseDocumentFileName.add( courtCaseFiles.getDocumentName() );
                        }
                    }
                }

                uploadedFilesList = new ArrayList <>();
                uploadedFilesListViewPage = new ArrayList <>(  );
                if (courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null && courtCaseRegisterRequestDTO.getCourtCaseFilesList().size() > 0){
                    uploadedFilesListViewPage = new ArrayList <>(  );
                    for (CourtCaseFiles courtCaseFiles : courtCaseRegisterRequestDTO.getCourtCaseFilesList()) {
                        if (courtCaseFiles.getDocumentName() != null) {
                            FileList fileList = new FileList();
                            fileList.setFileName( courtCaseFiles.getDocumentName() );
                            fileList.setFilePath( courtCaseFiles.getDocumentPath() );
                            uploadedFilesListViewPage.add( fileList );
                        }
                    }
                }

                if (courtCaseRegisterRequestDTO.getCourtCaseHearingsList()!= null && courtCaseRegisterRequestDTO.getCourtCaseHearingsList().size() > 0){
                    List <CourtCaseHearings> orderWiseViewPage = new ArrayList <>(  );
                    CourtCaseHearings courtCaseHearings = new CourtCaseHearings();
                    courtCaseHearings.setCounterStage( courtCaseRegisterRequestDTO.getCounterStage() );
                    courtCaseHearings.setLastHearingDate( courtCaseRegisterRequestDTO.getLastHearingDate() );
                    courtCaseHearings.setNextHearingDate(courtCaseRegisterRequestDTO.getNextHearingDate()  );
                    orderWiseViewPage.add( courtCaseHearings );
                    for (CourtCaseHearings caseHearings : courtCaseRegisterRequestDTO.getCourtCaseHearingsList()){
                        orderWiseViewPage.add(caseHearings);
                    }
                    this.updateHearingsListViewPage = orderWiseViewPage;
                } else {
                    updateHearingsListViewPage = new ArrayList <>(  );
                    CourtCaseHearings courtCaseHearings = new CourtCaseHearings();
                    courtCaseHearings.setCounterStage( courtCaseRegisterRequestDTO.getCounterStage() );
                    courtCaseHearings.setLastHearingDate( courtCaseRegisterRequestDTO.getLastHearingDate() );
                    courtCaseHearings.setNextHearingDate(courtCaseRegisterRequestDTO.getNextHearingDate()  );
                    this.updateHearingsListViewPage.add( courtCaseHearings );

                }

                return VIEW_PAGE;
            } else {
                errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
            }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading court Case Registers :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        return null;
    }

    public String loadHearingCourtCaseById() {
        try {

            petitionerNameList = new ArrayList <String>();
            respondentNameList = new ArrayList <String>();
            courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
            courtCaseDocumentFileName = new ArrayList <>();
            updateHearingsListViewPage = new ArrayList <>(  );
            String url = SERVER_URL + "/get/" + selectedCourtCaseDTOPage.getId();
            BaseDTO baseDTO = httpService.get( url );

            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContent() );
                courtCaseRegisterRequestDTO = mapper.readValue( jsonResponse,
                        new TypeReference <CourtCaseRegisterDTO>() {
                        } );
                if (courtCaseRegisterRequestDTO == null) {
                    log.info( " court Case register  is empty" );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                if (courtCaseRegisterRequestDTO.getComplaintRegistered() != null){
                    if (courtCaseRegisterRequestDTO.getComplaintRegistered()){
                        complaintRegistered = "yes";

                    } else {
                        complaintRegistered = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getCounterAffidavit() != null){
                    if (courtCaseRegisterRequestDTO.getCounterAffidavit()){
                        counterAffidavit = "yes";

                    } else {
                        counterAffidavit = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getVakalathFiled() != null){
                    if (courtCaseRegisterRequestDTO.getVakalathFiled()){
                        vakalathFiled = "yes";

                    } else {
                        vakalathFiled = "no";
                    }
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseDetailsList() != null && courtCaseRegisterRequestDTO.getCourtCaseDetailsList().size() > 0){
                    for (CourtCaseDetails courtCaseDetails : courtCaseRegisterRequestDTO.getCourtCaseDetailsList()) {
                        if (courtCaseDetails.getPetitionerName() != null) {
                            petitionerNameList.add( courtCaseDetails.getPetitionerName() );
                        }
                        if (courtCaseDetails.getRespondentName() != null) {
                            respondentNameList.add( courtCaseDetails.getRespondentName() );
                        }

                    }
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null && courtCaseRegisterRequestDTO.getCourtCaseFilesList().size() > 0){
                    for (CourtCaseFiles courtCaseFiles : courtCaseRegisterRequestDTO.getCourtCaseFilesList()) {
                        if (courtCaseFiles.getDocumentName() != null) {
                            courtCaseDocumentFileName.add( courtCaseFiles.getDocumentName() );
                        }
                    }
                }

                uploadedFilesList = new ArrayList <>();
                uploadedFilesListViewPage = new ArrayList <>(  );
                if (courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null && courtCaseRegisterRequestDTO.getCourtCaseFilesList().size() > 0){
                    uploadedFilesListViewPage = new ArrayList <>(  );
                    for (CourtCaseFiles courtCaseFiles : courtCaseRegisterRequestDTO.getCourtCaseFilesList()) {
                        if (courtCaseFiles.getDocumentName() != null) {
                            FileList fileList = new FileList();
                            fileList.setFileName( courtCaseFiles.getDocumentName() );
                            fileList.setFilePath( courtCaseFiles.getDocumentPath() );
                            uploadedFilesListViewPage.add( fileList );
                        }
                    }
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseHearingsList()!= null && courtCaseRegisterRequestDTO.getCourtCaseHearingsList().size() > 0){
                    List <CourtCaseHearings> orderWiseViewPage = new ArrayList <>(  );
                    CourtCaseHearings courtCaseHearings = new CourtCaseHearings();
                    courtCaseHearings.setCounterStage( courtCaseRegisterRequestDTO.getCounterStage() );
                    courtCaseHearings.setLastHearingDate( courtCaseRegisterRequestDTO.getLastHearingDate() );
                    courtCaseHearings.setNextHearingDate(courtCaseRegisterRequestDTO.getNextHearingDate()  );
                    orderWiseViewPage.add( courtCaseHearings );
                    for (CourtCaseHearings caseHearings : courtCaseRegisterRequestDTO.getCourtCaseHearingsList()){
                        orderWiseViewPage.add(caseHearings);
                    }
                    this.updateHearingsListViewPage = orderWiseViewPage;
                    lastHearingDateVal =  new DateTime(orderWiseViewPage.get(orderWiseViewPage.size()-1).getLastHearingDate()).minusDays(1).toDate();
                    nextHearingDateVal =  new DateTime(orderWiseViewPage.get(orderWiseViewPage.size()-1).getNextHearingDate()).minusDays(1).toDate();
                  
                } else {
                    updateHearingsListViewPage = new ArrayList <>(  );
                    CourtCaseHearings courtCaseHearings = new CourtCaseHearings();
                    courtCaseHearings.setCounterStage( courtCaseRegisterRequestDTO.getCounterStage() );
                    courtCaseHearings.setLastHearingDate( courtCaseRegisterRequestDTO.getLastHearingDate() );
                    courtCaseHearings.setNextHearingDate(courtCaseRegisterRequestDTO.getNextHearingDate()  );
                    this.updateHearingsListViewPage.add( courtCaseHearings );
                    //lastHearingDateVal = new DateTime(courtCaseHearings.getLastHearingDate()).minusDays(1).toDate();
                    lastHearingDateVal = new DateTime(courtCaseHearings.getLastHearingDate()).minusDays(1).toDate();
                    nextHearingDateVal = new DateTime(courtCaseHearings.getNextHearingDate()).minusDays(1).toDate();
                  

                }
                log.info( "<---END CourtCaseRegisterBean.loadHearingCourtCaseById ---> ");
                if(!updateHearingsListViewPage.isEmpty())
                {
                	//courtCaseHearings.setLastHearingDate(updateHearingsListViewPage.get(updateHearingsListViewPage.size()-1).getLastHearingDate());
                	courtCaseHearings.setLastHearingDate(updateHearingsListViewPage.get(updateHearingsListViewPage.size()-1).getNextHearingDate());
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(courtCaseHearings.getLastHearingDate());
                calendar.add(Calendar.DAY_OF_YEAR, 1);
                newHearingDateVal=calendar.getTime();
                
                return UPDATE_HEARING_PAGE;
            } else {
                errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
            }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading court Case Registers :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        log.info( "<---END CourtCaseRegisterBean.loadHearingCourtCaseById ---> ");
        return null;
    }

    public String submitHearing() {
        try {
            log.info( "<=======START CourtCaseRegisterBean.submitHearing ======>" + courtCaseHearings );
            for(CourtCaseHearings courtCase : updateHearingsListViewPage) {
            	log.info("first lasthearing==> "+courtCase.getLastHearingDate());
            	log.info("next hearing==> "+courtCaseHearings.getNextHearingDate());
            	if(courtCase.getLastHearingDate().after(courtCaseHearings.getNextHearingDate()) || courtCase.getLastHearingDate().equals(courtCaseHearings.getNextHearingDate())) {
            		errorMap.notify(ErrorDescription.getError(MastersErrorCode.NEXT_HEARING_DATE_ALREDY_INSERTED).getCode());
            		return null;
            	}
            }
            CourtCaseRegister courtCaseRegister = new CourtCaseRegister();
            courtCaseRegister.setId(courtCaseRegisterRequestDTO.getId() );
            courtCaseHearings.setCourtCaseRegister(courtCaseRegister  );
            BaseDTO response = httpService.post( SERVER_URL + "/updatehearing", courtCaseHearings );
            if (response != null && response.getStatusCode() == 0) {
                errorMap.notify( ErrorDescription.COURT_CASE_HEARING_SAVE_SUCCESS.getErrorCode() );
                return showListCourtCaseRegisterPage();
            } else {
                errorMap.notify( response.getStatusCode() );
            }
            log.info( "<=======END CourtCaseRegisterBean.submitHearing ======>" );
        } catch (Exception e) {
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
            log.info( "<=======found exception in  submitHearing method: " + e.getMessage() );
            return null;
        }
        return null;
    }

    public String editCourtRegisterById() {
        try {
            log.info( "<=======START CourtCaseRegisterBean.editCourtRegisterById ======>" );
            petitionerNameList = new ArrayList <String>();
            respondentNameList = new ArrayList <String>();
            courtCaseRegisterRequestDTO = new CourtCaseRegisterDTO();
            courtCaseDocumentFileName = new ArrayList <>();
            String url = SERVER_URL + "/get/" + selectedCourtCaseDTOPage.getId();
            BaseDTO baseDTO = httpService.get( url );
            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
            	
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContent() );
                courtCaseRegisterRequestDTO = mapper.readValue( jsonResponse,
                        new TypeReference <CourtCaseRegisterDTO>() { } );
                if (courtCaseRegisterRequestDTO == null) {
                    log.info( " court Case register  is empty" );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                if (courtCaseRegisterRequestDTO.getCourtCaseDetailsList() != null && courtCaseRegisterRequestDTO.getCourtCaseDetailsList().size() >0 ){
                    courtCaseDetailsListEditPage =courtCaseRegisterRequestDTO.getCourtCaseDetailsList();
                    for (CourtCaseDetails courtCaseDetails : courtCaseRegisterRequestDTO.getCourtCaseDetailsList()) {
                        if (courtCaseDetails.getPetitionerName() != null) {
                            petitionerNameList.add( courtCaseDetails.getPetitionerName() );
                        }
                        if (courtCaseDetails.getRespondentName() != null) {
                            respondentNameList.add( courtCaseDetails.getRespondentName() );
                        }

                    }
                }
                if(courtCaseRegisterRequestDTO != null && courtCaseRegisterRequestDTO.getComplaintReferenceNumber()!= null && courtCaseRegisterRequestDTO.getId() != null) {
                	log.error("Complaint register id==> "+courtCaseRegisterRequestDTO.getEmpComplaintRegisterId());
                	log.error("Complaint reference number==> "+courtCaseRegisterRequestDTO.getComplaintReferenceNumber());
                	empComplaintRegister.setId(courtCaseRegisterRequestDTO.getEmpComplaintRegisterId());
                	empComplaintRegister.setReferenceNumber(courtCaseRegisterRequestDTO.getComplaintReferenceNumber()); 
                	loadEmpComplaintRegisterById(courtCaseRegisterRequestDTO.getEmpComplaintRegisterId());
                }else {
                	log.error("Complaint reference number not found");
                }
                if(courtCaseRegisterRequestDTO != null && courtCaseRegisterRequestDTO.getComplaintRaisedDate()!= null) {
                	empComplaintRegister.setComplaintRaisedDate(courtCaseRegisterRequestDTO.getComplaintRaisedDate());
                }else {
                	log.error("Complaint raised date not found");
                }
                this.courtCaseFilesListEditPage = courtCaseRegisterRequestDTO.getCourtCaseFilesList();
                uploadedFilesList = new ArrayList <>();

                if (courtCaseRegisterRequestDTO.getCourtCaseFilesList() != null && courtCaseRegisterRequestDTO.getCourtCaseFilesList().size() > 0){
                    for (CourtCaseFiles courtCaseFiles : courtCaseRegisterRequestDTO.getCourtCaseFilesList()) {
                        if (courtCaseFiles.getDocumentName() != null) {
                            String filePath = courtCaseFiles.getDocumentPath();
                            File file = new File( filePath );
                            FileItem fileItem = new DiskFileItem( null, null, true, courtCaseFiles.getDocumentName(), 0, file );
                            UploadedFile uploadedFile = new DefaultUploadedFile( fileItem );
                            uploadedFilesList.add( uploadedFile );

                        }
                    }
                }
                if (courtCaseRegisterRequestDTO.getComplaintRegistered()){
                    createCaseRefPanelFlag = true;
                } else {
                    createCaseRefPanelFlag = false;
                }
                return ADD_PAGE;
            } else {
                errorMap.notify( baseDTO.getStatusCode() );
            }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading court Case Registers :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        log.info( "<======= END CourtCaseRegisterBean.editCourtRegisterById  =====> " );
        return null;
    }

    private void loadStatus() {
        log.info( "<======= START CourtCaseRegisterBean.loadStatus  =====> " );
        statusValues = new ArrayList <>();
        Object[] statusArray = CourtCaseStatus.class.getEnumConstants();
        for (Object status : statusArray) {
            statusValues.add( status );
        }
        log.info( "<======= END CourtCaseRegisterBean.loadStatus  =====> " );
    }

    private void loadCourtCaseType() {
        try {
            log.info( "<--- START of CourtCaseRegisterBean.courtCaseTypeValuesList ---> " );
            courtCaseTypeValuesList = new ArrayList <>();
            String url = SERVER_URL + "/getcourtcasetypedropdown";
            BaseDTO baseDTO = httpService.get( url );
            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContents() );
                courtCaseTypeValuesList = mapper.readValue( jsonResponse,
                        new TypeReference <List <CourtCaseType>>() {
                        } );
                if(courtCaseTypeValuesList != null && !courtCaseTypeValuesList.isEmpty()) {
                	log.info("courtCaseTypeValuesList size==> "+courtCaseTypeValuesList.size());
                }else {
                	log.error("courtCaseTypeValuesList is null or empty");
                }
                
                log.info( " court Case Type List Successfully Completed" );

            }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading court Case Type List :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        log.info( "<--- END of CourtCaseRegisterBean.courtCaseTypeValuesList ---> " + courtCaseTypeValuesList );
    }

    private void loadCourtType() {
        try {
            log.info( "<--- START of CourtCaseRegisterBean.loadCourtType ---> " );
            typesOfCourtValues = new ArrayList <>();
            String url = COURT_TYPE_URL + "/getall";
            BaseDTO baseDTO = httpService.get( url );
            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContents() );
                typesOfCourtValues = mapper.readValue( jsonResponse,
                        new TypeReference <List <CourtMaster>>() {
                        } );
                if(typesOfCourtValues != null && !typesOfCourtValues.isEmpty()) {
                	log.info("typesOfCourtValues size==> "+typesOfCourtValues.size());
                }else {
                	log.error("typesOfCourtValues is null or empty");
                }
                /*if (courtTypes == null) {
                    log.info( " court Types List is null" );
                    errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
                }
                typesOfCourtValues = courtTypes;*/
                log.info( " court Types list Successfully Completed" );
            } 

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading court Types list :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
        log.info( "<--- CourtCaseRegisterBean.loadCourtType typesOfCourtValues: ---> " + typesOfCourtValues );
    }

    public void loadheadandregionaloffice() {
        try {
            log.info( "<======START CourtCaseRegisterBean.loadHeadAndRegionalOffice =========>" );
           // headOrRegionalList = new ArrayList <EntityMaster>();
            headOrRegionalList = commonDataService.loadHeadAndRegionalOffice();
            if (headOrRegionalList != null || !headOrRegionalList.isEmpty()) {
                
                log.info("headOrRegionalList size==> "+headOrRegionalList.size());
            } else {
                log.error( "head and regional office list search Failed With status code :: " );
            }
            log.info( "<=========END CourtCaseRegisterBean.loadHeadAndRegionalOffice ==========>" );
        } catch (Exception ex) {
           log.error( "ExceptionException Ocured :: loadheadandregionaloffice==> ", ex );
           
        }

    }

    public void loadEmpComplaintRegister() {
        try {
            log.info( "<=========START CourtCaseRegisterBean.loadEmpComplaintRegister ==========>" );
            empComplaintRegisterList = new ArrayList <>();
            String url = SERVER_URL + "/getIdAndRefNumber";
            BaseDTO baseDTO = httpService.get( url );
            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContents() );
                empComplaintRegisterList = mapper.readValue( jsonResponse,
                        new TypeReference <List <EmpComplaintRegister>>() {
                        } );
                if (empComplaintRegisterList == null || empComplaintRegisterList.isEmpty()) {
                    log.error("emp Complaint Register List is empty" );
                  
                }else {
                	log.info("empComplaintRegisterList size==> "+empComplaintRegisterList.size());
                }
                log.info( " emp Complaint Register List Successfully Completed" );
            }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading emp Complaint Register List :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
       
    }
    
    public void loadEmpComplaintRegisterById(Long id) {
        try {
            log.info( "<=========START CourtCaseRegisterBean.loadEmpComplaintRegister ==========>" );
            empComplaintRegisterList = new ArrayList <>();
            String url = SERVER_URL + "/loadempcomplaintregisterbyid/"+id;
            log.info("loadEmpComplaintRegisterById url==> "+url);
            BaseDTO baseDTO = httpService.get( url );
            if (baseDTO != null && baseDTO.getStatusCode() == 0) {
                jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContents() );
                empComplaintRegisterList = mapper.readValue( jsonResponse,
                        new TypeReference <List <EmpComplaintRegister>>() {
                        } );
                if (empComplaintRegisterList == null || empComplaintRegisterList.isEmpty()) {
                    log.error("emp Complaint Register List is empty" );
                 
                }else {
                	log.info("empComplaintRegisterList size==> "+empComplaintRegisterList.size()); 
                }
               
            } else {
                log.error( "loadEmpComplaintRegisterById :: No data found :: " + id );
             }

        } catch (Exception e) {
            log.error( "ExceptionException Ocured while Loading emp Complaint Register List :: ", e );
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
        }
       
    }
    
  

    public String deleteCourtCaseRegister() {
        log.info( "<==== START  CourtCaseRegisterBean.deleteCourtCaseRegister :: selectedCourtCaseDTOPage Object =====>", selectedCourtCaseDTOPage );
        String url = SERVER_URL + "/delete/" + selectedCourtCaseDTOPage.getId();
        BaseDTO response = httpService.get( url );
        if (response != null) {
            errorMap.notify( ErrorDescription.COURT_CASE_REGISTER_DELETE_SUCCESS.getErrorCode() );
        } else {
            errorMap.notify( ErrorDescription.COURT_CASE_REGISTER_DELETE_FAILURE.getErrorCode() );
        }
        RequestContext context = RequestContext.getCurrentInstance();
        context.execute( "PF('confirmPetitionRegisterDelete').hide();" );
        log.info( "<==== END  CourtCaseRegisterBean.deleteCourtCaseRegister ");
        clearCourtCaseRegisterSelections();
        return LIST_PAGE;
    }

    public void addPetitionerName() {
        log.info( "<==== START  CourtCaseRegisterBean.addPetitionerName ");
        if(petitionerName.isEmpty())
        {
        	log.info("======Petitioner Name is Empty=========");
        	Util.addWarn("Petitionar Name is Empty");
        }
        if(!petitionerName.isEmpty())
        {
        	 petitionerNameList.add(petitionerName);
        }
        petitionerName="";
        log.info( "values" + petitionerNameList );
        log.info( "<==== END  CourtCaseRegisterBean.addPetitionerName ");
    }

    public void addRespondentName() {
        log.info( "<==== START  CourtCaseRegisterBean.addRespondentName ");
        if(respondentName.isEmpty()){
        	log.info("======Respondent Name is Empty=========");
        	Util.addWarn("Respondent Name is Empty");
        	return;
        }
        if(petitionerNameList != null && !petitionerNameList.isEmpty()){
        	respondentNameList.add( respondentName );
        }
        respondentName="";
        log.info( "values" + respondentNameList );
        log.info( "<==== END  CourtCaseRegisterBean.addRespondentName ");

    }

    public void deletePetitionerList(String petitionerName) {
        log.info( "<==== START  CourtCaseRegisterBean.deletePetitionerList ");
        try {
            if (petitionerNameList.contains( petitionerName )) {
                petitionerNameList.remove( petitionerName );
                log.info( "<==== END  CourtCaseRegisterBean.deletePetitionerList ");
            }
        } catch (Exception exp) {
            log.info( "Error In deletePetitionerList Metod", exp );
        }
    }

    public void deleteRespondentList(String respondentName) {
        log.info( "<==== START  CourtCaseRegisterBean.deleteRespondentList ");
        try {
            if (respondentNameList.contains( respondentName )) {
                respondentNameList.remove( respondentName );
                log.info( "<==== END  CourtCaseRegisterBean.deleteRespondentList ");
            }
        } catch (Exception exp) {
            log.info( "Error In deleteRespondentList Metod", exp );
        }
    }

    public String uploadDocument(UploadedFile file) {
        log.info( "<<=== CourtCaseRegisterBean. uploadDocument File Upload method called" );
        try {
            fileType = file.getFileName();
            if (!fileType.contains( ".pdf" ) && !fileType.contains( ".jpg" ) && !fileType.contains( ".png" )
                    && !fileType.contains( ".jpeg" )
                    && !fileType.contains( ".docx" )
                    && !fileType.contains( ".doc" )) {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages( true );
                errorMap.notify( ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode() );
                log.info( "<<====  error type in file upload" );
            }
            long size = file.getSize();
            //			size = size /  (1024*1024);
            if ((size / (1024 * 1024)) > 2) {
                FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages( true );
                errorMap.notify( ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode() );
                log.info( "<<====  Large size file uploaded" );
            }
            if (size > 0) {
                String createRaiseComplaintPathName = commonDataService.getAppKeyValue( "COURT_CASE_FILE_PATH" );
                String filePath = Util.fileUpload( createRaiseComplaintPathName, file );
                courtCaseDocumentFilePath.add( filePath );
                //errorMap.notify( ErrorDescription.COURT_CASE_FILE_UPLOAD_SUCCESS.getErrorCode() );
                log.info( " Uploading Document is uploaded successfully" );
                return filePath;
            }

        } catch (Exception e) {
            errorMap.notify( ErrorDescription.INTERNAL_ERROR.getErrorCode() );
            log.info( "<<==  Error in file upload " + e.getMessage() );
        }
        log.info( "<====== CourtCaseRegisterBean.handleFileUpload Ends====> " );
        return null;
    }

    public void addFilesList(FileUploadEvent event) {
        log.info( "<====addFilesList Method Start==>" );
        UploadedFile file = event.getFile();
       // uploadedFilesList.add( file );
        
        long size = file.getSize();
		//			size = size /  (1024*1024);
		
		
		if((size /  (1024*1024)) >2) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
			log.info("<<====  Large size file uploaded");
		}
		if (size >= 2000 ) {
			String createRaiseComplaintPathName = commonDataService.getAppKeyValue("COURT_CASE_FILE_PATH");
			String filePath = Util.fileUpload(createRaiseComplaintPathName, file);
//			FileName = filePath;
			 uploadedFilesList.add( file );
			errorMap.notify(ErrorDescription.COURT_CASE_FILE_UPLOAD_SUCCESS.getErrorCode());
			log.info(" Uploading Document is uploaded successfully");
		}
		else {
			Util.addWarn("File size should be less than 2 MB");
			return ;
		}
        log.info( "<=== ENDS addFilesList===> " );

    }

    public void deleteFilesList(UploadedFile file) {
        log.info( "<====addFilesList Method Start==>" );
        try {
            if (uploadedFilesList.contains( file )) {
                uploadedFilesList.remove( file );
            }
            log.info( "<====addFilesList Method Ends==>" );
        } catch (Exception exp) {
            log.info( "Error In deleteFilesList Metod", exp );
        }
    }

    public void getComplaintRaisedDate() {
        log.info( "Error In deleteRespondentList Metod", empComplaintRegister );
        Long id = empComplaintRegister.getId();
        for (EmpComplaintRegister eCR : empComplaintRegisterList) {
            if (eCR.getId().equals( id )) {
                empComplaintRegister.setComplaintRaisedDate( eCR.getComplaintRaisedDate() );
            }
        }
    }

    public void downloadFile(String filePath) {
        InputStream input = null;
        try {
            File file = new File(filePath);
            input = new FileInputStream(file);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            this.setFile(
                    new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
        } catch (FileNotFoundException e) {
            log.error("FileNotFoundException in filedownload method------- " + e);
        } catch (Exception e) {
            log.error("exception in filedownload method------- " + e);
        }
    }

    public class FileList{

        @Setter
        @Getter
        String fileName;

        @Setter
        @Getter
        String filePath;
    }
    
    public String getCaseNo() {
    	log.info("==========Start getCaseNo()===========");
    	try {
    	List<String> courtcaseNoList=new ArrayList<String>();
    	String url = SERVER_URL + "/courtcaseNo";
        BaseDTO baseDTO = httpService.get( url );
        if (baseDTO != null && baseDTO.getStatusCode() == 0) {
            jsonResponse = mapper.writeValueAsString( baseDTO.getResponseContents() );
            courtcaseNoList = mapper.readValue( jsonResponse,
                    new TypeReference <List <String>>() {
                    } );
    	}
        if((courtcaseNoList!=null && !courtcaseNoList.isEmpty()) && courtcaseNoList.contains(courtCaseRegisterRequestDTO.getCaseNo())) {
        	Util.addWarn("Court caseNo already Exist");
        	caseNoFlag=true;
        }else
        {
        	caseNoFlag=false;
        }
    	}
    	catch(Exception ex) {
    		log.info("Error in getCaseNo=====",ex);
    	}
    	log.info("==========End getCaseNo()===========");
    	return null;
    }
}
