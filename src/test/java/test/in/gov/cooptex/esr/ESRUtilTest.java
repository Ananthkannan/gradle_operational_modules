package test.in.gov.cooptex.esr;

import java.util.Map;

import in.gov.cooptex.personnel.hrms.dto.ESRPrintDTO;
import in.gov.cooptex.personnel.hrms.util.ESRUtil;
import in.gov.cooptex.personnel.rest.util.PrintESR;

/**
 * 
 */

/**
 * @author ftuser
 *
 */
public class ESRUtilTest {

	/**
	 * 
	 */
	public ESRUtilTest() {
		// TODO Auto-generated constructor stub
	}

	private void printESR(Long empId) {
		ESRUtil userUtil = new ESRUtil();
		Map<String, ESRPrintDTO> recordNameDataMap = userUtil.getESRDataListBySQLConn(empId);

		PrintESR printESR = new PrintESR("/temp");
		printESR.printData(recordNameDataMap);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ESRUtilTest esrUtilTest = new ESRUtilTest();
		esrUtilTest.printESR(142L);

	}

}
