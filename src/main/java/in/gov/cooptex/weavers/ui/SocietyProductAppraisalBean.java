/**
 * 
 */
package in.gov.cooptex.weavers.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.WrapperDTO;
import in.gov.cooptex.core.model.ProductCategory;
import in.gov.cooptex.core.model.ProductGroupMaster;
import in.gov.cooptex.core.model.ProductVarietyMaster;
import in.gov.cooptex.core.model.ProductVarietyPrice;
import in.gov.cooptex.core.model.SocietyProductAppraisal;
import in.gov.cooptex.core.model.YarnTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.dto.YarnTypeMasterDTO;
import in.gov.cooptex.operation.model.ProductCostingMaster;
import in.gov.cooptex.operation.model.SupplierMaster;
import in.gov.cooptex.operation.production.model.ProductDesignMaster;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

/**
 * @author Pratap
 *
 */
@Service("societyProductAppraisalBean")
@Scope("session")
@Log4j2
public class SocietyProductAppraisalBean {

	@Autowired
	AppPreference appPreference;

	private final String CREATE_SOCIETY_PRODUCT_APPRAISAL_PAGE = "/pages/weavers/createSocietyProductAppraisal.xhtml?faces-redirect=true;";
	private final String LIST_SOCIETY_PRODUCT_APPRAISAL_PAGE = "/pages/weavers/listSocietyProductAppraisal.xhtml?faces-redirect=true;";
	private final String VIEW_SOCIETY_PRODUCT_APPRAISAL_PAGE = "/pages/weavers/viewSocietyProductAppraisal.xhtml?faces-redirect=true;";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	LoginBean loginBean;

	String jsonResponse;

	ObjectMapper mapper;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	SocietyProductAppraisal societyProductAppraisal;

	@Getter
	@Setter
	LazyDataModel<SocietyProductAppraisal> lazySocietyProductAppraisalList;

	List<SocietyProductAppraisal> societyProductAppraisalList;

	@Getter
	@Setter
	List<SupplierMaster> supplierMasterList;

	@Getter
	@Setter
	List<ProductCategory> productCategoryList;

	@Getter
	@Setter
	List<ProductGroupMaster> productGroupList;

	@Getter
	@Setter
	List<ProductVarietyMaster> productVarietyMasterList;

	@Getter
	@Setter
	ProductCostingMaster productCostingMaster;

	@Getter
	@Setter
	ProductVarietyMaster productVarietyMaster;

	@Getter
	@Setter
	ProductGroupMaster productGroup;

	@Getter
	@Setter
	ProductCategory productCategory;

	@Getter
	@Setter
	SupplierMaster supplierMaster;

	String SOCIETY_PRODUCT_APPRAISAL_URL = "";

	@Getter
	@Setter
	SocietyProductAppraisal selectedSocietyProductAppraisal;

	@Setter
	@Getter
	StreamedContent file;

	@Setter
	@Getter
	StreamedContent downloadFile;

	@Getter
	@Setter
	Boolean viewRendered = true;

	@Getter
	@Setter
	boolean productcostRendered = false;

	@Getter
	@Setter
	ProductVarietyPrice productVarietyPrice;

	@Setter
	@Getter
	Double silkCost = 0.0, zariCost = 0.0, tanaCharges, totaSilkCost = 0.0, totaZariCost = 0.0,
			totalSilkWastageCost = 0.0, costPrice, margin = 0.0, purchasePrice = 0.0, purchasePriceRoundOff = 0.0,
			productPurchasePrice = 0.0, appriaserqnty;

	@Getter
	@Setter
	boolean blousetype;

	@Getter
	@Setter
	String bordertype, societyATnumber;

	@Getter
	@Setter
	List<ProductDesignMaster> productDesignMasterList;

	@Getter
	@Setter
	ProductDesignMaster productDesignMaster;

	@Getter
	@Setter
	List<YarnTypeMaster> yarnTypeMasterList = new ArrayList<YarnTypeMaster>();

	@Getter
	@Setter
	List<YarnTypeMasterDTO> yarnTypeMasterDtoList;

	@Getter
	@Setter
	YarnTypeMasterDTO selectedWarpYarnTypeMasterDTO;

	@Getter
	@Setter
	YarnTypeMasterDTO selectedWeftYarnTypeMasterDTO;
	
	@Getter
	@Setter
	Boolean productCostDetailsFlag = false;

	public String showSocietyProductAppraisalListPage() {
		log.info("<==== Starts SocietyProductAppraisalBean-showSocietyProductAppraisalListPage =====>");
		mapper = new ObjectMapper();
		selectedSocietyProductAppraisal = new SocietyProductAppraisal();
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		viewButtonFlag = false;
		action = "";
		supplierMaster = new SupplierMaster();
		productCategory = new ProductCategory();
		productGroup = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		productDesignMaster = new ProductDesignMaster();
		SOCIETY_PRODUCT_APPRAISAL_URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
				+ "/societyproductappraisal";
		// loadSupplierMasterList();
		// loadProductCategoryList();

		loadLazySocietyProductAppraisalList();
		log.info("<==== Ends SocietyProductAppraisalBean-showSocietyProductAppraisalListPage =====>");
		return LIST_SOCIETY_PRODUCT_APPRAISAL_PAGE;

	}

	public List<YarnTypeMasterDTO> getYarnTypeList() {
		log.info("--ProductVarietyMasterBean  getYarnTypeList---");

		yarnTypeMasterDtoList = new ArrayList<YarnTypeMasterDTO>();
		WrapperDTO wrapperDto = new WrapperDTO();
		RestTemplate restTemplate = new RestTemplate();
		try {
			String url = AppUtil.getPortalServerURL() + "/Yarntypemaster/getall";

			HttpEntity<String> entity = new HttpEntity<String>("parameters", loginBean.headers);
			HttpEntity<WrapperDTO> response = restTemplate.exchange(url, HttpMethod.GET, entity, WrapperDTO.class);
			log.info("<----Url--->" + url);
			wrapperDto = response.getBody();
			log.info("Wrapper Dto :" + response.getBody());
			if (wrapperDto != null) {
				if (wrapperDto.getYarnTypeMasterDtoCollection() != null) {
					yarnTypeMasterDtoList = (List<YarnTypeMasterDTO>) wrapperDto.getYarnTypeMasterDtoCollection();
					log.info("No of yarnTypeMasterDtoList :" + yarnTypeMasterDtoList.size());
				} else {
					yarnTypeMasterDtoList = new ArrayList<YarnTypeMasterDTO>();
				}
			}
			log.info("---getYarnTypeList--   End");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return yarnTypeMasterDtoList;

	}

	public String clear() {
		log.info("<==== Starts SocietyProductAppraisalBean-clear =====>");
		mapper = new ObjectMapper();
		selectedSocietyProductAppraisal = new SocietyProductAppraisal();
		editButtonFlag = false;
		deleteButtonFlag = false;
		addButtonFlag = true;
		viewButtonFlag = false;
		action = "";
		supplierMaster = new SupplierMaster();
		productCategory = new ProductCategory();
		productGroup = new ProductGroupMaster();
		productVarietyMaster = new ProductVarietyMaster();
		loadLazySocietyProductAppraisalList();
		log.info("<==== Ends SocietyProductAppraisalBean-clear =====>");
		return LIST_SOCIETY_PRODUCT_APPRAISAL_PAGE;
	}

	public String societyProductAppraisalListPageAction() {
		log.info("<========Starts SocietyProductAppraisalBean.societyProductAppraisalListPageAction=======>");
		try {
			productCostingMaster = new ProductCostingMaster();

			if (!action.equalsIgnoreCase("create")
					&& (selectedSocietyProductAppraisal == null || selectedSocietyProductAppraisal.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
				return null;
			}

			if (action.equalsIgnoreCase("Create")) {
				log.info("create SocietyProductAppraisal called..");
				// loadSupplierMasterList();
				loadProductCategoryList();
				yarnTypeMasterDtoList = getYarnTypeList();
				silkCost = 0.0;
				zariCost = 0.0;
				// tanaCharges = 0.0;
				totaSilkCost = 0.0;
				totaZariCost = 0.0;
				totalSilkWastageCost = 0.0;
				// costPrice = 0.0;
				margin = 0.0;
				purchasePrice = 0.0;
				purchasePriceRoundOff = 0.0;
				productPurchasePrice = 0.0;
				appriaserqnty = 0.00;
				supplierMaster = new SupplierMaster();
				productCategory = new ProductCategory();
				productGroup = new ProductGroupMaster();
				productVarietyMaster = new ProductVarietyMaster();
				societyProductAppraisal = new SocietyProductAppraisal();
				societyATnumber = null;
				societyProductAppraisal.setSociety(new SupplierMaster());
				societyProductAppraisal.setSociety(commonDataService.loadSupplierMasterByUserId(loginBean.getUserDetailSession().getId()));
				societyProductAppraisal.getSociety().setCode(loginBean.getUserInfoDTO().getShowRoomCode());
				selectedWarpYarnTypeMasterDTO = new YarnTypeMasterDTO();
				selectedWeftYarnTypeMasterDTO = new YarnTypeMasterDTO();
				// appriaserqnty = 0.0;
				return CREATE_SOCIETY_PRODUCT_APPRAISAL_PAGE;
			} else if (action.equalsIgnoreCase("View") || action.equalsIgnoreCase("Edit")) {
				log.info("view/edit SocietyProductAppraisal called..");
				String url = SOCIETY_PRODUCT_APPRAISAL_URL + "/get/" + selectedSocietyProductAppraisal.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					societyProductAppraisal = mapper.readValue(jsonResponse,
							new TypeReference<SocietyProductAppraisal>() {
							});
					// loadSupplierMasterList();
					loadProductCategoryList();
					if (societyProductAppraisal != null && societyProductAppraisal.getId() != null) {
						ProductVarietyMaster productVarietyMaster = societyProductAppraisal.getProductVariety();
						productCategory = societyProductAppraisal.getProductCategory();
						productGroup = societyProductAppraisal.getProductGroupMaster();
						if (productCategory != null)
							updateProductCategoryGroupList();
						if (productGroup != null)
							updateProductVarietyList();
						societyProductAppraisal.setProductVariety(productVarietyMaster);
						updateProductCasting();
						updateProductCost();

					}
					if (action.equalsIgnoreCase("Edit")) {
						//updateProductVartCode();
						selectedWarpYarnTypeMasterDTO = new YarnTypeMasterDTO();
						selectedWeftYarnTypeMasterDTO = new YarnTypeMasterDTO();
						if(societyProductAppraisal.getWarpYarnTypeMaster()!=null && societyProductAppraisal.getWarpYarnTypeMaster().getId()!=null)
						{
							selectedWarpYarnTypeMasterDTO.setId(societyProductAppraisal.getWarpYarnTypeMaster().getId());
							selectedWarpYarnTypeMasterDTO.setCode(societyProductAppraisal.getWarpYarnTypeMaster().getCode());
							selectedWarpYarnTypeMasterDTO.setName(societyProductAppraisal.getWarpYarnTypeMaster().getName());
						}
						if(societyProductAppraisal.getWeftYarnTypeMaster()!=null && societyProductAppraisal.getWeftYarnTypeMaster().getId()!=null)
						{
							selectedWeftYarnTypeMasterDTO.setId(societyProductAppraisal.getWeftYarnTypeMaster().getId());
							selectedWeftYarnTypeMasterDTO.setCode(societyProductAppraisal.getWeftYarnTypeMaster().getCode());
							selectedWeftYarnTypeMasterDTO.setName(societyProductAppraisal.getWeftYarnTypeMaster().getName());
						}
						setAppriaserqnty(1D);
						productDesignMaster = societyProductAppraisal.getProductDesignMaster();
						return CREATE_SOCIETY_PRODUCT_APPRAISAL_PAGE;
					} else {
						return VIEW_SOCIETY_PRODUCT_APPRAISAL_PAGE;
					}
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete Society SocietyProductAppraisal called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyProductAppraisalDelete').show();");
			}
		} catch (Exception e) {
			log.error(" Error Occured :: ", e);
		}
		log.info("<========Ends SocietyProductAppraisalBean.societyProductAppraisalListPageAction=======>");
		return null;
	}

	@Getter @Setter
	private StreamedContent outPutImageToDisplay;
	
	public void generateBarCode() {
		log.info("SocietyProductAppraisalBean. generateBarCode() Starts");
		
		try {			

			String url = SOCIETY_PRODUCT_APPRAISAL_URL + "/downloadbarcode";
			log.info("downloadBarCode [" + societyProductAppraisal + "]");

			InputStream inputStream = httpService.generatePDF(url, societyProductAppraisal);
			
			File file = new File(societyProductAppraisal.getAtNumber() + "_barcode.png");
			FileOutputStream outputStream = new FileOutputStream(file);
			IOUtils.copy(inputStream, outputStream);
									
			setOutPutImageToDisplay(new DefaultStreamedContent(new FileInputStream(file), "image/png"));
            log.info("file.getName()"+file.getName());
			if (file.exists()) {
				file.delete();
				log.info("file deleted successfully.......");
			}
			log.info("downloadBarCodeInCreatePage finished successfully....");
		} catch (Exception e) {
			log.error("Exception occured while downloadBarCodeInCreatePage ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<========= Ends downloadBarCodeInCreatePage called ===========>");
	}
	
	public String saveOrUpdate() {
		log.info("<== Starts SocietyProductAppraisalBean.saveorupdate.===>");
		try {
			// validateData();
			if (!productCostDetailsFlag) {
				AppUtil.addError("Please add Product Cost Details");
				return null;
			}
			if(societyProductAppraisal.getProductCost().equals(0D)) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.PRODUCT_COST_NOT_ZERO).getErrorCode());
				return null;
			}
			societyProductAppraisal.setUserId(loginBean.getUserDetailSession().getId());
			YarnTypeMaster warpYarnTypeMaster = new YarnTypeMaster();
			YarnTypeMaster weftYarnTypeMaster = new YarnTypeMaster();
			if (selectedWarpYarnTypeMasterDTO != null && selectedWarpYarnTypeMasterDTO.getId() != null) {
				warpYarnTypeMaster.setId(selectedWarpYarnTypeMasterDTO.getId());
			}
			if (selectedWeftYarnTypeMasterDTO != null && selectedWeftYarnTypeMasterDTO.getId() != null) {
				weftYarnTypeMaster.setId(selectedWeftYarnTypeMasterDTO.getId());
			}
			societyProductAppraisal.setAppriaserQty(appriaserqnty);
			societyProductAppraisal.setWarpYarnTypeMaster(warpYarnTypeMaster);
			societyProductAppraisal.setWeftYarnTypeMaster(weftYarnTypeMaster);
			String society_url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/societyproductappraisal/saveorupdate";
			if (productDesignMaster != null) {
				societyProductAppraisal.setDesignNumber(productDesignMaster.getDesignCode().toString());
			}
			loginBean.getUserInfoDTO();
			BaseDTO response = httpService.post(society_url, societyProductAppraisal);
			if (response != null && response.getStatusCode() == 0) {
				log.info("<===== Inside ProcurementCostingBean.saveProcurementCosting Reponse Clause  ======>");
				//errorMap.notify(ErrorDescription.PROCUREMENT_SAVED_SUCCESS.getCode());
				selectedSocietyProductAppraisal = null;
				
				if(!action.equalsIgnoreCase("Edit")) {
					List<String> atNumnberList = (List<String>) response.getResponseContent();
					int atNumberListSize = atNumnberList == null ? null : atNumnberList.size();
					
					if(atNumberListSize > 0) {
						societyProductAppraisal.setAtNumber(atNumnberList.get(0));
					}
					
					generateBarCode();
					RequestContext.getCurrentInstance().execute("PF('barCodeImgPrintDlg').show();");
					RequestContext.getCurrentInstance().execute("barcodeprint()");
					RequestContext.getCurrentInstance().execute("PF('barCodeImgPrintDlg').hide()");
				}
				return LIST_SOCIETY_PRODUCT_APPRAISAL_PAGE;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
			productCostDetailsFlag=false;
		} catch (Exception e) {
			log.info("<===== Exception Occured in SocietyProductAppraisalBean.saveProcurementCosting ======>" + e);

		}
		log.info("<===== Ends SocietyProductAppraisalBean.saveProcurementCosting ======>");
		return null;
	}

	public void validateData() {

		if (societyProductAppraisal.getDesignName() == null || societyProductAppraisal.getDesignName().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_DESIGN_ENTER.getErrorCode());
		} else if (societyProductAppraisal.getDesignNumber() == null
				|| societyProductAppraisal.getDesignNumber().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_DESIGN_ENTER.getErrorCode());
		} else if (societyProductAppraisal.getSilkWeight() == 0.0 || societyProductAppraisal.getSilkWeight() == null) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_SKILWEIGHT.getErrorCode());
		} else if (societyProductAppraisal.getZariWeight() == 0.0 || societyProductAppraisal.getZariWeight() == null) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_ZARIWEIGHT.getErrorCode());
		} else if (societyProductAppraisal.getSilkWastageWeight() == 0.0
				|| societyProductAppraisal.getSilkWastageWeight() == null) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_SLIKWASTAGE.getErrorCode());
		} else if (societyProductAppraisal.getTotalProductWeight() == 0.0
				|| societyProductAppraisal.getTotalProductWeight() == null) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_TOTOAL_PRODUCT.getErrorCode());
		} else if (societyProductAppraisal.getProductLength() == null
				|| societyProductAppraisal.getProductLength() == 0.0) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_PRODUCT_LENGTH.getErrorCode());
		} else if (societyProductAppraisal.getProductWidth() == null
				|| societyProductAppraisal.getProductWidth() == 0.0) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_PRPDUCT_WEIGHT.getErrorCode());
		} else if (societyProductAppraisal.getBodyColor() == null || societyProductAppraisal.getBodyColor().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_BODY_COLOR.getErrorCode());
		} else if (societyProductAppraisal.getBodyDesign() == null
				|| societyProductAppraisal.getBodyDesign().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_BODY_DESIGN.getErrorCode());
		} else if (societyProductAppraisal.getPalluDesign() == null
				|| societyProductAppraisal.getPalluDesign().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_PALLUDESIGN.getErrorCode());
		} else if (societyProductAppraisal.getZariType() == null || societyProductAppraisal.getZariType().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_ZARITYPE.getErrorCode());
		} else if (societyProductAppraisal.getBorderColor() == null
				|| societyProductAppraisal.getBorderColor().isEmpty()) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_BORDERCOLOR.getErrorCode());
		} else if (societyProductAppraisal.getBorderSize() == null || societyProductAppraisal.getBorderSize() == 0.0) {
			errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_ENTER_BORDERSIZE.getErrorCode());
		}

	}

	public void loadLazySocietyProductAppraisalList() {
		log.info("<===== Starts SocietyProductAppraisalBean.loadLazySocietyProductAppraisalList ======>");

		lazySocietyProductAppraisalList = new LazyDataModel<SocietyProductAppraisal>() {
			private static final long serialVersionUID = 5221554353084853593L;

			@Override
			public List<SocietyProductAppraisal> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SOCIETY_PRODUCT_APPRAISAL_URL + "/getlazysocietyproductappraisallist";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						societyProductAppraisalList = mapper.readValue(jsonResponse,
								new TypeReference<List<SocietyProductAppraisal>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return societyProductAppraisalList;
			}

			@Override
			public Object getRowKey(SocietyProductAppraisal res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public SocietyProductAppraisal getRowData(String rowKey) {
				for (SocietyProductAppraisal societyProductAppraisal : societyProductAppraisalList) {
					if (societyProductAppraisal.getId().equals(Long.valueOf(rowKey))) {
						selectedSocietyProductAppraisal = societyProductAppraisal;
						return societyProductAppraisal;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends SocietyProductAppraisalBean.loadLazySocietyProductAppraisalList ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts SocietyProductAppraisalBean.onRowSelect ========>" + event);
		selectedSocietyProductAppraisal = ((SocietyProductAppraisal) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		viewButtonFlag = true;
		log.info("<===Ends SocietyProductAppraisalBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts SocietyProductAppraisalBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService
					.delete(SOCIETY_PRODUCT_APPRAISAL_URL + "/deletebyid/" + selectedSocietyProductAppraisal.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.SOCIETY_PRODUCT_APPRAISAL_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmSocietyProductAppraisalDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete SocietyProductAppraisal ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.deleteConfirm ===========>");
		return showSocietyProductAppraisalListPage();
	}

	// public void loadSupplierMasterList() {
	// log.info("<===== Starts SocietyProductAppraisalBean.loadSupplierMasterList
	// ===========>");
	// try {
	// BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL +
	// "/getall/supplier");
	// if (response != null && response.getStatusCode() == 0) {
	// jsonResponse = mapper.writeValueAsString(response.getResponseContent());
	// supplierMasterList = mapper.readValue(jsonResponse, new
	// TypeReference<List<SupplierMaster>>() {
	// });
	// } else if (response != null && response.getStatusCode() != 0) {
	// errorMap.notify(response.getStatusCode());
	// } else {
	// errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
	// }
	// } catch (Exception e) {
	// log.error("Exception occured while loadSupplierMasterList ....", e);
	// errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
	// }
	// log.info("<===== Ends SocietyProductAppraisalBean.loadSupplierMasterList
	// ===========>");
	// }

	public void loadProductCategoryList() {
		log.info("<===== Starts SocietyProductAppraisalBean.loadProductCategoryList ===========>");
		try {

			BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL + "/getall/productcategorybyuser");

			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<ProductCategory>>() {
				});
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while loadProductCategoryList ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.loadProductCategoryList ===========>");
	}

	public void updateProductCategoryGroupList() {
		log.info("<===== Starts SocietyProductAppraisalBean.updateProductCategoryGroupList ===========>");
		if (productCategory == null || productCategory.getId() == null) {
			log.info("Product category is null");
			productGroup = new ProductGroupMaster();
			societyProductAppraisal.setProductVariety(new ProductVarietyMaster());
			productGroupList = new ArrayList<>();
			productVarietyMasterList = new ArrayList<>();
			productVarietyMaster = new ProductVarietyMaster();
			return;
		}

		String supplierCode = null;

		try {

			if (loginBean.getUserInfoDTO() != null) {
				supplierCode = loginBean.getUserInfoDTO().getShowRoomCode();
			}
			BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL + "/getall/productgroup/"
					+ productCategory.getId() + "/" + supplierCode);

			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productGroupList = mapper.readValue(jsonResponse, new TypeReference<List<ProductGroupMaster>>() {
				});
				societyProductAppraisal.setProductVariety(new ProductVarietyMaster());
				productVarietyMasterList = new ArrayList<>();
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while updateProductCategoryGroupList ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.updateProductCategoryGroupList ===========>");
	}

	public void updateDesignMaster() {
		if (productDesignMaster != null) {
			societyProductAppraisal.setDesignName(productDesignMaster.getDesignName());
			DecimalFormat decimalFormat = new DecimalFormat("#0");
			String designCode = decimalFormat.format(productDesignMaster.getDesignCode());
			log.info("designCode==> " + designCode);
			societyProductAppraisal.setDesignNumber(designCode);

		}

	}

	public void updateProductVartCode() {

		log.info("<===== Starts SocietyProductAppraisalBean.updateProductVartCode ===========>");

		try {
			societyProductAppraisal.setWarpCount(societyProductAppraisal.getProductVariety().getWarpYarnCountActual());
			societyProductAppraisal.setWeftCount(societyProductAppraisal.getProductVariety().getWeftYarnCountActual());
			if (societyProductAppraisal.getProductVariety().getWarpyarnTypeMaster() != null) {
				selectedWarpYarnTypeMasterDTO = new YarnTypeMasterDTO();
				selectedWarpYarnTypeMasterDTO
						.setId(societyProductAppraisal.getProductVariety().getWarpyarnTypeMaster().getId());
				selectedWarpYarnTypeMasterDTO
						.setCode(societyProductAppraisal.getProductVariety().getWarpyarnTypeMaster().getCode());
				selectedWarpYarnTypeMasterDTO
						.setName(societyProductAppraisal.getProductVariety().getWarpyarnTypeMaster().getName());
			}
			if (societyProductAppraisal.getProductVariety().getWeftyarnTypeMaster() != null) {
				selectedWeftYarnTypeMasterDTO = new YarnTypeMasterDTO();
				selectedWeftYarnTypeMasterDTO
						.setCode(societyProductAppraisal.getProductVariety().getWeftyarnTypeMaster().getCode());
				selectedWeftYarnTypeMasterDTO
						.setId(societyProductAppraisal.getProductVariety().getWeftyarnTypeMaster().getId());
				selectedWeftYarnTypeMasterDTO
						.setName(societyProductAppraisal.getProductVariety().getWeftyarnTypeMaster().getName());
			}
			BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL + "/getall/productvarietyRate/"
					+ societyProductAppraisal.getProductVariety().getId());
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productVarietyPrice = mapper.readValue(jsonResponse, new TypeReference<ProductVarietyPrice>() {
				});

				String jsonResponseDesing = mapper.writeValueAsString(response.getResponseContents());
				productDesignMasterList = mapper.readValue(jsonResponseDesing,
						new TypeReference<List<ProductDesignMaster>>() {
						});
				if (productVarietyPrice != null) {
					societyProductAppraisal.setProductCost(productVarietyPrice.getUnitRate());
					if(productVarietyPrice.getProductVarietyMaster() != null) {
						log.info("product varity master------------"+productVarietyPrice.getProductVarietyMaster().getCostingType());
						if(StringUtils.isNotEmpty(productVarietyPrice.getProductVarietyMaster().getCostingType())) {
							log.info("Product costing type not null or not empty");
							if (productVarietyPrice.getProductVarietyMaster().getCostingType()
									.equalsIgnoreCase("Common Costing")) {
								viewRendered = false;
								productcostRendered = true;
							} else if (productVarietyPrice.getProductVarietyMaster().getCostingType()
									.equalsIgnoreCase("Individual Costing")) {
		
								viewRendered = true;
								productcostRendered = false;
								societyProductAppraisal.setProductCost(0.0);
							}
						}else {
							log.info("Product costing type null or empty");
							AppUtil.addWarn("Please set the costing type");
							societyProductAppraisal.setProductVariety(null);
						}
					}

				}

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while updateProductVartCode ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.updateProductVartCode ===========>");
	}

	public void updateProductVarietyList() {
		log.info("<===== Starts SocietyProductAppraisalBean.updateProductVarietyList ===========>");
		if (productGroup == null || productGroup.getId() == null) {
			log.info("Product category group is null");
			productVarietyMaster = new ProductVarietyMaster();
			productVarietyMasterList = new ArrayList<>();
			societyProductAppraisal.setProductVariety(new ProductVarietyMaster());
			return;
		}
		String supplierCode = null;
		try {
			if (loginBean.getUserInfoDTO() != null) {
				supplierCode = loginBean.getUserInfoDTO().getShowRoomCode();
			}
			BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL + "/getall/productvariety/"
					+ productGroup.getId() + "/" + supplierCode);
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productVarietyMasterList = mapper.readValue(jsonResponse,
						new TypeReference<List<ProductVarietyMaster>>() {
						});
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while updateProductVarietyList ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.updateProductVarietyList ===========>");
	}

	public void downloadBarCode() {
		log.info("<========= Starts downloadBarCode called ===========>");
		if ((selectedSocietyProductAppraisal == null || selectedSocietyProductAppraisal.getId() == null)) {
			errorMap.notify(ErrorDescription.SELECT_RECORD.getCode());
			return;
		}
		try {
			if (selectedSocietyProductAppraisal.getAtNumber() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.WEAVERS_ATNUMBER_NOT_AVAILABLE).getCode());
				return;
			}

			String url = SOCIETY_PRODUCT_APPRAISAL_URL + "/downloadbarcode";
			log.info("downloadBarCode [" + selectedSocietyProductAppraisal + "]");

			InputStream inputStream = httpService.generatePDF(url, selectedSocietyProductAppraisal);

			downloadFile = new DefaultStreamedContent(inputStream, "application/image",
					selectedSocietyProductAppraisal.getAtNumber() + "_barcode.png");
			/*File f = new File(selectedSocietyProductAppraisal.getAtNumber() + "_barcode.png");
			log.info("file exist ::::::::" + f.exists());
			if (f.exists()) {
				// f.delete();
				log.info("file deleted successfully.......");
			}*/
			log.info("downloadQrCodePdf finished successfully....");
		} catch (Exception e) {
			log.error("Exception occured while downloadBarCode ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<========= Ends downloadBarCode called ===========>");
	}

	public void viewProductCosting() {
		log.info("viewProductCosting called.");
		if (productCostingMaster != null && productCostingMaster.getId() != null) {
			updateProductCost();
		}else {
			updateProductCasting();
		}
		RequestContext.getCurrentInstance().execute("PF('view_ind_cost_prep').show()");
		log.info("viewProductCosting ends.");
	}

	public void updateProductCasting() {
		log.info("<===== Starts SocietyProductAppraisalBean.updateProductCasting ===========>");
		if (societyProductAppraisal == null || societyProductAppraisal.getSociety() == null
				|| societyProductAppraisal.getSociety().getId() == null) {
			log.info("society is null");
			silkCost = 0.0;
			zariCost = 0.0;
			return;
		}

		try {
			BaseDTO response = httpService.get(SOCIETY_PRODUCT_APPRAISAL_URL + "/get/productcosting/"
					+ societyProductAppraisal.getSociety().getId());
			if (response != null && response.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				productCostingMaster = mapper.readValue(jsonResponse, new TypeReference<ProductCostingMaster>() {
				});
				log.info("productCostingMaster :::::::::::::" + productCostingMaster);
				if (productCostingMaster == null) {
					silkCost = 0.0;
					zariCost = 0.0;
					tanaCharges = 0.0;
					totaSilkCost = 0.0;
					totaZariCost = 0.0;
					totalSilkWastageCost = 0.0;
					// costPrice = 0.0;
					margin = 0.0;
					purchasePrice = 0.0;
					purchasePriceRoundOff = 0.0;
					productPurchasePrice = 0.0;
				} else {
					silkCost = productCostingMaster.getSilkCost();
					zariCost = productCostingMaster.getJariCost();
					updateProductCost();
					/*
					 * totaSilkCost = silkCost*societyProductAppraisal.getSilkWeight(); totaZariCost
					 * = zariCost*societyProductAppraisal.getZariWeight(); totalSilkWastageCost =
					 * silkCost*societyProductAppraisal.getSilkWastageWeight();
					 */
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while updateProductCasting ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends SocietyProductAppraisalBean.updateProductCasting ===========>");
	}

	public void updateTanaCharges() {
		log.info("updateTanaCharges called..." + tanaCharges);
		costPrice = totaZariCost + totaSilkCost + totalSilkWastageCost;
		log.info("updateTanaCharges ends..." + costPrice);
	}

	public void updateProfitMargin() {
		log.info("updateProfitMargin called..." + costPrice + "\t margin::::" + margin);

		if (margin == null)
			margin = 0.0;

		if (costPrice == null)
			costPrice = 0.0;

		if (costPrice != null && margin != null) {
			purchasePrice = costPrice * margin / 100;
			log.info("purchasePrice ::" + purchasePrice);
		}

		purchasePriceRoundOff = (double) Math.round(purchasePrice);
		productPurchasePrice = purchasePriceRoundOff;
		log.info("purchasePriceRoundOff ::::" + purchasePriceRoundOff + "\t purchasePriceRoundOff:::"
				+ purchasePriceRoundOff + "\t purchasePrice:::" + purchasePrice);
		societyProductAppraisal.setProductCost(purchasePriceRoundOff);
		log.info("updateProfitMargin ends..." + costPrice);
	}

	public void updateProductCost() {
		log.info("updateProductCost called..." + tanaCharges + "\t productCostingMaster::" + productCostingMaster);

		if (productCostingMaster != null) {
			if (margin == null)
				margin = 0.0;
			if (costPrice == null)
				costPrice = 0.0;
			if (tanaCharges == null)
				tanaCharges = 0.0;

			if (societyProductAppraisal.getSilkWeight() == null) {
				societyProductAppraisal.setSilkWeight(0.0);
			}
			if (societyProductAppraisal.getZariWeight() == null) {
				societyProductAppraisal.setZariWeight(0.0);
			}
			if (societyProductAppraisal.getSilkWastageWeight() == null) {
				societyProductAppraisal.setSilkWastageWeight(0.0);
			}
			log.info("updateProductCost :: silkCost==> " + silkCost);
			log.info("updateProductCost :: silkWeight==> " + societyProductAppraisal.getSilkWeight());

			if (silkCost == 0.0) {
				totaSilkCost = societyProductAppraisal.getSilkWeight();
			} else {
				totaSilkCost = silkCost * societyProductAppraisal.getSilkWeight();
			}
			log.info("updateProductCost :: totaSilkCost==> " + totaSilkCost);
			if (zariCost == 0.0) {
				totaZariCost = societyProductAppraisal.getZariWeight();
			} else {
				totaZariCost = zariCost * societyProductAppraisal.getZariWeight();
			}
			log.info("updateProductCost :: totaZariCost==> " + totaZariCost);
			totalSilkWastageCost = silkCost * societyProductAppraisal.getSilkWastageWeight();

			log.info("updateProductCost :: totalSilkWastageCost==> " + totalSilkWastageCost);

			costPrice = totaZariCost + totaSilkCost + totalSilkWastageCost + tanaCharges;

			log.info("updateProductCost :: costPrice==> " + costPrice);

			purchasePrice = costPrice + (costPrice * margin / 100);
			log.info("updateProductCost :: purchasePrice==> " + purchasePrice);

			purchasePriceRoundOff = (double) Math.round(purchasePrice);
			log.info("updateProductCost :: purchasePriceRoundOff==> " + purchasePriceRoundOff);
			productPurchasePrice = purchasePriceRoundOff;
			log.info("updateProductCost :: productPurchasePrice==> " + productPurchasePrice);
			log.info("purchasePriceRoundOff ::::" + purchasePriceRoundOff + "\t purchasePriceRoundOff:::"
					+ purchasePriceRoundOff + "\t purchasePrice:::" + purchasePrice);

			societyProductAppraisal.setProductCost(purchasePriceRoundOff);
		}
		log.info("updateProductCost ends..." + costPrice + "societyProductAppraisal productCost::::"
				+ societyProductAppraisal.getProductCost());
	}

	public void submit() {
		log.info("Productcosting submit called...");
		if (margin == null || margin == 0.00) {
			AppUtil.addError("Profit margin should not be empty");
			return;
		}
		updateProductCost();
		productCostDetailsFlag=true;
		RequestContext.getCurrentInstance().update("productCost cost");
		RequestContext.getCurrentInstance().execute("PF('view_ind_cost_prep').hide()");
	}
}
