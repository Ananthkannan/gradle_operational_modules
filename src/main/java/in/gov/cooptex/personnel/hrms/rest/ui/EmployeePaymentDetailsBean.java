package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.CadreMaster;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EmployeePayDetails;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeePaymentDetailsBean")
@Scope("session")
public class EmployeePaymentDetailsBean {

	ObjectMapper mapper;
	String jsonResponse;

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	HttpService httpService;
	@Autowired
	ErrorMap errorMap;
	@Autowired
	LoginBean loginBean;
	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean approveButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	EmployeePayDetails employeePayDetail;
	@Getter
	@Setter
	List<EmployeePayDetails> employeePayDetailsList;

	@Getter
	@Setter
	LazyDataModel<EmployeePayDetails> lazyEmployeePayDetailsList;

	@Getter
	@Setter
	EmployeePayDetails selectedEmployeePayDetail;

	@Getter
	@Setter
	CadreMaster cadreMaster;
	@Getter
	@Setter
	List<CadreMaster> caderMasterList;

	@Getter
	@Setter
	EntityMaster hadOrRegionOffice;
	@Getter
	@Setter
	List<EntityMaster> hadOrRegionOfficeList;

	@Getter
	@Setter
	EmployeeMaster employee;
	@Getter
	@Setter
	List<EmployeeMaster> employeeList;

	@Getter
	@Setter
	Double basicPay;

	@Setter
	@Getter
	List<Designation> designationList;

	public String loadEmployeePaymentDetails() {
		log.info("<----Loading employee payment details---->");
		mapper = new ObjectMapper();
		action = "LIST";
		addButtonFlag = false;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		editButtonFlag = false;
		approveButtonFlag = false;
		employeePayDetailsList = new ArrayList<EmployeePayDetails>();
		cadreMaster = null;
		hadOrRegionOffice = null;
		employee = null;
		basicPay = null;
		employeePayDetail = new EmployeePayDetails();
		selectedEmployeePayDetail = new EmployeePayDetails();
		loadMasterValues();
		loadLazyEmployeePayDetails();
		return "/pages/personnelHR/payRoll/listemployeePayDetails.xhtml?faces-redirect=true";
	}

	public void loadLazyEmployeePayDetails() {
		log.info("<===== Starts EmployeePaymentDetailsBean.loadLazyEmployeePayDetails ======>");

		lazyEmployeePayDetailsList = new LazyDataModel<EmployeePayDetails>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<EmployeePayDetails> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/paydetails/getalllazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						employeePayDetailsList = mapper.readValue(jsonResponse,
								new TypeReference<List<EmployeePayDetails>>() {
								});
						this.setRowCount(response.getTotalRecords());
						setTotalRecords(response.getTotalRecords());
						//totalRecords = employeePayDetailsList.size();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return employeePayDetailsList;
			}

			@Override
			public Object getRowKey(EmployeePayDetails res) {
				return res != null ? res.getEmployee().getId() : null;
			}

			@Override
			public EmployeePayDetails getRowData(String rowKey) {
				for (EmployeePayDetails empPayDetail : employeePayDetailsList) {
					if (empPayDetail.getEmployee().getId().equals(Long.valueOf(rowKey))) {
						selectedEmployeePayDetail = empPayDetail;
						return empPayDetail;
					}
				}
				return null;
			}
		};
		log.info("<===== Ends CommunityMasterBean.loadLazyCommunityList ======>");
	}

	public String employeePayDetailsListAction() {
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("Loading employee payment details create page.....");
				employeePayDetailsList = new ArrayList<EmployeePayDetails>();
				cadreMaster = null;
				hadOrRegionOffice = null;
				employee = null;
				basicPay = null;
				employeePayDetail = new EmployeePayDetails();
				employeeList = new ArrayList<EmployeeMaster>();

				return "/pages/personnelHR/payRoll/createEmployeePayDetails.xhtml?faces-redirect=true";
			}

		} catch (Exception e) {
			log.error("Exception occured while action on list....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	public void loadEmployees() {

		try {
			
			employee = new EmployeeMaster();
			employeeList = new ArrayList<EmployeeMaster>();
			if (cadreMaster != null && hadOrRegionOffice != null && cadreMaster.getId() != null && hadOrRegionOffice.getId() != null) {
				
				log.info("Loading employees cadre id==>" + cadreMaster.getId());
				log.info("head and reg office id==>" + hadOrRegionOffice.getId());
				
				String url = SERVER_URL + "/employee/getallemployee/" + cadreMaster.getId() + "/"
						+ hadOrRegionOffice.getId();
				log.info("loadEmployees url==>"+url);
				BaseDTO response = httpService.get(url);
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContents());
					employeeList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeMaster>>() {
					});
				} else {
					errorMap.notify(response.getStatusCode());
					return;
				}
			}
		} catch (Exception e) {
			log.error("Exception occured while loading employee....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}

	}

	public void generateEmployeePayDetails() {
		log.info("Generating employee payment details.....");
		try {
			log.info("Cadre id: " + cadreMaster.getId() + " Employee id: " + employee.getId() + " Amount: " + basicPay);
			BaseDTO response = httpService
					.get(SERVER_URL + "/paydetails/generate/" + employee.getId() + "/" + cadreMaster.getId());
			if (response.getStatusCode() == 93005) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());
				employeePayDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeePayDetails>>() {
				});
				log.info("Employee payment details list size--->" + employeePayDetailsList.size());
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(response.getStatusCode());
				return;
			}
		} catch (Exception e) {
			log.info("Error while generating employee payment details", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
	}

	public void clearGeneration() {
		log.info("Clearing genration.....");
		employeePayDetailsList = new ArrayList<EmployeePayDetails>();
		employeeList = new ArrayList<EmployeeMaster>();
		cadreMaster = null;
		hadOrRegionOffice = null;
		employee = null;
		basicPay = null;
	}

	public String saveEmployeePaymentDetails() {
		log.info("Submitting employee payment details....");
		try {
			if (employeePayDetailsList.size() == 0 || employeePayDetailsList == null) {
				errorMap.notify(ErrorDescription.GENERATE_EMPLOYEE_PAY_DETAILS.getErrorCode());
				return null;
			} else {
				log.info("Employee payment details list size--->" + employeePayDetailsList.size());
				employeePayDetail.setEmployeePayDetails(employeePayDetailsList);
				BaseDTO response = httpService.post(SERVER_URL + "/paydetails/add", employeePayDetail);
				if (response.getStatusCode() == 0) {
					//errorMap.notify(response.getStatusCode());
					AppUtil.addInfo("Employee Payment Details Saved Successfully");
					return loadEmployeePaymentDetails();
				} else {
					errorMap.notify(response.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.info("Error while generating employee payment details", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	public void loadMasterValues() {
		hadOrRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
		caderMasterList = commonDataService.loadCadreMaster();
		designationList = commonDataService.loadDesignation();
	}

	public String viewEmployeePayDetail() {
		try {
			if (selectedEmployeePayDetail == null)
				AppUtil.addWarn("Please select one employee");
			BaseDTO response = httpService
					.get(SERVER_URL + "/paydetails/get/" + selectedEmployeePayDetail.getEmployee().getId());
			if (response == null) {
				errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
				return null;
			}
			if (Long.valueOf(93007).equals(Long.valueOf(response.getStatusCode()))) {
				mapper = new ObjectMapper();
				jsonResponse = mapper.writeValueAsString(response.getResponseContents());

				employeePayDetailsList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeePayDetails>>() {
				});
				if (employeePayDetailsList != null && employeePayDetailsList.size() > 0) {
					employeePayDetail = employeePayDetailsList.get(0);
				} else {
					errorMap.notify(ErrorDescription.NO_DETAILS_FOUND_FOR_THIS_EMPLOYEE.getErrorCode());
					return null;
				}
				return "/pages/personnelHR/payRoll/viewEmployeePayDetails.xhtml?faces-redirect=true";

			} else {
				errorMap.notify(response.getStatusCode());
				return null;
			}
		} catch (Exception e) {
			log.error("Exception occurred while view Employee Pay detail...", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		return null;
	}

	public String cancel() {
		log.info("Cancel Button clicked.............");
		employeePayDetail = new EmployeePayDetails();
		employeePayDetailsList = new ArrayList<>();
		return loadEmployeePaymentDetails();
	}
}
