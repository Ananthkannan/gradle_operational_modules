package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.jfree.util.Log;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;

import commonDataService.AppConfigKey;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.enums.LeaveTravelConCessionStatus;
import in.gov.cooptex.core.accounts.model.HolidayMaster;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.LeaveTravelConcessionDto;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmpLtcHolidayDates;
import in.gov.cooptex.core.model.EmployeeFamilyDetails;
import in.gov.cooptex.core.model.EmployeeLeaveTravelConcession;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.LeaveRequest;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.model.RelationshipMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.operation.dto.EmployeeLTCCommentDTO;
import in.gov.cooptex.operation.dto.EmployeeLeaveTravelConcessionHolidayDatesDto;
import in.gov.cooptex.operation.exceptions.UIException;
import in.gov.cooptex.personnel.hrms.rest.ui.LeaveRequestBean.ActionTypeEnum;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("employeeHRAddLtcBean")
public class EmployeeHRAddLTCBean extends CommonBean {

	public static final String LTC_URL = AppUtil.getPortalServerURL() + "/leavetravelconcession/ltc";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	String url;

	private final String LIST_URL = "/pages/personnelHR/listLeaveTravelConcession.xhtml?faces-redirect=true;";
	private final String ADD_URL = "/pages/personnelHR/createLeaveTravelConcession.xhtml?faces-redirect=true;";
	private final String VIEW_URL = "/pages/personnelHR/viewLeaveTravelConcession.xhtml?faces-redirect=true;";
	private final String BILL_URL = "/pages/personnelHR/addLTCBills.xhtml?faces-redirect=true;";

	@Autowired
	ErrorMap errorMap;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	String pageAction, fileName;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	EmployeeMaster employeeMaster = new EmployeeMaster();

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	EmployeeLeaveTravelConcession employeeLeaveTravelConcession;

	@Getter
	@Setter
	String action, blockYears, appliedFor = "Self", travelMode;

	@Getter
	@Setter
	List<String> blockYearsList;

	@Getter
	@Setter
	Boolean addMember, isPermission = false, holidayPermission, addButton, viewButton, editButton, billButton,
			deleteButton, submitButton = true;

	@Getter
	@Setter
	Boolean billActionBtn = false;

	@Getter
	@Setter
	LeaveTypeMaster leaveTypeMaster;

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeFamilyDetails> employeeFamilyList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeFamilyDetails employeeFamily;

	@Getter
	@Setter
	List<EmployeeFamilyDetails> selectedFamilyDetails = new ArrayList<>();

	@Getter
	@Setter
	List<RelationshipMaster> relationshipMasterList = new ArrayList<>();

	@Getter
	@Setter
	RelationshipMaster selectedRelation;

	@Getter
	@Setter
	List<String> memberIds = new ArrayList<>();

	@Setter
	@Getter
	private List<String> fileList = new ArrayList<>();

	@Getter
	@Setter
	UploadedFile uploadedFile;

	long letterSize;

	long documentSize;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	List<String> fromdateToDateList = new ArrayList<>();

	@Getter
	@Setter
	LeaveRequest leaveDetail = null;

	String json;

	@Getter
	@Setter
	LazyDataModel<EmployeeLeaveTravelConcession> ltcDetailsList;

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	EmployeeLeaveTravelConcession selectedLTCDetails;

	@Getter
	@Setter
	LeaveTravelConcessionDto selectedLTCDId;

	@Getter
	@Setter
	LeaveTravelConcessionDto ltcDetails;

	@Getter
	@Setter
	EmployeeLeaveTravelConcession selectedLTCDetailss;

	@Getter
	@Setter
	int totalSize = 0;

	@Getter
	@Setter
	List<EntityMaster> entityMasterList;

	@Getter
	@Setter
	EntityMaster selectedHoRo;

	@Getter
	@Setter
	EntityTypeMaster selectedEntityTypeMaster;

	@Getter
	@Setter
	EntityMaster slectedEntity;

	@Getter
	@Setter
	List<EntityTypeMaster> entitytypelistByHeadAndRegionalId = new ArrayList<>();

	@Getter
	@Setter
	List<EntityMaster> entitylistByHeadAndRegionalIdAndEntityTypeId = new ArrayList<>();

	@Getter
	@Setter
	List<Department> departmentList;

	@Getter
	@Setter
	Department selectedDepartement;

	@Getter
	@Setter
	SectionMaster selectedSection;

	@Getter
	@Setter
	List<SectionMaster> sectionMasterListBasedOnDepatementID = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterBasedOnEntityList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterBasedOnSectionList = new ArrayList<>();

	@Getter
	@Setter
	List<FinancialYear> blockYearListFromFinacialYear = new ArrayList<>();

	@Getter
	@Setter
	Boolean departementFlag = true;

	@Getter
	@Setter
	Boolean entityEmployeeFlag = false;

	@Getter
	@Setter
	EmployeeMaster employeeMasterBasedOnEntityId = new EmployeeMaster();

	@Getter
	@Setter
	EmployeeMaster employeeMasterBasedOnSectionId = new EmployeeMaster();

	@Getter
	@Setter
	FinancialYear selectedBlocKYear;

	@Getter
	@Setter
	Long blocKYear;

	@Getter
	@Setter
	List<EmpLtcHolidayDates> empLtcHolidayDatesList = new ArrayList<EmpLtcHolidayDates>();

	@Getter
	@Setter
	List<EmployeeLeaveTravelConcessionHolidayDatesDto> empLTCHolidaysDateList = new ArrayList<EmployeeLeaveTravelConcessionHolidayDatesDto>();

	@Getter
	@Setter
	EmployeeLeaveTravelConcessionHolidayDatesDto empLTCHolidaysDate;

	@Getter
	@Setter
	EmpLtcHolidayDates empLtcHolidayDates;

	@Getter
	@Setter
	List<HolidayMaster> holidayListInBetweenEmployeeLeaveDates = new ArrayList<HolidayMaster>();

	@Getter
	@Setter
	Boolean holidayFlag = true;

	@Getter
	@Setter
	List<UserMaster> leaveTravelConcessionForwardToList;

	@Getter
	@Setter
	LeaveTravelConcessionDto leaveTravelConcessionDto;

	@Getter
	@Setter
	List<LeaveTravelConcessionDto> listPageEmployeeLtclist = new ArrayList<LeaveTravelConcessionDto>();

	@Getter
	@Setter
	LazyDataModel<LeaveTravelConcessionDto> empLTClazyLoadReport;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	private Boolean previousApproval = false;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean billApprovalFlag = false;

	@Getter
	@Setter
	Boolean buttonFlag = false;

	@Getter
	@Setter
	private Boolean finalApproveFlag = false;

	@Getter
	@Setter
	Double eligibleDistance;

	@Getter
	@Setter
	Double traveledDistance;

	@Getter
	@Setter
	Double availedDis;

	@Setter
	@Getter
	List<String> ltcBillList;

	@Setter
	@Getter
	List<String> billsList;

	@Getter
	@Setter
	List<EmployeeLTCCommentDTO> approveCommentList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeLTCCommentDTO userReplyComment = new EmployeeLTCCommentDTO();

	@Getter
	@Setter
	List<EmployeeLTCCommentDTO> rejectAndReplyCommentList = new ArrayList<>();

	@Getter
	@Setter
	Map<Long, Set<EmployeeLTCCommentDTO>> approvedCommentMap = new LinkedHashMap<Long, Set<EmployeeLTCCommentDTO>>();

	@Getter
	@Setter
	Map<Long, Set<EmployeeLTCCommentDTO>> rejectedCommentMap = new LinkedHashMap<Long, Set<EmployeeLTCCommentDTO>>();

	@Getter
	@Setter
	List<EmployeeLTCCommentDTO> staticReplyCommentList = new ArrayList<>();

	@Getter
	@Setter
	boolean enableFlag = false;

	@Getter
	@Setter
	boolean showApprovedTable = false;

	@Getter
	@Setter
	boolean showRejectTable = false;

	@Getter
	@Setter
	boolean showReplyBox = true;

	@Getter
	@Setter
	String commentName;

	@Getter
	@Setter
	Boolean commentShow = false;

	@Getter
	@Setter
	Date currentDate = new Date();

	@Getter
	@Setter
	List<String> holidaysList;

	@Getter
	@Setter
	String appliedTotalYear;

	@Getter
	@Setter
	String applyYearDifference;

	@Getter
	@Setter
	String appliedNoOfTimes;

	@Getter
	@Setter
	EmployeeMaster loginEmployee;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	Boolean currentStatus = false;

	@Getter
	@Setter
	String dependantName;

	@Getter
	@Setter
	Long age;

	@Getter
	@Setter
	UserMaster userMaster;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@PostConstruct
	public void init() {
		submitButton = true;
		addButton = false;
		editButton = true;
		viewButton = true;
		finalApproveFlag = false;
		buttonFlag = false;
		billButton = false; // This and delete hide and show, other btn will disabled.
		deleteButton = false;
		loadLazyEmployeeLeaveTravelConcessionList();
		log.info("Init is executed.....................");
		employeeMaster = new EmployeeMaster();
		loadLtcValues();
		loadAppConfigValues();
		entityMasterList = commonDataService.loadHeadAndRegionalOffice();
		loginEmployee = commonDataService.loadEmployeeLoggedInUserDetails();
		mapper = new ObjectMapper();
		selectedRelation = new RelationshipMaster();
		employeeFamily = new EmployeeFamilyDetails();
		empLTCHolidaysDate = new EmployeeLeaveTravelConcessionHolidayDatesDto();
		departementFlag = true;
		entityEmployeeFlag = false;
		addMember = false;
		employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
		empLtcHolidayDates = new EmpLtcHolidayDates();
		empLtcHolidayDatesList = new ArrayList<>();
		leaveTravelConcessionDto = new LeaveTravelConcessionDto();
		employeeFamilyList = new ArrayList<>();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		leaveTravelConcessionForwardToList = commonDataService
				.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
		log.info("leaveTravelConcessionForwardToList" + leaveTravelConcessionForwardToList);
		loadAllDepartement();
		loadFamilyRelationshipFromRelationshipMaster();
		loadLeaveTypeMaster();
		loadBlockYearsFromFinancialYearTable();
		String availedDiss = commonDataService.getAppKeyValue("LTC_DISTANCE");
		appliedTotalYear = commonDataService.getAppKeyValue("LTC_TOTAL_YEAR");
		appliedNoOfTimes = commonDataService.getAppKeyValue("LTC_NO_OF_TIMES");
		applyYearDifference = commonDataService.getAppKeyValue("LTC_YEAR_DIFFERENCE");

		for (int i = 0; i < 1; i++) {
			String s = new String();
			fileList.add(s);
		}

		availedDis = AppUtil.stringToDouble(availedDiss);

		log.info("<<<-- appliedTotalYear..." + appliedTotalYear);
		log.info("<<<-- appliedNoOfTimes..." + appliedNoOfTimes);
		log.info("<<<-- applyYearDifference..." + applyYearDifference);
		log.info("<<<<---aviledDis..." + availedDiss);
	}

	/**
	 * 
	 */
	private void loadAppConfigValues() {
		try {
			documentSize = Long.valueOf(commonDataService.getAppKeyValue(AppConfigKey.EMPLOYEE_PHOTO_SIZE));
		} catch (Exception ex) {
			log.error("Exception at loadAppConfigValues() ", ex);
		}

	}

	public String listPage() {
		clear();

		loadLazyEmployeeLeaveTravelConcessionList();
		return LIST_URL;
	}

	/**
	 * BillButton and Delete button was hide and show.Using Rendered. Add and edit
	 * button was disabled.
	 **/

	public String clear() {
		log.info("clear methos called:::");
		addButton = false;
		editButton = true;
		viewButton = true;
		billButton = false;
		deleteButton = false;
		finalApproveFlag = false;
		buttonFlag = false;
		selectedHoRo = null;
		selectedEntityTypeMaster = null;
		slectedEntity = null;
		selectedDepartement = null;
		selectedSection = null;
		employeeMasterBasedOnSectionId = null;
		employeeMasterBasedOnEntityId = null;
		employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
		selectedBlocKYear = null;
		leaveTypeMaster = null;
		leaveDetail = null;
		fileName = null;
		isPermission = null;
		empLtcHolidayDatesList = null;
		employeeFamilyList = null;
		appliedFor = null;
		travelMode = null;
		selectedLTCDetails = new EmployeeLeaveTravelConcession();
		selectedLTCDId = new LeaveTravelConcessionDto();
		leaveTravelConcessionDto = new LeaveTravelConcessionDto();

		// Chat
		showApprovedTable = false;
		approvedCommentMap = null;
		showRejectTable = false;
		rejectedCommentMap = null;
		showReplyBox = false;
		userReplyComment = null;
		rejectAndReplyCommentList = null;
		approveCommentList = null;

		submitButton = true;
		departementFlag = true;
		entityEmployeeFlag = false;
		addMember = false;

		return LIST_URL;
	}

	public void loadAllEntityTypesByRegionId() {
		log.info("<<<<---start loadAllEntityTypesByRegionId");
		departementFlag = true;
		entityEmployeeFlag = false;

		if (entitytypelistByHeadAndRegionalId != null) {
			entitytypelistByHeadAndRegionalId.clear();
			selectedEntityTypeMaster = new EntityTypeMaster();
		}
		if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
			entitylistByHeadAndRegionalIdAndEntityTypeId.clear();

		}
		/*
		 * if (departmentList != null) { departmentList.clear();
		 * 
		 * }
		 */
		/*
		 * if (sectionMasterListBasedOnDepatementID != null) {
		 * sectionMasterListBasedOnDepatementID.clear(); } if
		 * (employeeMasterBasedOnEntityList != null) {
		 * employeeMasterBasedOnEntityList.clear(); } if
		 * (employeeMasterBasedOnSectionList != null) {
		 * employeeMasterBasedOnSectionList.clear(); }
		 */

		try {

			if (selectedHoRo != null) {
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ selectedHoRo.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					entitytypelistByHeadAndRegionalId = mapper.readValue(jsonValue,
							new TypeReference<List<EntityTypeMaster>>() {
							});

					log.info("entitytypelistByHeadAndRegionalId list size is "
							+ entitytypelistByHeadAndRegionalId.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---end loadAllEntityTypesByRegionId");
	}

	public void loadAllEntityByRegionIdandEntityTypeId() {
		log.info("<<<<---start loadAllEntityByRegionIdandEntityTypeId");
		if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
			entitylistByHeadAndRegionalIdAndEntityTypeId.clear();
			slectedEntity = new EntityMaster();

		}
		/*
		 * if (departmentList != null) { departmentList.clear();
		 * 
		 * }
		 */
		/*
		 * if (sectionMasterListBasedOnDepatementID != null) {
		 * sectionMasterListBasedOnDepatementID.clear(); } if
		 * (employeeMasterBasedOnEntityList != null) {
		 * employeeMasterBasedOnEntityList.clear(); } if
		 * (employeeMasterBasedOnSectionList != null) {
		 * employeeMasterBasedOnSectionList.clear(); }
		 */

		try {

			if (selectedHoRo != null && selectedEntityTypeMaster != null) {
				Long regionId = selectedHoRo.getId();
				Long entityTypeId = selectedEntityTypeMaster.getId();
				String entityTypeName = selectedEntityTypeMaster.getEntityName();
				log.info("the selected entity name is" + " " + entityTypeName);

				if (entityTypeName.equalsIgnoreCase("Showroom")) {
					log.info("user selected the Showroom ");
					departementFlag = false;
					entityEmployeeFlag = true;
				} else {
					departementFlag = true;
					entityEmployeeFlag = false;
				}

				entitylistByHeadAndRegionalIdAndEntityTypeId = commonDataService
						.loadEntityByregionOrentityTypeId(regionId, entityTypeId);

				log.info("entitylistByHeadAndRegionalIdAndEntityTypeId List size is" + " "
						+ entitylistByHeadAndRegionalIdAndEntityTypeId.size());

				if (entitylistByHeadAndRegionalIdAndEntityTypeId != null) {
					for (EntityMaster entity : entitylistByHeadAndRegionalIdAndEntityTypeId) {
						log.info("the WorkLocation Id" + " " + entity.getId());
					}
				}
			}

		} catch (Exception e) {
			log.info("The cause for Exception is" + " " + e.getCause());
		}
		log.info("<<<<---end loadAllEntityByRegionIdandEntityTypeId");
	}

	public void loadAllDepartementOREmployeBaseOnDepartementFlag() {
		log.info("<<<<---start loadAllDepartementOREmployeBaseOnDepartementFlag");
		if (departementFlag) {
			log.info("<<<<---inside the departementFlag");
			loadAllDepartement();
		} else {
			loadAllEmployeBasedOnEntityId();
		}
		log.info("<<<<---end loadAllDepartementOREmployeBaseOnDepartementFlag");
	}

	public void loadAllDepartement() {
		log.info("<<<<---start loadAllDepartement");
		/*
		 * if (departmentList != null) { departmentList.clear(); selectedDepartement =
		 * new Department();
		 * 
		 * } if (sectionMasterListBasedOnDepatementID != null) {
		 * sectionMasterListBasedOnDepatementID.clear(); } if
		 * (employeeMasterBasedOnEntityList != null) {
		 * employeeMasterBasedOnEntityList.clear(); } if
		 * (employeeMasterBasedOnSectionList != null) {
		 * employeeMasterBasedOnSectionList.clear(); }
		 */
		try {
			departmentList = commonDataService.getDepartment();
			log.info("the list returned form data base size is" + " " + departmentList.size());
		} catch (Exception e) {
			log.info("the cause of the exception is " + " " + e.getCause());
		}
		log.info("<<<<---end loadAllDepartement");
	}

	public void loadAllEmployeBasedOnEntityId() {
		log.info("<<<<---start loadAllEmployeBasedOnEntityId...");
		try {
			log.info("selected entity id is" + " " + slectedEntity.getId());
			if (slectedEntity != null) {
				String url = SERVER_URL + "/employeeprofile/getEmployeListBaseOnEntityId/" + slectedEntity.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterBasedOnEntityList = mapper.readValue(jsonValue,
							new TypeReference<List<EmployeeMaster>>() {
							});

					log.info("employeeMasterBasedOnEntityList list size is " + employeeMasterBasedOnEntityList.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---start loadAllEmployeBasedOnEntityId...");
	}

	public void loadAllSectionBasedOnDepartementId() {
		log.info("<<<<---start loadAllSectionBasedOnDepartementId...");
		/*
		 * if (sectionMasterListBasedOnDepatementID != null) {
		 * sectionMasterListBasedOnDepatementID.clear(); selectedSection = new
		 * SectionMaster(); } if (employeeMasterBasedOnSectionList != null) {
		 * employeeMasterBasedOnSectionList.clear(); }
		 */

		log.info("inside loadAllSectionBasedOnDepartementId ");
		// employeeMasterBasedOnSectionList.clear();

		try {

			log.info("Department name==> " + selectedDepartement.getName());
			String url = SERVER_URL + "/employeeprofile/getSectionListBasedOnDepartementId/"
					+ selectedDepartement.getName();
			log.info("url==> " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				sectionMasterListBasedOnDepatementID = mapper.readValue(jsonValue,
						new TypeReference<List<SectionMaster>>() {
						});
				if (sectionMasterListBasedOnDepatementID != null || !sectionMasterListBasedOnDepatementID.isEmpty()) {
					log.info("sectionMasterListBasedOnDepatementID list size is "
							+ sectionMasterListBasedOnDepatementID.size());
				}
			}
			// }

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---end loadAllSectionBasedOnDepartementId...");
	}

	public void loadAllEmploymasaterBasedOnSectionId() {
		log.info("<<<<---start loadAllEmploymasaterBasedOnSectionId...");
		if (employeeMasterBasedOnSectionList != null) {
			employeeMasterBasedOnSectionList.clear();
			employeeMasterBasedOnSectionId = new EmployeeMaster();
		}

		try {
			log.info("the selected section is" + " " + selectedSection);
			if (selectedHoRo != null) {
				String url = SERVER_URL + "/employeeprofile/getEmployeListBaseOnSectionId/" + selectedSection.getId();
				BaseDTO baseDTO = httpService.get(url);

				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					employeeMasterBasedOnSectionList = mapper.readValue(jsonValue,
							new TypeReference<List<EmployeeMaster>>() {
							});

					log.info(
							"employeeMasterBasedOnSectionList list size is " + employeeMasterBasedOnSectionList.size());
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
			log.info("the cause of Error is" + " " + e.getMessage());
		}
		log.info("<<<<---end loadAllEmploymasaterBasedOnSectionId...");
	}

	public void selectBlockYearsFromFinancialYearTable() {
		log.info("<<<<---end selectBlockYearsFromFinancialYearTable...");
		try {
			if (blocKYear != null) {
				String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
				BaseDTO baseDTO = httpService.get(url);
				List<FinancialYear> blockYearList = new ArrayList<>();
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					blockYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
					});

					log.info("blockYearList list size is " + blockYearList.size());

					if (blockYearList.size() > 0) {

						for (FinancialYear yearObject : blockYearList) {
							FinancialYear endYear = new FinancialYear();

							Long year = yearObject.getStartYear() + 4;
							log.info("the end year is" + " " + yearObject.getStartYear() + " -" + year);
							endYear.setId(yearObject.getId());
							endYear.setStartYear(yearObject.getStartYear());
							endYear.setEndYear(year);
							if (yearObject.getStartYear().equals(blocKYear)) {
								log.info("selectd block year:.." + endYear);
								selectedBlocKYear = endYear;
								break;
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---end selectBlockYearsFromFinancialYearTable...");
	}

	public void loadBlockYearsFromFinancialYearTable() {
		log.info("<<<<---start loadBlockYearsFromFinancialYearTable...");
		try {
			if (blockYearListFromFinacialYear.size() > 0) {
				blockYearListFromFinacialYear.clear();
			}
			selectedBlocKYear = new FinancialYear();
			if (selectedHoRo != null) {

				String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
				BaseDTO baseDTO = httpService.get(url);
				List<FinancialYear> blockYearList = new ArrayList<>();
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
					blockYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
					});

					log.info("blockYearList list size is " + blockYearList.size());

					if (blockYearList.size() > 0) {

						for (FinancialYear yearObject : blockYearList) {
							FinancialYear endYear = new FinancialYear();
							Long year = yearObject.getStartYear() + 4;
							log.info("the end year is" + " " + yearObject.getStartYear() + " -" + year);
							endYear.setId(yearObject.getId());
							endYear.setStartYear(yearObject.getStartYear());
							endYear.setEndYear(year);
							blockYearListFromFinacialYear.add(endYear);
						}
					}
				}
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<<<<---end loadBlockYearsFromFinancialYearTable...");
	}

	public void loadLeaveTypeMaster() {
		log.info("<<<<---start loadLeaveTypeMaster...");
		try {
			String url = SERVER_URL + "/leavetype/ltcleave";
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				leaveTypeMasterList = mapper.readValue(jsonValue, new TypeReference<List<LeaveTypeMaster>>() {
				});

				log.info("leaveTypeMasterList list size is " + leaveTypeMasterList.size());
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
			log.info("the cause of Error is" + " " + e.getMessage());
		}
		log.info("<<<<---end loadLeaveTypeMaster...");
	}

	public String showPage() {
		log.info("showPage action ::::::" + action);
		selectedFamilyDetails = new ArrayList<>();
		if (action.equalsIgnoreCase("add")) {
			employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			memberIds = new ArrayList<>();
			ltcDetails = new LeaveTravelConcessionDto();
			leaveTravelConcessionDto = new LeaveTravelConcessionDto();
			selectedFamilyDetails = new ArrayList<>();
			fromdateToDateList.clear();
			employeeFamilyList = new ArrayList<>();
			empLtcHolidayDatesList = new ArrayList<>();
			appliedFor = "Self";
			isPermission = false;
			userMaster = new UserMaster();
			leaveDetail = new LeaveRequest();
			return ADD_URL;
		}
		if (action.equalsIgnoreCase("edit")) {
			if (selectedLTCDId == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			if (selectedLTCDId.getAppliedFor() != null && "Family".equalsIgnoreCase(selectedLTCDId.getAppliedFor())) {
				addMember = true;
			}
			EmployeeLeaveTravelConcession employeeLeaveTravelConcession = getById(selectedLTCDId.getLeaveTravelId());
			leaveTravelConcessionForwardToList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
			employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
			loadFamilyMembersView();
			log.info("employeeLeaveTravelConcession::::::::::::::::" + employeeLeaveTravelConcession);
			if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getHolidayDates() != null) {
				String holidayDate = employeeLeaveTravelConcession.getHolidayDates();
				holidaysList = Arrays.asList(holidayDate.split("\\s*,\\s*"));
			}

			setPreviousApproval(leaveTravelConcessionDto.getForwardFor());

			return ADD_URL;
		}
		if (action.equalsIgnoreCase("view") || action.equalsIgnoreCase("billsAdd")) {
			userMaster = new UserMaster();
			if (selectedLTCDId == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}

			EmployeeLeaveTravelConcession employeeLeaveTravelConcession = getById(selectedLTCDId.getLeaveTravelId());
			setPreviousApproval(leaveTravelConcessionDto.getForwardFor());
			employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
			loadFamilyMembersView();
			leaveTravelConcessionForwardToList = commonDataService
					.loadForwardToUsersByFeature(AppFeatureEnum.LOAN_AND_ADVANCE.toString());
			log.info("employeeLeaveTravelConcession::::::::::::::::" + employeeLeaveTravelConcession);

			// getRejectedCommentsAndReplayCommentList();
			// getApprovedCommentsList();

			if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getHolidayDates() != null) {
				String holidayDate = employeeLeaveTravelConcession.getHolidayDates();
				holidaysList = Arrays.asList(holidayDate.split("\\s*,\\s*"));
			}

			if ("VIEW".equalsIgnoreCase(action) || action.equalsIgnoreCase("billsAdd")) {
				/*
				 * if (leaveTravelConcessionDto.getForwardTo() != null) { if
				 * (login.getUserMaster().getId().equals(leaveTravelConcessionDto.getForwardTo()
				 * )) {
				 * 
				 * } }
				 */

				previousApproval = leaveTravelConcessionDto.getForwardFor();
				// forWardTo=amountInfo.getPpsalesRequestNote().getForwardTo();
				stage = leaveTravelConcessionDto.getStatus();
				finalApproval = leaveTravelConcessionDto.getForwardFor();
//				loadForwardTo();

				if (leaveTravelConcessionDto.getForwardTo().equals(loginBean.getUserDetailSession().getId())) {
					log.info("====Approval Flag Condition Current User and ForwardTo User are Equal============");
					setApprovalFlag(true);
				} else {
					log.info(
							"=======Approval Flag Condition Current User and ForwardTo User are Not Equal===============");
					setApprovalFlag(false);
				}

				if (action.equalsIgnoreCase("billsAdd")
						&& stage.equals(LeaveTravelConCessionStatus.FINALAPPROVED.toString())) {
					setApprovalFlag(true);
					setPreviousApproval(false);
					setBillsubmitFlag(true);
				} else {
					setBillsubmitFlag(false);
				}

				leaveTravelConcessionDto.setRemarks(null);
			}
			if ("billsAdd".equalsIgnoreCase(action))
				return BILL_URL;
			else
				return VIEW_URL;
		}
		if (action.equalsIgnoreCase("cancel") || action.equalsIgnoreCase("list")) {
			loadBlockYears();
			addButton = false;
			editButton = true;
			viewButton = true;
			billButton = false; // This and delete hide and show, other btn will disabled.
			deleteButton = false;
			selectedLTCDetails = new EmployeeLeaveTravelConcession();
			selectedLTCDId = new LeaveTravelConcessionDto();
			return LIST_URL;
		}

		return null;
	}

	@Getter
	@Setter
	String stage;

	@Getter
	@Setter
	Boolean finalApproval;

	@Getter
	@Setter
	Boolean billsubmitFlag = false;

	public void loadFamilyMembersView() {
		log.info("<<<<---start loadFamilyMembersView...");
		try {
			log.info("loadFamilyMembersView called..." + employeeLeaveTravelConcession);
			if (employeeLeaveTravelConcession.getFamilyDetailsIds() != null) {
				String familyIds = employeeLeaveTravelConcession.getFamilyDetailsIds();
				log.info("familyIdsList ids" + familyIds);
				List<String> familyList = Stream.of(familyIds.split("/")).collect(Collectors.toList());
				employeeFamilyList = new ArrayList<>();
				if (familyList != null) {
					for (String obj : familyList) {
						log.info("final lst of Family IDs ::::" + obj);
						String[] family = obj.split("-");
						EmployeeFamilyDetails familyObj = new EmployeeFamilyDetails();
						String name = family[0] != null ? family[0] : "";
						Long famId = Long.parseLong((family[1] != null ? family[1] : ""));
						Long age = Long.parseLong((family[2] != null ? family[2] : ""));
						familyObj.setDependentName(name);
						for (RelationshipMaster relation : relationshipMasterList) {
							if (famId.equals(relation.getId())) {
								familyObj.setRelationshipMaster(relation);
								break;
							}
						}
						familyObj.setAge(age);
						employeeFamilyList.add(familyObj);
					}
					if (action.equalsIgnoreCase("view") || action.equalsIgnoreCase("billsAdd"))
						RequestContext.getCurrentInstance().execute("PF('viewMember').show();");
				}
			}
		} catch (Exception e) {
			log.info("error found in loading family members", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<<<<---end loadFamilyMembersView...");
	}

	public void loadFamilyMembers() {
		log.info("<=======Starts EmployeeLTCBean.loadFamilyMembers ======>");
		try {
			BaseDTO response = httpService.get(
					AppUtil.getPortalServerURL() + "/employee/insurance/getfamilydetails/" + employeeMaster.getId());
			if (response.getStatusCode() == 0) {
				json = mapper.writeValueAsString(response.getResponseContents());
				employeeFamilyList = mapper.readValue(json, new TypeReference<List<EmployeeFamilyDetails>>() {
				});
				RequestContext.getCurrentInstance().execute("PF('addMember').show();");
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in loadingFamilyMembers ::", e);
		}
		log.info("<=======Ends EmployeeLTCBean.loadFamilyMembers ======>" + employeeFamilyList.size());
	}

	public void loadLtcValues() {
		loadMonth();
		// loadLeaveTypeMaster();
		getByEmployeeId();
		loadLazyEmployeeLeaveTravelConcessionList();
	}

	public EmployeeLeaveTravelConcession getById(Long id) {
		submitButton = false;
		ltcDetails = new LeaveTravelConcessionDto();
		BaseDTO baseDto = new BaseDTO();
		try {
			url = LTC_URL + "/getLtcwithnotificationId/" + id + "/"
					+ (systemNotificationId != null ? systemNotificationId : 0);
			log.info("LTC Requested URL :::::" + url);
			baseDto = httpService.get(url);
			if (baseDto != null) {
				json = mapper.writeValueAsString(baseDto.getResponseContent());
				ltcDetails = mapper.readValue(json, LeaveTravelConcessionDto.class);
			}

			if (systemNotificationId != null) {
				systemNotificationBean.loadTotalMessages();
			}
			if (ltcDetails != null) {
				ltcBillList = new ArrayList<>();
				billsList = new ArrayList<>();
				log.info("employeeLeaveTravelConcession is.." + ltcDetails);
				employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
				employeeLeaveTravelConcession = ltcDetails.getEmployeeLeaveTravelConcession();
				ltcDetails.getEmployeeLeaveTravelConcessionLog();
				ltcDetails.getEmployeeLeaveTravelConcessionNote();
				leaveTravelConcessionDto = ltcDetails;
				leaveTravelConcessionDto.setLeaveTravelId(ltcDetails.getEmployeeLeaveTravelConcession().getId());
				leaveTravelConcessionDto.setStatus(ltcDetails.getEmployeeLeaveTravelConcessionLog().getStage());
				List<String> billsFile = ltcDetails.getFileList();
				List<String> billsFileList = new ArrayList<>();
				if (billsFile != null && billsFile.size() > 0) {
					log.info("inside file list loaded.....");
					setBillsList(billsFile);
					for (String uploadPath : billsFile) {
						if (uploadPath != null && !uploadPath.isEmpty()) {
							String filePathValue[] = uploadPath.split("uploaded/");
							if (filePathValue != null) {
								log.info("0==>" + filePathValue[0]);
								log.info("1==>" + filePathValue[1]);
								/*
								 * if(fileFinalList.length == 0) { fileFinalList[fileFinalList.length] =
								 * filePathValue[1]; }else { fileFinalList[fileSize] = filePathValue[1]; }
								 */
								billsFileList.add(filePathValue[1]);
							} else {
								log.error("Uploaded file not found");
							}
						}
					}

					setLtcBillList(billsFileList);
				} else {
					log.info("inside empty file load.....");
					String s = new String();
					ltcBillList.add(s);
					billsList.add(s);
					fileList.add(s);
				}

				if (employeeLeaveTravelConcession != null) {
					appliedFor = employeeLeaveTravelConcession.getAvailedFor();
					travelMode = employeeLeaveTravelConcession.getTravelMode();
					leaveTypeMaster = employeeLeaveTravelConcession.getLeaveTypeMaster();
					isPermission = employeeLeaveTravelConcession.getHolidayPermission();

					if (employeeLeaveTravelConcession.getEntityMaster() != null) {

						loadAllEntityTypesByRegionId();
						selectedEntityTypeMaster = employeeLeaveTravelConcession.getEntityMaster()
								.getEntityTypeMaster();
						log.info("selectedEntityTypeMaster........" + selectedEntityTypeMaster);
						loadAllEntityByRegionIdandEntityTypeId();
						log.info("selectedEntityTypeMaster........" + selectedEntityTypeMaster);
						selectedEntityTypeMaster = employeeLeaveTravelConcession.getEntityMaster()
								.getEntityTypeMaster();

						log.info("selected entity typeMaster and Ho/Fo is.." + selectedEntityTypeMaster + " and"
								+ selectedHoRo);

						slectedEntity = employeeLeaveTravelConcession.getEntityMaster();
						if (slectedEntity.getCode() != 10) {
							selectedHoRo = commonDataService
									.findRegionByShowroomId(employeeLeaveTravelConcession.getEntityMaster().getId());
						} else {
							selectedHoRo = slectedEntity;
						}
						log.info("HORO is...." + selectedHoRo.getName());
						log.info("region name is...." + slectedEntity.getName());
						log.info("region code is...." + slectedEntity.getCode());
					}
					/*
					 * if(employeeLeaveTravelConcession != null &&
					 * employeeLeaveTravelConcession.getEntityMaster() != null) { selectedHoRo =
					 * commonDataService
					 * .findRegionByShowroomId(employeeLeaveTravelConcession.getEntityMaster().getId
					 * ()); }
					 */
					if (employeeLeaveTravelConcession.getSectionMaster() != null) {
						selectedDepartement = employeeLeaveTravelConcession.getSectionMaster().getDepartment();
						loadAllDepartementOREmployeBaseOnDepartementFlag();
						setFromDetails();
						loadAllSectionBasedOnDepartementId();
						setFromDetails();
						loadAllEmploymasaterBasedOnSectionId();

						setFromDetails();
						loadBlockYearsFromFinancialYearTable();
						selectBlockYearsFromFinancialYearTable();
						log.info("selected entity typeMaster and Ho/Fo is.." + selectedEntityTypeMaster + " and"
								+ selectedHoRo);
						setFromDetails();
						loadFamilyMembersView();
						loadEmpHolidayList();
						onLeaveTypeChange();
					}
				}
			}
			systemNotificationBean.loadTotalMessages();
		} catch (Exception e) {
			log.error("exception occured in getById", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return employeeLeaveTravelConcession;
	}

	public void setFromDetails() {
		if (employeeLeaveTravelConcession.getSectionMaster() != null) {
			selectedDepartement = employeeLeaveTravelConcession.getSectionMaster().getDepartment();
			selectedSection = employeeLeaveTravelConcession.getSectionMaster();
			log.info("Selected department and section is.." + selectedDepartement + " and " + selectedSection);
			employeeMasterBasedOnSectionId = ltcDetails.getEmployeeMaster();
			employeeMaster = ltcDetails.getEmployeeMaster();
			log.info("employee id is...." + employeeMasterBasedOnSectionId);
			String block = employeeLeaveTravelConcession.getBlockYears();
			if (block != null) {
				String[] blk = block.split("-");
				String year = blk[0] != null ? blk[0] : "";
				if (!year.equals(""))
					blocKYear = Long.parseLong(year);
			}
			log.info("blockYear....." + blocKYear);
		}
	}

	/*
	 * public void onchangeAppliedFor() { log.info("onchangeAppliedFor called ..: "
	 * + appliedFor); addMember = false;
	 * 
	 * if (appliedFor.equalsIgnoreCase("Self")) { addMember = true;
	 * loadFamilyRelationshipFromRelationshipMaster(); } }
	 */

	public void onchangeAppliedForMehtod() {
		log.info("onchangeAppliedFor called ..: " + appliedFor);
		addMember = false;
		if (appliedFor.equalsIgnoreCase("Family")) {
			addMember = true;
			loadFamilyRelationshipFromRelationshipMaster();
			loadFamilyMembersView();
		} else {
			if (employeeFamilyList != null) {
				employeeFamilyList.clear();
				employeeFamily = new EmployeeFamilyDetails();
				selectedRelation = new RelationshipMaster();
			}

		}
	}

	public void loadEmpHolidayList() {
		log.info("<---- start getempHolidayList --->");
		try {
			String url = LTC_URL + "/getHolidayByLtc/" + selectedLTCDId.getLeaveTravelId();
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				empLtcHolidayDatesList = mapper.readValue(jsonValue, new TypeReference<List<EmpLtcHolidayDates>>() {
				});
				if (action.equalsIgnoreCase("view") || action.equalsIgnoreCase("billsAdd"))
					RequestContext.getCurrentInstance().execute("PF('viewHoliday').show();");

				log.info("empLtcHolidayDates list size is " + empLtcHolidayDatesList.size());
			}
		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}
		log.info("<---- End getempHolidayList --->");
	}

	public void loadEligibleDistance() { 
		log.info("<---- start loadEligibleDistance --->");
		List<EmployeeLeaveTravelConcession> ltcDetailsList = new ArrayList<>();
		Calendar cal = Calendar.getInstance();
		// int currentYear=cal.get(Calendar.YEAR);
		int currentYear = cal.get(Calendar.YEAR);
		int months = cal.get(Calendar.YEAR);
		float monthValue = months / 12;
		log.info("<<<--- currentYear.. " + currentYear);
		log.info("<<<<==== currentMonth..." + months);
		log.info("<<<<==== monthValue..." + monthValue);

		int applyTimes = (appliedNoOfTimes != null && !appliedNoOfTimes.equals("")) ? Integer.parseInt(appliedNoOfTimes)
				: 0;
		int totalYear = (appliedTotalYear != null && !appliedTotalYear.equals("")) ? Integer.parseInt(appliedTotalYear)
				: 0;
		float diffYear = (applyYearDifference != null && !applyYearDifference.equals(""))
				? Float.parseFloat(applyYearDifference)
				: 0;
		log.info("<<<<==== applyTimes..." + applyTimes);
		log.info("<<<<==== totalYear..." + totalYear);
		log.info("<<<<==== diffYear..." + diffYear);
		float afterApplyStartYear = 0F;
		float afterApplyEndYear = 0F;
		double singleTimeDistance = availedDis / diffYear;
		boolean currentEligibleApply = false;
		EmployeeLeaveTravelConcession lastAppliedLTC = new EmployeeLeaveTravelConcession();

		employeeLeaveTravelConcession.setEligibleDistance(0D);
		Long employeeMasterId = null;
		try {
			if (employeeMasterBasedOnSectionId != null) {
				employeeMasterId = employeeMasterBasedOnSectionId.getId();
			} else if (employeeMasterBasedOnEntityId != null) {
				employeeMasterId = employeeMasterBasedOnEntityId.getId();
			}
			if (employeeMasterId != null && selectedBlocKYear != null) {
				String blockYear = selectedBlocKYear.getStartYear() + "-" + selectedBlocKYear.getEndYear();
				log.info("<<<-- blockedYears-->" + blockYear);
				String url = LTC_URL + "/findEligibleDistance/" + employeeMasterId + "/" + blockYear;
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					traveledDistance = mapper.readValue(jsonValue, Double.class);

					String jsonValues = mapper.writeValueAsString(baseDTO.getResponseContents());
					ltcDetailsList = mapper.readValue(jsonValues,
							new TypeReference<List<EmployeeLeaveTravelConcession>>() {
							});

					if (ltcDetailsList == null) {
						employeeLeaveTravelConcession.setEligibleDistance(availedDis);
						submitButton = false;
					}

					if (ltcDetailsList.size() > 0 && ltcDetailsList != null) {
						int sYear = selectedBlocKYear.getStartYear().intValue();
						int eYear = selectedBlocKYear.getEndYear().intValue();
						log.info("<<<<<===== sYear-->" + sYear);
						log.info("<<<<<===== eYear-->" + eYear);

						int appliedTourSize = ltcDetailsList.size();
						log.info("<<<<--- appliedTourSize" + appliedTourSize);

						if (appliedTourSize != 0 && diffYear != 0) {
							lastAppliedLTC = ltcDetailsList.get(ltcDetailsList.size() - 1);
							double lastAppliedDistance = lastAppliedLTC.getTravelledDistance();
							double currentEligibleDistance = (appliedTourSize * singleTimeDistance) + 1;
							if (lastAppliedDistance < availedDis) {
								currentEligibleApply = true;
							}
							afterApplyStartYear = appliedTourSize * diffYear;
							afterApplyEndYear = (appliedTourSize + 1) * diffYear;
						}

						log.info("<<<<--- afterApplyStartYear " + afterApplyStartYear);
						log.info("<<<<--- afterApplyEndYear " + afterApplyEndYear);

						float satartBetweenYear = (afterApplyStartYear) + sYear;
						float endBetweenYear = (afterApplyEndYear) + sYear;

						log.info("<<<<--- satartBetweenYear " + satartBetweenYear);
						log.info("<<<<--- endBetweenYear " + endBetweenYear);

						if (eYear <= currentYear) {
							errorMap.notify(
									ErrorDescription.getError(AdminErrorCode.LTC_CROSS_TO_END_DATE).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							// throw new UIException("Time to End for applied Travel concession.");
						} else if (ltcDetailsList.size() >= applyTimes) {
							errorMap.notify(
									ErrorDescription.getError(AdminErrorCode.LTC_REACHED_MAX_ATTEMPTS).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							// throw new UIException("You already applied "+applyTimes+" times.You reached
							// max applied times to choosed year.");
						} else if (currentEligibleApply == false) {
							errorMap.notify(
									ErrorDescription.getError(AdminErrorCode.LTC_REACHED_MAX_KM).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							employeeLeaveTravelConcession.setTravelledDistance(0D);
							// throw new UIException("Your Already applied the max kilo meter for choosed
							// year.");
						} else if (applyTimes > 0 && (satartBetweenYear <= currentYear || endBetweenYear <= currentYear)
								&& currentEligibleApply) {

							if (traveledDistance != null && traveledDistance > 0) {
								eligibleDistance = (availedDis - traveledDistance);
								if (eligibleDistance > 0) {
									employeeLeaveTravelConcession.setEligibleDistance(eligibleDistance);
									submitButton = false;
								} else {
									employeeLeaveTravelConcession.setEligibleDistance(0D);
									submitButton = true;
								}
							}
						} else {
							errorMap.notify(
									ErrorDescription.getError(AdminErrorCode.LTC_ALREADY_APPLIED).getErrorCode());
							RequestContext.getCurrentInstance().update("growls");
							// throw new UIException("You already applied the current period. you con't
							// applied now.");
						}
					} else {
						employeeLeaveTravelConcession.setEligibleDistance(availedDis);
						submitButton = false;
					}

					log.info("Hole employee avalilable distance..." + availedDis);
					log.info("This employee travled distance was..." + traveledDistance);
					log.info("This employee eligible is..." + eligibleDistance);
				}
			}
		} catch (Exception e) {
			log.error("the cause of Error is" + e);
		}
		log.info("<---- End loadEligibleDistance --->");
	}

	public void calculateDistance() {
		if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getTravelledDistance() != null) {
			if (employeeLeaveTravelConcession.getEligibleDistance() < employeeLeaveTravelConcession
					.getTravelledDistance()) {
				log.error("Invalid Distance..... ");
				AppUtil.addWarn("Distance greator than eligible");
				employeeLeaveTravelConcession.setTravelledDistance(eligibleDistance);
				// throw new UIException("Distance greator than eligible");
			}
		}
	}

	public void loadFamilyRelationshipFromRelationshipMaster() {
		log.info("addMember" + " " + addMember);

		try {
			String url = AppUtil.getPortalServerURL() + "/employeeprofile/getAllFamilyRelationShip";
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				relationshipMasterList = mapper.readValue(jsonValue, new TypeReference<List<RelationshipMaster>>() {
				});

				log.info("relationshipMasterList list size is " + relationshipMasterList.size());
			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

	}

	public void FilterTheWeekEndsAndGovernmentHolidayFromEmployeeLeaveDates() {

		Calendar startCal = Calendar.getInstance();
		Date startDate = empLTCHolidaysDate.getFromDate();
		Date EndDate = empLTCHolidaysDate.getToDate();
		LocalDate dateStart = convertToLocalDateViaInstant(startDate);
		LocalDate dateEnd = convertToLocalDateViaInstant(EndDate);
		log.info("the dates in between");
		Log.info("the Start date is " + dateStart.toString());
		Log.info("the Start date is " + dateStart.getDayOfWeek().toString());
		Log.info("the End  date is " + EndDate.toString());

		String dayOfWeek = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(startDate);

		log.info("name of day is" + " " + dayOfWeek.toString());

		Long NumberOfDays = getDifferenceDays(startDate, EndDate);

		log.info("the number Days between the selected dates is" + " " + NumberOfDays);
		// startCal.setTime();

		int count = 0;
		for (LocalDate date = dateStart; date.isBefore(dateEnd); date = date.plusDays(1)) {
			log.info("the date" + date.getDayOfWeek());
			count = count + 1;
		}
		log.info("the number of day is " + " " + count);

	}

	public static long getDifferenceDays(Date d1, Date d2) {
		long diff = d2.getTime() - d1.getTime();
		return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
	}

	public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	}

	public void addFamilMember() {
		log.info("inside addFamilMember member ");
		try {
			String relationName = selectedRelation.getName();
			if (dependantName == null || dependantName.trim().isEmpty()) {
				Util.addWarn("Name is Required");
				return;
			}
			if (age == null) {
				Util.addWarn("Age is Required");
				return;
			}
			if (relationName == null) {
				Util.addWarn("Relationship is Required");
				return;
			}
			employeeFamily.setDependentName(dependantName);
			employeeFamily.setAge(age);
			validateFamilMember();
			String listRelationName;

			log.info("relation name is " + " " + selectedRelation.getName());
			int daughter = 0;
			int son = 0;
			for (EmployeeFamilyDetails list : employeeFamilyList) {
				listRelationName = list.getRelationshipMaster().getName().toString();
				if ((!relationName.equalsIgnoreCase("Daughter") && !relationName.equalsIgnoreCase("Son"))) {
					log.info("inside fo no parent ......");
					if (listRelationName.equalsIgnoreCase(relationName)) {
						throw new UIException("The relation is already Exist");
					}
				} else {
					log.info("inside children......");
					log.info("daughter and son counts....." + daughter + " / " + son);
					if (daughter == 1 && son == 1) {
						throw new UIException("Child relation is exceed limit");
					} else if (daughter > 1 || son > 1) {
						throw new UIException("Child relation is exceed limit");
					}
					if ((relationName.equalsIgnoreCase("Daughter"))) {
						daughter = daughter + 1;
					}
					if (relationName.equalsIgnoreCase("Son")) {
						son = son + 1;
					}
				}
			}
			EmployeeFamilyDetails employeeFamileys = new EmployeeFamilyDetails();
			employeeFamileys.setId(employeeFamily.getId());
			employeeFamileys.setDependentName(employeeFamily.getDependentName());
			employeeFamileys.setAge(employeeFamily.getAge());
			employeeFamileys.setRelationshipMaster(selectedRelation);
			employeeFamilyList.add(employeeFamileys);

			log.info("size of employeeFamilyList is" + " " + employeeFamilyList.size());
		} catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
			if (ex.getMessage().contains("The Combination SectionAndDeSignation ByAppFeatureId is Aleready Exist")) {
				showClientError(ex.getMessage());
			}
		}
		dependantName = "";
		age = null;
		selectedRelation = new RelationshipMaster();
	}

	public void removeItem(EmployeeFamilyDetails item) {

		if (item != null) {
			employeeFamilyList.remove(item);
		}

	}

	public void addHoidaysInHolidaysTableOfLTc() {
		log.info("inside addHoidaysInHolidaysTableOfLTc ");

		try {

			if (empLtcHolidayDates.getHolidayDate() == null) {
				throw new UIException("please select the date");
			}

			Date selectedDate = empLtcHolidayDates.getHolidayDate();
			String NameOfSelectedtDate = new SimpleDateFormat("EEEE", Locale.ENGLISH).format(selectedDate);

			Boolean selectedDateIsHoliday = CheckSelectedDateIsGovHoliday(empLtcHolidayDates.getHolidayDate());
			log.info("selected day is goverment holiday " + " " + selectedDateIsHoliday);
			if (!(selectedDateIsHoliday)) {
				if (NameOfSelectedtDate.equalsIgnoreCase("saturday")
						|| NameOfSelectedtDate.equalsIgnoreCase("sunday")) {
					if (empLtcHolidayDatesList != null) {
						for (EmpLtcHolidayDates listDate : empLtcHolidayDatesList) {
							log.info("the First Condition" + " " + selectedDate.equals(listDate.getHolidayDate()));

							if (selectedDate.equals(listDate.getHolidayDate())) {
								throw new UIException("The Date  is already Exist");
							}
						}

						EmpLtcHolidayDates empLtcHolidayobject = new EmpLtcHolidayDates();
						empLtcHolidayobject.setHolidayDate(selectedDate);
						empLtcHolidayDatesList.add(empLtcHolidayobject);
						log.info("size of empLtcHolidayDatesList is" + " " + empLtcHolidayDatesList.size());

					} else {
						EmpLtcHolidayDates empLtcHolidayobjects = new EmpLtcHolidayDates();
						empLtcHolidayobjects.setHolidayDate(selectedDate);
						empLtcHolidayDatesList.add(empLtcHolidayobjects);
						log.info("size of empLTCHolidaysDateList is" + " " + empLTCHolidaysDateList.size());
					}
				} else {
					throw new UIException("Selected Date is not Sunday or Saturday");
				}
			} else {
				if (!(checkTheSelectedDateDupplicationInempLtcHolidayDatesList(selectedDate))) {

					EmpLtcHolidayDates empLtcHolidayobjects = new EmpLtcHolidayDates();
					empLtcHolidayobjects.setHolidayDate(selectedDate);
					empLtcHolidayDatesList.add(empLtcHolidayobjects);
				} else {
					throw new UIException("The Date  is already Exist");
				}
			}
			empLtcHolidayDates.setHolidayDate(null);

		} catch (UIException ex) {
			showClientError(ex.getMessage());
			log.error("UIException at addItem() >>>> " + ex.getMessage());
		} catch (Exception ex) {
			log.error("Exception at addItem() >>>> ", ex);
			if (ex.getMessage().contains("The Combination SectionAndDeSignation ByAppFeatureId is Aleready Exist")) {
				showClientError(ex.getMessage());
			}
		}
	}

	public Boolean checkTheSelectedDateDupplicationInempLtcHolidayDatesList(Date selectedDate) throws UIException {

		Boolean duplication = false;

		if (empLtcHolidayDatesList != null) {
			for (EmpLtcHolidayDates listDate : empLtcHolidayDatesList) {
				log.info("the First Condition" + " " + selectedDate.equals(listDate.getHolidayDate()));

				if (selectedDate.equals(listDate.getHolidayDate())) {
					duplication = true;
					throw new UIException("The Date  is already Exist");

				}
			}

			log.info("duplication" + " " + duplication);

		}

		return duplication;

	}

	public Boolean CheckSelectedDateIsGovHoliday(Date selectedDate) {
		Boolean isHoliday = false;
		try {

			EmpLtcHolidayDates holidayobject = new EmpLtcHolidayDates();
			if (selectedDate != null) {
				holidayobject.setHolidayDate(selectedDate);
			}

			BaseDTO baseDTO = null;
			String url = AppUtil.getPortalServerURL() + "/employeeprofile/getHolidayListInEmployeeLeaveDates";
			baseDTO = httpService.post(url, holidayobject);

			if (baseDTO != null) {
				if (baseDTO.getTotalRecords() > 0) {
					isHoliday = true;
				}

				log.info("count  is " + baseDTO.getTotalRecords());
				log.info("condition  is " + isHoliday);

			}

		} catch (Exception e) {
			log.info("the cause of Error is" + " " + e.getCause());
		}

		return isHoliday;
	}

	public void removeDataItem(EmpLtcHolidayDates item) {

		if (item != null) {
			empLtcHolidayDatesList.remove(item);
		}

	}

	private void showClientError(String errorMessage) {

		AppUtil.addError(errorMessage);
	}

	public void validateFamilMember() throws UIException {

		log.info("the family details" + employeeFamily);
		log.info("the relation ship is" + " " + selectedRelation);

		if (employeeFamily == null || selectedRelation == null) {
			throw new UIException("Invalid input");
		} else if (employeeFamily.getDependentName() == null) {
			throw new UIException("Please Entere The Name");
		} else if (selectedRelation == null) {
			throw new UIException("Please Select Reletio");
		} else if (employeeFamily.getAge() == null) {
			throw new UIException("Please Enter The Age");
		}
	}

	public void onchangeHolidayPermission() {
		log.info("onchangeAppliedFor called ..: " + isPermission);
		holidayPermission = false;
		if (!isPermission) {
			holidayPermission = true;
		}

		log.info("holidayPermission" + " " + holidayPermission);
		/*
		 * else { employeeLeaveTravelConcession.setHolidayPermission(isPermission); if
		 * (employeeLeaveTravelConcession.getFromDate() != null) {
		 * employeeLeaveTravelConcession.setFromDate(employeeLeaveTravelConcession.
		 * getFromDate()); } if (employeeLeaveTravelConcession.getToDate() != null) {
		 * employeeLeaveTravelConcession.setToDate(employeeLeaveTravelConcession.
		 * getToDate()); }
		 * 
		 * }
		 */
	}

	/*
	 * public void loadLeaveTypeMaster() { BaseDTO baseDto = new BaseDTO();
	 * leaveTypeMasterList = new ArrayList<LeaveTypeMaster>(); url =
	 * AppUtil.getPortalServerURL() + "/leave/ltcleave";
	 * log.info("LTC Request Url :::::  " + url); try { baseDto =
	 * httpService.get(url); if (baseDto != null && baseDto.getResponseContent() !=
	 * null) { mapper = new ObjectMapper(); json =
	 * mapper.writeValueAsString(baseDto.getResponseContent());
	 * 
	 * leaveTypeMasterList = mapper.readValue(json, new
	 * TypeReference<List<LeaveTypeMaster>>() { });
	 * log.info("Total size of LTC leave type  " + leaveTypeMasterList.size()); } }
	 * catch (Exception e) { log.error("Exception in loadLeaveTypeMaster "+" "+
	 * e.getCause()); } }
	 */
	public void loadBlockYears() {
		BaseDTO baseDto = new BaseDTO();
		blockYearsList = new ArrayList<>();
		url = LTC_URL + "/getall/blockyears";
		log.info("LTC loadBlockYears Request Url :::::  " + url);
		try {
			baseDto = httpService.get(url);
			if (baseDto != null) {
				mapper = new ObjectMapper();
				json = mapper.writeValueAsString(baseDto.getResponseContents());
				List<String> listObj = mapper.readValue(json, new TypeReference<List<String>>() {
				});
				log.info("block type list ::: " + listObj);
				listObj.forEach(list -> {
					log.info(" String :: " + list);
					blockYearsList.add(list);
				});
				log.info("Total size of LTC BlockYears   " + blockYearsList.size());
			}
		} catch (Exception e) {
			log.error("Exception in loadBlockYears ", e);
		}
	}

	public String deleteLTCDetail() {
		try {
			if (selectedLTCDId == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else if (leaveTravelConcessionDto.getStatus() != null && leaveTravelConcessionDto.getStatus()
					.equalsIgnoreCase(LeaveTravelConCessionStatus.APPROVED.toString())) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else if (leaveTravelConcessionDto.getStatus() != null && leaveTravelConcessionDto.getStatus()
					.equalsIgnoreCase(LeaveTravelConCessionStatus.REJECTED.toString())) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			}
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('confirmUserDelete').show();");

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
		}
		return null;

	}

	public String deleteLTCDetailConfirm() {
		try {
			log.info("Selected LTC Details  Id : : " + selectedLTCDId.getLeaveTravelId());
			url = LTC_URL + "/ltcDelete/" + selectedLTCDId.getLeaveTravelId();
			log.info(" LTC Details Delete URL called : - " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info(" LTC Details Delete REsponse :: " + url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info("  LTC Details Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.RECORD_DELETED_SUCCESSFULLY.getErrorCode());
				selectedLTCDetails = new EmployeeLeaveTravelConcession();
				return listPage();
			} else {
				log.error(" ExamCentre Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete ExamCentre  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String validateLTCRequest() {

		if (blockYears == null || blockYears.isEmpty()) {
			return null;
		} else {
			employeeLeaveTravelConcession.setBlockYears(blockYears);
		}
		if (appliedFor != null) {
			employeeLeaveTravelConcession.setAvailedFor(appliedFor);
		}
		if (travelMode != null) {
			employeeLeaveTravelConcession.setTravelMode(travelMode);
		}
		if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getLeaveTypeMaster() == null) {
			log.info("leavetype required");
			return null;
		}

		if (employeeLeaveTravelConcession != null && employeeLeaveTravelConcession.getLeaveTypeMaster() != null
				&& leaveDetail != null
				&& leaveDetail.getLeaveBalance() < employeeLeaveTravelConcession.getHolidayNoOfDays()) {
			log.info("No Of days cannot be greater than leave balance");
			return null;
		}

		if (appliedFor.equalsIgnoreCase("Family") && selectedFamilyDetails == null
				|| selectedFamilyDetails.size() == 0) {
			log.info("Family rmember required..");
			return null;
		}

		if (isPermission && fromdateToDateList == null || fromdateToDateList.size() == 0) {
			log.info("HolidayDates required");
			return null;
		}
		return null;
	}

	public String createLTCRequest() {
		BaseDTO baseDto = new BaseDTO();
		log.info(":: Create LTCRequest start::");
		try {

			if (employeeMaster != null && employeeMaster.getUserId() != null) {
				employeeLeaveTravelConcession.setEmployeeMaster(employeeMaster);
				employeeLeaveTravelConcession.setUserId(employeeMaster.getUserId());
				employeeLeaveTravelConcession.setAvailedFor(appliedFor);
			} else {
				return null;
			}

			url = LTC_URL + "/leaveTravelConcession";
			log.info("Create LTCRequest Mark URL :" + url);
			validateLTCRequest();
			// if (true) {
			baseDto = httpService.post(url, employeeLeaveTravelConcession);
			log.info("LTCRequest BaseDTO response:" + baseDto);
			if (baseDto == null) {
				log.info("::LTCRequest details response null. ::" + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDto.getStatusCode() == 0) {
					log.info("::LTCRequest  created successfully. ::" + baseDto);
					errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());
					selectedLTCDetails = new EmployeeLeaveTravelConcession();
					return "/pages/leaveManagement/listLeaveTravelConcession.xhtml?faces-redirect=true";
				} else {
					log.info("::LTCRequest creation failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}
			// }
			/*
			 * else { log.error("validation failed while creating LTC Request.."); return
			 * null; }
			 */

		} catch (Exception e) {
			log.error(":: Exception in LTCRequest create :::::::::::", e);
			errorMap.notify(baseDto.getStatusCode());
		}
		return null;
	}

	public String loadLTCRequest() {
		try {
			if (pageAction.equalsIgnoreCase("ADD")) {

				return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";

			} else if (pageAction.equalsIgnoreCase("EDIT")) {

			} else if (pageAction.equalsIgnoreCase("VIEW")) {

			}

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Employee LTC Bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public EmployeeMaster getByEmployeeId() {
		try {
			UserMaster user = loginBean.getUserDetailSession();
			mapper = new ObjectMapper();
			log.info("user ID ==>" + user.getId());
			if (user == null || user.getId() == null) {
				log.error(" No employee selected ");
				errorMap.notify(ErrorDescription.NO_EMP_SELECTED.getErrorCode());
			}
			url = AppUtil.getPortalServerURL() + "/employeeprofile/get/employee/" + user.getId();
			log.info("Employee Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);
			log.info("Employee Get By Id Response : - " + baseDTO);

			if (baseDTO == null || baseDTO.getResponseContent() == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			json = mapper.writeValueAsString(baseDTO.getResponseContent());
			employeeMaster = mapper.readValue(json, EmployeeMaster.class);

			return employeeMaster;
		} catch (Exception e) {
			log.error(" Error Occured While Getting employee By ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			return null;
		}
	}

	public void onchangeBlockDate() {
		log.info("onchangeBlockDate is called : ");
		try {

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockFromDate())) {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getBlockFromDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			}

			if (Objects.isNull(employeeLeaveTravelConcession.getBlockToDate())) {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getBlockToDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			String fromDate = dateFormat.format(employeeLeaveTravelConcession.getBlockFromDate());
			String toDate = dateFormat.format(employeeLeaveTravelConcession.getBlockToDate());

			log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

			Date firstDate = dateFormat.parse(fromDate);
			Date secondDate = dateFormat.parse(toDate);

			long diffInMillies = (secondDate.getTime() - firstDate.getTime());
			// long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
			// long diff=AppUtil.calculateWorkDays(firstDate,secondDate);
			long diff = AppUtil.calculateWorkDayswithCurrentDate(firstDate, secondDate);
			if (diffInMillies <= 0) {
				employeeLeaveTravelConcession.setBlockToDate(null);
			}
			if (firstDate.compareTo(secondDate) == 0) {
				log.info("Number of days for leave is : " + 1);
				employeeLeaveTravelConcession.setBlockNumberOfDays(1.0);
				;
			} else {
				log.info("Number of days for leave is : " + diff);
				employeeLeaveTravelConcession.setBlockNumberOfDays((double) diff);
			}

		} catch (ParseException e) {
			log.error("found error in block date onchange", e);
		}

		// make todate null if fromdate is greater than todate
		/*
		 * if (employeeLeaveTravelConcession.getBlockYearFrom() != null &&
		 * employeeLeaveTravelConcession.getBlockYearTo() != null &&
		 * employeeLeaveTravelConcession
		 * .getBlockYearFrom().after(employeeLeaveTravelConcession.getBlockYearTo())) {
		 * employeeLeaveTravelConcession.setBlockYearTo(null);
		 * log.info("setBlockYearTo set as null"); }
		 */

	}

	public void onchangeLeaveDate() {
		log.info("onchangeBlockDate is called : ");
		try {

			if (Objects.isNull(employeeLeaveTravelConcession.getHolidayFromDate())) {
				log.info("Block From Date is : " + employeeLeaveTravelConcession.getHolidayFromDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			}

			if (Objects.isNull(employeeLeaveTravelConcession.getHolidayToDate())) {
				log.info("Block to date is : " + employeeLeaveTravelConcession.getHolidayToDate());
				errorMap.notify(ErrorDescription.LEAVE_REQUEST_LEAVE_FROM_DATE_REQUIRED.getErrorCode());
				return;
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
			String fromDate = dateFormat.format(employeeLeaveTravelConcession.getHolidayFromDate());
			String toDate = dateFormat.format(employeeLeaveTravelConcession.getHolidayToDate());

			log.info("Leave request from Date is : {} and to date is : {} ", fromDate, toDate);

			Date firstDate = dateFormat.parse(fromDate);
			Date secondDate = dateFormat.parse(toDate);

			long diffInMillies = (secondDate.getTime() - firstDate.getTime());
			// long diff=AppUtil.calculateWorkDays(firstDate,secondDate);
			long diff = AppUtil.calculateWorkDayswithCurrentDate(firstDate, secondDate);
			if (diffInMillies <= 0) {
				employeeLeaveTravelConcession.setHolidayToDate(null);
			}
			if (firstDate.compareTo(secondDate) == 0) {
				log.info("Number of days for leave is : " + 1);
				employeeLeaveTravelConcession.setHolidayNoOfDays(1);
			} else {
				log.info("Number of days for leave is : " + diff);
				employeeLeaveTravelConcession.setHolidayNoOfDays((int) diff);
			}
		} catch (ParseException e) {
			log.error("found error in block date onchange", e);
		}

		if (employeeLeaveTravelConcession.getBlockToDate() != null
				&& employeeLeaveTravelConcession.getBlockFromDate() != null && employeeLeaveTravelConcession
						.getBlockFromDate().after(employeeLeaveTravelConcession.getBlockToDate())) {
			employeeLeaveTravelConcession.setBlockToDate(null);
			log.info("todate set as null");
		}

	}

	public void addHolidaysDate() {

		fromdateToDateList = new ArrayList<>();
		log.info("list of holidays date ::::::" + fromdateToDateList);

		if (isPermission) {

			if (employeeLeaveTravelConcession.getFromDate() != null
					&& employeeLeaveTravelConcession.getToDate() != null) {

				DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy ");

				String fromDate = dateFormat.format(employeeLeaveTravelConcession.getFromDate());

				String toDate = dateFormat.format(employeeLeaveTravelConcession.getToDate());

				String seperatedByDash = Joiner.on("To").join(fromDate, toDate);

				fromdateToDateList.add(seperatedByDash);
			}

			log.info("list of holidays date after add::::::" + fromdateToDateList);
		}

		employeeLeaveTravelConcession.setFromDate(null);
		employeeLeaveTravelConcession.setToDate(null);
	}

	public void holidayDateChange() {
		if (employeeLeaveTravelConcession.getFromDate() != null && employeeLeaveTravelConcession.getToDate() != null
				&& employeeLeaveTravelConcession.getFromDate().after(employeeLeaveTravelConcession.getToDate())) {
			employeeLeaveTravelConcession.setToDate(null);
		}
	}

	public void loadMonth() {
		try {

			Calendar cal = Calendar.getInstance();

			// get the value of all the calendar date fields.
			log.info("Calendar's Year: " + cal.get(Calendar.YEAR));

		} catch (Exception e) {
			log.error("ExceptionException Ocured while Loading Employee LTC Bean==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void onRowSelect(SelectEvent event) {
		addButton = true;
		billActionBtn = false;
		if (selectedLTCDId != null) {

			String loginEmpCode = loginBean.getEmployee().getEmpCode();

			if ((!loginEmpCode.equalsIgnoreCase(selectedLTCDId.getEmpCodeName())
					&& (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.SUBMITTED.toString())
							|| selectedLTCDId.getStatus()
									.equalsIgnoreCase(LeaveTravelConCessionStatus.APPROVED.toString())
							|| selectedLTCDId.getStatus()
									.equalsIgnoreCase(LeaveTravelConCessionStatus.FINALAPPROVED.toString())))) {
				editButton = true;
				deleteButton = false;
				addButton = true;
				billButton = false; // This and delete hide and show, other btn will disabled.
				viewButton = false;
			}
			if (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.REJECTED.toString())) {
				editButton = false;
				viewButton = false;
				addButton = true;
				billButton = false; // This and delete hide and show, other btn will disabled.
				deleteButton = true;
			}
			if (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.FINALAPPROVED.toString())) {
				billButton = true; // This and delete hide and show, other btn will disabled.
				deleteButton = false;
				editButton = true;
				viewButton = false;
				addButton = true;
				billActionBtn = true;
			}
			if (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.APPROVED.toString())) {
				addButton = true;
				editButton = true;
				billButton = false; // This and delete hide and show, other btn will disabled.
				deleteButton = true;
				viewButton = false;
			}
			if (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.BILL_APPROVED.toString())
					|| selectedLTCDId.getStatus()
							.equalsIgnoreCase(LeaveTravelConCessionStatus.BILL_SUBMITTED.toString())) {
				addButton = true;
				editButton = true;
				deleteButton = false;
				currentStatus = true;
				viewButton = false;
				billButton = true; // This and delete hide and show, other btn will disabled.
			}
			if (selectedLTCDId.getStatus()
					.equalsIgnoreCase(LeaveTravelConCessionStatus.BILL_FINALAPPROVED.toString())) {
				addButton = true;
				editButton = true;
				deleteButton = false;
				viewButton = false;
				billButton = true; // This and delete hide and show, other btn will disabled.
			}
			if (selectedLTCDId.getStatus().equalsIgnoreCase(LeaveTravelConCessionStatus.BILL_REJECTED.toString())) {
				addButton = true;
				editButton = true;
				deleteButton = false;
				viewButton = false;
				billButton = true; // This and delete hide and show, other btn will disabled.
				billActionBtn = true;
			}
		}
	}

	public String leaveTravelConcessionAction() {
		log.info("<=======Starts  EmployeeLTCBean.leaveTravelConcessionAction ======>");
		memberIds = new ArrayList<>();
		if (pageAction.equalsIgnoreCase("Create")) {
			employeeLeaveTravelConcession = new EmployeeLeaveTravelConcession();
			selectedFamilyDetails = new ArrayList<>();
			fromdateToDateList.clear();
			appliedFor = "Self";
			isPermission = false;
			return "/pages/leaveManagement/createLeaveTravelConcession.xhtml?faces-redirect=true";
		}
		log.info("<=======Ends  EmployeeLTCBean.leaveTravelConcessionAction ======>");
		return null;
	}

	/*
	 * public void loadFamilyMembers() {
	 * log.info("<=======Starts  EmployeeLTCBean.loadFamilyMembers ======>"); try {
	 * BaseDTO response = httpService.get(SERVER_URL +
	 * "/employee/insurance/getfamilydetails/" +employeeMaster.getId()); if
	 * (response.getStatusCode() == 0) { json =
	 * mapper.writeValueAsString(response.getResponseContents()); employeeFamilyList
	 * = mapper.readValue(json, new TypeReference<List<EmployeeFamilyDetails>>()
	 * {}); RequestContext.getCurrentInstance().execute("PF('addMember').show();");
	 * }else { errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); }
	 * }catch(Exception e) {
	 * log.error("Exception occured in loadingFamilyMembers ::",e); }
	 * log.info("<=======Ends  EmployeeLTCBean.loadFamilyMembers ======>"
	 * +employeeFamilyList.size()); }
	 */

	public void addOrRemoveFamilyMembers(boolean flag, Long id) {
		log.info("<=======Stars  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
		log.info("Flag is ::" + flag);
		if (flag) {
			memberIds.add(id.toString());
		} else {
			memberIds.remove(id.toString());
		}
		log.info("<=======Ends  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
	}

	public void canceLoadFamilyMembers() {
		memberIds = new ArrayList<>();
	}

	public void downloadIncomingFile(String downloadFile) {
		InputStream input = null;
		try {
			File files = new File(downloadFile);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}

	String downloadPath = "";

	public void downloadIncomingFileLTCBill(String downloadFile) {
		InputStream input = null;
		downloadPath = "";
		try {
			billsList.forEach(bills -> {
				if (bills != null && !bills.isEmpty()) {
					String[] parts = bills.split("/");
					String result = parts[parts.length - 1];
					if (result.equalsIgnoreCase(downloadFile)) {
						downloadPath = bills;
					}
				}
			});

			File files = new File(downloadPath);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}
		return;
	}

	public void addFamilyMembers() {
		RequestContext.getCurrentInstance().execute("PF('addMember').hide();");
		log.info("selectedFamilyDetails size ::" + selectedFamilyDetails);
		memberIds = new ArrayList<>();
		for (EmployeeFamilyDetails emp : selectedFamilyDetails) {
			memberIds.add(emp.getId().toString());
		}
		log.info("<=======Ends  EmployeeLTCBean.addOrRemoveFamilyMembers ======>" + memberIds.size());
	}

	public void fileUpload(FileUploadEvent event) {
		log.info("Upload button is pressed..");

		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload Document is null ");

				return;
			}
			uploadedFile = event.getFile();
			String fileType = uploadedFile.getFileName();

			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")) {

				AppUtil.addWarn("Please Upload The File Which is mention below");
				log.info("<<====  error type in file upload");
				return;
			}

			long size = uploadedFile.getSize();
			log.info("Size of document is" + " " + documentSize);
			log.info("priorityUpload size==>" + size);
			if ((size / (1024 * 1024)) > 2) {
				AppUtil.addWarn("File size is exced 2Mb ");
				log.info("<<====  Large size file uploaded");
				return;
			}

			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String timeStamp = Long.toString(timestamp.getTime());

			String PathName = commonDataService.getAppKeyValue("LEAVE_TRAVEL_CONCESSION");
			String fileDetail = Util.uploadDocument(PathName, uploadedFile, timeStamp);
			if (fileDetail.equalsIgnoreCase("Error")) {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				log.error("Same file already exist on target path");
			} else
				employeeLeaveTravelConcession.setDocumentPath(fileDetail);

			fileName = uploadedFile.getFileName();

			AppUtil.addInfo("File upload SuccessFully");
			log.info(" Relieving Document is uploaded successfully");
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}

	public void onLeaveTypeChange() {
		log.info(" On leave type change ");
		Long employeeMasterId = null;
		try {
			if (employeeMasterBasedOnSectionId != null) {
				employeeMasterId = employeeMasterBasedOnSectionId.getId();
			} else if (employeeMasterBasedOnEntityId != null) {
				employeeMasterId = employeeMasterBasedOnEntityId.getId();
			}
			if (leaveTypeMaster != null && employeeMasterId != null) {
				String resource = SERVER_URL + "/leaverequest/leave/" + employeeMasterId + "/"
						+ leaveTypeMaster.getId();
				BaseDTO baseDto = httpService.get(resource);
				json = mapper.writeValueAsString(baseDto.getResponseContent());
				leaveDetail = mapper.readValue(json, LeaveRequest.class);
				if (leaveDetail.getLeaveBalanceDouble() == null || leaveDetail.getLeaveBalanceDouble() < 0) {
					leaveDetail.setLeaveBalance(0D);
				} else {
					leaveDetail.setLeaveBalance(leaveDetail.getLeaveBalanceDouble());
				}
				log.info(" Employee consumed leave is :" + leaveDetail);
			}
		} catch (Exception e) {
			log.error(":: Exception while On leave type change ::", e);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getErrorCode());
		}
	}

	public String saveLTCForm() {
		try {

			log.info("Save process is started");

			if (isPermission && empLtcHolidayDatesList.isEmpty()) {
				log.info("===Employee Holiday List is empty=====");
				Util.addWarn("Holiday Permission Date cannot be Empty");
				return null;
			}
			if (leaveDetail != null && null != leaveDetail.getLeaveBalance()
					&& leaveDetail.getLeaveBalance().equals(0)) {
				log.info("===Total Number of Days is 0=====");
				Util.addWarn("You are not eligible for apply leave");
				return null;
			}

			if (employeeLeaveTravelConcession.getEligibleDistance() == null) {
				Util.addWarn("Eligible Distance must need");
				return null;
			}
			if (leaveTravelConcessionDto.getNote() == null || leaveTravelConcessionDto.getNote().isEmpty()) {
				errorMap.notify(ErrorDescription.CASH_TO_BANK_NOTES.getCode());
				return null;

			}
			if (employeeLeaveTravelConcession.getHolidayFromDate() != null
					&& employeeLeaveTravelConcession.getHolidayToDate() == null) {
				Util.addWarn("Leave to Date is required");
				return null;

			}

			if (appliedFor.equalsIgnoreCase("Family") && employeeFamilyList.isEmpty()) {
				Util.addWarn("Please Add Family Details");
				return null;
			}

			if (null != leaveDetail.getLeaveBalance()
					&& leaveDetail.getLeaveBalance() < employeeLeaveTravelConcession.getHolidayNoOfDays()) {
				Util.addWarn("No.Of leave days is greater than leave balance");
				return null;
			}
			if (employeeLeaveTravelConcession.getEligibleDistance() < employeeLeaveTravelConcession
					.getTravelledDistance()) {
				Util.addWarn("Travelled Distance should not  greater than Eligible Distance");
				return null;
			}

			if (userMaster != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			}

			if (slectedEntity != null) {
				leaveTravelConcessionDto.setEntityMaster(slectedEntity);
			} else {
				leaveTravelConcessionDto.setEntityMaster(selectedHoRo);
			}

			log.info("departementFlag" + departementFlag);

			if (departementFlag) {
				if (selectedSection.getId() != null) {
					leaveTravelConcessionDto.setSectionMaster(selectedSection);
				} else {
					log.info("sectionMaster is null");
				}

				if (employeeMasterBasedOnSectionId.getId() != null) {
					log.info("the selcete empMaster is" + " " + employeeMasterBasedOnSectionId);
					leaveTravelConcessionDto.setEmployeeMaster(employeeMasterBasedOnSectionId);
				} else {
					log.info("employeeMasterBasedOnSectionId is null");
				}

			} else {
				log.info("departementFlag" + departementFlag);
				if (employeeMasterBasedOnEntityId.getId() != null) {
					log.info("the selcete empMaster is" + " " + employeeMasterBasedOnEntityId);
					leaveTravelConcessionDto.setEmployeeMaster(employeeMasterBasedOnEntityId);
				}
			}

			if (selectedBlocKYear != null) {
				log.info("selectedBlockYear is...." + selectedBlocKYear);
				leaveTravelConcessionDto.setBlockFromYear(selectedBlocKYear.getStartYear());
				leaveTravelConcessionDto.setBlockToYear(selectedBlocKYear.getEndYear());
				leaveTravelConcessionDto.setSelectedBlocKYear(selectedBlocKYear);
			}

			employeeLeaveTravelConcession.setAvailedFor(appliedFor);
			// leaveTravelConcessionDto.setFileList(incomingFileList);

			if (addMember) {
				if (employeeFamilyList != null) {
					employeeLeaveTravelConcession.setLeaveAppliedFamily(true);
					employeeLeaveTravelConcession.setLeaveAppliedSelf(false);
					StringBuilder familyDatails = new StringBuilder();
					String name;
					String relationId;
					String age;
					String cobnination;
					for (EmployeeFamilyDetails family : employeeFamilyList) {
						name = family.getDependentName();
						relationId = family.getRelationshipMaster().getId().toString();
						age = family.getAge().toString();
						cobnination = name + "-" + relationId + "-" + age;

						log.info("cobnination" + cobnination);
						log.info("length is" + " " + familyDatails.length());
						log.info("length is" + " " + familyDatails);

						if (familyDatails.length() > 1) {
							familyDatails.append("/").append(cobnination);
						} else {
							familyDatails.append(cobnination);
						}
					}

					log.info("the familyDetails" + " " + familyDatails);
					employeeLeaveTravelConcession.setFamilyDetailsIds(familyDatails.toString());
				}
			} else {
				employeeLeaveTravelConcession.setLeaveAppliedSelf(true);
				employeeLeaveTravelConcession.setLeaveAppliedFamily(false);
			}

			if (travelMode != null) {
				log.info("the travel mode is" + travelMode);
				employeeLeaveTravelConcession.setTravelMode(travelMode);
			}

			if (leaveTypeMaster != null) {
				employeeLeaveTravelConcession.setLeaveTypeMaster(leaveTypeMaster);
			}
			if (!isPermission) {
				empLtcHolidayDatesList = new ArrayList<EmpLtcHolidayDates>();
			}
			if (empLtcHolidayDatesList != null && !empLtcHolidayDatesList.isEmpty()) {
				log.info("empLtcHolidayDatesList" + empLtcHolidayDatesList);
				List<Date> holidaylist = new ArrayList<Date>();
				for (EmpLtcHolidayDates list : empLtcHolidayDatesList) {
					holidaylist.add(list.getHolidayDate());
				}
				employeeLeaveTravelConcession.setHolidayPermission(true);
				leaveTravelConcessionDto.setHolidayList(holidaylist);
				log.info("the holidays list we sending to data base is" + " " + holidaylist.size());
			} else {
				employeeLeaveTravelConcession.setHolidayPermission(false);
				leaveTravelConcessionDto.setHolidayList(new ArrayList<Date>());
			}

			leaveTravelConcessionDto.setEmployeeLeaveTravelConcession(employeeLeaveTravelConcession);
			log.info("the leaveTravelConcessionDto we submiting to data base is" + " " + leaveTravelConcessionDto);

			BaseDTO baseDTO = new BaseDTO();
			String url = LTC_URL + "/saveEmployeTravelConcession";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);

			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("FileMovementConfigListBYAppFeatureId deleted successfully....!!!");
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_CREATED_SUCCESSFULLY).getErrorCode());
					// errorMap.notify(baseDTO.getStatusCode());

				} else {
					log.info("Error occured in Rest api ... ");
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_CREATED_SUCCESSFULLY).getErrorCode());
					// errorMap.notify(baseDTO.getStatusCode());
				}
			}
		} catch (Exception e) {
			log.info("the cause of error is" + " " + e.getCause());
			log.info("the cause of error message is" + " " + e.getMessage());
		}
		return listPage();
	}

	public void billsFileUpload(FileUploadEvent event) {
		log.info("<====== Incoming tapal Upload Event =====>" + event);
		if (event == null || event.getFile() == null) {
			log.error(" Upload Document is null ");
			return;
		}
		uploadedFile = event.getFile();
		long size = uploadedFile.getSize();
		log.info("uploadSignature size==>" + size);
		size = size / 1000;
		letterSize = Long.valueOf(commonDataService.getAppKeyValue("LTC_BILLS_FILE_SIZE"));
		if (size > letterSize) {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			errorMap.notify(ErrorDescription.POLICY_NOTE_UPLOAD_FILE_SIZE.getErrorCode());
			return;
		}
		String ltcPath = commonDataService.getAppKeyValue("LTC_BILLS_UPLOAD_PATH");

		log.info("ltcPath >>>> " + ltcPath);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String timeStamp = Long.toString(timestamp.getTime());
		String fileDetail = Util.uploadDocument(ltcPath, uploadedFile, timeStamp);
		log.info("fileName with path...." + fileDetail);
		if (fileDetail.equalsIgnoreCase("Error")) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("Same file already exist on target path");
		} else
			employeeLeaveTravelConcession.setDocumentPath(fileDetail);

		String name = uploadedFile.getFileName();
		setLtcBillsDocument(ltcBillList.size() - 1, fileDetail);
		ltcBillList.set(ltcBillList.size() - 1, name);
		fileList.set(ltcBillList.size() - 1, name);
		log.info("file list size....." + ltcBillList.size());
		RequestContext.getCurrentInstance().execute("PF('pridocupload').hide();");
		errorMap.notify(
				ErrorDescription.getError(AdminErrorCode.LTC_BILSS_DOCUMENT_UPLOADED_SUCCESSFULLY).getErrorCode());
		log.info(" billsFileUpload  is uploaded successfully");
	}

	public void setLtcBillsDocument(int index, String val) {
		billsList.add(index, val);
	}

	public void addRow() {
		String s = new String();
		ltcBillList.add(s);
	}

	public void deleteRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		if (rowIndex != 0) {
			if (ltcBillList.size() > rowIndex)
				ltcBillList.remove(rowIndex);

			if (billsList.size() > rowIndex)
				billsList.remove(rowIndex);
		}
		if (rowIndex == 0) {
			if (ltcBillList.size() > 1)
				ltcBillList.remove(rowIndex);

			if (billsList.size() > 1)
				billsList.remove(rowIndex);
		}
	}

	public void loadLazyEmployeeLeaveTravelConcessionListff() {
		// disableAddButton = false;
		log.info("<===== Lazy loading EMPLTC ======>");
		empLTClazyLoadReport = new LazyDataModel<LeaveTravelConcessionDto>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<LeaveTravelConcessionDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest.getFilters());
					String url = LTC_URL + "/getTheListEmployeeLTCByLazyLoad";
					BaseDTO response = httpService.post(url, paginationRequest);
					log.info("get LeaveTravelConcessionDto list");
					log.info("LeaveTravelConcessionDto Total records" + response.getTotalRecords());

					log.info("the object returnend from LeaveTravelConcessionDto" + response.getResponseContents());
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						listPageEmployeeLtclist = mapper.readValue(jsonResponse,
								new TypeReference<List<LeaveTravelConcessionDto>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyEmployeeLeaveTravelConcessionList List ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("list returned from data base while slecting Lazyload method"
						+ listPageEmployeeLtclist.toString());
				log.info("Ends lazy load....");

				return listPageEmployeeLtclist;
			}

		};
	}

	public void loadLazyEmployeeLeaveTravelConcessionList() {
		log.info("<===== Starts TdsPaymentBean.lazyTourTDSPaymentList ======>");
		empLTClazyLoadReport = new LazyDataModel<LeaveTravelConcessionDto>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<LeaveTravelConcessionDto> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					mapper = new ObjectMapper();
					log.info("Pagination request :::" + paginationRequest);
					String url = LTC_URL + "/getTheListEmployeeLTCByLazyLoad";
					log.info("url==>" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						log.info("get TourProgram list" + response.getTotalRecords());
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						listPageEmployeeLtclist = mapper.readValue(jsonResponse,
								new TypeReference<List<LeaveTravelConcessionDto>>() {
								});
						this.setRowCount(response.getTotalRecords());

						totalSize = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in getTDSList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return listPageEmployeeLtclist;
			}

			@Override
			public Object getRowKey(LeaveTravelConcessionDto res) {
				log.info("Get Lazy load LTC getRowKey:" + res.getLeaveTravelId());
				return res != null ? res.getLeaveTravelId() : null;
			}

			@Override
			public LeaveTravelConcessionDto getRowData(String rowKey) {
				try {
					for (LeaveTravelConcessionDto obj : listPageEmployeeLtclist) {
						if (obj.getLeaveTravelId().equals(Long.valueOf(rowKey))) {
							log.info("Get Lazy load LTC getRowKey:" + obj.getLeaveTravelId());
							selectedLTCDId = obj;
							return obj;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in LTC getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends LTCBean.lazyTDSPaymentList ======>" + listPageEmployeeLtclist.size());
	}

	public String saveBills() {
		log.info("<---- Start LTCBean bills save : ----->");
		BaseDTO baseDTO = null;
		try {
			if (userMaster != null && userMaster.getId() != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			} else {
				Util.addWarn("Forward To should not be empty");
				return null;
			}
			leaveTravelConcessionDto.setFileList(getBillsList());
			leaveTravelConcessionDto.setLeaveTravelId(selectedLTCDId.getLeaveTravelId());

			String url = LTC_URL + "/saveBills";
			log.info("LTC id----------" + employeeLeaveTravelConcession.getId());
			log.info("LTC DTO----------" + leaveTravelConcessionDto);
			if (leaveTravelConcessionDto.getFileList().size() == 1) {
				Util.addWarn("Add Leave Bills Documents ");
				return null;
			}
			baseDTO = httpService.post(url, leaveTravelConcessionDto);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_BILSS_DOCUMENT_SAVED).getErrorCode());
				log.info("Successfully Approved-----------------");
				clear();
				loadLazyEmployeeLeaveTravelConcessionList();
			}
		} catch (Exception e) {
			log.error("save bill exception ==> ", e);
		}
		return LIST_URL;
	}

	public String approveLTC() {
		// This is the View Page ForwardTo approved payment
		log.error("<---- Start LTCBean final approval: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("LTC id----------" + leaveTravelConcessionDto.getLeaveTravelId());
			log.info("previousApproval ----------" + previousApproval);
			log.info("current approveal ----------" + leaveTravelConcessionDto.getForwardFor());
			baseDTO = new BaseDTO();
			if (action.equalsIgnoreCase("view")) {
				if (previousApproval == false && leaveTravelConcessionDto.getForwardFor() == false) {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.APPROVED.toString());
				} else if (previousApproval == false && leaveTravelConcessionDto.getForwardFor() == true) {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.APPROVED.toString());
				} else if (previousApproval == true && !currentStatus) {
					log.error("<---- Approve Status change on Final Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.FINALAPPROVED.toString());
				} else if (previousApproval == true && currentStatus) {
					log.error("<---- Approve Status change on Final Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_FINALAPPROVED.toString());
				} else {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.APPROVED.toString());
				}
			} else {
				if (previousApproval == false && leaveTravelConcessionDto.getForwardFor() == false) {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_SUBMITTED.toString());
				} else if (previousApproval == false && leaveTravelConcessionDto.getForwardFor() == true) {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_APPROVED.toString());
				} else if (previousApproval == true) {
					log.error("<---- Approve Status change on Final Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_FINALAPPROVED.toString());
				} else {
					log.error("<---- Approve Status change on Approved  ----->");
					leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_APPROVED.toString());
				}
			}
			if (userMaster != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			}

			if (leaveTravelConcessionDto.getRemarks() == null) {
				AppUtil.addWarn("Please Enter Remarks");
				return null;
			}
			log.info("approve remarks=========>" + leaveTravelConcessionDto.getRemarks());
			String url = LTC_URL + "/ltcApprovedOrReject";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_APPROVED_SUCCESSFULLY).getErrorCode());
				log.info("Successfully Approved-----------------");
				selectedLTCDId = new LeaveTravelConcessionDto();
				// RequestContext.getCurrentInstance().update("growls");
				return LIST_URL;
			}
		} catch (Exception e) {
			log.error("approveLTC method inside exception-------", e);
		}
		return null;
	}

	public String rejectLTC() {
		// This is the View Page ForwardTo rejected payment
		log.error("<----Start LTCBean.rejected: ----->");
		BaseDTO baseDTO = null;
		try {
			log.info("LTCBean selected id----------" + leaveTravelConcessionDto.getLeaveTravelId());
			baseDTO = new BaseDTO();
			if (action.equalsIgnoreCase("view"))
				leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.REJECTED.toString());
			else
				leaveTravelConcessionDto.setStatus(LeaveTravelConCessionStatus.BILL_REJECTED.toString());

			if (userMaster != null) {
				leaveTravelConcessionDto.setForwardTo(userMaster.getId());
			}

			if (leaveTravelConcessionDto.getRemarks() == null) {
				AppUtil.addWarn("Please Enter Remarks");
				return null;
			}

			String url = LTC_URL + "/ltcApprovedOrReject";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);
			if (baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(AdminErrorCode.LTC_REJECTED_SUCCESSFULLY).getErrorCode());
				log.info("Successfully Rejected-----------------");
				// RequestContext.getCurrentInstance().update("growls");
				return LIST_URL;
			}
		} catch (Exception e) {
			log.error("LTC reject method inside exception-------", e);
		}
		return null;
	}

	/**
	 * Chat command List get and submit commands
	 */

	public void getApprovedCommentsList() {

		log.info("inside the method is getApprovedCommentsList()  ");

		try {
			BaseDTO baseDTO = new BaseDTO();

			log.info("govtSchemePlan" + leaveTravelConcessionDto);
			String url = LTC_URL + "/getallApprovedCommentsList";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);

			if (baseDTO.getResponseContent() != null) {
				log.info("baseDTO" + baseDTO.toString());
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				approveCommentList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeLTCCommentDTO>>() {
				});
			}

			Set<EmployeeLTCCommentDTO> approvedSet = new LinkedHashSet<>();
			approvedCommentMap = new LinkedHashMap<Long, Set<EmployeeLTCCommentDTO>>();
			if (approveCommentList != null && !approveCommentList.isEmpty()) {
				for (EmployeeLTCCommentDTO obj : approveCommentList) {
					approvedSet = new LinkedHashSet<>();
					approvedSet.add(obj);
					approvedCommentMap.put(obj.getId(), approvedSet);
				}
			}
			if (approvedCommentMap.size() > 0)
				showApprovedTable = true;

			log.info("approvedCommentMap" + approvedCommentMap);
			log.info("approveCommentList" + approveCommentList);
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
	}

	public void getRejectedCommentsAndReplayCommentList() {
		log.info("inside the  getRejectedCommentsAndReplayCommentList mehtod");

		try {
			BaseDTO baseDTO = new BaseDTO();
			log.info("leaveTravelConcessionDto" + leaveTravelConcessionDto);
			String url = LTC_URL + "/getallRejectedAndReplayCommentList";
			baseDTO = httpService.post(url, leaveTravelConcessionDto);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				rejectAndReplyCommentList = mapper.readValue(jsonResponse,
						new TypeReference<List<EmployeeLTCCommentDTO>>() {
						});
			}
			List<Long> userIdList = new ArrayList<>();
			if (rejectAndReplyCommentList != null && rejectAndReplyCommentList.size() > 0) {
				showRejectTable = true;
				Long loginId = loginBean.getUserMaster().getId();
				log.info("date format......" + loginBean.getUserInfoDTO().getDateFormat());
				userReplyComment = new EmployeeLTCCommentDTO();
				Set<EmployeeLTCCommentDTO> rejectSet = new LinkedHashSet<>();
				rejectedCommentMap = new LinkedHashMap<Long, Set<EmployeeLTCCommentDTO>>();
				for (EmployeeLTCCommentDTO obj : rejectAndReplyCommentList) {
					rejectSet = new LinkedHashSet<>();

					userIdList = obj.getForwardToList();
					userIdList.add(leaveTravelConcessionDto.getEmployeeLeaveTravelConcession().getCreatedBy().getId());
					if (obj.getForwardTo() != null) {
						if (obj.getForwardTo() == loginId)
							commentName = "Reject Comments";
						else
							commentName = "Reply Comments";
					}
					if (obj.getRejectedComments() != null)
						obj.setRejectShow(true);
					if (obj.getReplyComments() != null)
						obj.setReplyShow(true);
					if (userIdList.contains(loginId))
						commentShow = true;
					if (obj.getCurrentStage().equals(true))
						showReplyBox = false;
					if (obj.getCurrentLogStatus().equals(LeaveTravelConCessionStatus.FINALAPPROVED.toString()))
						showReplyBox = false;
					if ((obj.getCurrentLogStatus().trim().equals(LeaveTravelConCessionStatus.REJECTED.toString()))
							&& userIdList.contains(loginId))
						showReplyBox = true;
					if ((obj.getCurrentLogStatus().trim().equals(LeaveTravelConCessionStatus.APPROVED.toString())
							&& loginId.equals(leaveTravelConcessionDto.getEmployeeLeaveTravelConcession().getCreatedBy()
									.getId())))
						showReplyBox = false;
					if ((obj.getCurrentLogStatus().trim().equals(LeaveTravelConCessionStatus.APPROVED.toString()))
							&& obj.getCurrentForwardToUser().equals(loginId))
						showReplyBox = true;

					log.info("current Status and showReplyBox is..." + obj.getCurrentLogStatus() + "  " + showReplyBox);
					rejectSet.add(obj);
					rejectedCommentMap.put(obj.getId(), rejectSet);
					userReplyComment.setForwardTo(obj.getForwardTo());
					userReplyComment.setId(leaveTravelConcessionDto.getEmployeeLeaveTravelConcession().getId());
				}
				log.info("rejectedCommentMap" + rejectedCommentMap);
				log.info("showReplayBox" + showReplyBox);
				log.info("rejectAndReplyCommentList size" + rejectAndReplyCommentList.size());
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
	}

	public String saveReplyComments() {
		BaseDTO baseDTO = new BaseDTO();
		String url = LTC_URL + "/saveReplyComments";
		if (userReplyComment.getUserReplyComments() == null) {
			errorMap.notify(ErrorDescription.PLEASE_ENTER_REJECT_COMMENTS.getErrorCode());
			RequestContext.getCurrentInstance().update("growls");
			return null;
		}
		log.info("govtSchemePlan" + userReplyComment);
		if (userReplyComment != null) {
			log.info("the reply comments is going to save is " + userReplyComment);
			try {
				baseDTO = httpService.post(url, userReplyComment);
				if (baseDTO != null) {
					if (baseDTO.getStatusCode() == 0) {
						log.info("saveReplyComments is saved successfully");
						errorMap.notify(baseDTO.getStatusCode());

						return LIST_URL;
					}
				} else {
					log.info("base dto is empty from data base for  saveReplyComments method");
				}

			} catch (Exception e) {
				log.error("Error while saving  ReplyCommentsByGovSchemeplanId" + e);
			}

		} else {
			log.info("the reply comments is null");
		}

		return null;
	}

	public void showapproveCommentsByDefault() {
		log.info("inside the rest method");
		getApprovedCommentsList();

	}

	@Getter
	@Setter
	private Long systemNotificationId = 0l;

	@PostConstruct
	public String showNotificationViewPage() {
		log.info("Leave RequestBean showNotificationViewPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String id = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			id = httpRequest.getParameter("id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(id)) {
			selectedLTCDId = new LeaveTravelConcessionDto();
			action = "View";
			selectedLTCDId.setLeaveTravelId(Long.parseLong(id));
			systemNotificationId = Long.parseLong(notificationId);
			showPage();
		}
		log.info("Leave RequestBean showNotificationViewPage() Ends");
		return null;
	}

}
