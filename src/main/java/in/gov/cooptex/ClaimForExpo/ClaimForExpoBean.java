package in.gov.cooptex.ClaimForExpo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.exhibition.model.ExhibitionClaims;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.finance.dto.ExhibitionClaimDTO;
import in.gov.cooptex.finance.dto.ExhibitionClaimRequestDTO;
import in.gov.cooptex.finance.dto.ExpenseTypeDTO;
import in.gov.cooptex.operation.enums.ExhibitionClaimsStatus;
import in.gov.cooptex.operation.model.ExhibitionMaster;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Service("claimForExpoBean")
@Scope("session")
@Log4j2
public class ClaimForExpoBean extends CommonBean implements Serializable {

	final String LIST_PAGE = "/pages/admin/claimForExpo/listClaimForExpo.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/admin/claimForExpo/createClaimForExpo.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/admin/claimForExpo/viewClaimForExpo.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL() + "/api/v1/operation/exhibitionclaims";

	String jsonResponse;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	List<EntityMaster> headOrRegionalOffice = new ArrayList<>();

	@Getter
	@Setter
	List<ExhibitionMaster> nameOfExpoDropDown = new ArrayList<>();

	@Getter
	@Setter
	ExhibitionClaims exhibitionClaims = new ExhibitionClaims();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	ObjectMapper mapper = new ObjectMapper();

	@Setter
	@Getter
	List<ExhibitionClaimRequestDTO> createExhibitionClaim = new ArrayList<>();

	@Setter
	@Getter
	Long headOrRegional;

	@Setter
	@Getter
	Long nameOfExpoNameId;

	@Setter
	@Getter
	ExhibitionClaimDTO exhibitionClaimDetailsDTO = new ExhibitionClaimDTO();

	@Setter
	@Getter
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

	@Setter
	@Getter
	List<ExhibitionClaimDTO> exhibitionClaimDTOList;

	private static DecimalFormat decimalFormat = new DecimalFormat(".##");

	@Getter
	@Setter
	List<CreatePageExhibitionClaimListDTO> createClaimListDTOS = new ArrayList<>();

	@Setter
	@Getter
	Double totalExpoAmount;

	@Getter
	public final String APPROVAL = "Approval";

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	public final String FINAL_APPROVAL = "Final Approval";

	@Setter
	@Getter
	List<String> forwardForList = new ArrayList<String>() {
		{
			add(APPROVAL);
			add(FINAL_APPROVAL);
		}
	};

	@Setter
	@Getter
	public Boolean selectedForwardFor;

	@Setter
	@Getter
	CreatePageExhibitionClaimListDTO selectedCreatePageExhibitionClaimListDTO;

	@Setter
	@Getter
	public String selectedForwardForSelect = "Select";

	@Setter
	@Getter
	public Long selectedForwardTo;
	@Setter
	@Getter
	public String selectedForwardToString;

	@Setter
	@Getter
	public String reason;

	@Setter
	@Getter
	String fileType;

	@Getter
	@Setter
	String expoDocumentFileName;

	@Setter
	@Getter
	ExhibitionClaimRequestDTO selectedViewExhibitionClaimResponseDTO;

	@Getter
	@Setter
	ExhibitionClaimRequestDTO viewResponseDTO = new ExhibitionClaimRequestDTO();

	@Setter
	@Getter
	ExhibitionClaimDTO viewPageExhibitionClaimDetails = new ExhibitionClaimDTO();

	@Setter
	@Getter
	String approveOrReject = "";

	@Setter
	@Getter
	private StreamedContent file;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	LoginBean loginBean;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = false;

	@Getter
	@Setter
	Boolean editButtonFlag = false;

	@Getter
	@Setter
	Boolean deleteButtonFlag = false;

	@Getter
	@Setter
	Boolean viewButtonFlag = false;

	@Getter
	@Setter
	ExhibitionClaimDTO selectedExhibitionListPage;

	@Getter
	@Setter
	LazyDataModel<ExhibitionClaimDTO> lazyExhibitionClaimDTOSList;

	@Getter
	@Setter
	List<ExhibitionClaimDTO> exhibitionClaimListPage = new ArrayList<>();

	@Getter
	@Setter
	String note = "";

	@Autowired
	private CommonDataService commonDataService;

	@Getter
	@Setter
	List<String> statusValuesList;

	@Getter
	@Setter
	Boolean forwardToForFlag = false;

	@Getter
	@Setter
	Boolean approveRejectFlag = false;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	Double salesAmount;

	@Getter
	@Setter
	ExhibitionMaster exhibitionMaster;

	@Getter
	@Setter
	Boolean hideflag = true;

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterData;

	@Getter
	@Setter
	Long selectedNotificationId = null;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Getter
	@Setter
	UserMaster forwardtousermaster;

	@PostConstruct
	public String showViewListPage() {
		log.info("Claims showViewListPage() Starts");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String circularId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			circularId = httpRequest.getParameter("exhibition_claims_id");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
			log.info("circulariddd" + circularId);
		}
		if (StringUtils.isNotEmpty(circularId)) {
			// init();
			selectedExhibitionListPage = new ExhibitionClaimDTO();
			action = "VIEW";

			log.info("valueeee" + selectedExhibitionListPage.getExhibitionClaimId());
			selectedExhibitionListPage.setExhibitionClaimId(Long.parseLong(circularId));
			log.info("valueeee" + selectedExhibitionListPage.getExhibitionClaimId());
			selectedNotificationId = Long.parseLong(notificationId);

			viewExopClaimInfo();
		}
		log.info(" showViewListPage() Ends");

		return VIEW_PAGE;
	}

	public String showExhibitionClaimList() {
		log.info("<====== Starts ClaimForExpoBean.showExhibitionClaimList() ====>");

		addButtonFlag = true;
		lazyExhibitionClaimsResponse();
		loadStatusValues();
		log.info("<====== Ends ClaimForExpoBean.showExhibitionClaimList() ====>");
		exhibitionClaimsClearSearch();
		return LIST_PAGE;
	}

	public void loadAllCreatePageDropDown() {
		log.info("<====== Starts ClaimForExpoBean.loadAllCreatePageDropDown() ====>");

		headOrRegionalOffice = commonDataService.loadHeadAndRegionalOffice();
		// forwardToUsersList = commonDataService.loadForwardToUsers();
		forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CLAIM_EXPO");
		forwardtousermaster = new UserMaster();
		employeeMaster = commonDataService.loadEmployeeByUser(loginBean.getUserMaster().getId());
//		loadAllExpoNameDropDown();
		// loadListOfExpenses();
		exhibitionMaster = new ExhibitionMaster();
		this.exhibitionClaimDetailsDTO = new ExhibitionClaimDTO();
		this.selectedForwardForSelect = "Select";
		this.selectedForwardFor = null;
		this.selectedForwardTo = null;
		this.selectedForwardToString = "Select";
		this.reason = null;
		this.note = null;
		salesAmount = null;
		headOrRegional = null;
		fileType = null;
		log.info("<====== Ends ClaimForExpoBean.loadAllCreatePageDropDown() ====>");
	}

	private void loadStatusValues() {
		log.info("<====== Starts ClaimForExpoBean.loadStatusValues() ====>");
		Object[] statusArray = ExhibitionClaimsStatus.class.getEnumConstants();

		statusValuesList = new ArrayList<>();

		for (Object status : statusArray) {
			statusValuesList.add(status.toString());
		}
		log.info("<--- statusValuesList ---> " + statusValuesList);
	}

	private void loadAllExpoNameDropDown() {
		log.info("<====== Starts ClaimForExpoBean.loadAllExpoNameDropDown() ====>");
		try {
			nameOfExpoNameId = null;
			String url = SERVER_URL + "/getallnonclaimexhibitions";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				List<ExhibitionMaster> exhibitionMasters = mapper.readValue(jsonResponse,
						new TypeReference<List<ExhibitionMaster>>() {
						});
				if (exhibitionMasters == null) {
					log.info(" Exhibition  claims Request List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				nameOfExpoDropDown = exhibitionMasters;
				log.info(" Exhibition claims Type list Successfully Completed");
			} else {
				log.error("  Exhibition claims Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception Ocured while Loading Exhibition Claims Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<--- typesOfExhibitionClaimsValues ---> " + nameOfExpoDropDown);
	}

	public void loadListOfExpenses() {
		log.info("<====== Starts ClaimForExpoBean.loadListOfExpenses() ====>");
		try {
			nameOfExpoNameId = null;
			String url = SERVER_URL + "/getallexhibitionexpenses";
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				List<ExhibitionClaimDTO> exhibitionClaimDTOS = mapper.readValue(jsonResponse,
						new TypeReference<List<ExhibitionClaimDTO>>() {
						});
				if (exhibitionClaimDTOS == null) {
					log.info("  Exhibition claims Request is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				initializeListOfExpensesRowsData(exhibitionClaimDTOS);
				log.info("  Exhibition claims Type list Successfully Completed");
			} else {
				log.error("  Exhibition claims Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception Ocured while Loading  Exhibition claims Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<--- typesOfExhibitionClaimsValues ---> " + nameOfExpoDropDown);
	}

	private String horoName = "";

	private void initializeListOfExpensesRowsData(List<ExhibitionClaimDTO> exhibitionClaimDTOS) {
		this.exhibitionClaimDTOList = exhibitionClaimDTOS;
		createClaimListDTOS = new ArrayList<>();
		double amount = 0;
		horoName = "";
		headOrRegionalOffice.stream().forEach(h -> {
			if (headOrRegional == h.getId()) {
				horoName = h.getName();
			}
		});
		for (ExhibitionClaimDTO exhibition : exhibitionClaimDTOS) {
			if ((exhibition.getExpenseTypeDTOList() != null) && exhibition.getExpenseTypeDTOList().size() > 0) {
				for (ExpenseTypeDTO expense : exhibition.getExpenseTypeDTOList()) {
					if (expense.getType() != null) {
						CreatePageExhibitionClaimListDTO c = new CreatePageExhibitionClaimListDTO();
						c.setAmount(expense.getAmount());
						c.setExpoDate(getFormattedDate(exhibition.getExpoDate()));
						c.setExpoToDate(getFormattedDate(exhibition.getExpoToDate()));
						c.setExpoName(exhibition.getNameOfExpo());
						c.setHoro(horoName);
						c.setTypeOfExpenses(expense.getType());
						c.setExhibitionId(exhibition.getId());
						c.setExpenseId(expense.getId());
						createClaimListDTOS.add(c);
						amount += expense.getAmount();
					}
				}
			}

		}
		totalExpoAmount = amount;
	}

	public void loadAllExpoNameDropDownByHeadOrRegionalOffice() {
		exhibitionClaimDetailsDTO = new ExhibitionClaimDTO();
		createClaimListDTOS = new ArrayList<>();
		totalExpoAmount = 0.00;
		salesAmount = null;
		nameOfExpoNameId = null;
		RequestContext context = RequestContext.getCurrentInstance();
		try {
			String url;
			nameOfExpoDropDown = new ArrayList<>();
			if (headOrRegional == null) {
				url = SERVER_URL + "/getallnonclaimexhibitions";
			} else {
				url = SERVER_URL + "/getexhibitiondropdown/" + headOrRegional;
			}

			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				List<ExhibitionMaster> exhibitionMasters = mapper.readValue(jsonResponse,
						new TypeReference<List<ExhibitionMaster>>() {
						});
				if (exhibitionMasters == null) {
					log.info(" Exhibition claims Request List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				nameOfExpoDropDown = exhibitionMasters;
				log.info(" Exhibition claims Type list Successfully Completed");
			} else {
				log.error(" Exhibition claims Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				context.update("growls");
			}
		} catch (Exception e) {
			log.error("Exception Ocured while Loading  Exhibition claims Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<--- typesOfExhibitionClaimsValues ---> " + nameOfExpoDropDown);
	}

	public void loadAllExpoNameDropDownByHeadOrRegionalOffice1() {

		RequestContext context = RequestContext.getCurrentInstance();
		try {
			String url;
			nameOfExpoDropDown = new ArrayList<>();
			if (headOrRegional == null) {
				url = SERVER_URL + "/getallnonclaimexhibitions";
			} else {
				url = SERVER_URL + "/getexhibitiondropdown/" + headOrRegional;
			}

			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				List<ExhibitionMaster> exhibitionMasters = mapper.readValue(jsonResponse,
						new TypeReference<List<ExhibitionMaster>>() {
						});
				if (exhibitionMasters == null) {
					log.info(" Exhibition claims Request List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				nameOfExpoDropDown = exhibitionMasters;
				log.info(" Exhibition claims Type list Successfully Completed");
			} else {
				log.error(" Exhibition claims Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				context.update("growls");
			}
		} catch (Exception e) {
			log.error("Exception Ocured while Loading  Exhibition claims Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<--- typesOfExhibitionClaimsValues ---> " + nameOfExpoDropDown);
	}

	public void generate() {
		log.info("<<============= Generate Calling ==========>>");
		try {
			// loadListOfExpensesListByExpoName();
			createClaimListDTOS = new ArrayList<>();
			initializeListOfExpensesRowsData(new ArrayList<ExhibitionClaimDTO>() {
				{
					add(exhibitionClaimDetailsDTO);
				}
			});
		} catch (Exception e) {
			log.error("Exception Ocured while  generate Claims Request :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void loadListOfExpensesListByExpoName() {
		exhibitionClaimDetailsDTO = new ExhibitionClaimDTO();
		createClaimListDTOS = new ArrayList<>();
		totalExpoAmount = 0.00;
		salesAmount = null;
		try {
			if (exhibitionMaster == null) {
				return;
			}
			String url = SERVER_URL + "/getExhibition/" + exhibitionMaster.getId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO == null) {
				log.error(" Base DTO returned Null Values ");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				ExhibitionClaimDTO exhibitionClaimDTO = mapper.readValue(jsonResponse,
						new TypeReference<ExhibitionClaimDTO>() {
						});
				if (exhibitionClaimDTO == null) {
					log.info(" Exhibition claims Request List is null");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return;
				}
				exhibitionClaimDetailsDTO = exhibitionClaimDTO;
				createClaimListDTOS = new ArrayList<>();
				if (exhibitionMaster.getExhibitionEntity() != null
						&& exhibitionMaster.getExhibitionEntity().getId() != null) {
					getSalesAmount(exhibitionMaster);
				}
			} else {
				log.error(" Exhibition Claims Type list search Failed With status code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error("Exception Ocured while Loading Exhibition Claims Request list :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<--- typesOfExhibitionClaimsValues ---> " + nameOfExpoDropDown);
	}

	public String claimForExpoAddPageListAction() {
		log.info("<====== Starts ClaimForExpoBean.claimForExpoAddPageListAction() ====> action   :::" + action);
		createClaimListDTOS = new ArrayList<>();
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== claimForExpo add page called.......====>");
				loadAllCreatePageDropDown();
				return ADD_PAGE;
			}

			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmExhibistionClaimDelete').show();");
			}

		} catch (Exception e) {
			log.error("Exception Occured in PetitionRequestBean.petitionRegisterListAction()::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== ClaimForExpoBean.claimForExpoAddPageListAction() Ends====>");
		return null;
	}

	public String exhibitionClaimsClearSearch() {
		log.info("<====== Starts ClaimForExpoBean.exhibitionClaimsClearSearch() ====>");
		addButtonFlag = true;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		editButtonFlag = false;
		selectedExhibitionListPage = new ExhibitionClaimDTO();
		log.info("<====== ClaimForExpoBean.exhibitionClaimsClearSearch() Ends====>");
		return LIST_PAGE;
	}

	public String getFormattedDate(Date date) {
		log.info("<====== Starts ClaimForExpoBean.getFormattedDate()====>");
		if (date != null) {

			return simpleDateFormat.format(date);
		} else {
			return "";
		}
	}

	public void lazyExhibitionClaimsResponse() {
		log.info("<===== Starts ClaimForExpoBean.lazyExhibitionClaimsResponse ======>");
		lazyExhibitionClaimDTOSList = new LazyDataModel<ExhibitionClaimDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<ExhibitionClaimDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Exhibition Claims request :::" + paginationRequest);
					String url = SERVER_URL + "/getallexibitionclaimslistlazy";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						exhibitionClaimListPage = mapper.readValue(jsonResponse,
								new TypeReference<List<ExhibitionClaimDTO>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e)

				{
					log.error("Exception occured in lazyExhibitionClaimsResponse ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return exhibitionClaimListPage;
			}

			@Override
			public Object getRowKey(ExhibitionClaimDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public ExhibitionClaimDTO getRowData(String rowKey) {
				try {
					for (ExhibitionClaimDTO invoice : exhibitionClaimListPage) {
						if (invoice.getId().equals(Long.valueOf(rowKey))) {
							selectedExhibitionListPage = invoice;
							return invoice;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends ClaimForExpoBean.lazyExhibitionClaimsResponse() ======>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts ClaimForExpoBean.onRowSelect() ========>" + event);
		selectedExhibitionListPage = ((ExhibitionClaimDTO) event.getObject());
		log.info("<==== selectedInvoice Object =====>" + selectedExhibitionListPage);
		addButtonFlag = false;
		viewButtonFlag = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		employeeMaster = commonDataService.loadEmployeeByUser(loginBean.getUserMaster().getId());

		Long claimForExpoCreatedById = employeeMaster.getCreatedBy() == null ? null
				: employeeMaster.getCreatedBy().getId();

		Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();

		/*
		 * if (!selectedExhibitionListPage.getStatus().equals("APPROVED")) {
		 * editButtonFlag =false ;
		 * 
		 * } else { editButtonFlag = true; }
		 */

		if (selectedExhibitionListPage.getStatus().equals("REJECTED") && claimForExpoCreatedById.equals(loginUserId)) {
			editButtonFlag = false;
		}
		// deleteButtonFlag = true;
		log.info("<===Ends ClaimForExpoBean.onRowSelect() ========>");
	}

	public String parseDouble(Double amt) {
		log.info("<===Starts ClaimForExpoBean.parseDouble ========>" + amt);
		return decimalFormat.format(amt);
	}

	public void deleteCreateClaimListDTOS(CreatePageExhibitionClaimListDTO createPageExhibitionClaimListDTO) {
		log.info("<===Starts ClaimForExpoBean.deleteCreateClaimListDTOS() ========>");
		selectedCreatePageExhibitionClaimListDTO = null;
		selectedCreatePageExhibitionClaimListDTO = createPageExhibitionClaimListDTO;
		RequestContext.getCurrentInstance().execute("PF('removeDlg').show();");
	}

	public void confirmDelete() {
		log.info("<===Starts ClaimForExpoBean.deleteCreateClaimListDTOS() ========>");
		if (selectedCreatePageExhibitionClaimListDTO != null) {
			createClaimListDTOS.remove(selectedCreatePageExhibitionClaimListDTO);
		}
	}

	@Data
	public class CreatePageExhibitionClaimListDTO implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -853110766465956841L;
		private Long exhibitionId;
		private Long expenseId;
		private String horo;
		private String expoName;
		private String expoDate;
		private String expoToDate;
		private String typeOfExpenses;
		private double amount;
	}

	public void uploadDocument(FileUploadEvent event) {
		log.info("<<=== ClaimForExpoBean.uploadDocument() File Upload method called");
		try {
			UploadedFile file = event.getFile();
			fileType = file.getFileName();
			if (!fileType.contains(".pdf") && !fileType.contains(".jpg") && !fileType.contains(".png")
					&& !fileType.contains(".jpeg") && !fileType.contains(".docx") && !fileType.contains(".doc")) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.PROFILE_FILE_FORMAT_ERROR.getErrorCode());
				log.info("<<====  error type in file upload");
			}
			long size = file.getSize();
			size = (size / AppUtil.ONE_MB_IN_KB) / AppUtil.ONE_MB_IN_KB;
			// size = size / (1024*1024);
			if (size > 2) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.CREATE_RAISE_COMPLAINT_FILE_SIZE_ERROR.getErrorCode());
				log.info("<<====  Large size file uploaded");
			}
			if (size >= 0) {
				String createRaiseComplaintPathName = commonDataService.getAppKeyValue("EXPO_CLAIMS_FILE_PATH");
				String filePath = Util.fileUpload(createRaiseComplaintPathName, file);
				expoDocumentFileName = filePath;
				errorMap.notify(ErrorDescription.LTC_UPLOAD_SUCCESS.getErrorCode());
				log.info(" Uploading Document is uploaded successfully");
			}

		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.info("<<==  Error in file upload " + e.getMessage());
		}
		log.info("<======Ends ClaimForExpoBean.uploadDocument() ====> ");
	}

	public void clearNote() {
		log.info("<====== Starts ClaimForExpoBean.clearNote() ====>");
		note = "";
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
		log.info("<====== Ends ClaimForExpoBean.clearNote() ====>");
	}

	public String editExopClaimInfo() {
		log.info("<===== Starts editExopClaimInfo ======>");
		try {

			if (selectedExhibitionListPage.getExhibitionClaimId() == null) {
				AppUtil.addError("Please select one record.");
				return null;
			}

			String url = SERVER_URL + "/view/" + selectedExhibitionListPage.getExhibitionClaimId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO response = httpService.get(url);
			if (response != null) {
				if (response.getStatusCode() == 0) {

					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					selectedViewExhibitionClaimResponseDTO = mapper.readValue(jsonResponse,
							new TypeReference<ExhibitionClaimRequestDTO>() {
							});
					initializeListOfExpensesRowsData(
							selectedViewExhibitionClaimResponseDTO.getExhibitionClaimDTOList());
					this.exhibitionClaimDetailsDTO = selectedViewExhibitionClaimResponseDTO.getExhibitionClaimDTOList()
							.get(0);
					this.note = selectedViewExhibitionClaimResponseDTO.getNote();
					this.fileType = selectedViewExhibitionClaimResponseDTO.getFilePath();
					this.selectedForwardTo = selectedViewExhibitionClaimResponseDTO.getForwardToUser();
					this.reason = selectedViewExhibitionClaimResponseDTO.getRemarks();

					this.headOrRegionalOffice = commonDataService.loadHeadAndRegionalOffice();
					// this.forwardToUsersList = commonDataService.loadForwardToUsers();
					this.forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CLAIM_EXPO");
					// loadAllExpoNameDropDown();
					exhibitionMaster = new ExhibitionMaster();
					// exhibitionMaster.setId(exhibitionClaimDetailsDTO.getId());
					// exhibitionMaster.setExhibitionCode(exhibitionClaimDetailsDTO.getExpoCode());
					// exhibitionMaster.setExhibitionName(exhibitionClaimDetailsDTO.getNameOfExpo());
					//
					exhibitionMaster = exhibitionClaimDetailsDTO.getExhibitionMaster();
					EntityMaster entityMaster = new EntityMaster();
					if (exhibitionClaimDetailsDTO.getEntityId() != null) {
						entityMaster.setId(exhibitionClaimDetailsDTO.getEntityId());
						headOrRegional = exhibitionClaimDetailsDTO.getEntityId();
						getSalesAmount(exhibitionClaimDetailsDTO.getExhibitionMaster());
					}
					exhibitionMaster.setExhibitionEntity(entityMaster);
					log.info("exhibitionClaimDetailsDTO-------------" + exhibitionClaimDetailsDTO);
//					loadAllExpoNameDropDownByHeadOrRegionalOffice1();
					if (selectedForwardTo != null) {
						forwardToUsersList.forEach(userMaster -> {
							if (userMaster.getId().equals(selectedForwardTo)) {
								selectedForwardToString = userMaster.getUsername();
							}
						});
					} else {
						selectedForwardToString = "Select";
					}

					this.selectedForwardFor = selectedViewExhibitionClaimResponseDTO.getIsFinalApprove() ? true : false;

					return ADD_PAGE;

				} else {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				log.error("response is null");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in editExopClaimInfo ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends editExopClaimInfo ======>");
		return null;
	}

	public String submitAddRequest() {
		log.info("<=====Starts ClaimForExpoBean.submitAddRequest () =====>");
		ExhibitionClaimRequestDTO requestDTO = new ExhibitionClaimRequestDTO();
		if (fileType == null) {
			Util.addWarn("Document Should Not be Empty");
			return null;
		}

		if (selectedExhibitionListPage != null && selectedExhibitionListPage.getExhibitionClaimId() != null) {
			requestDTO.setExhibitionClaimId(selectedExhibitionListPage.getExhibitionClaimId());
		}
		// if(selectedForwardTo==null) {
		// requestDTO.setForwardToUser(forwardtousermaster.getId());
		// }
		selectedForwardTo = forwardtousermaster.getId();
		requestDTO.setNote(this.note);
		requestDTO.setActiveStatus(true);
		requestDTO.setFilePath(expoDocumentFileName);
		requestDTO.setForwardToUser(selectedForwardTo);
		requestDTO.setRemarks(this.reason);
		requestDTO.setIsFinalApprove(selectedForwardFor);

		Iterator<ExhibitionClaimDTO> claimDTOIterator = this.exhibitionClaimDTOList.iterator();
		while (claimDTOIterator.hasNext()) {

			ExhibitionClaimDTO exhibitionClaimDTO = claimDTOIterator.next();
			Iterator<ExpenseTypeDTO> expenseTypeIterator = exhibitionClaimDTO.getExpenseTypeDTOList().iterator();
			while (expenseTypeIterator.hasNext()) {

				ExpenseTypeDTO expenseTypeDTO = expenseTypeIterator.next();
				boolean needToRemove = true;

				Iterator<CreatePageExhibitionClaimListDTO> claimListDTOIterator = createClaimListDTOS.iterator();
				while (claimListDTOIterator.hasNext()) {
					CreatePageExhibitionClaimListDTO claimListDTO = claimListDTOIterator.next();
					if (claimListDTO.getExhibitionId().equals(exhibitionClaimDTO.getId())
							&& (claimListDTO != null && expenseTypeDTO != null && claimListDTO.getExpenseId() != null
									&& expenseTypeDTO.getId() != null
									&& claimListDTO.getExpenseId().longValue() == expenseTypeDTO.getId().longValue())) {
						needToRemove = false;
						claimListDTOIterator.remove();
						break;
					}
				}
				if (needToRemove) {
					expenseTypeIterator.remove();
				}

			}
			if (exhibitionClaimDTO.getExpenseTypeDTOList().size() == 0) {
				claimDTOIterator.remove();
			}

		}
		requestDTO.setExhibitionClaimDTOList(this.exhibitionClaimDTOList);
		log.info("<===== ClaimForExpoBean.submitAddRequest requestDTO : " + requestDTO);

		BaseDTO response = httpService.post(SERVER_URL + "/saveorupdate", requestDTO);
		if (response.getStatusCode() == 0) {
			if (requestDTO.getExhibitionClaimId() == null) {
				errorMap.notify(ErrorDescription.EXPO_CLAIM_SAVE_SUCCESS.getErrorCode());
			} else {
				errorMap.notify(
						ErrorDescription.getError(MastersErrorCode.CLAIM_FOR_EXPO_UPDATED_SUCCESSFULLY).getErrorCode());

			}
			return showExhibitionClaimList();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===== Ends ClaimForExpoBean.submitAddRequest =====>");

		return null;
	}

	public String deleteExhibistionClaim() {
		log.info("<==== Starts ClaimForExpoBean.deleteExhibistionClaim() =====>");
		String url = SERVER_URL + "/delete/" + selectedExhibitionListPage.getExhibitionClaimId();
		BaseDTO response = httpService.get(url);
		if (response.getStatusCode() == 0) {
			errorMap.notify(ErrorDescription.EXHIBISTION_CLAIM_DELETE_SUCCESS.getErrorCode());
		} else {
			errorMap.notify(ErrorDescription.EXHIBISTION_CLAIM_DELETE_FAILURE.getErrorCode());
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmExhibistionClaimDelete').hide();");
		log.info("<==== Ends ClaimForExpoBean.deleteExhibistionClaim  =====>");
		exhibitionClaimsClearSearch();
		return LIST_PAGE;

	}

	/**
	 * View Page Functionalities
	 */
	public String viewExopClaimInfo() {
		log.info("<===== Starts ClaimForExpoBean.viewExopClaimInfo oo======>");
		try {
			String url = SERVER_URL + "/view/" + selectedExhibitionListPage.getExhibitionClaimId() + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			BaseDTO response = httpService.get(url);
			if (response != null) {
				if (response.getStatusCode() == 0) {

					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					selectedViewExhibitionClaimResponseDTO = mapper.readValue(jsonResponse,
							new TypeReference<ExhibitionClaimRequestDTO>() {
							});
					if (selectedNotificationId != null) {
						systemNotificationBean.loadTotalMessages();
					}
					String jsonResponse1 = mapper.writeValueAsString(response.getResponseContents());
					employeeMasterData = mapper.readValue(jsonResponse1, new TypeReference<List<EmployeeMaster>>() {
					});

					// Approve and Reject Comments
					getComments();

					initializeListOfExpensesRowsData(
							selectedViewExhibitionClaimResponseDTO.getExhibitionClaimDTOList());
					// forwardToUsersList = commonDataService.loadForwardToUsers();
					forwardToUsersList = commonDataService.loadForwardToUsersByFeature("CLAIM_EXPO");
					viewPageExhibitionClaimDetails = selectedViewExhibitionClaimResponseDTO.getExhibitionClaimDTOList()
							.get(0);
					getSalesAmount(viewPageExhibitionClaimDetails.getExhibitionMaster());
					salesAmount = AppUtil.doubleFormat(salesAmount);
					note = selectedViewExhibitionClaimResponseDTO.getNote();
					/*
					 * if (selectedExhibitionListPage.getStatus() != null &&
					 * selectedExhibitionListPage.getStatus().equals(ExhibitionClaimsStatus.APPROVED
					 * .name()) ||
					 * selectedExhibitionListPage.getStatus().equals(ExhibitionClaimsStatus.REJECTED
					 * .name())) { forwardToForFlag = false; approveRejectFlag = false; } else {
					 * forwardToForFlag = true; approveRejectFlag = true; }
					 */

					if (selectedViewExhibitionClaimResponseDTO.getForwardToUser() != null) {
						if (selectedViewExhibitionClaimResponseDTO.getForwardToUser().longValue() == loginBean
								.getUserMaster().getId().longValue()) {
							approveRejectFlag = true;
							if (!selectedViewExhibitionClaimResponseDTO.getIsFinalApprove().equals(true)) {
								forwardToForFlag = true;
							} else {
								forwardToForFlag = false;
							}
						} else {
							forwardToForFlag = false;
							approveRejectFlag = false;
						}
					}

					// if (selectedViewExhibitionClaimResponseDTO.getForwardToUser().longValue()
					// ==loginBean.getUserMaster().getId().longValue()
					// && selectedViewExhibitionClaimResponseDTO.getIsFinalApprove().equals(true) )
					// {
					// setHideflag(true);
					// }
					// if (selectedViewExhibitionClaimResponseDTO.getForwardToUser().longValue()
					// ==loginBean.getUserMaster().getId().longValue()
					// && selectedViewExhibitionClaimResponseDTO.getIsFinalApprove().equals(false) )
					// {
					// setHideflag(false);
					// }
					if (selectedExhibitionListPage.getStatus().equalsIgnoreCase("FINAL_APPROVED")
							|| selectedExhibitionListPage.getStatus().equalsIgnoreCase("REJECTED")) {

						hideflag = false;
					}
					if (!selectedViewExhibitionClaimResponseDTO.getForwardToUser()
							.equals(loginBean.getUserDetailSession().getId())) {
						hideflag = false;
					}

					return VIEW_PAGE;

				} else {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in viewExopClaimInfo() ...", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<===== Ends ClaimForExpoBean.viewExopClaimInfo () ======>");
		return null;
	}

	public String statusUpdate() {
		log.info("<===== Starts ClaimForExpoBean.statusUpdate() =====>");

		ExhibitionClaimRequestDTO requestDTO = new ExhibitionClaimRequestDTO();
		if (selectedForwardFor != null) {
			selectedForwardTo = forwardtousermaster.getId();
		}
		requestDTO.setExhibitionClaimId(selectedViewExhibitionClaimResponseDTO.getExhibitionClaimId());
		requestDTO.setNote(this.note);
		requestDTO.setActiveStatus(true);
		requestDTO.setRemarks(this.reason);
		requestDTO.setActionName(approveOrReject);
		requestDTO.setForwardToUser(selectedForwardTo);
		requestDTO.setIsFinalApprove(selectedForwardFor);

		log.info("<===== ClaimForExpoBean.approveOrReject requestDTO : " + requestDTO);

		BaseDTO response = httpService.post(SERVER_URL + "/changestatus", requestDTO);
		if (response.getStatusCode() == 0) {
			if (approveOrReject.equals("APPROVED")) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.CLAIM_FOR_EXPO_APPROVED_SUCCESSFULLY)
						.getErrorCode());
			}
			if (approveOrReject.equals("FINAL_APPROVED")) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.CLAIM_FOR_EXPO_FINAL_APPROVED_SUCCESSFULLY)
						.getErrorCode());
			}
			if (approveOrReject.equals("REJECTED")) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.CLAIM_FOR_EXPO_REJECTED_SUCCESSFULLY)
						.getErrorCode());
			}

			reason = "";
			return showExhibitionClaimList();
		} else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<===== Ends ClaimForExpoBean.approveOrReject=====>");

		return null;
	}

	public void finalApprove() {
		log.info("<===== Starts ClaimForExpoBean.finalApprove()=====>");
		approveOrReject = ExhibitionClaimsStatus.FINAL_APPROVED.name();
		RequestContext.getCurrentInstance().execute("PF('dlgComments1').show();");
	}

	public void approve() {
		log.info("<===== Starts ClaimForExpoBean.approve()=====>");
		approveOrReject = ExhibitionClaimsStatus.APPROVED.name();
		if (!selectedViewExhibitionClaimResponseDTO.getIsFinalApprove().equals(true)) {
			approveOrReject = ExhibitionClaimsStatus.APPROVED.name();
			if (selectedForwardFor == null) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDFOR.getErrorCode());
				return;
			}
			if (forwardtousermaster == null) {
				errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDTO.getErrorCode());
				return;
			}
		}
		RequestContext.getCurrentInstance().execute("PF('dlgComments1').show();");
	}

	public void reject() {
		log.info("<===== Starts ClaimForExpoBean.reject()=====>");
		approveOrReject = ExhibitionClaimsStatus.REJECTED.name();
		// if(!selectedViewExhibitionClaimResponseDTO.getIsFinalApprove().equals(true))
		// {
		// if (selectedForwardFor == null) {
		// errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDFOR.getErrorCode());
		// return;
		// }
		// if (forwardtousermaster == null) {
		// errorMap.notify(ErrorDescription.POLICY_NOTE_PROCESS_FORWARDTO.getErrorCode());
		// return;
		// }
		// }
		RequestContext.getCurrentInstance().execute("PF('dlgComments1').show();");
	}

	public void downloadFile() {
		InputStream input = null;
		try {
			File file = new File(this.selectedViewExhibitionClaimResponseDTO.getFilePath());
			input = new FileInputStream(file);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			this.setFile(
					new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
		} catch (FileNotFoundException e) {
			log.error("FileNotFoundException in filedownload method------- " + e);
		} catch (Exception e) {
			log.error("exception in filedownload method------- " + e);
		}
	}

	public void getSalesAmount(ExhibitionMaster exhibitionMaster) {
		log.info("getSalesAmount Method Start::");
		try {
			String url = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/salesinvoice/getsalesamount";
			log.info("url=", url);
			BaseDTO response = httpService.post(url, exhibitionMaster);
			if (response != null && response.getStatusCode() == 0) {

				jsonResponse = mapper.writeValueAsString(response.getSumTotal());
				salesAmount = mapper.readValue(jsonResponse, new TypeReference<Double>() {
				});
				if (salesAmount == null) {
					salesAmount = 0.0;
				}
			}
		} catch (Exception ex) {
			log.error("error in getSalesAmount Method", ex);
		}
		log.info("getSalesAmount Method End::");
	}

	public void getComments() {
		try {
			viewResponseDTO = new ExhibitionClaimRequestDTO();
			BaseDTO response = httpService
					.get(SERVER_URL + "/getapproverejectcomments/" + selectedExhibitionListPage.getExhibitionClaimId());
			if (response != null && response.getStatusCode() == ErrorDescription.SUCCESS_RESPONSE.getCode()) {
				jsonResponse = mapper.writeValueAsString(response.getResponseContent());
				viewResponseDTO = mapper.readValue(jsonResponse, new TypeReference<ExhibitionClaimRequestDTO>() {
				});
			}
		} catch (Exception e) {
			log.error("Exception occured while getComments", e);
		}

	}

	public void clear() {
		log.info("-----clear method starts--------");
		loadAllExpoNameDropDown();
		exhibitionMaster = new ExhibitionMaster();
		this.exhibitionClaimDetailsDTO = new ExhibitionClaimDTO();
		this.selectedForwardForSelect = "Select";
		this.selectedForwardFor = null;
		this.selectedForwardTo = null;
		this.selectedForwardToString = "Select";
		totalExpoAmount = 0.00;
		this.reason = null;
		this.note = null;
		salesAmount = null;
		headOrRegional = null;
		fileType = null;
		createClaimListDTOS = new ArrayList<>();
		log.info("-----clear method ends--------");
	}

}
