package in.gov.cooptex.operation.rest.ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CallLetterNotificationMasterDto;
import in.gov.cooptex.core.dto.OralTestMarkSearchRequest;
import in.gov.cooptex.core.dto.OralTestMarkSearchResponse;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.OralTestMark;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("oralTestMarkBean")
public class OralTestMarkBean implements Serializable {

	private static final long serialVersionUID = 2326838305929530708L;

	public static final String ORAL_URL = AppUtil.getPortalServerURL() + "/oraltestmark";

	@Getter
	@Setter
	public String action, status, comments;

	String serverURL = null;

	@Autowired
	CommonDataService commonDataService;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	boolean addButtonFlag = false;

	@Getter
	@Setter
	boolean approveButtonFlag = false;

	@Getter
	@Setter
	boolean deleteButtonFlag = false;

	ObjectMapper mapper;

	@Getter
	@Setter
	OralTestMarkSearchRequest oralTestMarkSearchRequest;

	@Getter
	@Setter
	OralTestMarkSearchResponse selectedOralTestMark;

	String url;

	@Getter
	@Setter
	List<CallLetterNotificationMasterDto> notificationNumberList;

	@Getter
	@Setter
	CallLetterNotificationMasterDto notificationDto = new CallLetterNotificationMasterDto();;

	@Getter
	@Setter
	String notificationNo;

	@Getter
	@Setter
	String recruitmentForPost;

	@Getter
	@Setter
	Integer recruitmentYear;

	@Getter
	@Setter
	List<OralTestMark> oralTestMarkList = new ArrayList<>();

	@Getter
	@Setter
	OralTestMark oralTestMark = new OralTestMark();;

	@Getter
	@Setter
	List<Designation> designationList;

	@PostConstruct
	public void init() {
		oralTestMark = new OralTestMark();
		loadMasterValue();
		loadLazyEmployeeList();
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	}

	@Setter
	@Getter
	String FormattedTime = "HH:mm";

	@Getter
	@Setter
	LazyDataModel<OralTestMarkSearchResponse> oralTestMarkResponseLazyList;

	String jsonResponse;

	public void loadLazyEmployeeList() {

		log.info("< --  Start OralTestMark Lazy Load -- > ");
		oralTestMarkResponseLazyList = new LazyDataModel<OralTestMarkSearchResponse>() {

			private static final long serialVersionUID = -4079882975619852359L;

			@Override
			public List<OralTestMarkSearchResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					List<OralTestMarkSearchResponse> data = mapper.readValue(jsonResponse,
							new TypeReference<List<OralTestMarkSearchResponse>>() {
							});
					if (data == null) {
						log.info(" OralTestMarkSearchResponse Failed to Deserialize");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(
								" OralTestMark Search Successfully Completed of Total record :[ " + totalRecords + "]");
					} else {
						log.error(" OralTestMark Search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return data;
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading OralTestMark data :: s", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(OralTestMarkSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public OralTestMarkSearchResponse getRowData(String rowKey) {
				List<OralTestMarkSearchResponse> responseList = (List<OralTestMarkSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (OralTestMarkSearchResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedOralTestMark = res;
						return res;
					}
				}
				return null;
			}
		};

	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			OralTestMarkSearchRequest request = new OralTestMarkSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("notificationNo")) {
					log.info("notificationNo : " + value);
					request.setNotificationNo(value);
				}
				if (entry.getKey().equals("recruitmentPost")) {
					log.info("recruitmentPost : " + value);
					request.setRecruitmentPost(value);
				}
				if (entry.getKey().equals("recruitmentYear")) {
					log.info("recruitmentYear : " + value);
					request.setRecruitmentYear(Long.parseLong(value));
				}
				if (entry.getKey().equals("oralExamDate")) {
					log.info("oralExamDate : " + value);
					request.setOralExamDate(AppUtil.serverDateFormat(value));
				}
				if (entry.getKey().equals("status")) {
					log.info("status : " + value);
					request.setStatus(value);
				}
			}
			log.info("Search request data" + request);
			baseDTO = httpService.post(ORAL_URL + "/search", request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in OralTestMark search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}

	public String showPage() {
		log.info("::OralTestMark Page Button ::" + action);
		if (action.equalsIgnoreCase("ADD")) {
			notificationDto = null;
			recruitmentForPost = null;
			recruitmentYear = null;
			oralTestMarkList = new ArrayList<>();
			return "/pages/personnelHR/createOralMark.xhtml?faces-redirect=true;";
		} else if (action.equalsIgnoreCase("APPROVE")) {
			if (selectedOralTestMark == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			comments = null;
			oralTestMarkList = getById(selectedOralTestMark.getId());
			return "/pages/personnelHR/approvalOralMark.xhtml?faces-redirect=true;";
		} else if (action.equalsIgnoreCase("VIEW")) {
			if (selectedOralTestMark == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}
			recruitmentForPost = null;
			recruitmentYear = null;
			oralTestMarkList = getById(selectedOralTestMark.getId());
			return "/pages/personnelHR/viewOralMark.xhtml?faces-redirect=true;";
		} else if (action.equalsIgnoreCase("CANCEL") || action.equalsIgnoreCase("ORALTESTMARKLIST")) {
			addButtonFlag = false;
			approveButtonFlag = false;
			selectedOralTestMark = new OralTestMarkSearchResponse();
			return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true;";
		}
		return null;
	}

	public void loadMasterValue() {
		log.info("loadMasterValue start");
		try {
			notificationNumberList = commonDataService.loadOralSeletionNotficationNumber();
			designationList = commonDataService.loadDesignation();
		} catch (Exception e) {
			log.error("found exception while loadMasterValue ", e);
		}

	}

	public void onChangeNotificationNumber() {
		try {
			if (notificationDto != null && notificationDto.getNotificationNo() != null) {
				log.info("onChangeNotificationNumber called : [ " + notificationDto.getNotificationNo() + " ]");
				recruitmentForPost = notificationDto.getPostName();
				recruitmentYear = notificationDto.getReqYear();
			} else {
				recruitmentForPost = null;
				recruitmentYear = null;
			}
		} catch (Exception e) {
			log.error("found exception in onChangeNotificationNumber", e);
		}
	}

	public String generateOralTestMark() {
		BaseDTO baseDto = new BaseDTO();
		log.info("generateOralTestMark called..");

		try {
			url = ORAL_URL + "/generateoraltestmark";
			log.info("Request Url : " + url);
			baseDto = httpService.post(url, notificationDto);
			String jsonResponse = mapper.writeValueAsString(baseDto.getResponseContent());
			oralTestMarkList = mapper.readValue(jsonResponse, new TypeReference<List<OralTestMark>>() {
			});
			if (oralTestMarkList.size() == 0) {
				errorMap.notify(ErrorDescription.ORAL_TEST_MARK_LIST_IS_EMPTY.getErrorCode());
				return null;
			}
			log.info("after generated oral test mark : [ " + oralTestMarkList + " ]");
			log.info("total size of genrated oral test mark is :  [" + oralTestMarkList.size() + "]");
		} catch (Exception e) {
			log.error("found exception in generateOralTestMark ", e);
			errorMap.notify(baseDto.getStatusCode());
		}

		return null;
	}

	public String deleteOralTestMark() {

		try {
			if (selectedOralTestMark == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			} else if (selectedOralTestMark.getStatus().equals("APPROVED")) {
				errorMap.notify(ErrorDescription.RECORD_APPROVED.getErrorCode());
				return null;
			} else if (selectedOralTestMark.getStatus().equals("REJECTED")) {
				errorMap.notify(ErrorDescription.RECORD_REJECTED.getErrorCode());
				return null;
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete OralTestMark  :: ", e);
		}
		return null;
	}

	public String deleteOralTestMarkConfirm() {
		BaseDTO baseDTO = new BaseDTO();
		try {
			log.info("Selected OralTestMark  Id : : " + selectedOralTestMark.getId());
			String url = ORAL_URL + "/delete/id/" + selectedOralTestMark.getId();
			log.info("Oral Test Mark Delete URL called : - " + url);
			baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDTO.getStatusCode() == 0) {
					log.info(" OralTestMark Deleted SuccessFully ");
					errorMap.notify(ErrorDescription.ORAL_TEST_MARK_DELETED_SUCCESSFULLY.getErrorCode());
					addButtonFlag = false;
					selectedOralTestMark = new OralTestMarkSearchResponse();
					return null;
				} else {
					log.error(" OralTestMark Delete Operation Failed with status code " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}

		} catch (Exception e) {
			log.error(" Exception Occured While Delete OralTestMark  :: ", e);
			errorMap.notify(baseDTO.getStatusCode());
		}
		return null;
	}

	public void approveOrRejectDialogue() {
		if (status.equals("APPROVED"))
			status = "APPROVED";
		else if (status.equals("REJECTED"))
			status = "REJECTED";
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('confirmStatus').show();");
	}

	public String createOralTestMark() {
		BaseDTO baseDto = new BaseDTO();
		log.info(":: Create Exam Centre start::");
		try {
			String url = ORAL_URL + "/create";
			log.info("Create Oral Test Mark URL :" + url);
			baseDto = httpService.post(url, oralTestMarkList);
			log.info("CreateOralTestMark BaseDTO response:" + baseDto);
			if (baseDto == null) {
				log.info("::OralTestMark details response null. ::" + baseDto);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			} else {
				if (baseDto.getStatusCode() == 0) {
					log.info("::OralTestMark details saved successfully. ::" + baseDto);
					errorMap.notify(ErrorDescription.ORAL_TEST_MARK_SAVED_SUCCESSFULLY.getErrorCode());
					return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true;";
				} else {
					log.info("::OralTestMark details failed to save. ::" + baseDto);
					errorMap.notify(baseDto.getStatusCode());
					return null;
				}
			}
		} catch (Exception e) {
			log.error(":: Exception in creatingOralTestMark", e);
			errorMap.notify(baseDto.getStatusCode());
		}
		return null;

	}

	public String updateOralTestMark() {
		BaseDTO baseDto = new BaseDTO();
		log.info("updateOralTestMark OralTestMark start");
		try {
			if (selectedOralTestMark != null && selectedOralTestMark.getId() != null) {
				oralTestMarkList = getById(selectedOralTestMark.getId());
				oralTestMarkList.forEach(oral -> {
					if (status != null && status.equals("APPROVED"))
						oral.setStatus("APPROVED");
					else if (status != null && status.equals("REJECTED")) {
						oral.setStatus("REJECTED");
					}
					oral.setRemarks(comments);
				});
				String url = ORAL_URL + "/update";
				log.info("updateOralTestMark OralTestMark URL Request :" + url);
				baseDto = httpService.put(url, oralTestMarkList);
				log.info("SaveOralTestMark BaseDTO response:" + baseDto);
				if (baseDto == null) {
					log.info(":: OralTestMark BaseDTO Response Null:: " + baseDto);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				} else {
					if (baseDto.getStatusCode() == 0) {
						log.info(":: update OralTestMark updated successfully. ::" + baseDto);
						if (status != null && status.equals("APPROVED"))
							errorMap.notify(ErrorDescription.ORAL_TEST_MARK_APPROVED_SUCCESSFULLY.getErrorCode());
						else if (status != null && status.equals("REJECTED"))
							errorMap.notify(ErrorDescription.ORAL_TEST_MARK_REJECTED_SUCCESSFULLY.getErrorCode());

						addButtonFlag = false;
						selectedOralTestMark = new OralTestMarkSearchResponse();
						return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true";
					} else {
						log.info(":: update OralTestMark failed to save. ::" + baseDto);
						errorMap.notify(baseDto.getStatusCode());
						return null;
					}
				}

			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}

		} catch (Exception e) {
			log.error(":: Exception in Updating OralTestMark", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return null;
	}

	public List<OralTestMark> getById(Long id) {
		List<OralTestMark> oralTestMarkList = new ArrayList<>();
		BaseDTO baseDto = new BaseDTO();
		try {
			url = ORAL_URL + "/get/id/" + id;
			baseDto = httpService.get(url);
			if (baseDto != null) {
				oralTestMark = mapper.readValue(mapper.writeValueAsString(baseDto.getResponseContent()),
						OralTestMark.class);
				oralTestMarkList = mapper.readValue(mapper.writeValueAsString(baseDto.getResponseContents()),
						new TypeReference<List<OralTestMark>>() {
						});
				oralTestMarkList.forEach(oral -> {
					log.info("oral.getOralExamDate()" + oral.getOralExamDate());
					notificationNo = oral.getNotificationNumber();
					notificationNumberList = commonDataService.loadOralSeletionNotficationNumber();
					notificationNumberList.forEach(noti -> {
						if (noti.getNotificationNo().equals(notificationNo)) {
							recruitmentForPost = noti.getPostName();
							recruitmentYear = noti.getReqYear();
						}
					});
				});
			}
		} catch (Exception e) {
			log.error("exception occured in getById", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return oralTestMarkList;
	}

	public void clearOralTestExam() {
		log.info("clearOralTestExam called..");
		notificationDto = new CallLetterNotificationMasterDto();
		oralTestMarkList = new ArrayList<>();
		recruitmentForPost = null;
		recruitmentYear = null;
	}

	public String clear() {
		log.info(":: clearOralTestMark called..");
		selectedOralTestMark = new OralTestMarkSearchResponse();
		addButtonFlag = false;
		approveButtonFlag = false;
		return "/pages/personnelHR/listOralMark.xhtml?faces-redirect=true";
	}

	public void onRowSelect(SelectEvent event) {
		log.info("Row Select event called");
		selectedOralTestMark = ((OralTestMarkSearchResponse) event.getObject());
		addButtonFlag = true;
		if (selectedOralTestMark.getStatus().equalsIgnoreCase("INPROGRESS")) {
			approveButtonFlag = false;
		} else if (selectedOralTestMark.getStatus().equalsIgnoreCase("APPROVED")) {
			approveButtonFlag = true;

		} else if (selectedOralTestMark.getStatus().equalsIgnoreCase("REJECTED")) {
			approveButtonFlag = true;

		}
	}

}
