package in.gov.cooptex.personnel.hrms.rest.ui;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.EmployeeBonusDTO;
import in.gov.cooptex.core.dto.IncrementDTO;
import in.gov.cooptex.core.dto.IncrementForwardDTO;
import in.gov.cooptex.core.dto.IncrementListDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.Designation;
import in.gov.cooptex.core.model.EmpIncrement;
import in.gov.cooptex.core.model.EmpIncrementLog;
import in.gov.cooptex.core.model.EmpIncrementNote;
import in.gov.cooptex.core.model.EmployeeBonus;
import in.gov.cooptex.core.model.EmployeeBonusDetails;
import in.gov.cooptex.core.model.EmployeeBonusLog;
import in.gov.cooptex.core.model.EmployeeBonusNote;
import in.gov.cooptex.core.model.EmployeeIncrementDetails;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.FinancialYear;
import in.gov.cooptex.core.model.IncrementCycle;
import in.gov.cooptex.core.model.PayScaleMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.ui.SystemNotificationBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorCodeDescription;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.OperationErrorCode;
import in.gov.cooptex.exceptions.PersonnalErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("incrementBean")
public class IncrementBean extends CommonBean {

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	final String ADD_PAGE = "/pages/personnelHR/createIncrementAdmin.xhtml?faces-redirect=true";

	final String VIEW_PAGE = "/pages/personnelHR/viewIncrementAdmin.xhtml?faces-redirect=true";

	final String LIST_PAGE = "/pages/personnelHR/listIncrementAdmin.xhtml?faces-redirect=true";

	final String PROMOTION_AND_INCREMENT_PERCENTAGE = "PROMOTION_AND_INCREMENT_PERCENTAGE";

	final String BONUS_PERCENTAGE = "BONUS_PERCENTAGE";

	final String INCREMENT_BASIC_PERCENTAGE = "INCREMENT_BASIC_PERCENTAGE";

	final String REJECT = "REJECT";

	final String APPROVED = "APPROVED";

	final Integer percentageValue = 100;

	ObjectMapper mapper;

	String url, jsonResponse;

	@Autowired
	HttpService httpService;

	@Autowired
	SystemNotificationBean systemNotificationBean;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	AppPreference appPreference;

	@Getter
	@Setter
	Double eligibleBonusAmount;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	Boolean isAddButton, isDeleteButton, isEdit;

	@Getter
	@Setter
	String pageAction, incrementRemarks, incrementFinalApprovalRemarks, incrementRejectRemarks;

	@Getter
	@Setter
	List<EntityMaster> headRegionOfficeList;

	@Getter
	@Setter
	EntityMaster headRegionOffice = new EntityMaster();

	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();

	@Getter
	@Setter
	UserMaster forwardToUser = new UserMaster();

	@Getter
	@Setter
	Date currentDate = new Date();

	@Getter
	@Setter
	Boolean financialYearFlag = false;

	@Getter
	@Setter
	List<IncrementCycle> incrementCycleList = new ArrayList<>();

	@Getter
	@Setter
	List<IncrementDTO> incrementDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeBonusDTO> employeeBonusDTOList = new ArrayList<>();

	@Getter
	@Setter
	EmployeeBonusDTO employeeBonusDTO = new EmployeeBonusDTO();

	@Getter
	@Setter
	List<IncrementDTO> selectedIncrementDTOList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeBonusDTO> selectedBonusDTOList = new ArrayList<>();

	@Getter
	@Setter
	Double totalAmount;

	@Getter
	@Setter
	Double netPayableAmount;

	@Getter
	@Setter
	IncrementCycle incrementCycle = new IncrementCycle();

	@Getter
	@Setter
	List<FinancialYear> financialYearList;

	@Getter
	@Setter
	FinancialYear financialYear;

	@Getter
	@Setter
	Boolean forwardFor, isIncrementEmployeeStatus = false;

	@Getter
	@Setter
	Double incrementPercentage = null;

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	EmployeeMaster employeeMaster;

	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();

	@Getter
	@Setter
	String note;

	@Getter
	@Setter
	LazyDataModel<IncrementListDTO> incrementListDTOLazyList;

	@Getter
	@Setter
	List<IncrementListDTO> incrementListDTOList;

	@Getter
	@Setter
	IncrementListDTO selectedIncrementListDTORequest = new IncrementListDTO();

	@Getter
	@Setter
	List<EmployeeIncrementDetails> employeeIncrementDetailList = null;

	@Getter
	@Setter
	List<EmpIncrementLog> empIncrementLogList = new ArrayList<>();

	@Getter
	@Setter
	List<EmployeeBonusLog> employeeBonusLogList = new ArrayList<>();

	@Getter
	@Setter
	EmpIncrement empIncrement = new EmpIncrement();

	@Getter
	@Setter
	EmployeeBonus employeeBonus = new EmployeeBonus();

	@Getter
	@Setter
	EmployeeBonusDetails employeeBonusDetails = new EmployeeBonusDetails();

	@Getter
	@Setter
	Integer totalRecords = 0;

	@Getter
	@Setter
	Boolean viewFlag = false;

	@Getter
	@Setter
	Long noteId;

	@Getter
	@Setter
	Long tempForwardToUserId;

	@Getter
	@Setter
	String finalApproveStage;

	@Getter
	@Setter
	List<EntityMaster> loginUserHeadRegionOfficeList;

	@Getter
	@Setter
	IncrementDTO incrementDTO;

	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Getter
	@Setter
	Boolean hideFlag = false;

	@PostConstruct
	public void init() {
		log.info("IncrementBean Init is executed.....................");
		selectedIncrementDTOList = new ArrayList<>();
		selectedBonusDTOList = new ArrayList<>();
		mapper = new ObjectMapper();
		loadIncrementInformation();
		// loadEmployeeLoggedInUserDetails();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	/**
	 * Purpose : to load increment basic information (head office or region
	 * office,forward user and increment cycle)
	 */
	public void loadIncrementInformation() {
		log.info("<=IncrementBean :: loadIncrementInformation=>");
		try {
			loginUserHeadRegionOfficeList = new ArrayList<EntityMaster>();
			headRegionOfficeList = commonDataService.loadHeadAndRegionalOffice();
			for (EntityMaster entityMaster : headRegionOfficeList) {
				if (loginBean.getEmployee() != null && loginBean.getEmployee().getPersonalInfoEmployment() != null) {
					if (entityMaster.getId()
							.equals(loginBean.getEmployee().getPersonalInfoEmployment().getWorkLocation().getId())) {
						loginUserHeadRegionOfficeList.add(entityMaster);
					}
				}
			}

			employeeMaster = commonDataService.loadEmployeeParticularDetailsLoggedInUser();
			// forwardToUsersList =
			// commonDataService.loadForwardToUsersByFeature("INCREMENT");
			// forwardToUsersList=getForwardToList();
			loadIncrementCycle();
		} catch (Exception e) {
			log.error("Exception in loadIncrementInformation  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * purpose : to load increment cycle
	 */
	public void loadIncrementCycle() {
		log.info("<=IncrementBean :: loadIncrementCycle=>");
		try {
			url = SERVER_URL + "/incrementcycle/getAll";
			log.info("loadIncrementCycle :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					incrementCycleList = mapper.readValue(jsonValue, new TypeReference<List<IncrementCycle>>() {
					});
					log.info("IncrementBean :: incrementCycleList==> " + incrementCycleList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadIncrementCycle  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * purpose : to get logged user to display the notes
	 */
	/*
	 * public void loadEmployeeLoggedInUserDetails() {
	 * log.info("<=IncrementBean :: loadEmployeeLoggedInUserDetails=>"); BaseDTO
	 * baseDTO = null; try { String url = appPreference.getPortalServerURL() +
	 * "/employee/findempdetailsbyloggedinuser/" +
	 * loginBean.getUserMaster().getId();
	 * 
	 * baseDTO = httpService.get(url); if (baseDTO != null &&
	 * baseDTO.getStatusCode() == 0) { ObjectMapper mapper = new ObjectMapper();
	 * String jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); employeeMaster =
	 * mapper.readValue(jsonResponse, EmployeeMaster.class); } else {
	 * log.error(" Employee Details Not Found for the User " +
	 * loginBean.getUserMaster().getId()); return; } } catch (Exception e) {
	 * log.error("Exception Occured While loadEmployeeLoggedInUserDetails() :: ",
	 * e); } }
	 */

	/**
	 * purpose : to call Increment page
	 * 
	 * @return
	 */
	public String callIncrementPages() {
		log.info("<=IncrementBean :: callIncrementPages=>");
		try {
			if (pageAction.equalsIgnoreCase("LIST")) {
				isAddButton = true;
				isDeleteButton = true;
				loadLazyIncrementList();
				return LIST_PAGE;
			} else if (pageAction.equalsIgnoreCase("ADD")) {
				selectedIncrementDTOList = new ArrayList<>();
				selectedBonusDTOList = new ArrayList<>();
				incrementCycle = new IncrementCycle();
				headRegionOffice = new EntityMaster();
				incrementDTOList = new ArrayList<>();
				incrementPercentage = null;
				forwardToUser = null;
				forwardFor = null;
				boolean incrementFlag = false;
				if (incrementListDTOLazyList != null) {
					incrementFlag = incrementListDTOList.stream().anyMatch(p -> p.getStatus() != null
							&& ("SUBMITTED".equals(p.getStatus()) || "APPROVED".equals(p.getStatus())));
					// for (IncrementListDTO incrementListDTO : incrementListDTOLazyList) {
					// if(incrementListDTO.getStatus()!=null &&
					// !incrementListDTO.getStatus().isEmpty()) {
					// if(incrementListDTO.getStatus().equalsIgnoreCase("SUBMITTED")) {
					// incrementFlag=true;
					// break;
					// }else if(incrementListDTO.getStatus().equalsIgnoreCase("APPROVED")) {
					// incrementFlag=true;
					// break;
					// }
					// }
					// }
				}
				/*
				 * if (incrementFlag) { errorMap.notify(ErrorDescription
				 * .getError(PersonnalErrorCode.EMP_INCREMENT_PROCESS_PENDING_ERROR_MESSAGE).
				 * getCode()); return null; }
				 */

				loadAllFinancialYear();
				incrementRemarks = null;
				incrementFinalApprovalRemarks = null;
				incrementRejectRemarks = null;
				return ADD_PAGE;
			} else if (pageAction.equalsIgnoreCase("VIEW")) {
				if (selectedIncrementListDTORequest == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				finalApproveStage = selectedIncrementListDTORequest.getStatus();
				incrementRemarks = null;
				incrementFinalApprovalRemarks = null;
				incrementRejectRemarks = null;

				if (selectedIncrementListDTORequest.getIncrementCycle().equalsIgnoreCase("Bonus")) {
					return viewBonus();
				} else {
					return viewIncrement();

				}
			} else if (pageAction.equalsIgnoreCase("DELETE")) {
				if (selectedIncrementListDTORequest == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmUserDelete').show();");
				return "";
			} else if (pageAction.equalsIgnoreCase("EDIT")) {
				if (selectedIncrementListDTORequest != null
						&& !"REJECTED".equalsIgnoreCase(selectedIncrementListDTORequest.getStatus())) {
					AppUtil.addWarn("This Record cannot Edit");
					return null;
				}
				loadIncrementInformation();
				editIncrement();
				return ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in callIncrementPages  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * purpose : to call the increment view page
	 * 
	 * @return
	 */
	public String viewIncrement() {
		log.info("<==== Starts IncrementBean.viewIncrement =====>");
		try {
			Long incrementId = selectedIncrementListDTORequest.getIncrementId();
			log.info("viewIncrement :: incrementId==> " + incrementId);
			String url = SERVER_URL + "/increment/getincrementbyid/" + incrementId + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			;
			log.info("viewIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get increment value
				empIncrement = mapper.readValue(jsonResponse, EmpIncrement.class);
				headRegionOffice = empIncrement.getEntityMaster();
				if (headRegionOffice != null) {
					log.info("head office region office id==> " + headRegionOffice.getId());
				} else {
					log.error("Head office / Region office not found");
				}
				incrementCycle = empIncrement.getIncrementCycle();
				if (incrementCycle != null) {
					log.info("Increment cycle id==> " + incrementCycle.getId());
				} else {
					log.error("Increment cycle not found");
				}
				// --to get increment details
				getEmployeeIncrementDetailsByIncrementId(incrementId);
				// -- to get increment note details
				if (empIncrement.getEmpIncrementNoteList() != null
						&& !empIncrement.getEmpIncrementNoteList().isEmpty()) {
					for (EmpIncrementNote empIncrementNote : empIncrement.getEmpIncrementNoteList()) {
						noteId = empIncrementNote.getId();
						forwardToUser = empIncrementNote.getForwardTo();
						log.info("forwardToUser id=> " + forwardToUser.getId());
						forwardFor = empIncrementNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						note = empIncrementNote.getNote();
						tempForwardToUserId = forwardToUser.getId();
						log.info("note=> " + note);
					}
					// if(forwardToUser.getId() == )
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = forwardToUser.getId();

					log.info("loginUserId=> " + loginUserId);
					log.info("noteUserId=> " + noteUserId);
					if (loginUserId.equals(noteUserId)) {
						viewFlag = true;
					} else {
						viewFlag = false;
					}
				} else {
					log.error("Increment note not found");
				}
				// --to get increment log details
				if (empIncrement.getEmpIncrementLogList() != null && !empIncrement.getEmpIncrementLogList().isEmpty()) {
					empIncrementLogList = empIncrement.getEmpIncrementLogList();
				} else {
					log.error("Increment log not found");
				}

				selectedNotificationId = null;
				systemNotificationBean.loadTotalMessages();

				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in viewIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String viewBonus() {
		log.info("<==== Starts IncrementBean.viewIncrement =====>");
		try {
			Long bonusId = selectedIncrementListDTORequest.getIncrementId();
 			log.info("viewIncrement :: incrementId==> " + bonusId);
			String url = SERVER_URL + "/increment/getbonusbyid/" + bonusId + "/"
					+ (selectedNotificationId != null ? selectedNotificationId : 0);
			;
			log.info("viewIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get increment value
				employeeBonus = mapper.readValue(jsonResponse, EmployeeBonus.class);
				headRegionOffice = employeeBonus.getEntityMaster();
				if (headRegionOffice != null) {
					log.info("head office region office id==> " + headRegionOffice.getId());
				} else {
					log.error("Head office / Region office not found");
				}
				incrementCycle = employeeBonus.getIncrementCycle();
				if (incrementCycle != null) {
					log.info("Increment cycle id==> " + incrementCycle.getId());
				} else {
					log.error("Increment cycle not found");
				}
				// --to get increment details
				getEmployeeBonusDetailsByBonusId(bonusId);
				// -- to get increment note details
				if (employeeBonus.getEmployeeBonusNoteList() != null
						&& !employeeBonus.getEmployeeBonusNoteList().isEmpty()) {
					for (EmployeeBonusNote employeeBonusNote : employeeBonus.getEmployeeBonusNoteList()) {
						noteId = employeeBonusNote.getId();
						forwardToUser = employeeBonusNote.getForwardTo();
						log.info("forwardToUser id=> " + forwardToUser.getId());
						forwardFor = employeeBonusNote.getFinalApproval();
						log.info("forwardFor=> " + forwardFor);
						note = employeeBonusNote.getNote();
						tempForwardToUserId = forwardToUser.getId();
						log.info("note=> " + note);
					}
					// if(forwardToUser.getId() == )
					Long loginUserId = loginBean.getUserMaster().getId();
					Long noteUserId = forwardToUser.getId();

					log.info("loginUserId=> " + loginUserId);
					log.info("noteUserId=> " + noteUserId);
					if (loginUserId.equals(noteUserId)) {
						viewFlag = true;
					} else {
						viewFlag = false;
					}
				} else {
					log.error("Increment note not found");
				}
				// --to get increment log details
				if (employeeBonus.getEmployeeBonusLogList() != null
						&& !employeeBonus.getEmployeeBonusLogList().isEmpty()) {
					employeeBonusLogList = employeeBonus.getEmployeeBonusLogList();
				} else {
					log.error("Increment log not found");
				}

				selectedNotificationId = null;
				systemNotificationBean.loadTotalMessages();

				return VIEW_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception in viewIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	/**
	 * Purpose : to get employee increment details passing increment id for view
	 * screen
	 * 
	 * @param incrementId
	 */
	public void getEmployeeIncrementDetailsByIncrementId(Long incrementId) {
		log.info("<==== Starts IncrementBean.getEmployeeIncrementDetailsByIncrementId =====>");
		try {
			incrementDTO = new IncrementDTO();
			url = SERVER_URL + "/increment/getemployeeincrementdetailsbyincrementid/:incrementId";
			url = url.replace(":incrementId", incrementId.toString());

			log.info("getEmployeeIncrementDetailsByIncrementId :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					incrementDTO = mapper.readValue(jsonValue, IncrementDTO.class);
					if (incrementDTO != null && incrementDTO.getIncrementDetailsList() != null
							&& !incrementDTO.getIncrementDetailsList().isEmpty()) {
						incrementDTOList = incrementDTO.getIncrementDetailsList();
						log.info("IncrementBean :: incrementDTOList==> " + incrementDTOList.size());
						for (IncrementDTO incrementDTO : incrementDTOList) {
							Double basicpay = incrementDTO.getBasicpay();
							log.info("IncrementBean :: basicpay==> " + basicpay);
							Double revisedpay = incrementDTO.getRevisedBasicpay();
							log.info("IncrementBean :: revisedpay==> " + revisedpay);
							Double earningPay = revisedpay - basicpay;
							log.info("IncrementBean :: earningPay==> " + earningPay);
							Double calculateEaningAmount = basicpay / earningPay;
							log.info("IncrementBean :: calculateEaningAmount==> " + calculateEaningAmount);
							Double percentage = percentageValue / calculateEaningAmount;
							log.info("IncrementBean :: percentage==> " + percentage);
							if (selectedIncrementListDTORequest.getPercentage() != null) {
								incrementDTO.setIncrementPercentage(selectedIncrementListDTORequest.getPercentage());
							}
						}

					} else {
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
						incrementDTOList = new ArrayList<>();
						return;
					}

				} else {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
					incrementDTOList = new ArrayList<>();
				}
			}
		} catch (Exception e) {
			log.error("Exception in getEmployeeIncrementDetailsByIncrementId  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void getEmployeeBonusDetailsByBonusId(Long bonusId) {
		log.info("<==== Starts IncrementBean.getEmployeeIncrementDetailsByIncrementId =====>");
		try {
			incrementDTO = new IncrementDTO();
			url = SERVER_URL + "/increment/getemployeebonusdetailsbybonusid/:bonusId";
			url = url.replace(":bonusId", bonusId.toString());

			log.info("getEmployeeIncrementDetailsByIncrementId :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					employeeBonusDTO = mapper.readValue(jsonValue, EmployeeBonusDTO.class);
					if (employeeBonusDTO != null) {
						employeeBonusDTOList = employeeBonusDTO.getEmpBonusDeatilsList();
						log.info("IncrementBean :: employeeBonusDTOList==> " + employeeBonusDTOList.size());

						if (selectedIncrementListDTORequest.getPercentage() != null) {
							incrementDTO.setIncrementPercentage(selectedIncrementListDTORequest.getPercentage());
						}

					} else {
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
						incrementDTOList = new ArrayList<>();
						return;
					}

				} else {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
					employeeBonusDTOList = new ArrayList<>();
				}
			}
		} catch (Exception e) {
			log.error("Exception in getEmployeeIncrementDetailsByIncrementId  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	/**
	 * purpose : to load increment list page
	 */
	public void loadLazyIncrementList() {
		log.info("<==== Starts IncrementBean.loadLazyIncrementList =====>");
		incrementListDTOLazyList = new LazyDataModel<IncrementListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<IncrementListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/increment/loadincrementlist";
					log.info("loadLazyIncrementList :: url==> " + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContent());
						incrementListDTOList = mapper.readValue(jsonResponse,
								new TypeReference<List<IncrementListDTO>>() {
								});
						log.info(
								"loadLazyIncrementList :: incrementListDTOList size==> " + incrementListDTOList.size());
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("loadLazyIncrementList :: totalRecords==> " + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in lazyload ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return incrementListDTOList;
			}

			@Override
			public Object getRowKey(IncrementListDTO res) {

				return res != null ? res.getIncrementId() : null;

			}

			@Override
			public IncrementListDTO getRowData(String rowKey) {
				for (IncrementListDTO incrementListDTO : incrementListDTOList) {
					if (incrementListDTO.getIncrementId().equals(Long.valueOf(rowKey))) {
						selectedIncrementListDTORequest = incrementListDTO;
						return incrementListDTO;
					}
				}
				return null;
			}
		};
		log.info("<==== Ends BiometricBean.loadLazyBiometricAttendaceList =====>");
	}

	public void onRowSelect(SelectEvent event) {
		log.info(" onRowSelect method started");
		selectedIncrementListDTORequest = ((IncrementListDTO) event.getObject());
		isAddButton = false;
		isDeleteButton = true;
		isEdit = false;
		Long loginUserId = loginBean.getUserMaster() == null ? null : loginBean.getUserMaster().getId();
		if ("REJECTED".equalsIgnoreCase(selectedIncrementListDTORequest.getStatus())) {
			isEdit = true;
		}
		if (selectedIncrementListDTORequest.getStatus().equals(ApprovalStage.REJECTED)
				|| selectedIncrementListDTORequest.getStatus().equals(ApprovalStage.SUBMITTED)) {
			isDeleteButton = false;
		}
		if (loginUserId != null && loginUserId.equals(selectedIncrementListDTORequest.getUserId())) {
			isDeleteButton = false;
		} else {
			isDeleteButton = true;
		}
	}

	/**
	 * Purpose : to delete the increment details
	 * 
	 * @return
	 */
	public void deleteIncrement() {
		try {
			if (selectedIncrementListDTORequest == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return;
			}
			log.info("Selected increment  Id :: " + selectedIncrementListDTORequest.getIncrementId());
			String getUrl = SERVER_URL + "/increment/delete/id/:id";
			url = getUrl.replace(":id", selectedIncrementListDTORequest.getIncrementId().toString());
			log.info("deleteIncrement url ==> " + url);
			BaseDTO baseDTO = httpService.delete(url);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return;
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" Increment Deleted SuccessFully ");
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_DELETED_SUCCESSFULLY).getCode());
				cancelIncrement();
			} else {
				log.error(" Employee Delete Operation Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
		} catch (Exception e) {
			log.error(" Exception Occured While Delete Employee  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return;
	}

	public void clearIncrement() {
		incrementCycle = new IncrementCycle();
		headRegionOffice = new EntityMaster();
		selectedIncrementDTOList = new ArrayList<>();
		selectedBonusDTOList = new ArrayList<>();
		incrementDTOList = new ArrayList<>();
		incrementPercentage = null;
	}

	public String cancelIncrement() {
		isAddButton = true;
		isDeleteButton = true;
		incrementCycle = new IncrementCycle();
		headRegionOffice = new EntityMaster();
		incrementDTOList = new ArrayList<>();
		selectedIncrementDTOList = new ArrayList<>();
		selectedBonusDTOList = new ArrayList<>();
		incrementPercentage = null;
		incrementListDTOList = new ArrayList<>();
		selectedIncrementListDTORequest = new IncrementListDTO();
		forwardToUser = new UserMaster();
		forwardFor = false;
		note = null;
		isIncrementEmployeeStatus = false;
		loadLazyIncrementList();
		return LIST_PAGE;
	}

	/**
	 * purpose : to get employee and increment information to load the data table
	 */
	public void generateIncrementInformation() {
		log.info(" <=IncrementBean :: generateIncrementInformation=> ");
		try {
			selectedIncrementDTOList = new ArrayList<>();
			selectedBonusDTOList = new ArrayList<>();
			if (headRegionOffice == null && headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return;
			}
			if (incrementCycle == null && incrementCycle.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_CYCLE_NOT_FOUND).getCode());
				return;
			}
			log.info("generateIncrementInformation :: incrementCycle=> " + incrementCycle.getId());
			log.info("generateIncrementInformation :: headRegionOffice=> " + headRegionOffice.getId());
			log.info("generateIncrementInformation :: incrementPercentage=> " + incrementPercentage);

			if (incrementCycle.getIncrementCycleCode().equals("ADDBONUS")) {

				employeeBonusDTO.setFinancialYear(financialYear);

				employeeBonusDTO.setHeadandRegionOfficeId(headRegionOffice.getId());

				url = SERVER_URL + "/increment/getemployeebonusdetails";

				log.info("generateIncrementInformation :: url ==> " + url);
				BaseDTO baseDTO = httpService.post(url, employeeBonusDTO);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContent() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						employeeBonusDTOList = mapper.readValue(jsonValue, new TypeReference<List<EmployeeBonusDTO>>() {
						});
						if (employeeBonusDTOList != null && !employeeBonusDTOList.isEmpty()) {

							log.info("IncrementBean :: incrementDTOList==> " + employeeBonusDTOList.size());
							for (EmployeeBonusDTO employeeBonusDTO : employeeBonusDTOList) {
								Double basicpay = employeeBonusDTO.getBasicDa();
								
								
								log.info("IncrementBean :: basicpay==> " + basicpay);
								
								
								if (basicpay != null && basicpay != 0.0) {
									employeeBonusDTO.setHideFlag(false);
								}
								else {
									employeeBonusDTO.setHideFlag(true);
								}
								
								if(employeeBonusDTO.getIncomeTAx()==null) {
									employeeBonusDTO.setIncomeTAx(0.0);	
								}
								if(employeeBonusDTO.getStockDeficit()==null) {
									employeeBonusDTO.setStockDeficit(0.0);
								}
								if(employeeBonusDTO.getCreditSales()==null) {
									employeeBonusDTO.setCreditSales(0.0);
								}
								if(employeeBonusDTO.getOther()==null) {
									employeeBonusDTO.setOther(0.0);
								}
								
								
										
								/*
								 * if (basicpay != null && basicpay != 0.0 && incrementPercentage != null) {
								 * 
								 * eligibleBonusAmount = (basicpay + employeeBonusDTO.getNoOfWorkingDays()) /
								 * 365;
								 * 
								 * if (eligibleBonusAmount > 8400) { eligibleBonusAmount = 8400D; }
								 * 
								 * } else { log.info("Basic pay and increment Percentage not found"); }
								 */
							}

						} else {
							errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND)
									.getCode());
							employeeBonusDTOList = new ArrayList<>();
							return;
						}

					} else {
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
						employeeBonusDTOList = new ArrayList<>();
					}
				}

			} else {
				url = SERVER_URL + "/increment/getemployeeincrementdetails/:incrementCycleId/:headRegionOfficeId";
				url = url.replace(":incrementCycleId", incrementCycle.getId().toString());
				url = url.replace(":headRegionOfficeId", headRegionOffice.getId().toString());

				log.info("generateIncrementInformation :: url ==> " + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					ObjectMapper mapper = new ObjectMapper();
					if (baseDTO.getResponseContent() != null) {
						String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
						incrementDTOList = mapper.readValue(jsonValue, new TypeReference<List<IncrementDTO>>() {
						});
						if (incrementDTOList != null && !incrementDTOList.isEmpty()) {
							log.info("IncrementBean :: incrementDTOList==> " + incrementDTOList.size());
							for (IncrementDTO incrementDTO : incrementDTOList) {
								Double basicpay = incrementDTO.getBasicpay();
								log.info("IncrementBean :: basicpay==> " + basicpay);
								if (basicpay != null && basicpay != 0.0 && incrementPercentage != null) {
									log.info("IncrementBean :: basicpay==> " + basicpay);
									Double incrementAmount = (basicpay * incrementPercentage) / percentageValue;
									log.info("IncrementBean :: incrementAmount==> " + incrementAmount);
									if (incrementAmount != null && incrementAmount > 0) {
										int number = incrementAmount.intValue();
										int remainder = number % 10;
										if (remainder > 0) {
											remainder = 10 - remainder;
											number = number + remainder;
											incrementAmount = Double.valueOf(number) + 0.00;
										} else if (incrementAmount > number) {
											incrementAmount = Double.valueOf(number) + 10.00;
										}
									}
									Double revisedBasicpay = basicpay + incrementAmount;
									log.info("IncrementBean :: revisedBasicpay==> " + revisedBasicpay);
									incrementDTO.setIncrementAmount(incrementAmount);
									incrementDTO.setRevisedBasicpay(revisedBasicpay);
								} else {
									log.info("Basic pay and increment Percentage not found");
								}
							}

						} else {
							errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND)
									.getCode());
							incrementDTOList = new ArrayList<>();
							return;
						}

					} else {
						errorMap.notify(
								ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_NOT_FOUND).getCode());
						incrementDTOList = new ArrayList<>();
					}
				}
			}
		} catch (

		Exception e) {
			log.error("Exception in generateIncrementInformation  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/*
	 * Purpose : This is government declare percentage, this is input field (Pay
	 * Revision)
	 */
	public void calculateIncrementPrecentageForPR(IncrementDTO incrementDTO) {
		log.info("<=call calculateIncrementPrecentageForRP=>");
		try {
			if (incrementDTO.getBasicpay() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.BASIC_PAY_NOT_FOUND).getCode());
				return;
			}
			if (incrementDTO.getIncrementPercentage() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_PRECENTAGE_NOT_FOUND).getCode());
				return;
			}
			if (incrementDTO.getIncrementPercentage() > percentageValue) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.CHECK_PRECENTAGE_VALUE).getCode());
				return;
			}
			Double basicpay = incrementDTO.getBasicpay();
			log.info("IncrementBean :: basicpay==> " + basicpay);
			incrementPercentage = incrementDTO.getIncrementPercentage();
			log.info("IncrementBean :: incrementPercentage==> " + incrementPercentage);
			Double incrementAmount = (basicpay * incrementPercentage) / percentageValue;
			log.info("IncrementBean :: incrementAmount==> " + incrementAmount);
			incrementDTO.setIncrementAmount(incrementAmount);
			Double revisedBasicpay = basicpay + incrementAmount;
			log.info("IncrementBean :: revisedBasicpay==> " + revisedBasicpay);
			incrementDTO.setRevisedBasicpay(revisedBasicpay);

		} catch (Exception e) {
			log.error("Exception in calculateIncrementPrecentageForRP  :: ", e);

		}
	}

	/**
	 * purpose : to get load increment percentage
	 */
	public void loadIncrementPercentage() {
		log.info(" <=IncrementBean :: loadIncrementPercentage=> ");

		employeeBonusDTOList = null;
		try {
			if (incrementCycle == null && incrementCycle.getIncrementCycleCode() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_CYCLE_NOT_FOUND).getCode());
				return;
			}
			// --do not view "Pay Revision" details
			log.info("Increment cycle code==> " + incrementCycle.getIncrementCycleCode());
			if (incrementCycle.getIncrementCycleCode().equals("PR01")) {
				incrementPercentage = null;
				return;
				// String appKeyValue =
				// commonDataService.getAppKeyValue(AppConfigKey.INCREMENT_PAY_REVISION_PERCENTAGE);
				// if(appKeyValue == null) {
				// errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_PRECENTAGE_NOT_FOUND).getCode());
				// return;
				// }
				// incrementPercentage =
				// Double.parseDouble(commonDataService.getAppKeyValue(PROMOTION_AND_INCREMENT_PERCENTAGE));
			}
			log.info("incrementCycle code==> " + incrementCycle.getIncrementCycleCode());

			if (incrementCycle.getIncrementCycleCode().equals("P02")) {
				if (commonDataService.getAppKeyValue(PROMOTION_AND_INCREMENT_PERCENTAGE) == null) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.INCREMENT_PRECENTAGE_NOT_FOUND).getCode());
					return;
				}
				incrementPercentage = Double
						.parseDouble(commonDataService.getAppKeyValue(PROMOTION_AND_INCREMENT_PERCENTAGE));
			} else if (incrementCycle.getIncrementCycleCode().equals("ADDBONUS")) {

				financialYearFlag = true;
				if (commonDataService.getAppKeyValue(BONUS_PERCENTAGE) == null) {
					AppUtil.addWarn("BONUS_PERCENTAGE Not Found");
					return;
				}
				incrementPercentage = Double.parseDouble(commonDataService.getAppKeyValue(BONUS_PERCENTAGE));
			} else {
				if (commonDataService.getAppKeyValue(INCREMENT_BASIC_PERCENTAGE) == null) {
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.INCREMENT_PRECENTAGE_NOT_FOUND).getCode());
					return;
				}
				incrementPercentage = Double.parseDouble(commonDataService.getAppKeyValue(INCREMENT_BASIC_PERCENTAGE));
			}
			log.info("incrementPercentage==> " + incrementPercentage);

		} catch (Exception e) {
			log.error("Exception in loadIncrementPercentage  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * purpose : to set increment details in to DTO, when selected the check box
	 * 
	 * @param incrementDTO
	 */
	public void setIncrementDetails(IncrementDTO incrementDTO) {
		log.info(" <=IncrementBean :: setIncrementDetails=> ");
		try {
			log.info("isIncrementEmployeeStatus==> " + isIncrementEmployeeStatus);
			if (isIncrementEmployeeStatus == true) {
				log.info("saveEmployeeIncrement :: incrementDTO==> " + incrementDTO);
				selectedIncrementDTOList.add(incrementDTO);
				log.info("saveEmployeeIncrement :: selectedIncrementDTOList==> " + selectedIncrementDTOList);
			} else if (selectedIncrementDTOList.contains(incrementDTO)) {
				selectedIncrementDTOList.remove(incrementDTO);
			}

		} catch (Exception e) {
			log.error("Exception in saveEmployeeIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	/**
	 * purpose : to save the increment details
	 * 
	 * @return
	 */
	public String saveEmployeeIncrement() {
		log.info(" <=IncrementBean :: saveEmployeeIncrement=> ");
		try {
			if (headRegionOffice == null || headRegionOffice.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.HO_RO_NOT_FOUND).getCode());
				return null;
			}
			if (incrementCycle == null || incrementCycle.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.INCREMENT_CYCLE_NOT_FOUND).getCode());
				return null;
			}
			if (forwardToUser == null || forwardToUser.getId() == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_USER_NOT_FOUND).getCode());
				return null;
			}
			if (forwardFor == null) {
				errorMap.notify(ErrorDescription.getError(OperationErrorCode.FORWARD_FOR_NOT_FOUND).getCode());
				return null;
			}

			if (!incrementCycle.getIncrementCycleCode().equals("ADDBONUS")) {

				EmpIncrement empIncrement = new EmpIncrement();
				empIncrement.setEntityMaster(headRegionOffice);
				empIncrement.setIncrementCycle(incrementCycle);
				empIncrement.setPercentage(incrementPercentage);

				List<EmployeeIncrementDetails> employeeIncrementDetailsList = new ArrayList<>();
				if (selectedIncrementDTOList != null && !selectedIncrementDTOList.isEmpty()) {
					log.info("saveEmployeeIncrement :: selectedIncrementDTOList==> " + selectedIncrementDTOList);
					for (IncrementDTO incrementDTO : selectedIncrementDTOList) {
						EmployeeIncrementDetails employeeIncrementDetails = new EmployeeIncrementDetails();

						log.info("saveEmployeeIncrement:employee id==> " + incrementDTO.getEmployeeId());
						employeeIncrementDetails.setIncrementEmployeeId(incrementDTO.getEmployeeId());

						PayScaleMaster payScaleMaster = new PayScaleMaster();
						log.info("saveEmployeeIncrement:payscale id==> " + incrementDTO.getPayscaleId());
						payScaleMaster.setId(incrementDTO.getPayscaleId());
						employeeIncrementDetails.setPayscaleId(payScaleMaster);

						log.info("saveEmployeeIncrement:Increment amount id==> " + incrementDTO.getIncrementAmount());
						employeeIncrementDetails.setIncrementAmount(incrementDTO.getIncrementAmount());

						log.info("saveEmployeeIncrement:basic pay==> " + incrementDTO.getBasicpay());
						employeeIncrementDetails.setBasicPay(incrementDTO.getBasicpay());

						log.info("saveEmployeeIncrement:revised basic pay==> " + incrementDTO.getRevisedBasicpay());
						employeeIncrementDetails.setRevisedBasicPay(incrementDTO.getRevisedBasicpay());
						// employeeIncrementDetails.setIncrementReferenceNumber("");
						employeeIncrementDetails.setIncrementReferenceDate(new Date());
						log.info("saveEmployeeIncrement:Increment date==> " + incrementDTO.getIncrementDate());
						// employeeIncrementDetails.setIncrementEffectiveDate(incrementDTO.getIncrementDate());
						employeeIncrementDetails.setIncrementEffectiveDate(incrementDTO.getEffectiveDate());
						
						Designation designation = new Designation();
						log.info("saveEmployeeIncrement: designation id==> " + incrementDTO.getDesignationId());
						designation.setId(incrementDTO.getDesignationId());
						employeeIncrementDetails.setDesignation(designation);
						employeeIncrementDetails.setEmpIncrement(empIncrement);
						log.info("saveEmployeeIncrement: financial benefit from==> "
								+ incrementDTO.getFinancialBenefitFrom());
						employeeIncrementDetails.setFinancialBenefitFrom(incrementDTO.getFinancialBenefitFrom());
						employeeIncrementDetails.setApprovalStage(ApprovalStage.SUBMITTED);

						employeeIncrementDetailsList.add(employeeIncrementDetails);
						empIncrement.setEmployeeIncrementDetailsList(employeeIncrementDetailsList);

					}
				} else {
					log.error("Increment details not found");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_DETAILS_NOT_FOUND)
							.getCode());
					return null;
				}
				List<EmpIncrementNote> incrementNoteList = new ArrayList<>();
				EmpIncrementNote empIncrementNote = new EmpIncrementNote();
				log.info("saveEmployeeIncrement: note==> " + note);
				empIncrementNote.setNote(note);
				empIncrementNote.setEmpIncrement(empIncrement);
				log.info("saveEmployeeIncrement: forwardToUser id==> " + forwardToUser.getId());
				empIncrementNote.setForwardTo(forwardToUser);
				log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
				empIncrementNote.setFinalApproval(forwardFor);
				incrementNoteList.add(empIncrementNote);
				empIncrement.setEmpIncrementNoteList(incrementNoteList);

				List<EmpIncrementLog> incrementLogList = new ArrayList<>();
				EmpIncrementLog empIncrementLog = new EmpIncrementLog();
				empIncrementLog.setEmpIncrement(empIncrement);
				empIncrementLog.setStage(ApprovalStage.SUBMITTED);
				incrementLogList.add(empIncrementLog);
				empIncrement.setEmpIncrementLogList(incrementLogList);

				String url = SERVER_URL + "/increment/createemployeeincrement";
				log.info("saveEmployeeIncrement :: url==> " + url);
				BaseDTO baseDTO = httpService.put(url, empIncrement);
				log.info("Save Modernization Request : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				mapper.readValue(jsonResponse, EmpIncrement.class);
				if (baseDTO.getStatusCode() == 0) {
					cancelIncrement();
					loadLazyIncrementList();
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.INCREMENT_INSERTED_SUCCESSFULLY).getCode());
					return LIST_PAGE;
				} else {
					log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			} else {

				// EmployeeBonus empBonus = new EmployeeBonus();
				employeeBonus.setEntityMaster(headRegionOffice);
				employeeBonus.setIncrementCycle(incrementCycle);
				employeeBonus.setPercentage(incrementPercentage);

				Double totalAmount = 0.0;
				Double netPayAmount = 0.0;

				List<EmployeeBonusDetails> employeeBonusDetailsList = new ArrayList<>();
				if (selectedBonusDTOList != null && !selectedBonusDTOList.isEmpty()) {
					log.info("saveEmployeeIncrement :: selectedBonusDTOList==> " + selectedBonusDTOList);
					for (EmployeeBonusDTO employeeBonusDTO : selectedBonusDTOList) {
						EmployeeBonusDetails employeeBonusDetails = new EmployeeBonusDetails();

						if (employeeBonusDTO.getEmployeeId() != null) {
							employeeBonusDetails.setBonusEmployeeId(employeeBonusDTO.getEmployeeId());
						}

						if (employeeBonusDTO.getIncomeTAx() != null) {
							employeeBonusDetails.setIncomtax(employeeBonusDTO.getIncomeTAx());
							//totalAmount = totalAmount + employeeBonusDTO.getIncomeTAx();
						}

						if (employeeBonusDTO.getStockDeficit() != null) {
							employeeBonusDetails.setStockDeficit(employeeBonusDTO.getStockDeficit());
							//totalAmount = totalAmount + employeeBonusDTO.getStockDeficit();
						}

						if (employeeBonusDTO.getCreditSales() != null) {
							employeeBonusDetails.setCreditSales(employeeBonusDTO.getCreditSales());
							//totalAmount = totalAmount + employeeBonusDTO.getCreditSales();
						}

						if (employeeBonusDTO.getOther() != null) {
							employeeBonusDetails.setOther(employeeBonusDTO.getOther());
							//totalAmount = totalAmount + employeeBonusDTO.getOther();
						}

						if (employeeBonusDTO.getPfNumber() != null) {
							employeeBonusDetails.setPfNumber(employeeBonusDTO.getPfNumber());
						}

						if (employeeBonusDTO.getDesignation() != null) {
							employeeBonusDetails.setDesignamtionName(employeeBonusDTO.getDesignation());
						}

						if (employeeBonusDTO.getBasicDa() != null) {
							employeeBonusDetails.setBasicPay(employeeBonusDTO.getBasicDa());
						}

						Designation designation = new Designation();

						if (employeeBonusDTO.getDesignationId() != null) {
							designation.setId(employeeBonusDTO.getDesignationId());
							employeeBonusDetails.setDesignation(designation);
						}
						
						double tot= Math.round(employeeBonusDTO.getIncomeTAx()+employeeBonusDTO.getStockDeficit()+employeeBonusDTO.getCreditSales()+employeeBonusDTO.getOther());
						
						employeeBonusDetails.setTotalAmount(tot);
						double netPay = (employeeBonusDTO.getBasicDa()* employeeBonusDTO.getNoOfWorkingDays()/365) - tot;
						double totalnetamt = Math.round(netPay);
						employeeBonusDetails.setNetPayableAmount(totalnetamt);
						int totalworking = (int)Math.round(employeeBonusDTO.getNoOfWorkingDays());
						employeeBonusDetails.setTotalWorkingDays(totalworking);
						employeeBonusDetailsList.add(employeeBonusDetails);
						employeeBonus.setEmployeeBonusDetailsList(employeeBonusDetailsList);

					}
				} else {
					log.error("Increment details not found");
					errorMap.notify(ErrorDescription.getError(OperationErrorCode.EMPLOYEE_INCREMENT_DETAILS_NOT_FOUND)
							.getCode());
					return null;
				}
				List<EmployeeBonusNote> employeeBonusNoteList = new ArrayList<>();
				EmployeeBonusNote employeeBonusNote = new EmployeeBonusNote();
				log.info("saveEmployeeIncrement: note==> " + note);
				employeeBonusNote.setNote(note);
				employeeBonusNote.setEmployeeBonus(employeeBonus);
				log.info("saveEmployeeIncrement: forwardToUser id==> " + forwardToUser.getId());
				employeeBonusNote.setForwardTo(forwardToUser);
				log.info("saveEmployeeIncrement: forwardFor==> " + forwardFor);
				employeeBonusNote.setFinalApproval(forwardFor);
				employeeBonusNoteList.add(employeeBonusNote);
				employeeBonus.setEmployeeBonusNoteList(employeeBonusNoteList);

				List<EmployeeBonusLog> employeeBonusLogList = new ArrayList<>();
				EmployeeBonusLog employeeBonusLog = new EmployeeBonusLog();
				employeeBonusLog.setEmployeeBonus(employeeBonus);
				employeeBonusLog.setStage(ApprovalStage.SUBMITTED);
				employeeBonusLogList.add(employeeBonusLog);
				employeeBonus.setEmployeeBonusLogList(employeeBonusLogList);

				String url = SERVER_URL + "/increment/createemployeebonus";
				log.info("saveEmployeeIncrement :: url==> " + url);
				BaseDTO baseDTO = httpService.put(url, employeeBonus);
				log.info("Save Modernization Request : " + baseDTO);
				if (baseDTO == null) {
					log.error("Base DTO Returned Null Value");
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					return null;
				}
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				mapper.readValue(jsonResponse, EmployeeBonus.class);
				if (baseDTO.getStatusCode() == 0) {
					cancelIncrement();
					loadLazyIncrementList();
					errorMap.notify(
							ErrorDescription.getError(OperationErrorCode.INCREMENT_INSERTED_SUCCESSFULLY).getCode());
					return LIST_PAGE;
				} else {
					log.info("Error while saving increment with error code :: " + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception in saveEmployeeIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return LIST_PAGE;
	}

	/**
	 * purpose : to save approved or rejected status in log table
	 * 
	 * @param status
	 * @return
	 */
	public String submitApprovedRejectedStatus(String status) {
		try {
			log.info("status==> " + status);
			if (ApprovalStage.APPROVED.equals(status)) {
				if (forwardToUser == null || forwardToUser.getId() == null) {
					errorMap.notify(ErrorCodeDescription.INCOMINGTAPAL_FORWARD_TO_NOTNULL.getErrorCode());
					return null;
				}
				if (forwardFor == null) {
					errorMap.notify(ErrorCodeDescription.INCOMINGTAPAL_FORWARD_FOR_NOTNULL.getErrorCode());
					return null;
				}
			}
			log.info("noteId==> " + noteId);
			log.info("forwardFor==> " + forwardFor);
			if ("REJECT".equalsIgnoreCase(status)) {
				forwardToUser = new UserMaster();
				forwardToUser.setId(tempForwardToUserId);
			}
			IncrementForwardDTO incrementForwardDTO = new IncrementForwardDTO();

			if (empIncrementLogList.isEmpty()) {
				incrementForwardDTO.setBonusLogList(employeeBonusLogList);
			} else {
				incrementForwardDTO.setLogList(empIncrementLogList);

			}
			if ("FINAL_APPROVED".equals(status)) {
				incrementForwardDTO.setForwardRemarks(incrementFinalApprovalRemarks);
			} else if ("REJECT".equalsIgnoreCase(status)) {
				incrementForwardDTO.setForwardRemarks(incrementRejectRemarks);
			} else {
				incrementForwardDTO.setForwardRemarks(incrementRemarks);
			}

			if (empIncrement != null && empIncrement.getId() != null) {
				incrementForwardDTO.setEmpIncrement(empIncrement);

			} else {
				incrementForwardDTO.setEmpBonus(employeeBonus);
			}
			incrementForwardDTO.setNoteId(noteId);
			log.info("forward user id==> " + forwardToUser.getId());
			incrementForwardDTO.setForwardUserId(forwardToUser.getId());
			incrementForwardDTO.setForwardFor(forwardFor);
			if (status.equals(REJECT)) {
				incrementForwardDTO.setStatus(ApprovalStage.REJECTED);
			} else if (status.equals(APPROVED)) {
				incrementForwardDTO.setStatus(ApprovalStage.APPROVED);
			} else {
				incrementForwardDTO.setStatus(ApprovalStage.FINAL_APPROVED);
			}
			String url = SERVER_URL + "/increment/updateIncrementlog";
			log.info("submitRejectedStatus :: url==> " + url);
			BaseDTO baseDTO = httpService.put(url, incrementForwardDTO);
			log.info("submitRejectedStatus :: baseDTO==> " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if (baseDTO.getStatusCode() == 0) {
				if (incrementForwardDTO.getStatus().equals(ApprovalStage.REJECTED)) {
					AppUtil.addInfo("Increment rejected sucessfully");
				} else if (incrementForwardDTO.getStatus().equals(ApprovalStage.APPROVED)) {
					AppUtil.addInfo("Increment approved sucessfully");
				} else if (incrementForwardDTO.getStatus().equals(ApprovalStage.FINAL_APPROVED)) {
					AppUtil.addInfo("Increment final approved sucessfully");
				}
				cancelIncrement();
			} else {
				log.info("Error while saving submitApprovedRejectedStatus :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
				return null;
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
		return LIST_PAGE;
	}

	public String editIncrement() {
		log.info("<==== Starts IncrementBean.editIncrement =====>");
		try {
			Long incrementId = selectedIncrementListDTORequest.getIncrementId();
			log.info("viewIncrement :: incrementId==> " + incrementId);
			String url = SERVER_URL + "/increment/getincrementbyid/" + incrementId;
			log.info("viewIncrement :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() < 1) {
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				// --to get increment value
				empIncrement = mapper.readValue(jsonResponse, EmpIncrement.class);
				headRegionOffice = empIncrement.getEntityMaster();
				if (headRegionOffice != null) {
					log.info("head office region office id==> " + headRegionOffice.getId());
				} else {
					log.error("Head office / Region office not found");
				}
				incrementCycle = empIncrement.getIncrementCycle();
				if (incrementCycle != null) {
					log.info("Increment cycle id==> " + incrementCycle.getId());
				} else {
					log.error("Increment cycle not found");
				}
				getEmployeeIncrementDetailsByIncrementId(incrementId);
			}
		} catch (Exception e) {
			log.error("Exception in editIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public List<UserMaster> getForwardToList(String query) {
		log.info("<=IncrementBean :: getForwardToList=>");
		List<UserMaster> userMasterList = new ArrayList<UserMaster>();
		try {
			url = SERVER_URL + "/increment/forwardtousers/" + query;
			log.info("loadIncrementCycle :: url ==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				if (baseDTO.getResponseContent() != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					userMasterList = mapper.readValue(jsonValue, new TypeReference<List<UserMaster>>() {
					});
					log.info("IncrementBean :: getForwardToList==> " + userMasterList.size());
				} else {
					String msg = baseDTO.getErrorDescription();
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: " + msg);
				}
			}
		} catch (Exception e) {
			log.error("Exception in loadIncrementCycle  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return userMasterList;
	}

	@PostConstruct
	public String showViewListPage() {
		log.info("<==== Starts EmployeeIncrement-showFileMovementListPage =====>");
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String incrementId = "";
		String status = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			incrementId = httpRequest.getParameter("incrementid");
			status = httpRequest.getParameter("status");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (incrementId != null) {
			log.info("incrementId----------------->" + incrementId);
			Long fileId = Long.parseLong(incrementId);
			selectedIncrementListDTORequest = new IncrementListDTO();
			selectedIncrementListDTORequest.setIncrementId(fileId);
			selectedIncrementListDTORequest.setStatus(status);
			selectedNotificationId = Long.parseLong(notificationId);
			pageAction = "VIEW";
			finalApproveStage = selectedIncrementListDTORequest.getStatus();
			viewIncrement();
		}
		return null;
	}

	public void loadAllFinancialYear() {
		log.info("loadAllFinancialYear :: started");
		try {

			String url = SERVER_URL + "/employeeprofile/loadBlockYearsFromFinancialYearTable";
			log.info("loadAllFinancialYear :: url==> " + url);
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContents());
				financialYearList = mapper.readValue(jsonValue, new TypeReference<List<FinancialYear>>() {
				});
				if (financialYearList != null && !financialYearList.isEmpty()) {
					log.info("blockYearList list size is " + financialYearList.size());
				}
			}
		} catch (Exception e) {
			log.error("loadAllFinancialYear :: Exception=> " + e);
		}

	}

	/**
	 * purpose : to set increment details in to DTO, when selected the check box
	 * 
	 * @param incrementDTO
	 */
	public void setBonusDetails(EmployeeBonusDTO employeeBonusDTO) {
		log.info(" <=IncrementBean :: setIncrementDetails=> ");
		try {
			log.info("isIncrementEmployeeStatus==> " + isIncrementEmployeeStatus);
			if (isIncrementEmployeeStatus == true) {
				log.info("saveEmployeeIncrement :: ssss==> " + employeeBonusDTO);
				selectedBonusDTOList.add(employeeBonusDTO);
				log.info("saveEmployeeIncrement :: selectedBonusDTOList==> " + selectedBonusDTOList);
			} else if (selectedBonusDTOList.contains(employeeBonusDTO)) {
				selectedBonusDTOList.remove(employeeBonusDTO);
			}

		} catch (Exception e) {
			log.error("Exception in saveEmployeeIncrement  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
	}

	public void calculateBonusTotalAmount() {
		Double incomeTax = 0.0;
		Double stockDeficit = 0.0;
		Double creditSales = 0.0;
		Double otherAmt = 0.0;
		Double basicpay = 0.0;

		try {

			incomeTax = employeeBonusDetails.getCreditSales();
			stockDeficit = employeeBonusDetails.getStockDeficit();
			creditSales = employeeBonusDetails.getCreditSales();
			otherAmt = employeeBonusDetails.getOther();
			totalAmount = incomeTax + stockDeficit + creditSales + otherAmt;

			for (EmployeeBonusDTO employeeBonusDTO : employeeBonusDTOList) {
				basicpay = employeeBonusDTO.getBasicDa();
			}
			netPayableAmount = basicpay - totalAmount;

		} catch (Exception e) {

		}

	}
}

