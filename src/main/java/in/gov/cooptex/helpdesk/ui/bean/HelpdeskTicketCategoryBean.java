package in.gov.cooptex.helpdesk.ui.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.helpdesk.model.HelpdeskTicketCategory;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Scope("session")
@Service("helpdeskTicketCategoryBean")
public class HelpdeskTicketCategoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private final String CREATE_TICKET_CATEGORY_PAGE = "/pages/masters/parentcategory/createParentCategory.xhtml?faces-redirect=true;";
	private final String LIST_URL = "/pages/masters/parentcategory/listParentCategory.xhtml?faces-redirect=true;";
	private final String INPUT_FORM_VIEW_URL = "/pages/masters/parentcategory/viewParentCategory.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	AppPreference appPreference;

	ObjectMapper mapper;

	String jsonResponse;

	@Getter
	@Setter
	private String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	HelpdeskTicketCategory helpdeskTicketCategory;

	@Getter
	@Setter
	HelpdeskTicketCategory selectedHelpdeskTicketCategory;

	@Getter
	@Setter
	LazyDataModel<HelpdeskTicketCategory> lazyHelpdeskTicketCategoryList;

	// private List<HelpdeskTicketCategory> helpdeskTicketCategoryList;

	@Getter
	@Setter
	List<HelpdeskTicketCategory> helpdeskTicketCategoryList = new ArrayList<HelpdeskTicketCategory>();

	@Getter
	@Setter
	List<HelpdeskTicketCategory> parenthelpdeskTicketCategoryList = new ArrayList<HelpdeskTicketCategory>();

	@Getter
	@Setter
	HelpdeskTicketCategory parenthelpdeskTicketCategory;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean statusButtonFlag = true;

	@Getter
	@Setter
	private String pageHead = "";

	@Getter
	@Setter
	private List<String> listNames;

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;

	boolean forceLogin = false;

	// @Autowired
	// List<HelpdeskTicketCategory> parentTicketCategoryList;

	public String showHelpdeskTicketTicketCategoryPage() {
		log.info("<==== Starts HelpdeskTicketCategoryBean.showHelpdeskTicketTicketCategoryPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		helpdeskTicketCategoryList = new ArrayList<>();
		helpdeskTicketCategory = new HelpdeskTicketCategory();
		selectedHelpdeskTicketCategory = new HelpdeskTicketCategory();
		parenthelpdeskTicketCategoryList = loadHelpdeskTicketCategoryList();
		loadLazyHelpdeskTicketCategoryList();

		log.info("<==== Ends HelpdeskTicketCategoryBean.showHelpdeskTicketTicketCategoryPage =====>");
		return LIST_URL;
	}

	/*
	 * public void sample() { parentTicketCategoryList=null;
	 * 
	 * helpdeskTicketCategory = new HelpdeskTicketCategory(); }
	 */

	private void loadLazyHelpdeskTicketCategoryList() {
		log.info("<===== Starts HelpdeskTicketCategory.loadLazyHelpdeskTicketCategoryList ======>");
		lazyHelpdeskTicketCategoryList = new LazyDataModel<HelpdeskTicketCategory>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<HelpdeskTicketCategory> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					log.info("<=====inside load ======>");

					log.info("filters...." + filters);
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + appPreference.getOperationApiUrl()
							+ "/helpdeskticketcategory/gethelpdeskTicketCategorylist";

					log.info("url" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						helpdeskTicketCategoryList = mapper.readValue(jsonResponse,
								new TypeReference<List<HelpdeskTicketCategory>>() {
								});
						log.info("helpdeskTicketCategoryList in loadLazyHelpdeskTicketCategoryList()  :::"
								+ helpdeskTicketCategoryList);
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("=======totalrecords count at 179============" + totalRecords);

					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyHelpdeskTicketCategoryList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return helpdeskTicketCategoryList;
			}

			@Override
			public Object getRowKey(HelpdeskTicketCategory res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public HelpdeskTicketCategory getRowData(String rowKey) {
				try {
					for (HelpdeskTicketCategory helpdeskTicketCategory : helpdeskTicketCategoryList) {
						if (helpdeskTicketCategory.getId().equals(Long.valueOf(rowKey))) {
							selectedHelpdeskTicketCategory = helpdeskTicketCategory;
							return helpdeskTicketCategory;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends HelpdeskTicketCategory.loadLazyHelpdeskTicketCategoryList  ======>");

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts HelpdeskTicketCategory.onRowSelect ========>" + event);
		selectedHelpdeskTicketCategory = ((HelpdeskTicketCategory) event.getObject());
		log.info("selectedHelpdeskTicketCategory id" + selectedHelpdeskTicketCategory.getId());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends HelpdeskTicketCategory.onRowSelect ========>");
	}

	public String clear() {
		log.info("<---- Start HelpdeskTicketCategoryBean . clear ----->");
		showHelpdeskTicketTicketCategoryPage();

		log.info("<---- End HelpdeskTicketCategoryBean.clear ----->");

		return LIST_URL;

	}

	public String addHelpdeskTicketCategory() {

		log.info("<--- Start HelpdeskTicketCategoryBean.addHelpdeskTicketCategory ---->");
		try {
			action = "ADD";
			// helpdeskTicketCategory = new HelpdeskTicketCategory();

			// helpdeskTicketCategoryList.add(helpdeskTicketCategory);
			helpdeskTicketCategory.setActiveStatus(true);

			log.info("<---- End  HelpdeskTicketCategoryBean.addHelpdeskTicketCategory ---->");
		} catch (Exception e) {
			log.error("<<<<======Error occured while redirectin to user add page===>>>>>>", e);
		}
		return CREATE_TICKET_CATEGORY_PAGE;
	}

	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}

	public String create() {
		log.info("<=======Starts  HelpdeskTicketCategoryBean.create ======>");

		/*
		 * if (helpdeskTicketCategory.getName() == null ||
		 * helpdeskTicketCategory.getName().trim().isEmpty()) {
		 * errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_lNAME_REQUIRED.
		 * getErrorCode()); return null; }
		 */

		if (helpdeskTicketCategory.getActiveStatus() == null) {
			errorMap.notify(ErrorDescription.EDUCATION_QUALIFICTION_STATUS_REQUIRED.getErrorCode());
			return null;
		}

		if (action.equalsIgnoreCase("ADD")) {
			helpdeskTicketCategory.setCreatedBy(loginBean.getUserDetailSession());
			helpdeskTicketCategory.setCreatedDate(new Date());
			// helpdeskTicketCategory.setParentTicketCategory(HelpdeskTicketCategory.getId());
		} else if (action.equalsIgnoreCase("EDIT")) {
			helpdeskTicketCategory.setModifiedBy(loginBean.getUserDetailSession());
			helpdeskTicketCategory.setModifiedDate(new Date());
		}
		BaseDTO response = httpService.post(
				SERVER_URL + appPreference.getOperationApiUrl() + "/helpdeskticketcategory/create",
				helpdeskTicketCategory);
		// errorMap.notify(response.getStatusCode());
		if (response.getStatusCode() == 0) {
			if (action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.HELP_DESK_TICKET_CATEGORY_SAVE_SUCCESS.getErrorCode());
			else
				errorMap.notify(ErrorDescription.HELP_DESK_TICKET_CATEGORY_UPDATE_SUCCESS.getErrorCode());
			// return showListQualificationPage();
			return showHelpdeskTicketTicketCategoryPage();
		} /*
			 * else if (response.getStatusCode() ==
			 * ErrorDescription.SALESTYPE_NAME_ALREADY_EXIST.getErrorCode()) {
			 * errorMap.notify(ErrorDescription.SALESTYPE_NAME_ALREADY_EXIST.getErrorCode())
			 * ; }
			 */

		else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends HelpdeskTicketCategoryBean.create ======>");
		return null;
	}

	public String viewHelpdeskTicketCategory() {
		action = "View";
		log.info("<----Redirecting to user view page---->");
		if (selectedHelpdeskTicketCategory == null || selectedHelpdeskTicketCategory.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
			return null;
		}
		log.info("ID-->" + selectedHelpdeskTicketCategory.getId());

		selectedHelpdeskTicketCategory = getHelpdeskTicketCategoryDetails(selectedHelpdeskTicketCategory.getId());
		log.info("selected Helpdesk TicketCategory Object:::" + selectedHelpdeskTicketCategory);
		return INPUT_FORM_VIEW_URL;
	}

	private HelpdeskTicketCategory getHelpdeskTicketCategoryDetails(Long id) {
		log.info("<====== HelpdeskTicketCategoryBean.getHelpdeskTicketCategoryDetails starts====>" + id);
		try {
			if (selectedHelpdeskTicketCategory == null || selectedHelpdeskTicketCategory.getId() == null) {
				log.error(" No HelpdeskTicketCategory selected ");
				errorMap.notify(ErrorDescription.EDUCATION_QUALIFICATION_NO_SELECTED.getErrorCode());
			}
			log.info("Selected HelpdeskTicketCategory  Id : : " + selectedHelpdeskTicketCategory.getId());

			String url = SERVER_URL + appPreference.getOperationApiUrl() + "/helpdeskticketcategory/get/"
					+ selectedHelpdeskTicketCategory.getId();

			log.info("HelpdeskTicketCategory Get By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			HelpdeskTicketCategory helpdeskTicketCategoryDetail = mapper.readValue(jsonResponse,
					HelpdeskTicketCategory.class);

			if (helpdeskTicketCategoryDetail != null) {
				helpdeskTicketCategory = helpdeskTicketCategoryDetail;
				selectedHelpdeskTicketCategory = helpdeskTicketCategory.getParentTicketCategory();
			} else {
				log.error("HelpdeskTicketCategory object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}

			if (baseDTO.getStatusCode() == 0) {
				log.info(" HelpdeskTicketCategory Retrived  SuccessFully ");
			} else {
				log.error(" HelpdeskTicketCategory Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return helpdeskTicketCategory;
		} catch (Exception e) {
			log.error("Exception Ocured while get HelpdeskTicketCategory Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	public String backPage() {

		showHelpdeskTicketTicketCategoryPage();
		return LIST_URL;
	}

	public String deleteHelpdeskTicketCategory() {
		try {
			action = "Delete";
			if (selectedHelpdeskTicketCategory == null || selectedHelpdeskTicketCategory.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;

			} else {

				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}

	public String deleteHelpdeskTicketCategoryConfirm() {
		log.info("<===== Starts HelpdeskTicketCategoryBean.deleteConfirm ===========>");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + appPreference.getOperationApiUrl()
					+ "/helpdeskticketcategory/delete/" + selectedHelpdeskTicketCategory.getId());
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.HELP_DESK_TICKET_CATEGORY_DELETE_SUCCESS.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmTenderCategoryDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete HelpdeskTicketCategory ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends HelpdeskTicketCategoryBean.deleteConfirm ===========>");
		return showHelpdeskTicketTicketCategoryPage();
	}

	public String editHelpdeskTicketCategory() {
		log.info("<<<<< -------Start editHelpdeskTicketCategory called ------->>>>>> ");

		try {
			if (selectedHelpdeskTicketCategory == null || selectedHelpdeskTicketCategory.getId() == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
				return null;
			}

			if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit HelpdeskTicketCategory called..");
				String url = SERVER_URL + appPreference.getOperationApiUrl() + "/helpdeskticketcategory/get/"
						+ selectedHelpdeskTicketCategory.getId();
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					helpdeskTicketCategory = mapper.readValue(jsonResponse,
							new TypeReference<HelpdeskTicketCategory>() {
							});
					return CREATE_TICKET_CATEGORY_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in editHelpdeskTicketCategory ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends editHelpdeskTicketCategory ===========>" + action);
		return null;
	}

	public List<HelpdeskTicketCategory> loadHelpdeskTicketCategoryList() {
		log.info("HelpdeskTicketCategoryBean - loadHelpdeskTicketCategoryList-->");
		List<HelpdeskTicketCategory> HelpdeskTicketCategoryValue = new ArrayList<>();
		try {
			String url;
			BaseDTO baseDTO = new BaseDTO();
			url = appPreference.getPortalServerURL() + appPreference.getOperationApiUrl()
					+ "/helpdeskticketcategory/getActiveHelpdeskTicketCategory";
			baseDTO = httpService.get(url);
			if (baseDTO.getStatusCode() == 0) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				mapper.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false);
				HelpdeskTicketCategoryValue = mapper.readValue(jsonResponse,
						new TypeReference<List<HelpdeskTicketCategory>>() {
						});
				log.info("loadHelpdeskTicketCategoryList---------" + HelpdeskTicketCategoryValue.size());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
		} catch (Exception e) {
			log.info("inside loadHelpdeskTicketCategoryList method--", e);
		}
		return HelpdeskTicketCategoryValue;
	}
}
