package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("departmentBean")
@Scope("session")
public class DepartmentBean implements Serializable {/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private final String DEPT_LIST_PAGE = "/pages/masters/department/listDepartment.xhtml?faces-redirect=true;";
	private final String DEPT_ADD_PAGE = "/pages/masters/department/addDepartment.xhtml?faces-redirect=true;";
	private final String DEPT_VIEW_PAGE = "/pages/masters/department/viewDepartment.xhtml?faces-redirect=true;";

	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter @Setter
	Department selectedDepartment = new Department();

	@Getter @Setter
	Department department = new Department();

	@Getter @Setter
	private RestTemplate restTemplate;

	@Getter @Setter
	private String Url = null;

	@Getter @Setter
	LazyDataModel<Department> departmentlazyLoadList;

	@Getter @Setter
	private Map<String, Object> filterMap;

	@Getter @Setter
	private SortOrder sortingOrder;

	@Getter @Setter
	private String sortingField;

	@Getter @Setter
	private Integer resultSize;

	@Getter @Setter 
	private Integer defaultRowSize;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	Boolean addButtonFlag = true;
	
	@Getter
	@Setter
	Boolean editButtonFlag = true;
	
	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Autowired
	LoginBean loginBean;
	
	/**
	 * @return
	 */
	public String showListDepartmentPage() {
		log.info("showListDepartmentPage called..");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		selectedDepartment = new Department();
		loadDepartmentListLazy();
		return DEPT_LIST_PAGE;
	}


	public void loadDepartmentListLazy() {

		log.info("<-----Department list lazy load starts------>");
		departmentlazyLoadList = new LazyDataModel<Department>() {

			private static final long serialVersionUID = 1L;

			@Override
			public List<Department> load(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters){
				try {
					BaseDTO baseDTO = loadData(first / pageSize, pageSize, sortField, sortOrder, filters);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					List<Department> departmentList = mapper.readValue(jsonResponse,
							new TypeReference<List<Department>>() {
					});
					if (departmentList == null) {
						log.info(" DepartmentList is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Department list search Successfully Completed");
					} else {
						log.error(" Department list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					return departmentList;

				}catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Department list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}



			@Override
			public Object getRowKey(Department res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public Department getRowData(String rowKey) {
				List<Department> responseList = (List<Department>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (Department res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						selectedDepartment = res;
						return res;
					}
				}
				return null;
			}
		};
	}


	public BaseDTO loadData(int first, int pageSize, String sortField, SortOrder sortOrder,Map<String, Object> filters) {
		BaseDTO dataList = new BaseDTO();
		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");
			Department request = new Department();
			request.setFirst(first);
			request.setPagesize(pageSize);
			request.setSortField(sortField);
			request.setSortOrder(sortOrder.toString());
			Map<String, Object> searchFilters = new HashMap<>();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				String value = filter.getValue().toString();
				log.info("Key : " + filter.getKey() + " Value : " + value);

				if (filter.getKey().equals("name")) {
					log.info("departmentName : " + value);
					searchFilters.put("departmentName", value);
				}
				if (filter.getKey().equals("l_name")) {
					log.info("departmentName in tamil : " + value);
					searchFilters.put("departmentNameTamil", value);
				}
				if (filter.getKey().equals("securityDepositAmount")) {
					log.info("securityDepositAmount : " + value);
					searchFilters.put("securityDepositAmount", value);
				}
				if (filter.getKey().equals("createdDate")) {
					log.info("createdDate : " + value);
					searchFilters.put("createdDate",value);
				}
				if (filter.getKey().equals("status")) {
					log.info("Status : " + value);
					searchFilters.put("status",value);
				}	

			}
			request.setFilters(searchFilters);
			log.info("Search request data" + request);
			dataList = httpService.post(SERVER_URL + "/department/getalldepartmentlistlazy", request);
			log.info("Search request response " + dataList);

		}catch(Exception e) {
			log.error("Exception Occured in Department load ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

		return dataList;
	}

	public String departmentListAction() {
		log.info("<====== DepartmentBean.departmentListAction Starts====> action   :::"+action);
		try {
			if(action.equalsIgnoreCase("ADD")) {
				log.info("<====== department add page called.......====>");
				selectedDepartment = new Department();
				department = new Department();
				return DEPT_ADD_PAGE;
			}
			if(selectedDepartment==null) {
				errorMap.notify(ErrorDescription.SELECT_DEPARTMENT.getErrorCode());
				return null;
			}
			if(action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDepartmentDelete').show();");
			}else {
				log.info("<----Loading Department Edit page.....---->" + selectedDepartment.getId());
				department = new Department();
				BaseDTO response = httpService
						.get(SERVER_URL + "/department/getdepartment/" + selectedDepartment.getId());
				if (response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					department = mapper.readValue(jsonResponse, new TypeReference<Department>() {
					});
					log.info("<====== DepartmentBean.departmentListAction Ends====>");
					return DEPT_ADD_PAGE;
				} else {
					errorMap.notify(response.getStatusCode());
					log.info("<====== DepartmentBean.departmentListAction Ends====>");
					return null;
				}
			}

		} catch (Exception e) {
			log.error("Exception Occured in DepartmentBean.departmentListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== DepartmentBean.departmentListAction Ends====>");
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts DepartmentBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService
					.delete(SERVER_URL + "/department/delete/" + selectedDepartment.getId());
			if(response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.DEPARTMENT_DELETE_SUCCESS.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());	
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting department....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends DepartmentBean.deleteConfirm ---->");
		return showListDepartmentPage();
	}

	
	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts DepartmentBean.onRowSelect ========>"+event);
		selectedDepartment = ((Department) event.getObject());
		addButtonFlag=false;
		editButtonFlag=true;
		deleteButtonFlag=true;
		/*if(selectedDepartment.getStatus() == true ){
			deleteButtonFlag=true;
		}else if(selectedDepartment.getStatus() == false){
			deleteButtonFlag=false;
		}*/
		log.info("<===Ends DepartmentBean.onRowSelect ========>");
    }

	
	public String submit() {
		log.info("<=======Starts  DepartmentBean.submit ======>"+department);
		if(department.getName() == null || department.getName().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.DEPARTMENT_NAME_EMPTY.getErrorCode());
			return null;
		}
		if(department.getName().trim().length()<3  || department.getName().trim().length() > 75) {
			errorMap.notify(ErrorDescription.DEPARTMENT_NAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(department.getL_name() == null || department.getL_name().trim().isEmpty()) {
			errorMap.notify(ErrorDescription.DEPARTMENT_LNAME_EMPTY.getErrorCode());
			return null;
		}
		if(department.getL_name().trim().length()<3  || department.getL_name().trim().length() > 75) {
			errorMap.notify(ErrorDescription.DEPARTMENT_LNAME_MIN_MAX_ERROR.getErrorCode());
			return null;
		}
		if(department.getSecurityDepositAmount() == null) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.SECURITY_DEPOSIT_EMPTY).getErrorCode());
			return null;
		}
		if(department.getSecurityDepositAmount().equals(0D)) {
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.SECURITY_DEPOSIT_SHOULD_NOT_BE_ZERO).getErrorCode());
			return null;
		}
		if(department.getStatus() == null) {
			errorMap.notify(ErrorDescription.DEPARTMENT_STATUS_EMPTY.getErrorCode());
			return null;
		}
		if(action.equalsIgnoreCase("ADD")) {
			department.setCreatedBy(loginBean.getUserDetailSession());
		}else if (action.equalsIgnoreCase("EDIT")) {
			department.setModifiedBy(loginBean.getUserDetailSession());
		}
		BaseDTO response= httpService.post(SERVER_URL + "/department/save",department);
//		errorMap.notify(response.getStatusCode());
		if(response.getStatusCode()==0) {
			if(action.equalsIgnoreCase("ADD"))
				errorMap.notify(ErrorDescription.DEPARTMENT_SAVE_SUCCESS.getErrorCode());
			else 
				errorMap.notify(ErrorDescription.DEPARTMENT_UPDATE_SUCCESS.getErrorCode());
			return showListDepartmentPage();
		}else {
			errorMap.notify(response.getStatusCode());
		}
		log.info("<=======Ends DepartmentBean.submit ======>");
		return null;
	}
	
}
