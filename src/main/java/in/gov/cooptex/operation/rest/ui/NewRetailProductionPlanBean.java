package in.gov.cooptex.operation.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.CurrentStockStatusRequest;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ReportNames;
import in.gov.cooptex.core.enums.TemplateCode;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.MisPdfUtil;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.core.utilities.VelocityStringUtil;
import in.gov.cooptex.operation.production.dto.NewRetailProductionPlanDTO;
import in.gov.cooptex.operation.production.dto.RetailProductionPlanRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("newRetailProductionPlanBean")
@Scope("session")
public class NewRetailProductionPlanBean extends CommonBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7697405124505314744L;

	private final String HEAD_OFFICE_ADD_URL = "/pages/operation/retailproduction/retailProductionPlanNewReq.xhtml?faces-redirect=true;";
	
	@Autowired
	CommonDataService commonDataService;
	
	@Getter
	@Setter
	NewRetailProductionPlanDTO newRetailProductionPlanDTO=new NewRetailProductionPlanDTO();
	
	MisPdfUtil misPdfUtil = new MisPdfUtil();
	
	@Getter
	@Setter
	List<NewRetailProductionPlanDTO> newRetailProductionPlanDTOList=new ArrayList<NewRetailProductionPlanDTO>();
	
	@Getter
	@Setter
	List<NewRetailProductionPlanDTO> newRetailProductionPlanDTOProductList=new ArrayList<NewRetailProductionPlanDTO>();
	
	@Getter
	@Setter
	List<NewRetailProductionPlanDTO> newRetailProductionPlanDTOAllProductList=new ArrayList<NewRetailProductionPlanDTO>();
	
	@Getter
	@Setter
	List<NewRetailProductionPlanDTO> productPlanList=new ArrayList<NewRetailProductionPlanDTO>();
	
	@Getter
	@Setter
	String serverURL = AppUtil.getPortalServerURL();
	
	@Autowired
	AppPreference appPreference;
	
	@Autowired
	HttpService httpService;
	
	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	String planFromMonthYear;
	
	@Getter
	@Setter
	Set<String> planFromMonthYearList;
	
	@Getter
	@Setter
	String retailClosingStockMonthConfig;
	
	@Getter
	@Setter
	String retailProdPlanPeriodConfig;
	
	@Getter
	@Setter
	String previousSalesPeriodConfig;
	
	@Getter
	@Setter
	String planToMonthYear;
	
	@Getter
	@Setter
	Set<String> planToMonthYearList;
	
	@Getter
	@Setter
	Integer planFromMonth;

	@Getter
	@Setter
	Integer planFromYear;

	@Getter
	@Setter
	Integer planToMonth;
	
	@Getter
	@Setter
	Date dueDateLimit;

	@Getter
	@Setter
	Integer planToYear;
	
	@Getter
	@Setter
	Double totRetailSales=0.00,totBaseStock=0.00,totTotalEligible=0.00,totClosingStock=0.00,totNetEligibleRP=0.00,totNetEligiblePP=0.00;
	
	@Getter
	@Setter
	Double totperiodOneQty=0.00,totperiodOneValue=0.00,totperiodTwoQty=0.00,totperiodTwoValue=0.00,totHightQty=0.00,totHightValue=0.00,totAvgValue=0.00;
	
	@Getter
	@Setter
	String regionCodeName;
	
	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Getter
	@Setter
	List<String> totalList = new ArrayList<String>();
	
	@Getter
	@Setter
	String downloadPath;
	
	@Getter
	@Setter
	String downloadFileName = "";
	
	@Setter
	@Getter
	private StreamedContent file, pdfFile;
	
	
	public String getNewHeadOfficePlanForm() {
		log.info("RetailProductionPlanBean getHeadOfficePlanForm method started");
		try {
		if (!checkPlanConfig()) {
			Util.addWarn("Retail Production Plan Configuration is not valid");
			return null;
		}
		totRetailSales=0.00;
		totBaseStock=0.00;
		totTotalEligible=0.00;
		totClosingStock=0.00;
		totNetEligibleRP=0.00;
		totNetEligiblePP=0.00;
		totperiodOneQty=0.00;
		totperiodOneValue=0.00;
		totperiodTwoQty=0.00;
		totperiodTwoValue=0.00;
		//UserMaster um=userMaster;
		totHightQty=0.00;
		totHightValue=0.00;
		totAvgValue=0.00;
		newRetailProductionPlanDTOList=new ArrayList<NewRetailProductionPlanDTO>();
		newRetailProductionPlanDTOProductList=new ArrayList<NewRetailProductionPlanDTO>();
		newRetailProductionPlanDTOAllProductList=new ArrayList<NewRetailProductionPlanDTO>();
		newRetailProductionPlanDTO=new NewRetailProductionPlanDTO();
		preparePlanFromMonthYear();
		preparePlanToMonthYear();

		} catch (Exception e) {
			log.error("Exception at getHeadOfficePlanForm() ", e);
		}
		return HEAD_OFFICE_ADD_URL;
	}
	
	public void preparePlanFromMonthYear() {
		log.info("-----RetailProductionPlanBean.preparePlanFromMonthYear Method Started-----");
        try {
		if (retailProdPlanPeriodConfig.trim().equals("ANY")) {
			planFromMonthYearList = AppUtil.calculateMonthYear(new Date(), -1, 3);
		} else {
			Date date = new GregorianCalendar(AppUtil.getYear(new Date()), 3, 1).getTime();
			planFromMonthYearList = AppUtil.calculateMonthYear(date, 0, 1);
			for (String monthYear : planFromMonthYearList) {
				planFromMonthYear = monthYear;
			}
			log.info("RetailProductionPlanBean::planFromMonthYearList size : ",planFromMonthYearList.size()>0 ?planFromMonthYearList.size():0);
			calculateSalesDuration("planFrom");

		}
        }
        catch(Exception ex) {
        	log.error("Error preparePlanFromMonthYear()-----",ex);
        }
        log.info("----RetailProductionPlanBean.preparePlanFromMonthYear Method End-----");
		
	}
	
	
	
	public void preparePlanToMonthYear() {
		log.info("-------RetailProductionPlanBean.preparePlanToMonthYear Method Started-----------");
		if (retailProdPlanPeriodConfig.trim().equals("ANY")) {
			planToMonthYearList = AppUtil.calculateMonthYear(new Date(), 0, 12);
		} else {
			Date date = new GregorianCalendar(AppUtil.getYear(AppUtil.addOrMinusYear(new Date(), 1)), 2,
					AppUtil.getDay(new Date())).getTime();
			planToMonthYearList = AppUtil.calculateMonthYear(date, 0, 1);
			for (String monthYear : planToMonthYearList) {
				planToMonthYear = monthYear;
			}
			log.info("RetailProductionPlanBean::planToMonthYearList size : " , planToMonthYearList.size()>0 ?planToMonthYearList.size():0);
			calculateSalesDuration("planTo");
		}
		log.info("-------RetailProductionPlanBean.preparePlanToMonthYear Method End-----------");
		
	}
	
	public boolean checkPlanConfig() {

		retailClosingStockMonthConfig = commonDataService.getAppKeyValue("RETAIL_CLOSING_STOCK_MONTH");
		retailProdPlanPeriodConfig = commonDataService.getAppKeyValue("RETAIL_PROD_PLAN_PERIOD");
		previousSalesPeriodConfig = commonDataService.getAppKeyValue("PREVIOUS_SALES_PERIOD");
		if (retailClosingStockMonthConfig.trim().equals("ACTUAL") && retailProdPlanPeriodConfig.trim().equals("ANY")
				&& previousSalesPeriodConfig.trim().equals("ACTUAL")

				|| retailClosingStockMonthConfig.trim().equals("FEB")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("FEB-JAN")

				|| retailClosingStockMonthConfig.trim().equals("ACTUAL")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("FEB-JAN")

				|| retailClosingStockMonthConfig.trim().equals("ACTUAL")
						&& retailProdPlanPeriodConfig.trim().equals("APR-MAR")
						&& previousSalesPeriodConfig.trim().equals("ACTUAL")

		) {
			log.info("Retail Production Plan Configuration is valid");
			return true;
		} else {
			log.info("Retail Production Plan Configuration is not valid");
			return false;
		}
	}
	
	public void calculateSalesDuration(String durationType) {
		log.info("----RetailProductionPlanBean calculateSalesDuration method started----");
		String date = "";
		try {
		

		if (planFromMonthYear != null) {
			planFromMonth = AppUtil.getMonthNumber(planFromMonthYear.split("-")[0]);
			planFromYear = Integer.valueOf(planFromMonthYear.split("-")[1]);
			/*changePlanToMonthYear(planFromMonth, planFromYear);
			
			String dueMonthDuration=commonDataService.getAppKeyValue("INITIAL_PRODUCTION_PLAN_MONTH_DURATION");
			if(dueMonthDuration!=null && !dueMonthDuration.trim().equalsIgnoreCase("")) {
				dueDateLimit = new Date();
				Calendar cal = Calendar.getInstance();
				cal.setTime(dueDateLimit);
				cal.set(Calendar.MONTH, (cal.get(Calendar.MONTH)+Integer.valueOf(dueMonthDuration)));
				dueDateLimit = cal.getTime();
				log.info("Due Date Limit:::"+dueDateLimit);
			}
			else {
				log.info("Check INITIAL_PRODUCTION_PLAN_MONTH_DURATION Key value in AppConfig");
			}*/
			
		}
		
		if (planToMonthYear != null) {
			planToMonth = AppUtil.getMonthNumber(planToMonthYear.split("-")[0]);
			planToYear = Integer.valueOf(planToMonthYear.split("-")[1]);
			log.info("---planToMonth value------"+planToMonth);
		}

		if (planFromYear != null && planFromMonth != null) {
			//Date planFrom = new GregorianCalendar(planFromYear, planFromMonth - 1, 1).getTime();
			Date firstDay = null;
			if (previousSalesPeriodConfig.trim().equals("FEB-JAN")) {
				firstDay = new GregorianCalendar(AppUtil.getYear(new Date()), 1, 1).getTime();
			} else {
				firstDay = new GregorianCalendar(planFromYear, planFromMonth - 1, 1).getTime();
			}
			Date salesPeriodOneFrom = AppUtil.addOrMinusYear(firstDay, -2);
			Date salesPeriodTwoFrom = AppUtil.addOrMinusYear(firstDay, -1);
            newRetailProductionPlanDTO.setSalesPeriodOneFrom(new Date(salesPeriodOneFrom.getTime()));
			newRetailProductionPlanDTO.setSalesPeriodTwoFrom(new Date(salesPeriodTwoFrom.getTime()));
			
		}
	
		if (planToYear != null && planToMonth != null) {
			Date firstDay = null;
			Date firstDay1 = null;
			Date salesTo = null;
			Date salesPeriodOneTo = null;
			Date salesPeriodTwoTo = null;
			//Date planToDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
			//Date planTo = AppUtil.getLastDateOfMonth(planToDay);
		 if (previousSalesPeriodConfig.trim().equals("FEB-JAN")) {
				firstDay = new GregorianCalendar(AppUtil.getYear(new Date()), 0, 1).getTime();
				firstDay1 = new GregorianCalendar(AppUtil.getYear(new Date()), 0, -1).getTime();
				salesPeriodOneTo = AppUtil.getLastDateOfMonth(firstDay1);
				salesPeriodTwoTo = AppUtil.getLastDateOfMonth(firstDay);
			} else {
				firstDay = new GregorianCalendar(planToYear, planToMonth - 1, 1).getTime();
				salesTo = AppUtil.getLastDateOfMonth(firstDay);
				salesPeriodOneTo=AppUtil.addOrMinusYear(salesTo, -2);
				salesPeriodTwoTo = AppUtil.addOrMinusYear(salesTo, -1);
			}
		    newRetailProductionPlanDTO.setSalesPeriodOneTo(new Date(salesPeriodOneTo.getTime()));
			newRetailProductionPlanDTO.setSalesPeriodTwoTo(new Date(salesPeriodTwoTo.getTime()));
		}
		
		
		}catch (Exception e) {
			log.error("calculateSalesDuration exception-----",e);
		}
		log.info("-----RetailProductionPlanBean calculateSalesDuration method End----");
	}
	
	public void changePlanToMonthYear(int month, int year) {
		log.info("RetailProductionPlanBean.changePlanToMonthYear Method Started");

		planToMonthYearList = new LinkedHashSet<>();
		Date date = new GregorianCalendar(year, month - 1, 1).getTime();
		planToMonthYearList = AppUtil.calculateMonthYear(date, 0, 12);
		log.info("RetailProductionPlanBean.changePlanToMonthYear Method Completed : " + planToMonthYearList);
	}
	
	public String generateAllRegionSalesDetails() {
		log.info("---START NewRetailProductionPlanBean::generateAllRegionSalesDetails()----");
		try {
			BaseDTO baseDTO=new BaseDTO();
			if(planFromMonthYear ==null) {
				AppUtil.addWarn("Production Plan from  is empty");
				return null;
			}
			if(planToMonthYear ==null) {
				AppUtil.addWarn("Production Plan to  is empty");
				return null;
			}
			if(newRetailProductionPlanDTO.getSalesPeriodOneFrom()==null) {
				AppUtil.addWarn("Sales periodone from date is empty");
				return null;
			}
			if(newRetailProductionPlanDTO.getSalesPeriodOneTo()==null) {
				AppUtil.addWarn("Sales periodone To date is empty");
				return null;
			}
			if(newRetailProductionPlanDTO.getSalesPeriodTwoFrom()==null) {
				AppUtil.addWarn("Sales periodtwo from date is empty");
				return null;
			}
			if(newRetailProductionPlanDTO.getSalesPeriodTwoTo()==null) {
				AppUtil.addWarn("Sales periodtwo To date is empty");
				return null;
			}
			String url = serverURL + "/newRetail/plan/generate/region/salesdata";
			baseDTO = httpService.post(url, newRetailProductionPlanDTO);
			
			if(baseDTO!=null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				newRetailProductionPlanDTOList= mapper.readValue(jsonResponse,
							new TypeReference<List<NewRetailProductionPlanDTO>>() {
					});
				
				log.info("--newRetailProductionPlanDTOList size::",newRetailProductionPlanDTOList.size()>0?newRetailProductionPlanDTOList.size():0);
				
				totRetailSales =newRetailProductionPlanDTOList.stream().filter(a->a.getHighestQty()!=null).collect(Collectors.summingDouble(x->x.getHighestQty()));
				totBaseStock =newRetailProductionPlanDTOList.stream().filter(b->b.getBaseStockQty()!=null).collect(Collectors.summingDouble(y->y.getBaseStockQty()));
				totTotalEligible =newRetailProductionPlanDTOList.stream().filter(c->c.getTotEligibilityRP()!=null).collect(Collectors.summingDouble(z->z.getTotEligibilityRP()));
				totClosingStock =newRetailProductionPlanDTOList.stream().filter(d->d.getClosingStockValue()!=null).collect(Collectors.summingDouble(u->u.getClosingStockValue()));
				totNetEligibleRP =newRetailProductionPlanDTOList.stream().filter(e->e.getNetEligibilityRP()!=null).collect(Collectors.summingDouble(v->v.getNetEligibilityRP()));
				totNetEligiblePP =newRetailProductionPlanDTOList.stream().filter(f->f.getNetEligibilityRP()!=null).collect(Collectors.summingDouble(w->w.getNetEligibilityPP()));
			}
			
		}
		catch(Exception ex) {
		log.error("------------Exception-----",ex);	
		}
		log.info("---End NewRetailProductionPlanBean::generateAllRegionSalesDetails()----");
		return null;
	}
	
	public String getRegionWiseProductPlan(NewRetailProductionPlanDTO newRetailPPDTO) {
		log.info("---START NewRetailProductionPlanBean::getRegionWiseProductPlan() --------------");
		 newRetailProductionPlanDTOProductList=new ArrayList<NewRetailProductionPlanDTO>();
		try {
			regionCodeName=newRetailPPDTO.getRegionCode()+"/"+newRetailPPDTO.getRegionName();
			log.info("Region Id",newRetailPPDTO.getRegionId()!=null?newRetailPPDTO.getRegionId():null);
			if(newRetailPPDTO.getRegionId()!=null) {
				newRetailProductionPlanDTO.setRegionId(newRetailPPDTO.getRegionId());
			}
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/newRetail/plan/region/product/salesdata";
			baseDTO = httpService.post(url, newRetailProductionPlanDTO);
			if(baseDTO!=null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				newRetailProductionPlanDTOProductList= mapper.readValue(jsonResponse,
							new TypeReference<List<NewRetailProductionPlanDTO>>() {
					});
			
				totperiodOneQty =newRetailProductionPlanDTOProductList.stream().filter(a->a.getPeriodOneQty()!=null).collect(Collectors.summingDouble(x->x.getPeriodOneQty()));
				totperiodOneValue =newRetailProductionPlanDTOProductList.stream().filter(b->b.getPeriodOneValue()!=null).collect(Collectors.summingDouble(y->y.getPeriodOneValue()));
				totperiodTwoQty =newRetailProductionPlanDTOProductList.stream().filter(c->c.getPeriodTwoQty()!=null).collect(Collectors.summingDouble(z->z.getPeriodTwoQty()));
				totperiodTwoValue =newRetailProductionPlanDTOProductList.stream().filter(d->d.getPeriodTwoValue()!=null).collect(Collectors.summingDouble(u->u.getPeriodTwoValue()));
				totHightQty =newRetailProductionPlanDTOProductList.stream().filter(e->e.getHighestQty()!=null).collect(Collectors.summingDouble(v->v.getHighestQty()));
				totHightValue =newRetailProductionPlanDTOProductList.stream().filter(f->f.getHighestValue()!=null).collect(Collectors.summingDouble(w->w.getHighestValue()));
				totAvgValue =newRetailProductionPlanDTOProductList.stream().filter(g->g.getAvgPrice()!=null).collect(Collectors.summingDouble(t->t.getAvgPrice()));
				
				log.info("--newRetailProductionPlanDTOList size::",newRetailProductionPlanDTOProductList.size()>0?newRetailProductionPlanDTOProductList.size():0);
				
				totalList.add(String.valueOf(totperiodOneQty));
				totalList.add(String.valueOf(totperiodOneValue));
				totalList.add(String.valueOf(totperiodTwoQty));
				totalList.add(String.valueOf(totperiodTwoValue));
				totalList.add(String.valueOf(totHightQty));
				totalList.add(String.valueOf(totHightValue));
				totalList.add(String.valueOf(totAvgValue));
			}
		}
		catch(Exception ex) {
			log.error("------------Exception-----",ex);	
		}
		log.info("---End NewRetailProductionPlanBean::getRegionWiseProductPlan() --------------");
		return null;
	}
	
	public String getAllProductPlan() {
		log.info("---START NewRetailProductionPlanBean::getAllProductPlan() --------------");
		newRetailProductionPlanDTOAllProductList=new ArrayList<NewRetailProductionPlanDTO>();
		try {
			if(newRetailProductionPlanDTO.getRegionId()!=null) {
			newRetailProductionPlanDTO.setRegionId(null);
			}
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/newRetail/plan/region/product/salesdata";
			baseDTO = httpService.post(url, newRetailProductionPlanDTO);
			if(baseDTO!=null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				newRetailProductionPlanDTOAllProductList= mapper.readValue(jsonResponse,
							new TypeReference<List<NewRetailProductionPlanDTO>>() {
					});
			
				totperiodOneQty =newRetailProductionPlanDTOAllProductList.stream().filter(a->a.getPeriodOneQty()!=null).collect(Collectors.summingDouble(x->x.getPeriodOneQty()));
				totperiodOneValue =newRetailProductionPlanDTOAllProductList.stream().filter(b->b.getPeriodOneValue()!=null).collect(Collectors.summingDouble(y->y.getPeriodOneValue()));
				totperiodTwoQty =newRetailProductionPlanDTOAllProductList.stream().filter(c->c.getPeriodTwoQty()!=null).collect(Collectors.summingDouble(z->z.getPeriodTwoQty()));
				totperiodTwoValue =newRetailProductionPlanDTOAllProductList.stream().filter(d->d.getPeriodTwoValue()!=null).collect(Collectors.summingDouble(u->u.getPeriodTwoValue()));
				totHightQty =newRetailProductionPlanDTOAllProductList.stream().filter(e->e.getHighestQty()!=null).collect(Collectors.summingDouble(v->v.getHighestQty()));
				totHightValue =newRetailProductionPlanDTOAllProductList.stream().filter(f->f.getHighestValue()!=null).collect(Collectors.summingDouble(w->w.getHighestValue()));
				totAvgValue =newRetailProductionPlanDTOAllProductList.stream().filter(g->g.getAvgPrice()!=null).collect(Collectors.summingDouble(t->t.getAvgPrice()));
				
				log.info("--newRetailProductionPlanDTOList size::",newRetailProductionPlanDTOAllProductList.size()>0?newRetailProductionPlanDTOAllProductList.size():0);
			}
		}
		catch(Exception ex) {
			log.error("------------Exception-----",ex);	
		}
		log.info("---End NewRetailProductionPlanBean::getAllProductPlan() --------------");
		return null;
	}
	
	public String onChangeQuantity(NewRetailProductionPlanDTO newRetailProductionPlanDTO) {
		log.info("---START NewRetailProductionPlanBean::onChangeQuantity() ----");
		try {
			NewRetailProductionPlanDTO nrpp=newRetailProductionPlanDTO;
			log.info("quantity="+newRetailProductionPlanDTO.getRevisedQty());
			int index=newRetailProductionPlanDTOProductList.indexOf(newRetailProductionPlanDTO);
			newRetailProductionPlanDTOProductList.remove(index);
			nrpp.setRevisedValue(nrpp.getRevisedQty()*nrpp.getAvgPrice());
			newRetailProductionPlanDTOProductList.add(index, nrpp);
		}
		catch(Exception ex) {
			log.error("---exception-----",ex);
		}
		log.info("---End NewRetailProductionPlanBean::onChangeQuantity() ----");
		return null;
	}
	
	public String onChangeProductWisePlanQuantity(NewRetailProductionPlanDTO newRetailProductionPlanDTO) {
		log.info("---START NewRetailProductionPlanBean::onChangeQuantity() ----");
		try {
			NewRetailProductionPlanDTO nrpp=newRetailProductionPlanDTO;
			log.info("quantity="+newRetailProductionPlanDTO.getRevisedQty());
			int index=newRetailProductionPlanDTOAllProductList.indexOf(newRetailProductionPlanDTO);
			newRetailProductionPlanDTOAllProductList.remove(index);
			nrpp.setRevisedValue(nrpp.getRevisedQty()*nrpp.getAvgPrice());
			newRetailProductionPlanDTOAllProductList.add(index, nrpp);
		}
		catch(Exception ex) {
			log.error("---exception-----",ex);
		}
		log.info("---End NewRetailProductionPlanBean::onChangeQuantity() ----");
		return null;
	}
	
	private void generatePDF(List<Map<String, Object>> response) {
		log.info("===========START NewRetailProductionPlanBean.generatePDF =============");
		try {
			log.info("===========response is=============" + response.toString());
			String content = commonDataService.getTemplateDetailsforPortal(TemplateCode.REPORT_TEMPLATE.toString(),
					appPreference);
			String finalContent = VelocityStringUtil.getFinalContent("", content, null);
			String htmlData = generateHtml(newRetailProductionPlanDTO, response, finalContent);
			downloadPath = commonDataService.getAppKeyValueforPortal(AppConfigEnum.REPORT_PDF_DOWNLOAD_PATH.toString());
			log.info("===========downloadPath is=============" + downloadPath.toString());
			downloadFileName = AppUtil.getReportFileName(ReportNames.CURRENT_STOCK_STATUS_REPORT_FILE, "pdf");
			misPdfUtil.createPDF(htmlData, downloadPath + "/" + downloadFileName, loginBean);
		} catch (Exception e) {
			log.info("===========Exception Occured in NewRetailProductionPlanBean.generatePDF =============");
			log.info("Exception is =============" , e);
		}
		log.info("===========END NewRetailProductionPlanBean.generatePDF =============");
	}
	
	private String generateHtml(NewRetailProductionPlanDTO newRetailProductionPlanDTO , List<Map<String, Object>> response,
			String finalContent) {
		log.info("===========START NewRetailProductionPlanBean.generateHtml =============");
		try {
			Long index = (long) 0;
			Long footerIndex = (long) 0;
			DateFormat dateFormat = AppUtil.DATE_FORMAT;
			StringBuffer finalTableData = new StringBuffer(110);
			StringBuffer finalHeaderData = new StringBuffer(110);
			StringBuffer finalFooterData = new StringBuffer(110);
		    String  filterTypeData = "";
			

			Set<String> headerNames = new HashSet<String>();
			for (Map<String, Object> map : response) {
				StringBuffer list = new StringBuffer(110);
				index++;
				headerNames = map.keySet();
				String beginTr = "<tr> " + "<td  class='bor-left'>" + index + "</td>";
				list.append(beginTr);
				Iterator<String> setItr = headerNames.iterator();
				while (setItr.hasNext()) {
					String keyString = setItr.next();
					String td = new String();
					if (isDecimal(map.get(keyString))) {
						td = "<td class='text-right'>" + getFormattedValue(map.get(keyString)) + "</td>";
					} else {
						td = "<td class='text-left'>" + (map.get(keyString)) + "</td>";
					}
					list.append(td);

				}
				String endTr = "</tr>";
				list.append(endTr);
				finalTableData.append(list);
			}
			String headerData = "<th style='width: 3%' class='text-center bor-left'>#</th>";
			finalHeaderData.append(headerData);
			Iterator<String> setItr = headerNames.iterator();
			while (setItr.hasNext()) {
				String keyString = setItr.next();
				headerData = "<th>" + keyString + "</th>";
				finalHeaderData.append(headerData);
			}
			Iterator<String> footerItr = headerNames.iterator();
			while (footerItr.hasNext()) {
				footerIndex++;
				StringBuffer tfoot = new StringBuffer(110);
				String keyString = footerItr.next();
				if (keyString.equals("Opening Stock")) {
					String tr = "<tfoot> " + "<tr> " + "<td colspan='" + footerIndex + "'>Total</td> ";
					tfoot.append(tr);
					for (String totalCount : totalList) {
						String td = new String();

						td = "<td class='text-center'>" + totalCount + "</td>";

						tfoot.append(td);
					}
					String endTr = "</tr> " + "</tfoot>";
					tfoot.append(endTr);
					finalFooterData.append(tfoot);
				}

			}

			finalContent = finalContent.replace("$filterType", filterTypeData);
			finalContent = finalContent.replace("$columnData", finalHeaderData.toString());
			finalContent = finalContent.replace("$reportListData", finalTableData.toString());
			finalContent = finalContent.replace("$reportName", ReportNames.CURRENT_STOCK_STATUS_REPORT_FILE);
			finalContent = finalContent.replace("$footerData", finalFooterData.toString());
			log.info("===========END NewRetailProductionPlanBean.generateHtml =============");

		} catch (Exception e) {
			log.info("===========Exception Occured in NewRetailProductionPlanBean.generateHtml =============");
			log.info("===========Exception is =============" ,e);
		}

		return finalContent;
	}
	
	/**
	 * @param obj
	 * @return
	 */
	private String getFormattedValue(Object obj) {
		String value = null;
		try {

			// if (isDecimal(String.valueOf(obj))) {
			value = String.format("%.2f", (double) obj);
			// } else {
			// return (String) obj;
			// }

		} catch (Exception ex) {
			value = obj != null ? String.valueOf(obj) : null;
		}
		return value;
	}

	/**
	 * @param value
	 * @return
	 */
	private boolean isDecimal(Object value) {
		boolean isDecimal = true;
		try {
			Double.parseDouble(String.valueOf(value));
		} catch (Exception ex) {
			isDecimal = false;
		}
		return isDecimal;
	}
	
	public void downloadPdf() {
		InputStream input = null;
		try {
			File files = new File(downloadPath + "/" + downloadFileName);
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			pdfFile = (new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()),
					files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		} catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}

	}
	
	
	public String getProductWisePlan(NewRetailProductionPlanDTO productPlanDTO) {
		log.info("---Start getProductWisePlan() --------------");
		try {
			if(productPlanDTO.getProductId()!=null) {
			newRetailProductionPlanDTO.setProductId(productPlanDTO.getProductId());
			}
			BaseDTO baseDTO=new BaseDTO();
			String url = serverURL + "/newRetail/plan/product/plan/salesdata";
			baseDTO = httpService.post(url, newRetailProductionPlanDTO);
			if(baseDTO!=null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				productPlanList= mapper.readValue(jsonResponse,
							new TypeReference<List<NewRetailProductionPlanDTO>>() {
					});
				log.info("--productPlanList size::",productPlanList.size()>0?productPlanList.size():0);
			}
		}
		catch(Exception ex) {
			log.error("----Exception-----",ex);
		}
		log.info("---End getProductWisePlan() --------------");
		return null;
	}

}
