package in.gov.cooptex.personnel.hrms.rest.ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.enums.AdditionalChargeStage;
import in.gov.cooptex.core.enums.AppConfigEnum;
import in.gov.cooptex.core.enums.AppFeatureEnum;
import in.gov.cooptex.core.enums.ApprovalStage;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.operation.hrms.model.EmpRetirementRequestDetails;
import in.gov.cooptex.operation.hrms.model.EmployeeResignation;
import in.gov.cooptex.personnel.hrms.model.EmpResignationLog;
import in.gov.cooptex.personnel.hrms.model.EmpResignationNote;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("employeeResignationBean")
@Scope("session")
public class EmployeeResignationBean {

	final String RESIGNATION_REQUEST_LIST_PAGE = "/pages/personnelHR/employeeProfile/listResignationRequest.xhtml?faces-redirect=true";
	final String RESIGNATION_REQUEST_CREATE_PAGE = "/pages/personnelHR/employeeProfile/createResignationRequest.xhtml?faces-redirect=true";
	final String RESIGNATION_REQUEST_VIEW_PAGE = "/pages/personnelHR/employeeProfile/viewResignationRequest.xhtml?faces-redirect=true";
	final String HOME_PAGE = "/pages/home.xhtml?faces-redirect=true";

	final String SERVER_URL = AppUtil.getPortalServerURL();

	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	EmployeeResignation selectedResignation, resignation,empResignationStatus;

	@Getter
	@Setter
	List<EmployeeResignation> employeeResignationList;

	@Getter
	@Setter
	LazyDataModel<EmployeeResignation> employeeResignationLazyList;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	String action, focusProperty;

	@Autowired
	ObjectMapper mapper;

	String jsonResponse;

	@Autowired
	ErrorMap errorMap;

	@Setter
	@Getter
	String entitytip = "No regions selected";

	@Autowired
	CommonDataService commonDataService;

	@Setter
	@Getter
	Boolean addButtonFlag = true, viewButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	private String documentFileName;

	@Getter
	@Setter
	private UploadedFile uploadedFile;
	
	@Getter
	@Setter
	List<UserMaster> forwardToUsersList = new ArrayList<>();
	
	@Getter
	@Setter
	EmpResignationNote empResignationNote=new EmpResignationNote();
	
	@Getter
	@Setter
	EmpResignationLog empResignationLog=new EmpResignationLog();
	
	@Setter
	@Getter
	private StreamedContent file;
	
	@Setter
	@Getter
	private String resignationType;
	
	@Setter
	@Getter
	EmpRetirementRequestDetails empRetirementRequestDetails=new EmpRetirementRequestDetails();
	

	public String showResignationListPage() {
		log.info("<===== Starts employeeResignationBean.showResignationListPage ============>" + action);
		try {
			mapper = new ObjectMapper();
			editButtonFlag = true;
			deleteButtonFlag = true;
			addButtonFlag = true;
			selectedResignation = new EmployeeResignation();
			resignation = new EmployeeResignation();
			totalRecords = 0;
			loadResignationLazyList();
			action="ADD";
			
			empResignationStatus=new EmployeeResignation();
			empResignationStatus=checkEmpResignationStatus();
			if(empResignationStatus !=null && empResignationStatus.getStatus().equals("false"))
			{
				Util.addWarn("Employee Master Cannot created to this User");
				return null;
			}
			empRetirementRequestDetails=checkEmpRetirementStatus();
			if(empResignationStatus!=null && empResignationStatus.getEmployee()!=null) {
				if(empResignationStatus.getEmployee().getId().equals(loginBean.getEmployee().getId())) {
					resignation = getResignationDetails(empResignationStatus.getId());
					 if(resignation.getDocumentPath()!=null) {
							File file = new File(resignation.getDocumentPath());
							documentFileName=file.getName();
					}
					return RESIGNATION_REQUEST_VIEW_PAGE;
				}
			}else if(empRetirementRequestDetails!=null && empRetirementRequestDetails.getEmployee()!=null) {
				if(empRetirementRequestDetails.getEmployee().getId().equals(loginBean.getEmployee().getId())) {
					resignation=setRetirementDetailsToDesignation(empRetirementRequestDetails);
					resignation.setResigntionComments(empRetirementRequestDetails.getRetirementComments());
					empResignationStatus=new EmployeeResignation();
					empResignationStatus.setStatus(empRetirementRequestDetails.getStatus());
					if(empRetirementRequestDetails.getDocumentPath()!=null) {
						resignation.setDocumentPath(empRetirementRequestDetails.getDocumentPath()); 
						File file = new File(empRetirementRequestDetails.getDocumentPath());
						documentFileName=file.getName();
					}
					return RESIGNATION_REQUEST_VIEW_PAGE;
				}
			}else {
				  getResEmpDetails();
				  return RESIGNATION_REQUEST_CREATE_PAGE;
			}
			 
			log.info("<===== Ends employeeResignationBean.showResignationListPage ============>");
		}catch(Exception e) {
			log.error("showResignationListPage :: Exception==> "+e);
		}
		return null;
	}

	public String resignationListAction() {
		log.info("<====== EmployeeResignationBean.resignationListAction Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== Resignation add page called.......====>");
				selectedResignation = new EmployeeResignation();
				getResEmpDetails();
				return RESIGNATION_REQUEST_CREATE_PAGE;
			}
			if (selectedResignation == null) {
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_SELECT.getErrorCode());
				return null;
			}
			if (action.equalsIgnoreCase("EDIT")) {
				log.info("<====== EmployeeResignationBean add page called.......====>");
				if (selectedResignation == null || selectedResignation.getId() == null) {
					errorMap.notify(ErrorDescription.EMP_RESIGNATION_SELECT.getErrorCode());
					return null;
				}
				resignation = getResignationDetails(selectedResignation.getId());

				log.info("seleted Resignation::::::::" + resignation);
				return RESIGNATION_REQUEST_CREATE_PAGE;
			}

			if (action.equalsIgnoreCase("DELETE")) {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmResignationDelete').show();");
			}
			if (action.equalsIgnoreCase("VIEW")) {
				if (selectedResignation == null) {
					errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getErrorCode());
					return null;
				}
				log.info("ID-->" + selectedResignation.getId());

				selectedResignation = getResignationDetails(selectedResignation.getId());
				forwardToUsersList = commonDataService.loadForwardToUsersByFeature(AppFeatureEnum.RESIGNATION.name());
				log.info("selected Resignation Object:::" + selectedResignation);
				return RESIGNATION_REQUEST_VIEW_PAGE;
			}

		} catch (Exception e) {
			log.error("Exception Occured in EmployeeResignationBean.resignationListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== EmployeeResignationBean.resignationListAction Ends====>");
		return null;
	}

	private EmployeeResignation getResignationDetails(Long selectedId) {
		log.info("<====== resignation.getResignationDetails starts====>" + selectedId);
		try {

			String getUrl = SERVER_URL + "/resignation/get/:id";
			String url = getUrl.replace(":id",selectedId.toString());

			log.info("resignation Viw By Id  URL: - " + url);
			BaseDTO baseDTO = httpService.get(url);

			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeResignation resignationDetail = mapper.readValue(jsonResponse, EmployeeResignation.class);

			if (resignationDetail != null) {
				resignation = resignationDetail;
			} else {
				log.error("Resignation object failed to desearlize");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				return null;
			}
			if(resignation.getHeadAndRegionOffice()==null) {
				resignation.setHeadAndRegionOffice(new EntityMaster());
			}
			if (baseDTO.getStatusCode() == 0) {
				log.info(" resignation Retrived  SuccessFully ");
			} else {
				log.error(" resignation Retrived Failed with status code " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}
			return resignation;
		} catch (Exception e) {
			log.error("ExceptionException Ocured while get resignation Details==>", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return null;
	}

	@Getter
	@Setter
	List<EmployeeResignation> empList = new ArrayList<>();

	public void loadResignationLazyList() {
		employeeResignationLazyList = new LazyDataModel<EmployeeResignation>() {

			private static final long serialVersionUID = 2849181192334586867L;

			@Override
			public List<EmployeeResignation> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				EmployeeResignation request = new EmployeeResignation();

				try {
					request.setPaginationDTO(
							new PaginationDTO(first / pageSize, pageSize, sortField, sortOrder.toString(), filters));

					BaseDTO baseDTO = httpService.post(SERVER_URL + "/resignation/lazyload", request);
					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
					empList = mapper.readValue(jsonResponse, new TypeReference<List<EmployeeResignation>>() {
					});
					if (empList == null) {
						log.info(" Resignation is null");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}

					if (baseDTO.getStatusCode() == 0) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
						log.info(" Resignation list search Successfully Completed");
					} else {
						log.error(" Resignation list search Failed With status code :: " + baseDTO.getStatusCode());
						errorMap.notify(baseDTO.getStatusCode());
					}
					log.info("<-----Resignation lists------>" + empList.size());
					return empList;

				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Resignation list :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(EmployeeResignation object) {
				// TODO Auto-generated method stub
				return object != null ? object.getId() : null;
			}

			@Override
			public EmployeeResignation getRowData(String rowKey) {
				// TODO Auto-generated method stub
				for (EmployeeResignation emp : empList) {
					if (emp.getId().equals(Long.valueOf(rowKey))) {
						return emp;
					}
				}
				return null;
			}

		};
	}

	public void getResEmpDetails() {
		log.info("<---- : submitResignation :------>");
		try {

			UserMaster user = loginBean.getUserDetailSession();
			resignation.setUserId(user.getId());
			log.info("getId==>" + user.getId());

			String getUrl = SERVER_URL + "/resignation/get/request/:loginId";
			String url = getUrl.replace(":loginId", user.getId().toString());

			BaseDTO baseDTO = httpService.get(url);
			log.info("Save saveUpdateInterchange Response : " + baseDTO);
			if (baseDTO == null) {
				log.error("Base DTO Returned Null Value");
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			}
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			EmployeeResignation resignationDetail = mapper.readValue(jsonResponse, EmployeeResignation.class);
			log.info("resignation==>" + resignationDetail);
			if (resignationDetail != null) {
				resignation = resignationDetail;
			}
			if(resignation.getHeadAndRegionOffice()==null) {
				resignation.setHeadAndRegionOffice(new EntityMaster());
			}
			if (baseDTO.getStatusCode() == 0) {
				//errorMap.notify(ErrorDescription.SUCCESS_RESPONSE.getErrorCode());

			} else {
				log.info("Error while saving employee resignation with code :: " + baseDTO.getStatusCode());
				errorMap.notify(baseDTO.getStatusCode());
			}

		} catch (Exception e) {
			log.error("Exception in job Details Save  :: ", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}

	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts EmployeeResignationBean.onRowSelect ========>" + event);
		selectedResignation = ((EmployeeResignation) event.getObject());
		addButtonFlag = false;
		viewButtonFlag = true;
		
		log.info("<===Ends EmployeeResignationBean.onRowSelect ========>");
	}

	public String submit() {
		log.info("<=======Starts  EmployeeResignationBean.submit ======>");
		BaseDTO response =new BaseDTO();
		try
		{
			if(resignation.getEmployee().getPersonalInfoEmployment() !=null)
			{

				Date dateOfJoining = resignation.getEmployee().getPersonalInfoEmployment().getDateOfJoining();
				if (dateOfJoining == null) {
					errorMap.notify(ErrorDescription.ERROR_EMP_TRANSFER_JOINING_DATE_NOT_FOUND.getErrorCode());
					return null;
				}

				if(resignationType.equals("VRS")) {
					Date retirementDate = resignation.getEmployee().getPersonalInfoEmployment().getRetirementDate();
					if (retirementDate == null) {
						errorMap.notify(ErrorDescription.EMP_RETIREMENT_DATE_MANDATORY.getErrorCode());
						return null;
					}
					Date vrsDate = resignation.getVoluntaryDate();
	                if(vrsDate != null && !vrsDate.after(dateOfJoining) && !vrsDate.before(retirementDate))
	                {
	                	errorMap.notify(ErrorDescription.EMPLOYMENT_DATE_OF_JOINING.getErrorCode());
						return null;
	                }	
				}
				
			}
			if(resignationType.equals("VRS")) {
				resignation.setVoluntaryDate(new Date());
			}
			if(resignation.getEmployee().getPersonalInfoEmployment().getWorkLocation()==null)
			{
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.EMPLOYEE_WORKLOCATION_IS_EMPTY).getCode());
				return null;
			}
			if (action.equalsIgnoreCase("ADD")) {
				resignation.setCreatedBy(loginBean.getUserDetailSession());
			} else if (action.equalsIgnoreCase("EDIT")) {
				resignation.setModifiedBy(loginBean.getUserDetailSession());
			}
			if(resignationType.isEmpty()) {
				Util.addWarn("Please Select Retirment Type");
				return null;
			}
			if(resignationType.equalsIgnoreCase("RESIGNATION")) {
			 response = httpService.post(SERVER_URL + "/resignation/emprequest/create", resignation);
			}else if(resignationType.equalsIgnoreCase("VRS")) {
				response = httpService.post(SERVER_URL + "/resignation/empvrsrequest/create", resignation);
			}
			
			if (response.getStatusCode() == 0) {
				if(resignationType.equalsIgnoreCase("RESIGNATION")) {
				if (action.equalsIgnoreCase("ADD"))
				{
					errorMap.notify(ErrorDescription.EMP_RESIGNATION_SAVED_SUCCESSFULLY.getErrorCode());
					return HOME_PAGE;
					
				}else
				{
					errorMap.notify(ErrorDescription.EMP_RESIGNATION_UPDATED_SUCCESSFULLY.getErrorCode());	
				}
				}else if(resignationType.equalsIgnoreCase("VRS")) {
					if (action.equalsIgnoreCase("ADD"))
					{
						AppUtil.addInfo("Employee Voluntary Retirement saved sucessfully");
						return HOME_PAGE;
						
					}else
					{
						AppUtil.addInfo("Employee Voluntary Retirement updated sucessfully");	
					}
				}
			} else {
				errorMap.notify(response.getStatusCode());
			}
			log.info("<=======Ends employeeResignationBean.submit ======>");
		}catch (Exception e) {
			log.info("<=======Exception Occured in employeeResignationBean.submit ======>");
			errorMap.notify(response.getStatusCode());
			log.error("EmployeeResignationBean :: Submit==> ",e);
		}
		
		return null;
	}

	public String deleteConfirm() {
		log.info("<--- Starts EmployeeResignationBean.deleteConfirm ---->");
		try {
			BaseDTO response = httpService.delete(SERVER_URL + "/resignation/delete/" + selectedResignation.getId());
			if (response.getStatusCode() == 0)
				errorMap.notify(ErrorDescription.EMP_RESIGNATION_DELETED_SUCCESSFULLY.getErrorCode());
			else
				errorMap.notify(response.getStatusCode());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
		} catch (Exception e) {
			log.error("Exception occured while Deleting Resignation....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getErrorCode());
		}
		log.info("<--- Ends EmployeeResignationBean.deleteConfirm ---->");
		return showResignationListPage();
	}

	/*public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			InputStream inputStream = null;
			OutputStream outputStream = null;
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			uploadedFile = event.getFile();
			long size = uploadedFile.getSize();
			log.info("Employee retirement request document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				resignation.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue("EMPLOYEE_RETIREMENT_REQUEST_UPLOAD_PATH");
			File file = new File(uploadPath);
			uploadPath = file.getAbsolutePath() + "/";
			Path path = Paths.get(uploadPath);
			if (Files.notExists(path)) {
				log.info(" Path Doesn't exixts");
				Files.createDirectories(path);
			}
			log.info(" Uploaded document path is :" + uploadPath);
			documentFileName = uploadedFile.getFileName();
			String append = new Date().toGMTString() + documentFileName;
			resignation.setDocumentPath(uploadPath + append);
			errorMap.notify(ErrorDescription.INFO_EMP_RETIREMENT_DOCUMENT_UPLOADED.getErrorCode());
			inputStream = uploadedFile.getInputstream();
			log.info(" Path Doesn't exixts" + append);
			outputStream = new FileOutputStream(uploadPath + append);
			byte[] buffer = new byte[(int) uploadedFile.getSize()];
			int bytesRead = 0;
			while ((bytesRead = inputStream.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}
			log.info(" upload Path is >>>" + uploadPath);
			log.info(" Retirement Document is uploaded successfully");
			if (outputStream != null) {
				outputStream.close();
			}
			if (inputStream != null) {
				inputStream.close();
			}
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}*/

	public EmployeeResignation checkEmpResignationStatus() {
		log.error(" checkEmpResignationStatus method Start------- ");
		EmployeeResignation resignationDetail=new EmployeeResignation();
		try {
			if(loginBean.getEmployee()!=null)
			{
				log.error(" Employee Id----- "+loginBean.getEmployee().getId());
				String getUrl = SERVER_URL + "/resignation/getempresign/"+loginBean.getEmployee().getId();
				BaseDTO baseDTO = httpService.get(getUrl);
				
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				resignationDetail = mapper.readValue(jsonResponse, EmployeeResignation.class);
			}else
			{
				resignationDetail.setStatus("false");
				return resignationDetail;
			}
			
		}
		catch(Exception ex) {
			log.error("Error in checkEmpResignationStatus method Start--- ",ex);
		}
		log.error(" checkEmpResignationStatus method End------- ");
		return resignationDetail;
	}
	
	public void uploadDocument(FileUploadEvent event) {
		log.info("Upload button is pressed..");
		try {
			if (event == null || event.getFile() == null) {
				log.error(" Upload document is null ");
				errorMap.notify(ErrorDescription.CAN_SIG_EMPTY.getErrorCode());
				return;
			}
			uploadedFile = event.getFile();
			long size = uploadedFile.getSize();
			log.info("Employee Resignation document size is : " + size);
			size = size / 1000;
			if (size > 1000) {
				resignation.setDocumentPath(null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
				errorMap.notify(ErrorDescription.EMPLOYEE_PHOTO_SIZE.getErrorCode());
				return;
			}
			String uploadPath = commonDataService.getAppKeyValue(AppConfigEnum.EMPLOYEE_RESIGNATION_VRS_UPLOAD_PATH.toString());
			
			documentFileName = uploadedFile.getFileName();
			String docPath = Util.fileUpload(uploadPath, uploadedFile);
			resignation.setDocumentPath(docPath);
			errorMap.notify(ErrorDescription.SOCIETY_ENROLLMENT_FILE_UPLOAD_SUCCESS.getErrorCode());
			
		} catch (Exception exp) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
			log.error("found exception in file upload", exp);
		}
	}
	
	public void downloadfile() {
		InputStream input = null;
		try {
			if(resignation.getDocumentPath()!=null) {
			File files = new File(resignation.getDocumentPath());
			input = new FileInputStream(files);
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			file=(new DefaultStreamedContent(input, externalContext.getMimeType(files.getName()), files.getName()));
			log.error("exception in filedownload method------- " + files.getName());
			}
			
		} catch (FileNotFoundException e) {
			log.error("exception in filedownload method------- " + e);
		}
		catch (Exception exp) {
			log.error("exception in filedownload method------- " + exp);
		}
		
	}
	
	public EmpRetirementRequestDetails checkEmpRetirementStatus() {
		log.error(" EmpRetirementRequestDetails method Start------- ");
		EmpRetirementRequestDetails empRetirementRequestDetails=new EmpRetirementRequestDetails();
		try {
			log.error(" Employee Id----- "+loginBean.getEmployee().getId());
			String getUrl = SERVER_URL + "/resignation/getretirement/"+loginBean.getEmployee().getId();
			BaseDTO baseDTO = httpService.get(getUrl);
			
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
			empRetirementRequestDetails = mapper.readValue(jsonResponse, EmpRetirementRequestDetails.class);
		}
		catch(Exception ex) {
			log.error("Error in EmpRetirementRequestDetails method Start--- ",ex);
		}
		log.error(" EmpRetirementRequestDetails method End------- ");
		return empRetirementRequestDetails;
	}
	
	public EmployeeResignation setRetirementDetailsToDesignation(EmpRetirementRequestDetails empRetirementRequestDetails) {
		log.error(" setRetirementDetailsToDesignation method Start------- ");
		EmployeeResignation regination=new EmployeeResignation();
		try {
			if(empRetirementRequestDetails!=null) {
				regination.setEmployee(empRetirementRequestDetails.getEmployee());
				regination.setDepartment(empRetirementRequestDetails.getEmployee().getPersonalInfoEmployment().getCurrentDepartment());
				regination.setDesignation(empRetirementRequestDetails.getEmployee().getPersonalInfoEmployment().getCurrentDesignation());
				regination.setEntityType(empRetirementRequestDetails.getEmployee().getPersonalInfoEmployment().getEntityType());
				regination.setHeadAndRegionOffice(empRetirementRequestDetails.getEmployee().getPersonalInfoEmployment().getHeadAndRegionOffice());
				regination.setWorkLocation(empRetirementRequestDetails.getEmployee().getPersonalInfoEmployment().getWorkLocation()); 
				
			}
		}
		catch(Exception exp) {
			log.error("Error in setRetirementDetailsToDesignation method------- ",exp);
		}
		log.error(" setRetirementDetailsToDesignation method end------- ");
		return regination;
	}
	
	public String changeRetirementType() {
		if(resignation == null) {
			Util.addWarn("Employee Master Cannot created to this User");
			return null;
		}
		if(resignationType.equals("VRS")){
			int diff = getDiffYears(resignation.getEmployee().getPersonalInfoEmployment().getDateOfJoining(), new Date());
			if(diff < 20) {
				Util.addWarn("Employee Not Eligible for voluntary retirement");
				resignationType = null;
				return null;
			}
		}
	
		return null;
		
	}
	
	public static int getDiffYears(Date first, Date last) {
	    Calendar a = getCalendar(first);
	    Calendar b = getCalendar(last);
	    int diff = b.get(Calendar.YEAR) - a.get(Calendar.YEAR);
	    return diff;
	}

	public static Calendar getCalendar(Date date) {
	    Calendar cal =  Calendar.getInstance();
	    cal.setTime(date);
	    return cal;
	}
}
