package in.gov.cooptex.admin.meeting.ui;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.meeting.model.Meeting;
import in.gov.cooptex.admin.meeting.model.MeetingLog;
import in.gov.cooptex.admin.meeting.model.MeetingType;
import in.gov.cooptex.common.ui.CommonBean;
import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.Department;
import in.gov.cooptex.core.model.EmployeeMaster;
import in.gov.cooptex.core.model.EntityMaster;
import in.gov.cooptex.core.model.EntityTypeMaster;
import in.gov.cooptex.core.model.SectionMaster;
import in.gov.cooptex.core.model.UserMaster;
import in.gov.cooptex.core.model.VenueMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppFeatureCode;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.EmployeeValidator;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.exceptions.RestException;
import in.gov.cooptex.exceptions.Validate;
import in.gov.cooptex.personnel.hrms.dto.ExternalEmployees;
import in.gov.cooptex.personnel.hrms.dto.LoadEmployeeReponseDTO;
import in.gov.cooptex.personnel.hrms.dto.LoadEmployeeRequestDTO;
import in.gov.cooptex.personnel.hrms.dto.MeetingAddRequestDTO;
import in.gov.cooptex.personnel.hrms.dto.MeetingApproveRequestDTO;
import in.gov.cooptex.personnel.hrms.dto.MeetingListDTO;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("meetingRequestBean")
@Scope("session")
public class MeetingRequestBean extends CommonBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3276331276398702946L;
	final String LIST_PAGE = "/pages/admin/meeting/listMeetingDepartmentRequest.xhtml?faces-redirect=true";
	final String ADD_PAGE = "/pages/admin/meeting/createMeetingDepartmentRequest.xhtml?faces-redirect=true";
	final String VIEW_PAGE = "/pages/admin/meeting/viewMeetingDepartmentRequest.xhtml?faces-redirect=true";
                                  
	final String SERVER_URL = AppUtil.getPortalServerURL()+"/meeting/request";

	ObjectMapper mapper = new ObjectMapper();
	
	
	String jsonResponse;

	@Autowired
	HttpService httpService;

	@Getter
	@Setter
	private RestTemplate restTemplate;

	int meetingTypeListsize;

	int sectionMasterListsize;

	int venueMasterListsize;

	@Getter
	@Setter
	Meeting meeting;

	@Getter
	@Setter
	Date date;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Long totalatendees = null;

	@Getter
	@Setter
	List<MeetingType> meetingTypeList = new ArrayList<MeetingType>();

	@Getter
	@Setter
	List<Department> departmentList = new ArrayList<Department>();

	@Getter
	@Setter
	List<SectionMaster> sectionMasterList = new ArrayList<SectionMaster>();

	@Getter
	@Setter
	List<VenueMaster> venueMasterList = new ArrayList<VenueMaster>();

	@Getter
	@Setter
	List<EmployeeMaster> employeeMasterList = new ArrayList<EmployeeMaster>();

	@Getter
	@Setter
	List<EntityMaster> meetingentitylist = new ArrayList<>();

	@Getter
	@Setter
	String action;

	@Getter
	@Setter
	EntityMaster headangregionaofficeid;

	@Getter
	@Setter
	EntityTypeMaster selctedentitytypeid;

	@Getter
	@Setter
	boolean visibility = false;

	@Getter
	@Setter
	Boolean internal = false;

	@Getter
	@Setter
	Boolean external = true;

	@Getter
	@Setter
	Boolean internal_external = true;

	@Getter
	@Setter
	Long department;

	@Getter
	@Setter
	Long id;

	@Getter
	@Setter
	Long entityid;

	@Getter
	@Setter
	Long sectionMaster;

	@Getter
	@Setter
	Long entity;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	String selectedstatus;

	@Getter
	@Setter
	List<EntityMaster> headandregionalofficelist = new ArrayList<>();
	@Getter
	@Setter
	List<EntityTypeMaster> meetingentitytypelist = new ArrayList<>();

	@Getter
	@Setter
	List<Department> meetingdepartmentlist = new ArrayList<>();

	@Getter
	@Setter
	List<SectionMaster> meetingsectionlist = new ArrayList<>();

	@Getter
	@Setter
	MeetingAddRequestDTO meetingAddRequestDTO = new MeetingAddRequestDTO();

	@Getter
	@Setter
	List<UserMaster> userMasters;

	@Getter
	@Setter
	LoadEmployeeRequestDTO loadEmployeeRequestDTO;

	@Getter
	@Setter
	MeetingApproveRequestDTO meetingApproveRequestDTO = new MeetingApproveRequestDTO();

	@Getter
	@Setter
	Boolean addButtonFlag = false;
	

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	Boolean viewButtonFlag = true;

	@Getter
	@Setter
	Boolean approveButtonFlag = true;

	@Getter
	@Setter
	Boolean radioBtn = false;

	@Getter
	@Setter
	private UploadedFile studenttrainingFile;

	@Getter
	@Setter
	List<LoadEmployeeReponseDTO> employeelist;
	
	@Getter
	@Setter
	String currentCreatedDate = AppUtil.getFormattedCurrentDate();


	@Getter
	@Setter

	List<ExternalEmployees> extenalEmployeeList = new ArrayList<>();

	@Getter
	@Setter
	LoadEmployeeReponseDTO loadEmployeeReponseDTO;

	@Getter
	@Setter
	LazyDataModel<MeetingListDTO> lazymeetingList;

	@Getter
	@Setter
	List<MeetingListDTO> meetingList = null;

	@Getter
	@Setter
	MeetingListDTO selectedmeeting;

	@Getter
	@Setter
	MeetingLog meetingstatus;

	@Getter
	@Setter
	MeetingAddRequestDTO viewmeetingdetails;

	@Getter
	@Setter
	MeetingAddRequestDTO editmeetingdetails;

	// private Date meetingEndTime;

	@Getter
	@Setter
	Meeting metaingdetails;

	@Getter
	@Setter
	String apprremarks;

	@Getter
	@Setter
	String rejtremark;

	@Getter
	@Setter
	List<Meeting> createresponse = null;

	@Getter
	@Setter
	Boolean previousApproval = false;

	@Getter
	@Setter
	String remarkscomment;
	
	@Getter
	@Setter
	String remarks;


	@Getter
	@Setter
	String stage;
	@Autowired
	LoginBean loginBean;

	@Getter
	@Setter
	Boolean approvalFlag = false;

	@Getter
	@Setter
	Boolean finalapprovalFlag = false;
	
	@Getter @Setter
	long inTime ;
	
	@Getter
	@Setter
	Object[] commentAndLogList = new Object[] {};
	
	@Getter
	@Setter
	Long selectedNotificationId = null;
	
	@Getter
	@Setter
	EmployeeMaster createdEmployeeMaster;
	
	@Getter
	@Setter
	Boolean submitButtonFlag = true;
	
	@Getter
	@Setter
	Boolean hideflag = true;
	
	public String showMeetingList() {
		log.info("forward to user info" );
		
		employeelist=new ArrayList<>();
		clear();
		userMasters = new ArrayList<UserMaster>();
		addButtonFlag = false;
		meetingAddRequestDTO = new MeetingAddRequestDTO();
		// lazyloadmeetinglist();
		selectedmeeting = new MeetingListDTO();
		viewButtonFlag = true;
		editButtonFlag = true;
		deleteButtonFlag = true;
		createdEmployeeMaster=new EmployeeMaster();
		createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
		userMaster = new UserMaster();
		lazyLoadMeetingList();
		log.info("forward to user info" + userMasters);
		
		return LIST_PAGE;
	}

	public String showMeetingPageAction() {
		userMasters = new ArrayList<UserMaster>();
		try {
			if (action.equalsIgnoreCase("Create")) {
				clear();
				loadheadandregionaloffice();

				/* loadmeetingentitytypelist(); */
				loadmeetingdepartmentlist();
				loadVenue();
				userMasters = commonDataService.loadForwardToUsersByFeature(AppFeatureCode.MEETING_REQUEST);
				log.info("forward to user info" + userMasters.size());
				employeelist = new ArrayList<>();
				extenalEmployeeList = new ArrayList<>();
				ExternalEmployees extremplyes = new ExternalEmployees();
				extenalEmployeeList.add(extremplyes);
				totalatendees=null;
				createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();
				return ADD_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		return null;
	}

	public void init() {       
		meeting = new Meeting();
		departmentList = commonDataService.loadDepartmentList();
		meetingdepartmentlist = commonDataService.getDepartment();
		createdEmployeeMaster = commonDataService.loadEmployeeLoggedInUserDetails();

		loadMeetingType();
        
		lazyLoadMeetingList();

		loadSection();
	}
	
	public void loadMeetingType() {
		log.info("<<<< ----------Start loadMeetingType ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			meetingTypeList = new ArrayList<MeetingType>();

			String url = SERVER_URL + "/loadmeetingtype";
			log.info("loadMeetingType url==>"+url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				meetingTypeList = mapper.readValue(jsonResponse, new TypeReference<List<MeetingType>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (meetingTypeList != null) {
			meetingTypeListsize = meetingTypeList.size();
		} else {
			meetingTypeListsize = 0;
		}
		log.info("<<<< ----------loadMeetingType-meetingTypeListsize ------- >>>> " + meetingTypeListsize);
	}

	public void loadSection() {
		log.info("<<<< ----------Start loadSection ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			sectionMasterList = new ArrayList<SectionMaster>();

			String url = SERVER_URL + "/loadsection";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (sectionMasterList != null) {
			sectionMasterListsize = sectionMasterList.size();
		} else {
			sectionMasterListsize = 0;
		}
		log.info("<<<< ----------loadSection-sectionMasterListListsize ------- >>>> " + sectionMasterListsize);
	}

	public void loadVenue() {
		log.info("<<<< ----------Start loadVenue ------- >>>>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			venueMasterList = new ArrayList<VenueMaster>();

			String url = SERVER_URL + "/loadvenue";
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				venueMasterList = mapper.readValue(jsonResponse, new TypeReference<List<VenueMaster>>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (venueMasterList != null) {
			venueMasterListsize = venueMasterList.size();
		} else {
			venueMasterListsize = 0;
		}
		log.info("<<<< ----------loadVenue-venueMasterListsize ------- >>>> " + venueMasterListsize);
	}

	public String createMeeting() {

		log.info("<<<< ----------Start creatMereting ------- >>>>" + extenalEmployeeList.size());
		log.info("meeting edit fonction is clled");
		log.info("create meeting id called", meetingAddRequestDTO);
		meetingAddRequestDTO.setExternalEmployess(new ArrayList<>());
		if(meetingAddRequestDTO.getMeetingStartTime().after(meetingAddRequestDTO.getMeetingEndTime())) {
			//AppUtil.addError("Meeting start time should less than meeting end time");
			errorMap.notify(ErrorDescription.getError(MastersErrorCode.MEETING_START_TIME_SHOULD_LESS_THAN_END_TIME).getCode());
			return null;
		}
		
		if(employeelist.size()==0)
		{
			AppUtil.addError("Internal Employee list should not be empty.");
			return null;
		}
		
		if(employeelist.size() == 0 && extenalEmployeeList.size() == 0)
		{
			AppUtil.addError("Internal and external Employee list should not be empty.");
			return null;
		}
		
		Validate.notNullOrEmpty(userMaster,
				ErrorDescription.MEETING_REQUEST_FORWARD_TO_EMPTY);
		Validate.notNullOrEmpty(meetingAddRequestDTO.getForwardFor(),
				ErrorDescription.MEETING_REQUEST_FORWARD_FOR_EMPTY);
		if (meetingAddRequestDTO.getNote() == null || meetingAddRequestDTO.getNote().equals("")) {
			errorMap.notify(ErrorDescription.MEETING_REQUEST_NOTE_EMPTY.getCode());
			return "";
		}
		if (meetingAddRequestDTO.getId() == null) {
			try {
				
				if (meetingAddRequestDTO.getNote() == null || meetingAddRequestDTO.getNote().equals("")) {
					errorMap.notify(ErrorDescription.MEETING_REQUEST_NOTE_EMPTY.getCode());
					return "";
				}

				if (extenalEmployeeList.size() > 0) {
					for (ExternalEmployees externalEmployeeObj : extenalEmployeeList) {
						if (!externalEmployeeObj.getLandLineNumber().equalsIgnoreCase(null) && !externalEmployeeObj.getLandLineNumber().equalsIgnoreCase("")) {
							if (!EmployeeValidator.validateMobileNo(externalEmployeeObj.getLandLineNumber())) {
								log.error(" Employee Invalid Contact Number ");
								errorMap.notify(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getErrorCode());
								return null;
							}
						}
						if (!externalEmployeeObj.getEmailId().equalsIgnoreCase(null) && !externalEmployeeObj.getEmailId().equalsIgnoreCase("")) {
							if (!EmployeeValidator.validateEmail(externalEmployeeObj.getEmailId())) {
								log.error(" Employee Invalid Email Id ");
								errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getErrorCode());
								return null;
							}
						}
						if (!externalEmployeeObj.getEmpName().equalsIgnoreCase(null) && !externalEmployeeObj.getEmpName().equalsIgnoreCase("")) {
							meetingAddRequestDTO.getExternalEmployess().add(externalEmployeeObj);
						}
					}
				}
				BaseDTO baseDTO = new BaseDTO();
				meetingAddRequestDTO.setMeetingAttendees(totalatendees);
				if(meetingAddRequestDTO.getExternalEmployess()!=null) {
				log.info("-----ExternalEmployess---->" + meetingAddRequestDTO.getExternalEmployess().size());
				}
				if(meetingAddRequestDTO.getInternalEmployees()!=null) {
				log.info("-----InternalEmployess---->" + meetingAddRequestDTO.getInternalEmployees().size());
				}
				String url = AppUtil.getPortalServerURL() + "/meeting/create";
				if(userMaster!=null && userMaster.getId()!=null)
				{
					meetingAddRequestDTO.setForwardTo(userMaster.getId());
				}
				baseDTO = httpService.post(url, meetingAddRequestDTO);

				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("selectedSocietyRegistration saved successfully..........");
					errorMap.notify(ErrorDescription.MEETING_REQUEST_INSERTED_SUCCESSFULLY.getCode());

				} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
				log.info("create response", createresponse);
			} catch (Exception e) {
				log.error("found exception in onchangeEntityType: ", e);
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				return null;
			}
			return showMeetingList();
		} else {
			try {
				
				BaseDTO baseDTO = new BaseDTO();
				meetingAddRequestDTO.setMeetingAttendees(totalatendees);
				if (extenalEmployeeList.size() > 0) {
					for (ExternalEmployees externalEmployeeObj : extenalEmployeeList) {
						if (!externalEmployeeObj.getLandLineNumber().equalsIgnoreCase(null) && !externalEmployeeObj.getLandLineNumber().equalsIgnoreCase("")) {
							if (!EmployeeValidator.validateMobileNo(externalEmployeeObj.getLandLineNumber())) {
								log.error(" Employee Invalid Contact Number ");
								errorMap.notify(ErrorDescription.EMPLOYEE_MOBILE_INVALID.getErrorCode());
								return null;
							}
						}
						if (!externalEmployeeObj.getEmailId().equalsIgnoreCase(null) && !externalEmployeeObj.getEmailId().equalsIgnoreCase("")) {
							if (!EmployeeValidator.validateEmail(externalEmployeeObj.getEmailId())) {
								log.error(" Employee Invalid Email Id ");
								errorMap.notify(ErrorDescription.EMPLOYEE_PERSONAL_EMAIL_INVALID.getErrorCode());
								return null;
							}
						}
						if (!externalEmployeeObj.getEmpName().equalsIgnoreCase(null) && !externalEmployeeObj.getEmpName().equalsIgnoreCase("")) {
							meetingAddRequestDTO.getExternalEmployess().add(externalEmployeeObj);
						}
					}
				}

				meetingAddRequestDTO.setInternalEmployees(employeelist);
				if(userMaster!=null && userMaster.getId()!=null)
				{
					meetingAddRequestDTO.setForwardTo(userMaster.getId());
				}
				String url = AppUtil.getPortalServerURL() + "/meeting/update";
				baseDTO = httpService.post(url, meetingAddRequestDTO);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					log.info("selectedSocietyRegistration saved successfully..........");
					errorMap.notify(ErrorDescription.MEETING_REQUEST_INSERTED_SUCCESSFULLY.getCode());

				} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				} else {
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
				}
			} catch (Exception e) {
				log.error("found exception in onchangeEntityType: ", e);
				return null;
			}
		}
		log.info("<<<< ----------End createMeetingType ------- >>>>");
		meetingAddRequestDTO = new MeetingAddRequestDTO();

		return showMeetingList();

	}

	public void loadheadandregionaloffice() {
		log.info("<<<< ----------Starts loadHeadAndRegionalOffice ------- >>>>");
		headandregionalofficelist = commonDataService.loadHeadAndRegionalOffice();
		log.info("headandregionalofficelist---------------->>>>" + headandregionalofficelist.size());

		log.info("<<<< ----------Ends loadHeadAndRegionalOffice ------- >>>>");
	}

	public void loadAllEntityTypesByRegionId() {
		
		log.info("<======== meetingRequestBean.loadAllEntityByRegionId() calling ======>");
		meetingentitytypelist = new ArrayList<>();
		try {
			if(headangregionaofficeid != null) {
				if ( headangregionaofficeid.getEntityTypeMaster() != null
						&& headangregionaofficeid.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
					meetingAddRequestDTO.setEnityId(headangregionaofficeid.getId());
					visibility = true;
	
				} else {
					visibility = false;
				}
				String url = AppUtil.getPortalServerURL() + "/entitytypemaster/getAllEntityTypesByRegionId/"
						+ headangregionaofficeid.getId();
				log.info("url" + url);
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null) {
					String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
					meetingentitytypelist = mapper.readValue(jsonValue, new TypeReference<List<EntityTypeMaster>>() {
					});
				}
				log.info("meetingentitytypelist size" + meetingentitytypelist.size());
			}
		} catch (Exception e) {
			log.error("<========== Exception meetingRequestBean.loadAllEntityTypesByRegionId()==========>", e);
		}
	}

	public void loadAllEntityByRegionIdandEntityTypeId() {
		log.info("<======== meetingRequestBean.loadAllEntityByRegionIdandEntityTypeId() calling ======>");
		meetingentitylist = new ArrayList<>();
		Long regionId = 0l;
		Long entityTypeId = 0l;
		try {
			if (getHeadangregionaofficeid() != null) {
				regionId = getHeadangregionaofficeid().getId();
			}
			if (getSelctedentitytypeid() != null) {
				entityTypeId = getSelctedentitytypeid().getId();
			}
			if (regionId != null && entityTypeId != null) {
				meetingentitylist = commonDataService.loadEntityByregionOrentityTypeId(regionId, entityTypeId);
				log.info("loadAllEntityByRegionIdandEntityTypeId size" + meetingentitylist.size());
			}
		} catch (Exception e) {
			log.error("<========== Exception meetingRequestBean.loadAllEntityByRegionIdandEntityTypeId()==========>",
					e);
		}
	}

	public void loadmeetingdepartmentlist() {
		log.info("<<<< ----------Starts loadMeretingDepartmentList ------- >>>>");
		meetingdepartmentlist = commonDataService.getDepartment();
		log.info("meeting department list", meetingdepartmentlist);
		log.info("<<<< ----------End loadMeretingDepartmentList ------- >>>>");
	}

	public void onSelectdepartment() {
		log.info("<<<< ----------Starts loadMeretinSectionList ------- >>>>");
		meetingsectionlist = new ArrayList<>();
		//meetingsectionlist = commonDataService.getActiveSectionMastersByDepartment(department);
		meetingsectionlist=getActiveSectionMastersByDepartment();
		log.info("Section list........." + meetingsectionlist);
		log.info("<<<< ----------End loadMeretinSectionList ------- >>>>");
	}

	public void headAndregional() {
		log.info("<<<< ----------Starts filternig based on Head And Regional Office------- >>>>");
		log.info("entity master", headangregionaofficeid);

		try {

			if (headangregionaofficeid != null && headangregionaofficeid.getEntityTypeMaster() != null
					&& headangregionaofficeid.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
				meetingAddRequestDTO.setEnityId(headangregionaofficeid.getId());
				visibility = true;

			} else {
				visibility = false;
			}

		} catch (Exception exp) {
			log.error("found exception in onchangeEntityType: ", exp);
		}

		log.info("<<<< ----------End filternig based on Head And Regional Office------- >>>>");
	}

	public void loadEmployeeList() {
		employeelist = new ArrayList<>();
        if(employeelist==null) {
        	employeelist=new ArrayList<LoadEmployeeReponseDTO>();
        }
		log.info("<<<< ----------Starts loadEmployeeList------- >>>>" + employeelist.size());
		List<LoadEmployeeReponseDTO> loadEmployeeReponseDTOList = null;
		LoadEmployeeRequestDTO loadEmployeeRequestDTO = new LoadEmployeeRequestDTO();
		if (headangregionaofficeid != null && headangregionaofficeid.getEntityTypeMaster() != null
				&& headangregionaofficeid.getEntityTypeMaster().getEntityCode().equalsIgnoreCase("HEAD_OFFICE")) {
			loadEmployeeRequestDTO.setEntityId(headangregionaofficeid.getId());

		} else {
			loadEmployeeRequestDTO.setEntityId(entityid);
		}
		//loadEmployeeRequestDTO.setDepartmentId(department);
		//loadEmployeeRequestDTO.setSectionId(sectionMaster);
		List<Long> departmentIds = new ArrayList<Long>();
		List<Long> sectionIds = new ArrayList<Long>();
		for (Department obj : departmentList) {
			departmentIds.add(obj.getId());
		}
		for (SectionMaster secObj : sectionMasterList) {
			sectionIds.add(secObj.getId());
		}
		loadEmployeeRequestDTO.setDepartmentIds(departmentIds);
		loadEmployeeRequestDTO.setSectionIds(sectionIds);
		
		log.info("loadEmployeeList :: entity id==> " + entityid);
		log.info("loadEmployeeList :: department id==> " + department);
		log.info("loadEmployeeList :: sectionmaster id==> " + sectionMaster);

		log.info("loadEmployeeList :: request data" + loadEmployeeRequestDTO);
		try {
			// BaseDTO response = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/meeting/search";
			log.info("loadEmployeeList :: url==> "+url);
			BaseDTO baseDTO = httpService.post(url, loadEmployeeRequestDTO);

			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				loadEmployeeReponseDTOList = mapper.readValue(jsonResponse, new TypeReference<List<LoadEmployeeReponseDTO>>() {
				});
				if (loadEmployeeReponseDTOList == null || loadEmployeeReponseDTOList.isEmpty()) {
					
					errorMap.notify(ErrorDescription.MEETING_REQUEST_INTERNAL_EMPLOYEE_EMPTY_LIST.getCode());
				}else {
					for(LoadEmployeeReponseDTO loadEmployeeReponseDTO : loadEmployeeReponseDTOList) {
						employeelist.add(loadEmployeeReponseDTO);
					}
					log.info("loadEmployeeReponseDTOList size==> "+loadEmployeeReponseDTOList.size());
				}
				meetingAddRequestDTO.setInternalEmployees(employeelist);
				totalatendees = new Long(baseDTO.getTotalRecords());
			}

		} catch (Exception e) {
			log.error("Exception occured while loadAllroleList ... ", e);
		}

		log.info("<<<< ----------Ends loadEmployeeList------- >>>>");
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('uploademp').hide();");

	}

	public void lazyLoadMeetingList() {
		log.info("<<<< ----------Starts lazyLoadMeetingList------- >>>>");

		lazymeetingList = new LazyDataModel<MeetingListDTO>() {
			private static final long serialVersionUID = 8422543223567350599L;

			@Override
			public List<MeetingListDTO> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = SERVER_URL + "/getlistmeeting";
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						ObjectMapper mapper = new ObjectMapper();
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						meetingList = mapper.readValue(jsonResponse, new TypeReference<List<MeetingListDTO>>() {
						});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
					} else {
						meetingList = new ArrayList<>();
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) /*
										 * showBloodGroupListPageAction method used to action based view pages and
										 * render viewing
										 */ {
					log.error("Exception occured in meetingList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return meetingList;
			}

			@Override
			public Object getRowKey(MeetingListDTO res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public MeetingListDTO getRowData(String rowKey) {
				try {
					for (MeetingListDTO invoice : meetingList) {
						if (invoice.getId().equals(Long.valueOf(rowKey))) {
							selectedmeeting = invoice;
							return invoice;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<<<< ----------Ends lazyLoadMeetingList------- >>>>");

	}

	/**
	 * Create Note
	 */
	public void createParams() {
		log.info("<<<< ----------Starts createNote------- >>>>");
		// invoiceSaveRequestObj.setNote(note);
		log.info("Create Note", meetingAddRequestDTO.getNote());
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
		log.info("<<<< ----------Ends createNote------- >>>>");
	}

	public String selectedMeetingDateformat(Date d)

	{
		log.info("<<<< ----------Starts MeetingDate formate changing------ >>>>");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");

		log.info("<<<< ----------Ends MeetingDate formate changing------ >>>>");

		return simpleDateFormat.format(d);

	}

	public String meetingStartTimeFormat(Date d) {
		log.info("<<<< ----------Starts MeetingStartTime formate changing------ >>>>");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

		log.info("<<<< ----------Ends MeetingStartTime formate changing------ >>>>");
		return simpleDateFormat.format(d);
	}

	public String meetingEndTimeFormat(Date d) {
		log.info("<<<< ----------Starts MeetingSEndTime formate changing------ >>>>");

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");

		log.info("<<<< ----------Ends MeetingSEndTime formate changing------ >>>>");
		return simpleDateFormat.format(d);
	}

	public void changeCurrentStatus(String value) {
		log.info("Changing the status to " + value);
		setStage(value);
		setRemarkscomment("");
		setRemarks("");
	}

	public String viewDetails() {
		log.info("<<<< ----------Starts viewDetails------ >>>>");
		employeelist=new ArrayList<>();
		log.info("selected view id", selectedmeeting);
		long meetingid = selectedmeeting.getId();
		log.info("meeting id", meetingid);
		try {
			setApprovalFlag(false);
			clear();
			viewmeetingdetails = new MeetingAddRequestDTO();
			if (action.equalsIgnoreCase("View")) {
				userMasters = new ArrayList<UserMaster>();
				userMasters = commonDataService.loadForwardToUsersByFeature("MEETING_DEPARTMENT_REQUEST");
				log.info("view meeting list called..");

				String url = AppUtil.getPortalServerURL() + "/meeting/getbyid/" + meetingid +"/"+ (selectedNotificationId!=null?selectedNotificationId:0) ;
				BaseDTO baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					viewmeetingdetails = mapper.readValue(jsonResponse, new TypeReference<MeetingAddRequestDTO>() {
					});
					commentAndLogList =baseDTO.getCommentAndLogList();
					previousApproval = viewmeetingdetails.getForwardFor();
					log.info("forward for ---->" + viewmeetingdetails.getForwardFor());
					if (viewmeetingdetails.getForwardTo().equals(loginBean.getUserDetailSession().getId())) {
						log.info("============approval flag===============" + viewmeetingdetails.getForwardTo());
						if(viewmeetingdetails.getForwardFor()) {
							userMaster.setId(viewmeetingdetails.getForwardTo());
						}
						setApprovalFlag(true);
					}
					setFinalapprovalFlag(viewmeetingdetails.getForwardFor());
					
					if (viewmeetingdetails.getForwardTo().equals(loginBean.getUserDetailSession().getId()) && viewmeetingdetails.getForwardFor().equals(false)) {
						setHideflag(false);
						setSubmitButtonFlag(false);
					}
					if (viewmeetingdetails.getForwardTo().equals(loginBean.getUserDetailSession().getId()) && viewmeetingdetails.getForwardFor().equals(true)) {
						setHideflag(false);
						setSubmitButtonFlag(true);
					}
					if(viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("FINAL_APPROVED")||
							viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("REJECTED")){
						setHideflag(false);
						setSubmitButtonFlag(true);
						
					}
					
					
				} else {

					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}

				log.info("----bean.viewmeetingdetails.meetingLog.stage--------------------->"
						+ viewmeetingdetails.getMeetingLog().getStage());
				log.info("viewmeetingdetails ::::::::::note ::::::" + viewmeetingdetails.getNote());
				return VIEW_PAGE;
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("viewDetails exception------",e);
		}

		log.info("<<<< ----------Ends viewDetails------ >>>>");

		return null;

	}

	public void onRowSelect(SelectEvent event) {

		log.info("<<<< ----------Starts onRowSelect------ >>>>");
		
		selectedmeeting = ((MeetingListDTO) event.getObject());
		addButtonFlag = true;
		viewButtonFlag = false;
		deleteButtonFlag = false;
		
		if (selectedmeeting.getStatus().equalsIgnoreCase("INITIATED") || selectedmeeting.getStatus().equalsIgnoreCase("REJECTED")) {
			editButtonFlag = false;
			}else
		{
		  editButtonFlag = true;
		}
		if (selectedmeeting.getStatus().equalsIgnoreCase("FINAL_APPROVED") || selectedmeeting.getStatus().equalsIgnoreCase("APPROVED") ){
			deleteButtonFlag = true;
		}
		if (selectedmeeting.getStatus().equalsIgnoreCase("REJECTED") ){
			deleteButtonFlag = false;
		}
		log.info("<<<< ----------Starts onRowSelect-  ceu----- >>>>"+createdEmployeeMaster.getUserId());
		
		Long createdUserId=createdEmployeeMaster.getUserId();
		Long createdById=selectedmeeting.getCreatedBy();
		if(createdById.longValue()!=createdUserId.longValue())
		{
			editButtonFlag = true;
			deleteButtonFlag = true;
		}
						
		log.info("<<<< ----------Ends onRowSelect------ >>>>");
		}
	
	

	public String editMeetingDetails() {

		log.info("<<<< ----------Starts editMeetingDetails------ >>>>" + selectedmeeting.getStatus());

		log.info("selected Edit id" + selectedmeeting);
		long meetingid = selectedmeeting.getId();
		log.info("meeting id", meetingid);
		radioBtn = false;
		BaseDTO baseDTO=new BaseDTO();
		try {
			clear();
			if (selectedmeeting.getStatus().equalsIgnoreCase("INITIATED") || selectedmeeting.getStatus().equalsIgnoreCase("REJECTED")) {

				editmeetingdetails = new MeetingAddRequestDTO();
				loadVenue();
				loadheadandregionaloffice();
				loadmeetingdepartmentlist();
				log.info("Edit meeting list called..");

				String url = AppUtil.getPortalServerURL() + "/meeting/getbyid/" + meetingid +"/"+ (selectedNotificationId!=null?selectedNotificationId:0);
				 baseDTO = httpService.get(url);
				if (baseDTO != null && baseDTO.getStatusCode() == 0) {
					ObjectMapper mapper = new ObjectMapper();
					jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
					editmeetingdetails = mapper.readValue(jsonResponse, new TypeReference<MeetingAddRequestDTO>() {
					});

					meetingAddRequestDTO = editmeetingdetails;
					// employeelist = editmeetingdetails.getInternalEmployees();
					employeelist=new ArrayList<>();
					extenalEmployeeList = new ArrayList<>();
					if(null != editmeetingdetails.getExternalEmployess() && editmeetingdetails.getExternalEmployess().size() > 0){
						extenalEmployeeList = editmeetingdetails.getExternalEmployess();
					}else{
						ExternalEmployees extremplyes = new ExternalEmployees();
						extenalEmployeeList.add(extremplyes);
					}
					userMaster = null;
					userMasters = new ArrayList<UserMaster>();
					userMasters = commonDataService.loadForwardToUsersByFeature("MEETING_DEPARTMENT_REQUEST");
					for (LoadEmployeeReponseDTO intrdad : editmeetingdetails.getInternalEmployees()) {
						if (intrdad.getEmpMasterId() == null) {
							log.info("<-----Employee Master Id is null---->");
						} else {
							employeelist.add(intrdad);
						}
					}
					totalatendees=(long)employeelist.size();
					log.info("edit details ", meetingAddRequestDTO);
					return ADD_PAGE;
				}
			} else {
				errorMap.notify(ErrorDescription.MEETING_REQUEST_CAN_NOT_EDIT.getCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			log.error("<<<< ----------Exception-- >>>>",e);
		}
		log.info("<<<< ----------Ends editMeetingDetails------ >>>>");
		return null;
	}

	/*
	 * public String submitApprovedStatus() {
	 * log.info("<<<< ----------Starts submitApprovedStatus------ >>>>");
	 * 
	 * log.info("Create Note", meetingApproveRequestDTO.getRemarks());
	 * meetingApproveRequestDTO.setId(viewmeetingdetails.getId());
	 * meetingApproveRequestDTO.setRemarks(apprremarks);
	 * 
	 * RequestContext context = RequestContext.getCurrentInstance();
	 * context.execute("PF('dlgComments1').hide();");
	 * 
	 * log.info("submited info", meetingApproveRequestDTO);
	 * 
	 * try {
	 * 
	 * meetingstatus = new MeetingLog(); log.info("view meeting list called..");
	 * 
	 * if
	 * (viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("REJECTED"))
	 * { meetingApproveRequestDTO.setStatus("REJECTED"); } else { if
	 * (viewmeetingdetails.getForwardFor() == true && previousApproval == true &&
	 * viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("APPROVED")) {
	 * meetingApproveRequestDTO.setStatus("FINAL_APPROVED"); } else if
	 * (viewmeetingdetails.getForwardFor() == true ||
	 * viewmeetingdetails.getForwardFor() == false && previousApproval == false &&
	 * viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("APPROVED")) {
	 * meetingApproveRequestDTO.setStatus("APPROVED"); } } String url = SERVER_URL +
	 * "/meeting/approvemeeting"; BaseDTO baseDTO = httpService.post(url,
	 * meetingApproveRequestDTO); if (baseDTO != null && baseDTO.getStatusCode() ==
	 * 0) { ObjectMapper mapper = new ObjectMapper(); jsonResponse =
	 * mapper.writeValueAsString(baseDTO.getResponseContent()); meetingstatus =
	 * mapper.readValue(jsonResponse, new TypeReference<MeetingLog>() { });
	 * 
	 * } } catch (Exception e) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); }
	 * log.info("<<<< ----------Ends submitApprovedStatus------ >>>>");
	 * selectedmeeting = new MeetingListDTO(); return LIST_PAGE;
	 * 
	 * }
	 */

	/*
	 * public String submitRejectedStatus() {
	 * log.info("<<<< ----------Starts submitRejectedStatus------ >>>>");
	 * log.info("Create Note", meetingApproveRequestDTO.getRemarks());
	 * meetingApproveRequestDTO.setId(viewmeetingdetails.getId());
	 * meetingApproveRequestDTO.setRemarks(rejtremark); RequestContext context =
	 * RequestContext.getCurrentInstance();
	 * context.execute("PF('dlgComments2').hide();");
	 * 
	 * log.info("submited info", meetingApproveRequestDTO); try { if
	 * (viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("REJECTED"))
	 * { meetingApproveRequestDTO.setStatus("REJECTED"); } else { if
	 * (viewmeetingdetails.getForwardFor() == true && previousApproval == true &&
	 * viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("APPROVED")) {
	 * meetingApproveRequestDTO.setStatus("FINAL_APPROVED"); } else if
	 * (viewmeetingdetails.getForwardFor() == true ||
	 * viewmeetingdetails.getForwardFor() == false && previousApproval == false &&
	 * viewmeetingdetails.getMeetingLog().getStage().equalsIgnoreCase("APPROVED")) {
	 * meetingApproveRequestDTO.setStatus("APPROVED"); } } meetingstatus = new
	 * MeetingLog(); log.info("view meeting list called..");
	 * 
	 * String url = SERVER_URL + "/meeting/approvemeeting"; BaseDTO baseDTO =
	 * httpService.post(url, meetingApproveRequestDTO); if (baseDTO != null &&
	 * baseDTO.getStatusCode() == 0) { ObjectMapper mapper = new ObjectMapper();
	 * jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
	 * meetingstatus = mapper.readValue(jsonResponse, new
	 * TypeReference<MeetingLog>() { });
	 * 
	 * } } catch (Exception e) {
	 * errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode()); }
	 * log.info("<<<< ----------Ends submitRejectedStatus------ >>>>");
	 * 
	 * selectedmeeting = new MeetingListDTO(); return LIST_PAGE;
	 * 
	 * }
	 */
	// status method
	public String statusUpdate() {
		log.info("inside statusUdate ------->" + viewmeetingdetails.getForwardTo() + viewmeetingdetails.getForwardFor()
				+ viewmeetingdetails.getNote());
		try {
			
			// Validate.notNullOrEmpty(vewmeetingdetails.getNote(),
			// ErrorDescription.MEETING_REQUEST_NOTE_EMPTY);

			MeetingAddRequestDTO meetingAddRequestDTO = new MeetingAddRequestDTO();
			MeetingLog meetingLogObj = new MeetingLog();
			
			if (viewmeetingdetails.getForwardFor() == true && previousApproval == true
					&& meetingLogObj.getStage()!=null&& meetingLogObj.getStage().equalsIgnoreCase("APPROVED")) {
				Validate.notNullOrEmpty(userMaster.getId(),
						ErrorDescription.MEETING_REQUEST_FORWARD_TO_EMPTY);
				Validate.notNullOrEmpty(viewmeetingdetails.getForwardFor(),
						ErrorDescription.MEETING_REQUEST_FORWARD_FOR_EMPTY);
			}
			log.info("-------------------------"+remarks);
			if("APPROVED".equalsIgnoreCase(stage) || "FINAL APPROVED".equalsIgnoreCase(stage) || "REPORT FINAL APPROVED".equalsIgnoreCase(stage)) {
				meetingLogObj.setRemarks(remarks);
			}else {
			meetingLogObj.setRemarks(remarkscomment);
			}
			meetingLogObj.setStage(stage);
			meetingAddRequestDTO.setMeetingLog(meetingLogObj);
			meetingAddRequestDTO.setNote(viewmeetingdetails.getNote());
			meetingAddRequestDTO.setForwardTo(userMaster.getId());
			meetingAddRequestDTO.setForwardFor(viewmeetingdetails.getForwardFor());
			meetingAddRequestDTO.setId(viewmeetingdetails.getId());

			if (meetingLogObj.getStage().equalsIgnoreCase("REJECTED")) {
				meetingAddRequestDTO.getMeetingLog().setStage("REJECTED");
			} else {
				if (viewmeetingdetails.getForwardFor() == true && previousApproval == true
						&& meetingLogObj.getStage().equalsIgnoreCase("APPROVED")) {
					meetingAddRequestDTO.getMeetingLog().setStage("FINAL_APPROVED");
				} else if (viewmeetingdetails.getForwardFor() == true || viewmeetingdetails.getForwardFor() == false
						&& previousApproval == false && meetingLogObj.getStage().equalsIgnoreCase("APPROVED")) {
					meetingAddRequestDTO.getMeetingLog().setStage("APPROVED");
				}
			}
			String url= AppUtil.getPortalServerURL() + "/meeting/approvemeeting";
			BaseDTO response = httpService.post(url, meetingAddRequestDTO);
			if (response != null && response.getStatusCode() == 0) {
				if (meetingAddRequestDTO.getMeetingLog().getStage().equalsIgnoreCase("FINAL_APPROVED")
						|| meetingAddRequestDTO.getMeetingLog().getStage().equalsIgnoreCase("APPROVED")) {
					errorMap.notify(ErrorDescription.MEETING_REQUEST_APPROVED_SUCCESSFULLY.getCode());
				} else if (meetingAddRequestDTO.getMeetingLog().getStage().equalsIgnoreCase("REJECTED")) {
					errorMap.notify(ErrorDescription.MEETING_REQUEST_REJECTED_SUCCESSFULLY.getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (RestException restException) {
			errorMap.notify(restException.getStatusCode());
			return null;
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}

		return showMeetingList();
	}

	public String deleteMeeting() {
		log.info("<<<< ----------Starts deleteMeeting----- >>>>");
		long meetingid = selectedmeeting.getId();
		log.info("meeting id", meetingid);
		try {

			log.info("view meeting list called..");

			String url = AppUtil.getPortalServerURL() + "/meeting/delete/" + meetingid;
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null && baseDTO.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.MEETING_REQUEST_DELETED_SUCCESSFULLY.getErrorCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmDelete').hide();");
			} else if (baseDTO != null && baseDTO.getStatusCode() != 0) {
				errorMap.notify(baseDTO.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}

		log.info("<<<< ----------Ends deleteMeeting----- >>>>");
		return showMeetingList();

	}

	public String viewListpage() {
		selectedmeeting = new MeetingListDTO();
		return LIST_PAGE;

	}

	public String cancelrequest() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('uploademp').hide();");
		headangregionaofficeid = null;
		selctedentitytypeid = null;
		entityid = null;
		department = null;
		sectionMaster = null;
		return "";
	}

	public void addRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		log.info("-------extenalEmployeeList.get(rowIndex).getEmpName()----------------------->"
				+ extenalEmployeeList.get(rowIndex).getEmpName());
		if (extenalEmployeeList.get(rowIndex).getEmpName().equalsIgnoreCase(null)
				|| extenalEmployeeList.get(rowIndex).getEmpName().equalsIgnoreCase("")) {
			log.info("<----------inside empty emp name ------>");
			errorMap.notify(ErrorDescription.MEETING_REQUEST_EXTERNAL_NAME_EMPTY.getCode());
			return;
		} else {
			ExternalEmployees extremplyes = new ExternalEmployees();
			extenalEmployeeList.add(extremplyes);
		}
	}
	
	public void deleteInternalRecord()
	{
		totalatendees=(long)employeelist.size();
	}

	public void deleteRow(int rowIndex) {
		log.info("row index------" + rowIndex);
		extenalEmployeeList.remove(rowIndex);
		
		if (extenalEmployeeList.size() == 0) {
			ExternalEmployees extremplyes = new ExternalEmployees();
			extenalEmployeeList.add(extremplyes);
		}

	}

	public String clearAll() {

		selectedmeeting = new MeetingListDTO();
		addButtonFlag = true;

		return LIST_PAGE;

	}

	public void deleteInternalempRow(int rowIndex) {
		employeelist.remove(rowIndex);

	}

	// public Date getmeetingEndTime() {
	// return meetingEndTime;
	// }
	//
	// public void setMeetingEndTime(Date meetingEndTime) {
	// this.meetingEndTime = meetingEndTime;
	// }

	public void meetingType() {
		log.info("<============meetingType============>" + meetingAddRequestDTO.getMeetingTypeId());
		if (meetingAddRequestDTO.getMeetingTypeId() == 1) {
			internal = false;
			external = true;

		} else if (meetingAddRequestDTO.getMeetingTypeId() == 2) {
			internal = true;
			external = false;

		} else if (meetingAddRequestDTO.getMeetingTypeId() == 3) {
			internal = false;
			external = false;

		}

	}

	public void cancelviewnote() {
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('notedialog').hide();");
	}

	private Date todayDate = new Date();

	public Date getTodayDate() {
		return todayDate;
	}

	public void clear() {
//		meetingAddRequestDTO = new MeetingAddRequestDTO();
//		employeelist = new ArrayList<>();
//		extenalEmployeeList = new ArrayList<>();
		
		selctedentitytypeid = new EntityTypeMaster();		
		headangregionaofficeid = new EntityMaster();
		meetingentitytypelist=new ArrayList<>();
		meetingentitylist=new ArrayList<>();
		departmentList=new ArrayList<>();
//		department = null;
		entityid = 0L;
		totalatendees=(long)employeelist.size();
		sectionMasterList=new ArrayList<>();
		meetingsectionlist=new ArrayList<>();
		//sectionMaster = null;
//		headangregionaofficeid
	}
	
	public void loadEmployeeDetails()
	{
		departmentList=new ArrayList<>();
		sectionMasterList=new ArrayList<>();
		selctedentitytypeid = new EntityTypeMaster();		
		headangregionaofficeid = new EntityMaster();
		entityid = null;
	}
	
	
	public List<SectionMaster> getActiveSectionMastersByDepartment() {
		log.info("getActiveSectionMastersByDepartment Start===>");
		List<SectionMaster> sectionMasterList=new ArrayList<SectionMaster>();
		try {
			BaseDTO relationShipBaseDTO = new BaseDTO();
			String url = AppUtil.getPortalServerURL() + "/sectionmaster/getAllActiveByDepartmentList/";
			log.info("getActiveSectionMasters url===> " + url);
			relationShipBaseDTO = httpService.post(url,departmentList);
			if (relationShipBaseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonResponse = mapper.writeValueAsString(relationShipBaseDTO.getResponseContents());
				sectionMasterList = mapper.readValue(jsonResponse, new TypeReference<List<SectionMaster>>() {
				});
			}
			log.info("sectionMasterList.size ===> " + sectionMasterList != null ? sectionMasterList.size() : 0);
			log.info("getActiveSectionMastersByDepartment end===>");

		} catch (Exception e) {
			log.error("getActiveSectionMastersByDepartment error==>", e);
		}
		return sectionMasterList;
	}
	
	public void updateInTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(meetingAddRequestDTO.getMeetingStartTime());
		int hours = calendar.get(Calendar.HOUR_OF_DAY);
		inTime=((long) hours);
		inTime=inTime+1;
		
	}
	
	@PostConstruct
	public String showViewListPage() {
		log.info("MeetingRequestBean showViewListPage() Starts");
		viewmeetingdetails = new MeetingAddRequestDTO();
		mapper = new ObjectMapper();
		ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
		Object requestObj = context.getRequest();
		String meetingRequestId = "";
		String notificationId = "";
		if (requestObj instanceof HttpServletRequest) {
			HttpServletRequest httpRequest = (HttpServletRequest) requestObj;
			meetingRequestId = httpRequest.getParameter("meetingRequestId");
			notificationId = httpRequest.getParameter("notificationId");
			log.info("Selected Notification Id :" + notificationId);
		}
		if (StringUtils.isNotEmpty(meetingRequestId)) {
			selectedmeeting = new MeetingListDTO();
			action="VIEW";
			selectedmeeting.setId(Long.parseLong(meetingRequestId));
		    
			selectedNotificationId = Long.parseLong(notificationId);
			viewDetails();
			
		}
		log.info("MeetingRequestBean showViewListPage() Ends");
		addButtonFlag = true;
		return VIEW_PAGE;
	}

}
