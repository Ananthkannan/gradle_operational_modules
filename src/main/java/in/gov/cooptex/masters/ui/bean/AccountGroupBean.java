package in.gov.cooptex.masters.ui.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.accounts.dto.GlAccountRequest;
import in.gov.cooptex.core.accounts.dto.GlAccountResponse;
import in.gov.cooptex.core.accounts.model.GlAccountCategory;
import in.gov.cooptex.core.accounts.model.GlAccountGroup;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("accountGroupBean")
@Scope("session")
public class AccountGroupBean implements Serializable {
	/**
	* 
	*/

	private static final long serialVersionUID = 1L;
	private final String ACCOUNT_GROUP_LIST_PAGE = "/pages/masters/listAccountGroup.xhtml?faces-redirect=true;";
	private final String ACCOUNT_GROUP_ADD_PAGE = "/pages/masters/accountGroupCreate.xhtml?faces-redirect=true;";
	private final String ACCOUNT_GROUP_VIEW_PAGE = "/pages/masters/viewAccountGroup.xhtml?faces-redirect=true;";
	
	public static final String SERVER_URL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action, categoryName;

	@Getter
	@Setter
	private BaseDTO baseDTO;

	@Getter
	@Setter
	boolean disableAddButton = false;

	@Getter
	@Setter
	LazyDataModel<GlAccountResponse> accountGroupResponseLazyList;

	@Getter
	@Setter
	List<GlAccountCategory> accountGroupCategoryList;

	@Getter
	@Setter
	GlAccountResponse glAccountResponse;

	@Getter
	@Setter
	private GlAccountCategory glAccountCategory;

	@Getter
	@Setter
	private GlAccountGroup glAccountGroup;

	@Getter
	@Setter
	Integer totalRecords;

	@Autowired
	ErrorMap errorMap;

	@Autowired
	HttpService httpService;

	@Autowired
	CommonDataService commonDataService;

	@Getter
	@Setter
	private List<GlAccountGroup> listGlAccountGroup;

	AccountGroupBean() {
		glAccountGroup = new GlAccountGroup();
	}

	public String showListAccountGroupPage() {
		log.info("showListAccountGroupPage called..");
		disableAddButton = false;
		loadAccountGroupLazyList();
		return ACCOUNT_GROUP_LIST_PAGE;
	}

	public String accountGroupListAction() {
		log.info("<====== AccountGroupBean.accountGroupListAction Starts====> action   :::" + action);
		try {
			if (action.equalsIgnoreCase("ADD")) {
				log.info("<====== account group add page called.......====>");
				glAccountCategory = new GlAccountCategory();
				glAccountGroup = new GlAccountGroup();
				//accountGroupCategoryList = commonDataService.findAllGlCategory();
				findAllGlCategory();
				return ACCOUNT_GROUP_ADD_PAGE;
			}
		} catch (Exception e) {
			log.error("Exception Occured in AccountGroupBean.accountGroupListAction::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		log.info("<====== AccountGroupBean.accountGroupListAction Ends====>");
		return null;
	}
	
	public List<GlAccountCategory> findAllGlCategory() {
		log.info("findAllGlCategory started");
		try {
			String URL = SERVER_URL+"/accounts/get/allcategory";
			
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				accountGroupCategoryList = mapper.readValue(jsonResponse, new TypeReference<List<GlAccountCategory>>() {
				});

				log.info("findAllGlCategory finished");
			}
		} catch (Exception exception) {
			log.error("findAllGlCategory ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return accountGroupCategoryList;
	}

	public void loadAccountGroupCategory() {
		log.info("<<====  loadAccountGroupCategory =====>> " + glAccountCategory.getId());
		try {
			String url = AppUtil.getPortalServerURL() + "/accounts/group/getByAccountGroupParent/"
					+ glAccountCategory.getId();
			baseDTO = httpService.get(url);
			jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
			listGlAccountGroup = mapper.readValue(jsonResponse, new TypeReference<List<GlAccountGroup>>() {
			});
		} catch (Exception e) {
			log.error("<<=====  loadAccountGroupCategory error====>>" + e);
		}
	}

	public String submitGlAccountGroup() {
		log.info("<<====  submitGlAccountGroup STARTS ====>>");
		try {
			if (action.equalsIgnoreCase("ADD")) {
				if (glAccountGroup.getGlAccountGroupParent() != null) {
					glAccountGroup.setAccountGroupParentId(glAccountGroup.getGlAccountGroupParent().getId());
				}
				glAccountGroup.setStatus(true);
				glAccountGroup.setCategoryId(glAccountCategory.getId());
				String url = AppUtil.getPortalServerURL() + "/accounts/group/accountgroupcreate";
				baseDTO = httpService.post(url, glAccountGroup);
			}
			if (action.equalsIgnoreCase("EDIT")) {
				String url = AppUtil.getPortalServerURL() + "/accounts/group/accountgroupupdate";
				log.info("url" + url);
				if (glAccountGroup.getGlAccountGroupParent() != null) {
					glAccountGroup.setAccountGroupParentId(glAccountGroup.getGlAccountGroupParent().getId());
				}
				glAccountGroup.setCategoryId(glAccountCategory.getId());
				baseDTO = httpService.post(url, glAccountGroup);
			}
			log.info("BaseDTO Status Code " + baseDTO.getStatusCode());
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					if (action.equalsIgnoreCase("ADD")) {
						errorMap.notify(ErrorDescription.ACCOUNT_GROUP_SAVE_SUCCESS.getErrorCode());
					}
					if (action.equalsIgnoreCase("EDIT")) {
						errorMap.notify(ErrorDescription.ACCOUNT_GROUP_UPDATE_SUCCESS.getErrorCode());
					}
				} else {
					log.error("Status code:" + baseDTO.getStatusCode());
					errorMap.notify(baseDTO.getStatusCode());
					return null;
				}
			}
			log.info("<<====  submitGlAccountGroup ENDS ====>>");
		} catch (Exception exp) {
			log.error("Exception ", exp);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}

		return showListAccountGroupPage();
	}

	public void loadAccountGroupLazyList() {

		log.info("Lazy load == Account Group Master  ==>>");
		accountGroupResponseLazyList = new LazyDataModel<GlAccountResponse>() {

			private static final long serialVersionUID = 2784959485860775580L;

			@Override
			public List<GlAccountResponse> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				List<GlAccountResponse> data = null;
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					ObjectMapper objectMapper = new ObjectMapper();
					if (baseDTO != null) {
						String jsonResponse = objectMapper.writeValueAsString(baseDTO.getResponseContent());
						data = objectMapper.readValue(jsonResponse, new TypeReference<List<GlAccountResponse>>() {
						});
					}
					if (data != null) {
						this.setRowCount(baseDTO.getTotalRecords());
						totalRecords = baseDTO.getTotalRecords();
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}

				return data;

			}

			@Override
			public Object getRowKey(GlAccountResponse res) {
				log.info("Get Row Key called" + res);
				return res != null ? res.getId() : null;
			}

			@Override
			public GlAccountResponse getRowData(String rowKey) {
				log.info("Get Row Data called" + rowKey);
				List<GlAccountResponse> responseList = (List<GlAccountResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);

				for (GlAccountResponse res : responseList) {
					if (res.getId().longValue() == value.longValue()) {
						log.info("Returning row data " + res);
						return res;
					}
				}
				log.info("Returning null row data ");
				return null;
			}

		};
	}

	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) throws ParseException {
		log.info("<---Inside search called--->");

		log.info("First [" + first + "] pageSize [" + pageSize + "] sortOrder [" + sortOrder + "] sortField ["
				+ sortField + "]");

		glAccountResponse = new GlAccountResponse();

		BaseDTO baseDTO = new BaseDTO();

		GlAccountRequest request = new GlAccountRequest();
		PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
		request.setPaginationDTO(paginationDTO);

		for (Map.Entry<String, Object> entry : filters.entrySet()) {
			String value = entry.getValue().toString();
			String key = entry.getKey();

			log.info("Key : " + key + " Value : " + value);

			if (key.equals("categeoryCodeOrName")) {
				request.setCategoryCodeOrName(value);
			} else if (key.equals("groupCodeOrName")) {
				request.setGroupCodeOrName(value);
			} else if (key.equals("codeOrName")) {
				request.setCodeOrName(value);
			} else if (key.equals("localName")) {
				request.setLocalName(value);
			} else if (key.equals("activeStatus")) {
				request.setActiveStatus(value.equalsIgnoreCase("true") ? true : false);
			} else if (key.equals("createdDate")) {
				request.setCreatedDate(AppUtil.serverDateFormat(value));
			}
		}

		try {
			String url = AppUtil.getPortalServerURL() + "/accounts/group/searchGlAccountGroup";
			log.info("<<=====  URL::: " + url);
			baseDTO = httpService.post(url, request);

		} catch (Exception e) {
			log.error("Exception ", e);
			baseDTO.setStatusCode(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return baseDTO;
	}

	public String showViewForm() {
		if (glAccountResponse == null || glAccountResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ACCOUNT_GROUP.getErrorCode());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('growlWarnDlg').show();");
			return null;
		}
		log.info("id---> " + glAccountResponse.getId());
		glAccountGroup = getAccountGroupById(glAccountResponse.getId());
		log.info("parent id" + glAccountGroup.getParentGroupCodeName() + glAccountGroup.getAccountGroupParentId()
				+ "----");
		log.info("category id" + glAccountGroup.getCategoryName());
		log.info("parent id===++++++===========>" + glAccountGroup.getGlAccountGroupParent());

		return ACCOUNT_GROUP_VIEW_PAGE;
	}

	public void onRowSelect(SelectEvent event) {
		log.info("onRowSelect method started");
		glAccountResponse = ((GlAccountResponse) event.getObject());
		disableAddButton = true;
	}

	public GlAccountGroup getAccountGroupById(Long id) {
		String URL = AppUtil.getPortalServerURL() + "/accounts/group/accountGroup/get/" + id;
		GlAccountGroup glAccountGroup = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				log.info("json value======>" + jsonValue);
				glAccountGroup = mapper.readValue(jsonValue, GlAccountGroup.class);
				log.info("Account Group Code-----> " + glAccountGroup.getCode());
				log.info("Account Group Code-----> " + glAccountGroup.getCategoryId());
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return glAccountGroup;
	}

	public void glAccountGroupDelete() {
		if (glAccountResponse == null || glAccountResponse.getId() == null) {
			RequestContext.getCurrentInstance().execute("PF('dlg1').hide();");
			errorMap.notify(ErrorDescription.SELECT_ACCOUNT_GROUP.getErrorCode());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('growlWarnDlg').show();");
		} else {
			RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		}
	}

	public String delete() {
		String URL = AppUtil.getPortalServerURL() + "/accounts/group/accountGroup/delete/" + glAccountResponse.getId();
		log.info("url" + URL);
		try {
			log.info("id---> " + glAccountResponse.getId());
			glAccountGroup = getAccountGroupById(glAccountResponse.getId());
			log.info("parent id" + glAccountGroup.getParentGroupCodeName() + glAccountGroup.getAccountGroupParentId()
					+ "----");
			log.info("category id" + glAccountGroup.getCategoryName());
			log.info("parent id===++++++===========>" + glAccountGroup.getGlAccountGroupParent());
			BaseDTO baseDTO = httpService.delete(URL);

			if (baseDTO != null) {
				log.info("Deleted successfully");
				disableAddButton = false;
				errorMap.notify(baseDTO.getStatusCode());
				return "";
			}
		} catch (Exception e) {
			log.error("Exception ", e);
			errorMap.notify(baseDTO.getStatusCode());
			return null;
		}

		return null;
	}

	public GlAccountGroup getAccountGroupParentById(Long id) {
		String URL = AppUtil.getPortalServerURL() + "/accounts/group/getAccountGroupParentbyId/" + id;
		GlAccountGroup glAccountGroupParent = null;
		try {
			BaseDTO baseDTO = httpService.get(URL);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				glAccountGroupParent = mapper.readValue(jsonValue, GlAccountGroup.class);
			}
		} catch (Exception exception) {
			log.error("Exception ", exception);
			errorMap.notify(ErrorDescription.FAILURE_RESPONSE.getCode());
		}
		return glAccountGroupParent;
	}

	public String gotoEditPage() {
		log.info("action" + action);
		if (glAccountResponse == null || glAccountResponse.getId() == null) {
			errorMap.notify(ErrorDescription.SELECT_ACCOUNT_GROUP.getErrorCode());
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("PF('growlWarnDlg').show();");
			return null;
		}
		try {
			accountGroupCategoryList = findAllGlCategory();
			glAccountGroup = getAccountGroupById(glAccountResponse.getId());
			String url = AppUtil.getPortalServerURL() + "/accounts/group/getAccountGroupCategorybyId/"
					+ glAccountGroup.getCategoryId();
			BaseDTO baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				String jsonValue;
				jsonValue = mapper.writeValueAsString(baseDTO.getResponseContent());
				glAccountCategory = mapper.readValue(jsonValue, GlAccountCategory.class);
			}
			if (glAccountGroup.getAccountGroupParentId() != null) {
				GlAccountGroup parent = getAccountGroupParentById(glAccountGroup.getAccountGroupParentId());
				log.info("parent..." + parent.getId());
				glAccountGroup.setGlAccountGroupParent(parent);
			}
			glAccountCategory.setId(glAccountGroup.getCategoryId());
			loadAccountGroupCategory();
		} catch (Exception e) {
			log.error("Exception ", e);
		}
		return ACCOUNT_GROUP_ADD_PAGE;
	}

}
