package in.gov.cooptex.personnel.admin.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.common.ui.service.CommonDataService;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.model.AllowanceGradeCity;
import in.gov.cooptex.core.model.EmployeeTypeMaster;
import in.gov.cooptex.core.model.LeaveTypeMaster;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.util.Util;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.exceptions.MastersErrorCode;
import in.gov.cooptex.personnel.payroll.model.CityCompensatoryAllowance;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("cityCompensatoryAllowanceBean")
@Scope("session")
public class CityCompensatoryAllowanceBean {

	private final String CREATE_CITY_COMPENSATE_PAGE = "/pages/personnelHR/payRoll/createCityCompenAllowance.xhtml?faces-redirect=true;";
	private final String CITY_COMPENSATE_LIST_URL = "/pages/personnelHR/payRoll/listCityCompenAllowance.xhtml?faces-redirect=true;";
	private final String CITY_COMPENSATE_VIEW_URL = "/pages/personnelHR/payRoll/viewCityCompenAllowance.xhtml?faces-redirect=true;";

	private String serverURL = AppUtil.getPortalServerURL();

	ObjectMapper mapper = new ObjectMapper();

	String jsonResponse;

	@Getter
	@Setter
	String action;

	@Autowired
	HttpService httpService;

	@Autowired
	ErrorMap errorMap;

	@Getter
	@Setter
	CityCompensatoryAllowance cityCompensatoryAllowance, selectedCityCompensatoryAllowance;

	@Getter
	@Setter
	LazyDataModel<CityCompensatoryAllowance> lazyCityCompensatoryAllowanceList;

	List<CityCompensatoryAllowance> cityCompensatoryAllowanceList = new ArrayList<CityCompensatoryAllowance>();

	@Getter
	@Setter
	int totalRecords = 0;

	@Getter
	@Setter
	Boolean addButtonFlag = true;

	@Getter
	@Setter
	Boolean editButtonFlag = true;

	@Getter
	@Setter
	Boolean deleteButtonFlag = true;

	@Getter
	@Setter
	List<AllowanceGradeCity> allowanceGradeCityList = new ArrayList<AllowanceGradeCity>();

	@Getter
	@Setter
	List<LeaveTypeMaster> leaveTypeMasterList = new ArrayList<LeaveTypeMaster>();

	@Autowired
	LoginBean loginBean;

	@Autowired
	CommonDataService commonDataService;
	
	@Getter @Setter
	List<EmployeeTypeMaster> employeeTypeMasterList;

	public String showCityCompensatoryAllowanceListPage() {
		log.info("<==== Starts CityCompensatoryAllowanceBean.showCityCompensatoryAllowanceListPage =====>");
		mapper = new ObjectMapper();
		editButtonFlag = true;
		deleteButtonFlag = true;
		addButtonFlag = true;
		cityCompensatoryAllowance = new CityCompensatoryAllowance();
		selectedCityCompensatoryAllowance = new CityCompensatoryAllowance();
		loadLazyCityCompensatoryAllowanceList();
		log.info("<==== Ends CityCompensatoryAllowanceBean.showCityCompensatoryAllowanceListPage =====>");
		return CITY_COMPENSATE_LIST_URL;
	}

	public void loadLazyCityCompensatoryAllowanceList() {
		log.info("<===== Starts CityCompensatoryAllowanceBean.loadLazyCityCompensatoryAllowanceList ======>");
		lazyCityCompensatoryAllowanceList = new LazyDataModel<CityCompensatoryAllowance>() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -7079199200346366750L;

			@Override
			public List<CityCompensatoryAllowance> load(int first, int pageSize, String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {
				try {
					PaginationDTO paginationRequest = new PaginationDTO(first / pageSize, pageSize, sortField,
							sortOrder.toString(), filters);
					log.info("Pagination request :::" + paginationRequest);
					String url = serverURL + "/citycompensate/searchandload";
					log.info("Lazy load url is----------->>>>>" + url);
					BaseDTO response = httpService.post(url, paginationRequest);
					if (response != null && response.getStatusCode() == 0) {
						jsonResponse = mapper.writeValueAsString(response.getResponseContents());
						cityCompensatoryAllowanceList = mapper.readValue(jsonResponse,
								new TypeReference<List<CityCompensatoryAllowance>>() {
								});
						this.setRowCount(response.getTotalRecords());
						totalRecords = response.getTotalRecords();
						log.info("Total Record Count----------->>>>>" + totalRecords);
					} else {
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
					}
				} catch (Exception e) {
					log.error("Exception occured in loadLazyCityCompensatoryAllowanceList ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				log.info("Ends lazy load....");
				return cityCompensatoryAllowanceList;
			}

			@Override
			public Object getRowKey(CityCompensatoryAllowance res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public CityCompensatoryAllowance getRowData(String rowKey) {
				try {
					for (CityCompensatoryAllowance cityCompensatoryAllowance : cityCompensatoryAllowanceList) {
						if (cityCompensatoryAllowance.getId().equals(Long.valueOf(rowKey))) {
							selectedCityCompensatoryAllowance = cityCompensatoryAllowance;
							return cityCompensatoryAllowance;
						}
					}
				} catch (Exception e) {
					log.error("Exception occured in getRowData ...", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

		};
		log.info("<===== Ends CityCompensatoryAllowanceBean.loadLazyCityCompensatoryAllowanceList ======>");
	}

	public String showLeaveEligiblityListPageAction() {
		log.info("<===== Starts CityCompensatoryAllowanceBean.showAccountCategoryListPageAction ===========>" + action);
		try {
			if (!action.equalsIgnoreCase("CREATE") && (selectedCityCompensatoryAllowance == null
					|| selectedCityCompensatoryAllowance.getId() == null)) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
				return null;
			}
			if (action.equalsIgnoreCase("CREATE")) {
				log.info("create CityCompensatoryAllowance called..");
				cityCompensatoryAllowance = new CityCompensatoryAllowance();
				allowanceGradeCityList = loadAllowanceGradeCity();
				employeeTypeMasterList = commonDataService.getAllEmployeeType();
				return CREATE_CITY_COMPENSATE_PAGE;
			} else if (action.equalsIgnoreCase("EDIT")) {
				log.info("edit CityCompensatoryAllowance called..");
				allowanceGradeCityList = loadAllowanceGradeCity();
				employeeTypeMasterList = commonDataService.getAllEmployeeType();
				log.info("selectedCityCompensatoryAllowance id is" + selectedCityCompensatoryAllowance.getId());
				String url = serverURL + "/citycompensate/" + selectedCityCompensatoryAllowance.getId();
				log.info("Edit url is----------->>>>>" + url);
				BaseDTO response = httpService.get(url);
				if (response != null && response.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(response.getResponseContent());
					cityCompensatoryAllowance = mapper.readValue(jsonResponse,
							new TypeReference<CityCompensatoryAllowance>() {
							});
					return CREATE_CITY_COMPENSATE_PAGE;
				} else if (response != null && response.getStatusCode() != 0) {
					errorMap.notify(response.getStatusCode());
				}
			} else if (action.equalsIgnoreCase("DELETE")) {
				log.info("delete CityCompensatoryAllowance called..");
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCityCompensatorDelete').show();");
			} else if (action.equalsIgnoreCase("VIEW")) {
				log.info("view CityCompensatoryAllowance called..");
				log.info("selectedCityCompensatoryAllowance id is" + selectedCityCompensatoryAllowance.getId());
				String viewurl = serverURL + "/citycompensate/" + selectedCityCompensatoryAllowance.getId();
				log.info("View url is----------->>>>>" + viewurl);
				BaseDTO viewresponse = httpService.get(viewurl);
				if (viewresponse != null && viewresponse.getStatusCode() == 0) {
					jsonResponse = mapper.writeValueAsString(viewresponse.getResponseContent());
					cityCompensatoryAllowance = mapper.readValue(jsonResponse,
							new TypeReference<CityCompensatoryAllowance>() {
					});
					return CITY_COMPENSATE_VIEW_URL;
				}
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured in showAccountCategoryListPageAction ..", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
		}
		log.info("<===== Ends CityCompensatoryAllowanceBean.showAccountCategoryListPageAction ===========>" + action);
		return null;
	}

	public String saveOrUpdate() {
		log.info("<==== Starts CityCompensatoryAllowanceBean.saveOrUpdate =======>");
		try {
			if (cityCompensatoryAllowance.getAllowanceGradeCity() == null) {
				errorMap.notify(ErrorDescription
						.getError(MastersErrorCode.CITY_COMPENSATORY_ALLOWANCE_GRADLECITY_REQUIRED).getCode());
				return null;
			}
			if (cityCompensatoryAllowance.getEmployeeTypeMaster() == null) {
				errorMap.notify(ErrorDescription.EMPLOYEE_TYPE_NOT_NULL.getCode());
				return null;
			}
			if (cityCompensatoryAllowance.getPayRangeFrom() > cityCompensatoryAllowance.getPayRangeTo()) {
				log.error("PayRangeFrom Should Be less than PayRangeTo");
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.PAYRANGE_RESTRICTION).getCode());
				return null;
			}
			if (cityCompensatoryAllowance.getWithEffectFrom() == null) {
				errorMap.notify(ErrorDescription.EMP_SUSPENSION_DETAILS_FROM_DATE_REQUIRED.getCode());
				return null;
			}
			
			if (action.equalsIgnoreCase("EDIT")) {
				cityCompensatoryAllowance.setModifiedBy(loginBean.getUserDetailSession());
			}
			String url = serverURL + "/citycompensate/saveorupdate";
			log.info("saveOrUpdate url is" + url);
			BaseDTO response = httpService.put(url, cityCompensatoryAllowance);
			if (response != null && response.getStatusCode() == 0) {
				log.info("account category saved successfully..........");
				if (action.equalsIgnoreCase("CREATE")) {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CITY_COMPENSATORY_SAVE_SUCCESS).getCode());
				} else {
					errorMap.notify(
							ErrorDescription.getError(MastersErrorCode.CITY_COMPENSATORY_UPDATE_SUCCESS).getCode());
				}
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
				return null;
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while save or update .......", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			return null;
		}
		log.info("<==== Ends CityCompensatoryAllowanceBean.saveOrUpdate =======>");
		return showCityCompensatoryAllowanceListPage();
	}

	public void onRowSelect(SelectEvent event) {
		log.info("<===Starts CityCompensatoryAllowanceBean.onRowSelect ========>" + event);
		selectedCityCompensatoryAllowance = ((CityCompensatoryAllowance) event.getObject());
		addButtonFlag = false;
		editButtonFlag = true;
		deleteButtonFlag = true;
		log.info("<===Ends CityCompensatoryAllowanceBean.onRowSelect ========>");
	}

	public String deleteConfirm() {
		log.info("<===== Starts CityCompensatoryAllowanceBean.deleteConfirm ===========>");
		try {
			log.info("selectedCityCompensatoryAllowance id is" + selectedCityCompensatoryAllowance.getId());
			String url = serverURL + "/citycompensate/delete/" + selectedCityCompensatoryAllowance.getId();
			log.info("deleteConfirm url is" + url);
			BaseDTO response = httpService.delete(url);
			if (response != null && response.getStatusCode() == 0) {
				errorMap.notify(ErrorDescription.getError(MastersErrorCode.CITY_COMPENSATORY_DELETE_SUCCESS).getCode());
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmCityCompensatorDelete').hide();");
			} else if (response != null && response.getStatusCode() != 0) {
				errorMap.notify(response.getStatusCode());
			} else {
				errorMap.notify(ErrorDescription.INTERNAL_ERROR.getCode());
			}
		} catch (Exception e) {
			log.error("Exception occured while delete Leave Eligiblity ....", e);
			errorMap.notify(ErrorDescription.ERROR_GENERIC.getCode());
		}
		log.info("<===== Ends CityCompensatoryAllowanceBean.deleteConfirm ===========>");
		return showCityCompensatoryAllowanceListPage();
	}

	public String clear() {
		log.info("<===== Enter into CityCompensatoryAllowanceBean.clear ===========>");
		cityCompensatoryAllowance = new CityCompensatoryAllowance();
		log.info("<===== Exit into CityCompensatoryAllowanceBean.clear ===========>");
		return showCityCompensatoryAllowanceListPage();
	}

	public List<AllowanceGradeCity> loadAllowanceGradeCity() {
		log.info("loadAllowanceGradeCity==>");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + "/citycompensate/getallallowanceGrade";
			log.info("loadAllowanceGradeCity url==>" + url);
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContents());
				allowanceGradeCityList = mapper.readValue(jsonResponse, new TypeReference<List<AllowanceGradeCity>>() {
				});
			}

		} catch (Exception e) {
			log.info("Error in loadAllowanceGradeCity()>>>>>>>>" + e);
		}
		return allowanceGradeCityList;
	}

}
