package in.gov.cooptex.admin.vehicle.ui;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.gov.cooptex.admin.vehicle.model.VehicleArrival;
import in.gov.cooptex.admin.vehicle.model.VehicleDeparture;
import in.gov.cooptex.core.config.AppPreference;
import in.gov.cooptex.core.dto.BaseDTO;
import in.gov.cooptex.core.dto.PaginationDTO;
import in.gov.cooptex.core.dto.VehicleArrivalSearchRequest;
import in.gov.cooptex.core.dto.VehicleArrivalSearchResponse;
import in.gov.cooptex.core.dto.VehicleDepartureSearchRequest;
import in.gov.cooptex.core.dto.VehicleDepartureSearchResponse;
import in.gov.cooptex.core.ui.LoginBean;
import in.gov.cooptex.core.util.ErrorMap;
import in.gov.cooptex.core.utilities.AppUtil;
import in.gov.cooptex.core.utilities.HttpService;
import in.gov.cooptex.exceptions.AdminErrorCode;
import in.gov.cooptex.exceptions.ErrorDescription;
import in.gov.cooptex.personnel.rest.util.Util;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service("vehicleArrivalBean")
@Scope("session")
public class VehicleArrivalBean {
	
	private final String VEHICLE_ARRIVAL_CREATE_PAGE = "/pages/admin/vehicleManagement/createVehicleArrival.xhtml?faces-redirect=true;";
	//private final String VEHICLE_MANAGEMENT_LIST_PAGE = "/pages/admin/vehicleManagement/listVehicle.xhtml?faces-redirect=true;";
	
	private final String VEHICLE_MANAGEMENT_EDIT_PAGE = "/pages/admin/vehicleManagement/editVehicleArrival.xhtml?faces-redirect=true;";
	private final String VEHICLE_MANAGEMENT_VIEW_PAGE = "/pages/admin/vehicleManagement/viewVehicleArrival.xhtml?faces-redirect=true;";
	private final String VEHICLE_ARRIVAL_LIST_PAGE = "/pages/admin/vehicleManagement/listVehicleArrival.xhtml?faces-redirect=true;";

	private String serverURL = null;

	@Autowired
	AppPreference appPreference;

	@Autowired
	HttpService httpService;
	
	@Getter
	@Setter
	VehicleArrival vehicleArrival;
	
	@Getter
	@Setter
	Double totalkilometer;
	
	
	
	@Getter
	@Setter
	LazyDataModel<VehicleArrivalSearchResponse> vehicleArrivalResponseLazyList;
	
	@Autowired
	ErrorMap errorMap;
	
	@Getter
	@Setter
	VehicleArrivalSearchResponse selectedvehicleArrivaleResponse;
	
	
	@Getter
	@Setter
	int totalRecords;
	
	boolean forceLogin = false;
	

	@Autowired
	LoginBean loginBean;
	
	@Getter
	@Setter
	private String jsonResponse;
	
	@Getter
	@Setter
	String action;
	
	
	@Getter
	@Setter
	VehicleDepartureBean vehicledepartureBean;
	
	@Getter
	@Setter
	VehicleDeparture vehicleDeparture;
	
	@Getter
	@Setter
	VehicleManagementBean vehicleManagementBean;
	
	@PostConstruct
	public void init() {
		serverURL=appPreference.getPortalServerURL();
		loadLazyVehicleArrivalList();
	}
	
	
	
	public String createArival() {
		log.info("<<<< ----------Start TapalDeliveryTypeBean . getTapalDeliveryTypeView() ------- >>>>");
		if(vehicleManagementBean.selectedvehicledepartureResponse!=null) { 
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicleDeparture=new VehicleDeparture();
			
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclemanagement/getvehicleDeparture";
			baseDTO = httpService.post(url, vehicleManagementBean.selectedvehicledepartureResponse);
			vehicleDeparture = new VehicleDeparture();
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleDeparture = mapper.readValue(jsonResponse, new TypeReference<VehicleDeparture>() {
				});
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}

		if (vehicleDeparture != null)
			log.info("vehicleManagement getvehicleManagementView" + vehicleDeparture);
		else
			log.info("vehicleManagement getvehicleManagementView" + vehicleDeparture);

		log.info("<<<< ----------End vehicleManagement . getvehicleManagementView ------- >>>>");
		return VEHICLE_ARRIVAL_CREATE_PAGE;
		}
		
		else {
			Util.addWarn("Please Select Vehicle Departure");
			return VEHICLE_ARRIVAL_LIST_PAGE;
		}
	}
	

	public String showListPage() {
		log.info("<==== Starts ListPage =====>");
		ObjectMapper	mapper = new ObjectMapper();
		boolean editButtonFlag = true;
		vehicleArrival = new VehicleArrival();
		selectedvehicleArrivaleResponse = new VehicleArrivalSearchResponse();
		loadLazyVehicleArrivalList();
		log.info("<==== Ends ListPage =====>");

		return VEHICLE_ARRIVAL_LIST_PAGE;
	}
	
	public  String view() {
		if(selectedvehicleArrivaleResponse == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		return null;
		}
		getarrival();
		return VEHICLE_MANAGEMENT_VIEW_PAGE;

	}
	
	public  String edit() { 
		if(selectedvehicleArrivaleResponse == null) {
			errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
		return null;
		}
		getarrival();
		return VEHICLE_MANAGEMENT_EDIT_PAGE;

	}
	
	public String back() {
		return VEHICLE_ARRIVAL_LIST_PAGE;
	}
	
	
	public void loadLazyVehicleArrivalList() {

		log.info("< --  Start VehicleArrival Lazy Load -- > ");
		vehicleArrivalResponseLazyList = new LazyDataModel<VehicleArrivalSearchResponse>() {
			private static final long serialVersionUID = 1L;

			@Override
			public List<VehicleArrivalSearchResponse> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {
				try {
					BaseDTO baseDTO = getSearchData(first / pageSize, pageSize, sortField, sortOrder, filters);

					if (baseDTO == null) {
						log.error(" Base DTO returned Null Values ");
						errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						return null;
					}
					if (baseDTO != null) {
						ObjectMapper mapper = new ObjectMapper();
						mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
						jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
						List<VehicleArrivalSearchResponse> data = mapper.readValue(jsonResponse,
								new TypeReference<List<VehicleArrivalSearchResponse>>() {
								});
						if (data == null) {
							log.info(" VehicleDepartureSearchResponse Failed to Deserialize");
							errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
						}

						if (baseDTO.getStatusCode() == 0) {
							this.setRowCount(baseDTO.getTotalRecords());
							totalRecords = baseDTO.getTotalRecords();
							log.info(" Vehicle Departure Search  Successfully Completed");
						} else {
							log.error(
									" Vehicle Departure  Search Failed With status code :: " + baseDTO.getStatusCode());
							errorMap.notify(baseDTO.getStatusCode());
						}
						return data;
					}
				} catch (Exception e) {
					log.error("ExceptionException Ocured while Loading Vehicle Departure data :: ", e);
					errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
				}
				return null;
			}

			@Override
			public Object getRowKey(VehicleArrivalSearchResponse res) {
				return res != null ? res.getId() : null;
			}

			@Override
			public VehicleArrivalSearchResponse getRowData(String rowKey) {
				List<VehicleArrivalSearchResponse> responseList = (List<VehicleArrivalSearchResponse>) getWrappedData();
				Long value = Long.valueOf(rowKey);
				for (VehicleArrivalSearchResponse res : responseList) {
					if (res.getId() == value.longValue()) {
						selectedvehicleArrivaleResponse = res;
						return res;
					}
				}
				return null;
			}

		};

	}
	
	
	
	public BaseDTO getSearchData(int first, int pageSize, String sortField, SortOrder sortOrder,
			Map<String, Object> filters) {
		BaseDTO baseDTO = new BaseDTO();

		try {
			log.info("Inside search called page:[" + first + "] " + "pageSize:[" + pageSize + "] " + "sortOrder:["
					+ sortOrder + "] " + "sortField:[" + sortField + "]");

			VehicleArrivalSearchRequest request = new VehicleArrivalSearchRequest();
			PaginationDTO paginationDTO = new PaginationDTO(first, pageSize, sortField, sortOrder.toString());
			request.setPaginationDTO(paginationDTO);

			for (Map.Entry<String, Object> entry : filters.entrySet()) {
				String value = entry.getValue().toString();
				log.info("Key : " + entry.getKey() + " Value : " + value);

				if (entry.getKey().equals("vehicleRegistrarNo")) {
					log.info("vehicleRegistrarNo : " + value);
					request.setVehicleRegistrarNo(value);
				}

				if (entry.getKey().equals("driverName")) {
					log.info("employeeName : " + value);
					request.setDriverName(value);
				}
				if (entry.getKey().equals("destination")) {
					log.info("destination : " + value);
					request.setDestination(value);
				}

				if (entry.getKey().equals("employeeName")) {
					log.info("employeeName : " + value);
					request.setEmployeeName(value);
				}
				if (entry.getKey().equals("vehicleName")) {
					log.info("vehicleName : " + value); 
					request.setVehicleName(value);
				}
				if (entry.getKey().equals("arrivalDate")) {
					log.info("arrivalDate : " + value);
					request.setArrivalDate(AppUtil.serverDateFormat(value));
				}

			}
			log.info("Search request data" + request);
			String URL = AppUtil.getPortalServerURL() + appPreference.getOperationApiUrl() + "/vehiclearrival/search";
			baseDTO = httpService.post(URL, request);
			log.info("Search request response " + baseDTO);
		} catch (Exception e) {
			log.error("Exception Occured in Vehicle Departure  search ::", e);
			errorMap.notify(ErrorDescription.INTERNAL_ERROR.getErrorCode());
		}
		return baseDTO;
	}
	
	public String forceLogin() {
		log.info("force login called--->" + forceLogin);
		if (forceLogin) {
			ExternalContext extCon = FacesContext.getCurrentInstance().getExternalContext();
			extCon.invalidateSession();
			return "/login.xhtml?faces-redirect=true";
		}
		return null;
	}
	
	public String deleteVehicleArrival() {
		try {
			action = "Delete";
			if (selectedvehicleArrivaleResponse == null) {
				errorMap.notify(ErrorDescription.SELECT_ATLEAST_ONE_RECORD.getCode());
			} else {
				RequestContext context = RequestContext.getCurrentInstance();
				context.execute("PF('confirmVehicleArrivalDelete').show();");
			}
		} catch (Exception e) {
			log.error("Exception occured .... ", e);
		}
		return null;
	}
	
	
	public String deleteVehicleArrivalConfirm() {

		try {
			log.info("Delete confirmed for Vehicle Arrival [" + selectedvehicleArrivaleResponse + "]");
			BaseDTO baseDTO = new BaseDTO();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclearrival/delete";
			baseDTO = httpService.post(url, selectedvehicleArrivaleResponse);
			
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("Tapal Delivery Type deleted successfully....!!!");
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_ARRIVAL_DELETE_SUCCESSFULLY).getErrorCode());
					selectedvehicleArrivaleResponse = new VehicleArrivalSearchResponse();
				}
				else if (baseDTO.getStatusCode() == ErrorDescription.SAME_USER_UPDATE.getErrorCode()) {
					log.info("Same user update invalidating session-------->");
					forceLogin = true;
					return forceLogin();

				} else {
					log.info("Error occured in Rest api ... ");
					// Util.addError(baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
				}
			}
			showListPage();
		} catch (Exception e) {
			log.error("Exception occured while deleting user ....", e);
		}
		return null;
	}
	
	public void getarrival() {
		log.info("<<<< ----------Start getArrival() ------- >>>>");
		if(selectedvehicleArrivaleResponse!=null) {  
			serverURL=appPreference.getPortalServerURL();
		try {
			BaseDTO baseDTO = new BaseDTO();
			vehicleArrival=new VehicleArrival();
			long id=selectedvehicleArrivaleResponse.getId();
			String url = serverURL + appPreference.getOperationApiUrl() + "/vehiclearrival/get/"+id; 
		
			baseDTO = httpService.get(url);
			if (baseDTO != null) {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
				String jsonResponse = mapper.writeValueAsString(baseDTO.getResponseContent());
				vehicleArrival = mapper.readValue(jsonResponse, new TypeReference<VehicleArrival>() {});
				totalkilometer=vehicleArrival.getEndKilometer()-vehicleArrival.getVehicleDeparture().getStartKilometre();
				
			}
		} catch (JsonProcessingException jpEx) {
			log.error("Json processing exception occured while converting .... ", jpEx);
		} catch (IOException ioEx) {
			log.error("IO Exception occured .... ", ioEx);
		} catch (Exception e) {
			
			log.error("Exception occured .... ", e);
		}
	}
	}
	
	
	
	public String submitArrival() {
		log.info("...... vehicleArrival submit begin ....");
		try {
			BaseDTO baseDTO = new BaseDTO();
			String URL = serverURL + appPreference.getOperationApiUrl() + "/vehiclearrival/create";
 			vehicleArrival.setModifiedBy(loginBean.getUserDetailSession());
			vehicleArrival.setModifiedDate(new Date());
			if(vehicleArrival.getEndKilometer() <= vehicleArrival.getVehicleDeparture().getStartKilometre()) {
				//errorMap.notify(ErrorDescription.VEHICLE_END_KILOMETER_GREATER.getErrorCode());
				Util.addWarn("Please Enter EndKilometer greater than Started Kilometer");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			if(vehicleArrival.getVehicleDeparture().getDepartureDate().equals(vehicleArrival.getArrivalDate()))
			{
			if(vehicleArrival.getArrivalTime().before(vehicleArrival.getVehicleDeparture().getDepartureTime())) {
				Util.addWarn("Arrival Time should not be allowed for before departure  Time");
				RequestContext.getCurrentInstance().update("growls");
				return null;
			}
			}
			baseDTO = httpService.post(URL, vehicleArrival);
			if (baseDTO != null) {
				if (baseDTO.getStatusCode() == 0) {
					log.info("vehicleArrival saved successfully");
					showListPage();
					errorMap.notify(ErrorDescription.getError(AdminErrorCode.VEHICLE_ARRIVAL_UPDATE_SUCCESSFULLY).getErrorCode());
				} else {
					log.error("Status code:" + baseDTO.getStatusCode() + " Error Message: "
							+ baseDTO.getErrorDescription());
					errorMap.notify(baseDTO.getStatusCode());
					// return null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while creating vehicleArrival" + e);
		}
		log.info("...... vehicleArrival submit ended ....");
		return VEHICLE_ARRIVAL_LIST_PAGE;
	}
	
	
	
	public void gettotalkilometers() {
		if(vehicleArrival.getVehicleDeparture().getStartKilometre()<vehicleArrival.getEndKilometer()) {
			//totalkilometer	=vehicledeparture.getStartKilometre()+vehicleArrival.getEndKilometer();
			totalkilometer	= vehicleArrival.getEndKilometer()-vehicleArrival.getVehicleDeparture().getStartKilometre();
			log.info("totalkilometer-----------------"+totalkilometer);
		}
		else {
			log.info("End kilometer is less than start kilometer-----------------");
			totalkilometer=0.0;
			vehicleArrival.setEndKilometer(0d);
			Util.addWarn("Please Enter EndKilometer greater than Started Kilometer");
			RequestContext.getCurrentInstance().update("growls");
		}
		
	}
	
	public void validateArrival() {
		if(vehicleArrival.getArrivalDate().equals(vehicleArrival.getVehicleDeparture().getDepartureDate())) {
			if(vehicleArrival.getArrivalTime().before(vehicleArrival.getVehicleDeparture().getDepartureTime())) {
				vehicleArrival.setArrivalTime(null);
				AppUtil.addWarn("Vehicle Arrival Time Should not be Before the Departure Time On same Date");
			}
		}
	}

}
